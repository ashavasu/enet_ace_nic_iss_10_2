/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4port.c,v 1.7 2011/07/07 10:51:21 siva Exp $
 *
 * Description: This file contains all the functions called after algo processing
 *
 ***********************************************************************/
#ifndef _SECV4PORT_C_
#define _SECV4PORT_C_

#include "secv4com.h"
#include "secv4soft.h"

/******************************************************************************/
/*  Function Name : Secv4AlgoProcessPkt                                       */
/*  Description   : This is portable function which will take care of the     */
/*                : algorithm processing.Algorithm processing can be done     */
/*                : either software or hardware accelerator                   */
/*  Input(s)      : pSecv4TskletData -  Contains all the information needed to*/
/*                : invoke algorithm module calls/APIs.                       */
/*                : u4Mode - tunnel or transport                              */
/*                : u4Action - Encode or Decode                               */
/*  Output(s)     : None                                                      */
/*  Return Values : Returns the success and failure of posting                */
/******************************************************************************/
INT1
Secv4AlgoProcessPkt (tSecv4TaskletData * pSecv4TskletData, UINT4 u4Mode,
                     UINT4 u4Action)
{
    if (Secv4SoftProcessPkt (pSecv4TskletData, u4Mode, u4Action) == SEC_FAILURE)
    {
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}

/***********************************************************************/
/*  Function Name : RSecv4InitSesInHwAccel                             */
/*  Description   : This function Initializes the Session for the      */
/*                : Security Assocsciation and direction in the        */
/*                :                                                    */
/*                :                                                    */
/*  Input(s)      : u4SecAssocIndex - Security Assosciation Index      */
/*                : u4Action        - Direction (Encode/Decode)        */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/
INT1
Secv4InitSesInHwAccel (UINT4 u4SecAssocIndex, UINT4 u4Action)
{
    UNUSED_PARAM (u4SecAssocIndex);
    UNUSED_PARAM (u4Action);
    return SEC_SUCCESS;
}

/******************************************************************************/
/*  Function Name : Secv4RemSesInHwAccel                                      */
/*  Description   : This function will be called to Remove     a Session      */
/*                : in the hardware accelerator                               */
/*  Input(s)      : pSessionCtx - Pointer to the Session Context in the Hw Acc*/
/*  Output(s)     : None                                                      */
/*  Return Values : SEC_SUCCESS/SEC_FAILURE                                   */
/******************************************************************************/
INT1
Secv4RemSesInHwAccel (VOID *pSessionCtx)
{
    UNUSED_PARAM (pSessionCtx);
    return SEC_SUCCESS;
}

/***********************************************************************/
/*  Function Name :  Secv4HwInit                                       */
/*  Description   : This function gets the Initializes the list for    */
/*                : storing crypto requests in case of kernel tasklet  */
/*                :                                                    */
/*  Input(s)      : VOID - Input Nothing                               */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/
INT1
Secv4HwInit (VOID)
{
    return SEC_SUCCESS;
}

/***********************************************************************/
/*  Function Name : Secv4HwDeInit                                      */
/*  Description   : This function gets the De-Initializes the Engine    */
/*                : in the Hardware Accelerator                        */
/*                :                                                    */
/*  Input(s)      : VOID - Input Nothing                               */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/
INT1
Secv4HwDeInit (VOID)
{
    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Secv4MemInitCallBackCxt                           */
/*  Description     : This function allocates and Initializes Memory    */
/*                  : Pool call back context                            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SEC_SUCCESS                                       */
/************************************************************************/
INT1
Secv4MemInitCallBackCxt (VOID)
{
    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Secv4MemDeInitCallBackCxt                         */
/*  Description     : This function allocates and Releases    Memory    */
/*                  : Pool call back context                            */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                        */
/************************************************************************/
INT1
Secv4MemDeInitCallBackCxt (VOID)
{
    return SEC_SUCCESS;
}

#endif
