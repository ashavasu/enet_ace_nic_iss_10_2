/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4ip.c,v 1.14 2014/03/07 11:57:40 siva Exp $
 *
 * Description: This has functions for AH SubModule 
 *
 ***********************************************************************/

#include "secv4com.h"
#include "secv4soft.h"
#include "secv4esp.h"

#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif

/*****************************************************************************/
/*  Function Name : Secv4HandOverPktTOIP                                     */
/*  Description   : This function is invoked to verify whether correct set of*/
/*                  SA's are applied for decoding the packet and post the    */
/*                  decoded pkt to IP layer.                                 */
/*  Input(s)      : pData - Contains all info to pass pakt to IP             */
/*  Output(s)     : None                                                     */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                               */
/*****************************************************************************/
VOID
Secv4HandOverPktTOIP (VOID *pData)
{

    tSecv4TaskletData  *pTemp = NULL;
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         IpHdr;
    UINT1              *pu1Buf = NULL;
    INT1                i1Status = SEC_FAILURE;
    UINT1               u1RetStatus = SEC_FAILURE;
    UINT2               u2DstPortNumber = SEC_ANY_PORT;
    UINT2               u2SrcPortNumber = SEC_ANY_PORT;
    UINT4               u4OptLen = 0;
    UINT1               u1SecAssocMode = 0;
    UINT4               u4VpncIndex = 0;
    UINT1               u1SecAssocProtocol = 0;

    pTemp = (tSecv4TaskletData *) pData;

    if (pTemp == NULL)
    {
        return;
    }
    u1SecAssocMode = pTemp->pSaEntry->u1SecAssocMode;
    u4VpncIndex = pTemp->pSaEntry->u4VpncIndex;
    u1SecAssocProtocol = pTemp->pSaEntry->u1SecAssocProtocol;

    pu1Buf = (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pTemp->pCBuf, 0,
                                                     (SEC_IPV4_HEADER_SIZE +
                                                      sizeof (tSecv4UdpHdr)));
    /* Valid Offset is pointing to IP Header. */
    if (pu1Buf != NULL)
    {
        pIpHdr = (t_IP_HEADER *) (void *) pu1Buf;
        u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);
        if (pIpHdr->u1Proto == SEC_TCP || pIpHdr->u1Proto == SEC_UDP)
        {
            u2DstPortNumber =
                *((UINT2 *) (void *) (pu1Buf +
                                      (SEC_PORT_NUMBER_OFFSET + u4OptLen)));
            u2DstPortNumber = OSIX_NTOHS (u2DstPortNumber);
            u2SrcPortNumber =
                *((UINT2 *) (void *) (pu1Buf +
                                      (SEC_SRC_PORT_NUMBER_OFFSET + u4OptLen)));
            u2SrcPortNumber = OSIX_NTOHS (u2SrcPortNumber);
        }
        if (pIpHdr->u1Proto == SEC_NO_NEXT_HDR)
        {
            /* DUMMY PACKET RECEPTION: <VPN TRAFFIC FLOW CONFIDENTIALITY>
             * Packet is a Dummy packet and hence it should not be processed
             * further and is dropped */

            CRU_BUF_Release_MsgBufChain (pTemp->pCBuf, FALSE);
            Secv4UnLock ();
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pTemp);
            return;
        }

    }
    else
    {
        pIpHdr = &IpHdr;
        IPSEC_MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
        IPSEC_COPY_FROM_BUF (pTemp->pCBuf, (UINT1 *) pIpHdr, 0,
                             SEC_IPV4_HEADER_SIZE);
        u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);
        if (pIpHdr->u1Proto == SEC_TCP || pIpHdr->u1Proto == SEC_UDP)
        {
            CRU_BUF_Copy_FromBufChain (pTemp->pCBuf, (UINT1 *) &u2DstPortNumber,
                                       (SEC_PORT_NUMBER_OFFSET + u4OptLen),
                                       sizeof (UINT2));
            CRU_BUF_Copy_FromBufChain (pTemp->pCBuf, (UINT1 *) &u2SrcPortNumber,
                                       (SEC_SRC_PORT_NUMBER_OFFSET + u4OptLen),
                                       sizeof (UINT2));
        }
    }

    if (pIpHdr->u1Proto == SEC_NO_NEXT_HDR)
    {
        /* DUMMY PACKET RECEPTION: <VPN TRAFFIC FLOW CONFIDENTIALITY>
         * Packet is a Dummy packet and hence it should not be processed 
         * further and is dropped */

        CRU_BUF_Release_MsgBufChain (pTemp->pCBuf, FALSE);
        Secv4UnLock ();
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pTemp);
        return;
    }

    /* Verify Packet with Selector and Policy */
    i1Status =
        (INT1) (Secv4AssocVerify
                (IPSEC_NTOHL (pIpHdr->u4Src), IPSEC_NTOHL (pIpHdr->u4Dest),
                 pIpHdr->u1Proto, pTemp->u4IfIndex, u2SrcPortNumber,
                 u2DstPortNumber));

    /* General Stat Add */
    switch (i1Status)
    {
        case SEC_BYPASS:
            u1RetStatus = SEC_FAILURE;
            break;
        case SEC_FILTER:
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4HandOverPktTOIP: Packet Filterd\n");
            u1RetStatus = SEC_FAILURE;
            break;
        case SEC_APPLY:
            u1RetStatus = SEC_SUCCESS;
            break;
        default:
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4HandOverPktTOIP: Association Verify Fails \n");
            u1RetStatus = SEC_FAILURE;
            break;
    }

    if (u1RetStatus == SEC_FAILURE)
    {
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    Secv4UnLock ();

    Secv4PostInMsgToIP (pTemp->pCBuf, pTemp->u4IfIndex,
                        u1SecAssocMode, u4VpncIndex);
    Secv4UpdateIfStats (pTemp->u4IfIndex, SEC_INBOUND, SEC_APPLY);
    Secv4UpdateAhEspStats (pTemp->u4IfIndex, u1SecAssocProtocol,
                           SEC_INBOUND, SEC_ALLOW);
    MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pTemp);
    return;
}

/*****************************************************************************/
/*  Function Name : Secv4PostInMsgToIP                                       */
/*  Description   : This function is called from IPsec to post decoded msg to*/
/*                  IP.                                                      */
/*  Input(s)      : pCBuf -  Cru  Buffer                                  */
/*  Input(s)      : u4IfIndex - INterface on which the pkt was rcvd          */
/*                : u1AssocMode - Sec Assoc MOde - Transport/Tunnel          */
/*  Output(s)     : None                                                     */
/*  Return Values : None                                                     */
/*****************************************************************************/

VOID
Secv4PostInMsgToIP (tCRU_BUF_CHAIN_HEADER * pCBuf, UINT4 u4IfIndex,
                    UINT1 u1AssocMode, UINT4 u4VpncIndex)
{
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
#ifdef FLOWMGR_WANTED
    FlUtilUpdateFlowTypeFlowData (pCBuf, FLOW_CTRL_FLOW);
#endif
    CfaGetIfInfo (u4IfIndex, &IfInfo);

    if (OSIX_FAILURE == SecProcessContinueInBound (pCBuf,
                                                   u4IfIndex, u4VpncIndex,
                                                   u1AssocMode, OSIX_TRUE,
                                                   &IfInfo, CFA_ENET_IPV4))
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4PostInMsgToIP: Handing over to IP failed\n");
        CRU_BUF_Release_MsgBufChain (pCBuf, FALSE);
        return;
    }
    return;
}

 /****************************************************************************/
 /*  Function Name : Secv4PostOutMsgToIP                                     */
 /*  Description   : This function is called from IPsec to Post Pkt to IP    */
 /*  Input(s)      : pBufDesc -  Linear Buffer Descriptor                    */
 /*                : u4IfIndex - INterface on which the pkt was rcvd         */
 /*  Output(s)     : None                                                    */
 /*  Return Values : None                                                    */
 /****************************************************************************/

VOID
Secv4PostOutMsgToIP (tCRU_BUF_CHAIN_HEADER * pCBuf, UINT4 u4IfIndex)
{
    UINT4               u4IfMtu = 0;
    UINT4               u4IfIndexMtu = 0;
    t_IP_HEADER         IpHdr;
    t_IP_HEADER        *pIpHdr;
    t_ICMP              Icmp;

    pIpHdr =
        (t_IP_HEADER *) (void *) CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0,
                                                               SEC_IPV4_HEADER_SIZE);

    if (pIpHdr == NULL)
    {
        pIpHdr = &IpHdr;
        IPSEC_MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
        IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) pIpHdr, 0, SEC_IPV4_HEADER_SIZE);
    }

    if (Secv4GetIfMtu (u4IfIndex, &u4IfIndexMtu) == CFA_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4PostOutMsgToIP: Get MTU of Interface failed \n");
        CRU_BUF_Release_MsgBufChain (pCBuf, FALSE);
        return;
    }
    if (SecUtilIpIfGetIfIndexFromIpAddress (OSIX_NTOHL (pIpHdr->u4Src),
                                            &u4IfIndex) == OSIX_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pCBuf, FALSE);
        return;
    }

    /* Send it if it fits into mtu */
    if ((UINT2) IPSEC_NTOHS (pIpHdr->u2Totlen) <= u4IfIndexMtu)
    {
        if (OSIX_FAILURE == SecProcessContinueOutBound (pCBuf, u4IfIndex))
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4PostOutMsgToIP: "
                       "Failed in SecProcessContinueOutBound \n ");
            return;
        }
    }
    else
    {
        if (IPSEC_IS_DF_SET (IPSEC_NTOHS (pIpHdr->u2Fl_offs)) == FALSE)
        {
            if (Secv4FragmentAndSend (pIpHdr, pCBuf, u4IfIndex) == SEC_FAILURE)
            {
                SECv4_TRC (SECv4_DATA_PATH,
                           "Secv4PostOutMsgToIP: "
                           "Secv4FragmentAndSend returned failure\n");
            }
        }
        else
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4PostOutMsgToIP:frag needed, DF set\n");
            Icmp.i1Type = ICMP_DEST_UNREACH;
            Icmp.i1Code = ICMP_FRAG_NEEDED;
            Icmp.args.u4Unused = 0;
            Secv4GetIfMtu (u4IfIndex, &u4IfMtu);
            SECv4_ICMP_NH_MTU (Icmp) = (UINT2) u4IfMtu;
            Secv4SendIcmpErrMsg (pCBuf, &Icmp);
            return;
        }
    }

    return;
}

/******************************************************************************/
/*  Function Name : Secv4EspIncomingTunnel                                    */
/*  Description   : This fuction handles all the incoming ESP tunnel packets  */
/*                : after the algorithm processing by softwarwe or Hardare    */
/*                : accelerator                                               */
/*  Input(s)      : pData -  Contains all the information needed to           */
/*                : process after the algorithm                               */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/
VOID
Secv4EspIncomingTunnel (VOID *pData)
{
    tSecv4TaskletData  *pTemp = NULL;
    tSecv4Assoc        *pSaEntry = NULL;
    t_IP_HEADER        *pIpHdr = NULL;
    UINT4               u4EspTrAndDgstLen = 0;

    pTemp = (tSecv4TaskletData *) pData;

    if (pTemp == NULL)
    {
        return;
    }
    pSaEntry = pTemp->pSaEntry;

    /* Delete the ESP header and IV from buffer */
    if (IPSEC_BUF_MoveOffset (pTemp->pCBuf, (UINT4)
                              (SEC_ESP_HEADER_LEN +
                               pSaEntry->u1SecAssocIvSize)) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4EspIncomingTunnel : Move Offset Fails to "
                   " remove ESP Header \r\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }

    pIpHdr =
        (t_IP_HEADER *) (void *) CRU_BUF_Get_DataPtr_IfLinear (pTemp->pCBuf, 0,
                                                               SEC_IPV4_HEADER_SIZE);
    if (pIpHdr == NULL)
    {
        return;
    }
    /* Delete the ESP trailer and auth digest (if present) from the buffer */
    u4EspTrAndDgstLen =
        CRU_BUF_Get_ChainValidByteCount (pTemp->pCBuf) -
        IPSEC_NTOHS (pIpHdr->u2Totlen);
    if (IPSEC_BUF_DeleteAtEnd (pTemp->pCBuf, u4EspTrAndDgstLen) == NULL)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4EspIncomingTunnel: Buffer DeleteAtEnd Failed \r\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    Secv4HandOverPktTOIP (pTemp);
    return;
}

/******************************************************************************/
/*  Function Name : Secv4EspIncomingTransport                                 */
/*  Description   : This fuction handles all the incoming ESP transport       */
/*                : pkts after the algorithm processing by softwarwe/Hardare  */
/*                : accelerator                                               */
/*  Input(s)      : pData -  Contains all the information needed to           */
/*                : process after the algorithm                               */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/

VOID
Secv4EspIncomingTransport (VOID *pData)
{
    UINT1               au1Buf[SEC_IPV4_HEADER_SIZE + SEC_IPV4_MAX_OPT_LEN];
    t_IP_HEADER        *pIpHdr = NULL;
    tSecv4TaskletData  *pTemp = NULL;
    UINT1              *pu1Buf = NULL;
    tSecv4Assoc        *pSaEntry = NULL;
    UINT4               u4OptLen = 0;
    UINT4               u4Size = 0;
    UINT1               u1Proto = 0;
    UINT1               u1EspTrailerLen = 0;

    pTemp = (tSecv4TaskletData *) pData;

    if (pTemp == NULL)
    {
        return;
    }
    pSaEntry = pTemp->pSaEntry;

    if (IPSEC_COPY_FROM_BUF (pTemp->pCBuf, au1Buf, 0,
                             SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
    {
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    pIpHdr = (t_IP_HEADER *) (void *) au1Buf;

    /* Check if options are present */
    u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);

    if (u4OptLen != 0)
    {
        if (IPSEC_COPY_FROM_BUF (pTemp->pCBuf,
                                 (UINT1 *) (au1Buf + SEC_IPV4_HEADER_SIZE),
                                 SEC_IPV4_HEADER_SIZE, u4OptLen) == BUF_FAILURE)
        {
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;
        }
    }

    /* Delete the IP ,option,esp header and IV from buffer */
    if (IPSEC_BUF_MoveOffset
        (pTemp->pCBuf, (SEC_IPV4_HEADER_SIZE + u4OptLen +
                        SEC_ESP_HEADER_LEN + pSaEntry->u1SecAssocIvSize))
        == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4EspTunnelDecode : Move Offset Fails to remove Ip Header,"
                   "ESP Header and IV from buffer\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }

    /* Remove Auth Digest */
    if (pSaEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
    {
        if (IPSEC_BUF_DeleteAtEnd
            (pTemp->pCBuf, pSaEntry->u1SecAhAlgoDigestSize) == NULL)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4EspTransportDecode :Buffer DeleteAtEnd Failed \r\n");
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;
        }
    }

    u4Size = CRU_BUF_Get_ChainValidByteCount (pTemp->pCBuf);
    pu1Buf = (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pTemp->pCBuf, 0, u4Size);

    /* Get the orginal protocol  and Trailer Length */
    if (pu1Buf != NULL)
    {
        u1Proto = (UINT1) *(pu1Buf + (u4Size - SEC_ESP_TRAILER_SIZE_LEN));
        u1EspTrailerLen = (UINT1) *(pu1Buf + (u4Size - SEC_ESP_TRAILER_LEN));
    }
    else
    {
        IPSEC_COPY_FROM_BUF (pTemp->pCBuf, &u1Proto,
                             (u4Size - SEC_ESP_TRAILER_SIZE_LEN),
                             sizeof (UINT1));
        IPSEC_COPY_FROM_BUF (pTemp->pCBuf, &u1EspTrailerLen,
                             (u4Size - SEC_ESP_TRAILER_LEN), sizeof (UINT1));
    }

    if (u1EspTrailerLen > u4Size)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4CallbackEspTunnelDecode :Decryption Fails\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }

    /* Remove Padding bytes & ESP Trailer */
    if (IPSEC_BUF_DeleteAtEnd
        (pTemp->pCBuf, (UINT4) (u1EspTrailerLen + SEC_ESP_TRAILER_LEN)) == NULL)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4EspTransportDecode :Buffer DeleteAtEnd Failed \r\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    u4Size = CRU_BUF_Get_ChainValidByteCount (pTemp->pCBuf);
    if (u4Size == 0)
    {
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }

    pIpHdr->u2Totlen = IPSEC_HTONS ((u4Size + SEC_IPV4_HEADER_SIZE + u4OptLen));
    pIpHdr->u1Proto = u1Proto;
    pIpHdr->u2Cksum = 0;
    pIpHdr->u2Cksum = Secv4IPCalcChkSum ((UINT1 *) pIpHdr,
                                         (SEC_IPV4_HEADER_SIZE + u4OptLen));
    pIpHdr->u2Cksum = IPSEC_HTONS (pIpHdr->u2Cksum);

    /* Prepend the Ipheader and options */
    if (IPSEC_BUF_Prepend (pTemp->pCBuf, au1Buf,
                           (SEC_IPV4_HEADER_SIZE + u4OptLen)) == BUF_FAILURE)
    {
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    Secv4HandOverPktTOIP (pTemp);
}

/******************************************************************************/
/*  Function Name : Secv4AhOutGoingTunnel                                     */
/*  Description   : This fuction handles all the outging Ah tunnel packets    */
/*                : after the algorithm processing by softwarwe or Hardare    */
/*                : accelerator                                               */
/*  Input(s)      : pData -  Contains all the information needed to           */
/*                : process after the algorithm                               */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/

VOID
Secv4AhOutGoingTunnel (VOID *pData)
{
    tSecv4TaskletData  *pTemp;
    t_IP_HEADER         IpHdr;
    t_IP_HEADER        *pIpHdr = NULL;
    UINT1              *pu1Buf = NULL;

    pTemp = (tSecv4TaskletData *) pData;

    if (pTemp == NULL)
    {
        return;
    }

    pu1Buf =
        (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pTemp->pCBuf, 0,
                                                SEC_IPV4_HEADER_SIZE);
    if (pu1Buf == NULL)
    {

        if (IPSEC_COPY_FROM_BUF (pTemp->pCBuf, (UINT1 *) &IpHdr, 0,
                                 SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhIncomingPerformCB : Unable Remove Ah and "
                       "Ip Header from PAcket\n");
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;
        }
        pIpHdr = &IpHdr;
    }
    else
    {
        pIpHdr = (t_IP_HEADER *) (void *) pu1Buf;
    }

    pIpHdr->u1Tos = pTemp->u1Tos;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = SEC_MAX_HOP_LIMIT;
    pIpHdr->u2Cksum =
        Secv4IPCalcChkSum ((UINT1 *) pIpHdr, SEC_IPV4_HEADER_SIZE);
    pIpHdr->u2Cksum = (UINT2) IPSEC_HTONS (pIpHdr->u2Cksum);

    if (pu1Buf == NULL)
    {
        if (IPSEC_COPY_OVER_BUF (pTemp->pCBuf, (UINT1 *) pIpHdr, 0,
                                 SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
        {
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;
        }
    }
    Secv4UpdateIfStats (pTemp->u4IfIndex, SEC_OUTBOUND, SEC_APPLY);
    Secv4UpdateAhEspStats (pTemp->u4IfIndex, SEC_AH, SEC_OUTBOUND, SEC_ALLOW);
    Secv4UnLock ();
    Secv4PostOutMsgToIP (pTemp->pCBuf, pTemp->u4IfIndex);
    MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pTemp);
}

/******************************************************************************/
/*  Function Name : Secv4AhOutGoingTransport                                  */
/*  Description   : This fuction handles all the outging Ah transport packets */
/*                : after the algorithm processing by softwarwe or Hardare    */
/*                : accelerator                                               */
/*  Input(s)      : pData -  Contains all the information needed to           */
/*                : process after the algorithm                               */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/

VOID
Secv4AhOutGoingTransport (VOID *pData)
{
    tSecv4TaskletData  *pTemp;
    t_IP_HEADER        *pIpHdr = NULL;
    UINT4               u4OptLen = 0;
    UINT4               u4Size = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               au1Buf[SEC_IPV4_HEADER_SIZE + SEC_IPV4_MAX_OPT_LEN];

    pTemp = (tSecv4TaskletData *) pData;

    if (pTemp == NULL)
    {
        return;
    }

    u4Size = CRU_BUF_Get_ChainValidByteCount (pTemp->pCBuf);
    pu1Buf = (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pTemp->pCBuf, 0, u4Size);

    if (pu1Buf == NULL)
    {
        if (IPSEC_COPY_FROM_BUF (pTemp->pCBuf, au1Buf, 0,
                                 SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhIncomingPerformCB : Copy  "
                       "Ip Header from Buffer failed\n");
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;
        }
        pIpHdr = (t_IP_HEADER *) (void *) au1Buf;
    }
    else
    {
        pIpHdr = (t_IP_HEADER *) (void *) pu1Buf;
    }

    /* Restore the Muted Fields of the IPHeader-Options */
    u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);
    if (u4OptLen != 0)
    {
        if (IPSEC_COPY_OVER_BUF (pTemp->pCBuf, (UINT1 *) &pTemp->pu1OptBuf,
                                 SEC_IPV4_HEADER_SIZE,
                                 SEC_IPV4_HEADER_SIZE + u4OptLen)
            == BUF_FAILURE)
        {
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;
        }
        if (pu1Buf == NULL)
        {
            IPSEC_MEMCPY ((au1Buf + SEC_IPV4_HEADER_SIZE), pTemp->pu1OptBuf,
                          u4OptLen);
        }
        Secv4ReleaseRcvdIPOpts (pTemp->pu1OptBuf);
    }

    /* Restore back the Muted Fiels of the IP Header */
    pIpHdr->u1Tos = pTemp->u1Tos;
    pIpHdr->u2Fl_offs = pTemp->u2Fl_offs;
    pIpHdr->u1Ttl = pTemp->u1Ttl;
    pIpHdr->u2Cksum =
        Secv4IPCalcChkSum ((UINT1 *) pIpHdr, (SEC_IPV4_HEADER_SIZE + u4OptLen));
    pIpHdr->u2Cksum = (UINT2) IPSEC_HTONS (pIpHdr->u2Cksum);

    if (pu1Buf == NULL)
    {
        if (IPSEC_COPY_OVER_BUF (pTemp->pCBuf, (UINT1 *) pIpHdr, 0,
                                 SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
        {
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;
        }
    }
    Secv4UpdateIfStats (pTemp->u4IfIndex, SEC_OUTBOUND, SEC_APPLY);
    Secv4UpdateAhEspStats (pTemp->u4IfIndex, SEC_AH, SEC_OUTBOUND, SEC_ALLOW);
    Secv4UnLock ();
    Secv4PostOutMsgToIP (pTemp->pCBuf, pTemp->u4IfIndex);
    MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pTemp);
}

/******************************************************************************/
/*  Function Name : Secv4AhIncomingTunnel                                     */
/*  Description   : This fuction handles all the incoming Ah tunnel packets   */
/*                : after the algorithm processing by softwarwe or Hardare    */
/*                : accelerator                                               */
/*  Input(s)      : pData -  Contains all the information needed to           */
/*                : process after the algorithm                               */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/

VOID
Secv4AhIncomingTunnel (VOID *pData)
{
    tSecv4TaskletData  *pTemp = NULL;
    tSecv4Assoc        *pSaEntry = NULL;

    pTemp = (tSecv4TaskletData *) pData;
    pSaEntry = (tSecv4Assoc *) pTemp->pSaEntry;

    /* Remove IP header and AH header from the Packet */
    if (IPSEC_BUF_MoveOffset (pTemp->pCBuf, (UINT4) (SEC_IPV4_HEADER_SIZE
                                                     + SEC_AUTH_HEADER_SIZE +
                                                     pSaEntry->
                                                     u1SecAhAlgoDigestSize)) ==
        BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhIncomingPerformCB : Unable Remove Ah and "
                   "Ip Header from PAcket\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    Secv4HandOverPktTOIP (pTemp);
}

/******************************************************************************/
/*  Function Name : Secv4AhIncomingTransport                                  */
/*  Description   : This fuction handles all the incoming Ah transport pkets  */
/*                : after the algorithm processing by softwarwe or Hardare    */
/*                : accelerator                                               */
/*  Input(s)      : pData -  Contains all the information needed to           */
/*                : process after the algorithm                               */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/
VOID
Secv4AhIncomingTransport (VOID *pData)
{
    tSecv4TaskletData  *pTemp = NULL;
    tSecv4Assoc        *pSaEntry = NULL;
    UINT4               u4DataSize = 0;
    UINT4               u4OptLen = 0;
    t_IP_HEADER        *pIpHdr;
    UINT1               au1Buf[SEC_IPV4_MAX_OPT_LEN + SEC_IPV4_HEADER_SIZE];
    UINT1              *pu1DataBuf = NULL;

    pTemp = (tSecv4TaskletData *) pData;
    pSaEntry = (tSecv4Assoc *) pTemp->pSaEntry;

    if (IPSEC_COPY_FROM_BUF (pTemp->pCBuf, au1Buf, 0, SEC_IPV4_HEADER_SIZE) ==
        BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhIncomingPerformCB : Unable Copy IP Header "
                   "from Buffer\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }

    pIpHdr = (t_IP_HEADER *) (void *) au1Buf;
    /*Restore the muted fields in case of transport mode */
    pIpHdr->u1Ttl = pTemp->u1Ttl;
    pIpHdr->u2Fl_offs = pTemp->u2Fl_offs;
    pIpHdr->u1Tos = pTemp->u1Tos;

    /* Check if options are present */
    u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);
    if (u4OptLen != 0)
    {
        if (IPSEC_COPY_FROM_BUF
            (pTemp->pCBuf, (au1Buf + SEC_IPV4_HEADER_SIZE),
             SEC_IPV4_HEADER_SIZE, u4OptLen) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhIncomingPerformCB : Unable copy options from "
                       "buffer\n");
            Secv4HandleFailueAfterCryptoProcess (pTemp);
            return;

        }

    }

    /*Get Protocol */
    pu1DataBuf =
        (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pTemp->pCBuf,
                                                (SEC_IPV4_HEADER_SIZE +
                                                 u4OptLen),
                                                SEC_AUTH_HEADER_SIZE);
    if (pu1DataBuf == NULL)
    {
        CRU_BUF_Copy_FromBufChain (pTemp->pCBuf, &(pIpHdr->u1Proto),
                                   (SEC_IPV4_HEADER_SIZE + u4OptLen),
                                   sizeof (UINT1));
    }
    else
    {
        pIpHdr->u1Proto = *pu1DataBuf;
    }

    /*Remove Ah Header and digest */
    if (IPSEC_BUF_MoveOffset
        (pTemp->pCBuf,
         (SEC_IPV4_HEADER_SIZE + u4OptLen + SEC_AUTH_HEADER_SIZE +
          pSaEntry->u1SecAhAlgoDigestSize)) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhIncomingPerformCB : Unable Remove Ah and "
                   "Auth digest from PAcket\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    u4DataSize = CRU_BUF_Get_ChainValidByteCount (pTemp->pCBuf);

    pIpHdr->u2Totlen =
        IPSEC_HTONS ((u4DataSize + SEC_IPV4_HEADER_SIZE + u4OptLen));
    pIpHdr->u2Cksum = Secv4IPCalcChkSum ((UINT1 *) pIpHdr,
                                         (SEC_IPV4_HEADER_SIZE + u4OptLen));
    pIpHdr->u2Cksum = IPSEC_HTONS (pIpHdr->u2Cksum);

    if (IPSEC_BUF_Prepend
        (pTemp->pCBuf, au1Buf,
         (SEC_IPV4_HEADER_SIZE + u4OptLen)) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhIncomingPerformCB : Unable Prepend the IP Header to Buffer\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }

    Secv4HandOverPktTOIP (pTemp);
}

/******************************************************************************/
/*  Function Name : Secv4PutIpHdr                                             */
/*  Description   : This fuction Adds the new IP header on the buffer  and    */
/*                : copies the new checksum to the pkt                        */
/*  Input(s)      : pBuf  -  Actual Pkt on which the header should be put     */
/*                : pIp   - The new IP header                                 */
/*                : i1flag - Flag to indicate the presence of IP options      */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/
VOID
Secv4PutIpHdr (tCRU_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, INT1 i1flag)
{
    t_IP_HEADER        *pIpHdr;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1LinearBuf = FALSE;

    UNUSED_PARAM (i1flag);
    if (pBuf == NULL)
    {
        return;
    }
    if ((pIpHdr =
         (t_IP_HEADER *) (void *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                                sizeof
                                                                (t_IP_HEADER) -
                                                                1)) == NULL)
    {
        pIpHdr = &TmpIpHdr;
    }
    else
    {
        u1LinearBuf = TRUE;
    }

    pIpHdr->u1Ver_hdrlen =
        (UINT1) IP_VERS_AND_HLEN (pIp->u1Version, pIp->u2Olen);
    pIpHdr->u1Tos = pIp->u1Tos;
    pIpHdr->u2Totlen = CRU_HTONS (pIp->u2Len);
    pIpHdr->u2Id = CRU_HTONS (pIp->u2Id);
    pIpHdr->u2Fl_offs = CRU_HTONS (pIp->u2Fl_offs);
    pIpHdr->u1Ttl = pIp->u1Ttl;
    pIpHdr->u1Proto = pIp->u1Proto;
    pIpHdr->u2Cksum = 0;        /* Clear Checksum */

    pIpHdr->u4Src = IPSEC_NTOHL (pIp->u4Src);
    pIpHdr->u4Dest = IPSEC_NTOHL (pIp->u4Dest);

    if (u1LinearBuf == FALSE)
    {
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    if (pIp->u2Olen > 0)
    {
        /* If Options are present, then Copy them to the Buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, pIp->au1Options, IP_HDR_LEN,
                                   pIp->u2Olen);
    }
    /* Calculate the Check sum for the IP Header and copy it to the Buffer  */
    pIp->u2Cksum =
        Secv4IPCalcChkSum ((UINT1 *) pIpHdr,
                           (UINT4) (IP_HDR_LEN + pIp->u2Olen));

    pIpHdr->u2Cksum = IPSEC_HTONS (pIp->u2Cksum);
    if (u1LinearBuf != TRUE)
    {
        /* In Case of Linear buffer , it is already modified in the buffer */
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }
    return;
}
