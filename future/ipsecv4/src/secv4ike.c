/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4ike.c,v 1.15 2015/11/25 09:43:29 siva Exp $
 *
 * Description: This has functions for interacting with IKE
 *
 ***********************************************************************/

#include "secv4com.h"
#include "secv4ike.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fssecv4lw.h"

/***********************************************************************/
/*  Function Name : Secv4ProcessIKEMsg                                 */
/*  Description   : This function invokes the appropraite function to  */
/*                : process the IKE Message based on the Command type  */
/*                : of the IKE Message                                 */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Message rcvd from IKE         */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :None                                                */
/***********************************************************************/

VOID
Secv4ProcessIKEMsg (tIkeIPSecQMsg * pMsg)
{
    switch (IPSEC_NTOHL (pMsg->u4MsgType))
    {
        case IKE_IPSEC_INSTALL_SA:
            if (pMsg->IkeIfParam.InstallSA.bInstallDynamicIPSecDB == TRUE)
            {
                Secv4ProcessIkeInstallDynamicIpsecDBMsg (pMsg);
            }
            else
            {
                Secv4ProcessIkeInstallSAMsg (pMsg);
            }
            break;
        case IKE_IPSEC_DUPLICATE_SPI_REQ:
            Secv4ProcessIkeDupSPIMsg (pMsg);
            break;
        case IKE_IPSEC_PROCESS_DELETE_SA:
            Secv4ProcessIkeDeleteSAMsg (pMsg);
            break;
        case IKE_IPSEC_PROCESS_DEL_SA_BY_ADDR:
            Secv4ProcessIkeDeleteSAByAddrMsg (pMsg);
            break;
        case IKE_IPSEC_PROCESS_DEL_SA_FOR_LOCAL_CONF_CHG:
            Secv4ProcessIkeDeleteAllSAForLocal (pMsg);
        default:
            break;
    }
    return;
}

/***********************************************************************/
/*  Function Name : Secv4ProcessIkeInstallSAMsg                        */
/*  Description   : This function process the Install SA message from  */
/*                : IKE          .                                     */
/*                :                                                    */
/*  Input(s)      : pMsg - Pointer to the Msg Rcvd From IKE            */
/*                :                                                    */
/*  Output(s)     : Creates  SA in SAD                                 */
/*  Return        :SEC_SUCCESS or SEC_FAILURE                          */
/***********************************************************************/

INT1
Secv4ProcessIkeInstallSAMsg (tIkeIPSecQMsg * pMsg)
{

    tIPSecBundle       *pInstallSAMsg = NULL;
    tSecv4Selector     *pSelEntry = NULL;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4AccessIndex = SEC_FAILURE;
    UINT1               au1SaBundle[SEC_MAX_SA_BUNDLE_LEN];
    UINT4               u4IfIndex = 0;
    UINT4               u4SaIndex0 = 0;
    UINT4               u4SaIndex1 = 0;
    UINT4               u4StartSrcAddr = 0;
    UINT4               u4StartDestAddr = 0;
    UINT4               u4Action = 0;
    UINT4               u4ProtocolId = 0;

    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPSEC_MEMSET (&au1SaBundle, 0, sizeof (au1SaBundle));

    pInstallSAMsg = &(pMsg->IkeIfParam.InstallSA);

    Secv4ConvertIkeInstallSAMsgToHostOrder (pInstallSAMsg);
    if (SecUtilIpIfGetIfIndexFromIpAddress
        (pInstallSAMsg->LocTunnTermAddr.Ipv4Addr, &u4IfIndex) == OSIX_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Invalid Loc Tunnel Term Addr\n");
        return (SEC_FAILURE);
    }

    if (pInstallSAMsg->LocalProxy.u4Type != IKE_IPSEC_ID_IPV4_SUBNET)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Invalid Local Proxy Type\n");
        return (SEC_FAILURE);
    }

    u4StartSrcAddr = (pInstallSAMsg->LocalProxy.uNetwork.Ip4Subnet.Ip4Addr &
                      pInstallSAMsg->LocalProxy.uNetwork.Ip4Subnet.
                      Ip4SubnetMask);

    u4StartDestAddr = (pInstallSAMsg->RemoteProxy.uNetwork.Ip4Subnet.Ip4Addr &
                       pInstallSAMsg->RemoteProxy.uNetwork.Ip4Subnet.
                       Ip4SubnetMask);

    u4AccessIndex =
        Secv4GetAccessIndexByRange (u4StartDestAddr, u4StartSrcAddr);

    if (u4AccessIndex == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Access Look-Up For InBound Failed\n");
        return SEC_FAILURE;
    }

    if (pInstallSAMsg->aInSABundle[0].u4Spi != 0)
    {
        u4SaIndex0 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke
            (&pInstallSAMsg->aInSABundle[0],
             pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
             pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
             u4SaIndex0, pInstallSAMsg->aOutSABundle[0].u4Spi,
             0) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallSAMsg: Secv4InstallSAFromIke "
                       "Failed\n");
            return (SEC_FAILURE);
        }

        if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
        {
            u4SaIndex1 = Secv4GetFreeSAIdx ();
            if (Secv4InstallSAFromIke
                (&pInstallSAMsg->aInSABundle[1],
                 pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                 pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                 u4SaIndex1, pInstallSAMsg->aOutSABundle[1].u4Spi, 0) !=
                SEC_SUCCESS)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE,
                           "Secv4ProcessIkeInstallSAMsg: Secv4InstallSAFromIke Failed\n");
                return (SEC_FAILURE);
            }
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld.%ld", u4SaIndex0, u4SaIndex1);
        }
        else
        {
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld", u4SaIndex0);
        }

        u4ProtocolId = (UINT4) pInstallSAMsg->LocalProxy.u1HLProtocol;
        if (u4ProtocolId == 0)
        {
            u4ProtocolId = SEC_ANY_PROTOCOL;
        }
        pSelEntry =
            Secv4SelGetFormattedEntry (u4IfIndex,
                                       u4ProtocolId, u4AccessIndex,
                                       pInstallSAMsg->aInSABundle[0].
                                       u2HLPortNumber, SEC_INBOUND);
    }
    else if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
    {
        u4SaIndex1 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke
            (&pInstallSAMsg->aInSABundle[1],
             pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
             pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
             u4SaIndex1, pInstallSAMsg->aOutSABundle[1].u4Spi,
             0) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallSAMsg: Secv4InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%ld",
                        u4SaIndex1);

        u4ProtocolId = (UINT4) pInstallSAMsg->LocalProxy.u1HLProtocol;
        if (u4ProtocolId == 0)
        {
            u4ProtocolId = SEC_ANY_PROTOCOL;
        }
        pSelEntry =
            Secv4SelGetFormattedEntry (u4IfIndex,
                                       u4ProtocolId, u4AccessIndex,
                                       pInstallSAMsg->aInSABundle[1].
                                       u2HLPortNumber, SEC_INBOUND);
    }

    if (pSelEntry == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Selector Look-Up For InBound "
                   "Failed\n");
        return SEC_FAILURE;
    }

    if (pSelEntry->pPolicyEntry == NULL)
    {

        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4ProcessIkeInstallSAMsg: No InBound Policy Entry\n");
        return SEC_FAILURE;
    }

    if (pSelEntry->pPolicyEntry->pau1SaEntry[0] == NULL)
    {
        pSelEntry->pPolicyEntry->u1ReqIkeFlag = SEC_NEW_KEY;
        OctetStr.pu1_OctetList = au1SaBundle;
        OctetStr.i4_Length = (INT4) STRLEN (au1SaBundle);
        if (nmhTestv2Fsipv4SecPolicySaBundle (&u4SnmpErrorStatus,
                                              (INT4) pSelEntry->pPolicyEntry->
                                              u4PolicyIndex,
                                              &OctetStr) == SNMP_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallSAMsg: Unable To Attach InBound "
                       "SA's To Policy\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv4SecPolicySaBundle ((INT4) pSelEntry->pPolicyEntry->
                                       u4PolicyIndex, &OctetStr);
    }
    else
    {
        pSelEntry->pPolicyEntry->u1ReqIkeFlag = SEC_RE_KEY;
        Secv4AttachNewSaToPolicy (pSelEntry->pPolicyEntry, au1SaBundle);
    }

    if (pSelEntry->u4PktDirection == SEC_INBOUND)
    {
        u4Action = SEC_DECODE;
    }
    else
    {
        u4Action = SEC_ENCODE;
    }
    /* Initialize the session for the Sec Assoc in Hw Accelerator for outbound 
       direction */
    if (Secv4InitSesInHwAccel ((u4SaIndex0 ? u4SaIndex0 : u4SaIndex1), u4Action)
        == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Unable To Initialize  InBound Session  "
                   "in Hw Accel \n");
        return SEC_FAILURE;
    }

    pSelEntry = NULL;
    u4AccessIndex = SEC_FAILURE;
    u4SaIndex0 = 0;
    u4SaIndex1 = 0;
    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPSEC_MEMSET (&au1SaBundle, 0, sizeof (au1SaBundle));
    u4AccessIndex =
        Secv4GetAccessIndexByRange (u4StartSrcAddr, u4StartDestAddr);

    if (u4AccessIndex == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Access Look-Up For OutBound "
                   "Failed\n");
        return SEC_FAILURE;
    }

    if (pInstallSAMsg->aOutSABundle[0].u4Spi != 0)
    {

        u4SaIndex0 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke (&pInstallSAMsg->aOutSABundle[0],
                                   pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                                   pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                                   u4SaIndex0, pInstallSAMsg->
                                   aInSABundle[0].u4Spi, 0) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallSAMsg: Secv4InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
        {

            u4SaIndex1 = Secv4GetFreeSAIdx ();
            if (Secv4InstallSAFromIke (&pInstallSAMsg->aOutSABundle[1],
                                       pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                                       pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                                       u4SaIndex1, pInstallSAMsg->
                                       aInSABundle[1].u4Spi, 0) != SEC_SUCCESS)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE,
                           "Secv4ProcessIkeInstallSAMsg: Secv4InstallSAFromIke Failed\n");
                return (SEC_FAILURE);
            }
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld.%ld", u4SaIndex0, u4SaIndex1);
        }
        else
        {
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld", u4SaIndex0);
        }
        u4ProtocolId = (UINT4) pInstallSAMsg->RemoteProxy.u1HLProtocol;
        if (u4ProtocolId == 0)
        {
            u4ProtocolId = SEC_ANY_PROTOCOL;
        }
        pSelEntry =
            Secv4SelGetFormattedEntry (u4IfIndex, u4ProtocolId, u4AccessIndex,
                                       pInstallSAMsg->aOutSABundle[0].
                                       u2HLPortNumber, SEC_OUTBOUND);
    }
    else if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
    {

        u4SaIndex1 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke (&pInstallSAMsg->aOutSABundle[1],
                                   pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                                   pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                                   u4SaIndex1, pInstallSAMsg->
                                   aInSABundle[1].u4Spi, 0) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallSAMsg: Secv4InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%ld",
                        u4SaIndex1);
        u4ProtocolId = (UINT4) pInstallSAMsg->RemoteProxy.u1HLProtocol;
        if (u4ProtocolId == 0)
        {
            u4ProtocolId = SEC_ANY_PROTOCOL;
        }
        pSelEntry =
            Secv4SelGetFormattedEntry (u4IfIndex, u4ProtocolId, u4AccessIndex,
                                       pInstallSAMsg->aOutSABundle[1].
                                       u2HLPortNumber, SEC_OUTBOUND);
    }

    if (pSelEntry == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Selector Look-Up For OutBound Failed\n");
        return SEC_FAILURE;
    }

    if (pSelEntry->pPolicyEntry == NULL)
    {

        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4ProcessIkeInstallSAMsg: No OutBound Policy Entry \n");
        return SEC_FAILURE;
    }

    if (pSelEntry->pPolicyEntry->pau1SaEntry[0] == NULL)
    {
        pSelEntry->pPolicyEntry->u1ReqIkeFlag = SEC_NEW_KEY;
        OctetStr.pu1_OctetList = au1SaBundle;
        OctetStr.i4_Length = (INT4) STRLEN (au1SaBundle);
        if (nmhTestv2Fsipv4SecPolicySaBundle (&u4SnmpErrorStatus,
                                              (INT4) pSelEntry->pPolicyEntry->
                                              u4PolicyIndex,
                                              &OctetStr) == SNMP_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallSAMsg: Unable To Attach "
                       "OutBound SA To Policy\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv4SecPolicySaBundle ((INT4) pSelEntry->pPolicyEntry->
                                       u4PolicyIndex, &OctetStr);
    }
    else
    {
        pSelEntry->pPolicyEntry->u1ReqIkeFlag = SEC_RE_KEY;
        Secv4AttachNewSaToPolicy (pSelEntry->pPolicyEntry, au1SaBundle);
    }

    if (pSelEntry->u4PktDirection == SEC_INBOUND)
    {
        u4Action = SEC_DECODE;
    }
    else
    {
        u4Action = SEC_ENCODE;
    }

    /* Initialize the session for the Sec Assoc in Hw Accelerator for inbound 
       direction */
    if (Secv4InitSesInHwAccel ((u4SaIndex0 ? u4SaIndex0 : u4SaIndex1), u4Action)
        == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallSAMsg: Unable To Initialize  InBound Session  "
                   "in Hw Accel \n");
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}

/***********************************************************************/
/*  Function Name : Secv4ProcessIkeDupSPIMsg                           */
/*  Description   : This function checks if a given SPI is already in  */
/*                : use or not.                                        */
/*                :                                                    */
/*  Input(s)      : pInfo - Pointer to the message from IKE            */
/*                :                                                    */
/*  Output(s)     : Boolean Flag is Set to TRUE if the Spi is duplicate*/
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv4ProcessIkeDupSPIMsg (tIkeIPSecQMsg * pInfo)
{
    tIkeDupSpi         *pDupSPIInfo = NULL;
    tIkeQMsg           *pMsg = NULL;

    pDupSPIInfo = &(pInfo->IkeIfParam.DupSpiInfo);

    if ((Secv4GetAssocEntry
         (IPSEC_NTOHL (pDupSPIInfo->u4EspSpi),
          IPSEC_NTOHL (pDupSPIInfo->RemoteAddr.Ipv4Addr),
          SEC_ESP) != NULL) ||
        (Secv4GetAssocEntry
         (IPSEC_NTOHL (pDupSPIInfo->u4AhSpi),
          IPSEC_NTOHL (pDupSPIInfo->RemoteAddr.Ipv4Addr), SEC_AH) != NULL))
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeDupSPIMsg: Rcvd Duplicate Spi\n");
        pDupSPIInfo->bDupSpi = TRUE;
    }

    if (MemAllocateMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 **) (VOID *) &pMsg) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeDupSPIMsg:- Failed to allocate memblock for "
                   " tIkeQMsg \n");
        return;
    }

    if (pMsg == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeDupSPIMsg: MEM ALLOC failed \n");
        return;
    }
    IPSEC_MEMSET (pMsg, 0, sizeof (tIkeQMsg));
    pMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
        IPSEC_HTONL (IKE_IPSEC_DUPLICATE_SPI_REPLY);
    pMsg->u4MsgType = IPSEC_TASK;
    /* Fill the version in the reply message same as what is received 
     * in Duplicate SPI request */
    pMsg->u1IkeVer = pDupSPIInfo->u1IkeVersion;

    IPSEC_MEMCPY (&(pMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DupSpiInfo),
                  pDupSPIInfo, sizeof (tIkeDupSpi));
    IkeHandlePktFromIpsec (pMsg);
}

/***********************************************************************/
/*  Function Name : Secv4ProcessIkeDeleteSAMsg                         */
/*  Description   : This function Deletes all the SA's which match with*/
/*                : the rcvd Spi,DestAddr and Protocol                 */
/*                :                                                    */
/*  Input(s)      : pMsg - Pointer to Mesg rcvd from IKE               */
/*                :                                                    */
/*  Output(s)     : Deletes the Matched SA                             */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv4ProcessIkeDeleteSAMsg (tIkeIPSecQMsg * pMsg)
{
    UINT4               u4Counter = 0;
    tIkeDelSpi         *pDelSpiInfo = NULL;
    tSecv4Assoc        *pSecAssoc = NULL;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT4               u4InterfaceIndex = 0;
    UINT2               u2NumSPI = 0;

    pDelSpiInfo = &pMsg->IkeIfParam.DelSpiInfo;
    pDelSpiInfo->u2NumSPI = IPSEC_NTOHS (pDelSpiInfo->u2NumSPI);

    u2NumSPI = (UINT2) ((pDelSpiInfo->u2NumSPI <= IKE_MAX_NUM_SPI) ?
                        pDelSpiInfo->u2NumSPI : IKE_MAX_NUM_SPI);

    for (u4Counter = 0; u4Counter < u2NumSPI; u4Counter++)
    {
        pSecAssoc =
            Secv4GetAssocEntry (IPSEC_NTOHL (pDelSpiInfo->au4SPI[u4Counter]),
                                IPSEC_NTOHL (pDelSpiInfo->RemoteAddr.Ipv4Addr),
                                pDelSpiInfo->u1Protocol);

        if (pSecAssoc == NULL)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeDeleteSAMsg:"
                       "Unable to find SA specified by IKE\n");
        }
        else
        {
            if (SecUtilIpIfGetIfIndexFromIpAddress (IPSEC_NTOHL
                                                    (pDelSpiInfo->LocalAddr.
                                                     Ipv4Addr),
                                                    &u4InterfaceIndex) ==
                OSIX_FAILURE)
            {
                return;
            }

            Secv4DeletePeerSa (pSecAssoc->u4PeerSecAssocSpi,
                               IPSEC_NTOHL (pDelSpiInfo->LocalAddr.Ipv4Addr),
                               pDelSpiInfo->u1Protocol, u4InterfaceIndex,
                               pDelSpiInfo->bDelDynIPSecDB);

            if (pDelSpiInfo->bDelDynIPSecDB == TRUE)
            {
                /* Delete the Policy, Access & Selector Entries created for dynamic  */
                Secv4ProcessIkeDeleteDynamicIpsecDB (pSecAssoc->u4PolicyIndex,
                                                     u4InterfaceIndex,
                                                     SEC_OUTBOUND);
            }
            if (nmhTestv2Fsipv4SecAssocStatus (&u4SnmpErrorStatus,
                                               (INT4) pSecAssoc->
                                               u4SecAssocIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                return;
            }
            nmhSetFsipv4SecAssocStatus ((INT4) pSecAssoc->u4SecAssocIndex,
                                        DESTROY);

        }
    }
    return;
}

/***********************************************************************/
/*  Function Name : Secv4ProcessIkeDeleteSABySetTime                   */
/*  Description   : This function Deletes all the SA's present between */
/*                : the given pair of address by setting time          */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Msg Rcvd from IKE             */
/*                :                                                    */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4ProcessIkeDeleteSABySetTime (tIkeIPSecQMsg * pMsg)
{
    tIkeDelSAByAddr    *pDelSAByAddr = NULL;
    tSecv4Assoc        *pSecv4Assoc = NULL;
    UINT4               u4SecAssocIndex = 0;
    UINT4               u4PolicyIndex = 0;
    tSecv4Policy       *pEntry = NULL;
    UINT4               u4Time = 0;

    pDelSAByAddr = &(pMsg->IkeIfParam.DelSAInfo);

    pDelSAByAddr->RemoteAddr.Ipv4Addr =
        IPSEC_NTOHL (pDelSAByAddr->RemoteAddr.Ipv4Addr);
    pDelSAByAddr->LocalAddr.Ipv4Addr =
        IPSEC_NTOHL (pDelSAByAddr->LocalAddr.Ipv4Addr);

    TMO_SLL_Scan (&Secv4AssocList, pSecv4Assoc, tSecv4Assoc *)
    {

        if (((pSecv4Assoc->u4SecAssocDestAddr ==
              pDelSAByAddr->RemoteAddr.Ipv4Addr) &&
             (pSecv4Assoc->u4SecAssocSrcAddr ==
              pDelSAByAddr->LocalAddr.Ipv4Addr)) ||
            ((pSecv4Assoc->u4SecAssocDestAddr ==
              pDelSAByAddr->LocalAddr.Ipv4Addr) &&
             (pSecv4Assoc->u4SecAssocSrcAddr ==
              pDelSAByAddr->RemoteAddr.Ipv4Addr)))

        {
            u4SecAssocIndex = pSecv4Assoc->u4SecAssocIndex;
            u4PolicyIndex = pSecv4Assoc->u4PolicyIndex;
            pEntry = Secv4PolicyGetEntry (u4PolicyIndex);
            if (pEntry == NULL)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE,
                           "Secv4ProcessIkeDeleteSAByAddrMsg:"
                           "Unable to find policy for the given SA\n");
                continue;
            }
            u4Time = 5;
            Secv4StopTimer (&(pEntry->pau1SaEntry[0]->Secv4AssocSoftTimer));
            Secv4StartTimer (&(pEntry->pau1SaEntry[0]->Secv4AssocSoftTimer),
                             SECv4_SA_SOFT_TIMER_ID, u4Time);

            u4Time = 10;
            Secv4StopTimer (&(pEntry->pau1SaEntry[0]->Secv4AssocHardTimer));
            Secv4StartTimer (&(pEntry->pau1SaEntry[0]->Secv4AssocHardTimer),
                             SECv4_SA_HARD_TIMER_ID, u4Time);
        }
    }
    /* LOCK has been taken in the calling place */
#if 0
    Secv4IntimateIkeDeletionOfSa (pEntry, u4SecAssocIndex);
#endif
    UNUSED_PARAM (u4SecAssocIndex);
}

/***********************************************************************/
/*  Function Name : Secv4ProcessIkeDeleteSAByAddrMsg                   */
/*  Description   : This function Deletes all the SA's present between */
/*                : the given pair of address                          */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Msg Rcvd from IKE             */
/*                :                                                    */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv4ProcessIkeDeleteSAByAddrMsg (tIkeIPSecQMsg * pMsg)
{

    tIkeDelSAByAddr    *pDelSAByAddr = NULL;
    tSecv4Assoc        *pSecv4Assoc = NULL;
    UINT1               u1ParseCount = SECV4_ZERO;
    UINT4               u4SecAssocIndex = 0;
    UINT4               u4PolicyIndex = 0;
    tSecv4Policy       *pEntry = NULL;

    pDelSAByAddr = &(pMsg->IkeIfParam.DelSAInfo);

    pDelSAByAddr->RemoteAddr.Ipv4Addr =
        IPSEC_NTOHL (pDelSAByAddr->RemoteAddr.Ipv4Addr);
    pDelSAByAddr->LocalAddr.Ipv4Addr =
        IPSEC_NTOHL (pDelSAByAddr->LocalAddr.Ipv4Addr);


    /* There seems to be a logical issue here, where Secv4AssocList is scanned
     * for match and passed to delete the SA, where again the same
     * Secv4AssocList is modified (SA delete) which results in undefined
     * looping. for example, 254 entries in list results in 1750 iterations. For
     * now, trying to loop twice to give best try for deleting inbound SAs in one
     * iteration and outboud SAs in another iteration. 
     */
    for (u1ParseCount = SECV4_ZERO; u1ParseCount < SECV4_MAX_ITERATIONS; u1ParseCount++)
    {
        TMO_SLL_Scan (&Secv4AssocList, pSecv4Assoc, tSecv4Assoc *)
        {

            if (((pSecv4Assoc->u4SecAssocDestAddr ==
                  pDelSAByAddr->RemoteAddr.Ipv4Addr) &&
                 (pSecv4Assoc->u4SecAssocSrcAddr ==
                  pDelSAByAddr->LocalAddr.Ipv4Addr)) ||
                ((pSecv4Assoc->u4SecAssocDestAddr ==
                  pDelSAByAddr->LocalAddr.Ipv4Addr) &&
                 (pSecv4Assoc->u4SecAssocSrcAddr ==
                  pDelSAByAddr->RemoteAddr.Ipv4Addr)))

            {
                u4SecAssocIndex = pSecv4Assoc->u4SecAssocIndex;
                u4PolicyIndex = pSecv4Assoc->u4PolicyIndex;
                pEntry = Secv4PolicyGetEntry (u4PolicyIndex);
                if (pEntry == NULL)
                {
                    SECv4_TRC (SECv4_CONTROL_PLANE,
                               "Secv4ProcessIkeDeleteSAByAddrMsg:"
                               "Unable to find policy for the given SA\n");
                }
                else
                {
                    /* Just make sure if the SA is automatic. 
                       No need to disturb manual SA's */
                    if (SEC_AUTOMATIC != pSecv4Assoc->u1KeyingMode)
                    {
                        SECv4_TRC1(SECv4_CONTROL_PLANE,
                                   "Ignoring manual SA(%d) in "
                                   "IKE_DEL_SA_BY_ADDR\n\r", u4SecAssocIndex);
                        continue;
                    }

                    Secv4IntimateIkeDeletionOfSa (pEntry, u4SecAssocIndex);
                }
            }
        }
    }

}

/***********************************************************************/
/*  Function Name : Secv4ProcessIkeDeleteAllSAForLocal                 */
/*  Description   : This function Deletes all the SA's present between */
/*                : the given pair of address                          */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Msg Rcvd from IKE             */
/*                :                                                    */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv4ProcessIkeDeleteAllSAForLocal (tIkeIPSecQMsg * pMsg)
{

    tIkeDelSAByAddr    *pDelSAByAddr = NULL;
    tSecv4Assoc        *pSecv4Assoc = NULL;
    UINT4               u4SecAssocIndex = 0;
    UINT4               u4PolicyIndex = 0;
    tSecv4Policy       *pEntry = NULL;

    pDelSAByAddr = &(pMsg->IkeIfParam.DelSAInfo);

    pDelSAByAddr->RemoteAddr.Ipv4Addr =
        IPSEC_NTOHL (pDelSAByAddr->RemoteAddr.Ipv4Addr);
    pDelSAByAddr->LocalAddr.Ipv4Addr =
        IPSEC_NTOHL (pDelSAByAddr->LocalAddr.Ipv4Addr);

        TMO_SLL_Scan (&Secv4AssocList, pSecv4Assoc, tSecv4Assoc *)
        {
            /* When the Local configuration like Tunnel Term Addr Changes,
             * Delete all the SA's established with the old Tunnel Term Addr */
            if (((pDelSAByAddr->RemoteAddr.Ipv4Addr == 0) &&
                 (pSecv4Assoc->u4SecAssocSrcAddr ==
                  pDelSAByAddr->LocalAddr.Ipv4Addr)) ||
                ((pSecv4Assoc->u4SecAssocDestAddr ==
                  pDelSAByAddr->LocalAddr.Ipv4Addr) &&
                 (pDelSAByAddr->RemoteAddr.Ipv4Addr == 0)))

            {
                u4SecAssocIndex = pSecv4Assoc->u4SecAssocIndex;
                u4PolicyIndex = pSecv4Assoc->u4PolicyIndex;
                pEntry = Secv4PolicyGetEntry (u4PolicyIndex);
                if (pEntry == NULL)
                {
                    SECv4_TRC (SECv4_CONTROL_PLANE,
                               "Secv4ProcessIkeDeleteSAByAddrMsg:"
                               "Unable to find policy for the given SA\n");
                }
                else
                {
                    Secv4IntimateIkeDeletionOfSa (pEntry, u4SecAssocIndex);
                }
            }
        }

}

/***********************************************************************/
/*  Function Name : Secv4InstallSAFromIke                              */
/*  Description   : This function installs the SA rcvd from IKE        */
/*                :                                                    */
/*  Input(s)      : pIkeSA     - Points to the SA that is to be        */
/*                :              installed                             */
/*                : u4DestAddr - Remote Tunnel Termination Address     */
/*                : u4SrcAddr  - Local Tunnel Termination Address      */
/*                : u4SaIndex  - Index of SAD Entry                    */
/*                :                                                    */
/*  Output(s)     :Creates SA Entry in SAD                             */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/

INT1
Secv4InstallSAFromIke (tIPSecSA * pIkeSa, UINT4 u4DestAddr, UINT4 u4SrcAddr,
                       UINT4 u4SaIndex, UINT4 u4PeerSpi, UINT4 u4VpncIndex)
{
    UINT4               u4SnmpErrorStatus = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSecv4Assoc        *pSecv4Assoc = NULL;
    UINT4               u4Bytes = 0;
    INT4                i4Mode = 0;
    INT4                i4EncrAlgo = 0;

    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhTestv2Fsipv4SecAssocStatus
        (&u4SnmpErrorStatus, (INT4) u4SaIndex, CREATE_AND_GO) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocStatus Failed\n");
        return SEC_FAILURE;
    }

    if (nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, CREATE_AND_GO) ==
        SNMP_FAILURE)
    {

        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhSetFsipv4SecAssocStatus Failed\n");
        return SEC_FAILURE;
    }

    if (nmhTestv2Fsipv4SecAssocDstAddr
        (&u4SnmpErrorStatus, (INT4) u4SaIndex, u4DestAddr) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocDstAddr Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv4SecAssocDstAddr ((INT4) u4SaIndex, u4DestAddr);

    pSecv4Assoc = Secv4AssocGetEntry (u4SaIndex);

    if (pSecv4Assoc != NULL)
    {
        pSecv4Assoc->u4SecAssocSrcAddr = u4SrcAddr;
        pSecv4Assoc->u4PeerSecAssocSpi = u4PeerSpi;
        pSecv4Assoc->u1KeyingMode = SEC_AUTOMATIC;
        if (u4VpncIndex != 0)
        {
            pSecv4Assoc->u4VpncIndex = u4VpncIndex;
        }
        MEMCPY (&pSecv4Assoc->IkeNattInfo, &pIkeSa->IkeNattInfo,
                sizeof (tIkeNatT));
    }

    /* By default enabling Anti-Replay attack for SAs installed from IKE */
    if (nmhTestv2Fsipv4SecAssocAntiReplay (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                                           SEC_ANTI_REPLAY_ENABLE) ==
        SNMP_FAILURE)
    {

        nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocAntiReplay Failed\n");
        return SEC_FAILURE;
    }

    if (nmhSetFsipv4SecAssocAntiReplay ((INT4) u4SaIndex,
                                        SEC_ANTI_REPLAY_ENABLE) == SNMP_FAILURE)
    {

        nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhSetFsipv4SecAssocAntiReplay Failed\n");
        return SEC_FAILURE;
    }

    if (nmhTestv2Fsipv4SecAssocSpi (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                                    (INT4) pIkeSa->u4Spi) == SNMP_FAILURE)
    {

        nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocSpi Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv4SecAssocSpi ((INT4) u4SaIndex, (INT4) pIkeSa->u4Spi);

    if (pIkeSa->u1Mode == IKE_IPSEC_TUNNEL_MODE)
    {
        i4Mode = (INT4) SEC_TUNNEL;
    }
    else if (pIkeSa->u1Mode == IKE_IPSEC_TRANSPORT_MODE)
    {
        i4Mode = (INT4) SEC_TRANSPORT;
    }

    if (nmhTestv2Fsipv4SecAssocMode
        (&u4SnmpErrorStatus, (INT4) u4SaIndex, i4Mode) == SNMP_FAILURE)
    {

        nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocMode Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv4SecAssocMode ((INT4) u4SaIndex, i4Mode);

    if (nmhTestv2Fsipv4SecAssocProtocol
        (&u4SnmpErrorStatus, (INT4) u4SaIndex,
         (INT4) pIkeSa->u1IPSecProtocol) == SNMP_FAILURE)
    {

        nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocProtocol Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv4SecAssocProtocol ((INT4) u4SaIndex,
                                  (INT4) pIkeSa->u1IPSecProtocol);

    if (nmhTestv2Fsipv4SecAssocAhAlgo (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                                       (INT4) pIkeSa->u1HashAlgo) ==
        SNMP_FAILURE)
    {

        nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocAhAlgo Failed\n");
        return SEC_FAILURE;
    }

    nmhSetFsipv4SecAssocAhAlgo ((INT4) u4SaIndex, (INT4) pIkeSa->u1HashAlgo);

    if ((pIkeSa->u1HashAlgo != SEC_MD5)
        && (pIkeSa->u1HashAlgo != SEC_NULLAHALGO))
    {

        if (pIkeSa->u1HashAlgo == SEC_HMACMD5)
        {
            OctetStr.i4_Length = pIkeSa->u2AuthKeyLen;
            OctetStr.pu1_OctetList = pIkeSa->au1AuthKey;
        }
        else if (pIkeSa->u1HashAlgo == SEC_HMACSHA1)
        {
            OctetStr.i4_Length = pIkeSa->u2AuthKeyLen;
            OctetStr.pu1_OctetList = pIkeSa->au1AuthKey;
        }
        else if (pIkeSa->u1HashAlgo == SEC_XCBCMAC)
        {
            OctetStr.i4_Length = pIkeSa->u2AuthKeyLen;
            OctetStr.pu1_OctetList = pIkeSa->au1AuthKey;
        }
        else if (pIkeSa->u1HashAlgo == HMAC_SHA_256)
        {
            OctetStr.i4_Length = pIkeSa->u2AuthKeyLen;
            OctetStr.pu1_OctetList = pIkeSa->au1AuthKey;
        }
        else if (pIkeSa->u1HashAlgo == HMAC_SHA_384)
        {
            OctetStr.i4_Length = pIkeSa->u2AuthKeyLen;
            OctetStr.pu1_OctetList = pIkeSa->au1AuthKey;
        }
        else if (pIkeSa->u1HashAlgo == HMAC_SHA_512)
        {
            OctetStr.i4_Length = pIkeSa->u2AuthKeyLen;
            OctetStr.pu1_OctetList = pIkeSa->au1AuthKey;
        }

        if (nmhTestv2Fsipv4SecAssocAhKey (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                                          &OctetStr) == SNMP_FAILURE)
        {

            nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocAhKey Failed\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv4SecAssocAhKey ((INT4) u4SaIndex, &OctetStr);

    }

    if (pIkeSa->u1IPSecProtocol == SEC_ESP)
    {
        switch (pIkeSa->u1EncrAlgo)
        {
            case IKE_IPSEC_ESP_NULL:
                i4EncrAlgo = SEC_NULLESPALGO;
                break;
            case IKE_IPSEC_ESP_DES_CBC:
                i4EncrAlgo = SEC_DES_CBC;
                break;
            case IKE_IPSEC_ESP_3DES_CBC:
                i4EncrAlgo = SEC_3DES_CBC;
                break;
            case IKE_IPSEC_ESP_AES:
                i4EncrAlgo = SEC_AES;
                break;
            case IKE_IPSEC_ESP_AES_CTR:
                i4EncrAlgo = SEC_AESCTR;
                break;

            default:
                break;
        }

        if (nmhTestv2Fsipv4SecAssocEspAlgo
            (&u4SnmpErrorStatus, (INT4) u4SaIndex, i4EncrAlgo) == SNMP_FAILURE)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocEspAlgo Failed\n");
            return SEC_FAILURE;
        }

        nmhSetFsipv4SecAssocEspAlgo ((INT4) u4SaIndex, i4EncrAlgo);

        if (i4EncrAlgo != SEC_NULLESPALGO)
        {
            IPSEC_MEMSET (&OctetStr, 0, sizeof (OctetStr));
            if ((i4EncrAlgo == SEC_DES_CBC) &&
                (pIkeSa->u2EncrKeyLen != SEC_ESP_DES_KEY_LEN))
            {
                nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
                return SEC_FAILURE;
            }
            if ((i4EncrAlgo == SEC_DES_CBC) || (i4EncrAlgo == SEC_3DES_CBC))
            {
                OctetStr.i4_Length = SEC_ESP_DES_KEY_LEN;
                OctetStr.pu1_OctetList = pIkeSa->au1EncrKey;
            }
            else if (i4EncrAlgo == SEC_AES)
            {
                OctetStr.i4_Length = pIkeSa->u2EncrKeyLen;
                OctetStr.pu1_OctetList = pIkeSa->au1EncrKey;
            }
            else if (i4EncrAlgo == SEC_AESCTR)
            {
                OctetStr.i4_Length = (pIkeSa->u2EncrKeyLen);
                OctetStr.pu1_OctetList = pIkeSa->au1EncrKey;

            }

            if (nmhTestv2Fsipv4SecAssocEspKey
                (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                 &OctetStr) == SNMP_FAILURE)
            {

                SECv4_TRC (SECv4_CONTROL_PLANE,
                           "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocEspKey Failed\n");
                nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
                return SEC_FAILURE;
            }
            nmhSetFsipv4SecAssocEspKey ((INT4) u4SaIndex, &OctetStr);
            if (i4EncrAlgo == SEC_AESCTR)
            {
                IPSEC_MEMCPY (pSecv4Assoc->au1Nonce, pIkeSa->au1Nonce, 4);
            }
            if (i4EncrAlgo == SEC_3DES_CBC)
            {
                if (pIkeSa->u2EncrKeyLen != (3 * SEC_ESP_DES_KEY_LEN))
                {
                    nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
                    return SEC_FAILURE;
                }

                IPSEC_MEMSET (&OctetStr, 0, sizeof (OctetStr));
                OctetStr.i4_Length = SEC_ESP_DES_KEY_LEN;
                OctetStr.pu1_OctetList = &(pIkeSa->au1EncrKey[8]);

                if (nmhTestv2Fsipv4SecAssocEspKey2
                    (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                     &OctetStr) == SNMP_FAILURE)
                {

                    SECv4_TRC (SECv4_CONTROL_PLANE,
                               "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocEspKey2 Failed\n");
                    nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
                    return SEC_FAILURE;
                }
                nmhSetFsipv4SecAssocEspKey2 ((INT4) u4SaIndex, &OctetStr);

                IPSEC_MEMSET (&OctetStr, 0, sizeof (OctetStr));
                OctetStr.i4_Length = SEC_ESP_DES_KEY_LEN;
                OctetStr.pu1_OctetList = &(pIkeSa->au1EncrKey[16]);

                if (nmhTestv2Fsipv4SecAssocEspKey3
                    (&u4SnmpErrorStatus, (INT4) u4SaIndex,
                     &OctetStr) == SNMP_FAILURE)
                {

                    SECv4_TRC (SECv4_CONTROL_PLANE,
                               "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocEspKey3 Failed\n");
                    nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
                    return SEC_FAILURE;
                }
                nmhSetFsipv4SecAssocEspKey3 ((INT4) u4SaIndex, &OctetStr);

            }

        }

    }

    if (pIkeSa->u4LifeTime != 0)
    {

        if (nmhTestv2Fsipv4SecAssocLifetime
            (&u4SnmpErrorStatus, (INT4) u4SaIndex,
             (INT4) pIkeSa->u4LifeTime) == SNMP_FAILURE)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocLifetime Failed\n");
            return SEC_FAILURE;
        }
        nmhSetFsipv4SecAssocLifetime ((INT4) u4SaIndex,
                                      (INT4) pIkeSa->u4LifeTime);
    }
    if (pIkeSa->u4LifeTimeKB != 0)
    {
        u4Bytes = (SEC_BYTES_PER_KB * pIkeSa->u4LifeTimeKB);

        if (nmhTestv2Fsipv4SecAssocLifetimeInBytes
            (&u4SnmpErrorStatus, (INT4) u4SaIndex,
             (INT4) u4Bytes) == SNMP_FAILURE)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) u4SaIndex, DESTROY);
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4InstallSAFromIke: nmhTestv2Fsipv4SecAssocLifetimeInBytes Failed\n");
            return SEC_FAILURE;
        }
        nmhSetFsipv4SecAssocLifetimeInBytes ((INT4) u4SaIndex, (INT4) u4Bytes);
    }

    return SEC_SUCCESS;
}

/**************************************************************************/
/*  Function Name : Secv4RequeStIkeForNewOrReKey                          */
/*  Description   : This function is used to request IKE for new SA       */
/*                :                                                       */
/*  Input(s)      : u4IfIndex      - The Index of the Interface on which  */
/*                :                  the packet  arrived                  */
/*                : u4Protocol     - Higher Layer Protocol                */
/*                : u4Port         - Higher Layer Port Number             */
/*                : u4AccessIndex  - Index of the Access Data Base Entry  */
/*                : u4PktDir       -  Direction of the Packet             */
/*                : u1NewOrReKeyFlag - Flag indicating Request for New    */
/*                :                    Key or ReKeying                    */
/*                :                                                       */
/*  Output(s)     : None                                                  */
/*  Return        : None                                                  */
/**************************************************************************/

VOID
Secv4RequeStIkeForNewOrReKey (UINT4 u4IfIndex, UINT4 u4Protocol, UINT4 u4Port,
                              UINT4 u4AccessIndex, UINT4 u4PktDir,
                              UINT1 u1NewOrReKeyFlag,
                              tSecv4Policy * pSecv4Policy)
{

    tIkeQMsg           *pIkeQMsg = NULL;
    tSecv4Access       *pSecv4Access = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;

    IPSEC_MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (u4PktDir == SEC_INBOUND)
    {
        return;
    }
    if (MemAllocateMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 **) (VOID *) &pIkeQMsg)
        == MEM_FAILURE)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4RequeStIkeForNewOrReKey:- Failed to allocate memblock for "
                   " tIkeQMsg \n");
        return;
    }

    if (pIkeQMsg == NULL)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4RequeStIkeForNewOrReKey:- Memory Allocation Failure\n");
        return;
    }

    IPSEC_MEMSET (pIkeQMsg, 0, sizeof (tIkeQMsg));
    pIkeQMsg->u4MsgType = IPSEC_TASK;
    if (u1NewOrReKeyFlag == SEC_NEW_KEY)
    {
        pIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
            IPSEC_HTONL (IKE_IPSEC_NEW_KEY);
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.bRekey =
            FALSE;
    }
    else if (u1NewOrReKeyFlag == SEC_RE_KEY)
    {
        pIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
            IPSEC_HTONL (IKE_IPSEC_REKEY);
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.bRekey = TRUE;
        /* Get the Inbound SPI for the rekeying SPI */
        /* Get the Inbound SPI for the rekeying SPI */
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.
            u4ReKeyingSpi =
            IPSEC_HTONL (pSecv4Policy->pau1SaEntry[0]->u4PeerSecAssocSpi);

    }

    /* Copy the Protocol, SrcPort, Dst Port in each of the source
       and destination networks in the Trigger Info */
    if (u4Protocol == SEC_ANY_PROTOCOL)
    {
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1Protocol =
            0;
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
            u1HLProtocol = 0;
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.
            u1HLProtocol = 0;
    }
    else
    {
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1Protocol =
            (UINT1) u4Protocol;
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.
            u1HLProtocol = (UINT1) u4Protocol;
        pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
            u1HLProtocol = (UINT1) u4Protocol;
    }

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u2PortNumber =
        IPSEC_HTONS (((UINT2) u4Port));

    pSecv4Access = Secv4AccessGetEntry (u4AccessIndex);
    if (pSecv4Access == NULL)
    {
        SECv4_TRC (SEC_CNTRL_PATH,
                   "Secv4RequeStIkeForNewOrReKey: Access list not found");
        MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 *) pIkeQMsg);
        return;
    }

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.u4Type =
        IPSEC_HTONL (IKE_IPSEC_ID_IPV4_SUBNET);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.u4Type =
        IPSEC_HTONL (IKE_IPSEC_ID_IPV4_SUBNET);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
        uNetwork.Ip4Subnet.Ip4Addr = IPSEC_HTONL (pSecv4Access->u4SrcAddress);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
        uNetwork.Ip4Subnet.Ip4SubnetMask =
        IPSEC_HTONL (pSecv4Access->u4SrcMask);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.uNetwork.
        Ip4Subnet.Ip4Addr = IPSEC_HTONL (pSecv4Access->u4DestAddress);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.uNetwork.
        Ip4Subnet.Ip4SubnetMask = IPSEC_HTONL (pSecv4Access->u4DestMask);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
        u2StartPort = IPSEC_HTONS (pSecv4Access->u2LocalStartPort);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.SrcNet.
        u2EndPort = IPSEC_HTONS (pSecv4Access->u2LocalEndPort);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.
        u2StartPort = IPSEC_HTONS (pSecv4Access->u2RemoteStartPort);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.DestNet.
        u2EndPort = IPSEC_HTONS (pSecv4Access->u2RemoteEndPort);

    if (pSecv4Policy->pau1SaEntry[0] != NULL)
    {
        NetIpIfInfo.u4Addr = pSecv4Policy->pau1SaEntry[0]->u4SecAssocSrcAddr;
    }
    else
    {
        if (CfaGetIfIpAddr ((INT4) u4IfIndex, &NetIpIfInfo.u4Addr) ==
            OSIX_FAILURE)
        {
            SECv4_TRC1 (SEC_CNTRL_PATH, "Secv4RequeStIkeForNewOrReKey: "
                        "CfaGetIpAddr() returned failure for interface %lu!\r\n",
                        u4IfIndex);
            MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 *) pIkeQMsg);
            return;
        }
    }

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.LocalTEAddr.
        u4AddrType = IPSEC_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.LocalTEAddr.
        Ipv4Addr = IPSEC_HTONL (NetIpIfInfo.u4Addr);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.SaTriggerInfo.u1IkeVersion =
        pSecv4Policy->u1IkeVersion;

    IkeHandlePktFromIpsec (pIkeQMsg);
}

/***********************************************************************/
/*  Function Name : Secv4IntimateIkeInvalidSpi                         */
/*  Description   : This function is used to Intimate IKE the invalid  */
/*                : Spi                                                */
/*                :                                                    */
/*  Input(s)      : u4IfIndex  - The Index of the Interface on which   */
/*                : the packet  arrived                                */
/*                : u1Protocol - AH or ESP                             */
/*                : u4SrcAddr  - Source Address of the Packet          */
/*                : u4DestAddr - Dest Address of the Packet            */
/*                : u4Spi      - Spi from the Pkt Rcvd                 */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv4IntimateIkeInvalidSpi (UINT4 u4Spi, UINT4 u4Dest, UINT4 u4Src,
                            UINT1 u1Protocol)
{
    tIkeQMsg           *pIkeQMsg = NULL;

    if (MemAllocateMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, (UINT1 **) (VOID *) &pIkeQMsg)
        == MEM_FAILURE)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4IntimateIkeInvalidSpi:- Failed to allocate memblock for "
                   " tIkeQMsg \n");
        return;
    }
    if (pIkeQMsg == NULL)

    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4IntimateIkeInvalidSpi:- Memory Allocation Failure\n");
        return;
    }

    IPSEC_MEMSET (pIkeQMsg, 0, sizeof (tIkeQMsg));
    pIkeQMsg->u4MsgType = IPSEC_TASK;
    pIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
        IPSEC_HTONL (IKE_IPSEC_INVALID_SPI);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.u4Spi =
        IPSEC_HTONL (u4Spi);

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.u1Protocol =
        u1Protocol;

    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.LocalAddr.
        u4AddrType = IPSEC_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.RemoteAddr.
        u4AddrType = IPSEC_HTONL (IKE_IPSEC_ID_IPV4_ADDR);

    /* Dest of the Incoming Packet is our local address */
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.LocalAddr.
        Ipv4Addr = IPSEC_HTONL (u4Dest);
    pIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.InvalidSpiInfo.RemoteAddr.
        Ipv4Addr = IPSEC_HTONL (u4Src);

    IkeHandlePktFromIpsec (pIkeQMsg);
}

/***********************************************************************/
/*  Function Name : Secv4IntimateIkeDeletionOfSa                       */
/*  Description   : This function is used to intimate IKE deletion of  */
/*                : SA                                                 */
/*                :                                                    */
/*  Input(s)      : pSecv4Policy - Pointer to Policy Data Base         */
/*                :                                                    */
/*  Output(s)     : Deletion of SA from SAD                            */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv4IntimateIkeDeletionOfSa (tSecv4Policy * pSecv4Policy,
                              UINT4 u4SecAssocIndex)
{

    tIkeQMsg           *pAhIkeQMsg = NULL;
    tIkeQMsg           *pEspIkeQMsg = NULL;
    UINT4               u4IfIndex = 0;
    UINT1               u1Flag = SEC_NOT_FOUND;
    tSecv4Selector     *pSecv4Selector = NULL;
    UINT4               u4Count = 0;
    UINT4               u4SnmpErrorStatus = 0;
    UINT4               u4AHCount = 0;
    UINT4               u4ESPCount = 0;
    tSecv4Assoc        *pau1Secv4Assoc[SEC_MAX_BUNDLE_SA];
    UINT1               u1SaCount = 0;
    UINT1               u1MaxReKeySaCount = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPSEC_MEMSET (pau1Secv4Assoc, 0, SEC_MAX_BUNDLE_SA);

    TMO_SLL_Scan (&Secv4SelList, pSecv4Selector, tSecv4Selector *)
    {

        if (pSecv4Selector->pPolicyEntry == NULL)
        {
            continue;
        }
        if (pSecv4Selector->pPolicyEntry->u4PolicyIndex ==
            pSecv4Policy->u4PolicyIndex)
        {
            u1Flag = SEC_FOUND;
            break;
        }

    }

    if (u1Flag == SEC_NOT_FOUND)
    {
        return;
    }

    u4IfIndex = pSecv4Selector->u4IfIndex;
    u1Flag = SEC_NOT_FOUND;

    if (pSecv4Policy->pau1SaEntry[0] != NULL)
    {
        for (u4Count = 0; u4Count < pSecv4Policy->u1SaCount; u4Count++)
        {
            if (pSecv4Policy->pau1SaEntry[u4Count]->u4SecAssocIndex ==
                u4SecAssocIndex)
            {
                IPSEC_MEMCPY (pau1Secv4Assoc, pSecv4Policy->pau1SaEntry,
                              SEC_MAX_BUNDLE_SA);
                u1SaCount = pSecv4Policy->u1SaCount;
                u1Flag = SEC_FOUND;
                break;
            }
        }
    }
    if (u1Flag == SEC_NOT_FOUND)
    {
        for (u1MaxReKeySaCount = 0; u1MaxReKeySaCount < SEC_MAX_SA_TO_POLICY;
             u1MaxReKeySaCount++)
        {
            if ((pSecv4Policy->pau1NewSaEntry[u1MaxReKeySaCount][0]) != NULL)
            {
                for (u4Count = 0;
                     u4Count < pSecv4Policy->au1NewSaCount[u1MaxReKeySaCount];
                     u4Count++)
                {
                    if (pSecv4Policy->
                        pau1NewSaEntry[u1MaxReKeySaCount][u4Count]->
                        u4SecAssocIndex == u4SecAssocIndex)
                    {
                        IPSEC_MEMCPY (pau1Secv4Assoc, &(pSecv4Policy->
                                                        pau1NewSaEntry
                                                        [u1MaxReKeySaCount][0]),
                                      SEC_MAX_BUNDLE_SA);
                        u1SaCount =
                            pSecv4Policy->au1NewSaCount[u1MaxReKeySaCount];
                        u1Flag = SEC_FOUND;
                        break;
                    }
                }
            }
            if (u1Flag == SEC_FOUND)
            {
                break;
            }
        }
    }

    if (u1Flag == SEC_NOT_FOUND)
    {
        return;
    }

    for (u4Count = 0; u4Count < u1SaCount; u4Count++)
    {

        if (pau1Secv4Assoc[u4Count]->u1SecAssocProtocol == SEC_AH)
        {

            if (pSecv4Selector->u4PktDirection == SEC_INBOUND)
            {

                if (pAhIkeQMsg == NULL)
                {
                    if (MemAllocateMemBlock
                        (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                         (UINT1 **) (VOID *) &pAhIkeQMsg) == MEM_FAILURE)
                    {
                        SECv4_TRC (SECv4_OS_RESOURCE,
                                   "Secv4IntimateIkeDeletionOfSa:- Failed to allocate memblock for "
                                   " tIkeQMsg \n");
                        if (pEspIkeQMsg != NULL)
                        {
                            MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                                (UINT1 *) pEspIkeQMsg);
                        }
                        return;
                    }
                    if (pAhIkeQMsg == NULL)
                    {
                        SECv4_TRC (SECv4_OS_RESOURCE,
                                   "Secv4IntimateIkeDeletionOfSa:"
                                   "Memory Allocation Failure\n");
                        if (pEspIkeQMsg != NULL)
                        {
                            MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                                (UINT1 *) pEspIkeQMsg);
                        }
                        return;
                    }
                    IPSEC_MEMSET (pAhIkeQMsg, 0, sizeof (tIkeQMsg));
                    pAhIkeQMsg->u4MsgType = IPSEC_TASK;
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
                        IPSEC_HTONL (IKE_IPSEC_SEND_DELETE_SA);
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        LocalAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        RemoteAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        LocalAddr.Ipv4Addr =
                        IPSEC_HTONL (pau1Secv4Assoc[u4Count]->
                                     u4SecAssocDestAddr);
                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        RemoteAddr.Ipv4Addr =
                        IPSEC_HTONL (pau1Secv4Assoc[u4Count]->
                                     u4SecAssocSrcAddr);

                    pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        u1Protocol = SEC_AH;

                }

                pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    u2NumSPI = IPSEC_HTONS (((UINT2) (u4AHCount + 1)));

                pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    au4SPI[u4AHCount] =
                    IPSEC_HTONL (pau1Secv4Assoc[u4Count]->u4SecAssocSpi);
            }
            /* Delete the Policy, Access & Selector Entries created for dynamic  */
            if ((pSecv4Policy->pau1SaEntry[0] != NULL) &&
                (pSecv4Policy->pau1SaEntry[0]->u4PolicyIndex > 9000) &&
                (pSecv4Policy->u4PolicyIndex > 9000) &&
                (pSecv4Policy->pau1SaEntry[0]->u4PolicyIndex
                 == pSecv4Policy->u4PolicyIndex))
            {
                Secv4ProcessIkeDeleteDynamicIpsecDB (pSecv4Selector->
                                                     u4SelAccessIndex,
                                                     pSecv4Selector->u4IfIndex,
                                                     pSecv4Selector->
                                                     u4PktDirection);
            }

            if (nmhTestv2Fsipv4SecAssocStatus (&u4SnmpErrorStatus,
                                               (INT4) pau1Secv4Assoc[u4Count]->
                                               u4SecAssocIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                SECv4_TRC (SECv4_OS_RESOURCE,
                           "Secv4IntimateIkeDeletionOfSa:"
                           "nmhTestv2Fsipv4SecAssocStatus Failed\n");
                if (pAhIkeQMsg != NULL)
                {
                    MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                        (UINT1 *) pAhIkeQMsg);
                }
                if (pEspIkeQMsg != NULL)
                {
                    MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                        (UINT1 *) pEspIkeQMsg);
                }
                return;
            }
            nmhSetFsipv4SecAssocStatus ((INT4) pau1Secv4Assoc[u4Count]->
                                        u4SecAssocIndex, DESTROY);
            u4AHCount++;
        }                        /* protocol == AH */
        else if (pau1Secv4Assoc[u4Count]->u1SecAssocProtocol == SEC_ESP)
        {

            if (pSecv4Selector->u4PktDirection == SEC_INBOUND)
            {
                if (pEspIkeQMsg == NULL)
                {
                    if (MemAllocateMemBlock
                        (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                         (UINT1 **) (VOID *) &pEspIkeQMsg) == MEM_FAILURE)
                    {
                        SECv4_TRC (SECv4_OS_RESOURCE,
                                   "Secv4IntimateIkeDeletionOfSa:- Failed to allocate memblock for "
                                   " tIkeQMsg \n");
                        if (pAhIkeQMsg != NULL)
                        {
                            MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                                (UINT1 *) pAhIkeQMsg);
                        }
                        return;
                    }
                    if (pEspIkeQMsg == NULL)
                    {
                        SECv4_TRC (SECv4_OS_RESOURCE,
                                   "Secv4IntimateIkeDeletionOfSa:"
                                   "Memory Allocation Failure\n");
                        if (pAhIkeQMsg != NULL)
                        {
                            MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                                (UINT1 *) pAhIkeQMsg);
                        }
                        return;
                    }

                    IPSEC_MEMSET (pEspIkeQMsg, 0, sizeof (tIkeQMsg));
                    pEspIkeQMsg->u4MsgType = IPSEC_TASK;
                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.u4MsgType =
                        IPSEC_HTONL (IKE_IPSEC_SEND_DELETE_SA);

                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        LocalAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        RemoteAddr.u4AddrType =
                        IPSEC_HTONL (IKE_IPSEC_ID_IPV4_ADDR);
                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        LocalAddr.Ipv4Addr =
                        IPSEC_HTONL (pau1Secv4Assoc[u4Count]->
                                     u4SecAssocDestAddr);
                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        RemoteAddr.Ipv4Addr =
                        IPSEC_HTONL (pau1Secv4Assoc[u4Count]->
                                     u4SecAssocSrcAddr);

                    pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                        u1Protocol = SEC_ESP;
                }

                pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    u2NumSPI = IPSEC_HTONS (((UINT2) (u4ESPCount + 1)));
                pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
                    au4SPI[u4ESPCount] =
                    IPSEC_HTONL (pau1Secv4Assoc[u4Count]->u4SecAssocSpi);

            }

            /* Delete the Policy, Access & Selector Entries created for dynamic  */
            if ((pSecv4Policy->pau1SaEntry[0] != NULL) &&
                (pSecv4Policy->pau1SaEntry[0]->u4PolicyIndex > 9000) &&
                (pSecv4Policy->u4PolicyIndex > 9000) &&
                (pSecv4Policy->pau1SaEntry[0]->u4PolicyIndex
                 == pSecv4Policy->u4PolicyIndex))
            {
                Secv4ProcessIkeDeleteDynamicIpsecDB (pSecv4Selector->
                                                     u4SelAccessIndex,
                                                     pSecv4Selector->u4IfIndex,
                                                     pSecv4Selector->
                                                     u4PktDirection);
            }
            if (nmhTestv2Fsipv4SecAssocStatus (&u4SnmpErrorStatus,
                                               (INT4) pau1Secv4Assoc[u4Count]->
                                               u4SecAssocIndex,
                                               DESTROY) == SNMP_FAILURE)
            {
                SECv4_TRC (SECv4_OS_RESOURCE,
                           "Secv4IntimateIkeDeletionOfSa:"
                           "nmhTestv2Fsipv4SecAssocStatus Failed\n");
                if (pEspIkeQMsg != NULL)
                {
                    MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                        (UINT1 *) pEspIkeQMsg);
                }
                if (pAhIkeQMsg != NULL)
                {
                    MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl,
                                        (UINT1 *) pAhIkeQMsg);
                }
                return;
            }
            nmhSetFsipv4SecAssocStatus ((INT4) pau1Secv4Assoc[u4Count]->
                                        u4SecAssocIndex, DESTROY);
            u4ESPCount++;
        }                        /* protocol = ESP */

    }

    if (pAhIkeQMsg != NULL)
    {
        pAhIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
            u1IkeVersion = pSecv4Policy->u1IkeVersion;
        IkeHandlePktFromIpsec (pAhIkeQMsg);
    }

    if (pEspIkeQMsg != NULL)
    {
        pEspIkeQMsg->IkeIfParam.IkeIPSecParam.IPSecReq.DelSpiInfo.
            u1IkeVersion = pSecv4Policy->u1IkeVersion;
        IkeHandlePktFromIpsec (pEspIkeQMsg);
    }

    UNUSED_PARAM (u4IfIndex);
}

/***********************************************************************/
/*  Function Name : Secv4DeleteSa                                      */
/*  Description   : This function gets the Policy Entry based on the   */
/*                : SecAssocIndex and deletes all SA Matched to policy */
/*                :                                                    */
/*  Input(s)      : u4SecAssocIndex - Index of SAD                     */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
Secv4DeleteSa (UINT4 u4SecAssocIndex)
{

    tSecv4Policy       *pEntry = NULL;
    UINT1               u1Flag = SEC_NOT_FOUND;

    TMO_SLL_Scan (&Secv4PolicyList, pEntry, tSecv4Policy *)
    {
        if (pEntry->pau1SaEntry[0]->u4SecAssocIndex == u4SecAssocIndex)
        {
            u1Flag = SEC_FOUND;
            break;
        }

    }
    if (u1Flag == SEC_FOUND)
    {
        Secv4IntimateIkeDeletionOfSa (pEntry, u4SecAssocIndex);
    }

}

/***********************************************************************/
/*  Function Name : Secv4ConvertIkeInstallSAMsgToHostOrder             */
/*  Description   : This function converts the rcvd message to host    */
/*                : order                                              */
/*                :                                                    */
/*  Input(s)      :pInstallSAMsg :- Pointer to IPSec Bundle            */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
Secv4ConvertIkeInstallSAMsgToHostOrder (tIPSecBundle * pInstallSAMsg)
{

    pInstallSAMsg->LocTunnTermAddr.Ipv4Addr =
        IPSEC_NTOHL (pInstallSAMsg->LocTunnTermAddr.Ipv4Addr);

    pInstallSAMsg->RemTunnTermAddr.Ipv4Addr =
        IPSEC_NTOHL (pInstallSAMsg->RemTunnTermAddr.Ipv4Addr);

    pInstallSAMsg->LocalProxy.uNetwork.Ip4Subnet.Ip4Addr =
        IPSEC_NTOHL (pInstallSAMsg->LocalProxy.uNetwork.Ip4Subnet.Ip4Addr);

    pInstallSAMsg->LocalProxy.uNetwork.Ip4Subnet.Ip4SubnetMask =
        IPSEC_NTOHL (pInstallSAMsg->LocalProxy.uNetwork.Ip4Subnet.
                     Ip4SubnetMask);

    pInstallSAMsg->RemoteProxy.uNetwork.Ip4Subnet.Ip4Addr =
        IPSEC_NTOHL (pInstallSAMsg->RemoteProxy.uNetwork.Ip4Subnet.Ip4Addr);

    pInstallSAMsg->RemoteProxy.uNetwork.Ip4Subnet.Ip4SubnetMask =
        IPSEC_NTOHL (pInstallSAMsg->RemoteProxy.uNetwork.Ip4Subnet.
                     Ip4SubnetMask);

    if (pInstallSAMsg->aInSABundle[0].u4Spi != 0)
    {
        Secv4ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aInSABundle[0]);
    }

    if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
    {
        Secv4ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aInSABundle[1]);
    }

    if (pInstallSAMsg->aOutSABundle[0].u4Spi != 0)
    {
        Secv4ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aOutSABundle[0]);
    }

    if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
    {
        Secv4ConvertIkeIPSecSaToHostOrder (&pInstallSAMsg->aOutSABundle[1]);
    }

}

/***********************************************************************/
/*  Function Name : Secv4ConvertIkeIPSecSaToHostOrder                  */
/*  Description   : This function converts the IPSec Sa to Host Order  */
/*                :                                                    */
/*  Input(s)      :pInstallSAMsg :- Pointer to IPSec Bundle            */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
Secv4ConvertIkeIPSecSaToHostOrder (tIPSecSA * pIkeIPSecSa)
{

    pIkeIPSecSa->u4Spi = IPSEC_NTOHL (pIkeIPSecSa->u4Spi);

    pIkeIPSecSa->u2HLProtocol = IPSEC_NTOHS (pIkeIPSecSa->u2HLProtocol);

    pIkeIPSecSa->u2HLPortNumber = IPSEC_NTOHS (pIkeIPSecSa->u2HLPortNumber);

    pIkeIPSecSa->u4LifeTime = IPSEC_NTOHL (pIkeIPSecSa->u4LifeTime);

    pIkeIPSecSa->u4LifeTimeKB = IPSEC_NTOHL (pIkeIPSecSa->u4LifeTimeKB);

    pIkeIPSecSa->u2AuthKeyLen = IPSEC_NTOHS (pIkeIPSecSa->u2AuthKeyLen);

    pIkeIPSecSa->u2EncrKeyLen = IPSEC_NTOHS (pIkeIPSecSa->u2EncrKeyLen);
}

#if 0
/***********************************************************************/
/*  Function Name : IkeHandlePktFromIpsec                              */
/*  Description   : This function is used to post the message to IKE   */
/*                :                                                    */
/*  Input(s)      : tIkeQMsg - structure to hold the IKE Message       */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
IkeHandlePktFromIpsec (tIkeQMsg * pMsg)
{
    if (OsixSendToQ
        (SELF, (const UINT1 *) IKE_QUEUE_NAME, (tOsixMsg *) pMsg,
         OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "IKE - Send to Queue Failed\n");
        IPSEC_MEMFREE (pMsg);
        return;
    }
    if (OsixSendEvent (SELF, (const UINT1 *) IKE_TASK_NAME,
                       IKE_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "IKE - Send Event Failed\n");
        IPSEC_MEMFREE (pMsg);
        return;
    }
}

/***********************************************************************/
/*  Function Name : IkeHandlePktFromIpsec                              */
/*  Description   : This function hands over information to IKE        */
/*                :                                                    */
/*  Input(s)      :tIkeQMsg      :- Pointer to Q to be sent to ike     */
/*                :                                                    */
/*  Output(s)     :                                                    */
/*  Return        :                                                    */
/***********************************************************************/

VOID
IkeHandlePktFromIpsec (tIkeQMsg * pMsg)
{
    struct sk_buff     *skb = NULL;

    if ((skb =
         (struct sk_buff *) alloc_skb (sizeof (tIkeQMsg) + 50,
                                       GFP_ATOMIC)) == NULL)
    {
        return;
    }
#if 0
    ArOsixLkCopyFromUsrSpace (skb->data, pMsg, sizeof (tIkeQMsg));
#endif

    IPSEC_MEMCPY (skb->data, pMsg, sizeof (tIkeQMsg));
    IPSEC_MEMFREE (pMsg);

    skb->len = sizeof (tIkeQMsg);
    skb->tail = skb->data + skb->len;

    /* -1 is the device number given for ipsec-ike comm device which has interface number as 0 */
    /* print_packet((tIkeNewSA *)(skb + 2)); */
    if (GddSendToQ ((struct net_device *) -1, skb) < 0)
    {
        dev_kfree_skb_any (skb);
    }
    return;
}
#endif

VOID
Secv4DeletePeerSa (UINT4 u4PeerSpi, UINT4 u4PeerAddr, UINT1 u1Protocol,
                   UINT4 u4InterfaceIndex, BOOLEAN bIsDynamic)
{

    tSecv4Assoc        *pSecAssoc = NULL;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;

    pSecAssoc = Secv4GetAssocEntry (u4PeerSpi, u4PeerAddr, u1Protocol);

    if (pSecAssoc == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4DeletePeerSA:" "Unable to find SA specified by IKE\n");
    }
    else
    {
        if (bIsDynamic == TRUE)
        {
            /* Delete the Policy, Access & Selector Entries created for dynamic 
             * for INBOUND Direction */
            Secv4ProcessIkeDeleteDynamicIpsecDB (pSecAssoc->u4PolicyIndex,
                                                 u4InterfaceIndex, SEC_INBOUND);
        }
        if (nmhTestv2Fsipv4SecAssocStatus (&u4SnmpErrorStatus,
                                           (INT4) pSecAssoc->u4SecAssocIndex,
                                           DESTROY) == SNMP_FAILURE)
        {
            return;
        }
        nmhSetFsipv4SecAssocStatus ((INT4) pSecAssoc->u4SecAssocIndex, DESTROY);

    }
    return;
}
