/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssecv4lw.c,v 1.13 2014/03/07 11:57:40 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "secv4com.h"
#include  "fssnmp.h"
#include  "fssecv4lw.h"
#include "snmcdefn.h"
#include "snmccons.h"
#include "snmctdfs.h"
#include "snmcport.h"
#include "secv4esp.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SecGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsipv4SecGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecGlobalStatus (INT4 *pi4RetValFsipv4SecGlobalStatus)
{
    *pi4RetValFsipv4SecGlobalStatus = gSecv4Status;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SecVersion
 Input       :  The Indices

                The Object
                retValFsipv4SecVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecVersion (UINT4 *pu4RetValFsipv4SecVersion)
{
    *pu4RetValFsipv4SecVersion = gSecv4Version;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SecDummyPktGen
 Input       :  The Indices

                The Object
                retValFsVpnDummyPktGen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecDummyPktGen (INT4 *pi4RetValFsipv4SecDummyPktGen)
{
    *pi4RetValFsipv4SecDummyPktGen = (INT4) gSecv4DummyPktFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecDummyPktParam
 Input       :  The Indices

                The Object
                retValFsVpnDummyPktParam
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecDummyPktParam (INT4 *pi4RetValFsipv4SecDummyPktParam)
{
    *pi4RetValFsipv4SecDummyPktParam = (INT4) gSecv4DummyPktLength;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsipv4SecGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecGlobalDebug (INT4 *pi4RetValFsipv4SecGlobalDebug)
{

    *pi4RetValFsipv4SecGlobalDebug = (INT4) gu4Secv4Debug;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecMaxSA
 Input       :  The Indices

                The Object 
                retValFsipv4SecMaxSA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecMaxSA (INT4 *pi4RetValFsipv4SecMaxSA)
{
    UNUSED_PARAM (pi4RetValFsipv4SecMaxSA);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv4SecGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsipv4SecGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecGlobalStatus (INT4 i4SetValFsipv4SecGlobalStatus)
{
    if (gSecv4Status == i4SetValFsipv4SecGlobalStatus)
    {
        /* Already Enabled/Disabled , return SUCCESS */
        return (SNMP_SUCCESS);
    }

    if (i4SetValFsipv4SecGlobalStatus == SEC_ENABLE)
    {
        /*Initilizing the file descriptor in case of Freescale processor.
           In other cases this funtion will simply returns SEC_SUCCESS */
        if (Secv4HwInit () == SEC_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        Secv4HwDeInit ();
    }
    gSecv4Status = (UINT1) i4SetValFsipv4SecGlobalStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv4SecDummyPktGen
 Input       :  The Indices

                The Object
                setValFsVpnDummyPktGen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecDummyPktGen (INT4 i4SetValFsipv4SecDummyPktGen)
{
    gSecv4DummyPktFlag = (UINT1) i4SetValFsipv4SecDummyPktGen;
    if (gSecv4DummyPktFlag == SEC_ENABLE)
    {
        /* Start the Default Dummy Packet Timer */
        SECv4_INIT_TIMER (&gSecv4TmrDummyPkt);
        Secv4StartTimer (&gSecv4TmrDummyPkt, SECv4_DUMMY_PKT_TIMER_ID,
                         SECv4_DUMMY_TMR_INTERVAL);
    }
    else
    {
        Secv4StopTimer (&gSecv4TmrDummyPkt);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecDummyPktParam
 Input       :  The Indices

                The Object
                setValFsVpnDummyPktParam
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecDummyPktParam (INT4 i4SetValFsipv4SecDummyPktParam)
{
    gSecv4DummyPktLength = (UINT1) i4SetValFsipv4SecDummyPktParam;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecGlobalDebug
 Input       :  The Indices

                The Object 
                setValFsipv4SecGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecGlobalDebug (INT4 i4SetValFsipv4SecGlobalDebug)
{
/*
    switch (i4SetValFsipv4SecGlobalDebug)
    {
        case SEC_DISABLE_ALL:
            gu4Secv4Debug = SECv4_DISABLE_ALL_TRC;
            break;
        case SEC_ENABLE_ALL:
            gu4Secv4Debug |= SECv4_ENABLE_ALL_TRC;
            break;
        case SEC_INIT_SHUT:
            gu4Secv4Debug |= INIT_SHUT_TRC;
            break;
        case SEC_MGMT:
            gu4Secv4Debug |= MGMT_TRC;
            break;
        case SEC_DATA_PATH:
            gu4Secv4Debug |= DATA_PATH_TRC;
            break;
        case SEC_CNTRL_PATH:
            gu4Secv4Debug |= CONTROL_PLANE_TRC;
            break;
        case SEC_DUMP:
            gu4Secv4Debug |= DUMP_TRC;
            break;
        case SEC_OS_RESOURCE:
            gu4Secv4Debug |= OS_RESOURCE_TRC;
            break;
        case SEC_ALL_FAILURE:
            gu4Secv4Debug |= ALL_FAILURE_TRC;
            break;
        case SEC_BUFFER:
            gu4Secv4Debug |= BUFFER_TRC;
            break;
        default:*/
    gu4Secv4Debug = (UINT4) i4SetValFsipv4SecGlobalDebug;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsipv4SecMaxSA
 Input       :  The Indices

                The Object 
                setValFsipv4SecMaxSA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecMaxSA (INT4 i4SetValFsipv4SecMaxSA)
{
    UNUSED_PARAM (i4SetValFsipv4SecMaxSA);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsipv4SecGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecGlobalStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsipv4SecGlobalStatus)
{
    if ((i4TestValFsipv4SecGlobalStatus != SEC_ENABLE) &&
        (i4TestValFsipv4SecGlobalStatus != SEC_DISABLE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecDummyPktGen
 Input       :  The Indices

                The Object
                testValFsVpnDummyPktGen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecDummyPktGen (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsipv4SecDummyPktGen)
{
    if ((i4TestValFsipv4SecDummyPktGen != SEC_ENABLE) &&
        (i4TestValFsipv4SecDummyPktGen != SEC_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecDummyPktParam
 Input       :  The Indices

                The Object
                testValFsVpnDummyPktParam
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecDummyPktParam (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsipv4SecDummyPktParam)
{
    if ((i4TestValFsipv4SecDummyPktParam < 1) ||
        (i4TestValFsipv4SecDummyPktParam > SEC_MAX_DUMMY_PKT_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsipv4SecGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecGlobalDebug (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsipv4SecGlobalDebug)
{
    if ((i4TestValFsipv4SecGlobalDebug < 0) ||
        (i4TestValFsipv4SecGlobalDebug > 9))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecMaxSA
 Input       :  The Indices

                The Object 
                testValFsipv4SecMaxSA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecMaxSA (UINT4 *pu4ErrorCode, INT4 i4TestValFsipv4SecMaxSA)
{
    if ((UINT4) i4TestValFsipv4SecMaxSA < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Fsipv4SecSelectorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecSelectorTable
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv4SecSelectorTable (INT4 i4Fsipv4SelIndex,
                                                INT4 i4Fsipv4SelIfIndex,
                                                INT4 i4Fsipv4SelProtoIndex,
                                                INT4 i4Fsipv4SelAccessIndex,
                                                INT4 i4Fsipv4SelPort,
                                                INT4 i4Fsipv4SelPktDirection)
{

    if ((i4Fsipv4SelIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelIfIndex < 0)
        || (i4Fsipv4SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv4SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv4SelPktDirection != SEC_OUTBOUND)))
    {

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecSelectorTable
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv4SecSelectorTable (INT4 *pi4Fsipv4SelIndex,
                                        INT4 *pi4Fsipv4SelIfIndex,
                                        INT4 *pi4Fsipv4SelProtoIndex,
                                        INT4 *pi4Fsipv4SelAccessIndex,
                                        INT4 *pi4Fsipv4SelPort,
                                        INT4 *pi4Fsipv4SelPktDirection)
{

    tSecv4Selector     *pSecSelIf = NULL, *pFirstSecSelIf = NULL;
    pFirstSecSelIf = (tSecv4Selector *) TMO_SLL_First (&Secv4SelList);

    if (pFirstSecSelIf == NULL)
        return SNMP_FAILURE;

    /*  Get the First Lexicographical ordered Index */

    TMO_SLL_Scan (&Secv4SelList, pSecSelIf, tSecv4Selector *)
    {
        if (Secv4SelectorCompareGetNext (pSecSelIf,
                                         (INT4) pFirstSecSelIf->u4SelIndex,
                                         (INT4) pFirstSecSelIf->u4IfIndex,
                                         (INT4) pFirstSecSelIf->u4ProtoId,
                                         (INT4) pFirstSecSelIf->
                                         u4SelAccessIndex,
                                         (INT4) pFirstSecSelIf->u4Port,
                                         (INT4) pFirstSecSelIf->
                                         u4PktDirection) < 0)
        {
            pFirstSecSelIf = pSecSelIf;
        }
    }
    *pi4Fsipv4SelIndex = (INT4) pFirstSecSelIf->u4SelIndex;
    *pi4Fsipv4SelIfIndex = (INT4) pFirstSecSelIf->u4IfIndex;
    *pi4Fsipv4SelProtoIndex = (INT4) pFirstSecSelIf->u4ProtoId;
    *pi4Fsipv4SelPort = (INT4) pFirstSecSelIf->u4Port;
    *pi4Fsipv4SelPktDirection = (INT4) pFirstSecSelIf->u4PktDirection;
    *pi4Fsipv4SelAccessIndex = (INT4) pFirstSecSelIf->u4SelAccessIndex;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecSelectorTable
 Input       :  The Indices
                Fsipv4SelIndex
                nextFsipv4SelIndex
                Fsipv4SelIfIndex
                nextFsipv4SelIfIndex
                Fsipv4SelProtoIndex
                nextFsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                nextFsipv4SelAccessIndex
                Fsipv4SelPort
                nextFsipv4SelPort
                Fsipv4SelPktDirection
                nextFsipv4SelPktDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecSelectorTable (INT4 i4Fsipv4SelIndex,
                                       INT4 *pi4NextFsipv4SelIndex,
                                       INT4 i4Fsipv4SelIfIndex,
                                       INT4 *pi4NextFsipv4SelIfIndex,
                                       INT4 i4Fsipv4SelProtoIndex,
                                       INT4 *pi4NextFsipv4SelProtoIndex,
                                       INT4 i4Fsipv4SelAccessIndex,
                                       INT4 *pi4NextFsipv4SelAccessIndex,
                                       INT4 i4Fsipv4SelPort,
                                       INT4 *pi4NextFsipv4SelPort,
                                       INT4 i4Fsipv4SelPktDirection,
                                       INT4 *pi4NextFsipv4SelPktDirection)
{

    tSecv4Selector     *pSecSelIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;
    pSecSelIf = (tSecv4Selector *) TMO_SLL_First (&Secv4SelList);

    if (pSecSelIf == NULL)
        return (SNMP_FAILURE);

    /*  To Get the Next lexicographically ordered Index for the given Index */

    TMO_SLL_Scan (&Secv4SelList, pSecSelIf, tSecv4Selector *)
    {

        if ((Secv4SelectorCompareGetNext (pSecSelIf, i4Fsipv4SelIndex,
                                          i4Fsipv4SelIfIndex,
                                          i4Fsipv4SelProtoIndex,
                                          i4Fsipv4SelAccessIndex,
                                          i4Fsipv4SelPort,
                                          i4Fsipv4SelPktDirection)) > 0)
        {

            if (Flag == SEC_NOT_FOUND)
            {
                Flag = SEC_FOUND;
                *pi4NextFsipv4SelIndex = (INT4) pSecSelIf->u4SelIndex;
                *pi4NextFsipv4SelIfIndex = (INT4) pSecSelIf->u4IfIndex;
                *pi4NextFsipv4SelProtoIndex = (INT4) pSecSelIf->u4ProtoId;
                *pi4NextFsipv4SelPort = (INT4) pSecSelIf->u4Port;
                *pi4NextFsipv4SelPktDirection =
                    (INT4) pSecSelIf->u4PktDirection;
                *pi4NextFsipv4SelAccessIndex =
                    (INT4) pSecSelIf->u4SelAccessIndex;
            }
            else
            {

                if ((Secv4SelectorCompareGetNext (pSecSelIf,
                                                  *pi4NextFsipv4SelIndex,
                                                  *pi4NextFsipv4SelIfIndex,
                                                  *pi4NextFsipv4SelProtoIndex,
                                                  *pi4NextFsipv4SelAccessIndex,
                                                  *pi4NextFsipv4SelPort,
                                                  *pi4NextFsipv4SelPktDirection))
                    < 0)
                {
                    *pi4NextFsipv4SelIndex = (INT4) pSecSelIf->u4SelIndex;
                    *pi4NextFsipv4SelIfIndex = (INT4) pSecSelIf->u4IfIndex;
                    *pi4NextFsipv4SelProtoIndex = (INT4) pSecSelIf->u4ProtoId;
                    *pi4NextFsipv4SelPort = (INT4) pSecSelIf->u4Port;
                    *pi4NextFsipv4SelPktDirection =
                        (INT4) pSecSelIf->u4PktDirection;
                    *pi4NextFsipv4SelAccessIndex =
                        (INT4) pSecSelIf->u4SelAccessIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SelFilterFlag
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                retValFsipv4SelFilterFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SelFilterFlag (INT4 i4Fsipv4SelIndex, INT4 i4Fsipv4SelIfIndex,
                           INT4 i4Fsipv4SelProtoIndex,
                           INT4 i4Fsipv4SelAccessIndex, INT4 i4Fsipv4SelPort,
                           INT4 i4Fsipv4SelPktDirection,
                           INT4 *pi4RetValFsipv4SelFilterFlag)
{
    tSecv4Selector     *pSecSelIf = NULL;

    UNUSED_PARAM (i4Fsipv4SelIfIndex);
    UNUSED_PARAM (i4Fsipv4SelProtoIndex);
    UNUSED_PARAM (i4Fsipv4SelAccessIndex);
    UNUSED_PARAM (i4Fsipv4SelPort);

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsipv4SelFilterFlag = (INT4) pSecSelIf->u1SelFilterFlag;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SelPolicyIndex
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                retValFsipv4SelPolicyIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SelPolicyIndex (INT4 i4Fsipv4SelIndex, INT4 i4Fsipv4SelIfIndex,
                            INT4 i4Fsipv4SelProtoIndex,
                            INT4 i4Fsipv4SelAccessIndex, INT4 i4Fsipv4SelPort,
                            INT4 i4Fsipv4SelPktDirection,
                            INT4 *pi4RetValFsipv4SelPolicyIndex)
{
    tSecv4Selector     *pSecSelIf = NULL;
    UNUSED_PARAM (i4Fsipv4SelIfIndex);
    UNUSED_PARAM (i4Fsipv4SelProtoIndex);
    UNUSED_PARAM (i4Fsipv4SelAccessIndex);
    UNUSED_PARAM (i4Fsipv4SelPort);

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsipv4SelPolicyIndex = (INT4) pSecSelIf->u4SelPolicyIndex;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SelStatus
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                retValFsipv4SelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SelStatus (INT4 i4Fsipv4SelIndex, INT4 i4Fsipv4SelIfIndex,
                       INT4 i4Fsipv4SelProtoIndex, INT4 i4Fsipv4SelAccessIndex,
                       INT4 i4Fsipv4SelPort, INT4 i4Fsipv4SelPktDirection,
                       INT4 *pi4RetValFsipv4SelStatus)
{
    tSecv4Selector     *pSecSelIf = NULL;

    UNUSED_PARAM (i4Fsipv4SelIfIndex);
    UNUSED_PARAM (i4Fsipv4SelProtoIndex);
    UNUSED_PARAM (i4Fsipv4SelAccessIndex);
    UNUSED_PARAM (i4Fsipv4SelPort);

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);

    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {

        *pi4RetValFsipv4SelStatus = ACTIVE;
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv4SelFilterFlag
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                setValFsipv4SelFilterFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SelFilterFlag (INT4 i4Fsipv4SelIndex, INT4 i4Fsipv4SelIfIndex,
                           INT4 i4Fsipv4SelProtoIndex,
                           INT4 i4Fsipv4SelAccessIndex, INT4 i4Fsipv4SelPort,
                           INT4 i4Fsipv4SelPktDirection,
                           INT4 i4SetValFsipv4SelFilterFlag)
{
    tSecv4Selector     *pSecSelIf = NULL;

    UNUSED_PARAM (i4Fsipv4SelIfIndex);
    UNUSED_PARAM (i4Fsipv4SelProtoIndex);
    UNUSED_PARAM (i4Fsipv4SelAccessIndex);
    UNUSED_PARAM (i4Fsipv4SelPort);

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);
    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecSelIf->u1SelFilterFlag = (UINT1) i4SetValFsipv4SelFilterFlag;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv4SelPolicyIndex
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                setValFsipv4SelPolicyIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SelPolicyIndex (INT4 i4Fsipv4SelIndex, INT4 i4Fsipv4SelIfIndex,
                            INT4 i4Fsipv4SelProtoIndex,
                            INT4 i4Fsipv4SelAccessIndex, INT4 i4Fsipv4SelPort,
                            INT4 i4Fsipv4SelPktDirection,
                            INT4 i4SetValFsipv4SelPolicyIndex)
{
    tSecv4Selector     *pSecSelIf = NULL;

    UNUSED_PARAM (i4Fsipv4SelIfIndex);
    UNUSED_PARAM (i4Fsipv4SelProtoIndex);
    UNUSED_PARAM (i4Fsipv4SelAccessIndex);
    UNUSED_PARAM (i4Fsipv4SelPort);

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);
    if (pSecSelIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecSelIf->u4SelPolicyIndex = (UINT4) i4SetValFsipv4SelPolicyIndex;
    pSecSelIf->pPolicyEntry = Secv4PolicyGetEntry (pSecSelIf->u4SelPolicyIndex);

    if (Secv4DeleteFormattedSelectorEntries () != SEC_SUCCESS)
    {
        return (SNMP_FAILURE);
    }
    TMO_SLL_Init (&Secv4SelFormattedList);
    if (Secv4ConstSecList () != SEC_SUCCESS)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SelStatus
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                setValFsipv4SelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SelStatus (INT4 i4Fsipv4SelIndex, INT4 i4Fsipv4SelIfIndex,
                       INT4 i4Fsipv4SelProtoIndex, INT4 i4Fsipv4SelAccessIndex,
                       INT4 i4Fsipv4SelPort, INT4 i4Fsipv4SelPktDirection,
                       INT4 i4SetValFsipv4SelStatus)
{

    tSecv4Selector     *pSecSelIf = NULL;
    tSecv4Selector     *pSecFormSelIf = NULL;

    switch (i4SetValFsipv4SelStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (MemAllocateMemBlock
                (SECv4_SEL_MEMPOOL,
                 (UINT1 **) (VOID *) &pSecSelIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            IPSEC_MEMSET (pSecSelIf, 0, sizeof (tSecv4Selector));

            pSecSelIf->u4SelIndex = (UINT4) i4Fsipv4SelIndex;
            pSecSelIf->u4IfIndex = (UINT4) i4Fsipv4SelIfIndex;
            pSecSelIf->u4ProtoId = (UINT4) i4Fsipv4SelProtoIndex;
            pSecSelIf->u4Port = (UINT4) i4Fsipv4SelPort;
            pSecSelIf->u4PktDirection = (UINT4) i4Fsipv4SelPktDirection;
            pSecSelIf->u4SelAccessIndex = (UINT4) i4Fsipv4SelAccessIndex;
            TMO_SLL_Add (&Secv4SelList, (tTMO_SLL_NODE *) pSecSelIf);

            break;

        case ACTIVE:
            pSecSelIf = Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex,
                                          i4Fsipv4SelPktDirection);

            if (pSecSelIf == NULL)
            {
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:
        {
            /* get the entry for the given indicies from the selector dbase */
            pSecSelIf = Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex,
                                          i4Fsipv4SelPktDirection);
            if (pSecSelIf == NULL)
            {
                return (SNMP_FAILURE);
            }

            TMO_SLL_Delete (&Secv4SelList, (tTMO_SLL_NODE *) pSecSelIf);
            IPSEC_MEMSET (pSecSelIf, 0, sizeof (tSecv4Selector));
            if (MemReleaseMemBlock
                (SECv4_SEL_MEMPOOL, (UINT1 *) pSecSelIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            /* get the entry for the given indicies from the formatted selector
               database */
            pSecFormSelIf =
                Secv4SelGetFormattedEntry ((UINT4) i4Fsipv4SelIfIndex,
                                           (UINT4) i4Fsipv4SelProtoIndex,
                                           (UINT4) i4Fsipv4SelAccessIndex,
                                           i4Fsipv4SelPort,
                                           i4Fsipv4SelPktDirection);
            if (pSecFormSelIf != NULL)
            {
                TMO_SLL_Delete (&Secv4SelFormattedList,
                                (tTMO_SLL_NODE *) pSecFormSelIf);
                IPSEC_MEMSET (pSecFormSelIf, 0, sizeof (tSecv4Selector));
                MemReleaseMemBlock (SECv4_SEL_MEMPOOL, (UINT1 *) pSecFormSelIf);
            }
        }
            break;

        default:
            break;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SelFilterFlag
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                testValFsipv4SelFilterFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SelFilterFlag (UINT4 *pu4ErrorCode, INT4 i4Fsipv4SelIndex,
                              INT4 i4Fsipv4SelIfIndex,
                              INT4 i4Fsipv4SelProtoIndex,
                              INT4 i4Fsipv4SelAccessIndex,
                              INT4 i4Fsipv4SelPort,
                              INT4 i4Fsipv4SelPktDirection,
                              INT4 i4TestValFsipv4SelFilterFlag)
{

    tSecv4Selector     *pSecSelIf = NULL;

    if ((i4TestValFsipv4SelFilterFlag != SEC_FILTER)
        && (i4TestValFsipv4SelFilterFlag != SEC_ALLOW)
        && (i4TestValFsipv4SelFilterFlag != SEC_BYPASS)
        && (i4TestValFsipv4SelFilterFlag != SEC_APPLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check whether the Indices are within the range */

    if ((i4Fsipv4SelIndex < 0)
        || (i4Fsipv4SelIfIndex < 0)
        || (i4Fsipv4SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv4SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv4SelPktDirection != SEC_OUTBOUND) &&
            (i4Fsipv4SelPktDirection != SEC_ANY_DIRECTION)))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);
    if (pSecSelIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SelPolicyIndex
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                testValFsipv4SelPolicyIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SelPolicyIndex (UINT4 *pu4ErrorCode, INT4 i4Fsipv4SelIndex,
                               INT4 i4Fsipv4SelIfIndex,
                               INT4 i4Fsipv4SelProtoIndex,
                               INT4 i4Fsipv4SelAccessIndex,
                               INT4 i4Fsipv4SelPort,
                               INT4 i4Fsipv4SelPktDirection,
                               INT4 i4TestValFsipv4SelPolicyIndex)
{

    tSecv4Selector     *pSecSelIf = NULL;
    tSecv4Policy       *pSecPolicyIf = NULL;
/* Check whether the Indices are within the range */
    if (i4TestValFsipv4SelPolicyIndex <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check whether the Indices are within the range */

    if ((i4Fsipv4SelIndex < 0)
        || (i4Fsipv4SelIfIndex < 0)
        || (i4Fsipv4SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv4SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv4SelPktDirection != SEC_OUTBOUND) &&
            (i4Fsipv4SelPktDirection != SEC_ANY_DIRECTION)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);
    /*Check for the given policy index there is an entry in policy data base */
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4TestValFsipv4SelPolicyIndex);
    if (pSecSelIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SelStatus
 Input       :  The Indices
                Fsipv4SelIndex
                Fsipv4SelIfIndex
                Fsipv4SelProtoIndex
                Fsipv4SelAccessIndex
                Fsipv4SelPort
                Fsipv4SelPktDirection

                The Object 
                testValFsipv4SelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SelStatus (UINT4 *pu4ErrorCode, INT4 i4Fsipv4SelIndex,
                          INT4 i4Fsipv4SelIfIndex,
                          INT4 i4Fsipv4SelProtoIndex,
                          INT4 i4Fsipv4SelAccessIndex,
                          INT4 i4Fsipv4SelPort,
                          INT4 i4Fsipv4SelPktDirection,
                          INT4 i4TestValFsipv4SelStatus)
{
    tSecv4Selector     *pSecSelIf = NULL;
    tSecv4Selector     *pSecSelIf2 = NULL;
    tSecv4Access       *pSecAccIf = NULL;
    /* Check whether the Indices are within the range */

    if ((i4Fsipv4SelIndex < 0)
        || (i4Fsipv4SelIfIndex < 0)
        || (i4Fsipv4SelProtoIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelAccessIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SelPort < SEC_MIN_INTEGER)
        || ((i4Fsipv4SelPktDirection != SEC_INBOUND) &&
            (i4Fsipv4SelPktDirection != SEC_OUTBOUND) &&
            (i4Fsipv4SelPktDirection != SEC_ANY_DIRECTION)))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SelStatus != CREATE_AND_GO)
        && (i4TestValFsipv4SelStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return SNMP_FAILURE;
    }

    /* get the entry for the given indicies from the selector database */
    pSecSelIf =
        Secv4SelGetEntry ((UINT4) i4Fsipv4SelIndex, i4Fsipv4SelPktDirection);
    if ((i4TestValFsipv4SelStatus == CREATE_AND_GO))
        /*|| (i4TestValFsipv4SelStatus == CREATE_AND_WAIT)) */
    {

        if (pSecSelIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        /* Below Check prevents cntradictory configuration settings i.e,
         * Two differenet selectors, one  allowing & another filtering
         * the same accesslist, proto & port combinations.
         */
        pSecSelIf2 = Secv4SelGetEntry2 ((UINT4) i4Fsipv4SelIfIndex,
                                        (UINT4) i4Fsipv4SelAccessIndex,
                                        (UINT4) i4Fsipv4SelProtoIndex,
                                        i4Fsipv4SelPort);
        if (pSecSelIf2 != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
            return (SNMP_FAILURE);
        }
    }
    else
    {
        if (pSecSelIf == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
            return (SNMP_FAILURE);
        }
    }

    /*Check for the given access index there is an entry in the
       access list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SelAccessIndex);
    if (pSecAccIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

/*    if (i4TestValFsipv4SelStatus == DESTROY)
    {

        if (pSecSelIf == NULL)
        {

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }

    }
*/
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Fsipv4SecAccessTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecAccessTable
 Input       :  The Indices
                Fsipv4SecAccessIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv4SecAccessTable (INT4 i4Fsipv4SecAccessIndex)
{
    /* Check whether the Indices are within the range */

    if (i4Fsipv4SecAccessIndex < SEC_MIN_INTEGER)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecAccessTable
 Input       :  The Indices
                Fsipv4SecAccessIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv4SecAccessTable (INT4 *pi4Fsipv4SecAccessIndex)
{
    tSecv4Access       *pSecAccIf = NULL;
    UINT4               u4AccessIndex;
    pSecAccIf = (tSecv4Access *) TMO_SLL_First (&Secv4AccessList);
    if (pSecAccIf == NULL)
        return SNMP_FAILURE;
    u4AccessIndex = pSecAccIf->u4GroupIndex;
    /* Get the First lexicographically ordered first index */
    TMO_SLL_Scan (&Secv4AccessList, pSecAccIf, tSecv4Access *)
    {
        if (pSecAccIf->u4GroupIndex < u4AccessIndex)
        {
            u4AccessIndex = pSecAccIf->u4GroupIndex;
        }
    }

    *pi4Fsipv4SecAccessIndex = (INT4) u4AccessIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecAccessTable
 Input       :  The Indices
                Fsipv4SecAccessIndex
                nextFsipv4SecAccessIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecAccessTable (INT4 i4Fsipv4SecAccessIndex,
                                     INT4 *pi4NextFsipv4SecAccessIndex)
{
    tSecv4Access       *pSecAccIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;
    pSecAccIf = (tSecv4Access *) TMO_SLL_First (&Secv4AccessList);
    if (pSecAccIf == NULL)
        return SNMP_FAILURE;
    /* To get the Next lexicographically orederd index for the given Index */
    TMO_SLL_Scan (&Secv4AccessList, pSecAccIf, tSecv4Access *)
    {
        if (pSecAccIf->u4GroupIndex > (UINT4) i4Fsipv4SecAccessIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv4SecAccessIndex = (INT4) pSecAccIf->u4GroupIndex;
                Flag = SEC_FOUND;
            }
            else
            {
                if (pSecAccIf->u4GroupIndex <
                    (UINT4) *pi4NextFsipv4SecAccessIndex)
                {
                    *pi4NextFsipv4SecAccessIndex =
                        (INT4) pSecAccIf->u4GroupIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SecAccessStatus
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                retValFsipv4SecAccessStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAccessStatus (INT4 i4Fsipv4SecAccessIndex,
                             INT4 *pi4RetValFsipv4SecAccessStatus)
{

    tSecv4Access       *pSecAccIf = NULL;
    /* get the entry from the inaccess list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pi4RetValFsipv4SecAccessStatus = ACTIVE;
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SecSrcNet
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                retValFsipv4SecSrcNet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecSrcNet (INT4 i4Fsipv4SecAccessIndex,
                       UINT4 *pu4RetValFsipv4SecSrcNet)
{

    tSecv4Access       *pSecAccIf = NULL;
    /* get the entry from the inaccess list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsipv4SecSrcNet = pSecAccIf->u4SrcAddress;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecSrcMask
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                retValFsipv4SecSrcMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecSrcMask (INT4 i4Fsipv4SecAccessIndex,
                        UINT4 *pu4RetValFsipv4SecSrcMask)
{

    tSecv4Access       *pSecAccIf = NULL;
    /* get the entry from the inaccess list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsipv4SecSrcMask = pSecAccIf->u4SrcMask;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecDestNet
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                retValFsipv4SecDestNet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecDestNet (INT4 i4Fsipv4SecAccessIndex,
                        UINT4 *pu4RetValFsipv4SecDestNet)
{

    tSecv4Access       *pSecAccIf = NULL;
    /* get an entry from the outaccess list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsipv4SecDestNet = pSecAccIf->u4DestAddress;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecDestMask
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                retValFsipv4SecDestMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecDestMask (INT4 i4Fsipv4SecAccessIndex,
                         UINT4 *pu4RetValFsipv4SecDestMask)
{

    tSecv4Access       *pSecAccIf = NULL;
    /* get an entry from the outaccess list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsipv4SecDestMask = pSecAccIf->u4DestMask;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv4SecAccessStatus
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                setValFsipv4SecAccessStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAccessStatus (INT4 i4Fsipv4SecAccessIndex,
                             INT4 i4SetValFsipv4SecAccessStatus)
{

    tSecv4Access       *pSecAccIf = NULL;

    switch (i4SetValFsipv4SecAccessStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (MemAllocateMemBlock
                (SECv4_ACC_MEMPOOL,
                 (UINT1 **) (VOID *) &pSecAccIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            IPSEC_MEMSET (pSecAccIf, 0, sizeof (tSecv4Access));
            pSecAccIf->u4GroupIndex = (UINT4) i4Fsipv4SecAccessIndex;
            /* Add default Values to the access node */
            pSecAccIf->u4SrcAddress = SEC_MIN_IP_ADDRESS;
            pSecAccIf->u4SrcMask = SEC_MAX_IP_ADDRESS;
            pSecAccIf->u4DestAddress = SEC_MIN_IP_ADDRESS;
            pSecAccIf->u4DestMask = SEC_MAX_IP_ADDRESS;
            TMO_SLL_Add (&Secv4AccessList, (tTMO_SLL_NODE *) pSecAccIf);
            break;

        case ACTIVE:
            pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);

            if (pSecAccIf == NULL)
            {
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:

            pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
            if (pSecAccIf == NULL)
            {
                return SNMP_FAILURE;
            }
            TMO_SLL_Delete (&Secv4AccessList, (tTMO_SLL_NODE *) pSecAccIf);
            IPSEC_MEMSET (pSecAccIf, 0, sizeof (tSecv4Access));
            if (MemReleaseMemBlock
                (SECv4_ACC_MEMPOOL, (UINT1 *) pSecAccIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
            break;

        default:
            break;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecSrcNet
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                setValFsipv4SecSrcNet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecSrcNet (INT4
                       i4Fsipv4SecAccessIndex, UINT4 u4SetValFsipv4SecSrcNet)
{
    tSecv4Access       *pSecAccIf = NULL;
    /* get the entry from the access list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);

    if (pSecAccIf == NULL)
    {
        return SNMP_FAILURE;
    }
    pSecAccIf->u4SrcAddress = u4SetValFsipv4SecSrcNet;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecSrcMask
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                setValFsipv4SecSrcMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecSrcMask (INT4
                        i4Fsipv4SecAccessIndex, UINT4 u4SetValFsipv4SecSrcMask)
{

    tSecv4Access       *pSecAccIf = NULL;
    /* get the entry from the access list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAccIf->u4SrcMask = u4SetValFsipv4SecSrcMask;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecDestNet
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                setValFsipv4SecDestNet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecDestNet (INT4
                        i4Fsipv4SecAccessIndex, UINT4 u4SetValFsipv4SecDestNet)
{
    tSecv4Access       *pSecAccIf = NULL;
    /* get an entry from the access list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAccIf->u4DestAddress = u4SetValFsipv4SecDestNet;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecDestMask
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                setValFsipv4SecDestMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecDestMask (INT4
                         i4Fsipv4SecAccessIndex,
                         UINT4 u4SetValFsipv4SecDestMask)
{
    tSecv4Access       *pSecAccIf = NULL;

    /* get an entry from the outaccess list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAccIf->u4DestMask = u4SetValFsipv4SecDestMask;

    /* Check whether any access list exists for the same src and dest.
     * Dont allow to create overlapping entries.
     */
    if (Secv4CheckOverlappingAccessList (pSecAccIf) == SEC_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    /* Sort the entries lexographically to make the access easier */
    Secv4ConstAccessList ();

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAccessStatus
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                testValFsipv4SecAccessStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAccessStatus (UINT4
                                *pu4ErrorCode,
                                INT4
                                i4Fsipv4SecAccessIndex,
                                INT4 i4TestValFsipv4SecAccessStatus)
{

    tSecv4Access       *pSecAccIf = NULL;
    if (i4Fsipv4SecAccessIndex < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if ((i4TestValFsipv4SecAccessStatus !=
         CREATE_AND_GO) && (i4TestValFsipv4SecAccessStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return SNMP_FAILURE;
    }

    if (i4TestValFsipv4SecAccessStatus == DESTROY)
    {
        if (pSecAccIf == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsipv4SecAccessStatus == CREATE_AND_GO))
    {
        if (pSecAccIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecSrcNet
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                testValFsipv4SecSrcNet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecSrcNet (UINT4
                          *pu4ErrorCode,
                          INT4
                          i4Fsipv4SecAccessIndex,
                          UINT4 u4TestValFsipv4SecSrcNet)
{
    tSecv4Access       *pSecAccIf = NULL;

    /* get the entry from the access list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (!((IPSEC_IS_ADDR_CLASS_A (u4TestValFsipv4SecSrcNet))
          || (IPSEC_IS_ADDR_CLASS_B (u4TestValFsipv4SecSrcNet))
          || (IPSEC_IS_ADDR_CLASS_C (u4TestValFsipv4SecSrcNet))
          || (u4TestValFsipv4SecSrcNet == 0)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecSrcMask
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                testValFsipv4SecSrcMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecSrcMask (UINT4
                           *pu4ErrorCode,
                           INT4
                           i4Fsipv4SecAccessIndex,
                           UINT4 u4TestValFsipv4SecSrcMask)
{
    tSecv4Access       *pSecAccIf = NULL;

    /* get the entry from the access list */
    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4TestValFsipv4SecSrcMask > SEC_MAX_IP_ADDRESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecDestNet
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                testValFsipv4SecDestNet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecDestNet (UINT4 *pu4ErrorCode,
                           INT4 i4Fsipv4SecAccessIndex,
                           UINT4 u4TestValFsipv4SecDestNet)
{
    tSecv4Access       *pSecAccIf = NULL;

    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (!((IPSEC_IS_ADDR_CLASS_A (u4TestValFsipv4SecDestNet))
          || (IPSEC_IS_ADDR_CLASS_B (u4TestValFsipv4SecDestNet))
          || (IPSEC_IS_ADDR_CLASS_C (u4TestValFsipv4SecDestNet))
          || (u4TestValFsipv4SecDestNet == 0)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecDestMask
 Input       :  The Indices
                Fsipv4SecAccessIndex

                The Object 
                testValFsipv4SecDestMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecDestMask (UINT4 *pu4ErrorCode,
                            INT4 i4Fsipv4SecAccessIndex,
                            UINT4 u4TestValFsipv4SecDestMask)
{
    tSecv4Access       *pSecAccIf = NULL;

    if (u4TestValFsipv4SecDestMask > SEC_MAX_IP_ADDRESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSecAccIf = Secv4AccessGetEntry ((UINT4) i4Fsipv4SecAccessIndex);
    if (pSecAccIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Fsipv4SecPolicyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecPolicyTable
 Input       :  The Indices
                Fsipv4SecPolicyIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsipv4SecPolicyTable (INT4 i4Fsipv4SecPolicyIndex)
{

    if (i4Fsipv4SecPolicyIndex < SEC_MIN_INTEGER)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecPolicyTable
 Input       :  The Indices
                Fsipv4SecPolicyIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsipv4SecPolicyTable (INT4 *pi4Fsipv4SecPolicyIndex)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER;
    INT4                Flag = SEC_NOT_FOUND;

    /*To get the first lexicographically ordered index */
    TMO_SLL_Scan (&Secv4PolicyList, pSecPolicyIf, tSecv4Policy *)
    {
        Flag = SEC_FOUND;
        if (pSecPolicyIf->u4PolicyIndex < (UINT4) u4FirstIndex)
        {
            u4FirstIndex = pSecPolicyIf->u4PolicyIndex;
        }
    }
    if (!Flag)
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv4SecPolicyIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecPolicyTable
 Input       :  The Indices
                Fsipv4SecPolicyIndex
                nextFsipv4SecPolicyIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecPolicyTable (INT4 i4Fsipv4SecPolicyIndex,
                                     INT4 *pi4NextFsipv4SecPolicyIndex)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;

    /* To get the Next lexicographically orederd index for the given index */
    TMO_SLL_Scan (&Secv4PolicyList, pSecPolicyIf, tSecv4Policy *)
    {
        if (pSecPolicyIf->u4PolicyIndex > (UINT4) i4Fsipv4SecPolicyIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv4SecPolicyIndex =
                    (INT4) pSecPolicyIf->u4PolicyIndex;
                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecPolicyIf->u4PolicyIndex <
                    (UINT4) *pi4NextFsipv4SecPolicyIndex)
                {
                    *pi4NextFsipv4SecPolicyIndex =
                        (INT4) pSecPolicyIf->u4PolicyIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SecPolicyFlag
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                retValFsipv4SecPolicyFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecPolicyFlag (INT4 i4Fsipv4SecPolicyIndex,
                           INT4 *pi4RetValFsipv4SecPolicyFlag)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecPolicyFlag = pSecPolicyIf->u1PolicyFlag;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecPolicyMode
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                retValFsipv4SecPolicyMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecPolicyMode (INT4 i4Fsipv4SecPolicyIndex,
                           INT4 *pi4RetValFsipv4SecPolicyMode)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecPolicyMode = pSecPolicyIf->u1PolicyMode;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecPolicySaBundle
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                retValFsipv4SecPolicySaBundle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecPolicySaBundle (INT4 i4Fsipv4SecPolicyIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pRetValFsipv4SecPolicySaBundle)
{

    tSecv4Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);

    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pRetValFsipv4SecPolicySaBundle->pu1_OctetList != NULL)
    {
        IPSEC_STRCPY (pRetValFsipv4SecPolicySaBundle->
                      pu1_OctetList, pSecPolicyIf->au1PolicySaBundle);

        pRetValFsipv4SecPolicySaBundle->i4_Length =
            (INT4) STRLEN (pSecPolicyIf->au1PolicySaBundle);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecPolicyStatus
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                retValFsipv4SecPolicyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecPolicyStatus (INT4
                             i4Fsipv4SecPolicyIndex,
                             INT4 *pi4RetValFsipv4SecPolicyStatus)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecPolicyStatus = ACTIVE;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv4SecPolicyFlag
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                setValFsipv4SecPolicyFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecPolicyFlag (INT4
                           i4Fsipv4SecPolicyIndex,
                           INT4 i4SetValFsipv4SecPolicyFlag)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecPolicyIf->u1PolicyFlag = (UINT1) i4SetValFsipv4SecPolicyFlag;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecPolicyMode
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                setValFsipv4SecPolicyMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhSetFsipv4SecPolicyMode (INT4 i4Fsipv4SecPolicyIndex,
                           INT4 i4SetValFsipv4SecPolicyMode)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecPolicyIf->u1PolicyMode = (UINT1) i4SetValFsipv4SecPolicyMode;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecPolicySaBundle
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                setValFsipv4SecPolicySaBundle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecPolicySaBundle (INT4 i4Fsipv4SecPolicyIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pSetValFsipv4SecPolicySaBundle)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    INT4                i4BackCount = SEC_BACK_COUNT;
    INT4                i4Num = 0;
    INT4                i4Tens = SEC_TENS;
    UINT4               u4Time = 0;
    UINT4               u4Factor = 0;

    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (pSecPolicyIf->au1PolicySaBundle, 0, SEC_MAX_SA_BUNDLE_LEN);
    IPSEC_MEMSET (pSecPolicyIf->pau1SaEntry, 0, SEC_MAX_BUNDLE_SA);
    IPSEC_MEMCPY (pSecPolicyIf->au1PolicySaBundle,
                  pSetValFsipv4SecPolicySaBundle->pu1_OctetList,
                  pSetValFsipv4SecPolicySaBundle->i4_Length);
    pSecPolicyIf->u1SaCount = 0;
    /* Extract the secassoc indicies form the sa bundle in string format
       and store them as integers */
    do
    {
        i4BackCount++;
        i4Num = 0;
        while ((pSetValFsipv4SecPolicySaBundle->
                pu1_OctetList[i4BackCount] != '.')
               && (pSetValFsipv4SecPolicySaBundle->
                   pu1_OctetList[i4BackCount] != '\0'))
        {
            i4Num = i4Num * i4Tens +
                (pSetValFsipv4SecPolicySaBundle->
                 pu1_OctetList[i4BackCount] - '0');
            i4BackCount++;
        }
        pSecPolicyIf->pau1SaEntry[pSecPolicyIf->u1SaCount] =
            Secv4AssocGetEntry ((UINT4) i4Num);
        if ((pSecPolicyIf->u1SaCount < SEC_MAX_BUNDLE_SA) &&
            (pSecPolicyIf->pau1SaEntry[pSecPolicyIf->u1SaCount] != NULL))
        {
            pSecPolicyIf->pau1SaEntry[pSecPolicyIf->u1SaCount]->u4PolicyIndex =
                (UINT4) i4Fsipv4SecPolicyIndex;
        }
        pSecPolicyIf->u1SaCount++;
    }
    while ((i4BackCount < pSetValFsipv4SecPolicySaBundle->i4_Length)
           && (pSecPolicyIf->u1SaCount < SEC_MAX_BUNDLE_SA));

    if (pSecPolicyIf->u1PolicyMode == SEC_AUTOMATIC)
    {

        if (pSecPolicyIf->pau1SaEntry[0]->u4LifeTime != 0)
        {
            OsixGetSysTime (&u4Time);
            u4Factor = (SEC_MIN_SOFTLIFETIME_FACTOR + (u4Time % 10));
            u4Time =
                ((pSecPolicyIf->pau1SaEntry[0]->u4LifeTime * u4Factor) / 100);
            pSecPolicyIf->pau1SaEntry[0]->Secv4AssocSoftTimer.u4Param1 =
                pSecPolicyIf->u4PolicyIndex;
            pSecPolicyIf->pau1SaEntry[0]->Secv4AssocSoftTimer.u4SaIndex =
                pSecPolicyIf->pau1SaEntry[0]->u4SecAssocIndex;
            Secv4StartTimer (&(pSecPolicyIf->pau1SaEntry[0]->
                               Secv4AssocSoftTimer), SECv4_SA_SOFT_TIMER_ID,
                             u4Time);

            pSecPolicyIf->pau1SaEntry[0]->Secv4AssocHardTimer.u4Param1 =
                pSecPolicyIf->u4PolicyIndex;
            pSecPolicyIf->pau1SaEntry[0]->Secv4AssocHardTimer.u4SaIndex =
                (UINT4) pSecPolicyIf->pau1SaEntry[0]->u4SecAssocIndex;
            Secv4StartTimer (&(pSecPolicyIf->pau1SaEntry[0]->
                               Secv4AssocHardTimer), SECv4_SA_HARD_TIMER_ID,
                             pSecPolicyIf->pau1SaEntry[0]->u4LifeTime);
        }
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv4SecPolicyStatus
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                setValFsipv4SecPolicyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecPolicyStatus (INT4 i4Fsipv4SecPolicyIndex,
                             INT4 i4SetValFsipv4SecPolicyStatus)
{
    tSecv4Policy       *pSecPolicyIf = NULL;

    switch (i4SetValFsipv4SecPolicyStatus)

    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (MemAllocateMemBlock
                (SECv4_POL_MEMPOOL,
                 (UINT1 **) (VOID *) &pSecPolicyIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            IPSEC_MEMSET (pSecPolicyIf, 0, sizeof (tSecv4Policy));
            /* Add an entry in the policy list */
            pSecPolicyIf->u4PolicyIndex = (UINT4) i4Fsipv4SecPolicyIndex;
            SECv4_INIT_TIMER (&(pSecPolicyIf->Secv4PolicyTrigIkeTmr));
            TMO_SLL_Add (&Secv4PolicyList, (tTMO_SLL_NODE *) pSecPolicyIf);
            break;

        case ACTIVE:

            pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
            if (pSecPolicyIf == NULL)
            {
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:

            pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
            if (pSecPolicyIf == NULL)
            {
                return SNMP_FAILURE;
            }
            TMO_SLL_Delete (&Secv4PolicyList, (tTMO_SLL_NODE *) pSecPolicyIf);
            Secv4StopTimer (&(pSecPolicyIf->Secv4PolicyTrigIkeTmr));
            Secv4DeletePolicyPtrInSel ((UINT4) i4Fsipv4SecPolicyIndex);
            IPSEC_MEMSET (pSecPolicyIf, 0, sizeof (tSecv4Policy));
            if (MemReleaseMemBlock
                (SECv4_POL_MEMPOOL, (UINT1 *) pSecPolicyIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
            break;

        default:
            break;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecPolicyFlag
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                testValFsipv4SecPolicyFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecPolicyFlag (UINT4 *pu4ErrorCode,
                              INT4 i4Fsipv4SecPolicyIndex,
                              INT4 i4TestValFsipv4SecPolicyFlag)
{

    tSecv4Policy       *pSecPolicyIf = NULL;
    if ((i4TestValFsipv4SecPolicyFlag
         != SEC_APPLY) && (i4TestValFsipv4SecPolicyFlag != SEC_BYPASS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecPolicyMode
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                testValFsipv4SecPolicyMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecPolicyMode (UINT4 *pu4ErrorCode,
                              INT4 i4Fsipv4SecPolicyIndex,
                              INT4 i4TestValFsipv4SecPolicyMode)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    if ((i4TestValFsipv4SecPolicyMode
         != SEC_MANUAL) && (i4TestValFsipv4SecPolicyMode != SEC_AUTOMATIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecPolicySaBundle
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                testValFsipv4SecPolicySaBundle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecPolicySaBundle (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv4SecPolicyIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsipv4SecPolicySaBundle)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    INT4                i4BackCount = SEC_BACK_COUNT;
    INT4                i4Num = 0;
    INT4                i4Tens = SEC_TENS;

    if ((pTestValFsipv4SecPolicySaBundle->i4_Length < SEC_MIN_SA_BUNDLE_LEN)
        || (pTestValFsipv4SecPolicySaBundle->i4_Length >=
            SEC_MAX_SA_BUNDLE_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    do
    {
        i4BackCount++;
        i4Num = 0;
        while ((pTestValFsipv4SecPolicySaBundle->pu1_OctetList[i4BackCount] !=
                '.')
               && (pTestValFsipv4SecPolicySaBundle->
                   pu1_OctetList[i4BackCount] != '\0'))
        {
            i4Num = i4Num * i4Tens +
                (pTestValFsipv4SecPolicySaBundle->
                 pu1_OctetList[i4BackCount] - '0');
            i4BackCount++;
        }

        if (Secv4AssocGetEntry ((UINT4) i4Num) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
            return (SNMP_FAILURE);
        }
    }

    while (i4BackCount < pTestValFsipv4SecPolicySaBundle->i4_Length);
    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecPolicyStatus
 Input       :  The Indices
                Fsipv4SecPolicyIndex

                The Object 
                testValFsipv4SecPolicyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecPolicyStatus (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv4SecPolicyIndex,
                                INT4 i4TestValFsipv4SecPolicyStatus)
{

    tSecv4Policy       *pSecPolicyIf = NULL;

    if (i4Fsipv4SecPolicyIndex < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecPolicyStatus != CREATE_AND_GO)
        && (i4TestValFsipv4SecPolicyStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return SNMP_FAILURE;
    }

    pSecPolicyIf = Secv4PolicyGetEntry ((UINT4) i4Fsipv4SecPolicyIndex);
    if ((i4TestValFsipv4SecPolicyStatus == CREATE_AND_GO))
        /* || (i4TestValFsipv4SecPolicyStatus == CREATE_AND_WAIT )) */
    {

        if (pSecPolicyIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }

    if (i4TestValFsipv4SecPolicyStatus == DESTROY)
    {
        if (pSecPolicyIf == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv4SecAssocTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecAssocTable
 Input       :  The Indices
                Fsipv4SecAssocIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv4SecAssocTable (INT4 i4Fsipv4SecAssocIndex)
{
    if (i4Fsipv4SecAssocIndex < SEC_MIN_INTEGER)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecAssocTable
 Input       :  The Indices
                Fsipv4SecAssocIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsipv4SecAssocTable (INT4 *pi4Fsipv4SecAssocIndex)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER;
    INT4                Flag = SEC_NOT_FOUND;
    TMO_SLL_Scan (&Secv4AssocList, pSecAssocIf, tSecv4Assoc *)
    {
        if (pSecAssocIf->u4SecAssocIndex < (UINT4) u4FirstIndex)
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecAssocIf->u4SecAssocIndex;
        }
    }
    if (!Flag)
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv4SecAssocIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecAssocTable
 Input       :  The Indices
                Fsipv4SecAssocIndex
                nextFsipv4SecAssocIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecAssocTable (INT4 i4Fsipv4SecAssocIndex,
                                    INT4 *pi4NextFsipv4SecAssocIndex)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;
    TMO_SLL_Scan (&Secv4AssocList, pSecAssocIf, tSecv4Assoc *)
    {

        if (pSecAssocIf->u4SecAssocIndex > (UINT4) i4Fsipv4SecAssocIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv4SecAssocIndex =
                    (INT4) pSecAssocIf->u4SecAssocIndex;
                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecAssocIf->
                    u4SecAssocIndex < (UINT4) *pi4NextFsipv4SecAssocIndex)
                {
                    *pi4NextFsipv4SecAssocIndex =
                        (INT4) pSecAssocIf->u4SecAssocIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocDstAddr
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocDstAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocDstAddr (INT4 i4Fsipv4SecAssocIndex,
                             UINT4 *pu4RetValFsipv4SecAssocDstAddr)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsipv4SecAssocDstAddr = pSecAssocIf->u4SecAssocDestAddr;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocProtocol
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocProtocol (INT4 i4Fsipv4SecAssocIndex,
                              INT4 *pi4RetValFsipv4SecAssocProtocol)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocProtocol = (INT4) pSecAssocIf->u1SecAssocProtocol;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocSpi
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocSpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocSpi (INT4
                         i4Fsipv4SecAssocIndex,
                         INT4 *pi4RetValFsipv4SecAssocSpi)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocSpi = (INT4) pSecAssocIf->u4SecAssocSpi;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocMode
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocMode (INT4 i4Fsipv4SecAssocIndex,
                          INT4 *pi4RetValFsipv4SecAssocMode)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocMode = (INT4) pSecAssocIf->u1SecAssocMode;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocAhAlgo
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocAhAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocAhAlgo (INT4 i4Fsipv4SecAssocIndex,
                            INT4 *pi4RetValFsipv4SecAssocAhAlgo)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocAhAlgo = (INT4) pSecAssocIf->u1SecAssocAhAlgo;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocAhKey
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocAhKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocAhKey (INT4 i4Fsipv4SecAssocIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsipv4SecAssocAhKey)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocAhKey != NULL)
    {
        IPSEC_MEMCPY (pRetValFsipv4SecAssocAhKey->
                      pu1_OctetList, pSecAssocIf->pu1SecAssocAhKey,
                      pSecAssocIf->u1SecAssocAhKeyLength);
        pRetValFsipv4SecAssocAhKey->
            i4_Length = pSecAssocIf->u1SecAssocAhKeyLength;
    }
    else
    {
        pRetValFsipv4SecAssocAhKey->pu1_OctetList = 0;
        pRetValFsipv4SecAssocAhKey->i4_Length = 0;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocEspAlgo
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocEspAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocEspAlgo (INT4 i4Fsipv4SecAssocIndex,
                             INT4 *pi4RetValFsipv4SecAssocEspAlgo)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocEspAlgo = (INT4) pSecAssocIf->u1SecAssocEspAlgo;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocEspKey
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocEspKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocEspKey (INT4 i4Fsipv4SecAssocIndex, tSNMP_OCTET_STRING_TYPE
                            * pRetValFsipv4SecAssocEspKey)
{
    tSecv4Assoc        *pSecAssocIf = NULL;

    if (pRetValFsipv4SecAssocEspKey == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocEspKey != NULL)
    {
        if (pRetValFsipv4SecAssocEspKey->pu1_OctetList != NULL)
        {
            IPSEC_MEMCPY (pRetValFsipv4SecAssocEspKey->
                          pu1_OctetList, pSecAssocIf->pu1SecAssocEspKey,
                          pSecAssocIf->u1SecAssocEspKeyLength);

        }
        else
        {
            return (SNMP_FAILURE);
        }
        pRetValFsipv4SecAssocEspKey->
            i4_Length = pSecAssocIf->u1SecAssocEspKeyLength;
    }
    else
    {
        pRetValFsipv4SecAssocEspKey->pu1_OctetList = 0;
        pRetValFsipv4SecAssocEspKey->i4_Length = 0;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocEspKey2
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocEspKey2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocEspKey2 (INT4 i4Fsipv4SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv4SecAssocEspKey2)
{

    tSecv4Assoc        *pSecAssocIf = NULL;

    if (pRetValFsipv4SecAssocEspKey2 == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocEspKey2 != NULL)
    {
        pRetValFsipv4SecAssocEspKey2->
            i4_Length = pSecAssocIf->u1SecAssocEspKey2Length;
        if (pRetValFsipv4SecAssocEspKey2->pu1_OctetList != NULL)
        {
            IPSEC_MEMCPY (pRetValFsipv4SecAssocEspKey2->
                          pu1_OctetList, pSecAssocIf->pu1SecAssocEspKey2,
                          pSecAssocIf->u1SecAssocEspKey2Length);
        }
        else
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        pRetValFsipv4SecAssocEspKey2->pu1_OctetList = 0;
        pRetValFsipv4SecAssocEspKey2->i4_Length = 0;
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocEspKey3
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocEspKey3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocEspKey3 (INT4 i4Fsipv4SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsipv4SecAssocEspKey3)
{
    tSecv4Assoc        *pSecAssocIf = NULL;

    if (pRetValFsipv4SecAssocEspKey3 == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->pu1SecAssocEspKey3 != NULL)
    {
        if (pRetValFsipv4SecAssocEspKey3->pu1_OctetList != NULL)
        {
            IPSEC_MEMCPY (pRetValFsipv4SecAssocEspKey3->
                          pu1_OctetList, pSecAssocIf->pu1SecAssocEspKey3,
                          pSecAssocIf->u1SecAssocEspKey3Length);
        }
        else
        {
            return (SNMP_FAILURE);
        }
        pRetValFsipv4SecAssocEspKey3->
            i4_Length = pSecAssocIf->u1SecAssocEspKey3Length;
    }
    else
    {
        pRetValFsipv4SecAssocEspKey3->pu1_OctetList = 0;
        pRetValFsipv4SecAssocEspKey3->i4_Length = 0;
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocLifetimeInBytes
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object
                retValFsipv4SecAssocLifetimeInBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocLifetimeInBytes (INT4 i4Fsipv4SecAssocIndex,
                                     INT4
                                     *pi4RetValFsipv4SecAssocLifetimeInBytes)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocLifetimeInBytes
        = (INT4) pSecAssocIf->u4LifeTimeInBytes;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocLifetime
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object
                retValFsipv4SecAssocLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocLifetime (INT4 i4Fsipv4SecAssocIndex,
                              INT4 *pi4RetValFsipv4SecAssocLifetime)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocLifetime = (INT4) pSecAssocIf->u4LifeTime;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocAntiReplay
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocAntiReplay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocAntiReplay (INT4 i4Fsipv4SecAssocIndex,
                                INT4 *pi4RetValFsipv4SecAssocAntiReplay)
{

    tSecv4Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocAntiReplay = (INT4) pSecAssocIf->u1AntiReplayStatus;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAssocStatus
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                retValFsipv4SecAssocStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAssocStatus (INT4
                            i4Fsipv4SecAssocIndex,
                            INT4 *pi4RetValFsipv4SecAssocStatus)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsipv4SecAssocStatus = ACTIVE;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocDstAddr
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocDstAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocDstAddr (INT4
                             i4Fsipv4SecAssocIndex,
                             UINT4 u4SetValFsipv4SecAssocDstAddr)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    pSecAssocIf->u4SecAssocDestAddr = u4SetValFsipv4SecAssocDstAddr;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocProtocol
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocProtocol (INT4 i4Fsipv4SecAssocIndex,
                              INT4 i4SetValFsipv4SecAssocProtocol)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1SecAssocProtocol = (UINT1) i4SetValFsipv4SecAssocProtocol;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocSpi
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocSpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocSpi (INT4 i4Fsipv4SecAssocIndex,
                         INT4 i4SetValFsipv4SecAssocSpi)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u4SecAssocSpi = (UINT4) i4SetValFsipv4SecAssocSpi;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocMode
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocMode (INT4 i4Fsipv4SecAssocIndex,
                          INT4 i4SetValFsipv4SecAssocMode)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1SecAssocMode = (UINT1) i4SetValFsipv4SecAssocMode;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocAhAlgo
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocAhAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocAhAlgo (INT4
                            i4Fsipv4SecAssocIndex,
                            INT4 i4SetValFsipv4SecAssocAhAlgo)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    if (i4SetValFsipv4SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        pSecAssocIf->u1SecAhAlgoDigestSize = 0;
    }
    else if (i4SetValFsipv4SecAssocAhAlgo == HMAC_SHA_256)
    {
        pSecAssocIf->u1SecAhAlgoDigestSize = HMAC_SHA_256_AUTH_DIGEST_SIZE;
    }
    else if (i4SetValFsipv4SecAssocAhAlgo == HMAC_SHA_384)
    {
        pSecAssocIf->u1SecAhAlgoDigestSize = HMAC_SHA_384_AUTH_DIGEST_SIZE;
    }
    else if (i4SetValFsipv4SecAssocAhAlgo == HMAC_SHA_512)
    {
        pSecAssocIf->u1SecAhAlgoDigestSize = HMAC_SHA_512_AUTH_DIGEST_SIZE;
    }
    else
    {
        pSecAssocIf->u1SecAhAlgoDigestSize = SEC_DEF_AUTH_DIGEST_SIZE;
    }

    pSecAssocIf->u1SecAssocAhAlgo = (UINT1) i4SetValFsipv4SecAssocAhAlgo;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocAhKey
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocAhKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocAhKey (INT4
                           i4Fsipv4SecAssocIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsipv4SecAssocAhKey)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    UINT1               Count = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OutKey[(2 * SEC_ESP_AES_KEY3_LEN) + 1];

#ifdef SECv4_PRIVATE_KEY
    UINT1               Temp = SEC_KEY_CHANGE;
#endif

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (MemAllocateMemBlock
        (SECv4_SAD_AH_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocAhKey) != MEM_SUCCESS)
    {
        return (SNMP_FAILURE);
    }
    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocAhKey, 0,
                  ((2 * SEC_ESP_AES_KEY3_LEN) + 1));
    IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocAhKey,
                  pSetValFsipv4SecAssocAhKey->pu1_OctetList,
                  pSetValFsipv4SecAssocAhKey->i4_Length);
    pSecAssocIf->u1SecAssocAhKeyLength =
        (UINT1) pSetValFsipv4SecAssocAhKey->i4_Length;

    if (MemAllocateMemBlock
        (SECv4_SAD_AH_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocAhKey1) != MEM_SUCCESS)
    {
        MemReleaseMemBlock (SECv4_SAD_AH_MEMPOOL,
                            pSecAssocIf->pu1SecAssocAhKey);
        pSecAssocIf->pu1SecAssocAhKey = NULL;
        return (SNMP_FAILURE);
    }
    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocAhKey1, 0,
                  ((2 * SEC_ESP_AES_KEY3_LEN) + 1));
    IPSEC_MEMSET (au1OutKey, 0, 65);

    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        if (Secv4ConstructKey (pSetValFsipv4SecAssocAhKey->pu1_OctetList,
                               (UINT1) pSetValFsipv4SecAssocAhKey->i4_Length,
                               au1OutKey) == SEC_FAILURE)
        {
            MemReleaseMemBlock (SECv4_SAD_AH_MEMPOOL,
                                pSecAssocIf->pu1SecAssocAhKey);
            pSecAssocIf->pu1SecAssocAhKey = NULL;
            MemReleaseMemBlock (SECv4_SAD_AH_MEMPOOL,
                                pSecAssocIf->pu1SecAssocAhKey1);
            pSecAssocIf->pu1SecAssocAhKey1 = NULL;
            return (SNMP_FAILURE);
        }

        OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
        OctetStr.pu1_OctetList = au1OutKey;
        pSecAssocIf->u1SecAssocAhKeyLength = (UINT1) OctetStr.i4_Length;
    }
    else
    {
        OctetStr.i4_Length = pSetValFsipv4SecAssocAhKey->i4_Length;
        OctetStr.pu1_OctetList = pSetValFsipv4SecAssocAhKey->pu1_OctetList;
    }

    for (Count = 0; Count < OctetStr.i4_Length; Count++)
    {
#ifdef SECv4_PRIVATE_KEY
        pSecAssocIf->
            pu1SecAssocAhKey1[Count] = OctetStr.pu1_OctetList[Count] ^ Temp;
        Temp = pSecAssocIf->pu1SecAssocAhKey1[Count];
#else
        pSecAssocIf->pu1SecAssocAhKey1[Count] = OctetStr.pu1_OctetList[Count];
#endif
    }
    pSecAssocIf->pu1SecAssocAhKey1[Count] = '\0';

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocEspAlgo
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocEspAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocEspAlgo (INT4
                             i4Fsipv4SecAssocIndex,
                             INT4 i4SetValFsipv4SecAssocEspAlgo)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u1SecAssocEspAlgo = (UINT1) i4SetValFsipv4SecAssocEspAlgo;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocEspKey
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocEspKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocEspKey (INT4 i4Fsipv4SecAssocIndex,
                            tSNMP_OCTET_STRING_TYPE
                            * pSetValFsipv4SecAssocEspKey)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    unArCryptoKey       ArCryptoKey;

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));

    if (MemAllocateMemBlock
        (SECv4_SAD_ESP_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey) != MEM_SUCCESS)
    {
        return (SNMP_FAILURE);
    }
    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey, 0,
                  ((SEC_ESP_AES_KEY3_LEN + 1) * 2));

    /* Store the Plain-text key in pu1SecAssocEspKeyIn field */
    if (MemAllocateMemBlock
        (SECv4_SAD_ESP_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocEspKeyIn) != MEM_SUCCESS)
    {
        MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                            pSecAssocIf->pu1SecAssocEspKey);
        pSecAssocIf->pu1SecAssocEspKey = NULL;
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKeyIn, 0,
                  ((SEC_ESP_AES_KEY3_LEN + 1) * 2));
    IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocEspKeyIn,
                  pSetValFsipv4SecAssocEspKey->pu1_OctetList,
                  pSetValFsipv4SecAssocEspKey->i4_Length);

    pSecAssocIf->u1SecAssocEspKeyInLength = (UINT1)
        pSetValFsipv4SecAssocEspKey->i4_Length;

    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        if (Secv4ConstructKey (pSetValFsipv4SecAssocEspKey->pu1_OctetList,
                               (UINT1) pSetValFsipv4SecAssocEspKey->i4_Length,
                               pSecAssocIf->pu1SecAssocEspKey) == SEC_FAILURE)
        {
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKey);
            pSecAssocIf->pu1SecAssocEspKey = NULL;
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKeyIn);
            pSecAssocIf->pu1SecAssocEspKeyIn = NULL;
            return (SNMP_FAILURE);
        }

        pSecAssocIf->u1SecAssocEspKeyLength = (UINT1)
            STRLEN (pSecAssocIf->pu1SecAssocEspKey);
    }
    else
    {
        MEMCPY (pSecAssocIf->pu1SecAssocEspKey,
                pSetValFsipv4SecAssocEspKey->pu1_OctetList,
                pSetValFsipv4SecAssocEspKey->i4_Length);
        pSecAssocIf->u1SecAssocEspKeyLength = (UINT1)
            pSetValFsipv4SecAssocEspKey->i4_Length;
    }

    if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_DES_CBC) ||
        (pSecAssocIf->u1SecAssocEspAlgo == SEC_3DES_CBC))
    {
        if (MemAllocateMemBlock (SECv4_SAD_ESP_MEMPOOL, (UINT1 **)
                                 &pSecAssocIf->pu1SecAssocInitVector) !=
            MEM_SUCCESS)
        {
            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey, 0,
                          SEC_ESP_KEY_LENGTH);
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKey);
            pSecAssocIf->pu1SecAssocEspKey = NULL;
            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKeyIn, 0,
                          SEC_ESP_KEY_LENGTH);
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKeyIn);
            pSecAssocIf->pu1SecAssocEspKeyIn = NULL;
            return (SNMP_FAILURE);
        }
        IPSEC_STRCPY (pSecAssocIf->pu1SecAssocInitVector, "INITVECT");
        pSecAssocIf->u1SecAssocIvSize = DES_IV_SIZE;
        pSecAssocIf->u1SecEspMultFact = SEC_ESP_MULTIPLY_FACTOR;

        DesArKeyScheduler (pSecAssocIf->pu1SecAssocEspKey, &ArCryptoKey);
        IPSEC_MEMCPY (pSecAssocIf->Key, ArCryptoKey.tArDes.au8ArSubkey,
                      sizeof (ArCryptoKey.tArDes.au8ArSubkey));
    }
    else if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_AES) ||
             (pSecAssocIf->u1SecAssocEspAlgo == SEC_AES192) ||
             (pSecAssocIf->u1SecAssocEspAlgo == SEC_AES256) ||
             (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR) ||
             (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR192) ||
             (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR256))
    {
        if (MemAllocateMemBlock (SECv4_SAD_ESP_MEMPOOL, (UINT1 **)
                                 &pSecAssocIf->pu1SecAssocInitVector) !=
            MEM_SUCCESS)
        {
            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey, 0,
                          SEC_ESP_KEY_LENGTH);
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKey);
            pSecAssocIf->pu1SecAssocEspKey = NULL;
            /* FIPS compliance - Cryptographic keys zeroed at the
             * end of their lifecycle */
            IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKeyIn, 0,
                          SEC_ESP_KEY_LENGTH);
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKeyIn);
            pSecAssocIf->pu1SecAssocEspKeyIn = NULL;
            return (SNMP_FAILURE);
        }
        if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR) ||
            (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR192) ||
            (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR256))
        {
            SecUtlGetRandom (pSecAssocIf->pu1SecAssocInitVector,
                             IPSEC_AES_CTR_INIT_VECT_SIZE);
            pSecAssocIf->u1SecAssocIvSize = IPSEC_AES_CTR_INIT_VECT_SIZE;
            pSecAssocIf->u1SecEspMultFact = (2 * SEC_ESP_MULTIPLY_FACTOR);
        }
        else
        {

            IPSEC_STRCPY (pSecAssocIf->pu1SecAssocInitVector,
                          "AESCBCINITVECTOR");
            pSecAssocIf->u1SecAssocIvSize = AES_INIT_VECT_SIZE;
            pSecAssocIf->u1SecEspMultFact = (2 * SEC_ESP_MULTIPLY_FACTOR);

        }

        /* Do Key Schedule for Encryption and Decryption */

        AesArSetEncryptKey (pSecAssocIf->pu1SecAssocEspKey,
                            (UINT2) (pSecAssocIf->u1SecAssocEspKeyLength * 8),
                            &ArCryptoKey);
        IPSEC_MEMCPY (pSecAssocIf->au1AesEncrKey,
                      ArCryptoKey.tArAes.au1ArSubkey,
                      sizeof (ArCryptoKey.tArAes.au1ArSubkey));

        AesArSetDecryptKey (pSecAssocIf->pu1SecAssocEspKey,
                            (UINT2) (pSecAssocIf->u1SecAssocEspKeyLength * 8),
                            &ArCryptoKey);
        IPSEC_MEMCPY (pSecAssocIf->au1AesDecrKey,
                      ArCryptoKey.tArAes.au1ArSubkey,
                      sizeof (ArCryptoKey.tArAes.au1ArSubkey));
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocEspKey2
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocEspKey2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocEspKey2 (INT4 i4Fsipv4SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv4SecAssocEspKey2)
{
    unArCryptoKey       ArCryptoKey;
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));

    if (MemAllocateMemBlock
        (SECv4_SAD_ESP_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey2) != MEM_SUCCESS)
    {
        return (SNMP_FAILURE);
    }
    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey2, 0,
                  ((SEC_ESP_AES_KEY3_LEN + 1) * 2));

    if (MemAllocateMemBlock
        (SECv4_SAD_ESP_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey2In) != MEM_SUCCESS)
    {
        MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                            pSecAssocIf->pu1SecAssocEspKey2);
        pSecAssocIf->pu1SecAssocEspKey2 = NULL;
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey2In, 0,
                  ((SEC_ESP_AES_KEY3_LEN + 1) * 2));

    IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocEspKey2In,
                  pSetValFsipv4SecAssocEspKey2->pu1_OctetList,
                  pSetValFsipv4SecAssocEspKey2->i4_Length);

    pSecAssocIf->u1SecAssocEspKey2InLength =
        (UINT1) pSetValFsipv4SecAssocEspKey2->i4_Length;

    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        if (Secv4ConstructKey (pSetValFsipv4SecAssocEspKey2->pu1_OctetList,
                               (UINT1) pSetValFsipv4SecAssocEspKey2->i4_Length,
                               pSecAssocIf->pu1SecAssocEspKey2) == SEC_FAILURE)
        {
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKey2);
            pSecAssocIf->pu1SecAssocEspKey2 = NULL;
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKey2In);
            pSecAssocIf->pu1SecAssocEspKey2In = NULL;
            return (SNMP_FAILURE);
        }

        pSecAssocIf->u1SecAssocEspKey2Length = (UINT1)
            STRLEN (pSecAssocIf->pu1SecAssocEspKey2);
    }
    else
    {
        MEMCPY (pSecAssocIf->pu1SecAssocEspKey2,
                pSetValFsipv4SecAssocEspKey2->pu1_OctetList,
                pSetValFsipv4SecAssocEspKey2->i4_Length);
        pSecAssocIf->u1SecAssocEspKey2Length = (UINT1)
            pSetValFsipv4SecAssocEspKey2->i4_Length;
    }

    DesArKeyScheduler (pSecAssocIf->pu1SecAssocEspKey2, &ArCryptoKey);
    IPSEC_MEMCPY (pSecAssocIf->Key2, ArCryptoKey.tArDes.au8ArSubkey,
                  sizeof (ArCryptoKey.tArDes.au8ArSubkey));
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocEspKey3
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocEspKey3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocEspKey3 (INT4 i4Fsipv4SecAssocIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsipv4SecAssocEspKey3)
{
    unArCryptoKey       ArCryptoKey;
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
    if (MemAllocateMemBlock
        (SECv4_SAD_ESP_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey3) != MEM_SUCCESS)
    {
        return (SNMP_FAILURE);
    }
    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey3, 0,
                  (SEC_ESP_AES_KEY3_LEN + 1));

    if (MemAllocateMemBlock
        (SECv4_SAD_ESP_MEMPOOL,
         (UINT1 **) &pSecAssocIf->pu1SecAssocEspKey3In) != MEM_SUCCESS)
    {
        MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                            pSecAssocIf->pu1SecAssocEspKey3);
        pSecAssocIf->pu1SecAssocEspKey3 = NULL;
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey3In, 0,
                  ((SEC_ESP_AES_KEY3_LEN + 1) * 2));

    IPSEC_MEMCPY (pSecAssocIf->pu1SecAssocEspKey3In,
                  pSetValFsipv4SecAssocEspKey3->pu1_OctetList,
                  pSetValFsipv4SecAssocEspKey3->i4_Length);

    pSecAssocIf->u1SecAssocEspKey3InLength = (UINT1)
        pSetValFsipv4SecAssocEspKey3->i4_Length;

    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        if (Secv4ConstructKey (pSetValFsipv4SecAssocEspKey3->pu1_OctetList,
                               (UINT1) pSetValFsipv4SecAssocEspKey3->i4_Length,
                               pSecAssocIf->pu1SecAssocEspKey3) == SEC_FAILURE)
        {
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKey3);
            pSecAssocIf->pu1SecAssocEspKey3 = NULL;
            MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                pSecAssocIf->pu1SecAssocEspKey3In);
            pSecAssocIf->pu1SecAssocEspKey3In = NULL;
            return (SNMP_FAILURE);
        }

        pSecAssocIf->u1SecAssocEspKey3Length = (UINT1)
            STRLEN (pSecAssocIf->pu1SecAssocEspKey3);
    }
    else
    {
        MEMCPY (pSecAssocIf->pu1SecAssocEspKey3,
                pSetValFsipv4SecAssocEspKey3->pu1_OctetList,
                pSetValFsipv4SecAssocEspKey3->i4_Length);
        pSecAssocIf->u1SecAssocEspKey3Length = (UINT1)
            pSetValFsipv4SecAssocEspKey3->i4_Length;
    }
    DesArKeyScheduler (pSecAssocIf->pu1SecAssocEspKey3, &ArCryptoKey);
    IPSEC_MEMCPY (pSecAssocIf->Key3, ArCryptoKey.tArDes.au8ArSubkey,
                  sizeof (ArCryptoKey.tArDes.au8ArSubkey));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocLifetimeInBytes
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object
                setValFsipv4SecAssocLifetimeInBytes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocLifetimeInBytes (INT4 i4Fsipv4SecAssocIndex,
                                     INT4 i4SetValFsipv4SecAssocLifetimeInBytes)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->
        u4LifeTimeInBytes = (UINT4) i4SetValFsipv4SecAssocLifetimeInBytes;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocLifetime
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object
                setValFsipv4SecAssocLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocLifetime (INT4 i4Fsipv4SecAssocIndex,
                              INT4 i4SetValFsipv4SecAssocLifetime)
{
    tSecv4Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u4LifeTime = (UINT4) i4SetValFsipv4SecAssocLifetime;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocAntiReplay
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocAntiReplay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocAntiReplay (INT4 i4Fsipv4SecAssocIndex,
                                INT4 i4SetValFsipv4SecAssocAntiReplay)
{

    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        return (SNMP_FAILURE);
    }
    pSecAssocIf->u4PolicyIndex = (UINT4) i4Fsipv4SecAssocIndex;
    pSecAssocIf->u1AntiReplayStatus = (UINT1) i4SetValFsipv4SecAssocAntiReplay;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsipv4SecAssocStatus
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                setValFsipv4SecAssocStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecAssocStatus (INT4 i4Fsipv4SecAssocIndex,
                            INT4 i4SetValFsipv4SecAssocStatus)
{
    tSecv4Assoc        *pSecAssocIf = NULL;

    switch (i4SetValFsipv4SecAssocStatus)
    {

        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (MemAllocateMemBlock
                (SECv4_SAD_MEMPOOL,
                 (UINT1 **) (VOID *) &pSecAssocIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            IPSEC_MEMSET (pSecAssocIf, 0, sizeof (tSecv4Assoc));
            pSecAssocIf->u1SecAssocSeqCounterFlag = 32;
            pSecAssocIf->u4SecAssocIndex = (UINT4) i4Fsipv4SecAssocIndex;
            SECv4_INIT_TIMER (&(pSecAssocIf->Secv4AssocHardTimer));
            SECv4_INIT_TIMER (&(pSecAssocIf->Secv4AssocSoftTimer));
            TMO_SLL_Add (&Secv4AssocList, (tTMO_SLL_NODE *) pSecAssocIf);
            break;

        case ACTIVE:
            pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
            if (pSecAssocIf == NULL)
            {
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:

            pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
            if (pSecAssocIf == NULL)
            {
                return SNMP_FAILURE;
            }

            TMO_SLL_Delete (&Secv4AssocList, (tTMO_SLL_NODE *) pSecAssocIf);

            /* Remove the Session in the Hardware Accelerator */
            Secv4RemSesInHwAccel (pSecAssocIf->pSessionCtx);

            Secv4StopTimer (&(pSecAssocIf->Secv4AssocHardTimer));
            Secv4StopTimer (&(pSecAssocIf->Secv4AssocSoftTimer));
            if (pSecAssocIf->u1SecAssocProtocol == SEC_ESP)
            {
                if (pSecAssocIf->pu1SecAssocEspKey != NULL)
                {
                    /* FIPS compliance - Cryptographic keys zeroed at the
                     * end of their lifecycle */
                    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey, 0,
                                  SEC_ESP_KEY_LENGTH);
                    MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                        (UINT1 *) pSecAssocIf->
                                        pu1SecAssocEspKey);
                    pSecAssocIf->pu1SecAssocEspKey = NULL;
                    /* FIPS compliance - Cryptographic keys zeroed at the
                     * end of their lifecycle */
                    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKeyIn, 0,
                                  SEC_ESP_KEY_LENGTH);
                    MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                        (UINT1 *) pSecAssocIf->
                                        pu1SecAssocEspKeyIn);
                    pSecAssocIf->pu1SecAssocEspKeyIn = NULL;

                    /* For AES Key Scheduling Memory Used is Static Array.
                       Hence it should not be freed to MemPool of Des and
                       3des-cbc MemPool. Aes Requires different size of
                       Memory for Key Scheduling */
                    if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_DES_CBC) ||
                        (pSecAssocIf->u1SecAssocEspAlgo == SEC_3DES_CBC) ||
                        (pSecAssocIf->u1SecAssocEspAlgo == SEC_AES) ||
                        (pSecAssocIf->u1SecAssocEspAlgo == SEC_AES192) ||
                        (pSecAssocIf->u1SecAssocEspAlgo == SEC_AES256) ||
                        (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR) ||
                        (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR192) ||
                        (pSecAssocIf->u1SecAssocEspAlgo == SEC_AESCTR256))
                    {

                        IPSEC_MEMSET (pSecAssocIf->pu1SecAssocInitVector, 0,
                                      SEC_ESP_KEY_LENGTH);
                        MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                            (UINT1 *) pSecAssocIf->
                                            pu1SecAssocInitVector);
                        pSecAssocIf->pu1SecAssocInitVector = NULL;
                    }
                }
                if (pSecAssocIf->pu1SecAssocEspKey2 != NULL)
                {
                    /* FIPS compliance - Cryptographic keys zeroed at the
                     * end of their lifecycle */
                    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey2, 0,
                                  SEC_ESP_KEY_LENGTH);
                    MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                        (UINT1 *) pSecAssocIf->
                                        pu1SecAssocEspKey2);
                    pSecAssocIf->pu1SecAssocEspKey2 = NULL;
                    /* FIPS compliance - Cryptographic keys zeroed at the
                     * end of their lifecycle */
                    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey2In, 0,
                                  SEC_ESP_KEY_LENGTH);
                    MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                        (UINT1 *) pSecAssocIf->
                                        pu1SecAssocEspKey2In);
                    pSecAssocIf->pu1SecAssocEspKey2In = NULL;
                }
                if (pSecAssocIf->pu1SecAssocEspKey3 != NULL)
                {
                    /* FIPS compliance - Cryptographic keys zeroed at the
                     * end of their lifecycle */
                    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey3, 0,
                                  SEC_ESP_KEY_LENGTH);
                    MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                        (UINT1 *) pSecAssocIf->
                                        pu1SecAssocEspKey3);
                    pSecAssocIf->pu1SecAssocEspKey3 = NULL;
                    /* FIPS compliance - Cryptographic keys zeroed at the
                     * end of their lifecycle */
                    IPSEC_MEMSET (pSecAssocIf->pu1SecAssocEspKey3In, 0,
                                  SEC_ESP_KEY_LENGTH);
                    MemReleaseMemBlock (SECv4_SAD_ESP_MEMPOOL,
                                        (UINT1 *) pSecAssocIf->
                                        pu1SecAssocEspKey3In);
                    pSecAssocIf->pu1SecAssocEspKey3In = NULL;
                }
            }

            if (pSecAssocIf->pu1SecAssocAhKey != NULL)
            {
                /* FIPS compliance - Cryptographic keys zeroed at the
                 * end of their lifecycle */
                IPSEC_MEMSET (pSecAssocIf->pu1SecAssocAhKey1, 0,
                              SEC_AH_KEY_LENGTH);
                IPSEC_MEMSET (pSecAssocIf->pu1SecAssocAhKey, 0,
                              SEC_AH_KEY_LENGTH);
                MemReleaseMemBlock (SECv4_SAD_AH_MEMPOOL, (UINT1 *)
                                    pSecAssocIf->pu1SecAssocAhKey1);
                MemReleaseMemBlock (SECv4_SAD_AH_MEMPOOL,
                                    (UINT1 *) pSecAssocIf->pu1SecAssocAhKey);
            }

            Secv4DeleteSAPtrInPolicy (pSecAssocIf);
            IPSEC_MEMSET (pSecAssocIf, 0, sizeof (tSecv4Assoc));
            if (MemReleaseMemBlock (SECv4_SAD_MEMPOOL, (UINT1 *)
                                    pSecAssocIf) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
            pSecAssocIf = NULL;

            break;
        default:
            break;

    }

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocDstAddr
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocDstAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocDstAddr (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv4SecAssocIndex,
                                UINT4 u4TestValFsipv4SecAssocDstAddr)
{

    tSecv4Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (!((IPSEC_IS_ADDR_CLASS_A
           (u4TestValFsipv4SecAssocDstAddr))
          || (IPSEC_IS_ADDR_CLASS_B
              (u4TestValFsipv4SecAssocDstAddr))
          || (IPSEC_IS_ADDR_CLASS_C (u4TestValFsipv4SecAssocDstAddr))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocProtocol
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocProtocol (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv4SecAssocIndex,
                                 INT4 i4TestValFsipv4SecAssocProtocol)
{

    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecAssocProtocol != SEC_AH)
        && (i4TestValFsipv4SecAssocProtocol != SEC_ESP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocSpi
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocSpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocSpi (UINT4 *pu4ErrorCode, INT4 i4Fsipv4SecAssocIndex,
                            INT4 i4TestValFsipv4SecAssocSpi)
{

    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((UINT4) i4TestValFsipv4SecAssocSpi <= SEC_MIN_SPI)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocMode
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocMode (UINT4 *pu4ErrorCode,
                             INT4 i4Fsipv4SecAssocIndex,
                             INT4 i4TestValFsipv4SecAssocMode)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecAssocMode != SEC_TRANSPORT)
        && (i4TestValFsipv4SecAssocMode != SEC_TUNNEL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocAhAlgo
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocAhAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocAhAlgo (UINT4 *pu4ErrorCode,
                               INT4 i4Fsipv4SecAssocIndex,
                               INT4 i4TestValFsipv4SecAssocAhAlgo)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecAssocAhAlgo != SEC_MD5)
        && (i4TestValFsipv4SecAssocAhAlgo != SEC_KEYEDMD5)
        && (i4TestValFsipv4SecAssocAhAlgo != SEC_HMACMD5)
        && (i4TestValFsipv4SecAssocAhAlgo != SEC_HMACSHA1)
        && (i4TestValFsipv4SecAssocAhAlgo != HMAC_SHA_256)
        && (i4TestValFsipv4SecAssocAhAlgo != HMAC_SHA_384)
        && (i4TestValFsipv4SecAssocAhAlgo != HMAC_SHA_512)
        && (i4TestValFsipv4SecAssocAhAlgo != SEC_XCBCMAC)
        && (i4TestValFsipv4SecAssocAhAlgo != SEC_NULLAHALGO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocAhKey
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocAhKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocAhKey (UINT4 *pu4ErrorCode, INT4 i4Fsipv4SecAssocIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsipv4SecAssocAhKey)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    UINT4               u4Len;
    UINT1               au1OutKey[(2 * SEC_ESP_AES_KEY3_LEN) + 1];

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (au1OutKey, 0, ((2 * SEC_ESP_AES_KEY3_LEN) + 1));
    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        if (Secv4ConstructKey (pTestValFsipv4SecAssocAhKey->pu1_OctetList,
                               (UINT1) pTestValFsipv4SecAssocAhKey->i4_Length,
                               au1OutKey) == SEC_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return (SNMP_FAILURE);
        }
        u4Len = STRLEN (au1OutKey);
    }
    else
    {
        u4Len = (UINT4) pTestValFsipv4SecAssocAhKey->i4_Length;
    }

    switch (pSecAssocIf->u1SecAssocAhAlgo)
    {
        case SEC_HMACMD5:
        case SEC_KEYEDMD5:
            if (u4Len != SEC_HMACMD5_KEY_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case SEC_HMACSHA1:
            if (u4Len != SEC_HMACSHA1_KEY_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case SEC_XCBCMAC:
            if (u4Len != SEC_ESP_AES_KEY1_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_256:
            if (u4Len != (2 * SEC_ESP_AES_KEY1_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_384:
            if (u4Len != (3 * SEC_ESP_AES_KEY1_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_512:
            if (u4Len != (4 * SEC_ESP_AES_KEY1_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocEspAlgo
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocEspAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2Fsipv4SecAssocEspAlgo (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv4SecAssocIndex,
                                INT4 i4TestValFsipv4SecAssocEspAlgo)
{
    tSecv4Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecAssocEspAlgo != SEC_DES_CBC)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_NULLESPALGO)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_3DES_CBC)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_AES)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_AES192)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_AES256)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_AESCTR)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_AESCTR192)
        && (i4TestValFsipv4SecAssocEspAlgo != SEC_AESCTR256))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecAssocEspAlgo == SEC_NULLESPALGO) &&
        (pSecAssocIf->u1SecAssocAhAlgo == SEC_NULLAHALGO))
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocEspKey
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocEspKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocEspKey (UINT4 *pu4ErrorCode,
                               INT4 i4Fsipv4SecAssocIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsipv4SecAssocEspKey)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OutKey[SEC_ESP_AES_KEY3_LEN + 1];
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocEspAlgo == SEC_NULLESPALGO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    IPSEC_MEMSET (au1OutKey, 0, (SEC_ESP_AES_KEY3_LEN + 1));

    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        if (Secv4ConstructKey (pTestValFsipv4SecAssocEspKey->pu1_OctetList,
                               (UINT1) pTestValFsipv4SecAssocEspKey->i4_Length,
                               au1OutKey) == SEC_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
        OctetStr.pu1_OctetList = au1OutKey;
    }
    else
    {
        OctetStr.i4_Length = pTestValFsipv4SecAssocEspKey->i4_Length;
    }
    switch (pSecAssocIf->u1SecAssocEspAlgo)
    {
        case SEC_DES_CBC:
        case SEC_3DES_CBC:
            if (OctetStr.i4_Length == SEC_ESP_DES_KEY_LEN)
            {
                break;
            }
        case SEC_AES:
            if (OctetStr.i4_Length == SEC_ESP_AES_KEY1_LEN)
            {
                break;
            }
        case SEC_AES192:
            if (OctetStr.i4_Length == SEC_ESP_AES_KEY2_LEN)
            {
                break;
            }
        case SEC_AES256:
            if (OctetStr.i4_Length == SEC_ESP_AES_KEY3_LEN)
            {
                break;
            }
        case SEC_AESCTR:
            if (OctetStr.i4_Length == SEC_ESP_AES_KEY1_LEN)
            {
                break;
            }
        case SEC_AESCTR192:
            if (OctetStr.i4_Length == SEC_ESP_AES_KEY2_LEN)
            {
                break;
            }
        case SEC_AESCTR256:
            if (OctetStr.i4_Length == SEC_ESP_AES_KEY3_LEN)
            {
                break;
            }

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocEspKey2
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocEspKey2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocEspKey2 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv4SecAssocIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv4SecAssocEspKey2)
{

    tSecv4Assoc        *pSecAssocIf = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OutKey[SEC_ESP_AES_KEY3_LEN + 1];
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_NULLESPALGO) ||
        (pSecAssocIf->u1SecAssocEspAlgo != SEC_3DES_CBC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        IPSEC_MEMSET (au1OutKey, 0, (SEC_ESP_AES_KEY3_LEN + 1));
        if (Secv4ConstructKey (pTestValFsipv4SecAssocEspKey2->pu1_OctetList,
                               (UINT1) pTestValFsipv4SecAssocEspKey2->i4_Length,
                               au1OutKey) == SEC_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
        OctetStr.pu1_OctetList = au1OutKey;
    }
    else
    {
        OctetStr.i4_Length = pTestValFsipv4SecAssocEspKey2->i4_Length;
        OctetStr.pu1_OctetList = pTestValFsipv4SecAssocEspKey2->pu1_OctetList;
    }

    if (OctetStr.i4_Length != SEC_ESP_DES_KEY_LEN)
    {
        pSecAssocIf->u1SecAssocEspAlgo = SEC_NULLESPALGO;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocEspKey3
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocEspKey3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocEspKey3 (UINT4 *pu4ErrorCode,
                                INT4 i4Fsipv4SecAssocIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsipv4SecAssocEspKey3)
{
    tSecv4Assoc        *pSecAssocIf = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OutKey[SEC_ESP_AES_KEY3_LEN + 1];
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1SecAssocProtocol == SEC_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((pSecAssocIf->u1SecAssocEspAlgo == SEC_NULLESPALGO) ||
        (pSecAssocIf->u1SecAssocEspAlgo != SEC_3DES_CBC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (pSecAssocIf->u1KeyingMode != SEC_AUTOMATIC)
    {
        IPSEC_MEMSET (au1OutKey, 0, (SEC_ESP_AES_KEY3_LEN + 1));
        if (Secv4ConstructKey (pTestValFsipv4SecAssocEspKey3->pu1_OctetList,
                               (UINT1) pTestValFsipv4SecAssocEspKey3->i4_Length,
                               au1OutKey) == SEC_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        OctetStr.i4_Length = (INT4) STRLEN (au1OutKey);
        OctetStr.pu1_OctetList = au1OutKey;
    }
    else
    {
        OctetStr.i4_Length = pTestValFsipv4SecAssocEspKey3->i4_Length;
        OctetStr.pu1_OctetList = pTestValFsipv4SecAssocEspKey3->pu1_OctetList;
    }

    if (OctetStr.i4_Length != SEC_ESP_DES_KEY_LEN)
    {

        pSecAssocIf->u1SecAssocEspAlgo = SEC_NULLESPALGO;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocLifetimeInBytes
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object
                testValFsipv4SecAssocLifetimeInBytes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocLifetimeInBytes (UINT4 *pu4ErrorCode,
                                        INT4 i4Fsipv4SecAssocIndex,
                                        INT4
                                        i4TestValFsipv4SecAssocLifetimeInBytes)
{

    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (i4TestValFsipv4SecAssocLifetimeInBytes < SEC_MIN_LIFE_BYTES_VALUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocLifetime
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object
                testValFsipv4SecAssocLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocLifetime (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsipv4SecAssocIndex,
                                 INT4 i4TestValFsipv4SecAssocLifetime)
{

    tSecv4Assoc        *pSecAssocIf = NULL;

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);

    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecAssocLifetime <
         SEC_MIN_LIFE_TIME_VALUE)
        || (i4TestValFsipv4SecAssocLifetime > SEC_MAX_LIFE_TIME_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocAntiReplay
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocAntiReplay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocAntiReplay (UINT4 *pu4ErrorCode,
                                   INT4 i4Fsipv4SecAssocIndex,
                                   INT4 i4TestValFsipv4SecAssocAntiReplay)
{

    tSecv4Assoc        *pSecAssocIf = NULL;
    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsipv4SecAssocAntiReplay != SEC_ANTI_REPLAY_ENABLE)
        && (i4TestValFsipv4SecAssocAntiReplay != SEC_ANTI_REPLAY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecAssocStatus
 Input       :  The Indices
                Fsipv4SecAssocIndex

                The Object 
                testValFsipv4SecAssocStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecAssocStatus (UINT4 *pu4ErrorCode,
                               INT4 i4Fsipv4SecAssocIndex,
                               INT4 i4TestValFsipv4SecAssocStatus)
{

    tSecv4Assoc        *pSecAssocIf = NULL;

    if (i4Fsipv4SecAssocIndex < SEC_MIN_INTEGER)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSecAssocIf = Secv4AssocGetEntry ((UINT4) i4Fsipv4SecAssocIndex);
    if ((i4TestValFsipv4SecAssocStatus != CREATE_AND_GO)
        && (i4TestValFsipv4SecAssocStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsipv4SecAssocStatus == CREATE_AND_GO))
        /*    || (i4TestValFsipv4SecAssocStatus == CREATE_AND_WAIT)) */
    {
        if (pSecAssocIf != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }

    if (i4TestValFsipv4SecAssocStatus == DESTROY)
    {
        if (pSecAssocIf == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsipv4SecDFragBitTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecDFragBitTable
 Input       :  The Indices
                Fsipv4SecDFragBitIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsipv4SecDFragBitTable (INT4 i4Fsipv4SecDFragBitIndex)
{
    if ((i4Fsipv4SecDFragBitIndex > SEC_MAX_STAT_COUNT) ||
        (i4Fsipv4SecDFragBitIndex < SEC_MIN_STAT_COUNT))
    {

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecDFragBitTable
 Input       :  The Indices
                Fsipv4SecDFragBitIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv4SecDFragBitTable (INT4 *pi4Fsipv4SecDFragBitIndex)
{

    tSecv4DFrag        *pSecDFragIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER, u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecDFragIf = &gatSecv4DFrag[u4Count];
        if ((pSecDFragIf->u4IfIndex < (UINT4) u4FirstIndex) &&
            (pSecDFragIf->u4IfIndex != 0))
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecDFragIf->u4IfIndex;
        }
    }
    if ((Flag == SEC_NOT_FOUND) || (u4FirstIndex < SEC_MIN_STAT_COUNT))
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv4SecDFragBitIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecDFragBitTable
 Input       :  The Indices
                Fsipv4SecDFragBitIndex
                nextFsipv4SecDFragBitIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecDFragBitTable (INT4 i4Fsipv4SecDFragBitIndex,
                                       INT4 *pi4NextFsipv4SecDFragBitIndex)
{
    tSecv4DFrag        *pSecDFragIf = NULL;
    INT4                Flag = SEC_NOT_FOUND;
    UINT4               u4Count = 0;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {

        pSecDFragIf = &gatSecv4DFrag[u4Count];
        if (pSecDFragIf->u4IfIndex > (UINT4) i4Fsipv4SecDFragBitIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv4SecDFragBitIndex = (INT4) pSecDFragIf->u4IfIndex;
                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecDFragIf->u4IfIndex < (UINT4)
                    *pi4NextFsipv4SecDFragBitIndex)
                {
                    *pi4NextFsipv4SecDFragBitIndex =
                        (INT4) pSecDFragIf->u4IfIndex;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SecDFragBitStatus
 Input       :  The Indices
                Fsipv4SecDFragBitIndex

                The Object 
                retValFsipv4SecDFragBitStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecDFragBitStatus (INT4 i4Fsipv4SecDFragBitIndex,
                               INT4 *pi4RetValFsipv4SecDFragBitStatus)
{

    *pi4RetValFsipv4SecDFragBitStatus =
        (INT4) gatSecv4DFrag[i4Fsipv4SecDFragBitIndex].u1DFragBitStatus;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsipv4SecDFragBitStatus
 Input       :  The Indices
                Fsipv4SecDFragBitIndex

                The Object 
                setValFsipv4SecDFragBitStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsipv4SecDFragBitStatus (INT4 i4Fsipv4SecDFragBitIndex,
                               INT4 i4SetValFsipv4SecDFragBitStatus)
{
    if ((i4Fsipv4SecDFragBitIndex >= SEC_MAX_STAT_COUNT) ||
        (i4Fsipv4SecDFragBitIndex < SEC_MIN_STAT_COUNT))
    {
        return (SNMP_FAILURE);
    }

    gatSecv4DFrag[i4Fsipv4SecDFragBitIndex].u1DFragBitStatus =
        (UINT1) i4SetValFsipv4SecDFragBitStatus;

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsipv4SecDFragBitStatus
 Input       :  The Indices
                Fsipv4SecDFragBitIndex

                The Object 
                testValFsipv4SecDFragBitStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsipv4SecDFragBitStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsipv4SecDFragBitIndex,
                                  INT4 i4TestValFsipv4SecDFragBitStatus)
{
    UNUSED_PARAM (i4Fsipv4SecDFragBitIndex);

    if ((i4TestValFsipv4SecDFragBitStatus != SEC_DFBIT_COPY) &&
        (i4TestValFsipv4SecDFragBitStatus != SEC_DFBIT_CLEAR) &&
        (i4TestValFsipv4SecDFragBitStatus != SEC_DFBIT_SET))
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Fsipv4SecIfStatsTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecIfStatsTable
 Input       :  The Indices
                Fsipv4SecIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv4SecIfStatsTable (INT4 i4Fsipv4SecIfIndex)
{

    if ((i4Fsipv4SecIfIndex > SEC_MAX_STAT_COUNT) ||
        (i4Fsipv4SecIfIndex < SEC_MIN_STAT_COUNT))
    {

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecIfStatsTable
 Input       :  The Indices
                Fsipv4SecIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv4SecIfStatsTable (INT4 *pi4Fsipv4SecIfIndex)
{

    tSecv4Stat         *pSecStatsIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER, u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecStatsIf = &gatIpsecv4Stat[u4Count];
        if ((pSecStatsIf->
             u4Index < (UINT4) u4FirstIndex) && (pSecStatsIf->u4Index != 0))
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecStatsIf->u4Index;
        }
    }
    if ((Flag == SEC_NOT_FOUND) || (u4FirstIndex < SEC_MIN_STAT_COUNT))
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv4SecIfIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecIfStatsTable
 Input       :  The Indices
                Fsipv4SecIfIndex
                nextFsipv4SecIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecIfStatsTable (INT4 i4Fsipv4SecIfIndex,
                                      INT4 *pi4NextFsipv4SecIfIndex)
{

    tSecv4Stat         *pSecStatsIf = NULL;
    UINT4               u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {

        pSecStatsIf = &gatIpsecv4Stat[u4Count];
        if (pSecStatsIf->u4Index > (UINT4) i4Fsipv4SecIfIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv4SecIfIndex = (INT4) pSecStatsIf->u4Index;
                Flag = SEC_FOUND;
            }
            else
            {

                if (pSecStatsIf->u4Index < (UINT4) *pi4NextFsipv4SecIfIndex)
                {
                    *pi4NextFsipv4SecIfIndex = (INT4) pSecStatsIf->u4Index;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfInPkts
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfInPkts (INT4 i4Fsipv4SecIfIndex,
                         UINT4 *pu4RetValFsipv4SecIfInPkts)
{
    *pu4RetValFsipv4SecIfInPkts = gatIpsecv4Stat[i4Fsipv4SecIfIndex].u4IfInPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfOutPkts
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfOutPkts (INT4 i4Fsipv4SecIfIndex,
                          UINT4 *pu4RetValFsipv4SecIfOutPkts)
{

    *pu4RetValFsipv4SecIfOutPkts
        = gatIpsecv4Stat[i4Fsipv4SecIfIndex].u4IfOutPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfPktsApply
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfPktsApply
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfPktsApply (INT4 i4Fsipv4SecIfIndex,
                            UINT4 *pu4RetValFsipv4SecIfPktsApply)
{

    *pu4RetValFsipv4SecIfPktsApply
        = gatIpsecv4Stat[i4Fsipv4SecIfIndex].u4IfPktsApply;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfPktsDiscard
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfPktsDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfPktsDiscard (INT4 i4Fsipv4SecIfIndex,
                              UINT4 *pu4RetValFsipv4SecIfPktsDiscard)
{

    *pu4RetValFsipv4SecIfPktsDiscard
        = gatIpsecv4Stat[i4Fsipv4SecIfIndex].u4IfPktsDiscard;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecIfPktsBypass
 Input       :  The Indices
                Fsipv4SecIfIndex

                The Object
                retValFsipv4SecIfPktsBypass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecIfPktsBypass (INT4 i4Fsipv4SecIfIndex,
                             UINT4 *pu4RetValFsipv4SecIfPktsBypass)
{

    *pu4RetValFsipv4SecIfPktsBypass
        = gatIpsecv4Stat[i4Fsipv4SecIfIndex].u4IfPktsBypass;

    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Fsipv4SecAhEspStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecAhEspStatsTable
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsipv4SecAhEspStatsTable (INT4 i4Fsipv4SecAhEspIfIndex)
{

    if ((i4Fsipv4SecAhEspIfIndex < SEC_MIN_STAT_COUNT)
        || (i4Fsipv4SecAhEspIfIndex > SEC_MAX_STAT_COUNT))
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecAhEspStatsTable
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsipv4SecAhEspStatsTable (INT4 *pi4Fsipv4SecAhEspIfIndex)
{

    tSecv4AhEspStat    *pSecAhEspStatsIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER, u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;
    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecAhEspStatsIf = &gatIpsecv4AhEspStat[u4Count];
        if ((pSecAhEspStatsIf->u4Index < (UINT4) u4FirstIndex)
            && (pSecAhEspStatsIf->u4Index != 0))
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecAhEspStatsIf->u4Index;
        }
    }
    if ((Flag == SEC_NOT_FOUND) || (u4FirstIndex < SEC_MIN_STAT_COUNT))
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv4SecAhEspIfIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecAhEspStatsTable
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex
                nextFsipv4SecAhEspIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecAhEspStatsTable (INT4 i4Fsipv4SecAhEspIfIndex,
                                         INT4 *pi4NextFsipv4SecAhEspIfIndex)
{

    tSecv4AhEspStat    *pSecAhEspStatsIf = NULL;
    UINT4               u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;
    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {

        pSecAhEspStatsIf = &gatIpsecv4AhEspStat[u4Count];
        if (pSecAhEspStatsIf->u4Index > (UINT4) i4Fsipv4SecAhEspIfIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv4SecAhEspIfIndex =
                    (INT4) pSecAhEspStatsIf->u4Index;
                Flag = SEC_FOUND;
            }
            else
            {
                if (pSecAhEspStatsIf->
                    u4Index < (UINT4) *pi4NextFsipv4SecAhEspIfIndex)
                {
                    *pi4NextFsipv4SecAhEspIfIndex =
                        (INT4) pSecAhEspStatsIf->u4Index;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsipv4SecInAhPkts
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecInAhPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecInAhPkts (INT4 i4Fsipv4SecAhEspIfIndex,
                         UINT4 *pu4RetValFsipv4SecInAhPkts)
{

    *pu4RetValFsipv4SecInAhPkts
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4InAhPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecOutAhPkts
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecOutAhPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecOutAhPkts (INT4 i4Fsipv4SecAhEspIfIndex,
                          UINT4 *pu4RetValFsipv4SecOutAhPkts)
{

    *pu4RetValFsipv4SecOutAhPkts
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4OutAhPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAhPktsAllow
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecAhPktsAllow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAhPktsAllow (INT4 i4Fsipv4SecAhEspIfIndex,
                            UINT4 *pu4RetValFsipv4SecAhPktsAllow)
{

    *pu4RetValFsipv4SecAhPktsAllow
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4AhPktsAllow;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAhPktsDiscard
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecAhPktsDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAhPktsDiscard (INT4 i4Fsipv4SecAhEspIfIndex,
                              UINT4 *pu4RetValFsipv4SecAhPktsDiscard)
{

    *pu4RetValFsipv4SecAhPktsDiscard
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4AhPktsDiscard;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecInEspPkts
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecInEspPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecInEspPkts (INT4 i4Fsipv4SecAhEspIfIndex,
                          UINT4 *pu4RetValFsipv4SecInEspPkts)
{

    *pu4RetValFsipv4SecInEspPkts
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4InEspPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecOutEspPkts
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecOutEspPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecOutEspPkts (INT4 i4Fsipv4SecAhEspIfIndex,
                           UINT4 *pu4RetValFsipv4SecOutEspPkts)
{

    *pu4RetValFsipv4SecOutEspPkts
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4OutEspPkts;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecEspPktsDiscard
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecEspPktsDiscard
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecEspPktsDiscard (INT4 i4Fsipv4SecAhEspIfIndex,
                               UINT4 *pu4RetValFsipv4SecEspPktsDiscard)
{

    *pu4RetValFsipv4SecEspPktsDiscard
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4EspPktsDiscard;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecEspPktsAllow
 Input       :  The Indices
                Fsipv4SecAhEspIfIndex

                The Object
                retValFsipv4SecEspPktsAllow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecEspPktsAllow (INT4 i4Fsipv4SecAhEspIfIndex,
                             UINT4 *pu4RetValFsipv4SecEspPktsAllow)
{

    *pu4RetValFsipv4SecEspPktsAllow
        = gatIpsecv4AhEspStat[i4Fsipv4SecAhEspIfIndex].u4EspPktsAllow;

    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Fsipv4SecAhEspIntruTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsipv4SecAhEspIntruTable
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceFsipv4SecAhEspIntruTable (INT4
                                                  i4Fsipv4SecAhEspIntruIndex)
{
    if ((i4Fsipv4SecAhEspIntruIndex < SEC_MIN_INTEGER)
        || (i4Fsipv4SecAhEspIntruIndex > SEC_MAX_STAT_COUNT))
    {
        return (SNMP_FAILURE);
    }

    if (gatIpsecv4AhEspIntruStat
        [i4Fsipv4SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4Index == 0)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsipv4SecAhEspIntruTable
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsipv4SecAhEspIntruTable (INT4 *pi4Fsipv4SecAhEspIntruIndex)
{
    tSecv4AhEspIntruStat *pSecAhEspIntruStatsIf = NULL;
    UINT4               u4FirstIndex = SEC_MAX_INTEGER, u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;
    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecAhEspIntruStatsIf = &gatIpsecv4AhEspIntruStat[u4Count];
        if ((pSecAhEspIntruStatsIf->u4Index <
             (UINT4) u4FirstIndex) && (pSecAhEspIntruStatsIf->u4Index != 0))
        {
            Flag = SEC_FOUND;
            u4FirstIndex = pSecAhEspIntruStatsIf->u4Index;
        }
    }
    if ((Flag == SEC_NOT_FOUND) || (u4FirstIndex < SEC_MIN_STAT_COUNT))
    {
        return SNMP_FAILURE;
    }
    *pi4Fsipv4SecAhEspIntruIndex = (INT4) u4FirstIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsipv4SecAhEspIntruTable
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex
                nextFsipv4SecAhEspIntruIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsipv4SecAhEspIntruTable (INT4
                                         i4Fsipv4SecAhEspIntruIndex,
                                         INT4 *pi4NextFsipv4SecAhEspIntruIndex)
{
    tSecv4AhEspIntruStat *pSecAhEspIntruStatsIf = NULL;
    UINT4               u4Count = 0;
    INT4                Flag = SEC_NOT_FOUND;
    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        pSecAhEspIntruStatsIf = &gatIpsecv4AhEspIntruStat[u4Count];
        if (pSecAhEspIntruStatsIf->u4Index > (UINT4) i4Fsipv4SecAhEspIntruIndex)
        {
            if (Flag == SEC_NOT_FOUND)
            {
                *pi4NextFsipv4SecAhEspIntruIndex
                    = (INT4) pSecAhEspIntruStatsIf->u4Index;
                Flag = SEC_FOUND;
            }
            else
            {
                if (pSecAhEspIntruStatsIf->
                    u4Index < (UINT4) *pi4NextFsipv4SecAhEspIntruIndex)
                {
                    *pi4NextFsipv4SecAhEspIntruIndex
                        = (INT4) pSecAhEspIntruStatsIf->u4Index;
                }
            }
        }
    }
    if (Flag == SEC_NOT_FOUND)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsipv4SecAhEspIntruIfIndex
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex

                The Object
                retValFsipv4SecAhEspIntruIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAhEspIntruIfIndex (INT4 i4Fsipv4SecAhEspIntruIndex,
                                  INT4 *pi4RetValFsipv4SecAhEspIntruIfIndex)
{

    *pi4RetValFsipv4SecAhEspIntruIfIndex =
        (INT4) gatIpsecv4AhEspIntruStat
        [i4Fsipv4SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4IfIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAhEspIntruSrcAddr
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex

                The Object
                retValFsipv4SecAhEspIntruSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAhEspIntruSrcAddr (INT4 i4Fsipv4SecAhEspIntruIndex,
                                  UINT4 *pu4RetValFsipv4SecAhEspIntruSrcAddr)
{

    *pu4RetValFsipv4SecAhEspIntruSrcAddr =
        gatIpsecv4AhEspIntruStat
        [i4Fsipv4SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4AhEspIntruSrcAddr;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAhEspIntruDestAddr
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex

                The Object
                retValFsipv4SecAhEspIntruDestAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAhEspIntruDestAddr (INT4
                                   i4Fsipv4SecAhEspIntruIndex,
                                   UINT4 *pu4RetValFsipv4SecAhEspIntruDestAddr)
{

    *pu4RetValFsipv4SecAhEspIntruDestAddr =
        gatIpsecv4AhEspIntruStat
        [i4Fsipv4SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4AhEspIntruDestAddr;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAhEspIntruProto
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex

                The Object
                retValFsipv4SecAhEspIntruProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAhEspIntruProto (INT4 i4Fsipv4SecAhEspIntruIndex,
                                INT4 *pi4RetValFsipv4SecAhEspIntruProto)
{

    *pi4RetValFsipv4SecAhEspIntruProto
        =
        (INT4) gatIpsecv4AhEspIntruStat
        [i4Fsipv4SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4AhEspIntruProt;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsipv4SecAhEspIntruTime
 Input       :  The Indices
                Fsipv4SecAhEspIntruIndex

                The Object
                retValFsipv4SecAhEspIntruTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsipv4SecAhEspIntruTime (INT4 i4Fsipv4SecAhEspIntruIndex,
                               UINT4 *pu4RetValFsipv4SecAhEspIntruTime)
{

    *pu4RetValFsipv4SecAhEspIntruTime = gatIpsecv4AhEspIntruStat
        [i4Fsipv4SecAhEspIntruIndex - SEC_MIN_STAT_COUNT].u4AhEspIntruTime;
    return (SNMP_SUCCESS);
}
