/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4esp.c,v 1.16 2014/03/07 11:57:40 siva Exp $
 *
 * Description: This has functions for ESP SubModule
 *
 ***********************************************************************/

#include "secv4com.h"
#include "secv4esp.h"
#include "msr.h"
UINT1               gu1PaddingBuf[SEC_MAX_PAD_LEN];

/************************************************************************/
/*  Function Name : Secv4UdpEspTunnelEncode                             */
/*  Description   : This function gets the outgoing packet and          */
/*                : does tunnel mode encode based on SA Entry           */
/*                : Encapsulates the whole encrypted content inside     */
/*                : UDP datagram, source port used will be 4500, dest   */
/*                : Port will be the remote port present inside Natt    */
/*                : Information Structure.                              */
/*                :                                                     */
/*  Input(s)      : pCBuf  - Buffer contains IP packet to be secured    */
/*                : pSaEntry  - Pointer to a SA Entry                   */
/*                : u4IfIndex - The interface on which packet arrived   */
/*                :                                                     */
/*  Output(s)     : pCBuf - buffer contains secured IP packet           */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/*************************************************************************/
UINT1
Secv4UdpEspTunnelEncode (tCRU_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                         UINT4 u4IfIndex, t_IP_HEADER * pOrigIpHdr,
                         UINT1 u1NextHdr)
{
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    t_IP_HEADER         NewIpHdr;
    tSecv4UdpHdr        Secv4UdpHdr;
    UINT4               au4EspHeader[SEC_ESP_HEADER];
    UINT4               u4Size = 0;
    UINT1               u1Counter = 0;
    UINT1               u1Size = 0;
    UINT1               u1AuthDigestSize = 0;
    UINT2               u2UdpHdrLen = 0;
    UINT4               u4Mode = ESP;

    MEMSET (&NewIpHdr, 0, sizeof (t_IP_HEADER));
    /*Calculate the padding size */
    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4UdpEspTunnelEncode:-"
                   "Received packet size zero \n");
        return SEC_FAILURE;
    }
    if (pSaEntry->u1SecAssocEspAlgo == SEC_NULLESPALGO)
    {
        /* If the encryption algorithm is NULL, the data has to be aligned
         *  by 4 octet boundary */
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          SEC_ESP_NULL_MULTIPLY_FACTOR);

        if (u1Size != 0)
        {
            u1Size = (UINT1) (SEC_ESP_NULL_MULTIPLY_FACTOR - u1Size);
        }
    }
    else
    {
        u1Size =
            (UINT1) ((u4Size +
                      SEC_ESP_TRAILER_LEN) % (pSaEntry->u1SecEspMultFact));

        if (u1Size != 0)
        {
            u1Size = (UINT1) (pSaEntry->u1SecEspMultFact - u1Size);
        }
    }
    /* Fill the Padding Contents with the monotonically increasing sequence */

    if ((u1Size + 1) >= SEC_MAX_PAD_LEN)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4UdpEspTunnelEncode: -"
                   "Invalid Pad length\n");
        return SEC_FAILURE;
    }
    for (u1Counter = 0; u1Counter < u1Size; u1Counter++)
    {
        gu1PaddingBuf[u1Counter] = (UINT1) (u1Counter + 1);
    }
    gu1PaddingBuf[u1Size] = u1Size;
    gu1PaddingBuf[u1Size + 1] = u1NextHdr;

    if (pSaEntry->u1SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        u1AuthDigestSize = 0;
    }
    else
    {
        u1AuthDigestSize = pSaEntry->u1SecAhAlgoDigestSize;
    }
    if (CRU_BUF_Copy_OverBufChain (pCBuf, gu1PaddingBuf, u4Size,
                                   (UINT4) (u1Size + SEC_ESP_TRAILER_LEN +
                                            u1AuthDigestSize)) == CRU_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4UdpEspTunnelEncode: -"
                   "Copy over cru buffer failed for add Padding,"
                   "Padlen and Esp trailer \n");
        return SEC_FAILURE;
    }

    /* Prepend Init Vector */
    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        if (IPSEC_BUF_Prepend (pCBuf, pSaEntry->pu1SecAssocInitVector,
                               pSaEntry->u1SecAssocIvSize) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4UdpEspTunnelEncode:- "
                       "Prepend buffer failed  to add Init Vector\n");
            return SEC_FAILURE;
        }
    }

    /* Form ESP Header and Prepend to pBuf */
    au4EspHeader[0] = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    au4EspHeader[1] = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) au4EspHeader,
                           SEC_ESP_HEADER_LEN) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4UdpEspTunnelEncode:-"
                   "Prepend buffer failed to add ESP Header\n");
        return (SEC_FAILURE);
    }

    /*Calculate the new packet length */
    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4UdpEspTunnelEncode:-"
                   "Received packet size zero \n");
        return SEC_FAILURE;
    }

    /* Form New IPv4 Header and Prepend to pBuf */
    NewIpHdr.u1Ver_hdrlen = SEC_VERS_AND_HLEN (SEC_V4, 0);
    NewIpHdr.u1Tos = pOrigIpHdr->u1Tos;
    NewIpHdr.u2Id = pOrigIpHdr->u2Id;
    NewIpHdr.u2Fl_offs = 0;
    NewIpHdr.u1Ttl = SEC_MAX_HOP_LIMIT;
    NewIpHdr.u1Proto = SEC_ESP;
    NewIpHdr.u2Cksum = 0;
    NewIpHdr.u4Src = IPSEC_HTONL (pSaEntry->u4SecAssocSrcAddr);
    NewIpHdr.u4Dest = IPSEC_HTONL (pSaEntry->u4SecAssocDestAddr);

    /* append UDP Header */
    MEMSET (&Secv4UdpHdr, ZERO, sizeof (tSecv4UdpHdr));

    u2UdpHdrLen = sizeof (tSecv4UdpHdr);
    Secv4UdpHdr.u2SrcPort = IPSEC_HTONS (IKE_NATT_UDP_PORT);
    Secv4UdpHdr.u2DestPort = IPSEC_HTONS (pSaEntry->IkeNattInfo.u2RemotePort);
    Secv4UdpHdr.u2Len = (UINT2) (IPSEC_HTONS (((UINT2) u4Size + u2UdpHdrLen)));

    NewIpHdr.u1Proto = SEC_UDP;

    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) &Secv4UdpHdr,
                           u2UdpHdrLen) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4UdpEspTunnelEncode:-"
                   "Prepend buffer failed to new UDP header \n");
        return (SEC_FAILURE);
    }

    NewIpHdr.u2Totlen = (UINT2) (IPSEC_HTONS ((u4Size +
                                               ((UINT4) (u2UdpHdrLen)) +
                                               SEC_IPV4_HEADER_SIZE)));

    NewIpHdr.u2Cksum =
        Secv4IPCalcChkSum ((UINT1 *) &NewIpHdr, SEC_IPV4_HEADER_SIZE);
    NewIpHdr.u2Cksum = IPSEC_HTONS (NewIpHdr.u2Cksum);

    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) &NewIpHdr,
                           SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4UdpEspTunnelEncode:-"
                   "Prepend buffer failed to new IP header \n");
        return (SEC_FAILURE);
    }

    /* Store necessary data to AlgoMsg for further processing */
    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);

    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4UdpEspTunnelEncode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4UdpEspTunnelEncode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }

    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;
    pSecv4TaskletData->pCBuf = pCBuf;

    pSecv4TaskletData->u4AuthOffset = (UINT4) (SEC_IPV4_HEADER_SIZE +
                                               (UINT4) u2UdpHdrLen);
    pSecv4TaskletData->u4AuthBufSize = (UINT4) (u4Size - SEC_IPV4_HEADER_SIZE -
                                                (UINT4) u2UdpHdrLen -
                                                pSaEntry->
                                                u1SecAhAlgoDigestSize);

    pSecv4TaskletData->u4EncrOffset = (UINT4) (SEC_IPV4_HEADER_SIZE +
                                               SEC_ESP_HEADER_LEN +
                                               (UINT4) u2UdpHdrLen +
                                               (UINT4) pSaEntry->
                                               u1SecAssocIvSize);

    pSecv4TaskletData->u4EncrBufSize = (UINT4) (u4Size -
                                                (SEC_IPV4_HEADER_SIZE +
                                                 SEC_ESP_HEADER_LEN +
                                                 (UINT4) u2UdpHdrLen +
                                                 pSaEntry->u1SecAssocIvSize) -
                                                (UINT4) u1AuthDigestSize);

    pSecv4TaskletData->u4IcvOffset =
        (UINT4) (u4Size - pSaEntry->u1SecAhAlgoDigestSize);
    pSecv4TaskletData->u4Mode = ESP;
    pSecv4TaskletData->u4Action = SEC_ENCODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4Size;

    /*
     * Deliberately setting the mode as AH to avoid Encr/Decr 
     * in HW Accl case. for SW crypt bypass is handled in 
     * Secv4AlgoProcessIpsec 
     */
    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
        u4Mode = AUTH;

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, u4Mode, SEC_ENCODE) ==
        SEC_FAILURE)
    {
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4UdpEspTunnelEncode:-"
                   "Secv4AlgoProcessPkt failed \n");
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name : Secv4EspTunnelEncode                                */
/*  Description   : This function gets the outgoing packet and          */
/*                : does tunnel mode encode based on SA Entry           */
/*                :                                                     */
/*  Input(s)      : pCBuf  - Buffer contains IP packet to be secured    */
/*                : pSaEntry  - Pointer to a SA Entry                   */
/*                : u4IfIndex - The interface on which packet arrived   */
/*                :                                                     */
/*  Output(s)     : pCBuf - buffer contains secured IP packet           */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/*************************************************************************/
UINT1
Secv4EspTunnelEncode (tCRU_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                      UINT4 u4IfIndex, t_IP_HEADER * pOrigIpHdr,
                      UINT1 u1NextHdr)
{
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    t_IP_HEADER         NewIpHdr;
    UINT4               u4Size = 0;
    UINT4               au4EspHeader[SEC_ESP_HEADER];
    UINT1               u1Counter = 0;
    UINT1               u1Size = 0;
    UINT1               u1AuthDigestSize = 0;
    UINT4               u4Mode = ESP;

    MEMSET (&NewIpHdr, 0, sizeof (t_IP_HEADER));
    /*Calculate the padding size */
    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTunnelEncode:-"
                   "Received packet size zero \n");
        return SEC_FAILURE;
    }
    if (pSaEntry->u1SecAssocEspAlgo == SEC_NULLESPALGO)
    {
        /* If the encryption algorithm is NULL, the data has to be aligned
         *  by 4 octet boundary */
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          SEC_ESP_NULL_MULTIPLY_FACTOR);

        if (u1Size != 0)
        {
            u1Size = (UINT1) (SEC_ESP_NULL_MULTIPLY_FACTOR - u1Size);
        }
    }
    else
    {
        u1Size =
            (UINT1) ((u4Size +
                      SEC_ESP_TRAILER_LEN) % (pSaEntry->u1SecEspMultFact));

        if (u1Size != 0)
        {
            u1Size = (UINT1) (pSaEntry->u1SecEspMultFact - u1Size);
        }
    }
    /* Fill the Padding Contents with the monotonically increasing sequence */
    if ((u1Size + 1) >= SEC_MAX_PAD_LEN)
    {
        SECv4_TRC2 (SECv4_CONTROL_PLANE, "\n Secv4EspTunnelEncode: -"
                    "Invalid Pad Length %d Pkt Size %d\n", u1Size, u4Size);
        return SEC_FAILURE;
    }
    for (u1Counter = 0; u1Counter < u1Size; u1Counter++)
    {
        gu1PaddingBuf[u1Counter] = (UINT1) (u1Counter + 1);
    }
    gu1PaddingBuf[u1Size] = u1Size;
    gu1PaddingBuf[u1Size + 1] = u1NextHdr;

    if (pSaEntry->u1SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        u1AuthDigestSize = 0;
    }
    else
    {
        u1AuthDigestSize = pSaEntry->u1SecAhAlgoDigestSize;
    }
    if (CRU_BUF_Copy_OverBufChain (pCBuf, gu1PaddingBuf, u4Size,
                                   (UINT4) (u1Size + SEC_ESP_TRAILER_LEN +
                                            u1AuthDigestSize)) == CRU_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTunnelEncode: -"
                   "Copy over cru buffer failed for add Padding,"
                   "Padlen and Esp trailer \n");
        return SEC_FAILURE;
    }

    /* Prepend Init Vector */
    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        if (IPSEC_BUF_Prepend (pCBuf, pSaEntry->pu1SecAssocInitVector,
                               pSaEntry->u1SecAssocIvSize) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTunnelEncode:- "
                       "Prepend buffer failed  to add Init Vector\n");
            return SEC_FAILURE;
        }
    }

    /* Form ESP Header and Prepend to pBuf */
    au4EspHeader[0] = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    au4EspHeader[1] = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) au4EspHeader,
                           SEC_ESP_HEADER_LEN) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTunnelEncode:-"
                   "Prepend buffer failed to add ESP Header\n");
        return (SEC_FAILURE);
    }

    /*Calculate the new packet length */
    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTunnelEncode:-"
                   "Received packet size zero \n");
        return SEC_FAILURE;
    }

    /* Form New IPv4 Header and Prepend to pBuf */
    NewIpHdr.u1Ver_hdrlen = SEC_VERS_AND_HLEN (SEC_V4, 0);
    NewIpHdr.u1Tos = pOrigIpHdr->u1Tos;
    NewIpHdr.u2Totlen = (UINT2) (IPSEC_HTONS ((u4Size + SEC_IPV4_HEADER_SIZE)));
    NewIpHdr.u2Id = pOrigIpHdr->u2Id;
    NewIpHdr.u2Fl_offs = 0;
    NewIpHdr.u1Ttl = SEC_MAX_HOP_LIMIT;
    NewIpHdr.u1Proto = SEC_ESP;
    NewIpHdr.u2Cksum = 0;
    NewIpHdr.u4Src = IPSEC_HTONL (pSaEntry->u4SecAssocSrcAddr);
    NewIpHdr.u4Dest = IPSEC_HTONL (pSaEntry->u4SecAssocDestAddr);
    NewIpHdr.u2Cksum =
        Secv4IPCalcChkSum ((UINT1 *) &NewIpHdr, SEC_IPV4_HEADER_SIZE);
    NewIpHdr.u2Cksum = IPSEC_HTONS (NewIpHdr.u2Cksum);

    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) &NewIpHdr,
                           SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTunnelEncode:-"
                   "Prepend buffer failed to new IP header \n");
        return (SEC_FAILURE);
    }

    /* Store necessary data to AlgoMsg for further processing */
    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTunnelEncode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;

    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTunnelEncode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;

    }

    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = SEC_IPV4_HEADER_SIZE;

    pSecv4TaskletData->u4AuthBufSize = u4Size - SEC_IPV4_HEADER_SIZE -
        pSaEntry->u1SecAhAlgoDigestSize;

    pSecv4TaskletData->u4EncrOffset = (UINT4) (SEC_IPV4_HEADER_SIZE +
                                               SEC_ESP_HEADER_LEN +
                                               pSaEntry->u1SecAssocIvSize);

    pSecv4TaskletData->u4EncrBufSize = (UINT4) (u4Size -
                                                (UINT4) (SEC_IPV4_HEADER_SIZE +
                                                         SEC_ESP_HEADER_LEN +
                                                         pSaEntry->
                                                         u1SecAssocIvSize) -
                                                u1AuthDigestSize);

    pSecv4TaskletData->u4IcvOffset = u4Size - pSaEntry->u1SecAhAlgoDigestSize;
    pSecv4TaskletData->u4Mode = ESP;
    pSecv4TaskletData->u4Action = SEC_ENCODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4Size;

    /*
     * Deliberately setting the mode as AH to avoid Encr/Decr 
     * in HW Accl case. for SW crypt bypass is handled in 
     * Secv4AlgoProcessIpsec 
     */
    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
        u4Mode = AUTH;

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, u4Mode, SEC_ENCODE) ==
        SEC_FAILURE)
    {
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTunnelEncode:-"
                   "Secv4AlgoProcessPkt failed \n");
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}

/**********************************************************************/
/*  Function Name : Secv4EspTunnelDecode                              */
/*  Description   : This function gets the incomming packet and       */
/*                : does tunnel mode decode                           */
/*                : based on SA Entry                                 */
/*  Input(s)      : pCBuf - Buffer contains IP Secured packet         */
/*                : pSaEntry - Pointer to a SA Entry                  */
/*                : u4ProcessedLen - Length upto which the pkt is     */
/*                :                  processed                        */
/*                : u4IfIndex - Interface index  on which             */
/*                              packet arrived                        */
/*  Output(s)     : pCBuf - Buffer contains Decoded IP packet         */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                        */
/**********************************************************************/

UINT1
Secv4EspTunnelDecode (tCRU_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                      UINT4 u4ProcessedLen, UINT4 u4IfIndex,
                      t_IP_HEADER * pIpHdr)
{
    UINT4               u4SeqNumber = 0;
    UINT4               u4PktSize = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               u1AuthDigestSize = 0;
    UINT4               u4Mode = ESP;
    tSecv4TaskletData  *pSecv4TaskletData = NULL;

    INT1                ai1IpAddr[100];
    UINT2               u2IpTotLen;
    UINT1               u1IpHdrLen;

    IPADDR_HEX2STR (pIpHdr->u4Src, ai1IpAddr);

    /* Verify Sequence Number */
    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pCBuf);
        if (u4PktSize == 0)
        {
            SECv4_TRC1 (SECv4_CONTROL_PLANE, "\r\n Secv4EspTunnelDecode:- "
                        "Invalid Pkt Size :%s\r\n", ai1IpAddr);
            return (SEC_FAILURE);
        }

        pu1Buf = (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0, u4PktSize);
        if (pu1Buf == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u4SeqNumber,
                                       (u4ProcessedLen + SEC_ESP_SEQ_OFFSET),
                                       sizeof (UINT4));
            u4SeqNumber = IPSEC_NTOHL (u4SeqNumber);
        }
        else
        {
            u4SeqNumber =
                IPSEC_NTOHL (*
                             ((UINT4 *) (void *) (pu1Buf + u4ProcessedLen +
                                                  SEC_ESP_SEQ_OFFSET)));
        }

        if (Secv4SeqNumberVerification (u4SeqNumber, pSaEntry) == SEC_FAILURE)
        {
            SECv4_TRC1 (SECv4_DATA_PATH, "\r\n Secv4EspTunnelDecode:-"
                        "Replay attack from %s\r\n", ai1IpAddr);
            return SEC_FAILURE;
        }
    }

    /* Remove the Outer IP Header and  IP Options if Present  */
    if (IPSEC_BUF_MoveOffset (pCBuf, u4ProcessedLen) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH, "\nSecv4EspTunnelDecode : Move Offset Faild"
                   "to remove Ip Header\n");
        return (SEC_FAILURE);
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4PktSize == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTunnelDecode:- "
                   "Invalid Pkt Size \n");
        return (SEC_FAILURE);
    }

    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        /* Store InitVector for algo processing */
        if (IPSEC_COPY_FROM_BUF (pCBuf,
                                 (UINT1 *) pSaEntry->pu1SecAssocInitVector,
                                 SEC_ESP_HEADER_LEN,
                                 pSaEntry->u1SecAssocIvSize) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTunnelDecode:- "
                       "Copy from Buf failed for init vector\n");
            return SEC_FAILURE;
        }
    }
    if (pSaEntry->u1SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        u1AuthDigestSize = 0;
    }
    else
    {
        u1AuthDigestSize = pSaEntry->u1SecAhAlgoDigestSize;
    }

    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTunnelDecode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTunnelDecode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }

    u2IpTotLen = IPSEC_NTOHS (pIpHdr->u2Totlen);
    u1IpHdrLen = (UINT1) ((pIpHdr->u1Ver_hdrlen & 0x0f) * 4);

    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = 0;
    pSecv4TaskletData->u4AuthBufSize =
        (UINT4) ((u2IpTotLen - u1IpHdrLen) - pSaEntry->u1SecAhAlgoDigestSize);
    pSecv4TaskletData->u4EncrOffset =
        (UINT4) (SEC_ESP_HEADER_LEN + pSaEntry->u1SecAssocIvSize);
    pSecv4TaskletData->u4EncrBufSize =
        (UINT4) ((u2IpTotLen - u1IpHdrLen) - (SEC_ESP_HEADER_LEN +
                                              pSaEntry->u1SecAssocIvSize +
                                              u1AuthDigestSize));
    pSecv4TaskletData->u4IcvOffset =
        (UINT4) ((u2IpTotLen - u1IpHdrLen) - pSaEntry->u1SecAhAlgoDigestSize);
    pSecv4TaskletData->u4Mode = ESP;
    pSecv4TaskletData->u4Action = SEC_DECODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4PktSize;

    /*
     * Deliberately setting the mode as AH to avoid Encr/Decr 
     * in HW Accl case. for SW crypt bypass is handled in 
     * Secv4AlgoProcessIpsec 
     */
    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
        u4Mode = AUTH;

    if ((pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC) &&
        (pSaEntry->u1SecAssocAhAlgo == HMAC_SHA_512))
    {
        if (Secv4SoftProcessPkt (pSecv4TaskletData, u4Mode, SEC_DECODE) ==
            SEC_FAILURE)
        {
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "\nSecv4EspTunnelDecode:-"
                       "Secv4SoftProcessPkt failed \n");
            return SEC_FAILURE;
        }
        return SEC_SUCCESS;
    }

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, u4Mode, SEC_DECODE) ==
        SEC_FAILURE)
    {
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTunnelDecode:-"
                   "Secv4AlgoProcessPkt failed \n");
        return SEC_FAILURE;
    }

    return SEC_SUCCESS;
}

/**************************************************************************/
/*  Function Name : Secv4EspTransportEncode                               */
/*  Description   : This function gets the outgoing packet and            */
/*                : does transport mode encode based on SA entry          */
/*                :                                                       */
/*  Input(s)      : pCBuf - buffer contains IP packet                      */
/*                : pSaEntry - Pointer to a SA Entry                      */
/*                : u4IfIndex - The interface on which packet arrived     */
/*  Output(s)     : pCBuf - buffer contains Secured IP packet              */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                            */
/**************************************************************************/

UINT1
Secv4EspTransportEncode (tCRU_BUF_CHAIN_HEADER * pCBuf,
                         tSecv4Assoc * pSaEntry, UINT4 u4IfIndex,
                         t_IP_HEADER * pTmpIpHdr, UINT1 u1NextHdr)
{
    UINT4               au4EspHeader[SEC_ESP_HEADER];
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    UINT4               u4Size = 0;
    UINT4               u4OptLen = 0;
    UINT1               u1Size = 0;
    UINT1               au1OptBuf[SEC_IPV4_MAX_OPT_LEN + SEC_IPV4_HEADER_SIZE];
    UINT1               u1Counter = 0;
    UINT1               u1AuthDigestSize = 0;
    UINT4               u4Mode = ESP;

    if (pSaEntry->u1SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        u1AuthDigestSize = 0;
    }
    else
    {
        u1AuthDigestSize = pSaEntry->u1SecAhAlgoDigestSize;
    }

    /* Store the IP header */
    if (IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) &au1OptBuf, 0,
                             SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                   "Copy of IP header failed \n");
        return SEC_FAILURE;
    }

    if (SecUtilIpIfIsOurAddress (IPSEC_NTOHL (pTmpIpHdr->u4Src)) != TRUE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4EspTransportEncode Attempting to Apply "
                   "Transport Mode SA for the System acting as a Gateway\n");
        return SEC_FAILURE;
    }

    /* Get the Options Length form HdrVersion field of IP 
     * and store the options if present*/
    u4OptLen = (UINT4) IPSEC_OLEN (pTmpIpHdr->u1Ver_hdrlen);

    if (u4OptLen != 0)
    {
        if (IPSEC_COPY_FROM_BUF (pCBuf,
                                 (UINT1 *) (au1OptBuf + SEC_IPV4_HEADER_SIZE),
                                 SEC_IPV4_HEADER_SIZE, u4OptLen) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                       "Copy of IP header failed \n");
            return SEC_FAILURE;
        }
    }

    /* Remove IP Header and the options from the Packet */
    if (IPSEC_BUF_MoveOffset (pCBuf, (SEC_IPV4_HEADER_SIZE + u4OptLen)) ==
        BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                   "Removal of IP Header and options failed \n");
        return (SEC_FAILURE);
    }

    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                   "Invalid Pkt Size \n");
        return (SEC_FAILURE);
    }

    /* Calculate the no of Padding Bytes to be added
     * For DES-CBC and 3DES-CBC the Pkt  must be multiple of 8 Bytes
     * For AES the Pkt must be multiple of 16 Bytes */
    if (pSaEntry->u1SecAssocEspAlgo == SEC_NULLESPALGO)
    {
        /* If the encryption algorithm is NULL, the data has to be aligned
         *  by 4 octet boundary */
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          SEC_ESP_NULL_MULTIPLY_FACTOR);

        if (u1Size != 0)
        {
            u1Size = (UINT1) (SEC_ESP_NULL_MULTIPLY_FACTOR - u1Size);
        }
    }
    else
    {
        u1Size = (UINT1) ((u4Size + SEC_ESP_TRAILER_LEN) %
                          (pSaEntry->u1SecEspMultFact));
        if (u1Size != 0)
        {
            u1Size = (UINT1) ((pSaEntry->u1SecEspMultFact) - u1Size);
        }
    }

    /* Fill the Padding Contents with the monotonically increasing sequence */
    if ((u1Size + 1) >= SEC_MAX_PAD_LEN)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode: -"
                   "Invalid pad length \n");
        return SEC_FAILURE;
    }
    for (u1Counter = 0; u1Counter < u1Size; u1Counter++)
    {
        gu1PaddingBuf[u1Counter] = (UINT1) (u1Counter + 1);
    }
    gu1PaddingBuf[u1Size] = u1Size;
    gu1PaddingBuf[u1Size + 1] = u1NextHdr;

    u4Size = IPSEC_BUF_GetValidBytes (pCBuf);
    if (CRU_BUF_Copy_OverBufChain (pCBuf, gu1PaddingBuf, u4Size,
                                   (UINT4) (u1Size + SEC_ESP_TRAILER_LEN +
                                            u1AuthDigestSize)) == CRU_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                   "Failed to append the Esp Trailer to the Buffer\n");
        return (SEC_FAILURE);
    }

    /* Prepend the IV the Buffer */
    if (pSaEntry->u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        if (IPSEC_BUF_Prepend (pCBuf, pSaEntry->pu1SecAssocInitVector,
                               pSaEntry->u1SecAssocIvSize) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                       "Prepend of Esp trailer failed\n");
            return (SEC_FAILURE);
        }
    }

    /*Form Esp Header and prepend to the Buffer */
    au4EspHeader[0] = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    au4EspHeader[1] = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) au4EspHeader,
                           SEC_ESP_HEADER_LEN) == BUF_FAILURE)
    {
        return (SEC_FAILURE);
    }

    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                   "Invalid Pkt Size \n");
        return (SEC_FAILURE);
    }

    /* Form the IP Header with new length and checksum. 
     * Prepend to the buffer */
    pTmpIpHdr = (t_IP_HEADER *) (void *) au1OptBuf;
    pTmpIpHdr->u1Proto = SEC_ESP;
    pTmpIpHdr->u2Totlen = IPSEC_HTONS (((SEC_IPV4_HEADER_SIZE + u4OptLen) +
                                        u4Size));
    pTmpIpHdr->u2Cksum = 0;
    pTmpIpHdr->u2Cksum =
        Secv4IPCalcChkSum (au1OptBuf, (u4OptLen + SEC_IPV4_HEADER_SIZE));
    pTmpIpHdr->u2Cksum = IPSEC_HTONS (pTmpIpHdr->u2Cksum);

    /* Prepend IP Header and IP options to the Buffer */
    if (IPSEC_BUF_Prepend (pCBuf, au1OptBuf, (SEC_IPV4_HEADER_SIZE + u4OptLen))
        == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                   "Prepend IP Header &options to the Buffer failed\n");
        return (SEC_FAILURE);
    }

    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportEncode:- "
                   "Invalid Pkt Size \n");
        return (SEC_FAILURE);
    }
    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTransportEncode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTransportEncode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }

    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = (SEC_IPV4_HEADER_SIZE + u4OptLen);
    pSecv4TaskletData->u4AuthBufSize =
        (u4Size -
         (SEC_IPV4_HEADER_SIZE + u4OptLen + pSaEntry->u1SecAhAlgoDigestSize));
    pSecv4TaskletData->u4EncrOffset =
        (SEC_IPV4_HEADER_SIZE + u4OptLen + SEC_ESP_HEADER_LEN +
         pSaEntry->u1SecAssocIvSize);

    pSecv4TaskletData->u4EncrBufSize =
        (UINT4) (u4Size - (UINT4) (SEC_IPV4_HEADER_SIZE + SEC_ESP_HEADER_LEN +
                                   (UINT4) pSaEntry->u1SecAssocIvSize +
                                   u4OptLen + (UINT4) u1AuthDigestSize));
    pSecv4TaskletData->u4IcvOffset = u4Size - pSaEntry->u1SecAhAlgoDigestSize;
    pSecv4TaskletData->u4Mode = ESP;
    pSecv4TaskletData->u4Action = SEC_ENCODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4Size;

    /*
     * Deliberately setting the mode as AH to avoid Encr/Decr 
     * in HW Accl case. for SW crypt bypass is handled in 
     * Secv4AlgoProcessIpsec 
     */
    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
        u4Mode = AUTH;

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, u4Mode, SEC_ENCODE) ==
        SEC_FAILURE)
    {
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTransportEncode:-"
                   "Secv4AlgoProcessPkt failed \n");
        return SEC_FAILURE;
    }

    return (SEC_SUCCESS);
}

/************************************************************************/
/*  Function Name : Secv4EspTransportDecode                             */
/*  Description   : This function gets the incomming packet and         */
/*                : does transport mode decode based on sa entry        */
/*                :                                                     */
/*  Input(s)      : pCBuf - Buffer contains IP Secured packet           */
/*                : pSaEntry - Pointer to a SA Entry                    */
/*                : u4ProcessedLen - Indicates the length upto which    */
/*                : the packet is processed                             */
/*                : u4IfIndex - The interface index on which the Pkt    */
/*                :             arrived                                 */
/*  Output(s)     : pCBuf - buffer contains Decoded IP packet            */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/*************************************************************************/

UINT1
Secv4EspTransportDecode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                         UINT4 u4ProcessedLen, UINT4 u4IfIndex,
                         t_IP_HEADER * pIpHdr)
{
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    UINT4               u4OptLen = 0;
    UINT4               u4SeqNumber = 0;
    UINT1              *pu1Buf = NULL;
    UINT4               u4Size = 0;
    UINT1               u1AuthDigestSize = 0;
    UINT4               u4Mode = ESP;
    INT1                ai1IpAddr[100];
    IPADDR_HEX2STR (pIpHdr->u4Src, ai1IpAddr);

    u4Size = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4Size == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportDecode:- "
                   "Invalid Pkt Size \n");
        return (SEC_FAILURE);
    }

    u4OptLen = (u4ProcessedLen - SEC_IPV4_HEADER_SIZE);

    /* Verify Sequence Number */
    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        pu1Buf = (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0, u4Size);

        if (pu1Buf == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u4SeqNumber,
                                       (u4ProcessedLen + SEC_ESP_SEQ_OFFSET),
                                       sizeof (UINT4));
            u4SeqNumber = IPSEC_NTOHL (u4SeqNumber);
        }
        else
        {
            u4SeqNumber =
                IPSEC_NTOHL (*
                             ((UINT4 *) (void *) (pu1Buf + u4ProcessedLen +
                                                  SEC_ESP_SEQ_OFFSET)));
        }

        if (Secv4SeqNumberVerification (u4SeqNumber, pSaEntry) == SEC_FAILURE)
        {
            SECv4_TRC1 (SECv4_DATA_PATH, "\r\nReplay attack from %s\r\n",
                        ai1IpAddr);
            return SEC_FAILURE;
        }
    }
    if (pSaEntry->u1SecAssocAhAlgo == SEC_NULLAHALGO)
    {
        u1AuthDigestSize = 0;
    }
    else
    {
        u1AuthDigestSize = pSaEntry->u1SecAhAlgoDigestSize;
    }
    /*Store init vector for algo processing */
    if (IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) pSaEntry->pu1SecAssocInitVector,
                             (SEC_ESP_HEADER_LEN + SEC_IPV4_HEADER_SIZE +
                              u4OptLen),
                             pSaEntry->u1SecAssocIvSize) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4EspTransportDecode:- "
                   "Copy of init vector failed \n");
        return SEC_FAILURE;
    }

    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTransportDecode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4EspTransportDecode:- Failed to allocate memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }

    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = (SEC_IPV4_HEADER_SIZE + u4OptLen);
    pSecv4TaskletData->u4AuthBufSize = (u4Size -
                                        (UINT4) ((UINT4) pSaEntry->
                                                 u1SecAhAlgoDigestSize +
                                                 (UINT4) SEC_IPV4_HEADER_SIZE +
                                                 u4OptLen));
    pSecv4TaskletData->u4EncrOffset =
        (UINT4) (SEC_ESP_HEADER_LEN + (UINT4) pSaEntry->u1SecAssocIvSize +
                 SEC_IPV4_HEADER_SIZE + u4OptLen);
    pSecv4TaskletData->u4EncrBufSize =
        (UINT4) (u4Size -
                 ((UINT4) SEC_ESP_HEADER_LEN +
                  (UINT4) pSaEntry->u1SecAssocIvSize +
                  (UINT4) u1AuthDigestSize + (UINT4) SEC_IPV4_HEADER_SIZE +
                  u4OptLen));

    pSecv4TaskletData->u4IcvOffset = (u4Size - pSaEntry->u1SecAhAlgoDigestSize);
    pSecv4TaskletData->u4Mode = ESP;
    pSecv4TaskletData->u4Action = SEC_DECODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4Size;

    /*
     * Deliberately setting the mode as AH to avoid Encr/Decr 
     * in HW Accl case. for SW crypt bypass is handled in 
     * Secv4AlgoProcessIpsec 
     */
    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
        u4Mode = AUTH;

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, u4Mode, SEC_DECODE) ==
        SEC_FAILURE)
    {
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4EspTransportDecode:-"
                   "Secv4AlgoProcessPkt failed \n");
        return SEC_FAILURE;
    }

    return SEC_SUCCESS;
}

/****************************************************************************/
/* Function Name : Secv4EspOutGoingPerformCB                                */
/* Description   : This fuction handles all the outgoing ESP packets        */
/*               : after the algorintm processing by softwarwe or Hardare   */
/*               : accelerator                                              */
/* Input(s)      : pData -  Contains all the information needed to          */
/*               : process after the algorithm                              */
/* Output(s)     : None                                                     */
/* Return Values : None                                                     */
/****************************************************************************/
VOID
Secv4EspOutGoingPerformCB (VOID *pData)
{
    tSecv4TaskletData  *pTemp = NULL;

    pTemp = (tSecv4TaskletData *) pData;
    if (pTemp == NULL)
    {
        return;
    }

    Secv4UnLock ();
    Secv4UpdateIfStats (pTemp->u4IfIndex, SEC_OUTBOUND, SEC_APPLY);
    Secv4UpdateAhEspStats (pTemp->u4IfIndex, SEC_ESP, SEC_OUTBOUND, SEC_ALLOW);
    Secv4PostOutMsgToIP (pTemp->pCBuf, pTemp->u4IfIndex);
    MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pTemp);
}

/****************************************************************************/
/* Function Name : Secv4EspIncomingPerformCB                                */
/* Description   : This fuction handles all the incoming ESP packets        */
/*               : after the algorithm processing by softwarwe or Hardare   */
/*               : accelerator                                              */
/* Input(s)      : pData -  Contains all the information needed to          */
/*               : process after the algorithm                              */
/* Output(s)     : None                                                     */
/* Return Values : None                                                     */
/****************************************************************************/
VOID
Secv4EspIncomingPerformCB (VOID *pData)
{
    tSecv4TaskletData  *pTemp = NULL;

    pTemp = (tSecv4TaskletData *) pData;
    if (pTemp == NULL)
    {
        return;
    }

    if (pTemp->pSaEntry->u1SecAssocMode == SEC_TUNNEL)
    {
        Secv4EspIncomingTunnel (pData);
    }
    else if (pTemp->pSaEntry->u1SecAssocMode == SEC_TRANSPORT)
    {
        Secv4EspIncomingTransport (pData);
    }
    return;
}
