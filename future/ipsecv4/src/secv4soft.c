/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4soft.c,v 1.15 2014/03/07 11:57:40 siva Exp $
 *
 * Description: This has functions for Async Software Module
 *
 ***********************************************************************/
#ifndef _SECV4SOFT_C_
#define _SECV4SOFT_C_

#include "secv4com.h"
#include "secv4soft.h"
#include "secv4esp.h"

UINT1               gu1DataPtr[SEC_MAX_BUFFER_SIZE];

/******************************************************************************/
/*  Function Name : Secv4SoftProcessPkt                                       */
/*  Description   : In Sync scenario, this is a function call which will post */
/*                : post the message to queue and calls tasklet. In Async     */
/*                : scenario, this is a function call to the API provided by  */
/*                : Architecture.                                             */
/*  Input(s)      : pSecv4TskletData -  Contains all the information needed to    */
/*                : invoke algorithm module calls/APIs.                       */
/*                : u4Mode - tunnel or transport                              */
/*                : u4Action - encode or decode                               */
/*  Output(s)     : None                                                      */
/*  Return Values : Returns the success and failure of posting                */
/******************************************************************************/

INT1
Secv4SoftProcessPkt (tSecv4TaskletData * pSecv4TskletData, UINT4 u4Mode,
                     UINT4 u4Action)
{

    UNUSED_PARAM (u4Mode);
    UNUSED_PARAM (u4Action);
    /* Release the lock */
    Secv4UnLock ();
    /* Initialize the Tasklet as the earlier is expired */
    Secv4TaskletSchedFn ((UINT4 *) pSecv4TskletData);
    return SEC_SUCCESS;
}

/***********************************************************************/
/*  Function Name : Secv4TaskletSchedFn                                */
/*  Description   : This function gets the outgoing packet and         */
/*                : does tunnel mode encode based on SA Entry          */
/*                :                                                    */
/*  Input(s)      : VOID - Input Nothing                               */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/

VOID
Secv4TaskletSchedFn (UINT4 *pu4Data)
{
    tSecv4TaskletData  *pTemp = NULL;
    UINT1               au1AuthDigest[SEC_ALGO_DIGEST_SIZE];
    UINT1               au1RcvdDigest[SEC_ALGO_DIGEST_SIZE];

    IPSEC_MEMSET (au1RcvdDigest, '\0', SEC_ALGO_DIGEST_SIZE);
    IPSEC_MEMSET (au1AuthDigest, '\0', SEC_ALGO_DIGEST_SIZE);

    pTemp = (tSecv4TaskletData *) pu4Data;
    if (pTemp == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4TaskletSchedFn:-"
                   "Data to Secv4TaskletSchedFn NULL \n");
        return;
    }

    /* Taking the Lock */
    Secv4Lock ();

    if ((pTemp->pCBuf == NULL) || (pTemp->pSaEntry == NULL))
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4TaskletSchedFn:-"
                   "Buffer or Sa entry is NULL \n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }
    pTemp->pSaEntry = Secv4AssocGetEntry (pTemp->u4SecAssocIndex);
    if (pTemp->pSaEntry == NULL)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4TaskletSchedFn:- No related SecAssoc Callback\n");
        Secv4HandleFailueAfterCryptoProcess (pTemp);
        return;
    }

    if (pTemp->u4Action == SEC_ENCODE)
    {
        if ((pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AES) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AES192) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AES256) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
        {
            if (Secv4AlgoProcessIpsec (pTemp,
                                       pTemp->pSaEntry->
                                       pu1SecAssocInitVector,
                                       SEC_ENCR) == SEC_FAILURE)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4TaskletSchedFn:-"
                           "Secv4AlgoProcessIpsec failed \n");
                Secv4HandleFailueAfterCryptoProcess (pTemp);
                return;
            }
        }

        if ((pTemp->pSaEntry->u1SecAssocAhAlgo == SEC_HMACMD5) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == SEC_HMACSHA1) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == SEC_XCBCMAC) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == HMAC_SHA_256) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == HMAC_SHA_384) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == HMAC_SHA_512))
        {
            if (Secv4AlgoProcessIpsec (pTemp, au1AuthDigest, SEC_AUTH) ==
                SEC_FAILURE)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4TaskletSchedFn:-"
                           "Secv4AlgoProcessIpsec failed \n");
                Secv4HandleFailueAfterCryptoProcess (pTemp);
                return;
            }

            /* Copy the Auth Digest in to the pkt */
            if (IPSEC_COPY_OVER_BUF (pTemp->pCBuf, au1AuthDigest,
                                     pTemp->u4IcvOffset,
                                     pTemp->pSaEntry->u1SecAhAlgoDigestSize) ==
                CRU_FAILURE)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4TaskletSchedFn:-"
                           "Copy AuthDigest to Buf failed \n");
                Secv4HandleFailueAfterCryptoProcess (pTemp);
                return;
            }

        }
        if (pTemp->u4Mode == ESP)
        {
            Secv4EspOutGoingPerformCB (pTemp);
        }
        else
        {
            Secv4AhOutGoingPerformCB (pTemp);
        }
    }
    else                        /* SEC_DECODE */
    {
        if ((pTemp->pSaEntry->u1SecAssocAhAlgo == SEC_HMACMD5) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == SEC_HMACSHA1) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == SEC_XCBCMAC) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == HMAC_SHA_256) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == HMAC_SHA_384) ||
            (pTemp->pSaEntry->u1SecAssocAhAlgo == HMAC_SHA_512))
        {
            /* Coping the existing Auth Digest to local buffer */
            if (IPSEC_COPY_FROM_BUF (pTemp->pCBuf, au1RcvdDigest,
                                     pTemp->u4IcvOffset,
                                     pTemp->pSaEntry->u1SecAhAlgoDigestSize) ==
                BUF_FAILURE)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4TaskletSchedFn:-"
                           "Copy RcvdDigest from Buf failed \n");
                Secv4HandleFailueAfterCryptoProcess (pTemp);
                return;
            }
            if (pTemp->pSaEntry->u1SecAssocProtocol == SEC_AH)
            {
                if (IPSEC_COPY_OVER_BUF (pTemp->pCBuf, au1AuthDigest,
                                         pTemp->u4IcvOffset,
                                         pTemp->pSaEntry->
                                         u1SecAhAlgoDigestSize) == BUF_FAILURE)
                {
                    SECv4_TRC (SECv4_CONTROL_PLANE,
                               "\nSecv4TaskletSchedFn:-"
                               "Copy over Buf failed \n");
                    Secv4HandleFailueAfterCryptoProcess (pTemp);
                    return;
                }
            }
            /*  Computing the Authentication value */
            if (Secv4AlgoProcessIpsec (pTemp, au1AuthDigest, SEC_AUTH) ==
                SEC_FAILURE)
            {
                SECv4_TRC (SECv4_ALL_FAILURE, "Secv4TaskletSchedFn:-"
                           "Auth Req returned Failure\n");
                Secv4HandleFailueAfterCryptoProcess (pTemp);
                return;
            }
            /* Comparing the computed auth digest value with the value 
             * which is inside the packet */
            if (IPSEC_MEMCMP (au1RcvdDigest, au1AuthDigest,
                              pTemp->pSaEntry->u1SecAhAlgoDigestSize) != 0)
            {
                SECv4_TRC (SECv4_ALL_FAILURE, "Secv4TaskletSchedFn:-"
                           "Auth Compare Fails\n");
                Secv4HandleFailueAfterCryptoProcess (pTemp);
                return;
            }

        }
        if ((pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AES) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AES192) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AES256) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR192) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_AESCTR256) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_DES_CBC) ||
            (pTemp->pSaEntry->u1SecAssocEspAlgo == SEC_3DES_CBC))
        {

            /* Decrypting the packet */
            if (Secv4AlgoProcessIpsec (pTemp,
                                       pTemp->pSaEntry->
                                       pu1SecAssocInitVector,
                                       SEC_DECR) == SEC_FAILURE)
            {
                SECv4_TRC (SECv4_ALL_FAILURE, "\nSecv4TaskletSchedFn:-"
                           "Decrypt Req returned Failure\n");
                Secv4HandleFailueAfterCryptoProcess (pTemp);
                return;
            }
        }
        if (pTemp->u4Mode == ESP)
        {
            Secv4EspIncomingPerformCB (pTemp);
        }
        else
        {
            Secv4AhIncomingPerformCB (pTemp);
        }
    }
    return;
}

/******************************************************************************/
/*  Function Name : Secv4AlgoProcessIpsec                                     */
/*  Description   : In sync scenario, this is a function call. In async       */
/*                : calls, the message wud have been posted and handled       */
/*                : in the other end by another function. In that case        */
/*                : this is the function which will rcv the message and       */
/*                : process                                                   */
/*  Input(s)      : pSecv4TData - Contains all the information needed to      */
/*                : invoke algorithm module calls/APIs.                       */
/*                : pu1AuthICVEncrIV - Contains Auth - ICV or Encr - IV       */
/*                : u1Flag - Decides whether to invoke algo for Encode or     */
/*                :  Decode or Authenticate                                   */
/*  Output(s)     : None                                                      */
/*  Return Values : Returns the success and failure                           */
/******************************************************************************/

INT1
Secv4AlgoProcessIpsec (tSecv4TaskletData * pSecv4TData,
                       UINT1 *pu1AuthICVEncrIV, UINT1 u1Flag)
{

    unArCryptoKey       ArCryptoKey;
    UINT4               u4DataSize;
    UINT1              *pu1DataPtr;
    BOOLEAN             bIsDataLinear = OSIX_FALSE;
    UINT1               au1AesCounterBlock[IPSEC_AES_CTR_COUNTER_BLOCK_LEN];
    UINT1               au1AesCounter[IPSEC_AES_CTR_COUNTER_LEN] =
        { 0x00, 0x00, 0x00, 0x01 };
    UINT1               au1ArEncryptBuf[IPSEC_AES_BLOCK_SIZE];
    UINT4               u4AesCtrNum = 0;

    tCRU_BUF_CHAIN_HEADER *pCBuf = NULL;

    IPSEC_MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
    IPSEC_MEMSET (au1AesCounterBlock, SEC_ZERO,
                  IPSEC_AES_CTR_COUNTER_BLOCK_LEN);
    IPSEC_MEMSET (au1ArEncryptBuf, SEC_ZERO, IPSEC_AES_CTR_COUNTER_LEN);

    pCBuf = pSecv4TData->pCBuf;
    u4DataSize = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4DataSize == 0)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4AlgoProcessIpsec:-"
                   "Received paket size zero \n");
        return SEC_FAILURE;
    }

    pu1DataPtr =
        CRU_BUF_Get_DataPtr_IfLinear (pSecv4TData->pCBuf, 0, u4DataSize);

    if (pu1DataPtr == NULL)
    {
        pu1DataPtr = gu1DataPtr;
        if (IPSEC_COPY_FROM_BUF (pCBuf, gu1DataPtr, 0, u4DataSize) ==
            BUF_FAILURE)
        {
            return SEC_FAILURE;
        }
    }
    else
    {
        bIsDataLinear = OSIX_TRUE;
    }

    switch (u1Flag)
    {
        case SEC_AUTH:
        {
            pu1DataPtr += pSecv4TData->u4AuthOffset;

            switch (pSecv4TData->pSaEntry->u1SecAssocAhAlgo)
            {
                case SEC_HMACMD5:
                    arHmac_MD5 (pu1DataPtr, (INT4) pSecv4TData->u4AuthBufSize,
                                pSecv4TData->pSaEntry->pu1SecAssocAhKey1,
                                pSecv4TData->pSaEntry->u1SecAssocAhKeyLength,
                                pu1AuthICVEncrIV);
                    break;
                case SEC_HMACSHA1:
                    arHmac_Sha1 (pSecv4TData->pSaEntry->pu1SecAssocAhKey1,
                                 pSecv4TData->pSaEntry->u1SecAssocAhKeyLength,
                                 pu1DataPtr, (INT4) pSecv4TData->u4AuthBufSize,
                                 pu1AuthICVEncrIV);
                    break;
                case SEC_MD5:
                    Md5Algo (pu1DataPtr, (INT4) pSecv4TData->u4AuthBufSize,
                             pu1AuthICVEncrIV);
                    break;
                case SEC_KEYEDMD5:
                    KeyedMd5 (pu1DataPtr, (INT4) pSecv4TData->u4AuthBufSize,
                              pSecv4TData->pSaEntry->pu1SecAssocAhKey1,
                              pSecv4TData->pSaEntry->u1SecAssocAhKeyLength,
                              pu1AuthICVEncrIV);
                    break;
                case SEC_XCBCMAC:
                    AesArXcbcMac96 (pSecv4TData->pSaEntry->pu1SecAssocAhKey1,
                                    (UINT4) pSecv4TData->pSaEntry->
                                    u1SecAssocAhKeyLength, pu1DataPtr,
                                    (INT4) pSecv4TData->u4AuthBufSize,
                                    pu1AuthICVEncrIV);
                    break;
                case HMAC_SHA_256:
                    arHmacSha2 (2, pu1DataPtr,
                                (INT4) pSecv4TData->u4AuthBufSize,
                                pSecv4TData->pSaEntry->pu1SecAssocAhKey1,
                                (INT4) pSecv4TData->pSaEntry->
                                u1SecAssocAhKeyLength, pu1AuthICVEncrIV);
                    break;
                case HMAC_SHA_384:
                    arHmacSha2 (3, pu1DataPtr,
                                (INT4) pSecv4TData->u4AuthBufSize,
                                pSecv4TData->pSaEntry->pu1SecAssocAhKey1,
                                (INT4) pSecv4TData->pSaEntry->
                                u1SecAssocAhKeyLength, pu1AuthICVEncrIV);
                    break;
                case HMAC_SHA_512:
                    arHmacSha2 (4, pu1DataPtr,
                                (INT4) pSecv4TData->u4AuthBufSize,
                                pSecv4TData->pSaEntry->pu1SecAssocAhKey1,
                                (INT4) pSecv4TData->pSaEntry->
                                u1SecAssocAhKeyLength, pu1AuthICVEncrIV);
                    break;

                case SEC_NULLAHALGO:
                    break;
                default:
                    SECv4_TRC (SECv4_DATA_PATH,
                               "Secv4AlgoProcessIpsec:Unknown Auth Algo\n");
                    return SEC_FAILURE;
            }
        }
            break;
        case SEC_DECR:
        {
            /* Update the DataPtr to the EncrOffset */
            pu1DataPtr += (pSecv4TData->u4EncrOffset);

            switch (pSecv4TData->pSaEntry->u1SecAssocEspAlgo)
            {
                case SEC_DES_CBC:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    IPSEC_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key,
                                  sizeof (pSecv4TData->pSaEntry->Key));

                    if (DesArCbcDecrypt
                        (pu1DataPtr, pSecv4TData->u4EncrBufSize,
                         &ArCryptoKey, pu1AuthICVEncrIV) == DES_FAILURE)
                    {
                        SECv4_TRC (SECv4_DATA_PATH, "DESDecrypt Fails\n");
                        return SEC_FAILURE;
                    }
                    break;

                case SEC_3DES_CBC:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key,
                                  sizeof (pSecv4TData->pSaEntry->Key));
                    IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key2,
                                  sizeof (pSecv4TData->pSaEntry->Key2));
                    IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key3,
                                  sizeof (pSecv4TData->pSaEntry->Key3));

                    if (TDesArCbcDecrypt
                        (pu1DataPtr, pSecv4TData->u4EncrBufSize,
                         &ArCryptoKey, pu1AuthICVEncrIV) == DES_FAILURE)
                    {
                        SECv4_TRC (SECv4_DATA_PATH, "3DESDecrypt Fails\n");
                        return SEC_FAILURE;
                    }

                    break;
                case SEC_AES:
                case SEC_AES192:
                case SEC_AES256:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    AesArCbcDecrypt (pu1DataPtr, pSecv4TData->u4EncrBufSize,
                                     (UINT2) ((pSecv4TData->pSaEntry->
                                               u1SecAssocEspKeyLength) * 8),
                                     pSecv4TData->pSaEntry->au1AesDecrKey,
                                     pu1AuthICVEncrIV);

                    break;
                case SEC_AESCTR:
                case SEC_AESCTR192:
                case SEC_AESCTR256:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    u4AesCtrNum = 0;
                    MEMCPY (au1AesCounterBlock, pSecv4TData->pSaEntry->au1Nonce,
                            IPSEC_AES_CTR_NONCE_LEN);
                    MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN,
                            pu1AuthICVEncrIV, IPSEC_AES_CTR_INIT_VECT_SIZE);
                    MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN +
                            IPSEC_AES_CTR_INIT_VECT_SIZE, au1AesCounter,
                            IPSEC_AES_CTR_COUNTER_LEN);
                    AesArSetEncryptKey (pSecv4TData->pSaEntry->au1AesDecrKey,
                                        (UINT2) ((pSecv4TData->pSaEntry->
                                                  u1SecAssocEspKeyLength) * 8),
                                        &ArCryptoKey);
                    AesArCtrMode (pu1DataPtr, pu1DataPtr,
                                  pSecv4TData->u4EncrBufSize, &ArCryptoKey,
                                  au1AesCounterBlock, au1ArEncryptBuf,
                                  &u4AesCtrNum);

                    break;
                default:
                    SECv4_TRC (SECv4_DATA_PATH,
                               "Secv4AlgoProcessIpsec: Unknown Encr Algo\n");
                    return SEC_FAILURE;
            }
        }
            break;
        case SEC_ENCR:
        default:
        {
            /* Update the DataPtr to the EncrOffset */
            pu1DataPtr += pSecv4TData->u4EncrOffset;
            switch (pSecv4TData->pSaEntry->u1SecAssocEspAlgo)
            {
                case SEC_DES_CBC:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    IPSEC_MEMCPY (ArCryptoKey.tArDes.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key,
                                  sizeof (pSecv4TData->pSaEntry->Key));

                    if (DesArCbcEncrypt
                        (pu1DataPtr, pSecv4TData->u4EncrBufSize,
                         &ArCryptoKey, pu1AuthICVEncrIV) == DES_FAILURE)
                    {
                        SECv4_TRC (SECv4_DATA_PATH,
                                   "Secv4AlgoProcessIpsec: DESEncrypt Fails\n");
                        return SEC_FAILURE;
                    }
                    break;
                case SEC_3DES_CBC:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key,
                                  sizeof (pSecv4TData->pSaEntry->Key));
                    IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key2,
                                  sizeof (pSecv4TData->pSaEntry->Key2));
                    IPSEC_MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey,
                                  pSecv4TData->pSaEntry->Key3,
                                  sizeof (pSecv4TData->pSaEntry->Key3));

                    if (TDesArCbcEncrypt
                        (pu1DataPtr, pSecv4TData->u4EncrBufSize,
                         &ArCryptoKey, pu1AuthICVEncrIV) == DES_FAILURE)
                    {
                        SECv4_TRC (SECv4_DATA_PATH,
                                   "Secv4AlgoProcessIpsec: 3DESEncrypt Fails\n");
                        return SEC_FAILURE;
                    }

                    break;
                case SEC_AES:
                case SEC_AES192:
                case SEC_AES256:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    AesArCbcEncrypt (pu1DataPtr, pSecv4TData->u4EncrBufSize,
                                     (UINT2) ((pSecv4TData->pSaEntry->
                                               u1SecAssocEspKeyLength) * 8),
                                     pSecv4TData->pSaEntry->au1AesEncrKey,
                                     pu1AuthICVEncrIV);

                    break;

                case SEC_AESCTR:
                case SEC_AESCTR192:
                case SEC_AESCTR256:

                    if (gu1Secv4BypassCrypto == BYPASS_ENABLED)
                        break;

                    u4AesCtrNum = 0;
                    MEMCPY (au1AesCounterBlock, pSecv4TData->pSaEntry->au1Nonce,
                            IPSEC_AES_CTR_NONCE_LEN);
                    MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN,
                            pu1AuthICVEncrIV, IPSEC_AES_CTR_INIT_VECT_SIZE);
                    MEMCPY (au1AesCounterBlock + IPSEC_AES_CTR_NONCE_LEN +
                            IPSEC_AES_CTR_INIT_VECT_SIZE, au1AesCounter,
                            IPSEC_AES_CTR_COUNTER_LEN);
                    AesArSetEncryptKey (pSecv4TData->pSaEntry->au1AesEncrKey,
                                        (UINT2) ((pSecv4TData->pSaEntry->
                                                  u1SecAssocEspKeyLength) * 8),
                                        &ArCryptoKey);
                    AesArCtrMode (pu1DataPtr, pu1DataPtr,
                                  pSecv4TData->u4EncrBufSize, &ArCryptoKey,
                                  au1AesCounterBlock, au1ArEncryptBuf,
                                  &u4AesCtrNum);

                    break;
                default:
                    SECv4_TRC (SECv4_DATA_PATH,
                               "Secv4AlgoProcessIpsec: Unknown Encr Algo\n");
                    return SEC_FAILURE;
            }
        }
            break;
    }
    if (bIsDataLinear == OSIX_FALSE)
    {
        IPSEC_COPY_OVER_BUF (pSecv4TData->pCBuf, gu1DataPtr, 0, u4DataSize);
    }
    return (SEC_SUCCESS);
}

#endif
