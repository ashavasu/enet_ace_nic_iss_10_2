/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4tmr.c,v 1.8 2014/01/02 10:25:37 siva Exp $
 *
 * Description: This has functions for Secv4 Timer Submodule
 *
 ***********************************************************************/

#include "secv4com.h"

/***********************************************************************/
/*  Function Name : Secv4InitTimer                                     */
/*  Description   : This function initializes the timer list.          */
/*  Parameter(s)  : VOID                                               */
/*  Return Values : VOID                                               */
/***********************************************************************/
VOID
Secv4InitTimer (VOID)
{
    UINT1               au1TaskName[10];

    IPSEC_STRCPY (au1TaskName, (CONST UINT1 *) CFA_TASK_NAME);

    if (TmrCreateTimerList
        (au1TaskName, (UINT4) IPSEC_TIMER_EXPIRY_EVENT, NULL,
         &gSecv4TimerListId) != TMR_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE, "Timer Init Failed\n");
    }
    return;
}

/**********************************************************************/
/*  Function Name : Secv4DeInitTimer                                  */
/*  Description   :                                                   */
/*                : This function de-initializes the timer list.      */
/*  Parameter(s)  : VOID                                              */
/*  Return Values : VOID                                              */
/**********************************************************************/

VOID
Secv4DeInitTimer (VOID)
{

    if (TmrDeleteTimerList (gSecv4TimerListId) != TMR_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE, "Timer DeInit Failed\n");
    }
    return;
}

/************************************************************************/
/*  Function Name : Secv4StartTimer                                     */
/*  Description   : This function  starts the timer for the given value.*/
/*  Parameter(s)  : pTimer - Pointer to the timer to be started         */
/*                : TimeOut -  Time out value                           */
/*  Return Values : VOID                                                */
/************************************************************************/
VOID
Secv4StartTimer (tSecv4Timer * pTimer, UINT1 u1TmrId, UINT4 u4TimeOut)
{

    if (SECv4_IS_TIMER_INACTIVE (pTimer) == FALSE)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Attemp to start an already running timer   \n");
        return;
    }
    pTimer->u1TimerId = u1TmrId;
    if (TmrStartTimer
        (gSecv4TimerListId, &pTimer->TimerNode,
         (SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4TimeOut)) != TMR_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE, " Start Timer Failed\n");
    }
    return;
}

/**********************************************************************/
/*  Function Name : Secv4StopTimer                                    */
/*  Description   :                                                   */
/*                : This procedure stops the timer 'pTimer' .         */
/*  Parameter(s)  :                                                   */
/*                : pTimer  -  pointer to the Timer block             */
/*  Return Values : VOID                                              */
/**********************************************************************/

VOID
Secv4StopTimer (tSecv4Timer * pTimer)
{

    if (SECv4_IS_TIMER_INACTIVE (pTimer) == FALSE)
    {
        TmrStopTimer (gSecv4TimerListId, &pTimer->TimerNode);
        IPSEC_MEMSET (pTimer, 0, sizeof (tSecv4Timer));
        SECv4_INIT_TIMER (pTimer);
    }
    return;
}

/*******************************************************************************/
/*  Function Name : Secv4ProcessTimeOut                                        */
/*  Description   :                                                            */
/*                : This procedure calls the appropriate procedures to handle  */
/*                : the timer expiry condition.                                */
/*  Parameter(s)  : None                                                       */
/*  Return Values : VOID                                                       */
/*******************************************************************************/
VOID
Secv4ProcessTimeOut (VOID)
{

    tTimer             *pListHead = NULL;
    tSecv4Timer        *pSecv4Tmr = NULL;
#ifdef IKE_WANTED
    tSecv4Policy       *pSecv4Policy = NULL;
#endif
    UINT1               u1TmrId = 0;

    pListHead = TmrGetNextExpiredTimer (gSecv4TimerListId);

    while (pListHead != NULL)
    {
        pSecv4Tmr = (tSecv4Timer *) pListHead;
        u1TmrId = pSecv4Tmr->u1TimerId;
        SECv4_INIT_TIMER ((tSecv4Timer *) pListHead);
        switch (u1TmrId)
        {

#ifdef IKE_WANTED
            case SECv4_SA_HARD_TIMER_ID:
                SECv4_TRC (SECv4_OS_RESOURCE, "SA Hard Life Timer Expired\n");
                Secv4Lock ();
                Secv4KernelLock ();
                /* OsixBHDisable is called to disable IRQ from taking over the context */
                OsixBHDisable ();
                pSecv4Policy = Secv4PolicyGetEntry (pSecv4Tmr->u4Param1);
                if (pSecv4Policy == NULL)
                {
                    OsixBHEnable ();
                    Secv4KernelUnLock ();
                    Secv4UnLock ();
                    return;
                }
                Secv4IntimateIkeDeletionOfSa (pSecv4Policy,
                                              pSecv4Tmr->u4SaIndex);
                OsixBHEnable ();
                Secv4KernelUnLock ();
                Secv4UnLock ();
                break;

            case SECv4_SA_SOFT_TIMER_ID:
                SECv4_TRC (SECv4_OS_RESOURCE, "SA Soft Life Timer Expired\n");
                Secv4Lock ();
                Secv4KernelLock ();
                /* OsixBHDisable is called to disable IRQ from taking over the context */
                OsixBHDisable ();
                Secv4RekeySa (pSecv4Tmr);
                OsixBHEnable ();
                Secv4KernelUnLock ();
                Secv4UnLock ();
                break;
#endif
            case SECv4_REASM_TIMER_ID:
                SECv4_TRC (SECv4_OS_RESOURCE, "Reassembly Timer Expired\n");
                Secv4HandleReAsmTimerExpiry (pSecv4Tmr);
                break;
            case SECv4_DUMMY_PKT_TIMER_ID:
                SECv4_TRC (SECv4_OS_RESOURCE, "Dummy Pkt gen Timer Expired\n");
                Secv4HandleDummyPktGenTmrExpiry ();
                break;
            default:
                SECv4_TRC (SECv4_OS_RESOURCE, "InValid Timer ID\n");
                break;
        }
        pListHead = TmrGetNextExpiredTimer (gSecv4TimerListId);
    }
    return;
}

#ifdef IKE_WANTED
/*****************************************************************************/
/*  Function Name : Secv4RekeySa                                             */
/*  Description   :                                                          */
/*                : This procedure is used for Rekey ing SA                  */
/*  Parameter(s)  : None                                                     */
/*  Return Values : VOID                                                     */
/*****************************************************************************/
VOID
Secv4RekeySa (tSecv4Timer * pTimer)
{

    tSecv4Selector     *pEntry = NULL;
    tSecv4Policy       *pPolicy = NULL;
    UINT1               u1Flag = SEC_NOT_FOUND;

    pPolicy = Secv4PolicyGetEntry (pTimer->u4Param1);

    if (pPolicy == NULL)
    {
        return;
    }

    TMO_SLL_Scan (&Secv4SelFormattedList, pEntry, tSecv4Selector *)
    {
        if (pEntry->pPolicyEntry == NULL)
        {
            continue;
        }
        if (pEntry->pPolicyEntry->u4PolicyIndex == pPolicy->u4PolicyIndex)
        {
            u1Flag = SEC_FOUND;
            break;
        }
    }
    if (u1Flag == SEC_NOT_FOUND)
    {
        return;
    }

    Secv4RequeStIkeForNewOrReKey (pEntry->u4IfIndex, pEntry->u4ProtoId,
                                  pEntry->u4Port, pEntry->u4SelAccessIndex,
                                  pEntry->u4PktDirection, SEC_RE_KEY, pPolicy);
    return;
}
#endif
