/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4vpn.c,v 1.9 2014/01/25 13:54:46 siva Exp $
 *
 * Description: This has functions for interacting with VPN module
 *
 ***********************************************************************/
#include "secv4com.h"
# include "fssnmp.h"
#include "secv4cli.h"
#include "vpncli.h"
#include "fssecv4lw.h"
#include "fsike.h"
#include "vpn.h"

/**************************************************************************/
INT1
VpnFillIpsecParams (tVpnPolicy * pVpnPolicy)
{
    UINT4               u4VpnPolicyType = 0;
    UINT4               u4LocalSubnet = 0;
    UINT4               u4RemoteSubnet = 0;

    IPV4_MASKLEN_TO_MASK (u4LocalSubnet,
                          pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen);
    IPV4_MASKLEN_TO_MASK (u4RemoteSubnet,
                          pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen);

    Secv4Lock ();
    /*Incremented as two, since all the policy will be 
       indexed in a odd number which corresponds to outbound 
       packet processing and the next even number corresponds
       to inbound packet processing */
    if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        if (VpnFillSecAssocParams (pVpnPolicy->u4VpnPolicyIndex,
                                   pVpnPolicy->LocalTunnTermAddr.uIpAddr.
                                   Ip4Addr,
                                   pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                   u4VpnAhInboundSpi, pVpnPolicy->u1VpnMode,
                                   VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy),
                                   pVpnPolicy->RemoteTunnTermAddr.uIpAddr.
                                   Ip4Addr) == SNMP_FAILURE)
        {
            Secv4UnLock ();
            return (SNMP_FAILURE);
        }

        if (VpnFillSecAssocParams (((pVpnPolicy->u4VpnPolicyIndex) + 1),
                                   pVpnPolicy->RemoteTunnTermAddr.uIpAddr.
                                   Ip4Addr,
                                   pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                   u4VpnAhOutboundSpi, pVpnPolicy->u1VpnMode,
                                   VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy),
                                   pVpnPolicy->LocalTunnTermAddr.uIpAddr.
                                   Ip4Addr) == SNMP_FAILURE)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            Secv4UnLock ();
            return (SNMP_FAILURE);
        }

        if (VpnFillAuthSecAssocParams (pVpnPolicy->u4VpnPolicyIndex,
                                       (UINT1) pVpnPolicy->
                                       u4VpnSecurityProtocol,
                                       pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                       u1VpnAuthAlgo,
                                       pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                       au1VpnAhKey,
                                       pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                       u1VpnEncryptionAlgo,
                                       pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                       au2VpnEspKey) == SNMP_FAILURE)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
            Secv4UnLock ();
            return (SNMP_FAILURE);
        }

        if (VpnFillAuthSecAssocParams
            ((UINT4) ((pVpnPolicy->u4VpnPolicyIndex) + 1),
             (UINT1) pVpnPolicy->u4VpnSecurityProtocol,
             pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnAuthAlgo,
             pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey,
             pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo,
             pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey) ==
            SNMP_FAILURE)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
            Secv4UnLock ();
            return (SNMP_FAILURE);
        }
    }

    if (VpnFillAccessParams (pVpnPolicy->u4VpnPolicyIndex,
                             pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.
                             Ip4Addr, u4LocalSubnet,
                             pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.
                             Ip4Addr, u4RemoteSubnet) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        Secv4UnLock ();
        return (SNMP_FAILURE);
    }

    if (Secv4SetAccessListInfo (pVpnPolicy->u4VpnPolicyIndex,
                                pVpnPolicy->LocalProtectNetwork.u2StartPort,
                                pVpnPolicy->LocalProtectNetwork.u2EndPort,
                                pVpnPolicy->RemoteProtectNetwork.u2StartPort,
                                pVpnPolicy->RemoteProtectNetwork.u2EndPort,
                                pVpnPolicy->u4VpnProtocol) == SEC_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        Secv4UnLock ();
        return (SNMP_FAILURE);
    }

    if (VpnFillAccessParams (((pVpnPolicy->u4VpnPolicyIndex) + 1),
                             pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.
                             Ip4Addr, u4RemoteSubnet,
                             pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.
                             Ip4Addr, u4LocalSubnet) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        Secv4UnLock ();
        return (SNMP_FAILURE);
    }
    if (Secv4SetAccessListInfo (((pVpnPolicy->u4VpnPolicyIndex) + 1),
                                pVpnPolicy->RemoteProtectNetwork.u2StartPort,
                                pVpnPolicy->RemoteProtectNetwork.u2EndPort,
                                pVpnPolicy->LocalProtectNetwork.u2StartPort,
                                pVpnPolicy->LocalProtectNetwork.u2EndPort,
                                pVpnPolicy->u4VpnProtocol) == SEC_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4)
                                     ((pVpnPolicy->u4VpnPolicyIndex) + 1),
                                     DESTROY);
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        Secv4UnLock ();
        return (SNMP_FAILURE);
    }

    /* setting the Mode as Manual when configuring Policy Params for XAUTH 
     * in CAS IPSec DB */
    if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_CERTIFICATE) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
    {
        u4VpnPolicyType = VPN_IKE_PRESHAREDKEY;
    }
    else
    {
        u4VpnPolicyType = pVpnPolicy->u4VpnPolicyType;
    }

    if (VpnFillPolicyParams (pVpnPolicy->u4VpnPolicyIndex,
                             (UINT1) pVpnPolicy->u4VpnPolicyFlag,
                             (UINT1) u4VpnPolicyType, pVpnPolicy->u1VpnIkeVer)
        == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }

        Secv4UnLock ();
        return (SNMP_FAILURE);
    }
    if (VpnFillPolicyParams ((UINT4) ((pVpnPolicy->u4VpnPolicyIndex) + 1),
                             (UINT1) pVpnPolicy->u4VpnPolicyFlag,
                             (UINT1) u4VpnPolicyType, pVpnPolicy->u1VpnIkeVer)
        == SNMP_FAILURE)
    {
        nmhSetFsipv4SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }

        Secv4UnLock ();
        return (SNMP_FAILURE);
    }

    if (VpnFillSelectorParams
        (pVpnPolicy->u4VpnPolicyIndex, pVpnPolicy->u4IfIndex,
         pVpnPolicy->u4VpnProtocol, VPN_INBOUND,
         (UINT1) pVpnPolicy->u4VpnPolicyFlag, pVpnPolicy->u4VpnPolicyIndex,
         (pVpnPolicy->u4VpnPolicyIndex + 1)) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecPolicyStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }

        Secv4UnLock ();
        return (SNMP_FAILURE);
    }

    if (VpnFillSelectorParams
        (pVpnPolicy->u4VpnPolicyIndex, pVpnPolicy->u4IfIndex,
         pVpnPolicy->u4VpnProtocol, VPN_OUTBOUND, pVpnPolicy->u4VpnPolicyFlag,
         ((pVpnPolicy->u4VpnPolicyIndex) + 1),
         pVpnPolicy->u4VpnPolicyIndex) == SNMP_FAILURE)
    {
        nmhSetFsipv4SelStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                               (INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                               VPN_DEFAULT_PORT, VPN_INBOUND, DESTROY);
        nmhSetFsipv4SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecPolicyStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv4SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }

        Secv4UnLock ();
        return (SNMP_FAILURE);
    }
    /* Create the Session in Hw Accelerator, One for Inbound */
    if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        if (Secv4InitSesInHwAccel (pVpnPolicy->u4VpnPolicyIndex, SEC_DECODE) ==
            SEC_FAILURE)
        {
            nmhSetFsipv4SelStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                   (INT4) pVpnPolicy->u4IfIndex,
                                   (INT4) pVpnPolicy->u4VpnProtocol,
                                   (INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                   VPN_DEFAULT_PORT, VPN_INBOUND, DESTROY);
            nmhSetFsipv4SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                         DESTROY);
            nmhSetFsipv4SecPolicyStatus ((INT4)
                                         (pVpnPolicy->u4VpnPolicyIndex + 1),
                                         DESTROY);
            nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                         DESTROY);
            nmhSetFsipv4SecAccessStatus ((INT4)
                                         (pVpnPolicy->u4VpnPolicyIndex + 1),
                                         DESTROY);
            if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
            {
                nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                            DESTROY);
                nmhSetFsipv4SecAssocStatus ((INT4)
                                            (pVpnPolicy->u4VpnPolicyIndex + 1),
                                            DESTROY);
            }

            Secv4UnLock ();
            return (SNMP_FAILURE);

        }
        /* Create the Session in Hw Accelerator, One for Outbound */
        if (Secv4InitSesInHwAccel
            ((pVpnPolicy->u4VpnPolicyIndex + 1), SEC_ENCODE) == SEC_FAILURE)
        {
            nmhSetFsipv4SelStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                   (INT4) pVpnPolicy->u4IfIndex,
                                   (INT4) pVpnPolicy->u4VpnProtocol,
                                   (INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                   VPN_DEFAULT_PORT, VPN_INBOUND, DESTROY);
            nmhSetFsipv4SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                         DESTROY);
            nmhSetFsipv4SecPolicyStatus ((INT4)
                                         (pVpnPolicy->u4VpnPolicyIndex + 1),
                                         DESTROY);
            nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                         DESTROY);
            nmhSetFsipv4SecAccessStatus ((INT4)
                                         (pVpnPolicy->u4VpnPolicyIndex + 1),
                                         DESTROY);
            if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
            {
                nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                            DESTROY);
                nmhSetFsipv4SecAssocStatus ((INT4)
                                            (pVpnPolicy->u4VpnPolicyIndex + 1),
                                            DESTROY);
            }
            Secv4UnLock ();
            return (SNMP_FAILURE);
        }
    }
    Secv4UnLock ();
    return SNMP_SUCCESS;
}

INT1
VpnDeleteIpsecParams (tVpnPolicy * pVpnPolicy)
{
    Secv4Lock ();

    if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        nmhSetFsipv4SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                    DESTROY);
        nmhSetFsipv4SecAssocStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                    DESTROY);
    }
    else
    {
#ifdef IKE_WANTED
        Secv4DeleteAssoc (pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.
                          Ip4Addr,
                          pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.
                          Ip4Addr, pVpnPolicy->u4IfIndex,
                          pVpnPolicy->u4VpnProtocol,
                          pVpnPolicy->LocalProtectNetwork.u2StartPort,
                          pVpnPolicy->RemoteProtectNetwork.u2StartPort);

        Secv4DeleteAssoc (pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.
                          Ip4Addr,
                          pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.
                          Ip4Addr, pVpnPolicy->u4IfIndex,
                          pVpnPolicy->u4VpnProtocol,
                          pVpnPolicy->LocalProtectNetwork.u2StartPort,
                          pVpnPolicy->RemoteProtectNetwork.u2StartPort);
#endif
    }

    if ((pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_CERTIFICATE) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_PRESHAREDKEY))
    {
        nmhSetFsipv4SelStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                               (INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                               VPN_DEFAULT_PORT, VPN_INBOUND, DESTROY);
        nmhSetFsipv4SelStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                               (INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) pVpnPolicy->u4VpnPolicyIndex,
                               VPN_DEFAULT_PORT, VPN_OUTBOUND, DESTROY);

        nmhSetFsipv4SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecPolicyStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);

        nmhSetFsipv4SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv4SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
    }

    Secv4UnLock ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
INT1
VpnFillSecAssocParams (UINT4 u4SecAssocIndex, UINT4 u4SecAssocDestAddr,
                       UINT4 u4SecAssocSpi, UINT1 u1SecAssocMode,
                       UINT4 u4AntiReplayStatus, UINT4 u4SecAssocSrcAddr)
{
    UINT4               u4SnmpErrorStatus;
    tSecv4Assoc        *pSecAssocIf = NULL;

    if (nmhTestv2Fsipv4SecAssocStatus
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv4SecAssocStatus ((INT4) u4SecAssocIndex, CREATE_AND_GO) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pSecAssocIf = Secv4AssocGetEntry (u4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        nmhSetFsipv4SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    pSecAssocIf->u4SecAssocSrcAddr = u4SecAssocSrcAddr;

    if (nmhTestv2Fsipv4SecAssocDstAddr
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         u4SecAssocDestAddr) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecAssocDstAddr ((INT4) u4SecAssocIndex, u4SecAssocDestAddr);

    if (nmhTestv2Fsipv4SecAssocSpi (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
                                    (INT4) u4SecAssocSpi) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecAssocSpi ((INT4) u4SecAssocIndex, (INT4) u4SecAssocSpi);

    if (nmhTestv2Fsipv4SecAssocMode (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
                                     (INT4) u1SecAssocMode) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecAssocMode ((INT4) u4SecAssocIndex, (INT4) u1SecAssocMode);

    if (nmhTestv2Fsipv4SecAssocAntiReplay
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         (INT4) u4AntiReplayStatus) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecAssocAntiReplay ((INT4) u4SecAssocIndex,
                                    (INT4) u4AntiReplayStatus);

    return SNMP_SUCCESS;
}

/***************************************************************************/
INT1
VpnFillAuthSecAssocParams (UINT4 u4SecAssocIndex, UINT1 u1SecAssocProtocol,
                           UINT1 u1SecAssocAhAlgo, UINT1 *au1SecAssocAhKey,
                           UINT1 u1SecAssocEspAlgo, UINT1 *au1SecAssocEspKey)
{

    UINT4               u4SnmpErrorStatus;

    if (nmhTestv2Fsipv4SecAssocProtocol
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         (INT4) u1SecAssocProtocol) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* If the SA entry already has ESP and if it is being set AH then 
     * make Encryption algorithm for the entry as 0 */
    nmhSetFsipv4SecAssocProtocol ((INT4) u4SecAssocIndex,
                                  (INT4) u1SecAssocProtocol);

    if (u1SecAssocAhAlgo != 0)
    {
        if (VpnFillAuthParams (u4SecAssocIndex, u1SecAssocProtocol,
                               u1SecAssocAhAlgo,
                               au1SecAssocAhKey) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    if (u1SecAssocEspAlgo != 0)    /* Fill if encryption params specified */
    {
        if (VpnFillEncrParams (u4SecAssocIndex, u1SecAssocProtocol,
                               u1SecAssocEspAlgo,
                               au1SecAssocEspKey) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************/
INT4
VpnFillEncrParams (UINT4 u4SecAssocIndex, UINT1 u1SecAssocProtocol,
                   UINT1 u1SecAssocEspAlgo, UINT1 *au1EspKey)
{

    UINT4               i = 0;
    UINT4               j = 0;
    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OutKey[SEC_ESP_AES_KEY3_LEN + 1];
    UINT1               u1Len = 0;
    UINT1               au1SecAssocEspKey[IPSEC_ESP_KEY_LEN];
    UINT1               au1SecAssocEspKey2[68];
    UINT1               au1SecAssocEspKey3[68];

    if (u1SecAssocProtocol != SEC_ESP)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1SecAssocEspKey, 0, IPSEC_ESP_KEY_LEN);
    MEMSET (au1SecAssocEspKey2, 0, 68);
    MEMSET (au1SecAssocEspKey3, 0, 68);

    /* Store the incoming ESP key into 3 seperate arrays of 16 each as the IPSEC
     * low level takes the input in such a way. */
    if (u1SecAssocEspAlgo == VPN_3DES_CBC)
    {

        for (i = 0; i < 16; i++)
        {
            au1SecAssocEspKey[i] = au1EspKey[i];

        }

        i = ESP_START_SECOND_KEY;
        au1SecAssocEspKey[ESP_FIRST_KEY_TERMINATION] = '\0';

        for (j = 0; j < 16; j++)
        {
            au1SecAssocEspKey2[j] = au1EspKey[i];
            i++;
        }

        au1SecAssocEspKey2[ESP_SECOND_KEY_TERMINATION] = '\0';
        i = ESP_START_THIRD_KEY;

        for (j = 0; j < 16; j++)
        {
            au1SecAssocEspKey3[j] = au1EspKey[i];
            i++;
        }
        j++;
        au1SecAssocEspKey3[j] = '\0';

    }
    if (u1SecAssocEspAlgo != VPN_3DES_CBC)
    {
        if (STRLEN (au1EspKey) >= IPSEC_ESP_KEY_LEN)
        {
            return SNMP_FAILURE;
        }
        STRNCPY (au1SecAssocEspKey, au1EspKey, IPSEC_ESP_KEY_LEN - 1);
    }

    IPSEC_MEMSET (au1OutKey, 0, (SEC_ESP_AES_KEY3_LEN + 1));

    nmhSetFsipv4SecAssocProtocol ((INT4) u4SecAssocIndex,
                                  (INT4) u1SecAssocProtocol);

    if (nmhTestv2Fsipv4SecAssocEspAlgo
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         (INT4) u1SecAssocEspAlgo) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecAssocEspAlgo ((INT4) u4SecAssocIndex,
                                 (INT4) u1SecAssocEspAlgo);

    if (u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        OctetStr.i4_Length = (INT4) STRLEN (au1SecAssocEspKey);
        OctetStr.pu1_OctetList = au1SecAssocEspKey;

        if (nmhTestv2Fsipv4SecAssocEspKey
            (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
             &OctetStr) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsipv4SecAssocEspKey ((INT4) u4SecAssocIndex, &OctetStr) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (u1SecAssocEspAlgo == SEC_3DES_CBC)
        {
            OctetStr.i4_Length = (INT4) STRLEN (au1SecAssocEspKey2);
            OctetStr.pu1_OctetList = au1SecAssocEspKey2;

            if (nmhTestv2Fsipv4SecAssocEspKey2 (&u4SnmpErrorStatus,
                                                (INT4) u4SecAssocIndex,
                                                &OctetStr) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (nmhSetFsipv4SecAssocEspKey2 ((INT4) u4SecAssocIndex, &OctetStr)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            u1Len = (UINT1) STRLEN (au1SecAssocEspKey3);
            OctetStr.i4_Length = (INT4) STRLEN (au1SecAssocEspKey3);
            OctetStr.pu1_OctetList = au1SecAssocEspKey3;
            if (nmhTestv2Fsipv4SecAssocEspKey3 (&u4SnmpErrorStatus,
                                                (INT4) u4SecAssocIndex,
                                                &OctetStr) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (nmhSetFsipv4SecAssocEspKey3 ((INT4) u4SecAssocIndex, &OctetStr)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
    }

    UNUSED_PARAM (u1Len);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
INT1
VpnFillAuthParams (UINT4 u4SecAssocIndex, UINT1 u1SecAssocProtocol,
                   UINT1 u1SecAssocAhAlgo, UINT1 *au1SecAssocAhKey)
{

    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    if (u1SecAssocProtocol == VPN_AH)
    {
        if (nmhTestv2Fsipv4SecAssocAhAlgo
            (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
             (INT4) u1SecAssocAhAlgo) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        nmhSetFsipv4SecAssocAhAlgo ((INT4) u4SecAssocIndex,
                                    (INT4) u1SecAssocAhAlgo);

        if ((u1SecAssocAhAlgo != VPN_MD5)
            && (u1SecAssocAhAlgo != VPN_NULLAHALGO))
        {
            OctetStr.i4_Length = (INT4) STRLEN (au1SecAssocAhKey);
            OctetStr.pu1_OctetList = au1SecAssocAhKey;

            if (nmhTestv2Fsipv4SecAssocAhKey
                (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
                 &OctetStr) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (nmhSetFsipv4SecAssocAhKey ((INT4) u4SecAssocIndex, &OctetStr)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
    }
    else if ((u1SecAssocProtocol == VPN_ESP)
             && (u1SecAssocAhAlgo != VPN_NULLAHALGO))
    {
        if (nmhTestv2Fsipv4SecAssocAhAlgo (&u4SnmpErrorStatus,
                                           (INT4) u4SecAssocIndex,
                                           (INT4) u1SecAssocAhAlgo) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsipv4SecAssocAhAlgo
            ((INT4) u4SecAssocIndex, (INT4) u1SecAssocAhAlgo) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (u1SecAssocAhAlgo != VPN_MD5)
        {
            OctetStr.i4_Length = (INT4) STRLEN (au1SecAssocAhKey);
            OctetStr.pu1_OctetList = au1SecAssocAhKey;
            if (nmhTestv2Fsipv4SecAssocAhKey (&u4SnmpErrorStatus,
                                              (INT4) u4SecAssocIndex,
                                              &OctetStr) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (nmhSetFsipv4SecAssocAhKey ((INT4) u4SecAssocIndex, &OctetStr) ==
                SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
INT1
VpnFillAccessParams (UINT4 u4AccessIndex, UINT4 u4SrcAddress,
                     UINT4 u4SrcMask, UINT4 u4DestAddress, UINT4 u4DestMask)
{
    UINT4               u4SnmpErrorStatus;

    if (nmhTestv2Fsipv4SecAccessStatus
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv4SecAccessStatus ((INT4) u4AccessIndex, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhTestv2Fsipv4SecSrcNet
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         u4SrcAddress) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecSrcNet ((INT4) u4AccessIndex, u4SrcAddress);

    if (nmhTestv2Fsipv4SecSrcMask
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex, u4SrcMask) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecSrcMask ((INT4) u4AccessIndex, u4SrcMask);

    if (nmhTestv2Fsipv4SecDestNet
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         u4DestAddress) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecDestNet ((INT4) u4AccessIndex, u4DestAddress);

    if (nmhTestv2Fsipv4SecDestMask
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex, u4DestMask) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    if (nmhSetFsipv4SecDestMask ((INT4) u4AccessIndex, u4DestMask) ==
        SNMP_FAILURE)
    {
        nmhSetFsipv4SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
INT1
VpnFillPolicyParams (UINT4 u4PolicyIndex, UINT1 u1PolicyFlag,
                     UINT1 u1PolicyMode, UINT1 u1IkeVersion)
{
    INT4                i4Len = 0;
    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1PolicySaBundle[VPN_SYS_MAX_NUM_TUNNELS];

    IPSEC_MEMSET (au1PolicySaBundle, 0, VPN_SYS_MAX_NUM_TUNNELS);
    i4Len = SPRINTF ((CHR1 *) au1PolicySaBundle, "%u", u4PolicyIndex);
    au1PolicySaBundle[i4Len] = '\0';

    if (nmhTestv2Fsipv4SecPolicyStatus (&u4SnmpErrorStatus,
                                        (INT4) u4PolicyIndex,
                                        CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhSetFsipv4SecPolicyStatus ((INT4) u4PolicyIndex,
                                     CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (u1PolicyFlag == SEC_FILTER || u1PolicyFlag == SEC_ALLOW)
    {
        u1PolicyFlag = SEC_BYPASS;
    }

    if (nmhTestv2Fsipv4SecPolicyFlag (&u4SnmpErrorStatus,
                                      (INT4) u4PolicyIndex,
                                      (INT4) u1PolicyFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv4SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecPolicyFlag ((INT4) u4PolicyIndex, (INT4) u1PolicyFlag);

    if (nmhTestv2Fsipv4SecPolicyMode (&u4SnmpErrorStatus,
                                      (INT4) u4PolicyIndex, (INT4) u1PolicyMode)
        == SNMP_FAILURE)
    {
        nmhSetFsipv4SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SecPolicyMode ((INT4) u4PolicyIndex, (INT4) u1PolicyMode);

    if (u1PolicyMode == VPN_MANUAL)
    {
        OctetStr.pu1_OctetList = au1PolicySaBundle;
        OctetStr.i4_Length = i4Len;
        if (nmhSetFsipv4SecPolicySaBundle ((INT4) u4PolicyIndex, &OctetStr) ==
            SNMP_FAILURE)
        {
            nmhSetFsipv4SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
            return SNMP_FAILURE;
        }
    }

    if (Secv4SetIkeVersion (u4PolicyIndex, u1IkeVersion) == SEC_FAILURE)
    {
        nmhSetFsipv4SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
INT1
VpnFillSelectorParams (UINT4 u4VpnPolicyIndex, UINT4 u4IfIndex, UINT4 u4ProtoId,
                       UINT4 u4PktDirection, UINT4 u1SelFilterFlag,
                       UINT4 u4VpnPolicyEntry, UINT4 u4AccessIndex)
{

    UINT4               u4SnmpErrorStatus;
    UINT4               u4Port = VPN_DEFAULT_PORT;

    if (nmhTestv2Fsipv4SelStatus
        (&u4SnmpErrorStatus, (INT4) u4VpnPolicyIndex, (INT4) u4IfIndex,
         (INT4) u4ProtoId, (INT4) u4VpnPolicyIndex, (INT4) u4Port,
         (INT4) u4PktDirection, CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv4SelStatus
        ((INT4) u4VpnPolicyIndex, (INT4) u4IfIndex, (INT4) u4ProtoId,
         (INT4) u4AccessIndex, (INT4) u4Port, (INT4) u4PktDirection,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (u1SelFilterFlag == SEC_APPLY || u1SelFilterFlag == SEC_BYPASS)
    {
        u1SelFilterFlag = SEC_ALLOW;
    }

    if (nmhTestv2Fsipv4SelFilterFlag
        (&u4SnmpErrorStatus, (INT4) u4VpnPolicyIndex, (INT4) u4IfIndex,
         (INT4) u4ProtoId, (INT4) u4VpnPolicyIndex, (INT4) u4Port,
         (INT4) u4PktDirection, (INT4) u1SelFilterFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv4SelStatus ((INT4) u4VpnPolicyIndex, (INT4) u4IfIndex,
                               (INT4) u4ProtoId, (INT4) u4AccessIndex,
                               (INT4) u4Port, (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv4SelFilterFlag
        ((INT4) u4VpnPolicyIndex, (INT4) u4IfIndex, (INT4) u4ProtoId,
         (INT4) u4VpnPolicyIndex, (INT4) u4Port, (INT4) u4PktDirection,
         (INT4) u1SelFilterFlag);

    if (nmhTestv2Fsipv4SelPolicyIndex
        (&u4SnmpErrorStatus, (INT4) u4VpnPolicyIndex, (INT4) u4IfIndex,
         (INT4) u4ProtoId, (INT4) u4VpnPolicyIndex, (INT4) u4Port,
         (INT4) u4PktDirection, (INT4) u4VpnPolicyEntry) == SNMP_FAILURE)
    {
        nmhSetFsipv4SelStatus ((INT4) u4VpnPolicyIndex, (INT4) u4IfIndex,
                               (INT4) u4ProtoId, (INT4) u4AccessIndex,
                               (INT4) u4Port, (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv4SelPolicyIndex
        ((INT4) u4VpnPolicyIndex, (INT4) u4IfIndex, (INT4) u4ProtoId,
         (INT4) u4VpnPolicyIndex, (INT4) u4Port, (INT4) u4PktDirection,
         (INT4) u4VpnPolicyEntry) == SNMP_FAILURE)
    {
        nmhSetFsipv4SelStatus ((INT4) u4VpnPolicyIndex, (INT4) u4IfIndex,
                               (INT4) u4ProtoId, (INT4) u4AccessIndex,
                               (INT4) u4Port, (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }

    /* DF Bit Always cleared for the corresponding interface */
    nmhSetFsipv4SecDFragBitStatus ((INT4) u4IfIndex, SEC_DFBIT_CLEAR);

    return SNMP_SUCCESS;
}

/* EOF */
