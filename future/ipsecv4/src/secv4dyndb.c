/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4dyndb.c,v 1.5 2013/10/24 09:56:37 siva Exp $
 *
 * Description: This file has functions to create dynamic ipsec
 *              database (SA, Policy, Access list and selector)
 *              It also has functions to delete the entry created
 *              dynamically
 *
 ***********************************************************************/
#include "secv4com.h"
#include "secv4ike.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fssecv4lw.h"
#include "secv4dyndb.h"

/* MSAD  ADD -S- */
/***********************************************************************/
/*  Function Name : Secv4ProcessIkeInstallDynamicIpsecDBMsg            */
/*  Description   : This function installs the SA rcvd from IKE        */
/*                  It also installs a policy, access list and         */
/*                  selector for the new vpn client interface          */
/*                                                                     */
/*  Input(s)      : pMsg - Pointer to the Msg Rcvd From IKE            */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/

INT1
Secv4ProcessIkeInstallDynamicIpsecDBMsg (tIkeIPSecQMsg * pMsg)
{
    tIPSecBundle       *pInstallSAMsg = NULL;
    tIkeIpv4Subnet      LocalSubnet;
    tIkeIpv4Subnet      RemoteSubnet;
    tSecv4Selector     *pSelEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4AccessIndex = SEC_FAILURE;
    UINT4               u4PolicyIndex = SEC_FAILURE;
    UINT1               au1SaBundle[SEC_MAX_SA_BUNDLE_LEN];
    UINT4               u4SaIndex0 = 0;
    UINT4               u4SaIndex1 = 0;
    UINT4               u4LocalNetwork = 0;
    UINT4               u4LocalMask = 0;
    UINT4               u4RemoteNetwork = 0;
    UINT4               u4RemoteMask = 0;
    UINT4               u4InterfaceInIndex = 0;
    UINT4               u4InterfaceOutIndex = 0;
    UINT4               u4HLProtocol = 0;
    UINT4               u4AccIndex = 0;

    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPSEC_MEMSET (&au1SaBundle, 0, sizeof (au1SaBundle));

    pInstallSAMsg = &(pMsg->IkeIfParam.InstallSA);

    Secv4ConvertIkeInstallSAMsgToHostOrder (pInstallSAMsg);

    /* Based on the Network ID type, get the local and remote network */
    if (pInstallSAMsg->LocalProxy.u4Type == IKE_IPSEC_ID_IPV4_ADDR)
    {
        u4LocalNetwork = pInstallSAMsg->LocalProxy.uNetwork.Ip4Addr;
        u4LocalMask = 0xffffffff;
    }
    else if (pInstallSAMsg->LocalProxy.u4Type == IKE_IPSEC_ID_IPV4_SUBNET)
    {
        LocalSubnet = pInstallSAMsg->LocalProxy.uNetwork.Ip4Subnet;
        u4LocalNetwork = LocalSubnet.Ip4Addr & LocalSubnet.Ip4SubnetMask;
        u4LocalMask = LocalSubnet.Ip4SubnetMask;
    }
    else
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Unsupported Local Proxy Network Type\n");
        return (SEC_FAILURE);
    }

    if (pInstallSAMsg->RemoteProxy.u4Type == IKE_IPSEC_ID_IPV4_ADDR)
    {
        u4RemoteNetwork = pInstallSAMsg->RemoteProxy.uNetwork.Ip4Addr;
        u4RemoteMask = 0xffffffff;
    }
    else if (pInstallSAMsg->RemoteProxy.u4Type == IKE_IPSEC_ID_IPV4_SUBNET)
    {
        RemoteSubnet = pInstallSAMsg->RemoteProxy.uNetwork.Ip4Subnet;
        u4RemoteNetwork = RemoteSubnet.Ip4Addr & RemoteSubnet.Ip4SubnetMask;
        u4RemoteMask = RemoteSubnet.Ip4SubnetMask;
    }
    else
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Unsupported Remote Proxy Network Type\n");
        return (SEC_FAILURE);
    }

    if (pInstallSAMsg->bVpnServer == TRUE)
    {
        /* Need to get the CfaIfIndex of the port on which the Policy 
           will be applied */
        if (SecUtilIpIfGetIfIndexFromIpAddress
            (pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
             &u4InterfaceInIndex) == OSIX_FAILURE)
        {
            return (SEC_FAILURE);
        }

        u4InterfaceOutIndex = u4InterfaceInIndex;
    }
    else
    {
        u4InterfaceOutIndex = pInstallSAMsg->u4VpnIfIndex;
        if (SecUtilIpIfGetIfIndexFromIpAddress
            (pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
             &u4InterfaceInIndex) == OSIX_FAILURE)
        {
            return (SEC_FAILURE);
        }

    }
    /* If any IPSEC SA is there for the same crypto map , stop 
       the soft lifetimer to avoid rekey for the same SA */
    u4AccIndex = Secv4GetAccessIndexByRange (u4RemoteNetwork, u4LocalNetwork);

    pSelEntry =
        Secv4SelGetFormattedEntry (u4InterfaceInIndex,
                                   SEC_ANY_PROTOCOL, u4AccIndex,
                                   pInstallSAMsg->aInSABundle[0].
                                   u2HLPortNumber, SEC_INBOUND);
    if ((pSelEntry != NULL)
        && (pSelEntry->pPolicyEntry->pau1SaEntry[0] != NULL))
    {
        Secv4StopTimer (&pSelEntry->pPolicyEntry->pau1SaEntry[0]->
                        Secv4AssocSoftTimer);
    }
    u4AccIndex = Secv4GetAccessIndexByRange (u4LocalNetwork, u4RemoteNetwork);

    pSelEntry =
        Secv4SelGetFormattedEntry (u4InterfaceOutIndex,
                                   SEC_ANY_PROTOCOL, u4AccIndex,
                                   pInstallSAMsg->aOutSABundle[0].
                                   u2HLPortNumber, SEC_OUTBOUND);
    if ((pSelEntry != NULL) && pSelEntry->pPolicyEntry->pau1SaEntry[0] != NULL)
    {
        Secv4StopTimer (&pSelEntry->pPolicyEntry->pau1SaEntry[0]->
                        Secv4AssocSoftTimer);
    }

    /*                                                      */
    /* I N  B O U N D    D A T A  B A S E   U P D A T I O N */
    /*                                                      */

    /* Install Inbound SA bundle */
    if (pInstallSAMsg->aInSABundle[0].u4Spi != 0)
    {
        u4SaIndex0 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke
            (&pInstallSAMsg->aInSABundle[0],
             pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
             pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
             u4SaIndex0, pInstallSAMsg->aOutSABundle[0].u4Spi,
             pInstallSAMsg->u4VpnIfIndex) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                       "Secv4InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }

        if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
        {
            u4SaIndex1 = Secv4GetFreeSAIdx ();
            if (Secv4InstallSAFromIke
                (&pInstallSAMsg->aInSABundle[1],
                 pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                 pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                 u4SaIndex1, pInstallSAMsg->aOutSABundle[1].u4Spi,
                 pInstallSAMsg->u4VpnIfIndex) != SEC_SUCCESS)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE,
                           "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                           "Secv4InstallSAFromIke Failed\n");
                return (SEC_FAILURE);
            }
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld.%ld", u4SaIndex0, u4SaIndex1);
        }
        else
        {
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld", u4SaIndex0);
        }
    }
    else if (pInstallSAMsg->aInSABundle[1].u4Spi != 0)
    {
        u4SaIndex1 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke
            (&pInstallSAMsg->aInSABundle[1],
             pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
             pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
             u4SaIndex1, pInstallSAMsg->aOutSABundle[1].u4Spi,
             pInstallSAMsg->u4VpnIfIndex) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                       "Secv4InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%ld",
                        u4SaIndex1);
    }

    /* Install a new Policy to apply ipsec for the 
     * inbound sa bundle */
    u4PolicyIndex = Secv4GetFreePolicyIdx ();
    if (Secv4InstallSecPolicy (u4PolicyIndex, au1SaBundle,
                               pInstallSAMsg->u1IkeVersion) != SEC_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Inbound Secv4InstallSecPolicy Failed\n");
        return (SEC_FAILURE);
    }

    /* Install a new inbound access list as follows:
     *
     * Source ip/mask: VPN client IP/255.255.255.255
     * 
     * Destination ip/mask : Network/Subnet Mask
     */

    /* AccessIndex will be same as Policy Index */
    u4AccessIndex = u4PolicyIndex;
    if (Secv4InstallSecAccess (u4AccessIndex,
                               u4RemoteNetwork,
                               u4RemoteMask,
                               u4LocalNetwork, u4LocalMask) != SEC_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Inbound Secv4InstallSecAccess Failed\n");
        return (SEC_FAILURE);
    }

    if (pInstallSAMsg->LocalProxy.u1HLProtocol == 0)
    {
        u4HLProtocol = SEC_ANY_PROTOCOL;
    }
    else
    {
        u4HLProtocol = (UINT1) pInstallSAMsg->LocalProxy.u1HLProtocol;
    }
    if (pInstallSAMsg->LocalProxy.u2EndPort == 0)
    {
        pInstallSAMsg->LocalProxy.u2EndPort = SEC_MAX_PORT;
    }
    if (pInstallSAMsg->RemoteProxy.u2EndPort == 0)
    {
        pInstallSAMsg->RemoteProxy.u2EndPort = SEC_MAX_PORT;
    }

    Secv4SetAccessListInfo (u4AccessIndex,
                            pInstallSAMsg->LocalProxy.u2StartPort,
                            pInstallSAMsg->LocalProxy.u2EndPort,
                            pInstallSAMsg->RemoteProxy.u2StartPort,
                            pInstallSAMsg->RemoteProxy.u2EndPort, u4HLProtocol);

    /* Creates inbound selector entry */
    if (Secv4InstallSecSelector (u4InterfaceInIndex,
                                 SEC_INBOUND,
                                 u4AccessIndex, u4PolicyIndex) != SEC_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Inbound Secv4InstallSecSelector Failed\n");
        return (SEC_FAILURE);
    }
    /* Initialize the session for the Sec Assoc in Hw Accelerator for inbound 
       direction */
    if (Secv4InitSesInHwAccel
        ((u4SaIndex0 ? u4SaIndex0 : u4SaIndex1), SEC_DECODE) == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg : Unable To "
                   "Initialize  InBound Session  in Hw Accel \n");
        return SEC_FAILURE;
    }

    /*                                                      */
    /*  O U T   B O U N D  D A T A   B A S E  U P D A T I O N */
    /*                                                      */

    u4SaIndex0 = 0;
    u4SaIndex1 = 0;

    IPSEC_MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    IPSEC_MEMSET (&au1SaBundle, 0, sizeof (au1SaBundle));

    /* Install outbound SA bundle */
    if (pInstallSAMsg->aOutSABundle[0].u4Spi != 0)
    {

        u4SaIndex0 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke (&pInstallSAMsg->aOutSABundle[0],
                                   pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                                   pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                                   u4SaIndex0, pInstallSAMsg->aInSABundle[0].
                                   u4Spi,
                                   pInstallSAMsg->u4VpnIfIndex) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                       "Secv4InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
        {

            u4SaIndex1 = Secv4GetFreeSAIdx ();
            if (Secv4InstallSAFromIke (&pInstallSAMsg->aOutSABundle[1],
                                       pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                                       pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                                       u4SaIndex1, pInstallSAMsg->
                                       aInSABundle[1].u4Spi,
                                       pInstallSAMsg->u4VpnIfIndex) !=
                SEC_SUCCESS)
            {
                SECv4_TRC (SECv4_CONTROL_PLANE,
                           "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                           "Secv4InstallSAFromIke Failed\n");
                return (SEC_FAILURE);
            }
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld.%ld", u4SaIndex0, u4SaIndex1);
        }
        else
        {
            SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN,
                            "%ld", u4SaIndex0);
        }
    }
    else if (pInstallSAMsg->aOutSABundle[1].u4Spi != 0)
    {

        u4SaIndex1 = Secv4GetFreeSAIdx ();
        if (Secv4InstallSAFromIke (&pInstallSAMsg->aOutSABundle[1],
                                   pInstallSAMsg->RemTunnTermAddr.Ipv4Addr,
                                   pInstallSAMsg->LocTunnTermAddr.Ipv4Addr,
                                   u4SaIndex1, pInstallSAMsg->aInSABundle[1].
                                   u4Spi,
                                   pInstallSAMsg->u4VpnIfIndex) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                       "Secv4InstallSAFromIke Failed\n");
            return (SEC_FAILURE);
        }
        SECv4_SNPRINTF ((CHR1 *) au1SaBundle, SEC_MAX_SA_BUNDLE_LEN, "%ld",
                        u4SaIndex1);
    }

    /* Install a new Policy to apply ipsec for the 
     * outbound sa bundle */
    u4PolicyIndex = Secv4GetFreePolicyIdx ();
    if (Secv4InstallSecPolicy (u4PolicyIndex, au1SaBundle,
                               pInstallSAMsg->u1IkeVersion) != SEC_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Outbound Secv4InstallSecPolicy Failed\n");
        return (SEC_FAILURE);
    }

    /* Install outbound access list as follows:
     *
     * Source ip/mask: ANY/ANY
     * 
     * Destination ip/mask : VPN client IP/255.255.255.255
     */

    /* AccessIndex will be same as Policy Index */
    u4AccessIndex = u4PolicyIndex;
    if (Secv4InstallSecAccess (u4AccessIndex,
                               u4LocalNetwork,
                               u4LocalMask,
                               u4RemoteNetwork, u4RemoteMask) != SEC_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Secv4InstallSecAccess Failed\n");
        return (SEC_FAILURE);
    }

    if (pInstallSAMsg->LocalProxy.u1HLProtocol == 0)
    {
        u4HLProtocol = SEC_ANY_PROTOCOL;
    }
    else
    {
        u4HLProtocol = (UINT4) pInstallSAMsg->LocalProxy.u1HLProtocol;
    }

    Secv4SetAccessListInfo (u4AccessIndex,
                            pInstallSAMsg->LocalProxy.u2StartPort,
                            pInstallSAMsg->LocalProxy.u2EndPort,
                            pInstallSAMsg->RemoteProxy.u2StartPort,
                            pInstallSAMsg->RemoteProxy.u2EndPort, u4HLProtocol);

    /* Creates outbound selector entry */
    if (Secv4InstallSecSelector (u4InterfaceOutIndex,
                                 SEC_OUTBOUND,
                                 u4AccessIndex, u4PolicyIndex) != SEC_SUCCESS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Outbound Secv4InstallSecSelector Failed\n");

        return (SEC_FAILURE);
    }

    /* Initialize the session for the Sec Assoc in Hw Accelerator for inbound 
       direction */
    if (Secv4InitSesInHwAccel
        ((u4SaIndex0 ? u4SaIndex0 : u4SaIndex1), SEC_ENCODE) == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg : Unable To "
                   "Initialize  OutBound Session  in Hw Accel \n");
        return SEC_FAILURE;
    }
    /* Format the selector list */
    if (Secv4ConstSecList () == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4ProcessIkeInstallDynamicIpsecDBMsg: "
                   "Formatting Outbound Selector Failed\n");

        return (SEC_FAILURE);
    }
    return (SEC_SUCCESS);
}

/* MSAD  ADD -E- */

/***********************************************************************/
/*  Function Name : Secv4InstallSecPolicy                              */
/*  Description   : This function installs a new policy for the        */
/*                  associated SA bundle                    e          */
/*                                                                     */
/*  Input(s)      : u4PolicyIndex - Policy Index to be created         */
/*                  pu1PolicySaBundle - SA Bundle                      */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/
INT1
Secv4InstallSecPolicy (UINT4 u4PolicyIndex, UINT1 *pu1PolicySaBundle,
                       UINT1 u1IkeVersion)
{

    UINT4               u4SnmpErrorStatus;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    /* Create a new policy index */
    if (nmhTestv2Fsipv4SecPolicyStatus (&u4SnmpErrorStatus,
                                        (INT4) u4PolicyIndex, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        if (u4SnmpErrorStatus != SNMP_ERR_COMMIT_FAILED)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE, "Cannot create a policy !\r\n");

            return (SEC_FAILURE);
        }
    }

    if (nmhSetFsipv4SecPolicyStatus ((INT4) u4PolicyIndex,
                                     CREATE_AND_GO) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "Memory Allocation Failure !\r\n");

        return (SEC_FAILURE);
    }

    /* Set the Policy flag as APPLY */
    if (nmhTestv2Fsipv4SecPolicyFlag (&u4SnmpErrorStatus,
                                      (INT4) u4PolicyIndex,
                                      SEC_APPLY) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Cannot apply policy for vpn client!\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecPolicyFlag ((INT4) u4PolicyIndex, SEC_APPLY);

    /* Set the policy mode as automatic */
    if (nmhTestv2Fsipv4SecPolicyMode (&u4SnmpErrorStatus,
                                      (INT4) u4PolicyIndex,
                                      SEC_AUTOMATIC) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Cannot apply policy mode as automatic for vpn client!\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecPolicyMode ((INT4) u4PolicyIndex, SEC_AUTOMATIC);

    /* Associate the sa bundle to the policy */
    OctetStr.pu1_OctetList = pu1PolicySaBundle;
    OctetStr.i4_Length = (INT4) STRLEN (pu1PolicySaBundle);

    if (nmhTestv2Fsipv4SecPolicySaBundle (&u4SnmpErrorStatus,
                                          (INT4) u4PolicyIndex,
                                          &OctetStr) == SNMP_FAILURE)

    {
        if (u4SnmpErrorStatus != SNMP_ERR_COMMIT_FAILED)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE, "Cannot attach sa bundle !\r\n");

            return (SEC_FAILURE);
        }
    }

    nmhSetFsipv4SecPolicySaBundle ((INT4) u4PolicyIndex, &OctetStr);
    Secv4SetIkeVersion (u4PolicyIndex, u1IkeVersion);

    return (SEC_SUCCESS);

}

/***********************************************************************/
/*  Function Name : Secv4InstallSecAccess                              */
/*  Description   : This function installs a new access list for the   */
/*                  vpn client                                         */
/*                                                                     */
/*  Input(s)      : u4AccessIndex - Access Index to be created         */
/*                  u4SrcAddress  - Source network                     */
/*                  u4SrcMask     - Source subnet mast                 */
/*                  u4DestAddress - Destination network                */
/*                  u4DestMask    - Destination subnet mask            */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/
INT1
Secv4InstallSecAccess (UINT4 u4AccessIndex,
                       UINT4 u4SrcAddress, UINT4 u4SrcMask,
                       UINT4 u4DestAddress, UINT4 u4DestMask)
{
    UINT4               u4SnmpErrorStatus;

    /* Create a new access index */
    if (nmhTestv2Fsipv4SecAccessStatus
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        if (u4SnmpErrorStatus != SNMP_ERR_COMMIT_FAILED)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Cannot create an access list !\r\n");

            return (SEC_FAILURE);
        }
    }

    if (nmhSetFsipv4SecAccessStatus ((INT4) u4AccessIndex, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "Memory Allocation Failure !\r\n");

        return (SEC_FAILURE);
    }

    /* Set source network */
    if (nmhTestv2Fsipv4SecSrcNet (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
                                  u4SrcAddress) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Invalid Source Address specified !\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecSrcNet ((INT4) u4AccessIndex, u4SrcAddress);

    /* Set source mask */
    if (nmhTestv2Fsipv4SecSrcMask (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
                                   u4SrcMask) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Invalid Source subnet mask specified !\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecSrcMask ((INT4) u4AccessIndex, u4SrcMask);

    /* Set Destination network */
    if (nmhTestv2Fsipv4SecDestNet (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
                                   u4DestAddress) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Invalid Destination Address specified !\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecDestNet ((INT4) u4AccessIndex, u4DestAddress);

    /* Set Destination mask */
    if (nmhTestv2Fsipv4SecDestMask (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
                                    u4DestMask) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Invalid Destination subnet mask specified !\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecDestMask ((INT4) u4AccessIndex, u4DestMask);

    return (SEC_SUCCESS);
}

/***********************************************************************/
/*  Function Name : Secv4InstallSecSelector                            */
/*  Description   : This function installs a new selector for the      */
/*                  vpn client                                         */
/*                                                                     */
/*  Input(s)      : u4InterfaceIndex - Interface index of vpn client   */
/*                  u4PktDirection - Direction (in or out)             */
/*                  u4SelAccessIndex - Access list index               */
/*                  u4SelPolicyIndex - Policy index                    */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/
INT1
Secv4InstallSecSelector (UINT4 u4InterfaceIndex,
                         UINT4 u4PktDirection,
                         UINT4 u4SelAccessIndex, UINT4 u4SelPolicyIndex)
{
    UINT4               u4SnmpErrorStatus;
    UINT4               u4ProtoId = SEC_ANY_PROTOCOL;
    UINT4               u4Port = SEC_ANY_PORT;
    UINT4               u4SelIndex = u4SelAccessIndex;

    /* create a new selector entry for the vpn client */
    if (nmhTestv2Fsipv4SelStatus (&u4SnmpErrorStatus, (INT4) u4SelIndex,
                                  (INT4) u4InterfaceIndex,
                                  (INT4) u4ProtoId, (INT4) u4SelAccessIndex,
                                  (INT4) u4Port, (INT4) u4PktDirection,
                                  CREATE_AND_GO) == SNMP_FAILURE)
    {
        if (u4SnmpErrorStatus != SNMP_ERR_COMMIT_FAILED)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Cannot create a new selector !\r\n");

            return (SEC_FAILURE);
        }
    }

    if (nmhSetFsipv4SelStatus ((INT4) u4SelIndex,
                               (INT4) u4InterfaceIndex,
                               (INT4) u4ProtoId,
                               (INT4) u4SelAccessIndex,
                               (INT4) u4Port,
                               (INT4) u4PktDirection,
                               CREATE_AND_GO) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "Memory Allocation Failure !\r\n");

        return (SEC_FAILURE);
    }

    /* Set the filter flag to allow all packets to/from vpn client */
    if (nmhTestv2Fsipv4SelFilterFlag (&u4SnmpErrorStatus,
                                      (INT4) u4SelIndex,
                                      (INT4) u4InterfaceIndex,
                                      (INT4) u4ProtoId,
                                      (INT4) u4SelAccessIndex,
                                      (INT4) u4Port,
                                      (INT4) u4PktDirection,
                                      SEC_ALLOW) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "Cannot set the flag to allow !\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SelFilterFlag ((INT4) u4SelIndex, (INT4) u4InterfaceIndex,
                               (INT4) u4ProtoId,
                               (INT4) u4SelAccessIndex,
                               (INT4) u4Port, (INT4) u4PktDirection, SEC_ALLOW);

    /* Attach the policy index to the selector */
    if (nmhTestv2Fsipv4SelPolicyIndex (&u4SnmpErrorStatus,
                                       (INT4) u4SelIndex,
                                       (INT4) u4InterfaceIndex,
                                       (INT4) u4ProtoId,
                                       (INT4) u4SelAccessIndex,
                                       (INT4) u4Port,
                                       (INT4) u4PktDirection,
                                       (INT4) u4SelPolicyIndex) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Cannot attach the policy index to the selector !\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SelPolicyIndex ((INT4) u4SelIndex,
                                (INT4) u4InterfaceIndex,
                                (INT4) u4ProtoId,
                                (INT4) u4SelAccessIndex,
                                (INT4) u4Port, (INT4) u4PktDirection,
                                (INT4) u4SelPolicyIndex);

    /* DF Bit Always cleared for the corresponding interface */
    nmhSetFsipv4SecDFragBitStatus ((INT4) u4InterfaceIndex, SEC_DFBIT_CLEAR);

    return (SEC_SUCCESS);
}

/***********************************************************************/
/*  Function Name : Secv4ProcessIkeDeleteDynamicIpsecDB                */
/*  Description   : This function Deletes all the SA's which matches   */
/*                : the rcvd Spi,DestAddr and Protocol. It also        */
/*                : deletes the policy, access list and selector for   */
/*                : the associated SA                                  */
/*                                                                     */
/*  Input(s)      : u4PolicyIndex - Policy index to be deleted         */
/*                  u4AccessIndex - Access list index to be deleted    */
/*                  u4InterfaceIndex - VPN client interface index      */
/*                  u4PktDirection - Direction (in or out)             */
/*                                                                     */
/*  Output(s)     : None                                               */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/

INT1
Secv4ProcessIkeDeleteDynamicIpsecDB (UINT4 u4PolicyIndex,
                                     UINT4 u4InterfaceIndex,
                                     UINT4 u4PktDirection)
{
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT4               u4SelIndex = u4PolicyIndex;

    /* Delete the policy */

    if (nmhTestv2Fsipv4SecPolicyStatus (&u4SnmpErrorStatus,
                                        (INT4) u4PolicyIndex,
                                        DESTROY) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Cannot delete policy for vpn client!\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
    /* Delete selector */
    if (nmhTestv2Fsipv4SelStatus (&u4SnmpErrorStatus, (INT4) u4SelIndex, (INT4) u4InterfaceIndex,    /* interface index */
                                  SEC_ANY_PROTOCOL,    /* any proto */
                                  (INT4) u4PolicyIndex,    /* access list index */
                                  SEC_ANY_PORT,    /* any port number */
                                  (INT4) u4PktDirection,
                                  DESTROY) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Cannot delete selector entry for vpn client!\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SelStatus ((INT4) u4SelIndex, (INT4) u4InterfaceIndex,    /* interface index */
                           SEC_ANY_PROTOCOL,    /* any proto */
                           (INT4) u4PolicyIndex,    /* access list index */
                           SEC_ANY_PORT,    /* any port number */
                           (INT4) u4PktDirection, DESTROY);

    /* Delete access list */
    if (nmhTestv2Fsipv4SecAccessStatus (&u4SnmpErrorStatus,
                                        (INT4) u4PolicyIndex,
                                        DESTROY) == SNMP_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Cannot delete access list for vpn client!\r\n");

        return (SEC_FAILURE);
    }

    nmhSetFsipv4SecAccessStatus ((INT4) u4PolicyIndex, DESTROY);
    return (SEC_SUCCESS);
}
