/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4prcfrg.c,v 1.14 2014/11/23 09:32:31 siva Exp $
 *
 * Description:Consists of  fragment reassembly functions   
 *                
 *
 *******************************************************************/

#include "secv4com.h"
#include "secv4frgtyp.h"

/**********************************************************************************/
/* Function           : Secv4Reassemble                                           */
/* Description        : Processes IP datagram fragments.If incoming datagram is   */
/*                    : complete, returns the same.else if it is a fragment and   */
/*                    : completes a datagram, returns pointer completed datagram  */
/*                    : else put the fragment in proper place for reassembly and  */
/*                    : returns NULL.                                             */
/* Input(s)           : pIp, pBuf, u2Port                                         */
/* Output(s)          : None.                                                     */
/* Returns            : NULL - If buffer is fragment                              */
/*                    : Buffer pointer - pointer to complete packet.              */
/**********************************************************************************/

tCRU_BUF_CHAIN_HEADER *
Secv4Reassemble (t_IP_HEADER * pIpHdr, tCRU_BUF_CHAIN_HEADER * pBuf,
                 UINT2 u2Port)
{
    tSecv4Reassm       *pReasm = NULL;
    tSecv4Frag         *pLastFrag = NULL;
    tSecv4Frag         *pNextFrag = NULL;
    tSecv4Frag         *pTmpFrag = NULL;
    UINT2               u2Pos = 0;
    UINT2               u2Flag = 0;
    UINT2               u2StartOffset = 0;    /* Index of first byte in 
                                               fragment */
    UINT2               u2EndOffset = 0;    /* Index of first byte beyond 
                                               fragment */
    INT1                i1MoreFlag = 0;    /* 1 if not last fragment, 0 
                                           otherwise */
    t_IP               *pIp = NULL;
    t_IP                IpSt;

    UNUSED_PARAM (u2Port);

    /* t_IP_HEADER        *pIpHdr = NULL; */

    pIp = &IpSt;
    /* pIpHdr = (t_IP_HEADER *) pBuf->data; */

    IpSt.u1Version = SEC_V4;
    IpSt.u1Tos = pIpHdr->u1Tos;
    IpSt.u1Hlen = (UINT1) ((pIpHdr->u1Ver_hdrlen & 0x0f) * sizeof (UINT4));
    IpSt.u2Len = IPSEC_NTOHS (pIpHdr->u2Totlen);
    IpSt.u2Id = IPSEC_NTOHS (pIpHdr->u2Id);
    IpSt.u2Fl_offs = IPSEC_NTOHS (pIpHdr->u2Fl_offs);
    IpSt.u1Ttl = pIpHdr->u1Ttl;
    IpSt.u1Proto = pIpHdr->u1Proto;
    IpSt.u2Cksum = IPSEC_NTOHS (pIpHdr->u2Cksum);
    IpSt.u4Src = IPSEC_NTOHL (pIpHdr->u4Src);
    IpSt.u4Dest = IPSEC_NTOHL (pIpHdr->u4Dest);
    IpSt.u2Olen = (UINT2) (IpSt.u1Hlen - IP_HDR_LEN);

    u2StartOffset = (UINT2) SECv4_OFF_IN_BYTES (IpSt.u2Fl_offs);    /* Convert to 
                                                                       bytes */
    u2EndOffset =
        (UINT2) (u2StartOffset + IpSt.u2Len - (IP_HDR_LEN + IpSt.u2Olen));
    i1MoreFlag = ((IpSt.u2Fl_offs & SECv4_MF) ? TRUE : FALSE);

    if ((u2StartOffset == 0) && (i1MoreFlag == FALSE))    /* Complete pkt 
                                                           received */
    {
        return pBuf;
    }
    if (u2EndOffset > SECv4_IF_MAX_REASM_SIZE)
    {
        /* This is exceeding the maximum reassembly limit
         * If reassembly as up for this datagram let it timeout
         * on its own.  Else it is some unwanted pkt belonging to
         * old discarded reassembly.
         */
        IPSEC_BUF_Release (pBuf, FALSE);
        SECv4_TRC (SECv4_DATA_PATH, "Secv4Reassemble: Fragment size is high\n");
        return NULL;
    }
    /* Remove IP header portion from all fragments except first */
    if (u2StartOffset != 0)
    {
        if (IPSEC_BUF_MoveOffset (pBuf, (UINT4) (IP_HDR_LEN + IpSt.u2Olen)) ==
            CRU_FAILURE)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4Reassemble:IPSEC_BUF_MoveOffset failed\n");
            IPSEC_BUF_Release (pBuf, FALSE);
            return NULL;
        }
    }

    if ((pReasm = Secv4LookUpReasm (pIp)) == NULL)
    {
        /* First fragment; create new reassembly descriptor */
        if ((pReasm = Secv4CreateReasm (pIp)) == NULL)
        {
            /* No space for descriptor, drop fragment */
            SECv4_TRC (SECv4_DATA_PATH, " Unable to create lsit for frag\n");
            IPSEC_BUF_Release (pBuf, FALSE);
            return NULL;
        }
        Secv4StartTimer (&(pReasm->Timer), SECv4_REASM_TIMER_ID,
                         SECv4_DEF_REASM_TIME);
    }
    else
    {
        /* Keep restarting timer as long as we keep getting fragments */
        if ((pReasm->u2Len != 0) && (u2StartOffset > pReasm->u2Len))
        {
            /*
             *  A segment Rcvd whose Offset is more than the size of
             *  the datagram. Hence discard the datagram silently.
             */
            IPSEC_BUF_Release (pBuf, FALSE);
            return NULL;
        }
        Secv4StopTimer (&(pReasm->Timer));
        Secv4StartTimer (&(pReasm->Timer), SECv4_REASM_TIMER_ID,
                         SECv4_DEF_REASM_TIME);
    }

    /*
     * If this is the last fragment, we now know how long the
     * entire datagram is; record it
     */

    if (i1MoreFlag == FALSE)
    {
        pReasm->u2Len = u2EndOffset;
    }

    /*
     * Set pNextFrag to the first fragment which begins after us,
     * and pLastFrag to the last fragment which begins before us.
     */
    pLastFrag = NULL;

    TMO_DLL_Scan (&(pReasm->Secv4FragList), pNextFrag, tSecv4Frag *)
    {
        if (pNextFrag->u2StartOffset > u2StartOffset)
        {
            /* Next frag offset greater than ours */
            break;
        }
        else if ((pNextFrag->u2StartOffset == u2StartOffset) &&
                 (u2StartOffset == 0))
        {
          /** Two fragment Having Offset 0 (Case if the first fragment having
              complete Option Part
              This case Occurs when MTU is 64 and Ip header has maximum Option

              In this case the fragment having Option len large will be
              Considered as first fragment. Please do not allow this packet this may
              lead to security loop hole. **/
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4Reassemble: Two fragments with 0 offset.Drop the packet\n");
            IPSEC_BUF_Release (pBuf, FALSE);
            return NULL;
        }
        pLastFrag = pNextFrag;
    }

    /* Check for overlap with preceeding fragment */
    if ((pLastFrag != NULL) && (u2StartOffset < pLastFrag->u2EndOffset))
    {
        /* Strip overlap from new fragment */

        u2Pos = (UINT2) (pLastFrag->u2EndOffset - u2StartOffset);

        if (CRU_BUF_Get_ChainValidByteCount (pBuf) <= u2Pos)
        {
            IPSEC_BUF_Release (pBuf, FALSE);
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4Reassemble: Duplicate packet, deleting it\n");
            return NULL;
        }
        /* Avoid Security Loop Hole */
        IPSEC_BUF_Release (pBuf, FALSE);
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4Reassemble: offset overlaps with preceeding fragment.Drop the packet\n");
        return NULL;
    }

    /* Look for overlap with succeeding segments */
    while (pNextFrag != NULL)
    {

        pTmpFrag = (tSecv4Frag *) TMO_DLL_Next (&pReasm->Secv4FragList,
                                                &pNextFrag->Link);

        if (pNextFrag->u2StartOffset >= u2EndOffset)
        {
            break;                /* Past our end */
        }
        else
        {
            /* Avoid Security Loop Hole */
            IPSEC_BUF_Release (pBuf, FALSE);
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4Reassemble: offset overlaps with succeeding "
                       "fragment.Drop the packet\n");
            return NULL;
        }

/*
 * Below code will never be executed, above if check will either
 * break the loop, or in else part it will return NULL.
 * 
 */

#ifdef UNWANTED_CODE
        /*
         * Trim the front of this entry; if nothing is
         * left, remove it.
         */
        u2Pos = u2EndOffset - pNextFrag->u2StartOffset;
        if (IPSEC_BUF_MoveOffset (pNextFrag->pBuf, u2Pos) == CRU_FAILURE)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4Reassemble: IPSEC_BUF_MoveOffset Failed\n");
            IPSEC_BUF_Release (pBuf, FALSE);
            return NULL;
        }
        if (CRU_BUF_Get_ChainValidByteCount (pNextFrag->pBuf) == 0)
        {
            /* superseded; delete from list */
            TMO_DLL_Delete (&pReasm->Secv4FragList, &pNextFrag->Link);
            Secv4FreeFrag (pNextFrag);
        }
        else
        {
            pNextFrag->u2StartOffset = u2EndOffset;
            break;
        }
#endif
    }

    /*
     * Lastfrag now points, as before, to the fragment before us;
     * pNextFrag points at the next fragment. Check to see if we can
     * join to either or both fragments.
     */
    u2Flag = SECv4_FRAG_INSERT;

    if ((pLastFrag != NULL) && (pLastFrag->u2EndOffset == u2StartOffset))
    {
        u2Flag |= SECv4_FRAG_APPEND;
    }

    if ((pNextFrag != NULL) && (pNextFrag->u2StartOffset == u2EndOffset))
    {
        u2Flag |= SECv4_FRAG_PREPEND;
    }

    switch (u2Flag)
    {
        case SECv4_FRAG_INSERT:
            /* Insert new desc between pLastFrag and pNextFrag */
            if (TMO_DLL_Count (&pReasm->Secv4FragList) ==
                SECv4_MAX_FRAGMENTS_IN_DATAGRAM)
            {
                /*
                 * NOTE: At this position, there is no point in continuing with
                 *       reassembly since u have already dropped a fragment.
                 *       But we can't delete it bcos if we do it a new reassembly
                 *       will be started for the fragments following this.
                 */
                IPSEC_BUF_Release (pBuf, FALSE);
                return NULL;
            }

            pTmpFrag = Secv4NewFrag (u2StartOffset, u2EndOffset, pBuf);
            if (pTmpFrag == NULL)
            {
                SECv4_PRINT_MEM_ALLOC_FAILURE;
                return NULL;
            }
            TMO_DLL_Insert (&pReasm->Secv4FragList, &pLastFrag->Link,
                            &pTmpFrag->Link);
            break;

        case SECv4_FRAG_APPEND:    /* Append to pLastFrag */
            /*Frag Append operation */
            if (pLastFrag != NULL)
            {
                SECv4_CONCAT_BUFS (pLastFrag->pBuf, pBuf);
                pLastFrag->u2EndOffset = u2EndOffset;    /* Extend forward */
            }
            break;

        case SECv4_FRAG_PREPEND:
            /* Prepend to pNextFrag */
            /*Frag Prepend operation */
            if (pNextFrag != NULL)
            {
                SECv4_CONCAT_BUFS (pBuf, pNextFrag->pBuf);
                pNextFrag->pBuf = pBuf;
                pNextFrag->u2StartOffset = u2StartOffset;    /* Extend backward */
            }
            break;

        case (SECv4_FRAG_APPEND | SECv4_FRAG_PREPEND):
            /*Append And Prepend operation */
            /* Consolidate by appending this fragment and pNextFrag
             * to pLastFrag and removing the pNextFrag descriptor
             */
            if ((pLastFrag != NULL) && (pNextFrag != NULL))
            {
                SECv4_CONCAT_BUFS (pLastFrag->pBuf, pBuf);
                SECv4_CONCAT_BUFS (pLastFrag->pBuf, pNextFrag->pBuf);
                pNextFrag->pBuf = NULL;
                pLastFrag->u2EndOffset = pNextFrag->u2EndOffset;

                /* Finally unlink and delete the now unneeded pNextFrag */
                TMO_DLL_Delete (&(pReasm->Secv4FragList), &pNextFrag->Link);
                Secv4FreeFrag (pNextFrag);
            }
            break;

        default:
            break;
    }
    pTmpFrag = (tSecv4Frag *) TMO_DLL_First (&(pReasm->Secv4FragList));
    if (pTmpFrag == NULL)
    {
        return NULL;
    }
    if ((pTmpFrag->u2StartOffset == 0) &&
        (pReasm->u2Len != 0) && (pTmpFrag->u2EndOffset == pReasm->u2Len))
    {
        pBuf = pTmpFrag->pBuf;
        pTmpFrag->pBuf = NULL;

        /* Tell IP the entire length */
        IPSEC_MEMSET (&(pIp->u2Fl_offs), 0, sizeof (UINT2));
        IpSt.u2Len = (UINT2) (pReasm->u2Len + (IP_HDR_LEN + IpSt.u2Olen));
        Secv4PutIpHdr (pBuf, pIp, TRUE);
        /* print_packet(pBuf); */
        Secv4FreeReasm (pReasm);
        return pBuf;
    }
    return NULL;
}

/*************************************************************************************/
/* Function           : Secv4LookUpReasm                                             */
/* Description        : Looks up Reassembly Queue for a DataGram undergoing          */
/*                    :  reassembly                                                  */
/*                    : matching pIp                                                 */
/* Input(s)           : pIp - Pointer to IP Header Information                       */
/* Output(s)          : None                                                         */
/* Returns            : pointer - if found                                           */
/*                    : NULL    - if not found.                                      */
/*************************************************************************************/

tSecv4Reassm       *
Secv4LookUpReasm (t_IP * pIp)
{
    tSecv4Reassm       *pReasm = NULL;

    TMO_DLL_Scan (&Secv4ReasmList, pReasm, tSecv4Reassm *)
    {
        if ((pIp->u4Src == pReasm->u4Src) &&
            (pIp->u4Dest == pReasm->u4Dest) &&
            (pIp->u1Proto == pReasm->u1Proto) && (pIp->u2Id == pReasm->u2Id))
        {
            return pReasm;
        }
    }
    return NULL;
}

/************************************************************************************/
/* Function           : Secv4CreateReasm                                            */
/* Description        : Creates a Reassembly descriptor,puts at head of reassembly  */
/*                    : List.This Procedure also initializes the fragment list for  */
/*                    : reassembly                                                  */
/* Input(s)           : pIp                                                         */
/* Output(s)          : None                                                        */
/* Returns            : pointer to reassembly structure if SEC_SUCCESS, NULL if     */
/*                    : failure                                                     */
/************************************************************************************/

tSecv4Reassm       *
Secv4CreateReasm (t_IP * pIp)
{
    tSecv4Reassm       *pReasm = NULL;

    if (TMO_DLL_Count (&Secv4ReasmList) == SECv4_MAX_DATAGRAMS_FOR_REASSEMBLY)
    {
        /*
         * NOTE: An exception or a Count of No. of Datagrams dropped due to
         *       too many Datagrams arrived for reassembly can be had here.
         */

        /*
         * Reassembly wasn't taken up at all. Hence should be considered as
         * reassembly failure due to memory shortage.
         */
        return NULL;
    }

    if (MemAllocateMemBlock
        (SECv4_IP_REASSM_MEMPOOL, (UINT1 **) (VOID *) &pReasm) == MEM_SUCCESS)
    {
        pReasm->u4Src = pIp->u4Src;
        pReasm->u4Dest = pIp->u4Dest;
        pReasm->u2Id = pIp->u2Id;
        pReasm->u1Proto = pIp->u1Proto;
        pReasm->u2Len = 0;
        pReasm->Timer.u4Param1 = (FS_ULONG) pReasm;
        SECv4_INIT_TIMER (&(pReasm->Timer));
        TMO_DLL_Add (&Secv4ReasmList, (tTMO_DLL_NODE *) pReasm);
        /* Initialize the fragment list for this reassembly process */
        TMO_DLL_Init (&(pReasm->Secv4FragList));
        return (pReasm);
    }
    else
    {
        SECv4_PRINT_MEM_ALLOC_FAILURE;
        return (NULL);
    }
}

/*************************************************************************************/
/* Function           : Secv4FreeReasm                                               */
/* Descriptor         : Free all resources associated with a reassembly descriptor.  */
/* Input(s)           : pReasm                                                       */
/* Output(s)          : None                                                         */
/* Returns            : None                                                         */
/*************************************************************************************/

VOID
Secv4FreeReasm (tSecv4Reassm * pReasm)
{
    tSecv4Frag         *pFrag;

    Secv4StopTimer (&(pReasm->Timer));
    /* Remove from list of reassembly descriptors */
    TMO_DLL_Delete (&Secv4ReasmList, &pReasm->Link);
    /* Free any fragments on list, starting at beginning */
    while ((pFrag = (tSecv4Frag *) TMO_DLL_First (&pReasm->Secv4FragList)))
    {
        if (pFrag->pBuf != NULL)
        {
            IPSEC_BUF_Release (pFrag->pBuf, FALSE);
        }
        /* Should have deleted from reassembly list before freeing it. */
        TMO_DLL_Delete (&pReasm->Secv4FragList, &pFrag->Link);
        MemReleaseMemBlock (SECv4_IP_FRAG_MEMPOOL, (UINT1 *) pFrag);
    }
    MemReleaseMemBlock (SECv4_IP_REASSM_MEMPOOL, (UINT1 *) pReasm);
}

/*********************************************************************************/
/* Function           : Secv4NewFrag                                             */
/* Descriptor         : Create a fragment.                                       */
/* Input(s)           : u2StartOffset, u2EndOffset, pBuf                         */
/* Output(s)          : None                                                     */
/* Returns            : pointer to new fragment if SEC_SUCCESS                   */
/*                    : NULL if failure                                          */
/*********************************************************************************/
tSecv4Frag         *
Secv4NewFrag (UINT2 u2StartOffset, UINT2 u2EndOffset,
              tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tSecv4Frag         *pFrag;

    if (MemAllocateMemBlock (SECv4_IP_FRAG_MEMPOOL, (UINT1 **) (VOID *) &pFrag)
        != MEM_SUCCESS)
    {
        /* Drop fragment */
        SECv4_PRINT_MEM_ALLOC_FAILURE;
        IPSEC_BUF_Release (pBuf, FALSE);
        return NULL;
    }
    pFrag->pBuf = pBuf;
    pFrag->u2StartOffset = u2StartOffset;
    pFrag->u2EndOffset = u2EndOffset;
    return pFrag;
}

/*******************************************************************************/
/* Function           : Secv4FreeFrag                                          */
/* Descriptor         : Delete a Fragment                                      */
/* Input(s)           : pFrag                                                  */
/* Output(s)          : None                                                   */
/* Returns            : None                                                   */
/*******************************************************************************/

VOID
Secv4FreeFrag (tSecv4Frag * pFrag)
{
    if (pFrag->pBuf != NULL)
    {
        IPSEC_BUF_Release (pFrag->pBuf, FALSE);
    }
    MemReleaseMemBlock (SECv4_IP_FRAG_MEMPOOL, (UINT1 *) pFrag);

}

/*****************************************************************************/
/* Function           : Secv4FragmentAndSend                                 */
/* Description        : This procedure fragments the given message to send   */
/*                      over an interface.                                   */
/*                    : It copies appropriate flags into fragments and calls */
/*                    : interface send routine to send it over the interface.*/
/* Input(s)           : u2Port, pIp, pBuf                                    */
/* Output(s)          : None                                                 */
/* Returns            : SEC_SUCCESS, SEC_FAILURE                             */
/*****************************************************************************/

INT4
Secv4FragmentAndSend (t_IP_HEADER * pIpHdr, tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT4 u4IfIndex)
{
    t_IP                IpHdrForFrag;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pFragData = NULL;
    INT2                i2Orgmf = 0;    /* Temp to store original more flag */
    UINT2               u2Offset = 0;
    UINT2               u2FragDataSize = 0;    /* Size of this fragment's data     */
    INT2                i2IpHlen = 0;
    UINT2               u2Len = 0;
    UINT2               u2OrgLen = 0;    /* added for trace and debug */
    INT1                i1FirstFrag = TRUE;
    INT1                i1LastFrag = FALSE;
    t_IP               *pIp = NULL;
    UINT4               u4IfIndexMtu;
    UINT1               au1MacHdr[SEC_ETH_HEADER_SIZE];

    tMODULE_DATA        ModData;
    IPSEC_MEMSET (&ModData, 0, sizeof (tMODULE_DATA));
    IPSEC_MEMSET (au1MacHdr, 0, SEC_ETH_HEADER_SIZE);

    IPSEC_MEMSET (&IpHdrForFrag, 0, sizeof (t_IP));

    pIp = &IpHdrForFrag;

    pIp->u1Version = SEC_V4;
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u1Hlen = (UINT1) ((pIpHdr->u1Ver_hdrlen & 0x0f) * sizeof (UINT4));
    pIp->u2Len = IPSEC_NTOHS (pIpHdr->u2Totlen);
    pIp->u2Id = IPSEC_NTOHS (pIpHdr->u2Id);
    pIp->u2Fl_offs = IPSEC_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = IPSEC_NTOHS (pIpHdr->u2Cksum);
    pIp->u4Src = IPSEC_NTOHL (pIpHdr->u4Src);
    pIp->u4Dest = IPSEC_NTOHL (pIpHdr->u4Dest);
    pIp->u2Olen = (UINT2) (pIp->u1Hlen - IP_HDR_LEN);

    u2OrgLen = pIp->u2Len;
    i2IpHlen = (INT2) SECv4_TOTAL_HDR_LEN (pIp);

    /* Data length only. IP Hdr and Options are not included. */
    u2Len = (UINT2) (pIp->u2Len - i2IpHlen);
    u2Offset = (UINT2) SECv4_OFF_IN_BYTES (pIp->u2Fl_offs);

    /* Save original MF flag */
    i2Orgmf = (pIp->u2Fl_offs & SECv4_MF);

    Secv4GetIfMtu (u4IfIndex, &u4IfIndexMtu);
    /* As Long as there is data left */
    while (u2Len > 0)
    {
        pIp->u2Fl_offs = (UINT2) SECv4_OFF_IN_HDR_FORMAT (u2Offset);
        if ((UINT2) (u2Len + i2IpHlen) <= (UINT2) u4IfIndexMtu)
        {
            /* Last fragment; send all that remains */
            u2FragDataSize = u2Len;
            pIp->u2Fl_offs |= (UINT2) i2Orgmf;    /* Pass original MF flag */
            i1LastFrag = TRUE;
        }
        else
        {
            /* More to come, so send multpIple of 8 bytes */
            u2FragDataSize =
                (UINT2) (((UINT2) u4IfIndexMtu - i2IpHlen) & 0xfff8);
            pIp->u2Fl_offs |= SECv4_MF;
        }

        pIp->u2Len = (UINT2) (u2FragDataSize + i2IpHlen);
        /* Put IP header back on */
        if (i1FirstFrag == FALSE)
        {
            pTmpBuf =
                CRU_BUF_Allocate_MsgBufChain ((UINT4) SECv4_MAC_HDR_LEN +
                                              pIp->u2Olen,
                                              (UINT4) SECv4_MAC_HDR_LEN -
                                              IP_HDR_LEN);
            if (pTmpBuf == NULL)
            {
                IPSEC_BUF_Release (pBuf, FALSE);
                return SEC_FAILURE;
            }
            IPSEC_MEMCPY (SEC_GET_MODULE_DATA_PTR (pTmpBuf),
                          (tSecModuleData *) & ModData, sizeof (tMODULE_DATA));
            Secv4PutIpHdr (pTmpBuf, pIp, TRUE);
            /*
             * Extract data from original buffer and link it with our
             * header.
             */
            pFragData = pBuf;

            if (i1LastFrag == FALSE)
            {
                if (SECv4_FRAGMENT_BUF (pFragData, u2FragDataSize, &pBuf) ==
                    CRU_FAILURE)
                {
                    SECv4_TRC (SECv4_DATA_PATH,
                               "Secv4FragmentAndSend: FRAGMENT_BUF - "
                               "Subsequent Failed\n");
                    IPSEC_BUF_Release (pTmpBuf, FALSE);
                    IPSEC_BUF_Release (pFragData, FALSE);
                    return SEC_FAILURE;
                }
            }
            SECv4_CONCAT_BUFS (pTmpBuf, pFragData);
        }
        else
        {
            /*
             * Extract IP header  and data from front end and send it.
             */
            pTmpBuf = pBuf;
            if (SECv4_FRAGMENT_BUF
                (pTmpBuf, (UINT4) (u2FragDataSize + i2IpHlen),
                 &pBuf) == CRU_FAILURE)
            {
                SECv4_TRC (SECv4_DATA_PATH,
                           "Secv4FragmentAndSend: FRAGMENT_BUF - First "
                           "Failed\n");
                IPSEC_BUF_Release (pTmpBuf, FALSE);
                return SEC_FAILURE;
            }
            Secv4PutIpHdr (pTmpBuf, pIp, TRUE);
        }

        /* Now the datagram to send is in pTmpBuf; Add frag specific info */
        SECv4_PKT_ASSIGN_LEN (pTmpBuf, pIp->u2Len);
        SECv4_PKT_ASSIGN_FLAGS (pTmpBuf, pIp->u2Fl_offs);

        if (i1FirstFrag == TRUE)
        {
            IPSEC_MEMCPY ((tSecModuleData *) & ModData,
                          SEC_GET_MODULE_DATA_PTR (pTmpBuf),
                          sizeof (tMODULE_DATA));
        }

        if (SecProcessContinueOutBound (pTmpBuf, u4IfIndex) != OSIX_SUCCESS)
        {
            IPSEC_BUF_Release (pBuf, FALSE);
            return SEC_FAILURE;
        }
        u2Offset = (UINT2) ( u2Offset + u2FragDataSize );    /* Each time offset gets updated */
        u2Len = (UINT2) ( u2Len - u2FragDataSize );

        if (i1FirstFrag == TRUE)
        {
            i1FirstFrag = FALSE;
            /* If there are no options dont waste time in copying */
            if (pIp->u2Olen != 0)
            {
                /*
                 * After the first fragment, should remove those
                 * options that aren't supposed to be copied on fragmentation
                 */
                Secv4ConstructHdrForFrags (pIp, &IpHdrForFrag);
                pIp = &(IpHdrForFrag);
                i2IpHlen = (INT2) SECv4_TOTAL_HDR_LEN (pIp);
            }
        }
    }

    UNUSED_PARAM (u2OrgLen);
    return SEC_SUCCESS;
}

/************************************************************************************/
/* Function           : Secv4ConstructHdrForFrags                                   */
/* Description        : This procedure takes in two t_IP header type structures.    */
/*                    : It constructs the IP header for the fragments depending on  */
/*                    : options.                                                    */
/*                    : finally it updates i1Olen field in fragment IP header       */
/*                    : structure.                                                  */
/* Input(s)           : pIp_org, pIp_frag                                           */
/* Output(s)          : None                                                        */
/* Returns            : SEC_SUCCESS, SEC_FAILURE                                    */
/************************************************************************************/

VOID
Secv4ConstructHdrForFrags (t_IP * pIp_org, t_IP * pIp_frag)
{
    UINT2               u2Count = 0;
    UINT2               u2CurOlen = 0;
    INT1                i1IpOpteolFlag = FALSE;

    MEMCPY (pIp_frag, pIp_org, IP_HDR_LEN);    /* Copy all except options */
    pIp_frag->u2Olen = 0;

    while ((u2Count < pIp_org->u2Olen) && (u2Count < (IP_OPTION_LEN - 1)) &&
           (IPSEC_OPT_NUMBER (pIp_org->au1Options[u2Count]) != IPSEC_OPT_EOL))
    {
        u2CurOlen = (UINT2)
            ((pIp_org->au1Options[u2Count] ==
              IPSEC_OPT_NOP) ? 1 : pIp_org->au1Options[u2Count + 1]);
        if (SECv4_COPY_OPTION_TO_FRAG (pIp_org->au1Options[u2Count]) != FALSE)
        {
            i1IpOpteolFlag = TRUE;
            /* first byte is code second is length followed by option */
            /* length includes first two bytes also */
            pIp_frag->u2Olen = (UINT2) (u2CurOlen + pIp_frag->u2Olen);
        }
        u2Count = (UINT2) (u2CurOlen + u2Count);
    }
    if ((i1IpOpteolFlag == TRUE) && (u2Count < (IP_OPTION_LEN - 1)))
    {
        pIp_frag->au1Options[u2Count] = 0;
        pIp_frag->u2Olen++;
    }
}

/***********************************************************************************/
/* Function           : Secv4HandleReAsmTimerExpiry                                */
/* Descriptor         : This is called from IP_FWD_Handle_Timer_Expiry procedure.  */
/*                    : Deletes the Reassembly list in the timer structure.        */
/* Input(s)           : pTimer                                                     */
/* Output(s)          : None                                                       */
/* Returns            : None                                                       */
/***********************************************************************************/

VOID
Secv4HandleReAsmTimerExpiry (tSecv4Timer * pTimer)
{
    tSecv4Reassm       *pReasm = NULL;
    tSecv4Frag         *pFrag = NULL;

    pReasm = (tSecv4Reassm *) pTimer->u4Param1;
    if (pReasm == NULL)
    {
        return;
    }
    pFrag = (tSecv4Frag *) TMO_DLL_First (&pReasm->Secv4FragList);
    if (pFrag == NULL)
    {
        return;
    }
    if (pFrag->u2StartOffset == 0)
    {
        t_ICMP              Icmp;
        /*
         * First fragment is available, now you can send ICMP error message
         */
        Icmp.i1Type = ICMP_TIME_EXCEED;
        Icmp.i1Code = ICMP_FRAG_EXCEED;
        Icmp.args.u4Unused = 0;
        Secv4SendIcmpErrMsg (pFrag->pBuf, &Icmp);
    }
    Secv4FreeReasm (pReasm);
    return;
}

INT4
Secv4ReassemblyCleanup ()
{
    tSecv4Reassm       *pReasm = NULL;
    while ((pReasm = (tSecv4Reassm *) TMO_DLL_First (&Secv4ReasmList)) != NULL)
    {
        Secv4FreeReasm (pReasm);
    }
    return SEC_SUCCESS;
}
