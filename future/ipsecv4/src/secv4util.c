/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4util.c,v 1.17 2015/04/04 10:34:09 siva Exp $
 *
 * Description: This has util functions for IPSec
 *
 ***********************************************************************/

#include "secv4com.h"
#include "secv4soft.h"
#include "fssecv4lw.h"
#include "cli.h"
#include "arp.h"
#include "secv4cli.h"

/************************************************************************/
/*  Function Name : Secv4SeqNumberVerification                          */
/*  Description   : This function gets the incomming packet             */
/*                  SeqNumber Compares it with SA SeqNumber             */
/*                  window.                                             */
/*  Input(s)      : u4SeqNumber - Pkt SeqNumber.                        */
/*                  pSaEntry - SA Entry.                                */
/*  Output(s)     : None.                                               */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/************************************************************************/

INT1
Secv4SeqNumberVerification (UINT4 u4SeqNumber, tSecv4Assoc * pSaEntry)
{
    UINT4               u4Diff = 0;
    if (u4SeqNumber == 0)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4SeqNumberVerification: SeqNum is Zero\n");
        return SEC_FAILURE;
    }
    if (u4SeqNumber > pSaEntry->u4SecAssocSeqNumber)
    {
        u4Diff = u4SeqNumber - pSaEntry->u4SecAssocSeqNumber;
        if (u4Diff < pSaEntry->u1SecAssocSeqCounterFlag)
        {
            pSaEntry->u4SecAssocBitMask <<= u4Diff;
            pSaEntry->u4SecAssocBitMask |= 1;
        }
        else
        {
            pSaEntry->u4SecAssocBitMask = 1;
        }
        pSaEntry->u4SecAssocSeqNumber = u4SeqNumber;
        return SEC_SUCCESS;
    }
    u4Diff = pSaEntry->u4SecAssocSeqNumber - u4SeqNumber;
    if (u4Diff >= pSaEntry->u1SecAssocSeqCounterFlag)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4SeqNumberVerification: Packet is too old \n");
        return SEC_FAILURE;
    }
    if (pSaEntry->u4SecAssocBitMask & ((UINT4) 1 << u4Diff))
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4SeqNumberVerification: Duplicate Packet \n");
        return SEC_FAILURE;
    }
    pSaEntry->u4SecAssocBitMask |= ((UINT4) 1 << u4Diff);
    return SEC_SUCCESS;
}

/*************************************************************************/
/*  Function Name : Secv4AddExtraHeaderToIP                             */
/*  Description   : This function adds data from the given              */
/*                : offset.                                             */
/*  Input(s)      : pCBuf -  buffer contains IP packet                   */
/*                : pu1Ah: PoINT4er to authentication header            */
/*                : u4AhLen : Length of Authentication Header           */
/*                : pu1AhPadding :Used for padding the Ah Header        */
/*                : u4AhPadLen :Length of the Padding                   */
/*  Output(s)     : None.                                               */
/*  Global(s)     : None.                                               */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/***********************************************************************/

INT1
Secv4AddExtraHeaderToIP (tIP_BUF_CHAIN_HEADER * pCBuf, UINT1 *pu1Ah,
                         t_IP_HEADER * pIpHdr, UINT1 u1AhDataSize)
{
    tCRU_BUF_CHAIN_HEADER *pTempCBuf = NULL;
    UINT1               u1OptLen;
    UINT4               u4OptLen = 0;

    u1OptLen = *(UINT1 *) pIpHdr;
    u4OptLen = (UINT4) IPSEC_OLEN (u1OptLen);

    /*Fragment the buffer such that 
     * the ip hdr and options as  has been copied to another buffer */
    if (CRU_BUF_Fragment_BufChain (pCBuf, (UINT4)
                                   (SEC_IPV4_HEADER_SIZE + u4OptLen),
                                   &pTempCBuf) == CRU_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Fragment Buf Chaing Failed to remove ESP Header\n");
        return SEC_FAILURE;
    }

    /* Add padding before the buffer */
    if (IPSEC_BUF_Prepend (pTempCBuf, gau1v4AuthPadding, u1AhDataSize)
        == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AddExtraHeaderToIP: Prepend Buf failed\n");
        return (SEC_FAILURE);
    }

    /* Add auth header before paddding in the buffer */
    if (IPSEC_BUF_Prepend (pTempCBuf, pu1Ah, SEC_AUTH_HEADER_SIZE) ==
        BUF_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AddExtraHeaderToIP: Prepend Buf failed\n");
        return (SEC_FAILURE);

    }
    CRU_BUF_Concat_MsgBufChains (pCBuf, pTempCBuf);
    return SEC_SUCCESS;
}

/*************************************************************************/
/*  Function Name : Secv4ProcessOptionFields                            */
/*  Description   : This function is used to mute the options fields    */
/*                :                                                     */
/*  Input(s)      : pCBuf -  buffer contains IP packet                  */
/*                : u4OptLen : Length of all the options in the         */
/*                : buffer                                              */
/*                : pu1Options - Pointer to the option array            */
/*  Output(s)     : Mutes the options fields in the buffer.             */
/*  Global(s)     : None.                                               */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/***********************************************************************/

INT1
Secv4ProcessOptionFields (tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4OptLen,
                          UINT1 *pu1Options)
{
    INT4                i4Index = 0;
    INT2                i2OptLen = 0;
    INT1                i1Option = 0;
    UINT1               au1TempOptions[SEC_IPV4_MAX_OPT_LEN];

    IPSEC_MEMSET (au1TempOptions, 0, SEC_IPV4_MAX_OPT_LEN);

    IPSEC_MEMCPY (au1TempOptions, pu1Options, u4OptLen);

    while ((i4Index < (INT4) u4OptLen)
           && ((i1Option = IPSEC_OPT_NUMBER (pu1Options[i4Index]))
               != IPSEC_OPT_EOL) && (i4Index < (SEC_IPV4_MAX_OPT_LEN - 1)))
    {
        i2OptLen = (INT2)
            ((i1Option ==
              IPSEC_OPT_NOP) ? 1 : (UINT1) (pu1Options[i4Index + 1]));

        if ((UINT2) (i4Index + i2OptLen) > u4OptLen)
        {
            SECv4_TRC (SECv4_ALL_FAILURE,
                       "Secv4ProcessOptionFields: Invalid Option Length\n");
            return SEC_FAILURE;
        }

        if (i1Option == IPSEC_OPT_NOP)
        {
            i4Index += i2OptLen;
            continue;
        }

        if (i2OptLen < 2)
        {
            SECv4_TRC (SECv4_ALL_FAILURE,
                       "Secv4ProcessOptionFields: Invalid Option Length\n");
            return SEC_FAILURE;
        }

        switch (i1Option)
        {
            case IPSEC_OPT_SSROUTE:
            case IPSEC_OPT_LSROUTE:
            case IPSEC_OPT_RROUTE:
            case IPSEC_OPT_TSTAMP:
            case IPSEC_OPT_TROUTE:
            case IPSEC_OPT_STREAM_IDENT:
            case IPSEC_OPT_MTU_PROBE:
            case IPSEC_OPT_MTU_REPLY:
            {
                if ((i2OptLen <
                     (SEC_IPV4_MAX_OPT_LEN - (SEC_IPV4_HEADER_SIZE + i4Index)))
                    && ((SEC_IPV4_HEADER_SIZE + i4Index) <
                        SEC_IPV4_MAX_OPT_LEN))
                {
                    IPSEC_MEMSET (&au1TempOptions
                                  [SEC_IPV4_HEADER_SIZE + i4Index], 0,
                                  (size_t) i2OptLen);
                }
                break;
            }
            default:
                break;
        }
        i4Index += i2OptLen;
    }
    if (IPSEC_COPY_OVER_BUF (pCBuf, (UINT1 *) au1TempOptions,
                             SEC_IPV4_HEADER_SIZE, u4OptLen) == BUF_FAILURE)
    {
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}

/*************************************************************************/
/*  Function Name : Secv4ConstructKey                                   */
/*  Description   : This function is used to construct hexa decimal key */
/*                : from display string                                 */
/*                :                                                     */
/*  Input(s)      : pu1InKey : Key in String Format                     */
/*                : u1Len    : Length of the DisplayString              */
/*                : pu1OutKey: Key in Hex Format                        */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/***********************************************************************/
INT1
Secv4ConstructKey (UINT1 *pu1InKey, UINT1 u1Len, UINT1 *pu1OutKey)
{
    INT4                i4Count1 = 0, i4Num1 = 0, i4Num2 = 0, i4Count2 = 0;

    if ((u1Len != 40) && (u1Len != 32) && (u1Len != 16) && (u1Len != 96) &&
        (u1Len != 48) && (u1Len != 64) && (u1Len != 20) && (u1Len != 8)
        && (u1Len != 128))
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4ConstructKey: Invalid Key Length\n");
        return SEC_FAILURE;
    }

    while (i4Count1 < u1Len)
    {
        i4Num1 = ((*(pu1InKey + i4Count1)) - '0');
        i4Num2 = ((*(pu1InKey + i4Count1 + 1)) - '0');
        if ((i4Num1 > 54) || (i4Num2 > 54))
        {
            SECv4_TRC (SECv4_ALL_FAILURE,
                       "Secv4ConstructKey: Configured Key Not In Hex\n");
            return SEC_FAILURE;
        }
        if (i4Num1 > 48)
        {
            i4Num1 = ((i4Num1 - 48) + 9);
        }
        if (i4Num2 > 48)
        {
            i4Num2 = ((i4Num2 - 48) + 9);
        }
        (*(pu1OutKey + i4Count2)) = (UINT1) ((i4Num1 * 16) + (i4Num2));
        i4Count1 = i4Count1 + 2;
        i4Count2++;
        i4Num1 = 0;
        i4Num2 = 0;
    }
    (*(pu1OutKey + i4Count2)) = '\0';

    return SEC_SUCCESS;
}

/*************************************************************************/
/*  Function Name : Secv4MuteIpHdr                                      */
/*  Description   : This function is used to mute fields in IP Header   */
/*                :                                                     */
/*  Input(s)      : IpHdr : IP Header                                   */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/***********************************************************************/
VOID
Secv4MuteIpHdr (t_IP_HEADER * IpHdr)
{
    IpHdr->u1Tos = 0;
    IpHdr->u2Fl_offs = 0;
    IpHdr->u1Ttl = 0;
    IpHdr->u2Cksum = 0;
    return;
}

/*************************************************************************/
/*  Function Name : Secv4GetFreeSAIdx                                   */
/*  Description   : This function is used to get free sa index          */
/*                :                                                     */
/*  Input(s)      : None                                                */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : u4SaIndex                                           */
/***********************************************************************/

UINT4
Secv4GetFreeSAIdx (VOID)
{
    gu4Secv4FreeSaIndex++;
    return (gu4Secv4FreeSaIndex);
}

/*************************************************************************/
/*  Function Name : Secv4GetAccessIndexByRange                          */
/*  Description   : This function is used to get the access index       */
/*                :                                                     */
/*  Input(s)      : None                                                */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : u4SaIndex                                           */
/***********************************************************************/

UINT4
Secv4GetAccessIndexByRange (UINT4 u4StartSrcAddr, UINT4 u4StartDestAddr)
{
    tSecv4Access       *pEntry = NULL;

    TMO_SLL_Scan (&Secv4AccessList, pEntry, tSecv4Access *)
    {
        if ((u4StartSrcAddr == (pEntry->u4SrcAddress & pEntry->u4SrcMask)) &&
            (u4StartDestAddr == (pEntry->u4DestAddress & pEntry->u4DestMask)))
        {
            return (pEntry->u4GroupIndex);
        }
        else if (((pEntry->u4SrcAddress == 0) &&
                  (pEntry->u4SrcMask == 0xffffffff)) &&
                 ((pEntry->u4DestAddress == 0) &&
                  (pEntry->u4DestMask == 0xffffffff)))
        {
            return (pEntry->u4GroupIndex);
        }
    }
    return SEC_FAILURE;
}

/*************************************************************************/
/*  Function Name : Secv4CheckNoOfOutBoundBytesSecured                   */
/*  Description   : This function checks whether the no of bytes secured */
/*                : exceeded                                             */
/*                :                                                      */
/*  Input(s)      : pPolicy :- Pointer to Policy                         */
/*                : pBufDesc :-Pointer to the Buffer                     */
/*  Output(s)     :                                                      */
/*  Global(s)     : None.                                                */
/*  Return Values : u4SaIndex                                            */
/*************************************************************************/

INT1
Secv4CheckNoOfOutBoundBytesSecured (tSecv4Policy * pPolicy,
                                    tIP_BUF_CHAIN_HEADER * pCBuf)
{

    UINT4               u4ByteCount = 0;
    UINT4               u4ThresHoldByteCount = 0;

    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pCBuf);

    if (pPolicy->pau1SaEntry[0] == NULL)
    {
        return (SEC_REJECT);
    }

    if (pPolicy->pau1SaEntry[0]->u4LifeTimeInBytes != 0)
    {
        u4ThresHoldByteCount = ((pPolicy->pau1SaEntry[0]->u4LifeTimeInBytes *
                                 SEC_THRESHOLD_PERCENT) / 100);
        pPolicy->pau1SaEntry[0]->u4ByteCount =
            (pPolicy->pau1SaEntry[0]->u4ByteCount + u4ByteCount);
        if (pPolicy->pau1SaEntry[0]->u4ByteCount >
            pPolicy->pau1SaEntry[0]->u4LifeTimeInBytes)
        {
            return (SEC_BYTES_SECURED_EXCEEDED);
        }

        else if (pPolicy->pau1SaEntry[0]->u4ByteCount >= u4ThresHoldByteCount)
        {
            return (SEC_THRESHOLD_REACHED);
        }
    }

    return (SEC_BYTES_SECURED_NOT_EXCEEDED);
}

/*************************************************************************/
/*  Function Name : Secv4CheckNoOfInBoundBytesSecured                    */
/*  Description   : This function checks whether the no of bytes secured */
/*                : exceeded                                             */
/*                :                                                      */
/*  Input(s)      : pPolicy :- Pointer to Policy                         */
/*                : pBufDesc :-Pointer to the Buffer                     */
/*  Output(s)     :                                                      */
/*  Global(s)     : None.                                                */
/*  Return Values : u4SaIndex                                            */
/*************************************************************************/
INT1
Secv4CheckNoOfInBoundBytesSecured (tSecv4Assoc * pSecv4Assoc,
                                   tIP_BUF_CHAIN_HEADER * pCBuf)
{

    UINT4               u4ByteCount = 0;
    UINT4               u4ThresHoldByteCount = 0;

    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pCBuf);

    if (pSecv4Assoc->u4LifeTimeInBytes != 0)
    {
        u4ThresHoldByteCount = ((pSecv4Assoc->u4LifeTimeInBytes *
                                 SEC_THRESHOLD_PERCENT) / 100);
        pSecv4Assoc->u4ByteCount = pSecv4Assoc->u4ByteCount + u4ByteCount;
        if (pSecv4Assoc->u4ByteCount > pSecv4Assoc->u4LifeTimeInBytes)
        {
            return (SEC_BYTES_SECURED_EXCEEDED);
        }
        else if (pSecv4Assoc->u4ByteCount >= u4ThresHoldByteCount)
        {
            return (SEC_THRESHOLD_REACHED);
        }
    }

    return (SEC_BYTES_SECURED_NOT_EXCEEDED);

}

/*************************************************************************/
/*  Function Name : Secv4AttachNewSaToPolicy                             */
/*  Description   : This function attaches newly negotiated SA's toPolicy*/
/*                :                                                      */
/*  Input(s)      : pPolicy     :- Pointer to Policy                     */
/*                : pu1SaBundle :-Pointer to the Sa Bundle               */
/*  Output(s)     :                                                      */
/*  Global(s)     : None.                                                */
/*  Return Values : u4SaIndex                                            */
/*************************************************************************/
VOID
Secv4AttachNewSaToPolicy (tSecv4Policy * pu1Policy,
                          UINT1 au1SaBundle[SEC_MAX_SA_BUNDLE_LEN])
{

    INT4                i4BackCount = -1;
    INT4                i4Num = 0;
    INT4                i4Tens = 10;
    UINT1               u1SaBundleLength = 0;
    tSecv4Assoc        *pau1NewSaEntry[SEC_MAX_BUNDLE_SA];
    UINT1               u1NewSaCount = 0;
    UINT4               u1Count = 0;
    UINT4               u4Time = 0;
    UINT4               u4Factor = 0;

    IPSEC_MEMSET (pau1NewSaEntry, 0, SEC_MAX_BUNDLE_SA);
    u1SaBundleLength = (UINT1) STRLEN (au1SaBundle);

    /* Extract the secassoc indicies form the sa bundle in string format
       and store them as integers */
    do
    {
        i4BackCount++;
        i4Num = 0;
        while ((i4BackCount < SEC_MAX_SA_BUNDLE_LEN) &&
               (au1SaBundle[i4BackCount] != '.') &&
               (au1SaBundle[i4BackCount] != '\0'))
        {
            i4Num = i4Num * i4Tens + (au1SaBundle[i4BackCount] - '0');
            i4BackCount++;
        }
        if (u1NewSaCount >= SEC_MAX_BUNDLE_SA)
        {
            break;
        }
        pau1NewSaEntry[u1NewSaCount] = Secv4AssocGetEntry ((UINT4) i4Num);

        pau1NewSaEntry[u1NewSaCount]->u4PolicyIndex = pu1Policy->u4PolicyIndex;
        u1NewSaCount++;
    }
    while (i4BackCount < u1SaBundleLength);

    for (u1Count = (SEC_MAX_SA_TO_POLICY - 1); u1Count != 0; u1Count--)
    {
        IPSEC_MEMCPY (&(pu1Policy->pau1NewSaEntry[u1Count][0]),
                      &(pu1Policy->pau1NewSaEntry[u1Count - 1][0]),
                      SEC_MAX_BUNDLE_SA);
        IPSEC_MEMCPY (&(pu1Policy->au1NewPolicySaBundle[u1Count][0]),
                      &(pu1Policy->au1NewPolicySaBundle[u1Count - 1][0]),
                      SEC_MAX_SA_BUNDLE_LEN);
        pu1Policy->au1NewSaCount[u1Count] =
            pu1Policy->au1NewSaCount[u1Count - 1];

    }

    if (pu1Policy->pau1NewSaEntry[0][0] != NULL)
    {
        Secv4StopTimer (&
                        (pu1Policy->pau1NewSaEntry[0][0]->Secv4AssocSoftTimer));
    }

    IPSEC_MEMCPY (&(pu1Policy->pau1NewSaEntry[0][0]),
                  pu1Policy->pau1SaEntry, SEC_MAX_BUNDLE_SA);
    IPSEC_MEMCPY (&(pu1Policy->au1NewPolicySaBundle[0][0]),
                  pu1Policy->au1PolicySaBundle, SEC_MAX_SA_BUNDLE_LEN);
    pu1Policy->au1NewSaCount[0] = pu1Policy->u1SaCount;

    Secv4StopTimer (&(pu1Policy->pau1NewSaEntry[0][0]->Secv4AssocSoftTimer));

    IPSEC_MEMCPY (pu1Policy->pau1SaEntry, pau1NewSaEntry, SEC_MAX_BUNDLE_SA);
    IPSEC_MEMCPY (pu1Policy->au1PolicySaBundle, au1SaBundle,
                  SEC_MAX_SA_BUNDLE_LEN);
    pu1Policy->u1SaCount = u1NewSaCount;

    OsixGetSysTime (&u4Time);
    u4Factor = (SEC_MIN_SOFTLIFETIME_FACTOR + (u4Time % 10));
    if (pu1Policy->pau1SaEntry[0]->u4LifeTime != 0)
    {
        u4Time = ((pu1Policy->pau1SaEntry[0]->u4LifeTime * u4Factor) / 100);
        pu1Policy->pau1SaEntry[0]->Secv4AssocSoftTimer.u4Param1 =
            pu1Policy->u4PolicyIndex;
        pu1Policy->pau1SaEntry[0]->Secv4AssocSoftTimer.u4SaIndex =
            pu1Policy->pau1SaEntry[0]->u4SecAssocIndex;
        Secv4StartTimer (&(pu1Policy->pau1SaEntry[0]->Secv4AssocSoftTimer),
                         SECv4_SA_SOFT_TIMER_ID, u4Time);

        pu1Policy->pau1SaEntry[0]->Secv4AssocHardTimer.u4Param1 =
            pu1Policy->u4PolicyIndex;
        pu1Policy->pau1SaEntry[0]->Secv4AssocHardTimer.u4SaIndex =
            pu1Policy->pau1SaEntry[0]->u4SecAssocIndex;
        Secv4StartTimer (&(pu1Policy->pau1SaEntry[0]->Secv4AssocHardTimer),
                         SECv4_SA_HARD_TIMER_ID,
                         pu1Policy->pau1SaEntry[0]->u4LifeTime);
    }
}

/*************************************************************************/
/*  Function Name : Secv4GetFreePolicyIdx                               */
/*  Description   : This function is used to get free policy index      */
/*                :                                                     */
/*  Input(s)      : None                                                */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : u4PolicyIndex                                       */
/***********************************************************************/

UINT4
Secv4GetFreePolicyIdx (VOID)
{
    gu4Secv4FreePolicyIndex++;
    return (gu4Secv4FreePolicyIndex);
}

/*************************************************************************/
/*  Function Name : Secv4GetFreeAccessIdx                               */
/*  Description   : This function is used to get free policy index      */
/*                :                                                     */
/*  Input(s)      : None                                                */
/*                :                                                     */
/*  Output(s)     :                                                     */
/*  Global(s)     : None.                                               */
/*  Return Values : u4AccessIndex                                       */
/***********************************************************************/

UINT4
Secv4GetFreeAccessIdx (VOID)
{
    gu4Secv4FreeAccessIndex++;
    return (gu4Secv4FreeAccessIndex);
}

/***********************************************************************/
/*  Function Name : Secv4DeleteSelectorEntries                         */
/*  Description   : This function deletes Selector Database            */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the selector data base                     */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4DeleteSelectorEntries ()
{

    tSecv4Selector     *pSecv4Selector = NULL;
    UINT4               u4Entries = 0, u4Count = 0;

    u4Entries = TMO_SLL_Count (&Secv4SelList);
    for (u4Count = 0; u4Count < u4Entries; u4Count++)
    {
        pSecv4Selector = (tSecv4Selector *) TMO_SLL_First (&Secv4SelList);
        if (pSecv4Selector == NULL)
        {
            break;
        }
        nmhSetFsipv4SelStatus ((INT4) pSecv4Selector->u4SelIndex,
                               (INT4) pSecv4Selector->u4IfIndex,
                               (INT4) pSecv4Selector->u4ProtoId,
                               (INT4) pSecv4Selector->u4SelAccessIndex,
                               (INT4) pSecv4Selector->u4Port,
                               (INT4) pSecv4Selector->u4PktDirection, DESTROY);
    }
}

/***********************************************************************/
/*  Function Name : Secv4DeleteSecAssocEntries                         */
/*  Description   : This function deletes security assoc Database      */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the security association data base         */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4DeleteSecAssocEntries ()
{

    UINT4               u4Entries = 0;
    UINT4               u4Count = 0;
    tSecv4Assoc        *pSecv4Assoc = NULL;

    u4Entries = TMO_SLL_Count (&Secv4AssocList);
    for (u4Count = 0; u4Count < u4Entries; u4Count++)
    {
        pSecv4Assoc = (tSecv4Assoc *) TMO_SLL_First (&Secv4AssocList);
        if (pSecv4Assoc == NULL)
        {
            break;
        }
        nmhSetFsipv4SecAssocStatus ((INT4) pSecv4Assoc->u4SecAssocIndex,
                                    DESTROY);
    }
}

/***********************************************************************/
/*  Function Name : Secv4DeleteSecPolicyEntries                        */
/*  Description   : This function deletes policy Database              */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the policy data base                       */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4DeleteSecPolicyEntries ()
{

    UINT4               u4Entries = 0;
    UINT4               u4Count = 0;
    tSecv4Policy       *pSecv4Policy = NULL;

    u4Entries = TMO_SLL_Count (&Secv4PolicyList);
    for (u4Count = 0; u4Count < u4Entries; u4Count++)
    {
        pSecv4Policy = (tSecv4Policy *) TMO_SLL_First (&Secv4PolicyList);
        if (pSecv4Policy == NULL)
        {
            break;
        }
        nmhSetFsipv4SecPolicyStatus ((INT4) pSecv4Policy->u4PolicyIndex,
                                     DESTROY);
    }
}

/***********************************************************************/
/*  Function Name : Secv4DeleteSecAccessEntries                        */
/*  Description   : This function deletes Access Database              */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the access data base                       */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4DeleteSecAccessEntries ()
{

    UINT4               u4Entries = 0;
    UINT4               u4Count = 0;
    tSecv4Access       *pSecv4Access = NULL;

    u4Entries = TMO_SLL_Count (&Secv4AccessList);
    for (u4Count = 0; u4Count < u4Entries; u4Count++)
    {
        pSecv4Access = (tSecv4Access *) TMO_SLL_First (&Secv4AccessList);
        if (pSecv4Access == NULL)
        {
            break;
        }
        nmhSetFsipv4SecAccessStatus ((INT4) pSecv4Access->u4GroupIndex,
                                     DESTROY);
    }
}

/***********************************************************************/
/*  Function Name : Secv4DeleteIPSecDataBase                           */
/*  Description   : This function deletes Selector,Access,Policy       */
/*                : and security association database                  */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes all the data bases                         */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4DeleteIPSecDataBase ()
{
    nmhSetFsipv4SecGlobalStatus (SEC_DISABLE);
    Secv4DeleteSecAssocEntries ();
    Secv4DeleteSecPolicyEntries ();
    Secv4DeleteSecAccessEntries ();
    Secv4DeleteSelectorEntries ();
    return;
}

/****************************************************************************/
/*  Function Name   : Secv4SelGetEntry                                       */
/*  Description     : This function returns the Selector Entry from the     */
/*                  : Selector List based on the Inputs                    */
/*                  : SecurityAssociation List using the SPI as Index      */
/*  Input(s)        : Index :Refers to the Incoming Index                  */
/*                  : ProtId:Refers to the Protocol                        */
/*                  : AccessIndex:Refers to the accessIndex            */
/*                  : i4Port:Refers to the application of IP               */
/*                  : i4Direction :Refers to the Direction of the Packet   */
/*  Output(s)       : Selector DataBase                                    */
/*  Returns         : Pointer to Selector Entry or Null                     */
/***************************************************************************/

tSecv4Selector     *
Secv4SelGetEntry (UINT4 u4SelIndex, INT4 i4PktDirection)
{
    tSecv4Selector     *pEntry = NULL;

    TMO_SLL_Scan (&Secv4SelList, pEntry, tSecv4Selector *)
    {
        if ((pEntry->u4SelIndex == u4SelIndex) &&
            (pEntry->u4PktDirection == (UINT4) i4PktDirection))
        {
            return pEntry;
        }
    }
    return NULL;
}

tSecv4Selector     *
Secv4SelGetEntry2 (UINT4 u4SelIfIndex, UINT4 u4SelAccessIndex, UINT4 u4ProtoId,
                   INT4 u4Port)
{
    tSecv4Selector     *pEntry = NULL;

    TMO_SLL_Scan (&Secv4SelList, pEntry, tSecv4Selector *)
    {
        if ((pEntry->u4IfIndex == u4SelIfIndex) &&
            (pEntry->u4SelAccessIndex == u4SelAccessIndex) &&
            (pEntry->u4ProtoId == u4ProtoId) &&
            (pEntry->u4Port == (UINT4) u4Port))
        {
            return pEntry;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   : Secv4SelGetFormattedEntry                            */
/*  Description     : This function returns the Selector Entry from the    */
/*                  : Formatted Selector List based on the Inputs          */
/*                  : SecurityAssociation List using the SPI as Index      */
/*  Input(s)        : Index :Refers to the Incoming Index                  */
/*                  : ProtId:Refers to the Protocol                        */
/*                  : AccessIndex:Refers to the InaccessIndex            */
/*                  : i4Port:Refers to the application of IP               */
/*                  : i4Direction :Refers to the Direction of the Packet   */
/*  Output(s)       : Formatted Selector DataBase                          */
/*  Returns         : Pointer to Selector Entry or NULL                     */
/***************************************************************************/
tSecv4Selector     *
Secv4SelGetFormattedEntry (UINT4 Index,
                           UINT4 u4ProtId, UINT4 u4AccessIndex, INT4 i4Port,
                           INT4 i4PktDirection)
{
    tSecv4Selector     *pEntry = NULL;
    UNUSED_PARAM (i4PktDirection);

    TMO_SLL_Scan (&Secv4SelList, pEntry, tSecv4Selector *)
    {
        if (((pEntry->u4IfIndex == Index)
             || (pEntry->u4IfIndex == SEC_ANY_IFINDEX)) &&
            (pEntry->u4SelAccessIndex == u4AccessIndex) &&
            ((pEntry->u4ProtoId == u4ProtId)
             || (pEntry->u4ProtoId == SEC_ANY_PROTOCOL)) &&
            ((pEntry->u4Port == (UINT4) i4Port)
             || (pEntry->u4Port == SEC_ANY_PORT)))
        {
            return pEntry;
        }
    }
    return NULL;
}

/* Get the matching selector. The If-Index is not checked here, but the
 * direction is used instead.
 */
tSecv4Selector     *
Secv4SelGetFormattedEntryDir (UINT4 Index,
                              UINT4 ProtId, UINT4 u4AccessIndex, INT4 i4Port,
                              INT4 i4PktDirection)
{
    tSecv4Selector     *pEntry = NULL;

    UNUSED_PARAM (Index);

    TMO_SLL_Scan (&Secv4SelList, pEntry, tSecv4Selector *)
    {
        if ((pEntry->u4SelAccessIndex == u4AccessIndex) &&
            ((pEntry->u4ProtoId == ProtId)
             || (pEntry->u4ProtoId == SEC_ANY_PROTOCOL)) &&
            ((pEntry->u4Port == (UINT4) i4Port)
             || (pEntry->u4Port == SEC_ANY_PORT)) &&
            ((pEntry->u4PktDirection == (UINT4) i4PktDirection)
             || (pEntry->u4PktDirection == SEC_ANY_DIRECTION)))
        {
            return pEntry;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv4AccessGetEntry                                  */
/*  Description     : This function returns the Access Entry from the       */
/*                  : Access List based on the Inputs                       */
/*  Input(s)        : u4AccessIndex                                         */
/*  Output(s)       : AccessDataBase                                        */
/*  Returns         : Pointer to Access Entry or NULL                     */
/***************************************************************************/

tSecv4Access       *
Secv4AccessGetEntry (UINT4 u4AccessIndex)
{

    tSecv4Access       *pEntry = NULL;
    TMO_SLL_Scan (&Secv4AccessList, pEntry, tSecv4Access *)
    {
        if (pEntry->u4GroupIndex == u4AccessIndex)
        {
            return pEntry;
        }
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv4PolicyGetEntry                                   */
/*  Description     : This function returns the Policy Entry from the       */
/*                  : Policy List based on the Inputs                      */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the PolicyIndex                    */
/*  Output(s)       : Policy DataBase                                      */
/*  Returns         : Pointer to Policy Entry or NULL                       */
/***************************************************************************/

tSecv4Policy       *
Secv4PolicyGetEntry (UINT4 Index)
{
    tSecv4Policy       *pEntry = NULL;
    TMO_SLL_Scan (&Secv4PolicyList, pEntry, tSecv4Policy *)
    {
        if (pEntry->u4PolicyIndex == Index)
            return pEntry;
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv4DeletePolicyPtrInSel                            */
/*  Description     : This function makes the policy pointer in selector    */
/*                  : table to be null whose policy index is Index          */
/*  Input(s)        :                                                       */
/*                  : u4Index:Refers to the PolicyIndex                     */
/*  Output(s)       : Void                                                  */
/*  Returns         : Null                                                  */
/****************************************************************************/

VOID
Secv4DeletePolicyPtrInSel (UINT4 Index)
{
    tSecv4Selector     *pEntry = NULL;
    TMO_SLL_Scan (&Secv4SelList, pEntry, tSecv4Selector *)
    {
        if (pEntry->u4SelPolicyIndex == Index)
            pEntry->pPolicyEntry = NULL;
    }
}

/****************************************************************************/
/*  Function Name   :  Secv4DeleteSAPtrInPolicy                             */
/*  Description     : This function makes the SA pointer in Policy          */
/*                  : table to be null.                                     */
/*  Input(s)        :                                                       */
/*                  : u4Index:Refers to the PolicyIndex                     */
/*  Output(s)       : Void                                                  */
/*  Returns         : Null                                                  */
/****************************************************************************/

VOID
Secv4DeleteSAPtrInPolicy (tSecv4Assoc * pSecAssoc)
{
    tSecv4Policy       *pEntry = NULL;
    UINT1               u1ReKeySaCount = 0;
    UINT1               u1SaCount = 0;
    UINT1               u1Count = 0;

    TMO_SLL_Scan (&Secv4PolicyList, pEntry, tSecv4Policy *)
    {
        if (pEntry->pau1SaEntry[u1SaCount] == pSecAssoc)
        {
            IPSEC_MEMCPY (pEntry->pau1SaEntry,
                          &(pEntry->pau1NewSaEntry[0][0]), SEC_MAX_BUNDLE_SA);
            IPSEC_MEMCPY (&(pEntry->au1PolicySaBundle),
                          &(pEntry->au1NewPolicySaBundle[0][0]),
                          SEC_MAX_SA_BUNDLE_LEN);
            pEntry->u1SaCount = pEntry->au1NewSaCount[0];
            for (u1ReKeySaCount = 0;
                 u1ReKeySaCount < (SEC_MAX_SA_TO_POLICY - 1); u1ReKeySaCount++)
            {
                IPSEC_MEMCPY (&(pEntry->pau1NewSaEntry[u1ReKeySaCount][0]),
                              &(pEntry->pau1NewSaEntry[u1ReKeySaCount + 1]
                                [0]), SEC_MAX_BUNDLE_SA);
                IPSEC_MEMCPY (&(pEntry->au1NewPolicySaBundle
                                [u1ReKeySaCount][0]),
                              &(pEntry->au1NewPolicySaBundle
                                [u1ReKeySaCount + 1][0]),
                              SEC_MAX_SA_BUNDLE_LEN);
                pEntry->au1NewSaCount[u1ReKeySaCount] =
                    pEntry->au1NewSaCount[u1ReKeySaCount + 1];
            }
            IPSEC_MEMSET (&(pEntry->
                            pau1NewSaEntry[SEC_MAX_SA_TO_POLICY - 1][0]),
                          0, SEC_MAX_BUNDLE_SA);
            IPSEC_MEMSET (&(pEntry->
                            au1NewPolicySaBundle[SEC_MAX_SA_TO_POLICY -
                                                 1][0]), 0,
                          SEC_MAX_SA_BUNDLE_LEN);
            pEntry->au1NewSaCount[SEC_MAX_SA_TO_POLICY - 1] = 0;
            return;
        }
    }
    TMO_SLL_Scan (&Secv4PolicyList, pEntry, tSecv4Policy *)
    {
        for (u1ReKeySaCount = 0; u1ReKeySaCount < SEC_MAX_SA_TO_POLICY;
             u1ReKeySaCount++)
        {
            if ((pEntry->pau1NewSaEntry[u1ReKeySaCount][0]) == pSecAssoc)
            {
                for (u1Count = u1ReKeySaCount; u1Count <
                     (SEC_MAX_SA_TO_POLICY - 1); u1Count++)
                {
                    IPSEC_MEMCPY (&(pEntry->pau1NewSaEntry[u1Count]
                                    [0]),
                                  &(pEntry->pau1NewSaEntry[u1Count + 1][0]),
                                  SEC_MAX_BUNDLE_SA);
                    IPSEC_MEMCPY (&(pEntry->
                                    au1NewPolicySaBundle[u1Count][0]),
                                  &(pEntry->
                                    au1NewPolicySaBundle[u1Count + 1][0]),
                                  SEC_MAX_SA_BUNDLE_LEN);
                    pEntry->au1NewSaCount[u1Count] =
                        pEntry->au1NewSaCount[u1Count + 1];

                }
                IPSEC_MEMSET (&
                              (pEntry->
                               pau1NewSaEntry[SEC_MAX_SA_TO_POLICY - 1][0]),
                              0, SEC_MAX_BUNDLE_SA);
                IPSEC_MEMSET (&
                              (pEntry->
                               au1NewPolicySaBundle[SEC_MAX_SA_TO_POLICY -
                                                    1][0]), 0,
                              SEC_MAX_SA_BUNDLE_LEN);
                pEntry->au1NewSaCount[SEC_MAX_SA_TO_POLICY - 1] = 0;
                return;
            }

        }
    }
    return;
}

/****************************************************************************/
/*  Function Name   :  Secv4AssocGetEntry                                 */
/*  Description     : This function returns the SecAssoc Entry from the     */
/*                  : SecAssoc List based on the Inputs                    */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the SecurityParameterIndex         */
/*  Output(s)       : SecurityAssoc DataBase                               */
/*  Returns         : Pointer to SecAssoc Entry or NULL                     */
/***************************************************************************/

tSecv4Assoc        *
Secv4AssocGetEntry (UINT4 Index)
{
    tSecv4Assoc        *pEntry = NULL;
    TMO_SLL_Scan (&Secv4AssocList, pEntry, tSecv4Assoc *)
    {
        if (pEntry->u4SecAssocIndex == Index)
            return pEntry;
    }
    return NULL;
}

/****************************************************************************/
/*  Function Name   :  Secv4SelectorCompareGetNext                         */
/*  Description     : This function is used by the GetNext routine of a    */
/*                  : Table to the get the Lexicographically smallest      */
/*                  : entry                                                */
/*  Input(s)        :                                                      */
/*                  : pEntry:Pointer to the Selector Entry                 */
/*                  : i4IfIndex:Refers to the Interface Index              */
/*                  : i4ProtId:Refers to the Protocol                      */
/*                  : i4AccIndex:Refers to the AccessIndex                 */
/*                  : i4Fsipv4SelPort:Refers to the application ofIP       */
/*                  : i4Fsipv4SelPktDirection :Refers to the Packet        */
/*                  : Direction                                            */
/*  Output(s)       : The Next Small Index                                 */
/*  Returns         : Returns the Difference between the first mismatch    */
/*                  : Index                                                */
/***************************************************************************/
INT4
Secv4SelectorCompareGetNext (tSecv4Selector * pEntry, INT4 i4SelIndex,
                             INT4 i4IfIndex, INT4 i4ProtId, INT4 i4AccIndex,
                             INT4 i4Fsipv4SelPort, INT4 i4Fsipv4SelPktDirection)
{

    if (pEntry->u4SelIndex != (UINT4) i4SelIndex)
        return (INT4) (pEntry->u4SelIndex - (UINT4) i4SelIndex);
    if (pEntry->u4IfIndex != (UINT4) i4IfIndex)
        return (INT4) (pEntry->u4IfIndex - (UINT4) i4IfIndex);
    if (pEntry->u4ProtoId != (UINT4) i4ProtId)
        return (INT4) (pEntry->u4ProtoId - (UINT4) i4ProtId);
    if (pEntry->u4Port != (UINT4) i4Fsipv4SelPort)
        return (INT4) (pEntry->u4Port - (UINT4) i4Fsipv4SelPort);
    if (pEntry->u4PktDirection != (UINT4) i4Fsipv4SelPktDirection)
        return (INT4) (pEntry->u4PktDirection -
                       (UINT4) i4Fsipv4SelPktDirection);
    if (pEntry->u4SelAccessIndex != (UINT4) i4AccIndex)
        return (INT4) (pEntry->u4SelAccessIndex - (UINT4) i4AccIndex);
    /* Lexicographically Equal */

    return 0;
}

/****************************************************************************/
/*  Function Name   :  Secv4SelectorCompare                                  */
/*  Description     : This function is used by the GetFirst routine of a   */
/*                  : Table to the get the Lexicographically smallest      */
/*                  : entry                                                */
/*  Input(s)        :                                                      */
/*                  : pEntry:Pointer to the Selector Entry                   */
/*                  : pNextEntry:Refers to the outgoing node                */
/*                  :                                                      */
/*  Output(s)       : The Next Small Index                                 */
/*  Returns         : Returns the Difference between the first mismatch    */
/*                  : Index                                                */
/***************************************************************************/
INT4
Secv4SelectorCompare (tSecv4Selector * pEntry, tSecv4Selector * pNextEntry)
{
    if (pNextEntry != NULL)
    {
        if (pEntry->u4SelIndex != pNextEntry->u4SelIndex)
            return (INT4) (pEntry->u4SelIndex - pNextEntry->u4SelIndex);
        if (pEntry->u4IfIndex != pNextEntry->u4IfIndex)
            return (INT4) (pEntry->u4IfIndex - pNextEntry->u4IfIndex);
        if (pEntry->u4ProtoId != pNextEntry->u4ProtoId)
            return (INT4) (pEntry->u4ProtoId - pNextEntry->u4ProtoId);
        if (pEntry->u4Port != pNextEntry->u4Port)
            return (INT4) (pEntry->u4Port - pNextEntry->u4Port);
        if (pEntry->u4PktDirection != pNextEntry->u4PktDirection)
            return (INT4) (pEntry->u4PktDirection - pNextEntry->u4PktDirection);
        if (pEntry->u4SelAccessIndex != pNextEntry->u4SelAccessIndex)
            return (INT4) (pEntry->u4SelAccessIndex -
                           pNextEntry->u4SelAccessIndex);
    }
    /* Lexicographically Equal */
    return (0);
}

/****************************************************************************/
/*  Function Name   :  Secv4GetAssocEntry                              */
/*  Description     : This function returns the SecAssoc Entry from the     */
/*                  : SecAssoc List based on the Inputs                    */
/*  Input(s)        :                                                      */
/*                  : u4Index:Refers to the SecurityParameterIndex         */
/*                  : DestAddr:Refers to Ipv4 Destination Address          */
/*                  : u1Protocol:Refers to the Protocol                    */
/*  Output(s)       : SecurityAssoc DataBase                               */
/*  Returns         : Pointer to SecAssoc Entry or NULL                     */
/***************************************************************************/
tSecv4Assoc        *Secv4GetAssocEntry
    (UINT4 u4SpiIndex, UINT4 DestAddr, UINT1 u1Protocol)
{
    tSecv4Assoc        *pEntry = NULL;

    TMO_SLL_Scan (&Secv4AssocList, pEntry, tSecv4Assoc *)
    {
        if ((pEntry->u4SecAssocSpi == u4SpiIndex)
            && (pEntry->u1SecAssocProtocol == u1Protocol)
            && (IPSEC_MEMCMP (&pEntry->u4SecAssocDestAddr,
                              &DestAddr, sizeof (UINT4)) == 0))
        {
            return pEntry;
        }
    }
    return NULL;
}

/***************************************************************************/
/*  Function Name   : Secv4UpdateSecAssocSize                                */
/*  Description     : This function updates the size for each secasso      */
/*                  : entry in the SecAssoc List                           */
/*  Inputs          :None                                                  */
/*  Outputs         :Updates the size in secassoc entry base on algo       */
/*  Returns         :None                                                  */
/***************************************************************************/
VOID
Secv4UpdateSecAssocSize (VOID)
{
    tSecv4Assoc        *pEntry = NULL;

    TMO_SLL_Scan (&Secv4AssocList, pEntry, tSecv4Assoc *)
    {

        if (pEntry->u1SecAssocMode == SEC_TUNNEL)
        {
            pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_IPV4_HEADER_SIZE);
        }
        if (pEntry->u1SecAssocProtocol == SEC_ESP)
        {
            if ((pEntry->u1SecAssocAhAlgo == SEC_DES_CBC) ||
                (pEntry->u1SecAssocAhAlgo == SEC_3DES_CBC))
            {
                pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_ESP_DES_KEY_LEN);
            }
            pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_ESP_SIZE);
        }
        else
        {
            pEntry->u2Size = (UINT2) (pEntry->u2Size + SEC_AH_SIZE);
        }
        if (pEntry->u1SecAssocAhAlgo != SEC_NULLAHALGO)
            pEntry->u2Size =
                (UINT2) (pEntry->u2Size + pEntry->u1SecAhAlgoDigestSize);
    }
}

/****************************************************************************/
/*  Function Name   :  Secv4ConstSecList                                   */
/*  Description     : This function is used to construct a                 */
/*                  : Lexicographically ordered selector list              */
/*                  :                                                      */
/*  Input(s)        : None                                                 */
/*                  :                                                      */
/*  Output(s)       : Formatted Selector List                              */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                           */
/***************************************************************************/
INT1
Secv4ConstSecList (VOID)
{
    tSecv4Selector     *pSecSelIf = NULL;
    tSecv4Selector     *pNextSecSelIf = NULL;
    tSecv4Selector     *pFirstSecSelIf = NULL;
    UINT4               u4Count = 0;
    UINT4               u4SelCount = 0;
    UINT1               Flag = 0;

    /* Get the First Node in the Selector List */
    pSecSelIf = (tSecv4Selector *) TMO_SLL_First (&Secv4SelList);

    /* If the List is empty return success */
    if (pSecSelIf == NULL)
    {
        return (SEC_SUCCESS);
    }

    /* Get the Count of the entries in the selector list */
    u4SelCount = (TMO_SLL_Count (&Secv4SelList));

    /* Allocate memory for storing the first entry */
    if (MemAllocateMemBlock (SECv4_SEL_MEMPOOL,
                             (UINT1 **) (VOID *) &pFirstSecSelIf) !=
        MEM_SUCCESS)
    {
        return (SEC_FAILURE);
    }
    pFirstSecSelIf->u4SelIndex = pSecSelIf->u4SelIndex;
    pFirstSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
    pFirstSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
    pFirstSecSelIf->u4Port = pSecSelIf->u4Port;
    pFirstSecSelIf->u4PktDirection = pSecSelIf->u4PktDirection;
    pFirstSecSelIf->u4SelPolicyIndex = pSecSelIf->u4SelPolicyIndex;
    pFirstSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
    pFirstSecSelIf->u1SelFilterFlag = pSecSelIf->u1SelFilterFlag;
    pFirstSecSelIf->u4SelAccessIndex = pSecSelIf->u4SelAccessIndex;

    if (u4SelCount == 1)
    {
        TMO_SLL_Add (&Secv4SelFormattedList, (tTMO_SLL_NODE *) pFirstSecSelIf);
        return (SEC_SUCCESS);
    }

    /* If there are more than one entry find the smallest entry in the
       list */

    TMO_SLL_Scan (&Secv4SelList, pSecSelIf, tSecv4Selector *)
    {
        if (Secv4SelectorCompare (pSecSelIf, pFirstSecSelIf) < 0)
        {
            pFirstSecSelIf->u4SelIndex = pSecSelIf->u4SelIndex;
            pFirstSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
            pFirstSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
            pFirstSecSelIf->u4Port = pSecSelIf->u4Port;
            pFirstSecSelIf->u4PktDirection = pSecSelIf->u4PktDirection;
            pFirstSecSelIf->u4SelPolicyIndex = pSecSelIf->u4SelPolicyIndex;
            pFirstSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
            pFirstSecSelIf->u1SelFilterFlag = pSecSelIf->u1SelFilterFlag;
            pFirstSecSelIf->u4SelAccessIndex = pSecSelIf->u4SelAccessIndex;
        }
    }

    /* Add the Least entry Found */
    TMO_SLL_Add (&Secv4SelFormattedList, (tTMO_SLL_NODE *) pFirstSecSelIf);

    /* Proceed for the next smallest entry */
    for (u4Count = 0; u4Count < u4SelCount; u4Count++)
    {

        Flag = SEC_NOT_FOUND;

        TMO_SLL_Scan (&Secv4SelList, pSecSelIf, tSecv4Selector *)
        {

            if ((Secv4SelectorCompare (pSecSelIf, pFirstSecSelIf)) > 0)
            {

                if (Flag == SEC_NOT_FOUND)
                {
                    Flag = SEC_FOUND;
                    if (pNextSecSelIf != NULL)
                    {
                        IPSEC_MEMSET (pNextSecSelIf, 0,
                                      sizeof (tSecv4Selector));
                        if (MemReleaseMemBlock
                            (SECv4_SEL_MEMPOOL,
                             (UINT1 *) pNextSecSelIf) != MEM_SUCCESS)
                        {
                            return (SEC_FAILURE);
                        }
                    }

                    if (MemAllocateMemBlock (SECv4_SEL_MEMPOOL,
                                             (UINT1 **) (VOID *) &pNextSecSelIf)
                        != MEM_SUCCESS)
                    {
                        return (SEC_FAILURE);
                    }
                    pNextSecSelIf->u4SelIndex = pSecSelIf->u4SelIndex;
                    pNextSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
                    pNextSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
                    pNextSecSelIf->u4Port = pSecSelIf->u4Port;
                    pNextSecSelIf->u4PktDirection = pSecSelIf->u4PktDirection;
                    pNextSecSelIf->u4SelPolicyIndex =
                        pSecSelIf->u4SelPolicyIndex;
                    pNextSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
                    pNextSecSelIf->u1SelFilterFlag = pSecSelIf->u1SelFilterFlag;
                    pNextSecSelIf->u4SelAccessIndex =
                        pSecSelIf->u4SelAccessIndex;
                }
                else
                {

                    if ((Secv4SelectorCompare (pSecSelIf, pNextSecSelIf)) < 0)
                    {
                        if (pNextSecSelIf != NULL)
                        {
                            IPSEC_MEMSET (pNextSecSelIf, 0,
                                          sizeof (tSecv4Selector));
                            if (MemReleaseMemBlock (SECv4_SEL_MEMPOOL,
                                                    (UINT1 *) pNextSecSelIf) !=
                                MEM_SUCCESS)
                            {
                                return (SEC_FAILURE);
                            }
                        }

                        if (MemAllocateMemBlock (SECv4_SEL_MEMPOOL,
                                                 (UINT1 **) (VOID *)
                                                 &pNextSecSelIf) != MEM_SUCCESS)
                        {
                            return (SEC_FAILURE);
                        }
                        pNextSecSelIf->u4SelIndex = pSecSelIf->u4SelIndex;
                        pNextSecSelIf->u4IfIndex = pSecSelIf->u4IfIndex;
                        pNextSecSelIf->u4ProtoId = pSecSelIf->u4ProtoId;
                        pNextSecSelIf->u4Port = pSecSelIf->u4Port;
                        pNextSecSelIf->
                            u4PktDirection = pSecSelIf->u4PktDirection;
                        pNextSecSelIf->
                            u4SelPolicyIndex = pSecSelIf->u4SelPolicyIndex;
                        pNextSecSelIf->pPolicyEntry = pSecSelIf->pPolicyEntry;
                        pNextSecSelIf->
                            u1SelFilterFlag = pSecSelIf->u1SelFilterFlag;
                        pNextSecSelIf->u4SelAccessIndex =
                            pSecSelIf->u4SelAccessIndex;
                    }
                }
            }
        }

        if ((Flag == SEC_NOT_FOUND) && (u4Count >= u4SelCount))
            return (SEC_FAILURE);
        if (Flag == SEC_NOT_FOUND)
            continue;
        TMO_SLL_Add (&Secv4SelFormattedList, (tTMO_SLL_NODE *) pNextSecSelIf);
        pFirstSecSelIf = pNextSecSelIf;
        pNextSecSelIf = NULL;
    }
    pFirstSecSelIf = NULL;
    return (SEC_SUCCESS);
}

/***************************************************************************/
/*  Function Name   : Secv4ConstAccessList                                 */
/*  Description     : This function is used to construct a                 */
/*                  : Lexicographically ordered Access list. The ordering  */
/*                  : is based on the access index                         */
/*                  :                                                      */
/*  Input(s)        : None                                                 */
/*                  :                                                      */
/*  Output(s)       : Formatted Access List                                */
/*  Returns         : None                                                 */
/***************************************************************************/

VOID
Secv4ConstAccessList (VOID)
{
    tSecv4Access       *pEntry;
    tSecv4Access       *pPrevEntry = NULL;
    tSecv4Access        TmpEntry;
    UINT4               u4AccessCount;
    UINT4               u4Count;

    /* Check if any nodes are present in the list */
    pPrevEntry = (tSecv4Access *) TMO_SLL_First (&Secv4AccessList);
    if (pPrevEntry == NULL)
    {
        return;
    }

    /* Find out the number of entries in the access list */
    u4AccessCount = TMO_SLL_Count (&Secv4AccessList);
    /* Order the access list entries basing on the access index */

    for (u4Count = 0; u4Count < u4AccessCount; u4Count++)
    {
        pPrevEntry = (tSecv4Access *) TMO_SLL_First (&Secv4AccessList);
        TMO_SLL_Scan (&Secv4AccessList, pEntry, tSecv4Access *)
        {
            if (pEntry->u4GroupIndex < pPrevEntry->u4GroupIndex)
            {
                TmpEntry.u4GroupIndex = pEntry->u4GroupIndex;
                TmpEntry.u4SrcAddress = pEntry->u4SrcAddress;
                TmpEntry.u4SrcMask = pEntry->u4SrcMask;
                TmpEntry.u4DestAddress = pEntry->u4DestAddress;
                TmpEntry.u4DestMask = pEntry->u4DestMask;
                pEntry->u4GroupIndex = pPrevEntry->u4GroupIndex;
                pEntry->u4SrcAddress = pPrevEntry->u4SrcAddress;
                pEntry->u4SrcMask = pPrevEntry->u4SrcMask;
                pEntry->u4DestAddress = pPrevEntry->u4DestAddress;
                pEntry->u4DestMask = pPrevEntry->u4DestMask;
                pPrevEntry->u4GroupIndex = TmpEntry.u4GroupIndex;
                pPrevEntry->u4SrcAddress = TmpEntry.u4SrcAddress;
                pPrevEntry->u4SrcMask = TmpEntry.u4SrcMask;
                pPrevEntry->u4DestAddress = TmpEntry.u4DestAddress;
                pPrevEntry->u4DestMask = TmpEntry.u4DestMask;
            }
            pPrevEntry = pEntry;
        }
    }

    return;
}

/***************************************************************************/
/*  Function Name   : Secv4DeleteFormattedSelectorEntries                  */
/*  Description     : This function is used to delete formatted            */
/*                  : selector entries                                     */
/*                  :                                                      */
/*  Input(s)        : None                                                 */
/*                  :                                                      */
/*  Output(s)       : Empty Formatted Selector List                        */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                           */
/***************************************************************************/

INT1
Secv4DeleteFormattedSelectorEntries ()
{

    tSecv4Selector     *pSecv4FormatSel = NULL;
    UINT4               u4Entries = 0;
    UINT4               u4Count = 0;

    u4Entries = TMO_SLL_Count (&Secv4SelFormattedList);
    for (u4Count = 0; u4Count < u4Entries; u4Count++)
    {

        TMO_SLL_Scan (&Secv4SelFormattedList, pSecv4FormatSel, tSecv4Selector *)
        {
            if (pSecv4FormatSel != NULL)
            {
                break;
            }
        }
        if (pSecv4FormatSel != NULL)
        {

            TMO_SLL_Delete (&Secv4SelFormattedList,
                            (tTMO_SLL_NODE *) pSecv4FormatSel);
            IPSEC_MEMSET (pSecv4FormatSel, 0, sizeof (tSecv4Selector));
            if (MemReleaseMemBlock (SECv4_SEL_MEMPOOL,
                                    (UINT1 *) pSecv4FormatSel) != MEM_SUCCESS)
            {
                return (SEC_FAILURE);
            }

        }
    }
    return (SEC_SUCCESS);
}

/***************************************************************************/
/*  Function Name   : Secv4CheckOverlappingAccessList                      */
/*  Description     : This function is returns failure if the new entry    */
/*                  : overlaps with the old entry                          */
/*                  :                                                      */
/*  Input(s)        : None                                                 */
/*                  :                                                      */
/*  Output(s)       : Deletes the old entry                                */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                           */
/***************************************************************************/
UINT1
Secv4CheckOverlappingAccessList (tSecv4Access * pEntry)
{
    tSecv4Access       *pCurrEntry = NULL;
    UINT4               u4MinSrcMask;
    UINT4               u4MinDestMask;

    TMO_SLL_Scan (&Secv4AccessList, pCurrEntry, tSecv4Access *)
    {
        /* To avoid the same entry check */
        if (pEntry->u4GroupIndex != pCurrEntry->u4GroupIndex)
        {

            u4MinSrcMask = (pEntry->u4SrcMask < pCurrEntry->u4SrcMask) ?
                pEntry->u4SrcMask : pCurrEntry->u4SrcMask;

            u4MinDestMask = (pEntry->u4DestMask < pCurrEntry->u4DestMask) ?
                pEntry->u4DestMask : pCurrEntry->u4DestMask;

            /* if check is for the case when pEntry->u4SrcAddress == 0
               Eg :- Database contains ACL1 :-
               192.168.1.0/255.255.255.0 0.0.0.0/0.0.0.0 
               and going to add ACL2:-  
               0.0.0.0/0.0.0.0  192.168.1.0/255.255.255.0
               This is a valid entry.
               In this case it is enough to 
               verify whether Destination address is overlapping or not.
             */

            if ((pEntry->u4SrcAddress == 0) && (u4MinDestMask != 0) &&
                (pEntry->u4DestAddress & u4MinDestMask) ==
                (pCurrEntry->u4DestAddress & u4MinDestMask))
            {
                return (SEC_FAILURE);
            }
            /* if check is for the case when pEntry->u4DestAddress == 0
               Eg :-
               Database contains ACL1 :-
               0.0.0.0/0.0.0.0 192.168.1.0/255.255.255.0
               and going to add ACL2 :- 
               192.168.1.0/255.255.255.0 0.0.0.0/0.0.0.0
               This is a valid entry.
               In this case it is enough to
               verify whether Source address is overlapping or not.
             */

            if ((pEntry->u4DestAddress == 0) && (u4MinSrcMask != 0) &&
                (pEntry->u4SrcAddress & u4MinSrcMask) ==
                (pCurrEntry->u4SrcAddress & u4MinSrcMask))
            {
                return (SEC_FAILURE);
            }

            /* if check is for the case when pEntry->u4DestAddress != 0 
               and pEntry->u4SrcAddress != 0 */
            if (pEntry->u4SrcAddress != 0 && pEntry->u4DestAddress != 0)
            {
                u4MinSrcMask = (pEntry->u4SrcMask < pCurrEntry->u4SrcMask) ?
                    pEntry->u4SrcMask : pCurrEntry->u4SrcMask;

                u4MinDestMask = (pEntry->u4DestMask < pCurrEntry->u4DestMask) ?
                    pEntry->u4DestMask : pCurrEntry->u4DestMask;

                if (((pEntry->u4SrcAddress & u4MinSrcMask) ==
                     (pCurrEntry->u4SrcAddress & u4MinSrcMask)) &&
                    ((pEntry->u4DestAddress & u4MinDestMask) ==
                     (pCurrEntry->u4DestAddress & u4MinDestMask)))
                {
                    return (SEC_FAILURE);
                }
            }
        }
    }
    return (SEC_SUCCESS);

}

/***************************************************************************/
/*  Function Name   : Secv4HandleFailueAfterTasklet                        */
/*  Description     : This function is handles the failure void function   */
/*                  : like Secv4TaskletSchedFn.                            */
/*                  : Update statistics                                    */
/*  Input(s)        : None                                                 */
/*  Output(s)       : None                                                 */
/*  Returns         : None                                                 */
/***************************************************************************/
VOID
Secv4HandleFailueAfterCryptoProcess (tSecv4TaskletData * pTemp)
{
    if (pTemp->u4Action == SEC_ENCODE)
    {
        Secv4UpdateIfStats (pTemp->u4IfIndex, SEC_OUTBOUND, SEC_FILTER);
        if (pTemp->pSaEntry != NULL)
        {
            Secv4UpdateAhEspStats (pTemp->u4IfIndex,
                                   pTemp->pSaEntry->u1SecAssocProtocol,
                                   SEC_OUTBOUND, SEC_FILTER);
        }
    }
    else if (pTemp->u4Action == SEC_DECODE)
    {
        Secv4UpdateIfStats (pTemp->u4IfIndex, SEC_INBOUND, SEC_FILTER);
        if (pTemp->pSaEntry != NULL)
        {

            Secv4UpdateAhEspStats (pTemp->u4IfIndex,
                                   pTemp->pSaEntry->u1SecAssocProtocol,
                                   SEC_INBOUND, SEC_FILTER);
        }
    }
    if (pTemp->pCBuf != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pTemp->pCBuf, FALSE);
    }
    if (pTemp->pSaEntry != NULL)
    {

        if ((pTemp->u4Mode == AUTH) &&
            (pTemp->pSaEntry->u1SecAssocMode == SEC_TRANSPORT))
        {
            if (pTemp->pu1OptBuf != NULL)
            {
                Secv4ReleaseRcvdIPOpts (pTemp->pu1OptBuf);
            }
        }
    }
    Secv4UnLock ();

    MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pTemp);
}

/*************************************************************************/
/*  Function Name : Secv4IPCalcChkSum                                    */
/*  Description   : This function calculates teh IP checksum             */
/*                :                                                      */
/*  Input(s)      : pBuf   -  Pointer to Pkt Rcvd                        */
/*                : u4Size -  Size of the Pkt Rcvd                       */
/*  Output(s)     : None                                                 */
/*  Return Values : Calculated Check Sum                                 */
/*************************************************************************/
UINT2
Secv4IPCalcChkSum (UINT1 *pBuf, UINT4 u4Size)
{

    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) (void *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }
    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (IPSEC_NTOHS (u2Tmp));
}

/***************************************************************************/
/*  Function Name   : Secv4GetAssocEntryCount                                 */
/*  Description     : This function returns the total active SAs           */
/*  Input(s)        : u4IPSecSAsActive :Refers to the SA count             */
/*  Output(s)       : None                                                 */
/*  Returns         : None                                                 */
/***************************************************************************/

VOID
Secv4GetAssocEntryCount (UINT4 *u4IPSecSAsActive)
{

    *u4IPSecSAsActive = TMO_SLL_Count (&Secv4AssocList);
}

VOID
Secv4DeleteAssoc (UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT4 u4IfIndex,
                  UINT4 u4Protocol, UINT2 u2LocPort, UINT2 u2RemPort)
{
    tSecv4Policy       *pSecv4Policy = NULL;
    tSecv4Selector     *pSelEntry = NULL;
    UINT4               u4AccessIndex = 0;
    UINT4               u1PktDirection = SEC_OUTBOUND;
    UINT4               u4AssocEntryCount = 0;
    UINT4               u4Count = 0;

    Secv4GetAssocEntryCount (&u4AssocEntryCount);
    u4AccessIndex = Secv4GetAccessIndex (u4SrcAddr, u4DestAddr, u2LocPort,
                                         u2RemPort, (UINT2) u4Protocol);
    if (u4AccessIndex != SEC_FAILURE)
    {
        pSelEntry =
            Secv4SelGetFormattedEntry (u4IfIndex, u4Protocol,
                                       u4AccessIndex, (INT4) u2LocPort,
                                       (INT4) u1PktDirection);
        if (pSelEntry != NULL)
        {
            pSecv4Policy = pSelEntry->pPolicyEntry;
#ifdef IKE_WANTED
            for (u4Count = 0; u4Count < u4AssocEntryCount; u4Count++)
            {
                if (pSecv4Policy->pau1SaEntry[0] != NULL)
                {
                    Secv4IntimateIkeDeletionOfSa (pSecv4Policy,
                                                  pSecv4Policy->
                                                  pau1SaEntry[0]->
                                                  u4SecAssocIndex);
                }
            }
#endif
        }
    }
}

/***************************************************************************/
/*  Function Name   : Secv4GetIfMtu                                        */
/*  Description     : This function returns the Interface Mtu              */
/*  Input(s)        : u4IfIndex - IfIndex for which the mtu is required    */
/*  Output(s)       : None                                                 */
/*  Returns         : None                                                 */
/***************************************************************************/
INT4
Secv4GetIfMtu (UINT4 u4IfIndex, UINT4 *pu4IfMtu)
{
    if (SecUtilGetIfMtu (u4IfIndex, pu4IfMtu) == CFA_FAILURE)
    {
        return (CFA_FAILURE);
    }
    return CFA_SUCCESS;
}

/***************************************************************************/
/*  Function Name   : Secv4SetIkeVersion                                   */
/*  Description     : This function Sets the IKE version That should be    */
/*                    Used for this Policy.                                */
/*  Input(s)        : u4PolicyIndex - Policy Index.                        */
/*                    u1IkeVersion  - Version of IKE.                      */
/*  Output(s)       : None                                                 */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                           */
/***************************************************************************/
INT1
Secv4SetIkeVersion (UINT4 u4PolicyIndex, UINT1 u1IkeVersion)
{
    tSecv4Policy       *pSecPolicyIf = NULL;
    pSecPolicyIf = Secv4PolicyGetEntry (u4PolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SEC_FAILURE);
    }
    pSecPolicyIf->u1IkeVersion = u1IkeVersion;
    return (SEC_SUCCESS);
}

/***************************************************************************/
/*  Function Name   : Secv4SetAccessListInfo                               */
/*  Description     : This function Sets the Protocol and Port Info in the */
/*                    Selector database.                                   */
/*  Input(s)        : u4AccessListIndex - Access List Index.               */
/*                  : u2LocalStartPort  - Local Start Port                 */
/*                  : u2LocalEndPort    -  Local End Port                  */
/*                  : u2RemoteStartPort - Remote Start Port                */
/*                  : u2RemoteEndPort   - Remote End Port                  */
/*                  : u4Protocol        - IP Protocol                      */
/*  Output(s)       : None                                                 */
/*  Returns         : SEC_SUCCESS/SEC_FAILURE                              */
/***************************************************************************/
INT1
Secv4SetAccessListInfo (UINT4 u4AccessListIndex, UINT2 u2LocalStartPort,
                        UINT2 u2LocalEndPort, UINT2 u2RemoteStartPort,
                        UINT2 u2RemoteEndPort, UINT4 u4Protocol)
{
    tSecv4Access       *pSecAccIf = NULL;

    /* get an entry from the outaccess list */
    pSecAccIf = Secv4AccessGetEntry (u4AccessListIndex);
    if (pSecAccIf != NULL)
    {
        pSecAccIf->u4Protocol = u4Protocol;
        pSecAccIf->u2LocalStartPort = u2LocalStartPort;
        pSecAccIf->u2LocalEndPort = u2LocalEndPort;
        pSecAccIf->u2RemoteStartPort = u2RemoteStartPort;
        pSecAccIf->u2RemoteEndPort = u2RemoteEndPort;
        return SEC_SUCCESS;
    }
    return SEC_FAILURE;
}

/***************************************************************************
 * Function Name    :  SecUtilGetGlobalStatus
 * Description      :  This function returns the global IPSEC status
 *
 * Input (s)        :  None.
 *
 * Output (s)       :  None.
 * Returns          :  SEC_ENABLE/SEC_DISABLE.
 ***************************************************************************/
INT1
SecUtilGetGlobalStatus (VOID)
{
    return (INT1) gSecv4Status;
}

/*****************************************************************************/
/* Function Name      : Secv4GetBypassCapability                             */
/*                                                                           */
/* Description        : This function is used to get the IPsecv4 bypass      */
/*                      capability                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1Secv4BypassCrypto                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT1
Secv4GetBypassCapability (INT4 *pi4RetValFsVpnGlobalStatus)
{
    *pi4RetValFsVpnGlobalStatus = gu1Secv4BypassCrypto;
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Secv4SetBypassCapability                             */
/*                                                                           */
/* Description        : This function is used to set the bypass capability   */
/*                      for IPsecv4  Semaphore                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu1Secv4BypassCrypto                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT1
Secv4SetBypassCapability (INT4 i4SetValFsVpnGlobalStatus)
{
    gu1Secv4BypassCrypto = (UINT1) i4SetValFsVpnGlobalStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function     : Secv4ArpResolve
*
* Description  : To check whether the entry is available in the Arp cache.
*
* Input        : u4IpAddr - IpAddress to be resolved.
*
* Output       : pi1Hw_addr -Hardware address.
*                pu1EncapType - EncapType for that Entry.
*
* Returns      : ARP_SUCCESS/ARP_FAILURE
*
***************************************************************************/
INT1
Secv4ArpResolve (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType)
{
    if (ArpResolve (u4IpAddr, pi1Hw_addr, pu1EncapType) == ARP_FAILURE)
    {
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}
