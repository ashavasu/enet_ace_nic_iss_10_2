/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4kern.c,v 1.13 2015/01/19 11:35:15 siva Exp $
 *
 * Description: This has functions for Processing the IOCTL Call 
 ***********************************************************************/
#ifndef _SECV4KERN_C_
#define _SECV4KERN_C_

#include "secv4com.h"
#include "secv4io.h"
#include "cli.h"
#include "secv4winc.h"
#include "secv4kstubs.h"

extern UINT4        gu4CopyModIdx;
extern UINT4        gu4SecReadModIdx;
/***********************************************************************/
/*  Function Name : Secv4Ioctl                                         */
/*  Description   : This function Receives the IOCTL from kernel driver*/
/*                :and process the kernel message                      */
/*                :                                                    */
/*  Input(s)      : p - Kernel message                                 */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :None                                                */
/***********************************************************************/

int
Secv4Ioctl (unsigned long p)
{
    tIkeIPSecQMsg       IkeIpsecQMsg;
    tIkeIoctlRW        *pIkeMsg = NULL;
    tOsixKernUserInfo   OsixKerUseInfo;

    pIkeMsg = (tIkeIoctlRW *) p;

    IPSEC_MEMSET (&IkeIpsecQMsg, 0, sizeof (tIkeIPSecQMsg));

    OsixKerUseInfo.pDest = &IkeIpsecQMsg;
    OsixKerUseInfo.pSrc = pIkeMsg->pBuf;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, pIkeMsg->wLen, 0)
        == OSIX_FAILURE)
    {
        return -EFAULT;
    }

    Secv4KernelLock ();
    /* OsixBHDisable() is called to disable IRQ from taking over the context */
    OsixBHDisable ();
    Secv4ProcessIKEMsg (&IkeIpsecQMsg);
    OsixBHEnable ();
    Secv4KernelUnLock ();
    return IOCTL_SUCCESS;
}

/***********************************************************************/
/*  Function Name : Secv4DummyIoctl                                    */
/*  Description   : This function Receives the IOCTL from kernel driver*/
/*                :and process the kernel message                      */
/*                :                                                    */
/*  Input(s)      : p - Kernel message                                 */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :None                                                */
/***********************************************************************/

int
Secv4DummyIoctl (unsigned long p)
{
    tSecQueMsg          SecQueMsg;
    tSecv4DummyMsg     *pSecv4DummyMsg = NULL;
    tIkeIoctlRW        *pIkeMsg = NULL;
    tOsixKernUserInfo   OsixKerUseInfo;

    pIkeMsg = (tIkeIoctlRW *) p;

    IPSEC_MEMSET (&SecQueMsg, 0, sizeof (tSecQueMsg));

    OsixKerUseInfo.pDest = &SecQueMsg;
    OsixKerUseInfo.pSrc = pIkeMsg->pBuf;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, pIkeMsg->wLen, 0)
        == OSIX_FAILURE)
    {
        return -EFAULT;
    }
    pSecv4DummyMsg = &SecQueMsg.ModuleParam.Secv4DummyMsg;
    Secv4DummyPktGenKern (pSecv4DummyMsg->pSaEntry, pSecv4DummyMsg->u4IfIndex,
                          pSecv4DummyMsg->au1MacAddr);
    return IOCTL_SUCCESS;
}

/***********************************************************************/
/*  Function Name : IkeHandlePktFromIpsec                              */
/*  Description   : This function is used to post the message to IKE   */
/*                :                                                    */
/*  Input(s)      : tIkeQMsg - structure to hold the IKE Message       */
/*                :                                                    */
/*  Output(s)     :  None                                              */
/*  Return        :  None                                              */
/***********************************************************************/

VOID
IkeHandlePktFromIpsec (tIkeQMsg * pMsg)
{
    tSecQueMsg          SecQueMsg;
    tIkeQMsg           *pIkeMsg = NULL;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    INT1                i1RetVal = 0;

    pIkeMsg = &(SecQueMsg.ModuleParam.IkeQMsg);

    MEMCPY (pIkeMsg, pMsg, sizeof (tIkeQMsg));

    MemReleaseMemBlock (SECv4_IPSEC_MSG_TO_IKE_MEMPOOl, pMsg);
    SecQueMsg.u1MsgType = SEC_MSG_TO_IKE_FROM_KERNEL;

    pCruBuf = SEC_CRU_BUF_Allocate_MsgBufChain (sizeof (tSecQueMsg), 0);
    if (pCruBuf == NULL)
    {
        return;
    }
    CRU_BUF_Copy_OverBufChain (pCruBuf, (UINT1 *) &SecQueMsg, 0,
                               sizeof (tSecQueMsg));

    /* post the buffer to CFA queue */
    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pCruBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
        return;
    }

    return;
}

INT4
icmp_error_msg (tIP_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pIcmp);
    return SUCCESS;
}

/***********************************************************************/
/*  Function Name : Secv4FipsIoctl                                     */
/*  Description   : This function Receives the IOCTL from kernel driver*/
/*                :and process the kernel message                      */
/*                :                                                    */
/*  Input(s)      :None                                                */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :None                                                */
/***********************************************************************/
int
Secv4FipsIoctl (void)
{
    Secv4DeleteSecAssocEntries ();
    return IOCTL_SUCCESS;
}
#endif
