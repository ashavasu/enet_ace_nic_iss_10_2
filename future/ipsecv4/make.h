##########################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
# ------------------------------------------                             #
# $Id: make.h,v 1.5 2011/05/17 11:35:04 siva Exp $ #
# DESCRIPTION  : make.h file for IPSecv4                                 #
##########################################################################
###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

SEC_KEY_OPNS  = -USECv4_PRIVATE_KEY
TOTAL_OPNS =  ${SEC_KEY_OPNS} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################
IPSECv4_DIR = ${BASE_DIR}/ipsecv4

IPSECv4_INC_DIR = ${IPSECv4_DIR}/inc
IPSECv4_SRC_DIR = ${IPSECv4_DIR}/src
IPSECv4_OBJ_DIR = ${IPSECv4_DIR}/obj


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  = -I${IPSECv4_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################

IPSECV4_DPNDS  =  ${IPSECv4_INC_DIR}/secv4com.h \
                  ${IPSECv4_INC_DIR}/secv4trace.h \
                  ${IPSECv4_INC_DIR}/secv4ah.h \
                  ${IPSECv4_INC_DIR}/secv4io.h \
                  ${IPSECv4_INC_DIR}/secv4buf.h \
                  ${IPSECv4_INC_DIR}/secv4esp.h \
                  ${IPSECv4_INC_DIR}/fssecv4lw.h \
                  ${IPSECv4_INC_DIR}/fssecv4wr.h \
                  ${IPSECv4_INC_DIR}/fssecv4db.h \
                  ${IPSECv4_INC_DIR}/secv4ogi.h \
                  ${IPSECv4_INC_DIR}/secv4frgtyp.h \
                  ${IPSECv4_INC_DIR}/secv4ike.h \
                  ${IPSECv4_INC_DIR}/secv4dyndb.h\
                  ${IPSECv4_INC_DIR}/secv4soft.h \

# IPSEC_TYPE , can be SOFT.
ifeq (${IPSEC_TYPE},)
IPSEC_TYPE        = SOFT
endif

#############################################################################
