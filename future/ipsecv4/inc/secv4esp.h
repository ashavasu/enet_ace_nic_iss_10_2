
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id:secv4esp.h
*
* Description:This file contains the macros required for ESP Module. 
*
*******************************************************************/


#ifndef _SECV4ESP_H_
#define _SECV4ESP_H_

/* Macros related to esp header */
#define SEC_ESP_TRAILER_LEN                   2
#define SEC_ESP_TRAILER_SIZE_LEN              1
#define SEC_ESP_MULTIPLY_FACTOR               8
#define SEC_ESP_NULL_MULTIPLY_FACTOR          4
#define SEC_ESP_HEADER_LEN                    8
#define SEC_ESP_SEQ_OFFSET                    4
#define SEC_ESP_HEADER                        2

#endif
