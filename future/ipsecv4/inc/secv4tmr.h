
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4tmr.h,v 1.4 2011/03/14 11:52:24 siva Exp $
*
* Description:This file contains the macros required for timer Module. 
*
*******************************************************************/
#ifndef _SECV4TMR_H_
#define _SECV4TMR_H_

#ifdef SECMOD_WANTED
VOID Secv4TmrCallbkFun PROTO ((UINT4 u4Arg));
#endif
#endif
