/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssecv4lw.h,v 1.4 2012/04/02 12:24:48 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

#define  SEC_MAX_IP_ADDRESS                0xffffffff
#define  SEC_MIN_IP_ADDRESS                0x00000000

#define  SEC_KEY_CHANGE                    0x5C
#define  SEC_CALLOC                        calloc
#define  SEC_INIT_VAL                      1
#define  SEC_BACK_COUNT                   -1
#define  SEC_TENS                          10
#define  SEC_ESP_SIZE                      15
#define  SEC_AH_SIZE                       32


INT1
nmhGetFsipv4SecGlobalStatus ARG_LIST((INT4 *));

INT1
nmhGetFsipv4SecGlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetFsipv4SecMaxSA ARG_LIST((INT4 *));

INT1
nmhGetFsipv4SecDummyPktGen PROTO ((INT4 *pi4RetValFsipv4SecDummyPktGen));

INT1
nmhGetFsipv4SecDummyPktParam PROTO ((INT4 *pi4RetValFsipv4SecDummyPktParam));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv4SecGlobalStatus ARG_LIST((INT4 ));

INT1
nmhSetFsipv4SecGlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetFsipv4SecMaxSA ARG_LIST((INT4 ));

INT1
nmhSetFsipv4SecDummyPktGen PROTO ((INT4 i4SetValFsipv4SecDummyPktGen));

INT1
nmhSetFsipv4SecDummyPktParam PROTO ((INT4 i4SetValFsipv4SecDummyPktParam));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv4SecGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv4SecGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv4SecMaxSA ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsipv4SecDummyPktGen PROTO ((UINT4 *pu4ErrorCode,
            INT4 i4TestValFsipv4SecDummyPktGen));
INT1
nmhTestv2Fsipv4SecDummyPktParam PROTO ((UINT4 *pu4ErrorCode,
            INT4 i4TestValFsipv4SecDummyPktParam));

/* Proto Validate Index Instance for Fsipv4SecSelectorTable. */
INT1
nmhValidateIndexInstanceFsipv4SecSelectorTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecSelectorTable  */

INT1
nmhGetFirstIndexFsipv4SecSelectorTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecSelectorTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SelFilterFlag ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv4SelPolicyIndex ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsipv4SelStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv4SelFilterFlag ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsipv4SelPolicyIndex ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsipv4SelStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv4SelFilterFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SelPolicyIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SelStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for Fsipv4SecAccessTable. */
INT1
nmhValidateIndexInstanceFsipv4SecAccessTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecAccessTable  */

INT1
nmhGetFirstIndexFsipv4SecAccessTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecAccessTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SecAccessStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecSrcNet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecSrcMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecDestNet ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecDestMask ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv4SecAccessStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecSrcNet ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv4SecSrcMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv4SecDestNet ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv4SecDestMask ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv4SecAccessStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecSrcNet ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv4SecSrcMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv4SecDestNet ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv4SecDestMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Proto Validate Index Instance for Fsipv4SecPolicyTable. */
INT1
nmhValidateIndexInstanceFsipv4SecPolicyTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecPolicyTable  */

INT1
nmhGetFirstIndexFsipv4SecPolicyTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecPolicyTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SecPolicyFlag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecPolicyMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecPolicySaBundle ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv4SecPolicyStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv4SecPolicyFlag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecPolicyMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecPolicySaBundle ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv4SecPolicyStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv4SecPolicyFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecPolicyMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecPolicySaBundle ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv4SecPolicyStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for Fsipv4SecAssocTable. */
INT1
nmhValidateIndexInstanceFsipv4SecAssocTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecAssocTable  */

INT1
nmhGetFirstIndexFsipv4SecAssocTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecAssocTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SecAssocDstAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecAssocProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAssocSpi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAssocMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAssocAhAlgo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAssocAhKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv4SecAssocEspAlgo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAssocEspKey ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv4SecAssocEspKey2 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv4SecAssocEspKey3 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv4SecAssocLifetimeInBytes ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAssocLifetime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAssocAntiReplay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecVersion ARG_LIST((UINT4 *));

INT1
nmhGetFsipv4SecAssocStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv4SecAssocDstAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsipv4SecAssocProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocSpi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocAhAlgo ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocAhKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv4SecAssocEspAlgo ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocEspKey ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv4SecAssocEspKey2 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv4SecAssocEspKey3 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsipv4SecAssocLifetimeInBytes ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocLifetime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocAntiReplay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsipv4SecAssocStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv4SecAssocDstAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Fsipv4SecAssocProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocSpi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocAhAlgo ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocAhKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv4SecAssocEspAlgo ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocEspKey ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv4SecAssocEspKey2 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv4SecAssocEspKey3 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsipv4SecAssocLifetimeInBytes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocLifetime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocAntiReplay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsipv4SecAssocStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for Fsipv4SecDFragBitTable. */
INT1
nmhValidateIndexInstanceFsipv4SecDFragBitTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecDFragBitTable  */

INT1
nmhGetFirstIndexFsipv4SecDFragBitTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecDFragBitTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SecDFragBitStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv4SecDFragBitStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsipv4SecDFragBitStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for Fsipv4SecIfStatsTable. */
INT1
nmhValidateIndexInstanceFsipv4SecIfStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecIfStatsTable  */

INT1
nmhGetFirstIndexFsipv4SecIfStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecIfStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SecIfInPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecIfOutPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecIfPktsApply ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecIfPktsDiscard ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecIfPktsBypass ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv4SecAhEspStatsTable. */
INT1
nmhValidateIndexInstanceFsipv4SecAhEspStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecAhEspStatsTable  */

INT1
nmhGetFirstIndexFsipv4SecAhEspStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecAhEspStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SecInAhPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecOutAhPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecAhPktsAllow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecAhPktsDiscard ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecInEspPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecOutEspPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecEspPktsAllow ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecEspPktsDiscard ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Fsipv4SecAhEspIntruTable. */
INT1
nmhValidateIndexInstanceFsipv4SecAhEspIntruTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsipv4SecAhEspIntruTable  */

INT1
nmhGetFirstIndexFsipv4SecAhEspIntruTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsipv4SecAhEspIntruTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv4SecAhEspIntruIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAhEspIntruSrcAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecAhEspIntruDestAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsipv4SecAhEspIntruProto ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsipv4SecAhEspIntruTime ARG_LIST((INT4 ,UINT4 *));
