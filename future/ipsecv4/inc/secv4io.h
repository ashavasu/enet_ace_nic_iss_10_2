
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4io.h,v 1.4 2012/08/28 07:38:55 siva Exp $
*
* Description:This file contains the macros required for io Module.
*
*******************************************************************/


#ifndef _SECV4IO_H_
#define _SECV4IO_H_

UINT1
Secv4HandleIncomingPkt  PROTO ((tIP_BUF_CHAIN_HEADER ** ppCBuf,UINT4 u4IfIndex));
UINT1
Secv4HandleOutgoingPkt  PROTO ((tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4IfIndex));

#endif
