
/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4kstubs.h 
 *
 * Description:Contains prototypes for the stub functions
 *                               
 *******************************************************************/
#ifndef _SECV4KSTUBS_H_
#define _SECV4KSTUBS_H_
#include "secv4com.h"
#include "secv4soft.h"
#include "osix.h"

VOID Secv4FreeTimerData(FS_ULONG ptr);
VOID Secv4HwInitForKernel (VOID);
VOID Secv4TaskletSchedFnInKernel (tSecv4TaskletData * pSecv4TskletData);
VOID Secv4TaskletFn (tWorkStruct *arg);

#endif
