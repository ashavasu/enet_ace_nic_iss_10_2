/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4winc.h,v 1.3 
*
* Description:This file contains all header files for kernel 
*             compilation
*******************************************************************/
#ifndef _SECV4_W_INC_H
#define _SECV4_W_INC_H
#include "fsike.h"
#include "secv4ike.h"
#include "osix.h"
#include "arsec.h"

int Secv4Ioctl (unsigned long p);
int Secv4DummyIoctl (unsigned long p);
int Secv4FipsIoctl (void);
#endif /* _SECV4_W_INC_H */
