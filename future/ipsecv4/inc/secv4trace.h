/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4trace.h
 *
 * Description:This file contains the #defines of the trace needed by 
 *             all the modules of the SEC subsystem
 *
 *******************************************************************/
#ifndef __SEC_SECv4DEBUG_H__
#define __SEC_SECv4DEBUG_H__


#include "trace.h"

#define SECv4_ENABLE_ALL_TRC		0x0000ffff
#define SECv4_DISABLE_ALL_TRC		0

#define SECv4_INIT_SHUT	          INIT_SHUT_TRC
#define SECv4_MGMT		  MGMT_TRC
#define SECv4_DATA_PATH	          DATA_PATH_TRC
#define SECv4_CONTROL_PLANE	  CONTROL_PLANE_TRC
#define SECv4_DUMP		  DUMP_TRC
#define SECv4_OS_RESOURCE	  OS_RESOURCE_TRC	
#define SECv4_ALL_FAILURE 	  ALL_FAILURE_TRC	
#define SECv4_BUFFER		  BUFFER_TRC		
#define SECv4_MUST	          0x00001000		

#define SECv4_TRC_NAME	"IPSecv4"
#define SECv4_TRC_LINE_SEPERATOR	"----------------------"


#define SECv4_PKT_DUMP(mask,pBuf,Length,fmt)\
	{\
		if(Length < 20) {\
			UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,pBuf,Length,fmt)\
		}\
		else {\
			UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,pBuf,40,fmt)\
		}\
	}

#define SECv4_TRC(mask,fmt)\
	UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,fmt)
#define SECv4_TRC1(mask,fmt,arg1)\
	UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,fmt,arg1)
#define SECv4_TRC2(mask,fmt,arg1,arg2)\
	UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,fmt,arg1,arg2)
#define SECv4_TRC3(mask,fmt,arg1,arg2,arg3)\
	UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,fmt,arg1,arg2,arg3)
#define SECv4_TRC4(mask,fmt,arg1,arg2,arg3,arg4)\
	UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,fmt,arg1,arg2,arg3,arg4)
#define SECv4_TRC5(mask,fmt,arg1,arg2,arg3,arg4,arg5)\
	UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,fmt,arg1,arg2,arg3,arg4,arg5)
#define SECv4_TRC6(mask,fmt,arg1,arg2,arg3,arg4,arg5,arg6)\
	UtlTrcLog(gu4Secv4Debug,mask,SECv4_TRC_NAME,fmt,arg1,arg2,arg3,arg4,arg5,arg6)


#define SECv4_PRINT_MEM_ALLOC_FAILURE\
		SECv4_TRC(OS_RESOURCE_TRC,"Memory Allocation Failed\n")


#endif  /* __SEC_SECv4DEBUG_H__ */
