/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id:secv4io.h 
*
* Description:This file contains the macros required for ogi Module.
*
*******************************************************************/
# ifndef fssecOGP_H
# define fssecOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FSIPV4SECSCALARS                             (0)
# define SNMP_OGP_INDEX_FSIPV4SECSELECTORTABLE                       (1)
# define SNMP_OGP_INDEX_FSIPV4SECACCESSTABLE                         (2)
# define SNMP_OGP_INDEX_FSIPV4SECPOLICYTABLE                         (3)
# define SNMP_OGP_INDEX_FSIPV4SECASSOCTABLE                          (4)
# define SNMP_OGP_INDEX_FSIPV4SECDFRAGBITTABLE                       (5)
# define SNMP_OGP_INDEX_FSIPV4SECIFSTATSTABLE                        (6)
# define SNMP_OGP_INDEX_FSIPV4SECAHESPSTATSTABLE                     (7)
# define SNMP_OGP_INDEX_FSIPV4SECAHESPINTRUTABLE                     (8)

#endif /*  fssecv4OGP_H  */
