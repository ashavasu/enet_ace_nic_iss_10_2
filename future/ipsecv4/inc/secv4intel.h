/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4intel.h,v 1.4 2014/03/07 11:57:39 siva Exp $
 *
 * Description:All structure, macro and constant definitions
 *             used By SECv4 for intel platform                       
 *                               
 *******************************************************************/
#ifndef _SECV4INTEL_H_
#define _SECV4INTEL_H_

#include <lac/cpa_cy_common.h>
#include <lac/cpa_cy_sym.h>
#include <cpa.h>
#include <lac/cpa_cy_im.h>

VOID Secv4DumpPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pCBuf, UINT2 u2Len));
VOID Secv4PrintBuffer PROTO ((UINT1 *pBuf, UINT1 u1BufLength));

/* CallBack Context */
typedef struct {
    void            *private1;
    tSecv4Tasklet   tq_TskLet;
    CpaCySymOpData  OpData;
    CpaBufferList   CpaBufList;
    CpaFlatBuffer   CpaFlatBuf;
    UINT4           u4OpStatus;
    UINT1           au1RcvdDigest [SEC_AUTH_MAX_DIGEST_SIZE];
    CpaBoolean      bVerifyResult;
    UINT1           au1Allignment[3];
}tSecv4CallBack __attribute__ ((aligned (8)));

/* CallBack Context */
INT1
Secv4IntelInitSessioninHw PROTO ((UINT4 u4SecAssocIndex, UINT4 u4Action));

INT1 Secv4IntelRemSesInHw PROTO ((VOID *pSessionCtx));

INT1
Secv4IntelProcessPkt PROTO ((tSecv4TaskletData * pSecv4Local, UINT4 u4Mode,
                          UINT4 u4Action));

VOID
Secv4IntelHwCallBackFn PROTO ((VOID *pCallBackTag, UINT4 u4Status, 
                               CpaCySymOp OpType, void *pOpData, 
                               CpaBufferList *pBufList, 
                               CpaBoolean verifyResult));
VOID
Secv4HandlePktAfterCryptoProcess PROTO ((VOID *pData));
VOID Secv4IntelPostPktToHw PROTO ((VOID *Ptr));
#endif
