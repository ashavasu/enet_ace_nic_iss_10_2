/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4ike.h,v 1.5 2014/03/07 11:57:39 siva Exp $
*
* Description:This file contains the prototypes,type definitions 
*             and #define constants required for ipsec to ike interface.
*
*******************************************************************/

#ifndef _IPSECV4IKE_H_
#define _IPSECV4IKE_H_

#define SEC_BYTES_PER_KB      1024

/* -------------------- IKE releated Function protos-----*/
INT1 Secv4ProcessIkeInstallSAMsg PROTO((tIkeIPSecQMsg *pMsg));
VOID Secv4ProcessIkeDupSPIMsg PROTO((tIkeIPSecQMsg *pInfo));
VOID Secv4ProcessIkeDeleteSAMsg PROTO((tIkeIPSecQMsg *pMsg));
VOID Secv4ProcessIkeDeleteSAByAddrMsg PROTO((tIkeIPSecQMsg *pMsg));
VOID Secv4ProcessIkeDeleteAllSAForLocal PROTO ((tIkeIPSecQMsg * pMsg));
INT1 Secv4InstallSAFromIke PROTO((tIPSecSA *pIkeSa,UINT4 u4DestAddr,
                                  UINT4 u4SrcAddr,UINT4 u4SaIndex,
      UINT4 u4PeerSpi,UINT4 u4VpnIfIndex));

VOID
Secv4ProcessIkeDeleteSABySetTime PROTO ((tIkeIPSecQMsg * pMsg));
VOID Secv4ConvertIkeIPSecSaToHostOrder PROTO ((tIPSecSA *pIkeIPSecSa));
VOID Secv4ConvertIkeInstallSAMsgToHostOrder PROTO((tIPSecBundle *pIkeIPSecSa));
VOID Secv4DeletePeerSa PROTO((UINT4 u4PeerSpi, UINT4 u4PeerAddr, UINT1 u1Protocol, 
                              UINT4 u4InterfaceIndex, BOOLEAN bDynFlag));
INT1 Secv4ProcessIkeDeleteDynamicIpsecDB PROTO ((UINT4 u4PolicyIndex,
                                                 UINT4 u4VpncIfaceIndex,
                                                 UINT4 u4PktDirection));
#endif
