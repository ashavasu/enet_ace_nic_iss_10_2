/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id:secv4soft.h
 * *
 * * Description:This file contains the macros required for Tasklet
 * *              Operations.
 * *
 * *******************************************************************/

#ifndef _SECV4SOFT_H_
#define _SECV4SOFT_H_

VOID Secv4TaskletSchedFn (UINT4 *);
    
#endif /* _SECV4TASKLET_H_ */
