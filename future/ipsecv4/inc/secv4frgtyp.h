/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4frgtyp.h 
 *
 * Description:All structure, macro and constant definitions
 *             used By SECv4 For fragmentation And Reassembly                       
 *                               
 *******************************************************************/
#ifndef   __SECv4_SECv4FRAG_H__
#define   __SECv4_SECv4FRAG_H__


#define   SECv4_DEF_REASM_TIME        15
#define   SECv4_FRAG_OFFSET_MASK      0x1fff
#define   SECv4_MF                    0x2000


/* Used by fragment reassembly scheme */
#define   SECv4_FRAG_INSERT     0
#define   SECv4_FRAG_APPEND     1
#define   SECv4_FRAG_PREPEND    2


#define   SECv4_MAC_HDR_LEN                     36


#define   SECv4_COPY_OPTION_TO_FRAG(u1Code)    (u1Code & 0x80)

/* Time Exceeded codes */
#define   ICMP_FRAG_EXCEED                      1    /* Fragment reassembly 
                                                        time exceeded */

#define   SECv4_PKT_OFF_FLAGS                   6
/* To get the MTU of the Specified Interface */
#define  SECv4IF_MTU(u2Port)                (Ipif_glbtab[(u2Port)].pIf->u4Mtu)

#define  SECv4_MAX_REASM_SIZE(u2Port)\
         (Ipif_glbtab[(u2Port)].pIf->u4Reasm_max_size)

#define   SECv4_GET_IP_HLEN(pBuf,u1Hlen)\
          IPSEC_COPY_FROM_BUF(pBuf, &u1Hlen, IP_PKT_OFF_HLEN, sizeof(UINT1))

#define SECv4_CONCAT_BUFS(pBuf1, pBuf2)                             \
   CRU_BUF_Concat_MsgBufChains((pBuf1), (pBuf2))


#define SECv4_OFF_IN_BYTES(FlgOffs)\
        ((FlgOffs & SECv4_FRAG_OFFSET_MASK) << 3)

#define   SECv4_TOTAL_HDR_LEN(pIp)   (IP_HDR_LEN + pIp->u2Olen)

#define SECv4_OFF_IN_HDR_FORMAT(fl_offs)\
        (fl_offs >> 3)


#define SECv4_FRAGMENT_BUF(pBuf, u4Offset, ppFragBuf)               \
   CRU_BUF_Fragment_BufChain((pBuf), (u4Offset), (ppFragBuf))

#define  SECv4_PKT_ASSIGN_LEN(pBuf,u2Len)\
         {                                                                   \
            UINT2 u2Tmp;                                                     \
            u2Tmp = IPSEC_HTONS(u2Len);                                         \
            IPSEC_COPY_OVER_BUF(pBuf, (UINT1 *)&u2Tmp, IP_PKT_OFF_LEN, sizeof(UINT2));     \
         }

#define  SECv4_PKT_ASSIGN_FLAGS(pBuf,u2Flags)\
         {                                                                   \
            UINT2 u2Tmp;                                                     \
            u2Tmp = IPSEC_HTONS(u2Flags) ;    \
            IPSEC_COPY_OVER_BUF(pBuf, (UINT1 *)&u2Tmp, SECv4_PKT_OFF_FLAGS, sizeof(UINT2)) ; \
        }

#define   SECv4_COPY_OPTION_TO_FRAG(u1Code)    (u1Code & 0x80)


VOID Secv4ConstructHdrForFrags PROTO ((t_IP * pIp_org, t_IP * pIp_frag));
tSecv4Reassm   *Secv4LookUpReasm PROTO ((t_IP * pIp));
tSecv4Reassm   *Secv4CreateReasm PROTO ((t_IP * pIp));
tSecv4Frag     *Secv4NewFrag PROTO ((UINT2 u2StartOffset, UINT2 u2EndOffset,
                                     tCRU_BUF_CHAIN_HEADER * pBuf));
VOID Secv4FreeFrag PROTO ((tSecv4Frag * pFrag));

VOID Secv4FreeReasm PROTO ((tSecv4Reassm * pReasm));



#endif            /*__SECv4_SECv4FRAG_H__*/
