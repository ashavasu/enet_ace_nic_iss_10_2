
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4buf.h,v 1.4 2011/03/03 13:34:40 siva Exp $
*
* Description:This file contains tcros adn structures related to Buffer mgmt
*
*******************************************************************/

#ifndef _IPSECV4BUF_H_
#define _IPSECV4BUF_H_
typedef struct LINEAR_BUFFER_DESC{
UINT1                *pBuf;
t_IP_HEADER          *pIpHdr;
UINT1                *pu1Opt;
UINT4                 u4ReadOffset;
UINT4                 u4ValidDataSize;
UINT4                 u4AllocatedBufSize;
tMODULE_DATA          ModData;
/* VOID                 *pskb; */
}tLinearBufDesc;


#define SEC_GET_DATAPTR(pBufDesc)  (pBufDesc->pBuf + pBufDesc->u4ReadOffset)

#define SEC_GET_BUFFER_SIZE(pBufDesc)      (pBufDesc->u4ValidDataSize)

#define SEC_UPDATE_DATASIZE(pBufDesc, u4DataSize)          \
             (pBufDesc->u4ValidDataSize = u4DataSize)

#define SEC_UPDATE_READOFFSET(pBufDesc, u4DataSize)          \
             (pBufDesc->u4ReadOffset += u4DataSize)

#define SEC_DELETE_BUFFER_AT_END(pBufDesc, u4Offset)       \
             (pBufDesc->u4ValidDataSize -= u4Offset)

#define SEC_DELETE_BUFFER_AT_START(pBufDesc, u4Offset)     \
             pBufDesc->u4ValidDataSize -= u4Offset;        \
             pBufDesc->u4ReadOffset += u4Offset;           

#define SEC_PREPEND_BUFFER(pBufDesc, pBuf1, u4Offset)      \
     pBufDesc->u4ReadOffset -= u4Offset; \
     IPSEC_MEMCPY (pBufDesc->pBuf + pBufDesc->u4ReadOffset, pBuf1, u4Offset);\
     pBufDesc->u4ValidDataSize += u4Offset; 

#define SEC_UPDATE_DATA_PTR (pBufDesc,u4OffSet) \
    pBufDesc->u4ReadOffset += u4OffSet; \
    pBufDesc->u4ValidDataSize -= u4OffSet; \

 /* Buffer Function Prototypes */

tLinearBufDesc * Secv4AllocateBuffer PROTO((VOID));
VOID             SecUpdateDataPtr (tLinearBufDesc *pBufDesc, UINT4 u4OffSet);
VOID             Secv4ReleaseBuffer PROTO((tLinearBufDesc * pBufDesc));

#endif /* _IPSECV4BUF_H_ */
