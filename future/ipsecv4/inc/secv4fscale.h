/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4fscale.h,v 1.4 2014/03/07 11:57:39 siva Exp $
 *
 * Description:All structure, macro and constant definitions
 *             used By SECv4 for freescale platform                       
 *                               
 *******************************************************************/
#ifndef _SECV4FSCALE_H_
#define _SECV4FSCALE_H_


#define SEC2_DEVNAME "/dev/sec2"

#ifdef  _SECV4FSCALE_C_
INT4  gSecv4CallbackPoolId;
INT4  fd;
#else
extern INT4  gSecv4CallbackPoolId;
extern INT4  fd;
#endif

#ifndef KERNEL_WANTED
extern int SEC2_ioctl (int devDesc, register int ioctlCode, 
                       register void *param);
extern int SEC2Open (UINT1 *pDevHdr, int mode, int flags);
extern int SEC2Close (int devDesc);

#define SEC2_IOCTL(fd, fil, ioctlcode, param) SEC2_ioctl(fd, ioctlcode, param)

#else
extern int SEC2_ioctl(struct inode  *nd, struct file   *fil,
                      unsigned int   ioctlCode, unsigned long  param);
#define SEC2_IOCTL(fd, fil, ioctlcode, param) \
    SEC2_ioctl(NULL, NULL, ioctlcode, param)
#endif

/* Encryption request for FreeScale architecture */
typedef struct sEncryptReq
{
    UINT4    u4EncryptOpId;
    HMAC_PAD_REQ    sHmacReq;
    union {
        IPSEC_CBC_REQ        sIpsecReq;
        IPSEC_AES_CBC_REQ    sAesReq;
    } uEncryptAlgo;
} tEncryptReq;

/* CallBack Context */
typedef struct {
    void *saCtxt;
    void *bufCtxt;
    void *private2;
    UINT1 au1Digest[32];
    UINT1 au1TDESKey[24];
    tEncryptReq EncryptReq;
    UINT4 u4LnrFlag;
}tSecv4CallBack __attribute__ ((aligned (8)));

INT1 Secv4FreeScaleProcessPkt PROTO((tSecv4TaskletData *, UINT4 , UINT4 ));
VOID Secv4HandlePktAfterCryptoProcess(VOID *pData);
VOID Secv4FsScaleCallBackFn (VOID *pData);
 
#endif
