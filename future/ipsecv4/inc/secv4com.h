/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4com.h,v 1.26 2015/03/13 12:43:19 siva Exp $
*
* Description:This file contains the prototypes,type definitions
*             and #define constants required for IPSecv4.
*
*******************************************************************/

#ifndef _IPSECV4_H_
#define _IPSECV4_H_
#undef FWL_DEF_MAX_FILTERS
#include "lr.h"
#include "ip.h"
#include "osix.h"
#include "secv4.h"
#include "arHmac_api.h"
#include "shaarinc.h"
#include "arMD5_inc.h"
#include "arMD5_api.h"
#include "desarinc.h"
#include "aesarinc.h"
#include "secv4trace.h"
#include "secv4buf.h"
#include "fsike.h"
#include "fsutil.h"
#include "sec.h"
#include "cfa.h"
#include "vpn.h"
#include "secmod.h"

/*------------------Tasklet related Definitions ------------*/

#define ESP                            1
#define AUTH                           2  
#define ESP_AUTH                       3  
#define IxCryptoAccPcktLen 2048

#define  SECv4_IF_MAX_REASM_SIZE 45000

/* Constants related to Ipv4 headersize and position of the fields */

#define SEC_ETH_HEADER_SIZE                    14
#define SEC_IPV4_HEADER_LEN_OFFSET              2
#define SEC_IPV4_TOS_OFFSET                     1
#define SEC_IPV4_HEADER_LEN                     2
#define SEC_IPV4_HEADER_CKSUM_OFFSET           10
#define SEC_IPV4_MAX_OPT_LEN                   40
#define SEC_PROTO_OFFSET_FOR_INDEX             50 /* This shd be subtracted to get 0
                                                     or 1 from the protocol as the
                                                  values are 50 and 51 for ah and esp */
#define SEC_MODE_OFFSET_FOR_INDEX               1 /* This offset shd be subtracted to get
                                                   0 or 1 as index, as the values defined
                                                   for transport andtunnel are 1 and 2 */
#define SEC_IPV4_MAX_OPT_LEN                   40
#define SEC_V4                                  4
#define SEC_ICMPv4                              1
#define SEC_IP_IN_IP                            4
#define SEC_INVALID_INTERFACE                   0
#define SEC_MAX_FUNCTIONS                       4
#define SEC_AUTH_HEADER_SIZE                   12
#define SEC_MAX_BUFFER_SIZE                  3000
#define SEC_MAX_NO_OF_BUFFER                  512
#define SEC_ESP_TUNNEL_ENCODE_INDEX             0
#define SEC_ESP_UDP_TUNNEL_ENCODE_INDEX         4


#define SEC_MAX_SA_TO_POLICY                    8     /* Max Rekeyed SA attached to 
                  Policy */


#define   SECv4_MAX_DATAGRAMS_FOR_REASSEMBLY    100  /* Max. Number of Datagrams that are
                                                       allowed to undergo reassembly.*/
#define   SECv4_MAX_FRAGMENTS_IN_DATAGRAM       20  /* Max. Number of Fragments that are
                                                       allowed for a DataGram.*/


#define   SECv4_SA_HARD_TIMER_ID                1    /* Indicates SA LifeTime Timer */
#define   SECv4_SA_SOFT_TIMER_ID                2    /* Indicates SA Hard Timer */
#define   SECv4_REASM_TIMER_ID                  3    /* Indicate Reassembly Timer   */
#define   SECv4_DUMMY_PKT_TIMER_ID              4    /* Indicates Dummy pkt Timer */

#define   SECv4_DUMMY_TMR_INTERVAL             10

#define   SECv4_TIMER_INACTIVE                  5

#define   SEC_POSTED_TO_ALGO                    2

#define SEC_RE_KEY                              0   /* Definitions for IKE interface */
#define SEC_NEW_KEY                             1

/* To combine Version and Header Length in to one Byte */
#define SEC_VERS_AND_HLEN(x,y) ((x << 4) | (UINT1)((y + SEC_IPV4_HEADER_SIZE)>>2))

/* Used By Icmp for path discovery */
#define   SECv4_ICMP_NH_MTU(Icmp)   Icmp.args.SecFailure.u2Pointer

/* To extract the Spi from the Ah Header */
#define SEC_AH_SPI_OFFSET              24

/* Space needed for prepeindg hdr in case of tunnel encoding. */
#define SEC_MAX_PREPEND_HDR_SIZE              80

/* Threshold for life time of SA */
#define  SEC_THRESHOLD_PERCENT            80 /*Percentage of the Thresh Hold Time */

#define  SEC_MIN_SOFTLIFETIME_FACTOR        80
#define  SEC_MAX_SOFTLIFETIME_FACTOR        90

/* Time out Val for Triggering IKE 
   in case of SA establishment failure */
#define SEC_TRIGGER_IKE_TIME_INTERVAL       5


#define   IPSEC_OPT_NUMBER(Copy1Class2No5)    ((UINT1)(Copy1Class2No5 & 0x1f))

#define   IPSEC_IS_DF_SET(x)         (x & 0x4000)

#define   IPSEC_OPT_EOL              0
#define   IPSEC_OPT_NOP              1
#define   IPSEC_OPT_LSROUTE          3
#define   IPSEC_OPT_TSTAMP           4    /* class 2 */
#define   IPSEC_OPT_RROUTE           7
#define   IPSEC_OPT_STREAM_IDENT     8
#define   IPSEC_OPT_SSROUTE          9
#define   IPSEC_OPT_TROUTE           18
#define   IPSEC_OPT_MTU_PROBE        11
#define   IPSEC_OPT_MTU_REPLY        12

#define   IP_OPTION_LEN              40

#define IPSEC_DFBIT_SET              0x4000

#define  SEC_DFBIT_COPY              1  /*To Copy the df bit from inner header*/
#define  SEC_DFBIT_CLEAR             2  /*To clear the df bit */
#define  SEC_DFBIT_SET               3  /* To set the Df bit */

/* Used for flag value in SA life time in seconds */
#define  SEC_DELETE_SA                    1  /*Indicates whether SA Can Be Deleted */
#define  SEC_BYTES_SECURED_EXCEEDED      -1  /* Lifetime in bytes return value */
#define  SEC_BYTES_SECURED_NOT_EXCEEDED   1 /* Lifetime in bytes return value */
#define  SEC_THRESHOLD_REACHED            2   /* Lifetime in bytes return value */

#define  SEC_FOUND                        1
#define  SEC_NOT_FOUND                    0

/* for sending ucast pkts */
#define  SEC_LINK_UCAST              1
#define  SECv4_SNPRINTF              SNPRINTF

#define SECv4_NTFY_QUEUE            (const UINT1*) "SECQ"
#define SECv4_MAX_NTFY_PKTS         100

/* MemPool related constants */
#define SECv4_SEL_MEMPOOL           Secv4SelectorMemPoolId
#define SECv4_ACC_MEMPOOL           Secv4AccessMemPoolId
#define SECv4_POL_MEMPOOL           Secv4PolicyMemPoolId
#define SECv4_SAD_MEMPOOL           Secv4SADMemPoolId
#define SECv4_SAD_AH_MEMPOOL        Secv4SADAhMemPoolId
#define SECv4_SAD_ESP_MEMPOOL       Secv4SADEspMemPoolId
#define SECv4_CONTEXT_MEMPOOL       Secv4ContextMemPoolId
#define SECv4_AUTH_MSG_MEMPOOL      Secv4AuthMsgMemPoolId
#define SECv4_LINEAR_BUFDESC_POOL   Secv4LinearBufDescPoolId
#define SECv4_LINEAR_BUFFER_POOL    Secv4LinearBufferPoolId
#define SECv4_CONTEXT_TABLE_ID      Secv4ContextHashTableId
#define SECv4_RCVD_ICV_MEMPOOL              Secv4RcvdIcvMemPoolId
#define SECv4_ALGO_COMPUTE_ICV_MEMPOOL      Secv4ComputeIcvMemPoolId
#define SECv4_RCVD_IV_MEMPOOL               Secv4RcvdIvMemPoolId
#define SECv4_RCVD_IPHDR_MEMPOOl            Secv4RcvdIPHdrMemPoolId
#define SECv4_RCVD_IPOPTS_MEMPOOL           Secv4RcvdIPOptsMemPoolId
#define SECv4_IP_REASSM_MEMPOOL             Secv4IPReAssmMemPoolId
#define SECv4_IP_FRAG_MEMPOOL               Secv4IPFragMemPoolId
#define SECv4_TASKLET_MEMPOOl               Secv4TaskletMemPoolId




/* Macros for classification of IP addresses */
#define   IPSEC_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr & 0x80000000) == 0)
#define   IPSEC_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000)
#define   IPSEC_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000)
#define   IPSEC_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr & 0xf0000000) == 0xe0000000)
#define   IPSEC_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr & 0xf8000000) == 0xf0000000)

#define IPSEC_MAX_APPEND_SIZE  32



#define SEC_MAX_PAD_LEN (16 + SEC_ESP_TRAILER_LEN + SEC_AUTH_MAX_DIGEST_SIZE)

#define SEC_ESP_KEY_LENGTH (((SEC_ESP_AES_KEY3_LEN + 1) * 2 + 3) & 0xfffffffc)
#define SEC_AH_KEY_LENGTH ((((2 * SEC_ESP_AES_KEY3_LEN) + 1) * 2 + 3) & 0xfffffffc)

/* Other Macro definitions */
#define SECV4_ZERO             0
#define SECV4_MAX_ITERATIONS   2

#ifdef _SECV4INIT_C_
tMemPoolId   Secv4SelectorMemPoolId;
tMemPoolId   Secv4AccessMemPoolId;
tMemPoolId   Secv4PolicyMemPoolId;
tMemPoolId   Secv4SADMemPoolId;
tMemPoolId   Secv4SADAhMemPoolId;
tMemPoolId   Secv4SADEspMemPoolId;
tMemPoolId   Secv4AuthMsgMemPoolId;
tMemPoolId   Secv4ContextMemPoolId;
tMemPoolId   Secv4LinearBufDescPoolId;
tMemPoolId   Secv4LinearBufferPoolId;
tMemPoolId   Secv4RcvdIcvMemPoolId;
tMemPoolId   Secv4ComputeIcvMemPoolId;
tMemPoolId   Secv4RcvdIvMemPoolId;
tMemPoolId   Secv4RcvdIPHdrMemPoolId;
tMemPoolId   Secv4RcvdIPOptsMemPoolId;
tTMO_HASH_TABLE *Secv4ContextHashTableId;
tMemPoolId   Secv4IPReAssmMemPoolId;
tMemPoolId   Secv4IPFragMemPoolId;
tMemPoolId   Secv4TaskletMemPoolId;
tMemPoolId   Secv4NtfyQMemPoolId;

#else

extern tMemPoolId       Secv4SelectorMemPoolId;
extern tMemPoolId       Secv4AccessMemPoolId;
extern tMemPoolId       Secv4PolicyMemPoolId;
extern tMemPoolId       Secv4SADMemPoolId;
extern tMemPoolId       Secv4SADAhMemPoolId;
extern tMemPoolId       Secv4SADEspMemPoolId;
extern tMemPoolId       Secv4AuthMsgMemPoolId;
extern tMemPoolId       Secv4ContextMemPoolId;
extern tMemPoolId       Secv4LinearBufDescPoolId;
extern tMemPoolId       Secv4LinearBufferPoolId;
extern tMemPoolId   Secv4RcvdIcvMemPoolId;
extern tMemPoolId   Secv4ComputeIcvMemPoolId;
extern tMemPoolId   Secv4RcvdIvMemPoolId;
extern tMemPoolId   Secv4RcvdIPHdrMemPoolId;
extern tMemPoolId   Secv4RcvdIPOptsMemPoolId;
extern tTMO_HASH_TABLE *Secv4ContextHashTableId;
extern tMemPoolId       Secv4IPReAssmMemPoolId;
extern tMemPoolId       Secv4IPFragMemPoolId;
extern tMemPoolId       Secv4NtfyQMemPoolId;
extern tMemPoolId       Secv4TaskletMemPoolId;

#endif

#ifdef SECURITY_KERNEL_WANTED
PUBLIC UINT4 gu4SecReadModIdx;
#endif

#define  SECv4_INIT_TIMER(pTimerNode) ((pTimerNode)->u1TimerId = SECv4_TIMER_INACTIVE)                   
#define  SECv4_IS_TIMER_INACTIVE(pTimerNode)  (((pTimerNode)->u1TimerId == SECv4_TIMER_INACTIVE) ? TRUE : FALSE) 

/* Structures used for initiating timer value if life time in seconds configured */
typedef tTmrAppTimer tTimer;
typedef struct {
        tTimer TimerNode;
        FS_ULONG  u4Param1;         /* For Storing the Policy Pointer */
        UINT4  u4SaIndex;
        UINT2  u2Alignment;
        UINT1  u1TimerId;
  UINT1  u1Alignment;
} tSecv4Timer;


typedef struct secdfrag
{

    UINT4           u4IfIndex;        /*The interface index */
    UINT1           u1DFragBitStatus; /*Status of Dont Frag Bit */
    UINT1           au1Allignment[3]; /*For allignment purpose */
}tSecv4DFrag;

#ifdef NAT_WANTED
typedef struct secglobaladdr
{
    tTMO_SLL_NODE   NextSecGlobalAddr; /*Ptr to the next entry in the list */
    UINT4 u4GlobalIpAddr;
    UINT2 u2DestPort;
 UINT2 u2Allignment; /* Alignment Purpose */  
}tSecv4GlobalAddr;
#endif


typedef struct  secaccess
{
    tTMO_SLL_NODE   NextSecGroupIf; /*Pointer to the next entry in the list */
    UINT4        u4GroupIndex;      /*Pointer to inaccess entry */
    UINT4        u4SrcAddress;  /* the start addr for the inaccess entry*/
    UINT4        u4SrcMask;    /* the end addr addr for the outaccess entry */
    UINT4        u4DestAddress; /* the start addr for outaccess entry */
    UINT4        u4DestMask;   /* the endaddr for the outaccess entry */
    UINT4        u4Protocol;
    UINT2        u2LocalStartPort;
    UINT2        u2LocalEndPort;
    UINT2        u2RemoteStartPort;
    UINT2        u2RemoteEndPort;
#ifdef NAT_WANTED
    tTMO_SLL     Secv4GlobalAddrList;  /* Linked list containing global addresses
                                  and ports */
#endif
}tSecv4Access;

typedef unsigned long long tDesKey[16];

typedef struct SecAssoc
{
   tTMO_SLL_NODE  NextSecAssocIf;        /* pointer to the next node in the
                                            list*/
   tSecv4Timer  Secv4AssocHardTimer;     /* SA Timer for LifeTime in Seconds*/
   tSecv4Timer  Secv4AssocSoftTimer;     /* SA Timer for LifeTime in Seconds*/
   tIkeNatT     IkeNattInfo;              /* NAT-T Info */
   UINT1       au1AesEncrKey[240];        /* Aes Key */
   UINT1       au1AesDecrKey[240];        /* Aes Key */   
   UINT1       au1Nonce[IPSEC_AES_CTR_NONCE_LEN];        /* AES-CTR Nonce */        
   UINT1       *pu1SecAssocAhKey;        /* configured ah key */
   UINT1       *pu1SecAssocAhKey1;       /* transformed ah key */
   UINT1       *pu1SecAssocInitVector;   /* the initialisation vector */
  
   tDesKey      Key;                     /* transformed des-cbc key */
   tDesKey      Key2;                    /* transformed 3des-cbc key2 */
   tDesKey      Key3;                    /* transformed 3des-cbc key3 */
   UINT1       *pu1SecAssocEspKey;       /* configured esp key */
   UINT1       *pu1SecAssocEspKey2;      /* configured esp key2 */
   UINT1       *pu1SecAssocEspKey3;      /* configured esp key3 */
   UINT1       *pu1SecAssocEspKeyIn;       /* configured esp key */
   UINT1       *pu1SecAssocEspKey2In;      /* configured esp key2 */
   UINT1       *pu1SecAssocEspKey3In;      /* configured esp key3 */
   VOID        *pSessionCtx;             /* Session Context that is required
                                            in case of some H/W Accelerators*/
   
   UINT4       u4SecAssocIndex;          /* the sec assoc index */
   UINT4       u4SecAssocSeqNumber;      /* 0 */  /* Seq number */
   UINT4       u4SecAssocBitMask;        /* 0 */   /* Bitmask */
   UINT4       u4SecAssocSpi;            /* the spi */
   UINT4       u4PeerSecAssocSpi;        /* Spi of the Peer */
   UINT4       u4SecAssocDestAddr;       /* the secassoc end addr */
   UINT4       u4SecAssocSrcAddr;        /* the secassoc src addr */
   UINT4       u4ByteCount;              /* The no of Bytes secured by the SA */
   UINT4       u4LifeTimeInBytes;        /* lifetime in bytes */
   UINT4       u4LifeTime;               /* life time */
   UINT4       u4PolicyIndex;            /* Refers to Policy Index */
   UINT2       u2Size;                   /* Extra pay load added */
   UINT1       u1SecAssocProtocol;       /* secassoc protocol */
   UINT1       u1SecAssocSeqCounterFlag; /* 32 */
   UINT1       u1SecAssocMode;           /* secassoc mode -transport/tunnel */
   UINT1       u1SecAssocAhAlgo;         /* Ah algo */
   UINT1       u1SecAssocAhKeyLength;    /* ah keylen */
   UINT1       u1SecAssocAhKey1Length;    /* ah keylen */
   UINT1       u1SecAssocEspAlgo;        /* esp algo */
   UINT1       u1SecAssocEspKeyLength;   /* esp key len */
   UINT1       u1SecAssocEspKey2Length;  /* esp key2 len */
   UINT1       u1SecAssocEspKey3Length;  /* esp key3 len */
   UINT1       u1SecAssocEspKeyInLength;   /* esp key len */
   UINT1       u1SecAssocEspKey2InLength;  /* esp key2 len */
   UINT1       u1SecAssocEspKey3InLength;  /* esp key3 len */
   UINT1       u1AntiReplayStatus;       /* configured info of Anti replay
         Status */
   UINT1       u1DelSaFlag;              /* Flag for Deleting SA */
   UINT1       u1KeyingMode;              /* automatic or manual */
   UINT1       u1SecAssocIvSize;              /* alignment purpose */
   UINT1       u1SecEspMultFact;             /* Multiplying factor for esp */
   UINT4       u4VpncIndex;             /* VPNC Index */
   UINT1       u1SecAhAlgoDigestSize;   /* Auth digest size added */
   UINT1       au1Padding[3];   /* Auth digest size added */
   
 }tSecv4Assoc;

typedef struct secpolicy
{

   tTMO_SLL_NODE  NextSecPolicyIf;                       /* pointer to the next
                                                            entry in the list */

   tSecv4Timer    Secv4PolicyTrigIkeTmr;                 /* Timer for trigering
                   IKE to establish SA */

   tSecv4Assoc    *pau1SaEntry[SEC_MAX_BUNDLE_SA];        /* Array of pointer
                                                             to Sa entries */

   tSecv4Assoc    *pau1NewSaEntry[SEC_MAX_SA_TO_POLICY][SEC_MAX_BUNDLE_SA];
                                                             /* Array of pointer
                                                             to new sa entries */

   UINT1       au1PolicySaBundle[SEC_MAX_SA_BUNDLE_LEN];  /* pointer to sa
                                                            bundle */

   UINT1       au1NewPolicySaBundle[SEC_MAX_SA_TO_POLICY][SEC_MAX_SA_BUNDLE_LEN];                                                                    /* Indices of New sa
                                                             Bundle */

   UINT1       au1NewSaCount[SEC_MAX_SA_TO_POLICY ];     /* Array for the no of sa's 
               in the new sabundle */

   UINT4       u4PolicyIndex;                            /* refers to the
                                                            policy index */


   UINT1       u1PolicyFlag;                             /* Decides whether to
                                                            apply sec or
                                       bypass */

   UINT1       u1PolicyMode;                             /* specifies
                                                            automatic or
                                       manual mode */

   UINT1       u1SaCount;                                /* the no of sa's in
                                                            the sabundle */

   UINT1       u1ReqIkeFlag;                             /* Whether already
                                                            reuested ike for
                                                            new key */
   UINT1       u1IkeVersion;
   UINT1       au1Padding[3];
  }tSecv4Policy;



/* Data structures for nodes of various data bases */

 typedef struct secselector
{
    tTMO_SLL_NODE   NextSecSelIf;  /*Pointer to the next entry in the list */
    UINT4        u4SelIndex;        /*The interface index */
    UINT4        u4IfIndex;        /*The interface index */
    tSecv4Policy *pPolicyEntry;    /* Pointer to the corresponding policy whose
                                      index is stored in u4SelPolicyIndex in this structure */
    UINT4        u4ProtoId;        /*Identifies the protocol */
    UINT4        u4SelPolicyIndex; /*Points to the policy entry */
    UINT4        u4Port;           /*Identifies the port */
    UINT4        u4PktDirection;   /*Specifies whether inbound or outbound */
    UINT4        u4SelAccessIndex; /*Specifies Access Index */
    UINT4        u4Count;          /*Specifies the specific entry */
    UINT1        u1SelFilterFlag;  /*Specifies whether to filter or allow */
    UINT1        au1Allignment[3]; /*For allignment purpose */
 }tSecv4Selector;


typedef struct  SecStat
{
  UINT4   u4Index;         /* refers to the interface index */
  UINT4   u4IfInPkts;      /*count for no of incoming packets */
  UINT4   u4IfOutPkts;     /*count for no of outgoining packets */
  UINT4   u4IfPktsApply;   /*count for no of sec-applied packets */
  UINT4   u4IfPktsDiscard; /*count for no of packets discarded */
  UINT4   u4IfPktsBypass;  /*count for no of packets bypassed */
}tSecv4Stat;

typedef struct SecAhEspStat
{
  UINT4   u4Index;         /* refers to the interface index */
  UINT4  u4InAhPkts;     /*count for no of incoming ah pkts*/
  UINT4  u4OutAhPkts;    /*count for no of outgoining ah pkts */
  UINT4  u4AhPktsAllow;   /*count for no of ah pkts allow */
  UINT4  u4AhPktsDiscard;  /*count for no ah pkts discarded */
  UINT4  u4InEspPkts;      /* count for no of incoming esp pkts */
  UINT4  u4OutEspPkts;     /*count for no of outgoining esp pkts */
  UINT4  u4EspPktsAllow;    /*count for no of esp pkts allow */
  UINT4  u4EspPktsDiscard;   /*count for no esp pkts discarded */
}tSecv4AhEspStat;

typedef struct SecAhEspIntruderStat
{
  UINT4     u4Index;             /* the index to the row*/
  UINT4     u4IfIndex;             /* the interface index */
  UINT4     u4AhEspIntruSrcAddr;   /* intruder src addr */
  UINT4     u4AhEspIntruDestAddr;  /* intruder dest addr */
  UINT4     u4AhEspIntruProt;    /* intruder protocol */
  UINT4     u4AhEspIntruTime;    /*intruder time */
}tSecv4AhEspIntruStat;



typedef struct secalgocontext
{

    tSecv4Assoc     *pCurrentSa;           /* Current SA */
    tCRU_BUF_CHAIN_HEADER *pCBuf;                 /* Linear Buffer descriptor */
    UINT1           *pu1OptBuf;            /* Rcvd IP Options */
    UINT4            u4IfIndex;            /* Interface Index */
    UINT4            u4AuthOffset;
    UINT4            u4AuthBufSize;
    UINT4            u4EncrOffset;
    UINT4            u4EncrBufSize;
    UINT4            u4IcvOffset;
    UINT4            u4Mode;
    UINT2            u2Fl_offs;
    UINT1            u1Tos;
    UINT1            u1Ttl;
}tSecv4AlgoMsg;

/* Notification structure */
typedef struct IPCNOTIFYINFO
{
    UINT4 u4NotifyType;                             /* Notification type */
    UINT4 u4IfIndex;                                /* Wlan Index */
    union
    {
        tSecv4NotifySystemUp secv4NotifySystemUp;     /* System up notification */
    } unNotifyMsgs;
} tSecv4Notify;


/*
 * Reassembly queue contains information regarding all datagrams which
 * are undergoing reassembly process.  It is arrenged as a linked list
 * in which each entry corresponds to one datagram.  Each entry will have
 * a pointer to fragment list belonging to that datagram.  The reassmbly
 * queue entry is freed, once the entire datagram is reconstructed.
 */

typedef struct
{
    tTMO_DLL_NODE         Link;
    UINT4                 u4Src;            /* These fields uniquely identify a
                                             * datagram
                                             */
    UINT4                 u4Dest;
    UINT1                 u1Proto;
    UINT1                 u1AlignmentByte;  /* Byte for word alignment */
    UINT2                 u2AlignmentByte;  /* Byte for word alignment */
    UINT2                 u2Id;
    UINT2                 u2Len;            /* Entire datagram length, if known */
    tSecv4Timer           Timer;            /* To implement reassembly timeout */
    tTMO_DLL              Secv4FragList;    /* List of fragments collected so far */
}tSecv4Reassm;

/*
 * A linked list of fragments is maintained with each of the reassembly
 * queues during the process of reassembly. Once the list is complete
 * i.e. all the fragments are received, the queue entry is freed.
 */
typedef struct
{
    tTMO_DLL_NODE             Link;

    tCRU_BUF_CHAIN_HEADER    *pBuf;           /* Data buffer associated with this
                                               * fragment
                                               */
    UINT2                    u2StartOffset;   /* Begining and end offsets of this
                                               * fragment
                                               */
    UINT2                    u2EndOffset;
} tSecv4Frag;

/*  Tasklet data structure is maintained to post the data to 
 *  kernel tasklet or work queue  and in case of user space to the */
typedef struct sSecv4Tasklet
{
    tTMO_SLL_NODE Secv4TaskletNode;
    tSecv4Assoc *pSaEntry;
    tCRU_BUF_CHAIN_HEADER *pCBuf;
    UINT1           *pu1OptBuf;            /* Rcvd IP Options */
    UINT4 u4AuthOffset;
    UINT4 u4AuthBufSize;
    UINT4 u4EncrOffset;
    UINT4 u4EncrBufSize;
    UINT4 u4IcvOffset;
    UINT4 u4Mode;
    UINT4 u4Action;
    UINT4 u4IfIndex;
    UINT4 u4BufSize;
    UINT4 u4Secv4Flags;
    UINT4 u4SecAssocIndex;
    UINT2 u2Fl_offs;
    UINT1 u1Tos;
    UINT1 u1Ttl;
}tSecv4TaskletData;


/* Global definitions */
/* ------------ Global Variables -------- */
#ifdef _SECV4AH_C_
UINT1 gau1v4AuthPadding[] =
      { 0,0,0,0,0,0,0,0,0,0,0,0};
#else
extern UINT1 gau1v4AuthPadding[];
#endif

#ifdef _SECV4INIT_C_
UINT1   gSecv4Status;
UINT1               gSecv4Version;
tTMO_SLL            Secv4SelList;
tTMO_SLL            Secv4AccessList;
tTMO_SLL            Secv4PolicyList;
tTMO_SLL            Secv4AssocList;
tTMO_SLL            Secv4SelFormattedList;
UINT4               u4Secv4AhEspIntruStatCount;
tTMO_DLL            Secv4ReasmList;    /* Reassembly queue */
tOsixQId            ntfyQId;
tOsixSemId          gSecv4SemId;


tSecv4Stat           gatIpsecv4Stat[SEC_MAX_STAT_COUNT];
tSecv4AhEspStat      gatIpsecv4AhEspStat[SEC_MAX_STAT_COUNT];
tSecv4AhEspIntruStat gatIpsecv4AhEspIntruStat[SEC_MAX_STAT_COUNT];
tSecv4DFrag          gatSecv4DFrag[SEC_MAX_STAT_COUNT];
UINT4                gu4Secv4Debug = 0;
tTimerListId         gSecv4TimerListId;
UINT4                gu4Secv4FreeSaIndex = 5000;
UINT4                gu4Secv4FreePolicyIndex = 9000; /* VPNC ADD */
UINT4                gu4Secv4FreeAccessIndex = 9000; /* VPNC ADD */
UINT4                gu4Secv4LanIndex = 1;
UINT1                gu1Secv4BypassCrypto = BYPASS_DISABLED;
UINT1   gSecv4DummyPktFlag = SEC_DISABLE; /* Part of Traffic Flow confidentiality
                                          generates dummy pkts based on the flag*/
UINT1   gSecv4DummyPktLength = SEC_DEFAULT_DUMMY_PKT_LEN;
#else
extern  tSecv4Stat           gatIpsecv4Stat[SEC_MAX_STAT_COUNT];
extern  tSecv4AhEspStat      gatIpsecv4AhEspStat[SEC_MAX_STAT_COUNT];
extern  tSecv4AhEspIntruStat gatIpsecv4AhEspIntruStat[SEC_MAX_STAT_COUNT];
extern  tSecv4DFrag          gatSecv4DFrag[SEC_MAX_STAT_COUNT];
extern  UINT1   gSecv4Status;
extern  UINT1 gSecv4Version;
extern  tTMO_SLL  Secv4SelList;       /* selector database */
extern  tTMO_SLL  Secv4AccessList;    /* access database */
extern  tTMO_SLL  Secv4PolicyList;    /* policy database */
extern  tTMO_SLL  Secv4AssocList;     /* secassoc database */
extern  tTMO_SLL  Secv4SelFormattedList; /* formatted selector database */
extern  tTMO_DLL            Secv4ReasmList;    /* Reassembly queue */
extern  UINT4     u4Secv4AhEspIntruStatCount; /*the count for ahespintru entries */
extern  UINT4     gu4Secv4Debug;
extern tTimerListId      gSecv4TimerListId;
extern UINT4             gu4Secv4FreeSaIndex ;
extern UINT4             gu4Secv4FreePolicyIndex ; /* VPNC ADD */
extern UINT4             gu4Secv4FreeAccessIndex ; /* VPNC ADD */
extern tOsixQId          ntfyQId;
extern tOsixSemId        gSecv4SemId;
PUBLIC UINT1             gu1Secv4BypassCrypto;
PUBLIC UINT1   gSecv4DummyPktFlag;
PUBLIC UINT1   gSecv4DummyPktLength;
#endif


/* Prototypes of functions used across IPSecv4 */

/* -------------- Sec Init -------------------*/
INT1
Secv4MemPoolInit  PROTO((VOID));

VOID Secv4MemPoolDeInit PROTO((VOID));
 /* ----------------  Sec SecMain ------------- */
UINT1 Secv4InMain PROTO((tIP_BUF_CHAIN_HEADER * , tSecv4Assoc * ,
                         UINT4 , UINT4 ,t_IP_HEADER *));
UINT1 Secv4OutMain PROTO (( tIP_BUF_CHAIN_HEADER * , tSecv4Assoc *,
                            UINT4 ,t_IP_HEADER *));
VOID Secv4HandleDummyPktGenTmrExpiry PROTO ((VOID));
UINT4 Secv4DummyPktGen PROTO ((tSecv4Assoc *, UINT4 u4IfIndex));
UINT4 Secv4DummyPktGenKern PROTO ((tSecv4Assoc * pSaEntry, UINT4 u4IfIndex, tMacAddr au1DestAddr));

/* --------------- Sec AH -------------------- */
UINT1 Secv4AhTransportEncode PROTO((tIP_BUF_CHAIN_HEADER * , 
                             tSecv4Assoc * ,UINT4 ,t_IP_HEADER *, UINT1 u1NextHdr));
UINT1 Secv4AhTunnelEncode PROTO((tIP_BUF_CHAIN_HEADER * ,
                        tSecv4Assoc * ,UINT4 ,t_IP_HEADER *, UINT1 u1NextHdr));
UINT1 Secv4AhTransportDecode PROTO((tIP_BUF_CHAIN_HEADER * , 
                           tSecv4Assoc * ,UINT4 ,UINT4 ,t_IP_HEADER *));
UINT1 Secv4AhTunnelDecode PROTO((tIP_BUF_CHAIN_HEADER * , 
                                tSecv4Assoc * ,UINT4 ,UINT4 ,t_IP_HEADER *)); 


/* ---------------- Sec ESP ----------------  */
UINT1 Secv4EspTransportEncode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
            UINT4 u4Interface,t_IP_HEADER *pIpHdr, UINT1 u1NextHdr);
UINT1
Secv4EspTunnelEncode (tCRU_BUF_CHAIN_HEADER *pCBuf, tSecv4Assoc * pSaEntry,
                      UINT4 u4IfIndex,t_IP_HEADER *pIpHdr, UINT1 u1NextHdr);
UINT1
Secv4UdpEspTunnelEncode (tCRU_BUF_CHAIN_HEADER *pCBuf, tSecv4Assoc * pSaEntry,
                      UINT4 u4IfIndex,t_IP_HEADER *pIpHdr, UINT1 u1NextHdr);
UINT1
Secv4EspTransportDecode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                         UINT4 u4ProcessedLen, UINT4 u4IfIndex,t_IP_HEADER *pIpHdr);
UINT1 Secv4EspTunnelDecode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
            UINT4 u4ProcessedLen,UINT4 u4Interface,t_IP_HEADER *pIpHdr);


/* ---------------- Sec Util ---------------- */
UINT4 Secv4GetAccessIndexWithGlobalAddr PROTO((UINT4 u4GlobalIpAddr,
             UINT2 u2DestPort, UINT4 u4IpAddr, UINT1 u1Direction));
INT1 Secv4SeqNumberVerification PROTO((UINT4 u4SeqNumber,
                              tSecv4Assoc * pSaEntry));
INT1
Secv4AddExtraHeaderToIP PROTO ((tIP_BUF_CHAIN_HEADER *, UINT1 *,
                                t_IP_HEADER *, UINT1));
INT1 Secv4ProcessOptionFields  PROTO ((tIP_BUF_CHAIN_HEADER *,UINT4 ,
                                       UINT1 *));

INT1 Secv4ConstructKey(UINT1 *pu1InKey, UINT1 u1Len , UINT1 *pu1OutKey);
VOID Secv4MuteIpHdr(t_IP_HEADER *IpHdr);
VOID Secv4PostInMsgToIP(tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4IfIndex, 
                        UINT1 u1AssocMode,UINT4 u4VpncIndex);
VOID Secv4PostOutMsgToIP(tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4IfIndex);
UINT2 Secv4IPCalcChkSum (UINT1 *pBuf, UINT4 u4Size);
UINT4  Secv4GetAccessIndexByRange PROTO((UINT4 u4StartSrcAddr,
                                         UINT4 u4EndSrcAddr));
INT1
Secv4CheckNoOfOutBoundBytesSecured PROTO((tSecv4Policy *pPolicy,
                                          tIP_BUF_CHAIN_HEADER * pCBuf));
INT1
Secv4CheckNoOfInBoundBytesSecured PROTO((tSecv4Assoc *pSecAssoc,
                                         tIP_BUF_CHAIN_HEADER * pCBuf));
VOID
Secv4AttachNewSaToPolicy PROTO ((tSecv4Policy *pu1Policy,
                                UINT1 au1SaBundle[SEC_MAX_SA_BUNDLE_LEN]));
UINT4 Secv4GetFreeSAIdx PROTO((VOID));
/* VPNC ADD */
UINT4 Secv4GetFreePolicyIdx PROTO((VOID));
UINT4 Secv4GetFreeAccessIdx PROTO((VOID));
/* VPNC END */
UINT1 * Secv4AllocateRcvdIPOpts (VOID);
VOID Secv4ReleaseRcvdIPOpts (UINT1 *pu1RcvdIPOpts);
VOID Secv4HandOverPktTOIP (VOID *pu4Data);
INT1 Secv4ProcessIkeInstallDynamicIpsecDBMsg (tIkeIPSecQMsg * pMsg);
INT4 Secv4SelectorCompareGetNext (tSecv4Selector *, INT4, INT4, INT4, INT4,
                                  INT4, INT4);
UINT4 Secv4GetAccessIndex (UINT4 u4SrcAddr, UINT4 u4DestAddr, 
  UINT2 u2SrcPort, UINT2 u2DstPort, UINT2 u2Protocol);
/* --------------- Sec Conf ---------------- */

INT1  Secv4AssocOutLookUp PROTO ((UINT4 u4SrcAddr,UINT4 u4DestAddr,
                                  UINT4 u4Protocol,UINT4 u4Index,
                                  tSecv4Policy **pPolicy,
                                  UINT2 u2SrcPort, UINT2 u2DstPort));
INT4 Secv4AssocVerify PROTO ((UINT4 u4SrcAddr,UINT4 u4DestAddr,
                              UINT4 u4Protocol,UINT4 u4Index,
                              UINT2 u2SrcPort, UINT2 u2DstPort));


UINT1  Secv4CheckPolicySaBundle PROTO ((tSecv4Policy *pEntry,UINT4 *pu4SaIndexes));
UINT1  Secv4GetDfBitStatus PROTO ((UINT4 u4IfIndex));
VOID Secv4DeleteIPSecDataBase PROTO ((VOID));
VOID Secv4DeleteSelectorEntries PROTO ((VOID));
VOID Secv4DeleteSecAssocEntries PROTO ((VOID));
VOID Secv4DeleteSecPolicyEntries PROTO ((VOID));
VOID Secv4DeleteSecAccessEntries PROTO ((VOID));


/*--------------------Sec Stat------------------------------------------*/

VOID   Secv4UpdateIfStats PROTO((UINT4 u4Index,UINT1 u1Direction ,
       UINT1 u1Flag));
VOID   Secv4UpdateAhEspStats PROTO((UINT4 u4Index,UINT1 u1Algo,
                            UINT1 u1Direction, UINT1 u1Flag));

VOID   Secv4UpdateAhEspIntruStats PROTO((UINT4 u4Index,UINT4 u4SrcAddr,
                                 UINT4 DesAddr,
                                 UINT1 u1Prot , UINT4 u4Time));
VOID Secv4GetAssocEntryCount (UINT4 *u4IpSecSAsActive);

 /* --------------------------SecLow-------------------------------------*/

VOID Secv4DeletePolicyPtrInSel PROTO ((UINT4 Index));
VOID Secv4DeleteSAPtrInPolicy PROTO ((tSecv4Assoc *pSecAssoc));
tSecv4Selector *  Secv4SelGetEntry PROTO ((UINT4 SelIndex,
                                           INT4 i4PktDirection));

tSecv4Selector *  Secv4SelGetEntry2 PROTO ((UINT4 IfIndex,
                                            UINT4 u4AccessIndex, 
                                            UINT4 ProtId, 
                                            INT4 i4Port));

tSecv4Selector *  Secv4SelGetFormattedEntry PROTO ((UINT4 IfIndex,
                                                    UINT4 u4ProtId, 
                                                    UINT4 u4AccessIndex, 
                                                    INT4 i4Port,
                                                    INT4 i4PktDirection));

tSecv4Selector *  Secv4SelGetFormattedEntryDir PROTO ((UINT4 IfIndex,
                                                    UINT4 ProtId, 
                                                    UINT4 u4AccessIndex, 
                                                    INT4 i4Port,
                                                    INT4 i4PktDirection));

tSecv4Access *  Secv4AccessGetEntry PROTO ((UINT4 u4AccessIndex));

tSecv4Policy *  Secv4PolicyGetEntry PROTO ((UINT4 Index));

INT4  Secv4SelectorCompare PROTO ((tSecv4Selector *pEntry ,
tSecv4Selector *pNextEntry));
tSecv4Assoc * Secv4AssocGetEntry PROTO ((UINT4 Index));
tSecv4Assoc *
Secv4GetAssocEntry PROTO ((UINT4 u4SpiIndex,UINT4 DestAddr,
UINT1 u1Protocol ));
INT1 Secv4ConstSecList PROTO ((VOID));
VOID Secv4UpdateSecAssocSize PROTO ((VOID));
VOID Secv4ConstAccessList PROTO((VOID));
INT1 Secv4DeleteFormattedSelectorEntries PROTO((VOID));
UINT1 Secv4CheckOverlappingAccessList(tSecv4Access *pEntry);

/* -------------------- IKE releated Function protos-----*/
VOID Secv4RequeStIkeForNewOrReKey PROTO((UINT4 u4IfIndex,UINT4 u4Protocol,UINT4 u4Port,
                                         UINT4 u4AccessIndex, UINT4 u4PktDir,
                                         UINT1 u1NewOrReKeyFlag, 
                                         tSecv4Policy *pSecv4Policy));

VOID Secv4IntimateIkeInvalidSpi PROTO((UINT4 u4Spi, UINT4 u4Dest, UINT4 u4Src,
                                       UINT1 u1Protocol));
VOID
Secv4IntimateIkeDeletionOfSa PROTO ((tSecv4Policy *pSecv4Policy,
                            UINT4 u4SecAssocIndex));
VOID
Secv4DeleteSa PROTO ((UINT4 u4SecAssocIndex));


/*---------------------------Util------------------------------------------*/
INT1
Secv4SetIkeVersion PROTO ((UINT4 u4PolicyIndex, UINT1 u1IkeVersion));

INT1 
Secv4SetAccessListInfo PROTO ((UINT4 u4AccessListIndex, UINT2 u2LocalStartPort,
                               UINT2 u2LocalEndPort, UINT2 u2RemoteStartPort,
                               UINT2 u2RemoteEndPort, UINT4 u4Protocol));

VOID Secv4DeleteAssoc(UINT4 u4SrcAddr,UINT4 u4DestAddr,UINT4 IfIndex,
                              UINT4 u4Protocol, UINT2, UINT2);

UINT2 Secv4GetSecAssocOverHead PROTO((UINT4 u4SrcAddr, UINT4 u4DestAddr, 
                                      UINT2 u2Index, UINT1 u1Protocol));
    
/*-------------------------- Sec Tmr --------------------------------------*/

VOID Secv4InitTimer PROTO ((VOID));
VOID Secv4DeInitTimer PROTO((VOID));

VOID Secv4StartTimer PROTO ((tSecv4Timer * pTimer,UINT1 u1TimeId,
                             UINT4 TimeOut));
VOID Secv4RekeySa PROTO ((tSecv4Timer *pTimer));
VOID Secv4StopTimer PROTO ((tSecv4Timer * pTimer));
VOID Secv4HandleReAsmTimerExpiry PROTO ((tSecv4Timer * pTimer));

/*-----------------------------Secprcfrg------------------------------------*/

INT4
Secv4FragmentAndSend PROTO ((t_IP_HEADER * pIpHdr, 
                             tCRU_BUF_CHAIN_HEADER * pBuf, 
                             UINT4 u4IfIndex));

tCRU_BUF_CHAIN_HEADER *
Secv4Reassemble PROTO((t_IP_HEADER * pIpHdr, tCRU_BUF_CHAIN_HEADER * pBuf,
                       UINT2 u2Port));

INT4 Secv4ReassemblyCleanup PROTO((VOID));
INT4 Secv4GetIfMtu PROTO ((UINT4 u4IfIndex, UINT4 *pu4IfMtu));

/* -------------------- Ipsec Vpn  Protos ----------------*/
INT1
VpnFillIpsecParams PROTO((tVpnPolicy * ));

INT1
VpnDeleteIpsecParams PROTO ((tVpnPolicy * ));

INT1
VpnFillSecAssocParams PROTO((UINT4 ,UINT4 , UINT4 ,UINT1 , UINT4 , UINT4));

INT1
VpnFillAuthSecAssocParams PROTO((UINT4  ,UINT1 , UINT1 , UINT1 *, UINT1 , UINT1*));

INT1
VpnFillAuthParams PROTO((UINT4 , UINT1 , UINT1 , UINT1 *));

INT4
VpnFillEncrParams PROTO((UINT4 , UINT1 , UINT1 , UINT1 *));

INT1
VpnFillAccessParams PROTO((UINT4 , UINT4 , UINT4 ,UINT4 , UINT4 ));

INT1
VpnFillPolicyParams PROTO((UINT4  , UINT1 , UINT1 , UINT1));

INT1
VpnFillSelectorParams PROTO((UINT4 , UINT4 ,UINT4 , UINT4 ,UINT4 , UINT4 ,UINT4 ));

typedef struct SECV4UDPHDR
{
    UINT2     u2SrcPort;
    UINT2     u2DestPort;
    UINT2     u2Len;
    UINT2     u2Cksum;
} tSecv4UdpHdr;

#ifdef _SECV4INIT_C_
UINT1  (*EncodeProcedure[SEC_MAX_FUNCTIONS + 1])(tIP_BUF_CHAIN_HEADER * pCBuf, 
                                       tSecv4Assoc *pSaEntry,
                                             UINT4 u4Interface,
                                             t_IP_HEADER *pIpHdr, UINT1 u1NextHdr) =
{
     Secv4EspTunnelEncode,
     Secv4EspTransportEncode,
     Secv4AhTunnelEncode,
     Secv4AhTransportEncode,
     Secv4UdpEspTunnelEncode
};

UINT1  (*DecodeProcedure[SEC_MAX_FUNCTIONS])(tCRU_BUF_CHAIN_HEADER *pCBuf, 
                              tSecv4Assoc *pSaEntry,
                                    UINT4 u4ProcessedLen, 
                     UINT4 u4Interface,
                                    t_IP_HEADER *pIpHdr) =
{
     Secv4EspTunnelDecode,
     Secv4EspTransportDecode,
     Secv4AhTunnelDecode,
     Secv4AhTransportDecode
};


UINT1  gau1GetProcessFnPtr[2][2] =
{

{0,1}, /* 0 - Esp Tunnel and 1 - Esp Transport */
{2,3}  /* 2 - Ah  Tunnel and 3 - Ah  Transport */

};

tSecv4Timer        gSecv4TmrDummyPkt;
#else /* _SECV4INIT_C_ */
extern INT1  (*EncodeProcedure[SEC_MAX_FUNCTIONS + 1])(tIP_BUF_CHAIN_HEADER * pCBuf,
                                                   tSecv4Assoc *pSaEntry,
                                                   UINT4 u4Interface,
                                                   t_IP_HEADER *pIpHdr, UINT1 u1NextHdr);

extern INT1  (*DecodeProcedure[SEC_MAX_FUNCTIONS])(tIP_BUF_CHAIN_HEADER * pCBuf,
                   tSecv4Assoc *pSaEntry, UINT4 u4ProcessedLen, 
                   UINT4 u4Interface,t_IP_HEADER *pIpHdr);

extern UINT1  gau1GetProcessFnPtr[2][2];
extern tSecv4Timer        gSecv4TmrDummyPkt;
#endif /* _SECV4INIT_C_ */

/* Protos for external functions */
INT1 Secv4AlgoProcessPkt (tSecv4TaskletData *pAlgoContext,
                          UINT4 u4Mode, UINT4 u4Action);

INT1 Secv4SoftProcessPkt PROTO((tSecv4TaskletData *,UINT4 ,UINT4 ));

INT1 Secv4ArpResolve (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType);


VOID        Secv4EspIncomingTunnel (VOID *pData);
VOID        Secv4EspIncomingTransport (VOID *pData);
VOID        Secv4AhOutGoingTunnel (VOID *pData);
VOID        Secv4AhOutGoingTransport (VOID *pData);
VOID        Secv4AhIncomingTunnel (VOID *pData);
VOID        Secv4AhIncomingTransport (VOID *pData);

extern VOID RegisterFSVPN(VOID);
extern VOID IncIkePhase2ReKey(VOID);

/* --------------- Post Crypto processing APIS ----------------------*/
VOID
Secv4EspOutGoingPerformCB (VOID *pData);

VOID
Secv4AhOutGoingPerformCB (VOID *pData);

VOID
Secv4EspIncomingPerformCB (VOID *pData);

VOID
Secv4AhIncomingPerformCB (VOID *pData);
VOID
Secv4HandleFailueAfterCryptoProcess (tSecv4TaskletData *pTemp);
INT1
Secv4AlgoProcessIpsec (tSecv4TaskletData *pSecv4TData,
                       UINT1 *pu1AuthICVEncrIV, UINT1 u1Flag);

/* ------------------API related to  Hardware Accelerator ----------*/
INT1 Secv4HwInit PROTO ((VOID));
INT1 Secv4HwDeInit PROTO ((VOID));
INT1 Secv4InitSesInHwAccel PROTO ((UINT4 u4SecAssocIndex, UINT4 u4Action));
INT1 Secv4RemSesInHwAccel PROTO ((VOID *pSessionCtx));
INT1 Secv4MemInitCallBackCxt PROTO ((VOID));
INT1 Secv4MemDeInitCallBackCxt PROTO ((VOID));

VOID Secv4HwInitForKernel (VOID);
VOID Secv4TaskletSchedFnInKernel (tSecv4TaskletData * pSecv4TskletData);
/* ----------------- IP related utility Fn Prototypes------------------*/
VOID
Secv4PutIpHdr PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, INT1 i1flag));

INT1 Secv4GetBypassCapability PROTO ((INT4* pi4RetValFsVpnGlobalStatus));
INT1 Secv4SetBypassCapability PROTO ((INT4 i4SetValFsVpnGlobalStatus));
UINT1 VpnUtilRaAddrDbLookUp (UINT4 u4DestAddr);
#endif

