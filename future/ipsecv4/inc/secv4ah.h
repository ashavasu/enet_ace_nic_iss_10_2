
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id:secv4ah.h 
*
* Description:This file contains the macros required for AH Module.
*
*******************************************************************/


#ifndef _SECV4AH_H_
#define _SECV4AH_H_

/* Macros used by secv4ah.c */
#define SEC_AUTH_RSVD                         0
#define SEC_AH_FACTOR                         4 
#define SEC_AUTH_SEQ_NUM_OFFSET               8

/* Authentication Header */
typedef struct tAUTHHDR {
    UINT1 u1NxtHdr;
    UINT1 u1Len;
    UINT2 u2Rsvd;
    UINT4 u4SPI;
    UINT4 u4SeqNumber;
}tSecv4AuthHdr ;

#endif
