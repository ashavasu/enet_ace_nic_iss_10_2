
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4dyndb.h,v 1.4 2011/11/03 06:24:44 siva Exp $
*
* Description:This file contains the prototypes 
*             required for installing ipsec sa's
*
*******************************************************************/

#ifndef __IPSECV4DYNDB_H__
#define __IPSECV4DYNDB_H__ 1


/* ----- Function protos -----*/

INT1 Secv4InstallSecPolicy PROTO ((UINT4 u4PolicyIndex, UINT1 *pu1PolicySaBundle, 
                                  UINT1 u1IkeVersion));

INT1 Secv4InstallSecAccess PROTO ((UINT4 u4AccessIndex,
                                   UINT4 u4SrcAddress,  UINT4 u4SrcMask, 
                                   UINT4 u4DestAddress, UINT4 u4DestMask));

INT1 Secv4InstallSecSelector PROTO ((UINT4 u4VpncIfaceIndex, 
                                     UINT4 u4PktDirection,
                                     UINT4 u4SelAccessIndex,
                                     UINT4 u4PolicyIndex));
#endif /* __IPSECV4DYNDB_H__ */
