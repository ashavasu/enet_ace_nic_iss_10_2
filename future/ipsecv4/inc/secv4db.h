/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: secv4db.h,v 1.3 2010/11/02 09:36:39 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef __FSIPV4SECMBDB_H__
#define __FSIPV4SECMBDB_H__

/* INDEX Table Entries */
UINT1 Fsipv4SecSelectorTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER32, SNMP_DATA_TYPE_INTEGER, SNMP_DATA_TYPE_INTEGER32, SNMP_DATA_TYPE_INTEGER32, SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv4SecAccessTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv4SecPolicyTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv4SecAssocTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv4SecDFragBitTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv4SecIfStatsTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Fsipv4SecAhEspStatsTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsipv4SecAhEspIntruTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER32};

/*  MIB information  */
UINT4 fssec4 [ ] = { 1,3,6,1,4,1,2076,67 };
tSNMP_OID_TYPE fssec4OID = {8, fssec4};


/*  OID Description*/
UINT4 Fsipv4SecGlobalStatus [ ] = {1,3,6,1,4,1,2076,67,1,1};
UINT4 Fsipv4SecVersion [ ] = {1,3,6,1,4,1,2076,67,1,2};
UINT4 Fsipv4SecGlobalDebug [ ] = {1,3,6,1,4,1,2076,67,1,3};
UINT4 Fsipv4SecMaxSA [ ] = {1,3,6,1,4,1,2076,67,1,4};
UINT4 Fsipv4SelIfIndex [ ] = {1,3,6,1,4,1,2076,67,2,1,1,1};
UINT4 Fsipv4SelProtoIndex [ ] = {1,3,6,1,4,1,2076,67,2,1,1,2};
UINT4 Fsipv4SelAccessIndex [ ] = {1,3,6,1,4,1,2076,67,2,1,1,3};
UINT4 Fsipv4SelPort [ ] = {1,3,6,1,4,1,2076,67,2,1,1,4};
UINT4 Fsipv4SelPktDirection [ ] = {1,3,6,1,4,1,2076,67,2,1,1,5};
UINT4 Fsipv4SelFilterFlag [ ] = {1,3,6,1,4,1,2076,67,2,1,1,6};
UINT4 Fsipv4SelPolicyIndex [ ] = {1,3,6,1,4,1,2076,67,2,1,1,7};
UINT4 Fsipv4SelStatus [ ] = {1,3,6,1,4,1,2076,67,2,1,1,8};
UINT4 Fsipv4SecAccessIndex [ ] = {1,3,6,1,4,1,2076,67,2,2,1,1};
UINT4 Fsipv4SecAccessStatus [ ] = {1,3,6,1,4,1,2076,67,2,2,1,2};
UINT4 Fsipv4SecSrcNet [ ] = {1,3,6,1,4,1,2076,67,2,2,1,3};
UINT4 Fsipv4SecSrcMask [ ] = {1,3,6,1,4,1,2076,67,2,2,1,4};
UINT4 Fsipv4SecDestNet [ ] = {1,3,6,1,4,1,2076,67,2,2,1,5};
UINT4 Fsipv4SecDestMask [ ] = {1,3,6,1,4,1,2076,67,2,2,1,6};
UINT4 Fsipv4SecPolicyIndex [ ] = {1,3,6,1,4,1,2076,67,2,3,1,1};
UINT4 Fsipv4SecPolicyFlag [ ] = {1,3,6,1,4,1,2076,67,2,3,1,2};
UINT4 Fsipv4SecPolicyMode [ ] = {1,3,6,1,4,1,2076,67,2,3,1,3};
UINT4 Fsipv4SecPolicySaBundle [ ] = {1,3,6,1,4,1,2076,67,2,3,1,4};
UINT4 Fsipv4SecPolicyStatus [ ] = {1,3,6,1,4,1,2076,67,2,3,1,5};
UINT4 Fsipv4SecAssocIndex [ ] = {1,3,6,1,4,1,2076,67,2,4,1,1};
UINT4 Fsipv4SecAssocDstAddr [ ] = {1,3,6,1,4,1,2076,67,2,4,1,2};
UINT4 Fsipv4SecAssocProtocol [ ] = {1,3,6,1,4,1,2076,67,2,4,1,3};
UINT4 Fsipv4SecAssocSpi [ ] = {1,3,6,1,4,1,2076,67,2,4,1,4};
UINT4 Fsipv4SecAssocMode [ ] = {1,3,6,1,4,1,2076,67,2,4,1,5};
UINT4 Fsipv4SecAssocAhAlgo [ ] = {1,3,6,1,4,1,2076,67,2,4,1,6};
UINT4 Fsipv4SecAssocAhKey [ ] = {1,3,6,1,4,1,2076,67,2,4,1,7};
UINT4 Fsipv4SecAssocEspAlgo [ ] = {1,3,6,1,4,1,2076,67,2,4,1,8};
UINT4 Fsipv4SecAssocEspKey [ ] = {1,3,6,1,4,1,2076,67,2,4,1,9};
UINT4 Fsipv4SecAssocEspKey2 [ ] = {1,3,6,1,4,1,2076,67,2,4,1,10};
UINT4 Fsipv4SecAssocEspKey3 [ ] = {1,3,6,1,4,1,2076,67,2,4,1,11};
UINT4 Fsipv4SecAssocLifetimeInBytes [ ] = {1,3,6,1,4,1,2076,67,2,4,1,12};
UINT4 Fsipv4SecAssocLifetime [ ] = {1,3,6,1,4,1,2076,67,2,4,1,13};
UINT4 Fsipv4SecAssocAntiReplay [ ] = {1,3,6,1,4,1,2076,67,2,4,1,14};
UINT4 Fsipv4SecAssocStatus [ ] = {1,3,6,1,4,1,2076,67,2,4,1,15};
UINT4 Fsipv4SecDFragBitIndex [ ] = {1,3,6,1,4,1,2076,67,2,5,1,1};
UINT4 Fsipv4SecDFragBitStatus [ ] = {1,3,6,1,4,1,2076,67,2,5,1,2};
UINT4 Fsipv4SecIfIndex [ ] = {1,3,6,1,4,1,2076,67,3,1,1,1};
UINT4 Fsipv4SecIfInPkts [ ] = {1,3,6,1,4,1,2076,67,3,1,1,2};
UINT4 Fsipv4SecIfOutPkts [ ] = {1,3,6,1,4,1,2076,67,3,1,1,3};
UINT4 Fsipv4SecIfPktsApply [ ] = {1,3,6,1,4,1,2076,67,3,1,1,4};
UINT4 Fsipv4SecIfPktsDiscard [ ] = {1,3,6,1,4,1,2076,67,3,1,1,5};
UINT4 Fsipv4SecIfPktsBypass [ ] = {1,3,6,1,4,1,2076,67,3,1,1,6};
UINT4 Fsipv4SecAhEspIfIndex [ ] = {1,3,6,1,4,1,2076,67,3,2,1,1};
UINT4 Fsipv4SecInAhPkts [ ] = {1,3,6,1,4,1,2076,67,3,2,1,2};
UINT4 Fsipv4SecOutAhPkts [ ] = {1,3,6,1,4,1,2076,67,3,2,1,3};
UINT4 Fsipv4SecAhPktsAllow [ ] = {1,3,6,1,4,1,2076,67,3,2,1,4};
UINT4 Fsipv4SecAhPktsDiscard [ ] = {1,3,6,1,4,1,2076,67,3,2,1,5};
UINT4 Fsipv4SecInEspPkts [ ] = {1,3,6,1,4,1,2076,67,3,2,1,6};
UINT4 Fsipv4SecOutEspPkts [ ] = {1,3,6,1,4,1,2076,67,3,2,1,7};
UINT4 Fsipv4SecEspPktsAllow [ ] = {1,3,6,1,4,1,2076,67,3,2,1,8};
UINT4 Fsipv4SecEspPktsDiscard [ ] = {1,3,6,1,4,1,2076,67,3,2,1,9};
UINT4 Fsipv4SecAhEspIntruIndex [ ] = {1,3,6,1,4,1,2076,67,3,3,1,1};
UINT4 Fsipv4SecAhEspIntruIfIndex [ ] = {1,3,6,1,4,1,2076,67,3,3,1,2};
UINT4 Fsipv4SecAhEspIntruSrcAddr [ ] = {1,3,6,1,4,1,2076,67,3,3,1,3};
UINT4 Fsipv4SecAhEspIntruDestAddr [ ] = {1,3,6,1,4,1,2076,67,3,3,1,4};
UINT4 Fsipv4SecAhEspIntruProto [ ] = {1,3,6,1,4,1,2076,67,3,3,1,5};
UINT4 Fsipv4SecAhEspIntruTime [ ] = {1,3,6,1,4,1,2076,67,3,3,1,6};

/* MBDB entry list */
tMbDbEntry fssec4MibEntry[]= {
{{10, Fsipv4SecGlobalStatus},   NULL, Fsipv4SecGlobalStatusGet, Fsipv4SecGlobalStatusSet, Fsipv4SecGlobalStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{10, Fsipv4SecVersion},   NULL, Fsipv4SecVersionGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0},

{{10, Fsipv4SecGlobalDebug},   NULL, Fsipv4SecGlobalDebugGet, Fsipv4SecGlobalDebugSet, Fsipv4SecGlobalDebugTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0},

{{10, Fsipv4SecMaxSA},   NULL, Fsipv4SecMaxSAGet, Fsipv4SecMaxSASet, Fsipv4SecMaxSATest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0},

{{12, Fsipv4SelIfIndex},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelIfIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SelProtoIndex},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelProtoIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SelAccessIndex},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelAccessIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SelPort},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelPortGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SelPktDirection},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelPktDirectionGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SelFilterFlag},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelFilterFlagGet, Fsipv4SelFilterFlagSet, Fsipv4SelFilterFlagTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SelPolicyIndex},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelPolicyIndexGet, Fsipv4SelPolicyIndexSet, Fsipv4SelPolicyIndexTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SelStatus},   GetNextIndexFsipv4SecSelectorTable, Fsipv4SelStatusGet, Fsipv4SelStatusSet, Fsipv4SelStatusTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READCREATE, Fsipv4SecSelectorTableINDEX, 5},

{{12, Fsipv4SecAccessIndex},   GetNextIndexFsipv4SecAccessTable, Fsipv4SecAccessIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecAccessTableINDEX, 1},

{{12, Fsipv4SecAccessStatus},   GetNextIndexFsipv4SecAccessTable, Fsipv4SecAccessStatusGet, Fsipv4SecAccessStatusSet, Fsipv4SecAccessStatusTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READCREATE, Fsipv4SecAccessTableINDEX, 1},

{{12, Fsipv4SecSrcNet},   GetNextIndexFsipv4SecAccessTable, Fsipv4SecSrcNetGet, Fsipv4SecSrcNetSet, Fsipv4SecSrcNetTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsipv4SecAccessTableINDEX, 1},

{{12, Fsipv4SecSrcMask},   GetNextIndexFsipv4SecAccessTable, Fsipv4SecSrcMaskGet, Fsipv4SecSrcMaskSet, Fsipv4SecSrcMaskTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsipv4SecAccessTableINDEX, 1},

{{12, Fsipv4SecDestNet},   GetNextIndexFsipv4SecAccessTable, Fsipv4SecDestNetGet, Fsipv4SecDestNetSet, Fsipv4SecDestNetTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsipv4SecAccessTableINDEX, 1},

{{12, Fsipv4SecDestMask},   GetNextIndexFsipv4SecAccessTable, Fsipv4SecDestMaskGet, Fsipv4SecDestMaskSet, Fsipv4SecDestMaskTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsipv4SecAccessTableINDEX, 1},

{{12, Fsipv4SecPolicyIndex},   GetNextIndexFsipv4SecPolicyTable, Fsipv4SecPolicyIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecPolicyTableINDEX, 1},

{{12, Fsipv4SecPolicyFlag},   GetNextIndexFsipv4SecPolicyTable, Fsipv4SecPolicyFlagGet, Fsipv4SecPolicyFlagSet, Fsipv4SecPolicyFlagTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecPolicyTableINDEX, 1},

{{12, Fsipv4SecPolicyMode},   GetNextIndexFsipv4SecPolicyTable, Fsipv4SecPolicyModeGet, Fsipv4SecPolicyModeSet, Fsipv4SecPolicyModeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecPolicyTableINDEX, 1},

{{12, Fsipv4SecPolicySaBundle},   GetNextIndexFsipv4SecPolicyTable, Fsipv4SecPolicySaBundleGet, Fsipv4SecPolicySaBundleSet, Fsipv4SecPolicySaBundleTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv4SecPolicyTableINDEX, 1},

{{12, Fsipv4SecPolicyStatus},   GetNextIndexFsipv4SecPolicyTable, Fsipv4SecPolicyStatusGet, Fsipv4SecPolicyStatusSet, Fsipv4SecPolicyStatusTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READCREATE, Fsipv4SecPolicyTableINDEX, 1},

{{12, Fsipv4SecAssocIndex},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocDstAddr},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocDstAddrGet, Fsipv4SecAssocDstAddrSet, Fsipv4SecAssocDstAddrTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocProtocol},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocProtocolGet, Fsipv4SecAssocProtocolSet, Fsipv4SecAssocProtocolTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocSpi},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocSpiGet, Fsipv4SecAssocSpiSet, Fsipv4SecAssocSpiTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocMode},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocModeGet, Fsipv4SecAssocModeSet, Fsipv4SecAssocModeTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocAhAlgo},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocAhAlgoGet, Fsipv4SecAssocAhAlgoSet, Fsipv4SecAssocAhAlgoTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocAhKey},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocAhKeyGet, Fsipv4SecAssocAhKeySet, Fsipv4SecAssocAhKeyTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocEspAlgo},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocEspAlgoGet, Fsipv4SecAssocEspAlgoSet, Fsipv4SecAssocEspAlgoTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocEspKey},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocEspKeyGet, Fsipv4SecAssocEspKeySet, Fsipv4SecAssocEspKeyTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocEspKey2},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocEspKey2Get, Fsipv4SecAssocEspKey2Set, Fsipv4SecAssocEspKey2Test, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocEspKey3},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocEspKey3Get, Fsipv4SecAssocEspKey3Set, Fsipv4SecAssocEspKey3Test, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocLifetimeInBytes},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocLifetimeInBytesGet, Fsipv4SecAssocLifetimeInBytesSet, Fsipv4SecAssocLifetimeInBytesTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocLifetime},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocLifetimeGet, Fsipv4SecAssocLifetimeSet, Fsipv4SecAssocLifetimeTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocAntiReplay},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocAntiReplayGet, Fsipv4SecAssocAntiReplaySet, Fsipv4SecAssocAntiReplayTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecAssocStatus},   GetNextIndexFsipv4SecAssocTable, Fsipv4SecAssocStatusGet, Fsipv4SecAssocStatusSet, Fsipv4SecAssocStatusTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READCREATE, Fsipv4SecAssocTableINDEX, 1},

{{12, Fsipv4SecDFragBitIndex},   GetNextIndexFsipv4SecDFragBitTable, Fsipv4SecDFragBitIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv4SecDFragBitTableINDEX, 1},

{{12, Fsipv4SecDFragBitStatus},   GetNextIndexFsipv4SecDFragBitTable, Fsipv4SecDFragBitStatusGet, Fsipv4SecDFragBitStatusSet, Fsipv4SecDFragBitStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsipv4SecDFragBitTableINDEX, 1},

{{12, Fsipv4SecIfIndex},   GetNextIndexFsipv4SecIfStatsTable, Fsipv4SecIfIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsipv4SecIfStatsTableINDEX, 1},

{{12, Fsipv4SecIfInPkts},   GetNextIndexFsipv4SecIfStatsTable, Fsipv4SecIfInPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecIfStatsTableINDEX, 1},

{{12, Fsipv4SecIfOutPkts},   GetNextIndexFsipv4SecIfStatsTable, Fsipv4SecIfOutPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecIfStatsTableINDEX, 1},

{{12, Fsipv4SecIfPktsApply},   GetNextIndexFsipv4SecIfStatsTable, Fsipv4SecIfPktsApplyGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecIfStatsTableINDEX, 1},

{{12, Fsipv4SecIfPktsDiscard},   GetNextIndexFsipv4SecIfStatsTable, Fsipv4SecIfPktsDiscardGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecIfStatsTableINDEX, 1},

{{12, Fsipv4SecIfPktsBypass},   GetNextIndexFsipv4SecIfStatsTable, Fsipv4SecIfPktsBypassGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecIfStatsTableINDEX, 1},

{{12, Fsipv4SecAhEspIfIndex},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecAhEspIfIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecInAhPkts},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecInAhPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecOutAhPkts},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecOutAhPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecAhPktsAllow},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecAhPktsAllowGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecAhPktsDiscard},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecAhPktsDiscardGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecInEspPkts},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecInEspPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecOutEspPkts},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecOutEspPktsGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecEspPktsAllow},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecEspPktsAllowGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecEspPktsDiscard},   GetNextIndexFsipv4SecAhEspStatsTable, Fsipv4SecEspPktsDiscardGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspStatsTableINDEX, 1},

{{12, Fsipv4SecAhEspIntruIndex},   GetNextIndexFsipv4SecAhEspIntruTable, Fsipv4SecAhEspIntruIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsipv4SecAhEspIntruTableINDEX, 1},

{{12, Fsipv4SecAhEspIntruIfIndex},   GetNextIndexFsipv4SecAhEspIntruTable, Fsipv4SecAhEspIntruIfIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsipv4SecAhEspIntruTableINDEX, 1},

{{12, Fsipv4SecAhEspIntruSrcAddr},   GetNextIndexFsipv4SecAhEspIntruTable, Fsipv4SecAhEspIntruSrcAddrGet, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Fsipv4SecAhEspIntruTableINDEX, 1},

{{12, Fsipv4SecAhEspIntruDestAddr},   GetNextIndexFsipv4SecAhEspIntruTable, Fsipv4SecAhEspIntruDestAddrGet, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Fsipv4SecAhEspIntruTableINDEX, 1},

{{12, Fsipv4SecAhEspIntruProto},   GetNextIndexFsipv4SecAhEspIntruTable, Fsipv4SecAhEspIntruProtoGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsipv4SecAhEspIntruTableINDEX, 1},

{{12, Fsipv4SecAhEspIntruTime},   GetNextIndexFsipv4SecAhEspIntruTable, Fsipv4SecAhEspIntruTimeGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsipv4SecAhEspIntruTableINDEX, 1},


};

tMibData fssec4Entry = { 61, fssec4MibEntry };
#endif /* __MBDB_H__ */
