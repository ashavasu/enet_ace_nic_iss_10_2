#!/usr/bin/perl
#Copyright (C) 2014 Aricent Inc . All Rights Reserved

$start_replace_text = "START_REPLACE_";
$end_replace_text = "END_REPLACE_";
$super_replace_text = "REPLACE_";
$replace_id = 1;
$replace_special = "@";
$flag = 0;
$cmd = "";

$filename = $ARGV[0] . "/add.out";

open add_file, "<$filename" or die "cannot open $filename";
@add_read = <add_file>;
close(add_file);

$filename = $ARGV[0] . "/clicmds.out";

open cli_file, "<$filename" or die "cannot open $filename";
{
    $/ = undef;
    $cli_read = <cli_file>;
}
close(cli_file);

$start_replace_tag = $start_replace_text . $replace_id;
$end_replace_tag = $end_replace_text . $replace_id;
$replace_tag = $super_replace_text . $replace_id . $replace_special;

foreach $line (@add_read)
{
    if ($line =~ /$start_replace_tag/)
    {
        $flag = 1;
        next;
    }
    elsif ($line =~ /$end_replace_tag/)
    {
        $flag = 0;
        $cli_read =~ s/$replace_tag/$cmd/s;
#       print "\n----- Replace $replace_tag successful -----\n"; # Debug purpose
        $cmd = "";
        $replace_id++;
        $start_replace_tag = $start_replace_text . $replace_id;
        $end_replace_tag = $end_replace_text . $replace_id;
        $replace_tag = $super_replace_text . $replace_id . $replace_special;
    }
    elsif ($flag == 1)
    {
        $cmd = $cmd . $line;
    }
}

open cli_out, ">$ARGV[1]" or die "cannot open $ARGV[1]";
print cli_out $cli_read;
close(cli_out);
