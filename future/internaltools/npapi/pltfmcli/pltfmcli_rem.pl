#!/usr/bin/perl
#Copyright (C) 2014 Aricent Inc . All Rights Reserved

$replace_head = "REPLACE_";
$replace_special = "@";
$replace_tail = "\n";
$replace_id = 1;
$index = 0;
$cmd = "";

$filename = $ARGV[0] . "/" . $ARGV[1];

$comment = "perl -i -ne 'print unless m/^\\/\\*\\ \\\$Id\\:/' $filename";
system($comment);
$comment = "perl -i -ne 'print if /\\S/' $filename";
system($comment);

open rem_file, "<$filename" or die "cannot open $filename";
@rem_read = <rem_file>;
close(rem_file);

open cli_file, "<$ARGV[2]" or die "cannot open $ARGV[2]";
{
    $/ = undef;
    $cli_read = <cli_file>;
}
close(cli_file);

$rem_end_index = @rem_read;

if ($rem_read[$index] =~ /START_REMOVAL_BLOCK/)
{
    for ($index = $index + 1; $index < $rem_end_index; $index++)
    {
	if (($cmd eq "") && ($rem_read[$index] =~ /END_REMOVAL_BLOCK/))
	{
#	    print "\nNo commands specified to remove\n"; # Debug purpose
            $index += 1;
            last;
	}
        elsif ($rem_read[$index + 1] =~ /COMMAND|END_REMOVAL_BLOCK/)
        {
            $cmd = $cmd . $rem_read[$index];
            $q_cmd = join q{\s*}, map {quotemeta} split /\s*/, $cmd;
            $cli_read =~ s/$q_cmd//s;
#	    print "\nRemoving a specified command\n"; # Debug purpose
            $cmd = "";
            if ($rem_read[$index + 1] =~ /END_REMOVAL_BLOCK/)
            {
                $index +=2 ;
                last;
            }
        }
        else
        {
            $cmd = $cmd . $rem_read[$index];
        }
    }
}

if ($rem_read[$index] =~ /START_REPLACE_BLOCK/)
{
    for ($index = $index + 1; $index < $rem_end_index; $index++)
    {
	if (($cmd eq "") && ($rem_read[$index] =~ /END_REPLACE_BLOCK/))
	{
#	    print "\nNo commands specified to replace\n"; # Debug purpose
            last;
	}
        elsif ($rem_read[$index + 1] =~ /COMMAND|END_REPLACE_BLOCK/)
        {
            $cmd = $cmd . $rem_read[$index];
            $replace = $replace_head . $replace_id . $replace_special;
            $q_cmd = join q{\s*}, map {quotemeta} split /\s*/, $cmd;
#	    print "Replace command $replace_id"; # Debug purpose
            $cli_read =~ s/$q_cmd/$replace_tail$replace/s;
            $cmd = "";
            $replace_id++;
            if ($rem_read[$index + 1] =~ /END_REPLACE_BLOCK/)
            {
                last;
            }
        }
        else
        {
            $cmd = $cmd . $rem_read[$index];
        }
    }
}

$filename = $ARGV[0] . "/clicmds.out";

open cli_out, ">$filename" or die "cannot open $filename";
print cli_out $cli_read;
close(cli_out);
