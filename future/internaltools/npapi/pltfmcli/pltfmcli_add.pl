#!/usr/bin/perl
#Copyright (C) 2014 Aricent Inc . All Rights Reserved

$replace_id = 1;
$start_replace_head = "START_REPLACE_";
$end_replace_head = "END_REPLACE_";
$replace_tail = "\n";

$filename = $ARGV[0] . "/" . $ARGV[1];

$comment = "perl -i -ne 'print unless m/^\\/\\*\\ \\\$Id\\:/' $filename";
system($comment);
$comment = "perl -i -ne 'print if /\\S/' $filename";
system($comment);

open add_file, "<$filename" or die "cannot open $filename";
@add_read = <add_file>;
close(add_file);

$add_end_index = @add_read;

for ($index = 0; $index < $add_end_index; $index++)
{
    if ($add_read[$index] =~ /COMMAND/)
    {
        $temp = quotemeta($add_read[$index]);
        if ($index == 0)
        {
            $replace = $start_replace_head . $replace_id . $replace_tail;
            $add_read[$index] =~ s/$temp/$replace$add_read[$index]/s;
        }
        else
        {
            $replace = $end_replace_head . $replace_id . $replace_tail;
            $replace_id++;
            $replace = $replace . $start_replace_head . $replace_id . $replace_tail;
            $add_read[$index] =~ s/$temp/$replace$add_read[$index]/s;
        }
    }
    elsif ($index == $add_end_index - 1)
    {
        $temp = quotemeta($add_read[$index]);
        $replace = $end_replace_head . $replace_id . $replace_tail;
        $add_read[$index] =~ s/$temp/$add_read[$index]$replace/s;
        last;
    }
}

$filename = $ARGV[0] . "/add.out";

open add_out, ">$filename" or die "cannot open $filename";
print add_out @add_read;
close(add_out);
