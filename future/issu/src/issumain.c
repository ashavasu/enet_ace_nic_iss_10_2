/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: issumain.c,v 1.5 2016/08/11 09:42:01 siva Exp $
*
* Description: ISSU (In service software upgrade) main functions.
*********************************************************************/
#ifndef __ISSUMAIN_C__
#define __ISSUMAIN_C__

#include "issuinc.h"

/******************************************************************************
 * Function           : IssuModuleInit
 * Input(s)           : Input arguments.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Issu Task main routine to initialize ISSU task
 *                      parameters, main loop processing.
 ******************************************************************************/

VOID
IssuModuleInit (VOID)
{
    if (IssuMainTaskInit () == ISSU_FAILURE)
    {
        ISSU_TRC (ISSU_INIT_SHUT_TRC | ISSU_CONTROL_PATH_TRC |ISSU_CRITICAL_TRC |
          ISSU_ALL_FAILURE_TRC,"IssuMainTask failed to initialize\r\n");

        /* Deinitialize ISSU task */
        IssuMainTaskDeInit ();

        lrInitComplete (OSIX_FAILURE);

        return;
    }

    /* Register ISSU Mib with SNMP Agent */
    RegisterFSISSU ();

    lrInitComplete (OSIX_SUCCESS);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IssuMainTaskInit                                 */
/*                                                                           */
/*    Description         : This function will perform following task in ISSU*/
/*                          Module:                                          */
/*                          o  Initializes global variables                  */
/*                          o  Create semaphore for ISSU                     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : ISSU_SUCCESS - When this function successfully   */
/*                                         allocated Issu Module resource.   */
/*                          ISSU_FAILURE - When this function failed to      */
/*                                         allocate Issu Module resources.   */
/*****************************************************************************/
INT4
IssuMainTaskInit (VOID)
{

    ISSU_TRC (ISSU_INIT_SHUT_TRC |ISSU_CONTROL_PATH_TRC,
            "Initializing ISSU.....\n");

    MEMSET (&gIssuSystemInfo, 0, sizeof (tIssuSystemInfo));
    MEMSET (&gIssuSystemStatusInfo, 0,sizeof (tIssuSystemStatusInfo));

#ifdef NPAPI_WANTED
    /* Remove the Init script in File to avoid auto reload */
    IssuDeleteInitScript ();
#endif

    /* When the image comes up after completion of Load version update the
     * global values from Node Id file */
    if (IssGetIssuStartupMode() == ISSU_MAINTENANCE_MODE_ENABLE)
    {
        ISSU_TRC (ISSU_INIT_SHUT_TRC |ISSU_MGMT_TRC, 
                "Initializing ISSU after completion of Load version\n");

        /* If new Image comes in ISSU MODE, which means load version  
         * upgrade has occured set the command status */
        gIssuSystemInfo.u4IssuMode = (UINT4) IssGetIssuMode ();
        gIssuSystemInfo.u1IssuMaintenanceMode = ISSU_MAINTENANCE_MODE_ENABLE;
        gIssuSystemInfo.u4IssuCommand = ISSU_CMD_LOAD_VERSION;
        gIssuSystemStatusInfo.u4IssuCommandStatus = ISSU_CMD_IN_PROGRESS;
        gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = 
            ISSU_MAINTENANCE_OPER_ENABLE;
        gIssuSystemStatusInfo.u4IssuProcedureStatus = ISSU_PROCEDURE_IN_PROGRESS;
        MEMCPY (gIssuSystemStatusInfo.au1IssuRollbackSWPath,
                IssGetRollBackSwPath (), sizeof (gIssuSystemStatusInfo.au1IssuRollbackSWPath)-1);
        MEMCPY (gIssuSystemStatusInfo.au1IssuCurrentSWPath,
                IssGetCurrentSwPath (), sizeof (gIssuSystemStatusInfo.au1IssuCurrentSWPath)-1);
        MEMCPY (gIssuSystemInfo.au1IssuLoadSWPath,
                IssGetCurrentSwPath (), sizeof (gIssuSystemInfo.au1IssuLoadSWPath)-1);
    }
    else
    {
        /*Initialize the global variables*/
        gIssuSystemInfo.u4IssuMode = ISSU_FULL_COMPATIBLE_MODE;
        gIssuSystemInfo.u1IssuMaintenanceMode = ISSU_MAINTENANCE_MODE_DISABLE;
        gIssuSystemStatusInfo.u4IssuCommandStatus = ISSU_CMD_NOT_STARTED;
        gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = 
            ISSU_MAINTENANCE_OPER_DISABLE;
        gIssuSystemStatusInfo.u4IssuProcedureStatus = ISSU_PROCEDURE_NOT_INITIATED;
    }

    gIssuSystemInfo.u1IssuTrapStatus = ISSU_TRAP_ENABLE;
    gIssuSystemInfo.u4IssuTraceOption = ISSU_DEFAULT_TRACE; 
    gIssuSystemStatusInfo.u4LastUpgradeTime = IssGetIssuLastUpgradeTime ();
    MEMCPY (gIssuSystemStatusInfo.au1IssuRollbackSoftwareVersion, 
            IssGetRollBackIssVersion (), sizeof (gIssuSystemStatusInfo.au1IssuRollbackSoftwareVersion)-1);
    
    /* Create a Semphore for Protocol Lock */

    if (ISSU_CREATE_SEMAPHORE (ISSU_PROTO_SEM, ISSU_BINARY_SEM, ISSU_SEM_FLAGS,
                               &(gIssuSystemInfo.IssuProtocolSemId)) !=
        OSIX_SUCCESS)
    {

        ISSU_TRC (ISSU_CRITICAL_TRC | ISSU_ALL_FAILURE_TRC,
                "Issu Task Failed : Semaphore Creation Failed \n");
        return ISSU_FAILURE;
    }


    ISSU_TRC (ISSU_INIT_SHUT_TRC |ISSU_CONTROL_PATH_TRC, 
            "Initialized ISSU successfully.....\n");

    return ISSU_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IssuMainTaskDeInit                               */
/*                                                                           */
/*    Description         : This function will perform deinitialization of   */
/*                          ISSU Task                                        */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
IssuMainTaskDeInit (VOID)
{
    ISSU_TRC (ISSU_INIT_SHUT_TRC |ISSU_CONTROL_PATH_TRC,
            "De-Initializing ISSU.....\n");

    if (gIssuSystemInfo.IssuProtocolSemId != 0)
    {
        ISSU_DELETE_SEMAPHORE (gIssuSystemInfo.IssuProtocolSemId);
    }

    MEMSET (&gIssuSystemInfo, 0, sizeof (tIssuSystemInfo));
    MEMSET (&gIssuSystemStatusInfo, 0,sizeof (tIssuSystemStatusInfo));

    ISSU_TRC (ISSU_INIT_SHUT_TRC | ISSU_CONTROL_PATH_TRC, 
            "De-Initialized ISSU successfully.....\n");

    return;
}

#endif
