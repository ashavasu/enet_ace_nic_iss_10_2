/********************************************************************
 * * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * *
 * * $Id: issuapi.c,v 1.14 2016/07/30 10:41:02 siva Exp $
 * *
 * * Description: ISSU APIs to interface with applications.
 * *********************************************************************/
#ifndef __ISSUAPI_C__
#define __ISSUAPI_C__

#include "issuinc.h"

/*****************************************************************************/
/* Function Name      : IssuGetMaintModeOperation                            */
/*                                                                           */
/* Description        : This function is called to decide whether            */
/*                      miantenance mode operation is to be performed or not.*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FALSE                              */
/*****************************************************************************/
UINT4
IssuGetMaintModeOperation (VOID)
{
    UINT1             u1IssuMaintenanceMode = 0;
    UINT4             u4IssuMode = 0; 


    u1IssuMaintenanceMode = gIssuSystemInfo.u1IssuMaintenanceMode;
    u4IssuMode = gIssuSystemInfo.u4IssuMode;

    if ((u1IssuMaintenanceMode == ISSU_MAINTENANCE_MODE_ENABLE) &&
       (u4IssuMode == ISSU_FULL_COMPATIBLE_MODE))
    {
        return OSIX_TRUE;
    }

    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : IssuGetIssuMode                                      */
/*                                                                           */
/* Description        : This function is called to get ISSU mode from        */
/*                      external modules.                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : IssuMode.                                            */
/*****************************************************************************/
UINT4
IssuGetIssuMode (VOID)
{
    UINT4             u4FsIssuMode = 0;


    u4FsIssuMode = gIssuSystemInfo.u4IssuMode;    


    return u4FsIssuMode;
}

/*****************************************************************************/
/* Function Name      : IssuGetMaintenanceMode                               */
/*                                                                           */
/* Description        : This function is an utility to check whether         */
/*                      maintenance mode is set or not                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : i4FsIssuMaintenanceMode                              */
/*****************************************************************************/
INT4
IssuGetMaintenanceMode (VOID)
{
    INT4         i4FsIssuMaintenanceMode = 0;


    i4FsIssuMaintenanceMode = gIssuSystemInfo.u1IssuMaintenanceMode;


    return i4FsIssuMaintenanceMode;
}

/*****************************************************************************/
/* Function Name      : IssuSetOperMaintenanceMode                           */
/*                                                                           */
/* Description        : This function is called from ISSU or external        */
/*                      modules to set Maintenance Oper status to same as    */
/*                      maintenance admin status.                            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssuSetOperMaintenanceMode (UINT1 u1OperStatus)
{
    tIssuTrapInfo             IssuTrapInfo;

    MEMSET(&IssuTrapInfo, 0, sizeof (tIssuTrapInfo));

    ISSU_TRC_ARG1 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
            "Setting the Maintenance Oper Status as %d !!\r\n",u1OperStatus);

	/* Set the Oper Status for the maintenance mode */
    gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = u1OperStatus;

	/* Send MM Status Trap */
    IssuTrapInfo.u1IssuMaintenanceMode = gIssuSystemInfo.u1IssuMaintenanceMode;
    IssuTrapInfo.u1IssuMaintenanceOperStatus = u1OperStatus;
    IssuSnmpifSendTrap(ISSU_TRAP_MAINTENANCE_STATUS,&IssuTrapInfo);
 

    return;
}

/*****************************************************************************/
/* Function Name      : IssuIsOidAllowed                                     */
/*                                                                           */
/* Description        : This function is called from SNMP module to          */
/*                      check whether the given OID is allowed when          */
/*                      maintenance mode is enabled                          */
/* Input(s)           : MIB OID                                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS / ISSU_FAILURE                          */
/*****************************************************************************/
INT4
IssuIsOidAllowed (tSNMP_OID_TYPE OID)
{
    UINT4               u4RetVal = 0;
    INT2                i2Index = 0;
    UINT4               u4Count = 0;
    tSNMP_OID_TYPE      FilterString;

    MEMSET (&FilterString, 0, sizeof(tSNMP_OID_TYPE));
	
    /* If bulk update is in-progress, do not restrict any oid */
    if(RmIsBulkUpdateComplete() == RM_FAILURE)
    {
         return ISSU_SUCCESS;
    } 

    /* Scan through the allowed OID list and compare */
    while (gIssuAllowedOidList[i2Index].u4Length != 0)
    {
        FilterString.pu4_OidList = gIssuAllowedOidList[i2Index].u4OidString;
        FilterString.u4_Length = gIssuAllowedOidList[i2Index].u4Length;

        if (FilterString.u4_Length <= OID.u4_Length)
        {
            /* Check if part of a string matches the OID */
            u4RetVal = OIDCompare(FilterString,OID,&u4Count);

            /* To check the given OID is compared fully with 
             * allowed OID */
            if(gIssuAllowedOidList[i2Index].u4Length == u4Count)
            {
                if(u4RetVal == SNMP_EQUAL)
                {
                    /* Given OID exactly matches the allowed OID */
                    return ISSU_SUCCESS;
                } 
                else if (u4RetVal == SNMP_LESSER)
                {
                    /* Given OID is a subset of allowed OID group */
                    if(FilterString.pu4_OidList[u4Count - 1] == 
                           OID.pu4_OidList[u4Count - 1])
                    {
                        return ISSU_SUCCESS;
                    }
                }
            }
        }
        i2Index ++;
    }

    ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CRITICAL_TRC,
              "Comparison with allowed OID's Failed !!\r\n");


    return ISSU_FAILURE;

}
/*****************************************************************************/
/* Function Name      : IssuIsOidAllowedInStandby                            */
/*                                                                           */
/* Description        : This function is called from SNMP module to          */
/*                      check whether the given OID is allowed for snmpset   */
/*                      in standby node                                      */
/* Input(s)           : MIB OID                                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS / ISSU_FAILURE                          */
/*****************************************************************************/
INT4
IssuIsOidAllowedInStandby (tSNMP_OID_TYPE OID)
{
    UINT4               u4RetVal = 0;
    INT2                i2Index = 0;
    UINT4               u4Count = 0;
    tSNMP_OID_TYPE      FilterString;

    MEMSET (&FilterString, 0, sizeof(tSNMP_OID_TYPE));

    /* Scan through the allowed OID list and compare */
    while (gIssuStbyAllowedOidList[i2Index].u4Length != 0)
    {
        FilterString.pu4_OidList = gIssuStbyAllowedOidList[i2Index].u4OidString;
        FilterString.u4_Length = gIssuStbyAllowedOidList[i2Index].u4Length;

        if (FilterString.u4_Length <= OID.u4_Length)
        {
             /* Check if part of a string matches the OID */
            u4RetVal = OIDCompare(FilterString,OID,&u4Count);

            /* To check the given OID is compared fully with 
             * allowed OID */
            if(gIssuStbyAllowedOidList[i2Index].u4Length == u4Count)
            {
                if(u4RetVal == SNMP_EQUAL)
                {
                    /* Given OID exactly matches the allowed OID */
                    return ISSU_SUCCESS;
                } 
                else if (u4RetVal == SNMP_LESSER)
                {
                    /* Given OID is a subset of allowed OID group */
                    if(FilterString.pu4_OidList[u4Count - 1] == 
                           OID.pu4_OidList[u4Count - 1])
                    {
                        return ISSU_SUCCESS;
                    }
                }
            }
        }
        i2Index ++;
    }

    ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CRITICAL_TRC,
              "Comparison with allowed OID's for Standby Failed !!\r\n");


    return ISSU_FAILURE;

}
#ifdef CLI_WANTED
/*****************************************************************************/
/* Function Name      : IssuIsCliCmdAllowed                                  */
/*                                                                           */
/* Description        : This function is called from CLI module to           */
/*                      check whether the given CLI command is allowed when  */
/*                      maintenance mode is enabled                          */
/* Input(s)           : Cli command string.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS if command is allowed,                  */
/*                      ISSU_FAILURE if command is not allowed.              */
/*****************************************************************************/
INT4
IssuIsCliCmdAllowed (INT1 *pi1Command)
{
    INT2                 i2Index = 0;
    
    if(pi1Command == NULL)
    {
        return ISSU_SUCCESS;
    }
    /* Discard the spaces */
    if (isspace (*pi1Command))
    {
        while(isspace(*(++pi1Command)));
    }

    while (gIssuAllowedCmdList[i2Index] != NULL)
    {
        if (CliCommandMatchSubset ((CONST UINT1 *) gIssuAllowedCmdList[i2Index],
                    (UINT1 *) pi1Command) == CLI_SUCCESS)
        {
            return ISSU_SUCCESS;
        }
        else if (CliCompareExactSubset
                ((INT1 *) pi1Command ,
                (CONST INT1 *) gIssuAllowedCmdList[i2Index])
                 == TRUE)
        {
            /* Check if part of a string matches the command */
            return ISSU_SUCCESS;
        }
        i2Index++;
    }

    ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CRITICAL_TRC,
            "Comparison with allowed CLI commands Failed !!\r\n");

    return ISSU_FAILURE;
}
/*****************************************************************************/
/* Function Name      : IssuIsCliCmdRestricted                               */
/*                                                                           */
/* Description        : This function is called from CLI module to           */
/*                      check whether the given CLI command is restricted    */
/*                      when maintenance mode is enabled. The command must   */
/*                      be of priority one or less than one                  */
/* Input(s)           : Cli command string.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS if command is allowed,                  */
/*                      ISSU_FAILURE if command is not allowed.              */
/*****************************************************************************/
INT4
IssuIsCliCmdRestricted (INT1 *pi1Command)
{
    INT2                 i2Index = 0;
    
    if(pi1Command == NULL)
    {
        return ISSU_SUCCESS;
    }
    /* Discard the spaces */
    if (isspace (*pi1Command))
    {
        while(isspace(*(++pi1Command)));
    }

    while (gIssuRestrictedCmdList[i2Index] != NULL)
    {
        if (CliCommandMatchSubset ((CONST UINT1 *) gIssuRestrictedCmdList[i2Index],
                    (UINT1 *) pi1Command) == CLI_SUCCESS)
        {
             ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CRITICAL_TRC,
            "Cli command restricted in ISSU mode !!\r\n");
            return ISSU_FAILURE;
        }
        else if (CliCompareExactSubset
                ((INT1 *) pi1Command ,
                (CONST INT1 *) gIssuRestrictedCmdList[i2Index])
                 == TRUE)
        {
            /* Check if part of a string matches the command */
            ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CRITICAL_TRC,
            "Cli command restricted in ISSU mode !!\r\n");
            return ISSU_FAILURE;
        }
        i2Index++;
    }
    return ISSU_SUCCESS;
}
#endif /* CLI_WANTED */
/*****************************************************************************/
/* Function Name      : IssuUpdateProcedureStatus                            */
/*                                                                           */
/* Description        : This function is called from ISSU or external        */
/*                      modules to set ISSU procedure status		         */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID 
IssuUpdateProcedureStatus (VOID)
{

    /* Upon software upgrade load version command status will be set as 
     * in-progress 
     * After Bulk sync up completion set the command status of load version.
     * This check is to prevent the load version command trap during 
     * issual of redundancy force switchover */
	if (!((gIssuSystemInfo.u4IssuCommand == ISSU_CMD_LOAD_VERSION) && 
			(gIssuSystemStatusInfo.u4IssuCommandStatus == ISSU_CMD_IN_PROGRESS)))
    {
		return;
    }

    IssuSetCommandStatus (ISSU_CMD_LOAD_VERSION, ISSU_CMD_SUCCESSFUL);


    return;
}

/*****************************************************************************/
/* Function Name      : IssuIsHwProgrammingAllowed                           */
/*                                                                           */
/* Description        : This function return whether NP Programming is       */
/*                      allowed depending on the ISSU status                 */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FALSE                              */
/*****************************************************************************/
UINT4
IssuIsHwProgrammingAllowed (VOID)
{
    UINT1             u1OperStatus = 0;
    UINT4             u4IssuMode = 0;


    u1OperStatus = gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus;
    u4IssuMode = gIssuSystemInfo.u4IssuMode;

    if ((u1OperStatus == ISSU_MAINTENANCE_OPER_ENABLE) &&
       (u4IssuMode == ISSU_FULL_COMPATIBLE_MODE))
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}
/*****************************************************************************/
/* Function Name      : IssuApiForceStandbyCompleted                         */
/*                                                                           */
/* Description        : This function is called from RM module to update     */
/*                      the successful completion of force standby.          */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
IssuApiForceStandbyCompleted(VOID)
{
 
     IssuSetCommandStatus(ISSU_CMD_FORCE_STANDBY, ISSU_CMD_SUCCESSFUL);
     
     return;
}

/*****************************************************************************/
/* Function Name      : IssuApiIsLoadVersionTriggered                         */
/*                                                                           */
/* Description        : This function is called from HB module to check      */
/*                      whether loadversion triggred or not                  */
/*                                                                           */ 
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : gu1IsLoadVersionTriggered-OSIX_TRUE/OSIX_FALSE       */
/*****************************************************************************/
UINT1 IssuApiIsLoadVersionTriggered(VOID)
{
    return gu1IsLoadVersionTriggered;
}


/*****************************************************************************/
/* Function Name      : IssuApiPerformLoadVersionNPCmds                      */
/*                                                                           */
/* Description        : This function is called from MSR module              */
/*                      to program the BCM for graceful shutdown             */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_FAILURE/ISSU_SUCCESS                            */
/*****************************************************************************/
INT4
IssuApiPerformLoadVersionNPCmds (VOID)
{
#ifdef NPAPI_WANTED
    tIssuHwStatusInfo       IssuHwStatusInfo;
    
    MEMSET(&IssuHwStatusInfo, 0, sizeof(tIssuHwStatusInfo));

    IssuHwStatusInfo.u4IssuMode = gIssuSystemInfo.u4IssuMode;
    IssuHwStatusInfo.u4Opcode = ISSU_LOAD_SOFTWARE;
 
    /* Set the Global flag as Loadversion triggered */
    IssuSetLoadversionTriggered(OSIX_TRUE);
    /*Stop the HB timers to avoid sendin/receive HB packets */
    if (HbApiIssuHandleTimer (OSIX_FALSE) != HB_SUCCESS)

    {
        ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |
                ISSU_CRITICAL_TRC,"ISSU HbApiIssuStopTimer Faild !\r\n");
    }
    /* NPAPI call for Load version */
    if (FsNpIssuLoadVersionUpgrade (&IssuHwStatusInfo) != FNP_SUCCESS)
    {
        /* Reset the Flag as not triggerd */
        IssuSetLoadversionTriggered(OSIX_FALSE);
        /* Restart the Timers to continue the HB process*/
        if( HbApiIssuHandleTimer(OSIX_TRUE) != HB_SUCCESS )
        {
            ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |
                    ISSU_CRITICAL_TRC,"ISSU HbApiIssuStartTimer Faild !\r\n");
        }
        /* Remove the Init script in File to avoid Issu auto reload */
        IssuDeleteInitScript ();
        IssuSetCommandStatus(ISSU_CMD_LOAD_VERSION, ISSU_CMD_FAILED);
        ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |ISSU_CRITICAL_TRC,
                "FsNpIssuLoadVersionUpgrade Failed\r\n");
        return ISSU_FAILURE;
    }
#endif /*NPAPI_WANTED */
    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuApiRestoreCacheFile                              */
/*                                                                           */
/* Description        : This function is called from RM module to restore    */
/*                      the the hardware handles stored in the file before   */
/*                      booting ISS in warm-boot  mode                       */
/*                      procedure                                            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssuApiRestoreCacheFile(VOID)
{
#ifdef NPAPI_WANTED
    tIssuHwStatusInfo   IssuHwStatusInfo;

    MEMSET(&IssuHwStatusInfo, 0, sizeof (tIssuHwStatusInfo));

    IssuHwStatusInfo.u4Opcode = ISSU_RESTORE_NP_SHADOW_TABLE;

    if (FsNpIssuLoadVersionUpgrade (&IssuHwStatusInfo) != FNP_SUCCESS)
    {
        IssuSetCommandStatus(ISSU_CMD_LOAD_VERSION, ISSU_CMD_FAILED);
        ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |ISSU_CRITICAL_TRC,
                "IssuApiRestoreCacheFile Failed!\r\n");
        return ;
    }
#else
    return ;
#endif
}

#endif

