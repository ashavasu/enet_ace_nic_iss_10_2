/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissuwr.c,v 1.2 2015/11/20 10:40:23 siva Exp $
*
* Description: ISSU module Wrapper Routines
*********************************************************************/

#ifndef __FSISSUWR_C
#define __FSISSUWR_C

#include  "issuinc.h"
#include "fsissudb.h"

VOID
RegisterFSISSU ()
{
    SNMPRegisterMibWithLock (&fsissuOID, &fsissuEntry, IssuLock, IssuUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsissuOID, (const UINT1 *) "fsissu");
}

VOID
UnRegisterFSISSU ()
{
    SNMPUnRegisterMib (&fsissuOID, &fsissuEntry);
    SNMPDelSysorEntry (&fsissuOID, (const UINT1 *) "fsissu");
}

INT4
FsIssuMaintenanceModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuMaintenanceMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuMaintenanceOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuMaintenanceOperStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuLoadSWPathGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuLoadSWPath (pMultiData->pOctetStrValue));
}

INT4
FsIssuRollbackSWPathGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuRollbackSWPath (pMultiData->pOctetStrValue));
}

INT4
FsIssuCurrentSWPathGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuCurrentSWPath (pMultiData->pOctetStrValue));
}

INT4
FsIssuSoftwareCompatFilePathGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuSoftwareCompatFilePath (pMultiData->pOctetStrValue));
}

INT4
FsIssuSoftwareCompatCheckInitGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuSoftwareCompatCheckInit (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuSoftwareCompatCheckStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuSoftwareCompatCheckStatus
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuCommandGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuCommand (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuCommandStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuCommandStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuProcedureStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuProcedureStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuRollbackSoftwareVersionGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuRollbackSoftwareVersion (pMultiData->pOctetStrValue));
}

INT4
FsIssuTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuTraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuTrapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuTrapStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsIssuLastUpgradeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIssuLastUpgradeTime (&(pMultiData->u4_ULongValue)));
}

INT4 FsIssuSoftwareCompatForVersionGet(tSnmpIndex * pMultiIndex, 
									tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsIssuSoftwareCompatForVersion(pMultiData->pOctetStrValue));
}

INT4
FsIssuMaintenanceModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuMaintenanceMode (pMultiData->i4_SLongValue));
}

INT4
FsIssuLoadSWPathSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuLoadSWPath (pMultiData->pOctetStrValue));
}

INT4
FsIssuSoftwareCompatFilePathSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuSoftwareCompatFilePath (pMultiData->pOctetStrValue));
}

INT4
FsIssuSoftwareCompatCheckInitSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuSoftwareCompatCheckInit (pMultiData->i4_SLongValue));
}

INT4
FsIssuModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuMode (pMultiData->i4_SLongValue));
}

INT4
FsIssuCommandSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuCommand (pMultiData->i4_SLongValue));
}

INT4
FsIssuTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuTraceOption (pMultiData->i4_SLongValue));
}

INT4
FsIssuTrapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIssuTrapStatus (pMultiData->i4_SLongValue));
}

INT4 FsIssuSoftwareCompatForVersionSet(tSnmpIndex * pMultiIndex,
							tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhSetFsIssuSoftwareCompatForVersion(pMultiData->pOctetStrValue));
}

INT4
FsIssuMaintenanceModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuMaintenanceMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIssuLoadSWPathTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuLoadSWPath (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsIssuSoftwareCompatFilePathTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuSoftwareCompatFilePath
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsIssuSoftwareCompatCheckInitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuSoftwareCompatCheckInit
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIssuModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIssuCommandTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuCommand (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIssuTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuTraceOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIssuTrapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIssuTrapStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4 FsIssuSoftwareCompatForVersionTest(UINT4 *pu4Error ,
					tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhTestv2FsIssuSoftwareCompatForVersion(pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsIssuMaintenanceModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuMaintenanceMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIssuLoadSWPathDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuLoadSWPath (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIssuSoftwareCompatFilePathDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuSoftwareCompatFilePath
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIssuSoftwareCompatCheckInitDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuSoftwareCompatCheckInit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIssuModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuMode (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIssuCommandDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuCommand (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIssuTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuTraceOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIssuTrapStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIssuTrapStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsIssuSoftwareCompatForVersionDep(UINT4 *pu4Error,
					tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsIssuSoftwareCompatForVersion(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#endif /* end of FSISSUWR_C*/
