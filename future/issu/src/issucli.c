/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: issucli.c,v 1.10 2016/08/09 09:41:27 siva Exp $
*
* Description: Action routines of ISSU module specific CLI commands
*********************************************************************/

#ifndef ISSUCLI_C
#define ISSUCLI_C

#include "issuinc.h"

/*****************************************************************************/
/*                                                                           */
/* FUNCTION NAME    : cli_process_issu_cmd                                   */
/*                                                                           */
/* DESCRIPTION      : This function takes in variable no. of arguments       */
/*                    and process the commands for the ISSU module.          */
/*                    defined in issucli.h                                   */
/*                                                                           */
/* INPUT            : CliHandle -  CLIHandler                                */
/*                    u4Command -  Command Identifier                        */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : NONE                                                   */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_issu_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS];
    UINT1               u1Argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_SUCCESS;

    va_start (ap, u4Command);

    while (u1Argno < CLI_MAX_ARGS)
    {
        args[u1Argno++] = va_arg (ap, UINT1 *);
    }
    CLI_SET_ERR (0);
    va_end (ap);

    CliRegisterLock (CliHandle, IssuLock, IssuUnLock);

    /* Take the ISSU Module Lock */
    ISSU_LOCK();

    switch (u4Command)
    {
		case CLI_ISSU_LOAD_VERSION:
			if (args[0] != NULL)
			{
				i4RetStatus =
					IssuCliSetIssuCommand (CliHandle,ISSU_CMD_LOAD_VERSION,
							(UINT1 *) args[0]);
			}
			else
			{
				i4RetStatus =
					IssuCliSetIssuCommand (CliHandle,ISSU_CMD_LOAD_VERSION, NULL);
			}
			break;

        case CLI_ISSU_FORCE_STANDBY:
            i4RetStatus =
                IssuCliSetIssuCommand (CliHandle, ISSU_CMD_FORCE_STANDBY, NULL);
            break;

        case CLI_ISSU_COMPATIBILITY_MATRIX_PATH:
            i4RetStatus = IssuCliSetSoftwareCompatFilePath (CliHandle,
                                                            (UINT1 *) args[0]);
            break;

        case CLI_ISSU_COMPATIBILITY_CHECK_INIT:
            i4RetStatus = IssuCliSetSoftwareCompatCheckInit (CliHandle,
															(UINT1 *) args[0]);
            break;

        case CLI_ISSU_MODE:
            i4RetStatus =
                IssuCliSetIssuMode (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISSU_MAINTENANCE_MODE_ENABLE:
            i4RetStatus = IssuCliSetMaintenanceMode (CliHandle,
                                                     ISSU_MAINTENANCE_MODE_ENABLE);
            break;

        case CLI_ISSU_MAINTENANCE_MODE_DISABLE:
            i4RetStatus = IssuCliSetMaintenanceMode (CliHandle,
                                                     ISSU_MAINTENANCE_MODE_DISABLE);
            break;

        case CLI_TRAP_STATUS:
            i4RetStatus =
                IssuCliSetTrapStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

			/* SHOW COMMANDS */
        case CLI_SHOW_ISSU_STATE:
            i4RetStatus = IssuCliShowIssuState (CliHandle);
            break;

        case CLI_SHOW_ISSU_STATE_DETAIL:
            i4RetStatus = IssuCliShowIssuStateDetail (CliHandle);
            break;

        case CLI_SHOW_ISSU_COMP_MATRIX:
            i4RetStatus = IssuCliShowIssuCompatibilityMatrixFile (CliHandle);
            break;

        case CLI_SHOW_ISSU_COMP_MATRIX_STATUS:
            i4RetStatus =
                IssuCliShowIssuCompatMatrixStatus (CliHandle);
            break;

        case CLI_SHOW_ISSU_MAINTENANCE_MODE:
			IssuCliShowIssuMaintenanceMode (CliHandle);
            break;

        case CLI_SHOW_ISSU_MAINTENANCE_MODE_COMMANDS:
            IssuCliShowIssuMaintenanceModeCommands (CliHandle);
            break;
			
			/* DEBUG COMMAND */
        case CLI_ISSU_DEBUG:
            i4RetStatus =
                IssuCliSetTraceOption (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                       CLI_ENABLE);
            break;

        case CLI_ISSU_NO_DEBUG:
            i4RetStatus =
                IssuCliSetTraceOption (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                       CLI_DISABLE);
            break;

        default:
            i4RetStatus = CLI_FAILURE;
            break;
    }
    
    /* Display the error string */
    if ((i4RetStatus == CLI_FAILURE)
            && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ISSU_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", IssuCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    /* Release the Issu Module Lock */
    ISSU_UNLOCK();
    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliSetIssuCommand                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to set which ISSU command    */
/*                        is to be triggered                                 */
/*                                                                           */
/*     INPUT            : u4IssuCommand - load version or force active or    */
/*                         force standby                                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliSetIssuCommand (tCliHandle CliHandle, UINT4 u4IssuCommand,
                UINT1 *pu1LoadSwPath)
{
    UINT4                       u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE     LoadSwPath;

    MEMSET (&LoadSwPath, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhTestv2FsIssuCommand (&u4ErrorCode, (INT4) u4IssuCommand)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (pu1LoadSwPath != NULL)
    {
        LoadSwPath.i4_Length = (INT4) STRLEN (pu1LoadSwPath);
        LoadSwPath.pu1_OctetList = pu1LoadSwPath;

        if (nmhTestv2FsIssuLoadSWPath (&u4ErrorCode , &LoadSwPath) 
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                    "\r%% Invalid Load Software Path or file\r\n");
            return CLI_FAILURE;
        }
		/* Set the Load version Software path incase user has specified */
        if (nmhSetFsIssuLoadSWPath (&LoadSwPath) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                    "\r%% Failed to set Load Software Path\r\n");
            return CLI_FAILURE;
        }
    }
	if (nmhSetFsIssuCommand ((INT4) u4IssuCommand) != SNMP_SUCCESS)
	{
		return CLI_FAILURE;
	}

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliSetSoftwareCompatFilePath                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the compatibility               */
/*                        matrix file path                                   */
/*                                                                           */
/*     INPUT            : pMatrixPath - Path of compatibility matrix         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliSetSoftwareCompatFilePath (tCliHandle CliHandle,
                                  UINT1 *pu1MatrixFilePath)
{
    UINT4                           u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE         MatrixPath;

	UNUSED_PARAM (CliHandle);

    MEMSET (&MatrixPath, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MatrixPath.i4_Length = (INT4) STRLEN (pu1MatrixFilePath);
    MatrixPath.pu1_OctetList = pu1MatrixFilePath;

    if (nmhTestv2FsIssuSoftwareCompatFilePath (&u4ErrorCode, &MatrixPath)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIssuSoftwareCompatFilePath (&MatrixPath) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliSetSoftwareCompatCheckInit                  */
/*                                                                           */
/*     DESCRIPTION      : This function fetch the current ISS version and    */
/*                        verify the compatability by checking compatibility */
/*                        file                                               */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IssuCliSetSoftwareCompatCheckInit (tCliHandle CliHandle,
									UINT1 *pu1SwVersion)
{
    UINT4               			u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE         SwVersion;

    UNUSED_PARAM (CliHandle);

    MEMSET (&SwVersion, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    SwVersion.i4_Length = (INT4) STRLEN (pu1SwVersion);
    SwVersion.pu1_OctetList = pu1SwVersion;

    if (nmhSetFsIssuSoftwareCompatForVersion(&SwVersion) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIssuSoftwareCompatCheckInit (&u4ErrorCode,
                                                ISSU_COMP_CHECK_ENABLE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIssuSoftwareCompatCheckInit (ISSU_COMP_CHECK_ENABLE)
        == SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to initiate compatibility check\r\n");
        return CLI_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliSetIssuMode                                 */
/*                                                                           */
/*     DESCRIPTION      : This function Enables or Disables                  */
/*                        Maintenance Mode                                   */
/*                                                                           */
/*     INPUT            : i4MaintenanceMode -  Status of maintenance mode    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliSetIssuMode (tCliHandle CliHandle, INT4 i4IssuMode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIssuMode (&u4ErrorCode, i4IssuMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    
	if (nmhSetFsIssuMode (i4IssuMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set the ISSU mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliSetMaintenanceMode                          */
/*                                                                           */
/*     DESCRIPTION      : This function Enables or Disables                  */
/*                        Maintenance Mode                                   */
/*                                                                           */
/*     INPUT            : i4MaintenanceMode -  Status of maintenance mode    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliSetMaintenanceMode (tCliHandle CliHandle, INT4 i4MaintenanceMode)
{
    UINT4               u4ErrorCode = 0;

	UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsIssuMaintenanceMode (&u4ErrorCode, i4MaintenanceMode)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIssuMaintenanceMode (i4MaintenanceMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliSetTrapStatus                               */
/*                                                                           */
/*     DESCRIPTION      : This function Enables or Disables Traps            */
/*                                                                           */
/*     INPUT            : i4TrapStatus - Status of Trap notification         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliSetTrapStatus (tCliHandle CliHandle, INT4 i4TrapStatus)
{
    UINT4               u4ErrorCode = 0;

	UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsIssuTrapStatus (&u4ErrorCode, i4TrapStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIssuTrapStatus (i4TrapStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliSetTraceOption                              */
/*                                                                           */
/*     DESCRIPTION      : This function used to SET Trace Option             */
/*                                                                           */
/*     INPUT            : i4CliTraceVal                                      */
/*                            u1TraceFlag                                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliSetTraceOption (tCliHandle CliHandle, INT4 i4CliTraceVal,
                       UINT1 u1TraceFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4TraceVal = 0;

	UNUSED_PARAM (CliHandle);

    /* Get ISSU Trace Option */
    i4TraceVal = (INT4) gIssuSystemInfo.u4IssuTraceOption;

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4TraceVal |= i4CliTraceVal;
    }

    else
    {
        i4TraceVal &= (~(i4CliTraceVal));
    }

    if (nmhTestv2FsIssuTraceOption (&u4ErrorCode, i4TraceVal) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIssuTraceOption (i4TraceVal) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliShowIssuState                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays ISSU compatibility          */
/*                        matrix file                                        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliShowIssuState (tCliHandle CliHandle)
{
    INT4                	i4IssuMaintenanceOperStatus = 0;
    INT4                	i4IssuMode = 0;
    INT4                	i4IssuCommand = 0;
    UINT1      	au1CurrentSwVersion [ISSU_IMAGE_NAME_MAX_SIZE + 1];
    INT4                	i4CommandStatus = 0;
    tSNMP_OCTET_STRING_TYPE CurrentSwVersion;
                                          
    MEMSET (au1CurrentSwVersion, 0, sizeof(au1CurrentSwVersion));
    MEMSET (&CurrentSwVersion, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

    CurrentSwVersion.pu1_OctetList = au1CurrentSwVersion;
    CurrentSwVersion.i4_Length = ISSU_IMAGE_NAME_MAX_SIZE;

    nmhGetFsIssuMode (&i4IssuMode);
    IssuGetCurrentSoftwareVersion (&CurrentSwVersion);
    nmhGetFsIssuMaintenanceOperStatus (&i4IssuMaintenanceOperStatus);
    nmhGetFsIssuCommand (&i4IssuCommand);
    nmhGetFsIssuCommandStatus (&i4CommandStatus);

    if (RmGetNodeState() == RM_ACTIVE)
    {
        CliPrintf (CliHandle, "\r\nNode State                   : Active");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nNode State                   : Standby");
    }

    CliPrintf (CliHandle, "\r\nActive Software Version      : %s",
               CurrentSwVersion.pu1_OctetList);

    if (i4IssuMaintenanceOperStatus == ISSU_MAINTENANCE_OPER_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nMaintenance Mode Oper Status : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nMaintenance Mode Oper Status : Disabled");
    }

    if (i4IssuMode == ISSU_FULL_COMPATIBLE_MODE)
    {
        CliPrintf (CliHandle, "\r\nISSU Mode                    : Full-Compatible");
    }
    else if (i4IssuMode == ISSU_BASE_COMPATIBLE_MODE)
    {
        CliPrintf (CliHandle, "\r\nISSU Mode                    : Base-Compatible");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nISSU Mode                    : In-Compatible");
    }

    if (i4IssuCommand == ISSU_CMD_LOAD_VERSION)
    {
        CliPrintf (CliHandle,
                   "\r\nLast ISSU Command            : Load Version");
    }
    else if (i4IssuCommand == ISSU_CMD_FORCE_STANDBY)
    {
        CliPrintf (CliHandle,
                   "\r\nLast ISSU Command            : Force-Standby");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nLast ISSU Command            : ");
    }

    switch (i4CommandStatus)
    {
        case ISSU_CMD_NOT_STARTED:
            CliPrintf (CliHandle,
                       "\r\nLast ISSU Command-Status     : Not-Started\r\n");
            break;
        case ISSU_CMD_IN_PROGRESS:
            CliPrintf (CliHandle,
                       "\r\nLast ISSU Command-Status     : In-Progress\r\n");
            break;
        case ISSU_CMD_SUCCESSFUL:
            CliPrintf (CliHandle,
                       "\r\nLast ISSU Command-Status     : Successful\r\n");
            break;
        case ISSU_CMD_FAILED:
            CliPrintf (CliHandle,
                       "\r\nLast ISSU Command-Status     : Failed\r\n");
            break;
        default:
            break;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliShowIssuStateDetail                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays ISSU compatibility          */
/*                        matrix file                                        */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliShowIssuStateDetail (tCliHandle CliHandle)
{
    INT4                        i4NodeState = 0;
    INT4                        i4IssuMaintenanceMode = 0;
    INT4                		i4IssuMaintenanceOperStatus = 0;
    INT4                		i4TrapStatus = 0;
    INT4                        i4IssuMode = 0;
    INT4                		i4ProcedureStatus = 0;
    UINT4               		u4LastUpgradeTime = 0;
    UINT1    au1CurrentSwVersion [ISSU_IMAGE_NAME_MAX_SIZE +1];
    UINT1    au1RollbackSwVersion [ISSU_IMAGE_NAME_MAX_SIZE +1];
    UINT1    au1LoadSwPath [ISSU_PATH_MAX_SIZE +1];
    UINT1    au1RollbackSwPath [ISSU_PATH_MAX_SIZE +1];
    UINT1    au1CurrentSwPath [ISSU_PATH_MAX_SIZE +1];
    UINT1                       au1TimeStr[ISSU_STR_LEN];
    tSNMP_OCTET_STRING_TYPE     CurrentSwVersion;
    tSNMP_OCTET_STRING_TYPE     RollbackSwVersion;
    tSNMP_OCTET_STRING_TYPE     LoadSwPath;
    tSNMP_OCTET_STRING_TYPE     CurrentSwPath;
    tSNMP_OCTET_STRING_TYPE     RollbackSwPath;

    MEMSET (au1TimeStr, 0, sizeof(au1TimeStr));
    MEMSET (au1CurrentSwVersion, 0, sizeof(au1CurrentSwVersion));
    MEMSET (au1RollbackSwVersion, 0, sizeof(au1RollbackSwVersion));
    MEMSET (au1LoadSwPath, 0, sizeof(au1LoadSwPath));
    MEMSET (au1CurrentSwPath, 0, sizeof(au1CurrentSwPath));
    MEMSET (au1RollbackSwPath, 0, sizeof(au1RollbackSwPath));

    MEMSET (&CurrentSwVersion, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RollbackSwVersion, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
    MEMSET (&LoadSwPath, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
    MEMSET (&CurrentSwPath, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RollbackSwPath, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

    CurrentSwVersion.pu1_OctetList = au1CurrentSwVersion;
    CurrentSwVersion.i4_Length = ISSU_IMAGE_NAME_MAX_SIZE;

    RollbackSwVersion.pu1_OctetList = au1RollbackSwVersion;
    RollbackSwVersion.i4_Length = ISSU_IMAGE_NAME_MAX_SIZE;

    LoadSwPath.pu1_OctetList = au1LoadSwPath;
    LoadSwPath.i4_Length = ISSU_PATH_MAX_SIZE;

    CurrentSwPath.pu1_OctetList = au1CurrentSwPath;
    CurrentSwPath.i4_Length = ISSU_PATH_MAX_SIZE;

    RollbackSwPath.pu1_OctetList = au1RollbackSwPath;
    RollbackSwPath.i4_Length = ISSU_PATH_MAX_SIZE;

    i4NodeState = (INT4)RmGetNodeState ();
    nmhGetFsIssuMaintenanceMode (&i4IssuMaintenanceMode);
    nmhGetFsIssuMaintenanceOperStatus (&i4IssuMaintenanceOperStatus);
    nmhGetFsIssuTrapStatus (&i4TrapStatus);
    nmhGetFsIssuMode (&i4IssuMode);
    nmhGetFsIssuRollbackSoftwareVersion (&RollbackSwVersion);
    IssuGetCurrentSoftwareVersion (&CurrentSwVersion);
    nmhGetFsIssuProcedureStatus (&i4ProcedureStatus);
    nmhGetFsIssuLoadSWPath (&LoadSwPath);
    nmhGetFsIssuRollbackSWPath (&RollbackSwPath);
    nmhGetFsIssuCurrentSWPath (&CurrentSwPath);
    nmhGetFsIssuLastUpgradeTime (&u4LastUpgradeTime);
    IssuPrintUpgradeTime (u4LastUpgradeTime, au1TimeStr);

    if (i4NodeState == RM_ACTIVE)
    {
        CliPrintf (CliHandle, "\r\nNode State                   : Active");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nNode State                   : Standby");
    }

    if (i4IssuMaintenanceMode == ISSU_MAINTENANCE_MODE_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nMaintenance Mode Admin Status: Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nMaintenance Mode Admin Status: Disabled");
    }

    if (i4IssuMaintenanceOperStatus == ISSU_MAINTENANCE_OPER_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nMaintenance Mode Oper Status : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nMaintenance Mode Oper Status : Disabled");
    }

    if (i4TrapStatus == ISSU_TRAP_ENABLE)
    {
        CliPrintf (CliHandle, "\r\nTrap Status                  : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nTrap Status                  : Disabled");
    }

    if (i4IssuMode == ISSU_FULL_COMPATIBLE_MODE)
    {
        CliPrintf (CliHandle, "\r\nISSU Mode                    : Full-Compatible");
    }
    else if (i4IssuMode == ISSU_BASE_COMPATIBLE_MODE)
    {
        CliPrintf (CliHandle, "\r\nISSU Mode                    : Base-Compatible");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nISSU Mode                    : In-Compatible");
    }

    switch (i4ProcedureStatus)
    {
        case ISSU_PROCEDURE_NOT_INITIATED:
            CliPrintf (CliHandle,
                       "\r\nISSU Overall-Status          : Not-Initiated");
            break;
        case ISSU_PROCEDURE_IN_PROGRESS:
            CliPrintf (CliHandle,
                       "\r\nISSU Overall-Status          : In-Progress");
            break;
        case ISSU_PROCEDURE_SUCCESSFUL:
            CliPrintf (CliHandle,
                       "\r\nISSU Overall-Status          : Successful");
            break;
        case ISSU_PROCEDURE_FAILED:
            CliPrintf (CliHandle, "\r\nISSU Overall-Status          : Failed");
            break;
        default:
            break;
    }

    CliPrintf (CliHandle, "\r\nActive Software Version      : %s",
               CurrentSwVersion.pu1_OctetList);
    CliPrintf (CliHandle, "\r\nRollback Software Version    : %s",
               RollbackSwVersion.pu1_OctetList);
    CliPrintf (CliHandle, "\r\nLoad Software Path           : %s",
               LoadSwPath.pu1_OctetList);
    CliPrintf (CliHandle, "\r\nRollback Software Path       : %s",
               RollbackSwPath.pu1_OctetList);
    CliPrintf (CliHandle, "\r\nCurrent Software Path        : %s",
               CurrentSwPath.pu1_OctetList);
    if (u4LastUpgradeTime != 0)
    {
      CliPrintf (CliHandle, "\r\nLast Upgrade Time            : %s\r\n",
           au1TimeStr);
    }
    else
    {
      CliPrintf (CliHandle, "\r\nLast Upgrade Time            : \r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliShowIssuCompatibilityMatrixFile             */
/*                                                                           */
/*     DESCRIPTION      : This function displays ISSU compatibility          */
/*		                  matrix file                                     	 */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliShowIssuCompatibilityMatrixFile (tCliHandle CliHandle)
{
    FILE               *fp = NULL;
    UINT1               au1FlashFile[ISSU_FILE_NAME_LEN];
    UINT1              *pu1File;
    CHR1                au1BytesRead[FILE_BYTES_READ];

    MEMSET (au1FlashFile, 0, ISSU_FILE_NAME_LEN);
    MEMSET (au1BytesRead, 0, FILE_BYTES_READ);

    pu1File = au1FlashFile;

    SPRINTF ((CHR1 *) au1FlashFile, "%s/%s",
             gIssuSystemInfo.au1IssuSWCompatFilePath, ISSU_FILE_NAME);

    fp = fopen ((const char *) pu1File, "rb");
    if (NULL == fp)
    {
        CliPrintf (CliHandle, "\r%% Failed to open the file\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n");
    while (fgets (au1BytesRead, sizeof (au1BytesRead), fp) != NULL)
    {
        CliPrintf (CliHandle, "%s", au1BytesRead);
    }

    fclose (fp);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliShowIssuCompatMatrixStatus                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays ISSU compatibility          */
/*                        matrix status                                      */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssuCliShowIssuCompatMatrixStatus (tCliHandle CliHandle)
{
    INT4                        i4CheckStatus = 0;
    UINT1     au1CurrentSwVersion[ISSU_IMAGE_NAME_MAX_SIZE + 1];
    tSNMP_OCTET_STRING_TYPE     CurrentSwVersion;

    MEMSET (au1CurrentSwVersion, 0, sizeof (au1CurrentSwVersion));

    MEMSET (&CurrentSwVersion, 0, sizeof(tSNMP_OCTET_STRING_TYPE));
    
    CurrentSwVersion.pu1_OctetList = au1CurrentSwVersion;
    CurrentSwVersion.i4_Length = ISSU_IMAGE_NAME_MAX_SIZE;

    IssuGetCurrentSoftwareVersion (&CurrentSwVersion);
    nmhGetFsIssuSoftwareCompatCheckStatus (&i4CheckStatus);

    CliPrintf (CliHandle, "\r\nCurrent Version\r\t\t\t");
    CliPrintf (CliHandle, "Proposed Version\r\t\t\t\t\t\t");
    CliPrintf (CliHandle, "Status\r\n");
    CliPrintf (CliHandle, "\r\n---------------\r\t\t\t");
    CliPrintf (CliHandle, "----------------\r\t\t\t\t\t\t");
    CliPrintf (CliHandle, "------\r\n");

    CliPrintf (CliHandle, "%s\r\t\t\t", CurrentSwVersion.pu1_OctetList);
    CliPrintf (CliHandle, "%s\r\t\t\t\t\t\t", 
            gIssuSystemInfo.au1IssuSWCompatVersion);

    if (i4CheckStatus == ISSU_COMPATIBLE)
    {
        CliPrintf (CliHandle, "Compatible\r\n");
    }
    else if (i4CheckStatus == ISSU_NOT_COMPATIBLE)
    {
        CliPrintf (CliHandle, "Not-Compatible\r\n");
    }
    else if (i4CheckStatus == ISSU_CHECK_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "Check-In-Progress\r\n");
    }
    else if (i4CheckStatus == ISSU_COMP_FAILED)
    {
        CliPrintf (CliHandle, "Failed\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Not-Initiated\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliShowIssuMaintenanceMode                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays ISSU maintenance mode       */
/*                        Admin and Oper status                              */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssuCliShowIssuMaintenanceMode (tCliHandle CliHandle)
{
    INT4                i4IssuMaintenanceMode = 0;
    INT4                i4IssuMaintenanceOperStatus = 0;

    nmhGetFsIssuMaintenanceMode (&i4IssuMaintenanceMode);
    nmhGetFsIssuMaintenanceOperStatus (&i4IssuMaintenanceOperStatus);

    if (i4IssuMaintenanceMode == ISSU_MAINTENANCE_MODE_ENABLE)
    {
        CliPrintf (CliHandle,
                   "\r\nMaintenance Mode Admin Status    :   Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nMaintenance Mode Admin Status    :   Disabled");
    }

    if (i4IssuMaintenanceOperStatus == ISSU_MAINTENANCE_OPER_ENABLE)
    {
        CliPrintf (CliHandle,
                   "\r\nMaintenance Mode Oper Status     :   Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nMaintenance Mode Oper Status     :   Disabled\r\n");
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssuCliShowIssuMaintenanceModeCommands             */
/*                                                                           */
/*     DESCRIPTION      : This function displays the list of commands        */
/*                        allowed in ISSU maintenance mode                   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssuCliShowIssuMaintenanceModeCommands (tCliHandle CliHandle)
{
    INT2                 i2Index = 0;

    while (gIssuAllowedCmdList[i2Index] != NULL)
    {
        CliPrintf (CliHandle,"  %s\r\n",gIssuAllowedCmdList[i2Index]);
        i2Index++;
    }
    return;
}
/***************************************************************************
 *                                                                         *
 *     Function Name : IssuCliShowDebug                                    *
 *                                                                         *
 *     Description   : This function prints the debug level set for issu   *
 *                                                                         *
 *     Input(s)      : tCliHandle                                          *
                                                                           *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
                                                                           *
 ***************************************************************************/
VOID
IssuCliShowDebug (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    nmhGetFsIssuTraceOption (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "ISSU: \r\n");
    if ((i4DbgLevel & ISSU_INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, " ISSU Init-Shut trace is on\r\n");
    }
    if ((i4DbgLevel & ISSU_MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, " ISSU management trace is on\r\n");
    }
    if ((i4DbgLevel & ISSU_ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, " ISSU all failure trace is on\r\n");
    }
    if ((i4DbgLevel & ISSU_CONTROL_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, " ISSU control path trace is on\r\n");
    }
    if ((i4DbgLevel & ISSU_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, " ISSU critical trace is on\r\n");
    }

    return;
}
#endif
