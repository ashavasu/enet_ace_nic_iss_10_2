/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: issunpapi.c,v 1.1.1.1 2015/09/14 12:06:44 siva Exp $
*
* Description: All Network Processor API Function calls are done here.
*********************************************************************/
#ifndef __ISSUMAIN_C__
#define __ISSUMAIN_C__

#include "nputil.h"
#include "issunpwr.h"

/***************************************************************************
 *  
 *  Function Name       : IssuCfaNpGetLinkStatus
 * 
 *  Description         : This function using its arguments populates the
 *                        generic NP structure and invokes NpUtilHwProgram
 *                        The Generic NP wrapper after validating the H/W
 *                        parameters invokes CfaNpGetLinkStatus
 * 
 *  Input(s)            : Arguments of CfaNpGetLinkStatus
 * 
 *  Output(s)           : None
 * 
 *  Global Var Referred : None
 *  
 *  Global Var Modified : None.
 *  
 *  Use of Recursion    : None.
 * 
 *  Exceptions or Operating
 *  System Error Handling    : None.
 *
 *  Returns            : FNP_SUCCESS OR FNP_FAILURE
 * 
 ******************************************************************************/

UINT1
IssuCfaNpGetLinkStatus (UINT4 u4IfIndex)
{
    tFsHwNp                         FsHwNp;
    tCfaNpModInfo                  *pCfaNpModInfo = NULL;
    tCfaNpWrCfaNpGetLinkStatus     *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         CFA_NP_GET_LINK_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpCfaNpGetLinkStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1PortLinkStatus = CFA_IF_NP;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (CFA_IF_NP);
    }

    return pEntry->u1PortLinkStatus;
}

#endif
