/********************************************************************
 * * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * *
 * * $Id: issuutil.c,v 1.22 2016/08/11 09:42:01 siva Exp $
 * *
 * * Description: ISSU (In service software upgrade) utility functions.
 * *********************************************************************/
#ifndef __ISSUUTIL_C__
#define __ISSUUTIL_C__

#include "issuinc.h"

/*****************************************************************************/
/* Function Name      : IssuCheckCompatibility                               */
/*                                                                           */
/* Description        : This function is called to check the compatibility   */
/*                      among the software versions                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS/ISSU_FAILURE                            */
/*****************************************************************************/
INT4
IssuHandleCheckCompatibility (INT4 *pi4CompatCheckStatus,
					UINT1 *pu1CompatSwVersion)
{
    UINT1      au1FlashFile [ISSU_PATH_MAX_SIZE +1];
    UINT1      au1CurrentSwVersion [ISSU_IMAGE_NAME_MAX_SIZE +1];
    UINT1      au1CurrDir [ISSU_PATH_MAX_SIZE +1];
    INT4	   i4CheckStatus = ISSU_COMP_FAILED;
	INT4                i4RetVal = 0;

    MEMSET (au1FlashFile, 0, sizeof(au1FlashFile));
	MEMSET (au1CurrentSwVersion, 0, sizeof(au1CurrentSwVersion));
	MEMSET (au1CurrDir, 0, sizeof(au1CurrDir));

    MEMCPY (au1CurrentSwVersion, IssGetSoftwareVersion (),
            sizeof (au1CurrentSwVersion)-1);

	MEMCPY (au1FlashFile, gIssuSystemInfo.au1IssuSWCompatFilePath,
				sizeof(au1FlashFile)-1);

	if (STRNCMP (au1CurrentSwVersion, pu1CompatSwVersion,
                             ISSU_IMAGE_NAME_MAX_SIZE) == 0)
	{
		 *pi4CompatCheckStatus = ISSU_COMPATIBLE;
		 return ISSU_SUCCESS;
	}

	/* If Compatiblity matrix path is configured, check the compatiblity status 
 	 * in the configured path. In case of downgrade the compatiblity matrix in the 
 	 * configured path does not provide the correct status so get the current 
 	 * directory and verify the status in the matrix file in the current directory*/

	i4RetVal = IssuGetCompatFileCheckStatus (&i4CheckStatus, au1FlashFile);

	if (i4RetVal != ISSU_FAILURE)
	{
		if (i4CheckStatus == ISSU_COMPATIBLE)
		{
			*pi4CompatCheckStatus = ISSU_COMPATIBLE;
		}
		else
		{
			if(IssuGetCurrentWorkingDir ((CHR1 *) au1CurrDir) == ISSU_FAILURE)
			{
				ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC,
						"Failed to get current working directory for compatibilty!\r\n");
				return ISSU_FAILURE;
			}

			i4RetVal = IssuGetCompatFileCheckStatus (&i4CheckStatus, au1CurrDir);

			if (i4RetVal != ISSU_FAILURE)
			{
				if (i4CheckStatus == ISSU_COMPATIBLE)
				{
				     *pi4CompatCheckStatus = ISSU_COMPATIBLE;
                }
                else
                {
                    *pi4CompatCheckStatus = ISSU_NOT_COMPATIBLE;
                }

			}
		}
	}

	return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssuGetCompatFileCheckStatus                         */
/*                                                                           */
/* Description        : This function is called to get the compatibility     */
/*                      status of the proposed software versions             */
/*						with the current software version					 */
/*                                                                           */
/* Input(s)           : au1IssuSWCompatFilePath                              */
/*                                                                           */
/* Output(s)          : pi4CompatCheckStatus                                 */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS/ISSU_FAILURE                            */
/*****************************************************************************/
INT4
IssuGetCompatFileCheckStatus (INT4 *pi4CompatCheckStatus,
								UINT1 *pu1IssuSWCompatFilePath)
{
    FILE               *fp = NULL;
    UINT1              *pu1File = NULL;
    UINT1               au1FlashFile [ISSU_FILE_NAME_LEN];
    UINT1      au1CurrentSwVersion [ISSU_IMAGE_NAME_MAX_SIZE +1];
    UINT1      au1CompatSwVersion[ISSU_IMAGE_NAME_MAX_SIZE +1];
    CHR1                au1BytesRead [FILE_BYTES_READ];
    CHR1               *pLineToken = NULL;
	CHR1 			   *pWordToken = NULL;
    
    MEMSET (au1FlashFile, 0, sizeof(au1FlashFile));
	MEMSET (au1CurrentSwVersion, 0, sizeof(au1CurrentSwVersion));
	MEMSET (au1CompatSwVersion, 0, sizeof(au1CompatSwVersion));
    MEMSET (au1BytesRead, 0, sizeof(au1BytesRead));
    pu1File = au1FlashFile;

    MEMCPY (au1CurrentSwVersion, IssGetSoftwareVersion (),
            sizeof(au1CurrentSwVersion)-1);

	MEMCPY (au1CompatSwVersion, gIssuSystemInfo.au1IssuSWCompatVersion,
			sizeof(au1CompatSwVersion)-1);

    SPRINTF ((CHR1 *) au1FlashFile, "%s/%s",
             pu1IssuSWCompatFilePath, ISSU_FILE_NAME);

    fp = fopen ((const char *) pu1File, "rb");
    if (NULL == fp)
    {
        *pi4CompatCheckStatus = ISSU_COMP_FAILED;
        ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CRITICAL_TRC, 
                    "Failed to open the Compatibility file \n");
        return ISSU_FAILURE;
    }

    while (fgets (au1BytesRead, sizeof (au1BytesRead)-1, fp) != NULL)
    {
		pLineToken = STRTOK (au1BytesRead," -");
        if(pLineToken != NULL)
        { 
            if (STRNCMP (pLineToken, au1CurrentSwVersion,
                        ISSU_IMAGE_NAME_MAX_SIZE) == 0)
            {  
                if(STRLEN(au1BytesRead) == sizeof (au1BytesRead)-1)
		{  
                    ISSU_TRC (ISSU_CONTROL_PATH_TRC, 
                            "Compatibility versions for current versions may"
                             "exceeds the allowed list\n");
                }
                pWordToken = STRTOK (NULL," ,\n");
			while (pWordToken != NULL)
			{
                if (STRNCMP (pWordToken, au1CompatSwVersion,
                             ISSU_IMAGE_NAME_MAX_SIZE) == 0)
                {
                    *pi4CompatCheckStatus = ISSU_COMPATIBLE;
                    break;
                }
                else
                {
                    *pi4CompatCheckStatus = ISSU_NOT_COMPATIBLE;
                }
                    pWordToken = STRTOK (NULL," ,\n");
                }
            }
		}
    }
    if(*pi4CompatCheckStatus == ISSU_COMP_FAILED)
    {
	*pi4CompatCheckStatus = ISSU_COMP_FAILED;
	ISSU_TRC (ISSU_CRITICAL_TRC, 
		"Failed to get Current version in Compatibility file/"
                "Compatibility file not updated properly\n");
        fclose (fp);
	return ISSU_FAILURE;
    }
    fclose (fp);
    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuLock                                             */
/*                                                                           */
/* Description        : This function is called to Lock the semaphore for    */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS/SNMP_FAILURE                            */
/*****************************************************************************/
INT4
IssuLock (VOID)
{
    if (gIssuSystemInfo.IssuProtocolSemId != 0)
    {
        if (OsixSemTake (gIssuSystemInfo.IssuProtocolSemId) == OSIX_FAILURE)
        {
            ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC,
                    "Failed to Acquire ISSU Lock...\n");
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssuUnLock                                           */
/*                                                                           */
/* Description        : This function is called to Unlock the semaphore for  */
/*                      proper processing of multiple events at a time       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS/SNMP_FAILURE                            */
/*****************************************************************************/
INT4
IssuUnLock (VOID)
{
    if (gIssuSystemInfo.IssuProtocolSemId != 0)
    {
        OsixSemGive (gIssuSystemInfo.IssuProtocolSemId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssuGetCurrentSoftwareVersion                        */
/*                                                                           */
/* Description        : This function is called to get the current           */
/*                      running software version                             */
/*                                                                           */
/* Input(s)           : *pCurrentSwVersion                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IssuGetCurrentSoftwareVersion (tSNMP_OCTET_STRING_TYPE * pCurrentSwVersion)
{
    UINT1               au1SwVersion[ISSU_IMAGE_NAME_MAX_SIZE + 1];

    MEMSET (au1SwVersion, 0, sizeof(au1SwVersion));

    MEMCPY (au1SwVersion, IssGetSoftwareVersion (),
            sizeof(au1SwVersion)-1);

    MEMCPY (pCurrentSwVersion->pu1_OctetList, au1SwVersion,
            STRLEN (au1SwVersion));
    pCurrentSwVersion->i4_Length = (INT4) STRLEN (au1SwVersion);

    return;
}

/*****************************************************************************/
/* Function Name      : IssuMaintenanceModeEnable                            */
/*                                                                           */
/* Description        : This function is invoked from CLI/SNMP module when   */
/*                      enabling maintenance mode. This function sets the    */
/*                      maintenance admin status as TRUE, which will be      */
/*                      considered as ISSU IN-PROGRESS                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS/ISSU_FAILURE                            */
/*****************************************************************************/
UINT4
IssuMaintenanceModeEnable (VOID)
{
    tIssuTrapInfo           IssuTrapInfo;
	UINT1                   u1IssuOldOperStatus = 0;
#ifdef NPAPI_WANTED    
    tIssuHwStatusInfo       IssuHwStatusInfo;
    
    MEMSET(&IssuHwStatusInfo,0,sizeof(tIssuHwStatusInfo));
#endif    
    MEMSET(&IssuTrapInfo, 0, sizeof (tIssuTrapInfo));

    ISSU_TRC (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
            "Entering into Maintenance Mode\r\n");

    /* Assigning the value of procedure status */
    IssuSetProcedureStatus (ISSU_PROCEDURE_IN_PROGRESS);

    /* Assigning the command status value to default value while 
     * enabling the ISSU maintenance mode */
    gIssuSystemStatusInfo.u4IssuCommandStatus = ISSU_CMD_NOT_STARTED;

    /* Set MM Oper Status as In-progress */
    u1IssuOldOperStatus = gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus;
    /* Set MM Oper Status as In-progress */
    gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = 
        ISSU_MAINTENANCE_OPER_INPROGRESS;

    /* In Full-compatible mode, as warm boot will be triggered,
     *  following needs to be done 
     * 1. Freeze mac learning 
     * 2. Indicate MM to RM module for processing pending sync messages 
     *
     * In case of In-compatible mode, cold boot will be triggered
     * so no need to block mac learning or indicate RM module */
	if ((gIssuSystemInfo.u4IssuMode == ISSU_FULL_COMPATIBLE_MODE) && 
		(RmGetNodeState() == RM_ACTIVE))
    {
		/* Freeze MAC learning */
		if (VlanApiSetFdbFreezeStatus (VLAN_DISABLED) != VLAN_SUCCESS)
		{
			ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC,
					"IssuMaintenanceModeEnable -VlanApiSetFdbFreezeStatus failed\r\n");
            gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = u1IssuOldOperStatus;
			return ISSU_FAILURE;
		}
		/* Post an event to RM queue */
		if (RmApiIssuMaintenanceModeEnable() != RM_SUCCESS)
		{
			ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC,
					"IssuMaintenanceModeEnable - Posting event to RM failed\r\n");
            gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = u1IssuOldOperStatus;
            /* Enable the mac learing which is disable above */
     		if (VlanApiSetFdbFreezeStatus (VLAN_ENABLED) != VLAN_SUCCESS)
	    	{
		    	ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC, 
			    		"IssuMaintenanceModeEnable -failed " 
				        	"to re-enable mac learning \r\n");
		    }
			return ISSU_FAILURE;
		}

#ifdef NPAPI_WANTED        
	IssuHwStatusInfo.u4Opcode = ISSU_INIT_PROCEDURE;
	if (FsNpIssuLoadVersionUpgrade (&IssuHwStatusInfo) != FNP_SUCCESS)
	{
	    ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC, 
		    "IssuMaintenanceModeEnable - failed" 
		    " to init manintance mode in NP\r\n");
        /* Enable the mac learing which is disable above */
        if (VlanApiSetFdbFreezeStatus (VLAN_ENABLED) != VLAN_SUCCESS)
        {
            ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC, 
                    "IssuMaintenanceModeEnable - failed " 
                    "to re-enable mac learning \r\n");
        }
	    return ISSU_FAILURE;
	}
#endif		

		/* Oper status will be enabled once the pending RM sync
		 * messages are processed.
		 * Trap message will be sent after setting oper status */
	} 
    else
	{
        /* Set oper status and send trap */
		gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus =
			ISSU_MAINTENANCE_OPER_ENABLE;
		IssuTrapInfo.u1IssuMaintenanceMode = 
            gIssuSystemInfo.u1IssuMaintenanceMode;
		IssuTrapInfo.u1IssuMaintenanceOperStatus = 
            ISSU_MAINTENANCE_OPER_ENABLE;
		IssuSnmpifSendTrap(ISSU_TRAP_MAINTENANCE_STATUS, &IssuTrapInfo);
	}

    ISSU_TRC (ISSU_CRITICAL_TRC |ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
            "Entered into Maintenance Mode\r\n");
    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuMaintenanceModeDisable                           */
/*                                                                           */
/* Description        : This function is invoked from CLI/SNMP module when   */
/*                      disabling maintenance mode. This function sets the   */
/*                      maintenance admin status as 'FALSE'. Triggers the    */
/*                      system to exit from the maintenance mode to restore  */
/*                      normal system operations                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS/ISSU_FAILURE                            */
/*****************************************************************************/
UINT4
IssuMaintenanceModeDisable (VOID)
{
    tIssuTrapInfo           IssuTrapInfo;
    tUtlSysPreciseTime      CurrentTime;

#ifdef NPAPI_WANTED
    tIssuHwStatusInfo       IssuHwStatusInfo;
    UINT4                   u4IfIndex = 0;
    UINT1                   u1OperStatus = 0;
    UINT1                   u1LinkStatus = 0;
  
    MEMSET(&IssuHwStatusInfo,0,sizeof (tIssuHwStatusInfo));
#endif
    UINT1                   u1IssuOldOperStatus = 0;

    MEMSET(&IssuTrapInfo, 0, sizeof (tIssuTrapInfo));
    MEMSET(&CurrentTime, 0, sizeof (tUtlSysPreciseTime));

    /* Set MM Oper Status as In-progress */
    u1IssuOldOperStatus = gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus;
    gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = 
        ISSU_MAINTENANCE_OPER_INPROGRESS;

    if ((gIssuSystemInfo.u4IssuMode == ISSU_FULL_COMPATIBLE_MODE) &&
        (RmGetNodeState () == RM_ACTIVE))
    {
		/* Enable MAC learning */
		if (VlanApiSetFdbFreezeStatus (VLAN_ENABLED) != VLAN_SUCCESS)
		{
			ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC, 
					"IssuMaintenanceModeDisable - failed" 
					" to enable mac learning \r\n");
            /* Revert Oper Status upon Failure */
            gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = 
                u1IssuOldOperStatus; 
			return ISSU_FAILURE;
		}

#ifdef NPAPI_WANTED
		/* Audit and update Ports status from Active node */ 
        for (u4IfIndex = 1; u4IfIndex <=      
                SYS_DEF_MAX_PHYSICAL_INTERFACES; u4IfIndex++)
        {
            /* Get Oper Status from CFA */
            if (CfaGetIfOperStatus (u4IfIndex, &u1OperStatus) == CFA_SUCCESS)
            {
                /* Get the Link Status from the hardware */
                u1LinkStatus = IssuCfaNpGetLinkStatus (u4IfIndex); 
                if (u1LinkStatus != u1OperStatus)
                {
                    /* Update the Link Status to CFA incase of mismatch */

                    /* Release the ISSU lock before calling CFA API 
                     * Note: This is required as the CFA Api calls the 
                     * ISSU Api for checking the MM status */    
                    CfaInterfaceStatusChangeIndication(u4IfIndex, 
                            u1LinkStatus);
                } 
            }
        } /* end - for*/

	IssuHwStatusInfo.u4Opcode = ISSU_EXIT_PROCEDURE;
	if (FsNpIssuLoadVersionUpgrade (&IssuHwStatusInfo) != FNP_SUCCESS)
	{
	    ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC, 
		    "IssuMaintenanceModeDisable - failed" 
		    " to exit manintance mode in NP\r\n");
	    return ISSU_FAILURE;
	}
#endif
        if(RmGetIssuPeerNodeCount()==0)
        {
           /* Error Recovery Scenorio: Incase of peer not present
            * post the event to all protocols 
            */
            RmApiPostPeerEventToProtocols(RM_PEER_DOWN); 
            RmApiSetIssuPeerNodeCount(1);
        }
    }

    /* Set ISSU Overall Procedure Status to not-initiated  if load version
     * is not executed during exit from Maintenace Mode 
     * If Force-standby failed and we are exiting without executing load version 
     * the procedure status is set as Failed 
     * If load version has been issued, update the command status as
     * procedure status*/
    if((gIssuSystemInfo.u4IssuCommand == ISSU_CMD_LOAD_VERSION) ||
       ((gIssuSystemInfo.u4IssuCommand == ISSU_CMD_FORCE_STANDBY) &&
       (gIssuSystemStatusInfo.u4IssuCommandStatus == ISSU_CMD_FAILED)))
    {
        IssuSetProcedureStatus (gIssuSystemStatusInfo.u4IssuCommandStatus);
    } 
    else
    {
        IssuSetProcedureStatus (ISSU_PROCEDURE_NOT_INITIATED);
    }

    /* Get the current time and update the value in node id file
    * for ISSU last upgraded time */
    if (gIssuSystemStatusInfo.u4IssuProcedureStatus == ISSU_PROCEDURE_SUCCESSFUL)
    {
    	UtlGetPreciseSysTime (&CurrentTime);
        gIssuSystemStatusInfo.u4LastUpgradeTime = CurrentTime.u4Sec;
        IssSetIssuLastUpgradeTime (gIssuSystemStatusInfo.u4LastUpgradeTime);
    }

    /* Reset nodeid file for ISSU configurations */
    IssSetIssuStartupMode(ISSU_DEFAULT_STARTUP_MODE);
    IssSetIssuMode(ISSU_DEFAULT_MODE);
    IssSetIssuNodeRole(ISSU_DEFAULT_NODE_ROLE);
    IssWriteNodeidFile ();

    /* Set maintenance mode oper status */
    gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus = 
        ISSU_MAINTENANCE_OPER_DISABLE;
    IssuTrapInfo.u1IssuMaintenanceMode = gIssuSystemInfo.u1IssuMaintenanceMode;
    IssuTrapInfo.u1IssuMaintenanceOperStatus = ISSU_MAINTENANCE_OPER_DISABLE;
    IssuSnmpifSendTrap(ISSU_TRAP_MAINTENANCE_STATUS, &IssuTrapInfo);

    ISSU_TRC (ISSU_CRITICAL_TRC |ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
            "Exited from Maintenance Mode\r\n");
    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuHandleLoadVersion                                */
/*                                                                           */
/* Description        : This function is invoked from CLI/SNMP when giving   */
/*                      loadversion command/object. Node is rebooted with new*/
/*                      ISS image.ISSU mode already set for its operation and*/
/*                      also load the software from the patch specified.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Returns            : ISSU_SUCCESS or ISSU_FAILURE                         */
/*****************************************************************************/
UINT4
IssuHandleLoadVersion (VOID)
{
    UINT4                   u4IssuMode = 0;
    UINT1                   au1CurrentSwVersion[ISSU_IMAGE_NAME_MAX_SIZE +1];
    tSNMP_OCTET_STRING_TYPE CurrentSwVersion;
#ifdef NPAPI_WANTED
    tIssuHwStatusInfo         IssuHwStatusInfo;
#endif    
    
    MEMSET (au1CurrentSwVersion, 0, sizeof(au1CurrentSwVersion));
    MEMSET (&CurrentSwVersion, 0, sizeof(tSNMP_OCTET_STRING_TYPE));

    /* Validate Loadsoftware path */
    if (IssuCheckForValidFilePath(gIssuSystemInfo.au1IssuLoadSWPath) == ISSU_FAILURE)
    {
        IssuSetCommandStatus(ISSU_CMD_LOAD_VERSION, ISSU_CMD_FAILED);
        ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |
                ISSU_CRITICAL_TRC,"IssuCheckForValidFilePath faild!"
                               "-Loadsoftware path is not a valid one"
                               "(File may not present in the path)\r\n");
        return ISSU_FAILURE;
    }
#ifdef NPAPI_WANTED
    MEMSET(&IssuHwStatusInfo, 0, sizeof (tIssuHwStatusInfo));

    MEMCPY(IssuHwStatusInfo.au1IssuLoadSWPath, gIssuSystemInfo.au1IssuLoadSWPath,
            sizeof(gIssuSystemInfo.au1IssuLoadSWPath)-1);
    IssuHwStatusInfo.u4IssuMode = gIssuSystemInfo.u4IssuMode;
    IssuHwStatusInfo.u4Opcode = ISSU_LOAD_SOFTWARE;
    /* opens init_script file for next auto reload */
    if (IssuCreateInitScript (&IssuHwStatusInfo) == ISSU_FAILURE)
    {
        IssuSetCommandStatus(ISSU_CMD_LOAD_VERSION, ISSU_CMD_FAILED);
        ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |
                ISSU_CRITICAL_TRC,"IssuCreateInitScript Faild !\r\n");
        return ISSU_FAILURE;
    }
#endif /* NPAPI_WANTED */
    CurrentSwVersion.pu1_OctetList = au1CurrentSwVersion;
    CurrentSwVersion.i4_Length = ISSU_IMAGE_NAME_MAX_SIZE;
    
    /*Writing the ISSU mode and ISSU start-up mode into the nodeid file*/
    IssSetIssuStartupMode(ISSU_MAINTENANCE_MODE_ENABLE);

    u4IssuMode = gIssuSystemInfo.u4IssuMode;
    IssSetIssuMode(u4IssuMode);

    /* Set the Old software version for rollback incase of failure during 
     * new image upgrade */
    IssuGetCurrentSoftwareVersion (&CurrentSwVersion);
    MEMCPY(gIssuSystemStatusInfo.au1IssuRollbackSoftwareVersion,
           CurrentSwVersion.pu1_OctetList,CurrentSwVersion.i4_Length);

    /* Update the Current path to Rollback path */
    MEMCPY(gIssuSystemStatusInfo.au1IssuRollbackSWPath,
            gIssuSystemStatusInfo.au1IssuCurrentSWPath,
            sizeof (gIssuSystemStatusInfo.au1IssuCurrentSWPath)-1);

    /* Update the Load Version Sw Path to current path */
    MEMCPY (gIssuSystemStatusInfo.au1IssuCurrentSWPath,
            gIssuSystemInfo.au1IssuLoadSWPath,
            sizeof (gIssuSystemInfo.au1IssuLoadSWPath)-1);
    IssSetRollBackIssVersion(gIssuSystemStatusInfo.au1IssuRollbackSoftwareVersion);
    IssSetRollBackSwPath (gIssuSystemStatusInfo.au1IssuRollbackSWPath);
    IssSetCurrentSwPath (gIssuSystemStatusInfo.au1IssuCurrentSWPath);
    IssuSetCommandStatus(ISSU_CMD_LOAD_VERSION, ISSU_CMD_IN_PROGRESS);

    /* Write all the newly updated information to the nodeid file */
    IssWriteNodeidFile();
#if defined(LINUXSIM_WANTED) || defined(NPSIM_WANTED)
    PRINTF ("\nAlert: Switch needs to be restarted "
            "to complete the system restart process!!!!!!\n");
    /*
     *Shutdown OSIX in case the restart is attempted in Desktop linux version,
     *since restart cannot be triggered */
    OsixShutDown ();
#else
    /* Notify Msr to restart the system */
    IssLoadVersionRestartNotification();
#endif /* NPSIM_WANTED */

    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuHandleForceStandby                               */
/*                                                                           */
/* Description        : This function is invoked from CLI/SNMP module when   */
/*                      giving 'Force Standby' command                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Returns            : None                                                 */
/*****************************************************************************/
VOID
IssuHandleForceStandby (VOID)
{
    /* Setting the ISSU command status value to Successful */
    IssuSetCommandStatus(ISSU_CMD_FORCE_STANDBY, ISSU_CMD_IN_PROGRESS);

    ISSU_TRC (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
            "Sending force standby event to HB module\r\n");

    if(HbApiIssuForceStandBy() != HB_SUCCESS)
    {
        ISSU_TRC (ISSU_ALL_FAILURE_TRC | ISSU_CRITICAL_TRC, 
	            "IssuHandleForceStandby - failed" 
		    " to post Force Standby event to HB module\r\n");
     
        IssuSetCommandStatus(ISSU_CMD_FORCE_STANDBY, ISSU_CMD_FAILED);
        return;
    }
    /* Writing the ISSU node role into the nodeid file on Success case */
    IssSetIssuNodeRole(ISSU_NODE_ROLE_STANDBY);
    IssWriteNodeidFile();
    
    return;
}

/*****************************************************************************/
/* Function Name      : IssuSetProcedureStatus                               */
/*                                                                           */
/* Description        : This function is called from ISSU or external        */
/*                      modules to set overall ISSU procedure status         */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssuSetProcedureStatus (UINT4 u4ProcedureStatus)
{
    tIssuTrapInfo       IssuTrapInfo;

    MEMSET(&IssuTrapInfo, 0, sizeof (tIssuTrapInfo));

    ISSU_TRC_ARG1 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
            "Setting the procedure status as %d \r\n",u4ProcedureStatus);

    gIssuSystemStatusInfo.u4IssuProcedureStatus = u4ProcedureStatus;

    /* TRAP to be sent for successful and failure scenario of ISSU procedure */
    if ((u4ProcedureStatus == ISSU_PROCEDURE_SUCCESSFUL) ||
            (u4ProcedureStatus == ISSU_PROCEDURE_FAILED))
    {
        IssuTrapInfo.u4IssuProcedureStatus =
            gIssuSystemStatusInfo.u4IssuProcedureStatus;
        IssuSnmpifSendTrap(ISSU_TRAP_PROCEDURE_STATUS,&IssuTrapInfo);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : IssuSetCommandStatus                                 */
/*                                                                           */
/* Description        : This function is called from ISSU or external        */
/*                      modules to set ISSU command status                   */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID 
IssuSetCommandStatus (UINT4 u4IssuCommand, UINT4 u4CommandStatus)
{
    tIssuTrapInfo       IssuTrapInfo;

    MEMSET(&IssuTrapInfo, 0, sizeof (tIssuTrapInfo));

    ISSU_TRC_ARG2 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
            "Setting the command status as %d for command %d\r\n",
            u4CommandStatus,u4IssuCommand);

    gIssuSystemStatusInfo.u4IssuCommandStatus = u4CommandStatus; 

    /* TRAP to be sent for successful completion of Load Version command */
    if ((u4CommandStatus == ISSU_CMD_SUCCESSFUL) ||
        (u4CommandStatus == ISSU_CMD_FAILED))
     {
        IssuTrapInfo.u4IssuCommand = u4IssuCommand;
        IssuTrapInfo.u4IssuCommandStatus =
            gIssuSystemStatusInfo.u4IssuCommandStatus;
        IssuSnmpifSendTrap(ISSU_TRAP_COMMAND_STATUS,&IssuTrapInfo);
    }

    return;
}

/************************************************************************
*  Function Name   : IssuPrintUpgradeTime
*
*  Description     : This function will convert the time in structure to
                     string.
*
*  Input           : u4Secs -Seconds
*
*  Output          : pIssuUpgradeTime- Returns the Value in array
*
*  Returns         : NONE
************************************************************************/

VOID 
IssuPrintUpgradeTime (UINT4 u4Secs, UINT1 *pIssuUpgradeTime)
{
    tUtlTm              	tm;
    UINT1               	au1Mon[12];

    MEMSET (au1Mon, 0, sizeof (au1Mon));
    MEMSET (&tm, 0, sizeof (tUtlTm));

    /* Adding 5.30 hrs for converting to IST */
    u4Secs = u4Secs + 19800;

    /* tm structure filled for seconds */
    UtlGetTimeForSeconds (u4Secs, &tm);

    tm.tm_year = tm.tm_year - 30;

    /* Month is filled in array */
    if (tm.tm_mon == 0)
    {
        STRCPY (au1Mon, "Jan");
    }
    else if (tm.tm_mon == 1)
    {
        STRCPY (au1Mon, "Feb");
    }
    else if (tm.tm_mon == 2)
    {
        STRCPY (au1Mon, "Mar");
    }
    else if (tm.tm_mon == 3)
    {
        STRCPY (au1Mon, "Apr");
    }
    else if (tm.tm_mon == 4)
    {
        STRCPY (au1Mon, "May");
    }
    else if (tm.tm_mon == 5)
    {
        STRCPY (au1Mon, "Jun");
    }
    else if (tm.tm_mon == 6)
    {
        STRCPY (au1Mon, "Jul");
    }
    else if (tm.tm_mon == 7)
    {
        STRCPY (au1Mon, "Aug");
    }
    else if (tm.tm_mon == 8)
    {
        STRCPY (au1Mon, "Sep");
    }
    else if (tm.tm_mon == 9)
    {
        STRCPY (au1Mon, "Oct");
    }
    else if (tm.tm_mon == 10)
    {
        STRCPY (au1Mon, "Nov");
    }
    else if (tm.tm_mon == 11)
    {
        STRCPY (au1Mon, "Dec");
    }

    /* Values are printed in the structure */
    SPRINTF ((CHR1 *) pIssuUpgradeTime,
             "%.2u %s %4u %.2u:%.2u:%.2u",
             tm.tm_mday, au1Mon, tm.tm_year, tm.tm_hour, tm.tm_min, tm.tm_sec );
}

/*****************************************************************************/
/* Function Name      : IssuCheckForValidFilePath                            */
/*                                                                           */
/* Description        : This function is called from ISSU or external        */
/*                      modules to check whether the configured path is valid*/
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_FAILURE/ISSU_SUCCESS                            */
/*****************************************************************************/
INT4
IssuCheckForValidFilePath (UINT1 *pFilePath)
{
    if (FileStat ((const CHR1*) (pFilePath)) != OSIX_SUCCESS)
    {
        return ISSU_FAILURE;
    }

    return ISSU_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : IssuSetLoadversionTriggered                           */
/*                                                                           */
/* Description        : This function is called from ISSU module to set      */
/*                      u1IsLoadVersionTriggered after issu loadversion       */
/*                      triggerd                                             */
/*                                                                           */
/* Input(s)           : u1Value - OSIX_TRUE/OSIX_FALSE                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID IssuSetLoadversionTriggered(UINT1 u1Value)
{
    gu1IsLoadVersionTriggered = u1Value;
    return ;
}

#endif
