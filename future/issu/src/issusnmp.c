/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: issusnmp.c,v 1.2 2016/05/06 10:11:38 siva Exp $
 *
 * Description: This file contains the ISSU trap generation
 *              related code.
 *                            
 ********************************************************************/

#ifndef __ISSUSNMP_C__
#define __ISSUSNMP_C__

#include "snmputil.h"
#include "issuinc.h"
#include "fsissu.h"

/****************************************************************************
 *                                                                          *
 * Function     : IssuSnmpifSendTrap                                        *
 *                                                                          *
 * Description  : Routine to send trap to SNMP Agent.                       *
 *                                                                          *
 * Input        : u1TrapId: Trap identifier                                 *
 *                pTrapInfo : Pointer to structure having the trap info.    *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : VOID                                                      *
 *                                                                          *
 ****************************************************************************/
PUBLIC VOID
IssuSnmpifSendTrap (UINT1 u1TrapId,VOID *pTrapInfo)
{
#if ((defined(FUTURE_SNMP_WANTED))||(defined(SNMP_3_WANTED))||\
		(defined(SNMPV3_WANTED)))
	tSNMP_VAR_BIND          *pVbList = NULL;
	tSNMP_VAR_BIND 			*pStartVb = NULL;
	tSNMP_OID_TYPE          *pEnterpriseOid = NULL;
	tSNMP_COUNTER64_TYPE     u8CounterVal = { 0, 0 };
	tSNMP_COUNTER64_TYPE     SnmpCnt64Type;
	UINT1                    au1Buf[ISSU_BUFFER_SIZE];
	UINT4                    au4snmpTrapOid[] = {1,3,6,1,6,3,1,1,4,1,0};
	UINT4                    au4IssuTrapOid[] = {1,3,6,1,4,1,29601,2,103,2,0};
	tSNMP_OID_TYPE          *pSnmpTrapOid = NULL;
	tIssuTrapInfo           *pIssuTrapInfo = NULL;
	tSNMP_OID_TYPE          *pOid = NULL;

	pIssuTrapInfo = (tIssuTrapInfo *) pTrapInfo;

	SnmpCnt64Type.msn = 0;
	SnmpCnt64Type.lsn = 0;

	ISSU_TRC_ARG1 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
			"Trap to be sent for TRAP ID %d !!!\r\n",u1TrapId);

	/* The format of SNMP Trap varbinding which is being send using
	 * SNMP_AGT_RIF_Notify_v2Trap() is as given below:
	 *
	 *
	 ********************************************************************************
	 *                   *                   *    Varbind     *     Varbind    *
	 * sysUpTime = Time  * snmpTrapOID = OID * OID# 1 = Value * OID# 2 = Value *
	 *                   *                   *                *                *
	 ********************************************************************************
	 *
	 * First Var binding sysUpTime = Time - is filled by the SNMP_AGT_RIF_Notify_v2Trap().
	 * Second Var binding snmpTrapOID - OID should be first Varbind in our list 
	 * to the api SNMP_AGT_RIF_Notify_v2Trap().
	 * The remaining Varbind are updated based on the type of the trap, which
	 * are going to be generated.
	 *
	 */

	/* Allocate a memory for storing OID Value which is the OID of the
	 * Notification MIB object and assign it */

	pEnterpriseOid = alloc_oid (ISSU_TRAP_OID_LEN);
	if (pEnterpriseOid == NULL)
	{
		return;
	}

	/* Assigning TrapOid to EnterpriseOid and binding it to packet. */
	MEMCPY (pEnterpriseOid->pu4_OidList, au4IssuTrapOid, sizeof (au4IssuTrapOid));
	pEnterpriseOid->u4_Length = sizeof (au4IssuTrapOid) / sizeof (UINT4);
	pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

	/* Allocate the memory for the Varbind for snmpTrapOID MIB object OID and
	 * the OID of the MIB object for those trap is going to be generated.
	 */

	/* Allocate a memory for storing snmpTrapOID OID and assign it */
	pSnmpTrapOid = alloc_oid (ISSU_TRAP_OID_LEN);
	if (pSnmpTrapOid == NULL)
	{
		SNMP_FreeOid (pEnterpriseOid);
		return;
	}

	/* Assigning the OID value and its length of snmpTrapOid to psnmpTrapOid */
	MEMCPY (pSnmpTrapOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
	pSnmpTrapOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);


	/* Form a VarBind for snmpTrapOID OID and its value */
	pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
			SNMP_DATA_TYPE_OBJECT_ID, 0L, 0, NULL, pEnterpriseOid, u8CounterVal);

	/* If the pVbList is empty or values are binded then free the memory
	   allocated to pEnterpriseOid and pSnmpTrapOid. */
	if (pVbList == NULL)
	{
		SNMP_FreeOid (pEnterpriseOid);
		SNMP_FreeOid (pSnmpTrapOid);
		return;
	}

	pStartVb = pVbList;

	switch (u1TrapId)
	{
		case ISSU_TRAP_MAINTENANCE_STATUS :
			if (gIssuSystemInfo.u1IssuTrapStatus != ISSU_TRAP_ENABLE)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			SPRINTF ((CHR1 *) au1Buf, "fsIssuMaintenanceMode");
			if ((pOid = IssuMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			/* Forming a VarBind for BaseWtpId, its value and storing it in pVbList */
			pVbList->pNextVarBind =  SNMP_AGT_FormVarBind (pOid,
					SNMP_DATA_TYPE_INTEGER, 0, pIssuTrapInfo->u1IssuMaintenanceMode,
					NULL, NULL, SnmpCnt64Type);

			/* Checking if the pVbList is empty and freeing the memory allocated
			   to pSnmpTrapOid and pEnterpriseOid if it is NULL */
			if (pVbList->pNextVarBind == NULL)
			{
				SNMP_FreeOid (pSnmpTrapOid);
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pOid);
				return;
			}
			pVbList = pVbList->pNextVarBind;
			pVbList->pNextVarBind = NULL;

			SPRINTF ((CHR1 *) au1Buf, "fsIssuMaintenanceOperStatus");
			if ((pOid = IssuMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			/* Forming a VarBind for OID of MaintenanceOperStatus and its value */
			pVbList->pNextVarBind = 
				(tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
						SNMP_DATA_TYPE_INTEGER,
						0L, pIssuTrapInfo->u1IssuMaintenanceOperStatus,
						NULL, NULL, SnmpCnt64Type);

			ISSU_TRC_ARG2 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
					"Trap sent for Maintenance admin status %d Oper status %d\r\n",
					pIssuTrapInfo->u1IssuMaintenanceMode,
					pIssuTrapInfo->u1IssuMaintenanceOperStatus);

			/* Checking if the pVbList is empty and freeing the memory allocated
			   to pSnmpTrapOid and pEnterpriseOid if it is NULL */
			if (pVbList->pNextVarBind == NULL)
			{
				SNMP_FreeOid (pSnmpTrapOid);
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pOid);
				return;
			}
			pVbList = pVbList->pNextVarBind;
			pVbList->pNextVarBind = NULL;
			break;

		case ISSU_TRAP_COMMAND_STATUS:
			if (gIssuSystemInfo.u1IssuTrapStatus != ISSU_TRAP_ENABLE)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			SPRINTF ((CHR1 *) au1Buf, "fsIssuCommand");
			if ((pOid = IssuMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			pVbList->pNextVarBind =
				(tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
						SNMP_DATA_TYPE_INTEGER,
						0L,
						(INT4)pIssuTrapInfo->u4IssuCommand,
						NULL,NULL,
						u8CounterVal);

			/* Checking if the pVbList is empty and freeing the memory allocated
			   to pSnmpTrapOid and pEnterpriseOid if it is NULL */

			if (pVbList->pNextVarBind == NULL)
			{
				SNMP_FreeOid (pSnmpTrapOid);
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pOid);
				return;
			}
			pVbList = pVbList->pNextVarBind;
			pVbList->pNextVarBind = NULL;

			SPRINTF ((CHR1 *) au1Buf, "fsIssuCommandStatus");
			if ((pOid = IssuMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			/* Forming a VarBind , its value and storing it in pVbList */
			pVbList->pNextVarBind =  SNMP_AGT_FormVarBind (pOid,
					SNMP_DATA_TYPE_INTEGER,
					0L,
					(INT4)pIssuTrapInfo->u4IssuCommandStatus,
					NULL, NULL,
					u8CounterVal);

			ISSU_TRC_ARG2 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
					"Trap sent for command %d with command status as %d\r\n",
					pIssuTrapInfo->u4IssuCommand, pIssuTrapInfo->u4IssuCommandStatus);

			/* Checking if the pVbList is empty and freeing the memory allocated
			   to pSnmpTrapOid and pEnterpriseOid if it is NULL */

			if (pVbList->pNextVarBind == NULL)
			{
				SNMP_FreeOid (pSnmpTrapOid);
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pOid);
				return;
			}
			pVbList = pVbList->pNextVarBind;
			pVbList->pNextVarBind = NULL;
			break;

		case ISSU_TRAP_PROCEDURE_STATUS:
			if (gIssuSystemInfo.u1IssuTrapStatus != ISSU_TRAP_ENABLE)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			SPRINTF ((CHR1 *) au1Buf, "fsIssuProcedureStatus");
			if ((pOid = IssuMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
			{
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pSnmpTrapOid);
				return;
			}

			/* Forming a VarBind for OID of ifIndex and its value */
			pVbList->pNextVarBind =
				(tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
						SNMP_DATA_TYPE_INTEGER,
						0L,
						(INT4)pIssuTrapInfo->u4IssuProcedureStatus,
						NULL,NULL,
						u8CounterVal);

			ISSU_TRC_ARG1 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
					"Trap sent for procedure status %d\r\n",
					pIssuTrapInfo->u4IssuProcedureStatus);

			/* Checking if the pVbList is empty and freeing the memory allocated
			   to pSnmpTrapOid and pEnterpriseOid if it is NULL */

			if (pVbList->pNextVarBind == NULL)
			{
				SNMP_FreeOid (pSnmpTrapOid);
				SNMP_FreeOid (pEnterpriseOid);
				SNMP_FreeOid (pOid);
				return;
			}
			pVbList = pVbList->pNextVarBind;
			pVbList->pNextVarBind = NULL;
			break;

		default:
			break;
	}
#ifdef SNMP_2_WANTED
	/* The following API sends the Trap info to the FutureSNMP Agent. */
	SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
	ISSU_TRC_ARG1 (ISSU_MGMT_TRC |ISSU_CONTROL_PATH_TRC,
			"Trap sent successfully for TRAP ID %d !!!\r\n",u1TrapId);

	/* Replace this function with the Corresponding Function provided by 
	 * SNMP Agent for Notifying Traps.
	 */
#endif
#else

	UNUSED_PARAM (u1TrapId);
	UNUSED_PARAM (pTrapInfo);

#endif /* FUTURE_SNMP_WANTED */
	return;
}

/****************************************************************************
 *                                                                          *
 * Function     : IssuUtilParseSubIdNew                                      *
 *                                                                          *
 * Description  : returns the numeric value of ppu1TempPtr                  *
 *                                                                          *
 * Input        : **ppu1TempPtr                                             *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : value of ppu1TempPtr or INVALID                           *
 *                                                                          *
 ****************************************************************************/

INT4
IssuUtilParseSubIdNew (UINT1 **ppu1TempPtr)
{
    INT4                i4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        i4Value = (i4Value * ISSU_NUM_TEN) +
            (*pu1Tmp & ISSU_MAX_HEX_SINGLE_DIGIT);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4Value = -1;
    }
    *ppu1TempPtr = pu1Tmp;
    return (i4Value);
}
/******************************************************************************
* Function :   IssuMakeObjIdFrmDotNew
*
* Description: This Function retuns the OID  of the given string for the
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
* *******************************************************************************/

tSNMP_OID_TYPE     *
IssuMakeObjIdFromDotNew (INT1 *i1TextStr)
{
    tSNMP_OID_TYPE    *pOidPtr = NULL;
    INT1              *pTempPtr = NULL;
	INT1			  *pDotPtr = NULL;
    INT1               ai1tempBuffer [ISSU_SNMP_TEMP_BUFF];
    UINT2              u2Index = 0;
    UINT2              u2DotCount = 0;
    UINT1             *pu1TmpPtr = NULL;
    UINT2              u2BufferLen = 0;

    MEMSET (ai1tempBuffer, 0, sizeof (ai1tempBuffer));
    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*i1TextStr))
    {
        pDotPtr = (INT1 *) STRCHR ((INT1 *) i1TextStr, '.');

        /* if no dot, point to end of string */
        if (pDotPtr == NULL)
            pDotPtr = i1TextStr + STRLEN ((INT1 *) i1TextStr);
        pTempPtr = i1TextStr;

		for (u2Index = 0; ((pTempPtr < pDotPtr) && (u2Index < ISSU_BUFFER_SIZE));
				u2Index++)
        {
            if (u2Index < ISSU_SNMP_TEMP_BUFF)
            {
                ai1tempBuffer[u2Index] = *pTempPtr++;
            }
        }
        if (u2Index < ISSU_SNMP_TEMP_BUFF)
        {
            ai1tempBuffer[u2Index] = '\0';
        }

        for (u2Index = 0;
             u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))
             && orig_mib_oid_table[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (orig_mib_oid_table[u2Index].pName,
                         (INT1 *) ai1tempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1tempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1tempBuffer, orig_mib_oid_table[u2Index].pNumber,
                         (sizeof (ai1tempBuffer) - 1));
                break;
            }
        }

        if ((u2Index < (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID))) &&
            orig_mib_oid_table[u2Index].pName == NULL)
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u2BufferLen = (UINT2)(sizeof (ai1tempBuffer) - STRLEN (ai1tempBuffer));
        STRNCAT ((INT1 *) ai1tempBuffer, (INT1 *) pDotPtr,
                 (u2BufferLen <
                  STRLEN (pDotPtr) ? u2BufferLen : STRLEN (pDotPtr)));

    }
    else
    { /* is not alpha, so just copy into ai1tempBuffer */
        STRNCPY ((INT1 *) ai1tempBuffer, (INT1 *) i1TextStr,
                 (sizeof (ai1tempBuffer) - 1));
    }

    /* Now we've got something with numbers instead of an alpha header */
    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0; u2Index < ISSU_SNMP_TEMP_BUFF && ai1tempBuffer[u2Index] != '\0'; u2Index++)
    {
        if (ai1tempBuffer[u2Index] == '.')
            u2DotCount++;
    }
    if ((pOidPtr = alloc_oid ((INT4) (u2DotCount + 1))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) ai1tempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if ((pOidPtr->pu4_OidList[u2Index] =
             ((UINT4) (IssuUtilParseSubIdNew (&pu1TmpPtr)))) == (UINT4) -1)
        {
            free_oid (pOidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
            pu1TmpPtr++;        /* to skip over dot */
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (pOidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

#endif
