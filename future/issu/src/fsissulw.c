/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissulw.c,v 1.10 2016/03/17 12:37:48 siva Exp $
*
* Description: ISSU Protocol Low Level Routines
*********************************************************************/

#ifndef __FSISSULW_C
#define __FSISSULW_C

#include  "issuinc.h"

extern UINT4 FsIssuMaintenanceMode [11];
extern UINT4 FsIssuMode [11];
extern UINT4 FsIssuTraceOption [11];
extern UINT4 FsIssuTrapStatus [11];

/****************************************************************************
 Function    :  nmhGetFsIssuMaintenanceMode
 Input       :  The Indices

                The Object 
                retValFsIssuMaintenanceMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuMaintenanceMode (INT4 *pi4RetValFsIssuMaintenanceMode)
{
    *pi4RetValFsIssuMaintenanceMode =
        (INT4) gIssuSystemInfo.u1IssuMaintenanceMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuMaintenanceOperStatus
 Input       :  The Indices

                The Object 
                retValFsIssuMaintenanceOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuMaintenanceOperStatus (INT4 *pi4RetValFsIssuMaintenanceOperStatus)
{
    *pi4RetValFsIssuMaintenanceOperStatus =
        (INT4) gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuLoadSWPath
 Input       :  The Indices

                The Object 
                retValFsIssuLoadSWPath
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuLoadSWPath (tSNMP_OCTET_STRING_TYPE * pRetValFsIssuLoadSWPath)
{
    pRetValFsIssuLoadSWPath->i4_Length =
        (INT4) STRLEN (gIssuSystemInfo.au1IssuLoadSWPath);
    STRNCPY (pRetValFsIssuLoadSWPath->pu1_OctetList,
             gIssuSystemInfo.au1IssuLoadSWPath,
             pRetValFsIssuLoadSWPath->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuRollbackSWPath
 Input       :  The Indices

                The Object 
                retValFsIssuRollbackSWPath
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuRollbackSWPath (tSNMP_OCTET_STRING_TYPE *
                            pRetValFsIssuRollbackSWPath)
{
    pRetValFsIssuRollbackSWPath->i4_Length =
        (INT4) STRLEN (gIssuSystemStatusInfo.au1IssuRollbackSWPath);
    STRNCPY (pRetValFsIssuRollbackSWPath->pu1_OctetList,
             gIssuSystemStatusInfo.au1IssuRollbackSWPath,
             pRetValFsIssuRollbackSWPath->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuCurrentSWPath
 Input       :  The Indices

                The Object 
                retValFsIssuCurrentSWPath
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuCurrentSWPath (tSNMP_OCTET_STRING_TYPE * pRetValFsIssuCurrentSWPath)
{
    pRetValFsIssuCurrentSWPath->i4_Length =
        (INT4) STRLEN (gIssuSystemStatusInfo.au1IssuCurrentSWPath);
    STRNCPY (pRetValFsIssuCurrentSWPath->pu1_OctetList,
             gIssuSystemStatusInfo.au1IssuCurrentSWPath,
             pRetValFsIssuCurrentSWPath->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuSoftwareCompatFilePath
 Input       :  The Indices

                The Object 
                retValFsIssuSoftwareCompatFilePath
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuSoftwareCompatFilePath (tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsIssuSoftwareCompatFilePath)
{
    pRetValFsIssuSoftwareCompatFilePath->i4_Length =
        (INT4) STRLEN (gIssuSystemInfo.au1IssuSWCompatFilePath);
    STRNCPY (pRetValFsIssuSoftwareCompatFilePath->pu1_OctetList,
             gIssuSystemInfo.au1IssuSWCompatFilePath,
             pRetValFsIssuSoftwareCompatFilePath->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuSoftwareCompatCheckInit
 Input       :  The Indices

                The Object 
                retValFsIssuSoftwareCompatCheckInit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuSoftwareCompatCheckInit (INT4
                                     *pi4RetValFsIssuSoftwareCompatCheckInit)
{
    *pi4RetValFsIssuSoftwareCompatCheckInit = ISSU_COMP_CHECK_DISABLE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuSoftwareCompatCheckStatus
 Input       :  The Indices

                The Object 
                retValFsIssuSoftwareCompatCheckStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuSoftwareCompatCheckStatus (INT4
                                       *pi4RetValFsIssuSoftwareCompatCheckStatus)
{
    *pi4RetValFsIssuSoftwareCompatCheckStatus =
        (INT4) gIssuSystemStatusInfo.u4IssuSWCompatibilityCheckStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuMode
 Input       :  The Indices

                The Object 
                retValFsIssuMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuMode (INT4 *pi4RetValFsIssuMode)
{
    *pi4RetValFsIssuMode = (INT4) gIssuSystemInfo.u4IssuMode;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuCommand
 Input       :  The Indices

                The Object 
                retValFsIssuCommand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuCommand (INT4 *pi4RetValFsIssuCommand)
{
    *pi4RetValFsIssuCommand = (INT4) gIssuSystemInfo.u4IssuCommand;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuCommandStatus
 Input       :  The Indices

                The Object 
                retValFsIssuCommandStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuCommandStatus (INT4 *pi4RetValFsIssuCommandStatus)
{
    *pi4RetValFsIssuCommandStatus =
        (INT4) gIssuSystemStatusInfo.u4IssuCommandStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuProcedureStatus
 Input       :  The Indices

                The Object 
                retValFsIssuProcedureStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuProcedureStatus (INT4 *pi4RetValFsIssuProcedureStatus)
{
    *pi4RetValFsIssuProcedureStatus =
        (INT4) gIssuSystemStatusInfo.u4IssuProcedureStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuRollbackSoftwareVersion
 Input       :  The Indices

                The Object 
                retValFsIssuRollbackSoftwareVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuRollbackSoftwareVersion (tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsIssuRollbackSoftwareVersion)
{
    pRetValFsIssuRollbackSoftwareVersion->i4_Length =
        (INT4) STRLEN (gIssuSystemStatusInfo.au1IssuRollbackSoftwareVersion);
    STRNCPY (pRetValFsIssuRollbackSoftwareVersion->pu1_OctetList,
             gIssuSystemStatusInfo.au1IssuRollbackSoftwareVersion,
             pRetValFsIssuRollbackSoftwareVersion->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuTraceOption
 Input       :  The Indices

                The Object 
                retValFsIssuTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuTraceOption (INT4 *pi4RetValFsIssuTraceOption)
{
    *pi4RetValFsIssuTraceOption = (INT4) gIssuSystemInfo.u4IssuTraceOption;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuTrapStatus
 Input       :  The Indices

                The Object 
                retValFsIssuTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuTrapStatus (INT4 *pi4RetValFsIssuTrapStatus)
{
    *pi4RetValFsIssuTrapStatus = (INT4) gIssuSystemInfo.u1IssuTrapStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIssuLastUpgradeTime
 Input       :  The Indices

                The Object 
                retValFsIssuLastUpgradeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIssuLastUpgradeTime (UINT4 *pu4RetValFsIssuLastUpgradeTime)
{
    *pu4RetValFsIssuLastUpgradeTime = gIssuSystemStatusInfo.u4LastUpgradeTime;

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsIssuSoftwareCompatForVersion
 Input       :  The Indices

                The Object
                retValFsIssuSoftwareCompatForVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIssuSoftwareCompatForVersion(tSNMP_OCTET_STRING_TYPE * 
								pRetValFsIssuSoftwareCompatForVersion)
{
    pRetValFsIssuSoftwareCompatForVersion->i4_Length =
        (INT4) STRLEN (gIssuSystemInfo.au1IssuSWCompatVersion);
    STRNCPY (pRetValFsIssuSoftwareCompatForVersion->pu1_OctetList,
             gIssuSystemInfo.au1IssuSWCompatVersion,
             pRetValFsIssuSoftwareCompatForVersion->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuMaintenanceMode
 Input       :  The Indices

                The Object 
                setValFsIssuMaintenanceMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuMaintenanceMode (INT4 i4SetValFsIssuMaintenanceMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (gIssuSystemInfo.u1IssuMaintenanceMode == 
            i4SetValFsIssuMaintenanceMode)
    {
        return SNMP_SUCCESS;
    }

    gIssuSystemInfo.u1IssuMaintenanceMode =
        (UINT1) i4SetValFsIssuMaintenanceMode;

    if (i4SetValFsIssuMaintenanceMode == ISSU_MAINTENANCE_MODE_ENABLE)
    {
        /* Enter maintenance mode */
        if (IssuMaintenanceModeEnable() != ISSU_SUCCESS)
        {
            gIssuSystemInfo.u1IssuMaintenanceMode = 
                ISSU_MAINTENANCE_MODE_DISABLE;
            CLI_SET_ERR (CLI_ISSU_MM_ENABLE_FAILED);
            return SNMP_FAILURE;
        }
        if (RmGetNodeState () == RM_ACTIVE)
        {
            /* Notify the maintenance mode to StandBy */
            RM_GET_SEQ_NUM (&u4SeqNum);

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,FsIssuMaintenanceMode,
                                  u4SeqNum, FALSE, IssuLock, IssuUnLock, 0, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                              i4SetValFsIssuMaintenanceMode));
        }
    } 
    else if (i4SetValFsIssuMaintenanceMode == ISSU_MAINTENANCE_MODE_DISABLE)
    {
        /* Exit from maintenance mode */
        if (RmGetNodeState () == RM_ACTIVE)
        {
            /* Notify the maintenance mode to StandBy */
            RM_GET_SEQ_NUM (&u4SeqNum);

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,FsIssuMaintenanceMode,
                                  u4SeqNum, FALSE, IssuLock, IssuUnLock, 0, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                              i4SetValFsIssuMaintenanceMode));
             /*Delay has been introduced to sync-up the configuration 
              * with standby before further processing and it is mandatory 
              * to sync-up this configuration before doing any process */
             OsixTskDelay(5); 
        }
        if (IssuMaintenanceModeDisable() != ISSU_SUCCESS)
        {
            gIssuSystemInfo.u1IssuMaintenanceMode = 
                ISSU_MAINTENANCE_MODE_ENABLE;
            CLI_SET_ERR (CLI_ISSU_MM_DISABLE_FAILED);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuLoadSWPath
 Input       :  The Indices

                The Object 
                setValFsIssuLoadSWPath
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuLoadSWPath (tSNMP_OCTET_STRING_TYPE * pSetValFsIssuLoadSWPath)
{
    MEMSET (gIssuSystemInfo.au1IssuLoadSWPath, 0, ISSU_PATH_MAX_SIZE);

	STRNCPY (gIssuSystemInfo.au1IssuLoadSWPath,
			pSetValFsIssuLoadSWPath->pu1_OctetList,
			MEM_MAX_BYTES(pSetValFsIssuLoadSWPath->i4_Length,
				(ISSU_PATH_MAX_SIZE-1)));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuSoftwareCompatFilePath
 Input       :  The Indices

                The Object 
                setValFsIssuSoftwareCompatFilePath
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuSoftwareCompatFilePath (tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsIssuSoftwareCompatFilePath)
{
    MEMSET (gIssuSystemInfo.au1IssuSWCompatFilePath , 0, ISSU_PATH_MAX_SIZE);

	STRNCPY (gIssuSystemInfo.au1IssuSWCompatFilePath,
			pSetValFsIssuSoftwareCompatFilePath->pu1_OctetList,
			MEM_MAX_BYTES(pSetValFsIssuSoftwareCompatFilePath->i4_Length,
				(ISSU_PATH_MAX_SIZE-1)));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuSoftwareCompatCheckInit
 Input       :  The Indices

                The Object 
                setValFsIssuSoftwareCompatCheckInit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuSoftwareCompatCheckInit (INT4 i4SetValFsIssuSoftwareCompatCheckInit)
{
    INT4    i4CompCheckStatus = 0;

	if (i4SetValFsIssuSoftwareCompatCheckInit == ISSU_COMP_CHECK_ENABLE)
	{
		gIssuSystemStatusInfo.u4IssuSWCompatibilityCheckStatus =
			ISSU_CHECK_IN_PROGRESS;

		if ((IssuHandleCheckCompatibility (&i4CompCheckStatus,
				gIssuSystemInfo.au1IssuSWCompatVersion)) == ISSU_FAILURE)
		{
			gIssuSystemStatusInfo.u4IssuSWCompatibilityCheckStatus =
				ISSU_COMP_FAILED;
			return SNMP_FAILURE;
		}
		else
		{
			gIssuSystemStatusInfo.u4IssuSWCompatibilityCheckStatus =
				(UINT4) i4CompCheckStatus;
		}
	}

	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuMode
 Input       :  The Indices

                The Object 
                setValFsIssuMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuMode (INT4 i4SetValFsIssuMode)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    gIssuSystemInfo.u4IssuMode = (UINT4) i4SetValFsIssuMode;

	/* Write the ISSU mode to Node id file */
    IssSetIssuMode (gIssuSystemInfo.u4IssuMode);
    IssWriteNodeidFile ();

    if (RmGetNodeState () == RM_ACTIVE)
    {
        /* Notify the maintenance mode to StandBy */
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,FsIssuMode,
                u4SeqNum, FALSE, IssuLock, IssuUnLock, 0, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                    i4SetValFsIssuMode));
    }
   
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuCommand
 Input       :  The Indices

                The Object 
                setValFsIssuCommand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuCommand (INT4 i4SetValFsIssuCommand)
{
    gIssuSystemInfo.u4IssuCommand = (UINT4) i4SetValFsIssuCommand;

    if (i4SetValFsIssuCommand == ISSU_CMD_LOAD_VERSION)
    {
        /* Initiate Software Upgrade */
        if (IssuHandleLoadVersion () == ISSU_FAILURE)
        {
			CLI_SET_ERR (CLI_ISSU_SW_UPGRADE_FAILED);
			return SNMP_FAILURE;
        }
    }
    else if (i4SetValFsIssuCommand == ISSU_CMD_FORCE_STANDBY)
    {
        /* Handle Force Standby Command */
        IssuHandleForceStandby ();
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuTraceOption
 Input       :  The Indices

                The Object 
                setValFsIssuTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuTraceOption (INT4 i4SetValFsIssuTraceOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));


    gIssuSystemInfo.u4IssuTraceOption = (UINT4) i4SetValFsIssuTraceOption;

    if (RmGetNodeState () == RM_ACTIVE)
    {
        /* Notify the trace option to StandBy */
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,FsIssuTraceOption,
                u4SeqNum, FALSE, IssuLock, IssuUnLock, 0, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                    i4SetValFsIssuTraceOption));
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIssuTrapStatus
 Input       :  The Indices

                The Object 
                setValFsIssuTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIssuTrapStatus (INT4 i4SetValFsIssuTrapStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    gIssuSystemInfo.u1IssuTrapStatus = (UINT1) i4SetValFsIssuTrapStatus;

    if (RmGetNodeState () == RM_ACTIVE)
    {
        /* Notify the trap status to StandBy */
        RM_GET_SEQ_NUM (&u4SeqNum);

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo,FsIssuTrapStatus,
                u4SeqNum, FALSE, IssuLock, IssuUnLock, 0, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                    i4SetValFsIssuTrapStatus));
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsIssuSoftwareCompatForVersion
 Input       :  The Indices

                The Object
                setValFsIssuSoftwareCompatForVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsIssuSoftwareCompatForVersion(tSNMP_OCTET_STRING_TYPE *
								pSetValFsIssuSoftwareCompatForVersion)
{
    MEMSET (gIssuSystemInfo.au1IssuSWCompatVersion, 0, ISSU_IMAGE_NAME_MAX_SIZE);

	STRNCPY (gIssuSystemInfo.au1IssuSWCompatVersion,
			pSetValFsIssuSoftwareCompatForVersion->pu1_OctetList,
			MEM_MAX_BYTES(pSetValFsIssuSoftwareCompatForVersion->i4_Length,
				(ISSU_IMAGE_NAME_MAX_SIZE-1)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuMaintenanceMode
 Input       :  The Indices

                The Object 
                testValFsIssuMaintenanceMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuMaintenanceMode (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsIssuMaintenanceMode)
{

    /* Block Issu Maintenance Mode configuration in standby node */
    if ((RmGetNodeState () == RM_STANDBY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_MAINTENANCE_MODE_NOT_ALLOWED_IN_STANDBY);
        return SNMP_FAILURE;
    }

    /* Block Maintenance mode enable when Peer node is down */
    if ((i4TestValFsIssuMaintenanceMode == ISSU_MAINTENANCE_MODE_ENABLE) &&
        (RmGetPeerNodeCount () == 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_MAINTENANCE_MODE_NOT_ALLOWED_PEER_DOWN);
        return SNMP_FAILURE;
    }

    /* Check value for Issu Maintenance mode */
    if ((i4TestValFsIssuMaintenanceMode != ISSU_MAINTENANCE_MODE_ENABLE) &&
        (i4TestValFsIssuMaintenanceMode != ISSU_MAINTENANCE_MODE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if(gIssuSystemInfo.u1IssuMaintenanceMode == 
                (UINT1)i4TestValFsIssuMaintenanceMode)
    {
        return SNMP_SUCCESS;
    }

    /* Block disabling the maintenance mode when Issu command is in progress */
    if ((i4TestValFsIssuMaintenanceMode == ISSU_MAINTENANCE_MODE_DISABLE) &&
        (gIssuSystemStatusInfo.u4IssuCommandStatus == ISSU_CMD_IN_PROGRESS))
    {
        *pu4ErrorCode =  SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_MM_CMD_IN_PROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuLoadSWPath
 Input       :  The Indices

                The Object 
                testValFsIssuLoadSWPath
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuLoadSWPath (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsIssuLoadSWPath)
{
    /* Return failure when Load Software Path maximum size exceeded */
    if (pTestValFsIssuLoadSWPath->i4_Length >= ISSU_PATH_MAX_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_LOAD_SW_PATH_MAX_SIZE_EXCEEDED);
        return SNMP_FAILURE;
    }

    /* Fail if the particular file does not exists in the path */
	if (IssuCheckForValidFilePath (pTestValFsIssuLoadSWPath->pu1_OctetList) 
			!= ISSU_SUCCESS)
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		return SNMP_FAILURE;
	}

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuSoftwareCompatFilePath
 Input       :  The Indices

                The Object 
                testValFsIssuSoftwareCompatFilePath
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuSoftwareCompatFilePath (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsIssuSoftwareCompatFilePath)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pTestValFsIssuSoftwareCompatFilePath);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuSoftwareCompatCheckInit
 Input       :  The Indices

                The Object 
                testValFsIssuSoftwareCompatCheckInit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuSoftwareCompatCheckInit (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsIssuSoftwareCompatCheckInit)
{
    if ((i4TestValFsIssuSoftwareCompatCheckInit == ISSU_COMP_CHECK_ENABLE) &&
        (gIssuSystemInfo.au1IssuSWCompatVersion[0] == '\0'))
    {    
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIssuSoftwareCompatCheckInit != ISSU_COMP_CHECK_ENABLE) &&
        (i4TestValFsIssuSoftwareCompatCheckInit != ISSU_COMP_CHECK_DISABLE))
    {    
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuMode
 Input       :  The Indices

                The Object 
                testValFsIssuMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsIssuMode)
{
    /* Block Issu mode configuration in standby node */
    if ((RmGetNodeState () == RM_STANDBY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_NODE_BLOCKED);
        return SNMP_FAILURE;
    }
 
    /* Prevent setting of Issu mode during maintenance mode */
    if (gIssuSystemInfo.u1IssuMaintenanceMode == 
            ISSU_MAINTENANCE_MODE_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_MODE_SET_NOT_ALLOWED);
        return SNMP_FAILURE;
    }
   
	 /* Check for Base compatible Mode */
    if (i4TestValFsIssuMode == ISSU_BASE_COMPATIBLE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_BASECOMPATIBLE_MODE_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }
 
    /* Check values for Issu mode */
    if ((i4TestValFsIssuMode != ISSU_FULL_COMPATIBLE_MODE) &&
        (i4TestValFsIssuMode != ISSU_BASE_COMPATIBLE_MODE) &&
        (i4TestValFsIssuMode != ISSU_INCOMPATIBLE_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuCommand
 Input       :  The Indices

                The Object 
                testValFsIssuCommand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuCommand (UINT4 *pu4ErrorCode, INT4 i4TestValFsIssuCommand)
{

#if defined MSTP_WANTED || PVRST_WANTED || RSTP_WANTED
    UINT4        u4ContextId = 0;
#endif

    /* Check for ISSU command values */
    if ((i4TestValFsIssuCommand != ISSU_CMD_LOAD_VERSION) &&
        (i4TestValFsIssuCommand != ISSU_CMD_FORCE_STANDBY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if node is not in maintenence mode */
    if (gIssuSystemStatusInfo.u1IssuMaintenanceOperStatus == 
            ISSU_MAINTENANCE_OPER_DISABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_MAINTENANCE_MODE_DISABLED);
        return SNMP_FAILURE;
    }

    /* Check if command already in progress then, do not accept it */
    if (i4TestValFsIssuCommand == (INT4)gIssuSystemInfo.u4IssuCommand)
    {
        if (ISSU_CMD_IN_PROGRESS == gIssuSystemStatusInfo.u4IssuCommandStatus)
        {
            *pu4ErrorCode =  SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ISSU_MM_CMD_IN_PROGRESS);
            return SNMP_FAILURE;
        }
    }    
    /* Check if Force StandBy is given in already StandBy node*/
    /* Check if Force StandBy is given in already StandBy node
     * or in in-compatible mode */
    if (i4TestValFsIssuCommand == ISSU_CMD_FORCE_STANDBY)
    {
        if (RmGetNodeState () == RM_STANDBY)
	{
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ISSU_NODE_IN_STANDBY_STATE);
            return SNMP_FAILURE;
	}
        if(IssuGetIssuMode() != ISSU_INCOMPATIBLE_MODE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ISSU_NOT_INCOMPATIBLE_MODE);
            return SNMP_FAILURE;
        }
    }
 
    /* Check if Load version is given in Active node;
     * Load version is not allowed in Active node */
    if ((RmGetNodeState () == RM_ACTIVE) &&
        (i4TestValFsIssuCommand == ISSU_CMD_LOAD_VERSION))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISSU_NODE_IN_ACTIVE_STATE);
        return SNMP_FAILURE;
    }

    /* If Load version command is given and the node is
     * in full-compatible mode check for the status of modules
     * RSTP, MSTP and PVRST */
#if defined MSTP_WANTED || PVRST_WANTED || RSTP_WANTED
    if ((i4TestValFsIssuCommand == ISSU_CMD_LOAD_VERSION) &&
        (IssuGetIssuMode() == ISSU_FULL_COMPATIBLE_MODE))
    {
        if ((AstIsRstEnabledInContext (u4ContextId) ||
             AstIsPvrstEnabledInContext (u4ContextId)||
             AstIsMstEnabledInContext (u4ContextId)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ISSU_MSTP_RSTP_PVRST_ENABLED);
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuTraceOption
 Input       :  The Indices

                The Object 
                testValFsIssuTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuTraceOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsIssuTraceOption)
{
	if ((i4TestValFsIssuTraceOption < 0) || 
			(i4TestValFsIssuTraceOption > ISSU_ALL_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuTrapStatus
 Input       :  The Indices

                The Object 
                testValFsIssuTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIssuTrapStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsIssuTrapStatus)
{
    if ((i4TestValFsIssuTrapStatus != ISSU_TRAP_ENABLE) &&
        (i4TestValFsIssuTrapStatus != ISSU_TRAP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIssuSoftwareCompatForVersion
 Input       :  The Indices

                The Object
                testValFsIssuSoftwareCompatForVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsIssuSoftwareCompatForVersion(UINT4 *pu4ErrorCode , 
				tSNMP_OCTET_STRING_TYPE *pTestValFsIssuSoftwareCompatForVersion)
{
	UNUSED_PARAM (pu4ErrorCode);
	UNUSED_PARAM (pTestValFsIssuSoftwareCompatForVersion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuMaintenanceMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuMaintenanceMode (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuLoadSWPath
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuLoadSWPath (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuSoftwareCompatFilePath
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuSoftwareCompatFilePath (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuSoftwareCompatCheckInit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuSoftwareCompatCheckInit (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuCommand
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuCommand (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIssuTrapStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIssuTrapStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsIssuSoftwareCompatForVersion
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsIssuSoftwareCompatForVersion(UINT4 *pu4ErrorCode, 
					tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

#endif
