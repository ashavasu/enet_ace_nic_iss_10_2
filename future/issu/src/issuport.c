/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: issuport.c,v 1.9 2016/08/09 09:41:27 siva Exp $
 *
 * Description: This file contains the routines of ISSU modules
 *              which need to be ported when moving to a
 *              different target or while integrating with
 *              other products
 *
 ********************************************************************/

#ifndef __ISSUPORT_C__
#define __ISSUPORT_C__

#include "issuinc.h"

/****************************************************************************
 *                                                                          *
 * Function     :  IssuGetCurrentWorkingDir                                 *
 *                                                                          *
 * Description  : This function will return the current working directory   *
 *                 path                                                     *
 *                                                                          *
 * Input        : pu1Buf - Buffer to hold current working directory path    *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      :  ISSU_SUCCESS/ISSU_FAILURE                                *
 *                                                                          *
 ****************************************************************************/
INT4 
IssuGetCurrentWorkingDir(CHR1 *pu1Buf)
{
    if (getcwd(pu1Buf,ISSU_PATH_MAX_SIZE) == NULL)
    {
        return ISSU_FAILURE;
    }

    STRCAT(pu1Buf,"/");

    return ISSU_SUCCESS;
}

#ifdef NPAPI_WANTED

/****************************************************************************
 *                                                                          *
 * Function     : IssuCreateInitScript                                      *
 *                                                                          *
 * Description  : This function will create Init Script for load version    *
 *                software upgrade                                          *
 *                                                                          *
 * Input        : pIssuHwStatusInfo - Hw Status info                        *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : ISSU_SUCCESS/ISSU_FAILURE                                 *
 *                                                                          *
 ****************************************************************************/
INT4
IssuCreateInitScript (tIssuHwStatusInfo *pIssuHwStatusInfo)
{
    FILE               *fp = NULL;
    CHR1                au1Cwd[ISSU_PATH_MAX_SIZE +1];
    CHR1                au1NewLoadPath[ISSU_PATH_MAX_SIZE +1];
    UINT4               u4Len;
    UINT4               u4Temp;
    CHR1               *pFiles[ISSU_MAX_FILES]={"nodeid"};
    CHR1               *pFileAccess[ISSU_MAX_FILES]={"0751"};
    
    MEMSET (au1Cwd, 0, sizeof (au1Cwd));
    MEMSET (au1NewLoadPath, 0, sizeof (au1NewLoadPath));
    
    u4Len = STRLEN(pIssuHwStatusInfo->au1IssuLoadSWPath);
    for (u4Temp = u4Len-1;u4Temp; u4Temp--)
    {
        if((pIssuHwStatusInfo->au1IssuLoadSWPath)[u4Temp] == '/')
        {
            break;
        }
    }

    STRNCPY(au1NewLoadPath, pIssuHwStatusInfo->au1IssuLoadSWPath,u4Temp);
    au1NewLoadPath[u4Temp]='\0';

    /* opens init_script file for next auto reload */
    fp = FOPEN(ISSU_INIT_SCRIPT, "w+");
    if (fp == NULL)
    {
        ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |
                ISSU_CRITICAL_TRC,"Failed to open init_script file!"
                       "due to auto launch script path may not present!!)\r\n");
        return ISSU_FAILURE;
    }
    else
    {
       if (IssuGetCurrentWorkingDir (au1Cwd) == ISSU_FAILURE)
       {
           ISSU_TRC (ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |
                ISSU_CRITICAL_TRC,"Failed to get current directory path!\r\n");
           FCLOSE (fp);
           return ISSU_FAILURE;
       }

       u4Len = STRLEN(au1Cwd);
       fprintf (fp, "cd %s\n", au1NewLoadPath);

       for (u4Temp = 0; u4Temp < ISSU_MAX_FILES; u4Temp++)
       {
           STRNCPY (au1Cwd+u4Len, pFiles[u4Temp], ISSU_PATH_MAX_SIZE-u4Len);
           fprintf (fp,"cp -f %s %s \nchmod %s %s\n",au1Cwd,au1NewLoadPath,
                    pFileAccess[u4Temp],pFiles[u4Temp]);
       }
       fprintf (fp,"export SOC_BOOT_FLAGS=%s\n%s\n",
                pIssuHwStatusInfo-> u4IssuMode
                == ISSU_FULL_COMPATIBLE_MODE ?
                ISSU_WARMBOOT_FLAG : ISSU_COLDBOOT_FLAG,
                pIssuHwStatusInfo->au1IssuLoadSWPath);
       FCLOSE (fp);
    }
    return ISSU_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 * Function     : IssuDeleteInitScript                                      *
 *                                                                          *
 * Description  : This function will remove the Init Script                 *
 *                                                                          *
 * Input        : None                                                      *
 *                                                                          *
 * Output       : None                                                      *
 *                                                                          *
 * Returns      : None                                                      *
 *                                                                          *
 ****************************************************************************/
VOID
IssuDeleteInitScript (VOID)
{
    FILE                 *fp = NULL;

    fp = FOPEN(ISSU_INIT_SCRIPT,"w+");
    /* Remove the Init script in File to avoid auto reload */
    if (fp == NULL)
    {
        ISSU_TRC(ISSU_ALL_FAILURE_TRC |ISSU_CONTROL_PATH_TRC |
                ISSU_CRITICAL_TRC,"Failed to remove InitScript file\r\n");
    }
    else
    {
        FCLOSE(fp);
    }

    return;
}
#endif /*NPAPI_WANTED*/
#endif
