/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *  $Id: issuglob.h,v 1.4 2016/02/26 11:14:07 siva Exp $
 * 
 *  Description: This file contains the global variables for ISSU
 *  module
*******************************************************************/

#ifndef __ISSUGLOB_H__
#define __ISSUGLOB_H__

#ifdef __ISSUMAIN_C__
tIssuSystemInfo gIssuSystemInfo;
tIssuSystemStatusInfo gIssuSystemStatusInfo;

/* Global variable which contains all the allowed commands during ISSU maintenance mode */
CONST CHR1 *gIssuAllowedCmdList [ISSU_MAX_CMDS_ALLOWED] = {
   "clear screen",
   "debug issu ([init-shut] [mgmt] [fail] [ctrl] [critical] [all])",
   "end",
   "exit",
   "issu compatibility-check initiate {<proposed_sw_version>}",
   "issu compatibility-matrix-path <matrix_file_path>",
   "issu maintenance-mode",
   "issu trap {enable | disable}",
   "issu {loadversion [<new_image_path>] | force-standby}",
   "no debug issu ([init-shut] [mgmt] [fail] [ctrl] [critical] [all])",
   "no issu maintenance-mode",
   "redundancy force-switchover",
   "show interfaces description",
   "show issu compatibility-check { matrix-file | status }",
   "show issu maintenance-mode [commands]",
   "show issu state [detail]",
   "snmpwalk mib {name | oid} <value> [count <integer(1-100)>] [short]",
   NULL
};

/* Global variable which contains all the allowed OID's during ISSU maintenance mode */
tFilterStringList gIssuAllowedOidList [ISSU_MAX_OIDS_ALLOWED] = {
            {{1,3,6,1,4,1,29601,2,103},9}, /* issu object oids*/
            {{1,3,6,1,4,1,2076,99,1,8},10}, /* redundancy switch over flag oid */
            {{0},0}
};

tFilterStringList gIssuStbyAllowedOidList [ISSU_MAX_OIDS_ALLOWED] = {
 {{1,3,6,1,4,1,2076,81,1,15},10}, /* IssConfigRestoreOption */
 {{1,3,6,1,4,1,2076,81,1,20},10}, /* IssDlImageFromIp */
 {{1,3,6,1,4,1,2076,81,1,21},10}, /* IssDlImageName */
 {{1,3,6,1,4,1,2076,81,1,22},10}, /* IssInitiateDlImage */
 {{1,3,6,1,4,1,2076,81,1,24},10}, /* IssUploadLogFileToIp */
 {{1,3,6,1,4,1,2076,81,1,55},10}, /* IssDownLoadTransferMode */
 {{1,3,6,1,4,1,2076,81,1,56},10}, /* IssDownLoadUserName */
 {{1,3,6,1,4,1,2076,81,1,57},10}, /* IssDownLoadPassword */
 {{1,3,6,1,4,1,2076,81,1,58},10}, /* IssUploadLogTransferMode */
 {{1,3,6,1,4,1,2076,81,1,59},10}, /* IssUploadLogUserName */
 {{1,3,6,1,4,1,2076,81,1,60},10}, /* IssUploadLogPasswd */
 {{1,3,6,1,4,1,2076,81,1,83},10}, /* IssDlImageFromIpAddrType */
 {{1,3,6,1,4,1,2076,81,1,84},10}, /* IssDlImageFromIpvx */
 {{1,3,6,1,4,1,2076,81,1,85},10}, /* IssUploadLogFileToIpAddrType */
 {{1,3,6,1,4,1,2076,81,1,86},10}, /* IssUploadLogFileToIpvx */
 {{1,3,6,1,4,1,2076,81,1,25},10}, /* IssLogFileName */
 {{1,3,6,1,4,1,2076,81,1,26},10}, /* IssInitiateUlLogFile */
 {{1,3,6,1,4,1,2076,81,1,113},10}, /* IssUlRemoteLogFileName */
 {{1,3,6,1,4,1,2076,99,1,23},10}, /* FsRmCopyPeerSyLogFile */
 {{0},0}
};

/* Global variable which contains all the restricted commands possible in USEREXEC mode 
 * during ISSU maintenance mode */
CONST CHR1 *gIssuRestrictedCmdList [ISSU_MAX_CMDS_RESTRICTED] = {
    "clear interfaces [<interface-type>] [<interface-id>] counters",
    "ip igmp snooping clear counters [vlan <vlanid/vfi_id>]",  
    "clear application-priority counters [interface <ifXtype> <ifnum>]", 
    "ping vrf <vrf-name> [ip] {<IpAddress>|<dns_host_name>} [data <0-65535>] [df-bit] [{repeat|count} <packet_count(1-10)>] [size <packet_size(36-2080)>] [source <ip-address>] [timeout <time_out(1-100)>] [validate]",
    "clear priority-grouping counters [interface <ifXtype> <ifnum>] ", 
    "clear priority-flow-control counters [interface <ifXtype> <ifnum>]", 
    "ping [ip] {<IpAddress>|<dns_host_name>} [data <0-65535>] [df-bit] [{repeat|count} <packet_count(1-10)>] [size <packet_size(36-2080)>] [source <ip-address>] [timeout <time_out(1-100)>] [validate]",
    NULL
}; 
UINT1 gu1IsLoadVersionTriggered = OSIX_FALSE;    
#else
extern CONST CHR1 *gIssuAllowedCmdList [ISSU_MAX_CMDS_ALLOWED];
extern tIssuSystemInfo gIssuSystemInfo;
extern tIssuSystemStatusInfo gIssuSystemStatusInfo;
extern tFilterStringList gIssuAllowedOidList [ISSU_MAX_OIDS_ALLOWED];
extern tFilterStringList gIssuStbyAllowedOidList [ISSU_MAX_OIDS_ALLOWED];
extern CONST CHR1 *gIssuRestrictedCmdList [ISSU_MAX_CMDS_ALLOWED];
extern UINT1 gu1IsLoadVersionTriggered;
#endif

#endif /*end of __ISSUGLOB_H__*/
