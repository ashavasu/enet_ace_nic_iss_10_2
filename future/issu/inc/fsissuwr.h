/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissuwr.h,v 1.2 2015/11/20 10:40:22 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/

#ifndef _FSISSUWR_H
#define _FSISSUWR_H

VOID RegisterFSISSU(VOID);

VOID UnRegisterFSISSU(VOID);
INT4 FsIssuMaintenanceModeGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuMaintenanceOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuLoadSWPathGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuRollbackSWPathGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuCurrentSWPathGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatFilePathGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatCheckInitGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatCheckStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuModeGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuCommandGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuCommandStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuProcedureStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuRollbackSoftwareVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuTrapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuLastUpgradeTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatForVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsIssuMaintenanceModeSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuLoadSWPathSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatFilePathSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatCheckInitSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuModeSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuCommandSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuTrapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatForVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsIssuMaintenanceModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuLoadSWPathTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatFilePathTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatCheckInitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuCommandTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuTrapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuSoftwareCompatForVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIssuMaintenanceModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuLoadSWPathDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuSoftwareCompatFilePathDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuSoftwareCompatCheckInitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuCommandDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuTrapStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIssuSoftwareCompatForVersionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSISSUWR_H */
