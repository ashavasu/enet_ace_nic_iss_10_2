/********************************************************************
 Copyright (C) 2006 Aricent Inc . All Rights Reserved

 $Id: issumacr.h,v 1.3 2016/08/09 09:41:27 siva Exp $

 Description: ISSU related macros and constants
********************************************************************/

#ifndef __ISSUMACR_H__
#define __ISSUMACR_H__

/*Semaphore related constants*/
#define ISSU_PROTO_SEM                  ((UINT1 *)"ISUS")
#define ISSU_BINARY_SEM                 1
#define ISSU_SEM_FLAGS                  0 /* unused */

#define   ISSU_CREATE_SEMAPHORE(au1Name, count, flags, psemid) \
             OsixCreateSem((au1Name), (count), (flags), (psemid))

#define   ISSU_DELETE_SEMAPHORE(semid) OsixSemDel(semid)

/*Constants*/
#define ISSU_DEFAULT_FILE_PATH          FLASH
#define ISSU_FILE_NAME                  "compatibility_matrix.txt"
#define ISSU_FILE_NAME_LEN              100
#define FILE_BYTES_READ                 500
#define ISSU_STR_LEN                    50
#define ISSU_MAX_CMDS_ALLOWED           50
#define ISSU_MAX_OIDS_ALLOWED           50
#define ISSU_MAX_CMDS_RESTRICTED        50

/*Trap related constants*/
#define ISSU_TRAP_OID_LEN               11

#define ISSU_TRAP_MAINTENANCE_STATUS    1
#define ISSU_TRAP_COMMAND_STATUS        2
#define ISSU_TRAP_PROCEDURE_STATUS      3

#define ISSU_NUM_TEN                    10
#define ISSU_MAX_HEX_SINGLE_DIGIT       0xf
#define ISSU_BUFFER_SIZE                256
#define ISSU_SNMP_TEMP_BUFF             280

#endif /*__ISSUMACR_H__*/
