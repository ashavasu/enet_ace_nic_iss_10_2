/********************************************************************
* Copyright (C) 2008 Aricent Inc . All Rights Reserved
*
* $Id: issuinc.h,v 1.4 2016/03/10 13:13:32 siva Exp $
*
* Description: Contains ISSU related header files to be included
*********************************************************************/

#ifndef _ISSUINC_H
#define _ISSUINC_H

#include "lr.h"
#include "iss.h"
#include "cli.h"
#include "issu.h"
#include "hb.h"
#include "fssnmp.h"
#include "mstp.h"
#include "rstp.h"
#include "pvrst.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "issunp.h"
#endif

#include "issucli.h"
#include "issutdfs.h"
#include "issuglob.h"
#include "issuprot.h"
#include "fsissulw.h"
#include "issumacr.h"
#include "issutrc.h"
#include "issunpwr.h"
#include "fsissuwr.h"
#include "snmpcmn.h"
#endif /* _ISSUINC_H */
