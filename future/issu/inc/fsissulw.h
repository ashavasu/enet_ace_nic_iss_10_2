/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissulw.h,v 1.2 2015/11/20 10:40:22 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIssuMaintenanceMode ARG_LIST((INT4 *));

INT1
nmhGetFsIssuMaintenanceOperStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIssuLoadSWPath ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIssuRollbackSWPath ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIssuCurrentSWPath ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIssuSoftwareCompatFilePath ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIssuSoftwareCompatCheckInit ARG_LIST((INT4 *));

INT1
nmhGetFsIssuSoftwareCompatCheckStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIssuMode ARG_LIST((INT4 *));

INT1
nmhGetFsIssuCommand ARG_LIST((INT4 *));

INT1
nmhGetFsIssuCommandStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIssuProcedureStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIssuRollbackSoftwareVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIssuTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsIssuTrapStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIssuLastUpgradeTime ARG_LIST((UINT4 *));

INT1
nmhGetFsIssuSoftwareCompatForVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIssuMaintenanceMode ARG_LIST((INT4 ));

INT1
nmhSetFsIssuLoadSWPath ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIssuSoftwareCompatFilePath ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIssuSoftwareCompatCheckInit ARG_LIST((INT4 ));

INT1
nmhSetFsIssuMode ARG_LIST((INT4 ));

INT1
nmhSetFsIssuCommand ARG_LIST((INT4 ));

INT1
nmhSetFsIssuTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsIssuTrapStatus ARG_LIST((INT4 ));

INT1
nmhSetFsIssuSoftwareCompatForVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIssuMaintenanceMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIssuLoadSWPath ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIssuSoftwareCompatFilePath ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIssuSoftwareCompatCheckInit ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIssuMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIssuCommand ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIssuTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIssuTrapStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIssuSoftwareCompatForVersion ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIssuMaintenanceMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuLoadSWPath ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuSoftwareCompatFilePath ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuSoftwareCompatCheckInit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuCommand ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuTrapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIssuSoftwareCompatForVersion ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
