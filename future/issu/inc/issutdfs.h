/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: issutdfs.h,v 1.3 2016/08/09 09:41:27 siva Exp $
*
* Description: This file contains the data structures for ISSU module
*******************************************************************/
#ifndef __ISSUTDFS_H__
#define __ISSUTDFS_H__

#include "issumacr.h"

typedef struct {
    /* Semaphore ID for User Protocol Module */
    tOsixSemId   IssuProtocolSemId ;  
    /* Specifies the ISSU mode of upgrade process ,non-disruptive or disruptive */
    UINT4    u4IssuMode;
    /* Specifies the ISSU command i.e loadversion/forceactive/forcestandby */
    UINT4    u4IssuCommand;
    /*  Enables tracing in the selected ISSU module */
    UINT4    u4IssuTraceOption;
    /* To place the ISSU system in maintenance mode or exit from maintenance mode */
    UINT1         u1IssuMaintenanceMode;
    /* Enables or disables the trap for ISSU */ 
    UINT1         u1IssuTrapStatus;
    /*Specifies the path of the new software that can be loaded to the system */
    UINT1    au1IssuLoadSWPath[ISSU_PATH_MAX_SIZE+1];
    /* Path of the compatibility matrix file */
    UINT1  au1IssuSWCompatFilePath[ISSU_PATH_MAX_SIZE+1];
    /* Path of the compatibility Software Version */
    UINT1  au1IssuSWCompatVersion[ISSU_IMAGE_NAME_MAX_SIZE+1];
    UINT1  au1Pad[3];
}tIssuSystemInfo;

typedef struct {
    
    UINT4   u4IssuCommandStatus;/* Status of ISSU command */
    UINT4   u4IssuProcedureStatus;  /* Overall status of  ISSU procedure*/
    /* Status of software compatibility check initiated */
    UINT4   u4IssuSWCompatibilityCheckStatus;
    /* It holds successful upgrade/downgrade timestamp of last ISSU process  */
    UINT4   u4LastUpgradeTime; 
    /* The version of the rollback software When ISSU procedure has failed */
    UINT1 au1IssuRollbackSoftwareVersion[ISSU_IMAGE_NAME_MAX_SIZE+1];
    /* Path from which software will be loaded if ISSU fails */
    UINT1   au1IssuRollbackSWPath[ISSU_PATH_MAX_SIZE+1];
    /* Specifies the path of the current running software */
    UINT1 au1IssuCurrentSWPath[ISSU_PATH_MAX_SIZE+1];
    /* Status of configured Maintenancemode */
    UINT1     u1IssuMaintenanceOperStatus;
}tIssuSystemStatusInfo;

/* Data structure for trap related configurations */
typedef struct {
    UINT4       u4IssuCommand; 
    UINT4       u4IssuCommandStatus; /* Status of ISSU command */
    UINT4       u4IssuProcedureStatus; /* Overall status of  ISSU procedure*/
    UINT1       u1IssuMaintenanceMode; 
    UINT1       u1IssuMaintenanceOperStatus; 
    UINT1  au1Pad[2];
}tIssuTrapInfo;

/* Data structure for filtering OID's allowed inside maintenance mode */
typedef struct {
    UINT4       u4OidString[MAX_OID_LEN];
    UINT4     u4Length;
}tFilterStringList;

#endif /* end of __ISSUTDFS_H__ */
