/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issutrc.h,v 1.1.1.1 2015/09/14 12:06:44 siva Exp $
 *
 * Description:This file contains procedures and definitions
 * 			   used for debugging.
 ********************************************************************/

#ifndef __ISSUTRC_H__
#define __ISSUTRC_H__

/* Trace and debug flags */
#define  ISSU_TRC_FLAG 					gIssuSystemInfo.u4IssuTraceOption

/* Module names */
#define   ISSU_MOD_NAME           		((const char *)"ISSU")

/* Trace definitions */
#ifdef  TRACE_WANTED

#define ISSU_TRC(TraceType, Str)                                              \
        MOD_TRC(ISSU_TRC_FLAG, TraceType, ISSU_MOD_NAME, (const char *)Str)

#define ISSU_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(ISSU_TRC_FLAG, TraceType, ISSU_MOD_NAME, (const char *)Str, Arg1)

#define ISSU_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(ISSU_TRC_FLAG, TraceType, ISSU_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define ISSU_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(ISSU_TRC_FLAG, TraceType, ISSU_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)

#define ISSU_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)                       \
        MOD_TRC_ARG4(ISSU_TRC_FLAG, TraceType, ISSU_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4)

#define ISSU_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)                       \
        MOD_TRC_ARG5(ISSU_TRC_FLAG, TraceType, ISSU_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5)

#define ISSU_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)                       \
        MOD_TRC_ARG6(ISSU_TRC_FLAG, TraceType, ISSU_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)


#define   ISSU_DEBUG(x) {x}

#else  /* TRACE_WANTED */

#define ISSU_TRC(TraceType, Str)
#define ISSU_TRC_ARG1(TraceType, Str, Arg1)
#define ISSU_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define ISSU_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define ISSU_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3,Arg4)
#define ISSU_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5)
#define ISSU_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3,Arg4,Arg5,Arg6)

#define   ISSU_DEBUG(x)

#endif /* TRACE_WANTED */

#endif /*__ISSUTRC_H__*/
