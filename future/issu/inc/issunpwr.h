/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: issunpwr.h,v 1.1.1.1 2015/09/14 12:06:44 siva Exp $ 
*
* Description: Protocol Mib Data base
*********************************************************************/

#ifndef __ISSUNPWR_H__
#define __ISSUNPWR_H__
UINT1 IssuCfaNpGetLinkStatus PROTO ((UINT4));

#endif /*__ISSUMACS_H__*/
