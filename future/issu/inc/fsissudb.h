/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissudb.h,v 1.2 2015/11/20 10:40:22 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSUDB_H
#define _FSISSUDB_H


UINT4 fsissu [] ={1,3,6,1,4,1,29601,2,103};
tSNMP_OID_TYPE fsissuOID = {9, fsissu};


UINT4 FsIssuMaintenanceMode [ ] ={1,3,6,1,4,1,29601,2,103,1,1};
UINT4 FsIssuMaintenanceOperStatus [ ] ={1,3,6,1,4,1,29601,2,103,1,2};
UINT4 FsIssuLoadSWPath [ ] ={1,3,6,1,4,1,29601,2,103,1,3};
UINT4 FsIssuRollbackSWPath [ ] ={1,3,6,1,4,1,29601,2,103,1,4};
UINT4 FsIssuCurrentSWPath [ ] ={1,3,6,1,4,1,29601,2,103,1,5};
UINT4 FsIssuSoftwareCompatFilePath [ ] ={1,3,6,1,4,1,29601,2,103,1,6};
UINT4 FsIssuSoftwareCompatCheckInit [ ] ={1,3,6,1,4,1,29601,2,103,1,7};
UINT4 FsIssuSoftwareCompatCheckStatus [ ] ={1,3,6,1,4,1,29601,2,103,1,8};
UINT4 FsIssuMode [ ] ={1,3,6,1,4,1,29601,2,103,1,9};
UINT4 FsIssuCommand [ ] ={1,3,6,1,4,1,29601,2,103,1,10};
UINT4 FsIssuCommandStatus [ ] ={1,3,6,1,4,1,29601,2,103,1,11};
UINT4 FsIssuProcedureStatus [ ] ={1,3,6,1,4,1,29601,2,103,1,12};
UINT4 FsIssuRollbackSoftwareVersion [ ] ={1,3,6,1,4,1,29601,2,103,1,13};
UINT4 FsIssuTraceOption [ ] ={1,3,6,1,4,1,29601,2,103,1,14};
UINT4 FsIssuTrapStatus [ ] ={1,3,6,1,4,1,29601,2,103,1,15};
UINT4 FsIssuLastUpgradeTime [ ] ={1,3,6,1,4,1,29601,2,103,1,16};
UINT4 FsIssuSoftwareCompatForVersion [ ] ={1,3,6,1,4,1,29601,2,103,1,17};




tMbDbEntry fsissuMibEntry[]= {

{{11,FsIssuMaintenanceMode}, NULL, FsIssuMaintenanceModeGet, FsIssuMaintenanceModeSet, FsIssuMaintenanceModeTest, FsIssuMaintenanceModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsIssuMaintenanceOperStatus}, NULL, FsIssuMaintenanceOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIssuLoadSWPath}, NULL, FsIssuLoadSWPathGet, FsIssuLoadSWPathSet, FsIssuLoadSWPathTest, FsIssuLoadSWPathDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsIssuRollbackSWPath}, NULL, FsIssuRollbackSWPathGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIssuCurrentSWPath}, NULL, FsIssuCurrentSWPathGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIssuSoftwareCompatFilePath}, NULL, FsIssuSoftwareCompatFilePathGet, FsIssuSoftwareCompatFilePathSet, FsIssuSoftwareCompatFilePathTest, FsIssuSoftwareCompatFilePathDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsIssuSoftwareCompatCheckInit}, NULL, FsIssuSoftwareCompatCheckInitGet, FsIssuSoftwareCompatCheckInitSet, FsIssuSoftwareCompatCheckInitTest, FsIssuSoftwareCompatCheckInitDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsIssuSoftwareCompatCheckStatus}, NULL, FsIssuSoftwareCompatCheckStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsIssuMode}, NULL, FsIssuModeGet, FsIssuModeSet, FsIssuModeTest, FsIssuModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsIssuCommand}, NULL, FsIssuCommandGet, FsIssuCommandSet, FsIssuCommandTest, FsIssuCommandDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsIssuCommandStatus}, NULL, FsIssuCommandStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsIssuProcedureStatus}, NULL, FsIssuProcedureStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsIssuRollbackSoftwareVersion}, NULL, FsIssuRollbackSoftwareVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIssuTraceOption}, NULL, FsIssuTraceOptionGet, FsIssuTraceOptionSet, FsIssuTraceOptionTest, FsIssuTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsIssuTrapStatus}, NULL, FsIssuTrapStatusGet, FsIssuTrapStatusSet, FsIssuTrapStatusTest, FsIssuTrapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsIssuLastUpgradeTime}, NULL, FsIssuLastUpgradeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIssuSoftwareCompatForVersion}, NULL, FsIssuSoftwareCompatForVersionGet, FsIssuSoftwareCompatForVersionSet, FsIssuSoftwareCompatForVersionTest, FsIssuSoftwareCompatForVersionDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsissuEntry = { 17, fsissuMibEntry };

#endif /* _FSISSUDB_H */

