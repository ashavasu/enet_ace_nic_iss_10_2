/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: issuprot.h,v 1.7 2016/02/26 11:14:07 siva Exp $
*
* Description: Contains ISSU related function prototypes.
*********************************************************************/

#ifndef _ISSUPROT_H
#define _ISSUPROT_H

INT4
IssuMainTaskInit PROTO ((VOID));
VOID
IssuMainTaskDeInit PROTO ((VOID));

INT4  IssuHandleCheckCompatibility PROTO ((INT4 *, UINT1 *));
INT4  IssuGetCompatFileCheckStatus PROTO ((INT4 *, UINT1 *));
VOID  IssuGetCurrentSoftwareVersion PROTO ((tSNMP_OCTET_STRING_TYPE *));
UINT4 IssuMaintenanceModeEnable PROTO((VOID));
UINT4 IssuMaintenanceModeDisable PROTO((VOID));
UINT4 IssuHandleLoadVersion PROTO ((VOID));
VOID  IssuHandleForceStandby PROTO ((VOID));
VOID  IssuSnmpifSendTrap PROTO ((UINT1 ,VOID *));
INT4  IssuUtilParseSubIdNew PROTO ((UINT1 **));
VOID  IssuSetProcedureStatus PROTO ((UINT4));
VOID  IssuSetCommandStatus PROTO ((UINT4, UINT4));
tSNMP_OID_TYPE  *
IssuMakeObjIdFromDotNew PROTO ((INT1 *));
VOID  IssuPrintUpgradeTime PROTO ((UINT4, UINT1 *));
INT4 IssuGetCurrentWorkingDir PROTO ((CHR1 *));
INT4 IssuCheckForValidFilePath PROTO ((UINT1 *));
VOID IssuSetLoadversionTriggered PROTO ((UINT1 u1Value)); 
#ifdef NPAPI_WANTED
INT4 IssuCreateInitScript PROTO ((tIssuHwStatusInfo *));
VOID IssuDeleteInitScript PROTO ((VOID));
#endif

#endif
