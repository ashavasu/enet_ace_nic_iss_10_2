#!/bin/csh
# (C) 2010 Future Software Pvt. Ltd.
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   $Id: make.h,v 1.2 2016/02/08 10:31:51 siva Exp $                     |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :Aricent          | 
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 22 Oct 2010                                   |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

ISSU_COMPILATION_SWITCHES = -DISSU_TRACE_WANTED $(ISSU_EXP_FLAGS)

ISSU_FLAGS =$(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)\
            $(ISSU_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

ISSU = ${BASE_DIR}/issu
ISSUSRC=$(ISSU)/src/
ISSUOBJ=$(ISSU)/obj/
ISSUINC=$(ISSU)/inc/
ISSUINCL= \
    -I$(BASE_DIR)/issu\
    -I$(BASE_DIR)/issu/inc\
    -I$(BASE_DIR)/inc/cli\
    -I$(BASE_DIR)/inc/npapi\
    -I$(BASE_DIR)/inc\
    -I$(BASE_DIR)/util/mpls\
    -I$(BASE_DIR)/issu/src\
    -I${BASE_DIR}/snmpv3

ISSU_TEST_BASE_DIR  = ${BASE_DIR}/issu/test
ISSU_TEST_SRC_DIR   = ${ISSU_TEST_BASE_DIR}/src
ISSU_TEST_INC_DIR   = ${ISSU_TEST_BASE_DIR}/inc
ISSU_TEST_OBJ_DIR   = ${ISSU_TEST_BASE_DIR}/obj

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

ISSU_INCLUDES=$(COMMON_INCLUDE_DIRS) $(ISSUINCL)

#############################################################################
