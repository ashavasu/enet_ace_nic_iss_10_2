/* $Id: igmpsz.h,v 1.11 2016/06/24 09:42:19 siva Exp $*/
enum {
    MAX_IGMP_ADDR_ARRAY_STR_SIZING_ID,
    MAX_IGMP_GROUPLIST_NODES_SIZING_ID,
    MAX_IGMP_LOGICAL_IFACES_SIZING_ID,
    MAX_IGMP_MCAST_GRPS_SIZING_ID,
    MAX_IGMP_MCAST_SCHED_QUERIES_SIZING_ID,
    MAX_IGMP_MCAST_SRCS_SIZING_ID,
    MAX_IGMP_MCAST_SRS_REPORTERS_SIZING_ID,
    MAX_IGMP_Q_DEPTH_SIZING_ID,
    MAX_IGMP_QUERIES_SIZING_ID,
    MAX_IGMP_RECVBUF_BLKS_SIZING_ID,
    MAX_IGMP_RM_QUE_DEPTH_SIZING_ID,
    MAX_IGMP_SCHED_QUERY_SRCS_SIZING_ID,
    MAX_IGMP_SRCS_FOR_MRP_PER_GROUP_SIZING_ID,
    MAX_SRCS_TO_PKT_BLKS_U1_SIZING_ID,
    MAX_SRCS_TO_PKT_BLKS_U4_SIZING_ID,
    MAX_IGMP_HOST_MCAST_GROUP_SIZING_ID,
    MAX_IGMP_SSM_MAPPINGS_SIZING_ID,
    IGMP_MAX_SIZING_ID
};


#ifdef  _IGMPSZ_C
tMemPoolId IGMPMemPoolIds[ IGMP_MAX_SIZING_ID];
INT4  IgmpSizingMemCreateMemPools(VOID);
VOID  IgmpSizingMemDeleteMemPools(VOID);
INT4  IgmpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _IGMPSZ_C  */
extern tMemPoolId IGMPMemPoolIds[ ];
extern INT4  IgmpSizingMemCreateMemPools(VOID);
extern VOID  IgmpSizingMemDeleteMemPools(VOID);
extern INT4  IgmpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif /*  _IGMPSZ_C  */


#ifdef  _IGMPSZ_C
tFsModSizingParams FsIGMPSizingParams [] = {
{ "tIgmpArrayStr", "MAX_IGMP_ADDR_ARRAY_STR", sizeof(tIgmpArrayStr),MAX_IGMP_ADDR_ARRAY_STR, MAX_IGMP_ADDR_ARRAY_STR,0 },
{ "tIgmpGrpList", "MAX_IGMP_GROUPLIST_NODES", sizeof(tIgmpGrpList),MAX_IGMP_GROUPLIST_NODES, MAX_IGMP_GROUPLIST_NODES,0 },
{ "tIgmpIface", "MAX_IGMP_LOGICAL_IFACES", sizeof(tIgmpIface),MAX_IGMP_LOGICAL_IFACES, MAX_IGMP_LOGICAL_IFACES,0 },
{ "tIgmpGroup", "MAX_MCAST_GRPS", sizeof(tIgmpGroup),MAX_MCAST_GRPS, MAX_MCAST_GRPS,0 },
{ "tIgmpSchQuery", "MAX_IGMP_MCAST_SCHED_QUERIES", sizeof(tIgmpSchQuery),MAX_IGMP_MCAST_SCHED_QUERIES, MAX_IGMP_MCAST_SCHED_QUERIES,0 },
{ "tIgmpSource", "MAX_IGMP_MCAST_SRCS", sizeof(tIgmpSource),MAX_IGMP_MCAST_SRCS, MAX_IGMP_MCAST_SRCS,0 },
{ "tIgmpSrcReporter", "MAX_IGMP_MCAST_SRS_REPORTERS", sizeof(tIgmpSrcReporter),MAX_IGMP_MCAST_SRS_REPORTERS, MAX_IGMP_MCAST_SRS_REPORTERS,0 },
{ "tIgmpQMsg", "MAX_IGMP_Q_DEPTH", sizeof(tIgmpQMsg),MAX_IGMP_Q_DEPTH, MAX_IGMP_Q_DEPTH,0 },
{ "tIgmpQuery", "MAX_IGMP_QUERIES", sizeof(tIgmpQuery),MAX_IGMP_QUERIES, MAX_IGMP_QUERIES,0 },
{ "INT1[IGMP_MAX_MTU_SIZE]", "MAX_IGMP_RECVBUF_BLKS", sizeof(INT1[IGMP_MAX_MTU_SIZE]),MAX_IGMP_RECVBUF_BLKS, MAX_IGMP_RECVBUF_BLKS,0 },
{ "tIgmpRmMsg", "MAX_IGMP_RM_QUE_DEPTH", sizeof(tIgmpRmMsg),MAX_IGMP_RM_QUE_DEPTH, MAX_IGMP_RM_QUE_DEPTH,0 },
{ "tSourceNode", "MAX_IGMP_SCHED_QUERY_SRCS", sizeof(tSourceNode),MAX_IGMP_SCHED_QUERY_SRCS, MAX_IGMP_SCHED_QUERY_SRCS,0 },
{ "tIgmpIPvXAddr", "MAX_IGMP_SRCS_FOR_MRP_PER_GROUP", sizeof(tIgmpIPvXAddr),MAX_IGMP_SRCS_FOR_MRP_PER_GROUP, MAX_IGMP_SRCS_FOR_MRP_PER_GROUP,0 },
{ "UINT1[MAX_IGMP_SRCS_FOR_MRP_PER_GROUP*IPVX_IPV6_ADDR_LEN]", "MAX_SRCS_TO_PKT_BLKS_U1", sizeof(UINT1[MAX_IGMP_SRCS_FOR_MRP_PER_GROUP*IPVX_IPV6_ADDR_LEN]),MAX_SRCS_TO_PKT_BLKS_U1, MAX_SRCS_TO_PKT_BLKS_U1,0 },
{ "UINT4[MAX_IGMP_SRCS_FOR_MRP_PER_GROUP]", "MAX_SRCS_TO_PKT_BLKS_U4", sizeof(UINT4[MAX_IGMP_SRCS_FOR_MRP_PER_GROUP]),MAX_SRCS_TO_PKT_BLKS_U4, MAX_SRCS_TO_PKT_BLKS_U4,0 },
{ "tIgmpHostMcastGrpEntry", "MAX_IGMP_MCAST_GRPS", sizeof(tIgmpHostMcastGrpEntry), MAX_IGMP_MCAST_GRPS, MAX_IGMP_MCAST_GRPS,0 },
{ "tIgmpSSMMapGrpEntry", "MAX_IGMP_SSM_MAPPINGS", sizeof(tIgmpSSMMapGrpEntry), MAX_IGMP_SSM_MAPPINGS, MAX_IGMP_SSM_MAPPINGS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _IGMPSZ_C  */
extern tFsModSizingParams FsIGMPSizingParams [];
#endif /*  _IGMPSZ_C  */


