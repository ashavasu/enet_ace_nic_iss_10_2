
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsigmogi.h,v 1.2 2007/02/01 14:49:11 iss Exp $
 *
 * Description:mid level generated file
 *
 *******************************************************************/
# ifndef fsigmOGP_H
# define fsigmOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FSIGMP                                       (0)
# define SNMP_OGP_INDEX_FSIGMPINTERFACETABLE                         (1)
# define SNMP_OGP_INDEX_FSIGMPCACHETABLE                             (2)

#endif /*  fsigmpOGP_H  */
