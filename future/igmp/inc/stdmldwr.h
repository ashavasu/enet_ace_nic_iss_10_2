/*$Id: stdmldwr.h,v 1.3 2011/10/13 10:04:56 siva Exp $*/

#ifndef _STDMLDWRAP_H 
#define _STDMLDWRAP_H 
VOID RegisterSTDMLD(VOID);
INT4 MldInterfaceIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQueryIntervalTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQueryIntervalSet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQueryIntervalGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldInterfaceStatusSet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceStatusGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceVersionTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldInterfaceVersionSet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceVersionGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQuerierGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQueryMaxResponseDelayTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQueryMaxResponseDelaySet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQueryMaxResponseDelayGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceJoinsGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceGroupsGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceRobustnessTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldInterfaceRobustnessSet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceRobustnessGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceLastListenQueryIntvlTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldInterfaceLastListenQueryIntvlSet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceLastListenQueryIntvlGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceProxyIfIndexTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldInterfaceProxyIfIndexSet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceProxyIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQuerierUpTimeGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceQuerierExpiryTimeGet (tSnmpIndex *, tRetVal *);
INT4 MldCacheAddressGet (tSnmpIndex *, tRetVal *);
INT4 MldCacheIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 MldCacheSelfTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldCacheSelfSet (tSnmpIndex *, tRetVal *);
INT4 MldCacheSelfGet (tSnmpIndex *, tRetVal *);
INT4 MldCacheLastReporterGet (tSnmpIndex *, tRetVal *);
INT4 MldCacheUpTimeGet (tSnmpIndex *, tRetVal *);
INT4 MldCacheExpiryTimeGet (tSnmpIndex *, tRetVal *);
INT4 MldCacheStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MldCacheStatusSet (tSnmpIndex *, tRetVal *);
INT4 MldCacheStatusGet (tSnmpIndex *, tRetVal *);
INT4 MldInterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 MldCacheTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

 /*  GetNext Function Prototypes */

INT4 GetNextIndexMldCacheTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexMldInterfaceTable( tSnmpIndex *, tSnmpIndex *);
#endif
