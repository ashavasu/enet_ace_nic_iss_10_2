/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mlddefs.h,v 1.11 2015/09/25 07:09:04 siva Exp $
 *
 * Description:File containing all the definitions of MLD.
 *
 *******************************************************************/
#ifndef _MLDDEFS_H_
#define _MLDDEFS_H_

#define  MLD_MAX_REGISTER     2

#define  MLD_PROTID  58
#define  MLD_DEF_VRF_CTXT_ID  0
#define  ICMP6_MLD_PKT       0
#define  MLD_HEADER_LEN   8
#define  MLD_ENABLE       IGMP_ENABLE 
#define  MLD_DISABLE      IGMP_DISABLE

#define  MLD_VERSION_1             (UINT1) 61
#define  MLD_VERSION_2             (UINT1) 62
#define IP6_DEFAULT_CONTEXT         0

#define  MLD_QUERY                 130
#define  MLD_REPORT                131
#define  MLD_DONE                  132
#define  MLD_V2_REPORT             143

#define  MLDV2_MAX_RESPONSE_CODE  32768
#define  MLDV2_MAX_QQIC_VAL       128
#define  MLDV2_QRV_MAX_VALUE      7

#define  MLD_CHKSUM_FIELD_OFFSET  2
#define  MLD_CHKSUM_FIELD_SIZE    2

#define  MLD_V1_QUERY_LEN          sizeof(tMldPkt)

#define  MLD_GET_PARAMS(pBuf)      &(pBuf->ModuleData)

#define  MLD_MAX_MTU_SIZE      IGMP_MAX_MTU_SIZE

#define  NH_ICMP6                 58     /* ICMP6 header */

#define MLD_ANCILLARY_DATA_LEN    100

#define   MLD_CKSUM_CHUNK_SIZE                        (16 * 2)

#define   MLD_BYTES_IN_OCTECT                  8
#define   MLD_IP6_TYPE_LEN_BYTES               2
#define   MLD_IP6_HDR_LEN                 sizeof(tIp6Hdr)               
#define   MLD_DATA_LINK_HDR_LEN          14

#define  MLD_IFACE_UP    IGMP_IFACE_UP
#define  MLD_IFACE_DOWN  IGMP_IFACE_DOWN

#define  MLD_SRSP     IGMP_SRSP
#define  MLD_QRV      IGMP_QRV
#define  MLD_QQIC     IGMP_QQIC

#define  MLD_IPVX_ADDR_CLEAR           IPVX_ADDR_CLEAR
#define  MLD_IPVX_ADDR_INIT_IPV6       IPVX_ADDR_INIT_IPV6
#define  MLD_IPVX_ADDR_FMLY_IPV6       IPVX_ADDR_FMLY_IPV6
#define  MLD_IP_COPY_FROM_IPVX         IGMP_IP_COPY_FROM_IPVX
#define  MLD_EXTRACT_IPV6_HEADER       MldIpExtractHdr
#define  MLD_BUF_READ_OFFSET(buf)      CB_READ_OFFSET(buf)

#define  MLD_QUERIER_ROB_VARIABLE    IGMP_IF_ROB_VARIABLE

#define MLD_IS_ADDR_LINK_SCOPE_MULTI(a)       \
   ((a).u4_addr[0] == OSIX_HTONL(0xff020000))

#define  MLD_CHK_IF_SSM_RANGE(GrpAddr, retVal)\
{\
    tIPvXAddr TempGrpAddr; \
    IPVX_ADDR_CLEAR (&TempGrpAddr); \
    retVal = OSIX_FALSE;\
    IPVX_ADDR_COPY (&TempGrpAddr, &GrpAddr);\
    TempGrpAddr.au1Addr[1] = TempGrpAddr.au1Addr[1] & gau1SSMIPv6StartAddr.au1Addr[1]; \
    if((IPVX_ADDR_COMPARE (TempGrpAddr, gau1SSMIPv6StartAddr) >= 0)&&\
       (IPVX_ADDR_COMPARE (TempGrpAddr, gau1SSMIPv6EndAddr) < 0))\
    {\
        retVal = OSIX_TRUE;\
    }\
}

#define  MLD_CHK_IF_INVALID_SSM_RANGE(GrpAddr, retVal)\
{\
    tIPvXAddr TempGrpAddr; \
    IPVX_ADDR_CLEAR (&TempGrpAddr); \
    retVal = OSIX_FALSE;\
    IPVX_ADDR_COPY (&TempGrpAddr, &GrpAddr);\
    TempGrpAddr.au1Addr[1] = TempGrpAddr.au1Addr[1] & gau1InvalidSSMIPv6StartAddr.au1Addr[1]; \
    if((IPVX_ADDR_COMPARE (TempGrpAddr, gau1InvalidSSMIPv6StartAddr) >= 0)&&\
       (IPVX_ADDR_COMPARE (TempGrpAddr, gau1InvalidSSMIPv6EndAddr) <= 0))\
    {\
        retVal = OSIX_TRUE;\
    }\
}


#define MLD_GET_QQIC_FROM_QIVAL       IGMP_GET_CODE_FROM_TIME
#define MLD_GET_RESPCODE_FROM_RESPTIME(Time, Code)              \
{                                                               \
   UINT2   u2Code = 0;                                          \
   UINT2   u2CodeVal = 0;                                       \
   for(u2Code = 0x8000; u2Code < 0xffff; u2Code++)              \
   {                                                            \
        u2CodeVal = (UINT2) MLD_GET_MRT_FROM_MRC(u2Code);              \
        if (u2CodeVal >= Time)                                  \
        {                                                       \
           Code = u2Code;                                       \
           break;                                               \
        }                                                       \
   }                                                            \
   if (u2CodeVal < Time)                                        \
   {                                                            \
        Code = u2Code;                                          \
   }                                                            \
}

/* Macro for calculating Max. Response Time from Max. Response Code */
#define MLD_GET_MRT_FROM_MRC(u2Code) \
          ( ((u2Code & 0x0FFF) | 0x1000) << (((u2Code & 0x7000) >> 12) + 3) )

/*Encoding*/
#define MLDv2_EXP_BITS 3
#define MLDv2_MAX_EXP_VAL 0x07
#define MLDv2_MAX_MANT_VAL 0x1FFF
#define MLDV2_MAX_EXP_MANT_VAL 0x8000
#define MLDv2_HUNDRED 100
#define MLDV2_MAX_ENCODE_VAL (MLDv2_MAX_MANT_VAL << (MLDv2_MAX_EXP_VAL + MLDv2_EXP_BITS))

#define MLD_V1REP_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4MldRxv1Reports++; \
    if (pIgmpIfaceNode->u4MldRxv1Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4MldRxv1Reports = 0; \
    } \
    pIgmpIfaceNode->u4InJoins++; \
    if (pIgmpIfaceNode->u4InJoins == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InJoins = 0; \
    } \
}

#define MLD_V1REP_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4MldTxv1Reports++; \
    if (pIgmpIfaceNode->u4MldTxv1Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4MldTxv1Reports = 0; \
    } \
}

#define MLD_V2REP_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4MldRxv2Reports++; \
    if (pIgmpIfaceNode->u4MldRxv2Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4MldRxv2Reports = 0; \
    } \
    pIgmpIfaceNode->u4InJoins++; \
    if (pIgmpIfaceNode->u4InJoins == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InJoins = 0; \
    } \
}

#define MLD_V2REP_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4MldTxv2Reports++; \
    if (pIgmpIfaceNode->u4MldTxv2Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4MldTxv2Reports = 0; \
    } \
}

#define  MLD_IPVX_ADDR_COMPARE  IPVX_ADDR_COMPARE
#define  MLD_GET_IF_NODE        IGMP_GET_IF_NODE
#define  MLD_GRP_QUERY_RX       IGMP_GRP_QUERY_RX
#define  MLD_GEN_QUERY_RX       IGMP_GEN_QUERY_RX
#define  MLD_GRP_SRC_QUERY_RX   IGMP_GRP_SRC_QUERY_RX
#define  MLD_DONE_RX            IGMP_LEAVE_RX
#define  MLD_GEN_QUERY_TX       IGMP_GEN_QUERY_TX
#define  MLD_GRP_QUERY_TX       IGMP_GRP_QUERY_TX
#define  MLD_GRP_SRC_QUERY_TX   IGMP_GRP_SRC_QUERY_TX

#define  MLD_IF_LAST_MBR_QURY_IVAL   IGMP_IF_LAST_MEMBER_QIVAL

/* Gets the LMQT value for a given interface node*/
#define   MLD_LMQT(pIfNode) \
            (((MLD_IF_LAST_MBR_QURY_IVAL (pIfNode) / 10) * \
             MLD_IF_LAST_MEMBER_QRY_COUNT (pIfNode)) * 100)
#define  MLD_IF_LAST_MEMBER_QRY_COUNT IGMP_IF_LAST_MEMBER_QRY_COUNT
#define  MldPktProcessV1Query        IgmpProcessQuery
#define  MldPktProcessV1Report       IgmpProcessV2Report
#define  MldPktProcessV2GroupRecord  IgmpV3ProcessGroupRecord
#define  MldPktProcessDone           IgmpProcessLeave

/* to be moved to mldport.h */
#define  MLD_IP6_GET_IF_CONFIG_RECORD      MldPortIpv6GetIfInfo
#define  MLD_IP6_GET_IFINDEX_FROM_PORT     MldPortGetIp6IfIndexFromPort 

/*mldcmds.def and mldcli.c uses the follwing macros */
#define MLD_FAST_LEAVE_ENABLE           IGMP_FAST_LEAVE_ENABLE
#define MLD_FAST_LEAVE_DISABLE          IGMP_FAST_LEAVE_DISABLE
#define MLD_DEFAULT_QUERY_INTERVAL      IGMP_DEFAULT_QUERY_INTERVAL
#define MLD_NO_QUERY_INTERVAL           IGMP_NO_QUERY_INTERVAL
#define MLD_DEFAULT_ROB_VARIABLE        IGMP_DEFAULT_ROB_VARIABLE
#define MLD_NO_ROB_VARIABLE        IGMP_NO_ROB_VARIABLE
#define MLD_DEFAULT_LAST_MEMBER_QIVAL   IGMP_DEFAULT_LAST_MEMBER_QIVAL
#define MLD_NO_LAST_MEMBER_QIVAL   IGMP_NO_LAST_MEMBER_QIVAL
#define MLD_INVALID_INDEX               IGMP_INVALID_INDEX
#define MLD_DEFAULT_MAX_RESP_TIME       IGMP_DEFAULT_MAX_RESP_TIME
#define MLD_NO_MAX_RESP_TIME       IGMP_NO_MAX_RESP_TIME
#define MLD_CREATE                      IGMP_CREATE
#define MLD_SET_ACTIVE                  IGMP_SET_ACTIVE
#define MLD_CREATE_AND_GO               IGMP_CREATE_AND_GO
#define MLD_CREATE_AND_WAIT             IGMP_CREATE_AND_WAIT
#define MLD_DESTROY                     IGMP_DESTROY
#define MLD_NOT_IN_SERVICE              IGMP_NOT_IN_SERVICE
#define MLD_ACTIVE                      IGMP_ACTIVE
#define MLD_FALSE                       IGMP_FALSE
#define MLD_TRUE                        IGMP_TRUE
#define MLD_ZERO                        IGMP_ZERO
#define MLD_IFACE_OPER_UP               IGMP_IFACE_OPER_UP
#define MLD_IFACE_ADMIN_UP              IGMP_IFACE_ADMIN_UP
#define MLD_IFACE_ADMIN_DOWN            IGMP_IFACE_ADMIN_DOWN
#define MLD_MAX_INT4                    IGMP_MAX_INT4
#define MLD_SIXTEEN                    IGMP_SIXTEEN                                 
#define MLD_FFFF                       IGMP_FFFF                                
#define MLD_ONE                1

#define   MLD_ADDRCHG_INTERFACE_ADD                1
#define   MLD_ADDRCHG_INTERFACE_DEL                2

#define   MLD_IFACE_MIN_QUERY_INTERVAL               1
#define   MLD_IFACE_MAX_QUERY_INTERVAL               31744
#define   MLD_S_FLAG                    IGMP_S_FLAG
#define   MLD_NOT_OK                    IGMP_NOT_OK
/* Minimum value for query-min-response-time */
#define   MLD_MIN_QRY_MAX_RESP_TIME                  0
/* Maximum value for query-max-response-time */
#define   MLD_MAX_QRY_MAX_RESP_TIME                  31744
/* Minimum value for last-member-query-interval */
#define   MLD_MIN_LAST_MEMBER_QIVAL                    1
/* Maximum value for last-member-query-interval */
#define   MLD_MAX_LAST_MEMBER_QIVAL                  31744
#define   MLD_PROXY_STATUS()                     gIgmpConfig.i1MldProxyStatus
/* END: mldcmds.def and mldcli.c uses the follwing macros */

#if 0
/* to be moved to proto.h */
INT4
MldPortGetIpPortFrmIndex(UINT4 u4IfIndex, UINT4 *pu4Port);
INT4
MldPortIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo);
PUBLIC VOID
MldPortIpv6ModuleIfChgHandler (tNetIpv6HliParams * pIp6HliParams);
VOID
MldPortHandleV6IfStatusChg (tNetIpv6IfStatChange * pIfInfo);
INT4
MldPortGetIp6IfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex);

/* to be moved to mldglob.h */
tIPvXAddr gAllNodeMcastAddr = {2, 16, 0, {0xff, 0x02, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}};

tIPvXAddr gAllRtrMcastAddr = {2, 16, 0,{0xff, 0x02, 0x00, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02}};
#endif

#define MLD_INVALID_REGISTER           IGMP_INVALID_REGISTER 
#define MLD_GCM_MLDV1                  MLD_VERSION_1
#define MLD_GCM_MLDV2                  MLD_VERSION_2
#endif /*_MLDDEFS_H_*/

