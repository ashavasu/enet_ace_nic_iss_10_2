/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpport.h,v 1.21 2016/06/24 09:42:19 siva Exp $
 *
 * Description:This file holds the prototypes which is to be    
 *             ported to Target or different IP other than      
 *             FutureIP                                       
 *
 *******************************************************************/
#ifndef _IGMP_PORT_H_
#define _IGMP_PORT_H_

/* IGMP Task Details */
#define  IGMP_TASK_NAME                 "IGMP"

#define  IGMP_TIMER_EXPIRY_EVENT          1
#define  IGMP_PKT_RECV_EVENT              2
#define  IGMP_IP_IF_CHG_EVENT             4
#define  IGMP_INPUT_Q_EVENT               8
#define  IGMP_PACKET_ARRIVAL_EVENT       16
#define  MLD_IPADDR_CHG_EVENT            4096

#define  MLD_PKT_RECV_EVENT              1024

/* RM Specific details*/
#define  IGMP_RM_PKT_EVENT               2048

#define  IGMP_RM_PKT_QUEUE       "IGMPRMQ"
#define  IGMP_RM_PKT_QUEUE_DEPTH      10

/* IGMP PROXY EVENTS */
#define  IGP_TIMER_EXP_EVENT       32
#define  IGP_MCAST_DATA_EVENT      64
#define  IGP_PBMP_CHANGE_EVENT     128
#define  IGP_MFWD_DISABLE_EVENT    256
#define  IGP_MFWD_ENABLE_EVENT     512
#define  IGP_MDP_QUEUE_NAME       "IGPQ"
#define  IGP_MDP_QUEUE_DEPTH      100

#if defined (LNXIP4_WANTED) && defined (NP_KERNEL_WANTED) 
#define  IGMP_RECV_TASK_NAME            "IGRV"
#define  IGMP_RECV_TASK_PRIORITY        50
#endif

#define  IGMP_EVENT_WAIT_FLAG        (OSIX_WAIT | OSIX_EV_ANY)
#define  IGMP_EVENT_WAIT_TIMEOUT          0

#define  IGMP_ROUTING_TASK_PRIORITY      35
#define  IGMP_STACK_SIZE             OSIX_DEFAULT_STACK_SIZE

/* IGMP Queue Details */
#define  IGMP_Q          "GMQ"      

#define  IGMP_Q_DEPTH    FsIGMPSizingParams\
                         [MAX_IGMP_Q_DEPTH_SIZING_ID].u4PreAllocatedUnits       
#define  IGMP_RM_QUE_DEPTH    FsIGMPSizingParams\
                         [MAX_IGMP_RM_QUE_DEPTH_SIZING_ID].u4PreAllocatedUnits       
#define  IGMP_Q_MODE     OSIX_GLOBAL 

#define  IGMP_QUEUE_NAME_LEN             4
#define  IGMP_IP_DESTROY                32
#define  IGMP_IP_OPER_STATE_CHANGED      1

#define IgmpFillMem(xdst,xdata,xlen) memset((UINT1 *)xdst,xdata,xlen)
#define IGMP_IP_GET_IF_CFG_RECORD        NetIpv4GetIfInfo
#define IGMP_IP_GET_PORT_FROM_IFINDEX    NetIpv4GetPortFromIfIndex
#define IGMP_IP_GET_IFINDEX_FROM_PORT    NetIpv4GetCfaIfIndexFromPort
#define IGMP_INIT_COMPLETE(u4Status)     lrInitComplete(u4Status)
#ifdef LNXIP4_WANTED
#define IGMP_EXTRACT_IP_HEADER           IgmpIpExtractHdr
#else
#define IGMP_EXTRACT_IP_HEADER           IpExtractHdr
#endif

/* Macros for RAW sockets */
#define IGMP_SEND_TO           sendto 
#define IGMP_SET_SOCK_OPT      setsockopt 
#define IGMP_RECV_FROM         recvfrom 
#define IGMP_CLOSE             close 
#define IGMP_SOCKET            socket 
#define IGMP_SELECT            select 
#define IGMP_FCNTL             fcntl 
#define IGMP_RAWSOCK_NON_BLOCK 16 
#define IGMP_MAX_MTU_SIZE      1500 
#define IGMP_AUXILLARY_LEN     24 


#define   IP_OPT_EOL              0
#define   IP_OPT_NOP              1
#define   IP_OPT_ROUTERALERT     20
/* Extract option number from this octet */
#define   IP_OPT_NUMBER(Copy1Class2No5)    ((UINT1)(Copy1Class2No5 & 0x1f))

/* Macro to get the random number with in range */
/* -------------------------------------------- */
#define IGMP_GET_RANDOM_IN_RANGE(Range, RetValue) \
{\
  UINT4 TempTime; \
  OsixGetSysTime((tOsixSysTime *) &TempTime); \
  SRAND ((UINT4) TempTime); \
  (RetValue) = (UINT2)IGMP_RAND((Range)); \
}

#define GET_IFINDEX_FROMBUF(Buf, IfIndex) \
{ \
    tIpParms *pIpParms = NULL; \
    pIpParms = (tIpParms *)(&Buf->ModuleData); \
    IfIndex = pIpParms->u2Port; \
}

#define IGMP_QMSG_FREE(pQMsg) \
{ \
   IgmpMemRelease (gIgmpMemPool.IgmpQPoolId, (UINT1 *) pQMsg);\
}

/* Structure for the IGMP Q Message */
/* --------------------------------*/
typedef struct _IgmpPktInfo {
    tCRU_BUF_CHAIN_HEADER  *pPkt;
} tIgmpPktInfo;

#ifdef IGMPPRXY_WANTED
typedef struct _IgmpPrxyPbmpChg
{
  tMacAddr    MacAddr;
  UINT2       u2VlanId; 
}tIgmpPrxyPbmpChg;
#endif

typedef struct _IgmpQMsg {
        UINT4  u4MsgType ;
        union _IgmpQMsgParam{
                tIgmpPktInfo   IgmpPktInfo;
                tIgmpIfStatusInfo   IgmpIfStatusInfo;
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
                tMldIpv6AddrInfo   MldIpv6AddrInfo;
                            /* IPv6 address information */
#endif
#ifdef MBSM_WANTED
           struct MbsmIndication {
               tMbsmProtoMsg *pMbsmProtoMsg;
           }MbsmCardUpdate;
#endif
#ifdef IGMPPRXY_WANTED
               tIgmpPrxyPbmpChg IgpPbmpChangeMsg;  
#endif
        }IgmpQMsgParam;
}tIgmpQMsg;

typedef struct IgmpFilterPkt {
        tIgmpIPvXAddr   u4SrcIpAddr;     /* Source IP Address in received packet */
        tIgmpIPvXAddr   u4GrpAddress;    /* IGMP group IP address */
        UINT4       u4InIfIndex;   /* Incoming Interface Index */
        UINT1       u1PktType;     /* Report/Leave/Query */
        UINT1       u1Version;     /* IGMP version v1/v2/v3 */
        UINT1       au1Pad[2];     /* For padding */
}tIgmpFilter;

VOID IgmpCurrMbrInfoToMrp (UINT1 u1AddrType);
VOID IgmpReStartGrpTmrToLmqTmr (tIgmpIface* pIfNode, tIgmpGroup *pGrp);
VOID IgmpReStartSrcTmrToLmqTmr (tIgmpIface* pIfNode, tIgmpSource *pSrc);
VOID IgmpReStartGrpTmr (tIgmpIface* pIfNode, tIgmpGroup *pGrp);

VOID
IgmpHandleIncomingPkt (tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                       UINT4 u4Index,tIP_INTERFACE IfId,  UINT1 u1Flag);
VOID
IgmpHandleIfUpdates (tNetIpv4IfInfo *pIfInfo, UINT4 u4BitMap);
INT4
IgmpHandleProtocolDownStatus (UINT1 u1AddrType);
INT4
IgmpHandleIfProtocolDownStatus (UINT4 u4Port, UINT1 u1AddrType);
INT4
IgmpValidateV4SourceAddress (UINT4 u4Source, UINT1 u1ChkZeroAddress);
#ifdef IP_VX_WANTED
INT4 IpifJoinMcastGroup (UINT4 *pMcastGroupAddr, UINT4 u4IfIndex,
                         UINT1 u1ProtocolId);
INT4 IpifLeaveMcastGroup (UINT4 *pMcastGroupAddr, UINT4 u4IfIndex,
                          UINT1 u1ProtocolId);
#endif
INT4 IgmpCreateSocket (VOID);
VOID IgmpCloseSocket (VOID);
VOID IgmpHandlePktArrivalEvent (VOID);
VOID IgmpNotifyPktArrivalEvent(INT4 i4SocketId);

#ifdef LNXIP4_WANTED
INT4 IgmpSetSendSocketOptions (VOID);
UINT2 IgmpIpCalcCheckSum (UINT1 *pBuf, UINT4 u4Size);
INT4 IgmpIpExtractHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf);
#ifndef ISS_WANTED
/* These functions are for Stack IGMP porting on Linux IP.
 * These will not be needed for ISS solution. Incase of ISS, IGMP control
 * packets will be received from netip thru IGMP socket */
INT4 IgmpSetMcastSocketOption (tIgmpIface *pIfNode, UINT1 u1IfStatus);
INT4 IgmpSetRecvSocketOptions (VOID);
INT4 IgmpResetSocketOption (VOID);
#else /* ISS_WANTED */
#if defined NP_KERNEL_WANTED
/* Only for BCM Linuxip */
VOID IgmpRecvTaskMain (VOID);
INT4 IgmpCreateRecvTask (VOID);
INT4 IgmpDeleteRecvTask (VOID);
#else
/* This function is used to receive packets from netip IGMP socket.
 * For BCM, this is currently not tested. After testing, this can be
 * used for BCM Linuxip as well */
PUBLIC VOID IgmpNetIpReceivePkt (tCRU_BUF_CHAIN_HEADER * pBuf);
#endif /* NP_KERNEL_WANTED */
#endif /* ISS_WANTED */
#endif /* LNXIP4_WANTED */


BOOL1 IgmpFilterPackets (tIgmpFilter * pIgmpFilterInfo);

typedef   tMemPoolId                   tIgmpMemPoolId;

INT4 IgmpUtilChkIfPimRPInSSMMappedGroup PROTO ((UINT4, UINT4));

#endif /* _IGMP_PORT_H_ */

