/* $Id: fsmldlow.h,v 1.3 2015/02/13 11:13:56 siva Exp $*/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsmldNoOfCacheEntries ARG_LIST((UINT4 *));

INT1
nmhGetFsmldNoOfRoutingProtocols ARG_LIST((UINT4 *));

INT1
nmhGetFsmldTraceDebug ARG_LIST((UINT4 *));

INT1
nmhGetFsmldDebugLevel ARG_LIST((UINT4 *));

INT1
nmhGetFsmldMode ARG_LIST((INT4 *));

INT1
nmhGetFsmldProtocolUpDown ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsmldNoOfCacheEntries ARG_LIST((UINT4 ));

INT1
nmhSetFsmldNoOfRoutingProtocols ARG_LIST((UINT4 ));

INT1
nmhSetFsmldTraceDebug ARG_LIST((UINT4 ));

INT1
nmhSetFsmldDebugLevel ARG_LIST((UINT4 ));

INT1
nmhSetFsmldMode ARG_LIST((INT4 ));

INT1
nmhSetFsmldProtocolUpDown ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsmldNoOfCacheEntries ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsmldNoOfRoutingProtocols ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsmldTraceDebug ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsmldDebugLevel ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsmldMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsmldProtocolUpDown ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsmldNoOfCacheEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsmldNoOfRoutingProtocols ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsmldTraceDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsmldDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsmldMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsmldProtocolUpDown ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
