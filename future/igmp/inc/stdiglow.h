
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdiglow.h,v 1.3 2008/08/20 15:04:36 iss Exp $
 *
 * Description: This file contains proto types for low level routines
 *
 *******************************************************************/
/* Proto Validate Index Instance for IgmpInterfaceTable. */
INT1
nmhValidateIndexInstanceIgmpInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IgmpInterfaceTable  */

INT1
nmhGetFirstIndexIgmpInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIgmpInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIgmpInterfaceQueryInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIgmpInterfaceVersion ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceQuerier ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceQueryMaxResponseTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceQuerierUpTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceQuerierExpiryTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceVersion1QuerierTimer ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceWrongVersionQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceJoins ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceProxyIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIgmpInterfaceGroups ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceRobustness ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceLastMembQueryIntvl ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIgmpInterfaceVersion2QuerierTimer ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIgmpInterfaceQueryInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIgmpInterfaceStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIgmpInterfaceVersion ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIgmpInterfaceQueryMaxResponseTime ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIgmpInterfaceProxyIfIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIgmpInterfaceRobustness ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIgmpInterfaceLastMembQueryIntvl ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IgmpInterfaceQueryInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IgmpInterfaceStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IgmpInterfaceVersion ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IgmpInterfaceQueryMaxResponseTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IgmpInterfaceProxyIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IgmpInterfaceRobustness ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IgmpInterfaceLastMembQueryIntvl ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IgmpInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IgmpCacheTable. */
INT1
nmhValidateIndexInstanceIgmpCacheTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IgmpCacheTable  */

INT1
nmhGetFirstIndexIgmpCacheTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIgmpCacheTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIgmpCacheSelf ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetIgmpCacheLastReporter ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIgmpCacheUpTime ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIgmpCacheExpiryTime ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIgmpCacheStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetIgmpCacheVersion1HostTimer ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIgmpCacheVersion2HostTimer ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIgmpCacheSourceFilterMode ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIgmpCacheSelf ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetIgmpCacheStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IgmpCacheSelf ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2IgmpCacheStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IgmpCacheTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IgmpSrcListTable. */
INT1
nmhValidateIndexInstanceIgmpSrcListTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IgmpSrcListTable  */

INT1
nmhGetFirstIndexIgmpSrcListTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIgmpSrcListTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIgmpSrcListStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIgmpSrcListStatus ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IgmpSrcListStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IgmpSrcListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
