/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmgmddb.h,v 1.10 2016/06/24 09:42:18 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMGMDDB_H
#define _FSMGMDDB_H

UINT1 FsMgmdInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMgmdCacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMgmdSSMMapGroupTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsmgmd [] ={1,3,6,1,4,1,29601,2,62};
tSNMP_OID_TYPE fsmgmdOID = {9, fsmgmd};


UINT4 FsMgmdIgmpGlobalStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,1};
UINT4 FsMgmdIgmpTraceLevel [ ] ={1,3,6,1,4,1,29601,2,62,1,2};
UINT4 FsMgmdIgmpDebugLevel [ ] ={1,3,6,1,4,1,29601,2,62,1,3};
UINT4 FsMgmdMldGlobalStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,4};
UINT4 FsMgmdMldTraceLevel [ ] ={1,3,6,1,4,1,29601,2,62,1,5};
UINT4 FsMgmdMldDebugLevel [ ] ={1,3,6,1,4,1,29601,2,62,1,6};
UINT4 FsMgmdInterfaceIfIndex [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,1};
UINT4 FsMgmdInterfaceAddrType [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,2};
UINT4 FsMgmdInterfaceAdminStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,3};
UINT4 FsMgmdInterfaceFastLeaveStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,4};
UINT4 FsMgmdInterfaceOperStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,5};
UINT4 FsMgmdInterfaceIncomingPkts [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,6};
UINT4 FsMgmdInterfaceIncomingJoins [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,7};
UINT4 FsMgmdInterfaceIncomingLeaves [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,8};
UINT4 FsMgmdInterfaceIncomingQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,9};
UINT4 FsMgmdInterfaceOutgoingQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,10};
UINT4 FsMgmdInterfaceRxGenQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,11};
UINT4 FsMgmdInterfaceRxGrpQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,12};
UINT4 FsMgmdInterfaceRxGrpAndSrcQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,13};
UINT4 FsMgmdInterfaceRxIgmpv1v2Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,14};
UINT4 FsMgmdInterfaceRxIgmpv3Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,15};
UINT4 FsMgmdInterfaceRxMldv1Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,16};
UINT4 FsMgmdInterfaceRxMldv2Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,17};
UINT4 FsMgmdInterfaceTxGenQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,18};
UINT4 FsMgmdInterfaceTxGrpQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,19};
UINT4 FsMgmdInterfaceTxGrpAndSrcQueries [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,20};
UINT4 FsMgmdInterfaceTxIgmpv1v2Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,21};
UINT4 FsMgmdInterfaceTxIgmpv3Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,22};
UINT4 FsMgmdInterfaceTxMldv1Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,23};
UINT4 FsMgmdInterfaceTxMldv2Reports [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,24};
UINT4 FsMgmdInterfaceTxLeaves [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,25};
UINT4 FsMgmdInterfaceChannelTrackStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,26};
UINT4 FsMgmdInterfaceGroupListId [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,27};
UINT4 FsMgmdInterfaceLimit [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,28};
UINT4 FsMgmdInterfaceCurGrpCount [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,29};
UINT4 FsMgmdInterfaceCKSumError [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,30};
UINT4 FsMgmdInterfacePktLenError [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,31};
UINT4 FsMgmdInterfacePktsWithLocalIP [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,32};
UINT4 FsMgmdInterfaceSubnetCheckFailure [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,33};
UINT4 FsMgmdInterfaceQryFromNonQuerier [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,34};
UINT4 FsMgmdInterfaceReportVersionMisMatch [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,35};
UINT4 FsMgmdInterfaceQryVersionMisMatch [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,36};
UINT4 FsMgmdInterfaceUnknownMsgType [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,37};
UINT4 FsMgmdInterfaceInvalidV1Report [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,38};
UINT4 FsMgmdInterfaceInvalidV2Report [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,39};
UINT4 FsMgmdInterfaceInvalidV3Report [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,40};
UINT4 FsMgmdInterfaceRouterAlertCheckFailure [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,41};
UINT4 FsMgmdInterfaceIncomingSSMPkts [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,42};
UINT4 FsMgmdInterfaceInvalidSSMPkts [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,43};
UINT4 FsMgmdInterfaceJoinPktRate [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,44};
UINT4 FsMgmdInterfaceMalformedPkts [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,45};
UINT4 FsMgmdInterfaceSocketErrors [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,46};
UINT4 FsMgmdInterfaceBadScopeErrors [ ] ={1,3,6,1,4,1,29601,2,62,1,7,1,47};
UINT4 FsMgmdCacheAddrType [ ] ={1,3,6,1,4,1,29601,2,62,1,26,1,1};
UINT4 FsMgmdCacheAddress [ ] ={1,3,6,1,4,1,29601,2,62,1,26,1,2};
UINT4 FsMgmdCacheIfIndex [ ] ={1,3,6,1,4,1,29601,2,62,1,26,1,3};
UINT4 FsMgmdCacheGroupCompMode [ ] ={1,3,6,1,4,1,29601,2,62,1,26,1,4};
UINT4 FsMgmdGlobalLimit [ ] ={1,3,6,1,4,1,29601,2,62,1,8,1};
UINT4 FsMgmdGlobalCurGrpCount [ ] ={1,3,6,1,4,1,29601,2,62,1,8,2};
UINT4 FsMgmdSSMMapStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,8,3};
UINT4 FsMgmdSSMMapStartGrpAddress [ ] ={1,3,6,1,4,1,29601,2,62,1,9,1,1};
UINT4 FsMgmdSSMMapEndGrpAddress [ ] ={1,3,6,1,4,1,29601,2,62,1,9,1,2};
UINT4 FsMgmdSSMMapSourceAddress [ ] ={1,3,6,1,4,1,29601,2,62,1,9,1,3};
UINT4 FsMgmdSSMMapRowStatus [ ] ={1,3,6,1,4,1,29601,2,62,1,9,1,4};




tMbDbEntry fsmgmdMibEntry[]= {

{{11,FsMgmdIgmpGlobalStatus}, NULL, FsMgmdIgmpGlobalStatusGet, FsMgmdIgmpGlobalStatusSet, FsMgmdIgmpGlobalStatusTest, FsMgmdIgmpGlobalStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMgmdIgmpTraceLevel}, NULL, FsMgmdIgmpTraceLevelGet, FsMgmdIgmpTraceLevelSet, FsMgmdIgmpTraceLevelTest, FsMgmdIgmpTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMgmdIgmpDebugLevel}, NULL, FsMgmdIgmpDebugLevelGet, FsMgmdIgmpDebugLevelSet, FsMgmdIgmpDebugLevelTest, FsMgmdIgmpDebugLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMgmdMldGlobalStatus}, NULL, FsMgmdMldGlobalStatusGet, FsMgmdMldGlobalStatusSet, FsMgmdMldGlobalStatusTest, FsMgmdMldGlobalStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMgmdMldTraceLevel}, NULL, FsMgmdMldTraceLevelGet, FsMgmdMldTraceLevelSet, FsMgmdMldTraceLevelTest, FsMgmdMldTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMgmdMldDebugLevel}, NULL, FsMgmdMldDebugLevelGet, FsMgmdMldDebugLevelSet, FsMgmdMldDebugLevelTest, FsMgmdMldDebugLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{13,FsMgmdInterfaceIfIndex}, GetNextIndexFsMgmdInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceAddrType}, GetNextIndexFsMgmdInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceAdminStatus}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceAdminStatusGet, FsMgmdInterfaceAdminStatusSet, FsMgmdInterfaceAdminStatusTest, FsMgmdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMgmdInterfaceTableINDEX, 2, 0, 0, "1"},

{{13,FsMgmdInterfaceFastLeaveStatus}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceFastLeaveStatusGet, FsMgmdInterfaceFastLeaveStatusSet, FsMgmdInterfaceFastLeaveStatusTest, FsMgmdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMgmdInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsMgmdInterfaceOperStatus}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceIncomingPkts}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceIncomingPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceIncomingJoins}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceIncomingJoinsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceIncomingLeaves}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceIncomingLeavesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceIncomingQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceIncomingQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceOutgoingQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceOutgoingQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRxGenQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRxGenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRxGrpQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRxGrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRxGrpAndSrcQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRxGrpAndSrcQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRxIgmpv1v2Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRxIgmpv1v2ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRxIgmpv3Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRxIgmpv3ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRxMldv1Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRxMldv1ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRxMldv2Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRxMldv2ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxGenQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxGenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxGrpQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxGrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxGrpAndSrcQueries}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxGrpAndSrcQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxIgmpv1v2Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxIgmpv1v2ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxIgmpv3Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxIgmpv3ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxMldv1Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxMldv1ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxMldv2Reports}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxMldv2ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceTxLeaves}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceTxLeavesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceChannelTrackStatus}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceChannelTrackStatusGet, FsMgmdInterfaceChannelTrackStatusSet, FsMgmdInterfaceChannelTrackStatusTest, FsMgmdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMgmdInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsMgmdInterfaceGroupListId}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceGroupListIdGet, FsMgmdInterfaceGroupListIdSet, FsMgmdInterfaceGroupListIdTest, FsMgmdInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceLimit}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceLimitGet, FsMgmdInterfaceLimitSet, FsMgmdInterfaceLimitTest, FsMgmdInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceCurGrpCount}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceCurGrpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceCKSumError}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceCKSumErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfacePktLenError}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfacePktLenErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfacePktsWithLocalIP}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfacePktsWithLocalIPGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceSubnetCheckFailure}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceSubnetCheckFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceQryFromNonQuerier}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceQryFromNonQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceReportVersionMisMatch}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceReportVersionMisMatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceQryVersionMisMatch}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceQryVersionMisMatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceUnknownMsgType}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceUnknownMsgTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceInvalidV1Report}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceInvalidV1ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceInvalidV2Report}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceInvalidV2ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceInvalidV3Report}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceInvalidV3ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceRouterAlertCheckFailure}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceRouterAlertCheckFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceIncomingSSMPkts}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceIncomingSSMPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceInvalidSSMPkts}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceInvalidSSMPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceJoinPktRate}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceJoinPktRateGet, FsMgmdInterfaceJoinPktRateSet, FsMgmdInterfaceJoinPktRateTest, FsMgmdInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMgmdInterfaceTableINDEX, 2, 0, 0, "0"},

{{13,FsMgmdInterfaceMalformedPkts}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceMalformedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceSocketErrors}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceSocketErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{13,FsMgmdInterfaceBadScopeErrors}, GetNextIndexFsMgmdInterfaceTable, FsMgmdInterfaceBadScopeErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMgmdInterfaceTableINDEX, 2, 0, 0, NULL},

{{12,FsMgmdGlobalLimit}, NULL, FsMgmdGlobalLimitGet, FsMgmdGlobalLimitSet, FsMgmdGlobalLimitTest, FsMgmdGlobalLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMgmdGlobalCurGrpCount}, NULL, FsMgmdGlobalCurGrpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsMgmdSSMMapStatus}, NULL, FsMgmdSSMMapStatusGet, FsMgmdSSMMapStatusSet, FsMgmdSSMMapStatusTest, FsMgmdSSMMapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsMgmdSSMMapStartGrpAddress}, GetNextIndexFsMgmdSSMMapGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMgmdSSMMapGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsMgmdSSMMapEndGrpAddress}, GetNextIndexFsMgmdSSMMapGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMgmdSSMMapGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsMgmdSSMMapSourceAddress}, GetNextIndexFsMgmdSSMMapGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMgmdSSMMapGroupTableINDEX, 3, 0, 0, NULL},

{{13,FsMgmdSSMMapRowStatus}, GetNextIndexFsMgmdSSMMapGroupTable, FsMgmdSSMMapRowStatusGet, FsMgmdSSMMapRowStatusSet, FsMgmdSSMMapRowStatusTest, FsMgmdSSMMapGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMgmdSSMMapGroupTableINDEX, 3, 0, 1, NULL},

{{13,FsMgmdCacheAddrType}, GetNextIndexFsMgmdCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMgmdCacheTableINDEX, 3, 0, 0, NULL},

{{13,FsMgmdCacheAddress}, GetNextIndexFsMgmdCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMgmdCacheTableINDEX, 3, 0, 0, NULL},

{{13,FsMgmdCacheIfIndex}, GetNextIndexFsMgmdCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMgmdCacheTableINDEX, 3, 0, 0, NULL},

{{13,FsMgmdCacheGroupCompMode}, GetNextIndexFsMgmdCacheTable, FsMgmdCacheGroupCompModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMgmdCacheTableINDEX, 3, 0, 0, NULL},
};
tMibData fsmgmdEntry = { 64, fsmgmdMibEntry };

#endif /* _FSMGMDDB_H */

