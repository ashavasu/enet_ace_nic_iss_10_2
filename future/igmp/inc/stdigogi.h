
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdigogi.h,v 1.2 2007/02/01 14:49:11 iss Exp $
 *
 * Description: mid level generated file
 *
 *******************************************************************/
# ifndef stdigOGP_H
# define stdigOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_IGMPINTERFACETABLE                           (0)
# define SNMP_OGP_INDEX_IGMPCACHETABLE                               (1)
# define SNMP_OGP_INDEX_IGMPSRCLISTTABLE                             (2)

#endif /*  stdigmpOGP_H  */
