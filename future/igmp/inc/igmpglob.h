/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpglob.h,v 1.18 2016/06/24 09:42:19 siva Exp $
 *
 * Description:File containing the global data accessible by IGMP
 * 
 *******************************************************************/
#ifndef IGMPGLOB_H
#define IGMPGLOB_H

tRBTree  gIgmpGrpList;

tIgmpConfig  gIgmpConfig;
tIgmpSystemSize gIgmpSystemSize;

/* Global Table for maintaining IGMP Query Blocks */
tTMO_HASH_TABLE    *gpIgmpQueryTable;    /* Query Table pointer */

/* This variable holds the interface related information */
/* ----------------------------------------------------- */
tIgmpInterfaceInfo gIgmpIfInfo;

tIgmpMemPool   gIgmpMemPool;

tRBTree   gIgmpGroup; /* Global RB Tree that maintains all the 
                         groups along with the list of sources & reporters */
tRBTree   gIgmpSSMMappedGrp; /* Global RB Tree that maintains all the 
                         groups for which mapping is configured
                         along with the list of sources */
/* 
   Global that maintains Registration Information about the 
   Multicast Routing Protocols registered with IGMP 
*/
tIgmpRegister       gaIgmpRegister[IGMP_MAX_REGISTER];
tMldRegister       gaMldRegister[MLD_MAX_REGISTER];

tTimerListId        gIgmpTimerListId;
tOsixQId      gIgmpRmPktQId;
UINT4        gu4IgmpSysLogId;
tIgmpRedGblInfo     gIgmpRedGblInfo;

UINT4               gu4IgmpTrcFlg;
CHR1                gacIgmpSyslogmsg[MAX_LOG_STR_LEN];
UINT2               gu2NumOfRegMRPs; /* Actual number of MRPS Registered */
tIgmpIPvXAddr          *gpu4SrcSetArray;
tIgmpIPvXAddr           gIPvXZero;
tOsixTaskId         gIgmpTaskId;
tOsixQId            gIgmpQId;
tRBTree             gIgmpHostMcastGrpTable;
#if defined (LNXIP4_WANTED) && defined (NP_KERNEL_WANTED)
/* Only for BCM Linuxip */
INT4                gi4IgmpDevFd;
tOsixTaskId         gIgmpRecvTaskId;
#endif
UINT1       gau1Igmpv3Buffer[IGMP_MAX_MTU_SIZE];
UINT4       gu4MaxIgmpGrps = IGMP_ZERO;
UINT4       gu4MaxMldGrps = IGMP_ZERO;
tDbTblDescriptor gIgmpDynInfoList;
tDbDataDescInfo  gaIgmpDynDataDescList[IGMP_MAX_DYN_INFO_TYPE];

/* Offset table for Group DB Nodes */
tDbOffsetTemplate gaIgmpGrpOffsetTbl[] =
   {{(FSAP_OFFSETOF(tIgmpGroup, u4IfIndex) -
      FSAP_OFFSETOF(tIgmpGroup, GrpDbNode) - sizeof(tDbTblNode)), 4},
    {(FSAP_OFFSETOF(tIgmpGroup, u4GroupCompMode) -
      FSAP_OFFSETOF(tIgmpGroup, GrpDbNode) - sizeof(tDbTblNode)), 4},
    {-1, 0}};

/* Offset table for Source DB Nodes */
tDbOffsetTemplate gaIgmpSrcOffsetTbl[] =
   {{(FSAP_OFFSETOF(tIgmpSource, u4IfIndex) -
      FSAP_OFFSETOF(tIgmpSource, SrcDbNode) - sizeof(tDbTblNode)), 4},
    {-1, 0}};

/* Offset table for Source Reporter DB Nodes */
tDbOffsetTemplate gaIgmpSrcReporterOffsetTbl[] =
   {{(FSAP_OFFSETOF(tIgmpSrcReporter, u4IfIndex) -
      FSAP_OFFSETOF(tIgmpSrcReporter, SrcReporterDbNode) - sizeof(tDbTblNode)), 4},
    {-1, 0}};

/* Offset table for Query DB Nodes */
tDbOffsetTemplate gaIgmpQueryOffsetTbl[] =
   {{(FSAP_OFFSETOF(tIgmpQuery, u4IfIndex) -
      FSAP_OFFSETOF(tIgmpQuery, QuerierDbNode) - sizeof(tDbTblNode)), 4},
    {-1, 0}};

tIPvXAddr gAllNodeV6McastAddr = {2, 16, 0, {0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}};

tIPvXAddr gAllRtrV6McastAddr = {2, 16, 0,{0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02}};

tIPvXAddr gau1SSMIPv6StartAddr = {2, 16, 0, {0xff, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x01}};
tIPvXAddr gau1SSMIPv6EndAddr =   {2, 16, 0, {0xff, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}};

tIPvXAddr gau1InvalidSSMIPv6StartAddr = {2, 16, 0, {0xff, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};
tIPvXAddr gau1InvalidSSMIPv6EndAddr =   {2, 16, 0, {0xff, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00}};
#endif


