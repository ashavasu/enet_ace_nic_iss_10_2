/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpdefs.h,v 1.47 2017/02/06 10:45:26 siva Exp $
 *
 * Description: File containing the macros and constants used in IGMP
 *
 *******************************************************************/

#ifndef IGMPDEFS_H
#define IGMPDEFS_H

#include "igmp.h"

/* DEFINITIONS */
/* ----------- */
#define DVMRP_PKT                             0x13
#define   IGMP_FAILURE                                  0
#define   IGMP_SUCCESS                                  1
#define   IGMP_INVALID                               0xFF
#define   IGMP_PROTID                                   2
#define   IGMP_ZERO                                     0
#define   IGMP_ONE                                      1
#define   IGMP_TWO                                      2
#define   IGMP_THREE                                    3
#define   IGMP_FOUR                                     4
#define   IGMP_SIX                                      6
#define   IGMP_TEN                                     10
#define   IGMP_TWENTY                                   20
#define   IGMP_INVL_MAX_RESP_RATIO         11
#define   IGMP_SIXTEEN                                 16
#define   IGMP_FFFF                                0xffff
#define   IGMP_UP                                       1
#define   IGMP_DOWN                                     2
#define   IGMP_MAX_STATE                                3
#define   IGMP_MAX_EVENTS                               7
#define   IGMP_ENABLE                                   1
#define   IGMP_DISABLE                                  2
#define IGMP_MAX_INT4                                   0x7fffffff
#define IGMP_INVALID_INDEX                              -1      
#define IGMP_DELETE_INTERFACE                           0x00000020
#define IGMP_SEC_IFACE_DELETE                           5

#define   IGMP_VERSION_THREE                            3
#define   IGMP_SRC_TBL_INDICES_COUNT                 3
/* Added following hash defines to support IGMP standard MIB RFC:2933 
 */
#define IGMP_DBG_TIME_LEN                           30
#define IGMP_SEM_NAME                               "IGMS"
#define IGMP_MUTEX_SEM_VALUE                          1

#define   IGMP_CKSUM_CHUNK_SIZE                        (16 * 2)

#define   IGMP_FAST_LEAVE_DISABLE  0
#define   IGMP_FAST_LEAVE_ENABLE   1

#define   IGMP_CHANNEL_TRACK_DISABLE  0
#define   IGMP_CHANNEL_TRACK_ENABLE   1

#define   IGMP_RESV_SUP_RV                               2
#define   IGMP_RESV_OFFSET                               4

#define   IGMP_INVALID_REGISTER                         -1
#define   IGMP_MAX_IFINDEX_VALUE                    0xffffffff 

#define   IGMPV3_MAX_RESPONSE_CODE                  128   
#define   IGMP_MAX_RAND                               5   

#define   IGMP_V1V2_QUERY_LEN                         8
#define   IGMPV3_QUERY_LEN                            12 

/* Truth values */
#define   IGMP_TRUE                                     1
#define   IGMP_FALSE                                    0

#define   IGMP_SRC_MAPPED                               1
#define   IGMP_SRC_CONF                                 2

#define   IGMP_NULL_REPORTER       3

/* CLI defintions */
#define   IGMP_CREATE                                   1
#define   IGMP_SET_ACTIVE                               2

/* Definition for the RowStatus values */
#define   IGMP_ACTIVE                 1
#define   IGMP_NOT_IN_SERVICE         2
#define   IGMP_NOT_READY              3
#define   IGMP_CREATE_AND_GO          4
#define   IGMP_CREATE_AND_WAIT        5
#define   IGMP_DESTROY                6
#define   IGMP_GROUP_SQ_TIMER                           1
#define   IGMP_HOST_MCAST_JOIN_TIME        30

#define   IGMP_STARTUP_QUERY_TIMER_ID          1
#define   IGMP_QUERY_TIMER_ID                  2
#define   IGMP_QUERIER_PRESENT_TIMER_ID        3
#define   IGMP_GROUP_TIMER_ID                  4
#define   IGMP_GROUP_SQ_TIMER_ID               5
#define   IGMP_SOURCE_TIMER_ID         6
#define   IGMP_QUERY_SCHEDULE_TIMER_ID         7 
#define   IGMP_VERSION1_HOST_TIMER_ID          8       
#define   IGMP_VERSION2_HOST_TIMER_ID        9

#ifdef IGMPPRXY_WANTED
#define   IGMP_MAX_TIMERS                      13
#define   IGMP_HOST_MCAST_GROUP_TIMER_ID         12
#define   IGP_SRC_LIST_SIZE        (((MAX_IGMP_MCAST_SRCS+ 31)/32) * 4)
#else
#define   IGMP_MAX_TIMERS                      11
#define   IGMP_HOST_MCAST_GROUP_TIMER_ID         10
#endif

#define   IGMP_V3REP_SEND_FLAG                 0xf0
#define   IGMP_VERSION_FLAG                    0x0f

#define   IGMP_NULL_GROUP                      0x00000000
#define   IGMP_ALL_HOSTS_GROUP                 0xE0000001
#define   IGMP_ALL_ROUTERS_GROUP               0xE0000002

#define   IGMP_GROUP_INFO_NOTIFICATION_SIZE             9

#define   IGMP_MAX_REGISTER                            3 
#define   IGMP_PACKET_START_OFFSET                     34

#define   IGMP_PACKET_SIZE                              8 
#define   IGMP_PACKET_QUERY                          0x11
#define   IGMP_PACKET_V1_REPORT                      0x12
#define   IGMP_PACKET_V2_REPORT                      0x16
#define   IGMP_PACKET_LEAVE                          0x17
#define   IGMP_PACKET_V3_REPORT                0x22
#define   IGMP_V3_HDR_LEN                            8 
#define   IGMP_ETH_HDR_LEN                    14
#define   IGMP_IP_HDR_RTR_OPT_LEN                    24 
#define   IGMP_IP_RTR_OPT_VERS_AND_HLEN              0x46
#define   IGMP_TOS                                   0xc0
#define   IGMP_IP_DF_BIT                             0x0001 /* Set DF bit */
#define   IGMP_IP_DF                                 0x4000
#define   IGMP_IP_CKSUM_OFFSET                 10

#define   IGMP_DATA_START_OFFSET  (IGMP_IP_HDR_RTR_OPT_LEN +\
                                   IGMP_V3_HDR_LEN) /*24 + 8*/
#define   IGMP_MAX_PKT_SIZE       1500 - IGMP_DATA_START_OFFSET  
#define   MAX_MCAST_GRPS          (MAX_IGMP_MCAST_GRPS + MAX_MLD_MCAST_GRPS)
    
#ifdef L2RED_WANTED
#define IGMP_GET_NODE_STATUS()  gIgmpRedGblInfo.u1NodeStatus
#else
#define IGMP_GET_NODE_STATUS()  RM_ACTIVE
#endif

#ifdef L2RED_WANTED
#define IGMP_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define IGMP_GET_RMNODE_STATUS()  RM_ACTIVE
#endif


/* Group Compatibility Mode */
#define   IGMP_GCM_IGMPV3                               3
#define   IGMP_GCM_IGMPV2                               2
#define   IGMP_GCM_IGMPV1                               1

#define   IGMP_IF_STATUS_CHANGE                     1
#define   IGMP_IFACE_ADMIN_UP                       1     
#define   IGMP_IFACE_ADMIN_DOWN                     2  
#define   IGMP_IFACE_OPER_UP                        1     
#define   IGMP_IFACE_OPER_DOWN                      2  
#define   IGMP_IFACE_UP                             1
#define   IGMP_IFACE_DOWN                           2
#define   IGMP_INTERFACE_INVALID   IGMP_INVALID


/* Group Filter Modes */
#define   IGMP_FMODE_INCLUDE   1
#define   IGMP_FMODE_EXCLUDE   2

/* Group Record Types */
#define IGMP_GRP_MODE_IS_INCLUDE  1
#define IGMP_GRP_MODE_IS_EXCLUDE  2
#define IGMP_GRP_CHANGE_TO_INCLUDE_MODE  3
#define IGMP_GRP_CHANGE_TO_EXCLUDE_MODE  4
#define IGMP_GRP_ALLOW_NEW_SOURCES  5
#define IGMP_GRP_BLOCK_OLD_SOURCES  6

/* MACROS */
#define   IGMP_START_OF_MCAST_GRP_ADDR           0xe0000100
#define   IGMP_END_OF_MCAST_GRP_ADDR             0xefffffff
#define   IGMP_INVALID_SSM_GRP_ADDR          0xe8000000
#define   IGMP_START_OF_SSM_ONLY_GRP_ADDR        0xe8000000
#define   IGMP_END_OF_SSM_ONLY_GRP_ADDR          0xe8ffffff
#define   IGMP_NON_SSM_RANGE                     2
#define   IGMP_SSM_RANGE                         1

#define   IGMP_Q_PID                gIgmpMemPool.IgmpQPoolId.PoolId

#define   IGMP_HIWORD(u4Dword)          (u4Dword >> 16)
#define   IGMP_LOWORD(u4Dword)          (u4Dword & 0xffff)

#define   IGMP_MAX_LOGICAL_INTERFACES   FsIGMPSizingParams\
                                    [MAX_IGMP_LOGICAL_IFACES_SIZING_ID].\
                                     u4PreAllocatedUnits

#define   IGMP_HASH_CAST(Id)            (*(UINT4 *)&(Id))

#define IGMP_TMR_SET           1
#define IGMP_TMR_RESET         0
#define IGMP_MAX_TIMER_COUNTS  20
#define IGMP_MAX_RM_MSGS       40
#define IGMP_MAX_Q_MSGS        40
/* This portion is commented as sizing wanted switch is more specific
 * to futureIP stack. And it is not useful at this point.
 */

#define IGMP_MAX_MULTICAST_GROUPS    FsIGMPSizingParams\
                                     [MAX_IGMP_MCAST_GRPS_SIZING_ID].\
                                     u4PreAllocatedUnits

#define IGMP_MAX_MULTICAST_SOURCES    FsIGMPSizingParams\
                                      [MAX_IGMP_MCAST_SRCS_SIZING_ID].\
                                      u4PreAllocatedUnits

#define IGMP_MAX_SRCS_FOR_MRP_PER_GROUP FsIGMPSizingParams\
                                  [MAX_IGMP_SRCS_FOR_MRP_PER_GROUP_SIZING_ID].\
                                        u4PreAllocatedUnits 

#define IGMP_MAX_REPORTERS_PER_SOURCE         10

#define IGMP_TIMER_SET                         1
#define IGMP_TIMER_RESET                       0

/* The default query-interval is 125 seconds */
#define   IGMP_DEFAULT_QUERY_INTERVAL                 125
#define   IGMP_IFACE_MIN_QUERY_INTERVAL               1
#define   IGMP_IFACE_MAX_QUERY_INTERVAL               31744
#define   IGMP_NO_QUERY_INTERVAL                      0
/* The default IGMP version is 2 */
#define   IGMP_DEFAULT_VERSION                          2

/* This one is for IGMPV2 and IGMPV3 
 * The value is in tenths of seconds and the default
 * value is 100 (10 seconds)
 */
#define   IGMP_DEFAULT_MAX_RESP_TIME                  100
/* Minimum value for query-min-response-time */ 
#define   IGMP_MIN_QRY_MAX_RESP_TIME                    1
/* Maximum value for query-max-response-time */ 
#define   IGMP_MAX_QRY_MAX_RESP_TIME                  255
#define   IGMP_NO_MAX_RESP_TIME                  0
/* The default Robustness variable value */
#define   IGMP_DEFAULT_ROB_VARIABLE                     2
/* Minimum value for Robustness variable */ 
/* As per RFC 3376 Section 8.1 : The Robustness Variable MUST NOT be zero
 * , and SHOULD NOT be one.*/
#define   IGMP_MIN_ROB_VARIABLE                         2
/* Maximum value for robustness variable */ 
#define   IGMP_MAX_ROB_VARIABLE                         7
#define   IGMP_NO_ROB_VARIABLE                     0

/* Maximum value QRV field: section 4.1.6 */ 
#define   IGMP_QRV_MAX_VALUE                             7
/* Supress Router-Side Processing bit: section 4.1.5 */ 
#define   IGMP_S_FLAG                                    8
 
/* This one is for IGMPV2 and IGMPV3 
 * The value is in tenths of seconds and the default
 * value is 10 (1 second)
 */
#define   IGMP_DEFAULT_LAST_MEMBER_QIVAL               10
/* Minimum value for last-member-query-interval */ 
#define   IGMP_MIN_LAST_MEMBER_QIVAL                    1
/* Maximum value for last-member-query-interval */ 
#define   IGMP_MAX_LAST_MEMBER_QIVAL                  255
#define   IGMP_NO_LAST_MEMBER_QIVAL               0
/*default rate limit value for interface */
#define   IGMP_DEFAULT_RATE_LIMIT 0
#define   IGMP_DEFAULT_INT_RATE_LIMIT 0
#define   IGMP_INTERFACE_MIN_JOIN_RATE  100
#define   IGMP_INTERFACE_MAX_JOIN_RATE  1000

/*HA Group Re-Interval*/ 
#define IGMP_MAX_GRP_RESTART_TIME 50

/* Robustness variable: section 8.1 */
/* Gets the Robustness variable */
#define IGMP_IF_ROB_VARIABLE(pIfNode) pIfNode->u1Robustness

/* Query Interval: section 8.2 */
/* Gets the Query interval and its value is in seconds */
#define IGMP_IF_QUERY_INTERVAL(pIfNode) pIfNode->u2QueryInterval

/* Query Response Interval: section 8.3 */
/* Gets the Query response interval and its value is in tenths of seconds */
#define IGMP_IF_QUERY_RESPONSE_INTERVAL(pIfNode) (pIfNode->u2MaxRespTime)

/* Group Membership interval: section 8.4 */
/* The Query response interval is in tenths of seconds, converted into 
 * seconds, so the group-membership-interval value is in seconds
 */
#define IGMP_IF_GROUP_TIMER(pIfNode) \
           ((IGMP_IF_ROB_VARIABLE(pIfNode)) * \
         (IGMP_IF_QUERY_INTERVAL(pIfNode))) + \
      (IGMP_IF_QUERY_RESPONSE_INTERVAL(pIfNode)/IGMP_TEN)

/* Other Querier Present Interval: section 8.5 */
/* The Query response interval is in tenths of seconds, converted into 
 * seconds, so the Other-querier-present-interval value is in seconds
 */
#define IGMP_IF_OTHER_QUERIER_PRESENT_TIMER(pIfNode) \
           ((IGMP_IF_ROB_VARIABLE(pIfNode)) * \
             (IGMP_IF_QUERY_INTERVAL(pIfNode))) + \
               ((IGMP_IF_QUERY_RESPONSE_INTERVAL(pIfNode)/IGMP_TEN)/IGMP_TWO) \

/* Startup Query Interval: 8.6 */
/* Gets the startup-query-interval and its value is in seconds */
#define   IGMP_STARTUP_QIVAL(pIfNode) \
             (IGMP_IF_QUERY_INTERVAL(pIfNode)/IGMP_FOUR)

/* Gets the LMQT value for a given interface node*/
#define   IGMP_LMQT(pIfNode) \
            (((IGMP_IF_LAST_MEMBER_QIVAL (pIfNode) / IGMP_TEN) * \
             IGMP_IF_LAST_MEMBER_QRY_COUNT (pIfNode)) * 100)

/* Startup Query Count: 8.7 */
/* Gets the startup-query-count */
#define   MAX_IF_STARTUP_QUERIES(pIfNode)   IGMP_IF_ROB_VARIABLE(pIfNode)

/* Last Member Query Interval: 8.8 */
/* Gets the last-member-query-interval and its value is in tenths of 
 * seconds
 */ 
#define IGMP_IF_LAST_MEMBER_QIVAL(pIfNode)  (pIfNode->u4LastMemberQueryIntvl)

/* Last Member Query Count: 8.9 */
/* Gets the last-query-count */
#define   IGMP_IF_LAST_MEMBER_QRY_COUNT(pIfNode) IGMP_IF_ROB_VARIABLE(pIfNode)

/* Last Member Query Time: 8.10 */
/* The Query response interval is in tenths of seconds, converted into 
 * seconds, so the last-member-query-time value is in seconds
 */
#define   IGMP_LAST_MEMBER_QUERY_TIME(pIfNode)     \
             ((IGMP_IF_LAST_MEMBER_QIVAL(pIfNode))/IGMP_TEN) * \
                  IGMP_IF_LAST_MEMBER_QRY_COUNT(pIfNode)

/* Older Host Present Interval: 8.13 */
/* The Query response interval is in tenths of seconds, converted into 
 * seconds, so the older-host-present-interval value is in seconds
 */
#define IGMP_IF_OLDER_HOST_PRESENT_INTERVAL(pIfNode) \
           ((IGMP_IF_ROB_VARIABLE(pIfNode)) * \
         (IGMP_IF_QUERY_INTERVAL(pIfNode))) + \
      (IGMP_IF_QUERY_RESPONSE_INTERVAL(pIfNode)/IGMP_TEN)

#define   MAX_IF_GP_SP_QUERIES(pIfNode) \
                   ((IGMP_IF_ROB_VARIABLE(pIfNode)) - IGMP_ONE)
#define   MAX_IF_GRP_SRC_SP_QUERIES(pIfNode) \
                   ((IGMP_IF_ROB_VARIABLE(pIfNode)) - IGMP_ONE)


#define IGMP_REGISTER_WITH_IP          NetIpv4RegisterHigherLayerProtocol
#define IGMP_DEREGISTER_WITH_IP        NetIpv4DeRegisterHigherLayerProtocol 
 
#define IGMP_IS_FAST_LEAVE_ENABLED(pIfaceNode) \
        (pIfaceNode->u1FastLeaveFlg == IGMP_FAST_LEAVE_ENABLE)
#define IGMP_IS_CHANNEL_TRACK_ENABLED(pIfaceNode) \
        (pIfaceNode->u1ChannelTrackFlg == IGMP_CHANNEL_TRACK_ENABLE)
/*------------------------------------------------------------------*/
/* macro to generate random value. x is the max value */
#define IGMP_RAND(x) (1 + (RAND() % ((x) -1)))

#define IGMP_GET_TIME_IN_SEC(Time) { \
     (Time) = ((Time) / (SYS_TIME_TICKS_IN_A_SEC)); \
}

#define  IGMPSTARTTIMER(Timer, u1TimerId, u4Duration,u4MilliSec) \
{ \
    if (IGMP_GET_NODE_STATUS() == RM_ACTIVE) \
    {\
     if (TmrStart(gIgmpTimerListId, &(Timer.TimerBlk),\
                 u1TimerId, u4Duration, u4MilliSec) != TMR_SUCCESS) \
     {\
            Timer.u1TmrStatus =  IGMP_TMR_RESET;\
     }\
     else\
     {\
            Timer.u1TmrStatus = IGMP_TMR_SET;\
     }\
    }\
}

#define  IGMPSTOPTIMER(Timer) \
{ \
         TmrStop (gIgmpTimerListId, &(Timer.TimerBlk));\
         Timer.u1TmrStatus = IGMP_TMR_RESET ;\
}

#define IGMP_RESTART_TIMER(TimerListId, TmrRef, u1TimerId, u4Duration)   \
{\
   UINT4 u4RemainTime = 0; \
   UINT4 u4RetValue = 0; \
   UNUSED_PARAM (u4RetValue); \
   TmrGetRemainingTime (gIgmpTimerListId, &(TmrRef.TimerBlk.TimerNode),\
                        &u4RemainTime); \
   if (u4RemainTime != 0) {  \
      TmrStop (TimerListId, &(TmrRef.TimerBlk));   \
   } \
   if (u4Duration != 0) { \
      u4RetValue = TmrStart (TimerListId, &(TmrRef.TimerBlk), u1TimerId, u4Duration, 0);\
   } \
}

#define  IGMPPROCESSV1REPORT(pIfNode, u4Group, u4Src) \
         IgmpProcessGroup(pIfNode, u4Group, IGMP_VERSION_1, u4Src)

/* FSL: IGMP Packet flow directions */
#ifdef DEBUG_WANTED
#define IGMP_PKT_FLOW_IN    1
#define IGMP_PKT_FLOW_OUT   2
#endif

#define IGMP_GET_CODE_FROM_TIME(Time, Code)                     \
{                                                               \
   UINT1   u1Code = IGMP_ZERO;                                  \
   UINT2   u2CodeVal = IGMP_ZERO;                               \
   for(u1Code = 0x80; u1Code < 0xff; u1Code++)                  \
   {                                                            \
        u2CodeVal = (UINT2) IGMP_GET_MRT_FROM_MRC(u1Code);      \
        if (u2CodeVal >= Time)                                  \
        {                                                       \
           Code = u1Code;                                       \
           break;                                               \
        }                                                       \
   }                                                            \
   if (u2CodeVal < Time)                                        \
   {                                                            \
        Code = u1Code;                                          \
   }                                                            \
}
          
          
 
/* Macro for calculating Max. Response Time from Max. Response Code */
#define IGMP_GET_MRT_FROM_MRC(u1Code) ( ((u1Code & 0x0F) | 0x10) << (((u1Code & 0x70) >> 4) + 3) )  
/* Macro for calculating Querier's Query Interval from QQI Code */
#define IGMP_GET_QQI_FROM_QQIC(u1Code) ( ((u1Code & 0x0F) | 0x10) << (((u1Code & 0x70) >> 4) + 3) ) 

/* Macro for calculating Max. Response Code from Max. Response Time */
#define IGMP_GET_MRC_FROM_TIME(u4Time) 

/*Macro to check whether the group lies in SSM range*/
#define IGMP_CHK_IF_SSM_RANGE(u4GrpAddr, retVal)\
{ \
   retVal = IGMP_NON_SSM_RANGE;\
   if((u4GrpAddr > IGMP_START_OF_SSM_ONLY_GRP_ADDR)\
        && (u4GrpAddr <= IGMP_END_OF_SSM_ONLY_GRP_ADDR))\
    {\
        retVal = IGMP_SSM_RANGE;\
     }\
}

#define IGMP_QRV(x) ( (x)->u1ResvSupRV & (0x07) )
#define IGMP_SRSP(x) ( (x)->u1ResvSupRV & (0x08) )
#define IGMP_QQIC(x) ( (x)->u1QQIC) 

#define IGMP_GET_IF_HASHINDEX(u4IfIndex, u4HashIndex) \
     u4HashIndex = (u4IfIndex % gIgmpIfInfo.u4HashSize)

/* macro to find base address of structure given the member address */ 
#define IGMP_GET_BASE_PTR(type, memberName,pMember) \
              (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName) )

#define IGMP_GET_IF_NODE(IfIndex, u1AddrType)  \
              IgmpGetInterfaceNode ((UINT4)IfIndex, u1AddrType)
#define IGMP_DENY            0
#define IGMP_PERMIT          1

#define  IGMP_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

#define IGMP_NOT_A_NEW_GROUP                    1
#define IGMP_NEW_GROUP                          2
#define IGMP_GROUP_DEGRADED                     3
#define IGMP_GROUP_DELETION                     4
#define IGMP_GROUP_UPGRADED                     5
#define IGMP_PROXY_STATUS()                     gIgmpConfig.i4ProxyStatus
#define IGMP_SSM_MAP_GLOBAL_STATUS()            gIgmpConfig.u1IgmpSSMMapStatus

#define IGMP_GLOBAL_STATUS()                    gIgmpConfig.u1IgmpStatus
/* Statistic Macros */        
#define IGMP_GEN_QUERY_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4RxGenQueries++; \
    if (pIgmpIfaceNode->u4RxGenQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4RxGenQueries = 0; \
    } \
    pIgmpIfaceNode->u4InQueries++; \
    if (pIgmpIfaceNode->u4InQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InQueries = 0; \
    } \
    pIgmpIfaceNode->u4InPkts++; \
    if (pIgmpIfaceNode->u4InPkts == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InPkts = 0; \
    } \
}

#define IGMP_GEN_QUERY_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4TxGenQueries++; \
    if (pIgmpIfaceNode->u4TxGenQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4TxGenQueries = 0; \
    } \
    pIgmpIfaceNode->u4OutQueries++; \
    if (pIgmpIfaceNode->u4OutQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4OutQueries = 0; \
    } \
}

#define IGMP_GRP_QUERY_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4RxGrpQueries++; \
    if (pIgmpIfaceNode->u4RxGrpQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4RxGrpQueries = 0; \
    } \
    pIgmpIfaceNode->u4InQueries++; \
    if (pIgmpIfaceNode->u4InQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InQueries = 0; \
    } \
    pIgmpIfaceNode->u4InPkts++; \
    if (pIgmpIfaceNode->u4InPkts == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InPkts = 0; \
    } \
}

#define IGMP_GRP_QUERY_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4TxGrpQueries++; \
    if (pIgmpIfaceNode->u4TxGrpQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4TxGrpQueries = 0; \
    } \
    pIgmpIfaceNode->u4OutQueries++; \
    if (pIgmpIfaceNode->u4OutQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4OutQueries = 0; \
    } \
}

#define IGMP_GRP_SRC_QUERY_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4RxGrpSrcQueries++; \
    if (pIgmpIfaceNode->u4RxGrpSrcQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4RxGrpSrcQueries = 0; \
    } \
    pIgmpIfaceNode->u4InQueries++; \
    if (pIgmpIfaceNode->u4InQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InQueries = 0; \
    } \
    pIgmpIfaceNode->u4InPkts++; \
    if (pIgmpIfaceNode->u4InPkts == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InPkts = 0; \
    } \
}

#define IGMP_GRP_SRC_QUERY_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4TxGrpSrcQueries++; \
    if (pIgmpIfaceNode->u4TxGrpSrcQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4TxGrpSrcQueries = 0; \
    } \
    pIgmpIfaceNode->u4OutQueries++; \
    if (pIgmpIfaceNode->u4OutQueries == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4OutQueries = 0; \
    } \
}

#define IGMP_V1V2REP_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4IgmpRxv1v2Reports++; \
    if (pIgmpIfaceNode->u4IgmpRxv1v2Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4IgmpRxv1v2Reports = 0; \
    } \
    pIgmpIfaceNode->u4InJoins++; \
    if (pIgmpIfaceNode->u4InJoins == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InJoins = 0; \
    } \
}

#define IGMP_V1V2REP_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4IgmpTxv1v2Reports++; \
    if (pIgmpIfaceNode->u4IgmpTxv1v2Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4IgmpTxv1v2Reports = 0; \
    } \
}

#define IGMP_V3REP_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4IgmpRxv3Reports++; \
    if (pIgmpIfaceNode->u4IgmpRxv3Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4IgmpRxv3Reports = 0; \
    } \
    pIgmpIfaceNode->u4InJoins++; \
    if (pIgmpIfaceNode->u4InJoins == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InJoins = 0; \
    } \
}

#define IGMP_V3REP_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4IgmpTxv3Reports++; \
    if (pIgmpIfaceNode->u4IgmpTxv3Reports == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4IgmpTxv3Reports = 0; \
    } \
}

#define IGMP_LEAVE_RX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4InLeaves++; \
    if (pIgmpIfaceNode->u4InLeaves == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4InLeaves = 0; \
    } \
}

#define IGMP_LEAVE_TX(pIgmpIfaceNode) \
{\
    pIgmpIfaceNode->u4Txv2Leaves++; \
    if (pIgmpIfaceNode->u4Txv2Leaves == 0xFFFFFFFF) \
    { \
        pIgmpIfaceNode->u4Txv2Leaves = 0; \
    } \
}


#define IGMP_MEM_ALLOCATE(PoolId,pu1Block,type) \
        (pu1Block = (type *) IgmpMemAllocMemBlk (PoolId,sizeof(type)))\

/* IPvX original macros */
#define IGMP_IPVX_ADDR_FMLY_IPV4       IPVX_ADDR_FMLY_IPV4
#define IGMP_IPVX_ADDR_COPY            IPVX_ADDR_COPY
#define IGMP_IPVX_ADDR_CLEAR           IPVX_ADDR_CLEAR
#define IGMP_IPVX_ADDR_INIT_IPV4       IPVX_ADDR_INIT_IPV4
#define IGMP_IPVX_ADDR_INIT_IPV6       IPVX_ADDR_INIT_IPV6
#define IGMP_IPVX_ADDR_COMPARE         IPVX_ADDR_COMPARE
#define IPV4_MASK_TO_MASKLENX          IPV4_MASK_TO_MASKLEN
#define IPV4_MASKLEN_TO_MASKX          IPV4_MASKLEN_TO_MASK
#define IGMP_IP_COPY_FROM_IPVX(pIpAddr,IPvXAddr,u1Afi)\
{\
    if (u1Afi == IGMP_IPVX_ADDR_FMLY_IPV4)\
    {\
       MEMCPY (pIpAddr, IPvXAddr.au1Addr, sizeof (UINT4));\
    }\
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)\
    {\
       MEMCPY (pIpAddr, IPvXAddr.au1Addr, sizeof (tIp6Addr));\
    }\
}

/* End:  IPvX original macros */

#define  IGMP_IPV6_THIRD_WORD_OFFSET   12

/* added for fsmgmd, stdmgmd and cli */
/*Copies the Octet string to IPvX address */

#define IGMP_COPY_OCTET_TO_IPVX(IPvXAddr,pOctetString,u1AFI)\
{\
    if (u1AFI == IPVX_ADDR_FMLY_IPV4)\
    {\
        IPvXAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;\
        IPvXAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;\
        MEMCPY (IPvXAddr.au1Addr, pOctetString->pu1_OctetList,\
                IPVX_IPV4_ADDR_LEN);\
    }\
    else if (u1AFI == IPVX_ADDR_FMLY_IPV6)\
    {\
        IPvXAddr.u1Afi = IPVX_ADDR_FMLY_IPV6;\
        IPvXAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;\
        MEMCPY (IPvXAddr.au1Addr, pOctetString->pu1_OctetList,\
                IPVX_IPV6_ADDR_LEN);\
    }\
}
/* copies the IPvX address into SNMP octect string */

#define IGMP_COPY_IPVX_TO_OCTET(pOctetString,IPvXAddr,u1Afi)\
{\
    if (u1Afi == IPVX_ADDR_FMLY_IPV4)\
    {\
        MEMCPY (pOctetString->pu1_OctetList, IPvXAddr.au1Addr, \
                IPVX_IPV4_ADDR_LEN);\
        pOctetString->i4_Length = IPVX_IPV4_ADDR_LEN;\
    }\
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)\
    {\
        MEMCPY (pOctetString->pu1_OctetList, IPvXAddr.au1Addr, \
                IPVX_IPV6_ADDR_LEN);\
        pOctetString->i4_Length = IPVX_IPV6_ADDR_LEN;\
    }\
}

#define IGMP_MAX_PRINT_LEN 20

/* HA Specific info */
#define IGMP_RB_GREATER  1
#define IGMP_RB_LESSER        -1
#define IGMP_RB_EQUAL   0

#define NP_PRESENT  1
#define NP_NOT_PRESENT  2

#define IGMP_ADD_GROUP  1
#define IGMP_DELETE_GROUP 2

#define IGMP_ADD_SOURCE  1
#define IGMP_DELETE_SOURCE 2

#define IGMP_ADD_REPORTER  1
#define IGMP_DELETE_REPORTER 2

#define IGMP_GROUP_REPORTER  1
#define IGMP_SOURCE_REPORTER  2

#define IGMP_RM_MSG_MEMPOOL_ID  IGMPMemPoolIds[MAX_IGMP_RM_QUE_DEPTH_SIZING_ID]
#define IGMP_RM_PKT_Q_ID  gIgmpRmPktQId

/*MLD report timer expiry*/
#define  MLD_REPORT_TMR             4096 

enum
{
    IGMP_IGNORE = 10,
    IGMP_GROUP_MATCHED,
    IGMP_GROUP_NOT_MACHED,
    IGMP_INF_LIMIT_NOT_REACHED,
    IGMP_INF_LIMIT_NOT_CONFIGURED
};
#define IGMP_INTERFACE_LIMIT_COUNTED   0x01
#define IGMP_GLOBAL_LIMIT_COUNTED      0x02

#define IGMP_MCAST_GROUP_MEMPOOL_ID  IGMPMemPoolIds[MAX_IGMP_HOST_MCAST_GROUP_SIZING_ID]
#define IGMP_CHECK_FREE_PIM_QUEUE PimIsAllowedToPostQueMsg

#define   IGMP_MCAST_START_RESD_GRP_ADDR         0xe0000000
#define   IGMP_MCAST_END_RESD_GRP_ADDR           0xe00000ff

#endif /* IGMPDEFS_H */
