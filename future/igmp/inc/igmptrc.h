/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmptrc.h,v 1.8 2015/03/25 14:25:21 siva Exp $
 *
 * Description: Igmp Trace macros 
 *
 *******************************************************************/

#ifndef _IGMP_TRACE_H_
#define _IGMP_TRACE_H_
#include "igmp.h"
#include "utltrc.h"
#include "utltrci.h"

extern UINT4 gu4IgmpTrcFlg;
extern CHR1 gacIgmpSyslogmsg[MAX_LOG_STR_LEN];

#define IGMP_MAX_TRC_LEVEL  0xffffffff

#ifdef TRACE_WANTED

#define   MGMD_TRC_FLAG     gIgmpConfig.u4IgmpTrcFlag | gIgmpConfig.u4MldTrcFlag   
#define   MLD_TRC_FLAG      gIgmpConfig.u4MldTrcFlag
#define   IGMP_TRC_FLAG     gIgmpConfig.u4IgmpTrcFlag
#define   IGMP_TRC_NAME(mode)     IgmpTrcGetModuleName(mode)   

#define SYSLOG_IGMP_MSG(level, cmod, mode,mod,fmt, arg1, arg2) \
{\
    MEMSET (gacIgmpSyslogmsg, 0, sizeof (gacIgmpSyslogmsg));\
    if((STRLEN (fmt) + STRLEN (arg1)) < MAX_LOG_STR_LEN)\
 {\
    STRCAT (gacIgmpSyslogmsg, fmt);\
    STRCAT (gacIgmpSyslogmsg, arg1);\
    UtlSysTrcLog(level, cmod, mode, IGMP_TASK_NAME, mod, gacIgmpSyslogmsg, arg2);\
 }\
}
#define SYSLOG_IGMP_MSG_ARG1(level, cmod, mode,mod,fmt, arg1) \
{\
    MEMSET (gacIgmpSyslogmsg, 0, sizeof (gacIgmpSyslogmsg));\
    if((STRLEN (fmt) + STRLEN (arg1)) < MAX_LOG_STR_LEN)\
 {\
    STRCAT (gacIgmpSyslogmsg, fmt);\
    STRCAT (gacIgmpSyslogmsg, arg1);\
    UtlSysTrcLog(level, cmod, mode, IGMP_TASK_NAME, mod, gacIgmpSyslogmsg);\
 }\
}

/* General Macros */
#define IGMP_DUMP(cmod, mode,mod,pDupBuf) \
{\
 if((cmod & (IGMP_Rx_MODULE | IGMP_Tx_MODULE | MLD_DUMPRx_MODULE| MLD_DUMPTx_MODULE)) \
  == (IGMP_Rx_MODULE | IGMP_Tx_MODULE | MLD_DUMPRx_MODULE| MLD_DUMPTx_MODULE))\
 {\
  UtlDmpMsg(cmod, 0, pDupBuf, cmod, 0, IGMP_ALL_MODULES, FALSE);\
 }\
}

#define IGMP_TRC(cmod, mode,mod,fmt) \
{\
        if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt);\
        }\
}

#define IGMP_TRC_ARG1(cmod,mode,  mod, fmt, arg1) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, arg1);\
 }\
}
#define IGMP_TRC_ARG2( cmod, mode, mod, fmt,arg1,arg2) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, arg1, arg2);\
 }\
}
#define IGMP_TRC_ARG3( cmod, mode, mod, fmt,arg1,arg2,arg3) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2, arg3);\
 }\
}

#define IGMP_TRC_ARG4( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2, arg3, arg4);\
 }\
}

#define IGMP_TRC_ARG5( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4,arg5) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2,arg3, arg4, arg5);\
 }\
}

#define IGMP_TRC_ARG6( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
 {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2,arg3, arg4, arg5, arg6);\
 }\
}

#define MLD_TRC(cmod, mode,mod,fmt) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt);\
 }\
}
#define MLD_TRC_ARG1(cmod,mode,  mod, fmt, arg1) \
{\
       if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, arg1);\
 }\
}

#define MLD_TRC_ARG2( cmod, mode, mod, fmt,arg1,arg2) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, arg1, arg2);\
 }\
}

#define MLD_TRC_ARG3( cmod, mode, mod, fmt,arg1,arg2,arg3) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2,arg3);\
 }\
}

#define MLD_TRC_ARG4( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2,arg3, arg4);\
 }\
}

#define MLD_TRC_ARG5( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4,arg5) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2,arg3, arg4, arg5);\
 }\
}

#define MLD_TRC_ARG6( cmod, mode, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2,arg3, arg4, arg5, arg6);\
 }\
}
 
#define MGMD_TRC1(cmod, mode,mod,fmt, arg1) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, arg1);\
 }\
}

#define MGMD_TRC2(cmod, mode,mod,fmt, arg1, arg2) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2);\
 }\
}

#define MGMD_TRC3(cmod, mode,mod,fmt, arg1, arg2, arg3) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2, arg3);\
 }\
}


#define MGMD_TRC4(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2, arg3, arg4);\
 }\
}

#define MGMD_TRC5(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4, arg5) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2, arg3, arg4, arg5);\
 }\
}


#define MGMD_TRC6(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4, arg5, arg6) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, cmod, mode, IGMP_TASK_NAME, mod, fmt, \
   arg1, arg2, arg3, arg4, arg5, arg6);\
 }\
}

#define   MGMD_DBG_FLAG     gIgmpConfig.u4IgmpDbgFlag | gIgmpConfig.u4MldDbgFlag
#define   MLD_DBG_FLAG      gIgmpConfig.u4MldDbgFlag
#define   IGMP_DBG_FLAG     gIgmpConfig.u4IgmpDbgFlag
#define   IGMP_NAME         IgmpGetModuleName(u2IgmpTrcModule)

#define MGMD_DBG(Flg, Fmt) 


#define MGMD_DBG_EXT(Flg,Value, modname, Fmt) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, Value, IGMP_TASK_NAME, modname,Fmt);\
 }\
}
#define MGMD_DBG_NP(Flg,Value, modname, Fmt, arg1) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, Value, IGMP_TASK_NAME, modname,Fmt, arg1);\
 }\
}
#define MGMD_DBG_INFO(Flg,Value, modname, Fmt) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname,Fmt);\
 }\
}


#define MGMD_DBG1(Flg, Value, modname, Fmt, Arg1) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg,u2IgmpTrcModule, IGMP_TASK_NAME, modname, Fmt, Arg1);\
 }\
}

#define MGMD_DBG2(Flg, Value, modname, Fmt, Arg1, Arg2) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg,u2IgmpTrcModule, IGMP_TASK_NAME, modname, Fmt, Arg1, Arg2);\
 }\
}

#define MGMD_DBG3(Flg, Value, modname, Fmt,Arg1, Arg2, Arg3) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg,u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3);\
 }\
}

#define MGMD_DBG4(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule,  IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4);\
 }\
}

#define MGMD_DBG5(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4, Arg5);\
 }\
}


#define MGMD_DBG6(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
 }\
}
#define MLD_DBG(Flg, Fmt) 
#define MLD_DBG_INFO(Flg,Value, modname, Fmt) \
{\
        if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, Value, IGMP_TASK_NAME, modname,Fmt);\
 }\
}

#define MLD_DBG1(Flg, Value, modname, Fmt, Arg1) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, Fmt, Arg1);\
 }\
}

#define MLD_DBG2(Flg, Value, modname, Fmt, Arg1, Arg2) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2);\
 }\
}

#define MLD_DBG3(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3) \
{\
        if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3);\
 }\
}

#define MLD_DBG4(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4);\
 }\
}

#define MLD_DBG5(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
{\
   if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname,\
      Fmt, Arg1, Arg2, Arg3, Arg4, Arg5);\
 }\
}

#define MLD_DBG6(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
 }\
}

#define IGMP_DBG(Value , Fmt) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
 UtlSysTrcLog(SYSLOG_INVAL_LEVEL,IGMP_DBG_FLAG, Value, IGMP_TASK_NAME, "IGMP", Fmt);\
 }\
}

#define IGMP_DBG_INFO(Flg,Value, modname, Fmt) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL,Flg, Value, IGMP_TASK_NAME, modname, Fmt);\
 }\
}

#define IGMP_DBG1(Flg, Value, modname, Fmt, Arg1) \
{\
        if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL,Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, Fmt, Arg1);\
 }\
}
 
#define IGMP_DBG2(Flg, Value, modname, Fmt, Arg1, Arg2) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, Fmt, Arg1, Arg2);\
 }\
}

#define IGMP_DBG3(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname,\
    Fmt, Arg1, Arg2, Arg3);\
 }\
}

#define IGMP_DBG4(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4);\
 }\
}

#define IGMP_DBG5(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4, Arg5);\
 }\
}

#define IGMP_DBG6(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
{\
 if(IGMP_GET_NODE_STATUS () !=RM_STANDBY)\
        {\
        UtlSysTrcLog(SYSLOG_INVAL_LEVEL, Flg, u2IgmpTrcModule, IGMP_TASK_NAME, modname, \
   Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);\
 }\
}


#else

#define SYSLOG_IGMP_MSG(level, cmod, mode,mod,fmt, arg1, arg2)
#define SYSLOG_IGMP_MSG_ARG1(level, cmod, mode,mod,fmt, arg1)
#define IGMP_DUMP(cmod, mode,mod,pDupBuf)

#define IGMP_TRC(cmod,mode, mod, fmt)
#define IGMP_TRC_ARG1(cmod,mode, mod, fmt,arg1)
#define IGMP_TRC_ARG2(cmod,mode, mod, fmt,arg1,arg2)
#define IGMP_TRC_ARG3(cmod,mode, mod, fmt,arg1,arg2,arg3)
#define IGMP_TRC_ARG4(cmod,mode, mod, fmt,arg1,arg2,arg3,arg4)
#define IGMP_TRC_ARG5(cmod,mode, mod, fmt,arg1,arg2,arg3,arg4,arg5)
#define IGMP_TRC_ARG6(cmod,mode, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#define MLD_TRC(cmod, mode,mod,fmt)
#define MLD_TRC_ARG1(cmod, mode,mod,fmt, arg1)
#define MLD_TRC_ARG2(cmod, mode,mod,fmt, arg1, arg2)
#define MLD_TRC_ARG3(cmod, mode,mod,fmt, arg1, arg2, arg3)
#define MLD_TRC_ARG4(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4)
#define MLD_TRC_ARG5(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4, arg5)
#define MLD_TRC_ARG6(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4, arg5, arg6)

#define MGMD_TRC(cmod, mode,mod,fmt)
#define MGMD_TRC1(cmod, mode,mod,fmt, arg1)
#define MGMD_TRC2(cmod, mode,mod,fmt, arg1, arg2)
#define MGMD_TRC3(cmod, mode,mod,fmt, arg1, arg2, arg3)
#define MGMD_TRC4(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4)
#define MGMD_TRC5(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4, arg5)
#define MGMD_TRC6(cmod, mode,mod,fmt, arg1, arg2, arg3, arg4, arg5, arg6)


#define MGMD_DBG(Value, Fmt)
#define MGMD_DBG_INFO(Flg, Value, modname, Fmt)
#define MGMD_DBG_EXT(Flg, Value, modname, Fmt)
#define MGMD_DBG_NP(Flg,Value, modname, Fmt, arg1)
#define MGMD_DBG1(Flg, Value, modname, Fmt, Arg1)
#define MGMD_DBG2(Flg, Value, modname, Fmt, Arg1, Arg2)
#define MGMD_DBG3(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3)
#define MGMD_DBG4(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4)
#define MGMD_DBG5(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define MGMD_DBG6(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)


#define MLD_DBG(Value, Fmt)
#define MLD_DBG_INFO(Flg, Value, modname, Fmt)
#define MLD_DBG1(Flg, Value, modname, Fmt, Arg1)
#define MLD_DBG2(Flg, Value, modname, Fmt, Arg1, Arg2)
#define MLD_DBG3(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3)
#define MLD_DBG4(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4)
#define MLD_DBG5(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define MLD_DBG6(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)


#define IGMP_DBG(Value, Fmt)
#define IGMP_DBG_INFO(Flg, Value, modname, Fmt)
#define IGMP_DBG1(Flg, Value, modname, Fmt, Arg1)
#define IGMP_DBG2(Flg, Value, modname, Fmt, Arg1, Arg2)
#define IGMP_DBG3(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3)
#define IGMP_DBG4(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4)
#define IGMP_DBG5(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define IGMP_DBG6(Flg, Value, modname, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#endif

#define IGMP_DBG_ENTRY                  0x00010000
#define IGMP_DBG_EXIT                   0x00020000
#define IGMP_DBG_MEM                    0x00040000
#define IGMP_DBG_FAILURE                0x00080000
#define IGMP_DBG_MGMT                   UTL_DBG_SNMP_IF

#define IGMP_INIT_SHUT_TRC     INIT_SHUT_TRC
#define IGMP_MGMT_TRC          MGMT_TRC
#define IGMP_DATA_PATH_TRC     DATA_PATH_TRC
#define IGMP_CONTROL_PATH_TRC  CONTROL_PLANE_TRC
#define IGMP_DUMP_TRC          DUMP_TRC
#define IGMP_DUMPTx_TRC          DUMPTx_TRC
#define IGMP_DUMPRx_TRC          DUMPRx_TRC
#define IGMP_OS_RESOURCE_TRC   OS_RESOURCE_TRC
#define IGMP_ALL_FAILURE_TRC   ALL_FAILURE_TRC
#define IGMP_BUFFER_TRC        BUFFER_TRC

#define MLD_INIT_SHUT_TRC     INIT_SHUT_TRC
#define MLD_DATA_PATH_TRC     DATA_PATH_TRC
#define MLD_MGMT_TRC          MGMT_TRC
#define MLD_CONTROL_PATH_TRC  CONTROL_PLANE_TRC
#define MLD_DUMP_TRC          DUMP_TRC
#define MLD_DUMPTx_TRC          DUMPTx_TRC
#define MLD_DUMPRx_TRC          DUMPRx_TRC
#define MLD_OS_RESOURCE_TRC   OS_RESOURCE_TRC
#define MLD_ALL_FAILURE_TRC   ALL_FAILURE_TRC
#define MLD_BUFFER_TRC        BUFFER_TRC

#define MGMD_INIT_SHUT_TRC     INIT_SHUT_TRC
#define MGMD_DATA_PATH_TRC     DATA_PATH_TRC
#define MGMD_MGMT_TRC          MGMT_TRC
#define MGMD_CONTROL_PATH_TRC  CONTROL_PLANE_TRC
#define MGMD_DUMP_TRC          DUMP_TRC
#define MLD_DUMPRx_TRC          DUMPRx_TRC
#define MLD_DUMPTx_TRC          DUMPTx_TRC
#define MGMD_OS_RESOURCE_TRC   OS_RESOURCE_TRC
#define MGMD_ALL_FAILURE_TRC   ALL_FAILURE_TRC
#define MGMD_BUFFER_TRC        BUFFER_TRC
#define MGMD_ENTRY_TRC         0x00000100
#define MGMD_EXIT_TRC          0x00000200

    
#endif /* _IGMP_TRACE_H_ */
