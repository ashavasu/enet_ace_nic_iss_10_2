/* $Id: fsigmpwr.h,v 1.9 2016/06/24 09:42:18 siva Exp $ */
#ifndef _FSIGMPWR_H
#define _FSIGMPWR_H

VOID RegisterFSIGMP(VOID);

VOID UnRegisterFSIGMP(VOID);
INT4 FsIgmpGlobalStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpDebugLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpGlobalStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpDebugLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpGlobalStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpDebugLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpGlobalStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIgmpTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIgmpDebugLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsIgmpInterfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIgmpInterfaceAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceFastLeaveStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceIncomingPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceIncomingJoinsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceIncomingLeavesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceIncomingQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceOutgoingQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceRxGenQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceRxGrpQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceRxGrpAndSrcQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceRxv1v2ReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceRxv3ReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceTxGenQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceTxGrpQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceTxGrpAndSrcQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceTxv1v2ReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceTxv3ReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceTxv2LeavesGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceChannelTrackStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceGroupListIdGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceCurGrpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceCKSumErrorGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfacePktLenErrorGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfacePktsWithLocalIPGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceSubnetCheckFailureGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceQryFromNonQuerierGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceReportVersionMisMatchGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceQryVersionMisMatchGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceUnknownMsgTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceInvalidV1ReportGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceInvalidV2ReportGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceInvalidV3ReportGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceRouterAlertCheckFailureGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceIncomingSSMPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceInvalidSSMPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceJoinPktRateGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceFastLeaveStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceChannelTrackStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceGroupListIdSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceJoinPktRateSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceFastLeaveStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceChannelTrackStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceGroupListIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceJoinPktRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpInterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsIgmpCacheTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIgmpCacheGroupCompModeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIgmpGroupListTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIgmpGrpListRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpGrpListRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpGrpListRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpGroupListTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsIgmpGlobalLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpGlobalCurGrpCountGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpSSMMapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpGlobalLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpSSMMapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpGlobalLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpSSMMapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpGlobalLimitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIgmpSSMMapStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsIgmpSSMMapGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIgmpSSMMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpSSMMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpSSMMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpSSMMapGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSIGMPWR_H */
