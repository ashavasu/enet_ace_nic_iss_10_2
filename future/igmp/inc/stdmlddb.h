/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmlddb.h,v 1.2 2010/12/17 12:44:55 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDMLDDB_H
#define _STDMLDDB_H

UINT1 MldInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 MldCacheTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER};

UINT4 stdmld [] ={1,3,6,1,2,1,91};
tSNMP_OID_TYPE stdmldOID = {7, stdmld};


UINT4 MldInterfaceIfIndex [ ] ={1,3,6,1,2,1,91,1,1,1,1};
UINT4 MldInterfaceQueryInterval [ ] ={1,3,6,1,2,1,91,1,1,1,2};
UINT4 MldInterfaceStatus [ ] ={1,3,6,1,2,1,91,1,1,1,3};
UINT4 MldInterfaceVersion [ ] ={1,3,6,1,2,1,91,1,1,1,4};
UINT4 MldInterfaceQuerier [ ] ={1,3,6,1,2,1,91,1,1,1,5};
UINT4 MldInterfaceQueryMaxResponseDelay [ ] ={1,3,6,1,2,1,91,1,1,1,6};
UINT4 MldInterfaceJoins [ ] ={1,3,6,1,2,1,91,1,1,1,7};
UINT4 MldInterfaceGroups [ ] ={1,3,6,1,2,1,91,1,1,1,8};
UINT4 MldInterfaceRobustness [ ] ={1,3,6,1,2,1,91,1,1,1,9};
UINT4 MldInterfaceLastListenQueryIntvl [ ] ={1,3,6,1,2,1,91,1,1,1,10};
UINT4 MldInterfaceProxyIfIndex [ ] ={1,3,6,1,2,1,91,1,1,1,11};
UINT4 MldInterfaceQuerierUpTime [ ] ={1,3,6,1,2,1,91,1,1,1,12};
UINT4 MldInterfaceQuerierExpiryTime [ ] ={1,3,6,1,2,1,91,1,1,1,13};
UINT4 MldCacheAddress [ ] ={1,3,6,1,2,1,91,1,2,1,1};
UINT4 MldCacheIfIndex [ ] ={1,3,6,1,2,1,91,1,2,1,2};
UINT4 MldCacheSelf [ ] ={1,3,6,1,2,1,91,1,2,1,3};
UINT4 MldCacheLastReporter [ ] ={1,3,6,1,2,1,91,1,2,1,4};
UINT4 MldCacheUpTime [ ] ={1,3,6,1,2,1,91,1,2,1,5};
UINT4 MldCacheExpiryTime [ ] ={1,3,6,1,2,1,91,1,2,1,6};
UINT4 MldCacheStatus [ ] ={1,3,6,1,2,1,91,1,2,1,7};


tMbDbEntry stdmldMibEntry[]= {

{{11,MldInterfaceIfIndex}, GetNextIndexMldInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MldInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,MldInterfaceQueryInterval}, GetNextIndexMldInterfaceTable, MldInterfaceQueryIntervalGet, MldInterfaceQueryIntervalSet, MldInterfaceQueryIntervalTest, MldInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MldInterfaceTableINDEX, 1, 0, 0, "125"},

{{11,MldInterfaceStatus}, GetNextIndexMldInterfaceTable, MldInterfaceStatusGet, MldInterfaceStatusSet, MldInterfaceStatusTest, MldInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MldInterfaceTableINDEX, 1, 0, 1, NULL},

{{11,MldInterfaceVersion}, GetNextIndexMldInterfaceTable, MldInterfaceVersionGet, MldInterfaceVersionSet, MldInterfaceVersionTest, MldInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MldInterfaceTableINDEX, 1, 0, 0, "1"},

{{11,MldInterfaceQuerier}, GetNextIndexMldInterfaceTable, MldInterfaceQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MldInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,MldInterfaceQueryMaxResponseDelay}, GetNextIndexMldInterfaceTable, MldInterfaceQueryMaxResponseDelayGet, MldInterfaceQueryMaxResponseDelaySet, MldInterfaceQueryMaxResponseDelayTest, MldInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MldInterfaceTableINDEX, 1, 0, 0, "10"},

{{11,MldInterfaceJoins}, GetNextIndexMldInterfaceTable, MldInterfaceJoinsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MldInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,MldInterfaceGroups}, GetNextIndexMldInterfaceTable, MldInterfaceGroupsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, MldInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,MldInterfaceRobustness}, GetNextIndexMldInterfaceTable, MldInterfaceRobustnessGet, MldInterfaceRobustnessSet, MldInterfaceRobustnessTest, MldInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MldInterfaceTableINDEX, 1, 0, 0, "2"},

{{11,MldInterfaceLastListenQueryIntvl}, GetNextIndexMldInterfaceTable, MldInterfaceLastListenQueryIntvlGet, MldInterfaceLastListenQueryIntvlSet, MldInterfaceLastListenQueryIntvlTest, MldInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MldInterfaceTableINDEX, 1, 0, 0, "1"},

{{11,MldInterfaceProxyIfIndex}, GetNextIndexMldInterfaceTable, MldInterfaceProxyIfIndexGet, MldInterfaceProxyIfIndexSet, MldInterfaceProxyIfIndexTest, MldInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MldInterfaceTableINDEX, 1, 0, 0, "0"},

{{11,MldInterfaceQuerierUpTime}, GetNextIndexMldInterfaceTable, MldInterfaceQuerierUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MldInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,MldInterfaceQuerierExpiryTime}, GetNextIndexMldInterfaceTable, MldInterfaceQuerierExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MldInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,MldCacheAddress}, GetNextIndexMldCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MldCacheTableINDEX, 2, 0, 0, NULL},

{{11,MldCacheIfIndex}, GetNextIndexMldCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MldCacheTableINDEX, 2, 0, 0, NULL},

{{11,MldCacheSelf}, GetNextIndexMldCacheTable, MldCacheSelfGet, MldCacheSelfSet, MldCacheSelfTest, MldCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MldCacheTableINDEX, 2, 0, 0, "1"},

{{11,MldCacheLastReporter}, GetNextIndexMldCacheTable, MldCacheLastReporterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MldCacheTableINDEX, 2, 0, 0, NULL},

{{11,MldCacheUpTime}, GetNextIndexMldCacheTable, MldCacheUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MldCacheTableINDEX, 2, 0, 0, NULL},

{{11,MldCacheExpiryTime}, GetNextIndexMldCacheTable, MldCacheExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MldCacheTableINDEX, 2, 0, 0, NULL},

{{11,MldCacheStatus}, GetNextIndexMldCacheTable, MldCacheStatusGet, MldCacheStatusSet, MldCacheStatusTest, MldCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MldCacheTableINDEX, 2, 0, 1, NULL},
};
tMibData stdmldEntry = { 20, stdmldMibEntry };
#endif /* _STDMLDDB_H */

