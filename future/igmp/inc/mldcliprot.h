
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldcliprot.h,v 1.9 2015/07/02 06:07:08 siva Exp $
 *
 * Description: This file contains mld cli function proto types
 *
 *******************************************************************/
#ifndef __MLDCLIPROTO_H__
#define __MLDCLIPROTO_H__

/* Function Definitions */
INT1 MldCliSetStatus (tCliHandle CliHandle, INT4 i4Status);

INT1
MldCliSetIfaceStatus (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status);

INT1
MldCliSetFastLeave (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status);

INT1
MldCliSetMldVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version);

INT1
MldCliSetNoMldVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version);

INT1
MldCliSetQryInterval (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval);
INT1
MldCliSetQryMaxRespTime (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value);

INT1
MldCliSetRobustnessValue (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value);

INT1
MldCliSetLastMbrQryInterval (tCliHandle CliHandle, INT4 i4IfIndex, 
                              UINT4 u4Interval);

INT1
MldCliSetRateLimit (tCliHandle CliHandle, INT4 i4RateLimit);

INT1
MldCliSetInterfaceRateLimit (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4RateLimit);

INT1
MldCliDeleteInterface (tCliHandle CliHandle, INT4 i4IfIndex);

INT1 MldCliShowGlobalInfo (tCliHandle CliHandle);

INT1
MldCliShowInterfaceInfo (tCliHandle CliHandle, INT4 i4IfIndex);

INT1 MldCliShowGroupInfo (tCliHandle CliHandle);

INT1 MldCliShowSourceInfo (tCliHandle CliHandle);

INT1 MldCliSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus);
INT1 MldCliSetDebug (tCliHandle CliHandle, INT4 i4TraceStatus);

INT4
MldCliUpdateInterfaceTables (tCliHandle CliHandle, INT4 i4IfIndex, 
                              UINT1 u1Status, UINT1 *pu1Create, 
                              UINT1 u1NotInService);

INT1
MldCliShowStatistics (tCliHandle CliHandle, INT4 i4IfIndex);

INT1
MldCliClearStats (tCliHandle CliHandle, INT4 i4IfIndex);

INT4
MldShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
VOID
MldShowRunningConfigScalars (tCliHandle CliHandle);

INT4
MldShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index);

INT4
MldShowRunningConfigInterface (tCliHandle CliHandle);
VOID
IssMldShowDebugging (tCliHandle CliHandle);
#endif /* __MLDCLIPROTO_H__ */

