
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsigmmid.h,v 1.2 2007/02/01 14:49:11 iss Exp $
 *
 * Description:mid level generated file
 *
 *******************************************************************/
/*  Prototype for Get Test & Set for fsigmp.  */
tSNMP_VAR_BIND*
fsigmpGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
fsigmpSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
fsigmpTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for fsIgmpInterfaceTable.  */
tSNMP_VAR_BIND*
fsIgmpInterfaceEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for fsIgmpCacheTable.  */
tSNMP_VAR_BIND*
fsIgmpCacheEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));

