/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsigmlow.h,v 1.11 2016/06/24 09:42:18 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpGlobalStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIgmpTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFsIgmpDebugLevel ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsIgmpGlobalStatus ARG_LIST((INT4 ));

INT1
nmhSetFsIgmpTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFsIgmpDebugLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsIgmpGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIgmpTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIgmpDebugLevel ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIgmpGlobalStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIgmpTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIgmpDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIgmpInterfaceTable. */
INT1
nmhValidateIndexInstanceFsIgmpInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpInterfaceTable  */

INT1
nmhGetFirstIndexFsIgmpInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIgmpInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpInterfaceAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpInterfaceFastLeaveStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpInterfaceOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpInterfaceIncomingPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceIncomingJoins ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceIncomingLeaves ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceIncomingQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceOutgoingQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceRxGenQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceRxGrpQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceRxGrpAndSrcQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceRxv1v2Reports ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceRxv3Reports ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceTxGenQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceTxGrpQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceTxGrpAndSrcQueries ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceTxv1v2Reports ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceTxv3Reports ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceTxv2Leaves ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceChannelTrackStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpInterfaceGroupListId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceCurGrpCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceCKSumError ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfacePktLenError ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfacePktsWithLocalIP ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceSubnetCheckFailure ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceQryFromNonQuerier ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceReportVersionMisMatch ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceQryVersionMisMatch ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceUnknownMsgType ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceInvalidV1Report ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceInvalidV2Report ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceInvalidV3Report ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceRouterAlertCheckFailure ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceIncomingSSMPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceInvalidSSMPkts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpInterfaceJoinPktRate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpJoinPktRate ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIgmpInterfaceAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIgmpInterfaceFastLeaveStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIgmpInterfaceChannelTrackStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIgmpInterfaceGroupListId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsIgmpInterfaceLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsIgmpInterfaceJoinPktRate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIgmpJoinPktRate ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIgmpInterfaceAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIgmpInterfaceFastLeaveStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIgmpInterfaceChannelTrackStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIgmpInterfaceGroupListId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsIgmpInterfaceLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsIgmpInterfaceJoinPktRate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIgmpJoinPktRate ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIgmpInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIgmpCacheTable. */
INT1
nmhValidateIndexInstanceFsIgmpCacheTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpCacheTable  */

INT1
nmhGetFirstIndexFsIgmpCacheTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIgmpCacheTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpCacheGroupCompMode ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsIgmpGroupListTable. */
INT1
nmhValidateIndexInstanceFsIgmpGroupListTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpGroupListTable  */

INT1
nmhGetFirstIndexFsIgmpGroupListTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIgmpGroupListTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpGrpListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIgmpGrpListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIgmpGrpListRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIgmpGroupListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpGlobalLimit ARG_LIST((UINT4 *));

INT1
nmhGetFsIgmpGlobalCurGrpCount ARG_LIST((UINT4 *));

INT1
nmhGetFsIgmpSSMMapStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIgmpGlobalLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsIgmpSSMMapStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIgmpGlobalLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsIgmpSSMMapStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIgmpGlobalLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIgmpSSMMapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIgmpSSMMapGroupTable. */
INT1
nmhValidateIndexInstanceFsIgmpSSMMapGroupTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpSSMMapGroupTable  */

INT1
nmhGetFirstIndexFsIgmpSSMMapGroupTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIgmpSSMMapGroupTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpSSMMapRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIgmpSSMMapRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIgmpSSMMapRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIgmpSSMMapGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
