/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsigmcon.h,v 1.2 2007/02/01 14:49:11 iss Exp $
 *
 * Description:mid level generated file
 *
 *******************************************************************/
# ifndef fsigmOCON_H
# define fsigmOCON_H
/*
 *  The Constant Declarations for
 *  fsigmp
 */

# define FSIGMPGLOBALSTATUS                                (1)
# define FSIGMPTRACELEVEL                                  (2)
# define FSIGMPFASTLEAVESTATUS                             (3)

/*
 *  The Constant Declarations for
 *  fsIgmpInterfaceTable
 */

# define FSIGMPINTERFACEIFINDEX                            (1)
# define FSIGMPINTERFACEINCOMINGPKTS                       (2)
# define FSIGMPINTERFACEINCOMINGJOINS                      (3)
# define FSIGMPINTERFACEINCOMINGLEAVES                     (4)
# define FSIGMPINTERFACEINCOMINGQUERIES                    (5)
# define FSIGMPINTERFACEOUTGOINGQUERIES                    (6)

/*
 *  The Constant Declarations for
 *  fsIgmpCacheTable
 */

# define FSIGMPCACHEADDRESS                                (1)
# define FSIGMPCACHEIFINDEX                                (2)
# define FSIGMPCACHEGROUPCOMPMODE                          (3)

#endif /*  fsigmpOCON_H  */
