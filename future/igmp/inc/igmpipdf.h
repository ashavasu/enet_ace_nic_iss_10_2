/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpipdf.h,v 1.2 2007/02/01 14:49:11 iss Exp $
 *
 * Description:Header File Containing IGMP-IP common        
 *             typedefinitions or macro definitions.        
 *                               
 *******************************************************************/
#ifndef IPDEFS_H
#define IPDEFS_H

/* IGMP-IP Interface Filler #defines */
#define   IGMPIF_INVALID_INDEX           0xffff
#define   IGMP_PACKET_TTL                   1
#define   IGMP_PACKET_CHKSUM_OFFSET         2
#define   IGMP_HEADER_LEN                   8

#endif /* IPDEFS_H */
