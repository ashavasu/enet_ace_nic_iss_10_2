/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpextn.h,v 1.15 2017/02/06 10:45:26 siva Exp $
 * Description:File containing the external declarations for 
 *             the global variables used in IGMP.            
 * 
 *******************************************************************/
#ifndef IGMPEXTN_H
#define IGMPEXTN_H

extern tIgmpMemPool  gIgmpMemPool;
extern tRBTree  gIgmpGrpList;
extern tIgmpConfig    gIgmpConfig;

extern tIgmpSystemSize gIgmpSystemSize;

extern tIgmpInterfaceInfo gIgmpIfInfo;

extern tIgmpRegister   gaIgmpRegister[];
extern tMldRegister   gaMldRegister[];

extern tOsixSemId gIgmpSemId;

extern tTimerListId gIgmpTimerListId;
 
extern tRBTree   gIgmpGroup; /* Global RB Tree that maintains all the 
                         groups along with the list of sources & reporters */
extern tRBTree   gIgmpSSMMappedGrp; /* Global RB Tree that maintains all the 
                         groups for which mapping is configured
                         along with the list of sources */
extern UINT2 gu2NumOfRegMRPs;
extern tIgmpIPvXAddr *gpu4SrcSetArray;
extern tIgmpIPvXAddr   gIPvXZero;
extern tIPvXAddr gAllNodeV6McastAddr;
extern tIPvXAddr gAllRtrV6McastAddr;
extern tIPvXAddr gau1SSMIPv6StartAddr;
extern tIPvXAddr gau1SSMIPv6EndAddr;
extern tIPvXAddr gau1InvalidSSMIPv6StartAddr;
extern tIPvXAddr gau1InvalidSSMIPv6EndAddr;

extern tOsixTaskId         gIgmpTaskId;
extern tOsixQId            gIgmpQId;
extern tRBTree       gIgmpHostMcastGrpTable;

extern tOsixQId            gIgmpRmPktQId;
extern UINT4                gu4IgmpSysLogId;
extern tIgmpRedGblInfo     gIgmpRedGblInfo;
extern tRBTree             gIgmpRedTable;
extern tDbOffsetTemplate   gaIgmpGrpOffsetTbl[];
extern tDbOffsetTemplate   gaIgmpSrcOffsetTbl[];
extern tDbOffsetTemplate   gaIgmpQueryOffsetTbl[];
extern tDbOffsetTemplate   gaIgmpSrcReporterOffsetTbl[];

extern tDbTblDescriptor gIgmpDynInfoList;
extern tDbDataDescInfo  gaIgmpDynDataDescList[IGMP_MAX_DYN_INFO_TYPE];

extern tIgmpFSMFnPtr
gaIgmpFSM [IGMP_MAX_STATE][IGMP_MAX_EVENTS];

/* For BCM Linux IP alone, NP_KERNEL_WANTED will be defined.
 * Only incase of BCM Linuxip, IGMP control packets are received
 * thru a separate char device and task. This needs to be removed after
 * testing packet reception thru IGMP socket in netip */
#if defined (LNXIP4_WANTED) && defined (NP_KERNEL_WANTED)
extern INT4 gi4IgmpDevFd;
extern tOsixTaskId gIgmpRecvTaskId;
#endif

extern UINT1        gau1Igmpv3Buffer[];

#ifndef LNXIP4_WANTED
/* This function is used to retrieve associated interface IP address in
 * case of unnumbered interface from IP module */
extern INT4 IpGetSrcAddressOnInterface PROTO ((UINT2   u2Port, UINT4 u4Dest,
                                UINT4 *pu4SrcIp));
#endif
extern INT4 PimIsAllowedToPostQueMsg (VOID);
#endif /* _IGMPEXTN_H */
