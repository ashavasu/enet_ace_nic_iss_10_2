/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmlddb.h,v 1.3 2015/02/13 11:13:56 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMLDDB_H
#define _FSMLDDB_H


UINT4 fsmld [] ={1,3,6,1,4,1,2076,70};
tSNMP_OID_TYPE fsmldOID = {8, fsmld};


UINT4 FsmldNoOfCacheEntries [ ] ={1,3,6,1,4,1,2076,70,1,1};
UINT4 FsmldNoOfRoutingProtocols [ ] ={1,3,6,1,4,1,2076,70,1,2};
UINT4 FsmldTraceDebug [ ] ={1,3,6,1,4,1,2076,70,1,3};
UINT4 FsmldDebugLevel [ ] ={1,3,6,1,4,1,2076,70,1,4};
UINT4 FsmldMode [ ] ={1,3,6,1,4,1,2076,70,1,5};
UINT4 FsmldProtocolUpDown [ ] ={1,3,6,1,4,1,2076,70,1,6};




tMbDbEntry fsmldMibEntry[]= {

{{10,FsmldNoOfCacheEntries}, NULL, FsmldNoOfCacheEntriesGet, FsmldNoOfCacheEntriesSet, FsmldNoOfCacheEntriesTest, FsmldNoOfCacheEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,FsmldNoOfRoutingProtocols}, NULL, FsmldNoOfRoutingProtocolsGet, FsmldNoOfRoutingProtocolsSet, FsmldNoOfRoutingProtocolsTest, FsmldNoOfRoutingProtocolsDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 1, 0, NULL},

{{10,FsmldTraceDebug}, NULL, FsmldTraceDebugGet, FsmldTraceDebugSet, FsmldTraceDebugTest, FsmldTraceDebugDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsmldDebugLevel}, NULL, FsmldDebugLevelGet, FsmldDebugLevelSet, FsmldDebugLevelTest, FsmldDebugLevelDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsmldMode}, NULL, FsmldModeGet, FsmldModeSet, FsmldModeTest, FsmldModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsmldProtocolUpDown}, NULL, FsmldProtocolUpDownGet, FsmldProtocolUpDownSet, FsmldProtocolUpDownTest, FsmldProtocolUpDownDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsmldEntry = { 6, fsmldMibEntry };

#endif /* _FSMLDDB_H */

