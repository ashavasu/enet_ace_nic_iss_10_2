/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpinc.h,v 1.12 2015/02/13 11:13:56 siva Exp $
 *
 * Description:FILE CONTAINING  ALL THE INCLUDE FILES   
 *             needed by IGMP.                             
 *
 *******************************************************************/
#ifndef IGMPINC_H
#define IGMPINC_H

#include "lr.h"
#include "size.h"
#include "ip.h"
#include "ipv6.h"
#include "utilipvx.h"
#include "ip6util.h"
#include "igmp.h"
#include "mld.h"
#include "cfa.h"
#include "igmpcli.h"
#include "mldcli.h"
#include "igmpsys.h"
#include "fssnmp.h"
#include "fssyslog.h"
#include "dbutil.h"
#include "iss.h"
#include "rmgr.h"
#include "msr.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "igmpnp.h"
#include "mldnp.h"
#endif

#include "igmpdefs.h"
#include "mlddefs.h"
#include "igmptdfs.h"
#include "igmpred.h"
#include "igmpextn.h"
#include "igmpport.h"
#include "igmpipdf.h"
#include "igmpprot.h"
#include "mldprot.h"
#include "igmptrc.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fsigmlow.h"
#include "stdiglow.h"
#include "fsmgmdlw.h"
#include "stdmgmlw.h"
#include "stdmllow.h"
#include "igmpsz.h"

#ifdef NPAPI_WANTED
#include "igmpnpwr.h"
#include "mldnpwr.h"
#include "ipmcnpwr.h"
#endif


#endif /* IGMPINC_H */
