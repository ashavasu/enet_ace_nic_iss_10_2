
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdigcon.h,v 1.2 2007/02/01 14:49:11 iss Exp $
 *
 * Description:mid level generated file
 *
 *******************************************************************/
# ifndef stdigOCON_H
# define stdigOCON_H
/*
 *  The Constant Declarations for
 *  igmpInterfaceTable
 */

# define IGMPINTERFACEIFINDEX                              (1)
# define IGMPINTERFACEQUERYINTERVAL                        (2)
# define IGMPINTERFACESTATUS                               (3)
# define IGMPINTERFACEVERSION                              (4)
# define IGMPINTERFACEQUERIER                              (5)
# define IGMPINTERFACEQUERYMAXRESPONSETIME                 (6)
# define IGMPINTERFACEQUERIERUPTIME                        (7)
# define IGMPINTERFACEQUERIEREXPIRYTIME                    (8)
# define IGMPINTERFACEVERSION1QUERIERTIMER                 (9)
# define IGMPINTERFACEWRONGVERSIONQUERIES                  (10)
# define IGMPINTERFACEJOINS                                (11)
# define IGMPINTERFACEPROXYIFINDEX                         (12)
# define IGMPINTERFACEGROUPS                               (13)
# define IGMPINTERFACEROBUSTNESS                           (14)
# define IGMPINTERFACELASTMEMBQUERYINTVL                   (15)
# define IGMPINTERFACEVERSION2QUERIERTIMER                 (16)

/*
 *  The Constant Declarations for
 *  igmpCacheTable
 */

# define IGMPCACHEADDRESS                                  (1)
# define IGMPCACHEIFINDEX                                  (2)
# define IGMPCACHESELF                                     (3)
# define IGMPCACHELASTREPORTER                             (4)
# define IGMPCACHEUPTIME                                   (5)
# define IGMPCACHEEXPIRYTIME                               (6)
# define IGMPCACHESTATUS                                   (7)
# define IGMPCACHEVERSION1HOSTTIMER                        (8)
# define IGMPCACHEVERSION2HOSTTIMER                        (9)
# define IGMPCACHESOURCEFILTERMODE                         (10)

/*
 *  The Constant Declarations for
 *  igmpSrcListTable
 */

# define IGMPSRCLISTADDRESS                                (1)
# define IGMPSRCLISTIFINDEX                                (2)
# define IGMPSRCLISTHOSTADDRESS                            (3)
# define IGMPSRCLISTSTATUS                                 (4)

#endif /*  stdigmpOCON_H  */
