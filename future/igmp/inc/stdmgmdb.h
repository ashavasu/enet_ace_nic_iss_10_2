/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmgmdb.h,v 1.3 2011/08/04 11:10:41 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDMGMDB_H
#define _STDMGMDB_H

UINT1 MgmdHostInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 MgmdRouterInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 MgmdHostCacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 MgmdRouterCacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 MgmdInverseHostCacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MgmdInverseRouterCacheTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MgmdHostSrcListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MgmdRouterSrcListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 stdmgm [] ={1,3,6,1,2,1,185};
tSNMP_OID_TYPE stdmgmOID = {7, stdmgm};


UINT4 MgmdHostInterfaceIfIndex [ ] ={1,3,6,1,2,1,185,1,1,1,1};
UINT4 MgmdHostInterfaceQuerierType [ ] ={1,3,6,1,2,1,185,1,1,1,2};
UINT4 MgmdHostInterfaceQuerier [ ] ={1,3,6,1,2,1,185,1,1,1,3};
UINT4 MgmdHostInterfaceStatus [ ] ={1,3,6,1,2,1,185,1,1,1,4};
UINT4 MgmdHostInterfaceVersion [ ] ={1,3,6,1,2,1,185,1,1,1,5};
UINT4 MgmdHostInterfaceVersion1QuerierTimer [ ] ={1,3,6,1,2,1,185,1,1,1,6};
UINT4 MgmdHostInterfaceVersion2QuerierTimer [ ] ={1,3,6,1,2,1,185,1,1,1,7};
UINT4 MgmdHostInterfaceVersion3Robustness [ ] ={1,3,6,1,2,1,185,1,1,1,8};
UINT4 MgmdRouterInterfaceIfIndex [ ] ={1,3,6,1,2,1,185,1,2,1,1};
UINT4 MgmdRouterInterfaceQuerierType [ ] ={1,3,6,1,2,1,185,1,2,1,2};
UINT4 MgmdRouterInterfaceQuerier [ ] ={1,3,6,1,2,1,185,1,2,1,3};
UINT4 MgmdRouterInterfaceQueryInterval [ ] ={1,3,6,1,2,1,185,1,2,1,4};
UINT4 MgmdRouterInterfaceStatus [ ] ={1,3,6,1,2,1,185,1,2,1,5};
UINT4 MgmdRouterInterfaceVersion [ ] ={1,3,6,1,2,1,185,1,2,1,6};
UINT4 MgmdRouterInterfaceQueryMaxResponseTime [ ] ={1,3,6,1,2,1,185,1,2,1,7};
UINT4 MgmdRouterInterfaceQuerierUpTime [ ] ={1,3,6,1,2,1,185,1,2,1,8};
UINT4 MgmdRouterInterfaceQuerierExpiryTime [ ] ={1,3,6,1,2,1,185,1,2,1,9};
UINT4 MgmdRouterInterfaceWrongVersionQueries [ ] ={1,3,6,1,2,1,185,1,2,1,10};
UINT4 MgmdRouterInterfaceJoins [ ] ={1,3,6,1,2,1,185,1,2,1,11};
UINT4 MgmdRouterInterfaceProxyIfIndex [ ] ={1,3,6,1,2,1,185,1,2,1,12};
UINT4 MgmdRouterInterfaceGroups [ ] ={1,3,6,1,2,1,185,1,2,1,13};
UINT4 MgmdRouterInterfaceRobustness [ ] ={1,3,6,1,2,1,185,1,2,1,14};
UINT4 MgmdRouterInterfaceLastMemberQueryInterval [ ] ={1,3,6,1,2,1,185,1,2,1,15};
UINT4 MgmdRouterInterfaceLastMemberQueryCount [ ] ={1,3,6,1,2,1,185,1,2,1,16};
UINT4 MgmdRouterInterfaceStartupQueryCount [ ] ={1,3,6,1,2,1,185,1,2,1,17};
UINT4 MgmdRouterInterfaceStartupQueryInterval [ ] ={1,3,6,1,2,1,185,1,2,1,18};
UINT4 MgmdHostCacheAddressType [ ] ={1,3,6,1,2,1,185,1,3,1,1};
UINT4 MgmdHostCacheAddress [ ] ={1,3,6,1,2,1,185,1,3,1,2};
UINT4 MgmdHostCacheIfIndex [ ] ={1,3,6,1,2,1,185,1,3,1,3};
UINT4 MgmdHostCacheUpTime [ ] ={1,3,6,1,2,1,185,1,3,1,4};
UINT4 MgmdHostCacheLastReporter [ ] ={1,3,6,1,2,1,185,1,3,1,5};
UINT4 MgmdHostCacheSourceFilterMode [ ] ={1,3,6,1,2,1,185,1,3,1,6};
UINT4 MgmdRouterCacheAddressType [ ] ={1,3,6,1,2,1,185,1,4,1,1};
UINT4 MgmdRouterCacheAddress [ ] ={1,3,6,1,2,1,185,1,4,1,2};
UINT4 MgmdRouterCacheIfIndex [ ] ={1,3,6,1,2,1,185,1,4,1,3};
UINT4 MgmdRouterCacheLastReporter [ ] ={1,3,6,1,2,1,185,1,4,1,4};
UINT4 MgmdRouterCacheUpTime [ ] ={1,3,6,1,2,1,185,1,4,1,5};
UINT4 MgmdRouterCacheExpiryTime [ ] ={1,3,6,1,2,1,185,1,4,1,6};
UINT4 MgmdRouterCacheExcludeModeExpiryTimer [ ] ={1,3,6,1,2,1,185,1,4,1,7};
UINT4 MgmdRouterCacheVersion1HostTimer [ ] ={1,3,6,1,2,1,185,1,4,1,8};
UINT4 MgmdRouterCacheVersion2HostTimer [ ] ={1,3,6,1,2,1,185,1,4,1,9};
UINT4 MgmdRouterCacheSourceFilterMode [ ] ={1,3,6,1,2,1,185,1,4,1,10};
UINT4 MgmdInverseHostCacheIfIndex [ ] ={1,3,6,1,2,1,185,1,5,1,1};
UINT4 MgmdInverseHostCacheAddressType [ ] ={1,3,6,1,2,1,185,1,5,1,2};
UINT4 MgmdInverseHostCacheAddress [ ] ={1,3,6,1,2,1,185,1,5,1,3};
UINT4 MgmdInverseRouterCacheIfIndex [ ] ={1,3,6,1,2,1,185,1,6,1,1};
UINT4 MgmdInverseRouterCacheAddressType [ ] ={1,3,6,1,2,1,185,1,6,1,2};
UINT4 MgmdInverseRouterCacheAddress [ ] ={1,3,6,1,2,1,185,1,6,1,3};
UINT4 MgmdHostSrcListAddressType [ ] ={1,3,6,1,2,1,185,1,7,1,1};
UINT4 MgmdHostSrcListAddress [ ] ={1,3,6,1,2,1,185,1,7,1,2};
UINT4 MgmdHostSrcListIfIndex [ ] ={1,3,6,1,2,1,185,1,7,1,3};
UINT4 MgmdHostSrcListHostAddress [ ] ={1,3,6,1,2,1,185,1,7,1,4};
UINT4 MgmdHostSrcListExpire [ ] ={1,3,6,1,2,1,185,1,7,1,5};
UINT4 MgmdRouterSrcListAddressType [ ] ={1,3,6,1,2,1,185,1,8,1,1};
UINT4 MgmdRouterSrcListAddress [ ] ={1,3,6,1,2,1,185,1,8,1,2};
UINT4 MgmdRouterSrcListIfIndex [ ] ={1,3,6,1,2,1,185,1,8,1,3};
UINT4 MgmdRouterSrcListHostAddress [ ] ={1,3,6,1,2,1,185,1,8,1,4};
UINT4 MgmdRouterSrcListExpire [ ] ={1,3,6,1,2,1,185,1,8,1,5};




tMbDbEntry stdmgmMibEntry[]= {

{{11,MgmdHostInterfaceIfIndex}, GetNextIndexMgmdHostInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdHostInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdHostInterfaceQuerierType}, GetNextIndexMgmdHostInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdHostInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdHostInterfaceQuerier}, GetNextIndexMgmdHostInterfaceTable, MgmdHostInterfaceQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MgmdHostInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdHostInterfaceStatus}, GetNextIndexMgmdHostInterfaceTable, MgmdHostInterfaceStatusGet, MgmdHostInterfaceStatusSet, MgmdHostInterfaceStatusTest, MgmdHostInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MgmdHostInterfaceTableINDEX, 2, 0, 1, NULL},

{{11,MgmdHostInterfaceVersion}, GetNextIndexMgmdHostInterfaceTable, MgmdHostInterfaceVersionGet, MgmdHostInterfaceVersionSet, MgmdHostInterfaceVersionTest, MgmdHostInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MgmdHostInterfaceTableINDEX, 2, 0, 0, "3"},

{{11,MgmdHostInterfaceVersion1QuerierTimer}, GetNextIndexMgmdHostInterfaceTable, MgmdHostInterfaceVersion1QuerierTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdHostInterfaceTableINDEX, 2, 0, 0, "0"},

{{11,MgmdHostInterfaceVersion2QuerierTimer}, GetNextIndexMgmdHostInterfaceTable, MgmdHostInterfaceVersion2QuerierTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdHostInterfaceTableINDEX, 2, 0, 0, "0"},

{{11,MgmdHostInterfaceVersion3Robustness}, GetNextIndexMgmdHostInterfaceTable, MgmdHostInterfaceVersion3RobustnessGet, MgmdHostInterfaceVersion3RobustnessSet, MgmdHostInterfaceVersion3RobustnessTest, MgmdHostInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MgmdHostInterfaceTableINDEX, 2, 0, 0, "2"},

{{11,MgmdRouterInterfaceIfIndex}, GetNextIndexMgmdRouterInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceQuerierType}, GetNextIndexMgmdRouterInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceQuerier}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceQueryInterval}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceQueryIntervalGet, MgmdRouterInterfaceQueryIntervalSet, MgmdRouterInterfaceQueryIntervalTest, MgmdRouterInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MgmdRouterInterfaceTableINDEX, 2, 0, 0, "125"},

{{11,MgmdRouterInterfaceStatus}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceStatusGet, MgmdRouterInterfaceStatusSet, MgmdRouterInterfaceStatusTest, MgmdRouterInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MgmdRouterInterfaceTableINDEX, 2, 0, 1, NULL},

/*Default  IGMP version for mib-save-restore is changed to older version 2 .
 * This has to be reverted while fixing the HDC 56519 */
{{11,MgmdRouterInterfaceVersion}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceVersionGet, MgmdRouterInterfaceVersionSet, MgmdRouterInterfaceVersionTest, MgmdRouterInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MgmdRouterInterfaceTableINDEX, 2, 0, 0, "2"},

{{11,MgmdRouterInterfaceQueryMaxResponseTime}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceQueryMaxResponseTimeGet, MgmdRouterInterfaceQueryMaxResponseTimeSet, MgmdRouterInterfaceQueryMaxResponseTimeTest, MgmdRouterInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MgmdRouterInterfaceTableINDEX, 2, 0, 0, "100"},

{{11,MgmdRouterInterfaceQuerierUpTime}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceQuerierUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceQuerierExpiryTime}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceQuerierExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceWrongVersionQueries}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceWrongVersionQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceJoins}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceJoinsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceProxyIfIndex}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceProxyIfIndexGet, MgmdRouterInterfaceProxyIfIndexSet, MgmdRouterInterfaceProxyIfIndexTest, MgmdRouterInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MgmdRouterInterfaceTableINDEX, 2, 0, 0, "0"},

{{11,MgmdRouterInterfaceGroups}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceGroupsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceRobustness}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceRobustnessGet, MgmdRouterInterfaceRobustnessSet, MgmdRouterInterfaceRobustnessTest, MgmdRouterInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MgmdRouterInterfaceTableINDEX, 2, 0, 0, "2"},

{{11,MgmdRouterInterfaceLastMemberQueryInterval}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceLastMemberQueryIntervalGet, MgmdRouterInterfaceLastMemberQueryIntervalSet, MgmdRouterInterfaceLastMemberQueryIntervalTest, MgmdRouterInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MgmdRouterInterfaceTableINDEX, 2, 0, 0, "10"},

{{11,MgmdRouterInterfaceLastMemberQueryCount}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceLastMemberQueryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceStartupQueryCount}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceStartupQueryCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdRouterInterfaceStartupQueryInterval}, GetNextIndexMgmdRouterInterfaceTable, MgmdRouterInterfaceStartupQueryIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MgmdRouterInterfaceTableINDEX, 2, 0, 0, NULL},

{{11,MgmdHostCacheAddressType}, GetNextIndexMgmdHostCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdHostCacheAddress}, GetNextIndexMgmdHostCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MgmdHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdHostCacheIfIndex}, GetNextIndexMgmdHostCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdHostCacheUpTime}, GetNextIndexMgmdHostCacheTable, MgmdHostCacheUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdHostCacheLastReporter}, GetNextIndexMgmdHostCacheTable, MgmdHostCacheLastReporterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MgmdHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdHostCacheSourceFilterMode}, GetNextIndexMgmdHostCacheTable, MgmdHostCacheSourceFilterModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MgmdHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheAddressType}, GetNextIndexMgmdRouterCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheAddress}, GetNextIndexMgmdRouterCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheIfIndex}, GetNextIndexMgmdRouterCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheLastReporter}, GetNextIndexMgmdRouterCacheTable, MgmdRouterCacheLastReporterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheUpTime}, GetNextIndexMgmdRouterCacheTable, MgmdRouterCacheUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheExpiryTime}, GetNextIndexMgmdRouterCacheTable, MgmdRouterCacheExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheExcludeModeExpiryTimer}, GetNextIndexMgmdRouterCacheTable, MgmdRouterCacheExcludeModeExpiryTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheVersion1HostTimer}, GetNextIndexMgmdRouterCacheTable, MgmdRouterCacheVersion1HostTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheVersion2HostTimer}, GetNextIndexMgmdRouterCacheTable, MgmdRouterCacheVersion2HostTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdRouterCacheSourceFilterMode}, GetNextIndexMgmdRouterCacheTable, MgmdRouterCacheSourceFilterModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MgmdRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdInverseHostCacheIfIndex}, GetNextIndexMgmdInverseHostCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdInverseHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdInverseHostCacheAddressType}, GetNextIndexMgmdInverseHostCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdInverseHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdInverseHostCacheAddress}, GetNextIndexMgmdInverseHostCacheTable, MgmdInverseHostCacheAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MgmdInverseHostCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdInverseRouterCacheIfIndex}, GetNextIndexMgmdInverseRouterCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdInverseRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdInverseRouterCacheAddressType}, GetNextIndexMgmdInverseRouterCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdInverseRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdInverseRouterCacheAddress}, GetNextIndexMgmdInverseRouterCacheTable, MgmdInverseRouterCacheAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MgmdInverseRouterCacheTableINDEX, 3, 0, 0, NULL},

{{11,MgmdHostSrcListAddressType}, GetNextIndexMgmdHostSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdHostSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdHostSrcListAddress}, GetNextIndexMgmdHostSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MgmdHostSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdHostSrcListIfIndex}, GetNextIndexMgmdHostSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdHostSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdHostSrcListHostAddress}, GetNextIndexMgmdHostSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MgmdHostSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdHostSrcListExpire}, GetNextIndexMgmdHostSrcListTable, MgmdHostSrcListExpireGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdHostSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdRouterSrcListAddressType}, GetNextIndexMgmdRouterSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdRouterSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdRouterSrcListAddress}, GetNextIndexMgmdRouterSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MgmdRouterSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdRouterSrcListIfIndex}, GetNextIndexMgmdRouterSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MgmdRouterSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdRouterSrcListHostAddress}, GetNextIndexMgmdRouterSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MgmdRouterSrcListTableINDEX, 4, 0, 0, NULL},

{{11,MgmdRouterSrcListExpire}, GetNextIndexMgmdRouterSrcListTable, MgmdRouterSrcListExpireGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MgmdRouterSrcListTableINDEX, 4, 0, 0, NULL},
};
tMibData stdmgmEntry = { 58, stdmgmMibEntry };

#endif /* _STDMGMDB_H */

