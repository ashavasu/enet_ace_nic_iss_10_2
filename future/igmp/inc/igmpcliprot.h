
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpcliprot.h,v 1.15 2016/06/24 09:42:19 siva Exp $
 *
 * Description: This file contains igmp cli function proto types
 *
 *******************************************************************/
#ifndef __IGMPCLIPROTO_H__
#define __IGMPCLIPROTO_H__

/* Function Definitions */
INT1 IgmpCliSetStatus (tCliHandle CliHandle, INT4 i4Status);

INT1
IgmpCliSetIfaceStatus (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status);

INT1
IgmpCliSetFastLeave (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status);

INT1
IgmpCliSetChannelTrack (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status);

INT1
IgmpCliSetIgmpVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version);

INT1
IgmpCliSetNoIgmpVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version);

INT1
IgmpCliSetQryInterval (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval);
INT1
IgmpCliSetQryMaxRespTime (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value);

INT1
IgmpCliSetRobustnessValue (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value);

INT1
IgmpCliSetLastMbrQryInterval (tCliHandle CliHandle, INT4 i4IfIndex, 
                              UINT4 u4Interval);
INT1
IgmpCliAddStaticGroup(tCliHandle CliHandle, INT4 i4IfIndex, 
                      UINT4 u4GroupAddr, UINT4 u4SrcAddr);
INT1
IgmpCliDeleteStaticGroup (tCliHandle CliHandle, INT4 i4IfIndex, 
                          UINT4 u4GroupAddr, UINT4 u4SrcAddr);

INT1
IgmpCliDeleteInterface (tCliHandle CliHandle, INT4 i4IfIndex);

INT1
IgmpCliSetRateLimit (tCliHandle CliHandle, INT4 i4RateLimit);

INT1
IgmpCliSetInterfaceRateLimit (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4RateLimit);

INT1 IgmpCliShowGlobalInfo (tCliHandle CliHandle);

INT1
IgmpCliShowInterfaceInfo (tCliHandle CliHandle, INT4 i4IfIndex);

INT1 IgmpCliShowGroupInfo (tCliHandle CliHandle);

INT1 IgmpCliShowMembership (tCliHandle CliHandle, INT4 i4DispStatus);

INT1 IgmpCliShowGroupChannel (tCliHandle CliHandle,
                            UINT4 u4GrpAddr, INT4 i4DispStatus);
INT1
IgmpCliShowTrackedGroups (tCliHandle CliHandle,
                            tIgmpGroup *pGrp, INT4 i4DispStatus);

INT1 IgmpCliShowSourceInfo (tCliHandle CliHandle);

INT1 IgmpCliSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus);

INT1 IgmpCliSetDebug (tCliHandle CliHandle, INT4 i4DebugStatus);

INT4
IgmpCliUpdateInterfaceTables (tCliHandle CliHandle, INT4 i4IfIndex, 
                              UINT1 u1Status, UINT1 *pu1Create, 
                              UINT1 u1NotInService);

INT1
IgmpCliShowStatistics (tCliHandle CliHandle, INT4 i4IfIndex);

INT1
IgmpCliClearStats (tCliHandle CliHandle, INT4 i4IfIndex);

INT4
IgmpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);
VOID
IgmpShowRunningConfigScalars (tCliHandle CliHandle);
INT4
IgmpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index);
INT4
IgmpShowRunningConfigInterface (tCliHandle CliHandle);
VOID
IssIgmpShowDebugging (tCliHandle CliHandle);
INT4 IgmpCliShowGroupList(tCliHandle);
INT4 IgmpCliValidatePointers(UINT4 u4port, tIgmpIPvXAddr u4GrpAddr, tIgmpIPvXAddr u4SrcAddr);
VOID IgmpShowRunningConfigGroupListTable(tCliHandle CliHandle);
VOID IgmpShowRunningConfigSSMMapGroupTable(tCliHandle CliHandle);
INT4 IgmpCliSetGlobalLimit(tCliHandle ,UINT4);
INT4 IgmpCliSetInterfaceLimit (tCliHandle,INT4 i4IfaceIndex ,UINT4 u4IntfLimit, UINT4 u4GrpId);
INT4 IgmpCliSetGroupList(tCliHandle ,UINT4 u4GroupId, UINT4 u4GrpIp,UINT4 u4mask, INT4 i4Status);
INT4 IgmpCliSetSSMMapStatus (tCliHandle CliHandle, INT4 i4Status);
INT4 IgmpCliConfigureSSMMap (tCliHandle CliHandle, UINT4 u4StartGrpAddress,
                             UINT4 u4EndGrpAddress, UINT4 u4SrcAddress,
                             INT4 i4Status);
INT4 IgmpCliShowSSMMapGroup (tCliHandle CliHandle, UINT4 u4GrpAddress);
#endif /* __IGMPCLIPROTO_H__ */

