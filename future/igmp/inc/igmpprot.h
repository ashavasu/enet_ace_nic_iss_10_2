/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpprot.h,v 1.36 2017/02/06 10:45:26 siva Exp $
 *
 * Description:File containing  prototype declarations of  
 *             all the routines used/defined in IGMP.       
 * 
 *******************************************************************/
#ifndef IGMPPROT_H
#define IGMPPROT_H

VOID 
IgmpDispHhMmSs(UINT4, char*);
VOID 
IgmpReStartGrpSrcSchQryTmr (tIgmpIface *, tIgmpSchQuery *);
VOID 
IgmpStartUpQryIntvlExpTmr (VOID *pArg);
VOID 
IgmpQryIntvlExpTmr (VOID *pArg);
VOID 
IgmpOtherQuerierPresentExpTmr (VOID *pArg);
VOID IgmpGrpExpTmr (VOID *pArg);
VOID IgmpSrcExpTmr (VOID *pArg);
VOID IgmpGrpSrcSchQryExpTmr (VOID *pArg);
VOID IgmpV1HostPresentExpTmr (VOID *pArg);
VOID IgmpV2HostPresentExpTmr (VOID *pArg);
VOID IgmpTmrInitTmrDesc (VOID);
VOID IgmpMcastGrpExpTmr (VOID *pArg);

INT4 IgmpBlockInclude (tIgmpFSMInfo *);
INT4 IgmpBlockExclude (tIgmpFSMInfo *);
INT4 IgmpToExclude (tIgmpFSMInfo *);
INT4 IgmpHandleIsIncludeStateIn (tIgmpFSMInfo *);
INT4 IgmpHandleIsExcludeStateIn (tIgmpFSMInfo *);
INT4 IgmpHandleAllowStateIn (tIgmpFSMInfo *);
INT4 IgmpHandleBlockStateIn (tIgmpFSMInfo *);
INT4 IgmpHandleToExcludeStateIn (tIgmpFSMInfo *);
INT4 IgmpHandleToIncludeStateIn (tIgmpFSMInfo *);
INT4 IgmpHandleIsIncludeStateEx (tIgmpFSMInfo *);
INT4 IgmpHandleIsExcludeStateEx (tIgmpFSMInfo *);
INT4 IgmpHandleAllowStateEx (tIgmpFSMInfo *);
INT4 IgmpHandleBlockStateEx (tIgmpFSMInfo *);
INT4 IgmpHandleToExcludeStateEx (tIgmpFSMInfo *);
INT4 IgmpHandleToIncludeStateEx (tIgmpFSMInfo *);
VOID IgmpDeleteAllSrcReporters PROTO ((tIgmpSource *));
INT4 IgmpAction1 PROTO ((tIgmpFSMInfo * ));
tIgmpGroup *IgmpGrpLookUp PROTO ((tIgmpIface *, tIgmpIPvXAddr));
VOID GetIgmpSizingParams (tIgmpSystemSize *);
UINT2 IgmpCalcCheckSum PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4 , UINT4));
VOID  IgmpUpdateMrps PROTO ((tIgmpGroup *, UINT1));
VOID IgmpProcessPkts ARG_LIST ((VOID));
VOID IgmpProcessTimerExpiry PROTO ((tIgmpTimer *));
INT4 IgmpHandleEnableEvent (VOID);
INT4 IgmpHandleDisableEvent (VOID);
INT1 IgmpInitialize PROTO ((VOID));
VOID IgmpInput PROTO ((tCRU_BUF_CHAIN_HEADER *));
VOID
IgmpProcessIfChange PROTO ((tIgmpIfStatusInfo IfStatusInfo));
VOID
IgmpHandleSecondaryIPDelete PROTO ((tIgmpIface *pIfaceNode, UINT4 u4SecIPAddr, UINT4 u4SecIPMask));
INT4
IgmpAddV1V2Reporter (tIgmpGroup *pGrp, tIgmpIPvXAddr u4Reporter);
INT4
IgmpAddV3Reporter (tIgmpSource *pSrc, tIgmpIPvXAddr u4Reporter);
INT4
IgmpDeleteV1V2GrpReporter (tIgmpGroup *pGrp, tIgmpIPvXAddr u4Reporter);
VOID
IgmpDeleteAllV1V2GrpReporters (tIgmpGroup *pGrp);
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
VOID
MldProcessIpAddrChange PROTO ((tMldIpv6AddrInfo MldIpv6AddrInfo));
#endif
INT4 IgmpUtilIsSourcePresent PROTO ((tIgmpIPvXAddr u4IpAddr, UINT4 u4IfIndex,
                                     UINT1 u1AddrType));
VOID IgmpProcessV2Report PROTO ((tIgmpIface *, tIgmpIPvXAddr, tIgmpIPvXAddr));

VOID IgmpProcessGroup PROTO ((tIgmpIface *, tIgmpIPvXAddr, UINT1, tIgmpIPvXAddr));

VOID IgmpProcessLeave PROTO ((tIgmpIface *, tIgmpIPvXAddr));
VOID IgmpProcessQuery PROTO ((tIgmpIface *, tIgmpIPvXAddr, tIgmpIPvXAddr, 
                              UINT4, UINT1 *, UINT1, UINT1));
VOID
IgmpV3ProcessGroupRecord (tIgmpIface * pIfNode, tIPvXAddr  GrpAddress,
                          UINT1 u1GrpRecType, UINT1 *pu1Sources,
                          UINT2 u2NoOfSources, tIPvXAddr SenderAddr );

VOID IgmpProcessV3Query(tIgmpIface *pIfNode, 
                               tCRU_BUF_CHAIN_HEADER *pBuf, 
                               tIgmpIPvXAddr u4SrcAddr);
VOID IgmpDeleteGroup PROTO ((tIgmpIface *, tIgmpGroup *, UINT1 u1Staticflag));
VOID IgmpExpireGroup (tIgmpIface * pIfNode, tIgmpGroup * pGrp, 
                 UINT1 u1Version, UINT1 u1Consolidate, UINT1 u1Staticflag);
INT4 IgmpRBTreeGroupEntryCmp (tRBElem * pGroupEntryNode, tRBElem * pGroupEntryIn);
INT4 IgmpRBTreeMcastGroupEntryCmp (tRBElem * pMcastGroupEntryNode, 
                                   tRBElem * pMcastGroupEntryIn);
INT4 IgmpRBTreeGrpSrcReporterEntryCmp (tRBElem * pReporterNodeInRB, tRBElem * pReporterInput);
tIgmpQuery *IgmpGetQueryBlock PROTO ((UINT4));
tIgmpIface *IgmpGetIfaceBlock PROTO ((UINT4, UINT1));
INT1 IgmpSendQuery PROTO ((tIgmpIface *, UINT4));
tIgmpGroup *IgmpAddGroup PROTO ((tIgmpIface *, tIgmpIPvXAddr, UINT1 u1Staticflag));
VOID IgmpIfaceUp PROTO ((tIgmpIface *));
VOID IgmpIfUpHdlr PROTO ((tIgmpIface *));
VOID IgmpIfaceDown PROTO ((tIgmpIface *));
VOID IgmpIfDownHdlr PROTO ((tIgmpIface *));
VOID IgmpOperUpIfaceDs PROTO ((UINT4));
VOID IgmpProcessIfStatChg PROTO ((tIgmpIface *, UINT1));
tIgmpSchQuery * IgmpGetSchQueryGrp PROTO ((tIgmpQuery *, tIgmpIPvXAddr));
VOID IgmpPutHdr PROTO ((tIgmpPkt *, tCRU_BUF_CHAIN_HEADER *, UINT2));
VOID IgmpPutV3Hdr PROTO ((tIgmpV3Query *, tCRU_BUF_CHAIN_HEADER *, 
                                                                      UINT2));
UINT4 IgmpHash PROTO ((UINT4));
INT4 IgmpFindRegisterEntry (UINT1 protid);
                                   
INT4 IgmpFindFreeRegisterEntry PROTO ((VOID));

INT1 REGISTER_WITH_IP PROTO ((UINT1, VOID *, VOID *, VOID *));
INT1 DEREGISTER_WITH_IP PROTO ((UINT1));

INT1 IgmpGlobalMemInit PROTO((VOID));
INT4 IgmpMemAllocate (tMemPoolId MemPoolId, UINT1 **ppu1Block);
INT4 IgmpMemRelease (tMemPoolId MemPoolId, UINT1 *pu1Block);
VOID * IgmpMemAllocMemBlk PROTO ((tMemPoolId PoolId, UINT4 u4Size));
INT1 IgmpInitInterfaceInfo PROTO((VOID));
tIgmpIface* IgmpCreateInterfaceNode (UINT4 u4IfIndex, UINT1 u1AddrType,
                                     UINT1 u1IfStatus);
tIgmpIface * IgmpGetInterfaceNode PROTO((UINT4 u4IfIndex, UINT1 u1AddrType));
INT4 IgmpDeleteInterfaceNode PROTO((tIgmpIface *pIfaceNode));
INT4 IgmpFindIgmpIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4IgmpIndex));
INT4 IgmpGetIgmpIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4IgmpIndex));
INT4 IgmpDeleteIgmpIndex   PROTO ((UINT4 u4IgmpIndex));

UINT4 IgmpIfNodeAddCriteria (tTMO_HASH_NODE * pCurNode, UINT1 *pu1IfIndex);
INT1 IgmpGetIndexOfProtId PROTO ((UINT1 u1Protid));
INT1 IgmpGetRtList PROTO ((UINT4 u4GrpAddr, UINT1 * tlist));

INT1 IgmpSetInterfaceAdminStatus PROTO ((tIgmpIface *pIfaceNode, 
                                        UINT1 u1Status));
INT1 IgmpInitInterfaces PROTO ((VOID));

INT4 IgmpCheckReporterInList PROTO((tIgmpIPvXAddr u4Reporter, 
   tIgmpSource *pSrc));
INT4 IgmpDeleteReporter PROTO((tIgmpIface *pMcast,
                                      tIgmpSource *pSrc, tIgmpIPvXAddr u4Reporter));

VOID IgmpProcessV3Report PROTO ((tIgmpIface * pIfNode, UINT1 *pu1Buf, 
                                       UINT4 u4BufSize, tIgmpIPvXAddr u4Source));

tIgmpSource *IgmpSourceLookup PROTO ((tIgmpGroup *pGrp, 
          tIgmpIPvXAddr  u4SrcAddress,
                                             tIgmpIPvXAddr  u4Reporter));

tIgmpSource *IgmpISSourcePresentForGroup PROTO ((tIgmpGroup *pGrp));
tIgmpSource *IgmpCreateSource PROTO ((tIgmpGroup *pGrp, 
                                            tIgmpIPvXAddr u4SrcAddress,
                                            tIgmpIPvXAddr u4Reporter));

tIgmpSource *IgmpGetSource PROTO ((tIgmpGroup *pGrp,
                                            tIgmpIPvXAddr u4SrcAddress));

INT4 IgmpCheckSourceInList PROTO ((tIgmpIPvXAddr u4SrcAddress, UINT4,
                                          tIgmpIPvXAddr* pList));

INT4 IgmpCheckSourceInSchQueryList PROTO ( 
                                  (tIgmpIPvXAddr u4SrcAddress, tIgmpSchQuery* pSchQry));

INT4 IgmpCheckSourceInArray PROTO ((tIgmpIPvXAddr u4SrcAddress, 
        UINT1 *pArray, 
        UINT4 u4NumSrc));

VOID IgmpDeleteSource PROTO ((tIgmpGroup *pGrp, 
                   tIgmpSource *pSrc));

tIgmpSource *IgmpAddSource PROTO (( tIgmpGroup *pGrp, 
                            tIgmpIPvXAddr u4SrcAddress,
                                          tIgmpIPvXAddr u4Reporter, UINT1 u1ChkSrc));

INT4 IgmpAddSSMMappedSourcesForStaticGroup PROTO ((tIgmpGroup *, tIgmpSSMMapGrpEntry *,
        tIgmpIface *, tIgmpIPvXAddr *, INT4, UINT1));

INT4 IgmpGroupHandleIsInclude PROTO ((tIgmpIface* pIfNode,
                                             tIgmpIPvXAddr u4GrpAddr,
                               UINT4     u4NoOfSources,
                UINT1*    pu1Sources,
                                             tIgmpIPvXAddr u4Source));

INT4 IgmpGroupHandleIsExclude PROTO ((tIgmpIface* pMcast,
                                            tIgmpIPvXAddr     u4GrpAddr,
                                    UINT4     u4NoOfSources,
                                UINT1*    pu1Sources,
                                      tIgmpIPvXAddr     u4Source,
                                      UINT1     u1Consolidate));
INT4 IgmpGroupHandleToInclude PROTO ((tIgmpIface* pMcast,
                                             tIgmpIPvXAddr     u4GrpAddr,
                                     UINT4     u4NoOfSources,
                                UINT1*    pu1Sources,
                                             tIgmpIPvXAddr     u4Reporter));
INT4 IgmpGroupHandleToExclude PROTO ((tIgmpIface* pMcast,
                                             tIgmpIPvXAddr     u4GrpAddr,
                                     UINT4     u4NoOfSources,
                                     UINT1*    pu1Sources,
                                             tIgmpIPvXAddr     u4Reporter));
INT4 IgmpGroupHandleAllowNewSources PROTO ((tIgmpIface* pMcast,
                                             tIgmpIPvXAddr u4GrpAddr,
                                    UINT4     u4NoOfSources,
                                    UINT1*    pu1Sources,
                                                   tIgmpIPvXAddr    u4Reporter,
         UINT1 u1SGJoinFlg));
INT4 IgmpGroupHandleBlockOldSources PROTO ((tIgmpIface* pMcast,
                                             tIgmpIPvXAddr u4GrpAddr,
                                    UINT4     u4NoOfSources,
                                    UINT1*    pu1Sources,
                                                   tIgmpIPvXAddr     u4Reporter,
         UINT1 u1SGJoinFlg));

INT4 IgmpTxV2SchQuery PROTO ((tIgmpIface *pIfnode,
                                   tIgmpSchQuery *pSchQuery ));
INT4 IgmpTxSchQuery PROTO ((tIgmpIface *pIfnode,
                                   tIgmpSchQuery *pSchQuery ));

VOID IgmpSendGroupSpecificQuery PROTO((tIgmpIface *pMcast, 
                                              tIgmpGroup *pGrp));
VOID
IgmpMldSendSchQuery (tIgmpIface * pIfNode, tIgmpSchQuery  *pSchQuery);

VOID IgmpSendGroupAndSourceSpecificQuery PROTO( (tIgmpIface *pMcast, 
                                                        tIgmpGroup *pGrp,
                                                        UINT4 u4NoOfSrcs,
                            tIgmpIPvXAddr *pSources) );

INT4 IgmpScheduleQuery PROTO( (tIgmpIface *pIfNode , 
                                      tIgmpGroup *pGrp,
                 UINT4 u4NoOfSrcs, 
                                      tIgmpIPvXAddr *pSources) );

VOID IgmpHdlTransitionToNonQuerier (tIgmpIface *pIfNode);
VOID IgmpDelSchQrySrcNode (tIgmpSource *pSrc);
VOID IgmpDelSchQryGrpNode (tIgmpGroup *pGrp);
                
VOID IgmpExpireSource PROTO ((tIgmpSource * pSrc));

/* Prototypes for IGMP standard MIB RFC:2933 starts here */
INT1
nmhSetFsIgmpFastLeaveStatus ARG_LIST((INT4 ));
INT1
nmhGetFsIgmpFastLeaveStatus ARG_LIST((INT4 *));



/* Prototypes for Utility routines starts here */
INT1 IgmpUtilGetMembershipInfo PROTO ((UINT4 u4IgmpGrpAddr, UINT4 u4IgmpSrcAddr,
                            UINT4 u4IgmpReporterAddr, INT4 i4IgmpIndex,
                            UINT4 *pu4IgmpGrpAddr, UINT4 *pu4IgmpSrcAddr,
                            UINT4 *pu4IgmpReporterAddr, INT4 *pi4IgmpIndex));
UINT1 IgmpUtilGetMcastRouteEntryStatus PROTO ((UINT4 u4IgmpIndex, UINT4 u4Group));
INT1  IgmpUtilTstMcastRouteEntryStatus PROTO ((UINT4 u4Status));
UINT1 IgmpUtilSetMcastRouteEntryStatus PROTO ((UINT4 u4IgmpIndex,
                                               UINT4 u4Group, UINT1 u1Status));
INT1  IgmpUtilGetMcastRouteTableFirstIndex PROTO ((UINT4 * pu4Addr,
                                                  INT4 * pi4Index));
UINT1 IgmpUtilGetMcastNextGroupIndex PROTO ((UINT4 * pu4Addr));
UINT1 IgmpUtilGetMcastFirstIndices PROTO ((UINT1 u1ListIdx, INT4 * pi4Index));
UINT1 IgmpUtilGetMcastNextIndices PROTO ((UINT1 u1ListIdx, INT4 * pi4Index));
INT1  IgmpUtilGetMcastRouteTableNextIndex PROTO ((UINT4 * pu4Addr,
                                                 INT4 * pi4Index));
UINT1 IgmpUtilValidateMcastRouteTableInstance PROTO ((UINT4 u4Addr,
                                                      UINT4 u4IgmpIndex));
UINT1 IgmpUtilAddMcastRouteTableInstance PROTO ((UINT4 u4Addr, UINT1 u1Index));
INT1  IgmpGetFwdIfList (tCRU_BUF_CHAIN_HEADER * pBuf,
                            t_IP * pIp, UINT2 u2Port, UINT4 * pu4IfList);
INT1
IgmpUtilGetConfiguredQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                    UINT4 *pu4IgmpInterfaceConfiguredQueryIntvl);

/* Added support for IGMP standard MIB RFC:2933
 * Added these two functions to support
 * Mib objects : IgmpCacheStatus & IgmpInterfaceStatus
 */
VOID IgmpUtilInitInterface (UINT4 u4IfIndex, UINT1 u1AddrType,
                             tIgmpIface * pIfNode);
tIgmpIface* IgmpUtilCreateMcastRouteEntry (UINT4 u4IgmpIndex, UINT4 u4Group);
tIgmpIface * IgmpUtilGetIgmpCacheEntry (UINT4 u4IfIndex, UINT4 u4Group);


tIgmpGroup *
IgmpUtilCreateGroupEntry (tIgmpIface *pIface, UINT4 u4Group); 
tIgmpSource *
IgmpUtilCreateSourceEntry (tIgmpGroup *,  UINT4 u4Source, 
 UINT1 u1Status); 

INT1 nmhGetFsNoOfMcastGroups(INT4 *pi4RetValFsNoOfMcastGroups);
INT1 nmhSetFsNoOfMcastGroups(INT4 i4SetValFsNoOfMcastGroups);
INT1 nmhTestv2FsNoOfMcastGroups(UINT4 *pu4ErrorCode , 
                                INT4 i4TestValFsNoOfMcastGroups);


VOID IgmpProcessTimerExpiryEvent ARG_LIST((VOID));

#ifdef DEBUG_WANTED 
PUBLIC VOID IgmpPktDumpEnable  ARG_LIST ((VOID));
PUBLIC VOID IgmpPktDumpDisable ARG_LIST((VOID));
PUBLIC VOID IgmpDbgPrintIf     ARG_LIST ((UINT4 u4IfAddr));
PUBLIC VOID IgmpDbgPrintAllIfs ARG_LIST((VOID));
PUBLIC VOID IgmpDbgIfStatShow  ARG_LIST ((UINT4 u4IfAddr));
PUBLIC VOID IgmpDbgGrpMbrInfo  ARG_LIST ((VOID));
PUBLIC VOID DbgDumpIgmpPkt     ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                         UINT2 u2Len, 
                                         UINT1 u1Type, UINT1 u1PktDirection, 
                                         tIgmpIPvXAddr u4SrcAddr, tIgmpIPvXAddr u4DestAddr));

#endif /* DEBUG_WANTED */
INT4  IgmpSendPktToIp (tIP_BUF_CHAIN_HEADER * pBuf,tIgmpIPvXAddr u4Src,tIgmpIPvXAddr u4Dest, 
              UINT2 u2Len, UINT2 u2Port, UINT1 u1PktType); 
const char *
IgmpGetModuleName(UINT2 u2TraceModule);

const char *
IgmpTrcGetModuleName(UINT2 u2TraceModule);



VOID
IgmpFormIPHdr (tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT4 u4Src, UINT4 u4Dest, UINT1 u1PktType, UINT2 u2Len);

/* IGMP Proxy related function prototypes */
UINT1 IgpUtilCheckUpIface (UINT4 u4IfIndex);
VOID IgpPortHandleMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer);
VOID IgpMainHandleProxyEvents (UINT4 u4Event);
INT4 IpIsRtrAlertPresent (t_IP * pIp);
INT4 IgmpCheckProxyStatus (VOID);

INT4 IgpGrpUpdateGroupTable (tIgmpGroup *pIgmpGroupEntry, UINT1 u1Version, 
                        UINT1 u1NewInterface);
VOID IgpGrpConsolidate (tIgmpGroup *pIgmpGroupEntry, UINT1 u1Version, 
                   UINT1 u1GrpIfaceStat);
INT4 IgpUpIfFormAndSendV3Report (UINT4);
INT4 IgpUtilSendQuerierTrap (UINT4 u4Port, tIgmpIPvXAddr u4QuerierAddr);
INT4 IgpUpIfProcessQuery (UINT4 , tIgmpIPvXAddr , UINT4, UINT4 *, UINT1);
INT4 IgpUpIfDeleteUpIfaceEntry (UINT4 u4IfIndex, UINT1 u1Flag);
VOID
IgpUpIfUpdateUpIface (UINT4 u4IfIndex);
VOID IgpUtilRegisterFsIgpMib (VOID);
#ifdef MFWD_WANTED
INT4 IgpMfwdAddInterface (UINT4 u4IfIndex);
INT4 IgpMfwdDelInterface (UINT4 u4IfIndex);
#endif

#ifdef MBSM_WANTED
INT4 
IgpFwdHandleMbsmEvent (tMbsmSlotInfo *pSlotInfo);
#endif

/* UTIL related prototypes */
/*prototypes for stutlget.c */
INT1
IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable (INT4 i4IgmpInterfaceIfIndex,
                                                   INT4 i4IgmpInterfaceAddrType);
INT1
IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable (INT4 *pi4IgmpInterfaceIfIndex,
                                            INT4 *pi4IgmpInterfaceAddrType);
INT1
IgmpMgmtUtilNmhGetNextIndexIgmpInterfaceTable (INT4 i4IgmpInterfaceIfIndex,
                                   INT4 *pi4NextIgmpInterfaceIfIndex,
                                   INT4 i4IgmpInterfaceAddrType,
                                   INT4 *pi4NextIgmpInterfaceAddrType);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                  INT4 i4IgmpInterfaceAddrType,
                                  UINT4 *pu4RetValIgmpInterfaceQueryInterval);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceStatus (INT4 i4IgmpInterfaceIfIndex,
                           INT4 i4IgmpInterfaceAddrType,
                           INT4 *pi4RetValIgmpInterfaceStatus);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceVersion (INT4 i4IgmpInterfaceIfIndex,
                            INT4  i4IgmpInterfaceAddrType,
                            UINT4 *pu4RetValIgmpInterfaceVersion);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQuerier (INT4 i4IgmpInterfaceIfIndex,
                            INT4 i4IgmpInterfaceAddrType,
                            tIgmpIPvXAddr *pu4RetValIgmpInterfaceQuerier);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQueryMaxResponseTime (INT4 i4IgmpInterfaceIfIndex,
                            INT4 i4IgmpInterfaceAddrType,
                            UINT4 *pu4RetValIgmpInterfaceQueryMaxResponseTime);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQuerierUpTime (INT4 i4IgmpInterfaceIfIndex,
                                  INT4 i4IgmpInterfaceAddrType,
                                  UINT4 *pu4RetValIgmpInterfaceQuerierUpTime);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQuerierExpiryTime (INT4 i4IgmpInterfaceIfIndex,
                               INT4 i4IgmpInterfaceAddrType,
                               UINT4 *pu4RetValIgmpInterfaceQuerierExpiryTime);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceVersion1QuerierTimer (INT4 i4IgmpInterfaceIfIndex,
                            INT4 i4IgmpInterfaceAddrType,
                            UINT4 *pu4RetValIgmpInterfaceVersion1QuerierTimer);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceWrongVersionQueries (INT4 i4IgmpInterfaceIfIndex,
                             INT4 i4IgmpInterfaceAddrType,
                             UINT4 *pu4RetValIgmpInterfaceWrongVersionQueries);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceJoins (INT4 i4IgmpInterfaceIfIndex,
                          INT4 i4IgmpInterfaceAddrType,
                          UINT4 *pu4RetValIgmpInterfaceJoins);

INT1
IgmpMgmtUtilNmhGetIgmpInterfaceProxyIfIndex (INT4 i4IgmpInterfaceIfIndex,
                                 INT4 i4IgmpInterfaceAddrType,
                                 INT4 *pi4RetValIgmpInterfaceProxyIfIndex);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceGroups (INT4 i4IgmpInterfaceIfIndex,
                           INT4 i4IgmpInterfaceAddrType,
                           UINT4 *pu4RetValIgmpInterfaceGroups);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceRobustness (INT4 i4IgmpInterfaceIfIndex,
                               INT4  i4IgmpInterfaceAddrType,
                               UINT4 *pu4RetValIgmpInterfaceRobustness);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceLastMembQueryIntvl (INT4 i4IgmpInterfaceIfIndex,
                              INT4 i4IgmpInterfaceAddrType,
                              UINT4 *pu4RetValIgmpInterfaceLastMembQueryIntvl);
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceVersion2QuerierTimer (INT4 i4IgmpInterfaceIfIndex,
                            INT4 i4IgmpInterfaceAddrType,
                            UINT4 *pu4RetValIgmpInterfaceVersion2QuerierTimer);
INT1
IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                                      tIgmpIPvXAddr u4IgmpCacheAddress,
                                      INT4 i4IgmpCacheIfIndex);
INT1
IgmpMgmtUtilNmhGetFirstIndexIgmpCacheTable (INT4 *pi4IgmpCacheAddrType,
                                tIgmpIPvXAddr *pu4IgmpCacheAddress,
                                INT4 *pi4IgmpCacheIfIndex);
INT1
IgmpMgmtUtilNmhGetNextIndexIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                               INT4 *pi4NextIgmpCacheAddrType,
                               tIgmpIPvXAddr u4IgmpCacheAddress,
                               tIgmpIPvXAddr *pu4NextIgmpCacheAddress,
                               INT4 i4IgmpCacheIfIndex,
                               INT4 *pi4NextIgmpCacheIfIndex);
INT1
IgmpMgmtUtilNmhGetIgmpCacheSelf (INT4 i4IgmpCacheAddrType,
                     tIgmpIPvXAddr u4IgmpCacheAddress, INT4 i4IgmpCacheIfIndex,
                     INT4 *pi4RetValIgmpCacheSelf);
INT1
IgmpMgmtUtilNmhGetIgmpCacheLastReporter (INT4 i4IgmpCacheAddrType,
                             tIgmpIPvXAddr u4IgmpCacheAddress,
                             INT4 i4IgmpCacheIfIndex,
                             tIgmpIPvXAddr *pu4RetValIgmpCacheLastReporter);
INT1
IgmpMgmtUtilNmhGetIgmpCacheUpTime (INT4 i4IgmpCacheAddrType,
                       tIgmpIPvXAddr u4IgmpCacheAddress,
                       INT4 i4IgmpCacheIfIndex, UINT4 *pu4RetValIgmpCacheUpTime);
INT1
IgmpMgmtUtilNmhGetIgmpCacheExpiryTime (INT4 i4IgmpCacheAddrType,
                           tIgmpIPvXAddr u4IgmpCacheAddress,
                           INT4 i4IgmpCacheIfIndex,
                           UINT4 *pu4RetValIgmpCacheExpiryTime);
INT1
IgmpMgmtUtilNmhGetIgmpCacheStatus (INT4 i4IgmpCacheAddrType,
                       tIgmpIPvXAddr u4IgmpCacheAddress,
                       INT4 i4IgmpCacheIfIndex, INT4 *pi4RetValIgmpCacheStatus);
INT1
IgmpMgmtUtilNmhGetIgmpCacheVersion1HostTimer (INT4 i4IgmpCacheAddrType,
                                  tIgmpIPvXAddr u4IgmpCacheAddress,
                                  INT4 i4IgmpCacheIfIndex,
                                  UINT4 *pu4RetValIgmpCacheVersion1HostTimer);
INT1
IgmpMgmtUtilNmhGetIgmpCacheVersion2HostTimer (INT4 i4IgmpCacheAddrType,
                                  tIgmpIPvXAddr u4IgmpCacheAddress,
                                  INT4 i4IgmpCacheIfIndex,
                                  UINT4 *pu4RetValIgmpCacheVersion2HostTimer);
INT1
IgmpMgmtUtilNmhGetIgmpCacheSourceFilterMode (INT4 i4IgmpCacheAddrType,
                                 tIgmpIPvXAddr u4IgmpCacheAddress,
                                 INT4 i4IgmpCacheIfIndex,
                                 INT4 *pi4RetValIgmpCacheSourceFilterMode);
INT1
IgmpMgmtUtilNmhValidateIndexInstanceIgmpSrcListTable (INT4 i4IgmpSrcListAddrType,
                                          tIgmpIPvXAddr u4IgmpSrcListAddress,
                                          INT4 i4IgmpSrcListIfIndex,
                                          tIgmpIPvXAddr u4IgmpSrcListHostAddress);
INT1
IgmpMgmtUtilNmhGetFirstIndexIgmpSrcListTable (INT4 *pi4IgmpSrcListAddrType,
                                  tIgmpIPvXAddr *pu4IgmpSrcListAddress,
                                  INT4 *pi4IgmpSrcListIfIndex,
                                  tIgmpIPvXAddr *pu4IgmpSrcListHostAddress);
INT1
IgmpMgmtUtilNmhGetNextIndexIgmpSrcListTable (INT4 i4IgmpSrcListAddrType,
                                 INT4 *pi4NextIgmpSrcListAddrType,
                                 tIgmpIPvXAddr u4IgmpSrcListAddress,
                                 tIgmpIPvXAddr *pu4NextIgmpSrcListAddress,
                                 INT4 i4IgmpSrcListIfIndex,
                                 INT4 *pi4NextIgmpSrcListIfIndex,
                                 tIgmpIPvXAddr u4IgmpSrcListHostAddress,
                                 tIgmpIPvXAddr *pu4NextIgmpSrcListHostAddress);
INT1
IgmpMgmtUtilNmhGetIgmpSrcListStatus (INT4 i4IgmpSrcListAddrType,
                         tIgmpIPvXAddr u4IgmpSrcListAddress,
                         INT4 i4IgmpSrcListIfIndex,
                         tIgmpIPvXAddr u4IgmpSrcListHostAddress,
                         INT4 *pi4RetValIgmpSrcListStatus);
INT1
IgmpMgmtUtilNmhGetMgmdRouterInterfaceLastMemberQueryCount (
    INT4 i4IgmpInterfaceIfIndex, INT4 i4IgmpInterfaceAddrType,
    UINT4 *pu4RetValIgmpInterfaceLastMemberQueryCount);
INT1
IgmpMgmtUtilNmhGetMgmdRouterInterfaceStartupQueryCount (
    INT4 i4IgmpInterfaceIfIndex, INT4 i4IgmpInterfaceAddrType,
    UINT4 *pu4RetValIgmpInterfaceStartupQueryCount);
INT1
IgmpMgmdUtilNmhGetMgmdRouterInterfaceStartupQueryInterval (
    INT4 i4IgmpInterfaceIfIndex, INT4 i4IgmpInterfaceAddrType,
    UINT4 *pu4RetValIgmpInterfaceStartupQueryInterval);
INT1
IgmpMgmdUtilNmhGetMgmdRouterCacheExcludeModeExpiryTimer (
    INT4 i4IgmpCacheAddrType, tIgmpIPvXAddr IgmpCacheAddrX, INT4 i4IgmpCacheIfIndex,
    UINT4 *pu4RetValIgmpCacheExcludeModeExpiryTimer);
INT1
IgmpMgmtUtilNmhGetMgmdRouterSrcListExpire (
    INT4 i4IgmpSrcListAddrType, tIgmpIPvXAddr IgmpSrcListAddrX,
    INT4 i4IgmpSrcListIfIndex, tIgmpIPvXAddr IgmpSrcListHostAddrX,
    UINT4 *pu4RetValIgmpSrcListExpire);
extern INT1 nmhGetIssConfigSaveStatus (INT4 *pi4RetValIssConfigSaveStatus);
/*prototypes for stutlset.c */
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                  INT4 i4IgmpInterfaceQuerierType,
                                  UINT4 u4SetValIgmpInterfaceQueryInterval);
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceStatus (INT4 i4IgmpInterfaceIfIndex,
                           INT4 i4IgmpInterfaceQuerierType,
                           INT4 i4SetValIgmpInterfaceStatus);
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceVersion (INT4 i4IgmpInterfaceIfIndex,
                            INT4 i4IgmpInterfaceQuerierType,
                            UINT4 u4SetValIgmpInterfaceVersion);
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceQueryMaxResponseTime (INT4 i4IgmpInterfaceIfIndex,
                                INT4 i4IgmpInterfaceQuerierType,
                                UINT4 u4SetValIgmpInterfaceQueryMaxResponseTime);
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceProxyIfIndex (INT4 i4IgmpInterfaceIfIndex,
                                 INT4 i4IgmpInterfaceQuerierType,
                                 INT4 i4SetValIgmpInterfaceProxyIfIndex);
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceRobustness (INT4 i4IgmpInterfaceIfIndex,
                               INT4 i4IgmpInterfaceQuerierType,
                               UINT4 u4SetValIgmpInterfaceRobustness);
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceLastMembQueryIntvl (INT4 i4IgmpInterfaceIfIndex,
                                       INT4 i4IgmpInterfaceQuerierType,
                                       UINT4
                                       u4SetValIgmpInterfaceLastMembQueryIntvl);
INT1
IgmpMgmtUtilNmhSetIgmpCacheSelf (tIgmpIPvXAddr u4IgmpCacheAddress,
                     INT4  i4IgmpCacheAddrType,
                     INT4 i4IgmpCacheIfIndex, INT4 i4SetValIgmpCacheSelf);
INT1
IgmpMgmtUtilNmhSetIgmpCacheStatus (tIgmpIPvXAddr u4IgmpCacheAddress,
                       INT4 i4IgmpCacheAddrType,
                       INT4 i4IgmpCacheIfIndex, INT4 i4SetValIgmpCacheStatus);
INT1
IgmpMgmtUtilNmhSetIgmpSrcListStatus (tIgmpIPvXAddr u4IgmpSrcListAddress,
                         INT4 i4IgmpSrcListAddrType,
                         INT4 i4IgmpSrcListIfIndex,
                         tIgmpIPvXAddr u4IgmpSrcListHostAddress,
                         INT4 i4SetValIgmpSrcListStatus);

/*prototypes for stutltst.c routines */
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval (UINT4 *pu4ErrorCode,
                                     INT4 i4IgmpInterfaceIfIndex,
                                     INT4 i4IgmpInterfaceQuerierType,
                                     UINT4 u4TestValIgmpInterfaceQueryInterval);
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus (UINT4 *pu4ErrorCode,
                              INT4 i4IgmpInterfaceIfIndex,
                              INT4 i4IgmpInterfaceQuerierType,
                              INT4 i4TestValIgmpInterfaceStatus);
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion (UINT4 *pu4ErrorCode,
                               INT4 i4IgmpInterfaceIfIndex,
                               INT4 i4IgmpInterfaceQuerierType,
                               UINT4 u4TestValIgmpInterfaceVersion);
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime (UINT4 *pu4ErrorCode,
                             INT4 i4IgmpInterfaceIfIndex,
                             INT4 i4IgmpInterfaceQuerierType,
                             UINT4 u4TestValIgmpInterfaceQueryMaxResponseTime);
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceProxyIfIndex (UINT4 *pu4ErrorCode,
                                    INT4 i4IgmpInterfaceIfIndex,
                                    INT4 i4IgmpInterfaceQuerierType,
                                    INT4 i4TestValIgmpInterfaceProxyIfIndex);
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness (UINT4 *pu4ErrorCode,
                                  INT4 i4IgmpInterfaceIfIndex,
                                  INT4 i4IgmpInterfaceQuerierType,
                                  UINT4 u4TestValIgmpInterfaceRobustness);
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl (UINT4 *pu4ErrorCode,
                               INT4 i4IgmpInterfaceIfIndex,
                               INT4 i4IgmpInterfaceQuerierType,
                               UINT4 u4TestValIgmpInterfaceLastMembQueryIntvl);
INT1
IgmpMgmtUtilNmhDepv2IgmpInterfaceTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind);
INT1
IgmpMgmtUtilNmhTestv2IgmpCacheSelf (UINT4 *pu4ErrorCode,
                        tIgmpIPvXAddr u4IgmpCacheAddress, INT4 i4IgmpCacheIfIndex,
                        INT4 i4IgmpInterfaceQuerierType, 
                        INT4 i4TestValIgmpCacheSelf);
INT1
IgmpMgmtUtilNmhTestv2IgmpCacheStatus (UINT4 *pu4ErrorCode,
                          tIgmpIPvXAddr u4IgmpCacheAddress,
                          INT4 i4IgmpCacheIfIndex,
                          INT4 i4IgmpCacheAddrType,
                          INT4 i4TestValIgmpCacheStatus);
INT1
IgmpMgmtUtilNmhDepv2IgmpCacheTable (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind);
INT1
IgmpMgmtUtilNmhTestv2IgmpSrcListStatus (UINT4 *pu4ErrorCode,
                            tIgmpIPvXAddr u4IgmpSrcListAddress,
                            INT4 i4IgmpSrcListIfIndex,
                            INT4 i4IgmpSrcListAddrType,
                            tIgmpIPvXAddr u4IgmpSrcListHostAddress,
                            INT4 i4TestValIgmpSrcListStatus);
INT1
IgmpMgmtUtilNmhDepv2IgmpSrcListTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind);

INT1
IgmpMgmtUtilNmhGetFsIgmpGlobalStatus (INT4 *pi4RetValFsIgmpGlobalStatus,
                                      INT4  i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhGetFsIgmpTraceLevel (INT4 *pi4RetValFsIgmpTraceLevel,
                                    INT4 i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhGetFsIgmpDebugLevel (INT4 *pi4RetValFsIgmpDebugLevel,
                                    INT4 i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpInterfaceTable (
    INT4 i4FsIgmpInterfaceIfIndex, INT4 i4FsIgmpInterfaceAddrType);
INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpInterfaceTable (
    INT4 *pi4FsIgmpInterfaceIfIndex, INT4 *pi4FsIgmpInterfaceAddrType);
INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpInterfaceTable (INT4 i4FsIgmpInterfaceIfIndex,
                                     INT4 i4FsIgmpInterfaceAddrType,
                                     INT4 *pi4NextFsIgmpInterfaceIfIndex,
                                     INT4 *pi4FsIgmpInterfaceAddrType);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4FsIgmpInterfaceAddrType,
                                  INT4 *pi4RetValFsIgmpInterfaceAdminStatus);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus (
    INT4 i4FsIgmpInterfaceIfIndex, INT4 i4FsIgmpInterfaceAddrType,
    INT4 *pi4RetValFsIgmpInterfaceFastLeaveStatus);

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceChannelTrackStatus (
    INT4 i4FsIgmpInterfaceIfIndex, INT4 i4FsIgmpInterfaceAddrType,
    INT4 *pi4RetValFsIgmpInterfaceChannelTrackStatus);

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceOperStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                 INT4 i4FsIgmpInterfaceAddrType,
                                 INT4 *pi4RetValFsIgmpInterfaceOperStatus);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingPkts (INT4 i4FsIgmpInterfaceIfIndex,
                                   INT4 i4FsIgmpInterfaceAddrType,
                                   UINT4 *pu4RetValFsIgmpInterfaceIncomingPkts);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingJoins (INT4 i4FsIgmpInterfaceIfIndex,
                                    INT4 i4FsIgmpInterfaceAddrType,
                                    UINT4
                                    *pu4RetValFsIgmpInterfaceIncomingJoins);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingLeaves (INT4 i4FsIgmpInterfaceIfIndex,
                                     INT4 i4FsIgmpInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsIgmpInterfaceIncomingLeaves);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                      UINT1 i4FsIgmpInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsIgmpInterfaceIncomingQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceOutgoingQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                      INT4 i4FsIgmpInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsIgmpInterfaceOutgoingQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGenQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   INT4 i4FsIgmpInterfaceAddrType,
                                   UINT4 *pu4RetValFsIgmpInterfaceRxGenQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   INT4 i4FsIgmpInterfaceAddrType,
                                   UINT4 *pu4RetValFsIgmpInterfaceRxGrpQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpAndSrcQueries (
     INT4 i4FsIgmpInterfaceIfIndex, INT4 i4FsIgmpInterfaceAddrType,
     UINT4 *pu4RetValFsIgmpInterfaceRxGrpAndSrcQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                    INT4 i4FsIgmpInterfaceAddrType,
                                    UINT4
                                    *pu4RetValFsIgmpInterfaceRxv1v2Reports);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4FsIgmpInterfaceAddrType,
                                  UINT4 *pu4RetValFsIgmpInterfaceRxv3Reports);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGenQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   INT4 i4FsIgmpInterfaceAddrType,
                                   UINT4 *pu4RetValFsIgmpInterfaceTxGenQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   INT4 i4FsIgmpInterfaceAddrType,
                                   UINT4 *pu4RetValFsIgmpInterfaceTxGrpQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpAndSrcQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                         INT4 i4FsIgmpInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsIgmpInterfaceTxGrpAndSrcQueries);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                    INT4 i4FsIgmpInterfaceAddrType,
                                    UINT4
                                    *pu4RetValFsIgmpInterfaceTxv1v2Reports);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4FsIgmpInterfaceAddrType,
                                  UINT4 *pu4RetValFsIgmpInterfaceTxv3Reports);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv2Leaves (INT4 i4FsIgmpInterfaceIfIndex,
                                 INT4 i4FsIgmpInterfaceAddrType,
                                 UINT4 *pu4RetValFsIgmpInterfaceTxv2Leaves);
INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                                          tIgmpIPvXAddr u4IgmpCacheAddress,
                                          INT4 i4IgmpCacheIfIndex);
INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpCacheTable (INT4 *pi4IgmpCacheAddrType,
                                  tIgmpIPvXAddr *pu4IgmpCacheAddress,
                                  INT4 *pi4IgmpCacheIfIndex);
INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                                 INT4 *pi4IgmpCacheAddrType,
                                 tIgmpIPvXAddr u4IgmpCacheAddress,
                                 tIgmpIPvXAddr *pu4NextIgmpCacheAddress,
                                 INT4 i4IgmpCacheIfIndex,
                                 INT4 *pi4NextIgmpCacheIfIndex);
INT1
IgmpMgmtUtilNmhGetFsIgmpCacheGroupCompMode (INT4 i4IgmpCacheAddrType,
                                tIgmpIPvXAddr u4IgmpCacheAddress,
                                INT4 i4IgmpCacheIfIndex,
                                INT4 *pi4RetValFsIgmpCacheGroupCompMode);
/*fsutlset routines */
INT1
IgmpMgmtUtilNmhSetFsIgmpGlobalStatus (INT4 i4SetValFsIgmpGlobalStatus,
                                      INT4 i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhSetFsIgmpTraceLevel (INT4 i4SetValFsIgmpTraceLevel,
                                    INT4 i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhSetFsIgmpDebugLevel (INT4 i4SetValFsIgmpDebugLevel,
                                    INT4 i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceAdminStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4FsIgmpInterfaceAddrType,
                                  INT4 i4SetValFsIgmpInterfaceAdminStatus);
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceFastLeaveStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                      INT4 i4FsIgmpInterfaceAddrType,
                                      INT4
                                      i4SetValFsIgmpInterfaceFastLeaveStatus);
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                      INT4 i4FsIgmpInterfaceAddrType,
                                      INT4
                                      i4SetValFsIgmpInterfaceChannelTrackStatus);
/*fsutltst routines */
INT1
IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsIgmpGlobalStatus,
                             INT4 i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsIgmpTraceLevel,
                                   INT4 i4FsIgmpAddrType);
INT1
IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsIgmpDebugLevel,
                                   INT4 i4FsIgmpAddrType);

INT1
IgmpMgmtUtilNmhDepv2FsIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind);
INT1
IgmpMgmtUtilNmhDepv2FsIgmpTraceLevel (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind);
INT1
IgmpMgmtUtilNmhDepv2FsIgmpDebugLevel (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind);
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4FsIgmpInterfaceAddrType,
                                  INT4 i4TestValFsIgmpInterfaceFastLeaveStatus);
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4FsIgmpInterfaceAddrType,
                                  INT4 i4TestValFsIgmpInterfaceChannelTrackStatus);
INT1
IgmpMgmtUtilNmhDepv2FsIgmpInterfaceTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind);
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIgmpInterfaceIfIndex,
                                     INT4 i4FsIgmpInterfaceAddrType,
                                     INT4 i4TestValFsIgmpInterfaceAdminStatus);

/*prototypes for newly added functions in igmputil.c */
INT1
IgmpGetIfIndexFromPort (UINT4 u4Port, INT4 i4AddrType, UINT4 *pu4IfIndex);

INT1
IgmpGetPortFromIfIndex (INT4 i4IfIndex, INT4 i4AddrType, UINT4 *pu4Port);

tIgmpIface *
IgmpGetIfNode (UINT4 u4Port, INT4 i4AddrType);

UINT1              *
IgmpPrintIPvxAddress (tIgmpIPvXAddr AddrX);

UINT1      *
IgmpPrintAddress (UINT1 *pu1Addr, UINT1 u1Afi);

VOID
IgmpUtilUpdateMrps (tIgmpGroup * pGrp, UINT1 u1JoinLeaveFlag);

VOID
IgmpUtilIncMsrForFsScalars (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                            INT4 i4SetVal);
VOID
IgmpUtilIncMsrForFsIgmpTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                            INT4 i4Index, INT4 i4SetVal);
VOID
IgmpUtilIncMsrForIfTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                        INT4 i4Index, CHR1 cDataType, VOID *pSetVal,
                        UINT1 u1RowStatus, INT4 i4AddrType);
VOID
IgmpUtilIncMsrForIgmpCacheTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                               UINT4 u4Addr, INT4 i4Index, INT4 i4SetVal,
                               UINT1 u1RowStatus);
VOID
IgmpUtilIncMsrForIgmpSrcTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                             UINT4 u4CacheAddr, INT4 i4Index, UINT4 u4HostAddr,
                             INT4 i4SetVal, UINT1 u1RowStatus);
VOID
IgmpUtilIncMsrForMldCacheTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                              tSNMP_OCTET_STRING_TYPE *pAddr, INT4 i4Index,
                              INT4 i4SetVal, UINT1 u1RowStatus);





/*added*/
INT4
MldHandleEnableEvent (VOID);

INT1
ClearIgmpStat (UINT1 u1AddrType);

INT1
ClearMldStat (INT4 i4AddrType);

INT4
MldHandleDisableEvent (VOID);

/* IGMP High Availability related functions */
VOID IgmpRedDynDataDescInit (VOID);
VOID IgmpRedDescrTblInit (VOID);
VOID IgmpRedDbNodeInit (tDbTblNode *pDBNode, UINT4 u4Type);
INT4 IgmpRedInitGlobalInfo (VOID);
INT4 IgmpRedRmRegisterProtocols (tRmRegParams * pRmReg);
INT4 IgmpRedDeInitGlobalInfo (VOID);
INT4 IgmpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
VOID IgmpRedHandleRmEvents (VOID);
VOID IgmpRedHandleGoActive (VOID);
VOID IgmpRedHandleGoStandby (tIgmpRmMsg * pMsg);
VOID IgmpRedHandleIdleToActive (VOID);
VOID IgmpRedHandleIdleToStandby (VOID);
VOID IgmpRedHandleStandbyToActive (VOID);
VOID IgmpRedHandleActiveToStandby (VOID);
VOID IgmpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen);
VOID IgmpClearStats (tIgmpIface * pIfaceNode);
VOID IgmpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen);
INT4 IgmpRedRmReleaseMemoryForMsg (UINT1 *pu1Block);
INT4 IgmpRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length);
VOID IgmpRedSendBulkReqMsg (VOID);
VOID IgmpRedDbUtilAddTblNode (tDbTblDescriptor * pIgmpDataDesc,
                           tDbTblNode * pIgmpDbNode);
VOID IgmpRedSyncDynInfo (VOID);
VOID IgmpRedAddAllNodeInDbTbl (VOID);
VOID IgmpRedSendBulkUpdMsg (VOID);
VOID IgmpRedSendBulkUpdTailMsg (VOID);
VOID IgmpRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgmpRedProcessGrpInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgmpRedProcessSourceInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgmpRedProcessSrcReporterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgmpRedProcessQueryInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgpRedProcessUpIntInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgpRedProcessInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgpRedProcessFwdEntryInfo (tRmMsg * pMsg, UINT4 *pu4OffSet);
VOID IgmpRedHwAudit (VOID);
INT4 IgmpRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn);
VOID IgmpStartTimers (VOID);
VOID IgmpProxyStartTimers (VOID);
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceGroupListId ARG_LIST((INT4 ,INT4,UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceLimit ARG_LIST((INT4 , INT4,UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceCurGrpCount ARG_LIST((INT4 ,INT4,UINT4 *));
 
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceCKSumError ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfacePktLenError ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfacePktsWithLocalIP ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceSubnetCheckFailure ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceQryFromNonQuerier ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceQryVersionMisMatch ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceUnknownMsgType ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV1Report ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV2Report ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV3Report ARG_LIST((INT4, INT4, UINT4 *));


INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRouterAlertCheckFailure ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingSSMPkts ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidSSMPkts ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpJoinPktRate ARG_LIST((INT4, INT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceJoinPktRate ARG_LIST((INT4, INT4, INT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceMalformedPkts ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceSocketErrors ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceBadScopeErrors ARG_LIST((INT4, INT4, UINT4 *));

INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceGroupListId ARG_LIST((INT4  ,INT4,UINT4 ));

INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceLimit ARG_LIST((INT4  ,INT4,UINT4 ));

INT1
IgmpMgmtUtilNmhSetFsIgmpJoinPktRate ARG_LIST((INT4, INT4 ));

INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate ARG_LIST((INT4  ,INT4, INT4 ));

INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId ARG_LIST((UINT4 *  ,INT4  ,INT4, UINT4 ));

INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit ARG_LIST((UINT4 *  ,INT4  ,INT4, UINT4 ));
INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpGroupListTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *, INT4));

INT1
IgmpMgmtUtilNmhTestv2FsIgmpJoinPktRate ARG_LIST((UINT4 * ,INT4, INT4 ));

INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate ARG_LIST((UINT4 * ,INT4  ,INT4, INT4 ));

/* Proto type for GET_NEXT Routine.  */

INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpGroupListTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *, INT4));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
IgmpMgmtUtilNmhGetFsIgmpGrpListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4, INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4, INT4 ));

/* Low Level TEST Routines for.  */

INT1
IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4, INT4 ));

INT1
IgmpMgmtUtilNmhGetFsIgmpGlobalLimit ARG_LIST((UINT4 *,INT4));

INT1
IgmpMgmtUtilNmhGetFsIgmpGlobalCurGrpCount ARG_LIST((UINT4 *,INT4));

/* Low Level SET Routine for All Objects.  */

INT1
IgmpMgmtUtilNmhSetFsIgmpGlobalLimit ARG_LIST((INT4,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit ARG_LIST((UINT4 *  ,UINT4,INT4 ));
INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpGroupListTable(UINT4 u4FsIgmpGrpListId , UINT4 u4FsIgmpGrpIP , UINT4 u4FsIgmpGrpPrefixLen, INT4 i4AddrType);

VOID
IgmpUtilIncMsrForIgmpSSMMapTbl (UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4, UINT1);

tIgmpSSMMapGrpEntry * IgmpUtilChkIfSSMMappedGroup (tIgmpIPvXAddr , INT4 *);

tIgmpSSMMapGrpEntry * IgmpUtilChkIfSSMMapExists (tIgmpIPvXAddr , INT4 *);

INT4
IgmpUtilChkIfSuperSetOfSSMMappedGroup (tIgmpIPvXAddr StartAddrIn, tIgmpIPvXAddr EndAddrIn);

INT1
IgmpUtilDeleteSSMMappedGroup (tIgmpIPvXAddr, tIgmpIPvXAddr, tIgmpIPvXAddr);

VOID IgmpUtilClearAllSSMMappedGroups (VOID);

tIgmpSSMMapGrpEntry * IgmpGrpLookUpInSSMMapTable (tIgmpIPvXAddr, tIgmpIPvXAddr, tIgmpIPvXAddr);

INT1
IgmpChngStarGToSGJoinForSSMMappedGrp (tIgmpIface *, tIgmpSSMMapGrpEntry *, 
                            tIgmpIPvXAddr, tIgmpIPvXAddr, UINT1);

INT1
IgmpMatchGroupList(tIgmpIface * pIfNode, tIgmpIPvXAddr GrpAddr);

INT1 IgmpCheckForLimit (tIgmpIface * pIfNode, tIgmpIPvXAddr u4GrpAddr);
INT4 IgmpRBTreeGroupListCmp(tRBElem * pGroupEntryNode, tRBElem * pGroupEntryIn);
VOID IgmpResetAllInterfaceLimitCounters(VOID);
VOID IgmpResetLimitFlag(tIgmpIface   *pIfaceNode);
INT1 IgmpApplyLimitForGroup(tIgmpIface *pIfNode, tIgmpGroup *pGrp);
INT4 IgmpNpSetIfaceJoinRate (INT4,INT4,INT4);
INT4
IgmpFormAndSendV1V2Report (tIgmpIPvXAddr u4GrpAddr, UINT1 u1Version,
                           UINT1 u1Leave, UINT4 u4IfIndex);
INT4
IgmpSendReport (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4IfIndex,
                tIgmpIPvXAddr u4GrpAddress, UINT2 u2Len,
                UINT1 u1Version);
INT4
IgmpPortSocketSend (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4Src,
                    UINT4 u4Dest, UINT2 u2Len, UINT1 u1CntlPkt);

VOID
SortIgmpSSMMappedSources (tIgmpIPvXAddr * SrcAddr);

INT4
IgmpSSMMapRBTreeGroupEntryCmp (tRBElem *, tRBElem *);

INT1
IgmpMgmtUtilNmhGetFsIgmpSSMMapStatus (INT4 *, INT4);

INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable (UINT4, UINT4, UINT4);

INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable (UINT4 *, UINT4 *, UINT4 *);

INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);

INT1
IgmpMgmtUtilNmhGetFsIgmpSSMMapRowStatus (UINT4, UINT4, UINT4, INT4 *);

INT1
IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus (INT4, INT4);

INT1
IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus (UINT4, UINT4, UINT4, INT4);

INT1
IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus(UINT4 *, UINT4, UINT4, UINT4, INT4);

INT1
IgmpMgmtUtilNmhTestv2FsIgmpSSMMapStatus (UINT4 *, INT4, INT4);

INT1
IgmpChkIfGrpSrcExists (tIgmpIface *pIfNode);

#endif /* _IGMPPROT_H */
