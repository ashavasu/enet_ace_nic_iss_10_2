/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmgmdlw.h,v 1.9 2016/06/24 09:42:18 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMgmdIgmpGlobalStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMgmdIgmpTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMgmdIgmpDebugLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMgmdMldGlobalStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMgmdMldTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMgmdMldDebugLevel ARG_LIST((INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMgmdIgmpGlobalStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMgmdIgmpTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMgmdIgmpDebugLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMgmdMldGlobalStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMgmdMldTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMgmdMldDebugLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMgmdIgmpGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMgmdIgmpTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMgmdIgmpDebugLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMgmdMldGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMgmdMldTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMgmdMldDebugLevel ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMgmdIgmpGlobalStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMgmdIgmpTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMgmdIgmpDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMgmdMldGlobalStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMgmdMldTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMgmdMldDebugLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMgmdInterfaceTable. */
INT1
nmhValidateIndexInstanceFsMgmdInterfaceTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMgmdInterfaceTable  */

INT1
nmhGetFirstIndexFsMgmdInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMgmdInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMgmdInterfaceAdminStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMgmdInterfaceFastLeaveStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMgmdInterfaceOperStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMgmdInterfaceIncomingPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceIncomingJoins ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceIncomingLeaves ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceIncomingQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceOutgoingQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRxGenQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRxGrpQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRxGrpAndSrcQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRxIgmpv1v2Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRxIgmpv3Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRxMldv1Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRxMldv2Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxGenQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxGrpQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxGrpAndSrcQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxIgmpv1v2Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxIgmpv3Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxMldv1Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxMldv2Reports ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceTxLeaves ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceChannelTrackStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMgmdInterfaceGroupListId ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceLimit ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceCurGrpCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceCKSumError ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfacePktLenError ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfacePktsWithLocalIP ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceSubnetCheckFailure ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceQryFromNonQuerier ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceReportVersionMisMatch ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceQryVersionMisMatch ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceUnknownMsgType ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceInvalidV1Report ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceInvalidV2Report ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceInvalidV3Report ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceRouterAlertCheckFailure ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceIncomingSSMPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceInvalidSSMPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdJoinPktRate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMgmdInterfaceJoinPktRate ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMgmdInterfaceMalformedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceSocketErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMgmdInterfaceBadScopeErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMgmdInterfaceAdminStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMgmdInterfaceFastLeaveStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMgmdInterfaceChannelTrackStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMgmdInterfaceGroupListId ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMgmdInterfaceLimit ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMgmdJoinPktRate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMgmdInterfaceJoinPktRate ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMgmdInterfaceAdminStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMgmdInterfaceFastLeaveStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMgmdInterfaceChannelTrackStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMgmdInterfaceGroupListId ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMgmdInterfaceLimit ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMgmdJoinPktRate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMgmdInterfaceJoinPktRate ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMgmdInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMgmdCacheTable. */
INT1
nmhValidateIndexInstanceFsMgmdCacheTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMgmdCacheTable  */

INT1
nmhGetFirstIndexFsMgmdCacheTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMgmdCacheTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMgmdCacheGroupCompMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMgmdGlobalLimit ARG_LIST((UINT4 *));

INT1
nmhGetFsMgmdGlobalCurGrpCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMgmdSSMMapStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMgmdGlobalLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsMgmdSSMMapStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMgmdGlobalLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMgmdSSMMapStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMgmdGlobalLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMgmdSSMMapStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMgmdSSMMapGroupTable. */
INT1
nmhValidateIndexInstanceFsMgmdSSMMapGroupTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMgmdSSMMapGroupTable  */

INT1
nmhGetFirstIndexFsMgmdSSMMapGroupTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMgmdSSMMapGroupTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMgmdSSMMapRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMgmdSSMMapRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMgmdSSMMapRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMgmdSSMMapGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
