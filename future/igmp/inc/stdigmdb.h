/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdigmdb.h,v 1.3 2008/08/20 15:04:36 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDIGMDB_H
#define _STDIGMDB_H

UINT1 IgmpInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 IgmpCacheTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 IgmpSrcListTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 stdigm [] ={1,3,6,1,2,1,85};
tSNMP_OID_TYPE stdigmOID = {7, stdigm};


UINT4 IgmpInterfaceIfIndex [ ] ={1,3,6,1,2,1,85,1,1,1,1};
UINT4 IgmpInterfaceQueryInterval [ ] ={1,3,6,1,2,1,85,1,1,1,2};
UINT4 IgmpInterfaceStatus [ ] ={1,3,6,1,2,1,85,1,1,1,3};
UINT4 IgmpInterfaceVersion [ ] ={1,3,6,1,2,1,85,1,1,1,4};
UINT4 IgmpInterfaceQuerier [ ] ={1,3,6,1,2,1,85,1,1,1,5};
UINT4 IgmpInterfaceQueryMaxResponseTime [ ] ={1,3,6,1,2,1,85,1,1,1,6};
UINT4 IgmpInterfaceQuerierUpTime [ ] ={1,3,6,1,2,1,85,1,1,1,7};
UINT4 IgmpInterfaceQuerierExpiryTime [ ] ={1,3,6,1,2,1,85,1,1,1,8};
UINT4 IgmpInterfaceVersion1QuerierTimer [ ] ={1,3,6,1,2,1,85,1,1,1,9};
UINT4 IgmpInterfaceWrongVersionQueries [ ] ={1,3,6,1,2,1,85,1,1,1,10};
UINT4 IgmpInterfaceJoins [ ] ={1,3,6,1,2,1,85,1,1,1,11};
UINT4 IgmpInterfaceProxyIfIndex [ ] ={1,3,6,1,2,1,85,1,1,1,12};
UINT4 IgmpInterfaceGroups [ ] ={1,3,6,1,2,1,85,1,1,1,13};
UINT4 IgmpInterfaceRobustness [ ] ={1,3,6,1,2,1,85,1,1,1,14};
UINT4 IgmpInterfaceLastMembQueryIntvl [ ] ={1,3,6,1,2,1,85,1,1,1,15};
UINT4 IgmpInterfaceVersion2QuerierTimer [ ] ={1,3,6,1,2,1,85,1,1,1,16};
UINT4 IgmpCacheAddress [ ] ={1,3,6,1,2,1,85,1,2,1,1};
UINT4 IgmpCacheIfIndex [ ] ={1,3,6,1,2,1,85,1,2,1,2};
UINT4 IgmpCacheSelf [ ] ={1,3,6,1,2,1,85,1,2,1,3};
UINT4 IgmpCacheLastReporter [ ] ={1,3,6,1,2,1,85,1,2,1,4};
UINT4 IgmpCacheUpTime [ ] ={1,3,6,1,2,1,85,1,2,1,5};
UINT4 IgmpCacheExpiryTime [ ] ={1,3,6,1,2,1,85,1,2,1,6};
UINT4 IgmpCacheStatus [ ] ={1,3,6,1,2,1,85,1,2,1,7};
UINT4 IgmpCacheVersion1HostTimer [ ] ={1,3,6,1,2,1,85,1,2,1,8};
UINT4 IgmpCacheVersion2HostTimer [ ] ={1,3,6,1,2,1,85,1,2,1,9};
UINT4 IgmpCacheSourceFilterMode [ ] ={1,3,6,1,2,1,85,1,2,1,10};
UINT4 IgmpSrcListAddress [ ] ={1,3,6,1,2,1,85,1,3,1,1};
UINT4 IgmpSrcListIfIndex [ ] ={1,3,6,1,2,1,85,1,3,1,2};
UINT4 IgmpSrcListHostAddress [ ] ={1,3,6,1,2,1,85,1,3,1,3};
UINT4 IgmpSrcListStatus [ ] ={1,3,6,1,2,1,85,1,3,1,4};


tMbDbEntry stdigmMibEntry[]= {

{{11,IgmpInterfaceIfIndex}, GetNextIndexIgmpInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceQueryInterval}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceQueryIntervalGet, IgmpInterfaceQueryIntervalSet, IgmpInterfaceQueryIntervalTest, IgmpInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IgmpInterfaceTableINDEX, 1, 0, 0, "125"},

{{11,IgmpInterfaceStatus}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceStatusGet, IgmpInterfaceStatusSet, IgmpInterfaceStatusTest, IgmpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IgmpInterfaceTableINDEX, 1, 0, 1, NULL},

{{11,IgmpInterfaceVersion}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceVersionGet, IgmpInterfaceVersionSet, IgmpInterfaceVersionTest, IgmpInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IgmpInterfaceTableINDEX, 1, 0, 0, "2"},

{{11,IgmpInterfaceQuerier}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceQueryMaxResponseTime}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceQueryMaxResponseTimeGet, IgmpInterfaceQueryMaxResponseTimeSet, IgmpInterfaceQueryMaxResponseTimeTest, IgmpInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IgmpInterfaceTableINDEX, 1, 0, 0, "100"},

{{11,IgmpInterfaceQuerierUpTime}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceQuerierUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceQuerierExpiryTime}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceQuerierExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceVersion1QuerierTimer}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceVersion1QuerierTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceWrongVersionQueries}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceWrongVersionQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceJoins}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceJoinsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceProxyIfIndex}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceProxyIfIndexGet, IgmpInterfaceProxyIfIndexSet, IgmpInterfaceProxyIfIndexTest, IgmpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IgmpInterfaceTableINDEX, 1, 0, 0, "0"},

{{11,IgmpInterfaceGroups}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceGroupsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpInterfaceRobustness}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceRobustnessGet, IgmpInterfaceRobustnessSet, IgmpInterfaceRobustnessTest, IgmpInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IgmpInterfaceTableINDEX, 1, 0, 0, "2"},

{{11,IgmpInterfaceLastMembQueryIntvl}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceLastMembQueryIntvlGet, IgmpInterfaceLastMembQueryIntvlSet, IgmpInterfaceLastMembQueryIntvlTest, IgmpInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IgmpInterfaceTableINDEX, 1, 0, 0, "10"},

{{11,IgmpInterfaceVersion2QuerierTimer}, GetNextIndexIgmpInterfaceTable, IgmpInterfaceVersion2QuerierTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{11,IgmpCacheAddress}, GetNextIndexIgmpCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpCacheIfIndex}, GetNextIndexIgmpCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpCacheSelf}, GetNextIndexIgmpCacheTable, IgmpCacheSelfGet, IgmpCacheSelfSet, IgmpCacheSelfTest, IgmpCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IgmpCacheTableINDEX, 2, 0, 0, "1"},

{{11,IgmpCacheLastReporter}, GetNextIndexIgmpCacheTable, IgmpCacheLastReporterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpCacheUpTime}, GetNextIndexIgmpCacheTable, IgmpCacheUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpCacheExpiryTime}, GetNextIndexIgmpCacheTable, IgmpCacheExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpCacheStatus}, GetNextIndexIgmpCacheTable, IgmpCacheStatusGet, IgmpCacheStatusSet, IgmpCacheStatusTest, IgmpCacheTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IgmpCacheTableINDEX, 2, 0, 1, NULL},

{{11,IgmpCacheVersion1HostTimer}, GetNextIndexIgmpCacheTable, IgmpCacheVersion1HostTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpCacheVersion2HostTimer}, GetNextIndexIgmpCacheTable, IgmpCacheVersion2HostTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpCacheSourceFilterMode}, GetNextIndexIgmpCacheTable, IgmpCacheSourceFilterModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IgmpCacheTableINDEX, 2, 0, 0, NULL},

{{11,IgmpSrcListAddress}, GetNextIndexIgmpSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IgmpSrcListTableINDEX, 3, 0, 0, NULL},

{{11,IgmpSrcListIfIndex}, GetNextIndexIgmpSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IgmpSrcListTableINDEX, 3, 0, 0, NULL},

{{11,IgmpSrcListHostAddress}, GetNextIndexIgmpSrcListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IgmpSrcListTableINDEX, 3, 0, 0, NULL},

{{11,IgmpSrcListStatus}, GetNextIndexIgmpSrcListTable, IgmpSrcListStatusGet, IgmpSrcListStatusSet, IgmpSrcListStatusTest, IgmpSrcListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IgmpSrcListTableINDEX, 3, 0, 1, NULL},
};
tMibData stdigmEntry = { 30, stdigmMibEntry };
#endif /* _STDIGMDB_H */

