/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mldprot.h,v 1.9 2015/09/25 07:09:04 siva Exp $
 *
 * Description:File containing all the prototypes of MLD.
 *
 *******************************************************************/
#ifndef _MLDPROT_H_
#define _MLDPROT_H_

/* mldin.c */

INT4  MldSendPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tHlToIp6Params * pParams);
INT4  MldCreateSocket (VOID);

INT4
MldSetMcastSocketOption (UINT4 u4IfIndex, UINT1 u1IfStatus);

#ifdef LNXIP6_WANTED
INT4  MLDSetSendSockOptions (VOID);

INT4 MLDSendPktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                              tHlToIp6Params * pParams);
#endif

VOID  MldCloseSocket (VOID);

VOID
MldInPktHandler (tCRU_BUF_CHAIN_HEADER * pBuf);

VOID
MldInPktProcessV2Query (tIgmpIface * pIfNode, tCRU_BUF_CHAIN_HEADER * pBuf,
                        tIPvXAddr *pSenderAddr);
VOID
MldInPktProcessV2Report (tIgmpIface * pIfNode, tCRU_BUF_CHAIN_HEADER * pBuf, 
                         UINT2 u2BufSize, tIPvXAddr *pSenderAddr);

/* mldout.c */
INT1
MldOutSendQuery (tIgmpIface * pIfNode, tIPvXAddr *pGroup);

UINT2
Ip6Checksum (tIp6Addr * , tIp6Addr * , UINT4 , UINT1 ,
             tCRU_BUF_CHAIN_HEADER *);
INT4 MldIpExtractHdr (tIp6Hdr * , tCRU_BUF_CHAIN_HEADER * );

INT4 Ip6IsRtrAlertPresent (tCRU_BUF_CHAIN_HEADER * );

VOID               *
MldBufRead PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset, UINT4 u4Size));

INT4
MldOutCopyMldPktHdr (tCRU_BUF_CHAIN_HEADER *pPktBuf, UINT1 *pLinBuf,
                     UINT4 u4Len, UINT1 *pu1DstAddr, UINT1 *pu1SrcAddr);

INT4
MldOutSendPktToIPv6 (tCRU_BUF_CHAIN_HEADER * pBuf, tIPvXAddr *pSrcAddr,
                     tIPvXAddr *pDstAddr, UINT2 u2Len, UINT4 u4IfIndex);
INT4
MldOutTxV1SchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery);

UINT2
MldOutCalcIpv6CheckSum (UINT1 *pu1Buf, UINT4 u4Size, UINT1 *pSrcAddr,
                        UINT1 *pDestAddr, UINT1 u1Proto);
INT4
MldOutTxV2SchQuery(tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery);

UINT2
MldOutMrcEncode (UINT4 u4Mrc);
/* mldport.c */

INT4
MldPortGetIpPortFrmIndex(UINT4 u4IfIndex, UINT4 *pu4Port);

INT4
MldPortIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo);

PUBLIC VOID
MldPortIpv6ModuleIfChgHandler (tNetIpv6HliParams * pIp6HliParams);

VOID
MldPortHandleV6IfStatusChg (tNetIpv6IfStatChange * pIfInfo);

#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
VOID
MldPortHandleV6IfAddrChg (tNetIpv6AddrChange * pIfAddrInfo);
#endif

INT4
MldPortGetIp6IfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex);

INT4
MldPortEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                          tHlToIp6Params * pMldParams);

INT4 
MldPortFindRegisterEntry (UINT1 protid);

INT4 
MldPortFindFreeRegisterEntry (VOID);

VOID
MldPortUpdateMrps (tIgmpGroup * pGrp, UINT1 u1JoinLeaveFlag);

/* mldstub.c */

UINT1
MldpUtilCheckUpIface (UINT4 u4IfIndex);

INT1
MLDProtoInit ( VOID );
VOID
MLDProcessTmrEvt ( VOID );



#endif

