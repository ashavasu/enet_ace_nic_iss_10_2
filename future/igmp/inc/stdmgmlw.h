/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmgmlw.h,v 1.2 2010/12/17 12:44:51 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for MgmdHostInterfaceTable. */
INT1
nmhValidateIndexInstanceMgmdHostInterfaceTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MgmdHostInterfaceTable  */

INT1
nmhGetFirstIndexMgmdHostInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdHostInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMgmdHostInterfaceQuerier ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMgmdHostInterfaceStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetMgmdHostInterfaceVersion ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdHostInterfaceVersion1QuerierTimer ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdHostInterfaceVersion2QuerierTimer ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdHostInterfaceVersion3Robustness ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMgmdHostInterfaceStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetMgmdHostInterfaceVersion ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetMgmdHostInterfaceVersion3Robustness ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MgmdHostInterfaceStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2MgmdHostInterfaceVersion ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2MgmdHostInterfaceVersion3Robustness ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MgmdHostInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MgmdRouterInterfaceTable. */
INT1
nmhValidateIndexInstanceMgmdRouterInterfaceTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MgmdRouterInterfaceTable  */

INT1
nmhGetFirstIndexMgmdRouterInterfaceTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdRouterInterfaceTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMgmdRouterInterfaceQuerier ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMgmdRouterInterfaceQueryInterval ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetMgmdRouterInterfaceVersion ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceQueryMaxResponseTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceQuerierUpTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceQuerierExpiryTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceWrongVersionQueries ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceJoins ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceProxyIfIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetMgmdRouterInterfaceGroups ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceRobustness ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceLastMemberQueryInterval ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceLastMemberQueryCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceStartupQueryCount ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterInterfaceStartupQueryInterval ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMgmdRouterInterfaceQueryInterval ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetMgmdRouterInterfaceStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetMgmdRouterInterfaceVersion ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetMgmdRouterInterfaceQueryMaxResponseTime ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetMgmdRouterInterfaceProxyIfIndex ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetMgmdRouterInterfaceRobustness ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetMgmdRouterInterfaceLastMemberQueryInterval ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MgmdRouterInterfaceQueryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2MgmdRouterInterfaceStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2MgmdRouterInterfaceVersion ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2MgmdRouterInterfaceQueryMaxResponseTime ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2MgmdRouterInterfaceProxyIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2MgmdRouterInterfaceRobustness ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2MgmdRouterInterfaceLastMemberQueryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MgmdRouterInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MgmdHostCacheTable. */
INT1
nmhValidateIndexInstanceMgmdHostCacheTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MgmdHostCacheTable  */

INT1
nmhGetFirstIndexMgmdHostCacheTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdHostCacheTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMgmdHostCacheUpTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMgmdHostCacheLastReporter ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMgmdHostCacheSourceFilterMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for MgmdRouterCacheTable. */
INT1
nmhValidateIndexInstanceMgmdRouterCacheTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MgmdRouterCacheTable  */

INT1
nmhGetFirstIndexMgmdRouterCacheTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdRouterCacheTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMgmdRouterCacheLastReporter ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMgmdRouterCacheUpTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterCacheExpiryTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterCacheExcludeModeExpiryTimer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterCacheVersion1HostTimer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterCacheVersion2HostTimer ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetMgmdRouterCacheSourceFilterMode ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for MgmdInverseHostCacheTable. */
INT1
nmhValidateIndexInstanceMgmdInverseHostCacheTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MgmdInverseHostCacheTable  */

INT1
nmhGetFirstIndexMgmdInverseHostCacheTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdInverseHostCacheTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for MgmdInverseRouterCacheTable. */
INT1
nmhValidateIndexInstanceMgmdInverseRouterCacheTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MgmdInverseRouterCacheTable  */

INT1
nmhGetFirstIndexMgmdInverseRouterCacheTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdInverseRouterCacheTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for MgmdHostSrcListTable. */
INT1
nmhValidateIndexInstanceMgmdHostSrcListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MgmdHostSrcListTable  */

INT1
nmhGetFirstIndexMgmdHostSrcListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdHostSrcListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMgmdHostSrcListExpire ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for MgmdRouterSrcListTable. */
INT1
nmhValidateIndexInstanceMgmdRouterSrcListTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MgmdRouterSrcListTable  */

INT1
nmhGetFirstIndexMgmdRouterSrcListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMgmdRouterSrcListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMgmdRouterSrcListExpire ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));
