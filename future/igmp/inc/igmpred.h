/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpred.h,v 1.3 2015/09/18 10:43:55 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for IGMP Server module.
 *              
 *******************************************************************/
#ifndef __IGMP_RED_H
#define __IGMP_RED_H

/* Represents the message types encoded in the update messages */
typedef enum {
    IGMP_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    IGMP_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    IGMP_RED_GRP_INFO = 4,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as IGMP Group info. */
    IGMP_RED_SRC_INFO,    
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as IGMP Source info. */
    IGMP_RED_SRC_REPORTER_INFO,    
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as IGMP Source Reporters info. */
    IGMP_RED_QUERY_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as IGMP Query info. */
    IGP_RED_UP_INT_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as IGMP Upstream interface info. */
    IGP_RED_FWDENTRY_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as IGMP Forwarding entry info. */
    IGP_RED_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as IGMP Proxy info. */
    IGMP_MAX_DYN_INFO_TYPE,
             /* Number of structures used for dynamic info sync up through DB
              * mechanism. */
}eIgmpRedRmMsgType;

typedef enum{
    IGMP_HA_UPD_NOT_STARTED = 1,/* 1 */
    IGMP_HA_UPD_IN_PROGRESS,    /* 2 */
    IGMP_HA_UPD_COMPLETED,      /* 3 */
    IGMP_HA_UPD_ABORTED,        /* 4 */
    IGMP_HA_MAX_BLK_UPD_STATUS
} eIgmpHaBulkUpdStatus;

/* Macro Definitions for IGMP Server Redundancy */

#define IGMP_RM_GET_NUM_STANDBY_NODES_UP() \
          gIgmpRedGblInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define IGMP_NUM_STANDBY_NODES() gIgmpRedGblInfo.u1NumPeersUp

#define IGMP_RM_BULK_REQ_RCVD() gIgmpRedGblInfo.bBulkReqRcvd

#define IGMP_RED_MSG_SIZE(pInfo) \
        ((pInfo->u1Action == IGMP_RED_ADD_CACHE) ?  \
        IGMP_RED_DYN_INFO_SIZE : 4 + 4)

#define IGMP_IS_STANDBY_UP() \
          ((gIgmpRedGblInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* RM wanted */

#define IGMP_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define IGMP_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define IGMP_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define IGMP_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define IGMP_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define IGMP_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define IGMP_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define IGMP_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define IGMP_RED_MAX_MSG_SIZE        1500
#define IGMP_RED_TYPE_FIELD_SIZE     1
#define IGMP_RED_LEN_FIELD_SIZE      2
#define IGMP_RED_MIM_MSG_SIZE        (1 + 2 + 2)
#define IGMP_RED_DYN_INFO_SIZE       (6 + 4 + 2 + 2 + 1 + 1)

#define IGMP_ONE_BYTE                1
#define IGMP_TWO_BYTES               2
#define IGMP_FOUR_BYTES              4

#define IGMP_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define IGMP_RED_BULK_REQ_MSG_SIZE            3

#define IGMP_RED_BULQ_REQ_SIZE       3

#define IGMP_RED_ADD_CACHE            1
#define IGMP_RED_DEL_CACHE            2

#define IGMP_RED_CACHE_DEL         1
#define IGMP_RED_CACHE_DONT_DEL    0

#define IGMP_GROUP_DB_SIZE      (sizeof (tIgmpGroup) - FSAP_OFFSETOF (tIgmpGroup, u4IfIndex))
#define IGMP_SOURCE_DB_SIZE     (sizeof (tIgmpSource) - FSAP_OFFSETOF (tIgmpSource, u4IfIndex))
#define IGMP_REPORTER_DB_SIZE   (sizeof (tIgmpSrcReporter) - FSAP_OFFSETOF (tIgmpSrcReporter, u4IfIndex))
#define IGMP_QUERY_DB_SIZE      (sizeof (tIgmpQuery) - FSAP_OFFSETOF (tIgmpQuery, u4IfIndex))

/* Function prototypes for IGMP Redundancy */
#endif /* __IGMP_RED_H */
