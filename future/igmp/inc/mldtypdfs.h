
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldtypdfs.h,v 1.4 2015/10/08 10:37:46 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#ifndef _MLDTYPDFS_H_
#define _MLDTYPDFS_H_

#define  MLD_MAX_HOST_ENTRIES       50
#define  MLD_INVALID                0
#define  MLD_NO_LISTENER                1
#define  MLD_DELAY_LISTENER              2
#define  MLD_IDLE_LISTENER               3
#define  MLD_REPORT_TMR             4096 
#define MLD_SET        1
#define MLD_RESET      0

#define  MLDRelBuf            CRU_BUF_Release_MsgBufChain
#define  MLDAllocBuf          CRU_BUF_Allocate_MsgBufChain
#define  MLD_UNSOLICITED_REPORT_INTERVAL 10
#define MLD_SYS_TICKS  SYS_NUM_OF_TIME_UNITS_IN_A_SEC
#define  MLDStartTmr          TmrStartTimer
#define  MLDCreateQ           OsixCreateQ
#define  MLDCreateTimerList   TmrCreateTimerList

#define  IP6_TO_MLD_Q_NAME          (const UINT1 *)"MLDQ"
#define  IP6_TO_MLD_Q_DEPTH         10
#define  IP6_TO_MLD_Q_MODE          OSIX_GLOBAL
#define  MLD_TASK_NAME              (const UINT1 *)"MLD"
#define  MLD_TIMER_EVENT            0x00000010
#define  MLD_NO_LISTENERS                1

typedef struct MLDTmr{
    tTmrAppTimer  node;
    tIp6Addr      McastAddr;
    UINT4         u4IfIndex;
}tMLDTmr;

typedef struct MLDHdr{
    UINT1     u1Type;
    UINT1     u1Code;
    UINT2     u2ChkSum;
    UINT2     u2MaxRespDelay;
    UINT2     u2Reserved;
    tIp6Addr  McastAddr;
}tMLDHdr;

typedef struct MLDHostEntry
{
 tIp6Addr  McastAddr;/*Multicast address */
 tMLDTmr   tRepTmr;   /* Report delay timer */  
 VOID *pBuf;/* Last MLD packet sent */
   VOID      *pParams;
 UINT4     u4IfIndex;   /*Interface Id */ 
 UINT2     u2UsageCount;/* No. of applications using this group */ 
 UINT1     u1State;     /* NO_LISTENERS/DELAY_LISTEER/IDLE_LISTENER */
 UINT1     u1Flag;      /* Flag used for sending done */
}tMLDHostEntry;


tTimerListId        gMldTimerListId;
#define MLD_HOST_IFINDEX(X) \
            gMLDHostTable[X].u4IfIndex

#define MLD_HOST_MCASTADDR(X) \
            gMLDHostTable[X].McastAddr

#define MLD_HOST_STATE(X) \
            gMLDHostTable[X].u1State

#define MLD_HOST_REPORTTIMER(X) \
            gMLDHostTable[X].tRepTmr

#define MLD_HOST_USAGECOUNT(X) \
            gMLDHostTable[X].u2UsageCount

#define MLD_HOST_FLAG(X) \
            gMLDHostTable[X].u1Flag

#define MLD_HOST_BUF(X) \
            gMLDHostTable[X].pBuf

#endif /* _MLDTYPDFS_H_ */

