#ifndef _STDIGMWR_H
#define _STDIGMWR_H
INT4 GetNextIndexIgmpInterfaceTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDIGM(VOID);

VOID UnRegisterSTDIGM(VOID);
INT4 IgmpInterfaceQueryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceStatusGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceVersionGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQuerierGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQueryMaxResponseTimeGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQuerierUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQuerierExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceVersion1QuerierTimerGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceWrongVersionQueriesGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceJoinsGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceProxyIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceGroupsGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceRobustnessGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceLastMembQueryIntvlGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceVersion2QuerierTimerGet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQueryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceStatusSet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceVersionSet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQueryMaxResponseTimeSet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceProxyIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceRobustnessSet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceLastMembQueryIntvlSet(tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQueryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceQueryMaxResponseTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceProxyIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceRobustnessTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceLastMembQueryIntvlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpInterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIgmpCacheTable(tSnmpIndex *, tSnmpIndex *);
INT4 IgmpCacheSelfGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheLastReporterGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheStatusGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheVersion1HostTimerGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheVersion2HostTimerGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheSourceFilterModeGet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheSelfSet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheStatusSet(tSnmpIndex *, tRetVal *);
INT4 IgmpCacheSelfTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpCacheStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpCacheTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIgmpSrcListTable(tSnmpIndex *, tSnmpIndex *);
INT4 IgmpSrcListStatusGet(tSnmpIndex *, tRetVal *);
INT4 IgmpSrcListStatusSet(tSnmpIndex *, tRetVal *);
INT4 IgmpSrcListStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IgmpSrcListTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDIGMWR_H */
