/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsigmpdb.h,v 1.14 2016/06/24 09:42:18 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSIGMPDB_H
#define _FSIGMPDB_H

UINT1 FsIgmpInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsIgmpCacheTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsIgmpGroupListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsIgmpSSMMapGroupTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsigmp [] ={1,3,6,1,4,1,2076,36};
tSNMP_OID_TYPE fsigmpOID = {8, fsigmp};


UINT4 FsIgmpGlobalStatus [ ] ={1,3,6,1,4,1,2076,36,1,1};
UINT4 FsIgmpTraceLevel [ ] ={1,3,6,1,4,1,2076,36,1,2};
UINT4 FsIgmpDebugLevel [ ] ={1,3,6,1,4,1,2076,36,1,3};
UINT4 FsIgmpInterfaceIfIndex [ ] ={1,3,6,1,4,1,2076,36,1,4,1,1};
UINT4 FsIgmpInterfaceAdminStatus [ ] ={1,3,6,1,4,1,2076,36,1,4,1,2};
UINT4 FsIgmpInterfaceFastLeaveStatus [ ] ={1,3,6,1,4,1,2076,36,1,4,1,3};
UINT4 FsIgmpInterfaceOperStatus [ ] ={1,3,6,1,4,1,2076,36,1,4,1,4};
UINT4 FsIgmpInterfaceIncomingPkts [ ] ={1,3,6,1,4,1,2076,36,1,4,1,5};
UINT4 FsIgmpInterfaceIncomingJoins [ ] ={1,3,6,1,4,1,2076,36,1,4,1,6};
UINT4 FsIgmpInterfaceIncomingLeaves [ ] ={1,3,6,1,4,1,2076,36,1,4,1,7};
UINT4 FsIgmpInterfaceIncomingQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,8};
UINT4 FsIgmpInterfaceOutgoingQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,9};
UINT4 FsIgmpInterfaceRxGenQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,10};
UINT4 FsIgmpInterfaceRxGrpQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,11};
UINT4 FsIgmpInterfaceRxGrpAndSrcQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,12};
UINT4 FsIgmpInterfaceRxv1v2Reports [ ] ={1,3,6,1,4,1,2076,36,1,4,1,13};
UINT4 FsIgmpInterfaceRxv3Reports [ ] ={1,3,6,1,4,1,2076,36,1,4,1,14};
UINT4 FsIgmpInterfaceTxGenQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,15};
UINT4 FsIgmpInterfaceTxGrpQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,16};
UINT4 FsIgmpInterfaceTxGrpAndSrcQueries [ ] ={1,3,6,1,4,1,2076,36,1,4,1,17};
UINT4 FsIgmpInterfaceTxv1v2Reports [ ] ={1,3,6,1,4,1,2076,36,1,4,1,18};
UINT4 FsIgmpInterfaceTxv3Reports [ ] ={1,3,6,1,4,1,2076,36,1,4,1,19};
UINT4 FsIgmpInterfaceTxv2Leaves [ ] ={1,3,6,1,4,1,2076,36,1,4,1,20};
UINT4 FsIgmpInterfaceChannelTrackStatus [ ] ={1,3,6,1,4,1,2076,36,1,4,1,21};
UINT4 FsIgmpInterfaceGroupListId [ ] ={1,3,6,1,4,1,2076,36,1,4,1,22};
UINT4 FsIgmpInterfaceLimit [ ] ={1,3,6,1,4,1,2076,36,1,4,1,23};
UINT4 FsIgmpInterfaceCurGrpCount [ ] ={1,3,6,1,4,1,2076,36,1,4,1,24};
UINT4 FsIgmpInterfaceCKSumError [ ] ={1,3,6,1,4,1,2076,36,1,4,1,25};
UINT4 FsIgmpInterfacePktLenError [ ] ={1,3,6,1,4,1,2076,36,1,4,1,26};
UINT4 FsIgmpInterfacePktsWithLocalIP [ ] ={1,3,6,1,4,1,2076,36,1,4,1,27};
UINT4 FsIgmpInterfaceSubnetCheckFailure [ ] ={1,3,6,1,4,1,2076,36,1,4,1,28};
UINT4 FsIgmpInterfaceQryFromNonQuerier [ ] ={1,3,6,1,4,1,2076,36,1,4,1,29};
UINT4 FsIgmpInterfaceReportVersionMisMatch [ ] ={1,3,6,1,4,1,2076,36,1,4,1,30};
UINT4 FsIgmpInterfaceQryVersionMisMatch [ ] ={1,3,6,1,4,1,2076,36,1,4,1,31};
UINT4 FsIgmpInterfaceUnknownMsgType [ ] ={1,3,6,1,4,1,2076,36,1,4,1,32};
UINT4 FsIgmpInterfaceInvalidV1Report [ ] ={1,3,6,1,4,1,2076,36,1,4,1,33};
UINT4 FsIgmpInterfaceInvalidV2Report [ ] ={1,3,6,1,4,1,2076,36,1,4,1,34};
UINT4 FsIgmpInterfaceInvalidV3Report [ ] ={1,3,6,1,4,1,2076,36,1,4,1,35};
UINT4 FsIgmpInterfaceRouterAlertCheckFailure [ ] ={1,3,6,1,4,1,2076,36,1,4,1,36};
UINT4 FsIgmpInterfaceIncomingSSMPkts [ ] ={1,3,6,1,4,1,2076,36,1,4,1,37};
UINT4 FsIgmpInterfaceInvalidSSMPkts [ ] ={1,3,6,1,4,1,2076,36,1,4,1,38};
UINT4 FsIgmpInterfaceJoinPktRate [ ] ={1,3,6,1,4,1,2076,36,1,4,1,39};
UINT4 FsIgmpCacheAddress [ ] ={1,3,6,1,4,1,2076,36,1,5,1,1};
UINT4 FsIgmpCacheIfIndex [ ] ={1,3,6,1,4,1,2076,36,1,5,1,2};
UINT4 FsIgmpCacheGroupCompMode [ ] ={1,3,6,1,4,1,2076,36,1,5,1,3};
UINT4 FsIgmpGrpListId [ ] ={1,3,6,1,4,1,2076,36,1,6,1,1};
UINT4 FsIgmpGrpIP [ ] ={1,3,6,1,4,1,2076,36,1,6,1,2};
UINT4 FsIgmpGrpPrefixLen [ ] ={1,3,6,1,4,1,2076,36,1,6,1,3};
UINT4 FsIgmpGrpListRowStatus [ ] ={1,3,6,1,4,1,2076,36,1,6,1,4};
UINT4 FsIgmpGlobalLimit [ ] ={1,3,6,1,4,1,2076,36,1,7,1};
UINT4 FsIgmpGlobalCurGrpCount [ ] ={1,3,6,1,4,1,2076,36,1,7,2};
UINT4 FsIgmpSSMMapStatus [ ] ={1,3,6,1,4,1,2076,36,1,7,3};
UINT4 FsIgmpSSMMapStartGrpAddress [ ] ={1,3,6,1,4,1,2076,36,1,8,1,1};
UINT4 FsIgmpSSMMapEndGrpAddress [ ] ={1,3,6,1,4,1,2076,36,1,8,1,2};
UINT4 FsIgmpSSMMapSourceAddress [ ] ={1,3,6,1,4,1,2076,36,1,8,1,3};
UINT4 FsIgmpSSMMapRowStatus [ ] ={1,3,6,1,4,1,2076,36,1,8,1,4};




tMbDbEntry fsigmpMibEntry[]= {

{{10,FsIgmpGlobalStatus}, NULL, FsIgmpGlobalStatusGet, FsIgmpGlobalStatusSet, FsIgmpGlobalStatusTest, FsIgmpGlobalStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsIgmpTraceLevel}, NULL, FsIgmpTraceLevelGet, FsIgmpTraceLevelSet, FsIgmpTraceLevelTest, FsIgmpTraceLevelDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsIgmpDebugLevel}, NULL, FsIgmpDebugLevelGet, FsIgmpDebugLevelSet, FsIgmpDebugLevelTest, FsIgmpDebugLevelDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsIgmpInterfaceIfIndex}, GetNextIndexFsIgmpInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceAdminStatus}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceAdminStatusGet, FsIgmpInterfaceAdminStatusSet, FsIgmpInterfaceAdminStatusTest, FsIgmpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIgmpInterfaceTableINDEX, 1, 0, 0, "1"},

{{12,FsIgmpInterfaceFastLeaveStatus}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceFastLeaveStatusGet, FsIgmpInterfaceFastLeaveStatusSet, FsIgmpInterfaceFastLeaveStatusTest, FsIgmpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIgmpInterfaceTableINDEX, 1, 0, 0, "0"},

{{12,FsIgmpInterfaceOperStatus}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceIncomingPkts}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceIncomingPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceIncomingJoins}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceIncomingJoinsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceIncomingLeaves}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceIncomingLeavesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceIncomingQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceIncomingQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceOutgoingQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceOutgoingQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceRxGenQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceRxGenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceRxGrpQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceRxGrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceRxGrpAndSrcQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceRxGrpAndSrcQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceRxv1v2Reports}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceRxv1v2ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceRxv3Reports}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceRxv3ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceTxGenQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceTxGenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceTxGrpQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceTxGrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceTxGrpAndSrcQueries}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceTxGrpAndSrcQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceTxv1v2Reports}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceTxv1v2ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceTxv3Reports}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceTxv3ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceTxv2Leaves}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceTxv2LeavesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceChannelTrackStatus}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceChannelTrackStatusGet, FsIgmpInterfaceChannelTrackStatusSet, FsIgmpInterfaceChannelTrackStatusTest, FsIgmpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIgmpInterfaceTableINDEX, 1, 0, 0, "0"},

{{12,FsIgmpInterfaceGroupListId}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceGroupListIdGet, FsIgmpInterfaceGroupListIdSet, FsIgmpInterfaceGroupListIdTest, FsIgmpInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceLimit}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceLimitGet, FsIgmpInterfaceLimitSet, FsIgmpInterfaceLimitTest, FsIgmpInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceCurGrpCount}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceCurGrpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceCKSumError}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceCKSumErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfacePktLenError}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfacePktLenErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfacePktsWithLocalIP}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfacePktsWithLocalIPGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceSubnetCheckFailure}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceSubnetCheckFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceQryFromNonQuerier}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceQryFromNonQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceReportVersionMisMatch}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceReportVersionMisMatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceQryVersionMisMatch}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceQryVersionMisMatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceUnknownMsgType}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceUnknownMsgTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceInvalidV1Report}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceInvalidV1ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceInvalidV2Report}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceInvalidV2ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceInvalidV3Report}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceInvalidV3ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceRouterAlertCheckFailure}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceRouterAlertCheckFailureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceIncomingSSMPkts}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceIncomingSSMPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceInvalidSSMPkts}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceInvalidSSMPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsIgmpInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpInterfaceJoinPktRate}, GetNextIndexFsIgmpInterfaceTable, FsIgmpInterfaceJoinPktRateGet, FsIgmpInterfaceJoinPktRateSet, FsIgmpInterfaceJoinPktRateTest, FsIgmpInterfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIgmpInterfaceTableINDEX, 1, 0, 0, "0"},

{{12,FsIgmpCacheAddress}, GetNextIndexFsIgmpCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpCacheTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpCacheIfIndex}, GetNextIndexFsIgmpCacheTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIgmpCacheTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpCacheGroupCompMode}, GetNextIndexFsIgmpCacheTable, FsIgmpCacheGroupCompModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIgmpCacheTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpGrpListId}, GetNextIndexFsIgmpGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsIgmpGroupListTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpGrpIP}, GetNextIndexFsIgmpGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpGroupListTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpGrpPrefixLen}, GetNextIndexFsIgmpGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpGroupListTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpGrpListRowStatus}, GetNextIndexFsIgmpGroupListTable, FsIgmpGrpListRowStatusGet, FsIgmpGrpListRowStatusSet, FsIgmpGrpListRowStatusTest, FsIgmpGroupListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIgmpGroupListTableINDEX, 3, 0, 1, NULL},

{{11,FsIgmpGlobalLimit}, NULL, FsIgmpGlobalLimitGet, FsIgmpGlobalLimitSet, FsIgmpGlobalLimitTest, FsIgmpGlobalLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsIgmpGlobalCurGrpCount}, NULL, FsIgmpGlobalCurGrpCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsIgmpSSMMapStatus}, NULL, FsIgmpSSMMapStatusGet, FsIgmpSSMMapStatusSet, FsIgmpSSMMapStatusTest, FsIgmpSSMMapStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsIgmpSSMMapStartGrpAddress}, GetNextIndexFsIgmpSSMMapGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpSSMMapGroupTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpSSMMapEndGrpAddress}, GetNextIndexFsIgmpSSMMapGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpSSMMapGroupTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpSSMMapSourceAddress}, GetNextIndexFsIgmpSSMMapGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpSSMMapGroupTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpSSMMapRowStatus}, GetNextIndexFsIgmpSSMMapGroupTable, FsIgmpSSMMapRowStatusGet, FsIgmpSSMMapRowStatusSet, FsIgmpSSMMapRowStatusTest, FsIgmpSSMMapGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIgmpSSMMapGroupTableINDEX, 3, 0, 1, NULL},
};
tMibData fsigmpEntry = { 56, fsigmpMibEntry };

#endif /* _FSIGMPDB_H */

