/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmptdfs.h,v 1.38 2017/02/06 10:45:26 siva Exp $
 *
 * Description:File containing all the type declarations of  
 *             the data structures used by IGMP.             
 *
 *******************************************************************/
#ifndef IGMPTYPDFS_H
#define IGMPTYPDFS_H

/* DATA STRUCTURE DEFINITIONS */

#ifdef IGMPPRXY_WANTED
/* IGMP Proxy source list bitmap */
typedef UINT1 tIgmPxySrcBmp[IGP_SRC_LIST_SIZE];
#endif

typedef struct
{
    tTmrBlk           TimerBlk;
    UINT1             u1TmrStatus; 
    UINT1             au1Reserved[3];
}  tIgmpTimer;


typedef struct
{
    UINT1             u1PktType;
    UINT1             u1MaxRespTime;
    UINT2             u2CheckSum;
    UINT4             u4GroupAddr;
} tIgmpPkt;

typedef struct MldPkt{
    UINT1     u1Type;
    UINT1     u1Code;
    UINT2     u2ChkSum;
    UINT2     u2MaxRespDelay;
    UINT2     u2Reserved;
    tIp6Addr  McastAddr;
}tMldPkt;

typedef struct MldParams{
    tIp6Addr      SrcAddr;
    UINT4         u4IfIndex;
    UINT2         u2Len;
    UINT2         u2Padding;
}tMldParams;

typedef struct ExtHdr
{
    UINT1  u1NextHdr;  /* Type of Header immediately following the
                        *  Dest Options header
                        */
    UINT1  u1HdrLen;   /* Length of the Destination option header
                        * in 8-octet units, not including the first
                        * 8-octets
                        */
    UINT2 u2Reserved;
}
tMldExtHdr;

typedef struct MldRtAlertHdr {
    UINT1 u1OptType;     /* Option type - for router alert- 5*/
    UINT1 u1OptLen;      /* Option length */
    UINT2 u2OptValue;    /* Option value */
}tMldRtAlertHdr;

typedef struct
{
    tOsixSemId        SemId;
    UINT4             u4IgmpTrcFlag;
    UINT4       u4IgmpDbgFlag;
    UINT4             u4MldTrcFlag;
    UINT4             u4MldDbgFlag;
    UINT4             u4IgmpFastLeave;
    UINT1             au1IgmpTrcMode[12]; 
    tTmrDesc          aIgmpTmrDesc[IGMP_MAX_TIMERS];
                                       /* Array of timer descriptors having
                                        * the function pointer to handle
                                        * timer expiry and Offsets if the
                                        * timer nodes in the data structure.
                                        * Array is indexed using Timer ID */
    INT4              i4SocketId;
    INT4              i4MldSockId;
    INT4              i4ProxyStatus;
    INT4              i4GlobalIgmpRateLimit;
    INT4              i4GlobalMldRateLimit;
    UINT4             u4GlobalGrpLimit;
    UINT4             u4GlobalCurCnt;
    UINT1             u1IgmpStatus; 
    UINT1             u1MldStatus; 
    INT1              i1MldProxyStatus;
    UINT1             u1IgmpSSMMapStatus; 
} tIgmpConfig;

/* ---------------------------------------------------------- */

typedef struct
{
    tMemPoolId          IgmpQueryId;            /* tIgmpQuery */
    tMemPoolId          IgmpIfTblId;            /* tIgmpInterfaceInfo */
    tMemPoolId          IgmpIfId;               /* tIgmpIface */
    tMemPoolId          IgmpSrcSetArrayId;       
    tMemPoolId          IgmpQPoolId;            /* tIgmpQMsg */
    tMemPoolId          IgmpGroupId;            /* tIgmpGroup */
    tMemPoolId          IgmpSourceId;           /* tIgmpSource */
    tMemPoolId          IgmpSrcReporterId;      /* tIgmpSrcReporter */
    tMemPoolId          IgmpSchQueryId;         /* tIgmpSchQuery */  
    tMemPoolId          IgmpSchQuerySourceId;   /* tSourceNode */
    tMemPoolId          IgmpAddrStructArrayId;
    tMemPoolId          IgmpU4SrcsToPktId;
    tMemPoolId          IgmpU1SrcsToPktId;
    tMemPoolId          IgmpRecvBufId;
    tMemPoolId          IgmpRmQueId;
    tMemPoolId          IgmpGrpLstPoolId;
    tMemPoolId          IgmpHostMcastGrpId;  /* tIgmpHostMcastGrpEntry */
    tMemPoolId          IgmpSSMMapGrpId;    /* tIgmpSSMMapGrpEntry */
}tIgmpMemPool;

typedef struct
{
   tIgmpIPvXAddr         au4SrcsSBit[MAX_IGMP_SRCS_FOR_MRP_PER_GROUP];
}tIgmpArrayStr; 



typedef struct
{
    tIgmpTimer        Timer;            /* Query Timer node                  */
    struct _IgmpIface *pIfNode;         /* IGMP interface index              */
    tTMO_SLL       schQueryList;     /* tIgmpSchQuery: list of queries to be transmitted 
                    * on this interface 
                                         */    
    UINT4             u4QuerierUpTime;  /* Time since querier is up 
                    * zero incase of non-querier
                    */
    UINT1             u1StartUpQCount;  /* Number of times to query at the 
                    * start of router 
                    */
    UINT1             u1QRV;            /* Querier Robutstness value */
    UINT1             u1QQIC;           /* Querier's query interval code */
    UINT1       au1Pad1[1]; /* Padding bit */
/* The dynamic entries that are to be synced should be added after the
 * QuerierDbNode. And both the halves should be padded accordingly */ 
    tDbTblNode        QuerierDbNode; /* Data Base node to sync querier node 
      dynamic information */
    UINT4             u4IfIndex;        /* Interface index */
    tIgmpIPvXAddr     u4QuerierAddr;    /* Querier address on this interface */
    UINT1             u1Querier;        /* Flag to indicate querier or not  */
    UINT1       au1Pad2[3]; /* Padding bit */ 
} tIgmpQuery;


/* ---------------------------------------------------------- */
/* This structure holds Interface related information */
typedef struct _IgmpIface
{
    tTMO_SLL_NODE     IfHashLink;
    tTMO_SLL_NODE     IfGetNextLink;
    tTMO_SLL          GrpMbrList;      /* tIgmpGroup: List of Groups on this 
       interface */
    tIgmpQuery        *pIfaceQry;
    UINT4             u4IfIndex;       /* Interface index to access
                          * interface table corresponds to 
                                        * IP port number */
    tIgmpIPvXAddr         u4IfAddr;        /* I/f Address */
    UINT4             u4IfMask;        /* I/f Mask */
    UINT4             u4InPkts;        /* packets received in this iface */
    UINT4             u4InJoins;       /* Joins Received in this iface   */
    UINT4             u4InLeaves;      /* Leaves received in this iface  */
    UINT4             u4InQueries;     /* Queries coming thru this iface */
    UINT4             u4OutQueries;    /* Queries sent out on this iface */
    UINT4             u4WrongVerQueries; /* Wrong version Queries */
    UINT4             u4InterfaceGroups; /* Current groups joins in this i/f */
    UINT4             u4ProcessedJoins;  /* Groups joins that are processed */ 
    UINT4             u4LastMemberQueryIntvl; /* Last member query interval */
    UINT4             u4RxGenQueries;       /* No of General queries received */
    UINT4             u4TxGenQueries;       /* No of General queries sent */
    UINT4             u4TxGrpQueries;       /* No of Group specific queries
                                               sent */
    UINT4             u4TxGrpSrcQueries;    /* No of Group and source specific 
                                               queries sent */
    UINT4             u4IgmpRxv1v2Reports;  /* No of IgmpV1/V2 reports received */
    UINT4             u4IgmpRxv3Reports;    /* No of IgmpV3 reports received */
    UINT4             u4MldRxv1Reports;     /* No of Mldv1 reports received */
    UINT4             u4MldRxv2Reports;     /* No of Mldv2 reports received */
    UINT4             u4RxGrpQueries;       /* No of Group specific queries 
                                               received */
    UINT4             u4RxGrpSrcQueries;    /* No of Group and source specific 
                                               queries received */
    UINT4             u4IgmpTxv1v2Reports;  /* No of IgmpV1/V2 reports sent */
    UINT4             u4IgmpTxv3Reports;    /* No of IgmpV3 reports sent */
    UINT4             u4MldTxv1Reports;     /* No of MldV1 reports sent */
    UINT4             u4MldTxv2Reports;    /* No of MldV2 reports sent */
    UINT4             u4Txv2Leaves;         /* No of V2 leaves sent */
    UINT4             u4Mtu;                /* MTU for the interface */
    UINT4             u4GrpListId;          /* Exclude group list id*/
    UINT4             u4GrpLimit;          /* Group Limit for the interface*/
    UINT4             u4GrpCurCnt;    /*Current count of groups were added in interface*/
    UINT2             u2VlanId;             /* VLAN Identifier for the 
                                               interface */
    UINT2             u2QueryInterval;        /* Query interval  */ 
    UINT1             u1Robustness;      /* Robustness variable of this iface*/
    UINT1             u1EntryStatus;     /* Row status of this i/f */
    UINT1             u1AdminStatus;     /* admin status of this i/f */
    UINT1             u1IfStatus;        /* i/f status */
    UINT1             u1OperStatus;      /* Operational status of this i/f */
    UINT1             u1Version;         /* Version of IGMP  on this i/f  */
    UINT2             u2MaxRespTime;     /* Max Query  Response time   */
    UINT1             u1FastLeaveFlg;
    UINT1             u1ChannelTrackFlg; /* Channel Track status flag */
    UINT1             u1AddrType;
    UINT1             au1Pad[1];
    UINT4           u4CKSumError;
    UINT4           u4PktLenError;
    UINT4           u4PktWithLocalIP;
    UINT4           u4SubnetCheckFailure;
    UINT4           u4QryFromNonQuerier;
    UINT4           u4ReportVersionMisMatch;
    UINT4             u4QryVersionMisMatch;
    UINT4             u4UnknownMsgType;
    UINT4             u4InvalidV1Report;
    UINT4             u4InvalidV2Report;
    UINT4             u4InvalidV3Report;
    UINT4             u4RouterAlertCheckFailure;
    UINT4             u4IgmpInSSMPkts;
    UINT4             u4IgmpInvalidSSMPkts;
    UINT4             u4IgmpJoinPktRate;
    UINT4             u4MldInSSMPkts;
    UINT4             u4MldInvalidSSMPkts;
    UINT4             u4MldJoinPktRate;
    UINT2             u2ConfiguredQueryInterval;        /* Query interval  */
    UINT1             au1Pad2[2];
    UINT4             u4IgmpMalformedPkt;
    UINT4             u4MldMalformedPkt;
    UINT4             u4IgmpSocketErr;
    UINT4             u4MldSocketErr;
    UINT4             u4BadScopeErr;
    UINT4             u4MldBadScopeErr;
    UINT4             u4MldSubnetCheckFailure;
    UINT4             u4MldInvalidIntf;
#ifdef MULTICAST_SSM_ONLY
    UINT4             u4DroppedASMIncomingPkts;
#endif
 } tIgmpIface;

typedef struct {
tTMO_HASH_TABLE       *pIfHashTbl;
UINT4                 u4HashSize;
tTMO_SLL              IfGetNextList;
} tIgmpInterfaceInfo;

/* structure for Interface status */
/* ------------------------------ */
typedef struct _IgmpIfStatusInfo {
    UINT1             u1MsgType;
    UINT1             u1IfStatus;
    UINT1             u1AddrType;
    UINT1             u1AlignByte;
    UINT4             u4IfIndex;
    UINT4             u4CfaIfIndex;
    UINT4             u4SecIPAddr;
    UINT4             u4SecIPMask;
} tIgmpIfStatusInfo;
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
typedef struct
{
    tIPvXAddr  Ip6Addr;                        /* Interface IPv6 Address */
    UINT4      u4PrefixLength;                 /* Address Mask */
    UINT4      u4IfIndex;
    UINT1      u1AddrType;
    UINT1      u1MsgType;
    UINT1      au1Pad[2];
}
tMldIpv6AddrInfo;
#endif
typedef struct 
{
    tTMO_SLL_NODE     Link;    
    tTMO_SLL          SrcList;      /*tSourceNode */
    tIgmpTimer        schTimer;
    tIgmpQuery        *pIgmpQuery;
    tIgmpIPvXAddr         u4Group; 
    UINT1             u1RetrNum;
    UINT1             u1Reserved;
    UINT2             u2Reserved;
}tIgmpSchQuery;

typedef struct
{
    tRBNodeEmbd       RbNode;
    tTMO_SLL          srcList;           /* tIgmpSource: Linked list of source
              entries  */
    tRBTree     GrpReporterTree;
    tIgmpTimer        Timer;             /* Group Timer Id                 */
    tIgmpTimer        Version1HostTimer; /* Version1 Host timer node       */
    tIgmpTimer        Version2HostTimer; /* Version2 Host Timer Node       */
    tIgmpIface        *pIfNode;          /* Interface node                 */ 
    tTMO_SLL          SrcListForMrpUpdate; /* tIgmpSource: Source list for 
                 MRP Updation */
    UINT4             u4CacheUpTime;     /* Cache entry up time            */
    INT1              i1CacheSelfFlag;   /* Flag to indicate whether local
                         * system is member of this group
                         * address on this interface*/      
    UINT1             u1GpSpQcount;      /* Group specific Query count     */
    UINT1             u1EntryStatus;
    UINT1             u1DelSyncFlag;
/* The dynamic entries that are to be synced should be added after the
 * GrpDbNode. And both the halves should be padded accordingly */
    tDbTblNode        GrpDbNode;          /* Data Base node to sync group node
                                                dynamic information */
    UINT4       u4IfIndex;         /* Interface index */
    UINT4             u4GroupCompMode;   /* Group compatibility for V3     */
    tIgmpIPvXAddr     u4Group;           /* Group Address                  */
    tIgmpIPvXAddr     u4LastReporter;    /* Last reporter for this group 
                                          * in this interface              
                                          */
#ifdef IGMPPRXY_WANTED
    tIgmPxySrcBmp     InclSrcBmp;      /* Indicates the set of sources for 
                                          which include registrations are 
                                          received for this group record */
    tIgmPxySrcBmp     ExclSrcBmp;      /* Indicates the set of sources for 
                                          which exclude registrations are 
                                          received for this group record */
#endif
    UINT1       u1GrpModeFlag;  /* Flag to indicate the status of node
      ADD_GROUP/DELETE_GROUP */
    UINT1             u1FilterMode;      /* Group Filter Mode */ 
    UINT1             u1V2HostPresent;   /* Flag to indicate that Version2 host is 
      present on this interface */
    UINT1             u1V1HostPresent;   /* Flag to indicate that version1 host is
       present in this interface */
    UINT1             u1DynamicFlg;      /* This flag if true indicates group 
      has been added dynamically */ 
    UINT1             u1StaticFlg;       /* This flag if true indicates that 
                                          * this group has been statically added
                                          * to the cache.*/
    UINT1       u1LimitFlag;
    UINT1       u1DynSyncFlag; /* Decides whether this group needs 
                                to be synced. This field shouldnt be processed
                                in standby*/
    UINT1       au1Pad2[4];
} tIgmpGroup;

typedef struct
{
    tTMO_SLL_NODE     Link;
 tRBTree     SrcReporterTree;
    tIgmpTimer        srcTimer;
    tIgmpGroup        *pGrp;
    tIgmpIPvXAddr     u4HostAddress;
    UINT1             u1EntryStatus;
    UINT1             u1GrpSrcSpQcount;  /* GroupAndSource specific Query count */
    UINT1             u1SrsBitFlag;
    UINT1             u1DelSyncFlag;
/* The dynamic entries that are to be synced should be added after the
 * SourceDbNode. And both the halves should be padded accordingly */
    tDbTblNode        SrcDbNode;          /* Data Base node to sync source node
                                                dynamic information */
    UINT4       u4IfIndex;         /* Interface index */
    UINT1             u1DynJoinFlag;
    UINT1             au1Pad1[3];
    tIgmpIPvXAddr     u4GrpAddress;      /* Group Address */
    tIgmpIPvXAddr     u4SrcAddress;  /* Source Address */
    tIgmpIPvXAddr     u4LastReporter; /* Last Reporter address */
    UINT1       u1SrcModeFlag;  /* Flag to indicate the status of node
      ADD_SOURCE/DELETE_SOURCE */
    UINT1             u1FwdState;        /* whether to Forward traffic from 
                      the source */
    UINT1             u1UpdatedToMrp;
    UINT1             u1QryFlag;
    UINT1             u1DynamicFlg;      /* This flag if true indicates that
      the source is dynamically added */ 
    UINT1             u1StaticFlg;       /* This flag if true indicates that 
         this source has been statically added */
    UINT1       u1DynSyncFlag; /* Decides whether this source needs 
                                to be synced. This field shouldnt be processed
                                in standby*/
    UINT1       u1SrcSSMMapped;
}tIgmpSource;

typedef struct
{
    tIgmpIface        *pIfNode;          /* Interface node */ 
    tIgmpGroup        *pGrp;
    UINT4       u4NoOfSources;
    tIgmpIPvXAddr       u4Reporter;
    UINT1       *pu1Sources;
}tIgmpFSMInfo;

typedef INT4 (*tIgmpFSMFnPtr) (tIgmpFSMInfo *);

/* structure to maintain the req info about the registered multicast routing 
 * protocols
 */
typedef struct
{
    UINT1             u1ProtId;          /* multicast routing protocol 
         identifier */
    UINT1             u1Flag;            /* to tell whether already registered 
         or not */
    UINT2             u2AlignmentByte;
    VOID (*pHandleInput) (tCRU_BUF_CHAIN_HEADER *);
    VOID (*pUpdateIFace) (tNetIpv4IfInfo *, UINT4);
    VOID (*pFwdIfList)(VOID);
    VOID (*pLJNoteFuncPtr) (tGrpNoteMsg);
} tIgmpRegister;                 


typedef struct
{
    UINT1           u1ProtId;
    UINT1             u1Flag;            /* to tell whether already registered
                                            or not */
    UINT2             u2AlignmentByte;
    VOID (*pHandleInput) (tCRU_BUF_CHAIN_HEADER *);
    VOID (*pUpdateIFace) (tNetIpv6IfInfo *, UINT4);
    VOID (*pFwdIfList)(VOID);
    VOID (*pLJNoteFuncPtr) (UINT2, tIp6Addr, UINT4); /* For backward 
                                                       compatibilty */
    VOID (*pLJNoteFuncPtrV2) (tGrp6NoteMsg *pGrpInfo);
}tMldRegister; 

typedef struct sIgmpSizingParam{
    UINT4             u4MaxIfaces;
    UINT4             u4MaxGroups;
    UINT4             u4MaxSrcs;
}tIgmpSizingParams;

typedef struct
{
   tRBNodeEmbd        RbNode;
/* The dynamic entries that are to be synced should be added after the
 * SrcReporterDbNode. And both the halves should be padded accordingly */
   tDbTblNode         SrcReporterDbNode;         /* Data Base node to sync source reporter
      node dynamic information */
   UINT4       u4IfIndex;                /* Interface index */
   tIgmpIPvXAddr      GrpAddr;   /* Group Address */
   tIgmpIPvXAddr      SourceAddr;  /* Source Address */
   tIgmpIPvXAddr      u4SrcRepAddr;  /* Reporter Address */
   UINT1              u1RepModeFlag;  /* Flag to indicate the status of node
                                          ADD_REPORTER/DELETE_REPORTER */
   UINT1     u1GrpSrcFlag;
   UINT1              au1Pad1[2];
}tIgmpSrcReporter;


/* This Structure is used to construct source list for 
 * scheduling queries */
typedef struct
{
    tTMO_SLL_NODE     Link;
    tIgmpSource       *pSrc;
    tIgmpIPvXAddr         u4Source;  
    UINT2             u2Reserved;
    UINT1             u1Reserved;
    UINT1             u1RetrNum;
}tSourceNode;

typedef struct
{
    UINT1             u1MsgType;
    UINT1             u1MaxRespCode;
    UINT2             u2CheckSum; 
    UINT4             u4GroupAddr; 
    UINT1             u1ResvSupRV;
    UINT1             u1QQIC;
    UINT2             u2NumSrcs;
}tIgmpV3Query;

typedef struct
{
    UINT1     u1Type;
    UINT1     u1Code;
    UINT2     u2CheckSum; 
    UINT2     u2MaxRespCode;
    UINT2     u2Reserved;
    tIp6Addr  McastAddr;
    UINT1     u1ResvSupRV;
    UINT1     u1QQIC;
    UINT2     u2NumSrcs;
}tMldV2Query;


typedef struct
{
    UINT1             u1RecordType;
    UINT1             u1AuxDataLen;
    UINT2             u2NumSrcs;
    UINT4             u4GroupAddress;
}tIgmpGroupRec;

typedef struct
{
    UINT1        u1RecordType;
    UINT1        u1AuxDataLen;
    UINT2        u2NumSrcs;
    tIp6Addr     GroupAddr;
}tMldGroupRecord;

typedef struct 
{
    UINT1             u1MsgType;
    UINT1             u1Resvd;
    UINT2             u2CheckSum;
    UINT2             u2Resvd;
    UINT2             u2NumGrps;
} tIgmpV3Report;


typedef tIgmpV3Report tMldV2Report;

/*IGMP Redundancy related data structures */
typedef struct _IgmpRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tIgmpRmCtrlMsg;

typedef struct _IgmpRmMsg
{
     tIgmpRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tIgmpRmMsg;

typedef struct _sIgmpRedGblInfo{
    UINT1 u1BulkUpdStatus; /* bulk update status
                            * IGMP_HA_UPD_NOT_STARTED 0
                            * IGMP_HA_UPD_COMPLETED 1
                            * IGMP_HA_UPD_IN_PROGRESS 2,
                            * IGMP_HA_UPD_ABORTED 3 */
    UINT1 u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1 u1NumPeersUp;     /* Indicates number of standby nodes
                               that are up. */
    UINT1 bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
}tIgmpRedGblInfo;

typedef struct _IgmpGroupList{
    tRBNodeEmbd  RbNode;
    UINT4   u4GroupId;
    UINT4   u4GroupIP;
    UINT4   u4PrefixLen;
    UINT4   u4RowStatus;
}tIgmpGrpList;

typedef struct _IgmpHostMcastGrpEntry
{
    tRBNodeEmbd  IgmpMcastRbNode;
    tIPvXAddr McastAddr;/*Multicast address */
    tIgmpTimer        Timer;
    UINT4     u4IfIndex;   /*Interface Id */
    UINT1     u1Version;   /* Igmp Version*/
    UINT1             au1Pad[3];
}tIgmpHostMcastGrpEntry;

#endif /* IGMPTYPDFS_H */
