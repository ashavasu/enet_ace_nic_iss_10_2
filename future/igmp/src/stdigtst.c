/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdigtst.c,v 1.10 2011/10/13 10:05:01 siva Exp $
 *
 * Description:This file contains Low level routines  for stdigmp.mib  
 *
 *******************************************************************/

#include "include.h"
#include "stdigcon.h"
#include "stdigogi.h"
#include "midconst.h"
#include "igmpinc.h"

/* LOW LEVEL Routines for Table : IgmpInterfaceTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IgmpInterfaceQueryInterval
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceQueryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpInterfaceQueryInterval (UINT4 *pu4ErrorCode,
                                     INT4 i4IgmpInterfaceIfIndex,
                                     UINT4 u4TestValIgmpInterfaceQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval (pu4ErrorCode,
                                                         i4IgmpInterfaceIfIndex,
                                                         IPVX_ADDR_FMLY_IPV4,
                                                         u4TestValIgmpInterfaceQueryInterval);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2IgmpInterfaceStatus
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpInterfaceStatus (UINT4 *pu4ErrorCode,
                              INT4 i4IgmpInterfaceIfIndex,
                              INT4 i4TestValIgmpInterfaceStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus (pu4ErrorCode,
                                                  i4IgmpInterfaceIfIndex,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  i4TestValIgmpInterfaceStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2IgmpInterfaceVersion
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpInterfaceVersion (UINT4 *pu4ErrorCode,
                               INT4 i4IgmpInterfaceIfIndex,
                               UINT4 u4TestValIgmpInterfaceVersion)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion (pu4ErrorCode,
                                                   i4IgmpInterfaceIfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   u4TestValIgmpInterfaceVersion);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2IgmpInterfaceQueryMaxResponseTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceQueryMaxResponseTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpInterfaceQueryMaxResponseTime (UINT4 *pu4ErrorCode,
                                            INT4 i4IgmpInterfaceIfIndex,
                                            UINT4
                                            u4TestValIgmpInterfaceQueryMaxResponseTime)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime (pu4ErrorCode,
                                                                i4IgmpInterfaceIfIndex,
                                                                IPVX_ADDR_FMLY_IPV4,
                                                                u4TestValIgmpInterfaceQueryMaxResponseTime);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2IgmpInterfaceProxyIfIndex
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceProxyIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpInterfaceProxyIfIndex (UINT4 *pu4ErrorCode,
                                    INT4 i4IgmpInterfaceIfIndex,
                                    INT4 i4TestValIgmpInterfaceProxyIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceProxyIfIndex (pu4ErrorCode,
                                                        i4IgmpInterfaceIfIndex,
                                                        IPVX_ADDR_FMLY_IPV4,
                                                        i4TestValIgmpInterfaceProxyIfIndex);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2IgmpInterfaceRobustness
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceRobustness
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpInterfaceRobustness (UINT4 *pu4ErrorCode,
                                  INT4 i4IgmpInterfaceIfIndex,
                                  UINT4 u4TestValIgmpInterfaceRobustness)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness (pu4ErrorCode,
                                                      i4IgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      u4TestValIgmpInterfaceRobustness);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2IgmpInterfaceLastMembQueryIntvl
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceLastMembQueryIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpInterfaceLastMembQueryIntvl (UINT4 *pu4ErrorCode,
                                          INT4 i4IgmpInterfaceIfIndex,
                                          UINT4
                                          u4TestValIgmpInterfaceLastMembQueryIntvl)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl (pu4ErrorCode,
                                                              i4IgmpInterfaceIfIndex,
                                                              IPVX_ADDR_FMLY_IPV4,
                                                              u4TestValIgmpInterfaceLastMembQueryIntvl);

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IgmpInterfaceTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IgmpCacheTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IgmpCacheSelf
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                testValIgmpCacheSelf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpCacheSelf (UINT4 *pu4ErrorCode,
                        UINT4 u4IgmpCacheAddress, INT4 i4IgmpCacheIfIndex,
                        INT4 i4TestValIgmpCacheSelf)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpCacheSelf (pu4ErrorCode, IgmpCacheAddrX,
                                            i4IgmpCacheIfIndex,
                                            IPVX_ADDR_FMLY_IPV4,
                                            i4TestValIgmpCacheSelf);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2IgmpCacheStatus
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                testValIgmpCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpCacheStatus (UINT4 *pu4ErrorCode,
                          UINT4 u4IgmpCacheAddress,
                          INT4 i4IgmpCacheIfIndex,
                          INT4 i4TestValIgmpCacheStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpCacheStatus (pu4ErrorCode, IgmpCacheAddrX,
                                              i4IgmpCacheIfIndex,
                                              IPVX_ADDR_FMLY_IPV4,
                                              i4TestValIgmpCacheStatus);

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IgmpCacheTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IgmpSrcListTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IgmpSrcListStatus
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress

                The Object 
                testValIgmpSrcListStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IgmpSrcListStatus (UINT4 *pu4ErrorCode,
                            UINT4 u4IgmpSrcListAddress,
                            INT4 i4IgmpSrcListIfIndex,
                            UINT4 u4IgmpSrcListHostAddress,
                            INT4 i4TestValIgmpSrcListStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddrX;
    tIgmpIPvXAddr       SrcListHostAddrX;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddrX);
    IGMP_IPVX_ADDR_CLEAR (&SrcListHostAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (SrcListAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (SrcListHostAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListHostAddress);

    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpSrcListStatus (pu4ErrorCode, SrcListAddrX,
                                                i4IgmpSrcListIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                SrcListHostAddrX,
                                                i4TestValIgmpSrcListStatus);

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IgmpSrcListTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
