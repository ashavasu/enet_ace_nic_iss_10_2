/* $Id: fsmgmdwr.c,v 1.9 2016/06/24 09:42:20 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmgmdlw.h"
# include  "fsmgmdwr.h"
# include  "fsmgmddb.h"

VOID
RegisterFSMGMD ()
{
    SNMPRegisterMib (&fsmgmdOID, &fsmgmdEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsmgmdOID, (const UINT1 *) "fsmgmd");
}

VOID
UnRegisterFSMGMD ()
{
    SNMPUnRegisterMib (&fsmgmdOID, &fsmgmdEntry);
    SNMPDelSysorEntry (&fsmgmdOID, (const UINT1 *) "fsmgmd");
}

INT4
FsMgmdIgmpGlobalStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdIgmpGlobalStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMgmdIgmpTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdIgmpTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsMgmdIgmpDebugLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{                                                                                                                     UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdIgmpDebugLevel (&(pMultiData->i4_SLongValue)));
}                                                                                                                 

INT4
FsMgmdMldGlobalStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdMldGlobalStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMgmdMldTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdMldTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsMgmdMldDebugLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)                                           {
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdMldDebugLevel (&(pMultiData->i4_SLongValue)));                                            }


INT4
FsMgmdIgmpGlobalStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMgmdIgmpGlobalStatus (pMultiData->i4_SLongValue));
}

INT4
FsMgmdIgmpTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMgmdIgmpTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsMgmdIgmpDebugLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMgmdIgmpDebugLevel (pMultiData->i4_SLongValue));
}

INT4
FsMgmdMldGlobalStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMgmdMldGlobalStatus (pMultiData->i4_SLongValue));
}

INT4
FsMgmdMldTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMgmdMldTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsMgmdMldDebugLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMgmdMldDebugLevel (pMultiData->i4_SLongValue));
}

INT4
FsMgmdIgmpGlobalStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMgmdIgmpGlobalStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMgmdIgmpTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMgmdIgmpTraceLevel
            (pu4Error, pMultiData->i4_SLongValue));
}
INT4                                                                                                             
FsMgmdIgmpDebugLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMgmdIgmpDebugLevel
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMgmdMldGlobalStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMgmdMldGlobalStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMgmdMldTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMgmdMldTraceLevel (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMgmdMldDebugLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMgmdMldDebugLevel (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMgmdIgmpGlobalStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdIgmpGlobalStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMgmdIgmpTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdIgmpTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMgmdIgmpDebugLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdIgmpDebugLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMgmdMldGlobalStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdMldGlobalStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMgmdMldTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdMldTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMgmdMldDebugLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdMldDebugLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMgmdInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMgmdInterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMgmdInterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMgmdInterfaceAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMgmdInterfaceFastLeaveStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceFastLeaveStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMgmdInterfaceChannelTrackStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceChannelTrackStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMgmdInterfaceOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceOperStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMgmdInterfaceIncomingPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceIncomingPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceIncomingJoinsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceIncomingJoins
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceIncomingLeavesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceIncomingLeaves
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceIncomingQueriesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceIncomingQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceOutgoingQueriesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceOutgoingQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceRxGenQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceRxGenQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceRxGrpQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceRxGrpQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceRxGrpAndSrcQueriesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceRxGrpAndSrcQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceRxIgmpv1v2ReportsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceRxIgmpv1v2Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceRxIgmpv3ReportsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceRxIgmpv3Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceRxMldv1ReportsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceRxMldv1Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceRxMldv2ReportsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceRxMldv2Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxGenQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxGenQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxGrpQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxGrpQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceGroupListIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceGroupListId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceCurGrpCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceCurGrpCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxGrpAndSrcQueriesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxGrpAndSrcQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxIgmpv1v2ReportsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxIgmpv1v2Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxIgmpv3ReportsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxIgmpv3Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxMldv1ReportsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxMldv1Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxMldv2ReportsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxMldv2Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceTxLeavesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdInterfaceTxLeaves (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4 
FsMgmdInterfaceCKSumErrorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceCKSumError(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfacePktLenErrorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfacePktLenError(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfacePktsWithLocalIPGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfacePktsWithLocalIP(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceSubnetCheckFailureGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceSubnetCheckFailure(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceQryFromNonQuerierGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceQryFromNonQuerier(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceReportVersionMisMatchGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceReportVersionMisMatch(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceQryVersionMisMatchGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceQryVersionMisMatch(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceUnknownMsgTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceUnknownMsgType(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceInvalidV1ReportGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceInvalidV1Report(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceInvalidV2ReportGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceInvalidV2Report(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceInvalidV3ReportGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceInvalidV3Report(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceRouterAlertCheckFailureGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceRouterAlertCheckFailure(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}

INT4 
FsMgmdInterfaceIncomingSSMPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceIncomingSSMPkts(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceInvalidSSMPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceInvalidSSMPkts(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 
FsMgmdInterfaceJoinPktRateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdInterfaceJoinPktRate(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMgmdInterfaceMalformedPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsMgmdInterfaceMalformedPkts(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                &(pMultiData->u4_ULongValue)));

}
INT4 FsMgmdInterfaceSocketErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsMgmdInterfaceSocketErrors(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                &(pMultiData->u4_ULongValue)));

}
INT4 FsMgmdInterfaceBadScopeErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsMgmdInterfaceTable(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsMgmdInterfaceBadScopeErrors(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                &(pMultiData->u4_ULongValue)));

}

INT4
FsMgmdInterfaceAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMgmdInterfaceAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMgmdInterfaceFastLeaveStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMgmdInterfaceFastLeaveStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMgmdInterfaceChannelTrackStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsMgmdInterfaceChannelTrackStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMgmdInterfaceGroupListIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMgmdInterfaceGroupListId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsMgmdInterfaceLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMgmdInterfaceLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4 
FsMgmdInterfaceJoinPktRateSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMgmdInterfaceJoinPktRate(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}


INT4
FsMgmdInterfaceAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMgmdInterfaceAdminStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsMgmdInterfaceFastLeaveStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMgmdInterfaceFastLeaveStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMgmdInterfaceChannelTrackStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMgmdInterfaceChannelTrackStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsMgmdInterfaceGroupListIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMgmdInterfaceGroupListId (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsMgmdInterfaceLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMgmdInterfaceLimit (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4 
FsMgmdInterfaceJoinPktRateTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMgmdInterfaceJoinPktRate(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}


INT4
FsMgmdInterfaceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdInterfaceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMgmdCacheTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMgmdCacheTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMgmdCacheTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMgmdCacheGroupCompModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMgmdCacheTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMgmdCacheGroupCompMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMgmdGlobalLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdGlobalLimit (&(pMultiData->u4_ULongValue)));
}

INT4
FsMgmdGlobalCurGrpCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMgmdGlobalCurGrpCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsMgmdSSMMapStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsMgmdSSMMapStatus(&(pMultiData->i4_SLongValue)));
}

INT4
FsMgmdGlobalLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMgmdGlobalLimit (pMultiData->u4_ULongValue));
}

INT4
FsMgmdSSMMapStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsMgmdSSMMapStatus(pMultiData->i4_SLongValue));
}

INT4
FsMgmdGlobalLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMgmdGlobalLimit (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMgmdSSMMapStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsMgmdSSMMapStatus(pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMgmdGlobalLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMgmdGlobalLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
FsMgmdSSMMapStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMgmdSSMMapStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 
GetNextIndexFsMgmdSSMMapGroupTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMgmdSSMMapGroupTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMgmdSSMMapGroupTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}

INT4 
FsMgmdSSMMapRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMgmdSSMMapGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMgmdSSMMapRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 
FsMgmdSSMMapRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMgmdSSMMapRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 
FsMgmdSSMMapRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMgmdSSMMapRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 
FsMgmdSSMMapGroupTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMgmdSSMMapGroupTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
