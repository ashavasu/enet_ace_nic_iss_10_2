/******************************************************************************/
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsutltst.c,v 1.21 2017/12/19 10:04:20 siva Exp $
 *
 * Description: : Action routines for set/get objects in           
 *                           fsigmp.mib                           
 *
 *******************************************************************/

# include  "include.h"
# include  "fsigmcon.h"
# include  "fsigmogi.h"
# include  "midconst.h"
# include  "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE;
#endif
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsIgmpGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4TestValFsIgmpGlobalStatus,
                                         INT4 i4FsIgmpAddrType)
{
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus()\r \n");
    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if ((i4TestValFsIgmpGlobalStatus == IGMP_DISABLE) ||
            (i4TestValFsIgmpGlobalStatus == IGMP_ENABLE))
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus()\r \n");
            return SNMP_SUCCESS;
        }

        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Invalid value for IGMP global status.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus()\r \n");
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
        return SNMP_FAILURE;
    }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if ((i4TestValFsIgmpGlobalStatus == MLD_DISABLE) ||
            (i4TestValFsIgmpGlobalStatus == MLD_ENABLE))
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus()\r \n");
            return SNMP_SUCCESS;
        }

        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Invalid value for MLD global status.\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus()\r \n");
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
        return SNMP_FAILURE;
    }
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus()\r \n");
    return SNMP_FAILURE;
}

INT1
IgmpMgmtUtilNmhTestv2FsIgmpSSMMapStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4TestValFsIgmpSSMMapStatus,
                                         INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpSSMMapStatus()\r\n");
    UNUSED_PARAM (i4FsIgmpAddrType);

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if ((i4TestValFsIgmpSSMMapStatus == IGMP_DISABLE) ||
        (i4TestValFsIgmpSSMMapStatus == IGMP_ENABLE))
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpSSMMapStatus()\r\n");
        return SNMP_SUCCESS;
    }

    IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
              IgmpTrcGetModuleName (IGMP_ALL_MODULES),
              "Invalid value for IGMP SSM Mapping global status.\r\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpMgmtUtilNmhTestv2FsIgmpSSMMapStatus()\r\n");
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel
 Input       :  The Indices

                The Object 
                testValFsIgmpTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel (UINT4 *pu4ErrorCode,
                                       INT4 i4TestValFsIgmpTraceLevel,
                                       INT4 i4FsIgmpAddrType)
{
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    UNUSED_PARAM (i4TestValFsIgmpTraceLevel);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel()\r \n");
    if ((i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
        || (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel()\r \n");
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Out of Range Value, Please enter value <1->.\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel()\r \n");
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel
 Input       :  The Indices

                The Object
                testValFsIgmpTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel (UINT4 *pu4ErrorCode,
                                       INT4 i4TestValFsIgmpDebugLevel,
                                       INT4 i4FsIgmpAddrType)
{
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    UNUSED_PARAM (i4TestValFsIgmpDebugLevel);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel()\r \n");
    if ((i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
        || (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel()\r \n");
        return SNMP_SUCCESS;
    }
    else
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Out of Range Value, Please enter value <1->.\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel()\r \n");
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhDepv2FsIgmpGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhDepv2FsIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhDepv2FsIgmpTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhDepv2FsIgmpTraceLevel (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhDepv2FsIgmpDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhDepv2FsIgmpDebugLevel (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceFastLeaveStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4FsIgmpInterfaceIfIndex,
                                                     INT4
                                                     i4FsIgmpInterfaceAddrType,
                                                     INT4
                                                     i4TestValFsIgmpInterfaceFastLeaveStatus)
{
    UINT4               u4Port = 0;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus ()\r \n");
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface %d is configured as upstream interface\r\n",
                   u4Port);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus ()\r \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIgmpInterfaceFastLeaveStatus == IGMP_FAST_LEAVE_ENABLE)
        || (i4TestValFsIgmpInterfaceFastLeaveStatus == IGMP_FAST_LEAVE_DISABLE))
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus ()\r \n");
        return SNMP_SUCCESS;
    }

    IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
              IgmpTrcGetModuleName (IGMP_ALL_MODULES),
              "Out of Range Value, Please Enter <0-disable, 1-enable>\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus ()\r \n");
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceChannelTrackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus (UINT4 *pu4ErrorCode,
                                                        INT4
                                                        i4FsIgmpInterfaceIfIndex,
                                                        INT4
                                                        i4FsIgmpInterfaceAddrType,
                                                        INT4
                                                        i4TestValFsIgmpInterfaceChannelTrackStatus)
{
    UINT4               u4Version = 0;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus()\r \n");
    if (nmhGetIgmpInterfaceVersion (i4FsIgmpInterfaceIfIndex, &u4Version) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid version of Igmp on interface %d \r\n",
                   i4FsIgmpInterfaceIfIndex);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus()\r \n");
        return SNMP_FAILURE;
    }
    if (u4Version == IGMP_ZERO)
    {
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface %d is configured as upstream interface\r\n",
                   i4FsIgmpInterfaceIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus()\r \n");
        return SNMP_FAILURE;
    }
    if ((u4Version != IGMP_VERSION_THREE) &&
        (i4TestValFsIgmpInterfaceChannelTrackStatus ==
         IGMP_CHANNEL_TRACK_ENABLE))
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Explicit tracking can be enabled for IGMPv3 interface only\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_TRACK_FOR_V3);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus()\r \n");
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIgmpInterfaceChannelTrackStatus !=
         IGMP_CHANNEL_TRACK_ENABLE)
        && (i4TestValFsIgmpInterfaceChannelTrackStatus !=
            IGMP_CHANNEL_TRACK_DISABLE))
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Out of Range Value, Please Enter <0-disable, 1-enable>\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus()\r \n");
        return SNMP_FAILURE;
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus()\r \n");
    UNUSED_PARAM (i4FsIgmpInterfaceAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpJoinPktRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpJoinPktRate (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4FsIgmpInterfaceAddrType,
                                        INT4 i4TestValFsIgmpJoinPktRate)
{

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    if (gIgmpConfig.u1IgmpStatus != IGMP_ENABLE)
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Igmp not enabled globally\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Igmp not enabled globally"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_GLOBAL_STATUS_DISABLED);
        return SNMP_FAILURE;

    }

    if (((i4TestValFsIgmpJoinPktRate >= IGMP_INTERFACE_MIN_JOIN_RATE)
         && (i4TestValFsIgmpJoinPktRate <= IGMP_INTERFACE_MAX_JOIN_RATE))
        || (i4TestValFsIgmpJoinPktRate == IGMP_ZERO))
    {
        UNUSED_PARAM (i4FsIgmpInterfaceAddrType);
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceJoinPktRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate (UINT4 *pu4ErrorCode,
                                                 INT4
                                                 i4FsIgmpInterfaceIfIndex,
                                                 INT4
                                                 i4FsIgmpInterfaceAddrType,
                                                 INT4
                                                 i4TestValFsIgmpInterfaceJoinPktRate)
{
    UINT4               u4Version = 0;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate()\r \n");
    if (nmhGetIgmpInterfaceVersion (i4FsIgmpInterfaceIfIndex, &u4Version) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate()\r \n");
        return SNMP_FAILURE;
    }
    if (u4Version == IGMP_ZERO)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface index %d is configured as upstream interface\r\n",
                   i4FsIgmpInterfaceIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate()\r \n");
        return SNMP_FAILURE;
    }

    if (((i4TestValFsIgmpInterfaceJoinPktRate >= IGMP_INTERFACE_MIN_JOIN_RATE)
         && (i4TestValFsIgmpInterfaceJoinPktRate <=
             IGMP_INTERFACE_MAX_JOIN_RATE))
        || (i4TestValFsIgmpInterfaceJoinPktRate == IGMP_ZERO))
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate()\r \n");
        UNUSED_PARAM (i4FsIgmpInterfaceAddrType);
        return SNMP_SUCCESS;
    }
    else
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Out of Range Value, Please Enter value between 100 and 1000\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate()\r \n");
        return SNMP_FAILURE;
    }

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhDepv2FsIgmpInterfaceTable
 Input       :  The Indices
                FsIgmpInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhDepv2FsIgmpInterfaceTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsIgmpInterfaceIfIndex,
                                                 INT4 i4FsIgmpInterfaceAddrType,
                                                 INT4
                                                 i4TestValFsIgmpInterfaceAdminStatus)
{
    INT1                i1Status = IGMP_ZERO;
    UINT4               u4Port = 0;
    tIgmpIface         *pIfaceNode = NULL;
    UNUSED_PARAM (i4FsIgmpInterfaceIfIndex);
    UNUSED_PARAM (i4FsIgmpInterfaceAddrType);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus()\r \n");
    i1Status = SNMP_FAILURE;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (i4TestValFsIgmpInterfaceAdminStatus == IGMP_IFACE_ADMIN_DOWN)
    {
        if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                    i4FsIgmpInterfaceAddrType,
                                    &u4Port) == IGMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface index  %d is configured as upstream interface\r\n",
                       i4FsIgmpInterfaceIfIndex);
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus()\r \n");
            return SNMP_FAILURE;
        }

        pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);
        if (pIfaceNode == NULL)
        {
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface entry not found for port %d\r \n", u4Port);
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus()\r \n");
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsIgmpInterfaceAdminStatus == IGMP_IFACE_ADMIN_UP) ||
        (i4TestValFsIgmpInterfaceAdminStatus == IGMP_IFACE_ADMIN_DOWN))
    {
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Out of Range Value, Please Enter <0-disable, 1-enable>\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus()\r \n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId 
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceGroupListId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsIgmpInterfaceIfIndex,
                                                 INT4 i4FsIgmpInterfaceAddrType,
                                                 UINT4
                                                 u4TestValFsIgmpInterfaceGroupListId)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId()\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId"));
    /* Get the i/face node */
    UNUSED_PARAM (u4TestValFsIgmpInterfaceGroupListId);
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "IGMP Port for interface index %d not found\r\n",
                   i4FsIgmpInterfaceIfIndex);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId()\r \n");
        return SNMP_FAILURE;
    }
    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);
    if ((pIfaceNode == NULL))
    {
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Ifacenode not exist with %d", i4FsIgmpInterfaceIfIndex);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Interfacenode not exist with ifindex %d",
                      i4FsIgmpInterfaceIfIndex));
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId()\r \n");
        return SNMP_FAILURE;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId"));
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpInterfaceLimit
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit (UINT4 *pu4ErrorCode,
                                           INT4 i4FsIgmpInterfaceIfIndex,
                                           INT4 i4FsIgmpInterfaceAddrType,
                                           UINT4 u4TestValFsIgmpInterfaceLimit)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Ifacenode not exist with %d", i4FsIgmpInterfaceIfIndex);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit()\r \n");
        return SNMP_FAILURE;
    }
    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);
    if ((pIfaceNode == NULL))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Ifacenode not exist with %d", i4FsIgmpInterfaceIfIndex);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Interfacenode not exist with ifindex %d",
                      i4FsIgmpInterfaceIfIndex));
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit()\r \n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsIgmpInterfaceLimit != IGMP_ZERO) &&
        (pIfaceNode->u4GrpLimit != IGMP_ZERO) &&
        (u4TestValFsIgmpInterfaceLimit < pIfaceNode->u4GrpLimit))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                   "Limit cannot be less than already configured limit %u\n",
                   pIfaceNode->u4GrpLimit);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_GRP_LIMIT_LESS_THAN_PREV_LIMIT);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit()\r \n");
        return SNMP_FAILURE;
    }
    if (u4TestValFsIgmpInterfaceLimit > MAX_IGMP_MCAST_GRPS)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                   "Limit should be less than %d\n", MAX_IGMP_MCAST_GRPS);
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Limit should be less than %d",
                      i4FsIgmpInterfaceIfIndex));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_LIMIT_NOT_IN_RANGE);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit()\r \n");
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus 
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen

                The Object 
                testValFsIgmpGrpListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsIgmpGrpListId,
                                             UINT4 u4FsIgmpGrpIP,
                                             UINT4 u4FsIgmpGrpPrefixLen,
                                             INT4 i4FsIgmpInterfaceAddrType,
                                             INT4
                                             i4TestValFsIgmpGrpListRowStatus)
{
    tIgmpGrpList        GrpNode;
    tIgmpGrpList       *pGrpNode = NULL;
    tRBElem            *pRBElem = NULL;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4EndAdr;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus()\r \n");
    UNUSED_PARAM (i4FsIgmpInterfaceAddrType);
    if (u4FsIgmpGrpListId == IGMP_ZERO)
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Group List Id should not be zero \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Group List Id should not be zero "));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_INVALID_GRP_LIST_ID);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus()\r \n");
        return SNMP_FAILURE;
    }
    if ((u4FsIgmpGrpIP <= IGMP_START_OF_MCAST_GRP_ADDR) ||
        (u4FsIgmpGrpIP >= IGMP_END_OF_MCAST_GRP_ADDR))
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Group List IP is not in multicast range \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      " Group List IP is not in multicast range"));
        CLI_SET_ERR (CLI_IGMP_INVALID_GRP_LIST_IP_ADDRESS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus()\r \n");
        return SNMP_FAILURE;
    }
    if (u4FsIgmpGrpIP != (u4FsIgmpGrpIP & u4FsIgmpGrpPrefixLen))
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Inconistent maks is not in multicast range \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Inconistent maks is not in multicast range"));
        CLI_SET_ERR (CLI_IGMP_INVALID_GRP_LIST_IP_MASK);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus()\r \n");
        return SNMP_FAILURE;
    }
    GrpNode.u4GroupId = u4FsIgmpGrpListId;
    GrpNode.u4GroupIP = u4FsIgmpGrpIP;
    GrpNode.u4PrefixLen = u4FsIgmpGrpPrefixLen;
    pRBElem = RBTreeGet (gIgmpGrpList, (tRBElem *) & GrpNode);

    switch (i4TestValFsIgmpGrpListRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (pRBElem != NULL)
            {
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                          IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                          "Entry exists\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                              " Entry exists"));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            GrpNode.u4GroupIP = 0;
            GrpNode.u4PrefixLen = 0;
            pGrpNode =
                RBTreeGetNext (gIgmpGrpList, (tRBElem *) & GrpNode, NULL);
            while (pGrpNode != NULL)
            {
                if ((pGrpNode->u4GroupId != u4FsIgmpGrpListId))
                    break;
                u4EndAdr = (pGrpNode->u4GroupIP) | (~(pGrpNode->u4PrefixLen));
                if ((u4FsIgmpGrpIP >= pGrpNode->u4GroupIP) &&
                    (u4FsIgmpGrpIP <= u4EndAdr))
                {
                    IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                              IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                              "Summarised GroupList Record already exists\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                                  "Summarised GroupList Record already exists"));
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_IGMP_GRP_LIST_SUMMARY_RECORD_EXISTS);
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
                pGrpNode =
                    RBTreeGetNext (gIgmpGrpList, (tRBElem *) pGrpNode, NULL);
            }
            break;
        case NOT_IN_SERVICE:
        case ACTIVE:
        case DESTROY:
            if (pRBElem == NULL)
            {
                IGMP_TRC_ARG3 (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                               IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                               "No entry exists with %d, %d,%d \r\n",
                               u4FsIgmpGrpListId, u4FsIgmpGrpIP,
                               u4FsIgmpGrpPrefixLen);
                CLI_SET_ERR (CLI_IGMP_NO_GRP_LIST_RECORD_EXISTS);
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                              "No entry exists with %d, %d, %d",
                              u4FsIgmpGrpListId, u4FsIgmpGrpIP,
                              u4FsIgmpGrpPrefixLen));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetVal = SNMP_FAILURE;
            break;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus()\r \n");
    return i1RetVal;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit
 Input       :  The Indices

                The Object 
                testValFsIgmpGlobalLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit (UINT4 *pu4ErrorCode,
                                        UINT4 u4TestValFsIgmpGlobalLimit,
                                        INT4 i4AddrType)
{
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit()\r \n");
    UNUSED_PARAM (i4AddrType);
    if ((u4TestValFsIgmpGlobalLimit != IGMP_ZERO) &&
        (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO) &&
        (u4TestValFsIgmpGlobalLimit < gIgmpConfig.u4GlobalGrpLimit))
    {
        IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                       IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                       "Limit cannot be less than already configured limit %u\n",
                       gIgmpConfig.u4GlobalGrpLimit);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_GRP_LIMIT_LESS_THAN_PREV_LIMIT);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit()\r \n");
        return SNMP_FAILURE;
    }
    if (u4TestValFsIgmpGlobalLimit > MAX_IGMP_MCAST_GRPS)
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Limit should be less than MAX_IGMP_MCAST_GRPS\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      " Limit should be less than MAX_IGMP_MCAST_GRPS"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_LIMIT_NOT_IN_RANGE);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit()\r \n");
        return SNMP_FAILURE;
    }
    if (gIgmpConfig.u1IgmpStatus != IGMP_ENABLE)
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Igmp not enabled globally\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Igmp not enabled globally"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_GLOBAL_STATUS_DISABLED);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit()\r \n");
        return SNMP_FAILURE;
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress

                The Object
                testValFsIgmpSSMMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsIgmpSSMMapStartGrpAddress,
                                            UINT4 u4FsIgmpSSMMapEndGrpAddress,
                                            UINT4 u4FsIgmpSSMMapSourceAddress,
                                            INT4 i4TestValFsIgmpSSMMapRowStatus)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tIgmpIPvXAddr       u4StartGrp;
    tIgmpIPvXAddr       u4EndGrp;
    tIgmpIPvXAddr       u4Source;
    UINT4               u4Index = IGMP_ZERO;
    INT4                i4Status = IGMP_FALSE;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT1               u1AddFlag = IGMP_FALSE;
    UINT1               u1ExistFlag = IGMP_FALSE;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");

    if ((u4FsIgmpSSMMapStartGrpAddress >= IGMP_MCAST_START_RESD_GRP_ADDR) &&
        (u4FsIgmpSSMMapStartGrpAddress <= IGMP_MCAST_END_RESD_GRP_ADDR))
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Invalid Multicast Address Range \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Invalid Multicast Address Range "));
        CLI_SET_ERR (CLI_IGMP_INVALID_ADDRESS_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");
        return SNMP_FAILURE;
    }

    if ((u4FsIgmpSSMMapEndGrpAddress >= IGMP_MCAST_START_RESD_GRP_ADDR) &&
        (u4FsIgmpSSMMapEndGrpAddress <= IGMP_MCAST_END_RESD_GRP_ADDR))
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Invalid Multicast Address Range \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Invalid Multicast Address Range "));
        CLI_SET_ERR (CLI_IGMP_INVALID_ADDRESS_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");
        return SNMP_FAILURE;
    }
    if ((u4FsIgmpSSMMapStartGrpAddress < IGMP_START_OF_MCAST_GRP_ADDR) ||
        (u4FsIgmpSSMMapStartGrpAddress > IGMP_END_OF_MCAST_GRP_ADDR))
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Invalid IGMP SSM Mapping Start Group address \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Invalid IGMP SSM Mapping Start Group address "));
        CLI_SET_ERR (CLI_IGMP_INVALID_GRP_ADDRESS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");
        return SNMP_FAILURE;
    }
    if ((u4FsIgmpSSMMapEndGrpAddress < IGMP_START_OF_MCAST_GRP_ADDR) ||
        (u4FsIgmpSSMMapEndGrpAddress > IGMP_END_OF_MCAST_GRP_ADDR))
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Invalid IGMP SSM Mapping End Group address \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Invalid IGMP SSM Mapping End Group address "));
        CLI_SET_ERR (CLI_IGMP_INVALID_GRP_ADDRESS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");
        return SNMP_FAILURE;
    }
    if (IgmpValidateV4SourceAddress (u4FsIgmpSSMMapSourceAddress, IGMP_TRUE) ==
        IGMP_FAILURE)
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Invalid IGMP SSM Mapping Source address \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Invalid IGMP SSM Mapping Source address "));
        CLI_SET_ERR (CLI_IGMP_INVALID_SOURCE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");
        return SNMP_FAILURE;
    }
    if (u4FsIgmpSSMMapStartGrpAddress > u4FsIgmpSSMMapEndGrpAddress)
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                  IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                  "Starting multicast group range is greater than ending multicast group range \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "Starting multicast group range is greater than ending multicast group range "));
        CLI_SET_ERR (CLI_IGMP_SSMMAP_INCONSIS_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");
        return SNMP_FAILURE;
    }

    IGMP_IPVX_ADDR_CLEAR (&u4StartGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4EndGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4Source);
    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);

    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4Index);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4StartGrp, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpSSMMapStartGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4EndGrp, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpSSMMapEndGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4Source, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpSSMMapSourceAddress);

    switch (i4TestValFsIgmpSSMMapRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        case ACTIVE:
            /* Check for superset IGMP SSM Mapping */
            if ((IgmpUtilChkIfSuperSetOfSSMMappedGroup (u4StartGrp, u4EndGrp))
                == IGMP_TRUE)
            {
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                          IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                          "IGMP SSM Mapping for the given range of group address is a superset of "
                          "already configured range \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                              "IGMP SSM Mapping for the given range of group address is a superset of "
                              "already configured range "));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_IGMP_SSMMAP_SUPERSET);
                return SNMP_FAILURE;
            }

            /* Check for IGMP SSM Mapping using Start Group Address */
            pSSMMappedGrp = IgmpUtilChkIfSSMMapExists (u4StartGrp, &i4Status);
            if ((IGMP_TRUE == i4Status) && (NULL != pSSMMappedGrp) &&
                ((IGMP_IPVX_ADDR_COMPARE
                  (pSSMMappedGrp->StartGrpAddr, u4StartGrp) == IGMP_ZERO)
                 &&
                 (IGMP_IPVX_ADDR_COMPARE (pSSMMappedGrp->EndGrpAddr, u4EndGrp)
                  == IGMP_ZERO)))
            {
                /* If IGMP SSM Mapping entry found, then check for free source entry in it */
                for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC;
                     u4Index++)
                {
                    /* Check for an exact source match */
                    if (IGMP_IPVX_ADDR_COMPARE
                        (u4Source,
                         pSSMMappedGrp->SrcAddr[u4Index]) == IGMP_ZERO)
                    {
                        u1ExistFlag = IGMP_TRUE;
                        break;
                    }
                    /* Free source entry found */
                    else if (IGMP_IPVX_ADDR_COMPARE
                             (gIPvXZero,
                              pSSMMappedGrp->SrcAddr[u4Index]) == IGMP_ZERO)
                    {
                        u1AddFlag = IGMP_TRUE;
                        break;
                    }
                }
                if (IGMP_TRUE == u1ExistFlag)
                {
                    IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                              IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                              "IGMP SSM Mapping for the given range of group address to the source "
                              "address already exists \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                                  "IGMP SSM Mapping for the given range of group address to the source "
                                  "address already exists "));
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_IGMP_SSMMAP_EXISTS);
                    i1RetVal = SNMP_FAILURE;
                }
                else if (IGMP_FALSE == u1AddFlag)
                {
                    IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                              IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                              "IGMP SSM Mapping Source-List is full for the given range of group address \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                                  "IGMP SSM Mapping Source-List is full for the given range of group address "));
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    CLI_SET_ERR (CLI_IGMP_SSMMAP_SRC_LIST_FULL);
                    i1RetVal = SNMP_FAILURE;
                }
            }
            /* Check for IGMP SSM Mapping using End Group Address */
            else if (NULL == pSSMMappedGrp)
            {
                pSSMMappedGrp = IgmpUtilChkIfSSMMapExists (u4EndGrp, &i4Status);
                if ((IGMP_TRUE == i4Status) && (NULL != pSSMMappedGrp))
                {
                    IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                              IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                              "IGMP SSM Mapping for the given range of group address overlaps with "
                              "already configured range \r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                                  "IGMP SSM Mapping for the given range of group address overlaps with "
                                  "already configured range "));
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_IGMP_SSMMAP_OVERLAPS);
                    i1RetVal = SNMP_FAILURE;
                }
            }
            else
            {
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                          IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                          "IGMP SSM Mapping for the given range of group address overlaps with "
                          "already configured range \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                              "IGMP SSM Mapping for the given range of group address overlaps with "
                              "already configured range "));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_IGMP_SSMMAP_OVERLAPS);
                i1RetVal = SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case DESTROY:
            pSSMMappedGrp =
                IgmpGrpLookUpInSSMMapTable (u4StartGrp, u4EndGrp, u4Source);
            if (NULL == pSSMMappedGrp)
            {
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES,
                          IgmpTrcGetModuleName (IGMP_ALL_MODULES),
                          "IGMP SSM Mapping for the given range of group address to the source "
                          "address does not exist \r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                              "IGMP SSM Mapping for the given range of group address to the source "
                              "address does not exist "));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_IGMP_SSMMAP_NOT_EXISTS);
                i1RetVal = SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetVal = SNMP_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus()\r \n");
    return i1RetVal;
}
