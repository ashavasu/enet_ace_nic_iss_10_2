/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: mldnpapi.c,v 1.3 2015/03/25 14:25:56 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of IP module.
 ******************************************************************************/

#ifndef _MLDNPAPI_C_
#define _MLDNPAPI_C_
#include "igmpinc.h"
#include "nputil.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_NP_MODULE ;
#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : MldFsMldHwEnableMld                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMldHwEnableMld
 *                                                                          
 *    Input(s)            : Arguments of FsMldHwEnableMld
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldFsMldHwEnableMld ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLD_MOD,    /* Module ID */
                         FS_MLD_HW_ENABLE_MLD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldFsMldHwEnableMld()\r \n");
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                      "Error in enabling MLD in NP \n");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsMldHwEnableMld()\r \n");
	return (FNP_FAILURE);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsMldHwEnableMld()\r \n");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldFsMldHwDisableMld                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMldHwDisableMld
 *                                                                          
 *    Input(s)            : Arguments of FsMldHwDisableMld
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldFsMldHwDisableMld ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLD_MOD,    /* Module ID */
                         FS_MLD_HW_DISABLE_MLD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldFsMldHwDisableMld()\r \n");
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                  "Error in disabling MLD in NP \r\n");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsMldHwDisableMld()\r \n");
	return (FNP_FAILURE);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsMldHwDisableMld()\r \n");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldFsMldHwDestroyMLDFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMldHwDestroyMLDFilter
 *                                                                          
 *    Input(s)            : Arguments of FsMldHwDestroyMLDFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldFsMldHwDestroyMLDFilter ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLD_MOD,    /* Module ID */
                         FS_MLD_HW_DESTROY_M_L_D_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldFsMldHwDestroyMLDFilter()\r \n");
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                      "Error in destroying  MLD filter in NP\r\n");
        return (FNP_FAILURE);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsMldHwDestroyMLDFilter()\r \n");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldFsMldHwDestroyMLDFP                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMldHwDestroyMLDFP
 *                                                                          
 *    Input(s)            : Arguments of FsMldHwDestroyMLDFP
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldFsMldHwDestroyMLDFP ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLD_MOD,    /* Module ID */
                         FS_MLD_HW_DESTROY_M_L_D_F_P,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldFsMldHwDestroyMLDFP()\r \n");
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                     "Error in destroying MLD FP  in NP\n");
        return (FNP_FAILURE);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsMldHwDestroyMLDFP()\r \n");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldFsNpIpv6SetMldIfaceJoinRate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv6SetMldIfaceJoinRate
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv6SetMldIfaceJoinRate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldFsNpIpv6SetMldIfaceJoinRate (INT4 i4IfIndex, INT4 i4RateLimit)
{
    tFsHwNp             FsHwNp;
    tMldNpModInfo      *pMldNpModInfo = NULL;
    tMldNpWrFsNpIpv6SetMldIfaceJoinRate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLD_MOD,    /* Module ID */
                         FS_NP_IPV6_SET_MLD_IFACE_JOIN_RATE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldFsNpIpv6SetMldIfaceJoinRate()\r \n");
    pMldNpModInfo = &(FsHwNp.MldNpModInfo);
    pEntry = &pMldNpModInfo->MldNpFsNpIpv6SetMldIfaceJoinRate;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4RateLimit = i4RateLimit;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MLD_DBG1 (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                      "Error in setting MLD interaface join rate in NP for index %d\n",
		       pEntry->i4IfIndex);
        return (FNP_FAILURE);
    }
    MLD_DBG2 (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                      "MLD interaface join rate in NP for index %d is %d\n",
                      pEntry->i4IfIndex, pEntry->i4RateLimit); 
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsNpIpv6SetMldIfaceJoinRate()\r \n");
    return (FNP_SUCCESS);
}
#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : MldFsMldMbsmHwEnableMld                                         
 *                                                                          

 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMldMbsmHwEnableMld
 *                                                                          
 *    Input(s)            : Arguments of FsMldMbsmHwEnableMld
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldFsMldMbsmHwEnableMld (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMldNpModInfo      *pMldNpModInfo = NULL;
    tMldNpWrFsMldMbsmHwEnableMld *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLD_MOD,    /* Module ID */
                         FS_MLD_MBSM_HW_ENABLE_MLD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldFsMldMbsmHwEnableMld()\r \n");
    pMldNpModInfo = &(FsHwNp.MldNpModInfo);
    pEntry = &pMldNpModInfo->MldNpFsMldMbsmHwEnableMld;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MLD_DBG1 (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                      "Error in enabling MLD MBSM in NP for slot info %d\n",
		      pEntry->pSlotInfo);
        return (FNP_FAILURE);
    }
    MLD_DBG1 (MLD_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                      "MLD MBSM enabled in NP for slot info %d\n",
                      pEntry->pSlotInfo);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  MldFsMldMbsmHwEnableMld()\r \n");
    return (FNP_SUCCESS);
}
#endif
#endif

