/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdmgmlw.c,v 1.5 2017/02/06 10:45:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "igmpinc.h"
/* LOW LEVEL Routines for Table : MgmdHostInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdHostInterfaceTable
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdHostInterfaceTable (INT4 i4MgmdHostInterfaceIfIndex,
                                                INT4
                                                i4MgmdHostInterfaceQuerierType)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdHostInterfaceTable
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdHostInterfaceTable (INT4 *pi4MgmdHostInterfaceIfIndex,
                                        INT4 *pi4MgmdHostInterfaceQuerierType)
{
    UNUSED_PARAM (pi4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (pi4MgmdHostInterfaceQuerierType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdHostInterfaceTable
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                nextMgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType
                nextMgmdHostInterfaceQuerierType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdHostInterfaceTable (INT4 i4MgmdHostInterfaceIfIndex,
                                       INT4 *pi4NextMgmdHostInterfaceIfIndex,
                                       INT4 i4MgmdHostInterfaceQuerierType,
                                       INT4
                                       *pi4NextMgmdHostInterfaceQuerierType)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (pi4NextMgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (pi4NextMgmdHostInterfaceQuerierType);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMgmdHostInterfaceQuerier
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                retValMgmdHostInterfaceQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostInterfaceQuerier (INT4 i4MgmdHostInterfaceIfIndex,
                                INT4 i4MgmdHostInterfaceQuerierType,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValMgmdHostInterfaceQuerier)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (pRetValMgmdHostInterfaceQuerier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMgmdHostInterfaceStatus
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                retValMgmdHostInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostInterfaceStatus (INT4 i4MgmdHostInterfaceIfIndex,
                               INT4 i4MgmdHostInterfaceQuerierType,
                               INT4 *pi4RetValMgmdHostInterfaceStatus)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (pi4RetValMgmdHostInterfaceStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMgmdHostInterfaceVersion
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                retValMgmdHostInterfaceVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostInterfaceVersion (INT4 i4MgmdHostInterfaceIfIndex,
                                INT4 i4MgmdHostInterfaceQuerierType,
                                UINT4 *pu4RetValMgmdHostInterfaceVersion)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (pu4RetValMgmdHostInterfaceVersion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMgmdHostInterfaceVersion1QuerierTimer
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                retValMgmdHostInterfaceVersion1QuerierTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostInterfaceVersion1QuerierTimer (INT4 i4MgmdHostInterfaceIfIndex,
                                             INT4
                                             i4MgmdHostInterfaceQuerierType,
                                             UINT4
                                             *pu4RetValMgmdHostInterfaceVersion1QuerierTimer)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (pu4RetValMgmdHostInterfaceVersion1QuerierTimer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMgmdHostInterfaceVersion2QuerierTimer
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                retValMgmdHostInterfaceVersion2QuerierTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostInterfaceVersion2QuerierTimer (INT4 i4MgmdHostInterfaceIfIndex,
                                             INT4
                                             i4MgmdHostInterfaceQuerierType,
                                             UINT4
                                             *pu4RetValMgmdHostInterfaceVersion2QuerierTimer)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (pu4RetValMgmdHostInterfaceVersion2QuerierTimer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMgmdHostInterfaceVersion3Robustness
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                retValMgmdHostInterfaceVersion3Robustness
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostInterfaceVersion3Robustness (INT4 i4MgmdHostInterfaceIfIndex,
                                           INT4 i4MgmdHostInterfaceQuerierType,
                                           UINT4
                                           *pu4RetValMgmdHostInterfaceVersion3Robustness)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (pu4RetValMgmdHostInterfaceVersion3Robustness);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMgmdHostInterfaceStatus
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                setValMgmdHostInterfaceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdHostInterfaceStatus (INT4 i4MgmdHostInterfaceIfIndex,
                               INT4 i4MgmdHostInterfaceQuerierType,
                               INT4 i4SetValMgmdHostInterfaceStatus)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (i4SetValMgmdHostInterfaceStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMgmdHostInterfaceVersion
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                setValMgmdHostInterfaceVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdHostInterfaceVersion (INT4 i4MgmdHostInterfaceIfIndex,
                                INT4 i4MgmdHostInterfaceQuerierType,
                                UINT4 u4SetValMgmdHostInterfaceVersion)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (u4SetValMgmdHostInterfaceVersion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMgmdHostInterfaceVersion3Robustness
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                setValMgmdHostInterfaceVersion3Robustness
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdHostInterfaceVersion3Robustness (INT4 i4MgmdHostInterfaceIfIndex,
                                           INT4 i4MgmdHostInterfaceQuerierType,
                                           UINT4
                                           u4SetValMgmdHostInterfaceVersion3Robustness)
{
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (u4SetValMgmdHostInterfaceVersion3Robustness);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MgmdHostInterfaceStatus
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                testValMgmdHostInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdHostInterfaceStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4MgmdHostInterfaceIfIndex,
                                  INT4 i4MgmdHostInterfaceQuerierType,
                                  INT4 i4TestValMgmdHostInterfaceStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (i4TestValMgmdHostInterfaceStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdHostInterfaceVersion
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                testValMgmdHostInterfaceVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdHostInterfaceVersion (UINT4 *pu4ErrorCode,
                                   INT4 i4MgmdHostInterfaceIfIndex,
                                   INT4 i4MgmdHostInterfaceQuerierType,
                                   UINT4 u4TestValMgmdHostInterfaceVersion)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (u4TestValMgmdHostInterfaceVersion);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdHostInterfaceVersion3Robustness
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType

                The Object 
                testValMgmdHostInterfaceVersion3Robustness
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdHostInterfaceVersion3Robustness (UINT4 *pu4ErrorCode,
                                              INT4 i4MgmdHostInterfaceIfIndex,
                                              INT4
                                              i4MgmdHostInterfaceQuerierType,
                                              UINT4
                                              u4TestValMgmdHostInterfaceVersion3Robustness)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4MgmdHostInterfaceIfIndex);
    UNUSED_PARAM (i4MgmdHostInterfaceQuerierType);
    UNUSED_PARAM (u4TestValMgmdHostInterfaceVersion3Robustness);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MgmdHostInterfaceTable
 Input       :  The Indices
                MgmdHostInterfaceIfIndex
                MgmdHostInterfaceQuerierType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MgmdHostInterfaceTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MgmdRouterInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdRouterInterfaceTable
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdRouterInterfaceTable (INT4
                                                  i4MgmdRouterInterfaceIfIndex,
                                                  INT4
                                                  i4MgmdRouterInterfaceQuerierType)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdRouterInterfaceTable
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdRouterInterfaceTable (INT4 *pi4MgmdRouterInterfaceIfIndex,
                                          INT4
                                          *pi4MgmdRouterInterfaceQuerierType)
{
    INT1                i1Status = SNMP_FAILURE;
    *pi4MgmdRouterInterfaceQuerierType = IGMP_ZERO;
    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable
        (pi4MgmdRouterInterfaceIfIndex, pi4MgmdRouterInterfaceQuerierType);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdRouterInterfaceTable
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                nextMgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType
                nextMgmdRouterInterfaceQuerierType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdRouterInterfaceTable (INT4 i4MgmdRouterInterfaceIfIndex,
                                         INT4
                                         *pi4NextMgmdRouterInterfaceIfIndex,
                                         INT4 i4MgmdRouterInterfaceQuerierType,
                                         INT4
                                         *pi4NextMgmdRouterInterfaceQuerierType)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetNextIndexIgmpInterfaceTable
        (i4MgmdRouterInterfaceIfIndex, pi4NextMgmdRouterInterfaceIfIndex,
         i4MgmdRouterInterfaceQuerierType,
         pi4NextMgmdRouterInterfaceQuerierType);
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceQuerier
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceQuerier (INT4 i4MgmdRouterInterfaceIfIndex,
                                  INT4 i4MgmdRouterInterfaceQuerierType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValMgmdRouterInterfaceQuerier)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       QuerierAddr;

    IGMP_IPVX_ADDR_CLEAR (&QuerierAddr);
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerier (i4MgmdRouterInterfaceIfIndex,
                                                i4MgmdRouterInterfaceQuerierType,
                                                &QuerierAddr);
    if (i1Status == SNMP_SUCCESS)
    {
        IGMP_COPY_IPVX_TO_OCTET (pRetValMgmdRouterInterfaceQuerier,
                                 QuerierAddr, i4MgmdRouterInterfaceQuerierType);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceQueryInterval
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceQueryInterval (INT4 i4MgmdRouterInterfaceIfIndex,
                                        INT4 i4MgmdRouterInterfaceQuerierType,
                                        UINT4
                                        *pu4RetValMgmdRouterInterfaceQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQueryInterval
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceQueryInterval);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceStatus
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceStatus (INT4 i4MgmdRouterInterfaceIfIndex,
                                 INT4 i4MgmdRouterInterfaceQuerierType,
                                 INT4 *pi4RetValMgmdRouterInterfaceStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceStatus (i4MgmdRouterInterfaceIfIndex,
                                               i4MgmdRouterInterfaceQuerierType,
                                               pi4RetValMgmdRouterInterfaceStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceVersion
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceVersion (INT4 i4MgmdRouterInterfaceIfIndex,
                                  INT4 i4MgmdRouterInterfaceQuerierType,
                                  UINT4 *pu4RetValMgmdRouterInterfaceVersion)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceVersion (i4MgmdRouterInterfaceIfIndex,
                                                i4MgmdRouterInterfaceQuerierType,
                                                pu4RetValMgmdRouterInterfaceVersion);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceQueryMaxResponseTime
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceQueryMaxResponseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceQueryMaxResponseTime (INT4
                                               i4MgmdRouterInterfaceIfIndex,
                                               INT4
                                               i4MgmdRouterInterfaceQuerierType,
                                               UINT4
                                               *pu4RetValMgmdRouterInterfaceQueryMaxResponseTime)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQueryMaxResponseTime
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceQueryMaxResponseTime);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceQuerierUpTime
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceQuerierUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceQuerierUpTime (INT4 i4MgmdRouterInterfaceIfIndex,
                                        INT4 i4MgmdRouterInterfaceQuerierType,
                                        UINT4
                                        *pu4RetValMgmdRouterInterfaceQuerierUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerierUpTime
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceQuerierUpTime);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceQuerierExpiryTime
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceQuerierExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceQuerierExpiryTime (INT4 i4MgmdRouterInterfaceIfIndex,
                                            INT4
                                            i4MgmdRouterInterfaceQuerierType,
                                            UINT4
                                            *pu4RetValMgmdRouterInterfaceQuerierExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerierExpiryTime
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceQuerierExpiryTime);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceWrongVersionQueries
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceWrongVersionQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceWrongVersionQueries (INT4 i4MgmdRouterInterfaceIfIndex,
                                              INT4
                                              i4MgmdRouterInterfaceQuerierType,
                                              UINT4
                                              *pu4RetValMgmdRouterInterfaceWrongVersionQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceWrongVersionQueries
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceWrongVersionQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceJoins
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceJoins (INT4 i4MgmdRouterInterfaceIfIndex,
                                INT4 i4MgmdRouterInterfaceQuerierType,
                                UINT4 *pu4RetValMgmdRouterInterfaceJoins)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceJoins (i4MgmdRouterInterfaceIfIndex,
                                              i4MgmdRouterInterfaceQuerierType,
                                              pu4RetValMgmdRouterInterfaceJoins);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceProxyIfIndex
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceProxyIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceProxyIfIndex (INT4 i4MgmdRouterInterfaceIfIndex,
                                       INT4 i4MgmdRouterInterfaceQuerierType,
                                       INT4
                                       *pi4RetValMgmdRouterInterfaceProxyIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceProxyIfIndex
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pi4RetValMgmdRouterInterfaceProxyIfIndex);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceGroups
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceGroups
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceGroups (INT4 i4MgmdRouterInterfaceIfIndex,
                                 INT4 i4MgmdRouterInterfaceQuerierType,
                                 UINT4 *pu4RetValMgmdRouterInterfaceGroups)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceGroups (i4MgmdRouterInterfaceIfIndex,
                                               i4MgmdRouterInterfaceQuerierType,
                                               pu4RetValMgmdRouterInterfaceGroups);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceRobustness
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceRobustness
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceRobustness (INT4 i4MgmdRouterInterfaceIfIndex,
                                     INT4 i4MgmdRouterInterfaceQuerierType,
                                     UINT4
                                     *pu4RetValMgmdRouterInterfaceRobustness)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceRobustness (i4MgmdRouterInterfaceIfIndex,
                                                   i4MgmdRouterInterfaceQuerierType,
                                                   pu4RetValMgmdRouterInterfaceRobustness);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceLastMemberQueryInterval
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceLastMemberQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceLastMemberQueryInterval (INT4
                                                  i4MgmdRouterInterfaceIfIndex,
                                                  INT4
                                                  i4MgmdRouterInterfaceQuerierType,
                                                  UINT4
                                                  *pu4RetValMgmdRouterInterfaceLastMemberQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceLastMembQueryIntvl
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceLastMemberQueryInterval);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceLastMemberQueryCount
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceLastMemberQueryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceLastMemberQueryCount (INT4
                                               i4MgmdRouterInterfaceIfIndex,
                                               INT4
                                               i4MgmdRouterInterfaceQuerierType,
                                               UINT4
                                               *pu4RetValMgmdRouterInterfaceLastMemberQueryCount)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetMgmdRouterInterfaceLastMemberQueryCount
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceLastMemberQueryCount);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceStartupQueryCount
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceStartupQueryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceStartupQueryCount (INT4 i4MgmdRouterInterfaceIfIndex,
                                            INT4
                                            i4MgmdRouterInterfaceQuerierType,
                                            UINT4
                                            *pu4RetValMgmdRouterInterfaceStartupQueryCount)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetMgmdRouterInterfaceStartupQueryCount
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceStartupQueryCount);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterInterfaceStartupQueryInterval
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                retValMgmdRouterInterfaceStartupQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterInterfaceStartupQueryInterval (INT4
                                               i4MgmdRouterInterfaceIfIndex,
                                               INT4
                                               i4MgmdRouterInterfaceQuerierType,
                                               UINT4
                                               *pu4RetValMgmdRouterInterfaceStartupQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmdUtilNmhGetMgmdRouterInterfaceStartupQueryInterval
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         pu4RetValMgmdRouterInterfaceStartupQueryInterval);
    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMgmdRouterInterfaceQueryInterval
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                setValMgmdRouterInterfaceQueryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdRouterInterfaceQueryInterval (INT4 i4MgmdRouterInterfaceIfIndex,
                                        INT4 i4MgmdRouterInterfaceQuerierType,
                                        UINT4
                                        u4SetValMgmdRouterInterfaceQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceQueryInterval
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         u4SetValMgmdRouterInterfaceQueryInterval);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetMgmdRouterInterfaceStatus
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                setValMgmdRouterInterfaceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdRouterInterfaceStatus (INT4 i4MgmdRouterInterfaceIfIndex,
                                 INT4 i4MgmdRouterInterfaceQuerierType,
                                 INT4 i4SetValMgmdRouterInterfaceStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceStatus (i4MgmdRouterInterfaceIfIndex,
                                               i4MgmdRouterInterfaceQuerierType,
                                               i4SetValMgmdRouterInterfaceStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetMgmdRouterInterfaceVersion
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                setValMgmdRouterInterfaceVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdRouterInterfaceVersion (INT4 i4MgmdRouterInterfaceIfIndex,
                                  INT4 i4MgmdRouterInterfaceQuerierType,
                                  UINT4 u4SetValMgmdRouterInterfaceVersion)
{
    INT1                i1Status = SNMP_FAILURE;
    if (i4MgmdRouterInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
    {
        if (u4SetValMgmdRouterInterfaceVersion == IGMP_TWO)
        {
            u4SetValMgmdRouterInterfaceVersion = MLD_VERSION_1;
        }
        else if (u4SetValMgmdRouterInterfaceVersion == IGMP_VERSION_THREE)
        {
            u4SetValMgmdRouterInterfaceVersion = MLD_VERSION_2;
        }
    }
    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceVersion (i4MgmdRouterInterfaceIfIndex,
                                                i4MgmdRouterInterfaceQuerierType,
                                                u4SetValMgmdRouterInterfaceVersion);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetMgmdRouterInterfaceQueryMaxResponseTime
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                setValMgmdRouterInterfaceQueryMaxResponseTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdRouterInterfaceQueryMaxResponseTime (INT4
                                               i4MgmdRouterInterfaceIfIndex,
                                               INT4
                                               i4MgmdRouterInterfaceQuerierType,
                                               UINT4
                                               u4SetValMgmdRouterInterfaceQueryMaxResponseTime)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceQueryMaxResponseTime
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         u4SetValMgmdRouterInterfaceQueryMaxResponseTime);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetMgmdRouterInterfaceProxyIfIndex
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                setValMgmdRouterInterfaceProxyIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdRouterInterfaceProxyIfIndex (INT4 i4MgmdRouterInterfaceIfIndex,
                                       INT4 i4MgmdRouterInterfaceQuerierType,
                                       INT4
                                       i4SetValMgmdRouterInterfaceProxyIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceProxyIfIndex
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         i4SetValMgmdRouterInterfaceProxyIfIndex);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetMgmdRouterInterfaceRobustness
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                setValMgmdRouterInterfaceRobustness
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdRouterInterfaceRobustness (INT4 i4MgmdRouterInterfaceIfIndex,
                                     INT4 i4MgmdRouterInterfaceQuerierType,
                                     UINT4
                                     u4SetValMgmdRouterInterfaceRobustness)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceRobustness (i4MgmdRouterInterfaceIfIndex,
                                                   i4MgmdRouterInterfaceQuerierType,
                                                   u4SetValMgmdRouterInterfaceRobustness);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetMgmdRouterInterfaceLastMemberQueryInterval
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                setValMgmdRouterInterfaceLastMemberQueryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMgmdRouterInterfaceLastMemberQueryInterval (INT4
                                                  i4MgmdRouterInterfaceIfIndex,
                                                  INT4
                                                  i4MgmdRouterInterfaceQuerierType,
                                                  UINT4
                                                  u4SetValMgmdRouterInterfaceLastMemberQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceLastMembQueryIntvl
        (i4MgmdRouterInterfaceIfIndex, i4MgmdRouterInterfaceQuerierType,
         u4SetValMgmdRouterInterfaceLastMemberQueryInterval);
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MgmdRouterInterfaceQueryInterval
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                testValMgmdRouterInterfaceQueryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdRouterInterfaceQueryInterval (UINT4 *pu4ErrorCode,
                                           INT4 i4MgmdRouterInterfaceIfIndex,
                                           INT4
                                           i4MgmdRouterInterfaceQuerierType,
                                           UINT4
                                           u4TestValMgmdRouterInterfaceQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval (pu4ErrorCode,
                                                         i4MgmdRouterInterfaceIfIndex,
                                                         i4MgmdRouterInterfaceQuerierType,
                                                         u4TestValMgmdRouterInterfaceQueryInterval);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdRouterInterfaceStatus
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                testValMgmdRouterInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdRouterInterfaceStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4MgmdRouterInterfaceIfIndex,
                                    INT4 i4MgmdRouterInterfaceQuerierType,
                                    INT4 i4TestValMgmdRouterInterfaceStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus (pu4ErrorCode,
                                                  i4MgmdRouterInterfaceIfIndex,
                                                  i4MgmdRouterInterfaceQuerierType,
                                                  i4TestValMgmdRouterInterfaceStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdRouterInterfaceVersion
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                testValMgmdRouterInterfaceVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdRouterInterfaceVersion (UINT4 *pu4ErrorCode,
                                     INT4 i4MgmdRouterInterfaceIfIndex,
                                     INT4 i4MgmdRouterInterfaceQuerierType,
                                     UINT4 u4TestValMgmdRouterInterfaceVersion)
{
    INT1                i1Status = SNMP_FAILURE;
    if (i4MgmdRouterInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
    {
        if (u4TestValMgmdRouterInterfaceVersion == IGMP_TWO)
        {
            u4TestValMgmdRouterInterfaceVersion = MLD_VERSION_1;
        }
        else if (u4TestValMgmdRouterInterfaceVersion == IGMP_VERSION_THREE)
        {
            u4TestValMgmdRouterInterfaceVersion = MLD_VERSION_2;
        }
    }
    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion (pu4ErrorCode,
                                                   i4MgmdRouterInterfaceIfIndex,
                                                   i4MgmdRouterInterfaceQuerierType,
                                                   u4TestValMgmdRouterInterfaceVersion);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdRouterInterfaceQueryMaxResponseTime
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                testValMgmdRouterInterfaceQueryMaxResponseTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdRouterInterfaceQueryMaxResponseTime (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4MgmdRouterInterfaceIfIndex,
                                                  INT4
                                                  i4MgmdRouterInterfaceQuerierType,
                                                  UINT4
                                                  u4TestValMgmdRouterInterfaceQueryMaxResponseTime)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime (pu4ErrorCode,
                                                                i4MgmdRouterInterfaceIfIndex,
                                                                i4MgmdRouterInterfaceQuerierType,
                                                                u4TestValMgmdRouterInterfaceQueryMaxResponseTime);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdRouterInterfaceProxyIfIndex
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                testValMgmdRouterInterfaceProxyIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdRouterInterfaceProxyIfIndex (UINT4 *pu4ErrorCode,
                                          INT4 i4MgmdRouterInterfaceIfIndex,
                                          INT4 i4MgmdRouterInterfaceQuerierType,
                                          INT4
                                          i4TestValMgmdRouterInterfaceProxyIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceProxyIfIndex (pu4ErrorCode,
                                                        i4MgmdRouterInterfaceIfIndex,
                                                        i4MgmdRouterInterfaceQuerierType,
                                                        i4TestValMgmdRouterInterfaceProxyIfIndex);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdRouterInterfaceRobustness
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                testValMgmdRouterInterfaceRobustness
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdRouterInterfaceRobustness (UINT4 *pu4ErrorCode,
                                        INT4 i4MgmdRouterInterfaceIfIndex,
                                        INT4 i4MgmdRouterInterfaceQuerierType,
                                        UINT4
                                        u4TestValMgmdRouterInterfaceRobustness)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness (pu4ErrorCode,
                                                      i4MgmdRouterInterfaceIfIndex,
                                                      i4MgmdRouterInterfaceQuerierType,
                                                      u4TestValMgmdRouterInterfaceRobustness);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2MgmdRouterInterfaceLastMemberQueryInterval
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType

                The Object 
                testValMgmdRouterInterfaceLastMemberQueryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MgmdRouterInterfaceLastMemberQueryInterval (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4MgmdRouterInterfaceIfIndex,
                                                     INT4
                                                     i4MgmdRouterInterfaceQuerierType,
                                                     UINT4
                                                     u4TestValMgmdRouterInterfaceLastMemberQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl (pu4ErrorCode,
                                                              i4MgmdRouterInterfaceIfIndex,
                                                              i4MgmdRouterInterfaceQuerierType,
                                                              u4TestValMgmdRouterInterfaceLastMemberQueryInterval);
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MgmdRouterInterfaceTable
 Input       :  The Indices
                MgmdRouterInterfaceIfIndex
                MgmdRouterInterfaceQuerierType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MgmdRouterInterfaceTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MgmdHostCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdHostCacheTable
 Input       :  The Indices
                MgmdHostCacheAddressType
                MgmdHostCacheAddress
                MgmdHostCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdHostCacheTable (INT4 i4MgmdHostCacheAddressType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMgmdHostCacheAddress,
                                            INT4 i4MgmdHostCacheIfIndex)
{
    UNUSED_PARAM (i4MgmdHostCacheAddressType);
    UNUSED_PARAM (pMgmdHostCacheAddress);
    UNUSED_PARAM (i4MgmdHostCacheIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdHostCacheTable
 Input       :  The Indices
                MgmdHostCacheAddressType
                MgmdHostCacheAddress
                MgmdHostCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdHostCacheTable (INT4 *pi4MgmdHostCacheAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMgmdHostCacheAddress,
                                    INT4 *pi4MgmdHostCacheIfIndex)
{
    UNUSED_PARAM (pi4MgmdHostCacheAddressType);
    UNUSED_PARAM (pMgmdHostCacheAddress);
    UNUSED_PARAM (pi4MgmdHostCacheIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdHostCacheTable
 Input       :  The Indices
                MgmdHostCacheAddressType
                nextMgmdHostCacheAddressType
                MgmdHostCacheAddress
                nextMgmdHostCacheAddress
                MgmdHostCacheIfIndex
                nextMgmdHostCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdHostCacheTable (INT4 i4MgmdHostCacheAddressType,
                                   INT4 *pi4NextMgmdHostCacheAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMgmdHostCacheAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMgmdHostCacheAddress,
                                   INT4 i4MgmdHostCacheIfIndex,
                                   INT4 *pi4NextMgmdHostCacheIfIndex)
{
    UNUSED_PARAM (i4MgmdHostCacheAddressType);
    UNUSED_PARAM (pi4NextMgmdHostCacheAddressType);
    UNUSED_PARAM (pMgmdHostCacheAddress);
    UNUSED_PARAM (pNextMgmdHostCacheAddress);
    UNUSED_PARAM (i4MgmdHostCacheIfIndex);
    UNUSED_PARAM (pi4NextMgmdHostCacheIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMgmdHostCacheUpTime
 Input       :  The Indices
                MgmdHostCacheAddressType
                MgmdHostCacheAddress
                MgmdHostCacheIfIndex

                The Object 
                retValMgmdHostCacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostCacheUpTime (INT4 i4MgmdHostCacheAddressType,
                           tSNMP_OCTET_STRING_TYPE * pMgmdHostCacheAddress,
                           INT4 i4MgmdHostCacheIfIndex,
                           UINT4 *pu4RetValMgmdHostCacheUpTime)
{
    UNUSED_PARAM (i4MgmdHostCacheAddressType);
    UNUSED_PARAM (pMgmdHostCacheAddress);
    UNUSED_PARAM (i4MgmdHostCacheIfIndex);
    UNUSED_PARAM (pu4RetValMgmdHostCacheUpTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMgmdHostCacheLastReporter
 Input       :  The Indices
                MgmdHostCacheAddressType
                MgmdHostCacheAddress
                MgmdHostCacheIfIndex

                The Object 
                retValMgmdHostCacheLastReporter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostCacheLastReporter (INT4 i4MgmdHostCacheAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pMgmdHostCacheAddress,
                                 INT4 i4MgmdHostCacheIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValMgmdHostCacheLastReporter)
{
    UNUSED_PARAM (i4MgmdHostCacheAddressType);
    UNUSED_PARAM (pMgmdHostCacheAddress);
    UNUSED_PARAM (i4MgmdHostCacheIfIndex);
    UNUSED_PARAM (pRetValMgmdHostCacheLastReporter);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMgmdHostCacheSourceFilterMode
 Input       :  The Indices
                MgmdHostCacheAddressType
                MgmdHostCacheAddress
                MgmdHostCacheIfIndex

                The Object 
                retValMgmdHostCacheSourceFilterMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostCacheSourceFilterMode (INT4 i4MgmdHostCacheAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMgmdHostCacheAddress,
                                     INT4 i4MgmdHostCacheIfIndex,
                                     INT4
                                     *pi4RetValMgmdHostCacheSourceFilterMode)
{
    UNUSED_PARAM (i4MgmdHostCacheAddressType);
    UNUSED_PARAM (pMgmdHostCacheAddress);
    UNUSED_PARAM (i4MgmdHostCacheIfIndex);
    UNUSED_PARAM (pi4RetValMgmdHostCacheSourceFilterMode);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MgmdRouterCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdRouterCacheTable
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdRouterCacheTable (INT4 i4MgmdRouterCacheAddressType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pMgmdRouterCacheAddress,
                                              INT4 i4MgmdRouterCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable
        (i4MgmdRouterCacheAddressType, CacheAddr, i4MgmdRouterCacheIfIndex);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdRouterCacheTable
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdRouterCacheTable (INT4 *pi4MgmdRouterCacheAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMgmdRouterCacheAddress,
                                      INT4 *pi4MgmdRouterCacheIfIndex)
{
    INT4                i4AddrType = IGMP_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpCacheTable
        (&i4AddrType, &CacheAddr, pi4MgmdRouterCacheIfIndex);
    if (i1Status == SNMP_SUCCESS)
    {
	    IGMP_COPY_IPVX_TO_OCTET (pMgmdRouterCacheAddress, CacheAddr,
			    i4AddrType);
	    *pi4MgmdRouterCacheAddressType = i4AddrType;
	    return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdRouterCacheTable
 Input       :  The Indices
                MgmdRouterCacheAddressType
                nextMgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                nextMgmdRouterCacheAddress
                MgmdRouterCacheIfIndex
                nextMgmdRouterCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdRouterCacheTable (INT4 i4MgmdRouterCacheAddressType,
                                     INT4 *pi4NextMgmdRouterCacheAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMgmdRouterCacheAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMgmdRouterCacheAddress,
                                     INT4 i4MgmdRouterCacheIfIndex,
                                     INT4 *pi4NextMgmdRouterCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;
    tIgmpIPvXAddr       CacheNextAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_IPVX_ADDR_CLEAR (&CacheNextAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);
    i1Status =
        IgmpMgmtUtilNmhGetNextIndexIgmpCacheTable (i4MgmdRouterCacheAddressType,
                                                   pi4NextMgmdRouterCacheAddressType,
                                                   CacheAddr, &CacheNextAddr,
                                                   i4MgmdRouterCacheIfIndex,
                                                   pi4NextMgmdRouterCacheIfIndex);

    if (i1Status == SNMP_SUCCESS)
    {
        IGMP_COPY_IPVX_TO_OCTET (pNextMgmdRouterCacheAddress,
                                 CacheNextAddr,
                                 *pi4NextMgmdRouterCacheAddressType);
    }
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMgmdRouterCacheLastReporter
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex

                The Object 
                retValMgmdRouterCacheLastReporter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterCacheLastReporter (INT4 i4MgmdRouterCacheAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMgmdRouterCacheAddress,
                                   INT4 i4MgmdRouterCacheIfIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValMgmdRouterCacheLastReporter)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;
    tIgmpIPvXAddr       CacheLastReporter;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_IPVX_ADDR_CLEAR (&CacheLastReporter);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);
    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheLastReporter (i4MgmdRouterCacheAddressType,
                                                 CacheAddr,
                                                 i4MgmdRouterCacheIfIndex,
                                                 &CacheLastReporter);

    if (i1Status == SNMP_SUCCESS)
    {
        IGMP_COPY_IPVX_TO_OCTET (pRetValMgmdRouterCacheLastReporter,
                                 CacheLastReporter,
                                 i4MgmdRouterCacheAddressType);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterCacheUpTime
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex

                The Object 
                retValMgmdRouterCacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterCacheUpTime (INT4 i4MgmdRouterCacheAddressType,
                             tSNMP_OCTET_STRING_TYPE * pMgmdRouterCacheAddress,
                             INT4 i4MgmdRouterCacheIfIndex,
                             UINT4 *pu4RetValMgmdRouterCacheUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheUpTime (i4MgmdRouterCacheAddressType,
                                           CacheAddr, i4MgmdRouterCacheIfIndex,
                                           pu4RetValMgmdRouterCacheUpTime);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterCacheExpiryTime
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex

                The Object 
                retValMgmdRouterCacheExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterCacheExpiryTime (INT4 i4MgmdRouterCacheAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pMgmdRouterCacheAddress,
                                 INT4 i4MgmdRouterCacheIfIndex,
                                 UINT4 *pu4RetValMgmdRouterCacheExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheExpiryTime (i4MgmdRouterCacheAddressType,
                                               CacheAddr,
                                               i4MgmdRouterCacheIfIndex,
                                               pu4RetValMgmdRouterCacheExpiryTime);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterCacheExcludeModeExpiryTimer
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex

                The Object 
                retValMgmdRouterCacheExcludeModeExpiryTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterCacheExcludeModeExpiryTimer (INT4 i4MgmdRouterCacheAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pMgmdRouterCacheAddress,
                                             INT4 i4MgmdRouterCacheIfIndex,
                                             UINT4
                                             *pu4RetValMgmdRouterCacheExcludeModeExpiryTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);

    i1Status =
        IgmpMgmdUtilNmhGetMgmdRouterCacheExcludeModeExpiryTimer
        (i4MgmdRouterCacheAddressType, CacheAddr, i4MgmdRouterCacheIfIndex,
         pu4RetValMgmdRouterCacheExcludeModeExpiryTimer);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterCacheVersion1HostTimer
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex

                The Object 
                retValMgmdRouterCacheVersion1HostTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterCacheVersion1HostTimer (INT4 i4MgmdRouterCacheAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMgmdRouterCacheAddress,
                                        INT4 i4MgmdRouterCacheIfIndex,
                                        UINT4
                                        *pu4RetValMgmdRouterCacheVersion1HostTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheVersion1HostTimer
        (i4MgmdRouterCacheAddressType, CacheAddr, i4MgmdRouterCacheIfIndex,
         pu4RetValMgmdRouterCacheVersion1HostTimer);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterCacheVersion2HostTimer
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex

                The Object 
                retValMgmdRouterCacheVersion2HostTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterCacheVersion2HostTimer (INT4 i4MgmdRouterCacheAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMgmdRouterCacheAddress,
                                        INT4 i4MgmdRouterCacheIfIndex,
                                        UINT4
                                        *pu4RetValMgmdRouterCacheVersion2HostTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheVersion2HostTimer
        (i4MgmdRouterCacheAddressType, CacheAddr, i4MgmdRouterCacheIfIndex,
         pu4RetValMgmdRouterCacheVersion2HostTimer);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetMgmdRouterCacheSourceFilterMode
 Input       :  The Indices
                MgmdRouterCacheAddressType
                MgmdRouterCacheAddress
                MgmdRouterCacheIfIndex

                The Object 
                retValMgmdRouterCacheSourceFilterMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterCacheSourceFilterMode (INT4 i4MgmdRouterCacheAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMgmdRouterCacheAddress,
                                       INT4 i4MgmdRouterCacheIfIndex,
                                       INT4
                                       *pi4RetValMgmdRouterCacheSourceFilterMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       CacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&CacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (CacheAddr, pMgmdRouterCacheAddress,
                             i4MgmdRouterCacheAddressType);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheSourceFilterMode
        (i4MgmdRouterCacheAddressType, CacheAddr, i4MgmdRouterCacheIfIndex,
         pi4RetValMgmdRouterCacheSourceFilterMode);
    return i1Status;
}

/* LOW LEVEL Routines for Table : MgmdInverseHostCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdInverseHostCacheTable
 Input       :  The Indices
                MgmdInverseHostCacheIfIndex
                MgmdInverseHostCacheAddressType
                MgmdInverseHostCacheAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdInverseHostCacheTable (INT4
                                                   i4MgmdInverseHostCacheIfIndex,
                                                   INT4
                                                   i4MgmdInverseHostCacheAddressType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pMgmdInverseHostCacheAddress)
{
    UNUSED_PARAM (i4MgmdInverseHostCacheIfIndex);
    UNUSED_PARAM (i4MgmdInverseHostCacheAddressType);
    UNUSED_PARAM (pMgmdInverseHostCacheAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdInverseHostCacheTable
 Input       :  The Indices
                MgmdInverseHostCacheIfIndex
                MgmdInverseHostCacheAddressType
                MgmdInverseHostCacheAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdInverseHostCacheTable (INT4 *pi4MgmdInverseHostCacheIfIndex,
                                           INT4
                                           *pi4MgmdInverseHostCacheAddressType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMgmdInverseHostCacheAddress)
{
    UNUSED_PARAM (pi4MgmdInverseHostCacheIfIndex);
    UNUSED_PARAM (pi4MgmdInverseHostCacheAddressType);
    UNUSED_PARAM (pMgmdInverseHostCacheAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdInverseHostCacheTable
 Input       :  The Indices
                MgmdInverseHostCacheIfIndex
                nextMgmdInverseHostCacheIfIndex
                MgmdInverseHostCacheAddressType
                nextMgmdInverseHostCacheAddressType
                MgmdInverseHostCacheAddress
                nextMgmdInverseHostCacheAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdInverseHostCacheTable (INT4 i4MgmdInverseHostCacheIfIndex,
                                          INT4
                                          *pi4NextMgmdInverseHostCacheIfIndex,
                                          INT4
                                          i4MgmdInverseHostCacheAddressType,
                                          INT4
                                          *pi4NextMgmdInverseHostCacheAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMgmdInverseHostCacheAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextMgmdInverseHostCacheAddress)
{
    UNUSED_PARAM (i4MgmdInverseHostCacheIfIndex);
    UNUSED_PARAM (pi4NextMgmdInverseHostCacheIfIndex);
    UNUSED_PARAM (i4MgmdInverseHostCacheAddressType);
    UNUSED_PARAM (pi4NextMgmdInverseHostCacheAddressType);
    UNUSED_PARAM (pMgmdInverseHostCacheAddress);
    UNUSED_PARAM (pNextMgmdInverseHostCacheAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : MgmdInverseRouterCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdInverseRouterCacheTable
 Input       :  The Indices
                MgmdInverseRouterCacheIfIndex
                MgmdInverseRouterCacheAddressType
                MgmdInverseRouterCacheAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdInverseRouterCacheTable (INT4
                                                     i4MgmdInverseRouterCacheIfIndex,
                                                     INT4
                                                     i4MgmdInverseRouterCacheAddressType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pMgmdInverseRouterCacheAddress)
{
    UNUSED_PARAM (i4MgmdInverseRouterCacheIfIndex);
    UNUSED_PARAM (i4MgmdInverseRouterCacheAddressType);
    UNUSED_PARAM (pMgmdInverseRouterCacheAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdInverseRouterCacheTable
 Input       :  The Indices
                MgmdInverseRouterCacheIfIndex
                MgmdInverseRouterCacheAddressType
                MgmdInverseRouterCacheAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdInverseRouterCacheTable (INT4
                                             *pi4MgmdInverseRouterCacheIfIndex,
                                             INT4
                                             *pi4MgmdInverseRouterCacheAddressType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pMgmdInverseRouterCacheAddress)
{
    UNUSED_PARAM (pi4MgmdInverseRouterCacheIfIndex);
    UNUSED_PARAM (pi4MgmdInverseRouterCacheAddressType);
    UNUSED_PARAM (pMgmdInverseRouterCacheAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdInverseRouterCacheTable
 Input       :  The Indices
                MgmdInverseRouterCacheIfIndex
                nextMgmdInverseRouterCacheIfIndex
                MgmdInverseRouterCacheAddressType
                nextMgmdInverseRouterCacheAddressType
                MgmdInverseRouterCacheAddress
                nextMgmdInverseRouterCacheAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdInverseRouterCacheTable (INT4
                                            i4MgmdInverseRouterCacheIfIndex,
                                            INT4
                                            *pi4NextMgmdInverseRouterCacheIfIndex,
                                            INT4
                                            i4MgmdInverseRouterCacheAddressType,
                                            INT4
                                            *pi4NextMgmdInverseRouterCacheAddressType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMgmdInverseRouterCacheAddress,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextMgmdInverseRouterCacheAddress)
{
    UNUSED_PARAM (i4MgmdInverseRouterCacheIfIndex);
    UNUSED_PARAM (pi4NextMgmdInverseRouterCacheIfIndex);
    UNUSED_PARAM (i4MgmdInverseRouterCacheAddressType);
    UNUSED_PARAM (pi4NextMgmdInverseRouterCacheAddressType);
    UNUSED_PARAM (pMgmdInverseRouterCacheAddress);
    UNUSED_PARAM (pNextMgmdInverseRouterCacheAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : MgmdHostSrcListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdHostSrcListTable
 Input       :  The Indices
                MgmdHostSrcListAddressType
                MgmdHostSrcListAddress
                MgmdHostSrcListIfIndex
                MgmdHostSrcListHostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdHostSrcListTable (INT4 i4MgmdHostSrcListAddressType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pMgmdHostSrcListAddress,
                                              INT4 i4MgmdHostSrcListIfIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pMgmdHostSrcListHostAddress)
{
    UNUSED_PARAM (i4MgmdHostSrcListAddressType);
    UNUSED_PARAM (pMgmdHostSrcListAddress);
    UNUSED_PARAM (i4MgmdHostSrcListIfIndex);
    UNUSED_PARAM (pMgmdHostSrcListHostAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdHostSrcListTable
 Input       :  The Indices
                MgmdHostSrcListAddressType
                MgmdHostSrcListAddress
                MgmdHostSrcListIfIndex
                MgmdHostSrcListHostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdHostSrcListTable (INT4 *pi4MgmdHostSrcListAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMgmdHostSrcListAddress,
                                      INT4 *pi4MgmdHostSrcListIfIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMgmdHostSrcListHostAddress)
{
    UNUSED_PARAM (pi4MgmdHostSrcListAddressType);
    UNUSED_PARAM (pMgmdHostSrcListAddress);
    UNUSED_PARAM (pi4MgmdHostSrcListIfIndex);
    UNUSED_PARAM (pMgmdHostSrcListHostAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdHostSrcListTable
 Input       :  The Indices
                MgmdHostSrcListAddressType
                nextMgmdHostSrcListAddressType
                MgmdHostSrcListAddress
                nextMgmdHostSrcListAddress
                MgmdHostSrcListIfIndex
                nextMgmdHostSrcListIfIndex
                MgmdHostSrcListHostAddress
                nextMgmdHostSrcListHostAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdHostSrcListTable (INT4 i4MgmdHostSrcListAddressType,
                                     INT4 *pi4NextMgmdHostSrcListAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMgmdHostSrcListAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMgmdHostSrcListAddress,
                                     INT4 i4MgmdHostSrcListIfIndex,
                                     INT4 *pi4NextMgmdHostSrcListIfIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMgmdHostSrcListHostAddress,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMgmdHostSrcListHostAddress)
{
    UNUSED_PARAM (i4MgmdHostSrcListAddressType);
    UNUSED_PARAM (pi4NextMgmdHostSrcListAddressType);
    UNUSED_PARAM (pMgmdHostSrcListAddress);
    UNUSED_PARAM (pNextMgmdHostSrcListAddress);
    UNUSED_PARAM (i4MgmdHostSrcListIfIndex);
    UNUSED_PARAM (pi4NextMgmdHostSrcListIfIndex);
    UNUSED_PARAM (pMgmdHostSrcListHostAddress);
    UNUSED_PARAM (pNextMgmdHostSrcListHostAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMgmdHostSrcListExpire
 Input       :  The Indices
                MgmdHostSrcListAddressType
                MgmdHostSrcListAddress
                MgmdHostSrcListIfIndex
                MgmdHostSrcListHostAddress

                The Object 
                retValMgmdHostSrcListExpire
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdHostSrcListExpire (INT4 i4MgmdHostSrcListAddressType,
                             tSNMP_OCTET_STRING_TYPE * pMgmdHostSrcListAddress,
                             INT4 i4MgmdHostSrcListIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pMgmdHostSrcListHostAddress,
                             UINT4 *pu4RetValMgmdHostSrcListExpire)
{
    UNUSED_PARAM (i4MgmdHostSrcListAddressType);
    UNUSED_PARAM (pMgmdHostSrcListAddress);
    UNUSED_PARAM (i4MgmdHostSrcListIfIndex);
    UNUSED_PARAM (pMgmdHostSrcListHostAddress);
    UNUSED_PARAM (pu4RetValMgmdHostSrcListExpire);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MgmdRouterSrcListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMgmdRouterSrcListTable
 Input       :  The Indices
                MgmdRouterSrcListAddressType
                MgmdRouterSrcListAddress
                MgmdRouterSrcListIfIndex
                MgmdRouterSrcListHostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMgmdRouterSrcListTable (INT4
                                                i4MgmdRouterSrcListAddressType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pMgmdRouterSrcListAddress,
                                                INT4 i4MgmdRouterSrcListIfIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pMgmdRouterSrcListHostAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddr;
    tIgmpIPvXAddr       SrcListHostAddr;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcListHostAddr);
    IGMP_COPY_OCTET_TO_IPVX (SrcListAddr, pMgmdRouterSrcListAddress,
                             i4MgmdRouterSrcListAddressType);
    IGMP_COPY_OCTET_TO_IPVX (SrcListHostAddr, pMgmdRouterSrcListHostAddress,
                             i4MgmdRouterSrcListAddressType);
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpSrcListTable
        (i4MgmdRouterSrcListAddressType, SrcListAddr,
         i4MgmdRouterSrcListIfIndex, SrcListHostAddr);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMgmdRouterSrcListTable
 Input       :  The Indices
                MgmdRouterSrcListAddressType
                MgmdRouterSrcListAddress
                MgmdRouterSrcListIfIndex
                MgmdRouterSrcListHostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMgmdRouterSrcListTable (INT4 *pi4MgmdRouterSrcListAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMgmdRouterSrcListAddress,
                                        INT4 *pi4MgmdRouterSrcListIfIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMgmdRouterSrcListHostAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddr;
    tIgmpIPvXAddr       SrcListHostAddr;
    IGMP_IPVX_ADDR_CLEAR (&SrcListAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcListHostAddr);

    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpSrcListTable
        (pi4MgmdRouterSrcListAddressType, &SrcListAddr,
         pi4MgmdRouterSrcListIfIndex, &SrcListHostAddr);

    if (i1Status == SNMP_SUCCESS)
    {
        IGMP_COPY_IPVX_TO_OCTET (pMgmdRouterSrcListAddress, SrcListAddr,
                                 *pi4MgmdRouterSrcListAddressType);
        IGMP_COPY_IPVX_TO_OCTET (pMgmdRouterSrcListHostAddress, SrcListHostAddr,
                                 *pi4MgmdRouterSrcListAddressType);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMgmdRouterSrcListTable
 Input       :  The Indices
                MgmdRouterSrcListAddressType
                nextMgmdRouterSrcListAddressType
                MgmdRouterSrcListAddress
                nextMgmdRouterSrcListAddress
                MgmdRouterSrcListIfIndex
                nextMgmdRouterSrcListIfIndex
                MgmdRouterSrcListHostAddress
                nextMgmdRouterSrcListHostAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMgmdRouterSrcListTable (INT4 i4MgmdRouterSrcListAddressType,
                                       INT4
                                       *pi4NextMgmdRouterSrcListAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMgmdRouterSrcListAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextMgmdRouterSrcListAddress,
                                       INT4 i4MgmdRouterSrcListIfIndex,
                                       INT4 *pi4NextMgmdRouterSrcListIfIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMgmdRouterSrcListHostAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextMgmdRouterSrcListHostAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddr;
    tIgmpIPvXAddr       SrcListHostAddr;
    tIgmpIPvXAddr       SrcListNextAddr;
    tIgmpIPvXAddr       NextHostAddr;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcListHostAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcListNextAddr);
    IGMP_IPVX_ADDR_CLEAR (&NextHostAddr);

    IGMP_COPY_OCTET_TO_IPVX (SrcListAddr, pMgmdRouterSrcListAddress,
                             i4MgmdRouterSrcListAddressType);
    IGMP_COPY_OCTET_TO_IPVX (SrcListHostAddr, pMgmdRouterSrcListHostAddress,
                             i4MgmdRouterSrcListAddressType);

    i1Status =
        IgmpMgmtUtilNmhGetNextIndexIgmpSrcListTable
        (i4MgmdRouterSrcListAddressType, pi4NextMgmdRouterSrcListAddressType,
         SrcListAddr, &SrcListNextAddr, i4MgmdRouterSrcListIfIndex,
         pi4NextMgmdRouterSrcListIfIndex, SrcListHostAddr, &NextHostAddr);

    if (i1Status == SNMP_SUCCESS)
    {
        IGMP_COPY_IPVX_TO_OCTET (pNextMgmdRouterSrcListAddress,
                                 SrcListNextAddr,
                                 *pi4NextMgmdRouterSrcListAddressType);
        IGMP_COPY_IPVX_TO_OCTET (pNextMgmdRouterSrcListHostAddress,
                                 NextHostAddr,
                                 *pi4NextMgmdRouterSrcListAddressType);

    }
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMgmdRouterSrcListExpire
 Input       :  The Indices
                MgmdRouterSrcListAddressType
                MgmdRouterSrcListAddress
                MgmdRouterSrcListIfIndex
                MgmdRouterSrcListHostAddress

                The Object 
                retValMgmdRouterSrcListExpire
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMgmdRouterSrcListExpire (INT4 i4MgmdRouterSrcListAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pMgmdRouterSrcListAddress,
                               INT4 i4MgmdRouterSrcListIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pMgmdRouterSrcListHostAddress,
                               UINT4 *pu4RetValMgmdRouterSrcListExpire)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddr;
    tIgmpIPvXAddr       SrcListHostAddr;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcListHostAddr);

    IGMP_COPY_OCTET_TO_IPVX (SrcListAddr, pMgmdRouterSrcListAddress,
                             i4MgmdRouterSrcListAddressType);
    IGMP_COPY_OCTET_TO_IPVX (SrcListHostAddr, pMgmdRouterSrcListHostAddress,
                             i4MgmdRouterSrcListAddressType);

    i1Status =
        IgmpMgmtUtilNmhGetMgmdRouterSrcListExpire
        (i4MgmdRouterSrcListAddressType, SrcListAddr,
         i4MgmdRouterSrcListIfIndex, SrcListHostAddr,
         pu4RetValMgmdRouterSrcListExpire);

    return i1Status;
}
