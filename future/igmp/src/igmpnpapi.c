/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpnpapi.c,v 1.2 2015/03/18 13:39:49 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of IP module.
 ******************************************************************************/

#ifndef _IGMPNPAPI_C_
#define _IGMPNPAPI_C_

#include "nputil.h"
#include "igmpinc.h"
#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_NP_MODULE ;
#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : IgmpFsIgmpHwEnableIgmp                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsIgmpHwEnableIgmp
 *                                                                          
 *    Input(s)            : Arguments of FsIgmpHwEnableIgmp
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgmpFsIgmpHwEnableIgmp ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGMP_MOD,    /* Module ID */
                         FS_IGMP_HW_ENABLE_IGMP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
         IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                    "Enabling IGMP on  Hardware failed\r\n");
	return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgmpFsIgmpHwDisableIgmp                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsIgmpHwDisableIgmp
 *                                                                          
 *    Input(s)            : Arguments of FsIgmpHwDisableIgmp
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgmpFsIgmpHwDisableIgmp ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGMP_MOD,    /* Module ID */
                         FS_IGMP_HW_DISABLE_IGMP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                   "Disabling IGMP on  Hardware failed\r\n");
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgmpFsNpIpv4SetIgmpIfaceJoinRate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4SetIgmpIfaceJoinRate
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4SetIgmpIfaceJoinRate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgmpFsNpIpv4SetIgmpIfaceJoinRate (INT4 i4IfIndex, INT4 i4RateLimit)
{
    tFsHwNp             FsHwNp;
    tIgmpNpModInfo     *pIgmpNpModInfo = NULL;
    tIgmpNpWrFsNpIpv4SetIgmpIfaceJoinRate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGMP_MOD,    /* Module ID */
                         FS_NP_IPV4_SET_IGMP_IFACE_JOIN_RATE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgmpNpModInfo = &(FsHwNp.IgmpNpModInfo);
    pEntry = &pIgmpNpModInfo->IgmpNpFsNpIpv4SetIgmpIfaceJoinRate;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4RateLimit = i4RateLimit;
    IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                   "IGMP join rate on  Hardware for interface index %d is %d\n",
                    pEntry->i4IfIndex, pEntry->i4RateLimit);
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	 IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                   "Setting IGMP join rate on  Hardware failed for IGMP index %d\n",
                    pEntry->i4IfIndex);
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IgmpFsIgmpMbsmHwEnableIgmp                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsIgmpMbsmHwEnableIgmp
 *                                                                          
 *    Input(s)            : Arguments of FsIgmpMbsmHwEnableIgmp
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgmpFsIgmpMbsmHwEnableIgmp (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIgmpNpModInfo     *pIgmpNpModInfo = NULL;
    tIgmpNpWrFsIgmpMbsmHwEnableIgmp *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGMP_MOD,    /* Module ID */
                         FS_IGMP_MBSM_HW_ENABLE_IGMP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgmpNpModInfo = &(FsHwNp.IgmpNpModInfo);
    pEntry = &pIgmpNpModInfo->IgmpNpFsIgmpMbsmHwEnableIgmp;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_NP_MODULE, IGMP_NAME,
                   "Enabling  IGMP MBSM on  Hardware failed for slot info %d\n",
                    pEntry->pSlotInfo);
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif
#endif
