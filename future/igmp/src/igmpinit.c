/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpinit.c,v 1.44 2017/05/30 11:13:43 siva Exp $
 *
 * Description : This file contains the routines that are         
 *                           called at the time of IGMP initialization        
 *                                                                            
 ******************************************************************************/

#include "igmpinc.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_INIT_SHUT_MODULE;
#endif

/*********************************************************************/
/* Function           : IgmpInitialize                               */
/*                                                                   */
/* Description        : Allocates and initializes the IGMP related   */
/*                      data structures.                             */
/*                                                                   */
/* Input(s)           : None                                         */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : IGMP_OK/IGMP_NOT_OK                          */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpInitialize                        **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT1
IgmpInitialize (VOID)
{
    tNetIpRegInfo       RegInfo;
    UINT4               u4IPvXZero = IGMP_ZERO;
    UINT2               u2Count = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpInitialize()\r \n");
    IgmpFillMem (&gIgmpMemPool, IGMP_ZERO, sizeof (gIgmpMemPool));
    IgmpFillMem (&RegInfo, IGMP_ZERO, sizeof (tNetIpRegInfo));

    if (IgmpGlobalMemInit () == IGMP_NOT_OK)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Memory Allocation Failed \n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IgmpGetModuleName (IGMP_OS_RES_MODULE),
                       "Memory Allocation Failed \n");
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                         IGMP_OS_RES_MODULE, IGMP_NAME,
                         IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
                         "for poolid %d\r\n", gIgmpMemPool.IgmpQPoolId);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }

    if (IGMP_MEM_ALLOCATE
        (gIgmpMemPool.IgmpSrcSetArrayId, gpu4SrcSetArray,
         tIgmpIPvXAddr) == NULL)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Memory Allocation Failed \n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IgmpGetModuleName (IGMP_OS_RES_MODULE),
                       "Memory Allocation Failed \n");
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                         IGMP_OS_RES_MODULE, IGMP_NAME,
                         IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
                         "for array id %d\r\n", gIgmpMemPool.IgmpSrcSetArrayId);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }
    /* set the default values to gIgmpConfig */
    gIgmpConfig.u4IgmpTrcFlag = 0;
    gIgmpConfig.u4IgmpFastLeave = 0;
    gIgmpConfig.u4GlobalGrpLimit = 0;
    gIgmpConfig.u4GlobalCurCnt = 0;

    /* memset the gIPvXZero variable */
    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IPvXZero);

    if (IgmpInitInterfaceInfo () == IGMP_NOT_OK)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "IgmpInitInterfaceInfo Failed \n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }

    gIgmpGroup = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tIgmpGroup, RbNode), IgmpRBTreeGroupEntryCmp);

    if (gIgmpGroup == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IgmpGetModuleName (IGMP_OS_RES_MODULE),
                       "RB Tree creation for Igmp group table failed\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "RB Tree creation for Igmp group table failed\r\n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                              IGMP_OS_RES_MODULE, IGMP_NAME,
                              IgmpSysErrString[SYS_LOG_RB_TREE_CREATE_FAIL],
                              "for Igmp group table\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }

    gIgmpSSMMappedGrp = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tIgmpSSMMapGrpEntry, RbNode),
         IgmpSSMMapRBTreeGroupEntryCmp);

    if (gIgmpSSMMappedGrp == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IgmpGetModuleName (IGMP_OS_RES_MODULE),
                       "SSMMap RB Tree creation for Igmp group table failed\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "SSMMap RB Tree creation for Igmp group table failed\r\n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                              IGMP_OS_RES_MODULE, IGMP_NAME,
                              IgmpSysErrString[SYS_LOG_RB_TREE_CREATE_FAIL],
                              "for Igmp group table\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }

    gIgmpHostMcastGrpTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tIgmpHostMcastGrpEntry, IgmpMcastRbNode),
         IgmpRBTreeMcastGroupEntryCmp);
    if (gIgmpHostMcastGrpTable == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                       "RB Tree creation for Igmp Multicast group table failed\r\n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                              IGMP_OS_RES_MODULE, IGMP_NAME,
                              IgmpSysErrString[SYS_LOG_RB_TREE_CREATE_FAIL],
                              "for Mcast Igmp grouptable\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }

    for (u2Count = IGMP_ZERO; u2Count < IGMP_MAX_REGISTER; u2Count++)
    {
        gaIgmpRegister[u2Count].u1Flag = DOWN;
    }

    for (u2Count = 0; u2Count < MLD_MAX_REGISTER; u2Count++)
    {
        gaMldRegister[u2Count].u1Flag = DOWN;
    }

    if (TmrCreateTimerList ((const UINT1 *) IGMP_TASK_NAME,
                            IGMP_TIMER_EXPIRY_EVENT,
                            NULL, &gIgmpTimerListId) == TMR_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE,
                       IGMP_NAME, "Timer list creation failed !!!\n");
        return IGMP_NOT_OK;
    }

    IgmpTmrInitTmrDesc ();

    /* Register with IP */
    RegInfo.u1ProtoId = IGMP_PROTID;
    RegInfo.u2InfoMask |= NETIPV4_IFCHG_REQ;
    RegInfo.pIfStChng = IgmpHandleIfUpdates;
    RegInfo.pRtChng = NULL;
#ifdef LNXIP4_WANTED
    /* Register for receiving interface change updates */
    RegInfo.pProtoPktRecv = NULL;
#else
    RegInfo.u2InfoMask |= NETIPV4_PROTO_PKT_REQ;
    RegInfo.pProtoPktRecv = IgmpHandleIncomingPkt;
#endif
    if ((IGMP_REGISTER_WITH_IP (&RegInfo)) == FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE,
                       IGMP_NAME, "IGMP Registration with IP failed !!!\n");
        return IGMP_NOT_OK;
    }

#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
    /* MLD Registartion for MLD Packets receiving */
    if (NetIpv6RegisterHigherLayerProtocol (MLD_PROTID,
                                            NETIPV6_APPLICATION_RECEIVE,
                                            (VOID *) MLDInput)
        == NETIPV6_FAILURE)
    {

        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE,
                       IGMP_NAME, "Registration Failed\n ");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }

    if (NetIpv6RegisterHigherLayerProtocolInCxt (MLD_DEF_VRF_CTXT_ID,
                                                 MLD_PROTID,
                                                 NETIPV6_INTERFACE_PARAMETER_CHANGE
                                                 | NETIPV6_ADDRESS_CHANGE,
                                                 MldPortIpv6ModuleIfChgHandler)
        == NETIPV6_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE,
                       IGMP_NAME,
                       "MLD Registration with IPv6 Module failed !!!\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }
#endif
    gIgmpGrpList = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tIgmpGrpList, RbNode), IgmpRBTreeGroupListCmp);
    if (gIgmpGrpList == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                       "RB Tree creation for GroupList Table table failed\r\n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                              IGMP_OS_RES_MODULE, IGMP_NAME,
                              IgmpSysErrString[SYS_LOG_RB_TREE_CREATE_FAIL],
                              "for GroupList Table table\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitialize()\r \n");
        return IGMP_NOT_OK;
    }

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE,
                   IGMP_NAME, "Igmp init successful.\n");
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpInitialize()\r \n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpIfaceUp                                      */
/*                                                                         */
/*     Description   :  This function is  called to make IGMP UP on a      */
/*                       particular interface.                             */
/*     Input(s)      :  Interface Id of the interface on which IGMP should */
/*                       be made UP.                                       */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpIfaceUp                                 **/
/** $$TRACE_PROCEDURE_LEVEL = INTMD                                       **/
/***************************************************************************/
VOID
IgmpIfaceUp (tIgmpIface * pIfNode)
{
    tIgmpQuery         *pIgmp = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
#ifndef LNXIP4_WANTED
    tIgmpIPvXAddr       u4SrcAddrX;
#endif
#ifdef IP_VX_WANTED
    UINT4               u4AllHostsGroup = IGMP_ALL_HOSTS_GROUP;
    UINT4               u4IgmpIndex = IGMP_ZERO;
#endif
    UINT4               u4RandomTime = IGMP_ZERO;
#ifndef LNXIP4_WANTED
    UINT4               u4SrcAddr = IGMP_ZERO;
#endif

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpIfaceUp()\r \n");
    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));
#ifdef IP_VX_WANTED
    /*Changed for porting to use vxWorks IpStack */
    /* Adding membership for particular interface */
    if (IpifJoinMcastGroup (&u4AllHostsGroup, u4IgmpIndex, IGMP_PROTID)
        == IP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Failure in Adding membership for all host group. \n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Failure in Adding membership for all host group. \n");
    }
#endif
#ifdef MFWD_WANTED
    if (gIgmpConfig.i4ProxyStatus == IGMP_ENABLE)
    {
        IgpMfwdAddInterface (pIfNode->u4IfIndex);
    }
#endif

    /* Query timer is not started when IGMP is globally disabled */
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (gIgmpConfig.u1IgmpStatus == IGMP_DISABLE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "IGMP is globally disabled . \n");
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                           IGMP_NAME, "IGMP is globally disabled . \n");
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpIfaceUp()\r \n");
            return;
        }
    }
    else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (gIgmpConfig.u1MldStatus == IGMP_DISABLE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                           IGMP_NAME, "IGMP is globally disabled . \n");
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "IGMP is globally disabled . \n");
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpIfaceUp()\r \n");
            return;
        }
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (pIfNode->u4IfIndex) == IGMP_TRUE))
    {
        IgpUpIfUpdateUpIface (pIfNode->u4IfIndex);
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Deletion of Upstream interface failed \r\n");
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                  "Deletion of Upstream interface failed \r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpIfaceUp()\r \n");
        return;
    }
    else
    {
        IgmpGrpEntry.u4IfIndex = pIfNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfNode;

        pGrp = (tIgmpGroup *)
            RBTreeGetNext (gIgmpGroup, (tRBElem *) & IgmpGrpEntry, NULL);
        while (pGrp != NULL)
        {
            pNextGrp = (tIgmpGroup *) RBTreeGetNext
                (gIgmpGroup, (tRBElem *) pGrp, NULL);

            if (pGrp->pIfNode->u4IfIndex > pIfNode->u4IfIndex)
            {
                break;
            }

            if ((pGrp->pIfNode->u1AddrType != pIfNode->u1AddrType) ||
                ((pGrp->u1StaticFlg == IGMP_TRUE) &&
                 (pGrp->u1EntryStatus != IGMP_ACTIVE)))
            {
                pGrp = pNextGrp;
                continue;
            }

            /* SrcList can be > 0 for IGMPv2 interface with SSM
             * mapping configured, in which case handle the sources
             * in UpdateMrp */
            if (((pGrp->pIfNode->u1Version == IGMP_VERSION_2) &&
                 (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO)) ||
                (pGrp->pIfNode->u1Version == MLD_VERSION_1))
            {
                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    /* Call the PROXY routine to update the consolidate group
                     * membership data base for static group registration */
                    IgpGrpConsolidate (pGrp, IGMP_VERSION_2, IGMP_NEW_GROUP);
                }
                else
                {
                    /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
            }
            else
            {
                if (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO)
                {
                    if ((pGrp->u1StaticFlg == IGMP_TRUE) &&
                        (pGrp->pIfNode->u1Version == IGMP_VERSION_3))
                    {
                        pSrc =
                            IgmpAddSource (pGrp, gIPvXZero, gIPvXZero,
                                           IGMP_FALSE);
                        if (pSrc != NULL)
                        {
                            pSrc->u1StaticFlg = IGMP_TRUE;
                            pSrc->u1FwdState = IGMP_TRUE;
                            pSrc->u1SrcSSMMapped =
                                pSrc->u1SrcSSMMapped | IGMP_SRC_CONF;
                        }
                    }
                    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                    {
                        /* Call the PROXY routine to update the consolidate group
                         * membership data base for static group registration */
                        IgpGrpConsolidate (pGrp, IGMP_VERSION_3,
                                           IGMP_NEW_GROUP);

                        if (IgpUpIfFormAndSendV3Report (IGMP_ZERO)
                            == IGMP_FAILURE)
                        {
                            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE,
                                           IGMP_NAME, "IGMP PROXY: Sending "
                                           " upstream report failed \r\n");
                            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                                      IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                                      "IGMP PROXY: Sending upstream report failed \r\n");
                        }
                    }
                    else
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }
                    pGrp = pNextGrp;
                    continue;
                }

                pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);

                while (pSrc != NULL)
                {
                    pNextSrc =
                        (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList,
                                                      &pSrc->Link);

                    if ((pSrc->u1StaticFlg == IGMP_TRUE) &&
                        (pSrc->u1EntryStatus != IGMP_ACTIVE))
                    {
                        pSrc = pNextSrc;
                        continue;
                    }

                    if ((pSrc->u1FwdState == IGMP_TRUE) &&
                        (IGMP_PROXY_STATUS () == IGMP_DISABLE))
                    {
                        if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                                    gIPvXZero) != IGMP_ZERO)
                        {
                            TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                            TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                         &pSrc->Link);
                        }
                    }
                    pSrc = pNextSrc;
                }

                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    /* Call the PROXY routine to update the consolidate group
                     * membership data base for static group registration */
                    IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_NEW_GROUP);

                    if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
                    {
                        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE,
                                       IGMP_NAME, "IGMP PROXY: Sending "
                                       " upstream report failed \r\n");
                        IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                                  "IGMP PROXY: Sending upstream report failed \r\n");
                    }

                    pGrp = pNextGrp;
                    continue;
                }

                if (pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)
                {
                    if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_JOIN);
                    }
                }
                else if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                {
                    if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_JOIN);
                    }
                    else
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }
                }
            }
            pGrp = pNextGrp;
        }
    }

#ifdef IP_VX_WANTED
    u4IgmpIndex = pIfNode->u4IfIndex;
#endif
#ifndef LNXIP4_WANTED
    /* If source address is 0.0.0.0, do not initiate query since this will default
     * to queries being sent on VLAN 1 member ports even if IGMP is not enabled on
     * VLAN 1.
     */
    if (pIfNode->u1AddrType == IGMP_IPVX_ADDR_FMLY_IPV4)
    {
        IGMP_IPVX_ADDR_INIT_IPV4 (u4SrcAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4SrcAddr);
        if ((IGMP_IPVX_ADDR_COMPARE (pIfNode->u4IfAddr, u4SrcAddrX)) ==
            IGMP_ZERO)
        {
            /* Check source address of associated interface in case of unnumbered
             * interface. Required locks are taken inside the function so they
             * are ignored here.
             */
            u4SrcAddr = IGMP_ZERO;
            if (IpGetSrcAddressOnInterface ((UINT2) pIfNode->u4IfIndex,
                                            (UINT4) IGMP_ZERO,
                                            &u4SrcAddr) == IP_SUCCESS)
            {
                if (u4SrcAddr == IP_ANY_ADDR)
                {
                    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                                   "Source Address of Upstream interface is 0.0.0.0 "
                                   "so Query will not be sent on this interface \r\n");
                    IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                              IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                              "Source Address of Upstream interface is 0.0.0.0 "
                              "so Query will not be sent on this interface \r\n");
                    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                                  "Exiting IgmpIfaceUp() \r\n");
                    return;
                }
            }
            else
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                               "Source Address of Upstream interface is 0.0.0.0 "
                               "so Query will not be sent on this interface \r\n");
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "Source Address of Upstream interface is 0.0.0.0 "
                          "so Query will not be sent on this interface \r\n");
                MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                              IgmpGetModuleName (IGMP_EXIT_MODULE),
                              "Exiting IgmpIfaceUp() \r\n");
                return;
            }
        }
    }
#endif
    pIgmp = pIfNode->pIfaceQry;

    OsixGetSysTime (&(pIgmp->u4QuerierUpTime));
    /* update the Igmp query block with querier address */
    IGMP_IPVX_ADDR_COPY (&(pIgmp->u4QuerierAddr), &(pIfNode->u4IfAddr));
    pIgmp->u1Querier = TRUE;

    IGMP_GET_RANDOM_IN_RANGE (IGMP_MAX_RAND, u4RandomTime);
    /* Startup queries are sent randomly(0-5secs) */
    IGMPSTARTTIMER (pIgmp->Timer, IGMP_STARTUP_QUERY_TIMER_ID, u4RandomTime,
                    IGMP_ZERO);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpIfaceUp()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpIfaceDown                                   */
/*                                                                         */
/*     Description   :   This function is called to make IGMP DOWN/INVALID */
/*                       on a particular interface.                        */
/*     Input(s)      :   Interface Id, state to set.                       */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :   None                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpIfaceDown                               **/
/** $$TRACE_PROCEDURE_LEVEL = INTMD                                       **/
/***************************************************************************/
VOID
IgmpIfaceDown (tIgmpIface * pIfNode)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
#ifdef IP_VX_WANTED
    UINT4               u4AllHostsGroup = IGMP_ALL_HOSTS_GROUP;
    UINT4               u4IgmpIndex = IGMP_ZERO;
#endif
    UINT1               u1VerFlag = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpIfaceDown()\r \n");
    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

#ifdef IP_VX_WANTED
    u4IgmpIndex = pIfNode->u4IfIndex;
    /*Changed for porting to use vxWorks IpStack */
    /* Drop membership for particular interface */
    if (IpifLeaveMcastGroup (&u4AllHostsGroup, u4IgmpIndex, IGMP_PROTID)
        == IP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Failure in Leaving membership for all host group. \n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Failure in Leaving membership for all host group. \n");

    }
#endif

/* For IGMP stack on Linuxip. Not for ISS */
#if defined (LNXIP4_WANTED) && !defined (ISS_WANTED)
    /* Delete the interface from the virtual interface table */
    if (IgmpSetMcastSocketOption (pIfNode, IGMP_DOWN) == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG,
                       IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                       "Disabling multicast socket option failed \n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG,
                       IGMP_IO_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Disabling multicast socket option failed \n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpIfaceDown()\r \n");
        return;
    }
#endif

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (pIfNode->u4IfIndex) == IGMP_TRUE))
    {
        if (IgpUpIfDeleteUpIfaceEntry (pIfNode->u4IfIndex, IGMP_FALSE)
            != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Deletion of Upstream interface failed \r\n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                      IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                      "Deletion of Upstream interface failed \r\n");
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpIfaceDown()\r \n");
            return;
        }
    }
    else
    {
        /* clear querier related stuff */
        IgmpHdlTransitionToNonQuerier (pIfNode);
        /* Clear all membership information */
        IgmpGrpEntry.u4IfIndex = pIfNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfNode;
        pGrp = (tIgmpGroup *)
            RBTreeGetNext (gIgmpGroup, (tRBElem *) & IgmpGrpEntry, NULL);
        while (pGrp != NULL)
        {
            pNextGrp = (tIgmpGroup *) RBTreeGetNext
                (gIgmpGroup, (tRBElem *) pGrp, NULL);

            if (pGrp->pIfNode->u4IfIndex > pIfNode->u4IfIndex)
            {
                break;
            }

            if (pGrp->pIfNode->u1AddrType != pIfNode->u1AddrType)
            {
                pGrp = pNextGrp;
                continue;
            }

            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);

            while (pSrc != NULL)
            {
                pNextSrc =
                    (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);
                /* If a source record has both static and dynamic flags enabled
                   set the flag and retain the static record */
                if ((pSrc->u1StaticFlg == IGMP_TRUE)
                    && (pSrc->u1DynamicFlg == IGMP_TRUE))
                {
                    pSrc->u1DynamicFlg = IGMP_FALSE;
                    IGMPSTOPTIMER (pSrc->srcTimer);
                    pSrc->u1SrcModeFlag = IGMP_ADD_SOURCE;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pSrc->SrcDbNode);
                    IgmpRedSyncDynInfo ();
                    pSrc = pNextSrc;
                    continue;
                }
                else if (pSrc->u1StaticFlg == IGMP_TRUE)
                {
                    pSrc = pNextSrc;
                    continue;
                }
                pSrc->u1DelSyncFlag = IGMP_FALSE;
                IgmpDeleteSource (pGrp, pSrc);
                pSrc = pNextSrc;
            }
            /* If the group is learned dynamically and configured statically
               update the flag and stop the timer and retain the static join record */
            if ((pGrp->u1StaticFlg == IGMP_TRUE)
                && (pGrp->u1DynamicFlg == IGMP_TRUE))
            {
                pGrp->u1DynamicFlg = IGMP_FALSE;
                IGMPSTOPTIMER (pGrp->Timer);
                IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
                pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
                IgmpRedSyncDynInfo ();
                pGrp = pNextGrp;
                continue;
            }
            else if (pGrp->u1StaticFlg == IGMP_TRUE)
            {
                pGrp = pNextGrp;
                continue;
            }

            if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
                {
                    u1VerFlag = IGMP_VERSION_2;
                }
                else
                {
                    u1VerFlag = IGMP_VERSION_3;
                }
            }
            else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                if (pGrp->u4GroupCompMode != MLD_GCM_MLDV2)
                {
                    u1VerFlag = MLD_VERSION_1;
                }
                else
                {
                    u1VerFlag = MLD_VERSION_2;
                }
            }
            u1VerFlag |= IGMP_V3REP_SEND_FLAG;
            /* IGMP Proxy is enabled so update the consolidated
             * group membership data base */
            if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
            {
                /* Call the PROXY routine to update the consolidate group
                 * membership data base for group */
                IgpGrpConsolidate (pGrp, u1VerFlag, IGMP_GROUP_DELETION);
            }

            pGrp->u1DelSyncFlag = IGMP_FALSE;
            IgmpDeleteGroup (pIfNode, pGrp, pGrp->u1StaticFlg);
            pGrp = pNextGrp;
        }
        /*clearing statistics entry */
        /*NOTE:Interface Groups Counters are not set to zero */
        /*since static multicast groups could have been */
        /*added to this interface */
        pIfNode->u4InPkts = IGMP_ZERO;
        pIfNode->u4InJoins = IGMP_ZERO;
        pIfNode->u4InLeaves = IGMP_ZERO;
        pIfNode->u4InQueries = IGMP_ZERO;
        pIfNode->u4OutQueries = IGMP_ZERO;
        pIfNode->u4WrongVerQueries = IGMP_ZERO;
        pIfNode->u4ProcessedJoins = IGMP_ZERO;

        pIfNode->u4RxGenQueries = IGMP_ZERO;
        pIfNode->u4TxGenQueries = IGMP_ZERO;
        pIfNode->u4TxGrpQueries = IGMP_ZERO;
        pIfNode->u4TxGrpSrcQueries = IGMP_ZERO;
        pIfNode->u4RxGrpQueries = IGMP_ZERO;
        pIfNode->u4RxGrpSrcQueries = IGMP_ZERO;
        pIfNode->u4Txv2Leaves = IGMP_ZERO;
        pIfNode->u4CKSumError = IGMP_ZERO;
        pIfNode->u4PktLenError = IGMP_ZERO;;
        pIfNode->u4PktWithLocalIP = IGMP_ZERO;
        pIfNode->u4SubnetCheckFailure = IGMP_ZERO;;
        pIfNode->u4QryFromNonQuerier = IGMP_ZERO;;
        pIfNode->u4ReportVersionMisMatch = IGMP_ZERO;;
        pIfNode->u4QryVersionMisMatch = IGMP_ZERO;;
        pIfNode->u4UnknownMsgType = IGMP_ZERO;;
        pIfNode->u4InvalidV1Report = IGMP_ZERO;;
        pIfNode->u4InvalidV2Report = IGMP_ZERO;;
        pIfNode->u4InvalidV3Report = IGMP_ZERO;;
        pIfNode->u4RouterAlertCheckFailure = IGMP_ZERO;;
        pIfNode->u4BadScopeErr = IGMP_ZERO;
#ifdef MULTICAST_SSM_ONLY
        pIfNode->u4DroppedASMIncomingPkts = IGMP_ZERO;
#endif

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            pIfNode->u4IgmpRxv1v2Reports = IGMP_ZERO;
            pIfNode->u4IgmpRxv3Reports = IGMP_ZERO;
            pIfNode->u4IgmpTxv1v2Reports = IGMP_ZERO;
            pIfNode->u4IgmpTxv3Reports = IGMP_ZERO;
            pIfNode->u4IgmpInSSMPkts = IGMP_ZERO;
            pIfNode->u4IgmpInvalidSSMPkts = IGMP_ZERO;
            pIfNode->u4IgmpMalformedPkt = IGMP_ZERO;
            pIfNode->u4IgmpSocketErr = IGMP_ZERO;
        }
        else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            pIfNode->u4MldRxv1Reports = IGMP_ZERO;
            pIfNode->u4MldRxv2Reports = IGMP_ZERO;
            pIfNode->u4MldTxv1Reports = IGMP_ZERO;
            pIfNode->u4MldTxv2Reports = IGMP_ZERO;
            pIfNode->u4MldInSSMPkts = IGMP_ZERO;
            pIfNode->u4MldInvalidSSMPkts = IGMP_ZERO;
            pIfNode->u4MldMalformedPkt = IGMP_ZERO;
            pIfNode->u4MldSocketErr = IGMP_ZERO;
        }
    }
    /* Inform PIM about igmp/mld disable in interface */
    IgmpHandleIfProtocolDownStatus (pIfNode->u4IfIndex, pIfNode->u1AddrType);
#ifdef MFWD_WANTED
    if (gIgmpConfig.i4ProxyStatus == IGMP_ENABLE)
    {
        IgpMfwdDelInterface (pIfNode->u4IfIndex);
    }
#endif
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpIfaceDown()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpSetInterfaceAdminStatus                     */
/*                                                                         */
/*     Description   :    This function is called to set the status of     */
/*                        the interface running IGMP to a particular state.*/
/*     Input(s)      :    Port for which the status is to be set, state.   */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :   None                                              */
/*                                                                         */
/***************************************************************************/
INT1
IgmpSetInterfaceAdminStatus (tIgmpIface * pIfNode, UINT1 u1Status)
{
#if defined LNXIP4_WANTED && !defined NP_KERNEL_WANTED
    tNetIpMcastInfo     NetIpMcastInfo;
    UINT4               u4McStatus;
#endif
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpSetInterfaceAdminStatus()\r \n");
    if (u1Status == IGMP_IFACE_UP)
    {
        IgmpIfaceUp (pIfNode);
        IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "status of Igmp Interface %d made up\n",
                       pIfNode->u4IfIndex);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                   "status of Igmp Interface %d made up\n", pIfNode->u4IfIndex);

#if defined LNXIP4_WANTED && !defined NP_KERNEL_WANTED
        u4McStatus = ENABLED;
#endif
    }
    else
    {
        IgmpIfaceDown (pIfNode);
        IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "status of Igmp Interface %d made down \n",
                       pIfNode->u4IfIndex);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                   "status of Igmp Interface %d made down \n",
                   pIfNode->u4IfIndex);

#if defined LNXIP4_WANTED && !defined NP_KERNEL_WANTED
        u4McStatus = DISABLED;
#endif
    }

#if defined LNXIP4_WANTED && !defined NP_KERNEL_WANTED
    /* Enable/Disable Multicast routing on IP interface */
    MEMSET (&NetIpMcastInfo, 0, sizeof (NetIpMcastInfo));
    NetIpMcastInfo.u4IpPort = pIfNode->u4IfIndex;
    NetIpMcastInfo.u1McastProtocol = IGMP_PROTID;
    if (NetIpv4SetMcStatusOnPort (&NetIpMcastInfo, u4McStatus)
        == NETIPV4_FAILURE)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpSetInterfaceAdminStatus()\r \n");
        return IGMP_NOT_OK;
    }
#endif
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpSetInterfaceAdminStatus()\r \n");
    return (IGMP_OK);
}

/*********************************************************************/
/* Function           : IgmpShutDown                                 */
/*                                                                   */
/* Description        : Deletes and frees all IGMP data structures   */
/*                                                                   */
/* Input(s)           : None                                         */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
IgmpShutDown (VOID)
{

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpShutDown()\r \n");
    /* Deleting Hash Table if present */
    if (gIgmpIfInfo.pIfHashTbl != NULL)
    {
        TMO_SLL_Init (&(gIgmpIfInfo.IfGetNextList));
        TMO_HASH_Delete_Table (gIgmpIfInfo.pIfHashTbl, NULL);
        gIgmpIfInfo.pIfHashTbl = NULL;
    }

    gpu4SrcSetArray = NULL;

    /*  Free mempools */
    IgmpSizingMemDeleteMemPools ();

    if (gIgmpTimerListId != IGMP_ZERO)
    {
        if (TmrDeleteTimerList (gIgmpTimerListId) == TMR_FAILURE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG,
                           IGMP_IO_MODULE | IGMP_ALL_MODULES,
                           IGMP_NAME, "Timer list deletion failed !!!\n");
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE,
                           IgmpGetModuleName (IGMP_TMR_MODULE),
                           "Timer list deletion failed !!!\n");
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpShutDown()\r \n");
    return;
}

/***************************************************************************
 * Function Name    :  IgmpInitInterfaceInfo 
 *
 * Description      :  Intializes Igmp Interface structure
 *
 * Global Variables
 * Referred         :  gIgmpInterfaceTbl 
 *                     
 *
 * Global Variables
 * Modified         :  gIgmpInterfaceTbl
 *                    
 *
 * Input (s)        :  None
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

INT1
IgmpInitInterfaceInfo (VOID)
{
    INT1                i1Status = IGMP_OK;
    UINT4               u4HashTableSize = IGMP_ZERO;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpInitInterfaceInfo()\r \n");
    u4HashTableSize = IGMP_MAX_LOGICAL_INTERFACES;

    gIgmpIfInfo.u4HashSize = u4HashTableSize;

    gIgmpIfInfo.pIfHashTbl = TMO_HASH_Create_Table (u4HashTableSize,
                                                    IgmpIfNodeAddCriteria,
                                                    TRUE);
    if (gIgmpIfInfo.pIfHashTbl == NULL)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Failure initialising the Interface Table\n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE,
                       IGMP_NAME, "Failure initialising the Interface Table\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpInitInterfaceInfo()\r \n");
        return IGMP_NOT_OK;
    }
    TMO_SLL_Init (&gIgmpIfInfo.IfGetNextList);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpInitInterfaceInfo()\r \n");
    return (i1Status);

}

/***********************************************************************
* Function Name             : IgmpCreateInterfaceNode
*                                                                          
* Description        : The function creates an interface node   
*                      It  allocates memory for the interface node and 
*                       stores the default settings.
* Input (s)                 : u4Ifindex.
*                                                                          
* Output (s)                : None
*                                                                          
* Global Variables Referred : None
*                                                                          
* Global Variables Modified : None  
*                                                                          
* Returns                   : IGMP_OK / IGMP_NOT_OK
*                                           
***********************************************************************/

tIgmpIface         *
IgmpCreateInterfaceNode (UINT4 u4IfIndex, UINT1 u1AddrType, UINT1 u1IfStatus)
{
    tIgmpIface         *pIfNode = NULL;
    tIgmpQuery         *pIgmp = NULL;
    UINT4               u4HashIndex = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCreateInterfaceNode()\r \n");
    /* Create the Interface Node and initialize the node 
     * with the default value.  */
    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpIfId, pIfNode, tIgmpIface) == NULL)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpCreateInterfaceNode()\r \n");
        return NULL;
    }
    /* Add a new Query info block. */
    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQueryId, pIgmp, tIgmpQuery) == NULL)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME, "Mem Pool Creation Failure . \n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IgmpGetModuleName (IGMP_OS_RES_MODULE),
                       "Mem Pool Creation Failure . \n");
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                         IGMP_OS_RES_MODULE, IGMP_NAME,
                         IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
                         "for u4IfIndex %d\r\n", u4IfIndex);
        /* Delink and release this interface from the Interface list in
         * Instance structure
         */
        /* Now Release the InterfaceNode */
        IgmpMemRelease (gIgmpMemPool.IgmpIfId, (UINT1 *) pIfNode);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpCreateInterfaceNode()\r \n");
        return NULL;
    }

    /* Memory Block for the Interface node allocated successfully.
     * Lets proceed with the initialisation and add the newly 
     * created interface node into the interface table.
     */
    IgmpFillMem (pIfNode, IGMP_ZERO, sizeof (tIgmpIface));
    IgmpFillMem (pIgmp, IGMP_ZERO, sizeof (tIgmpQuery));

    /* Initialise and add the Interface node to the Interface list */
    pIfNode->pIfaceQry = pIgmp;
    IgmpUtilInitInterface (u4IfIndex, u1AddrType, pIfNode);

    pIfNode->u1EntryStatus = u1IfStatus;
    if (pIfNode->u1EntryStatus == IGMP_ACTIVE)
    {
        pIfNode->u1AdminStatus = IGMP_IFACE_ADMIN_UP;
    }
    if ((pIfNode->u1AdminStatus == IGMP_IFACE_ADMIN_UP) &&
        (pIfNode->u1OperStatus == IGMP_IFACE_OPER_UP))
    {
        pIfNode->u1IfStatus = IGMP_IFACE_UP;
    }

    IGMP_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gIgmpIfInfo.pIfHashTbl, &pIfNode->IfHashLink,
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCreateInterfaceNode()\r \n");
    return pIfNode;

}

/****************************************************************************
 * Function Name         : IgmpUtilInitInterface                *
 *                                        *
 * Description             : Sets the default value for the Interface node*
 *                                        *
 * Input (s)             : u4IfIndex - Interface Index            *
 *                                        *
 * Output (s)             : pIfNode - Pointer to the Interface Node        *
 *                                        *
 * Global Variables Referred : None                        *
 *                                        *
 * Global Variables Modified : None                        *
 *                                        *
 * Returns             : None                        *
 ****************************************************************************/

VOID
IgmpUtilInitInterface (UINT4 u4IfIndex, UINT1 u1AddrType, tIgmpIface * pIfNode)
{
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pPrevSllNode = NULL;
    tIgmpIface         *pCurIfNode = NULL;
    tNetIpv4IfInfo      IpIfRecord;
    tNetIpv6IfInfo      Ip6Info;
    INT4                i4RetValue = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT2               u2VlanId = 0;

    UNUSED_PARAM (i4RetValue);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpUtilInitInterface()\r \n");
    TMO_SLL_Init_Node (&(pIfNode->IfGetNextLink));
    pIfNode->u2QueryInterval = IGMP_DEFAULT_QUERY_INTERVAL;
    pIfNode->u2ConfiguredQueryInterval = IGMP_DEFAULT_QUERY_INTERVAL;

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        pIfNode->u2MaxRespTime = IGMP_DEFAULT_MAX_RESP_TIME;
        pIfNode->u4LastMemberQueryIntvl = IGMP_DEFAULT_LAST_MEMBER_QIVAL;
    }
    else if (u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        pIfNode->u2MaxRespTime = MLD_DEFAULT_MAX_RESP_TIME;
        pIfNode->u4LastMemberQueryIntvl = MLD_DEFAULT_LAST_MEMBER_QIVAL;
    }

    pIfNode->u4IfIndex = u4IfIndex;
    pIfNode->u1AddrType = u1AddrType;

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        pIfNode->u1Version = (UINT1) IGMP_VERSION_2;

        i4RetValue = IGMP_IP_GET_IF_CFG_RECORD (u4IfIndex, &IpIfRecord);
        IGMP_IPVX_ADDR_INIT_IPV4 (pIfNode->u4IfAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &(IpIfRecord.u4Addr));
        IPV4_MASK_TO_MASKLENX (pIfNode->u4IfMask, IpIfRecord.u4NetMask);
        pIfNode->u4Mtu = IpIfRecord.u4Mtu;
        pIfNode->u1OperStatus = (UINT1) IpIfRecord.u4Oper;

        IGMP_IP_GET_IFINDEX_FROM_PORT (u4IfIndex, &u4CfaIfIndex);
        pIfNode->u4IgmpJoinPktRate = IGMP_DEFAULT_INT_RATE_LIMIT;

    }
    else
    {
        pIfNode->u1Version = (UINT1) MLD_VERSION_1;

        MLD_IP6_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex, &u4CfaIfIndex);

        if (MLD_IP6_GET_IF_CONFIG_RECORD (u4CfaIfIndex, &Ip6Info) ==
            OSIX_FAILURE)
        {
            return;
        }
        IGMP_IPVX_ADDR_INIT_IPV6 ((pIfNode->u4IfAddr), IPVX_ADDR_FMLY_IPV6,
                                  (UINT1 *) Ip6Info.Ip6Addr.u1_addr);
        pIfNode->u4IfMask = IP6_ADDR_MAX_PREFIX / 8;
        pIfNode->u1OperStatus = (UINT1) Ip6Info.u4Oper;
        pIfNode->u4Mtu = Ip6Info.u4Mtu;
        pIfNode->u4MldJoinPktRate = IGMP_DEFAULT_INT_RATE_LIMIT;

    }

    if (CfaGetVlanId (u4CfaIfIndex, &u2VlanId) == CFA_SUCCESS)
    {
        pIfNode->u2VlanId = u2VlanId;
    }

    pIfNode->u1Robustness = IGMP_DEFAULT_ROB_VARIABLE;
    pIfNode->u4WrongVerQueries = IGMP_ZERO;
    pIfNode->u4InterfaceGroups = IGMP_ZERO;
    pIfNode->u4ProcessedJoins = IGMP_ZERO;
    pIfNode->u4OutQueries = IGMP_ZERO;
    pIfNode->u4InQueries = IGMP_ZERO;
    pIfNode->u1EntryStatus = IGMP_NOT_IN_SERVICE;
    pIfNode->u1AdminStatus = IGMP_IFACE_ADMIN_DOWN;
    pIfNode->u1FastLeaveFlg = IGMP_FAST_LEAVE_DISABLE;
    pIfNode->u1IfStatus = IGMP_DOWN;

    /* Initialize Query info block. */
    pIfNode->pIfaceQry->pIfNode = pIfNode;
    pIfNode->pIfaceQry->u1Querier = TRUE;
    IGMP_IPVX_ADDR_CLEAR (&(pIfNode->pIfaceQry->u4QuerierAddr));
    pIfNode->pIfaceQry->u1QRV = IGMP_ZERO;
    pIfNode->pIfaceQry->u1QQIC = IGMP_ZERO;
    pIfNode->pIfaceQry->u1StartUpQCount = IGMP_ZERO;
    pIfNode->pIfaceQry->u4IfIndex = u4IfIndex;
    /* Initialise the DB Node for Query */
    IgmpRedDbNodeInit (&(pIfNode->pIfaceQry->QuerierDbNode),
                       IGMP_RED_QUERY_INFO);
    /* Initialise the schedule query list in the hash node */
    TMO_SLL_Init (&pIfNode->pIfaceQry->schQueryList);

    TMO_SLL_Scan ((&gIgmpIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pCurIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pSllNode);
        if (pCurIfNode->u4IfIndex > pIfNode->u4IfIndex)
        {
            break;
        }
        else if (pCurIfNode->u4IfIndex == pIfNode->u4IfIndex)
        {
            if (pCurIfNode->u1AddrType > pIfNode->u1AddrType)
            {
                break;
            }
        }

        pPrevSllNode = pSllNode;
    }
    TMO_SLL_Insert (&gIgmpIfInfo.IfGetNextList, pPrevSllNode,
                    (tTMO_SLL_NODE *) & pIfNode->IfGetNextLink);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpUtilInitInterface()\r \n");
    return;
}

/****************************************************************************
 * FUNction Name         : IgmpGetInterfaceNode
 *                                        
 * Description           : This function does the following -       
 *                         Searches for the interface node in the interface
 *                         table and returns the interface node if present
 *                         
 *                                        
 * Input (s)             : u4IfIndex - The Index of the interface that is to
 *                         searched
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : Interface node if present
 *                         NULL if not present
 ****************************************************************************/

tIgmpIface         *
IgmpGetInterfaceNode (UINT4 u4IfIndex, UINT1 u1AddrType)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4HashIndex = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpGetInterfaceNode()\r \n");
    IGMP_GET_IF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Scan_Bucket (gIgmpIfInfo.pIfHashTbl, u4HashIndex, pIfaceNode,
                          tIgmpIface *)
    {
        if ((pIfaceNode->u4IfIndex == u4IfIndex) &&
            (pIfaceNode->u1AddrType == u1AddrType))
        {
            break;
        }
        else if (pIfaceNode->u4IfIndex > u4IfIndex)
        {
            pIfaceNode = NULL;
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Interface for index %d not found \n", u4IfIndex);
            break;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpGetInterfaceNode()\r \n");
    return pIfaceNode;
}

/****************************************************************************
 * Function Name         : IgmpIfNodeAddCriteria
 *                                        
 * Description           : This function is the call back function provided
 *                         to FSAP2 for inserting the interface node bucket 
 *                         list of the hash table in the assending order.
 *                         
 *                                        
 * Input (s)             : pCurNode - The Current node in the hash table with 
 *                         which the Interface Index is to be compared.
 *                         pu1IfIndex - The index of the interface that is to
 *                         be newly added to the bucket.
 *                                       
 * Output (s)            : None        
 *                                     
 * Global Variables 
 * Referred              : None   
 *                                   
 * Global Variables 
 * Modified              : None 
 *                                 
 * Returns               : return INSERT_PRIORTO if the current node ifindex
 *                         value is greater than the value to be inserted.
 *                         returns MATCH_NOT_FOUND other wise
 ****************************************************************************/

UINT4
IgmpIfNodeAddCriteria (tTMO_HASH_NODE * pCurNode, UINT1 *pu1IfIndex)
{
    tIgmpIface         *pIfNode = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpIfNodeAddCriteria()\r \n");
    pIfNode = (tIgmpIface *) pCurNode;
    if (pIfNode->u4IfIndex > (*((UINT4 *) (VOID *) pu1IfIndex)))
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpIfNodeAddCriteria()\r \n");
        return INSERT_PRIORTO;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpIfNodeAddCriteria()\r \n");
    return MATCH_NOT_FOUND;
}

/**********************************************************************
 * Function Name    :  IgmpDeleteInterfaceNode
 *
 * Description      : This function releases  the Interface node from Interface
 *                Table. 
 * Input(s)         :  u4Ifindex - cid
 *                     u1InfRelease - falg indicating memory release.
 * Output(s)        :  None.
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  
 *
 * Return(s)        :  
 ***********************************************************************/

INT4
IgmpDeleteInterfaceNode (tIgmpIface * pIfaceNode)
{
    UINT4               u4HashIndex = IGMP_ZERO;
    tIgmpQuery         *pIgmpQryNode = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpDeleteInterfaceNode()\r \n");
    IgmpIfaceDown (pIfaceNode);

    /* Delink and release this interface from the Interface list in
     * Instance structure
     */
    TMO_SLL_Delete (&(gIgmpIfInfo.IfGetNextList), &pIfaceNode->IfGetNextLink);

    IGMP_GET_IF_HASHINDEX (pIfaceNode->u4IfIndex, u4HashIndex);
    TMO_HASH_Delete_Node (gIgmpIfInfo.pIfHashTbl,
                          &pIfaceNode->IfHashLink, u4HashIndex);

    /* Now Release the InterfaceNode */
    pIgmpQryNode = pIfaceNode->pIfaceQry;
    IgmpMemRelease (gIgmpMemPool.IgmpQueryId, (UINT1 *) pIgmpQryNode);
    IgmpMemRelease (gIgmpMemPool.IgmpIfId, (UINT1 *) pIfaceNode);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpDeleteInterfaceNode()\r \n");
    return IGMP_OK;
}
