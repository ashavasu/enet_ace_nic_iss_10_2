/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpnpwr.c,v 1.2 2015/02/13 11:13:52 siva Exp $
 *
 * Description: This file contains the NP wrappers for IGMP module.
 *****************************************************************************/

#ifndef __IGMP_NPWR_C__
#define __IGMP_NPWR_C__

#include "nputil.h"
#include "igmpinc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : IgmpNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIgmpNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
IgmpNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIgmpNpModInfo     *pIgmpNpModInfo = NULL;
    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIgmpNpModInfo = &(pFsHwNp->IgmpNpModInfo);

    if (NULL == pIgmpNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_IGMP_HW_ENABLE_IGMP:
        {
            u1RetVal = FsIgmpHwEnableIgmp ();
            break;
        }
        case FS_IGMP_HW_DISABLE_IGMP:
        {
            u1RetVal = FsIgmpHwDisableIgmp ();
            break;
        }
        case FS_NP_IPV4_SET_IGMP_IFACE_JOIN_RATE:
        {
            tIgmpNpWrFsNpIpv4SetIgmpIfaceJoinRate *pEntry = NULL;
            pEntry = &pIgmpNpModInfo->IgmpNpFsNpIpv4SetIgmpIfaceJoinRate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4SetIgmpIfaceJoinRate (pEntry->i4IfIndex,
                                              pEntry->i4RateLimit);
            break;
        }
#ifdef MBSM_WANTED
        case FS_IGMP_MBSM_HW_ENABLE_IGMP:
        {
            tIgmpNpWrFsIgmpMbsmHwEnableIgmp *pEntry = NULL;
            pEntry = &pIgmpNpModInfo->IgmpNpFsIgmpMbsmHwEnableIgmp;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsIgmpMbsmHwEnableIgmp (pEntry->pSlotInfo);
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */
    return (u1RetVal);
}
#endif
