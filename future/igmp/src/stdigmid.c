/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdigmid.c,v 1.3 2008/08/20 15:05:06 iss Exp $
 *
 * Description: Mid level files
 *
 *******************************************************************/

# include  "include.h"
# include  "stdigmid.h"
# include  "stdiglow.h"
# include  "stdigcon.h"
# include  "stdigogi.h"
# include  "extern.h"
# include  "midconst.h"
#include "stdigmcli.h"
#include "fsigmpcli.h"
/****************************************************************************
 Function   : igmpInterfaceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
igmpInterfaceEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_igmpInterfaceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_igmpInterfaceIfIndex = FALSE;
    INT4                i4_next_igmpInterfaceIfIndex = FALSE;

    UINT4               u4_addr_ret_val_igmpInterfaceQuerier;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_igmpInterfaceTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_igmpInterfaceTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_igmpInterfaceIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceIgmpInterfaceTable
                     (i4_igmpInterfaceIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_igmpInterfaceIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexIgmpInterfaceTable
                     (&i4_igmpInterfaceIfIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_igmpInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_igmpInterfaceIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexIgmpInterfaceTable (i4_igmpInterfaceIfIndex,
                                                        &i4_next_igmpInterfaceIfIndex))
                    == SNMP_SUCCESS)
                {
                    i4_igmpInterfaceIfIndex = i4_next_igmpInterfaceIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_igmpInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case IGMPINTERFACEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_igmpInterfaceIfIndex;
            }
            else
            {
                i4_return_val = i4_next_igmpInterfaceIfIndex;
            }
            break;
        }
        case IGMPINTERFACEQUERYINTERVAL:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceQueryInterval (i4_igmpInterfaceIfIndex,
                                                  &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACESTATUS:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceStatus (i4_igmpInterfaceIfIndex,
                                           &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEVERSION:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceVersion (i4_igmpInterfaceIfIndex,
                                            &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEQUERIER:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceQuerier (i4_igmpInterfaceIfIndex,
                                            &u4_addr_ret_val_igmpInterfaceQuerier);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_igmpInterfaceQuerier);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEQUERYMAXRESPONSETIME:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceQueryMaxResponseTime
                (i4_igmpInterfaceIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEQUERIERUPTIME:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceQuerierUpTime (i4_igmpInterfaceIfIndex,
                                                  &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEQUERIEREXPIRYTIME:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceQuerierExpiryTime (i4_igmpInterfaceIfIndex,
                                                      &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEVERSION1QUERIERTIMER:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceVersion1QuerierTimer
                (i4_igmpInterfaceIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEWRONGVERSIONQUERIES:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceWrongVersionQueries (i4_igmpInterfaceIfIndex,
                                                        &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEJOINS:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceJoins (i4_igmpInterfaceIfIndex,
                                          &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEPROXYIFINDEX:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceProxyIfIndex (i4_igmpInterfaceIfIndex,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEGROUPS:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceGroups (i4_igmpInterfaceIfIndex,
                                           &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_GAUGE32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEROBUSTNESS:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceRobustness (i4_igmpInterfaceIfIndex,
                                               &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACELASTMEMBQUERYINTVL:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceLastMembQueryIntvl (i4_igmpInterfaceIfIndex,
                                                       &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_UNSIGNED32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPINTERFACEVERSION2QUERIERTIMER:
        {
            i1_ret_val =
                nmhGetIgmpInterfaceVersion2QuerierTimer
                (i4_igmpInterfaceIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : igmpInterfaceEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
igmpInterfaceEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_igmpInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_igmpInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case IGMPINTERFACEQUERYINTERVAL:
        {
            i1_ret_val =
                nmhSetIgmpInterfaceQueryInterval (i4_igmpInterfaceIfIndex,
                                                  p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IGMPINTERFACESTATUS:
        {
            i1_ret_val =
                nmhSetIgmpInterfaceStatus (i4_igmpInterfaceIfIndex,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IGMPINTERFACEVERSION:
        {
            i1_ret_val =
                nmhSetIgmpInterfaceVersion (i4_igmpInterfaceIfIndex,
                                            p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IGMPINTERFACEQUERYMAXRESPONSETIME:
        {
            i1_ret_val =
                nmhSetIgmpInterfaceQueryMaxResponseTime
                (i4_igmpInterfaceIfIndex, p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IGMPINTERFACEPROXYIFINDEX:
        {
            i1_ret_val =
                nmhSetIgmpInterfaceProxyIfIndex (i4_igmpInterfaceIfIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IGMPINTERFACEROBUSTNESS:
        {
            i1_ret_val =
                nmhSetIgmpInterfaceRobustness (i4_igmpInterfaceIfIndex,
                                               p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IGMPINTERFACELASTMEMBQUERYINTVL:
        {
            i1_ret_val =
                nmhSetIgmpInterfaceLastMembQueryIntvl (i4_igmpInterfaceIfIndex,
                                                       p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case IGMPINTERFACEIFINDEX:
            /*  Read Only Variables. */
        case IGMPINTERFACEQUERIER:
            /*  Read Only Variables. */
        case IGMPINTERFACEQUERIERUPTIME:
            /*  Read Only Variables. */
        case IGMPINTERFACEQUERIEREXPIRYTIME:
            /*  Read Only Variables. */
        case IGMPINTERFACEVERSION1QUERIERTIMER:
            /*  Read Only Variables. */
        case IGMPINTERFACEWRONGVERSIONQUERIES:
            /*  Read Only Variables. */
        case IGMPINTERFACEJOINS:
            /*  Read Only Variables. */
        case IGMPINTERFACEGROUPS:
            /*  Read Only Variables. */
        case IGMPINTERFACEVERSION2QUERIERTIMER:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : igmpInterfaceEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
igmpInterfaceEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_igmpInterfaceIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_igmpInterfaceIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceIgmpInterfaceTable(i4_igmpInterfaceIfIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case IGMPINTERFACEQUERYINTERVAL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpInterfaceQueryInterval (&u4ErrorCode,
                                                     i4_igmpInterfaceIfIndex,
                                                     p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IGMPINTERFACESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpInterfaceStatus (&u4ErrorCode,
                                              i4_igmpInterfaceIfIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IGMPINTERFACEVERSION:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpInterfaceVersion (&u4ErrorCode,
                                               i4_igmpInterfaceIfIndex,
                                               p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IGMPINTERFACEQUERYMAXRESPONSETIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpInterfaceQueryMaxResponseTime (&u4ErrorCode,
                                                            i4_igmpInterfaceIfIndex,
                                                            p_value->
                                                            u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IGMPINTERFACEPROXYIFINDEX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpInterfaceProxyIfIndex (&u4ErrorCode,
                                                    i4_igmpInterfaceIfIndex,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IGMPINTERFACEROBUSTNESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpInterfaceRobustness (&u4ErrorCode,
                                                  i4_igmpInterfaceIfIndex,
                                                  p_value->u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IGMPINTERFACELASTMEMBQUERYINTVL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_UNSIGNED32)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpInterfaceLastMembQueryIntvl (&u4ErrorCode,
                                                          i4_igmpInterfaceIfIndex,
                                                          p_value->
                                                          u4_ULongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case IGMPINTERFACEIFINDEX:
        case IGMPINTERFACEQUERIER:
        case IGMPINTERFACEQUERIERUPTIME:
        case IGMPINTERFACEQUERIEREXPIRYTIME:
        case IGMPINTERFACEVERSION1QUERIERTIMER:
        case IGMPINTERFACEWRONGVERSIONQUERIES:
        case IGMPINTERFACEJOINS:
        case IGMPINTERFACEGROUPS:
        case IGMPINTERFACEVERSION2QUERIERTIMER:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : igmpCacheEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
igmpCacheEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                   UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_igmpCacheTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpCacheAddress = FALSE;
    UINT4               u4_addr_next_igmpCacheAddress = FALSE;
    UINT1               u1_addr_igmpCacheAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_igmpCacheAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_igmpCacheIfIndex = FALSE;
    INT4                i4_next_igmpCacheIfIndex = FALSE;

    UINT4               u4_addr_ret_val_igmpCacheLastReporter;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_igmpCacheTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + INTEGER_LEN;

            if (LEN_igmpCacheTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_igmpCacheAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_igmpCacheAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpCacheAddress)));

                /* Extracting The Integer Index. */
                i4_igmpCacheIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceIgmpCacheTable
                     (u4_addr_igmpCacheAddress,
                      i4_igmpCacheIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_igmpCacheAddress[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_igmpCacheIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexIgmpCacheTable (&u4_addr_igmpCacheAddress,
                                                     &i4_igmpCacheIfIndex)) ==
                    SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_igmpCacheAddress)) =
                        OSIX_HTONL (u4_addr_igmpCacheAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_igmpCacheAddress[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_igmpCacheIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_igmpCacheAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_igmpCacheAddress =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpCacheAddress)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_igmpCacheIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexIgmpCacheTable (u4_addr_igmpCacheAddress,
                                                    &u4_addr_next_igmpCacheAddress,
                                                    i4_igmpCacheIfIndex,
                                                    &i4_next_igmpCacheIfIndex))
                    == SNMP_SUCCESS)
                {
                    u4_addr_igmpCacheAddress = u4_addr_next_igmpCacheAddress;
                    *((UINT4 *) (u1_addr_next_igmpCacheAddress)) =
                        OSIX_HTONL (u4_addr_igmpCacheAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_igmpCacheAddress[i4_count];
                    i4_igmpCacheIfIndex = i4_next_igmpCacheIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_igmpCacheIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case IGMPCACHEADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_igmpCacheAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_igmpCacheAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IGMPCACHEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_igmpCacheIfIndex;
            }
            else
            {
                i4_return_val = i4_next_igmpCacheIfIndex;
            }
            break;
        }
        case IGMPCACHESELF:
        {
            i1_ret_val =
                nmhGetIgmpCacheSelf (u4_addr_igmpCacheAddress,
                                     i4_igmpCacheIfIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPCACHELASTREPORTER:
        {
            i1_ret_val =
                nmhGetIgmpCacheLastReporter (u4_addr_igmpCacheAddress,
                                             i4_igmpCacheIfIndex,
                                             &u4_addr_ret_val_igmpCacheLastReporter);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                /* This part of the Code converts the ADDR to Octet String. */
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_ret_val_igmpCacheLastReporter);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPCACHEUPTIME:
        {
            i1_ret_val =
                nmhGetIgmpCacheUpTime (u4_addr_igmpCacheAddress,
                                       i4_igmpCacheIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPCACHEEXPIRYTIME:
        {
            i1_ret_val =
                nmhGetIgmpCacheExpiryTime (u4_addr_igmpCacheAddress,
                                           i4_igmpCacheIfIndex,
                                           &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPCACHESTATUS:
        {
            i1_ret_val =
                nmhGetIgmpCacheStatus (u4_addr_igmpCacheAddress,
                                       i4_igmpCacheIfIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPCACHEVERSION1HOSTTIMER:
        {
            i1_ret_val =
                nmhGetIgmpCacheVersion1HostTimer (u4_addr_igmpCacheAddress,
                                                  i4_igmpCacheIfIndex,
                                                  &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPCACHEVERSION2HOSTTIMER:
        {
            i1_ret_val =
                nmhGetIgmpCacheVersion2HostTimer (u4_addr_igmpCacheAddress,
                                                  i4_igmpCacheIfIndex,
                                                  &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_TIME_TICKS;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case IGMPCACHESOURCEFILTERMODE:
        {
            i1_ret_val =
                nmhGetIgmpCacheSourceFilterMode (u4_addr_igmpCacheAddress,
                                                 i4_igmpCacheIfIndex,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : igmpCacheEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
igmpCacheEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                   UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpCacheAddress = FALSE;
    UINT1               u1_addr_igmpCacheAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_igmpCacheIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_igmpCacheAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_igmpCacheAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpCacheAddress)));

        /* Extracting The Integer Index. */
        i4_igmpCacheIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case IGMPCACHESELF:
        {
            i1_ret_val =
                nmhSetIgmpCacheSelf (u4_addr_igmpCacheAddress,
                                     i4_igmpCacheIfIndex,
                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case IGMPCACHESTATUS:
        {
            i1_ret_val =
                nmhSetIgmpCacheStatus (u4_addr_igmpCacheAddress,
                                       i4_igmpCacheIfIndex,
                                       p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case IGMPCACHEADDRESS:
            /*  Read Only Variables. */
        case IGMPCACHEIFINDEX:
            /*  Read Only Variables. */
        case IGMPCACHELASTREPORTER:
            /*  Read Only Variables. */
        case IGMPCACHEUPTIME:
            /*  Read Only Variables. */
        case IGMPCACHEEXPIRYTIME:
            /*  Read Only Variables. */
        case IGMPCACHEVERSION1HOSTTIMER:
            /*  Read Only Variables. */
        case IGMPCACHEVERSION2HOSTTIMER:
            /*  Read Only Variables. */
        case IGMPCACHESOURCEFILTERMODE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : igmpCacheEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
igmpCacheEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                    UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpCacheAddress = FALSE;
    UINT1               u1_addr_igmpCacheAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_igmpCacheIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_igmpCacheAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_igmpCacheAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpCacheAddress)));

        /* Extracting The Integer Index. */
        i4_igmpCacheIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceIgmpCacheTable(u4_addr_igmpCacheAddress , i4_igmpCacheIfIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case IGMPCACHESELF:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpCacheSelf (&u4ErrorCode, u4_addr_igmpCacheAddress,
                                        i4_igmpCacheIfIndex,
                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case IGMPCACHESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpCacheStatus (&u4ErrorCode,
                                          u4_addr_igmpCacheAddress,
                                          i4_igmpCacheIfIndex,
                                          p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case IGMPCACHEADDRESS:
        case IGMPCACHEIFINDEX:
        case IGMPCACHELASTREPORTER:
        case IGMPCACHEUPTIME:
        case IGMPCACHEEXPIRYTIME:
        case IGMPCACHEVERSION1HOSTTIMER:
        case IGMPCACHEVERSION2HOSTTIMER:
        case IGMPCACHESOURCEFILTERMODE:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : igmpSrcListEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
igmpSrcListEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_igmpSrcListTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpSrcListAddress = FALSE;
    UINT4               u4_addr_next_igmpSrcListAddress = FALSE;
    UINT1               u1_addr_igmpSrcListAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_igmpSrcListAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_igmpSrcListIfIndex = FALSE;
    INT4                i4_next_igmpSrcListIfIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpSrcListHostAddress = FALSE;
    UINT4               u4_addr_next_igmpSrcListHostAddress = FALSE;
    UINT1               u1_addr_igmpSrcListHostAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_igmpSrcListHostAddress[ADDR_LEN] =
        NULL_STRING;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += ADDR_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_igmpSrcListTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + INTEGER_LEN + ADDR_LEN;

            if (LEN_igmpSrcListTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_igmpSrcListAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_igmpSrcListAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpSrcListAddress)));

                /* Extracting The Integer Index. */
                i4_igmpSrcListIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_igmpSrcListHostAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_igmpSrcListHostAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpSrcListHostAddress)));

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceIgmpSrcListTable
                     (u4_addr_igmpSrcListAddress, i4_igmpSrcListIfIndex,
                      u4_addr_igmpSrcListHostAddress)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_igmpSrcListAddress[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_igmpSrcListIfIndex;
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_igmpSrcListHostAddress[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexIgmpSrcListTable
                     (&u4_addr_igmpSrcListAddress, &i4_igmpSrcListIfIndex,
                      &u4_addr_igmpSrcListHostAddress)) == SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_igmpSrcListAddress)) =
                        OSIX_HTONL (u4_addr_igmpSrcListAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_igmpSrcListAddress[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_igmpSrcListIfIndex;
                    *((UINT4 *) (u1_addr_igmpSrcListHostAddress)) =
                        OSIX_HTONL (u4_addr_igmpSrcListHostAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_igmpSrcListHostAddress[i4_count];

                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_igmpSrcListAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_igmpSrcListAddress =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpSrcListAddress)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_igmpSrcListIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_igmpSrcListHostAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_igmpSrcListHostAddress =
                        OSIX_NTOHL (*
                                    ((UINT4
                                      *) (u1_addr_igmpSrcListHostAddress)));

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexIgmpSrcListTable
                     (u4_addr_igmpSrcListAddress,
                      &u4_addr_next_igmpSrcListAddress, i4_igmpSrcListIfIndex,
                      &i4_next_igmpSrcListIfIndex,
                      u4_addr_igmpSrcListHostAddress,
                      &u4_addr_next_igmpSrcListHostAddress)) == SNMP_SUCCESS)
                {
                    u4_addr_igmpSrcListAddress =
                        u4_addr_next_igmpSrcListAddress;
                    *((UINT4 *) (u1_addr_next_igmpSrcListAddress)) =
                        OSIX_HTONL (u4_addr_igmpSrcListAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_igmpSrcListAddress[i4_count];
                    i4_igmpSrcListIfIndex = i4_next_igmpSrcListIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_igmpSrcListIfIndex;
                    u4_addr_igmpSrcListHostAddress =
                        u4_addr_next_igmpSrcListHostAddress;
                    *((UINT4 *) (u1_addr_next_igmpSrcListHostAddress)) =
                        OSIX_HTONL (u4_addr_igmpSrcListHostAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4)
                            u1_addr_next_igmpSrcListHostAddress[i4_count];
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case IGMPSRCLISTADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_igmpSrcListAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_igmpSrcListAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IGMPSRCLISTIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_igmpSrcListIfIndex;
            }
            else
            {
                i4_return_val = i4_next_igmpSrcListIfIndex;
            }
            break;
        }
        case IGMPSRCLISTHOSTADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_igmpSrcListHostAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_igmpSrcListHostAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case IGMPSRCLISTSTATUS:
        {
            i1_ret_val =
                nmhGetIgmpSrcListStatus (u4_addr_igmpSrcListAddress,
                                         i4_igmpSrcListIfIndex,
                                         u4_addr_igmpSrcListHostAddress,
                                         &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : igmpSrcListEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
igmpSrcListEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpSrcListAddress = FALSE;
    UINT1               u1_addr_igmpSrcListAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_igmpSrcListIfIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpSrcListHostAddress = FALSE;
    UINT1               u1_addr_igmpSrcListHostAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += ADDR_LEN;
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_igmpSrcListAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_igmpSrcListAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpSrcListAddress)));

        /* Extracting The Integer Index. */
        i4_igmpSrcListIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_igmpSrcListHostAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_igmpSrcListHostAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpSrcListHostAddress)));

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case IGMPSRCLISTSTATUS:
        {
            i1_ret_val =
                nmhSetIgmpSrcListStatus (u4_addr_igmpSrcListAddress,
                                         i4_igmpSrcListIfIndex,
                                         u4_addr_igmpSrcListHostAddress,
                                         p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case IGMPSRCLISTADDRESS:
            /*  Read Only Variables. */
        case IGMPSRCLISTIFINDEX:
            /*  Read Only Variables. */
        case IGMPSRCLISTHOSTADDRESS:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : igmpSrcListEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
igmpSrcListEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpSrcListAddress = FALSE;
    UINT1               u1_addr_igmpSrcListAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_igmpSrcListIfIndex = FALSE;

    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_igmpSrcListHostAddress = FALSE;
    UINT1               u1_addr_igmpSrcListHostAddress[ADDR_LEN] = NULL_STRING;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += ADDR_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += ADDR_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_igmpSrcListAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_igmpSrcListAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpSrcListAddress)));

        /* Extracting The Integer Index. */
        i4_igmpSrcListIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /*  FOR Loop for extracting the Index of Type Address From Given OID. */
        for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++, i4_offset++)
            u1_addr_igmpSrcListHostAddress[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
        u4_addr_igmpSrcListHostAddress =
            OSIX_NTOHL (*((UINT4 *) (u1_addr_igmpSrcListHostAddress)));

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceIgmpSrcListTable(u4_addr_igmpSrcListAddress , i4_igmpSrcListIfIndex , u4_addr_igmpSrcListHostAddress)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case IGMPSRCLISTSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2IgmpSrcListStatus (&u4ErrorCode,
                                            u4_addr_igmpSrcListAddress,
                                            i4_igmpSrcListIfIndex,
                                            u4_addr_igmpSrcListHostAddress,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case IGMPSRCLISTADDRESS:
        case IGMPSRCLISTIFINDEX:
        case IGMPSRCLISTHOSTADDRESS:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
