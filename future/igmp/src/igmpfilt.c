/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpfilt.c,v 1.29 2017/02/06 10:45:27 siva Exp $
 *
 * Description:This file contains the routines required for     
 *             processing the  received IGMPV3 reports
 *
 *******************************************************************/
#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE ;
#endif

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpGroupHandleIsInclude                        */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type MODE_IS_INCLUDE                     */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGroupHandleIsInclude                    **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpGroupHandleIsInclude (tIgmpIface * pIfNode, tIgmpIPvXAddr u4GrpAddr,
                          UINT4 u4NoOfSources, UINT1 *pu1Sources,
                          tIgmpIPvXAddr u4Reporter)
{
    tIgmpFSMInfo        IgmpFSMInfoNode;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    INT4                i4Status = IGMP_NOT_OK;
    UINT1               u1FSMEventRcvd = IGMP_ZERO;
    UINT1               u1NewInterface = IGMP_NOT_A_NEW_GROUP;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpGroupHandleIsInclude()\r \n");
    IgmpFillMem (&IgmpFSMInfoNode, IGMP_ZERO, sizeof (tIgmpFSMInfo));

    pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
    if (pGrp == NULL)
    {
        pGrp = IgmpAddGroup (pIfNode, u4GrpAddr, IGMP_FALSE);
        if (pGrp == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,IGMP_NAME, 
                      "Group node not created  \n");
            IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
		      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "Group node not created  \n");
            return i4Status;
        }
        u1NewInterface = IGMP_NEW_GROUP;
        pGrp->u1DynamicFlg = IGMP_TRUE;

        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
        IGMP_TRC_ARG2 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, 
		       IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
    }
    else
    {
	if (pGrp->u1DynamicFlg != IGMP_TRUE)
	{
        pGrp->u1DynamicFlg = IGMP_TRUE;
		pGrp->u1DynSyncFlag = IGMP_TRUE;
	}
        if (IgmpApplyLimitForGroup (pIfNode, pGrp) == IGMP_IGNORE)
        {
            pGrp->u1DynamicFlg = IGMP_FALSE;
            return IGMP_OK;
        }
        if ((pGrp->u1V1HostPresent == IGMP_FALSE) &&
            (pGrp->u1V2HostPresent == IGMP_FALSE))
        {
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        }
    }
    IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);

    IgmpFSMInfoNode.pIfNode = pIfNode;
    IgmpFSMInfoNode.u4NoOfSources = u4NoOfSources;
    IgmpFSMInfoNode.pu1Sources = pu1Sources;
    IGMP_IPVX_ADDR_COPY (&(IgmpFSMInfoNode.u4Reporter), &u4Reporter);
    IgmpFSMInfoNode.pGrp = pGrp;

    /* Report Received */
    u1FSMEventRcvd = IGMP_GRP_MODE_IS_INCLUDE;    /*1 */

    /* Do Actions specified in section 6.4.1 */
    i4Status = gaIgmpFSM[pGrp->u1FilterMode][u1FSMEventRcvd] (&IgmpFSMInfoNode);
    if ((u4NoOfSources != IGMP_ZERO) && 
        (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO))
    {
	pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpDeleteGroup (pIfNode, pGrp, pGrp->u1StaticFlg);
        return IGMP_FAILURE;
    }

    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        /* Call the PROXY routine to update the consolidate group
         * membership data base for group registration */
        IgpGrpConsolidate (pGrp, IGMP_VERSION_3, u1NewInterface);
    }
    if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
        (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
    {
        pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
    }
	if(pGrp->u1DynSyncFlag == IGMP_TRUE)
	{
	pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
    	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
		pGrp->u1DynSyncFlag = IGMP_FALSE;
	}
    TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
    {
        if (pSrc->u1DynSyncFlag == IGMP_TRUE)
        {
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
            pSrc->u1DynSyncFlag = IGMP_FALSE;
        }
    }
    IgmpRedSyncDynInfo ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpGroupHandleIsInclude()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpGroupHandleIsExclude                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type MODE_IS_EXCLUDE                     */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGroupHandleIsExclude                    **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpGroupHandleIsExclude (tIgmpIface * pIfNode, tIgmpIPvXAddr u4GrpAddr,
                          UINT4 u4NoOfSources, UINT1 *pu1Sources,
                          tIgmpIPvXAddr u4Reporter, UINT1 u1Consolidate)
{
    tIgmpFSMInfo        IgmpFSMInfoNode;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    INT4                i4Status = IGMP_NOT_OK;
    UINT1               u1FSMEventRcvd = IGMP_ZERO;
    UINT1               u1NewInterface = IGMP_NOT_A_NEW_GROUP;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpGroupHandleIsExclude()\r \n");
    IgmpFillMem (&IgmpFSMInfoNode, IGMP_ZERO, sizeof (tIgmpFSMInfo));

    pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
    if (pGrp == NULL)
    {
        pGrp = IgmpAddGroup (pIfNode, u4GrpAddr, IGMP_FALSE);
        if (pGrp == NULL)
        {      
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,IGMP_NAME, 
                      "Group node not created  \n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, 
		      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "Group node not created  \n");
            return i4Status;
        }
        u1NewInterface = IGMP_NEW_GROUP;
        pGrp->u1DynamicFlg = IGMP_TRUE;

        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
	IGMP_TRC_ARG2 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, 
		       IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
    }
    else
    {
	if (pGrp->u1DynamicFlg != IGMP_TRUE)
	{
        pGrp->u1DynamicFlg = IGMP_TRUE;
		pGrp->u1DynSyncFlag = IGMP_TRUE;
	}
        if (IgmpApplyLimitForGroup (pIfNode, pGrp) == IGMP_IGNORE)
        {
            pGrp->u1DynamicFlg = IGMP_FALSE;
            return IGMP_OK;
        }
        if ((pGrp->u1V1HostPresent == IGMP_FALSE) &&
            (pGrp->u1V2HostPresent == IGMP_FALSE))
        {
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        }
    }
    IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);

    IgmpFSMInfoNode.pIfNode = pIfNode;
    IgmpFSMInfoNode.pGrp = pGrp;
    IgmpFSMInfoNode.u4NoOfSources = u4NoOfSources;
    IgmpFSMInfoNode.pu1Sources = pu1Sources;
    IGMP_IPVX_ADDR_COPY (&(IgmpFSMInfoNode.u4Reporter), &u4Reporter);

    /* Report Received */
    u1FSMEventRcvd = IGMP_GRP_MODE_IS_EXCLUDE;    /*2 */

    /* Do Actions specified in section 6.4.1 */
    i4Status = gaIgmpFSM[pGrp->u1FilterMode][u1FSMEventRcvd] (&IgmpFSMInfoNode);
    if ((u4NoOfSources != IGMP_ZERO) && 
        (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO))
    {
	pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpDeleteGroup (pIfNode, pGrp, pGrp->u1StaticFlg);
        return IGMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) && (u1Consolidate == IGMP_TRUE))
    {
        /* Call the PROXY routine to update the consolidate group
         * membership data base for group registration */
        IgpGrpConsolidate (pGrp, IGMP_VERSION_3, u1NewInterface);
    }

    /* This one is common action, section 6.4.1 
     * Reset the Group Timer  with interval = GMI 
     */
    if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pGrp->Timer);
    }

    IGMPSTARTTIMER (pGrp->Timer, IGMP_GROUP_TIMER_ID,
                    (UINT2) IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);

    IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
	if(pGrp->u1DynSyncFlag == IGMP_TRUE)
	{
	pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
    	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
		pGrp->u1DynSyncFlag = IGMP_FALSE;
	}
    TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
    {
        if (pSrc->u1DynSyncFlag == IGMP_TRUE)
        {
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
            pSrc->u1DynSyncFlag = IGMP_FALSE;
        }
    }
    IgmpRedSyncDynInfo ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpGroupHandleIsExclude()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpGroupHandleToInclude                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type CHANGE_TO_INCLUDE_MODE              */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGroupHandleToInclude                    **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpGroupHandleToInclude (tIgmpIface * pIfNode,
                          tIgmpIPvXAddr u4GrpAddr,
                          UINT4 u4NoOfSources,
                          UINT1 *pu1Sources, tIgmpIPvXAddr u4Reporter)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpFSMInfo        IgmpFSMInfoNode;
    INT4                i4Status = IGMP_NOT_OK;
    UINT1               u1FSMEventRcvd = IGMP_ZERO;
    UINT1               u1NewInterface = IGMP_NOT_A_NEW_GROUP;
    UINT1               u1VerFlag = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpGroupHandleToInclude()\r \n");
    IgmpFillMem (&IgmpFSMInfoNode, IGMP_ZERO, sizeof (tIgmpFSMInfo));

    pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
    if (pGrp == NULL)
    {
        if (u4NoOfSources == IGMP_ZERO)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                      "Include NONE for a new group is invalid\r\n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "Include NONE for a new group is invalid\r\n");
            return IGMP_OK;
        }

        pGrp = IgmpAddGroup (pIfNode, u4GrpAddr, IGMP_FALSE);
        if (pGrp == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                           "Group node not created  \n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "Group node not created  \n");
            return i4Status;
        }
        u1NewInterface = IGMP_NEW_GROUP;
        pGrp->u1DynamicFlg = IGMP_TRUE;

        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
        IGMP_TRC_ARG2 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
    }
    else
    {
        if (u4NoOfSources != IGMP_ZERO)
        {
		if (pGrp->u1DynamicFlg != IGMP_TRUE)
		{
                pGrp->u1DynamicFlg = IGMP_TRUE;
			pGrp->u1DynSyncFlag = IGMP_TRUE;
		}
                if (IgmpApplyLimitForGroup (pIfNode, pGrp) == IGMP_IGNORE)
                {
                    pGrp->u1DynamicFlg = IGMP_FALSE;
                    return IGMP_OK;
                }
        }
        if ((pGrp->u1V1HostPresent == IGMP_FALSE) &&
            (pGrp->u1V2HostPresent == IGMP_FALSE))
        {
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        }
    }
    IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
    if (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV1)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                  "TO_INCLUDE report is ignored!!!!! in GCM=1  \n");
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "TO_INCLUDE report is ignored!!!!! in GCM=1  \n");

        return i4Status;
    }

    IgmpFSMInfoNode.pIfNode = pIfNode;
    IgmpFSMInfoNode.u4NoOfSources = u4NoOfSources;
    IgmpFSMInfoNode.pu1Sources = pu1Sources;
    IGMP_IPVX_ADDR_COPY (&(IgmpFSMInfoNode.u4Reporter), &u4Reporter);
    IgmpFSMInfoNode.pGrp = pGrp;

    /* Report Received */
    u1FSMEventRcvd = IGMP_GRP_CHANGE_TO_INCLUDE_MODE;    /*3 */

    /* Do Actions specified in section 6.4.1 */
    i4Status = gaIgmpFSM[pGrp->u1FilterMode][u1FSMEventRcvd] (&IgmpFSMInfoNode);
    if ((u4NoOfSources != IGMP_ZERO) && 
        (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO))
    {
	pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpDeleteGroup (pIfNode, pGrp, pGrp->u1StaticFlg);
        return IGMP_FAILURE;
    }

    if (((TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO) &&
         (pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)) ||
        ((TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO) &&
         ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
          (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))))
    {
        if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
        {
            u1VerFlag = IGMP_VERSION_2;
            u1VerFlag |= IGMP_V3REP_SEND_FLAG;
        }
        else
        {
            u1VerFlag = IGMP_VERSION_3;
        }

	pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpExpireGroup (pGrp->pIfNode, pGrp, u1VerFlag, IGMP_TRUE, IGMP_FALSE);
    }
    else
    {
        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            if (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV3)
            {
                /* Call the PROXY routine to update the consolidate group
                 * membership data base for group registration */
                IgpGrpConsolidate (pGrp, IGMP_VERSION_3, u1NewInterface);
            }
        }
        if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
            (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
        {
            pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
        }
        IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
		if(pGrp->u1DynSyncFlag == IGMP_TRUE)
		{
	pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
        	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
			pGrp->u1DynSyncFlag = IGMP_FALSE;
		}
        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1DynSyncFlag == IGMP_TRUE)
            {
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
                pSrc->u1DynSyncFlag = IGMP_FALSE;
            }
        }
        IgmpRedSyncDynInfo ();
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpGroupHandleToInclude()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpGroupHandleToExclude                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type CHANGE_TO_EXCLUDE_MODE              */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGroupHandleToExclude                    **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpGroupHandleToExclude (tIgmpIface * pIfNode,
                          tIgmpIPvXAddr u4GrpAddr,
                          UINT4 u4NoOfSources,
                          UINT1 *pu1Sources, tIgmpIPvXAddr u4Reporter)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpFSMInfo        IgmpFSMInfoNode;
    INT4                i4Status = IGMP_NOT_OK;
    UINT1               u1FSMEventRcvd = IGMP_ZERO;
    UINT1               u1NewInterface = IGMP_NOT_A_NEW_GROUP;
    UINT1               u1Consolidate = IGMP_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpGroupHandleToExclude()\r \n");
    IgmpFillMem (&IgmpFSMInfoNode, IGMP_ZERO, sizeof (tIgmpFSMInfo));

    pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
    if (pGrp == NULL)
    {
        pGrp = IgmpAddGroup (pIfNode, u4GrpAddr, IGMP_FALSE);
        if (pGrp == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                      IGMP_NAME, "Group node not created  \n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES,
                      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE), "Group node not created  \n");
            return i4Status;
        }
        u1NewInterface = IGMP_NEW_GROUP;
        pGrp->u1DynamicFlg = IGMP_TRUE;

        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
        IGMP_TRC_ARG2 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
    }
    else
    {
	if (pGrp->u1DynamicFlg != IGMP_TRUE)
	{
        pGrp->u1DynamicFlg = IGMP_TRUE;
		pGrp->u1DynSyncFlag = IGMP_TRUE;
	}
        if (IgmpApplyLimitForGroup (pIfNode, pGrp) == IGMP_IGNORE)
        {
            pGrp->u1DynamicFlg = IGMP_FALSE;
            return IGMP_OK;
        }
        if ((pGrp->u1V1HostPresent == IGMP_FALSE) &&
            (pGrp->u1V2HostPresent == IGMP_FALSE))
        {
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        }
    }
    IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
    if ((pGrp->u4GroupCompMode == IGMP_GCM_IGMPV2) ||
        (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV1))
    {
        u4NoOfSources = IGMP_ZERO;
        pu1Sources = NULL;
    }

    IgmpFSMInfoNode.pIfNode = pIfNode;
    IgmpFSMInfoNode.u4NoOfSources = u4NoOfSources;
    
    /*For coverity workaround */
    if (pu1Sources != NULL)
    {
        IgmpFSMInfoNode.pu1Sources = pu1Sources;
    }
    IGMP_IPVX_ADDR_COPY (&(IgmpFSMInfoNode.u4Reporter), &u4Reporter);
    IgmpFSMInfoNode.pGrp = pGrp;

    /* Report Received */
    u1FSMEventRcvd = IGMP_GRP_CHANGE_TO_EXCLUDE_MODE;    /*4 */
    /* Reception of Current-state records, section 6.4.1 */

    /* Do Actions specified in section 6.4.1 */
    i4Status = gaIgmpFSM[pGrp->u1FilterMode][u1FSMEventRcvd] (&IgmpFSMInfoNode);
    if ((u4NoOfSources != IGMP_ZERO) && 
        (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO))
    {
	pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpDeleteGroup (pIfNode, pGrp, pGrp->u1StaticFlg);
        return IGMP_FAILURE;
    }

    if ((TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO) &&
        ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
         (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))))
    {
        if (u1NewInterface != IGMP_NEW_GROUP)
        {
            u1Consolidate = IGMP_TRUE;
        }
	pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpExpireGroup (pGrp->pIfNode, pGrp, IGMP_VERSION_3, u1Consolidate,
                         IGMP_FALSE);
    }
    else
    {
        /* This one is common action, section 6.4.1 
         * Reset the Group Timer  with interval = GMI 
         */
        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            /* Call the PROXY routine to update the consolidate group
             * membership data base for group registration */
            IgpGrpConsolidate (pGrp, IGMP_VERSION_3, u1NewInterface);
        }

        if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
        {
            IgmpReStartGrpTmr (pIfNode, pGrp);
        }
        else
        {
            IGMPSTARTTIMER (pGrp->Timer, IGMP_GROUP_TIMER_ID,
                            (UINT2) IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
        }
        IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
		if(pGrp->u1DynSyncFlag == IGMP_TRUE)
		{
		pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
        	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
			pGrp->u1DynSyncFlag = IGMP_FALSE;
		}
        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1DynSyncFlag == IGMP_TRUE)
            {
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
                pSrc->u1DynSyncFlag = IGMP_FALSE;
            }
        }
        IgmpRedSyncDynInfo ();
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpGroupHandleToExclude()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpGroupHandleAllowNewSources                */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGroupHandleAllowNewSources              **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpGroupHandleAllowNewSources (tIgmpIface * pIfNode,
                                tIgmpIPvXAddr u4GrpAddr,
                                UINT4 u4NoOfSources,
                                UINT1 *pu1Sources, tIgmpIPvXAddr u4Reporter,
				UINT1 u1SGJoinFlg)
{
    tIgmpFSMInfo        IgmpFSMInfoNode;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    INT4                i4Status = IGMP_NOT_OK;
    UINT1               u1FSMEventRcvd = IGMP_ZERO;
    UINT1               u1NewInterface = IGMP_NOT_A_NEW_GROUP;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpGroupHandleAllowNewSources()\r \n");
    IgmpFillMem (&IgmpFSMInfoNode, IGMP_ZERO, sizeof (tIgmpFSMInfo));
    pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
    if (pGrp == NULL)
    {
        pGrp = IgmpAddGroup (pIfNode, u4GrpAddr, IGMP_FALSE);
        if (pGrp == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                      IGMP_NAME, "Group node not created  \n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES,
                     IgmpTrcGetModuleName(IGMP_CNTRL_MODULE), "Group node not created  \n");
            return i4Status;
        }
        u1NewInterface = IGMP_NEW_GROUP;
        pGrp->u1DynamicFlg = IGMP_TRUE;
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
        IGMP_TRC_ARG2 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                       "Membership for Group %s added in interface %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
    }
    else
    {
	if (pGrp->u1DynamicFlg != IGMP_TRUE)
	{
        pGrp->u1DynamicFlg = IGMP_TRUE;
		pGrp->u1DynSyncFlag = IGMP_TRUE;
	}
        if (IgmpApplyLimitForGroup (pIfNode, pGrp) == IGMP_IGNORE)
        {
            pGrp->u1DynamicFlg = IGMP_FALSE;
            return IGMP_OK;
        }
        if ((pGrp->u1V1HostPresent == IGMP_FALSE) &&
            (pGrp->u1V2HostPresent == IGMP_FALSE))
        {
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        }
    }
    IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);

    IgmpFSMInfoNode.pIfNode = pIfNode;
    IgmpFSMInfoNode.u4NoOfSources = u4NoOfSources;
    IgmpFSMInfoNode.pu1Sources = pu1Sources;
    IGMP_IPVX_ADDR_COPY (&(IgmpFSMInfoNode.u4Reporter), &u4Reporter);
    IgmpFSMInfoNode.pGrp = pGrp;

    /* Report Received */
    u1FSMEventRcvd = IGMP_GRP_ALLOW_NEW_SOURCES;    /*5 */

    /* Do Actions specified in section 6.4.1 */
    i4Status = gaIgmpFSM[pGrp->u1FilterMode][u1FSMEventRcvd] (&IgmpFSMInfoNode);
    if ((u4NoOfSources != IGMP_ZERO) && 
        (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO))
    {
        pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpDeleteGroup (pIfNode, pGrp, pGrp->u1StaticFlg);
        return IGMP_NOT_OK;
    }

    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        /* Call the PROXY routine to update the consolidate group
         * membership data base for group registration */
        IgpGrpConsolidate (pGrp, IGMP_VERSION_3, u1NewInterface);
    }
    if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
        (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
    {
        pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
    }
    IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
	if(pGrp->u1DynSyncFlag == IGMP_TRUE)
	{
	pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
    	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
		pGrp->u1DynSyncFlag = IGMP_FALSE;
	}
    TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
    {
        if ((IgmpCheckSourceInArray (pSrc->u4SrcAddress, pu1Sources,
             u4NoOfSources) == IGMP_TRUE) && (u1SGJoinFlg == IGMP_TRUE) &&
	     (pSrc->u1DynJoinFlag != IGMP_TRUE))
	{
	    pSrc->u1DynJoinFlag = IGMP_TRUE;
            pSrc->u1DynSyncFlag = IGMP_TRUE;
	}
        if (pSrc->u1DynSyncFlag == IGMP_TRUE)
        {
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
            pSrc->u1DynSyncFlag = IGMP_FALSE;
        }
    }
    IgmpRedSyncDynInfo ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpGroupHandleAllowNewSources()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpGroupHandleBlockOldSources                */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type BLOCK_OLD_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGroupHandleBlockOldSources              **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpGroupHandleBlockOldSources (tIgmpIface * pIfNode,
                                tIgmpIPvXAddr u4GrpAddr,
                                UINT4 u4NoOfSources,
                                UINT1 *pu1Sources, tIgmpIPvXAddr u4Reporter, 
				UINT1 u1SGJoinFlg)
{
    tIgmpFSMInfo        IgmpFSMInfoNode;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    INT4                i4Status = IGMP_NOT_OK;
    UINT1               u1FSMEventRcvd = IGMP_ZERO;
    UINT1               u1NewInterface = IGMP_NOT_A_NEW_GROUP;
    UINT1               u1GrpDeleted = IGMP_FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpGroupHandleBlockOldSources()\r \n");
    IgmpFillMem (&IgmpFSMInfoNode, IGMP_ZERO, sizeof (tIgmpFSMInfo));

    pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
    if ((pGrp == NULL) || (pGrp->u1DynamicFlg == IGMP_FALSE))
    {
	
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Block Report Ignored for Group %s on i/f %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
        IGMP_TRC_ARG2 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                       "Block Report Ignored for Group %s on i/f %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
        return IGMP_OK;
    }
    else
    {
        if ((pGrp->u1V1HostPresent == IGMP_FALSE) &&
            (pGrp->u1V2HostPresent == IGMP_FALSE))
        {
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        }
    }

    /* IGMPV3 BLOCK messages are ignored when GCM is v2/v1 */
    if (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV3)
    {
        IgmpFSMInfoNode.pIfNode = pIfNode;
        IgmpFSMInfoNode.u4NoOfSources = u4NoOfSources;
        IgmpFSMInfoNode.pu1Sources = pu1Sources;
        IGMP_IPVX_ADDR_COPY (&(IgmpFSMInfoNode.u4Reporter), &u4Reporter);
        IgmpFSMInfoNode.pGrp = pGrp;

        /* Report Received */
        u1FSMEventRcvd = IGMP_GRP_BLOCK_OLD_SOURCES;    /*6 */

	if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
	    (pGrp->u1StaticFlg == IGMP_TRUE) && (pGrp->u1DynamicFlg == IGMP_TRUE) &&
	    (IgmpUtilChkIfSSMMappedGroup (pGrp->u4Group, &i4Status) != NULL))
	{
        	i4Status = gaIgmpFSM[IGMP_FMODE_INCLUDE][u1FSMEventRcvd]
            	(&IgmpFSMInfoNode);
	}
	else
	{
        /* Do Actions specified in section 6.4.1 */
        i4Status = gaIgmpFSM[pGrp->u1FilterMode][u1FSMEventRcvd]
            (&IgmpFSMInfoNode);
	}
        if ((u4NoOfSources != IGMP_ZERO) && 
                (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO))
        {
	    pGrp->u1DelSyncFlag = IGMP_TRUE;
            IgmpDeleteGroup (pIfNode, pGrp, pGrp->u1StaticFlg);
            return IGMP_NOT_OK;
        }

        if ((TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO) &&
            ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
             (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))))
        {
	    pGrp->u1DelSyncFlag = IGMP_TRUE;
            IgmpExpireGroup (pGrp->pIfNode, pGrp, IGMP_VERSION_3, IGMP_TRUE,
                             IGMP_FALSE);
            u1GrpDeleted = IGMP_TRUE;
        }
        else
        {
            if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
            {
                /* Call the PROXY routine to update the consolidate group
                 * membership data base for group registration */
                IgpGrpConsolidate (pGrp, IGMP_VERSION_3, u1NewInterface);
            }
            IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
        }
    }
    else
    {
        IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4Reporter);
    }

    if (u1GrpDeleted == IGMP_FALSE)
    {
		if(pGrp->u1DynSyncFlag == IGMP_TRUE)
		{
		pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
        	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
			pGrp->u1DynSyncFlag = IGMP_FALSE;
		}
        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if ((IgmpCheckSourceInArray (pSrc->u4SrcAddress, pu1Sources,
                  u4NoOfSources) == IGMP_TRUE) && (u1SGJoinFlg == IGMP_TRUE) &&
	     	(pSrc->u1DynJoinFlag != IGMP_FALSE))
	    {
	        pSrc->u1DynJoinFlag = IGMP_FALSE;
                pSrc->u1DynSyncFlag = IGMP_TRUE;
	    }
            if (pSrc->u1DynSyncFlag == IGMP_TRUE)
            {
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
                pSrc->u1DynSyncFlag = IGMP_FALSE;
            }
        }
        IgmpRedSyncDynInfo ();
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpGroupHandleBlockOldSources()\r \n");
    return i4Status;
}
