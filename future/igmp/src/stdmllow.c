/* $Id: stdmllow.c,v 1.7 2017/02/06 10:45:28 siva Exp $*/
#include "igmpinc.h"
#include "stdmgmcli.h"
#include "stdmldcli.h"
/* LOW LEVEL Routines for Table : MldInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceMldInterfaceTable (INT4 i4MldInterfaceIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable
        (i4MldInterfaceIfIndex, IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexMldInterfaceTable (INT4 *pi4MldInterfaceIfIndex)
{
    INT4                i4MldIfAddrType = IPVX_ADDR_FMLY_IPV6;
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable (pi4MldInterfaceIfIndex,
                                                        &i4MldIfAddrType);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
                nextMldInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMldInterfaceTable (INT4 i4MldInterfaceIfIndex,
                                  INT4 *pi4NextMldInterfaceIfIndex)
{
    INT4                i4MldIfAddrType = IGMP_ZERO;
    INT4                i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
    INT4                i4NextIndex = IGMP_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1IsTrue = 1;

    while (u1IsTrue)
    {
        i1RetVal =
            IgmpMgmtUtilNmhGetNextIndexIgmpInterfaceTable
            (i4MldInterfaceIfIndex, &i4NextIndex, i4CurAddrType,
             &i4MldIfAddrType);

        if (i1RetVal == SNMP_FAILURE || i4MldIfAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            break;
        }
        i4CurAddrType = IPVX_ADDR_FMLY_IPV4;
        i4MldInterfaceIfIndex = i4NextIndex;
    }
    *pi4NextMldInterfaceIfIndex = i4NextIndex;
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMldInterfaceQueryInterval
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQueryInterval (INT4 i4MldInterfaceIfIndex,
                                 UINT4 *pu4RetValMldInterfaceQueryInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetIgmpInterfaceQueryInterval (i4MldInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV6,
                                                      pu4RetValMldInterfaceQueryInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceStatus
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceStatus (INT4 i4MldInterfaceIfIndex,
                          INT4 *pi4RetValMldInterfaceStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhGetIgmpInterfaceStatus (i4MldInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV6,
                                                      pi4RetValMldInterfaceStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceVersion
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceVersion (INT4 i4MldInterfaceIfIndex,
                           UINT4 *pu4RetValMldInterfaceVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhGetIgmpInterfaceVersion (i4MldInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV6,
                                                       pu4RetValMldInterfaceVersion);

    *pu4RetValMldInterfaceVersion = *pu4RetValMldInterfaceVersion - 
                                     (UINT4)IGMP_ONE;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQuerier
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQuerier (INT4 i4MldInterfaceIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValMldInterfaceQuerier)
{
    tIgmpIPvXAddr       InterfaceQuerier;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&InterfaceQuerier);
    i1RetVal = IgmpMgmtUtilNmhGetIgmpInterfaceQuerier (i4MldInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV6,
                                                       &InterfaceQuerier);
    IGMP_COPY_IPVX_TO_OCTET (pRetValMldInterfaceQuerier,
                             InterfaceQuerier, IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQueryMaxResponseDelay
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQueryMaxResponseDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQueryMaxResponseDelay (INT4 i4MldInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValMldInterfaceQueryMaxResponseDelay)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetIgmpInterfaceQueryMaxResponseTime
        (i4MldInterfaceIfIndex, IPVX_ADDR_FMLY_IPV6,
         pu4RetValMldInterfaceQueryMaxResponseDelay);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceJoins
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceJoins (INT4 i4MldInterfaceIfIndex,
                         UINT4 *pu4RetValMldInterfaceJoins)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhGetIgmpInterfaceJoins (i4MldInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV6,
                                                     pu4RetValMldInterfaceJoins);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceGroups
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceGroups
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceGroups (INT4 i4MldInterfaceIfIndex,
                          UINT4 *pu4RetValMldInterfaceGroups)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhGetIgmpInterfaceGroups (i4MldInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV6,
                                                      pu4RetValMldInterfaceGroups);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceRobustness
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceRobustness
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceRobustness (INT4 i4MldInterfaceIfIndex,
                              UINT4 *pu4RetValMldInterfaceRobustness)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhGetIgmpInterfaceRobustness (i4MldInterfaceIfIndex,
                                                          IPVX_ADDR_FMLY_IPV6,
                                                          pu4RetValMldInterfaceRobustness);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceLastListenQueryIntvl
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceLastListenQueryIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceLastListenQueryIntvl (INT4 i4MldInterfaceIfIndex,
                                        UINT4
                                        *pu4RetValMldInterfaceLastListenQueryIntvl)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetIgmpInterfaceLastMembQueryIntvl
        (i4MldInterfaceIfIndex, IPVX_ADDR_FMLY_IPV6,
         pu4RetValMldInterfaceLastListenQueryIntvl);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceProxyIfIndex
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceProxyIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceProxyIfIndex (INT4 i4MldInterfaceIfIndex,
                                INT4 *pi4RetValMldInterfaceProxyIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetIgmpInterfaceProxyIfIndex (i4MldInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV6,
                                                     pi4RetValMldInterfaceProxyIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQuerierUpTime
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQuerierUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQuerierUpTime (INT4 i4MldInterfaceIfIndex,
                                 UINT4 *pu4RetValMldInterfaceQuerierUpTime)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerierUpTime (i4MldInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV6,
                                                      pu4RetValMldInterfaceQuerierUpTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldInterfaceQuerierExpiryTime
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                retValMldInterfaceQuerierExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldInterfaceQuerierExpiryTime (INT4 i4MldInterfaceIfIndex,
                                     UINT4
                                     *pu4RetValMldInterfaceQuerierExpiryTime)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerierExpiryTime (i4MldInterfaceIfIndex,
                                                          IPVX_ADDR_FMLY_IPV6,
                                                          pu4RetValMldInterfaceQuerierExpiryTime);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMldInterfaceQueryInterval
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceQueryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceQueryInterval (INT4 i4MldInterfaceIfIndex,
                                 UINT4 u4SetValMldInterfaceQueryInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhSetIgmpInterfaceQueryInterval (i4MldInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV6,
                                                      u4SetValMldInterfaceQueryInterval);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceQueryInterval,
                                sizeof (MgmdRouterInterfaceQueryInterval) /
                                sizeof (UINT4), i4MldInterfaceIfIndex,
                                'u', &u4SetValMldInterfaceQueryInterval,
                                FALSE, IPVX_ADDR_FMLY_IPV6);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceStatus
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceStatus (INT4 i4MldInterfaceIfIndex,
                          INT4 i4SetValMldInterfaceStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhSetIgmpInterfaceStatus (i4MldInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV6,
                                                      i4SetValMldInterfaceStatus);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceStatus,
                                sizeof (MgmdRouterInterfaceStatus) /
                                sizeof (UINT4), i4MldInterfaceIfIndex,
                                'i', &i4SetValMldInterfaceStatus,
                                TRUE, IPVX_ADDR_FMLY_IPV6);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceVersion
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceVersion (INT4 i4MldInterfaceIfIndex,
                           UINT4 u4SetValMldInterfaceVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (u4SetValMldInterfaceVersion == IGMP_ONE)
    {
        u4SetValMldInterfaceVersion = MLD_VERSION_1;
    }
    else if (u4SetValMldInterfaceVersion == IGMP_TWO)
    {
        u4SetValMldInterfaceVersion = MLD_VERSION_2;
    }
    i1RetVal = IgmpMgmtUtilNmhSetIgmpInterfaceVersion (i4MldInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV6,
                                                       u4SetValMldInterfaceVersion);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceVersion,
                                sizeof (MgmdRouterInterfaceVersion) /
                                sizeof (UINT4), i4MldInterfaceIfIndex,
                                'u', &u4SetValMldInterfaceVersion,
                                FALSE, IPVX_ADDR_FMLY_IPV6);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceQueryMaxResponseDelay
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceQueryMaxResponseDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceQueryMaxResponseDelay (INT4 i4MldInterfaceIfIndex,
                                         UINT4
                                         u4SetValMldInterfaceQueryMaxResponseDelay)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhSetIgmpInterfaceQueryMaxResponseTime
        (i4MldInterfaceIfIndex, IPVX_ADDR_FMLY_IPV6,
         u4SetValMldInterfaceQueryMaxResponseDelay);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceQueryMaxResponseTime,
                                sizeof (MgmdRouterInterfaceQueryMaxResponseTime)
                                / sizeof (UINT4), i4MldInterfaceIfIndex, 'u',
                                &u4SetValMldInterfaceQueryMaxResponseDelay,
                                FALSE, IPVX_ADDR_FMLY_IPV6);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceRobustness
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceRobustness
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceRobustness (INT4 i4MldInterfaceIfIndex,
                              UINT4 u4SetValMldInterfaceRobustness)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhSetIgmpInterfaceRobustness (i4MldInterfaceIfIndex,
                                                          IPVX_ADDR_FMLY_IPV6,
                                                          u4SetValMldInterfaceRobustness);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceRobustness,
                                sizeof (MgmdRouterInterfaceRobustness) /
                                sizeof (UINT4), i4MldInterfaceIfIndex,
                                'u', &u4SetValMldInterfaceRobustness,
                                FALSE, IPVX_ADDR_FMLY_IPV6);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceLastListenQueryIntvl
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceLastListenQueryIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceLastListenQueryIntvl (INT4 i4MldInterfaceIfIndex,
                                        UINT4
                                        u4SetValMldInterfaceLastListenQueryIntvl)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhSetIgmpInterfaceLastMembQueryIntvl
        (i4MldInterfaceIfIndex, IPVX_ADDR_FMLY_IPV6,
         u4SetValMldInterfaceLastListenQueryIntvl);

    #ifdef SNMP_3_WANTED
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceLastMemberQueryInterval,
                                sizeof
                                (MgmdRouterInterfaceLastMemberQueryInterval) /
                                sizeof (UINT4), i4MldInterfaceIfIndex, 'u',
                                &u4SetValMldInterfaceLastListenQueryIntvl,
                                FALSE, IPVX_ADDR_FMLY_IPV6);
    }
    #endif
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMldInterfaceProxyIfIndex
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                setValMldInterfaceProxyIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldInterfaceProxyIfIndex (INT4 i4MldInterfaceIfIndex,
                                INT4 i4SetValMldInterfaceProxyIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhSetIgmpInterfaceProxyIfIndex (i4MldInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV6,
                                                     i4SetValMldInterfaceProxyIfIndex);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceProxyIfIndex,
                                sizeof (MgmdRouterInterfaceProxyIfIndex) /
                                sizeof (UINT4), i4MldInterfaceIfIndex,
                                'i', &i4SetValMldInterfaceProxyIfIndex,
                                FALSE, IPVX_ADDR_FMLY_IPV6);
    }

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceQueryInterval
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceQueryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceQueryInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4MldInterfaceIfIndex,
                                    UINT4 u4TestValMldInterfaceQueryInterval)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval (pu4ErrorCode,
                                                                i4MldInterfaceIfIndex,
                                                                IPVX_ADDR_FMLY_IPV6,
                                                                u4TestValMldInterfaceQueryInterval);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceStatus
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceStatus (UINT4 *pu4ErrorCode,
                             INT4 i4MldInterfaceIfIndex,
                             INT4 i4TestValMldInterfaceStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus (pu4ErrorCode,
                                                         i4MldInterfaceIfIndex,
                                                         IPVX_ADDR_FMLY_IPV6,
                                                         i4TestValMldInterfaceStatus);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceVersion
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceVersion (UINT4 *pu4ErrorCode,
                              INT4 i4MldInterfaceIfIndex,
                              UINT4 u4TestValMldInterfaceVersion)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (u4TestValMldInterfaceVersion == IGMP_ONE)
    {
        u4TestValMldInterfaceVersion = MLD_VERSION_1;
    }
    else if (u4TestValMldInterfaceVersion == IGMP_TWO)
    {
        u4TestValMldInterfaceVersion = MLD_VERSION_2;
    }

    i1RetVal = IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion (pu4ErrorCode,
                                                          i4MldInterfaceIfIndex,
                                                          IPVX_ADDR_FMLY_IPV6,
                                                          u4TestValMldInterfaceVersion);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceQueryMaxResponseDelay
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceQueryMaxResponseDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceQueryMaxResponseDelay (UINT4 *pu4ErrorCode,
                                            INT4 i4MldInterfaceIfIndex,
                                            UINT4
                                            u4TestValMldInterfaceQueryMaxResponseDelay)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime (pu4ErrorCode,
                                                                i4MldInterfaceIfIndex,
                                                                IPVX_ADDR_FMLY_IPV6,
                                                                u4TestValMldInterfaceQueryMaxResponseDelay);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceRobustness
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceRobustness
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceRobustness (UINT4 *pu4ErrorCode,
                                 INT4 i4MldInterfaceIfIndex,
                                 UINT4 u4TestValMldInterfaceRobustness)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness (pu4ErrorCode,
                                                             i4MldInterfaceIfIndex,
                                                             IPVX_ADDR_FMLY_IPV6,
                                                             u4TestValMldInterfaceRobustness);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceLastListenQueryIntvl
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceLastListenQueryIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceLastListenQueryIntvl (UINT4 *pu4ErrorCode,
                                           INT4 i4MldInterfaceIfIndex,
                                           UINT4
                                           u4TestValMldInterfaceLastListenQueryIntvl)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl (pu4ErrorCode,
                                                              i4MldInterfaceIfIndex,
                                                              IPVX_ADDR_FMLY_IPV6,
                                                              u4TestValMldInterfaceLastListenQueryIntvl);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MldInterfaceProxyIfIndex
 Input       :  The Indices
                MldInterfaceIfIndex

                The Object 
                testValMldInterfaceProxyIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldInterfaceProxyIfIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4MldInterfaceIfIndex,
                                   INT4 i4TestValMldInterfaceProxyIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhTestv2IgmpInterfaceProxyIfIndex (pu4ErrorCode,
                                                               i4MldInterfaceIfIndex,
                                                               IPVX_ADDR_FMLY_IPV6,
                                                               i4TestValMldInterfaceProxyIfIndex);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MldInterfaceTable
 Input       :  The Indices
                MldInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MldInterfaceTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhDepv2IgmpInterfaceTable (pu4ErrorCode,
                                                       pSnmpIndexList,
                                                       pSnmpVarBind);
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : MldCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMldCacheTable
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceMldCacheTable (tSNMP_OCTET_STRING_TYPE *
                                       pMldCacheAddress, INT4 i4MldCacheIfIndex)
{
    tIgmpIPvXAddr       u4IgmpCacheAddress;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheAddress);
    IGMP_COPY_OCTET_TO_IPVX (u4IgmpCacheAddress, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable (IPVX_ADDR_FMLY_IPV6,
                                                            u4IgmpCacheAddress,
                                                            i4MldCacheIfIndex);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMldCacheTable
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexMldCacheTable (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                               INT4 *pi4MldCacheIfIndex)
{
	tIgmpIPvXAddr       u4IgmpCacheAddress;
	INT4                i4CacheAddrType = IPVX_ADDR_FMLY_IPV6;
	INT1                i1RetVal = SNMP_FAILURE;
	IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheAddress);
	i1RetVal = IgmpMgmtUtilNmhGetFirstIndexIgmpCacheTable (&i4CacheAddrType,
			&u4IgmpCacheAddress,
			pi4MldCacheIfIndex);
	if ((i4CacheAddrType == IPVX_ADDR_FMLY_IPV6) && (i1RetVal == SNMP_SUCCESS))
	{
		IGMP_COPY_IPVX_TO_OCTET (pMldCacheAddress,
				u4IgmpCacheAddress, IPVX_ADDR_FMLY_IPV6);
		return SNMP_SUCCESS;
	}
	return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexMldCacheTable
Input       :  The Indices
MldCacheAddress
                nextMldCacheAddress
                MldCacheIfIndex
                nextMldCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMldCacheTable (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                              tSNMP_OCTET_STRING_TYPE * pNextMldCacheAddress,
                              INT4 i4MldCacheIfIndex,
                              INT4 *pi4NextMldCacheIfIndex)
{
    tIgmpIPvXAddr       u4IgmpCacheAddress;
    tIgmpIPvXAddr       u4NextIgmpCacheAddress;
    INT4                i4NextCacheAddrType = IGMP_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1IsTrue = 1;
    INT4                i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
    IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheAddress);
    IGMP_IPVX_ADDR_CLEAR (&u4NextIgmpCacheAddress);
    IGMP_COPY_OCTET_TO_IPVX (u4IgmpCacheAddress, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    while (u1IsTrue)
    {
        i1RetVal =
            IgmpMgmtUtilNmhGetNextIndexIgmpCacheTable (i4CurAddrType,
                                                          &i4NextCacheAddrType,
                                                          u4IgmpCacheAddress,
                                                          &u4NextIgmpCacheAddress,
                                                          i4MldCacheIfIndex,
                                                          pi4NextMldCacheIfIndex);

        if (i1RetVal == SNMP_FAILURE
                || i4NextCacheAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            break;
        }
        i4CurAddrType = IPVX_ADDR_FMLY_IPV4;
        i4MldCacheIfIndex = *pi4NextMldCacheIfIndex;
        IGMP_IPVX_ADDR_COPY (&u4IgmpCacheAddress, &u4NextIgmpCacheAddress);
    }
    IGMP_COPY_IPVX_TO_OCTET (pNextMldCacheAddress,
                             u4NextIgmpCacheAddress, IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMldCacheSelf
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheSelf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheSelf (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                    INT4 i4MldCacheIfIndex, INT4 *pi4RetValMldCacheSelf)
{
    tIgmpIPvXAddr       u4MldCacheAddr;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&u4MldCacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (u4MldCacheAddr, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhGetIgmpCacheSelf (IPVX_ADDR_FMLY_IPV6,
                                                u4MldCacheAddr,
                                                i4MldCacheIfIndex,
                                                pi4RetValMldCacheSelf);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldCacheLastReporter
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheLastReporter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheLastReporter (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                            INT4 i4MldCacheIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValMldCacheLastReporter)
{
    tIgmpIPvXAddr       u4IgmpCacheAddress;
    tIgmpIPvXAddr       u4IgmpCacheLastReporter;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheAddress);
    IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheLastReporter);
    IGMP_COPY_OCTET_TO_IPVX (u4IgmpCacheAddress, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhGetIgmpCacheLastReporter (IPVX_ADDR_FMLY_IPV6,
                                                        u4IgmpCacheAddress,
                                                        i4MldCacheIfIndex,
                                                        &u4IgmpCacheLastReporter);
    IGMP_COPY_IPVX_TO_OCTET (pRetValMldCacheLastReporter,
                             u4IgmpCacheLastReporter, IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldCacheUpTime
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheUpTime (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                      INT4 i4MldCacheIfIndex, UINT4 *pu4RetValMldCacheUpTime)
{
    tIgmpIPvXAddr       u4IgmpCacheAddress;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheAddress);
    IGMP_COPY_OCTET_TO_IPVX (u4IgmpCacheAddress, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhGetIgmpCacheUpTime (IPVX_ADDR_FMLY_IPV6,
                                                  u4IgmpCacheAddress,
                                                  i4MldCacheIfIndex,
                                                  pu4RetValMldCacheUpTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldCacheExpiryTime
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheExpiryTime (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                          INT4 i4MldCacheIfIndex,
                          UINT4 *pu4RetValMldCacheExpiryTime)
{
    tIgmpIPvXAddr       u4IgmpCacheAddress;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheAddress);
    IGMP_COPY_OCTET_TO_IPVX (u4IgmpCacheAddress, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhGetIgmpCacheExpiryTime (IPVX_ADDR_FMLY_IPV6,
                                                      u4IgmpCacheAddress,
                                                      i4MldCacheIfIndex,
                                                      pu4RetValMldCacheExpiryTime);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMldCacheStatus
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                retValMldCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMldCacheStatus (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                      INT4 i4MldCacheIfIndex, INT4 *pi4RetValMldCacheStatus)
{
    tIgmpIPvXAddr       u4IgmpCacheAddress;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&u4IgmpCacheAddress);
    IGMP_COPY_OCTET_TO_IPVX (u4IgmpCacheAddress, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhGetIgmpCacheStatus (IPVX_ADDR_FMLY_IPV6,
                                                  u4IgmpCacheAddress,
                                                  i4MldCacheIfIndex,
                                                  pi4RetValMldCacheStatus);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMldCacheSelf
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                setValMldCacheSelf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldCacheSelf (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                    INT4 i4MldCacheIfIndex, INT4 i4SetValMldCacheSelf)
{
    tIgmpIPvXAddr       IgmpCacheAddr;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (IgmpCacheAddr, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhSetIgmpCacheSelf (IgmpCacheAddr,
                                                IPVX_ADDR_FMLY_IPV6,
                                                i4MldCacheIfIndex,
                                                i4SetValMldCacheSelf);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForMldCacheTbl (MldCacheSelf, sizeof (MldCacheSelf) /
                                      sizeof (UINT4), pMldCacheAddress,
                                      i4MldCacheIfIndex, i4SetValMldCacheSelf,
                                      FALSE);
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetMldCacheStatus
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                setValMldCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMldCacheStatus (tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                      INT4 i4MldCacheIfIndex, INT4 i4SetValMldCacheStatus)
{
    tIgmpIPvXAddr       IgmpCacheAddr;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (IgmpCacheAddr, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhSetIgmpCacheStatus (IgmpCacheAddr,
                                                  IPVX_ADDR_FMLY_IPV6,
                                                  i4MldCacheIfIndex,
                                                  i4SetValMldCacheStatus);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForMldCacheTbl (MldCacheStatus, sizeof (MldCacheStatus) /
                                      sizeof (UINT4), pMldCacheAddress,
                                      i4MldCacheIfIndex, i4SetValMldCacheStatus,
                                      TRUE);
    }
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MldCacheSelf
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                testValMldCacheSelf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldCacheSelf (UINT4 *pu4ErrorCode,
                       tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                       INT4 i4MldCacheIfIndex, INT4 i4TestValMldCacheSelf)
{
    tIgmpIPvXAddr       IgmpCacheAddr;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (IgmpCacheAddr, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhTestv2IgmpCacheSelf (pu4ErrorCode,
                                                   IgmpCacheAddr,
                                                   i4MldCacheIfIndex,
                                                   IPVX_ADDR_FMLY_IPV6,
                                                   i4TestValMldCacheSelf);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2MldCacheStatus
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex

                The Object 
                testValMldCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MldCacheStatus (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pMldCacheAddress,
                         INT4 i4MldCacheIfIndex, INT4 i4TestValMldCacheStatus)
{
    tIgmpIPvXAddr       IgmpCacheAddress;
    INT1                i1RetVal = SNMP_FAILURE;
    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddress);
    IGMP_COPY_OCTET_TO_IPVX (IgmpCacheAddress, pMldCacheAddress,
                             IPVX_ADDR_FMLY_IPV6);
    i1RetVal = IgmpMgmtUtilNmhTestv2IgmpCacheStatus (pu4ErrorCode,
                                                     IgmpCacheAddress,
                                                     i4MldCacheIfIndex,
                                                     IPVX_ADDR_FMLY_IPV6,
                                                     i4TestValMldCacheStatus);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MldCacheTable
 Input       :  The Indices
                MldCacheAddress
                MldCacheIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MldCacheTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhDepv2IgmpCacheTable (pu4ErrorCode,
                                                   pSnmpIndexList,
                                                   pSnmpVarBind);
    return i1RetVal;
}
