/* $Id: stdigmwr.c,v 1.4 2015/02/28 12:20:56 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdigmlow.h"
# include  "stdigmwr.h"
# include  "stdigmdb.h"
# include  "igmpinc.h"
#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE;
#endif

INT4
GetNextIndexIgmpInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : GetNextIndexIgmpInterfaceTable\n");

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIgmpInterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,  IGMP_NAME,
                       "Getting first index from interface table failed");
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpInterfaceTable\n");
	    return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIgmpInterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
             IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,  IGMP_NAME,
                       "Next Index not found from interface table");
 	     MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpInterfaceTable\n");
	     return SNMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpInterfaceTable\n");
    return SNMP_SUCCESS;
}

VOID
RegisterSTDIGM ()
{
    SNMPRegisterMibWithLock (&stdigmOID, &stdigmEntry, IgmpLock, IgmpUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdigmOID, (const UINT1 *) "stdigmp");
}

VOID
UnRegisterSTDIGM ()
{
    SNMPUnRegisterMib (&stdigmOID, &stdigmEntry);
    SNMPDelSysorEntry (&stdigmOID, (const UINT1 *) "stdigmp");
}

INT4
IgmpInterfaceQueryIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceQueryIntervalGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
	IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQueryIntervalGet\n");
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQueryIntervalGet\n");
    return (nmhGetIgmpInterfaceQueryInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceStatusGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
	IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
  	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceStatusGet\n");
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceStatusGet\n");
    return (nmhGetIgmpInterfaceStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IgmpInterfaceVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceVersionGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {	
	IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceVersionGet\n");
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceVersionGet\n");
    return (nmhGetIgmpInterfaceVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceQuerierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{   
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceQuerierGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
     	IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
 	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQuerierGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQuerierGet\n");
    return (nmhGetIgmpInterfaceQuerier (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceQueryMaxResponseTimeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceQueryMaxResponseTimeGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQueryMaxResponseTimeGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQueryMaxResponseTimeGet\n");
    return (nmhGetIgmpInterfaceQueryMaxResponseTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceQuerierUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceQuerierUpTimeGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQuerierUpTimeGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQuerierUpTimeGet\n");
    return (nmhGetIgmpInterfaceQuerierUpTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceQuerierExpiryTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceQuerierExpiryTimeGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQuerierExpiryTimeGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceQuerierExpiryTimeGet\n");
    return (nmhGetIgmpInterfaceQuerierExpiryTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceVersion1QuerierTimerGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceVersion1QuerierTimerGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceVersion1QuerierTimerGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceVersion1QuerierTimerGet\n");
    return (nmhGetIgmpInterfaceVersion1QuerierTimer
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceWrongVersionQueriesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceWrongVersionQueriesGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceWrongVersionQueriesGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceWrongVersionQueriesGet\n");
    return (nmhGetIgmpInterfaceWrongVersionQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceJoinsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceJoinsGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceJoinsGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceJoinsGet\n");
    return (nmhGetIgmpInterfaceJoins (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceProxyIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceProxyIfIndexGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceProxyIfIndexGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceProxyIfIndexGet\n");
    return (nmhGetIgmpInterfaceProxyIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IgmpInterfaceGroupsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceGroupsGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceGroupsGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceGroupsGet\n");
    return (nmhGetIgmpInterfaceGroups (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceRobustnessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceRobustnessGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceRobustnessGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceRobustnessGet\n");
    return (nmhGetIgmpInterfaceRobustness (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceLastMembQueryIntvlGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceLastMembQueryIntvlGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceLastMembQueryIntvlGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceLastMembQueryIntvlGet\n");
    return (nmhGetIgmpInterfaceLastMembQueryIntvl
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceVersion2QuerierTimerGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpInterfaceVersion2QuerierTimerGet\n");
    if (nmhValidateIndexInstanceIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceVersion2QuerierTimerGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpInterfaceVersion2QuerierTimerGet\n");
    return (nmhGetIgmpInterfaceVersion2QuerierTimer
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpInterfaceQueryIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpInterfaceQueryInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpInterfaceStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IgmpInterfaceVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpInterfaceVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceQueryMaxResponseTimeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetIgmpInterfaceQueryMaxResponseTime
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceProxyIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpInterfaceProxyIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IgmpInterfaceRobustnessSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpInterfaceRobustness (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceLastMembQueryIntvlSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetIgmpInterfaceLastMembQueryIntvl
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceQueryIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2IgmpInterfaceQueryInterval (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IgmpInterfaceStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IgmpInterfaceVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IgmpInterfaceVersion (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceQueryMaxResponseTimeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2IgmpInterfaceQueryMaxResponseTime (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
IgmpInterfaceProxyIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IgmpInterfaceProxyIfIndex (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
IgmpInterfaceRobustnessTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IgmpInterfaceRobustness (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
IgmpInterfaceLastMembQueryIntvlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2IgmpInterfaceLastMembQueryIntvl (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
IgmpInterfaceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IgmpInterfaceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIgmpCacheTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : GetNextIndexIgmpCacheTable\n");
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIgmpCacheTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Getting first index from IgmpCache table failed");
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpCacheTable\n");
	    return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIgmpCacheTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
             IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Getting next index from IgmpCache table failed");
	     MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpCacheTable\n");
	     return SNMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpCacheTable\n");
    return SNMP_SUCCESS;
}

INT4
IgmpCacheSelfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheSelfGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheSelfGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheSelfGet\n");
    return (nmhGetIgmpCacheSelf (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
IgmpCacheLastReporterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheLastReporterGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheLastReporterGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheLastReporterGet\n");
    return (nmhGetIgmpCacheLastReporter (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IgmpCacheUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheUpTimeGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheUpTimeGet\n");
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheUpTimeGet\n");
    return (nmhGetIgmpCacheUpTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
IgmpCacheExpiryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{   
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheExpiryTimeGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheExpiryTimeGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheExpiryTimeGet\n");
    return (nmhGetIgmpCacheExpiryTime (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
IgmpCacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{   
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheStatusGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheStatusGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheStatusGet\n");
    return (nmhGetIgmpCacheStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
IgmpCacheVersion1HostTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheVersion1HostTimerGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheVersion1HostTimerGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheVersion1HostTimerGet\n");
    return (nmhGetIgmpCacheVersion1HostTimer
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpCacheVersion2HostTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheVersion2HostTimerGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheVersion2HostTimerGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheVersion2HostTimerGet\n");
    return (nmhGetIgmpCacheVersion2HostTimer
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IgmpCacheSourceFilterModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpCacheSourceFilterModeGet\n");
    if (nmhValidateIndexInstanceIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheSourceFilterModeGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpCacheSourceFilterModeGet\n");
    return (nmhGetIgmpCacheSourceFilterMode
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IgmpCacheSelfSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpCacheSelf (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
IgmpCacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpCacheStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
IgmpCacheSelfTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2IgmpCacheSelf (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
IgmpCacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2IgmpCacheStatus (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
IgmpCacheTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IgmpCacheTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIgmpSrcListTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : GetNextIndexIgmpSrcListTable\n");
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIgmpSrcListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                     "Getting first index from IgmpSrc ListTable failed");
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpSrcListTable\n");
	    return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIgmpSrcListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {	
	    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                     "Getting next index from IgmpSrc ListTable failed");
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpSrcListTable\n");
            return SNMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : GetNextIndexIgmpSrcListTable\n");
    return SNMP_SUCCESS;
}

INT4
IgmpSrcListStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "ENTRY : IgmpSrcListStatusGet\n");
    if (nmhValidateIndexInstanceIgmpSrcListTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                         "Invalid index instance");
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpSrcListStatusGet\n");
	return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exit : IgmpSrcListStatusGet\n");
    return (nmhGetIgmpSrcListStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IgmpSrcListStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIgmpSrcListStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IgmpSrcListStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IgmpSrcListStatus (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IgmpSrcListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IgmpSrcListTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
