/* $Id: fsigmpwr.c,v 1.11 2016/06/24 09:42:19 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsigmpwr.h"
# include  "fsigmpdb.h"
# include  "igmpinc.h"

VOID
RegisterFSIGMP ()
{
    SNMPRegisterMibWithLock (&fsigmpOID, &fsigmpEntry, IgmpLock, IgmpUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsigmpOID, (const UINT1 *) "fsigmp");
}

VOID
UnRegisterFSIGMP ()
{
    SNMPUnRegisterMib (&fsigmpOID, &fsigmpEntry);
    SNMPDelSysorEntry (&fsigmpOID, (const UINT1 *) "fsigmp");
}

INT4
FsIgmpGlobalStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIgmpGlobalStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsIgmpTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIgmpTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4 FsIgmpDebugLevelGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsIgmpDebugLevel(&(pMultiData->i4_SLongValue)));
}

INT4
FsIgmpGlobalStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIgmpGlobalStatus (pMultiData->i4_SLongValue));
}

INT4
FsIgmpTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIgmpTraceLevel (pMultiData->i4_SLongValue));
}


INT4 FsIgmpDebugLevelSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsIgmpDebugLevel(pMultiData->i4_SLongValue));
}

INT4
FsIgmpGlobalStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIgmpGlobalStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIgmpTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIgmpTraceLevel (pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsIgmpDebugLevelTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsIgmpDebugLevel(pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIgmpInterfaceGroupListIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsIgmpInterfaceGroupListId (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsIgmpInterfaceLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsIgmpInterfaceLimit (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsIgmpGlobalStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIgmpGlobalStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIgmpTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIgmpTraceLevel (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsIgmpDebugLevelDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsIgmpDebugLevel(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsIgmpInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIgmpInterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIgmpInterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIgmpInterfaceAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIgmpInterfaceFastLeaveStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceFastLeaveStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIgmpInterfaceOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceOperStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIgmpInterfaceIncomingPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceIncomingPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceIncomingJoinsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceIncomingJoins
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceIncomingLeavesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceIncomingLeaves
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceIncomingQueriesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceIncomingQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceOutgoingQueriesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceOutgoingQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceRxGenQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceRxGenQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceGroupListIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceGroupListId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceCurGrpCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceCurGrpCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceCKSumErrorGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceCKSumError
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfacePktLenErrorGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfacePktLenError
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfacePktsWithLocalIPGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfacePktsWithLocalIP
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceSubnetCheckFailureGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceSubnetCheckFailure
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceQryFromNonQuerierGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceQryFromNonQuerier
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceReportVersionMisMatchGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceReportVersionMisMatch
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceQryVersionMisMatchGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceQryVersionMisMatch
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceUnknownMsgTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceUnknownMsgType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceInvalidV1ReportGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceInvalidV1Report
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceInvalidV2ReportGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceInvalidV2Report
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceInvalidV3ReportGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceInvalidV3Report
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceRouterAlertCheckFailureGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceRouterAlertCheckFailure
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceRxGrpQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceRxGrpQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceRxGrpAndSrcQueriesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceRxGrpAndSrcQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceRxv1v2ReportsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceRxv1v2Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceRxv3ReportsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceRxv3Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceTxGenQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceTxGenQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceTxGrpQueriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceTxGrpQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceTxGrpAndSrcQueriesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceTxGrpAndSrcQueries
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceTxv1v2ReportsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceTxv1v2Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceTxv3ReportsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceTxv3Reports
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceTxv2LeavesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceTxv2Leaves
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsIgmpInterfaceChannelTrackStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpInterfaceChannelTrackStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4 
FsIgmpInterfaceIncomingSSMPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsIgmpInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsIgmpInterfaceIncomingSSMPkts(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}

INT4 
FsIgmpInterfaceInvalidSSMPktsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsIgmpInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsIgmpInterfaceInvalidSSMPkts(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));
}
INT4 
FsIgmpInterfaceJoinPktRateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsIgmpInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsIgmpInterfaceJoinPktRate(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));
}

INT4
FsIgmpInterfaceAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIgmpInterfaceAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsIgmpInterfaceFastLeaveStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsIgmpInterfaceFastLeaveStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsIgmpInterfaceChannelTrackStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsIgmpInterfaceChannelTrackStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsIgmpInterfaceGroupListIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIgmpInterfaceGroupListId
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsIgmpInterfaceLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIgmpInterfaceLimit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4 
FsIgmpInterfaceJoinPktRateSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsIgmpInterfaceJoinPktRate(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4
FsIgmpInterfaceAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsIgmpInterfaceAdminStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsIgmpInterfaceFastLeaveStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsIgmpInterfaceFastLeaveStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsIgmpInterfaceChannelTrackStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsIgmpInterfaceChannelTrackStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4 
FsIgmpInterfaceJoinPktRateTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsIgmpInterfaceJoinPktRate(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}


INT4
FsIgmpInterfaceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIgmpInterfaceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsIgmpCacheTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIgmpCacheTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIgmpCacheTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIgmpCacheGroupCompModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpCacheTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpCacheGroupCompMode
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsIgmpGroupListTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIgmpGroupListTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIgmpGroupListTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsIgmpGrpListRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIgmpGroupListTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIgmpGrpListRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsIgmpGrpListRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsIgmpGrpListRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsIgmpGrpListRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsIgmpGrpListRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsIgmpGroupListTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIgmpGroupListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsIgmpGlobalLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIgmpGlobalLimit (&(pMultiData->u4_ULongValue)));
}

INT4
FsIgmpGlobalCurGrpCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsIgmpGlobalCurGrpCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsIgmpSSMMapStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsIgmpSSMMapStatus(&(pMultiData->i4_SLongValue)));
}

INT4
FsIgmpGlobalLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsIgmpGlobalLimit (pMultiData->u4_ULongValue));
}

INT4
FsIgmpSSMMapStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsIgmpSSMMapStatus(pMultiData->i4_SLongValue));
}

INT4
FsIgmpGlobalLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsIgmpGlobalLimit (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsIgmpSSMMapStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsIgmpSSMMapStatus(pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsIgmpGlobalLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsIgmpGlobalLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 
FsIgmpSSMMapStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsIgmpSSMMapStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 
GetNextIndexFsIgmpSSMMapGroupTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsIgmpSSMMapGroupTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsIgmpSSMMapGroupTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue),
			pFirstMultiIndex->pIndex[2].u4_ULongValue,
			&(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}

INT4 
FsIgmpSSMMapRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsIgmpSSMMapGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsIgmpSSMMapRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 
FsIgmpSSMMapRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsIgmpSSMMapRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 
FsIgmpSSMMapRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsIgmpSSMMapRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiIndex->pIndex[2].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 
FsIgmpSSMMapGroupTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsIgmpSSMMapGroupTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

