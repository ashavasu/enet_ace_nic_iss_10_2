/******************************************************************************/
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsigmtst.c,v 1.12 2016/06/24 09:42:19 siva Exp $
 *
 * Description: : Action routines for set/get objects in           
 *                           fsigmp.mib                           
 *
 *******************************************************************/

# include  "include.h"
# include  "fsigmcon.h"
# include  "fsigmogi.h"
# include  "midconst.h"
# include  "igmpinc.h"

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsIgmpGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsIgmpGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus (pu4ErrorCode,
                                                 i4TestValFsIgmpGlobalStatus,
                                                 IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpTraceLevel
 Input       :  The Indices

                The Object 
                testValFsIgmpTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpTraceLevel (UINT4 *pu4ErrorCode, INT4 i4TestValFsIgmpTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel (pu4ErrorCode,
                                               i4TestValFsIgmpTraceLevel,
                                               IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}
/****************************************************************************
 Function    :  nmhTestv2FsIgmpDebugLevel
 Input       :  The Indices

                The Object
                testValFsIgmpDebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpDebugLevel (UINT4 *pu4ErrorCode, INT4 i4TestValFsIgmpDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel (pu4ErrorCode,
                                               i4TestValFsIgmpDebugLevel,
                                               IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIgmpGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIgmpTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpTraceLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIgmpDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpDebugLevel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpInterfaceFastLeaveStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceFastLeaveStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpInterfaceFastLeaveStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsIgmpInterfaceIfIndex,
                                         INT4
                                         i4TestValFsIgmpInterfaceFastLeaveStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus (pu4ErrorCode,
                                                             i4FsIgmpInterfaceIfIndex,
                                                             IPVX_ADDR_FMLY_IPV4,
                                                             i4TestValFsIgmpInterfaceFastLeaveStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpInterfaceChannelTrackStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceChannelTrackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpInterfaceChannelTrackStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsIgmpInterfaceIfIndex,
                                            INT4
                                            i4TestValFsIgmpInterfaceChannelTrackStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus
        (pu4ErrorCode,
         i4FsIgmpInterfaceIfIndex,
         IPVX_ADDR_FMLY_IPV4, i4TestValFsIgmpInterfaceChannelTrackStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpJoinPktRate
 Input       :  The Indices

                The Object 
                testValFsIgmpJoinPktRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpJoinPktRate (UINT4 *pu4ErrorCode,
                                     INT4
                                     i4TestValFsIgmpJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpJoinPktRate
        (pu4ErrorCode, IPVX_ADDR_FMLY_IPV4, i4TestValFsIgmpJoinPktRate);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpInterfaceJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceJoinPktRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpInterfaceJoinPktRate (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIgmpInterfaceIfIndex,
                                     INT4
                                     i4TestValFsIgmpInterfaceJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate
        (pu4ErrorCode, i4FsIgmpInterfaceIfIndex,
         IPVX_ADDR_FMLY_IPV4, i4TestValFsIgmpInterfaceJoinPktRate);

    return i1Status;
}


/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIgmpInterfaceTable
 Input       :  The Indices
                FsIgmpInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpInterfaceTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIgmpInterfaceAdminStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpInterfaceAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIgmpInterfaceIfIndex,
                                     INT4 i4TestValFsIgmpInterfaceAdminStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus (pu4ErrorCode,
                                                         i4FsIgmpInterfaceIfIndex,
                                                         IPVX_ADDR_FMLY_IPV4,
                                                         i4TestValFsIgmpInterfaceAdminStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpInterfaceGroupListId
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceGroupListId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpInterfaceGroupListId (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIgmpInterfaceIfIndex,
                                     UINT4 u4TestValFsIgmpInterfaceGroupListId)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId (pu4ErrorCode,
                                                         i4FsIgmpInterfaceIfIndex,
                                                         IPVX_ADDR_FMLY_IPV4,
                                                         u4TestValFsIgmpInterfaceGroupListId);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpInterfaceLimit
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                testValFsIgmpInterfaceLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpInterfaceLimit (UINT4 *pu4ErrorCode,
                               INT4 i4FsIgmpInterfaceIfIndex,
                               UINT4 u4TestValFsIgmpInterfaceLimit)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit (pu4ErrorCode,
                                                   i4FsIgmpInterfaceIfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   u4TestValFsIgmpInterfaceLimit);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpGrpListRowStatus
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen

                The Object 
                testValFsIgmpGrpListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpGrpListRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsIgmpGrpListId,
                                 UINT4 u4FsIgmpGrpIP,
                                 UINT4 u4FsIgmpGrpPrefixLen,
                                 INT4 i4TestValFsIgmpGrpListRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpGrpListRowStatus (pu4ErrorCode,
                                                     u4FsIgmpGrpListId,
                                                     u4FsIgmpGrpIP,
                                                     u4FsIgmpGrpPrefixLen,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     i4TestValFsIgmpGrpListRowStatus);

    return i1Status;

}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpGlobalLimit
 Input       :  The Indices

                The Object 
                testValFsIgmpGlobalLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpGlobalLimit (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsIgmpGlobalLimit)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit (pu4ErrorCode,
                                                u4TestValFsIgmpGlobalLimit,
                                                IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}


/****************************************************************************
 Function    :  nmhTestv2FsIgmpSSMMapStatus
 Input       :  The Indices

                The Object 
                testValFsIgmpSSMMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsIgmpSSMMapStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsIgmpSSMMapStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpSSMMapStatus (pu4ErrorCode,
                                                 i4TestValFsIgmpSSMMapStatus,
                                                 IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}


/****************************************************************************
 Function    :  nmhTestv2FsIgmpSSMMapRowStatus
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress

                The Object 
                testValFsIgmpSSMMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsIgmpSSMMapRowStatus(UINT4 *pu4ErrorCode , 
                                UINT4 u4FsIgmpSSMMapStartGrpAddress , 
                                UINT4 u4FsIgmpSSMMapEndGrpAddress , 
                                UINT4 u4FsIgmpSSMMapSourceAddress , 
                                INT4 i4TestValFsIgmpSSMMapRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus (pu4ErrorCode,
                u4FsIgmpSSMMapStartGrpAddress,
                u4FsIgmpSSMMapEndGrpAddress,
                u4FsIgmpSSMMapSourceAddress,
                i4TestValFsIgmpSSMMapRowStatus);

    return i1Status;
}


/****************************************************************************
 Function    :  nmhDepv2FsIgmpGroupListTable
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpGroupListTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIgmpGlobalLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpGlobalLimit (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIgmpSSMMapStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhDepv2FsIgmpSSMMapStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, 
                            tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIgmpSSMMapGroupTable
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhDepv2FsIgmpSSMMapGroupTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, 
                                tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
