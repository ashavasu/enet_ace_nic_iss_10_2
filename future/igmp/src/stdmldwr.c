/* $Id: stdmldwr.c,v 1.3 2010/12/22 11:37:51 siva Exp $*/
#include "lr.h"
#include "fssnmp.h"
#include "stdmldwr.h"
#include "stdmllow.h"
#include "stdmlddb.h"

VOID
RegisterSTDMLD ()
{
    SNMPRegisterMib (&stdmldOID, &stdmldEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&stdmldOID, (const UINT1 *) "mldMIB");
}

INT4
MldInterfaceIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
MldInterfaceQueryIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    return (nmhTestv2MldInterfaceQueryInterval (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));
}

INT4
MldInterfaceQueryIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetMldInterfaceQueryInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
}

INT4
MldInterfaceQueryIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceQueryInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{

    return (nmhTestv2MldInterfaceStatus (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
MldInterfaceStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetMldInterfaceStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));
}

INT4
MldInterfaceStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &pMultiData->i4_SLongValue));
}

INT4
MldInterfaceVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{

    return (nmhTestv2MldInterfaceVersion (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));
}

INT4
MldInterfaceVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetMldInterfaceVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->u4_ULongValue));
}

INT4
MldInterfaceVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceVersion (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceQuerierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceQuerier (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));
}

INT4
MldInterfaceQueryMaxResponseDelayTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{

    return (nmhTestv2MldInterfaceQueryMaxResponseDelay (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        u4_ULongValue));
}

INT4
MldInterfaceQueryMaxResponseDelaySet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{

    return (nmhSetMldInterfaceQueryMaxResponseDelay
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
}

INT4
MldInterfaceQueryMaxResponseDelayGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceQueryMaxResponseDelay
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceJoinsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceJoins (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceGroupsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceGroups (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceRobustnessTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{

    return (nmhTestv2MldInterfaceRobustness (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));
}

INT4
MldInterfaceRobustnessSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetMldInterfaceRobustness (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));
}

INT4
MldInterfaceRobustnessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceRobustness (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceLastListenQueryIntvlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{

    return (nmhTestv2MldInterfaceLastListenQueryIntvl (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       u4_ULongValue));
}

INT4
MldInterfaceLastListenQueryIntvlSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{

    return (nmhSetMldInterfaceLastListenQueryIntvl
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
}

INT4
MldInterfaceLastListenQueryIntvlGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceLastListenQueryIntvl
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceProxyIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{

    return (nmhTestv2MldInterfaceProxyIfIndex (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));
}

INT4
MldInterfaceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MldInterfaceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MldInterfaceProxyIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetMldInterfaceProxyIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
MldInterfaceProxyIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceProxyIfIndex
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
MldInterfaceQuerierUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceQuerierUpTime
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
MldInterfaceQuerierExpiryTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldInterfaceQuerierExpiryTime
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
MldCacheAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldCacheTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;
    return SNMP_SUCCESS;
}

INT4
MldCacheIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldCacheTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
MldCacheSelfTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{

    return (nmhTestv2MldCacheSelf (pu4Error,
                                   pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->i4_SLongValue));
}

INT4
MldCacheSelfSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetMldCacheSelf (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiIndex->pIndex[1].i4_SLongValue,
                                pMultiData->i4_SLongValue));
}

INT4
MldCacheSelfGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldCacheTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldCacheSelf (pMultiIndex->pIndex[0].pOctetStrValue,
                                pMultiIndex->pIndex[1].i4_SLongValue,
                                &pMultiData->i4_SLongValue));
}

INT4
MldCacheLastReporterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldCacheTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldCacheLastReporter (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->pOctetStrValue));
}

INT4
MldCacheUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldCacheTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldCacheUpTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  &pMultiData->u4_ULongValue));
}

INT4
MldCacheExpiryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldCacheTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldCacheExpiryTime (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
MldCacheStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{

    return (nmhTestv2MldCacheStatus (pu4Error,
                                     pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
MldCacheStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetMldCacheStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiData->i4_SLongValue));
}

INT4
MldCacheStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceMldCacheTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMldCacheStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  &pMultiData->i4_SLongValue));
}

INT4
GetNextIndexMldCacheTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    tSNMP_OCTET_STRING_TYPE *p1mldCacheAddress;
    INT4                i4mldCacheIfIndex;
    p1mldCacheAddress = pNextMultiIndex->pIndex[0].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMldCacheTable (p1mldCacheAddress,
                                           &i4mldCacheIfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMldCacheTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue, p1mldCacheAddress,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4mldCacheIfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4mldCacheIfIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexMldInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    INT4                i4mldInterfaceIfIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMldInterfaceTable (&i4mldInterfaceIfIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMldInterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4mldInterfaceIfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4mldInterfaceIfIndex;
    return SNMP_SUCCESS;
}

INT4
MldCacheTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MldCacheTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
