/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mldin.c,v 1.20 2017/02/06 10:45:28 siva Exp $
 *
 * Description:This file contains the routines required for
 *             processing the  received MLD packet
 *
 *******************************************************************/
#include "igmpinc.h"

/*********************************************************************/
/* Function           : MldInPktHandler                              */
/*                                                                   */
/* Description        : Entry point for the incoming MLD  packets.   */
/*                      Decodes the packet and invokes the           */
/*                      appropriate routine.                         */
/*                                                                   */
/* Input(s)           : MLD buffer pointer,                          */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
MldInPktHandler (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIgmpIface         *pIfNode = NULL;
    tMldParams         *pParms = NULL;
    tIp6Addr           *pIp6Addr = NULL;
    tMldPkt             MldPkt;
    tIPvXAddr           SenderAddr;
    tIPvXAddr           GroupAddr;
    tIPvXAddr           u4SrcAddr;
    tIp6Hdr             Ip6Hdr;
    UINT4               u4IfIndex = MLD_ZERO;
    UINT4               u4Port = MLD_ZERO;
    UINT2               u2MldPktLen = MLD_ZERO;
    UINT1               u1Type = MLD_ZERO;
    UINT1               u1AddrType = IPVX_ADDR_FMLY_IPV6;
    UINT1               u1IsSSMGrp = OSIX_TRUE;
    UINT1               u1IsInvalidSSMGrp = OSIX_TRUE;
    UINT4               u4IpHdrLen = MLD_ZERO;
    UINT4               u4Index = 0;
    INT4                i4Status = MLD_ZERO;
   
    MLD_IPVX_ADDR_CLEAR (&u4SrcAddr);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE), 
		"Entering  MldInPktHandler.\n");

    pParms = (tMldParams *) MLD_GET_PARAMS (pBuf);
    u4IfIndex = pParms->u4IfIndex;
    u2MldPktLen = pParms->u2Len;
    IPVX_ADDR_INIT (SenderAddr, IPVX_ADDR_FMLY_IPV6, &pParms->SrcAddr);

    IGMP_TRC_ARG1 (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD",
                   "Pkt Rcvd, SenderAddr is %s \n",
                   IgmpPrintIPvxAddress (SenderAddr));

    IGMP_DUMP(MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD", pBuf);

    if (gIgmpConfig.u1MldStatus == (UINT1) MLD_DISABLE)
    {
        MLD_TRC (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD", "Failure, Global Status "
                  "Disabled. Pkt dropped.\n");
	IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD", pBuf);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldInPktHandler.\n");
        return;
    }
    if (MldPortGetIpPortFrmIndex (u4IfIndex, &u4Port) == OSIX_FAILURE)
    {
        MLD_TRC_ARG1 (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                       "MldInPktHandler:IPv6 Interface not found for %d. Pkt dropped.\n",
                       u4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting MldInPktHandler.\n");
        return;
    }

    /* Get the interface node from the port */

    pIfNode = MLD_GET_IF_NODE (u4Port, u1AddrType);

    if (pIfNode == NULL)
    {
        MLD_TRC_ARG1 (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                  "MLD is not enabled in this interface %d.\n",u4Port);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
         "Exiting MldInPktHandler.\n");
        return;
    }

    if (u2MldPktLen < sizeof (tMldPkt))
    {
        MLD_TRC_ARG1 (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | IGMP_ALL_FAILURE_TRC,
                       "MLD",
                       "MldInPktHandler:Failure, Pkt size %d rcvd is less than min MLD Pkt. Pkt dropped.\n",
                       u2MldPktLen);
        pIfNode->u4MldMalformedPkt++;
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldInPktHandler.\n");
        return;
    }
    /*As per the RFC-3590 */
    if ((IS_ADDR_UNSPECIFIED (pParms->SrcAddr) == TRUE))
    {
        MLD_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Packet with unspecified source address %s is dropped\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktHandler.\n");
        pIfNode->u4MldSubnetCheckFailure++; 
        return;
    }
    if ((IS_ADDR_LLOCAL (pParms->SrcAddr) == FALSE) &&
        (IS_ADDR_UNSPECIFIED (pParms->SrcAddr) == FALSE))
    {
        MLD_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Failure, Src addr is not link local.Pkt dropped.\n");

        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktHandler.\n");
        pIfNode->u4MldSubnetCheckFailure++;
        return;
    }

    /* If MLD is not enabled on the I/F,  drop the packet. */
    if (pIfNode->u1IfStatus != IGMP_IFACE_UP)
    {
        IGMP_TRC_ARG1 (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                       "Interface (%d) status is down.\n", pIfNode->u4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktHandler.\n");
        pIfNode->u4MldInvalidIntf++;
        return;
    }

     /* Extract the IP header from the buffer */
    if ((i4Status = MLD_EXTRACT_IPV6_HEADER (&Ip6Hdr, pBuf)) == IP_FAILURE)
    {
        pIfNode->u4PktLenError++;
        IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                  "Unable to extract IP header from Control Message Buffer\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting MldInPktHandler.\r \n");
        pIfNode->u4MldMalformedPkt++;
        return;
    }

    MLD_IPVX_ADDR_CLEAR (&u4SrcAddr);
    MLD_IPVX_ADDR_INIT_IPV6 (u4SrcAddr, MLD_IPVX_ADDR_FMLY_IPV6,
                              (UINT1 *) &(Ip6Hdr.srcAddr.ip6_addr_u.u1ByteAddr));

#if defined (IP6_WANTED) && (!defined (LNXIP6_WANTED))
    u4IpHdrLen = Ip6Hdr.u4Head;
    if (Ip6IsOurAddrInCxt (IP6_DEFAULT_CONTEXT, pIp6Addr, &u4Index) == CFA_SUCCESS)
    {
        pIfNode->u4PktWithLocalIP++;
    }
    /* Get Checksum  for MLD packet */
    if (Ip6Checksum (&Ip6Hdr.srcAddr, &Ip6Hdr.dstAddr, u4IpHdrLen, NH_ICMP6, pBuf) == FALSE)
    {
        IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                  "Checksum Failure In the received IGMP packet\n");
        pIfNode->u4CKSumError++;
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
        return;
    }
#else
        UNUSED_PARAM (u4Index);
        UNUSED_PARAM (pIp6Addr);
        UNUSED_PARAM (u4IpHdrLen);
#endif

    /* Read the MLD Header from the buffer */
    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &MldPkt,
                                   0, sizeof (tMldPkt)) == CRU_FAILURE)
    {
        IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Failure Could not" "read the mld hdr\n ");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktHandler.\n");
        return;
    }

    IPVX_ADDR_INIT (GroupAddr, IPVX_ADDR_FMLY_IPV6, &MldPkt.McastAddr);

    IGMP_TRC_ARG1 (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   "Group Address is %s \n", IgmpPrintIPvxAddress (GroupAddr));
    IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",pBuf );
#ifdef MULTICAST_SSM_ONLY 
    if ((u1Type == MLD_DONE) ||
        (u1Type == MLD_REPORT))
    {
        IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Multicast SSM only flag is enabled."
                  "Group not in SSM Range, Ignored !!!!");
        pIfNode->u4DroppedASMIncomingPkts++;
        return;
    }
#endif
    u1Type = MldPkt.u1Type;
    switch (u1Type)
    {
        case MLD_QUERY:

            IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                      "MLD Query rcvd\n");
 
            if ((IS_ADDR_MULTI (MldPkt.McastAddr) == FALSE) &&
                (IS_ADDR_UNSPECIFIED (MldPkt.McastAddr) == FALSE))
            {
                IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC,
                          "MLD",
                          "GrpAddr is not Multicast address\n");
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktHandler.\n");
                pIfNode->u4MldBadScopeErr++;
                return;
            }
            /* chk whether the query is a version 1 query */
            if (u2MldPktLen == (UINT2) MLD_V1_QUERY_LEN)
            {
#ifdef MULTICAST_SSM_ONLY
                 IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Multicast SSM only flag is enabled."
                  "V1 query is Ignored !!!!");
                  pIfNode->u4DroppedASMIncomingPkts++;
#else
                IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC,
                          "MLD", "Processing MLDv1 Query\n");

                MldPktProcessV1Query (pIfNode, GroupAddr, SenderAddr,
                                      0, NULL, 0, MLD_VERSION_1);
	
                if (IPVX_ADDR_COMPARE (GroupAddr, gIPvXZero) != IGMP_ZERO)
                {
                    MLD_GRP_QUERY_RX (pIfNode);
                }
                else
                {
                    MLD_GEN_QUERY_RX (pIfNode);
                }
#endif
            }
            /* the query is a version 2 query */
            else
            {
                IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC,
                          "MLD", "Processing MLDv2 Query\n");
                MldInPktProcessV2Query (pIfNode, pBuf, &SenderAddr);
            }
            break;

        case MLD_REPORT:
	    if (Ip6IsRtrAlertPresent (pBuf) == TRUE)
            {
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
                pIfNode->u4RouterAlertCheckFailure++;
                return;
            }

            if ((IS_ADDR_MULTI (MldPkt.McastAddr) == FALSE) &&
                (IS_ADDR_UNSPECIFIED (MldPkt.McastAddr) == FALSE))
            {
                IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC,
                          "MLD",
                          "GrpAddr is not Multicast address\n");
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldInPktHandler.\n");
                pIfNode->u4MldBadScopeErr++;
                return;
            }
            MLD_CHK_IF_SSM_RANGE (GroupAddr, u1IsSSMGrp)
            MLD_CHK_IF_INVALID_SSM_RANGE (GroupAddr, u1IsInvalidSSMGrp)
            if ((u1IsSSMGrp == OSIX_TRUE) || (u1IsInvalidSSMGrp == OSIX_TRUE))
            {
                pIfNode->u4InvalidV1Report++;
	        pIfNode->u4MldInvalidSSMPkts++;
    		IGMP_TRC_ARG1 (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   "Ignored... Group Address is %s in SSM Range\n",
                   IgmpPrintIPvxAddress (GroupAddr));
		IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   pBuf);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldInPktHandler.\n");
                return;
            }
            IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                      "Processing MLDv1 Report\n");
            MldPktProcessV1Report (pIfNode, GroupAddr, SenderAddr);
            MLD_V1REP_RX (pIfNode);
            break;

        case MLD_V2_REPORT:
            if (Ip6IsRtrAlertPresent (pBuf) == TRUE)
            {
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
                pIfNode->u4RouterAlertCheckFailure++;
                return;
            }
	     
	    if (pIfNode->u1Version == MLD_VERSION_1)
            {
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                          "Received a V2 Report on a MLD V1 Interface\n");\
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC,
                                "MLD",
                                "Received a V2 Report on a MLD V1 Interface\n");
                pIfNode->u4ReportVersionMisMatch++;
                return;
            }

            if (pIfNode->u1Version == MLD_VERSION_2)
            {

                IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC,
                          "MLD", "Processing MLDv2 Report\n");
                MldInPktProcessV2Report (pIfNode, pBuf, u2MldPktLen,
                                         &SenderAddr);
            }
            else
            {
                IGMP_TRC_ARG1 (MLD_TRC_FLAG,
                               IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                               "Mldv2 not enabled in the interface %d\n",
                               pIfNode->u4IfIndex);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldInPktHandler.\n");
		pIfNode->u4InvalidV2Report++;                
		return;
            }
            break;

        case MLD_DONE:

	    if (Ip6IsRtrAlertPresent (pBuf) == TRUE)
            {
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
                IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
                pIfNode->u4RouterAlertCheckFailure++;
                return;
            }
	
            if ((IS_ADDR_MULTI (MldPkt.McastAddr) == FALSE) &&
                (IS_ADDR_UNSPECIFIED (MldPkt.McastAddr) == FALSE))
            {
                IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC,
                          "MLD",
                          "GrpAddr is not Multicast address\n");
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldInPktHandler.\n");
                pIfNode->u4MldBadScopeErr++;
                return;
            }
            MLD_CHK_IF_SSM_RANGE (GroupAddr, u1IsSSMGrp)
            MLD_CHK_IF_INVALID_SSM_RANGE (GroupAddr, u1IsInvalidSSMGrp)
            if ((u1IsSSMGrp == OSIX_TRUE) || (u1IsInvalidSSMGrp == OSIX_TRUE))
            {
                pIfNode->u4MldInvalidSSMPkts++;
		MLD_TRC_ARG1 (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   "Ignored ... Group Address is %s in SSM Range\n",
                   IgmpPrintIPvxAddress (GroupAddr));
    		IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   pBuf);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldInPktHandler.\n");
                return;
            }
            IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                      "Processing MLD Done\n");
            MldPktProcessDone (pIfNode, GroupAddr);
            MLD_DONE_RX (pIfNode);
            break;

        default:
	    pIfNode->u4UnknownMsgType++;
            IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                      "Invalid Packet!!!!!!!!!!!!!!.\n ");
            IGMP_TRC (MLD_TRC_FLAG, ALL_FAILURE_TRC,
                       "MLD",
                      "Invalid Packet!!!!!!!!!!!!!!.\n ");
            break;
    }
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldInPktProcessV2Query                             */
/*                                                                         */
/*     Description   :  This function Process' the MLDv2 Query             */
/*                                                                         */
/*     Input(s)      :  pIfNode - IGMP/MLD Interface node                  */
/*                      pBuf - the Query Message                           */
/*                      pSenderAddr - the Sender's (Host) LLC address      */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
VOID
MldInPktProcessV2Query (tIgmpIface * pIfNode, tCRU_BUF_CHAIN_HEADER * pBuf,
                        tIPvXAddr * pSenderAddr)
{
    tMldV2Query         Mldv2Query;
    tIPvXAddr           GroupAddr;
    UINT1               u1Srsp = IGMP_ZERO;
    UINT1               u1QRV = IGMP_ZERO;
    UINT1               u1QQIC = IGMP_ZERO;
    UINT2               u2NoOfSrcs = IGMP_ZERO;
#ifdef MULTICAST_SSM_ONLY
    UINT1               u1IsSSMGrp = IGMP_ZERO;
#endif
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldInPktProcessV2Query.\n");

    if ((pIfNode->u1Version != MLD_VERSION_2) &&
        ((gIgmpConfig.i1MldProxyStatus != MLD_ENABLE) ||
         (MldpUtilCheckUpIface (pIfNode->u4IfIndex) != OSIX_SUCCESS)))
    {
        pIfNode->u4WrongVerQueries++;

        IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "MldInPktProcessV2Query: Failure, Neither MLDv2 configured nor"
                  " MLD Proxy Enabled on the Intf\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktProcessV2Query.\n");
        return;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &Mldv2Query, 0,
                               sizeof (tMldV2Query));

    if ((gIgmpConfig.i1MldProxyStatus != MLD_ENABLE) ||
        (MldpUtilCheckUpIface (pIfNode->u4IfIndex) != OSIX_SUCCESS))
    {
        u1Srsp = MLD_SRSP ((&Mldv2Query));
        u1QRV = MLD_QRV ((&Mldv2Query));
        pIfNode->pIfaceQry->u1QRV = u1QRV;
        u1QQIC = MLD_QQIC ((&Mldv2Query));
        pIfNode->pIfaceQry->u1QQIC = u1QQIC;
    }

    u2NoOfSrcs = OSIX_NTOHS (Mldv2Query.u2NumSrcs);
    IPVX_ADDR_INIT_FROMV6 (GroupAddr, &(Mldv2Query.McastAddr));
#ifdef MULTICAST_SSM_ONLY
    MLD_CHK_IF_SSM_RANGE (GroupAddr, u1IsSSMGrp)
        if ( u1IsSSMGrp != OSIX_TRUE)
        {
            IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                      "Multicast SSM only flag is enabled."
                      "Group not in SSM Range, Ignored !!!!");
            pIfNode->u4DroppedASMIncomingPkts++;
            return;
        }
#endif
    if (u2NoOfSrcs == IGMP_ZERO)
    {
        IgmpProcessQuery (pIfNode, GroupAddr, *pSenderAddr,
                          0, NULL, u1Srsp, MLD_VERSION_2);
        if (IPVX_ADDR_COMPARE (GroupAddr, gIPvXZero) != IPVX_ZERO)
        {
            MLD_GRP_QUERY_RX (pIfNode);
        }
        else
        {
            MLD_GEN_QUERY_RX (pIfNode);
        }
    }
    else
    {
        MEMSET (gau1Igmpv3Buffer, 0, MLD_MAX_MTU_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, gau1Igmpv3Buffer,
                                   sizeof (tMldV2Query),
                                   (IPVX_IPV6_ADDR_LEN * u2NoOfSrcs));
        IgmpProcessQuery (pIfNode, GroupAddr, *pSenderAddr,
                          u2NoOfSrcs, gau1Igmpv3Buffer, u1Srsp, MLD_VERSION_2);

        MLD_GRP_SRC_QUERY_RX (pIfNode);
    }
    IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
              "MldInPktProcessV2Query: Processed MLDv2 Query\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktProcessV2Query.\n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : MldInPktProcessV2Report                             */
/*                                                                         */
/*     Description   : Process the received V2 Report message.             */
/*                                                                         */
/*     Input(s)      : pIfNode - interface node                            */
/*                     pBuf - Buffer containing the MLD message            */
/*                     u2BufSize - MLD Message size                        */
/*                     pSenderAddr - Sender (Host) address                 */
/*                                                                         */
/*     Output(s)     : None                                                */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
VOID
MldInPktProcessV2Report (tIgmpIface * pIfNode, tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT2 u2BufSize, tIPvXAddr * pSenderAddr)
{
    tMldV2Report       *pReport = NULL;
    tMldGroupRecord    *pGroupRec = NULL;
    UINT1              *pu1Sources = NULL;
    UINT1              *pu1BufPtr = NULL;
    tIPvXAddr           GrpAddress;
    UINT2               u2TotalGrpRecs = 0;
    UINT2               u2Offset = 0;
    UINT2               u2NoOfSources = 0;
    UINT4               u4RecNum = 0;
    UINT1               u1GrpRecType = 0;
    UINT1               u1IsSSMGrp = OSIX_TRUE;
    UINT1               u1IsInvalidSSMGrp = OSIX_TRUE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktProcessV2Report.\n");
    if ((gIgmpConfig.i1MldProxyStatus == MLD_ENABLE) &&
        (MldpUtilCheckUpIface (pIfNode->u4IfIndex) == OSIX_SUCCESS))
    {
        /* MLD Proxy is enabled and the interface is configures 
         * as an upstream interface. No reports should be processed 
         * on upstream interface. so return. */
        MLD_TRC_ARG1 (MLD_TRC_FLAG, MLD_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Failure, Neither MLDv2 configured nor"
                  " MLD Proxy Enabled on the Intf %d\n", pIfNode->u4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktProcessV2Report.\n");
        return;
    }

    if (u2BufSize < sizeof (tMldV2Report))
    {
        MLD_TRC (MLD_TRC_FLAG, MLD_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Failure, Bufsize is less than report");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktProcessV2Report.\n");
        return;
    }

    pu1BufPtr = gau1Igmpv3Buffer;
    CRU_BUF_Copy_FromBufChain (pBuf, pu1BufPtr, 0, u2BufSize);

    pReport = (tMldV2Report *) (VOID *) pu1BufPtr;
    u2BufSize -= sizeof (tMldV2Report);

    u2TotalGrpRecs = OSIX_NTOHS (pReport->u2NumGrps);
    u2Offset = MLD_HEADER_LEN;

    /* Processing all Group Records Present in the V3 Report */
    for (u4RecNum = IGMP_ZERO; u4RecNum < u2TotalGrpRecs; u4RecNum++)
    {
        pGroupRec = (tMldGroupRecord *) (VOID *) (pu1BufPtr + u2Offset);
        if (u2BufSize < sizeof (tMldGroupRecord))
        {
            break;
        }
        u2Offset += sizeof (tMldGroupRecord);
        u2BufSize -= sizeof (tMldGroupRecord);

        IPVX_ADDR_INIT_FROMV6 (GrpAddress, &pGroupRec->GroupAddr);
        u2NoOfSources = OSIX_NTOHS (pGroupRec->u2NumSrcs);
        u1GrpRecType = pGroupRec->u1RecordType;
        pu1Sources = pu1BufPtr + u2Offset;

        /* Check for Group Address */
        if ((IS_ADDR_MULTI (pGroupRec->GroupAddr) == IGMP_FALSE) ||
	    	(MLD_IS_ADDR_LINK_SCOPE_MULTI (pGroupRec->GroupAddr)))
        {
            /* Invalid Group Destination Addres */
            u2Offset += (u2NoOfSources * sizeof (tIp6Addr));
            u2BufSize -= (sizeof (tIp6Addr) * u2NoOfSources);
    	    MLD_TRC_ARG1 (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   "Group Address is %s not in Multicast Range\n",
                                        IgmpPrintIPvxAddress (GrpAddress));
	    IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   pBuf);
	    pIfNode->u4MldBadScopeErr++;
            continue;
        }

        if ((u2NoOfSources != 0) &&
            (u2BufSize < (sizeof (tIp6Addr) * u2NoOfSources)))
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktProcessV2Report.\n");
            return;
        }

        MLD_CHK_IF_INVALID_SSM_RANGE (GrpAddress, u1IsInvalidSSMGrp)
        if (u1IsInvalidSSMGrp == OSIX_TRUE)
        {
	    pIfNode->u4MldInvalidSSMPkts++;
    	    IGMP_TRC_ARG1 (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
               "Ignored... Group Address is %s in invalid SSM Range\n",
               IgmpPrintIPvxAddress (GrpAddress));
	    IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
               pBuf);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
	    	 "Exiting MldInPktProcessV2Report.\n");
            return;
        }

        MLD_CHK_IF_SSM_RANGE (GrpAddress, u1IsSSMGrp)
#ifdef MULTICAST_SSM_ONLY
        if (u1IsSSMGrp != OSIX_TRUE)
        {
             IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                           "Multicast SSM only flag is enabled."
                           "Group %s not in SSM Range, Ignored !!!!\n",
                           IgmpPrintIPvxAddress (GrpAddress));
            pIfNode->u4DroppedASMIncomingPkts++;
            continue;
        }
#endif
        if (u1IsSSMGrp == OSIX_TRUE)
        {
	    pIfNode->u4MldInSSMPkts++;
    	    IGMP_TRC_ARG1 (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   "Group Address is %s in SSM Range\n", IgmpPrintIPvxAddress (GrpAddress));
	    IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   pBuf);
            /* Ignore a group record of either of the following mode type in SSM range dest address */
            if((u1GrpRecType == IGMP_GRP_CHANGE_TO_EXCLUDE_MODE) || (u1GrpRecType == IGMP_GRP_MODE_IS_EXCLUDE))
            {
                IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC,
                          "MLD", "Ignored Group record for types CHANGE_TO_EXCLUDE and IS_EXCLUDE mode for SSM rangen\n");
                pIfNode->u4MldInvalidSSMPkts++;
                pIfNode->u4InvalidV2Report++;
                return;
            }

        }

        MLD_TRC (MLD_TRC_FLAG, MLD_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
                  "Process MLDv2 Grp record\n");
        MldPktProcessV2GroupRecord (pIfNode, GrpAddress, u1GrpRecType,
                                    pu1Sources, u2NoOfSources, *pSenderAddr);

        u2BufSize -= (sizeof (tIp6Addr) * u2NoOfSources);
        u2Offset += (sizeof (tIp6Addr) * u2NoOfSources);

    }

    IGMP_TRC (MLD_TRC_FLAG, IGMP_INIT_SHUT_TRC | ALL_FAILURE_TRC, "MLD",
              "Processed MLDv2 report\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldInPktProcessV2Report.\n");
    MLD_V2REP_RX (pIfNode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MldIpExtractHdr                                            */
/*                                                                           */
/* Description  : Function to extract the IPV6 header                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP_SUCCESS / IP_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
MldIpExtractHdr (tIp6Hdr * pIp6Hdr, tCRU_BUF_CHAIN_HEADER * pBuf)
{

    tIp6Hdr             TmpIp6Hdr;
    tIp6Hdr            *pTmpIp6Hdr;
    MEMSET (&TmpIp6Hdr, 0, sizeof (tIp6Hdr));

    pTmpIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                           sizeof (tIp6Hdr));
    if (pTmpIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pTmpIp6Hdr = &TmpIp6Hdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pTmpIp6Hdr,
                                   0, sizeof (tIp6Hdr));
    }

    MEMCPY (pIp6Hdr, pTmpIp6Hdr, sizeof (tIp6Hdr));
    pIp6Hdr->u2Len = OSIX_NTOHS (pIp6Hdr->u2Len);
    return IP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : Ip6IsRtrAlertPresent                                 */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of Router Alert Option        */
/*                                                                           */
/* Input(s)           : pBuf            - IPv6 Message Buffer                */
/*                                                                           */
/*                    : pIp6            - IPv6 Header Pointer                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu4BalanceBytes  - balance remaining bytes           */
/*                                                                           */
/*                    : pu1IfsMldRtrAlert- Flag to denote the presence of    */
/*                                         Router Alert Option               */
/*                                                                           */
/*                    : pu1OptValue      - Next Option Value                 */
/*                                                                           */
/*****************************************************************************/
INT4
Ip6IsRtrAlertPresent (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tMldRtAlertHdr      RtAlertHdr;

    if (MldBufRead (pBuf, (UINT1 *) &RtAlertHdr, MLD_BUF_READ_OFFSET (pBuf),
                      sizeof (tMldRtAlertHdr)) == NULL)
    {
        return FALSE;
    }

    if (RtAlertHdr.u2OptValue == ICMP6_MLD_PKT)
    {
        return TRUE;
    }
    return FALSE;
}

/*****************************************************************************/
/* Function Name      : MldBufRead                                           */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of BufRead                    */
/*                                                                           */
/*****************************************************************************/

VOID               *
MldBufRead (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset,
              UINT4 u4Size)
{
    /* check if a copy is requested */

        if (CRU_BUF_Copy_FromBufChain (pBuf, pData, u4Offset, u4Size) == 0)
        {
            return (NULL);
        }
        MLD_BUF_READ_OFFSET (pBuf) = MLD_BUF_READ_OFFSET (pBuf) + u4Size;
        return ((VOID *) pData);

}

