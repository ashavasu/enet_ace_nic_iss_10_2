/******************************************************************************/
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsutlget.c,v 1.18 2017/02/06 10:45:27 siva Exp $
 *
 * Description: : Action routines for set/get objects in           
 *                           fsigmp.mib                           
 *
 *******************************************************************/

# include  "include.h"
# include  "fsigmcon.h"
# include  "fsigmogi.h"
# include  "midconst.h"
# include  "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE;
#endif
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsIgmpGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpGlobalStatus (INT4 *pi4RetValFsIgmpGlobalStatus,
                                      INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpGlobalStatus()\r \n");
    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        *pi4RetValFsIgmpGlobalStatus = gIgmpConfig.u1IgmpStatus;
         MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting IgmpMgmtUtilNmhGetFsIgmpGlobalStatus()\r \n");
        return SNMP_SUCCESS;
    }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        *pi4RetValFsIgmpGlobalStatus = gIgmpConfig.u1MldStatus;
         MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting IgmpMgmtUtilNmhGetFsIgmpGlobalStatus()\r \n");
        return SNMP_SUCCESS;
    }
     MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting IgmpMgmtUtilNmhGetFsIgmpGlobalStatus()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpSSMMapStatus
 Input       :  The Indices

                The Object
                retValFsIgmpSSMMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpSSMMapStatus (INT4 *pi4RetValFsIgmpSSMMapStatus,
                                        INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpSSMMapStatus()\r \n");

    *pi4RetValFsIgmpSSMMapStatus = (INT4) IGMP_SSM_MAP_GLOBAL_STATUS();

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting IgmpMgmtUtilNmhGetFsIgmpSSMMapStatus()\r \n");
    UNUSED_PARAM (i4FsIgmpAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpTraceLevel
 Input       :  The Indices

                The Object 
                retValFsIgmpTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpTraceLevel (INT4 *pi4RetValFsIgmpTraceLevel,
                                    INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpTraceLevel()\r \n");
    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        *pi4RetValFsIgmpTraceLevel = (INT4) gIgmpConfig.u4IgmpTrcFlag;
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpTraceLevel()\r \n");
	return SNMP_SUCCESS;
    }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        *pi4RetValFsIgmpTraceLevel = (INT4) gIgmpConfig.u4MldTrcFlag;
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpTraceLevel()\r \n");       
         return SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting IgmpMgmtUtilNmhGetFsIgmpTraceLevel()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpDebugLevel
 Input       :  The Indices

                The Object
                retValFsIgmpDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpDebugLevel (INT4 *pi4RetValFsIgmpDebugLevel,
                                    INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpDebugLevel()\r \n");
    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        *pi4RetValFsIgmpDebugLevel = (INT4) gIgmpConfig.u4IgmpDbgFlag;
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpDebugLevel()\r \n");
	return SNMP_SUCCESS;                                                                                          }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        *pi4RetValFsIgmpDebugLevel = (INT4) gIgmpConfig.u4MldDbgFlag;
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpDebugLevel()\r \n");
        return SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpDebugLevel()\r \n");
    return SNMP_SUCCESS;
}


/* LOW LEVEL Routines for Table : FsIgmpInterfaceTable. */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpInterfaceTable (INT4
                                                          i4FsIgmpInterfaceIfIndex,
                                                          INT4
                                                          i4FsIgmpInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpInterfaceTable()\r \n");
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable
        (i4FsIgmpInterfaceIfIndex, i4FsIgmpInterfaceAddrType);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpInterfaceTable()\r \n");
    return i1Status;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFirstIndexFsIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpInterfaceTable (INT4
                                                  *pi4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  *pi4FsIgmpInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFirstIndexFsIgmpInterfaceTable()\r \n");
    i1Status = IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable
        (pi4FsIgmpInterfaceIfIndex, pi4FsIgmpInterfaceAddrType);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFirstIndexFsIgmpInterfaceTable()\r \n");
    return i1Status;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetNextIndexFsIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIndex
                nextIgmpInterfaceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpInterfaceTable (INT4 i4FsIgmpInterfaceIfIndex,
                                                 INT4 i4FsIgmpInterfaceAddrType,
                                                 INT4
                                                 *pi4NextFsIgmpInterfaceIfIndex,
                                                 INT4
                                                 *pi4FsIgmpInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetNextIndexFsIgmpInterfaceTable()\r \n");

    i1Status =
        IgmpMgmtUtilNmhGetNextIndexIgmpInterfaceTable (i4FsIgmpInterfaceIfIndex,
                                                       pi4NextFsIgmpInterfaceIfIndex,
                                                       i4FsIgmpInterfaceAddrType,
                                                       pi4FsIgmpInterfaceAddrType);
    
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetNextIndexFsIgmpInterfaceTable()\r \n");
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              INT4
                                              *pi4RetValFsIgmpInterfaceAdminStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus()\r \n");
    /* Get the iface node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Ifacenode not exist with index %d", i4FsIgmpInterfaceIfIndex);
	CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus()\r \n");	
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pi4RetValFsIgmpInterfaceAdminStatus = pIfaceNode->u1AdminStatus;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Admin-Status of i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pi4RetValFsIgmpInterfaceAdminStatus);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus()\r \n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceFastLeaveStatus
 Output      :  The Get Low Lev Routine Take the Indices &
q
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  INT4
                                                  *pi4RetValFsIgmpInterfaceFastLeaveStatus)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus()\r \n");
    /* Get the iface node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);	
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n",
		  i4FsIgmpInterfaceIfIndex);
        *pi4RetValFsIgmpInterfaceFastLeaveStatus = IGMP_FAST_LEAVE_DISABLE;

        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
	*pi4RetValFsIgmpInterfaceFastLeaveStatus = (INT4)
            pIfaceNode->u1FastLeaveFlg;
	MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP Fast leave status of i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex, 
		       *pi4RetValFsIgmpInterfaceFastLeaveStatus);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus()\r \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceOperStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceOperStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                             INT4 i4FsIgmpInterfaceAddrType,
                                             INT4
                                             *pi4RetValFsIgmpInterfaceOperStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus()\r \n");
    /* Get the iface node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pi4RetValFsIgmpInterfaceOperStatus = pIfaceNode->u1OperStatus;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Oper-Status of i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pi4RetValFsIgmpInterfaceOperStatus);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceOperStatus()\r \n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingPkts
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingPkts (INT4 i4FsIgmpInterfaceIfIndex,
                                               INT4 i4FsIgmpInterfaceAddrType,
                                               UINT4
                                               *pu4RetValFsIgmpInterfaceIncomingPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingPkts()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceIncomingPkts = pIfaceNode->u4InPkts;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Number of incoming pkts on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceIncomingPkts);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingPkts()\r \n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingJoins
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingJoins (INT4 i4FsIgmpInterfaceIfIndex,
                                                INT4 i4FsIgmpInterfaceAddrType,
                                                UINT4
                                                *pu4RetValFsIgmpInterfaceIncomingJoins)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingJoins()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceIncomingJoins = pIfaceNode->u4InJoins;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Incoming Joins on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceIncomingJoins);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingJoins()\r \n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingLeaves
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingLeaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingLeaves (INT4 i4FsIgmpInterfaceIfIndex,
                                                 INT4 i4FsIgmpInterfaceAddrType,
                                                 UINT4
                                                 *pu4RetValFsIgmpInterfaceIncomingLeaves)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingLeaves()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceIncomingLeaves = pIfaceNode->u4InLeaves;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Incoming leaves on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceIncomingLeaves);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingLeaves()\r \n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingQueries
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                                  UINT1
                                                  i4FsIgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValFsIgmpInterfaceIncomingQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceIncomingQueries = pIfaceNode->u4InQueries;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Incoming Queries on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceIncomingQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingQueries()\r \n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceOutgoingQueries
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceOutgoingQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceOutgoingQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValFsIgmpInterfaceOutgoingQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceOutgoingQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceOutgoingQueries = pIfaceNode->u4OutQueries;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Outgoing Queries on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceOutgoingQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceOutgoingQueries()\r \n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGenQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxGenQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGenQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                               INT4 i4FsIgmpInterfaceAddrType,
                                               UINT4
                                               *pu4RetValFsIgmpInterfaceRxGenQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGenQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceRxGenQueries = pIfaceNode->u4RxGenQueries;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                       "Incoming General Queries on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceRxGenQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGenQueries()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxGrpQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                               INT4 i4FsIgmpInterfaceAddrType,
                                               UINT4
                                               *pu4RetValFsIgmpInterfaceRxGrpQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceRxGrpQueries = pIfaceNode->u4RxGrpQueries;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                       "Incoming Group specific Queries on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceRxGrpQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpQueries()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpAndSrcQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxGrpAndSrcQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpAndSrcQueries (INT4
                                                     i4FsIgmpInterfaceIfIndex,
                                                     INT4
                                                     i4FsIgmpInterfaceAddrType,
                                                     UINT4
                                                     *pu4RetValFsIgmpInterfaceRxGrpAndSrcQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpAndSrcQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceRxGrpAndSrcQueries =
            pIfaceNode->u4RxGrpSrcQueries;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Incoming Group and source specific Queries"
                       " on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceRxGrpAndSrcQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpAndSrcQueries()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxv1v2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                                INT4 i4FsIgmpInterfaceAddrType,
                                                UINT4
                                                *pu4RetValFsIgmpInterfaceRxv1v2Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValFsIgmpInterfaceRxv1v2Reports =
                pIfaceNode->u4IgmpRxv1v2Reports;
	    IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                           "Incoming IGMP v1/v2 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceRxv1v2Reports);
            i1Status = SNMP_SUCCESS;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceRxv1v2Reports =
                pIfaceNode->u4MldRxv1Reports;
            IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                           "Incoming MLD v1 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceRxv1v2Reports);
            i1Status = SNMP_SUCCESS;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxv3Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValFsIgmpInterfaceRxv3Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValFsIgmpInterfaceRxv3Reports =
                pIfaceNode->u4IgmpRxv3Reports;
	    IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                           "Incoming IGMP v3 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceRxv3Reports);
            i1Status = SNMP_SUCCESS;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceRxv3Reports = pIfaceNode->u4MldRxv2Reports;
	    IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                           "Incoming IGMP v3 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceRxv3Reports);
            i1Status = SNMP_SUCCESS;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGenQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxGenQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGenQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                               INT4 i4FsIgmpInterfaceAddrType,
                                               UINT4
                                               *pu4RetValFsIgmpInterfaceTxGenQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGenQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceTxGenQueries = pIfaceNode->u4TxGenQueries;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                       "Outgoing general queries on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceTxGenQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGenQueries()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxGrpQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                               INT4 i4FsIgmpInterfaceAddrType,
                                               UINT4
                                               *pu4RetValFsIgmpInterfaceTxGrpQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceTxGrpQueries = pIfaceNode->u4TxGrpQueries;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                       "Outgoing group specific queries on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceTxGrpQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpQueries()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpAndSrcQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxGrpAndSrcQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpAndSrcQueries (INT4
                                                     i4FsIgmpInterfaceIfIndex,
                                                     INT4
                                                     i4FsIgmpInterfaceAddrType,
                                                     UINT4
                                                     *pu4RetValFsIgmpInterfaceTxGrpAndSrcQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpAndSrcQueries()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceTxGrpAndSrcQueries =
            pIfaceNode->u4TxGrpSrcQueries;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                       "Outgoing group and source specific queries"
                       " on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceTxGrpAndSrcQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpAndSrcQueries()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv1v2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                                INT4 i4FsIgmpInterfaceAddrType,
                                                UINT4
                                                *pu4RetValFsIgmpInterfaceTxv1v2Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValFsIgmpInterfaceTxv1v2Reports =
                pIfaceNode->u4IgmpTxv1v2Reports;
	    IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                           "Outgoing IGMP v1/v2 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceTxv1v2Reports);
            i1Status = SNMP_SUCCESS;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceTxv1v2Reports =
                pIfaceNode->u4MldTxv1Reports;
	    IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                           "Outgoing IGMP v1/v2 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceTxv1v2Reports);
            i1Status = SNMP_SUCCESS;
        }

    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv3Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValFsIgmpInterfaceTxv3Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValFsIgmpInterfaceTxv3Reports =
                pIfaceNode->u4IgmpTxv3Reports;
            IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                           "Outgoing IGMP v3 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceTxv3Reports);
            i1Status = SNMP_SUCCESS;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceTxv3Reports = pIfaceNode->u4MldTxv2Reports;
            MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "Outgoing IGMP v3 reports on i/f %d is %d \n",
                           i4FsIgmpInterfaceIfIndex,
                           *pu4RetValFsIgmpInterfaceTxv3Reports);
            i1Status = SNMP_SUCCESS;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv2Leaves
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv2Leaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv2Leaves (INT4 i4FsIgmpInterfaceIfIndex,
                                             INT4 i4FsIgmpInterfaceAddrType,
                                             UINT4
                                             *pu4RetValFsIgmpInterfaceTxv2Leaves)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv2Leaves()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceTxv2Leaves = pIfaceNode->u4Txv2Leaves;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                       "Outgoing IGMP v2 Leaves on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceTxv2Leaves);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv2Leaves()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceGroupListId
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv2Leaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceGroupListId (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              *pi4RetValFsIgmpInterfaceGroupListId)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceGroupListId()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pi4RetValFsIgmpInterfaceGroupListId = (INT4) pIfaceNode->u4GrpListId;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "GroupListid mapped on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pi4RetValFsIgmpInterfaceGroupListId);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceGroupListId()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceLimit 
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv2Leaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceLimit (INT4 i4FsIgmpInterfaceIfIndex,
                                        INT4 i4FsIgmpInterfaceAddrType,
                                        UINT4 *pu4RetValFsIgmpInterfaceLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceLimit()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceLimit = pIfaceNode->u4GrpLimit;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Grouplimit on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceLimit);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceLimit()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceCurGrpCount
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv2Leaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceCurGrpCount (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValFsIgmpInterfaceCurGrpCount)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceCurGrpCount()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceCurGrpCount = pIfaceNode->u4GrpCurCnt;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "CurrentGroupCount on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceCurGrpCount);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceCurGrpCount()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceCKSumError
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceCKSumError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceCKSumError (INT4 i4FsIgmpInterfaceIfIndex,
                                             INT4 i4FsIgmpInterfaceAddrType,
                                             UINT4
                                             *pu4RetValFsIgmpInterfaceCKSumError)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceCKSumError()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceCKSumError = pIfaceNode->u4CKSumError;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Packets with checksum error on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceCKSumError);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceCKSumError()\r \n");
    return (i1Status);
}

/****************************************************************************
Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfacePktLenError
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfacePktLenError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfacePktLenError (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValFsIgmpInterfacePktLenError)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfacePktLenError()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfacePktLenError = pIfaceNode->u4PktLenError;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                       "Packets received with incorrect packet length on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfacePktLenError);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfacePktLenError()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfacePktsWithLocalIP
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfacePktsWithLocalIP 
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfacePktsWithLocalIP (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValFsIgmpInterfacePktsWithLocalIP)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfacePktsWithLocalIP()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfacePktsWithLocalIP = pIfaceNode->u4PktWithLocalIP;
        IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),       
		"Packets received on i/f %d with local IP address as source address is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfacePktsWithLocalIP);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfacePktsWithLocalIP()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceSubnetCheckFailure
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceSubnetCheckFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceSubnetCheckFailure (INT4
                                                     i4FsIgmpInterfaceIfIndex,
                                                     INT4
                                                     i4FsIgmpInterfaceAddrType,
                                                     UINT4
                                                     *pu4RetValFsIgmpInterfaceSubnetCheckFailure)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceSubnetCheckFailure()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {

            *pu4RetValFsIgmpInterfaceSubnetCheckFailure =
                pIfaceNode->u4SubnetCheckFailure;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceSubnetCheckFailure =
                pIfaceNode->u4MldSubnetCheckFailure;
        }
	    IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                       "Packets dropped due to subnet check failure on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceSubnetCheckFailure);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceSubnetCheckFailure()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceQryFromNonQuerier
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceQryFromNonQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceQryFromNonQuerier (INT4
                                                    i4FsIgmpInterfaceIfIndex,
                                                    INT4
                                                    i4FsIgmpInterfaceAddrType,
                                                    UINT4
                                                    *pu4RetValFsIgmpInterfaceQryFromNonQuerier)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceQryFromNonQuerier()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceQryFromNonQuerier =
            pIfaceNode->u4QryFromNonQuerier;
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Number of queries from non-querier on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceQryFromNonQuerier);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceQryFromNonQuerier()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceReportVersionMisMatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch (INT4
                                                        i4FsIgmpInterfaceIfIndex,
                                                        INT4
                                                        i4FsIgmpInterfaceAddrType,
                                                        UINT4
                                                        *pu4RetValFsIgmpInterfaceReportVersionMisMatch)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceReportVersionMisMatch =
            pIfaceNode->u4ReportVersionMisMatch;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                       "Packets received on i/f %d with report version mismatch is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceReportVersionMisMatch);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceQryVersionMisMatch
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceQryVersionMisMatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceQryVersionMisMatch (INT4
                                                     i4FsIgmpInterfaceIfIndex,
                                                     INT4
                                                     i4FsIgmpInterfaceAddrType,
                                                     UINT4
                                                     *pu4RetValFsIgmpInterfaceQryVersionMisMatch)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceQryVersionMisMatch()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceQryVersionMisMatch =
            pIfaceNode->u4QryVersionMisMatch;
        IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),               
		"Packets received on i/f %d with query version mismatch is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceQryVersionMisMatch);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceUnknownMsgType
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceUnknownMsgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceUnknownMsgType (INT4 i4FsIgmpInterfaceIfIndex,
                                                 INT4 i4FsIgmpInterfaceAddrType,
                                                 UINT4
                                                 *pu4RetValFsIgmpInterfaceUnknownMsgType)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceUnknownMsgType()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceUnknownMsgType = pIfaceNode->u4UnknownMsgType;
        IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),       
		"Packets received on i/f %d with unknown message type is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceUnknownMsgType);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceUnknownMsgType()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV1Report
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceInvalidV1Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV1Report (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValFsIgmpInterfaceInvalidV1Report)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV1Report()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceInvalidV1Report =
            pIfaceNode->u4InvalidV1Report;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                       "Invalid V1 report received on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceInvalidV1Report);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV1Report()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV2Report
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceInvalidV2Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV2Report (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValFsIgmpInterfaceInvalidV2Report)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV2Report()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceInvalidV2Report =
            pIfaceNode->u4InvalidV2Report;
        IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),       
		"Invalid V2 report received on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceInvalidV2Report);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV2Report()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV3Report
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceInvalidV3Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV3Report (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValFsIgmpInterfaceInvalidV3Report)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV3Report()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceInvalidV3Report =
            pIfaceNode->u4InvalidV3Report;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                       "Invalid V3 report received on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceInvalidV3Report);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV3Report()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceRouterAlertCheckFailure
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceRouterAlertCheckFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceRouterAlertCheckFailure (INT4
                                                          i4FsIgmpInterfaceIfIndex,
                                                          INT4
                                                          i4FsIgmpInterfaceAddrType,
                                                          UINT4
                                                          *pu4RetValFsIgmpInterfaceRouterAlertCheckFailure)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceRouterAlertCheckFailure()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValFsIgmpInterfaceRouterAlertCheckFailure =
            pIfaceNode->u4RouterAlertCheckFailure;
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                       "Packets dropped on i/f %d due to router alert check failure is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceRouterAlertCheckFailure);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceRouterAlertCheckFailure()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceChannelTrackStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceChannelTrackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceChannelTrackStatus (INT4
                                                     i4FsIgmpInterfaceIfIndex,
                                                     INT4
                                                     i4FsIgmpInterfaceAddrType,
                                                     INT4
                                                     *pi4RetValFsIgmpInterfaceChannelTrackStatus)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceChannelTrackStatus\r \n");
    /* Get the iface node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pi4RetValFsIgmpInterfaceChannelTrackStatus = (INT4)
            pIfaceNode->u1ChannelTrackFlg;
	MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP channel track status of i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pi4RetValFsIgmpInterfaceChannelTrackStatus);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceChannelTrackStatus()\r \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingSSMPkts
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingSSMPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingSSMPkts (INT4 i4FsIgmpInterfaceIfIndex,
                                               INT4 i4FsIgmpInterfaceAddrType,
                                               UINT4
                                               *pu4RetValFsIgmpInterfaceIncomingSSMPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingSSMPkts()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
	if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
	{
	    *pu4RetValFsIgmpInterfaceIncomingSSMPkts = 
				pIfaceNode->u4IgmpInSSMPkts;
	}
	else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
	{
	    *pu4RetValFsIgmpInterfaceIncomingSSMPkts = 
				pIfaceNode->u4MldInSSMPkts;
	}
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Number of incoming SSM pkts on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceIncomingSSMPkts);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingSSMPkts()\r \n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidSSMPkts
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceInvalidSSMPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidSSMPkts (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValFsIgmpInterfaceInvalidSSMPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidSSMPkts()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
	if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
	{
	    *pu4RetValFsIgmpInterfaceInvalidSSMPkts = 
				pIfaceNode->u4IgmpInvalidSSMPkts;
	}
	else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
	{
	    *pu4RetValFsIgmpInterfaceInvalidSSMPkts = 
				pIfaceNode->u4MldInvalidSSMPkts;
	}
	IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                       "Invalid V3 report received on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceInvalidSSMPkts);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidSSMPkts()\r \n");
    return (i1Status);
}


/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpJoinPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpJoinPktRate (INT4 i4FsIgmpAddrType,
                                              INT4
                                              *pi4RetValFsIgmpJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;

    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
	*pi4RetValFsIgmpJoinPktRate = 
	    (INT4) gIgmpConfig.i4GlobalIgmpRateLimit;

	i1Status = SNMP_SUCCESS;
    }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
	*pi4RetValFsIgmpJoinPktRate = 
	    (INT4) gIgmpConfig.i4GlobalMldRateLimit;

 	i1Status = SNMP_SUCCESS;
    }
    return (i1Status);
}


/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceJoinPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceJoinPktRate (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              INT4
                                              *pi4RetValFsIgmpInterfaceJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceJoinPktRate()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
	if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
	{
	    *pi4RetValFsIgmpInterfaceJoinPktRate = 
				(INT4) pIfaceNode->u4IgmpJoinPktRate;
	}
	else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
	{
	    *pi4RetValFsIgmpInterfaceJoinPktRate = 
				(INT4) pIfaceNode->u4MldJoinPktRate;
	}
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Packet rate on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pi4RetValFsIgmpInterfaceJoinPktRate);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceJoinPktRate()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceMalformedPkts
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceMalformedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceMalformedPkts (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4 
					      *pu4RetValFsIgmpInterfaceMalformedPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceMalformedPkts()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValFsIgmpInterfaceMalformedPkts =
                                 pIfaceNode->u4IgmpMalformedPkt;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceMalformedPkts =
                                (INT4) pIfaceNode->u4MldMalformedPkt;
        }
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Packet rate on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceMalformedPkts);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceMalformedPkts()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceSocketErrors
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceSocketErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceSocketErrors (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValFsIgmpInterfaceSocketErrors)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceSocketErrors()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValFsIgmpInterfaceSocketErrors =
                                 pIfaceNode->u4IgmpSocketErr;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceSocketErrors =
                                 pIfaceNode->u4MldSocketErr;
        }
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Packet rate on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceSocketErrors);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceSocketErrors()\r \n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpInterfaceBadScopeErrors
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceBadScopeErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpInterfaceBadScopeErrors (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValFsIgmpInterfaceBadScopeErrors)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpInterfaceBadScopeErrors()\r \n");
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4FsIgmpInterfaceIfIndex);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValFsIgmpInterfaceBadScopeErrors =
                                 pIfaceNode->u4BadScopeErr;
        }
        else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            *pu4RetValFsIgmpInterfaceBadScopeErrors =
                                 pIfaceNode->u4MldBadScopeErr;
        }
        MGMD_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Packet rate on i/f %d is %d \n",
                       i4FsIgmpInterfaceIfIndex,
                       *pu4RetValFsIgmpInterfaceBadScopeErrors);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpInterfaceBadScopeErrors()\r \n");
    return (i1Status);
}

/* LOW LEVEL Routines for Table : FsIgmpCacheTable. */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                                                      tIgmpIPvXAddr
                                                      u4IgmpCacheAddress,
                                                      INT4 i4IgmpCacheIfIndex)
{

    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4Port = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable()\r \n");
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex,
                                i4IgmpCacheAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
         IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Port number not found for index %d \r\n",i4IgmpCacheIfIndex);	
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable()\r \n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface node not found for port %d \r\n",u4Port);
        return SNMP_FAILURE;
    }

    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable (i4IgmpCacheAddrType,
                                                            u4IgmpCacheAddress,
                                                            i4IgmpCacheIfIndex);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable()\r \n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFirstIndexFsIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpCacheTable (INT4 *pi4IgmpCacheAddrType,
                                              tIgmpIPvXAddr *
                                              pu4IgmpCacheAddress,
                                              INT4 *pi4IgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFirstIndexFsIgmpCacheTable()\r \n");

    i1Status = IgmpMgmtUtilNmhGetFirstIndexIgmpCacheTable
                    (pi4IgmpCacheAddrType, pu4IgmpCacheAddress, pi4IgmpCacheIfIndex);

        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFirstIndexFsIgmpCacheTable()\r \n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetNextIndexFsIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                nextIgmpCacheAddress
                IgmpCacheIfIndex
                nextIgmpCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                                             INT4 *pi4IgmpCacheAddrType,
                                             tIgmpIPvXAddr u4IgmpCacheAddress,
                                             tIgmpIPvXAddr *
                                             pu4NextIgmpCacheAddress,
                                             INT4 i4IgmpCacheIfIndex,
                                             INT4 *pi4NextIgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetNextIndexFsIgmpCacheTable()\r \n");

    i1Status = IgmpMgmtUtilNmhGetNextIndexIgmpCacheTable
                    (i4IgmpCacheAddrType, pi4IgmpCacheAddrType,
                     u4IgmpCacheAddress, pu4NextIgmpCacheAddress,
                     i4IgmpCacheIfIndex, pi4NextIgmpCacheIfIndex);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetNextIndexFsIgmpCacheTable()\r \n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpCacheGroupCompMode
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValFsIgmpCacheGroupCompMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpCacheGroupCompMode (INT4 i4IgmpCacheAddrType,
                                            tIgmpIPvXAddr u4IgmpCacheAddress,
                                            INT4 i4IgmpCacheIfIndex,
                                            INT4
                                            *pi4RetValFsIgmpCacheGroupCompMode)
{
    INT1                i1Status = IGMP_ZERO;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpCacheGroupCompMode()\r \n");
    i1Status = SNMP_FAILURE;

    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex,
                                i4IgmpCacheAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface doesn't exist with index %d \r\n",i4IgmpCacheIfIndex);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG (IGMP_DBG_EXIT, "GetIgmpCacheGCM routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IfNode not found for port %d \r\n",u4Port);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        *pi4RetValFsIgmpCacheGroupCompMode = pGrp->u4GroupCompMode;
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Cache Group Comp Mode of IGMP cache Address 0x%s "
                   "and i/f %d is %d\n",
                   IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                   i4IgmpCacheIfIndex, *pi4RetValFsIgmpCacheGroupCompMode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpCacheGroupCompMode()\r \n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpGlobalLimit
 Input       :  The Indices

                The Object 
                retValFsIgmpGlobalLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpGlobalLimit (UINT4 *pu4RetValFsIgmpGlobalLimit,
                                     INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpGlobalLimit()\r \n");
    UNUSED_PARAM (i4FsIgmpAddrType);
    *pu4RetValFsIgmpGlobalLimit = gIgmpConfig.u4GlobalGrpLimit;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpGlobalLimit()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpGlobalLimit
 Input       :  The Indices

                The Object 
                retValFsIgmpGlobalLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpGlobalCurGrpCount (UINT4
                                           *pu4RetValFsIgmpGlobalCurGrpCount,
                                           INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpGlobalCurGrpCount()\r \n");
    UNUSED_PARAM (i4FsIgmpAddrType);
    *pu4RetValFsIgmpGlobalCurGrpCount = gIgmpConfig.u4GlobalCurCnt;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpGlobalCurGrpCount()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpGroupListTable
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpGroupListTable (UINT4 *pu4FsIgmpGrpListId,
                                                  UINT4 *pu4FsIgmpGrpIP,
                                                  UINT4 *pu4FsIgmpGrpPrefixLen,
                                                  INT4 i4AdressFamilyType)
{
    tIgmpGrpList       *pGrpList = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFirstIndexFsIgmpGroupListTable()\r \n");
    UNUSED_PARAM (i4AdressFamilyType);
    pGrpList = (tIgmpGrpList *) RBTreeGetFirst (gIgmpGrpList);
    if (pGrpList == NULL)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE,
                  IgmpGetModuleName(IGMP_GRP_MODULE), 
		  "No Entry exist in GroupList table \n");        
        CLI_SET_ERR (CLI_IGMP_NO_GRP_LIST_RECORD_EXISTS);
        return SNMP_FAILURE;
    }
    *pu4FsIgmpGrpListId = pGrpList->u4GroupId;
    *pu4FsIgmpGrpIP = pGrpList->u4GroupIP;
    *pu4FsIgmpGrpPrefixLen = pGrpList->u4PrefixLen;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFirstIndexFsIgmpGroupListTable()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpGroupListTable
 Input       :  The Indices
                FsIgmpGrpListId
                nextFsIgmpGrpListId
                FsIgmpGrpIP
                nextFsIgmpGrpIP
                FsIgmpGrpPrefixLen
                nextFsIgmpGrpPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpGroupListTable (UINT4 u4FsIgmpGrpListId,
                                                 UINT4 *pu4NextFsIgmpGrpListId,
                                                 UINT4 u4FsIgmpGrpIP,
                                                 UINT4 *pu4NextFsIgmpGrpIP,
                                                 UINT4 u4FsIgmpGrpPrefixLen,
                                                 UINT4
                                                 *pu4NextFsIgmpGrpPrefixLen,
                                                 INT4 i4AdressFamilyType)
{
    tIgmpGrpList        GrpList;
    tIgmpGrpList       *pGrpList = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetNextIndexFsIgmpGroupListTable()\r \n");
    UNUSED_PARAM (i4AdressFamilyType);
    GrpList.u4GroupId = u4FsIgmpGrpListId;
    GrpList.u4GroupIP = u4FsIgmpGrpIP;
    GrpList.u4PrefixLen = u4FsIgmpGrpPrefixLen;
    pGrpList =
        (tIgmpGrpList *) RBTreeGetNext (gIgmpGrpList, (tRBElem *) & GrpList,
                                        NULL);
    if (pGrpList != NULL)
    {
        *pu4NextFsIgmpGrpListId = pGrpList->u4GroupId;
        *pu4NextFsIgmpGrpIP = pGrpList->u4GroupIP;
        *pu4NextFsIgmpGrpPrefixLen = pGrpList->u4PrefixLen;
        return SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetNextIndexFsIgmpGroupListTable()\r \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpGroupListTable 
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpGroupListTable (UINT4
                                                          u4FsIgmpGrpListId,
                                                          UINT4 u4FsIgmpGrpIP,
                                                          UINT4
                                                          u4FsIgmpGrpPrefixLen,
                                                          INT4 i4AddrType)
{
    tIgmpGrpList       *pGrpNode = NULL;
    tIgmpGrpList        GrpNode;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpGroupListTable()\r \n");
    UNUSED_PARAM (i4AddrType);
    UNUSED_PARAM (u4FsIgmpGrpPrefixLen);
    if (u4FsIgmpGrpListId == IGMP_ZERO)
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES, IgmpTrcGetModuleName(IGMP_ALL_MODULES),
                  "Group List Id should not be zero \r\n");
        CLI_SET_ERR (CLI_IGMP_INVALID_GRP_LIST_ID);
        return SNMP_FAILURE;
    }
    if ((u4FsIgmpGrpIP <= IGMP_START_OF_MCAST_GRP_ADDR) ||
        (u4FsIgmpGrpIP >= IGMP_END_OF_MCAST_GRP_ADDR))
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES, IgmpTrcGetModuleName(IGMP_ALL_MODULES),
                  "Group List IP is not in multicast range \r\n");
	IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IgmpGetModuleName(IGMP_GRP_MODULE),
                   "Group List Id %d should not be zero \r\n",
                   u4FsIgmpGrpListId);	
        CLI_SET_ERR (CLI_IGMP_INVALID_GRP_LIST_IP_ADDRESS);
        return SNMP_FAILURE;
    }
    GrpNode.u4GroupId = u4FsIgmpGrpListId;
    GrpNode.u4GroupIP = u4FsIgmpGrpIP;
    GrpNode.u4PrefixLen = u4FsIgmpGrpPrefixLen;
    pGrpNode = (tIgmpGrpList *) RBTreeGet (gIgmpGrpList, (tRBElem *) & GrpNode);
    if (pGrpNode == NULL)
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES, IgmpTrcGetModuleName(IGMP_ALL_MODULES),
                  "Entry not exists with the inputs \r\n");
        CLI_SET_ERR (CLI_IGMP_NO_GRP_LIST_RECORD_EXISTS);
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
      "Exiting  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpGroupListTable()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFsIgmpGrpListRowStatus 
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen

                The Object 
                retValFsIgmpGrpListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetFsIgmpGrpListRowStatus (UINT4 u4FsIgmpGrpListId,
                                          UINT4 u4FsIgmpGrpIP,
                                          UINT4 u4FsIgmpGrpPrefixLen,
                                          INT4 i4AddrType,
                                          INT4 *pi4RetValFsIgmpGrpListRowStatus)
{
    tIgmpGrpList       *pGrpNode = NULL;
    tIgmpGrpList        GrpNode;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFsIgmpGrpListRowStatus()\r \n");
    UNUSED_PARAM (i4AddrType);
    GrpNode.u4GroupId = u4FsIgmpGrpListId;
    GrpNode.u4GroupIP = u4FsIgmpGrpIP;
    GrpNode.u4PrefixLen = u4FsIgmpGrpPrefixLen;
    pGrpNode = (tIgmpGrpList *) RBTreeGet (gIgmpGrpList, (tRBElem *) & GrpNode);
    if (pGrpNode == NULL)
    {
        CLI_SET_ERR (CLI_IGMP_NO_GRP_LIST_RECORD_EXISTS);
	IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IgmpGetModuleName(IGMP_GRP_MODULE),
                   "Group List Record doesnot exist\r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFsIgmpGrpListRowStatus = (INT4) pGrpNode->u4RowStatus;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting  IgmpMgmtUtilNmhGetFsIgmpGrpListRowStatus()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable
                    (UINT4 u4FsIgmpSSMMapStartGrpAddress ,
                     UINT4 u4FsIgmpSSMMapEndGrpAddress ,
                     UINT4 u4FsIgmpSSMMapSourceAddress)
{
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    tIgmpIPvXAddr           u4StartGrp;
    tIgmpIPvXAddr           u4EndGrp;
    tIgmpIPvXAddr           u4Source;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable()\r \n");

    IGMP_IPVX_ADDR_CLEAR (&u4StartGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4EndGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4Source);

    IGMP_IPVX_ADDR_INIT_IPV4 (u4StartGrp, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapStartGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4EndGrp, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapEndGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4Source, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapSourceAddress);

    pSSMMappedGrp = IgmpGrpLookUpInSSMMapTable (u4StartGrp, u4EndGrp,
            u4Source);

    if (NULL == pSSMMappedGrp)
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES, IgmpTrcGetModuleName(IGMP_ALL_MODULES),
                  "IGMP SSM Mapping for the given range of group address to the source address does not exist \r\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable()\r \n");
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
            "Exiting  IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable
                    (UINT4 *pu4FsIgmpSSMMapStartGrpAddress ,
                     UINT4 *pu4FsIgmpSSMMapEndGrpAddress ,
                     UINT4 *pu4FsIgmpSSMMapSourceAddress)
{
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    UINT4                   u4Index = IGMP_ZERO;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable()\r \n");

    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4Index);

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetFirst (gIgmpSSMMappedGrp);

    if (NULL == pSSMMappedGrp)
    {
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_ALL_MODULES, IgmpTrcGetModuleName(IGMP_ALL_MODULES),
                "No entries present in IGMP SSM Mapping table \r\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting  IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable()\r \n");
        return SNMP_FAILURE;
    }

    IGMP_IP_COPY_FROM_IPVX (pu4FsIgmpSSMMapStartGrpAddress,
            pSSMMappedGrp->StartGrpAddr, IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (pu4FsIgmpSSMMapEndGrpAddress,
            pSSMMappedGrp->EndGrpAddr, IPVX_ADDR_FMLY_IPV4);

    for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
    {
        if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero, pSSMMappedGrp->SrcAddr[u4Index]) != IGMP_ZERO)
        {
            IGMP_IP_COPY_FROM_IPVX (pu4FsIgmpSSMMapSourceAddress,
                    pSSMMappedGrp->SrcAddr[u4Index],
                    IPVX_ADDR_FMLY_IPV4);
            break;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
            "Exiting  IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                NextFsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                NextFsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress
                NextFsIgmpSSMMapSourceAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable
                    (UINT4 u4FsIgmpSSMMapStartGrpAddress ,
                     UINT4 *pu4NextFsIgmpSSMMapStartGrpAddress  ,
                     UINT4 u4FsIgmpSSMMapEndGrpAddress ,
                     UINT4 *pu4NextFsIgmpSSMMapEndGrpAddress  ,
                     UINT4 u4FsIgmpSSMMapSourceAddress ,
                     UINT4 *pu4NextFsIgmpSSMMapSourceAddress )
{
    tIgmpSSMMapGrpEntry     SSMMappedGrp;
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    tIgmpSSMMapGrpEntry     *pNextSSMMappedGrp= NULL;
    tIgmpIPvXAddr           u4Source;
    UINT4                   u4Index = IGMP_ZERO;
    UINT1                   u1FoundFlag = IGMP_FALSE;
    UINT1                   u1MatchFlag = IGMP_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable()\r \n");

    MEMSET (&SSMMappedGrp, IGMP_ZERO, sizeof (tIgmpSSMMapGrpEntry));
    IGMP_IPVX_ADDR_CLEAR (&u4Source);
    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);

    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4Index);
    IGMP_IPVX_ADDR_INIT_IPV4 (SSMMappedGrp.StartGrpAddr, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapStartGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (SSMMappedGrp.EndGrpAddr, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapEndGrpAddress);

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGet
        (gIgmpSSMMappedGrp,(tRBElem *) &SSMMappedGrp);

    if (NULL == pSSMMappedGrp)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting  IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable()\r \n");
        return SNMP_FAILURE;
    }

    IGMP_IPVX_ADDR_INIT_IPV4 (u4Source, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapSourceAddress);

    for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
    {
        if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero, pSSMMappedGrp->SrcAddr[u4Index]) == IGMP_ZERO)
        {
            continue;
        }
        else if (u1MatchFlag == IGMP_TRUE)
        {
            u1FoundFlag = IGMP_TRUE;
            break;
        }
        else if (IGMP_IPVX_ADDR_COMPARE (u4Source, pSSMMappedGrp->SrcAddr[u4Index]) == IGMP_ZERO)
        {
            u1MatchFlag = IGMP_TRUE;
        }
    }

    if (IGMP_TRUE == u1FoundFlag)
    {
        *pu4NextFsIgmpSSMMapStartGrpAddress = u4FsIgmpSSMMapStartGrpAddress;
        *pu4NextFsIgmpSSMMapEndGrpAddress = u4FsIgmpSSMMapEndGrpAddress;
        IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpSSMMapSourceAddress,
                pSSMMappedGrp->SrcAddr[u4Index],
                IPVX_ADDR_FMLY_IPV4);
    }
    else 
    {
        pNextSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetNext
            (gIgmpSSMMappedGrp, (tRBElem *) pSSMMappedGrp, NULL);

        if (NULL == pNextSSMMappedGrp)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                    "Exiting  IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable()\r \n");
            return SNMP_FAILURE;
        }

        IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpSSMMapStartGrpAddress,
                pNextSSMMappedGrp->StartGrpAddr, IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpSSMMapEndGrpAddress,
                pNextSSMMappedGrp->EndGrpAddr, IPVX_ADDR_FMLY_IPV4);

        for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
        {
            if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero, pNextSSMMappedGrp->SrcAddr[u4Index]) != IGMP_ZERO)
            {
                IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpSSMMapSourceAddress,
                        pNextSSMMappedGrp->SrcAddr[u4Index],
                        IPVX_ADDR_FMLY_IPV4);
                break;
            }
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
            "Exiting  IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable()\r \n");
    return SNMP_SUCCESS;
}

INT1
IgmpMgmtUtilNmhGetFsIgmpSSMMapRowStatus (UINT4 u4FsIgmpSSMMapStartGrpAddress ,
                            UINT4 u4FsIgmpSSMMapEndGrpAddress ,
                            UINT4 u4FsIgmpSSMMapSourceAddress ,
                            INT4 *pi4RetValFsIgmpSSMMapRowStatus)
{
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    tIgmpIPvXAddr u4StartGrp;
    tIgmpIPvXAddr u4EndGrp;
    tIgmpIPvXAddr u4Source;

    if (IGMP_ZERO == u4FsIgmpSSMMapSourceAddress)
    {
        return SNMP_FAILURE;
    }
    IGMP_IPVX_ADDR_CLEAR (&u4StartGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4EndGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4Source);

    IGMP_IPVX_ADDR_INIT_IPV4 (u4StartGrp, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapStartGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4EndGrp, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapEndGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4Source, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4FsIgmpSSMMapSourceAddress);

    pSSMMappedGrp = IgmpGrpLookUpInSSMMapTable (u4StartGrp, u4EndGrp,
            u4Source);

    if (NULL == pSSMMappedGrp)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsIgmpSSMMapRowStatus = IGMP_ACTIVE;
    return SNMP_SUCCESS;
}
