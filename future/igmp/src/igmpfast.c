
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpfast.c,v 1.18 2017/02/06 10:45:27 siva Exp $
 *
 * Description:This file contains the routines required for
 *             Fast Leave feature                               
 *
 *******************************************************************/

#include "igmpinc.h"
#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;
#endif
extern UINT4 gu4MaxMldGrps;
extern UINT4 gu4MaxIgmpGrps;

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpDeleteReporter                            */
/*                                                                       */
/*     Description   :     This function deletes the reporter from the     */
/*                          reporter list                                    */
/*                                                                         */
/*     Input(s)      :    Source address for which the node is created     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpDeleteReporter                           **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpDeleteReporter (tIgmpIface * pIfNode, tIgmpSource * pSrc,
                    tIgmpIPvXAddr u4Reporter)
{
    tIgmpSrcReporter   FindReporter;
    tIgmpSrcReporter   *pDelReporter = NULL;
	tIgmpGroup 		   *pGrp = NULL;
	UINT4			   u4Count = 0;
	
	UNUSED_PARAM (pIfNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                 "Entering IgmpDeleteReporter()\r \n");
	if(pSrc->SrcReporterTree == NULL)
	{
		return IGMP_OK;
	}
    MEMSET(&FindReporter, 0, sizeof(tIgmpSrcReporter));
    IGMP_IPVX_ADDR_COPY(&(FindReporter.u4SrcRepAddr),&u4Reporter);
    pDelReporter = (tIgmpSrcReporter *) RBTreeGet(pSrc->SrcReporterTree,
                                    (tRBElem *)&FindReporter);
    if(pDelReporter == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
            "Reporter Node not present in RBTree \r\n");
    	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting  IgmpDeleteReporter()\r \n");
        return IGMP_OK;
    }

    pDelReporter->u1RepModeFlag = IGMP_DELETE_REPORTER;
	pDelReporter->u1GrpSrcFlag = IGMP_SOURCE_REPORTER;
    if(RBTreeRemove (pSrc->SrcReporterTree, (tRBElem *) pDelReporter) == RB_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
            "Reporter Node not present in RBTree \r\n");
    	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting  IgmpDeleteReporter()\r \n");
    	return IGMP_NOT_OK;
    }
    else
	{
		IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
				&pDelReporter->SrcReporterDbNode);
		IgmpRedSyncDynInfo ();
		IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId, (UINT1 *) pDelReporter);
		RBTreeCount (pSrc->SrcReporterTree, &u4Count);
		if (u4Count == IGMP_ZERO)
		{
			RBTreeDelete(pSrc->SrcReporterTree);
			pSrc->SrcReporterTree = NULL;
			/* stop the source timer before removing it */
			if ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET)
			{
				IGMPSTOPTIMER (pSrc->srcTimer);
			}

			IgmpDelSchQrySrcNode (pSrc);
			pGrp = pSrc->pGrp;

			if (IGMP_PROXY_STATUS () == IGMP_DISABLE)
			{
				/* Inform MRP to stop forwarding */
				TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
				TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
				/* Updating MRPs */
				IgmpUpdateMrps (pGrp, IGMP_LEAVE);
			}

                IGMP_TRC_ARG2 (MGMD_TRC_FLAG, CONTROL_PLANE_TRC, IGMP_NAME,
					"Informed MRP abt source membership \n GroupAddress is %s SourceAddress is %s \n",
					IgmpPrintIPvxAddress (pSrc->pGrp->u4Group),
					IgmpPrintIPvxAddress(pSrc->u4SrcAddress));
			pSrc->u1SrcModeFlag = IGMP_DELETE_SOURCE;
			IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
			IgmpRedSyncDynInfo ();
			/* Release  the SourceNode */
			TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
            if (pGrp->u4Group.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
					if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress, gIPvXZero) !=
									IGMP_ZERO)
					{
                gu4MaxIgmpGrps--;
            }
			}
            else
            {
                gu4MaxMldGrps--;
            }
            IgmpMemRelease (gIgmpMemPool.IgmpSourceId, (UINT1 *) pSrc);
		}
		
		MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
					"Exiting  IgmpDeleteReporter()\r \n");
        return IGMP_OK;
    }
}




INT4
IgmpAddV3Reporter (tIgmpSource * pSrc, tIgmpIPvXAddr u4Reporter)
{
	tIgmpSrcReporter   FindReporter;
    tIgmpSrcReporter   *pNewReporter = NULL;
    tIgmpSrcReporter   *pOldReporter = NULL;
    UINT4               u4Status = RB_FAILURE;

	if(pSrc->SrcReporterTree == NULL)
	{
		return IGMP_NOT_OK;
	}

	if(IGMP_IPVX_ADDR_COMPARE (u4Reporter, gIPvXZero) == IGMP_ZERO)
	{
		return IGMP_NULL_REPORTER;
	}
	
	MEMSET(&FindReporter, 0, sizeof(tIgmpSrcReporter));
    IGMP_IPVX_ADDR_COPY(&(FindReporter.u4SrcRepAddr),&u4Reporter);
    pOldReporter = (tIgmpSrcReporter *) RBTreeGet(pSrc->SrcReporterTree,
                                    (tRBElem *)&FindReporter);
    if(pOldReporter != NULL)
    {
        IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
            "Reporter Node %s already present in Source %s of group %s \r\n",
			IgmpPrintIPvxAddress (u4Reporter),IgmpPrintIPvxAddress (pSrc->u4SrcAddress),
			IgmpPrintIPvxAddress (pSrc->u4GrpAddress));
        return IGMP_OK;
    }

	if((IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSrcReporterId,
                       pNewReporter,
                       tIgmpSrcReporter)) != NULL)
    {
        pNewReporter->u4IfIndex = pSrc->pGrp->u4IfIndex;
        IGMP_IPVX_ADDR_COPY(&(pNewReporter->u4SrcRepAddr),&u4Reporter);
		IGMP_IPVX_ADDR_COPY (&(pNewReporter->GrpAddr), &(pSrc->u4GrpAddress));
		IGMP_IPVX_ADDR_COPY (&(pNewReporter->SourceAddr), &(pSrc->u4SrcAddress));
		pNewReporter->u1RepModeFlag = IGMP_ADD_REPORTER;
		pNewReporter->u1GrpSrcFlag = IGMP_SOURCE_REPORTER;
        u4Status = RBTreeAdd(pSrc->SrcReporterTree, (tRBElem *) pNewReporter);
        if(u4Status == RB_FAILURE)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                    "Reporter Node addition failed for %s \r\n",
					IgmpPrintIPvxAddress (u4Reporter));
            IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId,
                    (UINT1 *) pNewReporter);
			return IGMP_NOT_OK;
        }
		IgmpRedDbNodeInit (&(pNewReporter->SrcReporterDbNode),
				IGMP_RED_SRC_REPORTER_INFO);
    	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                &pNewReporter->SrcReporterDbNode);
   		IgmpRedSyncDynInfo ();
		return IGMP_OK;
    }

	IGMP_DBG4 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
			"Reporter %s for group %s source %s is ignored as the threshold (%d) for reporters is reached \r\n",
			IgmpPrintIPvxAddress (u4Reporter), IgmpPrintIPvxAddress (pSrc->u4GrpAddress), IgmpPrintIPvxAddress (pSrc->u4SrcAddress),
			MAX_IGMP_MCAST_SRS_REPORTERS);

    return IGMP_NOT_OK;
}
