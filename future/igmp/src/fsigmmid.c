/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsigmmid.c,v 1.3 2008/08/20 15:05:06 iss Exp $
 *
 * Description: Mid Level Functions
 *
 *******************************************************************/

# include  "include.h"
# include  "fsigmmid.h"
# include  "fsigmlow.h"
# include  "fsigmcon.h"
# include  "fsigmogi.h"
# include  "extern.h"
# include  "midconst.h"
#include "stdigmcli.h"
#include "fsigmpcli.h"
/****************************************************************************
 Function   : fsigmpGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsigmpGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
           UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_fsigmp_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_fsigmp_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_fsigmp_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_fsigmp_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_fsigmp_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case FSIGMPGLOBALSTATUS:
        {
            i1_ret_val = nmhGetFsIgmpGlobalStatus (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIGMPTRACELEVEL:
        {
            i1_ret_val = nmhGetFsIgmpTraceLevel (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIGMPFASTLEAVESTATUS:
        {
            i1_ret_val = nmhGetFsIgmpFastLeaveStatus (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsigmpSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsigmpSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
           UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSIGMPGLOBALSTATUS:
        {
            i1_ret_val = nmhSetFsIgmpGlobalStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIGMPTRACELEVEL:
        {
            i1_ret_val = nmhSetFsIgmpTraceLevel (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIGMPFASTLEAVESTATUS:
        {
            i1_ret_val = nmhSetFsIgmpFastLeaveStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsigmpTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsigmpTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
            UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    if (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0)
    {
        return (SNMP_ERR_GEN_ERR);
    }
    switch (u1_arg)
    {

        case FSIGMPGLOBALSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsIgmpGlobalStatus (&u4ErrorCode,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIGMPTRACELEVEL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsIgmpTraceLevel (&u4ErrorCode,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIGMPFASTLEAVESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2FsIgmpFastLeaveStatus (&u4ErrorCode,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsIgmpInterfaceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIgmpInterfaceEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsIgmpInterfaceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsIgmpInterfaceIfIndex = FALSE;
    INT4                i4_next_fsIgmpInterfaceIfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsIgmpInterfaceTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsIgmpInterfaceTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsIgmpInterfaceIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsIgmpInterfaceTable
                     (i4_fsIgmpInterfaceIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsIgmpInterfaceIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsIgmpInterfaceTable
                     (&i4_fsIgmpInterfaceIfIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsIgmpInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsIgmpInterfaceIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsIgmpInterfaceTable
                     (i4_fsIgmpInterfaceIfIndex,
                      &i4_next_fsIgmpInterfaceIfIndex)) == SNMP_SUCCESS)
                {
                    i4_fsIgmpInterfaceIfIndex = i4_next_fsIgmpInterfaceIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsIgmpInterfaceIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIGMPINTERFACEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsIgmpInterfaceIfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsIgmpInterfaceIfIndex;
            }
            break;
        }
        case FSIGMPINTERFACEINCOMINGPKTS:
        {
            i1_ret_val =
                nmhGetFsIgmpInterfaceIncomingPkts (i4_fsIgmpInterfaceIfIndex,
                                                   &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIGMPINTERFACEINCOMINGJOINS:
        {
            i1_ret_val =
                nmhGetFsIgmpInterfaceIncomingJoins (i4_fsIgmpInterfaceIfIndex,
                                                    &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIGMPINTERFACEINCOMINGLEAVES:
        {
            i1_ret_val =
                nmhGetFsIgmpInterfaceIncomingLeaves (i4_fsIgmpInterfaceIfIndex,
                                                     &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIGMPINTERFACEINCOMINGQUERIES:
        {
            i1_ret_val =
                nmhGetFsIgmpInterfaceIncomingQueries (i4_fsIgmpInterfaceIfIndex,
                                                      &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIGMPINTERFACEOUTGOINGQUERIES:
        {
            i1_ret_val =
                nmhGetFsIgmpInterfaceOutgoingQueries (i4_fsIgmpInterfaceIfIndex,
                                                      &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsIgmpCacheEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIgmpCacheEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsIgmpCacheTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    UINT1               u1_octet_string[MAX_OID_LENGTH] = NULL_STRING;
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The Declaration of the Structure which stores
     *  the Address of length Four Bytes
     */
    UINT4               u4_addr_fsIgmpCacheAddress = FALSE;
    UINT4               u4_addr_next_fsIgmpCacheAddress = FALSE;
    UINT1               u1_addr_fsIgmpCacheAddress[ADDR_LEN] = NULL_STRING;
    UINT1               u1_addr_next_fsIgmpCacheAddress[ADDR_LEN] = NULL_STRING;

    INT4                i4_fsIgmpCacheIfIndex = FALSE;
    INT4                i4_next_fsIgmpCacheIfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += ADDR_LEN;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsIgmpCacheTable_INDEX =
                p_in_db->u4_Length + ADDR_LEN + INTEGER_LEN;

            if (LEN_fsIgmpCacheTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                for (i4_count = FALSE; i4_count < ADDR_LEN;
                     i4_count++, i4_offset++)
                    u1_addr_fsIgmpCacheAddress[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                u4_addr_fsIgmpCacheAddress =
                    OSIX_NTOHL (*((UINT4 *) (u1_addr_fsIgmpCacheAddress)));

                /* Extracting The Integer Index. */
                i4_fsIgmpCacheIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsIgmpCacheTable
                     (u4_addr_fsIgmpCacheAddress,
                      i4_fsIgmpCacheIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        u1_addr_fsIgmpCacheAddress[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsIgmpCacheIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsIgmpCacheTable
                     (&u4_addr_fsIgmpCacheAddress,
                      &i4_fsIgmpCacheIfIndex)) == SNMP_SUCCESS)
                {
                    *((UINT4 *) (u1_addr_fsIgmpCacheAddress)) =
                        OSIX_HTONL (u4_addr_fsIgmpCacheAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            u1_addr_fsIgmpCacheAddress[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsIgmpCacheIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += ADDR_LEN;
                    /*  FOR Loop for extracting the Index of Type Address From Given OID. */
                    for (i4_count = FALSE; i4_count < ADDR_LEN;
                         i4_count++, i4_offset++)
                        u1_addr_fsIgmpCacheAddress[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                    /*  This Library Fn converts the Char Array to a Word of 4 bytes. */
                    u4_addr_fsIgmpCacheAddress =
                        OSIX_NTOHL (*((UINT4 *) (u1_addr_fsIgmpCacheAddress)));

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsIgmpCacheIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsIgmpCacheTable
                     (u4_addr_fsIgmpCacheAddress,
                      &u4_addr_next_fsIgmpCacheAddress, i4_fsIgmpCacheIfIndex,
                      &i4_next_fsIgmpCacheIfIndex)) == SNMP_SUCCESS)
                {
                    u4_addr_fsIgmpCacheAddress =
                        u4_addr_next_fsIgmpCacheAddress;
                    *((UINT4 *) (u1_addr_next_fsIgmpCacheAddress)) =
                        OSIX_HTONL (u4_addr_fsIgmpCacheAddress);

                    for (i4_count = FALSE; i4_count < ADDR_LEN; i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) u1_addr_next_fsIgmpCacheAddress[i4_count];
                    i4_fsIgmpCacheIfIndex = i4_next_fsIgmpCacheIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsIgmpCacheIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIGMPCACHEADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_IP_ADDR_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_fsIgmpCacheAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            else
            {
                *((UINT4 *) (u1_octet_string)) =
                    OSIX_HTONL (u4_addr_next_fsIgmpCacheAddress);
                poctet_string =
                    (tSNMP_OCTET_STRING_TYPE *)
                    SNMP_AGT_FormOctetString (u1_octet_string, ADDR_LEN);
            }
            break;
        }
        case FSIGMPCACHEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsIgmpCacheIfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsIgmpCacheIfIndex;
            }
            break;
        }
        case FSIGMPCACHEGROUPCOMPMODE:
        {
            i1_ret_val =
                nmhGetFsIgmpCacheGroupCompMode (u4_addr_fsIgmpCacheAddress,
                                                i4_fsIgmpCacheIfIndex,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */
