/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpin.c,v 1.30 2017/02/06 10:45:27 siva Exp $
 *
 * Description:This file contains the routines required for
 *             processing the IGMP packets
 *
 *******************************************************************/

#include "igmpinc.h"
#include "fssocket.h"
#if defined LNXIP4_WANTED && defined NP_KERNEL_WANTED
#include <chrdev.h>
#endif

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE ;
#endif

/*********************************************************************/
/* Function           : IgmpInput                                    */
/*                                                                   */
/* Description        : Entry point for the incoming IGMP packets.   */
/*                      Decodes the packet and invokes the           */
/*                      appropriate routine.                         */
/*                                                                   */
/* Input(s)           : IP buffer pointer,                           */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = igmpInput                             **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
VOID
IgmpInput (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tCRU_BUF_CHAIN_HEADER *pDvmrpBuf = NULL;
    UINT2               u4Port = IGMP_ZERO;
    tIgmpIface         *pIfNode = NULL;
    tIgmpPkt            Igmp;
    tIgmpQuery         *pIgmp = NULL;
    UINT1               u1MaxRespCode = IGMP_ZERO;
    UINT1               u1Srsp = IGMP_ZERO;
    INT4                i4Status = IGMP_ZERO;
    INT4                i4Index = IGMP_NOT_OK;
    t_IP                Ip;
    tIgmpFilter         IgmpFilterPkt;
    tIgmpIPvXAddr       u4SrcAddr;
    tIgmpIPvXAddr       u4GroupAddr;
    tIgmpIPvXAddr       GroupIpvxAddr;
    UINT1               u1IpHdrLen = IGMP_ZERO;
    UINT1               u1IgmpPktType = IGMP_ZERO;
	UINT1               u1IsSecondary = IGMP_FALSE;
    UINT4               u4IgmpBufSize = IGMP_ZERO;
    UINT4               u4IpGrp = IGMP_ZERO;
    UINT4               u4TmpAddr = IGMP_ZERO;
    UINT4               u4IfAddr = IGMP_ZERO;
    UINT4               u4Mask = IGMP_ZERO;
    UINT4               u4PrevIpAddr = IGMP_ZERO;
    UINT4               u4SecIpAddr = IGMP_ZERO;
    UINT4               u4SecNetMask = IGMP_ZERO;
    tIgmpIPvXAddr       DestAddrX;
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tIgmpGroup          *pGrp = NULL;
    INT4                i4MapStatus = IGMP_FALSE;

    IGMP_IPVX_ADDR_CLEAR (&u4SrcAddr);
    IGMP_IPVX_ADDR_CLEAR (&u4GroupAddr);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpInput()\r \n");
    MEMSET (&IgmpFilterPkt, 0, sizeof (tIgmpFilter));

    /* Check if IGMP is globally enabled */
    if (gIgmpConfig.u1IgmpStatus == IGMP_DISABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IGMP not globally enabled - pkt dropped!!!\n");
	IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ",
                   pBuf);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
        return;
    }

    /* Get the port from Buffer */
    GET_IFINDEX_FROMBUF (pBuf, u4Port);
    /* Get the interface node from the port */

    pIfNode = IGMP_GET_IF_NODE (u4Port, IPVX_ADDR_FMLY_IPV4);
    if (pIfNode == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Interface %d not up - pkt dropped!!!\n", u4Port);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
        return;
    }
    if (IPVX_ADDR_COMPARE (pIfNode->u4IfAddr, gIPvXZero) == IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "No IP address mapped to the interface\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
        return;
    }

    /* If IGMP not enabled on the I/F,  drop the packet. */
    if (pIfNode->u1IfStatus != IGMP_IFACE_UP)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "IGMP is not enabled in this interface ( %d)."
                       " Dropping the packet\n", pIfNode->u4IfIndex);
	IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ",
                   pBuf);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
        return;
    }

    /* Extract the IP header from the buffer */
    if ((i4Status = IGMP_EXTRACT_IP_HEADER (&Ip, pBuf)) == IP_FAILURE)
    {
        pIfNode->u4PktLenError++;
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Unable to extract IP header from Control Message Buffer\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
	pIfNode->u4IgmpMalformedPkt++;
        return;
    }

    u1IpHdrLen = Ip.u1Hlen;
    u4IgmpBufSize = Ip.u2Len - u1IpHdrLen;
    IGMP_IPVX_ADDR_CLEAR (&u4SrcAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4SrcAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(Ip.u4Src));
    if (CfaIpIfIsOurAddress (Ip.u4Src) == CFA_SUCCESS)
    {
        pIfNode->u4PktWithLocalIP++;
    }
    /* subnet chk:  */
    if ((Ip.u4Src) != IGMP_ZERO)
    {
        IPV4_MASKLEN_TO_MASKX (u4Mask, pIfNode->u4IfMask);
        IGMP_IP_COPY_FROM_IPVX (&u4IfAddr, pIfNode->u4IfAddr,
                                IGMP_IPVX_ADDR_FMLY_IPV4);

        if (((Ip.u4Src) & u4Mask) != (u4IfAddr & u4Mask))
		{
			while( NetIpv4GetNextSecondaryAddress ((UINT2)pIfNode->u4IfIndex, u4PrevIpAddr,
						&u4SecIpAddr,&u4SecNetMask) == NETIPV4_SUCCESS)
			{
				if(((Ip.u4Src) & u4SecNetMask) == (u4SecIpAddr & u4SecNetMask))
				{
					u1IsSecondary = IGMP_TRUE;
					break;
				}
				u4PrevIpAddr = u4SecIpAddr;
			}
			if(u1IsSecondary != IGMP_TRUE)
			{

				IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
						"Subnet Error\n");
				pIfNode->u4SubnetCheckFailure++;
				MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
						IgmpGetModuleName(IGMP_EXIT_MODULE),
						"Exiting IgmpInput()\r \n");
				return;
			}
		}
    }
    /* Get Checksum  for IGMP packet */
    if (IgmpCalcCheckSum (pBuf, u4IgmpBufSize, u1IpHdrLen) != IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Checksum Failure In the received IGMP packet\n");
        pIfNode->u4CKSumError++;
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
        return;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &Igmp, u1IpHdrLen,
                               IGMP_HEADER_LEN);

    u1IgmpPktType = Igmp.u1PktType;
    u1MaxRespCode = Igmp.u1MaxRespTime;

    if (u1IgmpPktType != DVMRP_PKT)
    {
        IGMP_IPVX_ADDR_CLEAR (&u4GroupAddr);
        u4TmpAddr = OSIX_NTOHL (Igmp.u4GroupAddr);
        IGMP_IPVX_ADDR_INIT_IPV4 (u4GroupAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4TmpAddr);

        IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GroupAddr,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        /* Check for Group Address */
        if ((u1IgmpPktType != IGMP_PACKET_V3_REPORT) &&
            ((pIfNode->u1Version == IGMP_VERSION_1) ||
             (pIfNode->u1Version == IGMP_VERSION_2)))
        {
            if (u1IgmpPktType != IGMP_PACKET_QUERY)
            {
                if ((u4IpGrp < IGMP_START_OF_MCAST_GRP_ADDR) ||
                    (u4IpGrp > IGMP_END_OF_MCAST_GRP_ADDR))
                {
                    /* Invalid Group Destination Address */
       		     IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE,
                                IGMP_NAME,
                                "Invalid Group in the IGMP message\n");             
		     IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
				IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                              	"Invalid Group in the IGMP message\n");
                     MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
				IgmpGetModuleName(IGMP_EXIT_MODULE),
                		"Exiting IgmpInput()\r \n");
                    return;
                }
            }
        }
    }

    IGMP_IPVX_ADDR_CLEAR (&DestAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (DestAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(Ip.u4Dest));
    DbgDumpIgmpPkt (pBuf, Ip.u2Len, Igmp.u1PktType, IGMP_PKT_FLOW_IN,
                    u4SrcAddr, DestAddrX);
    IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP-",
                   pBuf);

    IGMP_IPVX_ADDR_CLEAR (&GroupIpvxAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (GroupIpvxAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IpGrp);

    /* check whether SSM mapping is configured for the group */
    pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (GroupIpvxAddr, &i4MapStatus);

#ifdef MULTICAST_SSM_ONLY
        if (((u1IgmpPktType == IGMP_PACKET_V1_REPORT) ||
            (u1IgmpPktType == IGMP_PACKET_V2_REPORT) ||
            (u1IgmpPktType == IGMP_PACKET_LEAVE)) && (i4MapStatus == IGMP_FALSE))
        {
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
					"Multicast SSM only flag is enabled. "
                                        "SSM mapping is not configured for the group %s Hence Ignored !!!", 
                        IgmpPrintIPvxAddress(GroupIpvxAddr)));
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                           "Enabled Multicast SSM only flag."
                           "Group %s not in SSM Mapped Range Ignored !!!! \n",
                           IgmpPrintIPvxAddress (GroupIpvxAddr));
            pIfNode->u4DroppedASMIncomingPkts++;
            return;
        }
#endif
    switch (u1IgmpPktType)
    {
        case IGMP_PACKET_V1_REPORT:
            IGMP_CHK_IF_SSM_RANGE (u4IpGrp, i4Status)
            if (((i4Status == IGMP_SSM_RANGE) && (i4MapStatus == IGMP_FALSE)) ||
		(u4IpGrp == IGMP_INVALID_SSM_GRP_ADDR))
            {
		IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                                IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                                "Group %s in SSM Range, Ignored !!!!\n",
                                IgmpPrintIPvxAddress (GroupIpvxAddr));
	                            IGMP_DBG1(MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
				"Group %s in SSM Range, Ignored !!!!\n",
				IgmpPrintIPvxAddress (GroupIpvxAddr));
		        IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ", pBuf);
                pIfNode->u4InvalidV1Report++;
                pIfNode->u4IgmpInvalidSSMPkts++;
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                		"Exiting IgmpInput()\r \n");
                return;
            }
            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4SrcIpAddr), &u4SrcAddr);
            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4GrpAddress), &u4GroupAddr);
            IgmpFilterPkt.u4InIfIndex = pIfNode->u4IfIndex;
            IgmpFilterPkt.u1PktType = IGMP_PACKET_V1_REPORT;
            IgmpFilterPkt.u1Version = IGMP_VERSION_1;

            if (IgmpFilterPackets (&IgmpFilterPkt) == IGMP_DENY)
            {
                IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
			       IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                               "IGMP V1 report filtered for group %s\n",
                               IgmpPrintIPvxAddress (GroupIpvxAddr));
		IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE,
                               IGMP_NAME,
                               "IGMP V1 report filtered for group %s\n",
                               IgmpPrintIPvxAddress (GroupIpvxAddr));
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		              "Exiting IgmpInput()\r \n");
                return;

            }

            if (i4MapStatus == IGMP_TRUE)
            {
                /* Converting (* G) join to (S G) join with the source address
                 * configured through SSM mapping */
                IgmpChngStarGToSGJoinForSSMMappedGrp (pIfNode, pSSMMappedGrp, 
                                            u4GroupAddr, u4SrcAddr, IGMP_JOIN);    
                pGrp = IgmpGrpLookUp (pIfNode, u4GroupAddr);
                if (pGrp != NULL)
                {
                    pGrp->u1V1HostPresent = IGMP_TRUE;
                } 
            }
            else
            {
            IGMPPROCESSV1REPORT (pIfNode, u4GroupAddr, u4SrcAddr);
            }
            IGMP_V1V2REP_RX (pIfNode);
            break;

        case IGMP_PACKET_V2_REPORT:
            if (IpIsRtrAlertPresent (&Ip) == FALSE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
		IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "Can't Process V2 Report Msg since router alert option"
                          "is not present. \n");
		        IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ", pBuf);
                pIfNode->u4RouterAlertCheckFailure++;
                return;

            }
            if (pIfNode->u1Version == IGMP_VERSION_1)
            {
                /* Section 4 - Compatibility with IGMPv1 Routers - RFC 2236 */
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                          "Received a V2 Report on a IGMP V1 Interface\n");\
		IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
				IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          	"Received a V2 Report on a IGMP V1 Interface\n");
                pIfNode->u4ReportVersionMisMatch++;
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
                return;
            }

            IGMP_CHK_IF_SSM_RANGE (u4IpGrp, i4Status)
            if (((i4Status == IGMP_SSM_RANGE) && (i4MapStatus == IGMP_FALSE)) ||
		(u4IpGrp == IGMP_INVALID_SSM_GRP_ADDR))
            {
	        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
				"Group %s in SSM Range, Ignored !!!!\n",
				    IgmpPrintIPvxAddress (GroupIpvxAddr));
		IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, 
				IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                                "Group %s in SSM Range, Ignored !!!!\n",
                                IgmpPrintIPvxAddress (GroupIpvxAddr));	
		        IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ", pBuf);
                pIfNode->u4InvalidV2Report++;
                pIfNode->u4IgmpInvalidSSMPkts++;
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                              "Exiting IgmpInput()\r \n");
                return;
            }
            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4SrcIpAddr), &u4SrcAddr);
            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4GrpAddress), &u4GroupAddr);
            IgmpFilterPkt.u4InIfIndex = pIfNode->u4IfIndex;
            IgmpFilterPkt.u1PktType = IGMP_PACKET_V2_REPORT;
            IgmpFilterPkt.u1Version = IGMP_VERSION_2;

            if (IgmpFilterPackets (&IgmpFilterPkt) == IGMP_DENY)
            {
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                               "IGMP V2 report filtered for group %s \r\n",
                               IgmpPrintIPvxAddress (GroupIpvxAddr));
		IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
				IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                               "IGMP V2 report filtered for group %s \r\n",
                               IgmpPrintIPvxAddress (GroupIpvxAddr));
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
                return;
            }

            if (i4MapStatus == IGMP_TRUE)
            {
                /* Converting (* G) join to (S G) join with the source address
                 * configured through SSM mapping */
                IgmpChngStarGToSGJoinForSSMMappedGrp (pIfNode, pSSMMappedGrp, 
                                            u4GroupAddr, u4SrcAddr, IGMP_JOIN);    
                pGrp = IgmpGrpLookUp (pIfNode, u4GroupAddr);
                if (pGrp != NULL)
                {
                    pGrp->u1V2HostPresent = IGMP_TRUE;
                } 
            }
            else
            {
            IgmpProcessV2Report (pIfNode, u4GroupAddr, u4SrcAddr);
            }
            IGMP_V1V2REP_RX (pIfNode);
            break;

        case IGMP_PACKET_V3_REPORT:
            if (IpIsRtrAlertPresent (&Ip) == FALSE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                          "Can't Process V3 Report Msg since router alert option"
                          "is not present. \n");
		IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "Can't Process V3 Report Msg since router alert option"
                          "is not present. \n");
		        IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ", pBuf);
                pIfNode->u4RouterAlertCheckFailure++;
                return;

            }
            if (Ip.u4Dest != IGMP_ALL_V3_ROUTERS_GROUP)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                        "Can't Process V3 Report Msg since destination IP address"
                        "is not all router multicast address. \n");
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
                        IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                        "Can't Process V3 Report Msg since destination IP address"
                        "is not all router multicast address. \n");
                IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ",
                        pBuf);
                pIfNode->u4InvalidV3Report++;
                return;
            }
            if (pIfNode->u1Version == IGMP_VERSION_3)
            {
                MEMSET (&gau1Igmpv3Buffer, IGMP_ZERO, IGMP_MAX_MTU_SIZE);
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &gau1Igmpv3Buffer,
                                           u1IpHdrLen, u4IgmpBufSize);

                IgmpProcessV3Report (pIfNode, (UINT1 *) &gau1Igmpv3Buffer,
                                     u4IgmpBufSize, u4SrcAddr);
            }
            else
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                          "Wrong version \n");
		IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),"Wrong version \n");
                pIfNode->u4ReportVersionMisMatch++;
                pIfNode->u4InvalidV3Report++;
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
		return;
            }

            IGMP_V3REP_RX (pIfNode);
            break;

        case IGMP_PACKET_LEAVE:
            if (pIfNode->u1Version == IGMP_VERSION_1)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                          "Can't Process Leave Msg in IGMP v1 enabled "
                          "Interface. \n");
		IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "Can't Process Leave Msg in IGMP v1 enabled "
                          "Interface. \n");
		        IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ", pBuf);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
               			 "Exiting IgmpInput()\r \n");
		return;
            }
            IGMP_CHK_IF_SSM_RANGE (u4IpGrp, i4Status)
            if (((i4Status == IGMP_SSM_RANGE) && (i4MapStatus == IGMP_FALSE))||
		(u4IpGrp == IGMP_INVALID_SSM_GRP_ADDR))
            {
	        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
				"Group %s in SSM Range, Ignored !!!!\n",
				IgmpPrintIPvxAddress (GroupIpvxAddr));
		IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE, 
				IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                                "Group %s in SSM Range, Ignored !!!!\n",
                                IgmpPrintIPvxAddress (GroupIpvxAddr));
		        IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ", pBuf);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		                "Exiting IgmpInput()\r \n");
		pIfNode->u4IgmpInvalidSSMPkts++;
                return;
            }
            if (IpIsRtrAlertPresent (&Ip) == FALSE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                          "Can't Process Leave Msg since router alert option"
                          "is not present. \n");
		IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "Can't Process Leave Msg since router alert option"
                          "is not present. \n");
		        IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ", pBuf);
                pIfNode->u4RouterAlertCheckFailure++;
                return;

            }

            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4SrcIpAddr), &u4SrcAddr);
            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4GrpAddress), &u4GroupAddr);
            IgmpFilterPkt.u4InIfIndex = pIfNode->u4IfIndex;
            IgmpFilterPkt.u1PktType = IGMP_PACKET_LEAVE;
            IgmpFilterPkt.u1Version = IGMP_VERSION_2;

            if (IgmpFilterPackets (&IgmpFilterPkt) == IGMP_DENY)
            {
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                               "IGMP Leave filtered for Group %s \r\n",
                               IgmpPrintIPvxAddress (GroupIpvxAddr));
		IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
				IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                               "IGMP Leave filtered for Group %s \r\n",
                               IgmpPrintIPvxAddress (GroupIpvxAddr));
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
		return;
            }
            if (i4MapStatus == IGMP_TRUE)
            {
                /* Converting (* G) join to (S G) join with the source address
                 * configured through SSM mapping */
                IgmpChngStarGToSGJoinForSSMMappedGrp (pIfNode, pSSMMappedGrp, 
                                            u4GroupAddr, u4SrcAddr, IGMP_LEAVE);    
            }
            else
            {
            IgmpProcessLeave (pIfNode, u4GroupAddr);
            }
            IGMP_LEAVE_RX (pIfNode);
            break;
       
        case IGMP_PACKET_QUERY:

            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4SrcIpAddr), &u4SrcAddr);
            IGMP_IPVX_ADDR_COPY (&(IgmpFilterPkt.u4GrpAddress), &u4GroupAddr);
            IgmpFilterPkt.u4InIfIndex = pIfNode->u4IfIndex;
            IgmpFilterPkt.u1PktType = IGMP_PACKET_QUERY;
            if (IgmpFilterPackets (&IgmpFilterPkt) == IGMP_DENY)
            {
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                               "IGMP Leave filtered for Group %s \r\n",
                               IgmpPrintIPvxAddress (u4GroupAddr));
		IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
				IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                               "IGMP Leave filtered for Group %s \r\n",
                               IgmpPrintIPvxAddress (u4GroupAddr));	
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
		return;
            }

            pIgmp = pIfNode->pIfaceQry;

            /* chk whether the query is a version 1 query */
            if ((u1MaxRespCode == IGMP_ZERO) &&
                (u4IgmpBufSize == IGMP_V1V2_QUERY_LEN))
            {

                IgmpProcessQuery (pIfNode, u4GroupAddr, u4SrcAddr,
                                  IGMP_ZERO, NULL, u1Srsp, IGMP_VERSION_1);

                IGMP_GEN_QUERY_RX (pIfNode);

            }
            /* chk whether the query is a version 2 query */
            else if ((u1MaxRespCode != IGMP_ZERO) &&
                     (u4IgmpBufSize == IGMP_V1V2_QUERY_LEN))
            {

                IgmpProcessQuery (pIfNode, u4GroupAddr, u4SrcAddr,
                                  IGMP_ZERO, NULL, u1Srsp, IGMP_VERSION_2);
                if (IGMP_IPVX_ADDR_COMPARE (u4GroupAddr, gIPvXZero) !=
                    IGMP_ZERO)
                {
                    IGMP_GRP_QUERY_RX (pIfNode);
                }
                else
                {
                    IGMP_GEN_QUERY_RX (pIfNode);
                }

            }
            /* chk whether the query is a version 3 query */
            else if ((u1MaxRespCode != IGMP_ZERO) &&
                     (u4IgmpBufSize >= IGMPV3_QUERY_LEN))
            {
                CRU_BUF_Move_ValidOffset (pBuf, u1IpHdrLen);
                IgmpProcessV3Query (pIfNode, pBuf, u4SrcAddr);
            }

            break;
        default:
            i4Index = IgmpGetIndexOfProtId (Igmp.u1PktType);
            if (i4Index != IGMP_NOT_OK)
            {
                if (gaIgmpRegister[i4Index].u1Flag == IGMP_UP)
                {
                    if ((pDvmrpBuf = CRU_BUF_Duplicate_BufChain (pBuf)) != NULL)
                    {
                        if (gaIgmpRegister[i4Index].pHandleInput != NULL)
                        {
                            gaIgmpRegister[i4Index].pHandleInput (pDvmrpBuf);
                        }
                        else
                        {
                            CRU_BUF_Release_MsgBufChain (pDvmrpBuf, IGMP_ZERO);
                        }
                    }
                }
            }
            pIfNode->u4UnknownMsgType++;
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                      "Invalid Packet!!!!!!!!!!!!!!.\n ");
	    IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
		       IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "Invalid Packet!!!!!!!!!!!!!!.\n ");
            pIfNode->u4IgmpMalformedPkt++;
            break;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting IgmpInput()\r \n");
    UNUSED_PARAM (pIgmp);
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpProcessV3Query                                 */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer             */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*             be scheduled                                                */
/*            u4GroupAddr - the group addres for which the query           */
/*             is to be scheduled                                          */
/*             u4NoOfSrcs - No. of sources                                 */
/*             pu1Sources - Source Addresses                               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpProcessV3Query                          **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpProcessV3Query (tIgmpIface * pIfNode, tCRU_BUF_CHAIN_HEADER * pBuf,
                    tIgmpIPvXAddr u4SrcAddr)
{
    tIgmpV3Query        IgmpQuery;
    UINT1               u1Srsp = IGMP_ZERO;
    UINT1               u1QRV = IGMP_ZERO;
    UINT1               u1QQIC = IGMP_ZERO;
    UINT2               u2NoOfSrcs = IGMP_ZERO;
    UINT4               u4TmpAddr = IGMP_ZERO;
#ifdef MULTICAST_SSM_ONLY
    INT4                i4Status = IGMP_ZERO;
#endif
    tIgmpIPvXAddr       u4GroupAddr;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProcessV3Query()\r \n");
    if ((pIfNode->u1Version != IGMP_VERSION_3) &&
        ((IGMP_PROXY_STATUS () != IGMP_ENABLE) ||
         (IgpUtilCheckUpIface (pIfNode->u4IfIndex) != IGMP_TRUE)))
    {
        pIfNode->u4WrongVerQueries++;
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "IGMPv3 Query but Not IGMPv3 Router!! \n");
	IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
		  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "IGMPv3 Query but Not IGMPv3 Router!! \n");
	IGMP_DUMP (IGMP_TRC_FLAG, IGMP_DUMP_TRC, "IGMP- ",
                   pBuf);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting  IgmpProcessV3Query()\r \n");
	return;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IgmpQuery, IGMP_ZERO,
                               sizeof (tIgmpV3Query));

    if ((IGMP_PROXY_STATUS () != IGMP_ENABLE) ||
        (IgpUtilCheckUpIface (pIfNode->u4IfIndex) != IGMP_TRUE))
    {
        u1Srsp = IGMP_SRSP ((&IgmpQuery));
        u1QRV = IGMP_QRV ((&IgmpQuery));
        pIfNode->pIfaceQry->u1QRV = u1QRV;
        u1QQIC = IGMP_QQIC ((&IgmpQuery));
        pIfNode->pIfaceQry->u1QQIC = u1QQIC;
    }

    u2NoOfSrcs = OSIX_NTOHS (IgmpQuery.u2NumSrcs);
    u4TmpAddr = OSIX_NTOHL (IgmpQuery.u4GroupAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4GroupAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4TmpAddr);
#ifdef MULTICAST_SSM_ONLY
    IGMP_CHK_IF_SSM_RANGE (u4TmpAddr, i4Status)
    if (i4Status != IGMP_SSM_RANGE)
    {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                           "Enabled Multicast SSM only flag."
                           "Group %s not in SSM Mapped Range Ignored !!!! \n",
                           IgmpPrintIPvxAddress (u4GroupAddr));
            pIfNode->u4DroppedASMIncomingPkts++;
            return;
    }
#endif
    if (u2NoOfSrcs == IGMP_ZERO)
    {
        IgmpProcessQuery (pIfNode, u4GroupAddr, u4SrcAddr,
                          IGMP_ZERO, NULL, u1Srsp, IGMP_VERSION_3);
        if (IGMP_IPVX_ADDR_COMPARE (u4GroupAddr, gIPvXZero) != IGMP_ZERO)
        {
            IGMP_GRP_QUERY_RX (pIfNode);
        }
        else
        {
            IGMP_GEN_QUERY_RX (pIfNode);
        }
    }
    else
    {
        MEMSET (&gau1Igmpv3Buffer, IGMP_ZERO, IGMP_MAX_MTU_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &gau1Igmpv3Buffer,
                                   sizeof (tIgmpV3Query),
                                   (sizeof (UINT4) * u2NoOfSrcs));

        IgmpProcessQuery (pIfNode, u4GroupAddr, u4SrcAddr,
                          u2NoOfSrcs, gau1Igmpv3Buffer, u1Srsp, IGMP_VERSION_3);

        IGMP_GRP_SRC_QUERY_RX (pIfNode);
    }
     MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting  IgmpProcessV3Query()\r \n");
}


/*********************************************************************/
/* Function           :     IgmpCalcCheckSum                         */
/*                                                                   */
/* Description        :  This function calculate the CheckSum        */
/*                       IgmpRegister given the MRP identifier       */
/*                                                                   */
/* Input(s)           :  pBuf - Pointer Variable of                  */
/*                                 type tCRU_BUF_CHAIN_HEADER        */
/*                       u4Size - Total pBuf Size                    */
/*                       u4OffSet - Offset value                     */
/* Output(s)          :  None                                        */
/*                                                                   */
/* Returns            :  checksum                                    */
/*                                                                   */
/*********************************************************************/
UINT2
IgmpCalcCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size, UINT4 u4Offset)
{
    UINT4               u4Sum = IGMP_ZERO;
    UINT2               u2Tmp = IGMP_ZERO;
    UINT1               u1Byte = IGMP_ZERO;
    UINT1              *pu1Buf = NULL;
    UINT1               au1TmpArray[IGMP_CKSUM_CHUNK_SIZE];
    UINT4               u4TmpSize = IGMP_CKSUM_CHUNK_SIZE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpCalcCheckSum()\r \n");
    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);

    if (pu1Buf == NULL)
    {
        while (u4Size > IGMP_CKSUM_CHUNK_SIZE)
        {
            u4TmpSize = IGMP_CKSUM_CHUNK_SIZE;
            pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4TmpSize);
            if (pu1Buf == NULL)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1TmpArray,
                                           u4Offset, u4TmpSize);
                pu1Buf = au1TmpArray;
            }

            while (u4TmpSize > IGMP_ZERO)
            {
                u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
                u4Sum += u2Tmp;
                pu1Buf += sizeof (UINT2);
                u4TmpSize -= sizeof (UINT2);
            }

            u4Size -= IGMP_CKSUM_CHUNK_SIZE;
            u4Offset += IGMP_CKSUM_CHUNK_SIZE;
        }
        while (u4Size > IGMP_ONE)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Tmp, u4Offset,
                                       sizeof (UINT2));
            u4Sum += u2Tmp;
            u4Size -= sizeof (UINT2);
            u4Offset += sizeof (UINT2);
        }

        if (u4Size == IGMP_ONE)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, &u1Byte, u4Offset, sizeof (UINT1));
            u2Tmp = IGMP_ZERO;
            *((UINT1 *) &u2Tmp) = u1Byte;
            u4Sum += u2Tmp;
        }
    }
    else
    {
        while (u4Size > IGMP_ONE)
        {
            u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
            u4Sum += u2Tmp;
            pu1Buf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }
        if (u4Size == IGMP_ONE)
        {
            u2Tmp = IGMP_ZERO;
            *((UINT1 *) &u2Tmp) = *pu1Buf;
            u4Sum += u2Tmp;
        }
    }
    u4Sum = (u4Sum >> IGMP_SIXTEEN) + (u4Sum & IGMP_FFFF);
    u4Sum += (u4Sum >> IGMP_SIXTEEN);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpCalcCheckSum()\r \n");
    return (((UINT2) OSIX_NTOHS (u2Tmp)));

}

#ifdef LNXIP4_WANTED
#ifndef ISS_WANTED
/* Only for IGMP stackon Linuxip. Not for ISS */
/*************************************************************************n
 * Function Name    :  IgmpHandlePktArrivalEvent
 *
 * Description      :  This function handles IGMP packets from LINUX IP.
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
IgmpHandlePktArrivalEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    tIgmpIface         *pIfNode = NULL;
    struct sockaddr_in  Recv_Node;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1               au1Cmsg[IGMP_AUXILLARY_LEN];
    struct iovec        Iov;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct msghdr       PktInfo;
    UINT1              *pRecvPkt = NULL;
    UINT4               u4IfIndex;
    INT4                i4SocketId;
    INT4                i4RecvBytes;
    tIpParms           *pIpParms = NULL;
    socklen_t           i4RecvNodeLen;
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandlePktArrivalEvent()\r \n");
    i4RecvNodeLen = sizeof (struct sockaddr_in);
    MEMSET (&Recv_Node, 0, sizeof (struct sockaddr_in));

    if ((pRecvPkt = MEM_MALLOC (IGMP_MAX_MTU_SIZE, UINT1)) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE, 
		  IgmpGetModuleName(IGMP_OS_RES_MODULE),
                  "Unable to allocate memory for recv pkt\n");
        return;
    }

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &Recv_Node;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pRecvPkt;
    Iov.iov_len = IGMP_MAX_MTU_SIZE;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    i4SocketId = gIgmpConfig.i4SocketId;
    i4RecvBytes = 0;

    while ((i4RecvBytes = recvmsg (i4SocketId, &PktInfo, 0)) > 0)
    {
        pIpPktInfo = (struct in_pktinfo *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

        u4IfIndex = (UINT4) pIpPktInfo->ipi_ifindex;

        pIfNode = IGMP_GET_IF_NODE (u4IfIndex, IPVX_ADDR_FMLY_IPV4);
        if (pIfNode == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                      "EXIT : Unable to get the interface node\n");
            MEM_FREE (pRecvPkt);
            return;
        }

        /* Copy the recvd linear buf to cru buf */
        if ((pPkt = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
		      IgmpGetModuleName(IGMP_BUFFER_MODULE),
                      "Unable to allocate buffer\n");
            SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                             IGMP_NAME,IgmpSysErrString[BUFFER_MEM_ALLOC_FAIL],
			    "Handling Igmp PktArrivalEvent for Interface Index :%d\r\n", 
			     u4IfIndex);
	    MEM_FREE (pRecvPkt);
            return;
        }

        pIpParms = (tIpParms *) (&pPkt->ModuleData);
        pIpParms->u2Port = u4IfIndex;

        CRU_BUF_Copy_OverBufChain (pPkt, pRecvPkt, 0, i4RecvBytes);

        pIpHdr =
            (t_IP_HEADER *) CRU_BUF_Get_DataPtr_IfLinear (pPkt,
                                                          0,
                                                          sizeof (t_IP_HEADER));

        if (pIpHdr == NULL)
        {
            /* The header is not contiguous in the buffer */
            pIpHdr = &TmpIpHdr;

            /* Copy the header */
            CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) pIpHdr,
                                       0, sizeof (t_IP_HEADER));
        }

        if (pIpHdr->u1Proto == IGMP_PROTID)
        {
            IgmpInput (pPkt);
            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        }
        else
        {
            /* In case of IGMP Proxy send the mcast data 
             * packet to IGMP Proxy MDP Queue */
            IgpPortHandleMcastDataPkt (pPkt);
        }
    }

    MEM_FREE (pRecvPkt);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpHandlePktArrivalEvent()\r \n");
    return;
}
#else
#ifdef NP_KERNEL_WANTED
/* Only for BCM Linuxip */
/***************************************************************************
* Function Name          : IgmpRecvTaskMain
*
* Description            : This function read packets from the char device and 
*                          posts event to the IGMP task.
*                 
* Global Variables 
* Referred               : None
*
* Gobal Variables 
* Modified               : None
*              
* Input (s)              : None 
*
* Output (s)             : None
*
* Returns                : IGMP_OK
*                          IGMP_NOT_OK
****************************************************************************/

VOID
IgmpRecvTaskMain (VOID)
{
    UINT1               au1Buf[IGMP_MAX_MTU_SIZE];
    tIP_INTERFACE       IfId;
    tHeader            *Header = NULL;
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    tHandlePacketRxCallBack *pData = NULL;
    tIpParms           *pIpParms = NULL;
    UINT4               u4IfIndex;
    UINT4               u4PktLen;
    UINT1              *pu1PktData = NULL;
    INT4                i4PktDataOffset;
    INT4                i4Len = 0;
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    MEMSET (&IfId, 0, sizeof (tIP_INTERFACE));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpRecvTaskMain()\r \n");
    while (1)
    {
        MEMSET (au1Buf, 0, IGMP_MAX_MTU_SIZE);
        if ((i4Len = read (gi4IgmpDevFd, au1Buf, IGMP_MAX_MTU_SIZE)) > 0)
        {
            Header = (tHeader *) au1Buf;
        }
        else
        {
            Header = NULL;
        }

        if (Header != NULL)
        {
            i4PktDataOffset = sizeof (tHeader) +
                sizeof (tHandlePacketRxCallBack);
            pData = (tHandlePacketRxCallBack *) (au1Buf + sizeof (tHeader));
            if (!pData)
            {
                continue;
            }

            u4IfIndex = pData->u4IfIndex;
            u4PktLen = pData->u4PktLen;
            pu1PktData = (UINT1 *) (au1Buf + i4PktDataOffset);

            /* Copy the recvd linear buf to cru buf */
            if ((pPkt = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0)) == NULL)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULES, IGMP_NAME,
                          "Unable to allocate buffer\n");
       		IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                      		IgmpGetModuleName(IGMP_BUFFER_MODULE),
                      		"Unable to allocate buffer\n");
		SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                                 IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
				 "Handling Igmp PktArrivalEvent for Interface Index :%d\r\n",
                                 u4IfIndex);
	        return;
            }

            pIpParms = (tIpParms *) (&pPkt->ModuleData);
            pIpParms->u2Port = u4IfIndex;

            CRU_BUF_Copy_OverBufChain (pPkt, pu1PktData + IGMP_ETH_HDR_LEN, 0,
                                       u4PktLen);

            pIpHdr =
                (t_IP_HEADER *) CRU_BUF_Get_DataPtr_IfLinear (pPkt,
                                                              0,
                                                              sizeof
                                                              (t_IP_HEADER));

            if (pIpHdr == NULL)
            {
                /* The header is not contiguous in the buffer */
                pIpHdr = &TmpIpHdr;

                /* Copy the header */
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) pIpHdr,
                                           0, sizeof (t_IP_HEADER));
            }

            if (pIpHdr->u1Proto == IGMP_PROTID)
            {
                IgmpHandleIncomingPkt (pPkt, 0, 0, IfId, 0);
            }
            else
            {
                /* In case of IGMP Proxy send the mcast data 
                 * packet to IGMP Proxy MDP Queue */
                IgpPortHandleMcastDataPkt (pPkt);
            }
        }
    }
 MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpRecvTaskMain()\r \n");
}
#endif /* NP_KERNEL_WANTED */
#endif /* ISS_WANTED */

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpIpExtractHdr                                           */
/*                                                                           */
/* Description  : Function to extract the IP header                          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP_SUCCESS / IP_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpIpExtractHdr (t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1Tmp;
    UINT1              *pu1Options;
    UINT4               u4TempSrc = 0, u4TempDest = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpIpExtractHdr()\r \n");
    pIpHdr =
        (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                               IP_HDR_LEN);

    if (pIpHdr == NULL)
    {

        /* The header is not contiguous in the buffer */

        pIpHdr = &TmpIpHdr;

        /* Copy the header */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    u1Tmp = pIpHdr->u1Ver_hdrlen;

    pIp->u1Version = (u1Tmp >> 4);
    pIp->u1Hlen = (u1Tmp & 0x0f) << 2;
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u2Len = CRU_NTOHS (pIpHdr->u2Totlen);

    if (pIp->u1Hlen < IP_HDR_LEN || pIp->u2Len < pIp->u1Hlen)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Discarding pkt rcvd with header length %d.\n",
                       pIp->u1Hlen);
	IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_OS_RES_MODULE | IGMP_ALL_MODULES,
    	                IgmpGetModuleName(IGMP_OS_RES_MODULE),
                       "Discarding pkt rcvd with header length %d.\n",
                       pIp->u1Hlen);

        return IP_FAILURE;
    }

    pIp->u2Olen = (((u1Tmp & 0x0f) * 4) - IP_HDR_LEN);
    pIp->u2Id = CRU_NTOHS (pIpHdr->u2Id);
    pIp->u2Fl_offs = CRU_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = CRU_NTOHS (pIpHdr->u2Cksum);

    MEMCPY (&u4TempSrc, &pIpHdr->u4Src, sizeof (UINT4));
    MEMCPY (&u4TempDest, &pIpHdr->u4Dest, sizeof (UINT4));

    u4TempSrc = CRU_NTOHL (u4TempSrc);
    u4TempDest = CRU_NTOHL (u4TempDest);

    MEMCPY (&pIp->u4Src, &u4TempSrc, sizeof (UINT4));
    MEMCPY (&pIp->u4Dest, &u4TempDest, sizeof (UINT4));

    if (pIp->u2Olen)
    {
        pu1Options =
            CRU_BUF_Get_DataPtr_IfLinear (pBuf, IP_HDR_LEN, pIp->u2Olen);
        if (pu1Options == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIp->au1Options,
                                       IP_HDR_LEN, pIp->u2Olen);
        }
        else
        {
            MEMCPY (pIp->au1Options, pu1Options, pIp->u2Olen);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpIpExtractHdr()\r \n");
    return IP_SUCCESS;
}
#else
/*************************************************************************n
 * Function Name    :  IgmpHandlePktArrivalEvent
 *
 * Description      :  This function handles IGMP packets from LINUX IP.
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
IgmpHandlePktArrivalEvent (VOID)
{
    INT1               *pi1RecvBuf = NULL;
    INT4                i4DataLen;
    struct sockaddr_in  PeerAddr;
    INT4                i4AddrLen;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandlePktArrivalEvent()\r \n");
    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpRecvBufId, pi1RecvBuf, INT1) ==
        NULL)
    {
	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
		  IgmpGetModuleName(IGMP_OS_RES_MODULE),
                  "Memory Allocation Failure.\n");

        return;
    }

    MEMSET (pi1RecvBuf, IGMP_ZERO, IGMP_MAX_MTU_SIZE);

    /* Call recvfrom to receive the packet  and release that
     * memory to RAW mem pool */
    if ((i4DataLen = recvfrom (gIgmpConfig.i4SocketId, pi1RecvBuf,
                               (INT4) IGMP_MAX_MTU_SIZE, 0,
                               (struct sockaddr *) &PeerAddr, &i4AddrLen)) <= 0)
    {
	IgmpMemRelease (gIgmpMemPool.IgmpRecvBufId, (UINT1 *) pi1RecvBuf);
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
		       IgmpGetModuleName(IGMP_OS_RES_MODULE),
                       "Memory is released\n");        
        return;
    }
    IgmpMemRelease (gIgmpMemPool.IgmpRecvBufId, (UINT1 *) pi1RecvBuf);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting  IgmpHandlePktArrivalEvent()\r \n");
    return;

}
#endif /* LNXIP4_WANTED */
