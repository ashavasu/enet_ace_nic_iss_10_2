/* $Id: fsmldlow.c,v 1.4 2015/02/13 11:13:52 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmldlow.h"
# include  "igmpinc.h"
# include  "fsmgmdcli.h"

extern UINT4        stdmld[7];
extern UINT4        fsmld[8];
extern UINT4        FsmldProtocolUpDown[10];
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsmldNoOfCacheEntries
 Input       :  The Indices

                The Object 
                retValFsmldNoOfCacheEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldNoOfCacheEntries (UINT4 *pu4RetValFsmldNoOfCacheEntries)
{
    UNUSED_PARAM (pu4RetValFsmldNoOfCacheEntries);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsmldNoOfRoutingProtocols
 Input       :  The Indices

                The Object 
                retValFsmldNoOfRoutingProtocols
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldNoOfRoutingProtocols (UINT4 *pu4RetValFsmldNoOfRoutingProtocols)
{
    UNUSED_PARAM (pu4RetValFsmldNoOfRoutingProtocols);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsmldTraceDebug
 Input       :  The Indices

                The Object 
                retValFsmldTraceDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldTraceDebug (UINT4 *pu4RetValFsmldTraceDebug)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetFsIgmpTraceLevel ((INT4 *) pu4RetValFsmldTraceDebug,
                                            IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsmldDebugLevel
 Input       :  The Indices

                The Object
                retValFsmldDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldDebugLevel (UINT4 *pu4RetValFsmldDebuglevel)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetFsIgmpDebugLevel ((INT4 *) pu4RetValFsmldDebuglevel,
                                            IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsmldMode
 Input       :  The Indices

                The Object 
                retValFsmldMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldMode (INT4 *pi4RetValFsmldMode)
{
    UNUSED_PARAM (pi4RetValFsmldMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsmldProtocolUpDown
 Input       :  The Indices

                The Object 
                retValFsmldProtocolUpDown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsmldProtocolUpDown (INT4 *pi4RetValFsmldProtocolUpDown)
{
    /* To find whether the MLD task is active, the global variable 
     * can be checked to wheather memory is allocated for it 
     */
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhGetFsIgmpGlobalStatus (pi4RetValFsmldProtocolUpDown,
                                              IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsmldNoOfCacheEntries
 Input       :  The Indices

                The Object 
                setValFsmldNoOfCacheEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldNoOfCacheEntries (UINT4 u4SetValFsmldNoOfCacheEntries)
{
    UNUSED_PARAM (u4SetValFsmldNoOfCacheEntries);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsmldNoOfRoutingProtocols
 Input       :  The Indices

                The Object 
                setValFsmldNoOfRoutingProtocols
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldNoOfRoutingProtocols (UINT4 u4SetValFsmldNoOfRoutingProtocols)
{
    UNUSED_PARAM (u4SetValFsmldNoOfRoutingProtocols);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsmldTraceDebug
 Input       :  The Indices

                The Object 
                setValFsmldTraceDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldTraceDebug (UINT4 u4SetValFsmldTraceDebug)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhSetFsIgmpTraceLevel (u4SetValFsmldTraceDebug,
                                                   IPVX_ADDR_FMLY_IPV6);
    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsScalars (FsMgmdMldTraceLevel,
                                    sizeof (FsMgmdMldTraceLevel) /
                                    sizeof (UINT4), u4SetValFsmldTraceDebug);

    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsmldDebugLevel
 Input       :  The Indices

                The Object
                setValFsmldDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldDebugLevel (UINT4 u4SetValFsmldDebuglevel)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhSetFsIgmpDebugLevel (u4SetValFsmldDebuglevel,
                                                   IPVX_ADDR_FMLY_IPV6);
    if (i1RetVal == SNMP_SUCCESS)
    {
#if 0
        IgmpUtilIncMsrForFsScalars (FsMgmdMldDebugLevel,
                                    sizeof (FsMgmdMldDebugLevel) /
                                    sizeof (UINT4), u4SetValFsmldDebuglevel);
#endif

    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsmldMode
 Input       :  The Indices

                The Object 
                setValFsmldMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldMode (INT4 i4SetValFsmldMode)
{
    UNUSED_PARAM (i4SetValFsmldMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsmldProtocolUpDown
 Input       :  The Indices

                The Object 
                setValFsmldProtocolUpDown
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsmldProtocolUpDown (INT4 i4SetValFsmldProtocolUpDown)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal =
        IgmpMgmtUtilNmhSetFsIgmpGlobalStatus (i4SetValFsmldProtocolUpDown,
                                              IPVX_ADDR_FMLY_IPV6);

    if (i1RetVal == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsScalars (FsMgmdMldGlobalStatus,
                                    sizeof (FsMgmdMldGlobalStatus) /
                                    sizeof (UINT4),
                                    i4SetValFsmldProtocolUpDown);

    }

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsmldNoOfCacheEntries
 Input       :  The Indices

                The Object 
                testValFsmldNoOfCacheEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldNoOfCacheEntries (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsmldNoOfCacheEntries)
{
    UNUSED_PARAM (u4TestValFsmldNoOfCacheEntries);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsmldNoOfRoutingProtocols
 Input       :  The Indices

                The Object 
                testValFsmldNoOfRoutingProtocols
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldNoOfRoutingProtocols (UINT4 *pu4ErrorCode,
                                    UINT4 u4TestValFsmldNoOfRoutingProtocols)
{
    UNUSED_PARAM (u4TestValFsmldNoOfRoutingProtocols);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsmldTraceDebug
 Input       :  The Indices

                The Object 
                testValFsmldTraceDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldTraceDebug (UINT4 *pu4ErrorCode, UINT4 u4TestValFsmldTraceDebug)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel (pu4ErrorCode,
                                                      (INT4) u4TestValFsmldTraceDebug,
                                                      IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsmldDebugLevel
 Input       :  The Indices 
 The Object
                testValFsmldDebugLevel                                                                         
 Output      :  The Test Low Lev Routine Take the Indices &                                                                       Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val                                                   Error Codes :  The following error codes are to be returned                                                                      SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldDebugLevel (UINT4 *pu4ErrorCode, UINT4 u4TestValFsmldDebuglevel)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel (pu4ErrorCode,
                                                      (INT4) u4TestValFsmldDebuglevel,
                                                      IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsmldMode
 Input       :  The Indices

                The Object 
                testValFsmldMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsmldMode)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsmldMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsmldProtocolUpDown
 Input       :  The Indices

                The Object 
                testValFsmldProtocolUpDown
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsmldProtocolUpDown (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsmldProtocolUpDown)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus (pu4ErrorCode,
                                                        i4TestValFsmldProtocolUpDown,
                                                        IPVX_ADDR_FMLY_IPV6);
    return i1RetVal;
}                                /* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsmldNoOfCacheEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldNoOfCacheEntries (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldNoOfRoutingProtocols
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldNoOfRoutingProtocols (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldTraceDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldTraceDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhDepv2FsIgmpTraceLevel (pu4ErrorCode,
                                                     pSnmpIndexList,
                                                     pSnmpVarBind);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsmldDebugLevel(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhDepv2FsIgmpDebugLevel (pu4ErrorCode,
                                                     pSnmpIndexList,
                                                     pSnmpVarBind);
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsmldProtocolUpDown
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsmldProtocolUpDown (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = IgmpMgmtUtilNmhDepv2FsIgmpGlobalStatus (pu4ErrorCode,
                                                       pSnmpIndexList,
                                                       pSnmpVarBind);
    return i1RetVal;
}
