#ifndef __IGMPCLI_C__
#define __IGMPCLI_C__
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpcli.c,v 1.89 2017/12/19 10:04:20 siva Exp $
 *
 * Description: this file handles routines for CLI SET/GET destined 
 *              for IGMP module                         
 *
 *******************************************************************/

#include "igmpinc.h"
#include "igmpcli.h"
#include "igmpcliprot.h"
#include "stdigmwr.h"
#include "fsigmpwr.h"
#include "cfa.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE;
#endif

/***************************************************************************/
/*                                                                         */
/*     Function Name :  cli_process_igmp_cmd                               */
/*                                                                         */
/*     Description   :  Calls appropriate command routine                  */
/*                                                                         */
/*     Input(s)      : .pInMsg    -Contains the command to be exicuted     */
/*                                                                         */
/*     Output(s)     : .ppRespMsg  -Output response message to be displayed*/
/*                                                                         */
/*     Returns       :    None                                             */
/*                                                                         */
/***************************************************************************/
INT1
cli_process_igmp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4Status = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst = 0;
    va_start (ap, u4Command);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering cli_process_igmp_cmd()\r \n");
    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
        i4IfaceIndex = (UINT4) i4Inst;
    }
    else
    {
        i4IfaceIndex = CLI_GET_IFINDEX ();
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store 5 arguements at the max. This is because igmp commands do not
     * take more than 5 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == IGMP_MAX_ARGS)
            break;
    }

    va_end (ap);
    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();
    CLI_SET_ERR (IGMP_ZERO);

    switch (u4Command)
    {
        case IGMP_CLI_STATUS:

            /* args[0] -> contains the IGMP Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) ==
                 CLI_ENABLE) ? IGMP_ENABLE : IGMP_DISABLE;
            i4RetStatus = IgmpCliSetStatus (CliHandle, i4Status);
            break;

        case IGMP_CLI_IFACE_STATUS:

            /* args[0] -> contains the IGMP Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) ==
                 CLI_ENABLE) ? IGMP_ENABLE : IGMP_DISABLE;
            i4RetStatus =
                IgmpCliSetIfaceStatus (CliHandle, i4IfaceIndex, i4Status);
            break;

        case IGMP_CLI_FAST_LEAVE:

            /* args[0] -> contains the IGMP fast Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) == CLI_ENABLE) ?
                IGMP_FAST_LEAVE_ENABLE : IGMP_FAST_LEAVE_DISABLE;
            i4RetStatus =
                IgmpCliSetFastLeave (CliHandle, i4IfaceIndex, i4Status);
            break;

        case IGMP_CLI_VERSION:

            /* args[0] -> contains the IGMP Version */
            i4RetStatus =
                IgmpCliSetIgmpVersion (CliHandle, i4IfaceIndex,
                                       CLI_PTR_TO_U4 (args[0]));
            break;

        case IGMP_CLI_NO_VERSION:

            /* args[0] -> contains the IGMP Version */
            i4RetStatus =
                IgmpCliSetNoIgmpVersion (CliHandle, i4IfaceIndex,
                                         CLI_PTR_TO_U4 (args[0]));
            break;

        case IGMP_CLI_QRY_INTRVL:

            /* args[0] -> contains the IGMP fast Enable/Disable Status */
            i4Status =
                (args[0] == NULL) ?
                IGMP_NO_QUERY_INTERVAL : (*(INT4 *) (args[0]));

            i4RetStatus =
                IgmpCliSetQryInterval (CliHandle, i4IfaceIndex, i4Status);
            break;

        case IGMP_CLI_QRY_MAXRESPTIME:

            /* args[0] -> contains the IGMP Max response time */
            i4Status =
                (args[0] == NULL) ?
                IGMP_NO_MAX_RESP_TIME : (*(INT4 *) (args[0]));

            i4RetStatus =
                IgmpCliSetQryMaxRespTime (CliHandle, i4IfaceIndex, i4Status);
            break;

        case IGMP_CLI_ROBUSTNESS:

            /* args[0] -> contains the IGMP Max response time */
            i4Status =
                (args[0] == NULL) ?
                IGMP_NO_ROB_VARIABLE : (*(INT4 *) (args[0]));

            i4RetStatus =
                IgmpCliSetRobustnessValue (CliHandle, i4IfaceIndex, i4Status);
            break;

        case IGMP_CLI_LAST_MBRQRYINTRL:

            /* args[0] -> contains the IGMP Max response time */
            i4Status =
                (args[0] == NULL) ?
                IGMP_NO_LAST_MEMBER_QIVAL : (*(INT4 *) (args[0]));

            i4RetStatus =
                IgmpCliSetLastMbrQryInterval (CliHandle, i4IfaceIndex,
                                              i4Status);
            break;

        case IGMP_CLI_STATIC_MEMBERSHIP:

            if (args[0] != NULL)
            {
                if (args[1] != NULL)
                {
                    i4RetStatus =
                        IgmpCliAddStaticGroup (CliHandle, i4IfaceIndex,
                                               (*args[0]), (*args[1]));
                }
                else
                {
                    i4RetStatus =
                        IgmpCliAddStaticGroup (CliHandle, i4IfaceIndex,
                                               (*args[0]), 0);
                }
            }

            break;

        case IGMP_CLI_NO_STATIC_MEMBERSHIP:

            if (args[0] != NULL)
            {
                if (args[1] != NULL)
                {
                    i4RetStatus =
                        IgmpCliDeleteStaticGroup (CliHandle, i4IfaceIndex,
                                                  (*args[0]), (*args[1]));
                }
                else
                {
                    i4RetStatus =
                        IgmpCliDeleteStaticGroup (CliHandle, i4IfaceIndex,
                                                  (*args[0]), 0);
                }
            }
            break;

        case IGMP_CLI_DEL_INTERFACE:

            i4RetStatus = IgmpCliDeleteInterface (CliHandle, i4IfaceIndex);
            break;

        case IGMP_CLI_CHANNEL_TRACK:

            /* args[0] -> contains the IGMP Channel track Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) == CLI_ENABLE) ?
                IGMP_CHANNEL_TRACK_ENABLE : IGMP_CHANNEL_TRACK_DISABLE;
            i4RetStatus =
                IgmpCliSetChannelTrack (CliHandle, i4IfaceIndex, i4Status);
            break;

        case IGMP_CLI_RATE_LIMIT:

            /* args[0] -> contains the rate limit value */
            i4RetStatus =
                IgmpCliSetRateLimit (CliHandle, *(INT4 *) (VOID *) (args[0]));
            break;

        case IGMP_CLI_NO_RATE_LIMIT:

            i4RetStatus =
                IgmpCliSetRateLimit (CliHandle, IGMP_DEFAULT_RATE_LIMIT);
            break;

        case IGMP_CLI_IF_RATE_LIMIT:

            /* args[0] -> contains the rate limit value for interface */
            i4RetStatus =
                IgmpCliSetInterfaceRateLimit (CliHandle, i4IfaceIndex,
                                              *(INT4 *) (VOID *) (args[0]));
            break;

        case IGMP_CLI_IF_NO_RATE_LIMIT:

            i4RetStatus =
                IgmpCliSetInterfaceRateLimit (CliHandle, i4IfaceIndex,
                                              IGMP_DEFAULT_INT_RATE_LIMIT);
            break;

        case IGMP_CLI_SHOW_GBL_CONFIG:
            i4RetStatus = IgmpCliShowGlobalInfo (CliHandle);
            break;

        case IGMP_CLI_SHOW_INTF_CONFIG:

            if (args[0] != NULL)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus =
                    IgmpCliShowInterfaceInfo (CliHandle, i4IfaceIndex);
            }
            else
            {
                i4RetStatus =
                    IgmpCliShowInterfaceInfo (CliHandle, IGMP_INVALID_INDEX);
            }
            break;

        case IGMP_CLI_SHOW_GROUPS:
            i4RetStatus = IgmpCliShowGroupInfo (CliHandle);
            break;

        case IGMP_CLI_SHOW_SOURCES:
            i4RetStatus = IgmpCliShowSourceInfo (CliHandle);
            break;

        case IGMP_CLI_SHOW_MEMBERSHIP:
            i4RetStatus = IgmpCliShowMembership (CliHandle,
                                                 CLI_PTR_TO_I4 (args[0]));
            break;

        case IGMP_CLI_SHOW_GRP_CHANNEL:
            i4RetStatus = IgmpCliShowGroupChannel (CliHandle, *(args[0]),
                                                   CLI_PTR_TO_I4 (args[1]));
            break;

        case IGMP_CLI_SHOW_STATS:

            if (args[0] != NULL)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus = IgmpCliShowStatistics (CliHandle, i4IfaceIndex);
            }
            else
            {
                i4RetStatus =
                    IgmpCliShowStatistics (CliHandle, IGMP_INVALID_INDEX);
            }

            break;

        case IGMP_CLI_CLEAR_STATS:
            if (args[0] != NULL)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus = IgmpCliClearStats (CliHandle, i4IfaceIndex);
            }
            else
            {
                i4RetStatus = IgmpCliClearStats (CliHandle, IGMP_INVALID_INDEX);
            }

            break;

        case IGMP_CLI_TRACE:
            /* args[0] contains Trace flag for the Modules */
            i4RetStatus = IgmpCliSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        case IGMP_CLI_DEBUG:
            /* args[0] contains Debug flag for the Modules */
            i4RetStatus = IgmpCliSetDebug (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        case IGMP_CLI_GLOBAL_LIMIT:
            /* args[0] contains the limit value */
            if (args[0] != NULL)
            {
                i4RetStatus = IgmpCliSetGlobalLimit (CliHandle, *(args[0]));
            }
            else
            {
                i4RetStatus = IgmpCliSetGlobalLimit (CliHandle, IGMP_ZERO);
            }
            break;
        case IGMP_CLI_INTF_LIMIT:
            /* args[0] contains the limit value , args[1] contains grouplist id */
            if (args[0] != NULL)
            {
                if (args[1] != NULL)
                {
                    i4RetStatus =
                        IgmpCliSetInterfaceLimit (CliHandle, i4IfaceIndex,
                                                  *(args[0]), *(args[1]));
                }
                else
                {
                    i4RetStatus =
                        IgmpCliSetInterfaceLimit (CliHandle, i4IfaceIndex,
                                                  *(args[0]), IGMP_ZERO);
                }
            }
            else
            {
                i4RetStatus = IgmpCliSetInterfaceLimit (CliHandle, i4IfaceIndex,
                                                        IGMP_ZERO, IGMP_ZERO);
            }
            break;
        case IGMP_CLI_GROUP_LIST_CONFIG:
            /* args[0] - GroupList ID 
             * args[1] - Group Ip
             * args[2] - Group Mask
             * args[3] - set/reset*/
            if (args[1] != NULL)
            {
                i4RetStatus =
                    IgmpCliSetGroupList (CliHandle, *args[0], *args[1],
                                         *args[2], CLI_PTR_TO_I4 (args[3]));
            }
            else
            {
                i4RetStatus =
                    IgmpCliSetGroupList (CliHandle, *args[0], 0, 0,
                                         CLI_PTR_TO_I4 (args[3]));
            }
            break;
        case IGMP_CLI_SHOW_LIST_CONFIG:
            i4RetStatus = IgmpCliShowGroupList (CliHandle);
            break;
        case IGMP_CLI_SSM_MAP_STATUS:
            /* args[0] -> contains the IGMP SSM Map Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) ==
                 CLI_ENABLE) ? IGMP_ENABLE : IGMP_DISABLE;
            i4RetStatus = IgmpCliSetSSMMapStatus (CliHandle, i4Status);
            break;
        case IGMP_CLI_SSM_MAP_CONFIG:
            /* args[0] -> Starting multicast group address
               args[1] -> Ending multicast group address
               args[2] -> Unicast IP address of source
               args[3] -> Configure / Delete SSM Mapping */
            i4RetStatus = IgmpCliConfigureSSMMap (CliHandle, (*args[0]),
                                                  (*args[1]), (*args[2]),
                                                  CLI_PTR_TO_I4 (args[3]));
            break;
        case IGMP_CLI_SHOW_SSM_MAP:
            /* args[0] -> Multicast group address */
            if (args[0] != NULL)
            {
                i4RetStatus = IgmpCliShowSSMMapGroup (CliHandle, (*args[0]));
            }
            else
            {
                i4RetStatus = IgmpCliShowSSMMapGroup (CliHandle, 0);
            }
            break;
        default:
            CliPrintf (CliHandle, "%% Unknown command \r\n");
            CliUnRegisterLock (CliHandle);
            IGMP_UNLOCK ();
            return CLI_ERROR;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_IGMP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", IgmpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    IGMP_UNLOCK ();
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  cli_process_igmp_cmd()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetStatus                                   */
/*                                                                         */
/*     Description   :  Enables/disables IGMP globally                     */
/*                                                                         */
/*     INPUT         : i4Status - IGMP Enable/ disable                      */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetStatus()\r \n");
    if (nmhTestv2FsIgmpGlobalStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Igmp is globally disabled\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpGlobalStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Enabling Igmp globally failed\n");
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetStatus()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetIfaceStatus                              */
/*                                                                         */
/*     Description   :  Enables/Disables Igmp on the interface             */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Status - IGMP Enable/disable                      */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetIfaceStatus (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetIfaceStatus()\r \n");
    if (nmhTestv2FsIgmpInterfaceAdminStatus (&u4ErrorCode, i4IfIndex, i4Status)
        == SNMP_FAILURE)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Igmpv2 admin status of interface %d is down \r\n",
                   i4IfIndex);
        return CLI_FAILURE;
    }

    if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                      IGMP_CREATE, &u1Create,
                                      IGMP_FALSE) != CLI_SUCCESS)
    {
        IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "Updation of interface entry %d failed\r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpInterfaceAdminStatus (i4IfIndex, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Igmp admin status of interface %d is down \r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (u1Create == IGMP_TRUE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_SET_ACTIVE, NULL,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetIfaceStatus()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetFastLeave                                */
/*                                                                         */
/*     Description   :  Enables/Disables fast leave on the interface       */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Status - IGMP fast leave Enable/disable           */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetFastLeave (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode;
    INT4                i4IfStatus = 0;
    UINT1               u1Create = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetFastLeave()\r \n");
    if (i4Status != IGMP_FAST_LEAVE_DISABLE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_CREATE, &u1Create,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhGetIgmpInterfaceStatus (i4IfIndex, &i4IfStatus) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Igmp not enabled on interface %d \n", i4IfIndex);
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2FsIgmpInterfaceFastLeaveStatus (&u4ErrorCode, i4IfIndex,
                                                 i4Status) != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid Interface Fast Leave Status");

        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpInterfaceFastLeaveStatus (i4IfIndex, i4Status)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface Fast Leave Status could not be set");
        return CLI_FAILURE;
    }

    if (u1Create == IGMP_TRUE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_SET_ACTIVE, NULL,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "Updation of interface entry %d failed\r\n", i4IfIndex);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetFastLeave()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetChannelTrack                             */
/*                                                                         */
/*     Description   :  Enables/Disables channel track on the interface    */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Status - IGMP channel track Enable/disable        */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetChannelTrack (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = IGMP_ZERO;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetChannelTrack()\r \n");
    if (nmhTestv2FsIgmpInterfaceChannelTrackStatus
        (&u4ErrorCode, i4IfIndex, i4Status) != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Invalid Interface channel track Status");
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpInterfaceChannelTrackStatus (i4IfIndex, i4Status)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface channel track Status could not be set for interface"
                   "%d\r\n", i4IfIndex);
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetChannelTrack()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetIgmpVersion                              */
/*                                                                         */
/*     Description   :  Configures the IGMP version for this interface     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Version - IGMP version                            */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetIgmpVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Port = 0;
    UINT1               u1Create = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetIgmpVersion()\r \n");

    /* As this mib object is read-Create object,
     * if the interface table entry is not avaiable
     * Create the interface and update the query interval
     */
    if (IgmpGetPortFromIfIndex (i4IfIndex,
                                IPVX_ADDR_FMLY_IPV4, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting  IgmpCliSetIgmpVersion()\r \n");
        return CLI_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);
    if (pIfaceNode != NULL)
    {
        if (pIfaceNode->u1Version == (UINT1) u4Version)
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpCliSetIgmpVersion()\r \n");
            IGMP_TRC_ARG1 (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Same Igmp Interface Version %d \r\n", u4Version);
            return CLI_SUCCESS;
        }
    }

    if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                      IGMP_CREATE, &u1Create,
                                      IGMP_FALSE) != CLI_SUCCESS)
    {
        IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "Updation of interface entry %d failed\r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhTestv2IgmpInterfaceVersion (&u4ErrorCode, i4IfIndex,
                                       u4Version) != SNMP_SUCCESS)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid Igmp Interface Version %d\n", u4Version);
        return CLI_FAILURE;
    }

    /* After Test Validation Row status modification is done
     * to avoid the impact caused during Test Validation failure */
    if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                      IGMP_CREATE, &u1Create,
                                      IGMP_TRUE) != CLI_SUCCESS)
    {
        IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "Updation of interface entry %d failed\r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhSetIgmpInterfaceVersion (i4IfIndex, u4Version) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Unable to set Igmp Interface Version on "
                   "interface %d\r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                      IGMP_SET_ACTIVE, NULL,
                                      IGMP_FALSE) != CLI_SUCCESS)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE,
                   IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                   "Updation of interface entry %d failed\r\n", i4IfIndex);
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetIgmpVersion()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetNoIgmpVersion                              */
/*                                                                         */
/*     Description   :  Configures the IGMP version for this interface     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Version - IGMP version                            */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetNoIgmpVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT1                i1Status = SNMP_FAILURE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetNoIgmpVersion\r \n");

    i1Status = nmhGetIgmpInterfaceStatus (i4IfIndex, &i4RowStatus);

    if (i1Status == SNMP_FAILURE)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid Interface Status for interface %d\r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhTestv2IgmpInterfaceVersion (&u4ErrorCode, i4IfIndex,
                                       u4Version) != SNMP_SUCCESS)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid Igmp Interface Version for interafce %d\r\n",
                   i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhSetIgmpInterfaceVersion (i4IfIndex, u4Version) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "IGMP interface vesrion %d could not be set on interface %d\r\n",
                   u4Version, i4IfIndex);
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetNoIgmpVersion()\r \n");

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetQryInterval                              */
/*                                                                         */
/*     Description   :  Configures the IGMP query interval for this        */
/*                      interface                                          */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Interval - IGMP query interval                    */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetQryInterval (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetQryInterval()\r \n");
    if (u4Interval != IGMP_NO_QUERY_INTERVAL)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_CREATE, &u1Create,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Interval = IGMP_DEFAULT_QUERY_INTERVAL;
    }

    if (nmhTestv2IgmpInterfaceQueryInterval (&u4ErrorCode, i4IfIndex,
                                             u4Interval) != SNMP_SUCCESS)
    {
        /*Query-interval between 1 to 10 should be configured
         *only for IGMP Version 1 */
        if (u4ErrorCode == IGMP_ONE)
        {
            CliPrintf (CliHandle,
                       "\r%%Query interval should be greater than 10"
                       " for IGMP Version 2 and 3\r\n");
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Query interval %d could not be set\n", u4Interval);

        }
        return CLI_FAILURE;
    }

    if (nmhSetIgmpInterfaceQueryInterval (i4IfIndex,
                                          u4Interval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_TRC_ARG2 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "setting Igmp Interface Query Interval %d"
                       "on index  %d failed\r\n", u4Interval, i4IfIndex);
        return CLI_FAILURE;
    }

    if (u1Create == IGMP_TRUE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_SET_ACTIVE, NULL,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpCliSetQryInterval()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetQryMaxRespTime                           */
/*                                                                         */
/*     Description   :  Configures the IGMP query mas response time for    */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Value - IGMP query max response time              */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetQryMaxRespTime (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetQryMaxRespTime()\r \n");
    if (u4Value != IGMP_NO_MAX_RESP_TIME)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_CREATE, &u1Create,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Value = IGMP_DEFAULT_MAX_RESP_TIME;
    }

    if (nmhTestv2IgmpInterfaceQueryMaxResponseTime (&u4ErrorCode, i4IfIndex,
                                                    u4Value) == SNMP_FAILURE)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid Query Max Response Time %d \n", u4Value);
        return CLI_FAILURE;
    }

    if (nmhSetIgmpInterfaceQueryMaxResponseTime (i4IfIndex,
                                                 u4Value) == SNMP_FAILURE)
    {
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Query Max Response Time of %d on interface %d"
                   "could not be set\r\n", u4Value, i4IfIndex);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1Create == IGMP_TRUE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_SET_ACTIVE, NULL,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetQryMaxRespTime()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetRobustnessValue                          */
/*                                                                         */
/*     Description   :  Configures the IGMP robustness value for           */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Value - IGMP robustness value                     */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetRobustnessValue (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Status = IGMP_ZERO;
    UINT1               u1Create = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetRobustnessValue()\r \n");
    if (u4Value != IGMP_NO_ROB_VARIABLE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_CREATE, &u1Create,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhGetIgmpInterfaceStatus (i4IfIndex, &i4Status) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Igmp not enabled on interface %d \n", i4IfIndex);
            return CLI_FAILURE;
        }
        u4Value = IGMP_DEFAULT_ROB_VARIABLE;
    }
    if (nmhTestv2IgmpInterfaceRobustness (&u4ErrorCode, i4IfIndex,
                                          u4Value) == SNMP_FAILURE)
    {
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid Interface Robustness value %d  for interface %d\n",
                   u4Value, i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhSetIgmpInterfaceRobustness (i4IfIndex, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface Robustness value of %d could not be set on interface %d\r\n",
                   u4Value, i4IfIndex);
        return CLI_FAILURE;
    }

    if (u1Create == IGMP_TRUE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_SET_ACTIVE, NULL,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetRobustnessValue()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetLastMbrQryInterval                       */
/*                                                                         */
/*     Description   :  Configures the IGMP last member query interval for */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Interval - IGMP last member query interval        */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetLastMbrQryInterval (tCliHandle CliHandle, INT4 i4IfIndex,
                              UINT4 u4Interval)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetLastMbrQryInterval()\r \n");

    if (u4Interval != IGMP_NO_LAST_MEMBER_QIVAL)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_CREATE, &u1Create,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Interval = IGMP_DEFAULT_LAST_MEMBER_QIVAL;
    }
    if (nmhTestv2IgmpInterfaceLastMembQueryIntvl (&u4ErrorCode, i4IfIndex,
                                                  u4Interval) == SNMP_FAILURE)
    {
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid Interface LastMemb QueryIntvl value %d for interface %d\r\n",
                   u4Interval, i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhSetIgmpInterfaceLastMembQueryIntvl (i4IfIndex,
                                               u4Interval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface LastMemb QueryIntvl value of %d"
                   "can't be set on index %d\r\n", u4Interval, i4IfIndex);
        return CLI_FAILURE;
    }

    if (u1Create == IGMP_TRUE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_SET_ACTIVE, NULL,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Updation of interface entry %d failed\r\n",
                           i4IfIndex);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetLastMbrQryInterval()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliAddStaticGroup                              */
/*                                                                         */
/*     Description   :  Configures the IGMP static group membership for    */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4GroupAddr - Group address                         */
/*                   : u4SrcAddr - Source address                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliAddStaticGroup (tCliHandle CliHandle, INT4 i4IfIndex,
                       UINT4 u4GroupAddr, UINT4 u4SrcAddr)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4Version = 0;
    UINT1               u1Create = IGMP_FALSE;
    UINT4               u4ErrCode = 0;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliAddStaticGroup()\r \n");
    if (IgmpCliUpdateInterfaceTables
        (CliHandle, i4IfIndex, IGMP_CREATE, &u1Create,
         IGMP_FALSE) != CLI_SUCCESS)
    {
        IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "Updation of interface entry %d failed\r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    nmhGetIgmpInterfaceVersion (i4IfIndex, &u4Version);

    /*Check whether IGMP version is 3 before the source address is configured */
    if (u4SrcAddr != 0 && u4Version != IGMP_VERSION_3)
    {
        CliPrintf (CliHandle,
                   "\r%%Source address can be configured only for IGMP version-3\r\n");
        return CLI_FAILURE;
    }
    if ((u4Version == IGMP_VERSION_2) || (u4Version == IGMP_VERSION_1))
    {
        if (nmhTestv2IgmpCacheStatus (&u4ErrorCode, u4GroupAddr, i4IfIndex,
                                      IGMP_CREATE_AND_GO) == SNMP_SUCCESS)
        {
            if (nmhSetIgmpCacheStatus (u4GroupAddr, i4IfIndex,
                                       IGMP_CREATE_AND_GO) == SNMP_FAILURE)
            {
                if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS)
                    && (u4ErrCode > CLI_IGMP_MAX_ERR))
                {
                    CliPrintf (CliHandle, "\r%%Static Group not added\r\n");
                }
                return CLI_FAILURE;
            }
        }
        else
        {
            if (u4ErrorCode == SNMP_ERR_BAD_VALUE)
            {
                CliPrintf (CliHandle,
                           "\r%%IGMP group cannot be added,"
                           "Group membership Dynamically learned\n");
                IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                               IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                               "Updation of interface entry %d failed\r\n",
                               i4IfIndex);

            }

            if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
            {
                CliPrintf (CliHandle,
                           "\r%% Configured IGMP group is already learned\n");
            }
            return CLI_FAILURE;
        }
    }
    else
    {
        if (u4Version == IGMP_VERSION_3)
        {
            if (nmhTestv2IgmpSrcListStatus (&u4ErrorCode, u4GroupAddr,
                                            i4IfIndex, u4SrcAddr,
                                            IGMP_CREATE_AND_GO) == SNMP_SUCCESS)
            {
                if (nmhSetIgmpSrcListStatus (u4GroupAddr, (INT4) i4IfIndex,
                                             u4SrcAddr, IGMP_CREATE_AND_GO)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
                (u4Version == IGMP_ZERO))
            {
                CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
                return CLI_FAILURE;
            }
            CLI_SET_ERR (CLI_IGMP_VERSION_TO_3);
            return CLI_FAILURE;
        }
    }

    if (u1Create == IGMP_TRUE)
    {
        if (IgmpCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                          IGMP_SET_ACTIVE, NULL,
                                          IGMP_FALSE) != CLI_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliAddStaticGroup()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliDeleteStaticGroup                           */
/*                                                                         */
/*     Description   :  Deletes the IGMP static group membership for       */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4GroupAddr - Group address                         */
/*                   : u4SrcAddr - Source address                          */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliDeleteStaticGroup (tCliHandle CliHandle, INT4 i4IfIndex,
                          UINT4 u4GroupAddr, UINT4 u4SrcAddr)
{
    UINT4               u4Version = 0;
    INT4                i4Status = IGMP_ZERO;
    UINT4               u4ErrorCode = 0;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliDeleteStaticGroup()\r \n");
    if (nmhGetIgmpInterfaceStatus (i4IfIndex, &i4Status) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Igmp not enabled on interface %d \n", i4IfIndex);
        return CLI_FAILURE;
    }

    nmhGetIgmpInterfaceVersion (i4IfIndex, &u4Version);
    if (u4SrcAddr != 0 && u4Version != IGMP_VERSION_3)
    {
        CliPrintf (CliHandle,
                   "\r%%Source address can be configured only for IGMP version-3\r\n");
        return CLI_FAILURE;
    }

    if ((u4Version == IGMP_VERSION_2) || (u4Version == IGMP_VERSION_1))
    {
        if (nmhTestv2IgmpCacheStatus (&u4ErrorCode, u4GroupAddr, i4IfIndex,
                                      IGMP_DESTROY) == SNMP_FAILURE)
        {
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Validating IGMP cache status on interface %d failed \n",
                       i4IfIndex);
            return CLI_FAILURE;
        }

        if (nmhSetIgmpCacheStatus (u4GroupAddr, i4IfIndex,
                                   IGMP_DESTROY) == SNMP_FAILURE)
        {
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "setting IGMP cache status on interface %d failed\n",
                       i4IfIndex);
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2IgmpSrcListStatus (&u4ErrorCode, u4GroupAddr,
                                        i4IfIndex, u4SrcAddr,
                                        IGMP_DESTROY) == SNMP_FAILURE)
        {
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Validating IGMP source list status on interface %d"
                       "failed\n", i4IfIndex);
            return CLI_FAILURE;
        }
        if (nmhSetIgmpSrcListStatus (u4GroupAddr, i4IfIndex,
                                     u4SrcAddr, IGMP_DESTROY) == SNMP_FAILURE)
        {
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Setting IGMP source list status on interface %d"
                       "failed\n", i4IfIndex);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliDeleteStaticGroup()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliDeleteInterface                             */
/*                                                                         */
/*     Description   :  Deletes IGMP capable interface                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliDeleteInterface (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4Status = 0;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliDeleteInterface()\r \n");
    if (nmhGetIgmpInterfaceStatus (i4IfIndex, &i4Status) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Igmp not enabled on interface %d \n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhSetIgmpInterfaceStatus (i4IfIndex, IGMP_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Setting interface status 6 on interface %d failed\n",
                   i4IfIndex);
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliDeleteInterface()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetRateLimit                                */
/*                                                                         */
/*     Description   :  Configures the IGMP rate limit for joins packets   */
/*                      Globally                                           */
/*                                                                         */
/*     INPUT         : i4RateLimit - IGMP interface join rate              */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetRateLimit (tCliHandle CliHandle, INT4 i4RateLimit)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsIgmpJoinPktRate (&u4ErrorCode, i4RateLimit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpJoinPktRate (i4RateLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetInterfaceRateLimit                       */
/*                                                                         */
/*     Description   :  Configures the IGMP rate limit for joins packets   */
/*                      in this interface                                  */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4RateLimit - IGMP interface join rate              */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetInterfaceRateLimit (tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4RateLimit)
{
    UINT4               u4ErrorCode = 0;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetInterfaceRateLimit()\r \n");
    if (nmhTestv2FsIgmpInterfaceJoinPktRate
        (&u4ErrorCode, i4IfIndex, i4RateLimit) == SNMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Invalid interface join pkt rate on interface %d \n",
                   i4IfIndex);
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpInterfaceJoinPktRate (i4IfIndex,
                                          i4RateLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Setting interface join pkt rate on interface %d failed\n",
                   i4IfIndex);
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetInterfaceRateLimit()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowGlobalInfo                              */
/*                                                                         */
/*     Description   :  Displays IGMP global configuration                 */
/*                                                                         */
/*     INPUT         : None                                                */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowGlobalInfo (tCliHandle CliHandle)
{
    INT4                i4Status = 0;
    UINT4               u4Limit = 0;
    UINT4               u4CurCnt = 0;
    INT4                i4JoinPktRate = 0;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowGlobalInfo()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4Status);

    if (i4Status == IGMP_ENABLE)
    {
        CliPrintf (CliHandle, "IGMP is globally enabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP is globally disabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally disabled\r\n");
    }

    if (gIgmpConfig.i4ProxyStatus == IGMP_ENABLE)
    {
        CliPrintf (CliHandle, "IGMP Proxy is globally"
                   " enabled in the system\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP Proxy is globally enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP Proxy is globally"
                   " disabled in the system\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP Proxy is globally disabled\r\n");

    }
    nmhGetFsIgmpGlobalLimit (&u4Limit);
    {
        if (u4Limit != IGMP_ZERO)
        {
            nmhGetFsIgmpGlobalCurGrpCount (&u4CurCnt);
            CliPrintf (CliHandle,
                       "IGMP Global State Limit : %d out of max %d \r\n",
                       u4CurCnt, u4Limit);
        }
    }
    nmhGetFsIgmpJoinPktRate (&i4JoinPktRate);
    CliPrintf (CliHandle,
               "IGMP Global rate limit for joins packets : %d \r\n",
               i4JoinPktRate);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowGlobalInfo()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowInterfaceInfo                           */
/*                                                                         */
/*     Description   :  Displays IGMP interface configuration              */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowInterfaceInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tNetIpv4IfInfo      IpIfRecord;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Port = 0;
    UINT4               u4MaskBits = 0;
    UINT4               u4IfaceGroups = 0;
    UINT4               u4IfaceQryIntrval = 0;
    UINT4               u4IfaceVersion = 0;
    UINT4               u4IfaceQuerier = 0;
    UINT4               u4IfaceQryMaxRespTime = 0;
    UINT4               u4RobustnessValue = 0;
    UINT4               u4IfaceLastMbrQryIntrval = 0;
    UINT4               u4CKSumError = IGMP_ZERO;
    UINT4               u4PktLenError = IGMP_ZERO;
    UINT4               u4PktWithLocalIP = IGMP_ZERO;
    UINT4               u4SubnetCheckFailure = IGMP_ZERO;
    UINT4               u4QryFromNonQuerier = IGMP_ZERO;
    UINT4               u4ReportVersionMisMatch = IGMP_ZERO;
    UINT4               u4QryVersionMisMatch = IGMP_ZERO;
    UINT4               u4UnknownMsgType = IGMP_ZERO;
    UINT4               u4InvalidV1Report = IGMP_ZERO;
    UINT4               u4InvalidV2Report = IGMP_ZERO;
    UINT4               u4InvalidV3Report = IGMP_ZERO;
    UINT4               u4RouterAlertCheckFailure = IGMP_ZERO;
    INT4                i4IfaceFastLeaveStatus = 0;
    INT4                i4IfaceChannelTrackStatus = 0;
    INT4                i4IfaceOperStatus = 0;
    INT4                i4IfaceAdminStatus = 0;
    INT4                i4RetValue = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CHR1               *pu1String = NULL;
    UINT1               u1isShowAll = TRUE;
    UINT1               u1UpStream = IGMP_FALSE;
    INT4                i4Status;
    INT4                i4RateLimit = 0;
    UINT4               u4IntfGrpLimit = 0;
    UINT4               u4GrpListId = 0;
    UINT4               u4GrpCurCnt = 0;
    UINT4               u4IgmpInSSMPkts = 0;
    UINT4               u4IgmpInvalidSSMPkts = 0;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowInterfaceInfo()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4Status);

    if (i4Status == IGMP_ENABLE)
    {
        CliPrintf (CliHandle, "IGMP is globally enabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP is globally disabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally Disabled\r\n");
    }
    if (nmhGetFirstIndexIgmpInterfaceTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Getting first index from interface table failed\r\n");
        return CLI_SUCCESS;
    }

    do
    {
        if (i4IfIndex != IGMP_INVALID_INDEX)
        {
            if (i4IfIndex != i4NextIfIndex)
            {
                i4CurrIfIndex = i4NextIfIndex;

                if (nmhGetNextIndexIgmpInterfaceTable (i4CurrIfIndex,
                                                       &i4NextIfIndex) ==
                    SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }

        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        CfaGetInterfaceNameFromIndex (i4NextIfIndex, au1InterfaceName);
        i4RetValue =
            IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4NextIfIndex, &u4Port);
        i4RetValue = IGMP_IP_GET_IF_CFG_RECORD (u4Port, &IpIfRecord);

        if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
            (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
        {
            u1UpStream = IGMP_TRUE;
        }
        else
        {
            nmhGetIgmpInterfaceQueryInterval (i4NextIfIndex,
                                              &u4IfaceQryIntrval);
            nmhGetIgmpInterfaceVersion (i4NextIfIndex, &u4IfaceVersion);
            nmhGetIgmpInterfaceQuerier (i4NextIfIndex, &u4IfaceQuerier);
            nmhGetIgmpInterfaceQueryMaxResponseTime (i4NextIfIndex,
                                                     &u4IfaceQryMaxRespTime);
            nmhGetIgmpInterfaceRobustness (i4NextIfIndex, &u4RobustnessValue);
            nmhGetIgmpInterfaceLastMembQueryIntvl (i4NextIfIndex,
                                                   &u4IfaceLastMbrQryIntrval);
            nmhGetFsIgmpInterfaceFastLeaveStatus (i4NextIfIndex,
                                                  &i4IfaceFastLeaveStatus);
            nmhGetFsIgmpInterfaceChannelTrackStatus (i4NextIfIndex,
                                                     &i4IfaceChannelTrackStatus);
            nmhGetIgmpInterfaceGroups (i4NextIfIndex, &u4IfaceGroups);
        }

        nmhGetFsIgmpInterfaceOperStatus (i4NextIfIndex, &i4IfaceOperStatus);
        nmhGetFsIgmpInterfaceAdminStatus (i4NextIfIndex, &i4IfaceAdminStatus);
        nmhGetFsIgmpInterfaceGroupListId (i4NextIfIndex, &u4GrpListId);
        nmhGetFsIgmpInterfaceLimit (i4NextIfIndex, &u4IntfGrpLimit);
        nmhGetFsIgmpInterfaceCurGrpCount (i4NextIfIndex, &u4GrpCurCnt);
        nmhGetFsIgmpInterfaceCKSumError (i4NextIfIndex, &u4CKSumError);
        nmhGetFsIgmpInterfacePktLenError (i4NextIfIndex, &u4PktLenError);
        nmhGetFsIgmpInterfacePktsWithLocalIP (i4NextIfIndex, &u4PktWithLocalIP);
        nmhGetFsIgmpInterfaceSubnetCheckFailure (i4NextIfIndex,
                                                 &u4SubnetCheckFailure);
        nmhGetFsIgmpInterfaceQryFromNonQuerier (i4NextIfIndex,
                                                &u4QryFromNonQuerier);
        nmhGetFsIgmpInterfaceReportVersionMisMatch (i4NextIfIndex,
                                                    &u4ReportVersionMisMatch);
        nmhGetFsIgmpInterfaceQryVersionMisMatch (i4NextIfIndex,
                                                 &u4QryVersionMisMatch);
        nmhGetFsIgmpInterfaceUnknownMsgType (i4NextIfIndex, &u4UnknownMsgType);
        nmhGetFsIgmpInterfaceInvalidV1Report (i4NextIfIndex,
                                              &u4InvalidV1Report);
        nmhGetFsIgmpInterfaceInvalidV2Report (i4NextIfIndex,
                                              &u4InvalidV2Report);
        nmhGetFsIgmpInterfaceInvalidV3Report (i4NextIfIndex,
                                              &u4InvalidV3Report);
        nmhGetFsIgmpInterfaceRouterAlertCheckFailure (i4NextIfIndex,
                                                      &u4RouterAlertCheckFailure);
        nmhGetFsIgmpInterfaceIncomingSSMPkts (i4NextIfIndex, &u4IgmpInSSMPkts);
        nmhGetFsIgmpInterfaceInvalidSSMPkts (i4NextIfIndex,
                                             &u4IgmpInvalidSSMPkts);
        nmhGetFsIgmpInterfaceJoinPktRate (i4NextIfIndex, &i4RateLimit);

        CliPrintf (CliHandle, "%s,", au1InterfaceName);
        if (IpIfRecord.u4Oper == IGMP_IFACE_OPER_UP)
        {
            CliPrintf (CliHandle, " line protocol is up\r\n");
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "line protocol is up\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " line protocol is down\r\n");
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "line protocol is Down\r\n");
        }

        CLI_CONVERT_IPADDR_TO_STR (pu1String, IpIfRecord.u4Addr);
        CliPrintf (CliHandle, " Internet Address is %s/", pu1String);

        u4MaskBits = CliGetMaskBits (IpIfRecord.u4NetMask);
        CliPrintf (CliHandle, "%d\r\n", u4MaskBits);

        if ((i4IfaceOperStatus == IGMP_IFACE_OPER_UP) &&
            (i4IfaceAdminStatus == IGMP_IFACE_ADMIN_UP))
        {
            CliPrintf (CliHandle, " IGMP is enabled on interface\r\n");
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is enabled on interface %d\r\n", i4NextIfIndex);
        }
        else
        {
            CliPrintf (CliHandle, " IGMP is disabled on interface\r\n");
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is disabled on interface %d\r\n", i4NextIfIndex);
        }

        if (u1UpStream == IGMP_TRUE)
        {
            CliPrintf (CliHandle,
                       " Interface is configured as Upstream interface\r\n");
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface %d is configured as Upstream interface \r\n",
                       i4NextIfIndex);
        }
        else
        {
            CliPrintf (CliHandle,
                       " Current IGMP router version is %d\r\n",
                       u4IfaceVersion);
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Current IGMP router version is %d\r\n", u4IfaceVersion);
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface %d is configured as Upstream interface \r\n",
                       i4NextIfIndex);

            CliPrintf (CliHandle,
                       " IGMP rate limit for joins packets is %d\r\n",
                       i4RateLimit);
            if (i4IfaceChannelTrackStatus == IGMP_CHANNEL_TRACK_ENABLE)
            {
                CliPrintf (CliHandle, " Explicit Tracking is enabled\r\n");
            }
            if (u4IntfGrpLimit != IGMP_ZERO)
            {
                CliPrintf (CliHandle,
                           " IGMP Interface State Limit %d out of max %d \r\n",
                           u4GrpCurCnt, u4IntfGrpLimit);
                if (u4GrpListId != IGMP_ZERO)
                {
                    CliPrintf (CliHandle,
                               " IGMP Exempt GroupList Id %d \r\n",
                               u4GrpListId);
                }
            }

            CliPrintf (CliHandle,
                       " IGMP query interval is %d seconds\r\n",
                       u4IfaceQryIntrval);

            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_QRY_MODULE,
                       IgmpGetModuleName (IGMP_QRY_MODULE),
                       "IGMP query interval is %d seconds\r\n",
                       u4IfaceQryIntrval);

            CliPrintf (CliHandle,
                       " Last member query response interval is %d seconds\r\n",
                       u4IfaceLastMbrQryIntrval);
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_QRY_MODULE,
                       IgmpGetModuleName (IGMP_QRY_MODULE),
                       "Last member query response interval is %d seconds\r\n",
                       u4IfaceLastMbrQryIntrval);

            CliPrintf (CliHandle,
                       " IGMP max query response time is %d seconds\r\n",
                       u4IfaceQryMaxRespTime);
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_QRY_MODULE,
                       IgmpGetModuleName (IGMP_QRY_MODULE),
                       "IGMP max query response time is %d seconds\r\n",
                       u4IfaceQryMaxRespTime);

            CliPrintf (CliHandle,
                       " Robustness value is %d\r\n", u4RobustnessValue);

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IfaceQuerier);
            CliPrintf (CliHandle, " IGMP querying router is %s", pu1String);
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP querying router is %s\r\n", pu1String);

            if (IpIfRecord.u4Addr == u4IfaceQuerier)
            {
                CliPrintf (CliHandle, " (this system)\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

            if (i4IfaceFastLeaveStatus == IGMP_FAST_LEAVE_ENABLE)
            {
                CliPrintf (CliHandle,
                           " Fast leave is enabled on this interface\r\n");
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "Fast leave is enabled on this interface %d\r\n",
                           i4NextIfIndex);
            }
            else
            {
                CliPrintf (CliHandle,
                           " Fast leave is disabled on this interface\r\n");
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "Fast leave is disabled on this interface %d\r\n",
                           i4NextIfIndex);
            }
            if (u4IfaceGroups)
            {
                CliPrintf (CliHandle,
                           " Number of multicast groups joined %d\r\n",
                           u4IfaceGroups);
            }
            else
            {
                CliPrintf (CliHandle, " No multicast groups joined\r\n");
            }
            CliPrintf (CliHandle, " Checksum errors: %d,", u4CKSumError);
            CliPrintf (CliHandle, " Packet length errors: %d\r\n",
                       u4PktLenError);
            CliPrintf (CliHandle, " Packets with Local IP as source: %d,",
                       u4PktWithLocalIP);
            CliPrintf (CliHandle, " Source subnet check failures: %d\r\n",
                       u4SubnetCheckFailure);
            CliPrintf (CliHandle, " Query from non-querier: %d\r\n",
                       u4QryFromNonQuerier);
            CliPrintf (CliHandle, " Report version mismatch: %d,",
                       u4ReportVersionMisMatch);
            CliPrintf (CliHandle, " Query version mismatch:  %d\r\n",
                       u4QryVersionMisMatch);
            CliPrintf (CliHandle, " Unknown IGMP message type: %d\r\n",
                       u4UnknownMsgType);
            CliPrintf (CliHandle, " Invalid V1 reports: %d,",
                       u4InvalidV1Report);
            CliPrintf (CliHandle, " Invalid V2 reports: %d,",
                       u4InvalidV2Report);
            CliPrintf (CliHandle, " Invalid v3 reports: %d\r\n",
                       u4InvalidV3Report);
            CliPrintf (CliHandle,
                       " Packets dropped due to router-alert check: %d\r\n",
                       u4RouterAlertCheckFailure);
            CliPrintf (CliHandle, " Reports in SSM range: %d\r\n",
                       u4IgmpInSSMPkts);
            CliPrintf (CliHandle, " Invalid Reports in SSM range: %d\r\n",
                       u4IgmpInvalidSSMPkts);
        }

        u1UpStream = IGMP_FALSE;

        i4CurrIfIndex = i4NextIfIndex;
        if (nmhGetNextIndexIgmpInterfaceTable (i4CurrIfIndex,
                                               &i4NextIfIndex) == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowInterfaceInfo()\r \n");
    UNUSED_PARAM (i4RetValue);
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowGroupInfo                               */
/*                                                                         */
/*     Description   :  Displays IGMP group information                    */
/*                                                                         */
/*     INPUT         : None                                                */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowGroupInfo (tCliHandle CliHandle)
{
    UINT4               u4CurrentSysTime = 0;
    UINT4               u4UpTime = 0;
    CHR1                TimeFormat[17] = "";
    CHR1                UpTime[17] = "";
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpTimer         *pTimer = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT4               u4RemainingTime = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4IgmpIfIndex = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1AdminStatus = 0;
    CHR1               *pu1String = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4IgmpInterfaceAdminStatus = 0;
    INT4                i4Flag = 0;
    UINT4               u4IpGrp = 0;
    UINT4               u4IpRep = 0;
    INT4                i4Status;
    UINT1               u1FilterMode = IGMP_FMODE_INCLUDE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowGroupInfo()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4Status);

    if (i4Status == IGMP_ENABLE)
    {
        CliPrintf (CliHandle, "IGMP is globally enabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP is globally disabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally disabled\r\n");
        return CLI_SUCCESS;
    }
    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    if (pIfGetNextLink == NULL)
    {
        return CLI_SUCCESS;
    }
    do
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            continue;
        }
        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        i4IgmpIfIndex = (INT4) pIfaceNode->u4IfIndex;

        IGMP_IP_GET_IFINDEX_FROM_PORT (i4IgmpIfIndex, &u4IfIndex);
        if (CfaGetInterfaceNameFromIndex (u4IfIndex, au1InterfaceName) ==
            OSIX_FAILURE)
        {
            continue;
        }
        if (nmhGetFsIgmpInterfaceAdminStatus (u4IfIndex,
                                              &i4IgmpInterfaceAdminStatus)
            == SNMP_FAILURE)
        {
            continue;
        }
        CfaApiGetIfAdminStatus (u4IfIndex, &u1AdminStatus);
        if (u1AdminStatus == CFA_IF_DOWN)
        {
            continue;
        }

        /*Check whether IGMP is enabled on the VLAN */
        if (i4IgmpInterfaceAdminStatus != IGMP_IFACE_ADMIN_DOWN)
        {
            MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

            IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
            IgmpGrpEntry.pIfNode = pIfaceNode;

            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) & IgmpGrpEntry,
                                                 NULL);
            while (pGrp != NULL)
            {
                u4RemainingTime = 0;
                IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &(pGrp->u4Group));

                if ((pGrp->u4IfIndex != pIfaceNode->u4IfIndex) ||
                    (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    /* Group entry doesn't belong to 
                     * specified interface index */
                    break;
                }

                if (i4Flag == 0)
                {
                    CliPrintf (CliHandle,
                               "\nI - Include Mode,  E - Exclude Mode\r\n"
                               "S - Static Mbr,    D - Dynamic Mbr\r\n");

                    CliPrintf (CliHandle,
                               "\r\n%-16s%-4s%-13s%-17s%-17s%-16s\r\n",
                               "GroupAddress", "Flg", "Iface", "UpTime",
                               "ExpiryTime", "LastReporter");

                    CliPrintf (CliHandle, "%-16s%-4s%-13s%-17s%-17s%-16s\r\n",
                               "---------------", "---", "------------",
                               "----------------", "----------------",
                               "---------------");
                    i4Flag = 1;
                }
                IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, pGrp->u4Group,
                                        IPVX_ADDR_FMLY_IPV4);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpGrp)
                    CliPrintf (CliHandle, "%-16s", pu1String);

                pTimer = &(pGrp->Timer);
                if (pIfaceNode->u1Version == IGMP_VERSION_3)
                {
                    if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
                        (IgmpUtilChkIfSSMMappedGroup (pGrp->u4Group, &i4Status)
                         != NULL))
                    {
                        u1FilterMode = IGMP_FMODE_INCLUDE;
                    }
                    else
                    {
                        u1FilterMode = pGrp->u1FilterMode;
                    }
                    if (u1FilterMode == IGMP_FMODE_INCLUDE)
                    {
                        if (pGrp->u1StaticFlg == IGMP_TRUE)
                        {
                            if (pGrp->u1DynamicFlg == IGMP_TRUE)
                            {
                                CliPrintf (CliHandle, "%-4s", "ISD");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-4s", "IS");
                            }
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-4s", "ID");
                        }
                    }
                    else if (u1FilterMode == IGMP_FMODE_EXCLUDE)
                    {
                        if (pGrp->u1StaticFlg == IGMP_TRUE)
                        {
                            if (pGrp->u1DynamicFlg == IGMP_TRUE)
                            {
                                CliPrintf (CliHandle, "%-4s", "ESD");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-4s", "ES");
                            }
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-4s", "ED");
                        }
                    }
                }
                else
                {
                    if ((pGrp->u1StaticFlg == IGMP_TRUE) &&
                        (pGrp->u1DynamicFlg == IGMP_TRUE))
                    {
                        CliPrintf (CliHandle, "%-4s", "SD");
                    }

                    if ((pGrp->u1StaticFlg == IGMP_TRUE) &&
                        (pGrp->u1DynamicFlg == IGMP_FALSE))
                    {
                        CliPrintf (CliHandle, "%-4s", "S");
                    }

                    if ((pGrp->u1StaticFlg == IGMP_FALSE) &&
                        (pGrp->u1DynamicFlg == IGMP_TRUE))
                    {
                        CliPrintf (CliHandle, "%-4s", "D");
                    }
                }

                CliPrintf (CliHandle, "%-13s", au1InterfaceName);

                OsixGetSysTime (&u4CurrentSysTime);

                u4UpTime = u4CurrentSysTime - pGrp->u4CacheUpTime;
                IgmpDispHhMmSs (u4UpTime, UpTime);
                CliPrintf (CliHandle, "%-17s", UpTime);

                IgmpDispHhMmSs (u4RemainingTime, TimeFormat);
                if (pGrp->Timer.u1TmrStatus == IGMP_TMR_SET)
                {
                    if (TmrGetRemainingTime (gIgmpTimerListId,
                                             &(pGrp->Timer.TimerBlk.TimerNode),
                                             &u4RemainingTime) != TMR_SUCCESS)
                    {
                        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,
                                       IGMP_NAME,
                                       "Tmr Get remaining time failure ! \n");
                    }

                    IgmpDispHhMmSs (u4RemainingTime, TimeFormat);
                }

                CliPrintf (CliHandle, "%-17s", TimeFormat);

                IGMP_IP_COPY_FROM_IPVX (&u4IpRep, pGrp->u4LastReporter,
                                        IPVX_ADDR_FMLY_IPV4);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpRep)
                    if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
                {
                    CliPrintf (CliHandle, "-\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "%-16s\r\n", pu1String);
                }
                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);
            }
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while ((pIfGetNextLink = (TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                            &(pIfaceNode->IfGetNextLink)))) !=
           NULL);
    UNUSED_PARAM (pTimer);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowGroupInfo()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowMembership                              */
/*                                                                         */
/*     Description   :  Displays IGMP group information                    */
/*                                                                         */
/*     INPUT         : None                                                */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowMembership (tCliHandle CliHandle, INT4 i4DispStatus)
{
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT4               u4IfIndex = 0;
    INT4                i4IgmpIfIndex = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    CHR1               *pu1String = NULL;
    INT4                i4IgmpInterfaceAdminStatus = 0;
    INT4                i4Flag = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4Status;
    UINT4               u4MaxPrintCount = 0;
    UINT1               u1AddrType = 0;
    UINT1               u1ChannelTrackFlg = 0;
    UINT4               u4NodeIfIndex = 0;
    UINT1               u1Version = 0;
    tIgmpIPvXAddr       u4ZeroAddr;

    MEMSET (&u4ZeroAddr, 0, sizeof (tIgmpIPvXAddr));

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowMembership()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4Status);

    if (i4Status == IGMP_ENABLE)
    {
        CliPrintf (CliHandle, "IGMP is globally enabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP is globally disabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally disabled\r\n");
        return CLI_SUCCESS;
    }
    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    if (pIfGetNextLink == NULL)
    {
        return CLI_SUCCESS;
    }
    do
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);
        u1AddrType = pIfaceNode->u1AddrType;
        u1ChannelTrackFlg = pIfaceNode->u1ChannelTrackFlg;
        u4NodeIfIndex = pIfaceNode->u4IfIndex;
        u1Version = pIfaceNode->u1Version;
        if ((u1AddrType == IPVX_ADDR_FMLY_IPV6) ||
            ((i4DispStatus == IGMP_DISPLAY_TRACK) &&
             (u1ChannelTrackFlg == IGMP_CHANNEL_TRACK_DISABLE)))

        {
            continue;
        }
        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        i4IgmpIfIndex = (INT4) u4NodeIfIndex;

        IGMP_IP_GET_IFINDEX_FROM_PORT (i4IgmpIfIndex, &u4IfIndex);
        if (CfaGetInterfaceNameFromIndex (u4IfIndex, au1InterfaceName) ==
            OSIX_FAILURE)
        {
            continue;
        }
        if (nmhGetFsIgmpInterfaceAdminStatus (u4IfIndex,
                                              &i4IgmpInterfaceAdminStatus)
            == SNMP_FAILURE)
        {
            continue;
        }
        /*Check whether IGMP is enabled on the VLAN */
        if (i4IgmpInterfaceAdminStatus != IGMP_IFACE_ADMIN_DOWN)
        {
            MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

            IgmpGrpEntry.u4IfIndex = u4NodeIfIndex;
            IgmpGrpEntry.pIfNode = pIfaceNode;

            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) & IgmpGrpEntry,
                                                 NULL);
            while (pGrp != NULL)
            {
                MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

                IgmpGrpEntry.u4IfIndex = u4NodeIfIndex;
                IgmpGrpEntry.pIfNode = pIfaceNode;
                IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &(pGrp->u4Group));

                if ((pGrp->u4IfIndex != u4NodeIfIndex) ||
                    (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    /* Group entry doesn't belong to 
                     * specified interface index */
                    break;
                }

                if (i4Flag == 0)
                {
                    CliPrintf (CliHandle,
                               "\nFlags: A - Aggregate T - tracked\r\n"
                               "S - Static , R - Reported through v3\r\n"
                               "1,2,3 - The version of IGMP\r\n");

                    CliPrintf (CliHandle,
                               "\nReporter:\r\n"
                               "<mac-or-ip-address> - IP address of the"
                               "host that has sent IGMP report\r\n");

                    CliPrintf (CliHandle,
                               "\r\n%-34s%-6s%-13s%-13s\r\n",
                               "Channel/Group", "Flags",
                               "Interface", "Reporter");

                    CliPrintf (CliHandle, "%-33s%-6s%-13s%-13s\r\n",
                               "--------------------------------",
                               "-----", "------------", "------------");
                    i4Flag = 1;
                }
                IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pGrp->u4Group,
                                        IPVX_ADDR_FMLY_IPV4);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr)
                    CliPrintf (CliHandle, "*,%-33s", pu1String);

                if (u1Version == IGMP_VERSION_3)
                {
                    if (u1ChannelTrackFlg == IGMP_CHANNEL_TRACK_ENABLE)
                    {
                        if (pGrp->u1StaticFlg == IGMP_TRUE)
                        {
                            CliPrintf (CliHandle, "%-6s", "3ATS");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-6s", "3ATR");
                        }
                    }
                    else
                    {
                        if (pGrp->u1StaticFlg == IGMP_TRUE)
                        {
                            CliPrintf (CliHandle, "%-6s", "3AS");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-6s", "3AR");
                        }
                    }
                }
                else if (u1Version == IGMP_VERSION_2)
                {
                    if (pGrp->u1StaticFlg == IGMP_TRUE)
                    {
                        CliPrintf (CliHandle, "%-6s", "2AS");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-6s", "2A");
                    }
                }
                else
                {
                    if (pGrp->u1StaticFlg == IGMP_TRUE)
                    {
                        CliPrintf (CliHandle, "%-6s", "1AS");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-6s", "1A");
                    }

                }

                CliPrintf (CliHandle, "%-10s", au1InterfaceName);

                IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pGrp->u4LastReporter,
                                        IPVX_ADDR_FMLY_IPV4);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr)
                    if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
                {
                    CliPrintf (CliHandle, "-\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "%-13s\r\n", pu1String);
                }

                /* Display the tracked groups for group entry pGrp */
                IgmpCliShowTrackedGroups (CliHandle, pGrp, i4DispStatus);

                u4MaxPrintCount += IGMP_SIX;
                if (u4MaxPrintCount > IGMP_MAX_PRINT_LEN)
                {
                    CliFlush (CliHandle);
                    if (IgmpCliValidatePointers
                        (u4NodeIfIndex, u4ZeroAddr, u4ZeroAddr) == IGMP_FAILURE)
                    {
                        break;
                    }
                    u4MaxPrintCount = 0;
                }

                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) & IgmpGrpEntry,
                                                     NULL);
            }
        }
        CliFlush (CliHandle);
        if (IgmpCliValidatePointers (u4NodeIfIndex, u4ZeroAddr, u4ZeroAddr) ==
            IGMP_FAILURE)
        {
            break;
        }
    }
    while ((pIfGetNextLink = (TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                            &(pIfaceNode->IfGetNextLink)))) !=
           NULL);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpCliShowMembership()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowGroupChannel                            */
/*                                                                         */
/*     Description   :  Displays IGMP group channel information            */
/*                                                                         */
/*     INPUT         : GrpAddr, Display status                             */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowGroupChannel (tCliHandle CliHandle,
                         UINT4 u4GrpAddr, INT4 i4DispStatus)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpIPvXAddr       GrpAddr;
    UINT4               u4IfIndex = IGMP_ZERO;
    UINT4               u4IpAddr = 0;
    INT4                i4Flag = 0;
    INT4                i4Status;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    CHR1               *pu1String = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowGroupChannel()\r \n");
    IGMP_IPVX_ADDR_CLEAR (&GrpAddr);

    nmhGetFsIgmpGlobalStatus (&i4Status);
    if (i4Status == IGMP_ENABLE)
    {
        CliPrintf (CliHandle, "IGMP is globally enabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP is globally disabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally disabled\r\n");
        return CLI_SUCCESS;
    }

    IGMP_IPVX_ADDR_INIT_IPV4 (GrpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4GrpAddr);
    pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
    while (pGrp != NULL)
    {
        /* Get the next group entry with the same group address */
        pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) pGrp, NULL);

        /* Verify the given group address is retrieved */
        if (IGMP_IPVX_ADDR_COMPARE (pGrp->u4Group, GrpAddr) != IGMP_ZERO)
        {
            /* If the obtained group address is not equal to the given
             * group address continue to get the next grp entry*/
            pGrp = pNextGrp;
            continue;
        }

        if ((pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) ||
            ((i4DispStatus == IGMP_DISPLAY_TRACK) &&
             (pGrp->pIfNode->u1ChannelTrackFlg == IGMP_CHANNEL_TRACK_DISABLE)))
        {
            pGrp = pNextGrp;
            continue;
        }

        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        IGMP_IP_GET_IFINDEX_FROM_PORT ((INT4) pGrp->u4IfIndex, &u4IfIndex);
        CfaGetInterfaceNameFromIndex (u4IfIndex, au1InterfaceName);

        if (i4Flag == 0)
        {
            CliPrintf (CliHandle,
                       "\nFlags: A - Aggregate T - tracked\r\n"
                       "S - Static , R - Reported through v3\r\n"
                       "1,2,3 - The version of IGMP\r\n");

            CliPrintf (CliHandle,
                       "\nReporter:\r\n"
                       "<mac-or-ip-address> - IP address of the"
                       "host that has sent IGMP report\r\n");

            CliPrintf (CliHandle,
                       "\r\n%-34s%-6s%-13s%-13s\r\n",
                       "Channel/Group", "Flags", "Interface", "Reporter");

            CliPrintf (CliHandle, "%-33s%-6s%-13s%-13s\r\n",
                       "---------------------", "-----",
                       "------------", "------------");
            i4Flag = 1;
        }

        /* Displaying the Group address related channel entries */
        IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pGrp->u4Group, IPVX_ADDR_FMLY_IPV4);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr)
            CliPrintf (CliHandle, "*,%-33s", pu1String);

        if (pGrp->pIfNode->u1Version == IGMP_VERSION_3)
        {
            if (pGrp->pIfNode->u1ChannelTrackFlg == IGMP_CHANNEL_TRACK_ENABLE)
            {
                if (pGrp->u1StaticFlg == IGMP_TRUE)
                {
                    CliPrintf (CliHandle, "%-6s", "3ATS");
                }
                else
                {
                    CliPrintf (CliHandle, "%-6s", "3ATR");
                }
            }
            else
            {
                if (pGrp->u1StaticFlg == IGMP_TRUE)
                {
                    CliPrintf (CliHandle, "%-6s", "3AS");
                }
                else
                {
                    CliPrintf (CliHandle, "%-6s", "3AR");
                }
            }
        }
        else
        {
            if (pGrp->u1StaticFlg == IGMP_TRUE)
            {
                CliPrintf (CliHandle, "%-6s", "2AS");
            }
            else
            {
                CliPrintf (CliHandle, "%-6s", "2A");
            }
        }
        CliPrintf (CliHandle, "%-13s", au1InterfaceName);

        if (pGrp->pIfNode->u1Version == IGMP_VERSION_3)
        {
            CliPrintf (CliHandle, "   \r\n");
        }
        else
        {
            IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pGrp->u4LastReporter,
                                    IPVX_ADDR_FMLY_IPV4);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr)
                if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
            {
                CliPrintf (CliHandle, "-\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "%-13s\r\n", pu1String);
            }
        }

        /* Display the tracked groups for group entry pGrp */
        IgmpCliShowTrackedGroups (CliHandle, pGrp, i4DispStatus);

        pGrp = pNextGrp;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowGroupChannel()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowTrackedGroups                           */
/*                                                                         */
/*     Description   :  Displays IGMP group channel information            */
/*                                                                         */
/*     INPUT         : GrpEntry, Display status                            */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowTrackedGroups (tCliHandle CliHandle,
                          tIgmpGroup * pGrp, INT4 i4DispStatus)
{
    tIgmpSource        *pSource = NULL;
    tIgmpSource        *pTempSource = NULL;
    tIgmpSrcReporter   *pReporter = NULL;
    UINT4               u4IpAddr = 0;
    UINT4               u4IfIndex = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    CHR1               *pu1String = NULL;
    UINT4               u4MaxPrintCount = 0;
    UINT1               u1Flush = IGMP_TRUE;
    UINT4               u4IfBkp = 0;
    tIgmpIPvXAddr       u4GrpBkp;
    tIgmpIPvXAddr       u4SrcBkp;
    tIgmpIPvXAddr       u4TempSrcBkp;
    tIgmpSrcReporter    FindReporter;

    MEMSET (&u4GrpBkp, 0, sizeof (tIgmpIPvXAddr));
    MEMSET (&u4SrcBkp, 0, sizeof (tIgmpIPvXAddr));
    MEMSET (&u4TempSrcBkp, 0, sizeof (tIgmpIPvXAddr));
    u4IfBkp = pGrp->pIfNode->u4IfIndex;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowTrackedGroups()\r \n");
    /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
    IGMP_IP_GET_IFINDEX_FROM_PORT ((INT4) pGrp->u4IfIndex, &u4IfIndex);
    CfaGetInterfaceNameFromIndex (u4IfIndex, au1InterfaceName);

    IGMP_IPVX_ADDR_COPY (&(u4GrpBkp), &(pGrp->u4Group));

    /* Displaying all the channel entries */
    if ((i4DispStatus == IGMP_DISPLAY_ALL) ||
        (i4DispStatus == IGMP_DISPLAY_TRACK))
    {
        TMO_DYN_SLL_Scan (&(pGrp->srcList), pSource, pTempSource, tIgmpSource *)
        {
            if (IGMP_IPVX_ADDR_COMPARE (pSource->u4SrcAddress,
                                        gIPvXZero) == IGMP_ZERO)
            {
                continue;
            }

            IGMP_IPVX_ADDR_COPY (&(u4SrcBkp), &(pSource->u4SrcAddress));
            IGMP_IPVX_ADDR_COPY (&(u4TempSrcBkp), &(pSource->u4SrcAddress));
            if (i4DispStatus == IGMP_DISPLAY_ALL)
            {
                /* (S,G) - with aggregated reporters to be displayed
                 * only if DISPLAY_ALL is requested */
                IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pSource->u4SrcAddress,
                                        IPVX_ADDR_FMLY_IPV4);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr)
                    CliPrintf (CliHandle, "%-15s,", pu1String);
                IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pGrp->u4Group,
                                        IPVX_ADDR_FMLY_IPV4);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr)
                    CliPrintf (CliHandle, "%-19s", pu1String);
                if (IGMP_IS_CHANNEL_TRACK_ENABLED (pGrp->pIfNode))
                {
                    if (pGrp->u1StaticFlg == IGMP_TRUE)
                    {
                        CliPrintf (CliHandle, "%-6s", "ATS");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-6s", "ATR");
                    }
                }
                else
                {
                    if (pGrp->u1StaticFlg == IGMP_TRUE)
                    {
                        CliPrintf (CliHandle, "%-6s", "AS");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-6s", "AR");
                    }
                }

                CliPrintf (CliHandle, "%-10s", au1InterfaceName);
                if ((pGrp->u1StaticFlg == IGMP_FALSE) &&
                    (IGMP_IS_CHANNEL_TRACK_ENABLED (pGrp->pIfNode)))
                {
                    CliPrintf (CliHandle, "   \r\n");
                }
                else
                {
                    IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pSource->u4LastReporter,
                                            IPVX_ADDR_FMLY_IPV4);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr)
                        if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
                    {
                        CliPrintf (CliHandle, "-\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-13s\r\n", pu1String);
                    }
                }
                u4MaxPrintCount += IGMP_THREE;
            }
            if (pGrp->pIfNode->u1ChannelTrackFlg == IGMP_CHANNEL_TRACK_ENABLE)
            {
                /* (S,G) with tracked reporters to be displayed 
                 * only if tracking is enabled */
                pReporter =
                    (tIgmpSrcReporter *) RBTreeGetFirst (pSource->
                                                         SrcReporterTree);
                while (pReporter != NULL)
                {
                    IGMP_IP_COPY_FROM_IPVX (&u4IpAddr,
                                            pSource->u4SrcAddress,
                                            IPVX_ADDR_FMLY_IPV4);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                               u4IpAddr) CliPrintf (CliHandle,
                                                                    "%-11s,",
                                                                    pu1String);
                    IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, pGrp->u4Group,
                                            IPVX_ADDR_FMLY_IPV4);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                               u4IpAddr) CliPrintf (CliHandle,
                                                                    "%-11s",
                                                                    pu1String);
                    CliPrintf (CliHandle, "%-6s", "T");
                    CliPrintf (CliHandle, "%-10s", au1InterfaceName);
                    IGMP_IP_COPY_FROM_IPVX (&u4IpAddr,
                                            pReporter->u4SrcRepAddr,
                                            IPVX_ADDR_FMLY_IPV4);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                               u4IpAddr) CliPrintf (CliHandle,
                                                                    "%-13s\r\n",
                                                                    pu1String);
                    u4MaxPrintCount += IGMP_TWO;
                    if (u4MaxPrintCount > IGMP_MAX_PRINT_LEN)
                    {
                        if (u1Flush == IGMP_TRUE)
                        {
                            CliFlush (CliHandle);
                            u1Flush = IGMP_FALSE;
                        }
                        if (IgmpCliValidatePointers
                            (u4IfBkp, u4GrpBkp, u4SrcBkp) == IGMP_FAILURE)
                        {
                            break;
                        }
                        u4MaxPrintCount = 0;
                        u1Flush = IGMP_TRUE;
                    }
                    MEMSET (&FindReporter, 0, sizeof (tIgmpSrcReporter));
                    IGMP_IPVX_ADDR_COPY (&(FindReporter.u4SrcRepAddr),
                                         &(pReporter->u4SrcRepAddr));

                    pReporter =
                        RBTreeGetNext (pSource->SrcReporterTree,
                                       (tRBElem *) & FindReporter, NULL);
                }
            }
            if (u4MaxPrintCount > IGMP_MAX_PRINT_LEN)
            {
                if (u1Flush == IGMP_TRUE)
                {
                    CliFlush (CliHandle);
                    u1Flush = IGMP_FALSE;
                }
                if (IgmpCliValidatePointers (u4IfBkp, u4GrpBkp, u4TempSrcBkp) ==
                    IGMP_FAILURE)
                {
                    break;
                }
                u4MaxPrintCount = 0;
                u1Flush = IGMP_TRUE;
            }
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowTrackedGroups()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowSourceInfo                              */
/*                                                                         */
/*     Description   :  Displays IGMP source information                   */
/*                                                                         */
/*     INPUT         : None                                                */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowSourceInfo (tCliHandle CliHandle)
{
    CHR1                TimeFormat[17] = "";
    tIgmpIface         *pIfaceNode = NULL;
    tTMO_SLL_NODE      *pSrcLink = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpGroup          IgmpGroupEntry;
    UINT4               u4RemainingTime = IGMP_ZERO;
    INT4                i4IgmpIfIndex = IGMP_ZERO;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1AdminStatus = 0;
    UINT4               u4LastReporter = IGMP_ZERO;
    CHR1               *pu1String = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4IfIndex = 0;
    UINT4               u4Version = 0;
    INT4                i4Flag = 0;
    INT4                i4IgmpInterfaceAdminStatus = 0;
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    INT4                i4Status;
    UINT1               u1FilterMode = IGMP_FMODE_INCLUDE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowSourceInfo()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4Status);

    if (i4Status == IGMP_ENABLE)
    {
        CliPrintf (CliHandle, "IGMP is globally enabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP is globally disabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally disabled\r\n");
        return CLI_SUCCESS;
    }
    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);
    if (pIfGetNextLink == NULL)
    {
        return CLI_SUCCESS;
    }
    do
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            continue;
        }
        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        i4IgmpIfIndex = (INT4) pIfaceNode->u4IfIndex;
        IGMP_IP_GET_IFINDEX_FROM_PORT (i4IgmpIfIndex, &u4IfIndex);
        if ((CfaGetInterfaceNameFromIndex (u4IfIndex,
                                           au1InterfaceName) == OSIX_FAILURE))
        {
            continue;
        }
        if (nmhGetFsIgmpInterfaceAdminStatus (u4IfIndex,
                                              &i4IgmpInterfaceAdminStatus)
            == SNMP_FAILURE)
        {
            continue;
        }
        CfaApiGetIfAdminStatus (u4IfIndex, &u1AdminStatus);
        if (u1AdminStatus == CFA_IF_DOWN)
        {
            continue;
        }

        if (i4IgmpInterfaceAdminStatus != IGMP_IFACE_ADMIN_DOWN)
        {
            MEMSET (&IgmpGroupEntry, 0, sizeof (tIgmpGroup));

            IgmpGroupEntry.u4IfIndex = pIfaceNode->u4IfIndex;
            IgmpGroupEntry.pIfNode = pIfaceNode;

            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) & IgmpGroupEntry,
                                                 NULL);
            nmhGetIgmpInterfaceVersion (u4IfIndex, &u4Version);
            while (pGrp != NULL)
            {
                if ((u4Version == IGMP_VERSION_2)
                    && (TMO_SLL_Count (&pGrp->srcList) == IGMP_ZERO))
                {
                    pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                         (tRBElem *) pGrp,
                                                         NULL);
                    continue;
                }

                IGMP_IPVX_ADDR_COPY (&(IgmpGroupEntry.u4Group),
                                     &(pGrp->u4Group));

                if ((pGrp->u4IfIndex != pIfaceNode->u4IfIndex) ||
                    (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    /* Group entry doesn't belong to specified interface index */
                    break;
                }
                if (i4Flag == 0)
                {
                    CliPrintf (CliHandle,
                               "\nI - Include Mode,  E - Exclude Mode\r\n"
                               "S - Static Mbr, D - Dynamic Mbr\n"
                               "F - Forward List, N - Non-Forward List\n");
                    CliPrintf (CliHandle,
                               "\r\n%-16s%-13s%-16s%-5s%-17s%-16s\r\n",
                               "GroupAddress", "Iface", "SrcAddress", "Flg",
                               "ExpiryTime", "LastReporter");

                    CliPrintf (CliHandle, "%-16s%-13s%-16s%-5s%-17s%-16s\r\n",
                               "---------------", "------------",
                               "---------------", "----", "----------------",
                               "---------------");
                    i4Flag = 1;

                }
                TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
                {
                    pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);
                    if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                                gIPvXZero) == IGMP_ZERO)
                    {
                        continue;
                    }

                    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, pGrp->u4Group,
                                            IPVX_ADDR_FMLY_IPV4);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpGrp)
                        CliPrintf (CliHandle, "%-16s", pu1String);

                    CliPrintf (CliHandle, "%-13s", au1InterfaceName);

                    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, pSrc->u4SrcAddress,
                                            IPVX_ADDR_FMLY_IPV4);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpSrc)
                        CliPrintf (CliHandle, "%-16s", pu1String);
                    if (u4Version == IGMP_VERSION_3)
                    {
                        if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
                            (IgmpUtilChkIfSSMMappedGroup
                             (pGrp->u4Group, &i4Status) != NULL))
                        {
                            u1FilterMode = IGMP_FMODE_INCLUDE;
                        }
                        else
                        {
                            u1FilterMode = pGrp->u1FilterMode;
                        }
                        if (u1FilterMode == IGMP_FMODE_INCLUDE)
                        {
                            if (pSrc->u1StaticFlg == IGMP_TRUE)
                            {
                                if (pSrc->u1DynamicFlg == IGMP_TRUE)
                                {
                                    if (pSrc->u1FwdState == IGMP_TRUE)
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ISDF");
                                    }
                                    else
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ISDN");
                                    }
                                }
                                else
                                {
                                    if (pSrc->u1FwdState == IGMP_TRUE)
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ISF");
                                    }
                                    else
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ISN");
                                    }
                                }
                            }
                            else
                            {
                                if (pSrc->u1FwdState == IGMP_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "IDF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "IDN");
                                }
                            }
                        }
                        else if (u1FilterMode == IGMP_FMODE_EXCLUDE)
                        {
                            if (pSrc->u1StaticFlg == IGMP_TRUE)
                            {
                                if (pSrc->u1DynamicFlg == IGMP_TRUE)
                                {
                                    if (pSrc->u1FwdState == IGMP_TRUE)
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ESDF");
                                    }
                                    else
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ESDN");
                                    }
                                }
                                else
                                {
                                    if (pSrc->u1FwdState == IGMP_TRUE)
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ESF");
                                    }
                                    else
                                    {
                                        CliPrintf (CliHandle, "%-5s", "ESN");
                                    }
                                }
                            }
                            else
                            {
                                if (pSrc->u1FwdState == IGMP_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "EDF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "EDN");
                                }
                            }
                        }
                    }
                    if ((u4Version == IGMP_VERSION_2)
                        || (u4Version == IGMP_VERSION_1))
                    {
                        if (pSrc->u1StaticFlg == IGMP_TRUE)
                        {
                            if (pSrc->u1DynamicFlg == IGMP_TRUE)
                            {
                                if (pSrc->u1FwdState == IGMP_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "SDF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "SDN");
                                }
                            }
                            else
                            {
                                if (pSrc->u1FwdState == IGMP_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "SF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "SN");
                                }
                            }
                        }
                        else
                        {
                            if (pSrc->u1FwdState == IGMP_TRUE)
                            {
                                CliPrintf (CliHandle, "%-5s", "DF");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-5s", "DN");
                            }
                        }
                    }
                    IgmpDispHhMmSs (u4RemainingTime, TimeFormat);
                    if (pSrc->srcTimer.u1TmrStatus == IGMP_TMR_SET)
                    {
                        if (TmrGetRemainingTime (gIgmpTimerListId,
                                                 &(pSrc->srcTimer.TimerBlk.
                                                   TimerNode),
                                                 &u4RemainingTime) !=
                            TMR_SUCCESS)
                        {
                            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,
                                           IGMP_NAME,
                                           "Tmr Get remaining time failure ! \n");
                        }

                        IgmpDispHhMmSs (u4RemainingTime, TimeFormat);
                    }
                    else
                    {
                        u4RemainingTime = IGMP_ZERO;
                        IgmpDispHhMmSs (u4RemainingTime, TimeFormat);
                    }
                    CliPrintf (CliHandle, "%-17s", TimeFormat);

                    IGMP_IP_COPY_FROM_IPVX (&u4LastReporter,
                                            pSrc->u4LastReporter,
                                            IPVX_ADDR_FMLY_IPV4);

                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4LastReporter)
                        if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
                    {
                        CliPrintf (CliHandle, "-\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-16s\r\n", pu1String);
                    }
                    u4LastReporter = 0;
                }                /* End of Srclist scan */
                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);
            }
        }
        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while ((pIfGetNextLink = (TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                            &(pIfaceNode->IfGetNextLink))))
           != NULL);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowSourceInfo()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowStatistics                              */
/*                                                                         */
/*     Description   :  Displays IGMP statistics information               */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface Index                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliShowStatistics (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4RxGenQueries = 0;
    UINT4               u4RxGrpQueries = 0;
    UINT4               u4RxGrpSrcQueries = 0;
    UINT4               u4Rxv1v2Reports = 0;
    UINT4               u4Rxv3Reports = 0;
    UINT4               u4RxLeaves = 0;
    UINT4               u4TxGenQueries = 0;
    UINT4               u4TxGrpQueries = 0;
    UINT4               u4TxGrpSrcQueries = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Txv1v2Reports = 0;
    UINT4               u4Txv3Reports = 0;
    UINT4               u4TxLeaves = 0;
    UINT4               u4IgmpInSSMPkts = 0;
    UINT4               u4IgmpInvalidSSMPkts = 0;
    UINT1               u1IsShowAll = TRUE;
    INT4                i4IgmpInterfaceAdminStatus = 0;
    INT4                i4Status;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowStatistics()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4Status);

    if (i4Status == IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "IGMP is globally disabled\r\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP is globally disabled\r\n");
    }
    if (nmhGetFirstIndexIgmpInterfaceTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (i4IfIndex != IGMP_INVALID_INDEX)
        {
            if (i4IfIndex != i4NextIfIndex)
            {
                i4CurrIfIndex = i4NextIfIndex;

                if (nmhGetNextIndexIgmpInterfaceTable (i4CurrIfIndex,
                                                       &i4NextIfIndex) ==
                    SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }

        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        CfaGetInterfaceNameFromIndex (i4NextIfIndex, au1InterfaceName);

        nmhGetFsIgmpInterfaceAdminStatus (i4NextIfIndex,
                                          &i4IgmpInterfaceAdminStatus);
        /*Check whether IGMP is enabled on the VLAN
         * if enabled print the IGMP statistics
         * else continue */
        if (i4IgmpInterfaceAdminStatus != IGMP_IFACE_ADMIN_DOWN)
        {
            nmhGetFsIgmpInterfaceRxGenQueries (i4NextIfIndex, &u4RxGenQueries);
            nmhGetFsIgmpInterfaceRxGrpQueries (i4NextIfIndex, &u4RxGrpQueries);
            nmhGetFsIgmpInterfaceRxGrpAndSrcQueries (i4NextIfIndex,
                                                     &u4RxGrpSrcQueries);
            nmhGetFsIgmpInterfaceRxv1v2Reports (i4NextIfIndex,
                                                &u4Rxv1v2Reports);
            nmhGetFsIgmpInterfaceRxv3Reports (i4NextIfIndex, &u4Rxv3Reports);
            nmhGetFsIgmpInterfaceTxGenQueries (i4NextIfIndex, &u4TxGenQueries);
            nmhGetFsIgmpInterfaceTxGrpQueries (i4NextIfIndex, &u4TxGrpQueries);
            nmhGetFsIgmpInterfaceTxGrpAndSrcQueries (i4NextIfIndex,
                                                     &u4TxGrpSrcQueries);
            nmhGetFsIgmpInterfaceTxv1v2Reports (i4NextIfIndex,
                                                &u4Txv1v2Reports);
            nmhGetFsIgmpInterfaceTxv3Reports (i4NextIfIndex, &u4Txv3Reports);
            nmhGetFsIgmpInterfaceTxv2Leaves (i4NextIfIndex, &u4TxLeaves);
            nmhGetFsIgmpInterfaceIncomingLeaves (i4NextIfIndex, &u4RxLeaves);
            nmhGetFsIgmpInterfaceIncomingSSMPkts (i4NextIfIndex,
                                                  &u4IgmpInSSMPkts);
            nmhGetFsIgmpInterfaceInvalidSSMPkts (i4NextIfIndex,
                                                 &u4IgmpInvalidSSMPkts);

            CliPrintf (CliHandle, "IGMP Statistics for %s\r\n",
                       au1InterfaceName);

            CliPrintf (CliHandle,
                       "  Number of General queries received %d\r\n",
                       u4RxGenQueries);
            CliPrintf (CliHandle,
                       "  Number of Group Specific queries received %d\r\n",
                       u4RxGrpQueries);
            CliPrintf (CliHandle,
                       "  Number of Group and Source Specific queries received"
                       " %d\r\n", u4RxGrpSrcQueries);
            CliPrintf (CliHandle, "  Number of v1/v2 reports received %d\r\n",
                       u4Rxv1v2Reports);
            CliPrintf (CliHandle, "  Number of v3 reports received %d\r\n",
                       u4Rxv3Reports);
            CliPrintf (CliHandle, "  Number of v2 leaves received %d\r\n",
                       u4RxLeaves);

            CliPrintf (CliHandle,
                       "  Number of General queries transmitted %d\r\n",
                       u4TxGenQueries);
            CliPrintf (CliHandle,
                       "  Number of Group Specific queries transmitted %d\r\n",
                       u4TxGrpQueries);
            CliPrintf (CliHandle,
                       "  Number of Group and Source Specific queries transmitted"
                       " %d\r\n", u4TxGrpSrcQueries);
            CliPrintf (CliHandle, "  Reports in SSM range: %d\r\n",
                       u4IgmpInSSMPkts);
            CliPrintf (CliHandle, "  Invalid Reports in SSM range: %d\r\n",
                       u4IgmpInvalidSSMPkts);

            if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
            {
                CliPrintf (CliHandle,
                           "  Number of v1/v2 reports transmitted %d\r\n",
                           u4Txv1v2Reports);
                CliPrintf (CliHandle,
                           "  Number of v3 reports transmitted %d\r\n",
                           u4Txv3Reports);
                CliPrintf (CliHandle,
                           "  Number of v2 leaves transmitted %d\r\n",
                           u4TxLeaves);
            }
        }
        i4CurrIfIndex = i4NextIfIndex;

        if (nmhGetNextIndexIgmpInterfaceTable (i4CurrIfIndex,
                                               &i4NextIfIndex) == SNMP_FAILURE)
        {
            u1IsShowAll = FALSE;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        if ((u4PagingStatus == CLI_FAILURE) ||
            (i4IfIndex != IGMP_INVALID_INDEX))
        {
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowStatistics()\r \n");
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : IgmpCliClearStats
 *
 *     DESCRIPTION      : Clears IGMP transmit and receive statistics
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
IgmpCliClearStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT1               u1IsShowAll = TRUE;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    UNUSED_PARAM (CliHandle);

    /* Clears IGMP statistics info */

    if (nmhGetFirstIndexIgmpInterfaceTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting MldCliClearStats.\n");
        return CLI_FAILURE;
    }

    do
    {

        if (i4IfIndex != IGMP_INVALID_INDEX)
        {
            if (i4IfIndex != i4NextIfIndex)
            {
                i4CurrIfIndex = i4NextIfIndex;

                if (nmhGetNextIndexIgmpInterfaceTable (i4CurrIfIndex,
                                                       &i4NextIfIndex) ==
                    SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }

        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */

        if (IgmpGetPortFromIfIndex (i4NextIfIndex,
                                    IPVX_ADDR_FMLY_IPV4,
                                    &u4Port) == IGMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            return SNMP_FAILURE;
        }

        pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);
        CfaGetInterfaceNameFromIndex ((UINT4) i4NextIfIndex, au1InterfaceName);
        if (pIfaceNode != NULL)
        {
            pIfaceNode->u4InPkts = IGMP_ZERO;
            pIfaceNode->u4InJoins = IGMP_ZERO;
            pIfaceNode->u4InLeaves = IGMP_ZERO;
            pIfaceNode->u4InQueries = IGMP_ZERO;
            pIfaceNode->u4OutQueries = IGMP_ZERO;
            pIfaceNode->u4WrongVerQueries = IGMP_ZERO;
            pIfaceNode->u4ProcessedJoins = IGMP_ZERO;
            pIfaceNode->u4RxGenQueries = IGMP_ZERO;
            pIfaceNode->u4TxGenQueries = IGMP_ZERO;
            pIfaceNode->u4TxGrpQueries = IGMP_ZERO;
            pIfaceNode->u4TxGrpSrcQueries = IGMP_ZERO;
            pIfaceNode->u4RxGrpQueries = IGMP_ZERO;
            pIfaceNode->u4RxGrpSrcQueries = IGMP_ZERO;
            pIfaceNode->u4Txv2Leaves = IGMP_ZERO;
            pIfaceNode->u4CKSumError = IGMP_ZERO;
            pIfaceNode->u4PktLenError = IGMP_ZERO;;
            pIfaceNode->u4PktWithLocalIP = IGMP_ZERO;
            pIfaceNode->u4SubnetCheckFailure = IGMP_ZERO;;
            pIfaceNode->u4QryFromNonQuerier = IGMP_ZERO;;
            pIfaceNode->u4ReportVersionMisMatch = IGMP_ZERO;;
            pIfaceNode->u4QryVersionMisMatch = IGMP_ZERO;;
            pIfaceNode->u4UnknownMsgType = IGMP_ZERO;;
            pIfaceNode->u4InvalidV1Report = IGMP_ZERO;;
            pIfaceNode->u4InvalidV2Report = IGMP_ZERO;;
            pIfaceNode->u4RouterAlertCheckFailure = IGMP_ZERO;;
            pIfaceNode->u4BadScopeErr = IGMP_ZERO;
            pIfaceNode->u4IgmpRxv1v2Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpRxv3Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpTxv1v2Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpTxv3Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpInSSMPkts = IGMP_ZERO;
            pIfaceNode->u4IgmpInvalidSSMPkts = IGMP_ZERO;
            pIfaceNode->u4IgmpMalformedPkt = IGMP_ZERO;
            pIfaceNode->u4IgmpSocketErr = IGMP_ZERO;
#ifdef MULTICAST_SSM_ONLY
            pIfaceNode->u4DroppedASMIncomingPkts = IGMP_ZERO;
#endif
            i4CurrIfIndex = i4NextIfIndex;
            if (nmhGetNextIndexIgmpInterfaceTable (i4CurrIfIndex,
                                                   &i4NextIfIndex)
                == SNMP_FAILURE)
            {
                u1IsShowAll = FALSE;
            }

        }
    }
    while (u1IsShowAll);

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpCliSetTrace                                     */
/*                                                                         */
/*     Description   : Confiures IGMP trace flag                           */
/*                                                                         */
/*     INPUT         : u4Trace - Trace flags                               */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus)
{
    UINT4               u4ErrorCode;
    UNUSED_PARAM (CliHandle);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetTrace()\r \n");
    if (nmhTestv2FsIgmpTraceLevel (&u4ErrorCode, i4TraceStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIgmpTraceLevel (i4TraceStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpCliSetDebug                                     */
/*                                                                         */
/*     Description   : Confiures IGMP debug flag                           */
/*                                                                         */
/*     INPUT         : u4Debug - Debug flags                               */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpCliSetDebug (tCliHandle CliHandle, INT4 i4DebugStatus)
{

    UINT4               u4ErrorCode;
    if (nmhTestv2FsIgmpDebugLevel (&u4ErrorCode, i4DebugStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIgmpDebugLevel (i4DebugStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : IgmpDispHhMmSs
*  Description   : Converts total seconds into Days,HH:MM:SS format
*
*  Input(s)      :  Seconds, pointer to char buffer
*  Output(s)     : .None
*  Return Values : .None
*********************************************************************/
VOID
IgmpDispHhMmSs (UINT4 u4Time, CHR1 * Time)
{
    UINT4               u4Days, u4Hrs, u4Mins, u4Secs, u4CentiSecs;

    MEMSET (Time, 0, 16);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpDispHhMmSs()\r \n");
    if (u4Time == 0)
    {
        SPRINTF ((CHR1 *) Time, "[0d 00:00:00.00]");
        return;
    }

    u4CentiSecs = u4Time % 100;
    u4Time = u4Time / 100;

    u4Secs = u4Time % 60;
    u4Time = u4Time / 60;

    u4Mins = u4Time % 60;
    u4Time = u4Time / 60;

    u4Hrs = u4Time % 24;
    u4Days = u4Time / 24;

    SPRINTF ((CHR1 *) Time, "[%ud %02u:%02u:%02u.%02u]", u4Days, u4Hrs,
             u4Mins, u4Secs, u4CentiSecs);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpDispHhMmSs()\r \n");
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgmpCliUpdateInterfaceTable                        */
/*                                                                           */
/*     DESCRIPTION      : This function creates or updates a interface entry */
/*                                                                           */
/*     INPUT            : i4IfIndex  - Interface index                       */
/*                        u1Status    - IGMP_CREATE/IGMP_SET_ACTIVE            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpCliUpdateInterfaceTables (tCliHandle CliHandle,
                              INT4 i4IfIndex, UINT1 u1Status,
                              UINT1 *pu1Create, UINT1 u1NotInservice)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliUpdateInterfaceTables()\r \n");
    /* Check if the Interface already exists. If the Get
     * function returns Success, for CREATE and WAIT then a interface already
     * exists. Otherwise we need to create an entry.
     */

    if (u1Status == IGMP_CREATE)
    {
        nmhGetIgmpInterfaceStatus (i4IfIndex, &i4RowStatus);
        if (i4RowStatus == 0)
        {
            /* No Entry exists with this VlanId */
            if (nmhTestv2IgmpInterfaceStatus (&u4ErrCode, i4IfIndex,
                                              IGMP_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetIgmpInterfaceStatus (i4IfIndex,
                                           IGMP_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_IGMP_INTERFACE_MEM_EXHAUST);
                return (CLI_FAILURE);
            }
            /* set it to TRUE to indicate the interface is newly created */
            *pu1Create = IGMP_TRUE;
        }
        else
        {
            /* u1NotInservice is used to identify whether the interface node 
             * can be set to not in service for the table objects,
             * only version and Interface status are allowed to put the 
             * row status to not in service */
            if (u1NotInservice == IGMP_FALSE)
            {
                return (CLI_SUCCESS);
            }

            /* Entry already exists with this IfIndex, so make it to
             * not-in-service and proceed further.
             */

            if (nmhTestv2IgmpInterfaceStatus (&u4ErrCode, i4IfIndex,
                                              IGMP_NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetIgmpInterfaceStatus (i4IfIndex,
                                           IGMP_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
    }
    else
    {
        if (nmhTestv2IgmpInterfaceStatus (&u4ErrCode, i4IfIndex,
                                          IGMP_ACTIVE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIgmpInterfaceStatus (i4IfIndex, IGMP_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliUpdateInterfaceTables()\r \n");
    return CLI_SUCCESS;
}

VOID
IgmpShowRunningConfigGroupListTable (tCliHandle CliHandle)
{
    UINT4               u4GrpId = 0;
    UINT4               u4GrpIp = 0;
    UINT4               u4Mask = 0;
    UINT4               u4NextGrpId = 0;
    UINT4               u4NextGrpIp = 0;
    UINT4               u4NextMask = 0;
    INT4                i4ScalarObject = 0;
    CHR1               *pu1String = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpShowRunningConfigGroupListTable()\r \n");

    nmhGetFsIgmpGlobalStatus (&i4ScalarObject);
    if (i4ScalarObject != IGMP_DISABLE)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                      IgmpGetModuleName (IGMP_ENTRY_MODULE),
                      "Entering IgmpShowRunningConfigGroupListTable()\r \n");
    }
    else
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting IgmpShowRunningConfigGroupListTable()\r \n");
        return;
    }

    if (SNMP_FAILURE ==
        nmhGetFirstIndexFsIgmpGroupListTable (&u4NextGrpId, &u4NextGrpIp,
                                              &u4NextMask))
    {
        return;
    }
    do
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpIp);
        CliPrintf (CliHandle, "ip igmp group-list %d %s ",
                   u4NextGrpId, pu1String);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextMask);
        CliPrintf (CliHandle, "%s\n ", pu1String);
        u4GrpId = u4NextGrpId;
        u4GrpIp = u4NextGrpIp;
        u4Mask = u4NextMask;
    }
    while (SNMP_FAILURE !=
           nmhGetNextIndexFsIgmpGroupListTable (u4GrpId, &u4NextGrpId,
                                                u4GrpIp, &u4NextGrpIp,
                                                u4Mask, &u4NextMask));
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpShowRunningConfigGroupListTable()\r \n");
    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IgmpShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the IGMP  Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgmpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpShowRunningConfig()\r \n");

    IgmpShowRunningConfigScalars (CliHandle);
    IgmpShowRunningConfigGroupListTable (CliHandle);
    if (u4Module == ISS_IGMP_SHOW_RUNNING_CONFIG)
    {
        IgmpShowRunningConfigInterface (CliHandle);
    }

    IGMP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpShowRunningConfig()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgmpShowRunningConfigScalars                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Igmp for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IgmpShowRunningConfigScalars (tCliHandle CliHandle)
{
    INT4                i4ScalarObject;
    INT4                i4JoinPktRate;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpShowRunningConfigScalars()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4ScalarObject);
    if (i4ScalarObject != IGMP_DISABLE)
    {
        CliPrintf (CliHandle, "set ip igmp enable\r\n");
    }
    nmhGetFsIgmpGlobalLimit ((UINT4 *) &i4ScalarObject);
    {
        if (i4ScalarObject != IGMP_ZERO)
        {
            CliPrintf (CliHandle, "ip igmp limit %d \r\n", i4ScalarObject);
        }
    }
    nmhGetFsIgmpJoinPktRate (&i4JoinPktRate);
    if ((i4JoinPktRate >= IGMP_INTERFACE_MIN_JOIN_RATE)
        && (i4JoinPktRate <= IGMP_INTERFACE_MAX_JOIN_RATE))
    {
        CliPrintf (CliHandle, "ip igmp join ratelimit %d \r\n", i4JoinPktRate);
    }

    if ((nmhGetFsIgmpSSMMapStatus (&i4ScalarObject)) == SNMP_FAILURE)
    {
        return;
    }
    if (i4ScalarObject == IGMP_TRUE)
    {
        CliPrintf (CliHandle, "ip multicast ssm-map enable\r\n");
    }
    IgmpShowRunningConfigSSMMapGroupTable (CliHandle);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpShowRunningConfigScalars()\r \n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgmpShowRunningConfigSSMMapGroupTable              */
/*                                                                           */
/*     DESCRIPTION      : This function displays SSM Map Group Table in Igmp */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IgmpShowRunningConfigSSMMapGroupTable (tCliHandle CliHandle)
{
    UINT4               u4StartGrpAddress = IGMP_ZERO;
    UINT4               u4NextStartGrpAddress = IGMP_ZERO;
    UINT4               u4EndGrpAddress = IGMP_ZERO;
    UINT4               u4NextEndGrpAddress = IGMP_ZERO;
    UINT4               u4SrcAddress = IGMP_ZERO;
    UINT4               u4NextSrcAddress = IGMP_ZERO;
    CHR1               *pu1String = NULL;

    if ((nmhGetFirstIndexFsIgmpSSMMapGroupTable (&u4StartGrpAddress,
                                                 &u4EndGrpAddress,
                                                 &u4SrcAddress)) ==
        SNMP_FAILURE)
    {
        return;
    }

    u4NextStartGrpAddress = u4StartGrpAddress;
    u4NextEndGrpAddress = u4EndGrpAddress;
    u4NextSrcAddress = u4SrcAddress;
    do
    {
        u4StartGrpAddress = u4NextStartGrpAddress;
        u4EndGrpAddress = u4NextEndGrpAddress;
        u4SrcAddress = u4NextSrcAddress;

        CliPrintf (CliHandle, "ip multicast ssm-map ");
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4StartGrpAddress);
        CliPrintf (CliHandle, "%s ", pu1String);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4EndGrpAddress);
        CliPrintf (CliHandle, "%s ", pu1String);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddress);
        CliPrintf (CliHandle, "%s\r\n", pu1String);

    }
    while ((nmhGetNextIndexFsIgmpSSMMapGroupTable (u4StartGrpAddress,
                                                   &u4NextStartGrpAddress,
                                                   u4EndGrpAddress,
                                                   &u4NextEndGrpAddress,
                                                   u4SrcAddress,
                                                   &u4NextSrcAddress)) ==
           SNMP_SUCCESS);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgmpShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current interface specific  */
/*                        configurations in OSPF                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
IgmpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT1                i1OutCome = SNMP_FAILURE;
    CHR1               *pu1String = NULL;
    INT4                i4TableObject = IGMP_ZERO;
    INT4                i4IfIndex;
    INT4                i4IfIndx;
    INT4                i4PrevIfIndex;
    INT4                i4FilterMode = IGMP_ZERO;
    UINT4               u4GrpIpAddr = IGMP_ZERO;
    UINT4               u4GroupIp = IGMP_ZERO;
    UINT4               u4PrevGrpIpAddr = IGMP_ZERO;
    UINT4               u4SrcIpAddr = IGMP_ZERO;
    UINT4               u4PrevSrcIpAddr = IGMP_ZERO;
    UINT4               u4TableObject = IGMP_ZERO;
    UINT4               u4TableObject1 = IGMP_ZERO;
    UINT4               u4Version = IGMP_ZERO;
    UINT1               u1BangStatus = FALSE;
    INT4                i4Result = IGMP_ZERO;
    INT4                i4SourceFound = IGMP_ZERO;
    INT4                i4RetVal = IGMP_FAILURE;
    INT1                ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1InterfaceName = NULL;
    tIgmpIPvXAddr       GrpAddrX;
    IGMP_IPVX_ADDR_CLEAR (&GrpAddrX);
    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpShowRunningConfigInterfaceDetails()\r \n");
    pi1InterfaceName = &ai1InterfaceName[0];
    MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (nmhValidateIndexInstanceFsIgmpInterfaceTable (i4Index) == SNMP_SUCCESS)
    {
        nmhGetFsIgmpInterfaceAdminStatus (i4Index, &i4TableObject);
        CfaCliConfGetIfName ((UINT4) i4Index, pi1InterfaceName);
        CliPrintf (CliHandle, "interface %s\r\n", pi1InterfaceName);
        u1BangStatus = TRUE;
        if (i4TableObject != IGMP_DISABLE)
        {
            CliPrintf (CliHandle, "set ip igmp enable\r\n");
        }

        nmhGetFsIgmpInterfaceFastLeaveStatus (i4Index, &i4TableObject);
        if (i4TableObject != IGMP_FAST_LEAVE_DISABLE)
        {
            CliPrintf (CliHandle, "ip igmp immediate-leave\r\n");
        }

        nmhGetIgmpInterfaceVersion (i4Index, &u4Version);
        if ((u4Version != IGMP_VERSION_2) && (u4Version != IGMP_ZERO))
        {
            CliPrintf (CliHandle, "ip igmp version %d\r\n", u4Version);
        }
        nmhGetFsIgmpInterfaceChannelTrackStatus (i4Index, &i4TableObject);
        if (i4TableObject != IGMP_CHANNEL_TRACK_DISABLE)
        {
            CliPrintf (CliHandle, "ip igmp explicit-tracking\r\n");
        }
        i4RetVal = IgmpUtilGetConfiguredQueryInterval (i4Index, &u4TableObject);
        if (i4RetVal == IGMP_SUCCESS)
        {
            if ((u4TableObject != IGMP_DEFAULT_QUERY_INTERVAL)
                && (u4TableObject != IGMP_ZERO))
            {
                CliPrintf (CliHandle, "ip igmp query-interval %d\r\n",
                           u4TableObject);
            }
        }
        nmhGetIgmpInterfaceQueryMaxResponseTime (i4Index, &u4TableObject);
        if ((u4TableObject != IGMP_DEFAULT_MAX_RESP_TIME) &&
            (u4TableObject != IGMP_ZERO))
        {
            CliPrintf (CliHandle, "ip igmp query-max-response-time %d\r\n",
                       u4TableObject);
        }

        nmhGetIgmpInterfaceRobustness (i4Index, &u4TableObject);
        if (u4TableObject != IGMP_DEFAULT_ROB_VARIABLE)
        {
            CliPrintf (CliHandle, "ip igmp robustness %d\r\n", u4TableObject);
        }

        nmhGetIgmpInterfaceLastMembQueryIntvl (i4Index, &u4TableObject);
        if ((u4TableObject != IGMP_DEFAULT_LAST_MEMBER_QIVAL) &&
            (u4TableObject != IGMP_ZERO))
        {
            CliPrintf (CliHandle, "ip igmp last-member-query-interval %d\r\n",
                       u4TableObject);
        }
        nmhGetFsIgmpInterfaceJoinPktRate (i4Index, &i4TableObject);
        if (i4TableObject != IGMP_ZERO)
        {
            CliPrintf (CliHandle, "ip igmp join ratelimit %d\r\n",
                       i4TableObject);

        }
        nmhGetFsIgmpInterfaceLimit (i4Index, &u4TableObject);
        if (u4TableObject != IGMP_ZERO)
        {
            nmhGetFsIgmpInterfaceGroupListId (i4Index, &u4TableObject1);
            if (u4TableObject1 != IGMP_ZERO)
            {
                CliPrintf (CliHandle, "ip igmp limit %d except %d\r\n",
                           u4TableObject, u4TableObject1);
            }
            else
            {
                CliPrintf (CliHandle, "ip igmp limit %d\r\n", u4TableObject);
            }
        }

        i1OutCome = nmhGetFirstIndexIgmpCacheTable (&u4GrpIpAddr, &i4IfIndex);
        while (i1OutCome != SNMP_FAILURE)
        {
            if (i4IfIndex == i4Index)
            {
                nmhGetIgmpCacheStatus (u4GrpIpAddr, i4IfIndex, &i4TableObject);
                i4Result =
                    IgmpUtilIsStaticEntry (u4GrpIpAddr, (UINT4) i4IfIndex,
                                           IPVX_ADDR_FMLY_IPV4, IGMP_FALSE);
                if ((i4TableObject == ACTIVE) && (i4Result == TRUE) &&
                    (u4Version != IGMP_VERSION_3))
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpIpAddr);
                    CliPrintf (CliHandle, "ip igmp static-group %s \r\n",
                               pu1String);
                }

                IGMP_IPVX_ADDR_INIT_IPV4 (GrpAddrX, IPVX_ADDR_FMLY_IPV4,
                                          (UINT1 *) &u4GrpIpAddr);
                i4SourceFound =
                    IgmpUtilIsSourcePresent (GrpAddrX, (UINT4) i4IfIndex,
                                             IPVX_ADDR_FMLY_IPV4);
                if ((i4SourceFound == TRUE) && (u4Version == IGMP_VERSION_3)
                    && (i4Result == TRUE))
                {
                    i1OutCome =
                        nmhGetNextIndexIgmpSrcListTable (u4GrpIpAddr,
                                                         &u4GroupIp, i4Index,
                                                         &i4IfIndx, u4SrcIpAddr,
                                                         &u4SrcIpAddr);
                    u4PrevGrpIpAddr = IGMP_ZERO;
                    while (i1OutCome != SNMP_FAILURE)
                    {
                        if ((i4Index == i4IfIndx) && (u4GrpIpAddr == u4GroupIp))
                        {
                            nmhGetIgmpSrcListStatus (u4GroupIp, i4IfIndx,
                                                     u4SrcIpAddr,
                                                     &i4TableObject);
                            nmhGetIgmpCacheSourceFilterMode (u4GroupIp,
                                                             i4IfIndx,
                                                             &i4FilterMode);
                            if (i4TableObject == ACTIVE)
                            {
                                if ((u4PrevGrpIpAddr != u4GroupIp) &&
                                    (i4FilterMode == IGMP_FMODE_EXCLUDE))
                                {
                                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                                               u4GroupIp);
                                    CliPrintf (CliHandle,
                                               "ip igmp static-group %s ",
                                               pu1String);
                                    CliPrintf (CliHandle, "\r\n");
                                }
                                if (((IgmpUtilIsSourceConfigured
                                      (u4GroupIp, u4SrcIpAddr, i4IfIndx,
                                       IPVX_ADDR_FMLY_IPV4)) == IGMP_TRUE)
                                    && (u4SrcIpAddr != IGMP_ZERO))
                                {
                                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                                               u4GroupIp);
                                    CliPrintf (CliHandle,
                                               "ip igmp static-group %s ",
                                               pu1String);
                                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                                               u4SrcIpAddr);
                                    CliPrintf (CliHandle, "source %s \r\n",
                                               pu1String);
                                }
                            }
                        }
                        i4PrevIfIndex = i4IfIndx;
                        u4PrevGrpIpAddr = u4GroupIp;
                        u4PrevSrcIpAddr = u4SrcIpAddr;
                        i1OutCome = nmhGetNextIndexIgmpSrcListTable
                            (u4PrevGrpIpAddr, &u4GroupIp,
                             i4PrevIfIndex, &i4IfIndx, u4PrevSrcIpAddr,
                             &u4SrcIpAddr);
                    }
                }
                else if ((i4SourceFound == FALSE)
                         && (u4Version == IGMP_VERSION_3) && (i4Result == TRUE))
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4GrpIpAddr);
                    CliPrintf (CliHandle, "ip igmp static-group %s \r\n",
                               pu1String);
                }
            }
            i4PrevIfIndex = i4IfIndex;
            u4PrevGrpIpAddr = u4GrpIpAddr;
            i1OutCome = nmhGetNextIndexIgmpCacheTable (u4PrevGrpIpAddr,
                                                       &u4GrpIpAddr,
                                                       i4PrevIfIndex,
                                                       &i4IfIndex);
        }
    }
    if (u1BangStatus == TRUE)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    IGMP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpShowRunningConfigInterfaceDetails()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgmpShowRunningConfigInterface                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
IgmpShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT4                i4IfIndex;
    INT4                i4NextIfIndex;
    INT4                i4ScalarObject = 0;
    UINT1               u1isShowAll = TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpShowRunningConfigInterface()\r \n");

    nmhGetFsIgmpGlobalStatus (&i4ScalarObject);
    if (i4ScalarObject != IGMP_DISABLE)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                      IgmpGetModuleName (IGMP_ENTRY_MODULE),
                      "Entering IgmpShowRunningConfigInterface()\r \n");
    }
    if (nmhGetFirstIndexFsIgmpInterfaceTable (&i4NextIfIndex) == SNMP_SUCCESS)
    {
        do
        {

            IGMP_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            IgmpShowRunningConfigInterfaceDetails (CliHandle, i4NextIfIndex);

            CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
            IGMP_LOCK ();

            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
            i4IfIndex = i4NextIfIndex;

            if (nmhGetNextIndexFsIgmpInterfaceTable (i4IfIndex, &i4NextIfIndex)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll == TRUE);
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpShowRunningConfigInterface()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssIgmpShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the IGMP debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssIgmpShowDebugging (tCliHandle CliHandle)
{

    INT4                i4IgmpTrace = 0;
    INT4                i4IgmpDebug = 0;
    INT4                i4IgmpStatus = 0;
    INT4                i4IgmpDebugFlag = 0;
    INT4                i4IgmpTraceFlag = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IssIgmpShowDebugging()\r \n");
    nmhGetFsIgmpGlobalStatus (&i4IgmpStatus);
    if (i4IgmpStatus != IGMP_DISABLE)
    {

        nmhGetFsIgmpDebugLevel (&i4IgmpDebugFlag);
        nmhGetFsIgmpTraceLevel (&i4IgmpTraceFlag);
        if ((i4IgmpDebugFlag == 0) && (i4IgmpTraceFlag == 0))
        {
            return;
        }

        if ((i4IgmpDebugFlag != 0) | (i4IgmpTraceFlag != 0))
        {
            CliPrintf (CliHandle, "\r\n IGMP :");
        }

        if ((i4IgmpDebugFlag & IGMP_INIT_SHUT_MODULE) != 0)
        {
            CliPrintf (CliHandle, "\r\n debug ip igmp init");
            i4IgmpDebug = 1;
        }
        if ((i4IgmpDebugFlag & IGMP_GRP_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp grp");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " grp");
            }
        }
        if ((i4IgmpDebugFlag & IGMP_QRY_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp qry");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " qry");
            }

        }
        if ((i4IgmpDebugFlag & IGMP_TMR_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp tmr");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " tmr");
            }
        }
        if ((i4IgmpDebugFlag & IGMP_OS_RES_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp resource");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " os_res");
            }
        }
        if ((i4IgmpDebugFlag & IGMP_IO_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp i/o");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " i/o");
            }
        }
        if ((i4IgmpDebugFlag & IGMP_BUFFER_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp buffer");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " buffer");
            }
        }

        if ((i4IgmpDebugFlag & IGMP_NP_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp np");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " np");
            }
        }

        if ((i4IgmpDebugFlag & IGMP_MGMT_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp mgmt");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " mgmt");
            }
        }

        if ((i4IgmpDebugFlag & IGMP_ENTRY_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp entry");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " entry");
            }
        }
        if ((i4IgmpDebugFlag & IGMP_EXIT_MODULE) != 0)
        {
            if (i4IgmpDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp exit");
                i4IgmpDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " exit");
            }
        }
        if ((i4IgmpTraceFlag & IGMP_DATA_MODULE) != 0)
        {
            CliPrintf (CliHandle, "\r\n debug ip igmp trace data");
            i4IgmpTrace = 1;
        }
        if ((i4IgmpTraceFlag & IGMP_CNTRL_MODULE) != 0)
        {
            if (i4IgmpTrace == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp trace control");
                i4IgmpTrace = 1;
            }
            else
            {
                CliPrintf (CliHandle, " control");
            }
        }
        if ((i4IgmpTraceFlag & IGMP_Rx_MODULE) != 0)
        {
            if (i4IgmpTrace == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp trace Rx");
                i4IgmpTrace = 1;
            }
            else
            {
                CliPrintf (CliHandle, " Rx");
            }
        }
        if ((i4IgmpTraceFlag & IGMP_Tx_MODULE) != 0)
        {
            if (i4IgmpTrace == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp trace Tx");
                i4IgmpTrace = 1;
            }
            else
            {
                CliPrintf (CliHandle, " Tx");
            }
        }
        CliPrintf (CliHandle, "\r\n");
    }
    else
    {
        return;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IssIgmpShowDebugging()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpCliSetGlobalLimit                            */
/*                                                                         */
/*     Description   : Set/Reset Interface Limit           */
/*                                                                         */
/*     INPUT         : u4Limit - Global Limit */
/*     OUTPUT        :               */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT4
IgmpCliSetGlobalLimit (tCliHandle CliHandle, UINT4 u4Limit)
{
    UINT4               u4ErrorCode;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetGlobalLimit()\r \n");
    if (nmhTestv2FsIgmpGlobalLimit (&u4ErrorCode, u4Limit) == SNMP_SUCCESS)
    {
        if (nmhSetFsIgmpGlobalLimit (u4Limit) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetGlobalLimit()\r \n");
    return CLI_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpCliSetInterfaceLimit                            */
/*                                                                         */
/*     Description   : Set/Reset Interface Limit           */
/*                                                                         */
/*     INPUT         : i4IfaceIndex - interface index
 *                     u4IntfLimit  - Interface Limit
 *                     u4GrpId      - GroupList Id to exempt Limit on interface*/
/*                                                                         */
/*     OUTPUT        :               */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT4
IgmpCliSetInterfaceLimit (tCliHandle CliHandle, INT4 i4IfaceIndex,
                          UINT4 u4IntfLimit, UINT4 u4GrpId)
{
    UINT4               u4ErrorCode;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetInterfaceLimit()\r \n");
    if (nmhTestv2FsIgmpInterfaceLimit (&u4ErrorCode, i4IfaceIndex, u4IntfLimit)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhTestv2FsIgmpInterfaceGroupListId
        (&u4ErrorCode, i4IfaceIndex, u4GrpId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIgmpInterfaceLimit (i4IfaceIndex, u4IntfLimit) == SNMP_SUCCESS)
    {
        if (nmhSetFsIgmpInterfaceGroupListId (i4IfaceIndex, u4GrpId) ==
            SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
    }
    CLI_FATAL_ERROR (CliHandle);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetInterfaceLimit()\r \n");
    return CLI_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetGroupList*/
/*                                                                         */
/*     Description   :  Configures/deletes Igmp Group List record          */
/*                                                                         */
/*     INPUT         :  u4GroupId - GroupList Id   
 *                      u4GrpIp - Group IP                 
 *                      u4mask -  Mask
 *                      i4Status - RowStatus                               */
/*                                                                         */
/*     OUTPUT        :   none            */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT4
IgmpCliSetGroupList (tCliHandle CliHandle, UINT4 u4GroupId, UINT4 u4GrpIp,
                     UINT4 u4mask, INT4 i4Status)
{
    UINT4               u4NextGrpId;
    UINT4               u4MatchGrpId = u4GroupId;
    UINT4               u4NextGrpIP;
    UINT4               u4NextGrpmask;
    UINT4               u4ErrorCode;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetGroupList()\r \n");
    if (i4Status == CLI_ENABLE)
    {
        i4Status = CREATE_AND_GO;
        if (nmhTestv2FsIgmpGrpListRowStatus
            (&u4ErrorCode, u4GroupId, u4GrpIp, u4mask,
             i4Status) == SNMP_SUCCESS)
        {
            if (nmhSetFsIgmpGrpListRowStatus
                (u4GroupId, u4GrpIp, u4mask, i4Status) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
    }
    else
    {
        i4Status = DESTROY;
        if (u4GrpIp == 0)
        {                        /* Delete All entries matches with GrpId */
            while (SNMP_SUCCESS ==
                   nmhGetNextIndexFsIgmpGroupListTable (u4GroupId, &u4NextGrpId,
                                                        u4GrpIp, &u4NextGrpIP,
                                                        u4mask, &u4NextGrpmask))
            {
                if (u4MatchGrpId != u4NextGrpId)
                {
                    break;
                }
                if (SNMP_SUCCESS != nmhTestv2FsIgmpGrpListRowStatus
                    (&u4ErrorCode, u4NextGrpId, u4NextGrpIP, u4NextGrpmask,
                     i4Status))
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsIgmpGrpListRowStatus (u4NextGrpId, u4NextGrpIP,
                                                  u4NextGrpmask,
                                                  i4Status) != SNMP_SUCCESS)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

                u4GroupId = u4NextGrpId;
                u4GrpIp = u4NextGrpIP;
                u4mask = u4NextGrpmask;
            }
        }
        else
        {                        /*delete specified entry */
            if (SNMP_SUCCESS != nmhTestv2FsIgmpGrpListRowStatus
                (&u4ErrorCode, u4GroupId, u4GrpIp, u4mask, i4Status))
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsIgmpGrpListRowStatus (u4GroupId, u4GrpIp,
                                              u4mask, i4Status) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliSetGroupList()\r \n");
    return CLI_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowGroupList*/
/*                                                                         */
/*     Description   :  Shows configured Igmp Group List records           */
/*                                                                         */
/*     INPUT         : CliHandle                         */
/*                                                                         */
/*     OUTPUT        :               */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT4
IgmpCliShowGroupList (tCliHandle CliHandle)
{
    UINT4               u4GrpId = 0;
    UINT4               u4GrpIp = 0;
    UINT4               u4Mask = 0;
    UINT4               u4NextGrpId = 0;
    UINT4               u4NextGrpIp = 0;
    UINT4               u4NextMask = 0;
    CHR1               *pu1String = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowGroupList()\r \n");
    i1RetVal =
        nmhGetFirstIndexFsIgmpGroupListTable (&u4NextGrpId, &u4NextGrpIp,
                                              &u4NextMask);

    if (i1RetVal == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (u4GrpId != u4NextGrpId)
        {
            CliPrintf (CliHandle, "\r\n GroupList : %u ", u4NextGrpId);
            CliPrintf (CliHandle, "\r\n --------------- ");
        }
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextGrpIp);
        CliPrintf (CliHandle, "\r\n  Group Address : %-16s Mask : ", pu1String);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4NextMask);
        CliPrintf (CliHandle, "%-16s \n", pu1String);
        u4GrpId = u4NextGrpId;
        u4GrpIp = u4NextGrpIp;
        u4Mask = u4NextMask;
    }
    while (SNMP_FAILURE !=
           nmhGetNextIndexFsIgmpGroupListTable (u4GrpId, &u4NextGrpId,
                                                u4GrpIp, &u4NextGrpIp,
                                                u4Mask, &u4NextMask));
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCliShowGroupList()\r \n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliSetSSMMapStatus                             */
/*                                                                         */
/*     Description   :  Enables/disables IGMP SSM Map status globally.     */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                     i4Status - IGMP Enable/disable                      */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT4
IgmpCliSetSSMMapStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliSetSSMMapStatus()\r\n");
    if (nmhTestv2FsIgmpSSMMapStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Igmp SSM Map is globally disabled\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpSSMMapStatus (i4Status) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Enabling Igmp SSM Map globally failed\r\n");
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpCliSetSSMMapStatus()\r\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliConfigureSSMMap                             */
/*                                                                         */
/*     Description   :  Configures/Deletes the static SSM mapping for the  */
/*                      group-address defined in the range to the given    */
/*                      source address.                                    */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                     u4StartGrpAddress - Starting multicast group address*/
/*                     u4EndGrpAddress - Ending multicast group address    */
/*                     u4SrcAddress - Unicast IP address of source         */
/*                     i4Status - Configure/Delete static SSM mapping      */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT4
IgmpCliConfigureSSMMap (tCliHandle CliHandle, UINT4 u4StartGrpAddress,
                        UINT4 u4EndGrpAddress, UINT4 u4SrcAddress,
                        INT4 i4Status)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliConfigureSSMMap()\r\n");

#ifdef PIM_WANTED
    if (IGMP_CHECK_FREE_PIM_QUEUE () == OSIX_FAILURE)
    {
        CliPrintf (CliHandle,
                   "IGMP is busy in processing already configured SSM Mapped entries... "
                   "Please try again later\r\n");
        return CLI_FAILURE;
    }
#endif

    UNUSED_PARAM (CliHandle);
    if (CLI_ENABLE == i4Status)
    {
        if ((nmhGetFsIgmpSSMMapRowStatus (u4StartGrpAddress, u4EndGrpAddress,
                                          u4SrcAddress,
                                          &i4RetVal)) == SNMP_SUCCESS)
        {
            /* need to check retval? */
            CLI_SET_ERR (CLI_IGMP_SSMMAP_EXISTS);
            return CLI_FAILURE;
        }
        if ((nmhTestv2FsIgmpSSMMapRowStatus
             (&u4ErrorCode, u4StartGrpAddress, u4EndGrpAddress, u4SrcAddress,
              IGMP_CREATE_AND_GO)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetFsIgmpSSMMapRowStatus (u4StartGrpAddress, u4EndGrpAddress,
                                          u4SrcAddress,
                                          IGMP_CREATE_AND_GO)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if ((nmhGetFsIgmpSSMMapRowStatus (u4StartGrpAddress, u4EndGrpAddress,
                                          u4SrcAddress,
                                          &i4RetVal)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IGMP_SSMMAP_NOT_EXISTS);
            return CLI_FAILURE;
        }
        if ((nmhTestv2FsIgmpSSMMapRowStatus
             (&u4ErrorCode, u4StartGrpAddress, u4EndGrpAddress, u4SrcAddress,
              IGMP_DESTROY)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetFsIgmpSSMMapRowStatus (u4StartGrpAddress, u4EndGrpAddress,
                                          u4SrcAddress,
                                          IGMP_DESTROY)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpCliConfigureSSMMap()\r\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpCliShowSSMMapGroup                             */
/*                                                                         */
/*     Description   :  Displays the SSM mapped IGMP group and source      */
/*                      information.                                       */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                     u4GrpAddress - Multicast group address              */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT4
IgmpCliShowSSMMapGroup (tCliHandle CliHandle, UINT4 u4GrpAddress)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tIgmpIPvXAddr       StartGrpAddr;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4StartGrpAddress = IGMP_ZERO;
    UINT4               u4NextStartGrpAddress = IGMP_ZERO;
    UINT4               u4EndGrpAddress = IGMP_ZERO;
    UINT4               u4NextEndGrpAddress = IGMP_ZERO;
    UINT4               u4SrcAddress = IGMP_ZERO;
    UINT4               u4NextSrcAddress = IGMP_ZERO;
    UINT4               u4Index = IGMP_ZERO;
    UINT1               u1Flag = IGMP_FALSE;
    CHR1               *pu1String = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCliShowSSMMapGroup()\r\n");

    if ((nmhGetFsIgmpSSMMapStatus (&i4RetVal)) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    if (IGMP_DISABLE == i4RetVal)
    {
        CliPrintf (CliHandle, "%% IGMP SSM Mapping is disabled \r\n");
        return CLI_SUCCESS;
    }

    if ((nmhGetFirstIndexFsIgmpSSMMapGroupTable (&u4StartGrpAddress,
                                                 &u4EndGrpAddress,
                                                 &u4SrcAddress)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%% No entries present in IGMP SSM Mapping table \r\n");
        return CLI_SUCCESS;
    }

    if (IGMP_ZERO == u4GrpAddress)
    {
        CliPrintf (CliHandle,
                   "\r\n%-21s%-19s%-15s\r\n",
                   "Start Group-Address", "End Group-Address", "Source-List");
        CliPrintf (CliHandle,
                   "%-21s%-19s%-15s",
                   "-------------------", "-----------------", "-----------");

        u4NextStartGrpAddress = u4StartGrpAddress;
        u4NextEndGrpAddress = u4EndGrpAddress;
        u4NextSrcAddress = u4SrcAddress;
        do
        {
            if ((IGMP_FALSE == u1Flag) ||
                ((u4StartGrpAddress != u4NextStartGrpAddress) &&
                 (u4EndGrpAddress != u4NextEndGrpAddress)))
            {
                u4StartGrpAddress = u4NextStartGrpAddress;
                u4EndGrpAddress = u4NextEndGrpAddress;
                u4SrcAddress = u4NextSrcAddress;

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4StartGrpAddress);
                CliPrintf (CliHandle, "\r\n%-21s", pu1String);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4EndGrpAddress);
                CliPrintf (CliHandle, "%-19s", pu1String);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddress);
                CliPrintf (CliHandle, "%s", pu1String);
            }
            else
            {
                u4SrcAddress = u4NextSrcAddress;

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddress);
                CliPrintf (CliHandle, ", %s", pu1String);
            }

            u1Flag = IGMP_TRUE;
        }
        while ((nmhGetNextIndexFsIgmpSSMMapGroupTable (u4StartGrpAddress,
                                                       &u4NextStartGrpAddress,
                                                       u4EndGrpAddress,
                                                       &u4NextEndGrpAddress,
                                                       u4SrcAddress,
                                                       &u4NextSrcAddress)) ==
               SNMP_SUCCESS);
    }
    else
    {
        IGMP_IPVX_ADDR_CLEAR (&StartGrpAddr);
        IGMP_IPVX_ADDR_INIT_IPV4 (StartGrpAddr, IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4GrpAddress);

        pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (StartGrpAddr, &i4RetVal);
        if ((IGMP_TRUE == i4RetVal) && (NULL != pSSMMappedGrp))
        {
            CliPrintf (CliHandle,
                       "\r\n%-21s%-19s%-15s\r\n",
                       "Start Group-Address", "End Group-Address",
                       "Source-List");
            CliPrintf (CliHandle, "%-21s%-19s%-15s", "-------------------",
                       "-----------------", "-----------");

            IGMP_IP_COPY_FROM_IPVX (&u4StartGrpAddress,
                                    pSSMMappedGrp->StartGrpAddr,
                                    IPVX_ADDR_FMLY_IPV4);
            IGMP_IP_COPY_FROM_IPVX (&u4EndGrpAddress, pSSMMappedGrp->EndGrpAddr,
                                    IPVX_ADDR_FMLY_IPV4);

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4StartGrpAddress);
            CliPrintf (CliHandle, "\r\n%-21s", pu1String);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4EndGrpAddress);
            CliPrintf (CliHandle, "%-19s", pu1String);

            for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
            {
                if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero,
                                            pSSMMappedGrp->SrcAddr[u4Index]) !=
                    IGMP_ZERO)
                {
                    if (IGMP_FALSE == u1Flag)
                    {
                        IGMP_IP_COPY_FROM_IPVX (&u4SrcAddress,
                                                pSSMMappedGrp->SrcAddr[u4Index],
                                                IPVX_ADDR_FMLY_IPV4);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddress);
                        CliPrintf (CliHandle, "%s", pu1String);
                        u1Flag = IGMP_TRUE;
                    }
                    else
                    {
                        IGMP_IP_COPY_FROM_IPVX (&u4SrcAddress,
                                                pSSMMappedGrp->SrcAddr[u4Index],
                                                IPVX_ADDR_FMLY_IPV4);
                        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcAddress);
                        CliPrintf (CliHandle, ", %s", pu1String);
                    }
                }
            }
        }
        else
        {
            CliPrintf (CliHandle,
                       "%% Entry not present in IGMP SSM Mapping table \r\n");
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpCliShowSSMMapGroup()\r\n");
    return CLI_SUCCESS;
}

INT4
IgmpCliValidatePointers (UINT4 u4Port, tIgmpIPvXAddr u4GrpAddr,
                         tIgmpIPvXAddr u4SrcAddr)
{
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpSource        *pSrc = NULL;

    pIfNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);
    if (pIfNode == NULL)
    {
        return IGMP_FAILURE;
    }

    if (IGMP_IPVX_ADDR_COMPARE (u4GrpAddr, gIPvXZero) == IGMP_ZERO)
    {
        return IGMP_SUCCESS;
    }

    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));
    IgmpGrpEntry.u4IfIndex = pIfNode->u4IfIndex;
    IgmpGrpEntry.pIfNode = pIfNode;
    IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &u4GrpAddr);

    pGrp = (tIgmpGroup *) RBTreeGet (gIgmpGroup, (tRBElem *) & IgmpGrpEntry);
    if (pGrp == NULL)
    {
        return IGMP_FAILURE;
    }

    if (IGMP_IPVX_ADDR_COMPARE (u4SrcAddr, gIPvXZero) == IGMP_ZERO)
    {
        return IGMP_SUCCESS;
    }

    TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress, u4SrcAddr) == IGMP_ZERO)
        {
            break;
        }
    }

    if (pSrc == NULL)
    {
        return IGMP_FAILURE;
    }

    return IGMP_SUCCESS;
}

#endif
