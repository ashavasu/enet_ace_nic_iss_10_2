/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpdbg.c,v 1.6 2015/03/25 14:25:22 siva Exp $
 *
 * Description:This file contains debug routines for IGMP
 *
 *******************************************************************/

#include "igmpinc.h"
#include "time.h"


#define IGMP_MAX_STATUS_STR_LEN 5

/* This flag is used to enable dumping of Igmp packets */
static UINT4        gu4IgmpPktDumpEnable = FALSE;

/*****************************************************************************/
/*                                                                           */
/* Function     : DbgDumpIgmpPkt                                              */
/*                                                                           */
/* Description  : Dumps the contents of the Igmp packet (CRU buffer)         */
/*                                                                           */
/* Input        : pBuf       : pointer to the buffer to be dumped            */
/*                u2Len      : length of the information to be dumped        */
/*                u1Type     : packet type                                   */
/*                u1PktDirection : flag indicating outgoing or               */
/*                                 incoming message                          */
/*               u4SrcAddr   : source address                                */
/*               u4DestAddr  : Destination address                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

#ifdef __STDC__
PUBLIC VOID
DbgDumpIgmpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, UINT1 u1Type,
                UINT1 u1PktDirection, tIgmpIPvXAddr u4SrcAddr,
                tIgmpIPvXAddr u4DestAddr)
#else
PUBLIC VOID
DbgDumpIgmpPkt (pBuf, u2Len, u1Type, u1PktDirection, u4SrcAddr, u4DestAddr)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT2               u2Len;
     UINT1               u1Type;
     UINT1               u1PktDirection;
     tIgmpIPvXAddr       u4SrcAddr;
     tIgmpIPvXAddr       u4DestAddr;
#endif
{
    UINT1               u1Temp;
    INT4                i4ReadOffset;
    UINT2               u2TotalLen;

    if (gu4IgmpPktDumpEnable != TRUE)
    {
        return;
    }

    if (u1PktDirection == IGMP_PKT_FLOW_IN)
    {
        PRINTF ("\n Incoming ");
    }
    else
    {
        PRINTF ("\n Outgoing ");
    }

    switch (u1Type)
    {
        case IGMP_PACKET_V1_REPORT:
            printf (" IGMP V1 Report ");
            break;

        case IGMP_PACKET_V2_REPORT:
            printf (" IGMP V2 Report ");
            break;

        case IGMP_PACKET_LEAVE:
            printf (" Leave Group Msg ");
            break;

        case IGMP_PACKET_QUERY:
            printf (" Query Packet ");
            break;

        default:
            printf (" unknown packet ");
            break;
    }
    PRINTF (" 0x%s  -> 0x%s \n",
            IgmpPrintIPvxAddress (u4SrcAddr),
            IgmpPrintIPvxAddress (u4DestAddr));
    u2TotalLen = u2Len;

    CB_READ_OFFSET (pBuf) = IGMP_ZERO;
    i4ReadOffset = IGMP_ZERO;
    while (u2Len--)
    {
        GET_1_BYTE (pBuf, i4ReadOffset, u1Temp);
        CB_READ_OFFSET (pBuf) += IGMP_ONE;
        PRINTF ("%02x ", u1Temp);
        if (((u2TotalLen - u2Len) % IGMP_V1V2_QUERY_LEN) == IGMP_ZERO)
        {
            PRINTF ("\n");
        }
        i4ReadOffset++;
    }
}


/*---------------------------------------------------------------------------*/
/*                          End of file igmpdbg.c                            */
/*---------------------------------------------------------------------------*/
