/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: mldnpwr.c,v 1.2 2015/02/13 11:13:52 siva Exp $
 *
 * Description: This file contains the NP wrappers for MLD module.
 *****************************************************************************/

#ifndef __MLD_NPWR_C__
#define __MLD_NPWR_C__
#include "igmpinc.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : MldNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMldNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
MldNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldNpModInfo      *pMldNpModInfo = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldNpWrHwProgram()\r \n");
    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMldNpModInfo = &(pFsHwNp->MldNpModInfo);

    if (NULL == pMldNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MLD_HW_ENABLE_MLD:
        {
            u1RetVal = FsMldHwEnableMld ();
            break;
        }
        case FS_MLD_HW_DISABLE_MLD:
        {
            u1RetVal = FsMldHwDisableMld ();
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_FILTER:
        {
            u1RetVal = FsMldHwDestroyMLDFilter ();
            break;
        }
        case FS_MLD_HW_DESTROY_M_L_D_F_P:
        {
            u1RetVal = FsMldHwDestroyMLDFP ();
            break;
        }
        case FS_NP_IPV6_SET_MLD_IFACE_JOIN_RATE:
        {
            tMldNpWrFsNpIpv6SetMldIfaceJoinRate *pEntry = NULL;
            pEntry = &pMldNpModInfo->MldNpFsNpIpv6SetMldIfaceJoinRate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv6SetMldIfaceJoinRate (pEntry->i4IfIndex,
                                             pEntry->i4RateLimit);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MLD_MBSM_HW_ENABLE_MLD:
        {
            tMldNpWrFsMldMbsmHwEnableMld *pEntry = NULL;
            pEntry = &pMldNpModInfo->MldNpFsMldMbsmHwEnableMld;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsMldMbsmHwEnableMld (pEntry->pSlotInfo);
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering MldNpWrHwProgram()\r \n");
    return (u1RetVal);
}
#endif
