/******************************************************************************/
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsigmget.c,v 1.18 2017/02/06 10:45:27 siva Exp $
 *
 * Description: : Action routines for set/get objects in           
 *                           fsigmp.mib                           
 *
 *******************************************************************/

# include  "include.h"
# include  "fsigmcon.h"
# include  "fsigmogi.h"
# include  "midconst.h"
# include  "igmpinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsIgmpGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpGlobalStatus (INT4 *pi4RetValFsIgmpGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGlobalStatus (pi4RetValFsIgmpGlobalStatus,
                                              IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpTraceLevel
 Input       :  The Indices

                The Object 
                retValFsIgmpTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpTraceLevel (INT4 *pi4RetValFsIgmpTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpTraceLevel (pi4RetValFsIgmpTraceLevel,
                                            IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpDebugLevel
 Input       :  The Indices

                The Object
                retValFsIgmpDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpDebugLevel (INT4 *pi4RetValFsIgmpDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpDebugLevel (pi4RetValFsIgmpDebugLevel,
                                            IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}


/* LOW LEVEL Routines for Table : FsIgmpInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIgmpInterfaceTable (INT4 i4FsIgmpInterfaceIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpInterfaceTable
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIgmpInterfaceTable (INT4 *pi4FsIgmpInterfaceIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4FsIgmpInterfaceAddrType = IPVX_ADDR_FMLY_IPV4;

    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable
        (pi4FsIgmpInterfaceIfIndex, &i4FsIgmpInterfaceAddrType);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIndex
                nextIgmpInterfaceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIgmpInterfaceTable (INT4 i4FsIgmpInterfaceIfIndex,
                                     INT4 *pi4NextFsIgmpInterfaceIfIndex)
{
    INT4                i4FsIgmpInterfaceAddrType = IGMP_ZERO;
    INT4                i4NextIndex = IGMP_ZERO;
    INT4                i4CurAddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1IsTrue = 1;

    while (u1IsTrue)
    {
        i1Status =
            IgmpMgmtUtilNmhGetNextIndexFsIgmpInterfaceTable
            (i4FsIgmpInterfaceIfIndex, i4CurAddrType, &i4NextIndex,
             &i4FsIgmpInterfaceAddrType);

        if (i1Status == SNMP_FAILURE ||
            i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            break;
        }
        i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
        i4FsIgmpInterfaceIfIndex = i4NextIndex;
    }
    *pi4NextFsIgmpInterfaceIfIndex = i4NextIndex;
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceAdminStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceAdminStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 *pi4RetValFsIgmpInterfaceAdminStatus)
{

    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus (i4FsIgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pi4RetValFsIgmpInterfaceAdminStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceFastLeaveStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceFastLeaveStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceFastLeaveStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                      INT4
                                      *pi4RetValFsIgmpInterfaceFastLeaveStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pi4RetValFsIgmpInterfaceFastLeaveStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceOperStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceOperStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                 INT4 *pi4RetValFsIgmpInterfaceOperStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceOperStatus (i4FsIgmpInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     pi4RetValFsIgmpInterfaceOperStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceIncomingPkts
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceIncomingPkts (INT4 i4FsIgmpInterfaceIfIndex,
                                   UINT4 *pu4RetValFsIgmpInterfaceIncomingPkts)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingPkts (i4FsIgmpInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV4,
                                                       pu4RetValFsIgmpInterfaceIncomingPkts);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceIncomingJoins
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceIncomingJoins (INT4 i4FsIgmpInterfaceIfIndex,
                                    UINT4
                                    *pu4RetValFsIgmpInterfaceIncomingJoins)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingJoins
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceIncomingJoins);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceIncomingLeaves
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingLeaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceIncomingLeaves (INT4 i4FsIgmpInterfaceIfIndex,
                                     UINT4
                                     *pu4RetValFsIgmpInterfaceIncomingLeaves)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingLeaves
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceIncomingLeaves);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceIncomingQueries
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceIncomingQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceIncomingQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIgmpInterfaceIncomingQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingQueries
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceIncomingQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceOutgoingQueries
 Input       :  The Indices
                IgmpInterfaceIndex

                The Object 
                retValFsIgmpInterfaceOutgoingQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceOutgoingQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIgmpInterfaceOutgoingQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceOutgoingQueries
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceOutgoingQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceRxGenQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxGenQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceRxGenQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   UINT4 *pu4RetValFsIgmpInterfaceRxGenQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGenQueries (i4FsIgmpInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV4,
                                                       pu4RetValFsIgmpInterfaceRxGenQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceRxGrpQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxGrpQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceRxGrpQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   UINT4 *pu4RetValFsIgmpInterfaceRxGrpQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpQueries (i4FsIgmpInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV4,
                                                       pu4RetValFsIgmpInterfaceRxGrpQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceRxGrpAndSrcQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxGrpAndSrcQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceRxGrpAndSrcQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValFsIgmpInterfaceRxGrpAndSrcQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpAndSrcQueries
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceRxGrpAndSrcQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceRxv1v2Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxv1v2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceRxv1v2Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                    UINT4
                                    *pu4RetValFsIgmpInterfaceRxv1v2Reports)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceRxv1v2Reports);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceRxv3Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceRxv3Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceRxv3Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                  UINT4 *pu4RetValFsIgmpInterfaceRxv3Reports)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports (i4FsIgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pu4RetValFsIgmpInterfaceRxv3Reports);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceTxGenQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxGenQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceTxGenQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   UINT4 *pu4RetValFsIgmpInterfaceTxGenQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGenQueries (i4FsIgmpInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV4,
                                                       pu4RetValFsIgmpInterfaceTxGenQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceTxGrpQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxGrpQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceTxGrpQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                   UINT4 *pu4RetValFsIgmpInterfaceTxGrpQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpQueries (i4FsIgmpInterfaceIfIndex,
                                                       IPVX_ADDR_FMLY_IPV4,
                                                       pu4RetValFsIgmpInterfaceTxGrpQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceTxGrpAndSrcQueries
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxGrpAndSrcQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceTxGrpAndSrcQueries (INT4 i4FsIgmpInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValFsIgmpInterfaceTxGrpAndSrcQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpAndSrcQueries
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceTxGrpAndSrcQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceTxv1v2Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv1v2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceTxv1v2Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                    UINT4
                                    *pu4RetValFsIgmpInterfaceTxv1v2Reports)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceTxv1v2Reports);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceTxv3Reports
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv3Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceTxv3Reports (INT4 i4FsIgmpInterfaceIfIndex,
                                  UINT4 *pu4RetValFsIgmpInterfaceTxv3Reports)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports (i4FsIgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pu4RetValFsIgmpInterfaceTxv3Reports);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceTxv2Leaves
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceTxv2Leaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceTxv2Leaves (INT4 i4FsIgmpInterfaceIfIndex,
                                 UINT4 *pu4RetValFsIgmpInterfaceTxv2Leaves)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv2Leaves (i4FsIgmpInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     pu4RetValFsIgmpInterfaceTxv2Leaves);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceGroupListId
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceGroupListId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceGroupListId (INT4 i4FsIgmpInterfaceIfIndex,
                                  UINT4 *pu4RetValFsIgmpInterfaceGroupListId)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceGroupListId (i4FsIgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pu4RetValFsIgmpInterfaceGroupListId);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceLimit
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceLimit (INT4 i4FsIgmpInterfaceIfIndex,
                            UINT4 *pu4RetValFsIgmpInterfaceLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceLimit (i4FsIgmpInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                pu4RetValFsIgmpInterfaceLimit);

    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceCurGrpCount
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceCurGrpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceCurGrpCount (INT4 i4FsIgmpInterfaceIfIndex,
                                  UINT4 *pu4RetValFsIgmpInterfaceCurGrpCount)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceCurGrpCount (i4FsIgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pu4RetValFsIgmpInterfaceCurGrpCount);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceCKSumError
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceCKSumError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceCKSumError (INT4 i4FsIgmpInterfaceIfIndex,
                                 UINT4 *pu4RetValFsIgmpInterfaceCKSumError)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceCKSumError (i4FsIgmpInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     pu4RetValFsIgmpInterfaceCKSumError);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfacePktLenError
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfacePktLenError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfacePktLenError (INT4 i4FsIgmpInterfaceIfIndex,
                                  UINT4 *pu4RetValFsIgmpInterfacePktLenError)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfacePktLenError (i4FsIgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pu4RetValFsIgmpInterfacePktLenError);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfacePktsWithLocalIP
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfacePktsWithLocalIP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfacePktsWithLocalIP (INT4 i4FsIgmpInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIgmpInterfacePktsWithLocalIP)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfacePktsWithLocalIP
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfacePktsWithLocalIP);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceSubnetCheckFailure
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceSubnetCheckFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceSubnetCheckFailure (INT4 i4FsIgmpInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValFsIgmpInterfaceSubnetCheckFailure)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceSubnetCheckFailure
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceSubnetCheckFailure);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceQryFromNonQuerier
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceQryFromNonQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceQryFromNonQuerier (INT4 i4FsIgmpInterfaceIfIndex,
                                        UINT4
                                        *pu4RetValFsIgmpInterfaceQryFromNonQuerier)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceQryFromNonQuerier
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceQryFromNonQuerier);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceReportVersionMisMatch
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceReportVersionMisMatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceReportVersionMisMatch (INT4 i4FsIgmpInterfaceIfIndex,
                                            UINT4
                                            *pu4RetValFsIgmpInterfaceReportVersionMisMatch)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceReportVersionMisMatch);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceQryVersionMisMatch
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceQryVersionMisMatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceQryVersionMisMatch (INT4 i4FsIgmpInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValFsIgmpInterfaceQryVersionMisMatch)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceQryVersionMisMatch
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceQryVersionMisMatch);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceUnknownMsgType
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceUnknownMsgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceUnknownMsgType (INT4 i4FsIgmpInterfaceIfIndex,
                                     UINT4
                                     *pu4RetValFsIgmpInterfaceUnknownMsgType)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceUnknownMsgType
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceUnknownMsgType);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceInvalidV1Report
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceInvalidV1Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceInvalidV1Report (INT4 i4FsIgmpInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIgmpInterfaceInvalidV1Report)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV1Report
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceInvalidV1Report);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceInvalidV2Report
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceInvalidV2Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceInvalidV2Report (INT4 i4FsIgmpInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIgmpInterfaceInvalidV2Report)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV2Report
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceInvalidV2Report);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceInvalidV3Report
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceInvalidV3Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceInvalidV3Report (INT4 i4FsIgmpInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValFsIgmpInterfaceInvalidV3Report)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV3Report
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceInvalidV3Report);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceRouterAlertCheckFailure
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object
                retValFsIgmpInterfaceRouterAlertCheckFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceRouterAlertCheckFailure (INT4 i4FsIgmpInterfaceIfIndex,
                                              UINT4
                                              *pu4RetValFsIgmpInterfaceRouterAlertCheckFailure)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRouterAlertCheckFailure
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceRouterAlertCheckFailure);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceChannelTrackStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceChannelTrackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpInterfaceChannelTrackStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                         INT4
                                         *pi4RetValFsIgmpInterfaceChannelTrackStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceChannelTrackStatus
        (i4FsIgmpInterfaceIfIndex,
         IPVX_ADDR_FMLY_IPV4, pi4RetValFsIgmpInterfaceChannelTrackStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceIncomingSSMPkts
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceIncomingSSMPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIgmpInterfaceIncomingSSMPkts(INT4 i4FsIgmpInterfaceIfIndex , 
				     UINT4 
				     *pu4RetValFsIgmpInterfaceIncomingSSMPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingSSMPkts
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceIncomingSSMPkts);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceInvalidSSMPkts
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceInvalidSSMPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIgmpInterfaceInvalidSSMPkts(INT4 i4FsIgmpInterfaceIfIndex , 
				    UINT4 
				    *pu4RetValFsIgmpInterfaceInvalidSSMPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidSSMPkts
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValFsIgmpInterfaceInvalidSSMPkts);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpInterfaceJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                retValFsIgmpInterfaceJoinPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIgmpInterfaceJoinPktRate (INT4 i4FsIgmpInterfaceIfIndex , 
				  INT4 
				  *pi4RetValFsIgmpInterfaceJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceJoinPktRate
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pi4RetValFsIgmpInterfaceJoinPktRate);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpJoinPktRate
 Input       :  The Indices

                The Object 
                retValFsIgmpJoinPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIgmpJoinPktRate (INT4 
		         *pi4RetValFsIgmpJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpJoinPktRate
        (IPVX_ADDR_FMLY_IPV4,
         pi4RetValFsIgmpJoinPktRate);
    return i1Status;
}



/* LOW LEVEL Routines for Table : FsIgmpCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIgmpCacheTable (UINT4 u4IgmpCacheAddress,
                                          INT4 i4IgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable
        (IPVX_ADDR_FMLY_IPV4, IgmpCacheAddrX, i4IgmpCacheIfIndex);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIgmpCacheTable (UINT4 *pu4IgmpCacheAddress,
                                  INT4 *pi4IgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4IgmpCacheAddrType = IPVX_ADDR_FMLY_IPV4;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);

        i1Status =
        IgmpMgmtUtilNmhGetFirstIndexFsIgmpCacheTable (&i4IgmpCacheAddrType,
                                                         &IgmpCacheAddrX,
                                                         pi4IgmpCacheIfIndex);

    if ((i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4) && (i1Status == SNMP_SUCCESS))
        {
        IGMP_IP_COPY_FROM_IPVX (pu4IgmpCacheAddress, IgmpCacheAddrX,
                                IPVX_ADDR_FMLY_IPV4);
        return SNMP_SUCCESS;
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                nextIgmpCacheAddress
                IgmpCacheIfIndex
                nextIgmpCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIgmpCacheTable (UINT4 u4IgmpCacheAddress,
                                 UINT4 *pu4NextIgmpCacheAddress,
                                 INT4 i4IgmpCacheIfIndex,
                                 INT4 *pi4NextIgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_SUCCESS;
    INT4                IgmpCacheNextAddrType = IGMP_ZERO;
    INT4                i4CurAddrType = IPVX_ADDR_FMLY_IPV4;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    while (i1Status == SNMP_SUCCESS)
    {
        i1Status =
            IgmpMgmtUtilNmhGetNextIndexFsIgmpCacheTable (i4CurAddrType,
                                                         &IgmpCacheNextAddrType,
                                                         IgmpCacheAddrX,
                                                         &IgmpCacheAddrX,
                                                         i4IgmpCacheIfIndex,
                                                         pi4NextIgmpCacheIfIndex);
        if ((i1Status == SNMP_SUCCESS) &&
            (IgmpCacheNextAddrType == IPVX_ADDR_FMLY_IPV4))
        {
            IGMP_IP_COPY_FROM_IPVX (pu4NextIgmpCacheAddress,
                                    IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4);
            break;
        }
        i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
        i4IgmpCacheIfIndex = *pi4NextIgmpCacheIfIndex;
    }

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpCacheGroupCompMode
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValFsIgmpCacheGroupCompMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpCacheGroupCompMode (UINT4 u4IgmpCacheAddress,
                                INT4 i4IgmpCacheIfIndex,
                                INT4 *pi4RetValFsIgmpCacheGroupCompMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpCacheGroupCompMode (IPVX_ADDR_FMLY_IPV4,
                                                    IgmpCacheAddrX,
                                                    i4IgmpCacheIfIndex,
                                                    pi4RetValFsIgmpCacheGroupCompMode);

    return i1Status;
}

/* LOW LEVEL Routines for Table : FsIgmpGroupListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIgmpGroupListTable
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsIgmpGroupListTable (UINT4 u4FsIgmpGrpListId,
                                              UINT4 u4FsIgmpGrpIP,
                                              UINT4 u4FsIgmpGrpPrefixLen)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpGroupListTable
        (u4FsIgmpGrpListId, u4FsIgmpGrpIP, u4FsIgmpGrpPrefixLen,
         IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpGroupListTable
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIgmpGroupListTable (UINT4 *pu4FsIgmpGrpListId,
                                      UINT4 *pu4FsIgmpGrpIP,
                                      UINT4 *pu4FsIgmpGrpPrefixLen)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexFsIgmpGroupListTable (pu4FsIgmpGrpListId,
                                                          pu4FsIgmpGrpIP,
                                                          pu4FsIgmpGrpPrefixLen,
                                                          IPVX_ADDR_FMLY_IPV4);

    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpGroupListTable
 Input       :  The Indices
                FsIgmpGrpListId
                nextFsIgmpGrpListId
                FsIgmpGrpIP
                nextFsIgmpGrpIP
                FsIgmpGrpPrefixLen
                nextFsIgmpGrpPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIgmpGroupListTable (UINT4 u4FsIgmpGrpListId,
                                     UINT4 *pu4NextFsIgmpGrpListId,
                                     UINT4 u4FsIgmpGrpIP,
                                     UINT4 *pu4NextFsIgmpGrpIP,
                                     UINT4 u4FsIgmpGrpPrefixLen,
                                     UINT4 *pu4NextFsIgmpGrpPrefixLen)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetNextIndexFsIgmpGroupListTable (u4FsIgmpGrpListId,
                                                         pu4NextFsIgmpGrpListId,
                                                         u4FsIgmpGrpIP,
                                                         pu4NextFsIgmpGrpIP,
                                                         u4FsIgmpGrpPrefixLen,
                                                         pu4NextFsIgmpGrpPrefixLen,
                                                         IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpGrpListRowStatus
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen

                The Object 
                retValFsIgmpGrpListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpGrpListRowStatus (UINT4 u4FsIgmpGrpListId, UINT4 u4FsIgmpGrpIP,
                              UINT4 u4FsIgmpGrpPrefixLen,
                              INT4 *pi4RetValFsIgmpGrpListRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGrpListRowStatus (u4FsIgmpGrpListId,
                                                  u4FsIgmpGrpIP,
                                                  u4FsIgmpGrpPrefixLen,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  pi4RetValFsIgmpGrpListRowStatus);

    return i1Status;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpGlobalLimit
 Input       :  The Indices

                The Object 
                retValFsIgmpGlobalLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpGlobalLimit (UINT4 *pu4RetValFsIgmpGlobalLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGlobalLimit (pu4RetValFsIgmpGlobalLimit,
                                             IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpGlobalCurGrpCount
 Input       :  The Indices

                The Object 
                retValFsIgmpGlobalCurGrpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpGlobalCurGrpCount (UINT4 *pu4RetValFsIgmpGlobalCurGrpCount)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGlobalCurGrpCount
        (pu4RetValFsIgmpGlobalCurGrpCount, IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}


/****************************************************************************
 Function    :  nmhGetFsIgmpSSMMapStatus
 Input       :  The Indices

                The Object 
                retValFsIgmpSSMMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIgmpSSMMapStatus(INT4 *pi4RetValFsIgmpSSMMapStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpSSMMapStatus (pi4RetValFsIgmpSSMMapStatus,
                                              IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}


/* LOW LEVEL Routines for Table : FsIgmpSSMMapGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIgmpSSMMapGroupTable
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
nmhValidateIndexInstanceFsIgmpSSMMapGroupTable
                    (UINT4 u4FsIgmpSSMMapStartGrpAddress , 
                     UINT4 u4FsIgmpSSMMapEndGrpAddress , 
                     UINT4 u4FsIgmpSSMMapSourceAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable
        (u4FsIgmpSSMMapStartGrpAddress, u4FsIgmpSSMMapEndGrpAddress, u4FsIgmpSSMMapSourceAddress);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpSSMMapGroupTable
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
nmhGetFirstIndexFsIgmpSSMMapGroupTable
                    (UINT4 *pu4FsIgmpSSMMapStartGrpAddress , 
                     UINT4 *pu4FsIgmpSSMMapEndGrpAddress , 
                     UINT4 *pu4FsIgmpSSMMapSourceAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable (pu4FsIgmpSSMMapStartGrpAddress,
                pu4FsIgmpSSMMapEndGrpAddress, pu4FsIgmpSSMMapSourceAddress);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpSSMMapGroupTable
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                nextFsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                nextFsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress
                nextFsIgmpSSMMapSourceAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
nmhGetNextIndexFsIgmpSSMMapGroupTable
                    (UINT4 u4FsIgmpSSMMapStartGrpAddress ,
                     UINT4 *pu4NextFsIgmpSSMMapStartGrpAddress  , 
                     UINT4 u4FsIgmpSSMMapEndGrpAddress ,
                     UINT4 *pu4NextFsIgmpSSMMapEndGrpAddress  , 
                     UINT4 u4FsIgmpSSMMapSourceAddress ,
                     UINT4 *pu4NextFsIgmpSSMMapSourceAddress )
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable (u4FsIgmpSSMMapStartGrpAddress,
                pu4NextFsIgmpSSMMapStartGrpAddress,
                u4FsIgmpSSMMapEndGrpAddress,
                pu4NextFsIgmpSSMMapEndGrpAddress,
                u4FsIgmpSSMMapSourceAddress,
                pu4NextFsIgmpSSMMapSourceAddress);

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpSSMMapRowStatus
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress

                The Object 
                retValFsIgmpSSMMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIgmpSSMMapRowStatus (UINT4 u4FsIgmpSSMMapStartGrpAddress , 
                            UINT4 u4FsIgmpSSMMapEndGrpAddress , 
                            UINT4 u4FsIgmpSSMMapSourceAddress , 
                            INT4 *pi4RetValFsIgmpSSMMapRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpSSMMapRowStatus (u4FsIgmpSSMMapStartGrpAddress,
                u4FsIgmpSSMMapEndGrpAddress,
                u4FsIgmpSSMMapSourceAddress,
                pi4RetValFsIgmpSSMMapRowStatus);

    return i1Status;
}
