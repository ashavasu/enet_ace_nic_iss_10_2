/*****************************************************************************/
/*    FILE  NAME            : igpstb.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP                                           */
/*    MODULE NAME           : IGMP Proxy Stub files                          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains routines related to         */
/*                            updating the forwarding entries                */
/* $Id: igpstb.c,v 1.4 2010/12/20 12:23:56 siva Exp $                     */
/*---------------------------------------------------------------------------*/

#include "igmpinc.h"

/*****************************************************************************/
/* Function Name      : IgpUtilCheckUpIface                                  */
/*                                                                           */
/* Description        : This function will check whether the interface is    */
/*                      configured as upstream interface                     */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interfcae Index                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_TRUE/IGMP_FALSE                                 */
/*****************************************************************************/
UINT1
IgpUtilCheckUpIface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return IGMP_FALSE;
}

/*****************************************************************************/
/* Function Name      : IgpMainHandleProxyEvents                             */
/*                                                                           */
/* Description        : This function is used to handle all the events       */
/*                      related to IGMP Proxy implementation                 */
/*                                                                           */
/* Input(s)           : u4Event                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IgpMainHandleProxyEvents (UINT4 u4Event)
{
    UNUSED_PARAM (u4Event);
    return;
}

/*****************************************************************************/
/* Function Name      : IgpMainStatusDisable                                 */
/*                                                                           */
/* Description        : This function will disable IGMP Proxy functionality  */
/*                      in the router                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
VOID
IgpMainStatusDisable (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : IgpPortHandleMcastDataPkt                            */
/*                                                                           */
/* Description        : This function will post a multicast data packet      */
/*                      receive event to the IGMP task                       */
/*                                                                           */
/* Input(s)           : *pBuffer - Pointer to the packet buffer              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpPortHandleMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    /* Free the CRU buffer */
    CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
    return;
}

/*****************************************************************************/
/* Function Name      : IgpGrpUpdateGroupTable                               */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      membership data base                                 */
/*                                                                           */
/* Input(s)           : pIgmpGroupEntry - Pointer to the group entry         */
/*                      u1Version  - IGMP version of report received         */
/*                      u1NewInterface  - Indicates the join has come on a   */
/*                      a new IGMP capable interface                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpGrpUpdateGroupTable (tIgmpGroup * pIgmpGroupEntry, UINT1 u1Version,
                        UINT1 u1NewInterface)
{
    UNUSED_PARAM (pIgmpGroupEntry);
    UNUSED_PARAM (u1Version);
    UNUSED_PARAM (u1NewInterface);
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpGrpConsolidate                                    */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      membership data base and for a single group and also */
/*                      updates the source bitmap of per interface IGMP      */
/*                      group entry                                          */
/*                                                                           */
/* Input(s)           : pIgmpGroupEntry - Pointer to the group entry         */
/*                      u1Version  - IGMP version of report received         */
/*                      u1GrpIfaceStat  - Indicates the status of the group  */
/*                      on the IGMP capable interface                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Nothing                                              */
/*****************************************************************************/
VOID
IgpGrpConsolidate (tIgmpGroup * pIgmpGroupEntry, UINT1 u1Version,
                   UINT1 u1GrpIfaceStat)
{
    UNUSED_PARAM (pIgmpGroupEntry);
    UNUSED_PARAM (u1Version);
    UNUSED_PARAM (u1GrpIfaceStat);
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfFormAndSendV3Report                           */
/*                                                                           */
/* Description        : This function will encode and send the v3 report to  */
/*                      the upstream                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Global(s)          : gpv3RepSendBuffer - Global buffer for keeping the v3 */
/*                                          report to be sent                */
/*                      gIgmpProxyInfo.u2PktOffset - Offset on               */
/*                                                   gpv3RepSendBuffer       */
/*                      gIgmpProxyInfo.u1NoOfGrpRecords - No of group        */
/*                                            records in the current packet  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfFormAndSendV3Report (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUtilSendQuerierTrap                               */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when a query with lower IP address is received on    */
/*                      downstream interface                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u4QuerierAddr - Address of the querier which has     */
/*                                      sent a query                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUtilSendQuerierTrap (UINT4 u4Port, tIgmpIPvXAddr u4QuerierAddr)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4QuerierAddr);
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfProcessQuery                                  */
/*                                                                           */
/* Description        : This function will process upstream queries and send */
/*                      reports accordingly                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index of upstream router       */
/*                      u4GrpAddress - IP address of the group               */
/*                      u4NoOfSrcs - Number of sources in the query          */
/*                      pu4Sources - List of IP address of sources           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfProcessQuery (UINT4 u4IfIndex, tIgmpIPvXAddr u4GrpAddress,
                     UINT4 u4NoOfSrcs, UINT4 *pu4Sources, UINT1 u1Version)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4GrpAddress);
    UNUSED_PARAM (u4NoOfSrcs);
    UNUSED_PARAM (pu4Sources);
    UNUSED_PARAM (u1Version);
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfDeleteUpIfaceEntry                            */
/*                                                                           */
/* Description        : This function deletes the upstream interface entry   */
/*                                                                           */
/* Input(s)           : u4IfIndex        - Upstream interface index          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpUpIfDeleteUpIfaceEntry (UINT4 u4IfIndex, UINT1 u1Flag)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Flag);
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfUpdateUpIface                                 */
/*                                                                           */
/* Description        : This function will update the Upstream interface     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUpIfUpdateUpIface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUtilRegisterFsIgpMib                              */
/*                                                                           */
/* Description        : This function will register the fsigp.mib            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUtilRegisterFsIgpMib ()
{
    return;
}

#ifdef MFWD_WANTED
/****************************************************************************/
/* Function Name         : IgpMfwdAddIface                                  */
/*                                                                          */
/* Description           : This function creates a update buffer for adding */
/*                         an interface to MFWD                             */
/*                                                                          */
/* Input (s)             : u4IfIndex - Interface Index                      */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : IGMP_SUCCESS/IGMP_FAILURE                        */
/****************************************************************************/
INT4
IgpMfwdAddInterface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return IGMP_SUCCESS;
}

/****************************************************************************/
/* Function Name         : IgpMfwdDelInterface                              */
/*                                                                          */
/* Description           : This function Deletes the specified interface    */
/*                         from the MFWD                                    */
/*                                                                          */
/* Input (s)             : u4IfIndex - Interface Index                      */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : None                                             */
/****************************************************************************/
INT4
IgpMfwdDelInterface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return IGMP_SUCCESS;
}
#endif

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : IgpFwdHandleMbsmEvent                                */
/*                                                                           */
/* Description        : This function will add all the multicast forwarding  */
/*                      entries in the new line card inserted                */
/*                                                                           */
/* Input(s)           : pSlotInfo - pointer to the Line card information     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpFwdHandleMbsmEvent (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return IGMP_SUCCESS;
}
#endif
