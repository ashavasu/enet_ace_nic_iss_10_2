/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpsrc.c,v 1.33 2017/02/06 10:45:28 siva Exp $
 *
 * Description:This file contains the routines required for
 *                Group Membership Module                          
 *
 *******************************************************************/

#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE ;
#endif
extern UINT4       gu4MaxIgmpGrps;
extern UINT4       gu4MaxMldGrps;

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpSourceLookup                              */
/*                                                                         */
/*     Description   :     This function checks for the existance of       */
/*                         a given source in the list of sources           */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Pointer to the multicast interface               */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpSourceLookup                            **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
tIgmpSource        *
IgmpSourceLookup (tIgmpGroup * pGrp, tIgmpIPvXAddr u4SrcAddress,
                  tIgmpIPvXAddr u4Reporter)
{
    tIgmpSource        *pSrc = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			 "Entering the funtion IgmpSourceLookup.\n");
    TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress, u4SrcAddress) ==
            IGMP_ZERO)
		{
                        IGMP_IPVX_ADDR_COPY (&(pSrc->u4LastReporter), &u4Reporter);
			if (IgmpAddV3Reporter(pSrc, u4Reporter) == IGMP_NOT_OK)
			{
				IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
						"Failure in adding Reporter node %s to Source %s for group %s\n",
						IgmpPrintIPvxAddress (u4Reporter),IgmpPrintIPvxAddress (u4SrcAddress),
						IgmpPrintIPvxAddress (pGrp->u4Group));
			}
			return pSrc;
		}
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpSourceLookup.\n");
    return NULL;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpCreateSource                              */
/*                                                                         */
/*     Description   :     This function creates a new source node         */
/*                         and initialises it with given address           */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Source address for which the node is created     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpCreateSource                            **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
tIgmpSource        *
IgmpCreateSource (tIgmpGroup * pGrp, tIgmpIPvXAddr u4SrcAddress,
                  tIgmpIPvXAddr u4Reporter)
{
    tIgmpSource        *pSrc = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE), 
		"Entering the funtion IgmpCreateSource.\n");

    if ((pGrp->u4Group.u1AddrLen == IPVX_IPV4_ADDR_LEN) && 
        (gu4MaxIgmpGrps < MAX_IGMP_MCAST_SRCS))
    {
        IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSourceId, pSrc, tIgmpSource);
    }
    else if ((pGrp->u4Group.u1AddrLen == IPVX_IPV6_ADDR_LEN) && 
             (gu4MaxMldGrps < MAX_MLD_MCAST_SRCS))
    {
        IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSourceId, pSrc, tIgmpSource);
    }
    if (pSrc == NULL)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG,IGMP_OS_RES_MODULE , 
		       IgmpGetModuleName(IGMP_OS_RES_MODULE),
                       "Failure in allocating Memory block for Source Node \n");
	SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,IGMP_OS_RES_MODULE,
                              IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"for source Node address %s\n",IgmpPrintIPvxAddress(u4SrcAddress));
        return NULL;
    }

    /* initialise the sll node */
    TMO_SLL_Init_Node (&pSrc->Link);
    pSrc->pGrp = pGrp;
    pSrc->u4IfIndex = pGrp->u4IfIndex;
    IGMP_IPVX_ADDR_COPY (&(pSrc->u4SrcAddress), &u4SrcAddress);
    IGMP_IPVX_ADDR_COPY (&(pSrc->u4GrpAddress), &(pGrp->u4Group));
    pSrc->u1EntryStatus = pGrp->pIfNode->u1EntryStatus;
    pSrc->u1FwdState = IGMP_FALSE;
    pSrc->u1UpdatedToMrp = IGMP_FALSE;
    pSrc->u1QryFlag = IGMP_FALSE;
    pSrc->u1StaticFlg = IGMP_FALSE;
    pSrc->u1DynamicFlg = IGMP_FALSE;
    pSrc->u1SrcModeFlag = IGMP_ADD_SOURCE;
    pSrc->u1DelSyncFlag = IGMP_TRUE;
    pSrc->u1DynSyncFlag = IGMP_TRUE;
    pSrc->u1SrcSSMMapped = IGMP_ZERO;
    pSrc->u1DynJoinFlag = IGMP_FALSE;
    IGMP_IPVX_ADDR_COPY (&(pSrc->u4LastReporter), &u4Reporter);
    /* Initialise the Source DB Node */
    IgmpRedDbNodeInit (&(pSrc->SrcDbNode), IGMP_RED_SRC_INFO);

	pSrc->SrcReporterTree  = RBTreeCreateEmbedded
		(FSAP_OFFSETOF (tIgmpSrcReporter, RbNode), IgmpRBTreeGrpSrcReporterEntryCmp);
	if(pSrc->SrcReporterTree == NULL)
	{
		IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
				"Failure in creating Source Reporter Tree for source %s\n",
				IgmpPrintIPvxAddress (u4SrcAddress));
		IgmpMemRelease (gIgmpMemPool.IgmpSourceId, (UINT1 *) pSrc);
		return NULL;
	}
	

	if (IgmpAddV3Reporter(pSrc, u4Reporter) == IGMP_NOT_OK)
	{
		IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
				"Failure in adding Reporter node %s to Source %s - source creation failed\n",
				IgmpPrintIPvxAddress (u4Reporter),
				IgmpPrintIPvxAddress (u4SrcAddress));
		IgmpMemRelease (gIgmpMemPool.IgmpSourceId, (UINT1 *) pSrc);
		return NULL;
	}
    
    if (pGrp->u4Group.u1AddrLen == IPVX_IPV4_ADDR_LEN)
    {
        gu4MaxIgmpGrps++;
    }
    else
    {
        gu4MaxMldGrps++;
    }

	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpCreateSource.\n");

    return pSrc;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpCheckSourceInList                         */
/*                                                                         */
/*     Description   :     This function creates a new source node         */
/*                         and initialises it with given address           */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Source address for which the node is created     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpCheckSourceInList                       **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpCheckSourceInList (tIgmpIPvXAddr u4SrcAddress, UINT4 u4NoOfSrcs,
                       tIgmpIPvXAddr * pList)
{
    UINT4               u4Index = IGMP_ZERO;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpCheckSourceInList.\n");

    for (u4Index = IGMP_ZERO; u4Index < u4NoOfSrcs; u4Index++)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pList[u4Index], u4SrcAddress) == IGMP_ZERO)
        {
            return TRUE;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpCheckSourceInList.\n");

    return FALSE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpCheckSourceInSchQueryList                 */
/*                                                                         */
/*     Description   :     This function creates a new source node         */
/*                         and initialises it with given address           */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Source address for which the node is created     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpCheckSourceInSchQueryList               **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpCheckSourceInSchQueryList (tIgmpIPvXAddr u4SrcAddress,
                               tIgmpSchQuery * pSchQry)
{
    tSourceNode        *pSrcNode = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpCheckSourceInschQueryList.\n");

    TMO_SLL_Scan (&pSchQry->SrcList, pSrcNode, tSourceNode *)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pSrcNode->u4Source, u4SrcAddress) ==
            IGMP_ZERO)
        {
            return TRUE;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpCheckSourceInschQueryList.\n");

    return FALSE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpCheckSourceInArray                        */
/*                                                                         */
/*     Description   :     This function creates a new source node         */
/*                         and initialises it with given address           */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Source address for which the node is created     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpCheckSourceInArray                       **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpCheckSourceInArray (tIgmpIPvXAddr u4SrcAddress, UINT1 *pu1Array,
                        UINT4 u4NumSrc)
{
    UINT4               u4Index = IGMP_ZERO;
    UINT4              *pu4Array = NULL;
    UINT4               u4TmpAddr = IGMP_ZERO;
    UINT4               u4SrcAddrPos = IGMP_ZERO;
    tIgmpIPvXAddr       ArrayAddr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpCheckSourceInArray.\n");
    MEMSET (&ArrayAddr, IGMP_ZERO, sizeof (tIgmpIPvXAddr));
    pu4Array = (UINT4 *) (VOID *) pu1Array;
    for (; u4Index < u4NumSrc; u4Index++)
    {
        if (u4SrcAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IPVX_ADDR_CLEAR (&ArrayAddr);
            u4TmpAddr = OSIX_NTOHL (pu4Array[u4Index]);
            IGMP_IPVX_ADDR_INIT_IPV4 (ArrayAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                                      (UINT1 *) &u4TmpAddr);

        }
        else if (u4SrcAddress.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            u4SrcAddrPos = (UINT2) (u4Index * (UINT1) IPVX_IPV6_ADDR_LEN);
            IPVX_ADDR_INIT_FROMV6 (ArrayAddr, (pu1Array + u4SrcAddrPos));
        }

        if (IGMP_IPVX_ADDR_COMPARE (u4SrcAddress, ArrayAddr) == IGMP_ZERO)
        {
            return TRUE;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpCheckSourceInArray.\n");

    return FALSE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpDeleteSource                              */
/*                                                                         */
/*     Description   :     This function creates a new source node         */
/*                         and initialises it with given address           */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Source address for which the node is created     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpDeleteSource                            **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpDeleteSource (tIgmpGroup * pGrp, tIgmpSource * pSrc)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpDeleteSource.\n");

    if ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pSrc->srcTimer);
    }

    /* Delete all the host reported to this source */
    IgmpDeleteAllSrcReporters (pSrc);
	if(pSrc->SrcReporterTree != NULL)
	{
		RBTreeDelete(pSrc->SrcReporterTree);
		pSrc->SrcReporterTree = NULL;
	}
    /* Delete src from SchQry */
    IgmpDelSchQrySrcNode (pSrc);

    /* yes, Delete and Release  the SourceNode */
    if (pSrc->u1DelSyncFlag == IGMP_TRUE)
    {
    	pSrc->u1SrcModeFlag = IGMP_DELETE_SOURCE;
    	IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
    	IgmpRedSyncDynInfo ();
    }
    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
    IgmpMemRelease (gIgmpMemPool.IgmpSourceId, (UINT1 *) pSrc);
    MGMD_DBG (IGMP_DBG_MGMT, "MemReleased - Source Node..\n");

    if (pGrp->u4Group.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        gu4MaxIgmpGrps--;
    }
    else
    {
        gu4MaxMldGrps--;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpDeleteSource.\n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpAddSource                                 */
/*                                                                         */
/*     Description   :     This function adds the source to the list of    */
/*                         sources in the multicast interface block of     */
/*                         a given group                                   */
/*                                                                         */
/*     Input(s)      :    Source node to be added into the list            */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpAddSource                               **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
tIgmpSource        *
IgmpAddSource (tIgmpGroup * pGrp, tIgmpIPvXAddr u4SrcAddress,
               tIgmpIPvXAddr u4Reporter, UINT1 u1ChkSrcZero)
{
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    tIgmpSource        *pSource = NULL;
    tIgmpSource        *pTempSrc = NULL;
    tIgmpIPvXAddr       u4SrcAddr;
    tIp6Addr            SrcIp6Addr;
    tTMO_SLL_NODE      *pPrev = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4TmpAddr = IGMP_ZERO;
    UINT4               u4Source = IGMP_ZERO;
    INT4                i4Status = IGMP_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpAddSource.\n");

    MEMSET (&u4SrcAddr, IGMP_ZERO, sizeof (tIgmpIPvXAddr));

    if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IGMP_IP_COPY_FROM_IPVX (&u4Source, u4SrcAddress,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        IGMP_IPVX_ADDR_CLEAR (&u4SrcAddr);
        u4TmpAddr = OSIX_NTOHL (u4Source);
		if(IgmpValidateV4SourceAddress (u4TmpAddr, u1ChkSrcZero) == IGMP_FAILURE)
		{
			IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
					"Invalid Source address. \n");
			return NULL;
		}
        IGMP_IPVX_ADDR_INIT_IPV4 (u4SrcAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4TmpAddr);

    }

    else if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        IGMP_IPVX_ADDR_COPY (&u4SrcAddr, &u4SrcAddress);
        MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
        MEMCPY (&SrcIp6Addr, u4SrcAddr.au1Addr, sizeof (tIp6Addr));
        if (IS_ADDR_MULTI (SrcIp6Addr) == TRUE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                      "Src address cant be a multicast address. \n");
            return NULL;
        }
    }
    if ((pSource = IgmpSourceLookup (pGrp, u4SrcAddr, u4Reporter)) != NULL)
    {
        return pSource;
    }

    if ((pSource = IgmpCreateSource (pGrp, u4SrcAddr, u4Reporter)) != NULL)
    {
        /* Sort the sources and add into the list */
        TMO_SLL_Scan (&pGrp->srcList, pLstNode, tTMO_SLL_NODE *)
        {
            pTempSrc = (tIgmpSource *) pLstNode;
            if (IGMP_IPVX_ADDR_COMPARE
                (pTempSrc->u4SrcAddress, pSource->u4SrcAddress) < IGMP_ZERO)
            {
                pPrev = pLstNode;
            }
            else if (IGMP_IPVX_ADDR_COMPARE
                     (pTempSrc->u4SrcAddress,
                      pSource->u4SrcAddress) > IGMP_ZERO)
            {
                break;
            }
        }
        TMO_SLL_Insert (&(pGrp->srcList), pPrev, &(pSource->Link));

        pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (pGrp->u4Group, &i4Status);
        if ((IGMP_TRUE == i4Status) && (NULL != pSSMMappedGrp))
        {
            pSource->u1SrcSSMMapped = pSource->u1SrcSSMMapped | IGMP_SRC_MAPPED;
        }

        return pSource;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpAddSource.\n");
    return NULL;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpGetSource                              */
/*                                                                         */
/*     Description   :     This function gets the source node for         */
/*                         the given Source Address                        */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Source address for which the node is created     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the Source node                     */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGetSource                               **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
tIgmpSource        *
IgmpGetSource (tIgmpGroup * pGrp, tIgmpIPvXAddr u4SrcAddress)
{
    tIgmpSource        *pSrc = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpGetSource.\n");

    TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress, u4SrcAddress) ==
            IGMP_ZERO)
        {
            break;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpGetSource.\n");

    return pSrc;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpDeleteAllSrcReporters                         */
/*                                                                         */
/*     Description   :   This function deletes all host reported for       */
/*                       this Source Address                               */
/*                                                                         */
/*     Input(s)      :   Source Node                                       */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :   NONE                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGetSource                               **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpDeleteAllSrcReporters (tIgmpSource * pSrc)
{
    tIgmpSrcReporter   *pDelReporter = NULL;
    tIgmpSrcReporter   *pNxtReporter = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpDeleteAllSources.\n");
    
	if(pSrc->SrcReporterTree == NULL)
	{
		return;
	}

	pDelReporter = (tIgmpSrcReporter *) RBTreeGetFirst (pSrc->SrcReporterTree);

    while(pDelReporter != NULL)
    {
        pNxtReporter = RBTreeGetNext (pSrc->SrcReporterTree, (tRBElem *) pDelReporter, NULL);
		pDelReporter->u1RepModeFlag = IGMP_DELETE_REPORTER;
		pDelReporter->u1GrpSrcFlag = IGMP_SOURCE_REPORTER;
		if (pSrc->u1DelSyncFlag == IGMP_TRUE)
		{
			IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
					&pDelReporter->SrcReporterDbNode);
			IgmpRedSyncDynInfo ();
		}
		RBTreeRemove (pSrc->SrcReporterTree, (tRBElem *) pDelReporter);
        IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId, (UINT1 *) pDelReporter);
        pDelReporter = pNxtReporter;
        /*sync not sent to standby as this func() is called only group delete*/
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpDeleteAllSources.\n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpISSourcePresentForGroup                     */
/*                                                                         */
/*     Description   :     This function checks for the existence of       */
/*                         the  sources for the particular group           */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Pointer to the multicast interface               */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpISSourcePresentForGroup                 **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
tIgmpSource        *
IgmpISSourcePresentForGroup (tIgmpGroup * pGrp)
{
    tIgmpSource        *pSrc = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering the funtion IgmpISSourcePresentForGroup.\n");
    /*Check whether the sources are present for the particular group */
    pSrc = (tIgmpSource *) TMO_SLL_First (&(pGrp->srcList));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting the funtion IgmpISSourcePresentForGroup.\n");

    return pSrc;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpAddSSMMappedSourcesForStaticGroup           */
/*                                                                         */
/*     Description   :     This function adds SSM Mapped sources for       */
/*                         the configured static group if the group        */
/*                         falls in a configured SSM mapping range.        */
/*                                                                         */
/*     Input(s)      :     pGrp, pSSMMappedGrp, pIfaceNode,                */
/*                         i4SetValIgmpSrcListStatus, u1NewGrpFlag         */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     IGMP_OK / IGMP_NOT_OK                           */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpAddSSMMappedSourcesForStaticGroup       **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
INT4
IgmpAddSSMMappedSourcesForStaticGroup (tIgmpGroup * pGrp,
                                        tIgmpSSMMapGrpEntry * pSSMMappedGrp,
                                        tIgmpIface * pIfaceNode,
                                        tIgmpIPvXAddr * pSrcAddrX,
                                        INT4 i4SetValIgmpSrcListStatus,
                                        UINT1 u1NewGrpFlag)
{
    tIgmpIPvXAddr   ReporterAddrZero;
    tIgmpIPvXAddr   HostAddrX;
    tIgmpIPvXAddr   SrcAddrX;
    tIgmpSource     *pSrc = NULL;
    UINT4           u4IPvXZero = IGMP_ZERO;
    UINT4           u4Index = IGMP_ZERO;
    UINT4           u4SrcAddr = IGMP_ZERO;
    UINT4           u4TmpAddr = IGMP_ZERO;
    UINT1           u1UpdateMrp = IGMP_FALSE;
    UINT1           u1ErrFlag = IGMP_FALSE;

    IGMP_IPVX_ADDR_CLEAR (&SrcAddrX);
    if (pSrcAddrX != NULL)
    {
        IGMP_IPVX_ADDR_COPY (&SrcAddrX, pSrcAddrX);
    }

    MEMSET (&ReporterAddrZero, IGMP_ZERO, sizeof (tIgmpIPvXAddr));

    for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
    {
        IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
        IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                (UINT1 *) &u4IPvXZero);
        if (((pSrcAddrX == NULL) && (IGMP_IPVX_ADDR_COMPARE (gIPvXZero,
                    pSSMMappedGrp->SrcAddr[u4Index]) != IGMP_ZERO)) ||
            ((pSrcAddrX != NULL) && (IGMP_IPVX_ADDR_COMPARE (SrcAddrX,
                    pSSMMappedGrp->SrcAddr[u4Index]) == IGMP_ZERO)))
        {
            IGMP_IP_COPY_FROM_IPVX (&u4SrcAddr, pSSMMappedGrp->SrcAddr[u4Index],
                    IPVX_ADDR_FMLY_IPV4);
            u4TmpAddr = OSIX_HTONL (u4SrcAddr);
            IGMP_IPVX_ADDR_CLEAR (&HostAddrX);
            IGMP_IPVX_ADDR_INIT_IPV4 (HostAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                    (UINT1 *) &u4TmpAddr);
            pSrc = IgmpAddSource (pGrp, HostAddrX, ReporterAddrZero, IGMP_TRUE);
            if (pSrc == NULL)
            {
                IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                        "IGMP SSM Mapped Source %s is not associated with "
                        "configured Static Group %s added in i/f %d \n",
                        IgmpPrintIPvxAddress (pSSMMappedGrp->SrcAddr[u4Index]),
                        IgmpPrintIPvxAddress (pGrp->u4Group),
                        pIfaceNode->u4IfIndex);
                u1ErrFlag = IGMP_TRUE;
                continue;
            }

            /* This is required when a source node that is both configured statically
             * and SSM mapped, its u1SrcSSMMapped is modified by removing the the SSM
             * mapping and then configured again, then existing source node needs to
             * be updated with IGMP_SRC_MAPPED. But IgmpAddSource just returns the
             * node if IgmpSourceLookup succeeds. So the updation is taken care below.
             */
            if (!(pSrc->u1SrcSSMMapped & IGMP_SRC_MAPPED))
            {
                pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_MAPPED;
            }
            if (pGrp->u1DynamicFlg == IGMP_TRUE)
            {
                pGrp->u1StaticFlg = IGMP_TRUE;
            }
            /* If the group is already learned in EXCLUDE filter mode
               then retain the EXCLUDE mode even if INCLUDE for the same group is received */
            if (pGrp->u1FilterMode != IGMP_FMODE_EXCLUDE)
            {
                pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
            }
            pSrc->u1StaticFlg = IGMP_TRUE;

            if (pSrc->u1FwdState == IGMP_FALSE)
            {
                pSrc->u1UpdatedToMrp = IGMP_FALSE;
            }

            pSrc->u1FwdState = IGMP_TRUE;

            if (u1NewGrpFlag == IGMP_TRUE)
            {
                pGrp->u1EntryStatus = IGMP_ACTIVE;
            }
            if (i4SetValIgmpSrcListStatus == IGMP_CREATE_AND_GO)
            {
                pSrc->u1EntryStatus = IGMP_ACTIVE;
                if ((gIgmpConfig.u1IgmpStatus == IGMP_ENABLE) &&
                        (IGMP_PROXY_STATUS() == IGMP_DISABLE) &&
                        (pGrp->u1EntryStatus == IGMP_ACTIVE) &&
                        (pGrp->pIfNode->u1OperStatus == IGMP_IFACE_OPER_UP))
                {
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                    u1UpdateMrp = IGMP_TRUE;
                }
            }
            else
            {
                pSrc->u1EntryStatus = IGMP_NOT_READY;
            }
        } /* end of empty source address check */
    } /* end of source address loop */

    if ((gIgmpConfig.u1IgmpStatus == IGMP_ENABLE) &&
        (IGMP_PROXY_STATUS () == IGMP_ENABLE))
    {
        /* Call the PROXY routine to update the consolidate group
         * membership data base for static group registration */
        IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_NEW_GROUP);

        if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE,
                    IGMP_NAME, "IGMP PROXY: Sending "
                    " upstream report failed \r\n");
        }

        return IGMP_OK;
    }
    else if (u1UpdateMrp == IGMP_TRUE)
    {
        IgmpUpdateMrps (pGrp, IGMP_JOIN);
    }

    if (u1ErrFlag == IGMP_TRUE)
    {
        if (u1NewGrpFlag == IGMP_TRUE)
        {
            pGrp->u1StaticFlg = IGMP_FALSE;
            pGrp->u1DelSyncFlag = IGMP_FALSE;
            IgmpDeleteGroup (pIfaceNode, pGrp, IGMP_TRUE);
        }
        return IGMP_NOT_OK;
    }
    return IGMP_OK;
}
