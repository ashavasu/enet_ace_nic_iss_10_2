/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stutlget.c,v 1.20 2017/02/06 10:45:28 siva Exp $
 *
 * Description:This file contains Low level routines  for stdigmp.mib  
 *
 *******************************************************************/

#include "include.h"
#include "stdigcon.h"
#include "stdigogi.h"
#include "midconst.h"
#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE ;
#endif

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable (INT4
                                                        i4IgmpInterfaceIfIndex,
                                                        INT4
                                                        i4IgmpInterfaceAddrType)
{

    tIgmpIface         *pIfaceNode = NULL;
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable"
              " routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }
    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "validate Index Instance for i/f %d successful \n",
                       i4IgmpInterfaceIfIndex);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "ValidateIndexInstanceIgmpInterfaceTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable (INT4 *pi4IgmpInterfaceIfIndex,
                                                INT4 *pi4IgmpInterfaceAddrType)
{

    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4IfIndex = 0;
    INT1                i1Status = SNMP_FAILURE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetFirstIndexIgmpInterfaceTbl routine Entry\n");

    if( (*pi4IgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4) ||
            (*pi4IgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6))
    {
        pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);
        while (pIfGetNextLink != NULL)
        {
            pIfNode =
                IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pIfGetNextLink);
            if (pIfNode->u1AddrType == (UINT1 )*pi4IgmpInterfaceAddrType)
            {
                IgmpGetIfIndexFromPort (pIfNode->u4IfIndex,
                        (INT4) pIfNode->u1AddrType,&u4IfIndex);
                *pi4IgmpInterfaceIfIndex = (INT4) u4IfIndex;
                *pi4IgmpInterfaceAddrType = (INT4) pIfNode->u1AddrType;
                i1Status = SNMP_SUCCESS;
                break;
            }
            pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                    &(pIfNode->IfGetNextLink));
        }
    }
    else
    {
    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);
    if (pIfGetNextLink != NULL)
    {
        pIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pIfGetNextLink);
        IgmpGetIfIndexFromPort (pIfNode->u4IfIndex, pIfNode->u1AddrType,
                                &u4IfIndex);
        *pi4IgmpInterfaceIfIndex = u4IfIndex;
        *pi4IgmpInterfaceAddrType = pIfNode->u1AddrType;
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,
                       IGMP_NAME,
                       "First index for Igmp Interface table is %d\n",
                       *pi4IgmpInterfaceIfIndex);

        i1Status = SNMP_SUCCESS;
    }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetFirstIndexIgmpInterfaceTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetNextIndexIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
                nextIgmpInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
IgmpMgmtUtilNmhGetNextIndexIgmpInterfaceTable (INT4 i4IgmpInterfaceIfIndex,
                                               INT4
                                               *pi4NextIgmpInterfaceIfIndex,
                                               INT4 i4IgmpInterfaceAddrType,
                                               INT4
                                               *pi4NextIgmpInterfaceAddrType)
{

    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfNode = NULL;
    tIgmpIface         *pIfNextNode = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetNextIndexIgmpInterfaceTbl routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode != NULL)
    {
        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfNode->IfGetNextLink));

        if (pIfGetNextLink != NULL)
        {
            pIfNextNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                             pIfGetNextLink);
            IgmpGetIfIndexFromPort (pIfNextNode->u4IfIndex,
                                    pIfNextNode->u1AddrType, &u4IfIndex);
            *pi4NextIgmpInterfaceIfIndex = u4IfIndex;
            *pi4NextIgmpInterfaceAddrType = pIfNextNode->u1AddrType;

            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "If Index next to i/f %d is %d \n",
                           i4IgmpInterfaceIfIndex,
                           *pi4NextIgmpInterfaceIfIndex);

            i1Status = SNMP_SUCCESS;
        }
    }
	else
	{
		pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);
		while (pIfGetNextLink != NULL)
		{
			pIfNode = 
				IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pIfGetNextLink);
			if (pIfNode->u1AddrType == (UINT1) i4IgmpInterfaceAddrType)
			{
				IgmpGetIfIndexFromPort (pIfNode->u4IfIndex, 
						(INT4) pIfNode->u1AddrType,&u4IfIndex);
				*pi4NextIgmpInterfaceIfIndex = (INT4) u4IfIndex;
				*pi4NextIgmpInterfaceAddrType = (INT4) pIfNode->u1AddrType;
				i1Status = SNMP_SUCCESS;
				break;
			}
			pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
											&(pIfNode->IfGetNextLink));
		}
	}

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetNextIndexIgmpInterfaceTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceQueryInterval
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                              INT4 i4IgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValIgmpInterfaceQueryInterval)
{

    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4MibSaveResult;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceQueryInterval GET routine Entry\n");

    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceQueryInterval = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        nmhGetIssConfigSaveStatus (&i4MibSaveResult);
        if(i4MibSaveResult == MIB_SAVE_IN_PROGRESS)
        {      
            *pu4RetValIgmpInterfaceQueryInterval =
                 (UINT4) pIfaceNode->u2ConfiguredQueryInterval;
        }
        else
        {
             *pu4RetValIgmpInterfaceQueryInterval =
                (UINT4) pIfaceNode->u2QueryInterval;
        } 
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IgmpGetModuleName(IGMP_TMR_MODULE),
                       "Query Interval on i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceQueryInterval);
        i1Status = SNMP_SUCCESS;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceQueryInterval GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceStatus
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceStatus (INT4 i4IgmpInterfaceIfIndex,
                                       INT4 i4IgmpInterfaceAddrType,
                                       INT4 *pi4RetValIgmpInterfaceStatus)
{

    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceStatus GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pi4RetValIgmpInterfaceStatus = pIfaceNode->u1EntryStatus;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       " Interface Status of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex, *pi4RetValIgmpInterfaceStatus);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceStatus GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceVersion
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceVersion (INT4 i4IgmpInterfaceIfIndex,
                                        INT4 i4IgmpInterfaceAddrType,
                                        UINT4 *pu4RetValIgmpInterfaceVersion)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceVersion GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceVersion = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        if (i4IgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
        {
            if (pIfaceNode->u1Version == MLD_VERSION_1)
            {
                *pu4RetValIgmpInterfaceVersion = (UINT4) IGMP_TWO;
            }
            else if (pIfaceNode->u1Version == MLD_VERSION_2)
            {
                *pu4RetValIgmpInterfaceVersion = (UINT4) IGMP_VERSION_THREE;
            }
        }
        else if (i4IgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            *pu4RetValIgmpInterfaceVersion = (UINT4) pIfaceNode->u1Version;
        }
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       " IGMP Version of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex, *pu4RetValIgmpInterfaceVersion);
        i1Status = SNMP_SUCCESS;
    }
    else
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP Interface %d not found\n",i4IgmpInterfaceIfIndex);
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceVersion GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceQuerier
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQuerier (INT4 i4IgmpInterfaceIfIndex,
                                        INT4 i4IgmpInterfaceAddrType,
                                        tIgmpIPvXAddr *
                                        pu4RetValIgmpInterfaceQuerier)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpQuery         *pIgmp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceQuerier GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        IGMP_IPVX_ADDR_CLEAR (pu4RetValIgmpInterfaceQuerier);
        return SNMP_SUCCESS;
    }
    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        pIgmp = pIfaceNode->pIfaceQry;
        IGMP_IPVX_ADDR_COPY (pu4RetValIgmpInterfaceQuerier,
                             &(pIgmp->u4QuerierAddr));
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface Querier on i/f %d is 0x%s \n",
                       i4IgmpInterfaceIfIndex,
                       IgmpPrintIPvxAddress (*pu4RetValIgmpInterfaceQuerier));
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceQuerier GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceQueryMaxResponseTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQueryMaxResponseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQueryMaxResponseTime (INT4
                                                     i4IgmpInterfaceIfIndex,
                                                     INT4
                                                     i4IgmpInterfaceAddrType,
                                                     UINT4
                                                     *pu4RetValIgmpInterfaceQueryMaxResponseTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpInterfaceQueryMaxResponseTime GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceQueryMaxResponseTime = IGMP_ZERO;
        return SNMP_SUCCESS;
    }
    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValIgmpInterfaceQueryMaxResponseTime =
            (UINT4) pIfaceNode->u2MaxRespTime;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Query-MaxResponse Time of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceQueryMaxResponseTime);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceMaxResponseTime GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceQuerierUpTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQuerierUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQuerierUpTime (INT4 i4IgmpInterfaceIfIndex,
                                              INT4 i4IgmpInterfaceAddrType,
                                              UINT4
                                              *pu4RetValIgmpInterfaceQuerierUpTime)
{
    tIgmpQuery         *pIgmp = NULL;
    UINT4               u4CurrentSysTime = IGMP_ZERO;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceQuerierUpTime GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceQuerierUpTime = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    if (pIfaceNode != NULL)
    {
        pIgmp = pIfaceNode->pIfaceQry;
        OsixGetSysTime (&u4CurrentSysTime);

        *pu4RetValIgmpInterfaceQuerierUpTime = u4CurrentSysTime -
            pIgmp->u4QuerierUpTime;

        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Querier Up-Time of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceQuerierUpTime);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceQuerierUpTime GET routine Exit\n");
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceQuerierExpiryTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQuerierExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceQuerierExpiryTime (INT4 i4IgmpInterfaceIfIndex,
                                                  INT4 i4IgmpInterfaceAddrType,
                                                  UINT4
                                                  *pu4RetValIgmpInterfaceQuerierExpiryTime)
{
    tIgmpQuery         *pIgmp = NULL;
    UINT4               u4RemainingTime = IGMP_ZERO;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpInterfaceQuerierExpiryTime GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceQuerierExpiryTime = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        pIgmp = pIfaceNode->pIfaceQry;
        /* Querier expiry time is the amount of time remaining before the
         * other querier present timer expires.
         * If the local router is Querier, the value of this object is zero
         */
        if (pIgmp->u1Querier == TRUE)
        {
            *pu4RetValIgmpInterfaceQuerierExpiryTime = IGMP_ZERO;
        }
        else
        {
            if (pIgmp->Timer.u1TmrStatus == IGMP_TMR_SET)
            {
                if (TmrGetRemainingTime (gIgmpTimerListId,
                                         &(pIgmp->Timer.TimerBlk.TimerNode),
                                         &u4RemainingTime) != TMR_SUCCESS)
                {
                    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                              "Tmr Get remaining time failure ! \n");
                    return SNMP_FAILURE;
                }
            }
            IGMP_GET_TIME_IN_SEC (u4RemainingTime);
            *pu4RetValIgmpInterfaceQuerierExpiryTime = u4RemainingTime;
        }

        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "QuerierExpiry Time of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceQuerierExpiryTime);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "IgmpInterfaceQuerierExpiryTime GET routine Exit\n");
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceVersion1QuerierTimer
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceVersion1QuerierTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceVersion1QuerierTimer (INT4
                                                     i4IgmpInterfaceIfIndex,
                                                     INT4
                                                     i4IgmpInterfaceAddrType,
                                                     UINT4
                                                     *pu4RetValIgmpInterfaceVersion1QuerierTimer)
{

    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    UNUSED_PARAM (i4IgmpInterfaceAddrType);
    *pu4RetValIgmpInterfaceVersion1QuerierTimer = IGMP_ZERO;

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "This object not supported by FutureIGMP while only"
              "implements Igmp-Router functionality\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceWrongVersionQueries
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceWrongVersionQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceWrongVersionQueries (INT4 i4IgmpInterfaceIfIndex,
                                                    INT4
                                                    i4IgmpInterfaceAddrType,
                                                    UINT4
                                                    *pu4RetValIgmpInterfaceWrongVersionQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpInterfaceWrongVersionQueries GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValIgmpInterfaceWrongVersionQueries =
            pIfaceNode->u4WrongVerQueries;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Wrong version queries of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceWrongVersionQueries);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "IgmpInterfaceWrongVersionQueries GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceJoins
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceJoins (INT4 i4IgmpInterfaceIfIndex,
                                      INT4 i4IgmpInterfaceAddrType,
                                      UINT4 *pu4RetValIgmpInterfaceJoins)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceJoins GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValIgmpInterfaceJoins = pIfaceNode->u4ProcessedJoins;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Processed Joins of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex, *pu4RetValIgmpInterfaceJoins);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceJoins GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceProxyIfIndex
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceProxyIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceProxyIfIndex (INT4 i4IgmpInterfaceIfIndex,
                                             INT4 i4IgmpInterfaceAddrType,
                                             INT4
                                             *pi4RetValIgmpInterfaceProxyIfIndex)
{

    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    UNUSED_PARAM (i4IgmpInterfaceAddrType);

    *pi4RetValIgmpInterfaceProxyIfIndex = IGMP_ZERO;
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "This object not supported by IGMP as there is no proxy "
              "support\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceGroups
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceGroups
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceGroups (INT4 i4IgmpInterfaceIfIndex,
                                       INT4 i4IgmpInterfaceAddrType,
                                       UINT4 *pu4RetValIgmpInterfaceGroups)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceGroups GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceGroups = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValIgmpInterfaceGroups = pIfaceNode->u4InterfaceGroups;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "InterfaceGroups of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex, *pu4RetValIgmpInterfaceGroups);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceGroups GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceRobustness
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceRobustness
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceRobustness (INT4 i4IgmpInterfaceIfIndex,
                                           INT4 i4IgmpInterfaceAddrType,
                                           UINT4
                                           *pu4RetValIgmpInterfaceRobustness)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "IgmpInterfaceRobustness GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceRobustness = IGMP_DEFAULT_ROB_VARIABLE;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)

    {
        *pu4RetValIgmpInterfaceRobustness = (UINT4) pIfaceNode->u1Robustness;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "InterfaceGroups of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceRobustness);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpInterfaceRobustness GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceLastMembQueryIntvl
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceLastMembQueryIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceLastMembQueryIntvl (INT4 i4IgmpInterfaceIfIndex,
                                                   INT4 i4IgmpInterfaceAddrType,
                                                   UINT4
                                                   *pu4RetValIgmpInterfaceLastMembQueryIntvl)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpInterfaceLastMembQueryIntvl GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceLastMembQueryIntvl = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {

        *pu4RetValIgmpInterfaceLastMembQueryIntvl = (UINT4)
            (pIfaceNode->u4LastMemberQueryIntvl);

        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "InterfaceLastMemberQuery Interval of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceLastMembQueryIntvl);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "IgmpInterfaceLastMembQueryIntvl GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpInterfaceVersion2QuerierTimer
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceVersion2QuerierTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpInterfaceVersion2QuerierTimer (INT4
                                                     i4IgmpInterfaceIfIndex,
                                                     INT4
                                                     i4IgmpInterfaceAddrType,
                                                     UINT4
                                                     *pu4RetValIgmpInterfaceVersion2QuerierTimer)
{
    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    UNUSED_PARAM (i4IgmpInterfaceAddrType);
    *pu4RetValIgmpInterfaceVersion2QuerierTimer = IGMP_ZERO;

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "This object not supported by FutureIGMP while only"
              "implements Igmp-Router functionality\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                                                    tIgmpIPvXAddr
                                                    u4IgmpCacheAddress,
                                                    INT4 i4IgmpCacheIfIndex)
{
    INT1                i1RetStatus;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    i1RetStatus = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex,
                                i4IgmpCacheAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    if (pIfaceNode == NULL)
    {
        MGMD_DBG (IGMP_DBG_FAILURE, "Create the interface first\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "IgmpCacheTable Test routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    /* Validate the CacheAddress */
    pGrp = IgmpGrpLookUp (pIfaceNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        i1RetStatus = SNMP_SUCCESS;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "validate Index Instance for grpAddr 0x%s and "
                       "%d successful \n",
                       IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                       i4IgmpCacheIfIndex);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "ValidateIndexInstanceIgmpCacheTbl routine Exit\n");
    return (i1RetStatus);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFirstIndexIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
IgmpMgmtUtilNmhGetFirstIndexIgmpCacheTable (INT4 *pi4IgmpCacheAddrType,
                                            tIgmpIPvXAddr * pu4IgmpCacheAddress,
                                            INT4 *pi4IgmpCacheIfIndex)
{
    INT1                i1Status = IGMP_ZERO;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4IfIndex = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetFirstIndexIgmpCacheTbl routine Entry\n");

    i1Status = SNMP_FAILURE;

    pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
    while (pGrp != NULL)
    {
        if (*pi4IgmpCacheAddrType == IGMP_ZERO)
    {
        IgmpGetIfIndexFromPort (pGrp->pIfNode->u4IfIndex,
                                pGrp->pIfNode->u1AddrType, &u4IfIndex);
        *pi4IgmpCacheIfIndex = (INT4) u4IfIndex;
        *pi4IgmpCacheAddrType = pGrp->pIfNode->u1AddrType;
        IGMP_IPVX_ADDR_COPY (pu4IgmpCacheAddress, &(pGrp->u4Group));
        i1Status = SNMP_SUCCESS;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "First indices of Igmp Cache table are GrpAddr"
                       "0x%s and "
                       "%d\n",
                       IgmpPrintIPvxAddress (*pu4IgmpCacheAddress),
                       *pi4IgmpCacheIfIndex);
            break;
        }
        else if ((*pi4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4) ||
                (*pi4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV6))
        {
            if (*pi4IgmpCacheAddrType == pGrp->pIfNode->u1AddrType)
            {
                IgmpGetIfIndexFromPort (pGrp->pIfNode->u4IfIndex,
                        pGrp->pIfNode->u1AddrType, &u4IfIndex);
                *pi4IgmpCacheIfIndex = (INT4) u4IfIndex;
                *pi4IgmpCacheAddrType = pGrp->pIfNode->u1AddrType;
                IGMP_IPVX_ADDR_COPY (pu4IgmpCacheAddress, &(pGrp->u4Group));
                i1Status = SNMP_SUCCESS;
                IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                        "First indices of Igmp Cache table are GrpAddr"
                        "0x%s and "
                        "%d\n",
                        IgmpPrintIPvxAddress (*pu4IgmpCacheAddress),
                        *pi4IgmpCacheIfIndex);
                break;
            }
        }
        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                (tRBElem *) pGrp, NULL);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetFirstIndexIgmpCacheTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetNextIndexIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                nextIgmpCacheAddress
                IgmpCacheIfIndex
                nextIgmpCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
IgmpMgmtUtilNmhGetNextIndexIgmpCacheTable (INT4 i4IgmpCacheAddrType,
                                           INT4 *pi4NextIgmpCacheAddrType,
                                           tIgmpIPvXAddr u4IgmpCacheAddress,
                                           tIgmpIPvXAddr *
                                           pu4NextIgmpCacheAddress,
                                           INT4 i4IgmpCacheIfIndex,
                                           INT4 *pi4NextIgmpCacheIfIndex)
{
    INT1               i1Status = SNMP_FAILURE;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup          IgmpGroupEntry;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetNextIndexIgmpCacheTbl routine Entry\n");

    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    MEMSET (&IgmpGroupEntry, IGMP_ZERO, sizeof (tIgmpGroup));

    IgmpGroupEntry.u4IfIndex = u4Port;
    IgmpGroupEntry.pIfNode = pIfNode;
    IGMP_IPVX_ADDR_COPY (&(IgmpGroupEntry.u4Group), &u4IgmpCacheAddress);

    pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                         (tRBElem *) & IgmpGroupEntry, NULL);

    if (pGrp != NULL) 
    {
        IgmpGetIfIndexFromPort (pGrp->pIfNode->u4IfIndex,
                                pGrp->pIfNode->u1AddrType, &u4IfIndex);
        *pi4NextIgmpCacheAddrType = (INT4) (pGrp->pIfNode->u1AddrType);
        *pi4NextIgmpCacheIfIndex = (INT4) u4IfIndex;
        IGMP_IPVX_ADDR_COPY (pu4NextIgmpCacheAddress, &(pGrp->u4Group));
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetNextIndexIgmpCacheTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheSelf
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheSelf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheSelf (INT4 i4IgmpCacheAddrType,
                                 tIgmpIPvXAddr u4IgmpCacheAddress,
                                 INT4 i4IgmpCacheIfIndex,
                                 INT4 *pi4RetValIgmpCacheSelf)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpCacheSelf routine Entry\n");

    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetNextIndexIgmpCahceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        *pi4RetValIgmpCacheSelf = (INT4) pGrp->i1CacheSelfFlag;
        i1Status = SNMP_SUCCESS;
    }
    if (i1Status != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "GetIgmpCacheSelf routine failure\n");
        return i1Status;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Cache self flag of IGMP cache Address 0x%s and i/f %d is %d\n",
                   IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                   i4IgmpCacheIfIndex, *pi4RetValIgmpCacheSelf);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetIgmpCacheSelf routine Exit\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheLastReporter
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheLastReporter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheLastReporter (INT4 i4IgmpCacheAddrType,
                                         tIgmpIPvXAddr u4IgmpCacheAddress,
                                         INT4 i4IgmpCacheIfIndex,
                                         tIgmpIPvXAddr *
                                         pu4RetValIgmpCacheLastReporter)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpCacheLastReporter routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetNextIndexIgmpCahceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        IGMP_IPVX_ADDR_COPY (pu4RetValIgmpCacheLastReporter,
                             &(pGrp->u4LastReporter));
        i1Status = SNMP_SUCCESS;
    }
    if (i1Status != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "GetIgmpCacheLastReporter routine failure\n");
        return i1Status;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Cache LastReporter of IGMP cache Address 0x%s and i/f %d is %s\n",
                   IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                   i4IgmpCacheIfIndex, IgmpPrintIPvxAddress (*pu4RetValIgmpCacheLastReporter));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),                                         "GetNextIndexIgmpCahceTbl routine Exit\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheUpTime
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheUpTime (INT4 i4IgmpCacheAddrType,
                                   tIgmpIPvXAddr u4IgmpCacheAddress,
                                   INT4 i4IgmpCacheIfIndex,
                                   UINT4 *pu4RetValIgmpCacheUpTime)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4CurrentSysTime = IGMP_ZERO;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpCacheUpTime routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "GetNextIndexIgmpCahceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        OsixGetSysTime (&u4CurrentSysTime);
        *pu4RetValIgmpCacheUpTime = u4CurrentSysTime - pGrp->u4CacheUpTime;
        i1Status = SNMP_SUCCESS;
    }
    if (i1Status != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "GetIgmpCacheUpTime routine failure\n");
        return i1Status;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Cache up time of IGMP cache Address 0x%s and i/f %d is %d secs \n",
                   IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                   i4IgmpCacheIfIndex, *pu4RetValIgmpCacheUpTime);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),                                                 "GetNextIndexIgmpCahceTbl routine Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheExpiryTime
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheExpiryTime (INT4 i4IgmpCacheAddrType,
                                       tIgmpIPvXAddr u4IgmpCacheAddress,
                                       INT4 i4IgmpCacheIfIndex,
                                       UINT4 *pu4RetValIgmpCacheExpiryTime)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4RemainingTime = IGMP_ZERO;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpCacheExpiryTime routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetNextIndexIgmpCahceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        if (pGrp->i1CacheSelfFlag == IGMP_TRUE)
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "IGMP cache entry with Address 0x%s and"
                           "i/f %d has local"
                           " membership \n",
                           IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                           i4IgmpCacheIfIndex);
            return SNMP_SUCCESS;
        }

        if (pGrp->Timer.u1TmrStatus == IGMP_TMR_SET)
        {
            if (TmrGetRemainingTime (gIgmpTimerListId,
                                     &(pGrp->Timer.TimerBlk.TimerNode),
                                     &u4RemainingTime) != TMR_SUCCESS)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Tmr Get remaining time failure ! \n");
                return SNMP_FAILURE;
            }
            IGMP_GET_TIME_IN_SEC (u4RemainingTime);
            *pu4RetValIgmpCacheExpiryTime = u4RemainingTime;
        }
        else
        {
            *pu4RetValIgmpCacheExpiryTime = IGMP_ZERO;
        }

        i1Status = SNMP_SUCCESS;
    }
    if (i1Status != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "GetIgmpCacheExpiryTime routine failure\n");
        return i1Status;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Cache Expiry time of IGMP cache Address 0x%s and"
                   "i/f %d is %d secs \n",
                   IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                   i4IgmpCacheIfIndex, *pu4RetValIgmpCacheExpiryTime);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "GetNextIndexIgmpCahceTbl routine Exit\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheStatus
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheStatus (INT4 i4IgmpCacheAddrType,
                                   tIgmpIPvXAddr u4IgmpCacheAddress,
                                   INT4 i4IgmpCacheIfIndex,
                                   INT4 *pi4RetValIgmpCacheStatus)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpCacheStatus routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);
    if (NULL == pIfNode)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        *pi4RetValIgmpCacheStatus = pGrp->u1EntryStatus;
        i1Status = SNMP_SUCCESS;
    }

    if (pGrp == NULL)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "Get IgmpSrcListStatus failure\n");
        return i1Status;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),                                           "GetIgmpCacheStatus routine Exit\n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheVersion1HostTimer
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheVersion1HostTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheVersion1HostTimer (INT4 i4IgmpCacheAddrType,
                                              tIgmpIPvXAddr u4IgmpCacheAddress,
                                              INT4 i4IgmpCacheIfIndex,
                                              UINT4
                                              *pu4RetValIgmpCacheVersion1HostTimer)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4RemainingTime = IGMP_ZERO;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpCacheExpiryTime routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetIgmpCacheExpiryTime routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        if (pGrp->Version1HostTimer.u1TmrStatus == IGMP_TMR_SET)
        {
            if (TmrGetRemainingTime (gIgmpTimerListId,
                                     &(pGrp->Version1HostTimer.TimerBlk.
                                       TimerNode),
                                     &u4RemainingTime) != TMR_SUCCESS)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Tmr Get remaining time failure ! \n");
                return SNMP_FAILURE;
            }

            IGMP_GET_TIME_IN_SEC (u4RemainingTime);
            *pu4RetValIgmpCacheVersion1HostTimer = u4RemainingTime;
        }
        else
        {
            *pu4RetValIgmpCacheVersion1HostTimer = IGMP_ZERO;
        }
        i1Status = SNMP_SUCCESS;
    }
    if (i1Status != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "GetIgmpCacheVersion1HostTimer routine failure\n");
        return i1Status;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Cache Version 1 Host Timer of IGMP cache Address 0x%s and "
                   "i/f %d is %d secs \n",
                   IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                   i4IgmpCacheIfIndex, *pu4RetValIgmpCacheVersion1HostTimer);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "GetIgmpCacheExpiryTime routine Exit\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheVersion2HostTimer
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheVersion2HostTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheVersion2HostTimer (INT4 i4IgmpCacheAddrType,
                                              tIgmpIPvXAddr u4IgmpCacheAddress,
                                              INT4 i4IgmpCacheIfIndex,
                                              UINT4
                                              *pu4RetValIgmpCacheVersion2HostTimer)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4RemainingTime = IGMP_ZERO;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			"GetIgmpCacheExpiryTime routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			"GetNextIndexIgmpCahceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        if (pGrp->Version2HostTimer.u1TmrStatus == IGMP_TMR_SET)
        {
            if (TmrGetRemainingTime (gIgmpTimerListId,
                                     &(pGrp->Version2HostTimer.TimerBlk.
                                       TimerNode),
                                     &u4RemainingTime) != TMR_SUCCESS)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Tmr Get remaining time failure ! \n");
                return SNMP_FAILURE;
            }
            IGMP_GET_TIME_IN_SEC (u4RemainingTime);
            *pu4RetValIgmpCacheVersion2HostTimer = u4RemainingTime;
        }
        else
        {
            *pu4RetValIgmpCacheVersion2HostTimer = IGMP_ZERO;
        }
        i1Status = SNMP_SUCCESS;
    }
    if (i1Status != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "GetIgmpCacheVersion2HostTimer routine failure\n");
        return i1Status;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Cache Version 2 Host Timer of IGMP cache Address 0x%s and "
                   "i/f %d is %d secs \n",
                   IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                   i4IgmpCacheIfIndex, *pu4RetValIgmpCacheVersion2HostTimer);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "GetNextIndexIgmpCahceTbl routine Exit\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpCacheSourceFilterMode
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheSourceFilterMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpCacheSourceFilterMode (INT4 i4IgmpCacheAddrType,
                                             tIgmpIPvXAddr u4IgmpCacheAddress,
                                             INT4 i4IgmpCacheIfIndex,
                                             INT4
                                             *pi4RetValIgmpCacheSourceFilterMode)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpCacheExpiryTime routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex, i4IgmpCacheAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetNextIndexIgmpCahceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }
    if ((pIfNode->u1Version == IGMP_VERSION_3) ||
        (pIfNode->u1Version == MLD_VERSION_2))
    {
        pGrp = IgmpGrpLookUp (pIfNode, u4IgmpCacheAddress);
        if (pGrp != NULL)
        {
            *pi4RetValIgmpCacheSourceFilterMode = (INT4) pGrp->u1FilterMode;
            i1Status = SNMP_SUCCESS;
        }
        if (i1Status != SNMP_SUCCESS)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                      IGMP_NAME,
                      "GetIgmpCacheSourceFilterMode routine failure\n");
            return i1Status;
        }
        IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Filter Mode of IGMP cache Address 0x%s and "
                       "i/f %d is %d secs \n",
                       IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                       i4IgmpCacheIfIndex, *pi4RetValIgmpCacheSourceFilterMode);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "GetNextIndexIgmpCahceTbl routine Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhValidateIndexInstanceIgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
IgmpMgmtUtilNmhValidateIndexInstanceIgmpSrcListTable (INT4
                                                      i4IgmpSrcListAddrType,
                                                      tIgmpIPvXAddr
                                                      u4IgmpSrcListAddress,
                                                      INT4 i4IgmpSrcListIfIndex,
                                                      tIgmpIPvXAddr
                                                      u4IgmpSrcListHostAddress)
{

    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "ValidateIndexInstanceIgmpSrcListTbl routine Entry\n");
    /* See whether the cache entry for the particular interface index 
       and group address is present */
    if (IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable
        (i4IgmpSrcListAddrType, u4IgmpSrcListAddress,
         i4IgmpSrcListIfIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (IgmpGetPortFromIfIndex (i4IgmpSrcListIfIndex,
                                i4IgmpSrcListAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpSrcListAddrType);

    if (pIfaceNode == NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, 
		       "Interface %d not up \n", u4Port);
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    /* Validate the Source Address */
    pGrp = IgmpGrpLookUp (pIfaceNode, u4IgmpSrcListAddress);
    if (pGrp != NULL)
    {
        TMO_SLL_Scan (&(pGrp->srcList), pNextSrcLink, tTMO_SLL_NODE *)
        {
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pNextSrcLink);
            if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                        u4IgmpSrcListHostAddress) == IGMP_ZERO)
            {
                i1Status = SNMP_SUCCESS;
                IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                               "validate Index Instance for grpAddr 0x%s and %d"
                               "and 0x%s successful \n",
                               IgmpPrintIPvxAddress (u4IgmpSrcListAddress),
                               i4IgmpSrcListIfIndex,
                               IgmpPrintIPvxAddress (u4IgmpSrcListHostAddress));
                break;
            }
        }
    }
    if (pSrc == NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME, "Source node not found %s\n ",
                       IgmpPrintIPvxAddress(u4IgmpSrcListHostAddress));
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "ValidateIndexInstanceIgmpSrcListTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetFirstIndexIgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
IgmpMgmtUtilNmhGetFirstIndexIgmpSrcListTable (INT4 *pi4IgmpSrcListAddrType,
                                              tIgmpIPvXAddr *
                                              pu4IgmpSrcListAddress,
                                              INT4 *pi4IgmpSrcListIfIndex,
                                              tIgmpIPvXAddr *
                                              pu4IgmpSrcListHostAddress)
{
    INT1                i1Status;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pSrcLink = NULL;
    tIgmpGroup          IgmpGroupEntry;
    UINT4               u4IfIndex = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE), 
		"GetFirstIndexIgmpCacheTbl routine Entry\n");
    MEMSET (&IgmpGroupEntry, IGMP_ZERO, sizeof (tIgmpGroup));

    i1Status = SNMP_FAILURE;

    pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
    while (pGrp != NULL)
    {
        TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
        {
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);
            if (pSrc != NULL)
            {
                IgmpGetIfIndexFromPort (pGrp->pIfNode->u4IfIndex,
                                        pGrp->pIfNode->u1AddrType, &u4IfIndex);
                *pi4IgmpSrcListIfIndex = (INT4) u4IfIndex;
                *pi4IgmpSrcListAddrType = (INT4) (pGrp->pIfNode->u1AddrType);
                IGMP_IPVX_ADDR_COPY (pu4IgmpSrcListAddress, &(pGrp->u4Group));
                IGMP_IPVX_ADDR_COPY (pu4IgmpSrcListHostAddress,
                                     &(pSrc->u4SrcAddress));
                i1Status = SNMP_SUCCESS;
                IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                               "First indiceS of Igmp Cache table are "
                               "GrpAddr 0x%s,"
                               "IfIndex %d SrcAddress 0x%s\n",
                               IgmpPrintIPvxAddress
                               (*pu4IgmpSrcListAddress),
                               *pi4IgmpSrcListIfIndex,
                               IgmpPrintIPvxAddress
                               (*pu4IgmpSrcListHostAddress));
                return (i1Status);
            }
        }
        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) pGrp, NULL);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "GetFirstIndexIgmpSrcListTbl routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetNextIndexIgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                nextIgmpSrcListAddress
                IgmpSrcListIfIndex
                nextIgmpSrcListIfIndex
                IgmpSrcListHostAddress
                nextIgmpSrcListHostAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
IgmpMgmtUtilNmhGetNextIndexIgmpSrcListTable (INT4 i4IgmpSrcListAddrType,
                                             INT4 *pi4NextIgmpSrcListAddrType,
                                             tIgmpIPvXAddr u4IgmpSrcListAddress,
                                             tIgmpIPvXAddr *
                                             pu4NextIgmpSrcListAddress,
                                             INT4 i4IgmpSrcListIfIndex,
                                             INT4 *pi4NextIgmpSrcListIfIndex,
                                             tIgmpIPvXAddr
                                             u4IgmpSrcListHostAddress,
                                             tIgmpIPvXAddr *
                                             pu4NextIgmpSrcListHostAddress)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpIface         *pIfNode = NULL;
    tTMO_SLL_NODE      *pSrcLink = NULL;
    tIgmpGroup          IgmpGrpEntry;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex = 0;
    INT1                i1Status = SNMP_FAILURE;
    BOOL1               bIsGrpSrcFound = TRUE;

    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetNextIndexIgmpSrcListTbl routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpSrcListIfIndex, i4IgmpSrcListAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpSrcListAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }
    IgmpGrpEntry.u4IfIndex = u4Port;
    IgmpGrpEntry.pIfNode = pIfNode;
    IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &u4IgmpSrcListAddress);

    /* Get the group entry corresponding to the input given */
    pGrp = (tIgmpGroup *) RBTreeGet (gIgmpGroup, (tRBElem *) & IgmpGrpEntry);

    if (pGrp == NULL)
    {
        /* No entry exists for the given input, then get the
         * next valid group entry */
        bIsGrpSrcFound = FALSE;
    }
    else
    {
        /* Group entry found search for next valid source address */
        TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
        {
            /* Scan for Source list */
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);

            if (IGMP_IPVX_ADDR_COMPARE (u4IgmpSrcListHostAddress,
                                        pSrc->u4SrcAddress) < IGMP_ZERO)
            {
                /* Next Valid Source address found */
                IgmpGetIfIndexFromPort (pGrp->pIfNode->u4IfIndex,
                                        pGrp->pIfNode->u1AddrType, &u4IfIndex);
                *pi4NextIgmpSrcListAddrType =
                    (INT4) (pGrp->pIfNode->u1AddrType);
                *pi4NextIgmpSrcListIfIndex = (INT4) u4IfIndex;
                IGMP_IPVX_ADDR_COPY (pu4NextIgmpSrcListAddress,
                                     &(pGrp->u4Group));
                IGMP_IPVX_ADDR_COPY (pu4NextIgmpSrcListHostAddress,
                                     &(pSrc->u4SrcAddress));
                return SNMP_SUCCESS;
            }
        }
        /* there is no valid next entry for the given input
         * in the given group entry, so get the first entry from the 
         * next group entry */
        bIsGrpSrcFound = FALSE;
    }

    if (bIsGrpSrcFound == FALSE)
    {
        while (1)
        {
            pGrp = (tIgmpGroup *) RBTreeGetNext
                (gIgmpGroup, (tRBElem *) & IgmpGrpEntry, NULL);

            if (pGrp == NULL)
            {
                i1Status = SNMP_FAILURE;
                break;
            }
            pSrcLink = (tTMO_SLL_NODE *) TMO_SLL_First (&(pGrp->srcList));
            if (pSrcLink != NULL)
            {
                pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);
                IgmpGetIfIndexFromPort (pGrp->pIfNode->u4IfIndex,
                                        pGrp->pIfNode->u1AddrType, &u4IfIndex);
                *pi4NextIgmpSrcListAddrType =
                    (INT4) (pGrp->pIfNode->u1AddrType);
                *pi4NextIgmpSrcListIfIndex = (INT4) u4IfIndex;
                IGMP_IPVX_ADDR_COPY (pu4NextIgmpSrcListAddress,
                                     &(pGrp->u4Group));
                IGMP_IPVX_ADDR_COPY (pu4NextIgmpSrcListHostAddress,
                                     &(pSrc->u4SrcAddress));
                i1Status = SNMP_SUCCESS;
                break;
            }
            /* Continue to get the next valid entry for IPv4 type */
            IgmpGrpEntry.u4IfIndex = pGrp->pIfNode->u4IfIndex;
            IgmpGrpEntry.pIfNode = pGrp->pIfNode;
            IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &(pGrp->u4Group));
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "GetNextIndexIgmpCacheTbl routine Exit\n");
    return (i1Status);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetIgmpSrcListStatus
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress

                The Object 
                retValIgmpSrcListStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetIgmpSrcListStatus (INT4 i4IgmpSrcListAddrType,
                                     tIgmpIPvXAddr u4IgmpSrcListAddress,
                                     INT4 i4IgmpSrcListIfIndex,
                                     tIgmpIPvXAddr u4IgmpSrcListHostAddress,
                                     INT4 *pi4RetValIgmpSrcListStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pSrcLink = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpSrcListStatus routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpSrcListIfIndex,
                                i4IgmpSrcListAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpSrcListAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetIgmpSourceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4IgmpSrcListAddress);
    if (pGrp != NULL)
    {
        TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
        {
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);

            if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                        u4IgmpSrcListHostAddress) == IGMP_ZERO)
            {
                *pi4RetValIgmpSrcListStatus = pSrc->u1EntryStatus;
                i1Status = SNMP_SUCCESS;
                UNUSED_PARAM (i1Status);
                break;
            }

        }
    }
    if ((pGrp == NULL) || (pSrc == NULL))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "Get IgmpSrcListStatus failure\n");
        return SNMP_FAILURE;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Entry status of IGMP cache entry with"
                   "Address 0x%s and i/f %d is %d\n",
                   IgmpPrintIPvxAddress (u4IgmpSrcListAddress),
                   i4IgmpSrcListIfIndex, *pi4RetValIgmpSrcListStatus);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "GetIgmpSourceTbl routine Exit\n");
    return SNMP_SUCCESS;

}

/* Newly added functions regarding the stdmgmd.mib */
/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetMgmdRouterInterfaceLastMemberQueryCount
 Input       :  The Indices
                IgmpInterfaceAddrType
                IgmpSrcListIfIndex

                The Object 
                RetValIgmpInterfaceLastMemberQueryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetMgmdRouterInterfaceLastMemberQueryCount (INT4
                                                           i4IgmpInterfaceIfIndex,
                                                           INT4
                                                           i4IgmpInterfaceAddrType,
                                                           UINT4
                                                           *pu4RetValIgmpInterfaceLastMemberQueryCount)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpInterfaceLastMembQueryCount GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType, &u4Port)
        == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceLastMemberQueryCount = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValIgmpInterfaceLastMemberQueryCount =
            IGMP_IF_LAST_MEMBER_QRY_COUNT (pIfaceNode);
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "InterfaceLastMemberQuery Count of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceLastMemberQueryCount);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "IgmpInterfaceLastMembQueryCount GET routine Exit\n");
    return (i1Status);

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetMgmdRouterInterfaceStartupQueryCount
 Input       :  The Indices
                IgmpInterfaceAddrType
                IgmpSrcListIfIndex

                The Object 
                RetValIgmpInterfaceStartupQueryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetMgmdRouterInterfaceStartupQueryCount (INT4
                                                        i4IgmpInterfaceIfIndex,
                                                        INT4
                                                        i4IgmpInterfaceAddrType,
                                                        UINT4
                                                        *pu4RetValIgmpInterfaceStartupQueryCount)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpInterfaceStartUpQueryCount GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceStartupQueryCount = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {
        *pu4RetValIgmpInterfaceStartupQueryCount =
            MAX_IF_STARTUP_QUERIES (pIfaceNode);
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "InterfaceStartUpQuery Count of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceStartupQueryCount);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "IgmpInterfaceStartUpQueryCount GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmdUtilNmhGetMgmdRouterInterfaceStartupQueryInterval
 Input       :  The Indices
                IgmpInterfaceAddrType
                IgmpSrcListIfIndex

                The Object 
                RetValIgmpInterfaceStartupQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmdUtilNmhGetMgmdRouterInterfaceStartupQueryInterval (INT4
                                                           i4IgmpInterfaceIfIndex,
                                                           INT4
                                                           i4IgmpInterfaceAddrType,
                                                           UINT4
                                                           *pu4RetValIgmpInterfaceStartupQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "IgmpInterfaceLastMembQueryIntvl GET routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        *pu4RetValIgmpInterfaceStartupQueryInterval = IGMP_ZERO;
        return SNMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {

        *pu4RetValIgmpInterfaceStartupQueryInterval =
            IGMP_STARTUP_QIVAL (pIfaceNode);
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "InterfaceStartUpQuery Intvl of i/f %d is %d \n",
                       i4IgmpInterfaceIfIndex,
                       *pu4RetValIgmpInterfaceStartupQueryInterval);
        i1Status = SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
              "IgmpInterfaceStartUpQueryIntvl GET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmdUtilNmhGetMgmdRouterCacheExcludeModeExpiryTimer
 Input       :  The Indices
                IgmpCacheAddrType
                IgmpCacheAddrX
                IgmpCacheIfIndex

                The Object 
                RetValIgmpCacheExcludeModeExpiryTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmdUtilNmhGetMgmdRouterCacheExcludeModeExpiryTimer (INT4
                                                         i4IgmpCacheAddrType,
                                                         tIgmpIPvXAddr
                                                         IgmpCacheAddrX,
                                                         INT4
                                                         i4IgmpCacheIfIndex,
                                                         UINT4
                                                         *pu4RetValIgmpCacheExcludeModeExpiryTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "GetIgmpCacheExcludeModeExpiryTimer routine Entry\n");
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex,
                                i4IgmpCacheAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetNextIndexIgmpCahceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }
    pGrp = IgmpGrpLookUp (pIfNode, IgmpCacheAddrX);
    if (pGrp != NULL)
    {
        i1Status = SNMP_SUCCESS;
        if (pGrp->Timer.u1TmrStatus == IGMP_TMR_SET)
        {
            if (TmrGetRemainingTime (gIgmpTimerListId,
                                     &(pGrp->Timer.TimerBlk.TimerNode),
                                     pu4RetValIgmpCacheExcludeModeExpiryTimer)
                != TMR_SUCCESS)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Tmr Get remaining time failure ! \n");
            }
        }
        else
        {
            *pu4RetValIgmpCacheExcludeModeExpiryTimer = IGMP_ZERO;
        }
    }
    if (i1Status != SNMP_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME,
                  "GetIgmpCacheExcludeModeExpiryTimer routine failure\n");
        return i1Status;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "CacheExModeExpryTmr of IGMP cache Address 0x%s and i/f %d is %d\n",
                   IgmpPrintIPvxAddress (IgmpCacheAddrX), i4IgmpCacheIfIndex,
                   *pu4RetValIgmpCacheExcludeModeExpiryTimer);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),                                         "GetNextIndexIgmpCahceTbl routine Exit\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhGetMgmdRouterSrcListExpire
 Input       :  The Indices
                IgmpSrcListAddrType
                IgmpSrcListAddrX
                IgmpSrcListIfIndex
                IgmpSrcListHostAddrX

                The Object 
                RetValIgmpSrcListExpire
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhGetMgmdRouterSrcListExpire (INT4 i4IgmpSrcListAddrType,
                                           tIgmpIPvXAddr IgmpSrcListAddrX,
                                           INT4 i4IgmpSrcListIfIndex,
                                           tIgmpIPvXAddr IgmpSrcListHostAddrX,
                                           UINT4 *pu4RetValIgmpSrcListExpire)
{
    INT1                i1Status;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pSrcLink = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "GetIgmpSrcListExpire routine Entry\n");
    i1Status = SNMP_FAILURE;
    if (IgmpGetPortFromIfIndex (i4IgmpSrcListIfIndex,
                                i4IgmpSrcListAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4IgmpSrcListAddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "GetIgmpSourceTbl routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfNode, IgmpSrcListAddrX);
    if (pGrp != NULL)
    {
        TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
        {
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);

            if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                        IgmpSrcListHostAddrX) == IGMP_ZERO)
            {
                if (pSrc->srcTimer.u1TmrStatus == IGMP_TMR_SET)
                {
                    if (TmrGetRemainingTime (gIgmpTimerListId,
                                             &(pSrc->srcTimer.TimerBlk.
                                               TimerNode),
                                             pu4RetValIgmpSrcListExpire) !=
                        TMR_SUCCESS)
                    {
                        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                  "Tmr Get remaining time failure ! \n");
                    }
                    i1Status = SNMP_SUCCESS;
                    UNUSED_PARAM (i1Status);
                }
                break;
            }
        }
    }
    if ((pGrp == NULL) || (pSrc == NULL))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "Get IgmpSrcListExpire failure\n");
        return SNMP_FAILURE;
    }
    IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Entry status of IGMP cache entry with"
                   "Address 0x%s and i/f %d is %d\n",
                   IgmpPrintIPvxAddress (IgmpSrcListAddrX),
                   i4IgmpSrcListIfIndex, *pu4RetValIgmpSrcListExpire);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "GetIgmpSourceTbl routine Exit\n");
    return SNMP_SUCCESS;

}
