/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpstb.c,v 1.2 2013/12/20 03:52:27 siva Exp $ 
 *
 * Description: This file contains IGMP Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __IGMPRED_C
#define __IGMPRED_C

#include "igmpinc.h"
#include "igmpred.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDynDataDescInit                           */
/*                                                                           */
/*    Description         : This function will initialize dynamic info       */
/*                          data descriptor for the different DB nodes.      */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDynDataDescInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Igmp descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDescrTblInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Igmp db node.      */
/*                                                                           */
/*    Input(s)            : DBNode - Igmp data strucutres DBNode             */
/*                    u4Type - Dynamic info type                       */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDbNodeInit (tDbTblNode * pDBNode, UINT4 u4Type)
{
    UNUSED_PARAM (pDBNode);
    UNUSED_PARAM (u4Type);
    return;
}

/************************************************************************
 *  Function Name   : IgmpRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the IGMP module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
IgmpRedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : IgmpRedRmRegisterProtocols                        
 *                                                                      
 *  Description     : This function is invoked by the IGMP module to    
 *                    register itself with the RM module. This          
 *                    registration is required to send/receive peer     
 *                    synchronization messages and control events       
 *                                                                      
 *  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       
 *                                                                      
 *  Output(s)       : None.                                             
 *                                                                      
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       
 ************************************************************************/

PUBLIC INT4
IgmpRedRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : IgmpRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the IGMP module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register IGMP with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
IgmpRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : IgmpRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to IGMP        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
IgmpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : IgmpRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the IGMP module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the IGMP Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
IgmpRedHandleRmEvents (VOID)
{
    return;
}

/************************************************************************
 * Function Name      : IgmpRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the IGMP upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
IgmpRedHandleGoActive (VOID)
{
    return;
}

/************************************************************************
 * Function Name      : IgmpRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the IGMP upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
IgmpRedHandleGoStandby (tIgmpRmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : IgmpStartTimers                            */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpStartTimers (VOID)
{
    return;
}

#ifdef IGMPPRXY_WANTED
/************************************************************************/
/* Function Name      : IgmpStartTimers                            */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpProxyStartTimers (VOID)
{
    return;
}
#endif
/************************************************************************/
/* Function Name      : IgmpRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the IGMP module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
IgmpRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : IgmpRedSendMsgToRm                        */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
IgmpRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : IgmpRedSendBulkReqMsg                            */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedSendBulkReqMsg (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pIgmpDataDesc - This is Igmp sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pIgmpDbNode - This is db node defined in the IGMP*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDbUtilAddTblNode (tDbTblDescriptor * pIgmpDataDesc,
                         tDbTblNode * pIgmpDbNode)
{
    UNUSED_PARAM (pIgmpDataDesc);
    UNUSED_PARAM (pIgmpDbNode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate IGMP dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedSyncDynInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedAddAllNodeInDbTbl (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedSendBulkUpdMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the IGMP offers an IP address         */
/*                      to the IGMP client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pIgmpBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
IgmpRedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessGrpInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node group node.                 */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessGrpInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessSourceInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node source node.                */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessSourceInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessSrcReporterInfo                   */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node source reporter node.       */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessSrcReporterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessQueryInfo                         */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node query node.                 */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessQueryInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

#ifdef IGMPPRXY_WANTED
/************************************************************************/
/* Function Name      : IgpRedProcessUpIntInfo                          */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node upstream interface node.    */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgpRedProcessUpIntInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : IgpRedProcessInfo                               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node global info.                */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgpRedProcessInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : IgpRedProcessFwdEntryInfo                       */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node forward entry node.         */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgpRedProcessFwdEntryInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************
 * Function Name      : IgmpRedHwAudit                                  
 *                                                                      
 * Description        : This function does the hardware audit in the    
 *                      following approach.                             
 *                                                                      
 *                      When there is a transaction between standby and 
 *                      active node, the IgmpRedTable is walked, if     
 *                      there  are any entries in the table, they are   
 *                      verified with the hardware, if the entry is     
 *                      present in the hardware, then the entry is      
 *                      added to the sofware. If not, the entry is      
 *                      deleted.                                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/
VOID
IgmpRedHwAudit (VOID)
{
    return;
}

/*-------------------------------------------------------------------+
 * Function           : IgmpRBTreeRedEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : IGMP_RB_LESSER  if pRBElem <  pRBElemIn
 *                      IGMP_RB_GREATER if pRBElem >  pRBElemIn
 *                      IGMP_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy cache entries in lexicographic
 * order of the indices of the Redundancy cache entry.
+-------------------------------------------------------------------*/

INT4
IgmpRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    UNUSED_PARAM (pRBElem);
    UNUSED_PARAM (pRBElemIn);
    return IGMP_RB_EQUAL;
}
#endif
#endif
