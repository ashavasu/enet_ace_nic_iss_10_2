/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpqury.c,v 1.30 2017/02/10 12:50:23 siva Exp $
 *
 * Description:This file contains routines  for processing      
 *             IGMP Queries                                     
 *
 *******************************************************************/

#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_QRY_MODULE ;
#endif

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpProcessQuery                                   */
/*                                                                         */
/*     Description   :  Process the received Query Message                 */
/*                                                                         */
/*     Input(s)      :  InterfaceId on which the message is received.      */
/*                      u4Group: Group Address to which the query is       */
/*                                                   associated .          */
/*                      u4SrcAddr: Source Address of the message.          */
/*                      u1MaxRespTime : Response time in packet.           */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpProcessQuery                            **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpProcessQuery (tIgmpIface * pIfNode, tIgmpIPvXAddr u4Group,
                  tIgmpIPvXAddr u4SrcAddr, UINT4 u4NoOfSrcs,
                  UINT1 *pu1Sources, UINT1 u1Srsp, UINT1 u1Version)
{
    tIgmpQuery         *pIgmp = NULL;
    UINT4               u4SrcAddrPos = 0;
    UINT4               u4SrcIndex = IGMP_ZERO;
    UINT4               u4TmpAddr = IGMP_ZERO;
    UINT4              *pu4SrcArray = NULL;
    tIgmpIPvXAddr       u4Source;
    tIgmpSource        *pSrc = NULL;
    tIgmpGroup         *pGrp = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProcessQuery()\r \n");
    pIgmp = pIfNode->pIfaceQry;

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (pIfNode->u4IfIndex) == IGMP_TRUE))
    {
        IgpUpIfProcessQuery (pIfNode->u4IfIndex, u4Group, u4NoOfSrcs,
                             ((UINT4 *) (VOID *) pu1Sources), u1Version);
        return;
    }

    /* If the router is not explicitily configured to use the incoming version
     * of query, it should log a warning and return */
    if (pIfNode->u1Version != u1Version)
    {
        pIfNode->u4WrongVerQueries++;
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_QRY_MODULE, IGMP_NAME,
                  "Igmp query version is different from the router version\n");
        IGMP_TRC(MGMD_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                  "Igmp query version is different from the router version\n");
        pIfNode->u4QryVersionMisMatch++;
        MGMD_DBG (IGMP_DBG_EXIT, "IgmpProcessQuery\n");
        return;
    }

    /* If Query is from a higher IP address, just ignore that fellow */
    if (IGMP_IPVX_ADDR_COMPARE (u4SrcAddr, pIfNode->u4IfAddr) < IGMP_ZERO)
    {
        if (((IGMP_PROXY_STATUS () != IGMP_ENABLE) &&
             (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)) ||
            ((gIgmpConfig.i1MldProxyStatus != MLD_ENABLE) &&
             (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)))
        {
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_QRY_MODULE, IGMP_NAME,
                      "Query From Lower Address Router %s \n",
		      IgmpPrintIPvxAddress (u4SrcAddr));
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_Rx_MODULE, IgmpTrcGetModuleName(IGMP_Rx_MODULE),
                      "Query From Lower Address Router \n");
            pIfNode->u4QryFromNonQuerier++;
            /*
             * It seems to be from a lower address fellow.
             * If we are the Querier, let him continue querying, it is time to 
             * rest for a while.
             */

            /* If version 1, there is no querier election. Every IGMP
             * router on the interface acts as the querier */
            if (u1Version != IGMP_VERSION_1)
            {
                if (pIgmp->u1Querier == TRUE)
                {
                    IgmpHdlTransitionToNonQuerier (pIfNode);
                }

                /* update querier address */
                IGMP_IPVX_ADDR_COPY (&(pIgmp->u4QuerierAddr), &u4SrcAddr);
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgmp->QuerierDbNode);
                IgmpRedSyncDynInfo ();
            }

            if (((pIfNode->u1Version == IGMP_VERSION_3) &&
                 (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)) ||
                ((pIfNode->u1Version == MLD_VERSION_2) &&
                 (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)))
            {
                /* Querier's robustness variable, section 4.1.6 */
                if (pIgmp->u1QRV != IGMP_ZERO)
                {
                    pIfNode->u1Robustness = pIgmp->u1QRV;
                }
                else
                {
                    /* RFC 3376 : Section 4.1.6:
                     * Robust variable should return to default when
                     * the QVR of a received IGMP pkt is 0 */
                    pIfNode->u1Robustness = IGMP_DEFAULT_ROB_VARIABLE;
                } 
                /* Querier's Query Interval Code, section 4.1.7 */
                if (pIgmp->u1QQIC != IGMP_ZERO)
                {
                    if (pIgmp->u1QQIC >= IGMPV3_MAX_RESPONSE_CODE)
                    {
                        pIfNode->u2QueryInterval = (UINT2)
                            IGMP_GET_QQI_FROM_QQIC (pIgmp->u1QQIC);
                    }
                    else
                    {
                        pIfNode->u2QueryInterval = (UINT2) pIgmp->u1QQIC;
                    }
                }
                else
                {
                    /* RFC 3376: Section 4.1.7
                     * Query interval should return to default when
                     * the QQIC of a received IGMP pkt is 0 */
                    pIfNode->u2QueryInterval = IGMP_DEFAULT_QUERY_INTERVAL;
                } 
            }

            /* If version 1, there is no querier election. Every IGMP
             * router on the interface acts as the querier */
            if (u1Version != IGMP_VERSION_1)
            {
                IGMPSTARTTIMER (pIgmp->Timer, IGMP_QUERIER_PRESENT_TIMER_ID,
                                (UINT2)
                                IGMP_IF_OTHER_QUERIER_PRESENT_TIMER (pIfNode),
                                IGMP_ZERO);
            }
        }
        else
        {

            if (IgpUtilSendQuerierTrap (pIfNode->u4IfIndex,
                                        u4SrcAddr) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_QRY_MODULE, IGMP_NAME,
                          "Unable to send Trap on detection of Querier "
                          "on downstream interface\n");
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_Tx_MODULE, IgmpTrcGetModuleName(IGMP_Tx_MODULE),
                          "Unable to send Trap on detection of Querier "
                          "on downstream interface\n");
                return;
            }
        }
    }

    if ((u1Srsp == IGMP_ZERO)
         && (pIgmp->u1Querier == IGMP_FALSE))
    {
        /* Membership Query with Suppress Router-side Processing Flag cleared */
        /* Updates timers, section 4.1.5 and 6.6.1 */
        /* Get the Group Node */

        pGrp = IgmpGrpLookUp (pIfNode, u4Group);
        if (pGrp == NULL)
        {
            return;
        }
        if (u4NoOfSrcs == IGMP_ZERO)
        {
            /* Group Timer is lowered to LMQT, section 6.6.1 */

            /* As per RFC:2236 sec 3, non-querier sets group timer
             * as [Last Member Query Count] times the [Max Response Time] if its
              * existing group membership timer is greater than [Last Member Query
              * Count] times the Max Response Time specified in the message*/
             if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
             {
                  IgmpReStartGrpTmrToLmqTmr (pIfNode, pGrp);
             }

        }
        else
        {
            /* Source Timer for sources are lowered to LMQT, section 6.6.1 */
            pu4SrcArray = ((UINT4 *) (VOID *) pu1Sources);
            for (u4SrcIndex = IGMP_ZERO; u4SrcIndex < u4NoOfSrcs; u4SrcIndex++)
            {
                IGMP_IPVX_ADDR_CLEAR (&u4Source);
                if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    u4TmpAddr = OSIX_NTOHL (pu4SrcArray[u4SrcIndex]);
                    IGMP_IPVX_ADDR_INIT_IPV4 (u4Source,
                                              IGMP_IPVX_ADDR_FMLY_IPV4,
                                              (UINT1 *) &u4TmpAddr);
                }
                else
                {
                    u4SrcAddrPos = (UINT2) (u4SrcIndex *
                                            (UINT1) IPVX_IPV6_ADDR_LEN);
                    IPVX_ADDR_INIT_FROMV6 (u4Source,
                                           (pu1Sources + u4SrcAddrPos));
                }
                pSrc = IgmpSourceLookup (pGrp, u4Source, gIPvXZero);
                if ((pSrc != NULL) &&
                    ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET))
                {
                    IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                }
            }
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpProcessQuery()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpSendGroupSpecificQuery                         */
/*                                                                         */
/*     Description   :  This function schedules a grp sp. query to be sent */
/*                                                                         */
/*     Input(s)      :  Pointer to the multicast interface on which        */
/*                      the group specific query is to be sent             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpSendGroupSpecificQuery                  **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpSendGroupSpecificQuery (tIgmpIface * pIfNode, tIgmpGroup * pGrp)
{
    tIgmpSchQuery      *pSchQuery = NULL;
    /* if the query is a GroupAndSource specific query to be scheduled and if 
     * a query for this groupandsource is already sheduled, just ignore  
     */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpSendGroupSpecificQuery()\r \n");
    pSchQuery = IgmpGetSchQueryGrp (pIfNode->pIfaceQry, pGrp->u4Group);
    if (pSchQuery == NULL)
    {
        IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSchQueryId, pSchQuery,
                           tIgmpSchQuery);
        if (pSchQuery == NULL)
        {
            return;
        }
        /* Initialise the sll node */
        TMO_SLL_Init_Node (&pSchQuery->Link);
        pSchQuery->pIgmpQuery = pIfNode->pIfaceQry;
        /*  copy the group address into the node */
        IGMP_IPVX_ADDR_COPY (&(pSchQuery->u4Group), &(pGrp->u4Group));
        /* Initialise the source list in the query node */
        TMO_SLL_Init (&(pSchQuery->SrcList));
        /* add the node to the list of scheduled queries */
        TMO_SLL_Add (&(pIfNode->pIfaceQry->schQueryList), &pSchQuery->Link);
    }

    /*  copy the transmission number into the node */
    pSchQuery->u1RetrNum = pIfNode->u1Robustness;

    IgmpMldSendSchQuery (pIfNode, pSchQuery);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpSendGroupSpecificQuery()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpMldSendSchQuery                               */
/*                                                                         */
/*     Description   :   This function is used to send the scheduled query */
/*     Input(s)      :   pSchQuery - Scheduled query info                  */
/*                       pIfNode - Interface node                          */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
VOID
IgmpMldSendSchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery)
{
    UINT1               u1AddrType = 0;
    UINT1               u1Version = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMldSendSchQuery()\r \n");
    u1AddrType = pSchQuery->pIgmpQuery->pIfNode->u1AddrType;
    u1Version = pSchQuery->pIgmpQuery->pIfNode->u1Version;

    switch (u1AddrType)
    {
        case IPVX_ADDR_FMLY_IPV4:

            if (u1Version == IGMP_VERSION_3)
            {
                IgmpTxSchQuery (pIfNode, pSchQuery);
            }
            else if (u1Version == IGMP_VERSION_2)
            {
                IgmpTxV2SchQuery (pIfNode, pSchQuery);
            }
            break;

        case IPVX_ADDR_FMLY_IPV6:

            if (u1Version == MLD_VERSION_2)
            {
                MldOutTxV2SchQuery (pIfNode, pSchQuery);
            }
            else if (u1Version == MLD_VERSION_1)
            {
                MldOutTxV1SchQuery (pIfNode, pSchQuery);
            }
            break;
        default:
            break;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMldSendSchQuery()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpSendGroupAndSourceSpecificQuery                */
/*                                                                         */
/*     Description   :  This function schedules a grp. and Source sp.      */
/*                 query to be sent on a particular interface         */
/*                                                                         */
/*     Input(s)      :  pMcast - Pointer to the multicast interface on     */
/*             which the group specific query is to be sent       */
/*             u4NoOfSrcs - No. of sources                */
/*             pu1Sources - Source Addresses               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpSendGroupAndSourceSpecificQuery         **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpSendGroupAndSourceSpecificQuery (tIgmpIface * pIfNode,
                                     tIgmpGroup * pGrp,
                                     UINT4 u4NoOfSrcs, tIgmpIPvXAddr * pSources)
{
    /* Schedule a query for this group and the input sources 
     * on the    interface  by invoking 
     */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpSendGroupAndSourceSpecificQuery()\r \n");
    if ((pIfNode->u1Version == MLD_VERSION_2) || 
        ((pIfNode->u1Version == IGMP_VERSION_3) &&
            (pGrp->u1V2HostPresent == IGMP_FALSE)))
    {
    IgmpScheduleQuery (pIfNode, pGrp, u4NoOfSrcs, pSources);
    }
    else
    {
        IgmpSendGroupSpecificQuery (pIfNode, pGrp);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpSendGroupAndSourceSpecificQuery()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpScheduleQuery                             */
/*                                                                         */
/*     Description   :  This function schedules a the queries on a         */
/*                 interface                                          */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*             be scheduled                         */
/*            u4GroupAddr - the group addres for which the query */
/*             is to be scheduled                    */
/*             u4NoOfSrcs - No. of sources                */
/*             pu1Sources - Source Addresses               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpScheduleQuery                      **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
INT4
IgmpScheduleQuery (tIgmpIface * pIfNode,
                   tIgmpGroup * pGrp, UINT4 u4NoOfSrcs,
                   tIgmpIPvXAddr * pSources)
{
    tIgmpQuery         *pIgmpQuery = NULL;
    tIgmpSchQuery      *pSchQuery = NULL;
    tSourceNode        *pSrcNode = NULL;
    UINT1               u1NewGrpFlag = IGMP_FALSE;
    UINT1               u1NewSrcFlag = IGMP_FALSE;
    UINT4               u4SrcIndex = IGMP_ZERO;
    tIgmpIPvXAddr       u4GroupAddr;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpScheduleQuery()\r \n");
    IGMP_IPVX_ADDR_CLEAR (&u4GroupAddr);
    IGMP_IPVX_ADDR_COPY (&u4GroupAddr, &(pGrp->u4Group));

    /* get the query block for this interface */
    pIgmpQuery = pIfNode->pIfaceQry;

    if ((u4NoOfSrcs == IGMP_ZERO) || (pIgmpQuery->u1Querier != TRUE))
    {
        return IGMP_OK;
    }

    /* if the query is a GroupAndSource specific query to be scheduled and if 
     * a query for this groupandsource is already sheduled, just ignore  
     */
    pSchQuery = IgmpGetSchQueryGrp (pIgmpQuery, u4GroupAddr);
    if (pSchQuery == NULL)
    {
        IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSchQueryId, pSchQuery,
                           tIgmpSchQuery);
        if (pSchQuery == NULL)
        {
            return IGMP_NOT_OK;
        }

        u1NewGrpFlag = IGMP_TRUE;
        /* Initialise the sll node */
        TMO_SLL_Init_Node (&pSchQuery->Link);
        pSchQuery->pIgmpQuery = pIgmpQuery;
        /*  copy the group address into the node */
        IGMP_IPVX_ADDR_COPY (&(pSchQuery->u4Group), &u4GroupAddr);
        /*  copy the transmission number into the node */
        pSchQuery->u1RetrNum = IGMP_ZERO;
        pSchQuery->u2Reserved = IGMP_ZERO;
        pSchQuery->u1Reserved = IGMP_ZERO;
        /* Initialise the source list in the query node */
        TMO_SLL_Init (&(pSchQuery->SrcList));
    }

    for (u4SrcIndex = IGMP_ZERO; u4SrcIndex < u4NoOfSrcs; u4SrcIndex++)
    {
        if (IgmpCheckSourceInSchQueryList (pSources[u4SrcIndex],
                                           pSchQuery) != TRUE)
        {
            IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSchQuerySourceId,
                               pSrcNode, tSourceNode);
            if (pSrcNode == NULL)
            {
                /* Can think about a better error handling scenario here
                 * One option is why not send the query for this source
                 * immediately. without any schedule query node.
                 */
                continue;
            }
            u1NewSrcFlag = IGMP_TRUE;
            /* Initialise the sll node */
            TMO_SLL_Init_Node (&pSrcNode->Link);
            /* fill the source addrress in the node */
            pSrcNode->pSrc = IgmpGetSource (pGrp, pSources[u4SrcIndex]);
            IGMP_IPVX_ADDR_COPY (&(pSrcNode->u4Source),
                                 &(pSources[u4SrcIndex]));
            /* Add this node to the source list */
            TMO_SLL_Add (&(pSchQuery->SrcList), &pSrcNode->Link);
            pSrcNode->u1RetrNum = pIfNode->u1Robustness;
        }
    }
    if ((u1NewGrpFlag == IGMP_TRUE) || (u1NewSrcFlag == IGMP_TRUE))
    {
        if (TMO_SLL_Count (&(pSchQuery->SrcList)) != IGMP_ZERO)
        {
            if (u1NewGrpFlag == IGMP_TRUE)
            {
            /* add the node to the list of scheduled queries */
                TMO_SLL_Add (&(pIgmpQuery->schQueryList), 
                             &pSchQuery->Link);
            /* Set the timer Id in the schTimer of the node to 
             * IGMP_QUERY_SCHEDULE_TIMER_ID 
             */
            pSchQuery->schTimer.TimerBlk.u1TimerId =
                IGMP_QUERY_SCHEDULE_TIMER_ID;
            }
            /* Invoke IgmpTxSchQuery to send a query and schedule 
             * next queries 
             */
            if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                IgmpTxSchQuery (pIfNode, pSchQuery);
            }
            else
            {
                MldOutTxV2SchQuery (pIfNode, pSchQuery);
            }
        }
        else
        {
            IgmpMemRelease (gIgmpMemPool.IgmpSchQueryId, (UINT1 *) pSchQuery);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpScheduleQuery()\r \n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpGetSchQueryGrp                                 */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer             */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*             be scheduled                                                */
/*            u4GroupAddr - the group addres for which the query           */
/*             is to be scheduled                                          */
/*             u4NoOfSrcs - No. of sources                                 */
/*             pu1Sources - Source Addresses                               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGetSchQueryGrp                          **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
tIgmpSchQuery      *
IgmpGetSchQueryGrp (tIgmpQuery * pIgmpQuery, tIgmpIPvXAddr u4GroupAddr)
{
    tIgmpSchQuery      *pSchQuery = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpGetSchQueryGrp()\r \n");
    TMO_SLL_Scan (&(pIgmpQuery->schQueryList), pSchQuery, tIgmpSchQuery *)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pSchQuery->u4Group, u4GroupAddr) ==
            IGMP_ZERO)
        {
            break;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpGetSchQueryGrp()\r \n");
    return pSchQuery;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpGetSchQueryGrp                                 */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer             */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*             be scheduled                                                */
/*            u4GroupAddr - the group addres for which the query           */
/*             is to be scheduled                                          */
/*             u4NoOfSrcs - No. of sources                                 */
/*             pu1Sources - Source Addresses                               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGetSchQueryGrp                          **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpDelSchQrySrcNode (tIgmpSource * pSrc)
{
    tIgmpSchQuery      *pSchQuery = NULL;
    tSourceNode        *pSrcQry = NULL;
    tIgmpQuery         *pIgmpQry = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpDelSchQrySrcNode()\r \n");
    pIgmpQry = pSrc->pGrp->pIfNode->pIfaceQry;

    TMO_SLL_Scan (&(pIgmpQry->schQueryList), pSchQuery, tIgmpSchQuery *)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pSchQuery->u4Group,
                                    pSrc->pGrp->u4Group) == IGMP_ZERO)
        {
            TMO_SLL_Scan (&(pSchQuery->SrcList), pSrcQry, tSourceNode *)
            {
                if (IGMP_IPVX_ADDR_COMPARE (pSrcQry->u4Source,
                                            pSrc->u4SrcAddress) == IGMP_ZERO)
                {
                    break;
                }
            }
            break;
        }
    }

    if (pSrcQry == NULL)
    {
        return;
    }

    TMO_SLL_Delete (&(pSchQuery->SrcList), &pSrcQry->Link);
    IgmpMemRelease (gIgmpMemPool.IgmpSchQuerySourceId, (UINT1 *) pSrcQry);

    if ((TMO_SLL_Count (&(pSchQuery->SrcList)) == IGMP_ZERO) &&
        (pSchQuery->u1RetrNum == IGMP_ZERO))
    {
        IGMPSTOPTIMER (pSchQuery->schTimer);

        TMO_SLL_Delete (&(pIgmpQry->schQueryList), &pSchQuery->Link);
        IgmpMemRelease (gIgmpMemPool.IgmpSchQueryId, (UINT1 *) pSchQuery);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpDelSchQrySrcNode()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpDelSchQryGrpNode                               */
/*                                                                         */
/*     Description   :  This function removes the Scheduled Query Node     */
/*                      after LMQI and robustness vlaue expires            */
/*                                                                         */
/*     Input(s)      :  pGrp - Group node for which query is scheduled     */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
    /***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpDelSchQryGrpNode                        **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpDelSchQryGrpNode (tIgmpGroup * pGrp)
{
    tIgmpSchQuery      *pSchQuery = NULL;
    tIgmpQuery         *pIgmpQry = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG,IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpDelSchQryGrpNode()\r \n");

    pIgmpQry = pGrp->pIfNode->pIfaceQry;

    TMO_SLL_Scan (&(pIgmpQry->schQueryList), pSchQuery, tIgmpSchQuery *)
    {
        if (IGMP_IPVX_ADDR_COMPARE (pSchQuery->u4Group,
                                    pGrp->u4Group) == IGMP_ZERO)
        {
            break;
        }
    }

    if (pSchQuery != NULL)
    {
    	IGMPSTOPTIMER (pSchQuery->schTimer);
    	TMO_SLL_Delete (&(pIgmpQry->schQueryList), &pSchQuery->Link);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpDelSchQryGrpNode()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpHdlTransitionToNonQuerier                      */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer             */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*             be scheduled                                                */
/*            u4GroupAddr - the group addres for which the query           */
/*             is to be scheduled                                          */
/*             u4NoOfSrcs - No. of sources                                 */
/*             pu1Sources - Source Addresses                               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpHdlTransitionToNonQuerier               **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpHdlTransitionToNonQuerier (tIgmpIface * pIfNode)
{
    tIgmpSchQuery      *pSchQuery = NULL;
    tSourceNode        *pSrcQry = NULL;
    tIgmpQuery         *pIgmp = pIfNode->pIfaceQry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHdlTransitionToNonQuerier()\r \n");
    pIgmp->u1Querier = FALSE;
    /* We have to stop the periodic Query timer. */

    if ((pIgmp->Timer).u1TmrStatus == IGMP_TIMER_SET)
    {
        IGMPSTOPTIMER (pIgmp->Timer);
    }
    /* Make startup query count to zero */
    pIgmp->u1StartUpQCount = IGMP_ZERO;

    /* All the queries that were scheduled have to be stopped */
    while ((pSchQuery = (tIgmpSchQuery *)
            TMO_SLL_First (&(pIgmp->schQueryList))) != NULL)
    {
        IGMPSTOPTIMER (pSchQuery->schTimer);

        while ((pSrcQry = (tSourceNode *)
                TMO_SLL_First (&(pSchQuery->SrcList))) != NULL)
        {
            TMO_SLL_Delete (&(pSchQuery->SrcList), &pSrcQry->Link);
            IgmpMemRelease (gIgmpMemPool.IgmpSchQuerySourceId,
                            (UINT1 *) pSrcQry);
        }

        IGMPSTOPTIMER (pIgmp->Timer);

        TMO_SLL_Delete (&(pIgmp->schQueryList), &pSchQuery->Link);
        IgmpMemRelease (gIgmpMemPool.IgmpSchQueryId, (UINT1 *) pSchQuery);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpHdlTransitionToNonQuerier()\r \n");
}
