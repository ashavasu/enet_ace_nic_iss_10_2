
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpfsm.c,v 1.23 2017/02/06 10:45:27 siva Exp $
 *
 * Description:This file contains the routines required for
 *
 *******************************************************************/

#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE ;
#endif
tIgmpFSMFnPtr       gaIgmpFSM[IGMP_MAX_STATE][IGMP_MAX_EVENTS] = {
    /* Dummy State */
    {
     NULL, NULL, NULL, NULL, NULL, NULL, NULL}
    ,
    /* INCLUDE state */
    {
     NULL,                        /* Dummy Event */
     IgmpHandleIsIncludeStateIn, IgmpHandleIsExcludeStateIn,
     IgmpHandleToIncludeStateIn, IgmpHandleToExcludeStateIn,
     IgmpHandleAllowStateIn, IgmpHandleBlockStateIn}
    ,
    /* EXCLIUDE state */
    {
     NULL,                        /* Dummy Event */
     IgmpHandleIsIncludeStateEx, IgmpHandleIsExcludeStateEx,
     IgmpHandleToIncludeStateEx, IgmpHandleToExcludeStateEx,
     IgmpHandleAllowStateEx, IgmpHandleBlockStateEx}

};

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleIsIncludeStateIn                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleIsIncludeStateIn (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    INT4                i4Status = IGMP_OK;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleIsIncludeStateIn()\r \n");
    i4Status = IgmpAction1 (pIgmpFSMInfoNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleIsIncludeStateIn()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleIsExcludeStateIn                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleIsExcludeStateIn (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpIPvXAddr       SrcAddr;
    tIgmpIPvXAddr       u4Reporter;
    tIgmpSource        *pSrc = NULL;
    tIgmpIPvXAddr      *pOldSrcSet = gpu4SrcSetArray;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT1              *pu1Sources = NULL;
    UINT4               u4OldSrcCount = IGMP_ZERO;
    UINT4               u4Index = IGMP_ZERO;
    INT4                i4Status = IGMP_OK;
    UINT4               u4NoOfSources = IGMP_ZERO;
    UINT4               u4SrcAddrPos = 0;
    UINT4               u4Count = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleIsExcludeStateIn()\r \n");
    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    pGrp->u1FilterMode = IGMP_FMODE_EXCLUDE;

    if (u4NoOfSources == IGMP_ZERO)
    {
        /* V2 Join Report(*,G) */
        if (IGMP_PROXY_STATUS () == IGMP_DISABLE)
        {
            /* Updating MRPs  */
            IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                      "Updated to MRPs.\n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                      "Updated to MRPs.\n");
        }
		if(IgmpAddV1V2Reporter(pGrp, u4Reporter) == IGMP_NOT_OK)
		{
			IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
					"Reporter (%s) addition failed for Group %s added in i/f %d \n",
					IgmpPrintIPvxAddress (u4Reporter), IgmpPrintIPvxAddress (pGrp->u4Group),
					pIfNode->u4IfIndex);
		}
    }

    /* gets the old source set */
    TMO_SLL_Scan (&pGrp->srcList, pSrc, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_COPY (&(pOldSrcSet[u4Index++]), &(pSrc->u4SrcAddress));
    }
    u4OldSrcCount = TMO_SLL_Count (&pGrp->srcList);

    if (IGMP_PROXY_STATUS () == IGMP_DISABLE)
    {
        /* This for loop performs (A*B) updation to MRP B'cause of change
         * in Group Filter Mode to EXCLUDE.
         * A is each element in the pOldSrcSet and 
         * B is each element in the received pu1Sources 
         */
        for (u4Index = IGMP_ZERO; u4Index < u4OldSrcCount; u4Index++)
        {
            if (((pSrc = IgmpGetSource (pGrp, pOldSrcSet[u4Index])) != NULL) &&
                (IgmpCheckSourceInArray (pOldSrcSet[u4Index], pu1Sources,
                                         u4NoOfSources) == IGMP_TRUE))
            {
                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
            }
        }
        /*(A*B) = 0 */
        if (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) != IGMP_ZERO)
        {
            /* Updating MRPs */
            IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                      "Updated to MRPs.\n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                      "Updated to MRPs.\n");
        }
    }

    /* This for loop performs Delete (A-B) 
     * A is each element in the pOldSrcSet and 
     * B is each element in the received pu1Sources 
     */
    for (u4Index = IGMP_ZERO; u4Index < u4OldSrcCount; u4Index++)
    {
        if (u4NoOfSources != IGMP_ZERO)
        {
            if (IgmpCheckSourceInArray (pOldSrcSet[u4Index], pu1Sources,
                                        u4NoOfSources) == IGMP_TRUE)
            {
                continue;
            }
        }

        if ((pSrc = IgmpGetSource (pGrp, pOldSrcSet[u4Index])) != NULL)
        {
            /* delete (A-B) */
            if (pSrc->u1StaticFlg == IGMP_FALSE)
            {
                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                if (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) != IGMP_ZERO)
                {
                    /* Updating MRPs for A-B */
                    IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                            "Updated to MRPs.\n");
                    IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                            "Updated to MRPs.\n");
                }
                IgmpDeleteSource (pGrp, pSrc);
            }
        }
    }

    /* This for loop calculates (B - A)
     * Here B is the list that we are running in the for loop.
     * And A is the set of all sources in the pOldSrcSet 
     */
    for (u4Index = IGMP_ZERO; u4Index < u4NoOfSources; u4Index++)
    {
        IGMP_IPVX_ADDR_CLEAR (&SrcAddr);

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                                      (pu1Sources +
                                       (IPVX_IPV4_ADDR_LEN * u4Index)));
        }
        else
        {
            u4SrcAddrPos = (UINT2) (u4Index * (UINT1) IPVX_IPV6_ADDR_LEN);
            IPVX_ADDR_INIT_FROMV6 (SrcAddr,
                                   ((pIgmpFSMInfoNode->pu1Sources) +
                                    u4SrcAddrPos));
        }

        pSrc = IgmpAddSource (pGrp, SrcAddr, u4Reporter, IGMP_TRUE);
        if (pSrc == NULL)
        {
            continue;
        }

        if (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                   pOldSrcSet) == IGMP_TRUE)
        {
            /* If the report has come with exclude mode change for the
             * source address already present in database, we need
             * to check whether there are other receivers for the same
             * source. Only if there are no other receivers the
             * forward state can be set as false and MRP should be 
             * informed to prune the source entry */
			RBTreeCount (pSrc->SrcReporterTree, &u4Count);
            if (u4Count == IGMP_ONE)
            {
                /* Stop the source timer; */
                if (pSrc->srcTimer.u1TmrStatus == IGMP_TMR_SET)
                {
                    IGMPSTOPTIMER (pSrc->srcTimer);
                    pSrc->u1FwdState = IGMP_FALSE;
                    pSrc->u1UpdatedToMrp = IGMP_FALSE;
                }

                if ((pSrc->u1UpdatedToMrp == IGMP_FALSE) &&
                    (IGMP_PROXY_STATUS () == IGMP_DISABLE))
                {
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                }
            }
        }
    }

    /*(B-A) = 0 */
    if ((TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) != IGMP_ZERO) &&
        (IGMP_PROXY_STATUS () == IGMP_DISABLE))
    {
        /* Updating MRPs */
        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,IGMP_NAME,
                  "Updated to MRPs.\n");
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE, IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                  "Updated to MRPs.\n");
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleIsExcludeStateIn()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleToIncludeStateIn                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleToIncludeStateIn (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tIgmpIPvXAddr      *pOldSrcSet = gpu4SrcSetArray;
    UINT4               u4NumSrc = IGMP_ZERO;
    tIgmpIPvXAddr      *pSrcSet = NULL;
    UINT4               u4OldSrcCount = IGMP_ZERO;
    UINT1               u1SrcDelFlag = IGMP_FALSE;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    UINT1              *pu1Sources = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;
    UINT4               u4Index = IGMP_ZERO;
    INT4                i4Status = IGMP_OK;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleToIncludeStateIn()\r \n");
    /* Initialize the source */
    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    /* The below condition is fine because Nothing needs to be changed
     * in the group's source list for V3. Only a query needs to be sent
     * for (A - B) {but B here being NULL}. Query is sent further up in this
     * function
     */
    if (u4NoOfSources == IGMP_ZERO)
    {
        if ((pGrp->u4GroupCompMode == IGMP_GCM_IGMPV2) &&
            (IGMP_PROXY_STATUS () == IGMP_DISABLE))
        {
            /* Updating MRPs  */
            IgmpUpdateMrps (pGrp, IGMP_LEAVE);
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                      "Updated To MRPs.\n");
	    IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                      "Updated To MRPs.\n");
        }
    }
    else
    {
        i4Status = IgmpAction1 (pIgmpFSMInfoNode);
        if (i4Status == IGMP_NOT_OK)
        {
            return i4Status;
        }
    }

    /* Copy all the source records to the pOldSrcSet */
    TMO_SLL_Scan (&pGrp->srcList, pSrc, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_COPY (&(pOldSrcSet[u4Index++]), &(pSrc->u4SrcAddress));
    }
    u4OldSrcCount = TMO_SLL_Count (&(pGrp->srcList));

    /* send group and source specific querry Q(G,A-B) */
    /* we don't know (A-B)NoOfSrcs since we maintain a common list,
     * this loop will get it.
     */
    for (pSrc = (tIgmpSource *) TMO_SLL_First (&(pGrp->srcList));
         pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
    {
        pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);
        u1SrcDelFlag = IGMP_FALSE;

        if ((IgmpCheckSourceInList
             (pSrc->u4SrcAddress, u4OldSrcCount, pOldSrcSet) == TRUE) &&
            (IgmpCheckSourceInArray (pSrc->u4SrcAddress,
                                     pu1Sources, u4NoOfSources) == FALSE))
        {
            if ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
                (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
            {
                IgmpDeleteReporter (pIfNode, pSrc, u4Reporter);
                u1SrcDelFlag = IGMP_TRUE;
            }
            if ((u1SrcDelFlag == IGMP_FALSE) ||
                (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))))
            {
                /* Source Timer lowered to LMQT */
                IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                pSrc->u1QryFlag = IGMP_TRUE;
                u4NumSrc++;
            }
        }
    }

    /* Now we know NoOfSrcs, so allocate memory */
    /* Allocate memory for the source list */
    if (u4NumSrc > IGMP_ZERO)
    {
        pSrcSet = gpu4SrcSetArray;
        u4NumSrc = IGMP_ZERO;

        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1QryFlag == IGMP_TRUE)
            {
                IGMP_IPVX_ADDR_COPY (&(pSrcSet[u4NumSrc++]),
                                     &(pSrc->u4SrcAddress));
                pSrc->u1QryFlag = IGMP_FALSE;
            }
        }

        if (u4NumSrc > IGMP_ZERO)
        {
            /* Sends GrpAndSrc Qry  */
            IgmpSendGroupAndSourceSpecificQuery (pIfNode, pGrp,
                                                 u4NumSrc, pSrcSet);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleToIncludeStateIn()\r \n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleToExcludeStateIn                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleToExcludeStateIn (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    INT4                i4Status = IGMP_NOT_OK;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleToExcludeStateIn()\r \n");
    i4Status = IgmpToExclude (pIgmpFSMInfoNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleToExcludeStateIn()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleAllowStateIn                          */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleAllowStateIn (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    INT4                i4Status = IGMP_OK;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleAllowStateIn()\r \n");
    i4Status = IgmpAction1 (pIgmpFSMInfoNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleAllowStateIn()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleBlockStateIn                          */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleBlockStateIn (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    INT4                i4Status = IGMP_NOT_OK;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleBlockStateIn()\r \n");
    i4Status = IgmpBlockInclude (pIgmpFSMInfoNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleBlockStateIn()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleIsIncludeStateEx                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleIsIncludeStateEx (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    INT4                i4Status = IGMP_OK;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleIsIncludeStateEx()\r \n");
    i4Status = IgmpAction1 (pIgmpFSMInfoNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleIsIncludeStateEx()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleIsExcludeStateEx                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleIsExcludeStateEx (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tIgmpIPvXAddr      *pOldSrcSet = gpu4SrcSetArray;
    UINT4               u4OldSrcCount = IGMP_ZERO;
    UINT4               u4Index1 = IGMP_ZERO;
    UINT4               u4Index = IGMP_ZERO;
    tIgmpIPvXAddr       SrcAddr;
    UINT1              *pu1Sources = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;
    UINT4               u4SrcAddrPos = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleIsExcludeStateEx()\r \n");
    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    /* Copy all the source records to the pOldSrcSet */
    TMO_SLL_Scan (&pGrp->srcList, pSrc, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_COPY (&(pOldSrcSet[u4Index++]), &(pSrc->u4SrcAddress));
    }
    u4OldSrcCount = TMO_SLL_Count (&(pGrp->srcList));

    for (u4Index = IGMP_ZERO; u4Index < u4NoOfSources; u4Index++)
    {
        /* Add the source to the interface source list */
        IGMP_IPVX_ADDR_CLEAR (&SrcAddr);

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                                      (pu1Sources +
                                       (IPVX_IPV4_ADDR_LEN * u4Index)));
        }
        else
        {
            u4SrcAddrPos = (UINT2) (u4Index * (UINT1) IPVX_IPV6_ADDR_LEN);
            IPVX_ADDR_INIT_FROMV6 (SrcAddr,
                                   ((pIgmpFSMInfoNode->pu1Sources) +
                                    u4SrcAddrPos));
        }

        pSrc = IgmpAddSource (pGrp, SrcAddr, u4Reporter, IGMP_TRUE);
        if (pSrc == NULL)
        {
            continue;
        }
        /* start source timer (A-X-Y) = GMI */
        if (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                   pOldSrcSet) == FALSE)
        {
            IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
                            IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
            pSrc->u1FwdState = IGMP_TRUE;

            if ((pSrc->u1UpdatedToMrp == IGMP_FALSE) &&
                (IGMP_PROXY_STATUS () == IGMP_DISABLE))
            {
                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
            }
        }
    }

    /* (A-X-Y) = GMI */
    if ((TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) > IGMP_ZERO) &&
        (IGMP_PROXY_STATUS () == IGMP_DISABLE))
    {
        /* Updating MRPs */
        IgmpUpdateMrps (pGrp, IGMP_JOIN);
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                  "Updated To MRPs.\n");
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE, IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                  "Updated To MRPs.\n");
    }

    /* Delete (X-A) 
     * Note: Since one list for the Sources, this 
     * Deletes (Y-A) also
     */
    for (u4Index1 = IGMP_ZERO; u4Index1 < u4OldSrcCount; u4Index1++)
    {
        if (u4NoOfSources != IGMP_ZERO)
        {
            if ((IgmpCheckSourceInArray (pOldSrcSet[u4Index1], pu1Sources,
                                         u4NoOfSources) == IGMP_TRUE))
            {
                continue;
            }
        }

        if ((pSrc = IgmpGetSource (pGrp, pOldSrcSet[u4Index1])) != NULL)
        {
            /* Deletes source Node */
            if (pSrc->u1StaticFlg == IGMP_FALSE)
            {
                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                if (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) != IGMP_ZERO)
                {
                    /* Updating MRPs for X-A */
                    IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                            "Updated to MRPs.\n");
                    IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                            "Updated to MRPs.\n");
                }
                IgmpDeleteSource (pGrp, pSrc);
            }
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleIsExcludeStateEx()\r \n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleToIncludeStateEx                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleToIncludeStateEx (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tIgmpIPvXAddr      *pOldSrcSet = gpu4SrcSetArray;
    UINT4               u4NumSrc = IGMP_ZERO;
    tIgmpIPvXAddr      *pSrcSet = NULL;
    UINT4               u4OldSrcCount = IGMP_ZERO;
    UINT1               u1SrcDelFlag = IGMP_FALSE;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    UINT1              *pu1Sources = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;
    UINT4               u4Index = IGMP_ZERO;
    INT4                i4Status = IGMP_ZERO;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleToIncludeStateEx()\r \n");
    /* Initialize the source */
    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    /* Copy all the source records to the pOldSrcSet */
    TMO_SLL_Scan (&pGrp->srcList, pSrc, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_COPY (&(pOldSrcSet[u4Index++]), &(pSrc->u4SrcAddress));
    }
    u4OldSrcCount = TMO_SLL_Count (&(pGrp->srcList));

    i4Status = IgmpAction1 (pIgmpFSMInfoNode);
    if (i4Status == IGMP_NOT_OK)
    {
        return i4Status;
    }
    /*send Q(G,X-A) */
    for (pSrc = (tIgmpSource *)
         TMO_SLL_First (&(pGrp->srcList));
         pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
    {
        pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);
        u1SrcDelFlag = IGMP_FALSE;

        if ((IgmpCheckSourceInArray (pSrc->u4SrcAddress, pu1Sources,
                                     u4NoOfSources) == FALSE) &&
            (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                    pOldSrcSet) == TRUE))
        {
            if ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET)
            {
                if ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
                    (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
                {
                    IgmpDeleteReporter (pIfNode, pSrc, u4Reporter);
                    u1SrcDelFlag = IGMP_TRUE;
                }

                if ((u1SrcDelFlag == IGMP_FALSE) ||
                    (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))))
                {
                    /* Source Timer lowered to LMQT */
                    IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                    pSrc->u1QryFlag = IGMP_TRUE;
                    u4NumSrc++;
                }
            }
        }
    }

    if (u4NumSrc > IGMP_ZERO)
    {
        pSrcSet = gpu4SrcSetArray;
        u4NumSrc = IGMP_ZERO;

        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1QryFlag == IGMP_TRUE)
            {
                IGMP_IPVX_ADDR_COPY (&(pSrcSet[u4NumSrc++]),
                                     &(pSrc->u4SrcAddress));
                pSrc->u1QryFlag = IGMP_FALSE;
            }
        }
        /*send Q(G,X-A) */
        if (u4NumSrc > IGMP_ZERO)
        {
            /* Sends GrpAndSrc Qry  */
            IgmpSendGroupAndSourceSpecificQuery (pIfNode, pGrp,
                                                 u4NumSrc, pSrcSet);
        }
    }
    if (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
    {
        /*send Q(G) */
        IgmpReStartGrpTmrToLmqTmr (pIfNode, pGrp);
        IgmpSendGroupSpecificQuery (pIfNode, pGrp);
    }
    pGrp->u1FilterMode = IGMP_FMODE_EXCLUDE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleToIncludeStateEx()\r \n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleToExcludeStateEx                      */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleToExcludeStateEx (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tIgmpIPvXAddr      *pOldSrcSet = gpu4SrcSetArray;
    UINT4               u4Index = IGMP_ZERO;
    UINT4               u4Index1 = IGMP_ZERO;
    UINT4               u4NumSrc = IGMP_ZERO;
    tIgmpIPvXAddr      *pSrcSet = NULL;
    UINT4               u4OldSrcCount = IGMP_ZERO;
    UINT1               u1NumOfSrcTobeUpdated = IGMP_ZERO;
    UINT1               u1SrcDelFlag = IGMP_FALSE;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    tIgmpIPvXAddr       SrcAddr;
    UINT1              *pu1Sources = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;
    UINT4               u4SrcAddrPos = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleToExcludeStateEx()\r \n");

    /* Initialize the source */
    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    /* Copy all the source records to the pOldSrcSet */
    TMO_SLL_Scan (&pGrp->srcList, pSrc, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_COPY (&(pOldSrcSet[u4Index++]), &(pSrc->u4SrcAddress));
    }
    u4OldSrcCount = TMO_SLL_Count (&(pGrp->srcList));

    /*(A-X-Y)=group timer */
    for (u4Index = IGMP_ZERO; u4Index < u4NoOfSources; u4Index++)
    {
        IGMP_IPVX_ADDR_CLEAR (&SrcAddr);

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                                      (pu1Sources +
                                       (IPVX_IPV4_ADDR_LEN * u4Index)));
        }
        else
        {
            u4SrcAddrPos = (UINT2) (u4Index * (UINT1) IPVX_IPV6_ADDR_LEN);
            IPVX_ADDR_INIT_FROMV6 (SrcAddr,
                                   ((pIgmpFSMInfoNode->pu1Sources) +
                                    u4SrcAddrPos));
        }

        pSrc = IgmpAddSource (pGrp, SrcAddr, u4Reporter, IGMP_TRUE);
        if (pSrc == NULL)
        {
            continue;
        }

        /*(A-X-Y)=group timer */
        if (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                   pOldSrcSet) == FALSE)
        {
            IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
                            IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);

            pSrc->u1FwdState = IGMP_TRUE;
            if ((pSrc->u1UpdatedToMrp == IGMP_FALSE) &&
                (IGMP_PROXY_STATUS () == IGMP_DISABLE))
            {
                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                            gIPvXZero) != IGMP_ZERO)
                {
                    /* Src Address 0.0.0.0 is added for EXCL NONE report.
                     * If 0.0.0.0 address is added to MRP, other sources
                     * need not be added, since EXCL NONE is superset */
                    break;
                }
            }
        }
    }

    /* (A-X-Y) = GMI */
    if ((TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) > IGMP_ZERO) &&
        (IGMP_PROXY_STATUS () == IGMP_DISABLE))
    {
        /* Updating MRPs */
        IgmpUpdateMrps (pGrp, IGMP_JOIN);
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Updated to MRPs.\n");
    }

    u1NumOfSrcTobeUpdated = IGMP_ZERO;

    /* On receiving (EXCL (G,NONE) report add a source as 0.0.0.0
     * with the incoming reporter IP */
    if ((IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)) &&
        (u4NoOfSources == IGMP_ZERO))
    {
        IGMP_IPVX_ADDR_CLEAR (&SrcAddr);
        if ((pSrc = IgmpAddSource (pGrp, SrcAddr, u4Reporter, IGMP_FALSE)) != NULL)
        {
            IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
                            IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
        }
    }

    /* delete( X-A), also delete (Y-A), b'coz common src list */
    for (u4Index1 = IGMP_ZERO; u4Index1 < u4OldSrcCount; u4Index1++)
    {
        if (u4NoOfSources != IGMP_ZERO)
        {
            if (IgmpCheckSourceInArray (pOldSrcSet[u4Index1], pu1Sources,
                                        u4NoOfSources) == IGMP_TRUE)
            {
                continue;
            }
        }
        if ((u4NoOfSources == IGMP_ZERO) && (pSrc != NULL) &&
            (IGMP_IPVX_ADDR_COMPARE
             (pSrc->u4SrcAddress, gIPvXZero) == IGMP_ZERO))
        {
            /* Delete is not needed when EXCL, NONE
             * has come from different reporters */
            continue;
        }
        /* Deletes */
        if ((pSrc = IgmpGetSource (pGrp, pOldSrcSet[u4Index1])) != NULL)
        {
            /* Deletes source Node */
            if (pSrc->u1StaticFlg == IGMP_FALSE)
            {
                if (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))
                {
                    /* Delete source only when the reporter that sent the
                     * request for (S,G) previously has now sent NONE,G */
                    IgmpDeleteReporter (pGrp->pIfNode, pSrc, u4Reporter);
                    if ((pSrc != NULL) && (pSrc->u1FwdState == IGMP_FALSE))
                    {
                        pSrc->u1FwdState = IGMP_TRUE;
                    }
                }
                else
                {
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                    if (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) != IGMP_ZERO)
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                                "Updated to MRPs.\n");
                        IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,IgmpTrcGetModuleName(IGMP_DATA_MODULE),
                                "Updated to MRPs.\n");
                    }
                    IgmpDeleteSource (pGrp, pSrc);
                }
            }
        }
    }
    /* Send Q(G, A-Y) */
    u4NumSrc = IGMP_ZERO;

    if (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
    {
        for (pSrc = (tIgmpSource *)
             TMO_SLL_First (&(pGrp->srcList));
             pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
        {
            pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);
            u1SrcDelFlag = IGMP_FALSE;

            if ((IgmpCheckSourceInArray (pSrc->u4SrcAddress,
                                         pu1Sources, u4NoOfSources) == TRUE) &&
                (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                        pOldSrcSet) == TRUE))
            {
                /* one common list for fwd & non-fwd sources , the below chk is 
                 * to send queries only related to non-fwd sources */
                if (pSrc->u1FwdState == IGMP_TRUE)
                {
                    if (IGMP_IS_FAST_LEAVE_ENABLED (pIfNode))
                    {
                        IgmpDeleteReporter (pIfNode, pSrc, u4Reporter);
                        u1SrcDelFlag = IGMP_TRUE;
                    }

                    if (u1SrcDelFlag == IGMP_FALSE)
                    {
                        /* Source Timer lowered to LMQT */
                        IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                        pSrc->u1QryFlag = IGMP_TRUE;
                        u4NumSrc++;
                    }
                }
            }
        }
    }
    if (u4NumSrc > IGMP_ZERO)
    {
        pSrcSet = gpu4SrcSetArray;
        u4NumSrc = IGMP_ZERO;

        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1QryFlag == IGMP_TRUE)
            {
                IGMP_IPVX_ADDR_COPY (&(pSrcSet[u4NumSrc++]),
                                     &(pSrc->u4SrcAddress));
                pSrc->u1QryFlag = IGMP_FALSE;
            }
        }

        if (u4NumSrc > IGMP_ZERO)
        {
            /* Sends GrpAndSrc Qry and also lowers src timer to LMQT */
            IgmpSendGroupAndSourceSpecificQuery (pIfNode, pGrp, u4NumSrc,
                                                 pSrcSet);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleToExcludeStateEx()\r \n");
    UNUSED_PARAM (u1NumOfSrcTobeUpdated);
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleAllowStateEx                          */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleAllowStateEx (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    INT4                i4Status = IGMP_OK;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleAllowStateEx()\r \n");
    i4Status = IgmpAction1 (pIgmpFSMInfoNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleAllowStateEx()\r \n");
    return i4Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpHandleBlockStateEx                          */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpHandleBlockStateEx (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    INT4                i4Status = IGMP_NOT_OK;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpHandleBlockStateEx()\r \n");
    i4Status = IgmpBlockExclude (pIgmpFSMInfoNode);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpHandleBlockStateEx()\r \n");
    return i4Status;
}
