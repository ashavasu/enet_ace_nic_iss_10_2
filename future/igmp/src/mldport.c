/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mldport.c,v 1.22 2015/09/18 10:43:56 siva Exp $            
 *
 * Description:This file contains the routines required for     
 *              porting MLD on different targets                 
 *
 *******************************************************************/

#include "igmpinc.h"
#include "fssocket.h"
#include "mldtypdfs.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE ;
#endif
tMLDHostEntry       gMLDHostTable[MLD_MAX_HOST_ENTRIES];

VOID                MLDInitTmr (tMLDTmr * pTmr, UINT2 u2TimerId,
                                UINT4 u4IfIndex, tIp6Addr * McastAddr);
tMLDHostEntry      *MLDAddHostEntry (tIp6Addr * Ip6Addr, UINT4 u4IfIndex);
tMLDHostEntry      *MLDGetHostEntry (UINT4 u4IfIndex, tIp6Addr * Ip6Addr);
INT1                MLDSendReport (tMLDHostEntry * pHostEntry);
INT1                MLDSendDone (tMLDHostEntry * pHostEntry);
VOID                MLDFormHdr (UINT1 u1Type, UINT4 u4IfIndex,
                                tIp6Addr McastAddr, tMLDHdr * pMLDHdr);
INT4                MLDSendPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tHlToIp6Params * pParams);
VOID                MLDReportTmrExpiry (tIp6Addr * McastAddr, UINT4 u4IfIndex);

#define MLD_LINK_SCOPE_MCASTADDR(a)    {(a).u4_addr[0] = CRU_HTONL(0xFF020000);\
                                        (a).u4_addr[1] = 0x0;\
                                        (a).u4_addr[2] = 0x0;\
                                        (a).u4_addr[3] = CRU_HTONL(0x02);}

/*********************************************************************/
/* Function           : IgmpHandleIncomingPkt                        */
/*                                                                   */
/* Description        : function called by IP wheenver IP recieves   */
/*                      IGMP packet. It is passed to IP at the       */
/*                      time of IGMP is registering with IP. This    */
/*                      function enqueues the received packet into   */
/*                      IGMP queue.                                  */
/*                                                                   */
/* Input(s)           : messagebuffer                                */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/* MLDV2_CHG: MldPortHandleIncomingPkt name should be changed MLDInput after 
 * obsoleting netip6/mld */
PUBLIC VOID
MLDInput (tNetIpv6HliParams * pIp6HliParams)
{
    tIgmpQMsg          *pQMsg = NULL;
    tMldParams         *pParams = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortHandleIncomingPkt.\n");
    pParams = (tMldParams *) MLD_GET_PARAMS
        (pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf);

    pParams->u4IfIndex = pIp6HliParams->unIpv6HlCmdType.AppRcv.u4Index;
    pParams->u2Len = (UINT2) pIp6HliParams->unIpv6HlCmdType.AppRcv.u4PktLength;

    MEMCPY (&pParams->SrcAddr,
            &(pIp6HliParams->unIpv6HlCmdType.AppRcv.Ip6SrcAddr),
            sizeof (tIp6Addr));

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_OS_RES_MODULE | MLD_ALL_MODULES, "MLD",
                  "MldPortHandleIncomingPkt:Failure, Cannot allocate memory to "
                  "IgmpQMsg.\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_OS_RES_MODULE,
                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
				"to IgmpQMsg");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortHandleIncomingPkt.\n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf,
                                     FALSE);
        return;
    }

    pQMsg->u4MsgType = MLD_PKT_RECV_EVENT;
    pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt =
        pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf;

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) !=
        OSIX_SUCCESS)
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf,
                                     FALSE);
        IGMP_QMSG_FREE (pQMsg);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortHandleIncomingPkt.\n");
        return;
    }

    /* Send a EVENT to IGMP */
    if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortHandleIncomingPkt.\n");
        return;
    }

    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : SparsePimIpv6Interface                              */
/*                                                                         */
/*     Description   : This function is provided to IPv6 as a callback     */
/*                     function. This is called by IPv6 when PIM pkts      */
/*                     are received or IPv6 Route changes or               */
/*                     Interface status changes.                           */
/*                                                                         */
/*     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  VOIDV.                                   */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
MldPortIpv6ModuleIfChgHandler (tNetIpv6HliParams * pIp6HliParams)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortIpv6ModuleIfChgHandler.\n");
    switch (pIp6HliParams->u4Command)
    {
        case NETIPV6_INTERFACE_PARAMETER_CHANGE:
            MldPortHandleV6IfStatusChg (&(pIp6HliParams->unIpv6HlCmdType.
                                          IfStatChange));
            break;

/*        case NETIPV6_ROUTE_CHANGE:
            break;*/
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
        case NETIPV6_ADDRESS_CHANGE:
            MldPortHandleV6IfAddrChg (&
                                      (pIp6HliParams->unIpv6HlCmdType.
                                       AddrChange));
            break;
#endif
        default:
            break;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),                                         "Exit MldPortHandleV6IfStatusChg.\n");
}

VOID
MldPortHandleV6IfStatusChg (tNetIpv6IfStatChange * pIfInfo)
{
    tIgmpQMsg          *pQMsg = NULL;
    UINT4               u4Mask = 0;
    UINT1               u1MsgType = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortHandleV6IfStatusChg.\n");
    u4Mask = pIfInfo->u4Mask;

    if ((u4Mask == NETIPV6_INTERFACE_STATUS_CHANGE) &&
        (pIfInfo->u4IfStat == NETIPV6_IF_DELETE))
    {
        u1MsgType = IGMP_DELETE_INTERFACE;
    }
    else if (u4Mask == NETIPV6_INTERFACE_STATUS_CHANGE)
    {
        u1MsgType = IGMP_IF_STATUS_CHANGE;
    }
    else
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE,
                  "MLD", "If Change - NO Oper Status Change. \n");

        MLD_DBG (IGMP_DBG_EXIT, "Exit : MldPortHandleV6IfStatusChg.\n");
        return;
    }

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_OS_RES_MODULE | MLD_CONTROL_PLANE_MODULE, "MLD",
                  "MLD Allocating Memory for IgmpQMsg - Failed.\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_OS_RES_MODULE,
                              IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
				"to IgmpQMsg\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortHandleV6IfStatusChg.\n");
        return;
    }

    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1MsgType = u1MsgType;
    pQMsg->u4MsgType = IGMP_IP_IF_CHG_EVENT;
    if (pIfInfo->u4IfStat == NETIPV6_IF_UP)
    {
        pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1IfStatus = IGMP_IFACE_UP;
    }
    else if (pIfInfo->u4IfStat == NETIPV6_IF_DOWN)
    {
        pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1IfStatus = IGMP_IFACE_DOWN;
    }

    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4IfIndex = pIfInfo->u4IpPort;
    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4CfaIfIndex = pIfInfo->u4Index;
    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1AddrType = IPVX_ADDR_FMLY_IPV6;

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
                  "Enqueuing I/f change info from IP to MLD - FAILED \n");
        IGMP_QMSG_FREE (pQMsg);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortHandleV6IfStatusChg.\n");
        return;
    }
    /* Send a EVENT to IGMP */
    if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
                  "Sending a event from IP to MLD - FAILED \n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_CNTRL_MODULE,
                              IGMP_NAME,IgmpSysErrString[EVENT_SEND_FAIL],
			"from IP to MLD\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortHandleV6IfStatusChg.\n");
        return;
    }

}
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
VOID
MldPortHandleV6IfAddrChg (tNetIpv6AddrChange * pAddrChange)
{
    tIgmpQMsg          *pQMsg = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering MldPortHandleV6IfAddrChg.\n");

    if (pAddrChange->Ipv6AddrInfo.u4Type != ADDR6_LLOCAL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE,"MLD",
                      "Notification is ignored due to invalid address type.\n");

        MLD_DBG (IGMP_DBG_EXIT, "Exit : MldPortHandleV6IfAddrChg.\n");
        return;
    }

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG,
                      MLD_OS_RES_MODULE | MLD_CONTROL_PLANE_MODULE, "MLD",
                      "MLD Allocating Memory for IgmpQMsg - Failed.\r\n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG,
                              IGMP_OS_RES_MODULE, IGMP_NAME,
                              IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
                              "to IgmpQMsg\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exit MldPortHandleV6IfAddrChg.\n");
        return;
    }

    pQMsg->u4MsgType = MLD_IPADDR_CHG_EVENT;

    if (pAddrChange->u4Mask & NETIPV6_ADDRESS_ADD)
        pQMsg->IgmpQMsgParam.MldIpv6AddrInfo.u1MsgType =
            MLD_ADDRCHG_INTERFACE_ADD;
    else if (pAddrChange->u4Mask & NETIPV6_ADDRESS_DELETE)
        pQMsg->IgmpQMsgParam.MldIpv6AddrInfo.u1MsgType =
            MLD_ADDRCHG_INTERFACE_DEL;

    IPVX_ADDR_INIT_IPV6 (pQMsg->IgmpQMsgParam.MldIpv6AddrInfo.Ip6Addr,
                         IPVX_ADDR_FMLY_IPV6,
                         pAddrChange->Ipv6AddrInfo.Ip6Addr.u1_addr);
    pQMsg->IgmpQMsgParam.MldIpv6AddrInfo.u4PrefixLength =
        pAddrChange->Ipv6AddrInfo.u4PrefixLength;
    pQMsg->IgmpQMsgParam.MldIpv6AddrInfo.u1AddrType =
        (UINT1) pAddrChange->Ipv6AddrInfo.u4Type;

    if (MldPortGetIpPortFrmIndex
        (pAddrChange->u4Index,
         &(pQMsg->IgmpQMsgParam.MldIpv6AddrInfo.u4IfIndex)) == OSIX_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE,
                      "MLD", "Interface not found. \n");
        IGMP_QMSG_FREE (pQMsg);
        MLD_DBG (IGMP_DBG_EXIT, "Exit : MldPortHandleV6IfAddrChg.\n");
        return;
    }

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES,"MLD",
                 "Enqueuing I/f Address change info from IP to MLD- FAILED\n");
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_CNTRL_MODULE,
                         IGMP_NAME,IgmpSysErrString[QUEUE_SEND_FAIL],
                         "for Queue id  %d",gIgmpQId);

        IGMP_QMSG_FREE (pQMsg);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exit MldPortHandleV6IfAddrChg.\n");
        return;
    }
    /* Send a EVENT to IGMP */
    if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES,
                 "MLD", "Sending a event from IP to MLD - FAILED \n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG,
                              IGMP_CNTRL_MODULE, IGMP_NAME,
                              IgmpSysErrString[EVENT_SEND_FAIL],
                              "from IP to MLD\n");
        IGMP_QMSG_FREE (pQMsg);
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exit MldPortHandleV6IfAddrChg.\n");
        return;
    }
}
#endif

INT4
MldPortGetIp6IfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortGetIp6IfIndexFromPort.\n");
    if (NetIpv4GetCfaIfIndexFromPort (u4Port, pu4IfIndex) == NETIPV4_FAILURE)
    {
        MLD_TRC_ARG1 (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
                  "Getting cfa index from port %d FAILED \n", u4Port);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortGetIp6IfIndexFromPort.\n");
        return OSIX_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortGetIp6IfIndexFromPort.\n");
    return OSIX_SUCCESS;
}

INT4
MldPortIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    INT4                i4RetStat = OSIX_FAILURE;

#ifdef IP6_WANTED
    i4RetStat = NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortIpv6GetIfInfo.\n");
    if (i4RetStat == NETIPV6_SUCCESS)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortIpv6GetIfInfo.\n");
        return OSIX_SUCCESS;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);
#endif
    MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
              "Getting if info from index FAILED \n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortIpv6GetIfInfo.\n");
    return i4RetStat;
}

INT4
MldPortGetIpPortFrmIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    INT4                i4Status = OSIX_SUCCESS;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortGetIpPortFrmIndex.\n");
    *pu4Port = CfaGetIfIpPort (u4IfIndex);
    if (*pu4Port == CFA_INVALID_INDEX)
    {
        MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
                  "Getting ip port from index FAILED \n");
        i4Status = OSIX_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortGetIpPortFrmIndex.\n");
    return i4Status;
}

#ifdef LNXIP6_WANTED
/****************************************************************************
 Function    :  MLDSendPktToIpv6
 Input       :  tCRU_BUF_CHAIN_HEADER * pBuf MLD data packet
                tHlToIp6Params * pParams - Params required to send the packet
                out
 Output      :  None
 Description :  Copies the MLD data from the CRU buffer to linear buffer and
                sends over the socket.
 Returns     :  MLD_SUCCESS/MLD_FAILURE.
****************************************************************************/
INT4
MLDSendPktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf, tHlToIp6Params * pParams)
{
    INT4                i4HopLimit = 0;
    DBL8                d8HbH = 0;
    INT4                i4RetVal = 0;
    UINT1              *pu1MldPkt = NULL;
    struct sockaddr_in6 Dest6Addr;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct iovec        Iov;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct msghdr       PktInfo;
    tCfaIfInfo          IfInfo;
    UINT1               au1Cmsg[MLD_ANCILLARY_DATA_LEN];

    MEMSET (&Iov, 0, sizeof (struct iovec));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&au1Cmsg, 0, MLD_ANCILLARY_DATA_LEN);
    MEMSET (&Dest6Addr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    if (pParams == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Allocate the memory for linear buffer */
    pu1MldPkt = MEM_MALLOC (pParams->u4Len, UINT1);
    if (pu1MldPkt == NULL)
    {
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_BUFFER_MODULE,
             IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],"for linear buffer\n");
	CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    /* Copy the Data from CRU buffer to linear buffer */
    i4RetVal = CRU_BUF_Copy_FromBufChain (pBuf, pu1MldPkt, 0, pParams->u4Len);
    if (i4RetVal == CRU_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_BUFFER_MODULE, IgmpGetModuleName(IGMP_BUFFER_MODULE),
                  "Copy from CRU Buff " "to linear buffer failed \n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE], "from CRU  buff to linear\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MEM_FREE (pu1MldPkt);
        return OSIX_FAILURE;
    }

    /* Release the CRU buffer */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    i4HopLimit = pParams->u1Hlim;
    i4RetVal =
        setsockopt (gIgmpConfig.i4MldSockId, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                    &i4HopLimit, sizeof (INT4));
    if (i4RetVal < 0)
    {
        perror ("MLD - IPV6_MULTICAST_HOPS ");
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, IGMP_NAME,
                  "set socket option - FAILED \n");
        MEM_FREE (pu1MldPkt);
        return OSIX_FAILURE;
    }
    /*Hop by Hop header */
    d8HbH = TRUE;
    i4RetVal =
        setsockopt (gIgmpConfig.i4MldSockId, IPPROTO_IPV6, IPV6_HOPOPTS,
                    &d8HbH, sizeof (DBL8));
    if (i4RetVal < 0)
    {
        perror ("MLD - IPV6_HOPOPTS ");
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, IGMP_NAME,
                  "set socket option - FAILED \n");
        MEM_FREE (pu1MldPkt);
        return OSIX_FAILURE;
    }
    Dest6Addr.sin6_family = AF_INET6;
    MEMCPY (&(Dest6Addr.sin6_addr), &(pParams->Ip6DstAddr), sizeof (tIp6Addr));

    Iov.iov_base = (void *) pu1MldPkt;
    Iov.iov_len = pParams->u4Len;

    PktInfo.msg_name = (void *) &Dest6Addr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) &au1Cmsg;
    PktInfo.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    pIpPktInfo = (struct in6_pktinfo *) (VOID *) CMSG_DATA (pCmsgInfo);
    MEMCPY (&(pIpPktInfo->ipi6_addr), &(pParams->Ip6SrcAddr),
            sizeof (tIp6Addr));
    /* Get the interface port number */
    CfaGetIfInfo (pParams->u4Index, &IfInfo);
    pIpPktInfo->ipi6_ifindex = IfInfo.i4IpPort;

    i4RetVal = sendmsg (gIgmpConfig.i4MldSockId, &PktInfo, 0);

    if (i4RetVal < 0)
    {
        perror ("MLD - sendmsg ");
        MLD_DBG_INFO (IGMP_DBG_FLAG, MLD_MGMT_MODULE, IGMP_NAME, "sendmsg - FAILED \n");
        MEM_FREE (pu1MldPkt);
        return OSIX_FAILURE;
    }

    MEM_FREE (pu1MldPkt);
    return OSIX_SUCCESS;
}
#endif

/***************************************************************************
 * Function Name    :  MldPortEnqueuePktToIpv6
 *
 * Description      :  Enqueues the Pkt to IPv6; if FS IPv6 is used the message
 *                     will be posted to IP Queue
 *
 * Input (s)        :  pBuf -  Points to MLD control packet or multicast data
 *                             packet
 *                     pMldParams - Points to structure holding the forwarding
 *                                  info
 *
 * Output (s)       :  None
 *
 * Returns          :  OSIX_SUCCESS OR OSIX_FAILURE
 ****************************************************************************/

INT4
MldPortEnqueuePktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tHlToIp6Params * pMldParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortEnqueuePktToIpv6.\n");
    pMldParams->pBuf = pBuf;
    pMldParams->u4ContextId = MLD_DEF_VRF_CTXT_ID;

    /* Enqueue the MLD pkts to IPv6 Q */
#ifdef LNXIP6_WANTED
    if (MLDSendPktToIpv6 (pBuf, pMldParams) == OSIX_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
                  "Enqueuing Pkt to IPv6 FAILED \n");
        i4RetVal = OSIX_FAILURE;
    }

#else
#ifdef IP6_WANTED
    if (Ip6RcvFromHl (pBuf, pMldParams) == IP6_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
                  "Enqueuing Pkt to IPv6 FAILED \n");
        i4RetVal = OSIX_FAILURE;
    }

#endif
#endif

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortEnqueuePktToIpv6.\n");
    return i4RetVal;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     MldPortUpdateMrps                               */
/*                                                                         */
/*     Description   :     This function updates the MRPs                  */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    GrpSrcInfo - variable of tGrpNoteMsg             */
/*                        pGrp - pointer to  tIgmpGroup                    */
/*                        u1JoinLeaveFlag - Join/Leave                     */
/*                        u1FilterMode - Include/Exclude                   */
/*                        u1NumOfSrcs - No of sources being reported.      */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpUtilCreateGroupEntry                    **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
MldPortUpdateMrps (tIgmpGroup * pGrp, UINT1 u1JoinLeaveFlag)
{
    UINT4               u4Count = 0;
    tGrp6NoteMsg        GrpSrcInfo;
    tIgmpSource        *pMrpSrcNode = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortUpdateMrps.\n");
    IgmpFillMem (&GrpSrcInfo, 0, sizeof (tGrp6NoteMsg));

    /* Updating MRP's */
    GrpSrcInfo.u4IfIndex = pGrp->pIfNode->u4IfIndex;
    IGMP_IP_COPY_FROM_IPVX (&(GrpSrcInfo.GrpAddr), pGrp->u4Group,
                            IPVX_ADDR_FMLY_IPV6);
    GrpSrcInfo.u1Flag = u1JoinLeaveFlag;

    if (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) == 0)
    {
        for (u4Count = 0; u4Count < MLD_MAX_REGISTER; u4Count++)
        {
            if (gaMldRegister[u4Count].u1Flag != IGMP_UP)
            {
                continue;
            }
            if ((gaMldRegister[u4Count].pLJNoteFuncPtr == NULL) &&
                (gaMldRegister[u4Count].pLJNoteFuncPtrV2 != NULL))
            {

                gaMldRegister[u4Count].pLJNoteFuncPtrV2 (&GrpSrcInfo);
            }
            else if (gaMldRegister[u4Count].pLJNoteFuncPtr != NULL)
            {

                gaMldRegister[u4Count].pLJNoteFuncPtr (u1JoinLeaveFlag,
                                                       GrpSrcInfo.GrpAddr,
                                                       GrpSrcInfo.u4IfIndex);
            }
            else
            {
                MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES,
                          "MLD", "The MRP not registered for MLD  packets \n");
            }
        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exit MldPortUpdateMrps.\n");
        return;
    }

    /* fill the sources for MRP updation */
    while ((pMrpSrcNode = (tIgmpSource *)
            TMO_SLL_First (&(pGrp->SrcListForMrpUpdate))) != NULL)
    {
        IGMP_IP_COPY_FROM_IPVX (&(GrpSrcInfo.SrcAddr[u4Count]),
                                (pMrpSrcNode->u4SrcAddress),
                                IPVX_ADDR_FMLY_IPV6);
        u4Count++;

        /* After this source is updated then it can become part of the
         * main source list of the group node.
         */
        TMO_SLL_Delete (&(pGrp->SrcListForMrpUpdate), &pMrpSrcNode->Link);
        TMO_SLL_Add (&(pGrp->srcList), &pMrpSrcNode->Link);
        pMrpSrcNode->u1UpdatedToMrp = IGMP_TRUE;

        if ((u4Count == IGMP_MAX_SOURCES) ||
            (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) == 0))
        {
            GrpSrcInfo.u1NumOfSrcs = (UINT1) u4Count;
            for (u4Count = 0; u4Count < MLD_MAX_REGISTER; u4Count++)
            {
                if (gaMldRegister[u4Count].u1Flag != IGMP_UP)
                {
                    continue;
                }
                if (gaMldRegister[u4Count].pLJNoteFuncPtrV2 != NULL)
                {
                    gaMldRegister[u4Count].pLJNoteFuncPtrV2 (&GrpSrcInfo);
                }
            }
            u4Count = 0;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exit MldPortUpdateMrps.\n");
    return;
}

/*********************************************************************/
/* Function           :  MldHlReg                                    */
/*                                                                   */
/* Description        :  This function is called from Multicast      */
/*                       Routing  protocol to register themselves    */
/*                       with  MLD                                   */
/*                                                                   */
/*                                                                   */
/* Input(s)           : handlinput is a function pointer used        */
/*                      whenever there is any packet is to be        */
/*                      redirected  to the Multicast Routing Prot    */
/*                                                                   */
/*                                                                   */
/* Output(s)          : none                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = MldRegisterMRP                        **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT1
MLDHlReg (UINT2 u2ProtoId, VOID (*pLJNoteFuncPtr) (UINT2, tIp6Addr, UINT4))
{
    INT4                i4Index = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDHlReg.\n");
    i4Index = MldPortFindRegisterEntry (u2ProtoId);
    if (i4Index == MLD_INVALID_REGISTER)
    {
        if ((i4Index = MldPortFindFreeRegisterEntry ()) == MLD_INVALID_REGISTER)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDHlReg.\n");
            return OSIX_FAILURE;
        }
        gaMldRegister[i4Index].u1ProtId = u2ProtoId;
        gaMldRegister[i4Index].u1Flag = IGMP_UP;
        gaMldRegister[i4Index].pLJNoteFuncPtr = pLJNoteFuncPtr;
        gaMldRegister[i4Index].pLJNoteFuncPtrV2 = NULL;
    }
    else
    {
        gaMldRegister[i4Index].pLJNoteFuncPtr = pLJNoteFuncPtr;
    }
    IgmpCurrMbrInfoToMrp (IPVX_ADDR_FMLY_IPV6);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDHlReg.\n");
    return OSIX_SUCCESS;
}

/*********************************************************************/
/* Function           :  MLDv2HlReg                                  */
/*                                                                   */
/* Description        :  This function is called from Multicast      */
/*                       Routing  protocol to register themselves    */
/*                       with  MLD to get MLDv1 and MLDv2 packets    */
/*                                                                   */
/*                                                                   */
/* Input(s)           : handlinput is a function pointer used        */
/*                      whenever there is any packet is to be        */
/*                      redirected  to the Multicast Routing Prot    */
/*                                                                   */
/*                                                                   */
/* Output(s)          : none                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = MldRegisterMRP                        **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT1
MLDv2HlReg (UINT2 u2ProtoId, VOID (*pLJNoteFuncPtr) (tGrp6NoteMsg * pGrpInfo))
{
    INT4                i4Index = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDv2HlReg.\n");
    i4Index = MldPortFindRegisterEntry (u2ProtoId);
    if (i4Index == MLD_INVALID_REGISTER)
    {
        if ((i4Index = MldPortFindFreeRegisterEntry ()) == MLD_INVALID_REGISTER)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDv2HlReg.\n");
            return OSIX_FAILURE;
        }
        gaMldRegister[i4Index].u1ProtId = u2ProtoId;
        gaMldRegister[i4Index].u1Flag = IGMP_UP;
        gaMldRegister[i4Index].pLJNoteFuncPtr = NULL;
        gaMldRegister[i4Index].pLJNoteFuncPtrV2 = (VOID*) pLJNoteFuncPtr;
    }
    else
    {
        gaMldRegister[i4Index].pLJNoteFuncPtrV2 = (VOID*) pLJNoteFuncPtr;
    }
    IgmpCurrMbrInfoToMrp (IPVX_ADDR_FMLY_IPV6);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDv2HlReg.\n");
    return OSIX_SUCCESS;
}

/*********************************************************************/
/* Function           : MldHlDeReg                                   */
/*                                                                   */
/* Description        : De register with MLD                         */
/*                                                                   */
/* Input(s)           : MRP identifier,  Function pointer to be      */
/*                      to be called when a packet arrives for the   */
/*                      MRP. Function pointer to handle interface    */
/*                      changes, Function pointer to call for multica*/
/*                      Routes, Function pointer to call to inform   */
/*                      Leave or join updates.                       */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = MldDeRegisterMRP                      **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT1
MLDHlDeReg (UINT2 u2ProtoId)
{
    INT4                i4Index = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDHlDeReg.\n");
    i4Index = MldPortFindRegisterEntry (u2ProtoId);

    if (i4Index != MLD_INVALID_REGISTER)
    {
        gaMldRegister[i4Index].u1Flag = IGMP_DOWN;
        gaMldRegister[i4Index].u1ProtId = 0;
        gaMldRegister[i4Index].pHandleInput = NULL;
        gaMldRegister[i4Index].pUpdateIFace = NULL;
        gaMldRegister[i4Index].pFwdIfList = NULL;
        gaMldRegister[i4Index].pLJNoteFuncPtr = NULL;
        gaMldRegister[i4Index].pLJNoteFuncPtrV2 = NULL;
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDHlDeReg.\n");
        return OSIX_SUCCESS;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDHlDeReg.\n");
    return OSIX_FAILURE;
}

/*********************************************************************/
/* Function           : MldPortFindFreeRegisterEntry                 */
/*                                                                   */
/* Description        : Finds the free entry in the given register   */
/*                                                                   */
/* Input(s)           : Registration register                        */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : freeindex                                    */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = MldFindFreeRegisterEntry              **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT4
MldPortFindFreeRegisterEntry (VOID)
{
    INT4                i4Index;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortFindFreeRegisterEntry.\n");
    for (i4Index = 0; i4Index < MLD_MAX_REGISTER; i4Index++)
    {
        if (gaMldRegister[i4Index].u1Flag == DOWN)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortFindFreeRegisterEntry.\n");
            return i4Index;
        }
    }

    MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
              "No free register entry exists \n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortFindFreeRegisterEntry.\n");
    return MLD_INVALID_REGISTER;
}

/*********************************************************************/
/* Function           :  MldPortFindRegisterEntry                    */
/*                                                                   */
/* Description        : function will find whether give multicast    */
/*                       protocl  is registered with IGMP or not     */
/*                                                                   */
/* Input(s)           : register table, keys.                        */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : returns -1 if found otherwise index          */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = MldFindRegisterEntry                  **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT4
MldPortFindRegisterEntry (UINT1 u1ProtId)
{
    INT4                i4Index;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldPortFindRegisterEntry.\n");
    for (i4Index = 0; i4Index < MLD_MAX_REGISTER; i4Index++)
    {
        if (gaMldRegister[i4Index].u1ProtId == u1ProtId)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortFindRegisterEntry.\n");
            return i4Index;
        }
    }
    MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
              "Given register entry does not exist \n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MldPortFindRegisterEntry.\n");
    return MLD_INVALID_REGISTER;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MldEnableMcastSocketOption                                */
/*                                                                           */
/* Description  : Function to add a virtual interface                        */
/*                                                                           */
/* Input        : u4IfIndex                                                  */
/*                u1IfStatus                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
MldSetMcastSocketOption (UINT4 u4IfIndex, UINT1 u1IfStatus)
{
    tNetIp6McastInfo    NetIp6McastInfo;
    UINT4               u4Status = 0;
    INT4                i4RetVal = 0;

    MEMSET (&NetIp6McastInfo, 0, sizeof (tNetIp6McastInfo));

    NetIp6McastInfo.u4IfIndex = u4IfIndex;
    NetIp6McastInfo.u2McastProtocol = ICMPV6_PROTOCOL_ID;

    u4Status = (IGMP_DOWN == u1IfStatus ? NETIPV6_DISABLED : NETIPV6_ENABLED);

#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
    i4RetVal = NetIpv6McastSetMcStatusOnIface (&NetIp6McastInfo, u4Status);
#endif
    return i4RetVal;
}

/****************************************************************************
 Function    :  MLDCreateSocket
 Input       :  None
 Output      :  None
 Description :  Creates the socket which is used to send the MLD Pkts out
 Returns     :  OSIX_SUCCESS/OSIX_FAILURE.
****************************************************************************/
INT4
MldCreateSocket (VOID)
{
    gIgmpConfig.i4MldSockId = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);

    if (gIgmpConfig.i4MldSockId < 0)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE | MLD_ALL_MODULES,
                  "MLD", "ICMPv6 Socket creation - FAILED \n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#ifdef LNXIP6_WANTED
/****************************************************************************
 Function    :  MLDSetSendSockOptions
 Input       :  None
 Output      :  None
 Description :  Set the socket options to the MLD socket
 Returns     :  OSIX_SUCCESS/OSIX_FAILURE.
 *****************************************************************************/
INT4
MLDSetSendSockOptions (VOID)
{
    INT4                i4Option = FALSE;
    INT4                i4RetVal = OSIX_SUCCESS;

    i4RetVal =
        setsockopt (gIgmpConfig.i4MldSockId, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4Option, sizeof (INT4));
    if (i4RetVal != 0)
    {
        perror ("MLD - IPV6_MULTICAST_LOOP ");
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE | MLD_ALL_MODULES,
                  "MLD", "IPV6_MULTICAST_LOOP set socket option- FAILED \n");
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : MldCloseSocket                                            */
/*                                                                           */
/* Description  : Close Socket                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
MldCloseSocket (VOID)
{
    IGMP_CLOSE (gIgmpConfig.i4MldSockId);
    gIgmpConfig.i4MldSockId = -1;
}

/****************************************************************************
 Function    :  MLDIsCacheEntryPresent
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function returns the TRUE based on if the  cache entry
                exists, else returns FALSE.
 Returns     :  TRUE/FALSE
****************************************************************************/
INT1
MLDIsCacheEntryPresent (tIp6Addr * Ip6Addr, UINT4 u4IfIndex)
{
    tIgmpIPvXAddr       GrpAddr;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4Port = 0;

    if (IgmpGetPortFromIfIndex (u4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                &u4Port) == IGMP_FAILURE)
    {
        return FALSE;
    }

    pIfNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV6);

    if (pIfNode == NULL)
    {
        return FALSE;
    }

    IPVX_ADDR_INIT_IPV6 (GrpAddr, IPVX_ADDR_FMLY_IPV6, (UINT1 *) Ip6Addr)
        if (IgmpGrpLookUp (pIfNode, GrpAddr) != NULL)
    {
        return TRUE;
    }
    return FALSE;
}

/****************************************************************************
 Function    :  MLDReportTmrExpiry
 Input       :  Multicast Address
                Interface
 Output      :  None.
 Description :  This function is called when the retrasmission timer expires.
                It will send "lastquerycount" queries then inform the
                     routing protocol.
 Returns     :  None.
****************************************************************************/
VOID
MLDReportTmrExpiry (tIp6Addr * McastAddr, UINT4 u4IfIndex)
{
    tMLDHostEntry      *pHostEntry = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDReportTmrExpir.\n");
    MLD_DBG_INFO (MLD_DBG_FLAG, MLD_OS_RES_MODULE, "MLD",
              "Report Delay Tmr Expired\n");

    pHostEntry = MLDGetHostEntry (u4IfIndex, McastAddr);
    if (pHostEntry == NULL)
    {
        MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES, "MLD",
                  "Cannot find entry!!!\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDReportTmrExpiry.\n");

        return;
    }

    switch (pHostEntry->u1State)
    {
        case MLD_NO_LISTENERS:
        case MLD_IDLE_LISTENER:
            break;
        case MLD_DELAY_LISTENER:

            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, "MLD", "Sending Report packet\n");
            MLDSendReport (pHostEntry);
            pHostEntry->u1Flag = MLD_SET;
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, "MLD",
                      "Starting send report timer again\n");
            MLDStartTmr (gMldTimerListId, &((pHostEntry->tRepTmr).node),
                         MLD_UNSOLICITED_REPORT_INTERVAL * MLD_SYS_TICKS);
            break;
        default:
            break;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDReportTmrExpiry.\n");

    return;
}

/****************************************************************************
 Function    :  MLDProcessTmrEvt
 Input       :  None.
 Output      :  None.
 Description :  This function called when mld report timer expired.
 Returns     :  None.
****************************************************************************/
VOID
MLDProcessTmrEvt (VOID)
{
    tTmrAppTimer       *pExpTmr = NULL;
    UINT4               u4IfIndex;
    tIp6Addr            McastAddr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDProcessTmrEvt.\n");
    pExpTmr = TmrGetNextExpiredTimer (gMldTimerListId);

    while (pExpTmr != NULL)
    {
        u4IfIndex = ((tMLDTmr *) pExpTmr)->u4IfIndex;
        McastAddr = ((tMLDTmr *) pExpTmr)->McastAddr;

        switch (pExpTmr->u4Data)
        {
            case MLD_REPORT_TMR:
                MLDReportTmrExpiry (&McastAddr, u4IfIndex);
                break;

            default:
                break;

        }                        /*end of switch */

        pExpTmr = TmrGetNextExpiredTimer (gMldTimerListId);

    }                            /* end of while */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDProcessTmrEvt.\n");

    return;
}

/****************************************************************************
 Function    :  MLDGetHostEntry
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function returns the pointer to the cache entry if exists,
                else returns NULL.
 Returns     :  Pointer to the cache entry/NULL
****************************************************************************/
tMLDHostEntry      *
MLDGetHostEntry (UINT4 u4IfIndex, tIp6Addr * Ip6Addr)
{
    UINT4               u4Count;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDGetHostEntry.\n");

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_HOST_ENTRIES; u4Count++)
    {
        if (MLD_HOST_STATE (u4Count) == MLD_INVALID)
        {
            continue;
        }

        if ((MLD_HOST_IFINDEX (u4Count) == u4IfIndex) &&
            (!(MEMCMP
               (&MLD_HOST_MCASTADDR (u4Count), Ip6Addr, sizeof (tIp6Addr)))))
        {
            return &gMLDHostTable[u4Count];
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDGetHostEntry.\n");
    return NULL;
}

/****************************************************************************
 Function    :  MLDInitTmr
 Input       :  Pointer to the timer
                Timer Id
                Interface
                Multicast Address
 Output      :  None.
 Description :  This function initializes some timer with the interface and
                multicast address.
 Returns     :  None.
****************************************************************************/
VOID
MLDInitTmr (tMLDTmr * pTmr, UINT2 u2TimerId,
            UINT4 u4IfIndex, tIp6Addr * McastAddr)
{

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDInitTmr.\n");
    pTmr->node.u4Data = u2TimerId;
    pTmr->u4IfIndex = u4IfIndex;

    if (McastAddr != NULL)
    {
        MEMCPY (&pTmr->McastAddr, McastAddr, sizeof (tIp6Addr));
    }
    else
    {
        MEMSET (&pTmr->McastAddr, MLD_ZERO, sizeof (tIp6Addr));
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDInitTmr.\n");
    return;
}

/****************************************************************************
 Function    :  MLDProtoInit
 Input       :  None.
 Output      :  None.
 Description :  This function initializes all the tables(interface & cache).
                Creates the queue and timer.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDProtoInit ()
{

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDProtoInit.\n");

    if (MLDCreateTimerList ((const UINT1 *) "IGMP", MLD_REPORT_TMR,
                            NULL, &gMldTimerListId) != OSIX_SUCCESS)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_INIT_SHUT_MODULE | MLD_ALL_MODULES, "MLD",
                  "Exiting MLDProtoInit:Tmr Creation Failed\n ");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDProtoInit.\n");

        return FAILURE;
    }

    MLD_DBG_INFO (MLD_DBG_FLAG, MLD_OS_RES_MODULE, "MLD",
              "Creation of Timer Successful \n");

    MEMSET (gMLDHostTable, MLD_ZERO,
            (sizeof (tMLDHostEntry) * MLD_MAX_HOST_ENTRIES));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDProtoInit.\n");

    return SUCCESS;

}

/****************************************************************************
 Function    :  MLDAddHostEntry
 Input       :  Multicast address
                  interface
 Output      :  None.
 Description :  This function adds a new entry in the host table if it does
                  not exists. It returns the pointer to the entry added if
                success else returns NULL.
 Returns     :  Pointer to the host entry/NULL
 ******************************************************************************/
tMLDHostEntry      *
MLDAddHostEntry (tIp6Addr * Ip6Addr, UINT4 u4IfIndex)
{
    UINT4               u4Count = MLD_ZERO;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDAddHostEntry.\n");

    if (!IS_ADDR_MULTI (*Ip6Addr))
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Exiting  MLDAddHostEntry: Ip6 address should be"
                  "multicast address\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDProtoInit.\n");
        return NULL;
    }

    for (u4Count = MLD_ZERO; u4Count < MLD_MAX_HOST_ENTRIES; u4Count++)
    {
        if (MLD_HOST_STATE (u4Count) == MLD_INVALID)
        {
            break;
        }
    }

    if (u4Count == MLD_MAX_HOST_ENTRIES)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Exiting  MLDAddHostEntry: host count reaches maximum\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDProtoInit.\n");
        return NULL;
    }

    MLD_HOST_STATE (u4Count) = MLD_NO_LISTENER;

    MLD_HOST_IFINDEX (u4Count) = u4IfIndex;

    MEMCPY (&MLD_HOST_MCASTADDR (u4Count), Ip6Addr, sizeof (tIp6Addr));

    MLDInitTmr (&MLD_HOST_REPORTTIMER (u4Count), MLD_REPORT_TMR, u4IfIndex,
                Ip6Addr);

    MLD_HOST_USAGECOUNT (u4Count) = MLD_ZERO;

    MLD_HOST_FLAG (u4Count) = MLD_RESET;

    MLD_HOST_BUF (u4Count) = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDProtoInit.\n");
    return &gMLDHostTable[u4Count];
}

/****************************************************************************
 Function    :  MLDFormHdr
 Input       :  u1Type
                u4IfIndex
                McastAddr
 Output      :  pMLDHdr
 Description :  This function updates pMLDHdr with the MLD header values provided
                as parameters to this function.
 Returns     :  None.
****************************************************************************/
VOID
MLDFormHdr (UINT1 u1Type, UINT4 u4IfIndex, tIp6Addr McastAddr,
            tMLDHdr * pMLDHdr)
{

    UNUSED_PARAM (u4IfIndex);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDAddHostEntry.\n");

    pMLDHdr->u1Type = u1Type;
    pMLDHdr->u1Code = MLD_ZERO;
    pMLDHdr->u2ChkSum = MLD_ZERO;
    pMLDHdr->u2Reserved = MLD_ZERO;
    MEMCPY (&pMLDHdr->McastAddr, &McastAddr, sizeof (tIp6Addr));

    pMLDHdr->u2MaxRespDelay = MLD_ZERO;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDProtoInit.\n");

    return;
}

/****************************************************************************
 Function    :  MLDSendReport
 Input       :  Pointer to the host table.
 Output      :  None.
 Description :  This function sends a REPORT.  If the MLD Hdr is
                not updated in the entry, this function forms an MLD Hdr
                and updates it in the entry.  The CRU Buffer is duplicated
                and the mld header is copied in the buffer and calls the
                a function (MLDSendPkt()) to send the packet.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDSendReport (tMLDHostEntry * pHostEntry)
{

    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tHlToIp6Params      Params;
    tMLDHdr             TempBuf;
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDSendReport.\n");

    MEMSET (&Params, 0, sizeof (tHlToIp6Params));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&TempBuf, 0, sizeof (tMLDHdr));
    if (pHostEntry->pBuf == NULL)
    {
        /* Sending report for this group for the first time.
         *          * So update the buffer
         * */

        pHostEntry->pBuf = &TempBuf;

        MLDFormHdr (MLD_REPORT, pHostEntry->u4IfIndex, pHostEntry->McastAddr,
                    (tMLDHdr *) pHostEntry->pBuf);

        Params.u1Cmd = IP6_LAYER4_DATA;
        Params.u1Proto = NH_ICMP6;
        Params.u4Len = sizeof (tMLDHdr);
        Params.u1Hlim = MLD_ONE;
        Params.u4Index = pHostEntry->u4IfIndex;
#ifdef IP6_WANTED
        if (NetIpv6GetIfInfo (pHostEntry->u4IfIndex, &NetIpv6IfInfo)
            != NETIPV6_SUCCESS)
        {
            pHostEntry->pBuf = NULL;
            return FAILURE;
        }
#endif
        MEMCPY (&Params.Ip6SrcAddr, &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr));
        MEMCPY (&Params.Ip6DstAddr, &pHostEntry->McastAddr, sizeof (tIp6Addr));
        pHostEntry->pParams = &Params;
    }

    pDupBuf = MLDAllocBuf (sizeof (tMLDHdr), MLD_ZERO);
    if (pDupBuf == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_INIT_SHUT_MODULE | MLD_ALL_MODULES, "MLD",
                  "Exiting MLDSendReport:Memory allocation Failed!!\n ");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDSendReport.\n");
        pHostEntry->pBuf = NULL;
        pHostEntry->pParams = NULL;
        return FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pDupBuf, (UINT1 *) pHostEntry->pBuf,
                                   MLD_ZERO, sizeof (tMLDHdr)) == CRU_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, "MLD",
                  "Failure in Copying"
                  " in  Buffer!!!\n");
        MLDRelBuf (pDupBuf, FALSE);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDSendReport.\n");
        pHostEntry->pBuf = NULL;
        pHostEntry->pParams = NULL;
        return FAILURE;
    }

    if (MLDSendPkt (pDupBuf, (tHlToIp6Params *) pHostEntry->pParams) == FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Could not send"
                  " packet to IP6 module\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDSendReport.\n");
    }
    else
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_CONTROL_PLANE_MODULE, "MLD",
                  "Exiting MLDSendReport:Successfully\n");
    }
    pHostEntry->pBuf = NULL;
    pHostEntry->pParams = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDSendReport.\n");
    return SUCCESS;
}

/****************************************************************************
 Function    :  MLDSendDone
 Input       :  Pointer to the host table.
 Output      :  None.
 Description :  This function sends a DONE.  If the MLD Hdr is 
                not updated in the entry, this function forms an MLD Hdr 
                and updates it in the entry.  The CRU Buffer is duplicated
                and the mld header is copied in the buffer and calls the 
                a function (MLDSendPkt()) to send the packet. 
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT1
MLDSendDone (tMLDHostEntry * pHostEntry)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tHlToIp6Params      Params;
    tMLDHdr             TempBuf;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tIp6Addr            Ip6DstAddr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDSendDone.\n");

    MEMSET (&Params, 0, sizeof (tHlToIp6Params));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&TempBuf, 0, sizeof (tMLDHdr));

    pHostEntry->pBuf = &TempBuf;

    MLDFormHdr (MLD_DONE, pHostEntry->u4IfIndex, pHostEntry->McastAddr,
                (tMLDHdr *) pHostEntry->pBuf);

    Params.u1Cmd = IP6_LAYER4_DATA;
    Params.u1Proto = NH_ICMP6;
    Params.u4Len = sizeof (tMLDHdr);
    Params.u1Hlim = MLD_ONE;
    Params.u4Index = pHostEntry->u4IfIndex;
#ifdef IP6_WANTED
    if (NetIpv6GetIfInfo (pHostEntry->u4IfIndex, &NetIpv6IfInfo)
        != NETIPV6_SUCCESS)
    {
        pHostEntry->pBuf = NULL;
        return FAILURE;
    }
#endif
    MEMCPY (&Params.Ip6SrcAddr, &NetIpv6IfInfo.Ip6Addr, sizeof (tIp6Addr));
    MEMCPY (&Params.Ip6DstAddr, &pHostEntry->McastAddr, sizeof (tIp6Addr));
    pHostEntry->pParams = &Params;

    pDupBuf = MLDAllocBuf (sizeof (tMLDHdr), MLD_ZERO);
    if (pDupBuf == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_INIT_SHUT_MODULE | MLD_ALL_MODULES, "MLD",
                  "Exiting MLDSendDone.:Memory allocation Failed!!\n ");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDSendDone..\n");
        pHostEntry->pBuf = NULL;
        pHostEntry->pParams = NULL;
        return FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pDupBuf, (UINT1 *) pHostEntry->pBuf,
                                   MLD_ZERO, sizeof (tMLDHdr)) == CRU_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, "MLD",
                  "Failure in Copying" " in  Buffer!!!\n");
        MLDRelBuf (pDupBuf, FALSE);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDSendDone..\n");
        pHostEntry->pBuf = NULL;
        pHostEntry->pParams = NULL;
        return FAILURE;
    }

    MLD_LINK_SCOPE_MCASTADDR (Ip6DstAddr);
    MEMCPY (&(((tHlToIp6Params *) pHostEntry->pParams)->Ip6DstAddr),
            &Ip6DstAddr, sizeof (tIp6Addr));

    if (MLDSendPkt (pDupBuf, (tHlToIp6Params *) pHostEntry->pParams) == FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Exiting MLDSendDone:Could not send"
                  " packet to IP6 module\n");
    }
    else
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_CONTROL_PLANE_MODULE, "MLD",
                  "Exiting MLDSendDone:Successfully\n");
    }
    pHostEntry->pBuf = NULL;
    pHostEntry->pParams = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDSendDone.\n");
    return SUCCESS;
}

/****************************************************************************
 Function    :  MLDSendPkt
 Input       :  tCRU_BUF_CHAIN_HEADER * pBuf MLD data packet
                tHlToIp6Params * pParams - Params required to send the packet
 Output      :  None.
 Description :  This function sends a MLD packet to the IP layer.
 Returns     :  MLD_SUCCESS/MLD_FAILURE
****************************************************************************/
INT4
MLDSendPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tHlToIp6Params * pParams)
{

#ifdef LNXIP6_WANTED
    if (MLDSendPktToIpv6 (pBuf, pParams) == FAILURE)
    {
        return OSIX_FAILURE;
    }
#else
#ifdef IP6_WANTED
    if (Ip6RcvFromHl (pBuf, pParams) == IP6_FAILURE)
    {
        return FAILURE;
    }
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pParams);
#endif
#endif

    return SUCCESS;
}

/****************************************************************************
 Function    :  MLDStartListening
 Input       :  u4IfIndex
                Multicast address
 Output      :  None.
 Description :  This function is called by any application when they want to
                join some multicast group.This functionality is used for 
                MLD host. Currently this is used as stub since Aricent MLD 
                supports only router functionality
 Returns     :  OSIX_SUCCESS/OSIX_FAILURE.
****************************************************************************/
INT4
MLDStartListening (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{

    tMLDHostEntry      *pHostEntry = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDStartListening.\n");

    pHostEntry = MLDGetHostEntry (u4IfIndex, pMcastAddr);
    if (pHostEntry != NULL)
    {
        /* Entry already has been added 
         * so just increment the usage count
         */
        pHostEntry->u2UsageCount++;
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_CONTROL_PLANE_MODULE, "MLD",
                  "Exiting MLDStartListening: Successfully\n");

        return SUCCESS;
    }
#ifdef IP6_WANTED
    if (NetIpv6McastJoin (u4IfIndex, pMcastAddr) == NETIPV6_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Exiting MLDStartListening: Failure in "
                  "adding multicast address at IP6 level\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDStartListening.\n");
        return FAILURE;
    }
#endif

    pHostEntry = MLDAddHostEntry (pMcastAddr, u4IfIndex);
    if (pHostEntry == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Exiting MLDStartListening: Failure in " "adding entry\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDStartListening.\n");
        return FAILURE;
    }
    /*sending the mld report */
    MLDSendReport (pHostEntry);
    pHostEntry->u1Flag = MLD_SET;
    pHostEntry->u1State = MLD_DELAY_LISTENER;
    pHostEntry->u2UsageCount++;

    MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE, "MLD",
              "Starting Report Delay Timer with"
              "Unsolicited report interval\n ");

    MLDStartTmr (gMldTimerListId, &((pHostEntry->tRepTmr).node),
                 MLD_UNSOLICITED_REPORT_INTERVAL * MLD_SYS_TICKS);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDStartListening.\n");
    return SUCCESS;

}

/****************************************************************************
 Function    :  MLDStopListening
 Input       :  u4IfIndex
                Multicast address
 Output      :  None.
 Description :  This function is called by any application when they want to
                join some multicast group.This functionality is used for 
                MLD host. Currently this is used as stub since Aricent MLD 
                supports only router functionality
 Returns     :  OSIX_SUCCESS/OSIX_FAILURE.
****************************************************************************/
INT4
MLDStopListening (UINT4 u4IfIndex, tIp6Addr * pMcastAddr)
{
    tMLDHostEntry      *pHostEntry = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MLDStopListening.\n");

    pHostEntry = MLDGetHostEntry (u4IfIndex, pMcastAddr);
    if (pHostEntry == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Host Entry is NULL\n ");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDStopListening..\n");
        return FAILURE;
    }

    if (pHostEntry->u2UsageCount != MLD_ONE)
    {
        /* Some other application is listening to the same group */
        pHostEntry->u2UsageCount--;
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_CONTROL_PLANE_MODULE, "MLD",
                  "Some other application is listening to the same group\n ");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
 		"Exit MLDStopListening..\n");
        return SUCCESS;
    }
#ifdef IP6_WANTED
    if (NetIpv6McastLeave (u4IfIndex, pMcastAddr) == NETIPV6_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_ALL_MODULES, "MLD",
                  "Exiting MLDStopListening.: Failure in "
                  "removing multicast address at IP6 level\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDStopListening..\n");
        return FAILURE;
    }
#endif

    if (pHostEntry->u1Flag == MLD_SET)
    {
        MLDSendDone (pHostEntry);
    }

    TmrStopTimer (gMldTimerListId, &(pHostEntry->tRepTmr.node));
    if (pHostEntry->pBuf)
        MEM_FREE (pHostEntry->pBuf);
    if (pHostEntry->pParams)
        MEM_FREE (pHostEntry->pParams);

    MLD_DBG_INFO (MLD_DBG_FLAG, MLD_CONTROL_PLANE_MODULE, "MLD",
              "Stopping Report Delay Timer with"
              "Unsolicited report interval\n ");
    MEMSET (pHostEntry, MLD_ZERO, sizeof (tMLDHostEntry));

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exit MLDStopListening.\n");

    return SUCCESS;

}

/*****************************************************************************/
/* Function Name      : MldpUtilCheckUpIface                                 */
/*                                                                           */
/* Description        : This function will check whether the interface is    */
/*                      configured as upstream interface                     */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interfcae Index                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_TRUE/IGMP_FALSE                                 */
/*****************************************************************************/
UINT1
MldpUtilCheckUpIface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : MLDGetGrpInterfacesList                              */
/*                                                                           */
/* Description        : This function will gets interface list for specified */
/*                      group                                                */
/*                                                                           */
/* Input(s)           : pSrvAddr   -Group address                            */
/*                                                                           */
/* Output(s)          : au1InterfaceList                                     */
/*                              Interface list                               */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT1
MLDGetGrpInterfacesList (tIp6Addr * pSrvAddr, UINT4 *au1InterfaceList)
{

    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpIface         *pIfaceNode = NULL;
    INT4                i4IgmpIfIndex = IGMP_ZERO;
    UINT4               u4IfIndex = 0;
    INT4                i4IgmpInterfaceAdminStatus = 0;
    UINT1               u1Count = 0;

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);
    if (pIfGetNextLink == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliShowGroupInfo.\n");
        return OSIX_FAILURE;
    }
    do
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            continue;
        }

        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        i4IgmpIfIndex = (INT4) pIfaceNode->u4IfIndex;

        MLD_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IgmpIfIndex, &u4IfIndex);
        if (CfaGetInterfaceNameFromIndex (u4IfIndex, au1InterfaceName) ==
            OSIX_FAILURE)
        {
            continue;
        }
        if (nmhGetFsMgmdInterfaceAdminStatus ((INT4) u4IfIndex,
                                              IPVX_ADDR_FMLY_IPV6,
                                              &i4IgmpInterfaceAdminStatus)
            == SNMP_FAILURE)
        {
            continue;
        }
        /*Check whether MLD is enabled on the VLAN */
        if (i4IgmpInterfaceAdminStatus != MLD_IFACE_ADMIN_DOWN)
        {
            MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));
            IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
            IgmpGrpEntry.pIfNode = pIfaceNode;

            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) & IgmpGrpEntry,
                                                 NULL);
            while (pGrp != NULL)
            {
                if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
                {
                    /* Group entry doesn't belong to specified interface index */
                    break;
                }
                if (pGrp->pIfNode->u1AddrType != pIfaceNode->u1AddrType)
                {
                    pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup, (tRBElem *) pGrp, NULL);
                    continue;
                }

                if (MEMCMP
                    (((tIp6Addr *) (VOID *) ((pGrp->u4Group).au1Addr)),
                     pSrvAddr, sizeof (tIp6Addr)) == IGMP_ZERO)
                {
                    au1InterfaceList[u1Count] = u4IfIndex;
                    u1Count++;
                    break;
                }

                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);
            }
        }

    }
    while ((pIfGetNextLink = (TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                            &(pIfaceNode->IfGetNextLink)))) !=
           NULL);
    if (u1Count == 0)
    {
        return OSIX_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exit MldCliShowGroupInfo.\n");
    return OSIX_SUCCESS;
}
