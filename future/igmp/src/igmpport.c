/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpport.c,v 1.51 2017/02/06 10:45:28 siva Exp $
 *
 * Description:This file contains the routines required for     
 *              porting IGMP on different targets                 
 *
 *******************************************************************/

#include "igmpinc.h"
#include <fssocket.h>
#if defined (LNXIP4_WANTED) && defined (NP_KERNEL_WANTED)
#include <chrdev.h>
#endif
#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE ;
#endif

tIgmpHostMcastGrpEntry      *IgmpHostAddGrpEntry (tIPvXAddr * Ip4Addr, UINT4 u4IfIndex,
                                     UINT1 u1Version);
tIgmpHostMcastGrpEntry      *IgmpHostGetGrpEntry (UINT4 u4IfIndex, tIPvXAddr * Ip4Addr);
VOID               IgmpHostRemoveGrpEntry (tIgmpHostMcastGrpEntry*);

/*gau1IgmpRawPkt is used to hold the IGMP packet info being sent to IP */
PRIVATE UINT1       gau1IgmpRawPkt[IGMP_MAX_MTU_SIZE];
/*********************************************************************/
/* Function           :   IgmpRegisterMRP                            */
/*                                                                   */
/* Description        :  This function is called from Multicast      */
/*                       Routing  protocol to register themselves    */
/*                       with IGMP                                   */
/*                                                                   */
/*                                                                   */
/* Input(s)           : handlinput is a function pointer used        */
/*                      whenever there is any packet is to be        */
/*                      redirected  to the Multicast Routing Prot    */
/*                                                                   */
/*                                                                   */
/* Output(s)          : none                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpRegisterMRP                       **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT1
IgmpRegisterMRP (UINT1 u1PktType,
                 VOID (*pInfn) (tCRU_BUF_CHAIN_HEADER *),
                 VOID (*pUpdIfs) (tNetIpv4IfInfo * pNetIpv4Info, UINT4),
                 VOID (*pFwdIfList) (VOID),
                 VOID (*pLJNoteFuncPtr) (tGrpNoteMsg))
{
    INT4                i4Index = IGMP_ZERO;

    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        return IGMP_OK;
    }

    i4Index = IgmpFindRegisterEntry (u1PktType);
    if (i4Index == IGMP_INVALID_REGISTER)
    {
        if ((i4Index = IgmpFindFreeRegisterEntry ()) == IGMP_INVALID_REGISTER)
        {
            return IGMP_NOT_OK;
        }
    }
    gaIgmpRegister[i4Index].u1ProtId = u1PktType;
    gaIgmpRegister[i4Index].pHandleInput = pInfn;
    gaIgmpRegister[i4Index].pUpdateIFace = pUpdIfs;
    gaIgmpRegister[i4Index].pFwdIfList = pFwdIfList;
    gaIgmpRegister[i4Index].pLJNoteFuncPtr = pLJNoteFuncPtr;
    gaIgmpRegister[i4Index].u1Flag = IGMP_UP;
    IgmpCurrMbrInfoToMrp (IPVX_ADDR_FMLY_IPV4);

    return IGMP_OK;
}

/*********************************************************************/
/* Function           : IgmpDeRegisterMRP                           */
/*                                                                   */
/* Description        : De register with IGMP                        */
/*                                                                   */
/* Input(s)           : MRP identifier,  Function pointer to be      */
/*                      to be called when a packet arrives for the   */
/*                      MRP. Function pointer to handle interface    */
/*                      changes, Function pointer to call for multica*/
/*                      Routes, Function pointer to call to inform   */
/*                      Leave or join updates.                       */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpDeRegisterMRP                     **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT1
IgmpDeRegisterMRP (UINT1 u1PktType)
{
    INT4                i4Index = IGMP_ZERO;

    i4Index = IgmpFindRegisterEntry (u1PktType);

    if (i4Index != IGMP_INVALID_REGISTER)
    {
        gaIgmpRegister[i4Index].u1Flag = IGMP_DOWN;
        gaIgmpRegister[i4Index].u1ProtId = IGMP_ZERO;
        gaIgmpRegister[i4Index].pHandleInput = NULL;
        gaIgmpRegister[i4Index].pUpdateIFace = NULL;
        gaIgmpRegister[i4Index].pFwdIfList = NULL;
        gaIgmpRegister[i4Index].pLJNoteFuncPtr = NULL;
        return IGMP_OK;
    }

    return IGMP_NOT_OK;
}

/*********************************************************************/
/* Function           :  IgmpFindRegisterEntry                       */
/*                                                                   */
/* Description        : function will find whether give multicast    */
/*                       protocl  is registered with IGMP or not     */
/*                                                                   */
/* Input(s)           : register table, keys.                        */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : returns -1 if found otherwise index          */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpFindRegisterEntry                 **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT4
IgmpFindRegisterEntry (UINT1 u1ProtId)
{
    INT4                i4Index;

    for (i4Index = IGMP_ZERO; i4Index < IGMP_MAX_REGISTER; i4Index++)
    {
        if (gaIgmpRegister[i4Index].u1ProtId == u1ProtId)
        {
            return i4Index;
        }
    }

    return IGMP_INVALID_REGISTER;
}

/*********************************************************************/
/* Function           : IgmpFindFreeRegisterEntry                    */
/*                                                                   */
/* Description        : Finds the free entry in the given register   */
/*                                                                   */
/* Input(s)           : Registration register                        */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : freeindex                                    */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpFindFreeRegisterEntry             **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
INT4
IgmpFindFreeRegisterEntry (VOID)
{
    INT4                i4Index;

    for (i4Index = IGMP_ZERO; i4Index < IGMP_MAX_REGISTER; i4Index++)
    {
        if (gaIgmpRegister[i4Index].u1Flag == DOWN)
        {
            return i4Index;
        }
    }

    return IGMP_INVALID_REGISTER;
}

/***************************************************************************
 * Function Name    :  IgmpHandleIfProtocolDownStatus
 *
 * Description      :  This function is used to post the status change info
 *                     about the IGMP status in an interface.  
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  u4Event
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

PUBLIC INT4
IgmpHandleIfProtocolDownStatus (UINT4 u4Port, UINT1 u1AddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			"Entering  IgmpHandleIfProtocolDownStatus \n");
#ifdef PIM_WANTED
    SparsePimHandleIgmpIfDown (u4Port, u1AddrType);
#else
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1AddrType);
#endif
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting  IgmpHandleIfProtocolDownStatus \n");
    return SUCCESS;
}


/***************************************************************************
 * Function Name    :  IgmpHandleProtocolDownStatus
 *
 * Description      :  This function is used to post the status change info
 *                     about the IGMP module.  
 *
 * Global Variables 
 * Referred         :  None
 *
 * Global Variables 
 * Modified         :  None
 *              
 * Input (s)        :  u4Event
 *
 * Output (s)       :  None
 *
 * Returns          :  None
 ****************************************************************************/

PUBLIC INT4
IgmpHandleProtocolDownStatus (UINT1 u1AddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			"Entering  IgmpHandleProtocolDownStatus \n");
#ifdef PIM_WANTED
    SparsePimHandleIgmpStatus (u1AddrType);
#else
    UNUSED_PARAM (u1AddrType);
#endif
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting  IgmpHandleProtocolDownStatus \n");
    return SUCCESS;
}


/*********************************************************************/
/* Function           : IgmpHandleIfUpdates                          */
/* Description        : This function is called by IP whenever       */
/*                      thereis any change in the interface          */
/*                      attributes. This function ptr is passed to   */
/*                      IP at the time of IGMP Registrtaion with IP. */
/*                      data structures.                             */
/*                                                                   */
/* Input(s)           : pointer to the  interface                    */
/*                        record whose contents are changed.         */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpHandleIfUpdates                   **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                  **/
/*********************************************************************/
VOID
IgmpHandleIfUpdates (tNetIpv4IfInfo * pIfInfo, UINT4 u4BitMap)
{
    tIgmpQMsg          *pQMsg = NULL;
    INT4                i4Index = IGMP_NOT_OK;
	UINT4				u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			"Entering  IgmpHandleIfUpdates \n");

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        return;
    }
	/*initialize for secondary IP*/
	pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4SecIPAddr = IGMP_ZERO;
	pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4SecIPMask = IGMP_ZERO;
    /* Check if the Event is Oper status change or
     * Interface deletion event */
    if (u4BitMap == IGMP_DELETE_INTERFACE)
    {
        pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1MsgType = IGMP_DELETE_INTERFACE;
    }
    else if (u4BitMap == OPER_STATE)
    {
        pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1MsgType = IGMP_IF_STATUS_CHANGE;
    }
    else if (u4BitMap & IF_SECIP_DELETED)
    {
        /*raise a seperate event to handle secondary IP delete*/
        pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1MsgType = IGMP_SEC_IFACE_DELETE;
        if(IGMP_IP_GET_PORT_FROM_IFINDEX (pIfInfo->u4IfIndex, &u4Port) == NETIPV4_FAILURE)
		{
        	IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "IGMP_IP_GET_PORT_FROM_IFINDEX %d failed !!\r\n",pIfInfo->u4IfIndex);
			MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
					"Exiting  IgmpHandleIfUpdates \n");
			IGMP_QMSG_FREE (pQMsg);
			return;
		}
        /*port no. is stored in place of IfIndex*/
        pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4IfIndex = u4Port;
	pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4CfaIfIndex = pIfInfo->u4IfIndex;
		pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4SecIPAddr = pIfInfo->u4Addr;
	    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4SecIPMask = pIfInfo->u4NetMask;
    }	
    else
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE ,
                  IGMP_NAME, "If Change - NO Oper Status Change \n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			"Exiting  IgmpHandleIfUpdates \n");
        IGMP_QMSG_FREE (pQMsg);
        return;
    }

    pQMsg->u4MsgType = IGMP_IP_IF_CHG_EVENT;
    if(!(u4BitMap & IF_SECIP_DELETED))
    {
    	if (IGMP_IP_GET_IFINDEX_FROM_PORT (pIfInfo->u4IfIndex,
                                       &(pQMsg->IgmpQMsgParam.
                                         IgmpIfStatusInfo.u4CfaIfIndex))
        	== NETIPV4_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "IGMP_IP_GET_IFINDEX_FROM_PORT %d failed !!\r\n",pIfInfo->u4IfIndex);
        IGMP_QMSG_FREE (pQMsg);
        return;
    }
    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4IfIndex = pIfInfo->u4IfIndex;
    }
    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1IfStatus = (UINT1) pIfInfo->u4Oper;
    pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u1AddrType = IPVX_ADDR_FMLY_IPV4;

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Enqueuing I/f change info from IP to IGMP - FAILED \n");
	IGMP_TRC_ARG1(IGMP_TRC_FLAG, IGMP_CNTRL_MODULE , IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                 "Enqueuing I/f %d change info from IP to IGMP - FAILED \n",
                   pQMsg->IgmpQMsgParam.IgmpIfStatusInfo.u4IfIndex);

        IGMP_QMSG_FREE (pQMsg);
        return;
    }
    /* Send a EVENT to IGMP */
    if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE , IGMP_NAME,
                  "Sending a event from IP to IGMP - FAILED \n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Sending a event from IP to IGMP - FAILED \n");
        return;
    }

/* update  i/f  changes  to Register MRP's */
    for (i4Index = 0; i4Index < IGMP_MAX_REGISTER; i4Index++)
    {
        if (gaIgmpRegister[i4Index].u1Flag == IGMP_UP)
        {
            if (gaIgmpRegister[i4Index].pUpdateIFace != NULL)
            {
                gaIgmpRegister[i4Index].pUpdateIFace (pIfInfo, u4BitMap);
            }
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting  IgmpHandleIfUpdates \n");
}

/*********************************************************************/
/* Function           : IgmpHandleIncomingPkt                        */
/*                                                                   */
/* Description        : function called by IP wheenver IP recieves   */
/*                      IGMP packet. It is passed to IP at the       */
/*                      time of IGMP is registering with IP. This    */
/*                      function enqueues the received packet into   */
/*                      IGMP queue.                                  */
/*                                                                   */
/* Input(s)           : messagebuffer                                */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/

PUBLIC VOID
IgmpHandleIncomingPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                       UINT4 u4Index, tIP_INTERFACE IfId, UINT1 u1Flag)
{
    tIgmpQMsg          *pQMsg = NULL;

    UNUSED_PARAM (u1Flag);
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (IfId);

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    pQMsg->u4MsgType = IGMP_PKT_RECV_EVENT;
    pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt = pBuf;

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) !=
        OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Enqueuing I/f change info from IP to IGMP - FAILED \n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Enqueuing I/f change info from IP to IGMP - FAILED \n");
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        IGMP_QMSG_FREE (pQMsg);
        return;
    }

    /* Send a EVENT to IGMP */
    if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "Sending a event from IP to IGMP - FAILED \n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Sending a event from IP to IGMP - FAILED \n");
        return;
    }

    return;
}

/*************************************************************************
 * Function Name      : IgmpSendPktToIp
 *
 * Description          : This routine fills the unicast params structure
 *                and sends the packet.
 *
 * Global Variables
 * Referred          : None
 *
 * Gobal Variables
 * Modified          : None
 *
 * Input (s)          : Src,dest, protocol, tos, ttl, Buffer, length,
 *                Default Id, Df flag, option length and
 *                interface index.
 *
 * Output (s)          : None.
 *
 * Returns          : IP_SUCCESS or IP_FAILURE
 **************************************************************************/

INT4
IgmpSendPktToIp (tIP_BUF_CHAIN_HEADER * pBuf, tIgmpIPvXAddr u4Src,
                 tIgmpIPvXAddr u4Dest, UINT2 u2Len, UINT2 u2Port,
                 UINT1 u1PktType)
{
    struct sockaddr_in  DestAddr;
    INT4                i4SendBytes;
    UINT4               u4IpSrc = IGMP_ZERO;
    UINT4               u4IpDest = IGMP_ZERO;
    INT4                i4OptVal;
    struct in_addr      IfAddr;
    struct ip_mreqn     MCReq;

    MEMSET (&MCReq, ZERO, sizeof (struct ip_mreqn));
    MEMSET (&gau1IgmpRawPkt, IGMP_ZERO, IGMP_MAX_MTU_SIZE);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, u4Src, IGMP_IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpDest, u4Dest, IGMP_IPVX_ADDR_FMLY_IPV4);
    IgmpFormIPHdr (pBuf, u4IpSrc, u4IpDest, u1PktType, u2Len);

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &gau1IgmpRawPkt, 0, u2Len) ==
        CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return IGMP_NOT_OK;
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    MEMSET (&(DestAddr), 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4IpDest);
    DestAddr.sin_port = 0;

    IfAddr.s_addr = OSIX_HTONL (u4IpSrc);

#ifdef LNXIP4_WANTED
    i4OptVal = 1;
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           IP_MULTICAST_TTL, &i4OptVal, sizeof (i4OptVal)) < 0)
    {
        perror ("Unable to set IP_MULTICAST_TTL: socket option failed\n");
        return IGMP_NOT_OK;
    }
#endif

    i4OptVal = IGMP_TRUE;
    if (IGMP_SET_SOCK_OPT
        (gIgmpConfig.i4SocketId, IPPROTO_IP, IP_HDRINCL, (VOID *) &i4OptVal,
         sizeof (i4OptVal)) < 0)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "Set socket option failed for IP_HDRINCL\r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),          
		"Set socket option failed for IP_HDRINCL\r\n");
        return IGMP_NOT_OK;
    }

    MEMCPY (&MCReq.imr_address, &IfAddr,
                                sizeof (struct in_addr));
    MCReq.imr_ifindex = u2Port;

    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP, IP_MULTICAST_IF,
                           (char *) &(MCReq), sizeof (struct ip_mreqn)) < 0)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "Setsockopt Failed for IP_MULTICAST_IF\n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IgmpGetModuleName(IGMP_MGMT_MODULE),  
		"Setsockopt Failed for IP_MULTICAST_IF\n");
        return IGMP_NOT_OK;
    }

    if ((i4SendBytes = IGMP_SEND_TO ((gIgmpConfig.i4SocketId), &gau1IgmpRawPkt,
                                     u2Len, 0,
                                     ((struct sockaddr *) &DestAddr),
                                     (sizeof (struct sockaddr_in)))) < 0)
    {
        return IGMP_NOT_OK;
    }

    return IGMP_OK;
}

#ifdef LNXIP4_WANTED
/*****************************************************************************/
/*  Function Name   : IgmpIpCalcCheckSum                                     */
/*  Description     : Routine to calculate  checksum.                        */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pBuf         - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*  Output(s)       : None.                                                  */
/*  Returns         : The Checksum                                           */
/*              THE INPUT  BUFFER IS ASSUMED TO BE IN NETWORK BYTE ORDER     */
/*              AND RETURN VALUE  WILL BE IN HOST ORDER , SO APPLICATION     */
/*              HAS TO CONVERT THE RETURN VALUE APPROPRIATELY                */
/*****************************************************************************/
UINT2
IgmpIpCalcCheckSum (UINT1 *pBuf, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) (VOID *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = ~((UINT2) u4Sum);

    return (OSIX_NTOHS (u2Tmp));
}
#endif

/*************************************************************************
 * Function Name          : IgmpReStartGrpTmrToLmqTmr
 *
 * Description            : This function performs the following checks 
 *                            
 * Global Variables 
 * Referred               : None
 *
 * Gobal Variables 
 * Modified               : None
 *              
 * Input (s)              : Interface index
 *
 * Output (s)             : none 
 *
 * Returns                : 
 *************************************************************************/
VOID
IgmpReStartGrpTmrToLmqTmr (tIgmpIface * pIfNode, tIgmpGroup * pGrp)
{
    UINT4               u4DurationSec = IGMP_ZERO;
    UINT4               u4DurationMSec = IGMP_ZERO;
    UINT4               u4RemTime = IGMP_ZERO;

    u4DurationSec = (IGMP_IF_LAST_MEMBER_QIVAL (pIfNode) / IGMP_TEN) *
        IGMP_IF_LAST_MEMBER_QRY_COUNT (pIfNode);

    u4DurationMSec = (((IGMP_IF_LAST_MEMBER_QIVAL (pIfNode) % IGMP_TEN) *
                       IGMP_IF_LAST_MEMBER_QRY_COUNT (pIfNode) * 100));

    if (TmrGetRemainingTime (gIgmpTimerListId,
                             &(pGrp->Timer.TimerBlk.TimerNode),
                             &u4RemTime) != TMR_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "Tmr Get remaining time failure ! \n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_TMR_MODULE, IgmpGetModuleName(IGMP_TMR_MODULE),
		  "Tmr Get remaining time failure ! \n");
    }

    if (u4RemTime > u4DurationSec)
    {
        if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
        {
            IGMPSTOPTIMER (pGrp->Timer);
        }

        IGMPSTARTTIMER (pGrp->Timer, IGMP_GROUP_TIMER_ID, u4DurationSec,
                        u4DurationMSec);
    }

    return;
}

/*************************************************************************
 * Function Name          : IgmpReStartSrcTmrToLmqTmr
 *
 * Description            : This function performs the following checks 
 *                            
 * Global Variables 
 * Referred               : None
 *
 * Gobal Variables 
 * Modified               : None
 *              
 * Input (s)              : Interface index
 *
 * Output (s)             : none 
 *
 * Returns                : 
 *************************************************************************/
VOID
IgmpReStartSrcTmrToLmqTmr (tIgmpIface * pIfNode, tIgmpSource * pSrc)
{
    UINT4               u4Duration = IGMP_ZERO;
    UINT4               u4RemTime = IGMP_ZERO;

    u4Duration = (IGMP_IF_LAST_MEMBER_QIVAL (pIfNode) / IGMP_TEN) *
        IGMP_IF_LAST_MEMBER_QRY_COUNT (pIfNode);

    if (TmrGetRemainingTime (gIgmpTimerListId,
                             &(pSrc->srcTimer.TimerBlk.TimerNode),
                             &u4RemTime) != TMR_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "Tmr Get remaining time failure ! \n");
	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_TMR_MODULE, IgmpGetModuleName(IGMP_TMR_MODULE),
                  "Tmr Get remaining time failure ! \n");
    }

    if ((u4RemTime / 100) > u4Duration)
    {
        pSrc->u1SrsBitFlag = IGMP_TRUE;
        if ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET)
        {
            IGMPSTOPTIMER (pSrc->srcTimer);
        }

        IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID, u4Duration,
                        IGMP_ZERO);
    }

    return;
}

/*************************************************************************
 * Function Name          : IgmpReStartGrpTmr
 *
 * Description            : This function performs the following checks 
 *                            
 * Global Variables 
 * Referred               : None
 *
 * Gobal Variables 
 * Modified               : None
 *              
 * Input (s)              : Interface index
 *
 * Output (s)             : none 
 *
 * Returns                : 
 *************************************************************************/
VOID
IgmpReStartGrpTmr (tIgmpIface * pIfNode, tIgmpGroup * pGrp)
{
    if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pGrp->Timer);
    }

    IGMPSTARTTIMER (pGrp->Timer, IGMP_GROUP_TIMER_ID,
                    (UINT2) IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);

    return;
}

/*************************************************************************
 * Function Name          : IgmpReStartGrpSrcSchQryTmr
 *
 * Description            : This function performs the following checks 
 *                            
 * Global Variables 
 * Referred               : None
 *
 * Gobal Variables 
 * Modified               : None
 *              
 * Input (s)              : Interface index
 *
 * Output (s)             : none 
 *
 * Returns                : 
 *************************************************************************/
VOID
IgmpReStartGrpSrcSchQryTmr (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQry)
{
    UINT4               u4Duration = IGMP_ZERO;

    u4Duration = (IGMP_IF_LAST_MEMBER_QIVAL (pIfNode) / IGMP_TEN);

    if ((pSchQry->schTimer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pSchQry->schTimer);
    }

    IGMPSTARTTIMER (pSchQry->schTimer, IGMP_QUERY_SCHEDULE_TIMER_ID,
                    u4Duration, IGMP_ZERO);

    return;
}

/*************************************************************************
 * Function Name          : IgmpUpdCurrMbrInfoToMrp
 *
 * Description            : This function performs the following checks
 *
 * Global Variables
 * Referred               : None
 *
 * Gobal Variables
 * Modified               : None
 *
 * Input (s)              : Interface index
 *
 * Output (s)             : none
 *
 * Returns                :
 *************************************************************************/
VOID
IgmpCurrMbrInfoToMrp (UINT1 u1AddrType)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;

    IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Updating current group membership with address type %d to MRP\r\n",u1AddrType);

    IGMP_LOCK ();
    if (gIgmpGroup == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "No Group entries found in the group database\r\n");
        IGMP_UNLOCK ();
        return;
    }

    pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);

    while (pGrp != NULL)
    {
        pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) pGrp, NULL);
        if (pGrp->pIfNode->u1AddrType != u1AddrType)
        {
            pGrp = pNextGrp;
            continue;
        }
        if ((pGrp->u1StaticFlg == IGMP_TRUE) &&
            (pGrp->u1EntryStatus != IGMP_ACTIVE))
        {
            pGrp = pNextGrp;
            continue;
        }

        /* SrcList can be > 0 for IGMPv2 interface with SSM
         * mapping configured, in which case handle the sources
         * in UpdateMrp */
        if (((pGrp->pIfNode->u1Version == IGMP_VERSION_2) &&
            (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO)) ||
            (pGrp->pIfNode->u1Version == MLD_VERSION_1))
        {
            /* Updating MRPs */
            IgmpUpdateMrps (pGrp, IGMP_JOIN);
        }
        else
        {
            if (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO)
            {
                /* Updating MRPs */
                IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                pGrp = pNextGrp;
                continue;
            }

            for (pSrc = (tIgmpSource *)
                 TMO_SLL_First (&(pGrp->srcList));
                 pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
            {
                pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);

                if ((pSrc->u1StaticFlg == IGMP_TRUE) &&
                    (pSrc->u1EntryStatus != IGMP_ACTIVE))
                {
                    continue;
                }

                if (pSrc->u1FwdState == IGMP_TRUE)
                {
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                }
            }                    /* Scan for SourceNode - END */

            if (pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)
            {
                if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                {
                    /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
            }
            else if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
            {
                if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                {
                    /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
                else
                {
                   /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                }
            }
        }
        pGrp = pNextGrp;
    }
    IGMP_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpCreateSocket                                           */
/*                                                                           */
/* Description  : Create Socket                                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpCreateSocket (VOID)
{
    INT4                i4SockFd;

    i4SockFd = IGMP_SOCKET (AF_INET, SOCK_RAW, IGMP_PROTID);
    if (i4SockFd == FAILURE)
    {
        perror ("Unable to create IGMP socket\n");
        return IGMP_FAILURE;
    }

    gIgmpConfig.i4SocketId = i4SockFd;

    return IGMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpCloseSocket                                            */
/*                                                                           */
/* Description  : Close Socket                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IgmpCloseSocket (VOID)
{
    IGMP_CLOSE (gIgmpConfig.i4SocketId);
    gIgmpConfig.i4SocketId = -1;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpNotifyPktArrivalEvent                                  */
/*                                                                           */
/* Description  : Function to notify IGMP task about packet reception        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
IgmpNotifyPktArrivalEvent (INT4 i4SocketId)
{
    /* Send a EVENT to IGMP */
    UNUSED_PARAM (i4SocketId);
    OsixEvtSend (gIgmpTaskId, IGMP_PACKET_ARRIVAL_EVENT);
}

/*     LINUX IP WANTED */
#ifdef LNXIP4_WANTED
#ifndef ISS_WANTED
/* Only for IGMP stack on Linuxip. Not for ISS */
/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpSetRecvSocketOptions                                   */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpSetRecvSocketOptions (VOID)
{
    INT4                i4Option = 0;
    UINT1               u1Option = 0;

    if ((IGMP_FCNTL (gIgmpConfig.i4SocketId, F_SETFL, O_NONBLOCK)) < 0)
    {
        return IGMP_FAILURE;
    }

    i4Option = TRUE;
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           MRT_INIT, &i4Option, sizeof (i4Option)) < 0)
    {
        perror ("Unable to set MRT_INIT: socket option failed\n");
        return IGMP_FAILURE;
    }

    i4Option = TRUE;
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP, IP_PKTINFO,
                           (UINT1 *) &i4Option, sizeof (i4Option)) < 0)
    {
        perror ("Unable to set IP_PKTINFO: socket option failed\n");
        return IGMP_FAILURE;
    }

    return IGMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpResetSocketOption                                      */
/*                                                                           */
/* Description  : Function to reset socket options                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpResetSocketOption (VOID)
{
    INT4                i4Option = 0;

    i4Option = TRUE;
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           MRT_DONE, &i4Option, sizeof (i4Option)) < 0)
    {
        perror ("Unable to set MRT_INIT: socket option failed\n");
        return IGMP_FAILURE;
    }

    return IGMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpEnableMcastSocketOption                                */
/*                                                                           */
/* Description  : Function to add a virtual interface                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpSetMcastSocketOption (tIgmpIface * pIfNode, UINT1 u1IfStatus)
{
    struct vifctl       viface;
    struct in_addr      Address;
    UINT4               u4Src = IGMP_ZERO;

    IGMP_IP_COPY_FROM_IPVX (&u4Src, pIfNode->u4IfAddr,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    Address.s_addr = OSIX_HTONL (u4Src);

    MEMSET (&viface, 0, sizeof (viface));
    viface.vifc_vifi = pIfNode->u4IfIndex;
    viface.vifc_flags = 0x4;
    viface.vifc_threshold = 1;
    viface.vifc_rate_limit = 0;
    viface.vifc_lcl_addr = Address;

    if (u1IfStatus == IGMP_UP)
    {
        if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP, MRT_ADD_VIF,
                               (UINT1 *) &viface, sizeof (viface)) < 0)
        {
            perror ("Unable to set MRT_ADD_VIF: socket option failed\n");
            return IGMP_FAILURE;
        }
    }
    else
    {
        if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP, MRT_DEL_VIF,
                               (UINT1 *) &viface, sizeof (viface)) < 0)
        {
            perror ("Unable to set MRT_DEL_VIF: socket option failed\n");
            return IGMP_FAILURE;
        }
    }
    return IGMP_SUCCESS;
}

#else /* ISS_WANTED */
#ifdef NP_KERNEL_WANTED
/* Only for BCM Linuxip */
/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpCreateRecvTask                                         */
/*                                                                           */
/* Description  : This function opens the IGMP char device and creates a     */
/*                thread for reading the packets                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpCreateRecvTask (VOID)
{
    gi4IgmpDevFd = FileOpen ((const UINT1 *) IGMP_DEVICE_FILE_NAME,
                             OSIX_FILE_RO);
    if (gi4IgmpDevFd < 0)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Can't open device file: <%s>\n", IGMP_DEVICE_FILE_NAME);

        return IGMP_FAILURE;
    }

    if (OsixTskCrt ((UINT1 *) IGMP_RECV_TASK_NAME, IGMP_RECV_TASK_PRIORITY,
                    IGMP_STACK_SIZE,
                    (OsixTskEntry) IgmpRecvTaskMain, 0,
                    &gIgmpRecvTaskId) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Osix Create Task Failed for reading thread!!! \n");
        FileClose (gi4IgmpDevFd);
        return IGMP_FAILURE;
    }

    return IGMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpDeleteRecvTask                                         */
/*                                                                           */
/* Description  : Closes the char device and deletes the reading thread      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpDeleteRecvTask (VOID)
{
    FileClose (gi4IgmpDevFd);
    OsixTskDel (gIgmpRecvTaskId);
    return IGMP_SUCCESS;
}
#else /* NP_KERNEL_WANTED */
/* For Linuxip other than BCM */
/*************************************************************************
 * Function           : IgmpNetIpReceivePkt
 *                                                                 
 * Description        : Function registered as callback with netip/lnxip
 *                      to receive IGMP packet.  This  
 *                      function enqueues the received packet into 
 *                      IGMP queue.                                
 *                                                                 
 * Input(s)           : messagebuffer                              
 *                                                                 
 * Output(s)          : None                                       
 *                                                                 
 * Returns            : None                                       
 *                                                                 
 *************************************************************************/
PUBLIC VOID
IgmpNetIpReceivePkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIgmpQMsg          *pQMsg = NULL;

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    pQMsg->u4MsgType = IGMP_PKT_RECV_EVENT;
    pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt = pBuf;

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) !=
        OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Enqueuing I/f change info from IP to IGMP - FAILED \n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Enqueuing I/f change info from IP to IGMP - FAILED \n");
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_CNTRL_MODULE,
                                        IGMP_NAME,IgmpSysErrString[QUEUE_SEND_FAIL],
				"for Queue id  %d",gIgmpQId);
	CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        IGMP_QMSG_FREE (pQMsg);
        return;
    }

    /* Send a EVENT to IGMP */
    if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Sending a event from IP to IGMP - FAILED \n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE , IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Sending a event from IP to IGMP - FAILED \n");
	SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_CNTRL_MODULE,
                                        IGMP_NAME,IgmpSysErrString[EVENT_SEND_FAIL],
					"for task Id %d",gIgmpTaskId);
        return;
    }

    return;
}
#endif /* NP_KERNEL_WANTED */
#endif /* ISS_WANTED */

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpSetSendSocketOptions                                   */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IGMP_SUCCESS                                               */
/*                IGMP_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IgmpSetSendSocketOptions (VOID)
{
    INT4                i4Option = 0;

    i4Option = TRUE;
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           IP_MULTICAST_TTL, &i4Option, sizeof (i4Option)) < 0)
    {
        perror ("Unable to set IP_MULTICAST_TTL: socket option failed\n");
        return IGMP_FAILURE;
    }

    i4Option = FALSE;
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           IP_MULTICAST_LOOP, &i4Option, sizeof (i4Option)) < 0)
    {
        perror ("Unable to set IP_MULTICAST_LOOP: socket option failed\n");
        return IGMP_FAILURE;
    }

    return IGMP_SUCCESS;
}

#endif /* LNXIP4_WANTED */

/*****************************************************************************/
/* Function Name      : IgmpFilterPackets                                    */
/*                                                                           */
/* Description        : This function will check whether the IGMP packets    */
/*                      needs  to be processed                               */
/*                                                                           */
/* Input(s)           : pIgmpFilterInfo - IGMP filter information            */
/*                                                                           */
/* Return Value(s)    : IGMP_PERMIT/ IGMP_DENY                                 */
/*****************************************************************************/

BOOL1
IgmpFilterPackets (tIgmpFilter * pIgmpFilterInfo)
{
    /* currently this function is stub, always returns PERMIT */
    UNUSED_PARAM (pIgmpFilterInfo);
    return IGMP_PERMIT;
}

/****************************************************************************/
/* Function           : IgmpCheckProxyStatus                                */
/*  Description       : This function checks the igmp proxy status          */
/* Input(s)           : None                                                */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : TRUE/FALSE                                          */
/****************************************************************************/
INT4
IgmpCheckProxyStatus (VOID)
{
    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        return TRUE;
    }
    return FALSE;

}

/****************************************************************************/
/* Function           : IgmpIsEnableOnInterface                             */
/*  Description       : This function checks the igmp tatus on interface    */
/* Input(s)           : u4IfIndex- Interface index                          */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : TRUE/FALSE                                          */
/****************************************************************************/

INT4
IgmpIsEnableOnInterface (UINT4 u4IfIndex)
{
    tIgmpIface         *pIgmpIfNode = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering  IgmpIsEnableOnInterface \n");
    pIgmpIfNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);
    if (NULL == pIgmpIfNode)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "IGMP is not enabled on %d interface \r\n", u4IfIndex);
        IGMP_TRC_ARG1(IGMP_TRC_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES, 
		      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "IGMP is not enabled on %d interface \r\n", u4IfIndex);
        return FALSE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting  IgmpIsEnableOnInterface \n");
    return TRUE;
}

/*****************************************************************************/
/* Function Name      : IgmpSendReportOrLeave                                */
/*                                                                           */
/* Description        : This function will construct and send the V1/V2/V3   */
/*                      reports/leave                                        */
/*                                                                           */
/* Input(s)           : u4GrpAddr - Ip address of the group                  */
/*                      u1Leave - Whether the report to be sent is a leave   */
/*                      u4IfIndex - Send report to only this interface.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
IgmpSendReportOrLeave (tIPvXAddr IPvXGrpAddr, UINT1 u1Leave, UINT4 u4IfIndex)
{
    tIgmpIface         *pIgmpIfNode = NULL;
    tIgmpHostMcastGrpEntry      *pGrpEntry = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering  IgmpSendReportOrLeave \n");

    pIgmpIfNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);

    if (pIgmpIfNode == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "IGMP is not enabled on %d interface \r\n", u4IfIndex);
	IGMP_TRC_ARG1(IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "IGMP is not enabled on %d interface \r\n", u4IfIndex);

        return;
    }
    
    pGrpEntry = IgmpHostGetGrpEntry(u4IfIndex, &IPvXGrpAddr); 
    if(((u1Leave == IGMP_FALSE) && (pGrpEntry != NULL)) ||
       ((u1Leave == IGMP_TRUE) &&(pGrpEntry == NULL) ))
    {
        /* Entry already has been added */
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Entry exists for IP:%s in IGMPHostMcastGrp table."
                  "Exiting IgmpSendReportOrLeave: Successfully\n",
                  IgmpPrintIPvxAddress(IPvXGrpAddr));
        return;
    }
    if (pIgmpIfNode->u1Version != IGMP_VERSION_3)
    {
        if (IgmpFormAndSendV1V2Report (IPvXGrpAddr, pIgmpIfNode->u1Version,
                                       u1Leave, u4IfIndex) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                      IGMP_NAME,
                      "Sending of V1/V2 report to upstream failed\r\n");
            IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE ,
                      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "Sending of V1/V2 report to upstream failed\r\n");
            return;
        }
    }  
    if(u1Leave == IGMP_TRUE)
    {
        /* Entry exists, stop the Timer and remove the entry from RBTree */
        IGMPSTOPTIMER (pGrpEntry->Timer);
        IgmpHostRemoveGrpEntry (pGrpEntry);
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "Removed the entry for IP:%s from IGMPHostMcastGrp table\r\n",
                  IgmpPrintIPvxAddress(IPvXGrpAddr));

    }
    else
    {
        /* Add an entry for Mcast Ip in RBTree and start the timer */
        pGrpEntry = IgmpHostAddGrpEntry (&IPvXGrpAddr, u4IfIndex, pIgmpIfNode->u1Version);
        if (pGrpEntry == NULL)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,                                                                                 
                      "Exiting IgmpSendReportOrLeave: Failure in adding entry for Mcast Ip %s.\n", 
                      IgmpPrintIPvxAddress(IPvXGrpAddr));
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			IgmpGetModuleName(IGMP_EXIT_MODULE),"Exiting IgmpSendReportOrLeave.\n");
            return;
        }
        IGMPSTARTTIMER (pGrpEntry->Timer, IGMP_HOST_MCAST_GROUP_TIMER_ID, IGMP_HOST_MCAST_JOIN_TIME,
                        (UINT4) SYS_NUM_OF_TIME_UNITS_IN_A_SEC * IGMP_HOST_MCAST_JOIN_TIME);    
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting  IgmpSendReportOrLeave \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgmpFormAndSendV1V2Report                            */
/*                                                                           */
/* Description        : This function will construct and send the V1/V2 report*/
/*                                                                           */
/* Input(s)           : u4GrpAddr - Ip address of the group                  */
/*                      u1Version  - IGMP version (V1/V2)                    */
/*                      u1Leave - Whether the report to be sent is a leave   */
/*                      u4IfIndex - Send report to only this interface.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgmpFormAndSendV1V2Report (tIgmpIPvXAddr u4GrpAddr, UINT1 u1Version,
                           UINT1 u1Leave, UINT4 u4IfIndex)
{
    UINT4               u4PktSize = IGMP_ZERO;
    tIgmpPkt            IgmpPkt;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IpGrp = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering  IgmpFormAndSendV1V2Report \n");

    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddr, IPVX_ADDR_FMLY_IPV4);

    u4PktSize = IGMP_IP_HDR_RTR_OPT_LEN + IGMP_PACKET_SIZE;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, IGMP_ZERO);

    if (pBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE | IGMP_ALL_MODULES, 
			IgmpGetModuleName(IGMP_BUFFER_MODULE),
                        "Buffer allocation failure for forming"
                        "V1/V2 report messages\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                      IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
			"for forming V1/V2 report messages");	
        return IGMP_FAILURE;
    }

    IgmpPkt.u1MaxRespTime = IGMP_ZERO;
    IgmpPkt.u2CheckSum = IGMP_ZERO;
    IgmpPkt.u4GroupAddr = OSIX_HTONL (u4IpGrp);

    if (u1Leave == IGMP_TRUE)
    {
        IgmpPkt.u1PktType = IGMP_PACKET_LEAVE;
    }
    else if (u1Version == IGMP_VERSION_1)
    {
        IgmpPkt.u1PktType = IGMP_PACKET_V1_REPORT;
    }
    else
    {
        IgmpPkt.u1PktType = IGMP_PACKET_V2_REPORT;
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpPkt,
                               IGMP_IP_HDR_RTR_OPT_LEN, IGMP_PACKET_SIZE);

    IgmpPkt.u2CheckSum = IgmpCalcCheckSum (pBuf, IGMP_PACKET_SIZE,
                                           IGMP_IP_HDR_RTR_OPT_LEN);

    IgmpPkt.u2CheckSum = OSIX_HTONS (IgmpPkt.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpPkt.u2CheckSum,
                               IGMP_IP_HDR_RTR_OPT_LEN + IGMP_TWO, IGMP_TWO);

    /* Send the packet upstream after filling IP header */
    if (IgmpSendReport (pBuf, u4IfIndex, u4GrpAddr,
                        (UINT2) u4PktSize, u1Version) == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Sending of V3 report to upstream failed\r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES, 
		  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Sending of V3 report to upstream failed\r\n");
        return IGMP_FAILURE;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting  IgmpFormAndSendV1V2Report \n");

    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgmpSendReport                                       */
/*                                                                           */
/* Description        : This function sends out the upstream report to the   */
/*                      specified index; If the given index is zero, send    */
/*                      the report to all upstream interfaces having the     */
/*                      given version                                        */
/*                                                                           */
/* Input(s)           : pBuffer  - Pointer to the packet buffer              */
/*                      u4IfIndex - index of upstream interface              */
/*                      u4GrpAddress - group for which, report is being sent */
/*                      u2Len - length of the packet                         */
/*                      u1Version - Version of report being sendt            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgmpSendReport (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4IfIndex,
                tIgmpIPvXAddr u4GrpAddress, UINT2 u2Len, UINT1 u1Version)
{
    tIgmpIface         *pIgmpIfNode = NULL;
    UINT4               u4Dest = IGMP_ZERO;
    UINT4               u4Src = IGMP_ZERO;
    UINT1               u1Leave = IGMP_ZERO;
    UINT4               u4IpGrp = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering  IgmpSendReport \n");

    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddress, IPVX_ADDR_FMLY_IPV4);

    if (CRU_BUF_Copy_FromBufChain (pBuffer, &u1Leave, IGMP_IP_HDR_RTR_OPT_LEN,
                                   IGMP_ONE) == CRU_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Getting the packet type from buffer failed\r\n");
        return IGMP_FAILURE;
    }

    if (u1Leave == IGMP_PACKET_LEAVE)
    {
        u4Dest = IGMP_ALL_ROUTERS_GROUP;
    }
    else if (u1Version != IGMP_VERSION_3)
    {
        u4Dest = u4IpGrp;
    }
    else
    {
        u4Dest = IGMP_ALL_V3_ROUTERS_GROUP;
    }

    if (u4IfIndex != IGMP_ZERO)
    {
        pIgmpIfNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);

        if (pIgmpIfNode == NULL)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE | IGMP_ALL_MODULES,
                       IgmpGetModuleName(IGMP_INIT_SHUT_MODULE), 
		       "IGMP is not enabled on the interface %d \r\n",u4IfIndex);
            IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE ,IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                      "IGMP is not enabled on the interface %d \r\n",u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
            return IGMP_FAILURE;
        }

        IGMP_IP_COPY_FROM_IPVX (&u4Src, pIgmpIfNode->u4IfAddr,
                                IPVX_ADDR_FMLY_IPV4);
        /* form the IP header and attach it to packet */
        IgmpFormIPHdr (pBuffer, u4Src, u4Dest, IGMP_ZERO, u2Len);

        if (IgmpPortSocketSend (pBuffer, u4Src, u4Dest, u2Len,
                                IGMP_TRUE) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                      IGMP_NAME, "Sending packet failed\r\n");
            CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
            return IGMP_FAILURE;
        }

        /* Update the statistics */
        if (u1Version != IGMP_VERSION_3)
        {
            IGMP_V1V2REP_TX (pIgmpIfNode);
        }
        else
        {
            IGMP_V3REP_TX (pIgmpIfNode);
        }

    }
    CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting  IgmpSendReport \n");

    return IGMP_SUCCESS;
}

/****************************************************************************/
/* Function           : IgmpPortSocketSend                                   */
/*                    :                                                     */
/* Descriptio         : This function will send IGMP control and data       */
/*                      packets on to IGMP capable interfaces               */
/*                                                                          */
/* Input(s)           : pBuffer - pointer to packet buffer                  */
/*                    : u4Src - Source IP address                           */
/*                    : u4Dest - Destination Ip address                     */
/*                    : u2Len - Length of PDU                               */
/*                    : u1CntlPkt - Whether the packet is a control packet  */
/*                                  or not                                  */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : IGMP_SUCCESS/IGMP_FAILURE                           */
/****************************************************************************/
INT4
IgmpPortSocketSend (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4Src,
                    UINT4 u4Dest, UINT2 u2Len, UINT1 u1CntlPkt)
{
    struct sockaddr_in  DestAddr;
    struct in_addr      IfAddr;
#ifdef LNXIP4_WANTED
    UINT1               u1OptVal = IGMP_TRUE;
#endif
    INT4                i4OptVal = IGMP_TRUE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering  IgmpPortSocketSend \n");

    MEMSET (&(DestAddr), IGMP_ZERO, sizeof (struct sockaddr_in));

    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4Dest);

    IfAddr.s_addr = OSIX_HTONL (u4Src);

    MEMSET (&gau1IgmpRawPkt, IGMP_ZERO, IGMP_MAX_MTU_SIZE);

    if (CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &gau1IgmpRawPkt,
                                   IGMP_ZERO, u2Len) == CRU_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Getting the packet type from buffer failed\r\n");	
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
		  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Getting the packet type from buffer failed\r\n");	
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return IGMP_FAILURE;
    }

#ifdef LNXIP4_WANTED
    if (u1CntlPkt == IGMP_TRUE)
    {
        /* for control packets, set TTL option */
        if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                               IP_MULTICAST_TTL, &u1OptVal,
                               sizeof (u1OptVal)) < IGMP_ZERO)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                      IGMP_NAME, "setsockopt failed for IP_MULTICAST_TTL\r\n");
            IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
		      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "setsockopt failed for IP_MULTICAST_TTL\r\n");
            return IGMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u1CntlPkt);
#endif
    /* Do not append IP header, since we are providing complete packet,
     * including IP header */
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           IP_HDRINCL, &i4OptVal,
                           sizeof (i4OptVal)) < IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Setsockopt failed for IP_HDRINCL\r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Setsockopt failed for IP_HDRINCL\r\n");
        return IGMP_FAILURE;
    }

    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           IP_MULTICAST_IF, (char *) &IfAddr,
                           sizeof (struct in_addr)) < IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Setsockopt Failed for IP_MULTICAST_IF\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
		  "Setsockopt Failed for IP_MULTICAST_IF\n");
        return IGMP_FAILURE;
    }

    if (IGMP_SEND_TO (gIgmpConfig.i4SocketId, &gau1IgmpRawPkt, u2Len, IGMP_ZERO,
                      (struct sockaddr *) &DestAddr,
                      sizeof (struct sockaddr_in)) < IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Sendto failed while attempting to" "send control packet \n");
        return IGMP_FAILURE;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering  IgmpPortSocketSend\n");

    return IGMP_SUCCESS;
}

#ifdef FS_NPAPI
/***************************************************************/
/*  Function Name   : IgmpNpSetIfaceRateLimit                  */
/*  Description     : This function sets the join rate    */
/*                    limit value for interface                */
/*  Input(s)        : i4IfIndex - Interface index              */
/*                    i4AddrType - IP address type             */
/*  Output(s)       : i4RateLimit - Rate limit value.          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGMP_SUCCESS/IGMP_FAILURE              */
/***************************************************************/
INT4
IgmpNpSetIfaceJoinRate (INT4 i4IfIndex, INT4 i4AddrType, 
			INT4 i4RateLimit)
{
#ifdef IGMP_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (IgmpFsNpIpv4SetIgmpIfaceJoinRate 
		(i4IfIndex, i4RateLimit) == FNP_FAILURE)
        {
	    IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, "IGMP",
			"Unable to set rate limit value %d for index %d \r\n",
			i4RateLimit, i4IfIndex);
            return IGMP_FAILURE;
        }
    }
#endif

#ifdef MLD_WANTED
    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (FsNpIpv6SetMldIfaceJoinRate 
		(i4IfIndex, i4RateLimit) == FNP_FAILURE)
        {
	    IGMP_DBG2 (MLD_DBG_FLAG, IGMP_ALL_MODULES, "MLD",
			"Unable to set rate limit value %d for index %d \r\n",
			i4RateLimit, i4IfIndex);
            return IGMP_FAILURE;
        }
    }
#endif
    return IGMP_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  IgmpHostAddGrpEntry
 Input       :  Multicast address
                interface
                version
 Output      :  None.
 Description :  This function adds a new entry in the gIgmpHostMcastGrpTable
                if it does not exists.
                It returns the pointer to the entry added if success
                else returns NULL.
 Returns     :  Pointer to the tIgmpHostMcastGrpEntry entry/NULL
 ******************************************************************************/
tIgmpHostMcastGrpEntry      *
IgmpHostAddGrpEntry (tIPvXAddr* Ip4Addr, UINT4 u4IfIndex, UINT1 u1Version)
{
    tIgmpHostMcastGrpEntry   *pIgmpHostMcastGrpEntry = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering IgmpHostAddGrpEntry.\n");

    /* Allocate memory for the new node */
    pIgmpHostMcastGrpEntry =
        (tIgmpHostMcastGrpEntry *)
        MemAllocMemBlk (IGMP_MCAST_GROUP_MEMPOOL_ID);
    if (pIgmpHostMcastGrpEntry == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_MEM, IGMP_ALL_MODULES, IGMP_NAME,
                  "Exiting  IgmpHostAddGrpEntry.: Fail to Allocate Memory"
                  " to add an entry in IGMPHostMcastGrp table for IP:%s\n",
                  IgmpPrintIPvxAddress(*Ip4Addr));
	SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                        IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
			"in adding an entry in IGMPHostMcastGrp table for IP:%s\n",
			IgmpPrintIPvxAddress(*Ip4Addr));
        return NULL;
    }
    else    
    {
        MEMSET(pIgmpHostMcastGrpEntry, 0, sizeof(tIgmpHostMcastGrpEntry));
        pIgmpHostMcastGrpEntry->u4IfIndex = u4IfIndex;
        pIgmpHostMcastGrpEntry->u1Version = u1Version;
        MEMCPY(&(pIgmpHostMcastGrpEntry->McastAddr), Ip4Addr, 
               sizeof(tIPvXAddr));
        if (RBTreeAdd
            (gIgmpHostMcastGrpTable,
             (tRBElem *) pIgmpHostMcastGrpEntry) != RB_SUCCESS)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                      "Exiting  IgmpHostAddGrpEntry.: Fail to Add the node."
                      "in IGMPHostMcastGrp table for IP:%s\n",
                      IgmpPrintIPvxAddress(*Ip4Addr));
            MemReleaseMemBlock (IGMP_MCAST_GROUP_MEMPOOL_ID,
                                (UINT1 *) pIgmpHostMcastGrpEntry);
            return NULL;
        }    
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting IgmpHostAddGrpEntry.\n");
    return pIgmpHostMcastGrpEntry;
}

/****************************************************************************
 Function    :  IgmpHostGetGrpEntry
 Input       :  Multicast address
                interface
 Output      :  None.
 Description :  This function returns the pointer to the IGMPHostMcastGrp
                entry if exists, else returns NULL.
 Returns     :  Pointer to the tIgmpHostMcastGrpEntry entry/NULL
****************************************************************************/
tIgmpHostMcastGrpEntry      *
IgmpHostGetGrpEntry (UINT4 u4IfIndex, tIPvXAddr* Ip4Addr)
{
    tIgmpHostMcastGrpEntry IgmpHostMcastGrpEntry;
    tIgmpHostMcastGrpEntry *pIgmpHostMcastGrpEntry = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering IgmpHostGetGrpEntry.\n");
    MEMSET (&IgmpHostMcastGrpEntry, 0, sizeof (tIgmpHostMcastGrpEntry));

    IgmpHostMcastGrpEntry.u4IfIndex = u4IfIndex;
    MEMCPY(&(IgmpHostMcastGrpEntry.McastAddr), Ip4Addr, sizeof(tIPvXAddr));

    pIgmpHostMcastGrpEntry =  RBTreeGet (gIgmpHostMcastGrpTable,
               (tRBElem *) & IgmpHostMcastGrpEntry);
    if (pIgmpHostMcastGrpEntry != NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Entry for Mcast Ip %s found in IGMPHostMcastGrp table.\n",
                       IgmpPrintIPvxAddress(*Ip4Addr));
        IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                       "Entry for Mcast Ip %s found in IGMPHostMcastGrp table.\n",
                       IgmpPrintIPvxAddress(*Ip4Addr));
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			"Exiting IgmpHostGetGrpEntry.\n");
        return pIgmpHostMcastGrpEntry;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			"Exiting IgmpHostGetGrpEntry.\n");
    return NULL;
}

/****************************************************************************
 Function    :  IgmpHostRemoveGrpEntry
 Input       :  pIgmpHostMcastGrpEntry
 Output      :  None.
 Description :  This function removes the entry in tIgmpHostMcastGrpEntry
                from RBTree if exists, else returns.
 Returns     :  VOID
****************************************************************************/

VOID
IgmpHostRemoveGrpEntry (tIgmpHostMcastGrpEntry* pIgmpHostMcastGrpEntry)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			"Entering IgmpHostRemoveGrpEntry.\n");

    if (RBTreeRemove (gIgmpHostMcastGrpTable,
               (tRBElem *)pIgmpHostMcastGrpEntry) == RB_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "IgmpHostRemoveGrpEntry: "
                  "RB Tree node deletion failed\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                        IGMP_NAME,IgmpSysErrString[SYS_LOG_RB_TREE_DEL_FAIL],
			"from IgmpHostGrpEntry");
    }
    else
    {
        MemReleaseMemBlock (IGMP_MCAST_GROUP_MEMPOOL_ID,
                            (UINT1 *) pIgmpHostMcastGrpEntry);
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting IgmpHostRemoveGrpEntry.\n");
    return;

}

/****************************************************************************
 Function    :  IgmpValidateSourceAddress
 Input       :  u4SourceIp & u1CheckZeroAddress
 Output      :  None.
 Description :  This function validates the Source IP.
 Returns     :  IGMP_SUCCESS or IGMP_FAILURE
****************************************************************************/

INT4
IgmpValidateV4SourceAddress (UINT4 u4SourceIp, UINT1 u1CheckZeroAddress) 
{
	tIgmpIPvXAddr SrcAddr;

	MEMSET(&SrcAddr, 0, sizeof(tIgmpIPvXAddr));

	if (!((CFA_IS_ADDR_CLASS_A (u4SourceIp) ?
				(IP_IS_ZERO_NETWORK (u4SourceIp) ? 0 : 1) : 0) ||
				(CFA_IS_ADDR_CLASS_B (u4SourceIp)) ||
				(CFA_IS_ADDR_CLASS_C (u4SourceIp))))
	{
		if((u4SourceIp == 0) && (u1CheckZeroAddress == IGMP_FALSE))
		{
			return IGMP_SUCCESS;
		}
		IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
				(UINT1 *) &u4SourceIp);
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Invalid IP as source %s\n",
                       IgmpPrintIPvxAddress(SrcAddr));
		return IGMP_FAILURE;
	}
	
	if((u4SourceIp >= IGMP_START_OF_MCAST_GRP_ADDR) &&
		(u4SourceIp <= IGMP_END_OF_MCAST_GRP_ADDR))
	{
		IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
				(UINT1 *) &u4SourceIp);
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "source %s cannot be in Multicast Range\n",
                       IgmpPrintIPvxAddress(SrcAddr));
		return IGMP_FAILURE;
	}
	
	return IGMP_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpUtilChkIfPimRPInSSMMappedGroup               */
/*                                                                         */
/*     Description   :    This function checks whether SSM mapping overlaps*/
/*                        RP group range configured.                       */
/*                                                                         */
/*     Input(s)      :    u4StartRPAddr, u4EndRPAddr                       */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    IGMP_TRUE, if overlapping                        */
/*                        IGMP_FALSE, otherwise                            */
/*                                                                         */
/***************************************************************************/
INT4
IgmpUtilChkIfPimRPInSSMMappedGroup (UINT4 u4StartRPAddr, UINT4 u4EndRPAddr)
{
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    UINT4                   u4Count = IGMP_ZERO;
    UINT4                   u4StartGrpAddr = IGMP_ZERO;
    UINT4                   u4EndGrpAddr = IGMP_ZERO;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "Entering the function IgmpUtilChkIfPimRPInSSMMappedGroup\n");

    RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
    if ((IGMP_SSM_MAP_GLOBAL_STATUS() == IGMP_DISABLE) || (u4Count == IGMP_ZERO))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                  "No Group entries found in the SSM mapped group database\r\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "Exiting the function IgmpUtilChkIfPimRPInSSMMappedGroup\n");
        return (IGMP_FALSE);
    }

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetFirst (gIgmpSSMMappedGrp);

    while (pSSMMappedGrp != NULL)
    {
        u4StartGrpAddr = u4EndGrpAddr = IGMP_ZERO;
        IGMP_IP_COPY_FROM_IPVX (&u4StartGrpAddr, pSSMMappedGrp->StartGrpAddr, IGMP_IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (&u4EndGrpAddr, pSSMMappedGrp->EndGrpAddr, IGMP_IPVX_ADDR_FMLY_IPV4);

        /* Either of the below conditions could indicate overlapping RP Group address:
         * 1. Check if Start RP Address is greater than Start SSM Map Address and
         *    End RP Address is less than End SSM Map Address
         * 2. Check if RP Address is superset of SSM map address
         * 3. Check if RP Address is subset of SSM map address
         * 4. Check if RP Address overlaps with SSM map address
         */

        if (((u4StartRPAddr >= u4StartGrpAddr) && (u4EndRPAddr <= u4EndGrpAddr))
            || ((u4StartRPAddr <= u4StartGrpAddr) && (u4EndRPAddr <= u4EndGrpAddr))
            || ((u4StartRPAddr >= u4StartGrpAddr) && (u4EndRPAddr >= u4EndGrpAddr))
            || ((u4StartRPAddr <= u4StartGrpAddr) && (u4EndRPAddr >= u4EndGrpAddr)))
        {
            IGMP_TRC_ARG4 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                    "RP group range %x - %x overlaps with SSM Map group range %x - %x\r\n",
                    u4StartRPAddr, u4EndRPAddr, u4StartGrpAddr, u4EndGrpAddr);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                    "Exiting the function IgmpUtilChkIfPimRPInSSMMappedGroup\n");
            return (IGMP_TRUE);
        }
        pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetNext 
                        (gIgmpSSMMappedGrp, (tRBElem *) pSSMMappedGrp, NULL);
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "Exiting the function IgmpUtilChkIfPimRPInSSMMappedGroup\n");
    return (IGMP_FALSE);
}

/************************************************************************/
 /* IGMP - SNMP */

/************************************************************************/
