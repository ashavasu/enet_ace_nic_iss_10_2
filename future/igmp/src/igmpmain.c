/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpmain.c,v 1.58 2017/10/05 12:27:55 siva Exp $
 *
 * Description:This file contains the routines required for     
 *             processing the  received IGMP packet             
 *
 *******************************************************************/

#ifndef __IGMPMAIN_C__
#define __IGMPMAIN_C__

#include "igmpinc.h"
#include "igmpglob.h"
#include "fssocket.h"
#ifdef SNMP_2_WANTED
#include "fsigmpwr.h"
#include "stdigmwr.h"
#include "fsmgmdwr.h"
#include "stdmgmwr.h"
#include "stdmldwr.h"
#include "fsmldwr.h"
#endif
#if defined LNXIP4_WANTED && defined NP_KERNEL_WANTED
#include <chrdev.h>
#endif

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE;
#endif
/*************************************************************************
 * Function Name    :  IgmpTaskMain
 *
 * Description      :  This function awaits on timer and packet arrival 
 *                     events and calls corresponding API to handle 
 *                     these events
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
IgmpTaskMain (INT1 *pi1Param)
{
    UINT4               u4EventReceived = IGMP_ZERO;
    UNUSED_PARAM (pi1Param);

    gIgmpConfig.u1IgmpStatus = IGMP_DISABLE;
    IGMP_PROXY_STATUS () = IGMP_DISABLE;
    IGMP_SSM_MAP_GLOBAL_STATUS () = IGMP_DISABLE;

    gIgmpConfig.u1MldStatus = MLD_DISABLE;
    gIgmpConfig.i1MldProxyStatus = MLD_DISABLE;

    gu4IgmpSysLogId = SYS_LOG_REGISTER ((UINT1 *) IGMP_TASK_NAME,
                                        SYSLOG_CRITICAL_LEVEL);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpTaskMain\n");
    if (OsixTskIdSelf (&gIgmpTaskId) != OSIX_SUCCESS)
    {
        IGMP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Create Q for Rxing IGMP control packets */
    if (OsixQueCrt ((UINT1 *) IGMP_Q, OSIX_MAX_Q_MSG_LEN,
                    IGMP_Q_DEPTH, &gIgmpQId) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Unable to create IP - IGMP Queue \n");
        IGMP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixCreateSem ((const UINT1 *) IGMP_SEM_NAME, IGMP_MUTEX_SEM_VALUE,
                       IGMP_ZERO, &gIgmpConfig.SemId) != OSIX_SUCCESS)
    {
        OsixQueDel (gIgmpQId);
        IGMP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (IGMP_OK != IgmpInitialize ())
    {
        OsixQueDel (gIgmpQId);
        OsixSemDel (gIgmpConfig.SemId);
        IgmpShutDown ();
        IGMP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /*Mld host initialization */
    IGMP_LOCK ();
    MLDProtoInit ();
    IGMP_UNLOCK ();

#if defined LNXIP4_WANTED && defined NP_KERNEL_WANTED
    system ("/bin/mknod " IGMP_DEVICE_FILE_NAME " c 101 0");
#endif

#ifdef SNMP_2_WANTED
    RegisterFSIGMP ();
    RegisterSTDIGM ();
    RegisterSTDMGM ();
    RegisterFSMGMD ();
    RegisterSTDMLD ();
    RegisterFSMLD ();
    IgpUtilRegisterFsIgpMib ();
#endif

    if (IgmpRedInitGlobalInfo () == OSIX_FAILURE)
    {
        OsixQueDel (gIgmpQId);
        OsixSemDel (gIgmpConfig.SemId);
        IgmpShutDown ();
        IgmpRedDeInitGlobalInfo ();
        IGMP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    IgmpRedDynDataDescInit ();

    IgmpRedDescrTblInit ();

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                   IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                   "IGMP initialization successful \n");

    IGMP_INIT_COMPLETE (OSIX_SUCCESS);

    while (IGMP_ONE)
    {

        /* Wait and receive events  - Packet arrival and Timer Event */
        if (OsixEvtRecv (gIgmpTaskId,
                         (IGMP_TIMER_EXPIRY_EVENT | IGMP_INPUT_Q_EVENT |
                          IGMP_PACKET_ARRIVAL_EVENT | IGMP_RM_PKT_EVENT
                          | IGP_TIMER_EXP_EVENT | IGP_MCAST_DATA_EVENT
                          | IGP_PBMP_CHANGE_EVENT | IGP_MFWD_ENABLE_EVENT
                          | IGP_MFWD_DISABLE_EVENT | MLD_REPORT_TMR), OSIX_WAIT,
                         &u4EventReceived) != OSIX_SUCCESS)
        {
            continue;
        }

        if (u4EventReceived & IGMP_TIMER_EXPIRY_EVENT)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received timer event \n");
            IgmpProcessTimerExpiryEvent ();
        }

        if (u4EventReceived & IGMP_INPUT_Q_EVENT)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received Packet reception event \n");
            IgmpProcessPkts ();
        }

        /* This event will been sent when a IGMP packet is received 
         * through socket interface */
#if !(defined (LNXIP4_WANTED) && defined (ISS_WANTED))
        if (u4EventReceived & IGMP_PACKET_ARRIVAL_EVENT)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received IGMP packet from IP \n");
            IGMP_LOCK ();
            IgmpHandlePktArrivalEvent ();
            if (OSIX_FAILURE ==
                SelAddFd (gIgmpConfig.i4SocketId, IgmpNotifyPktArrivalEvent))
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                               "Failed to Add Callback function to Igmp SocketId \r\n");
            }

            IGMP_UNLOCK ();
        }
#endif
        /* This event will been sent when a IGMP Proxy device timers 
         * get expired */
        if (u4EventReceived & IGP_TIMER_EXP_EVENT)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received IGMP Proxy Timer expiry event \n");
            IGMP_LOCK ();
            IgpMainHandleProxyEvents (IGP_TIMER_EXP_EVENT);
            IGMP_UNLOCK ();
        }

        /* This event will been sent when a multicast data packet is 
         * received */
        if (u4EventReceived & IGP_MCAST_DATA_EVENT)
        {
            MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                          "Received Multicast data packet event\r\n");
            IGMP_LOCK ();
            IgpMainHandleProxyEvents (IGP_MCAST_DATA_EVENT);
            IGMP_UNLOCK ();
        }

        /* This event will been sent by IGS /VLAN whne a port bitmap
         * change has occured */
        if (u4EventReceived & IGP_PBMP_CHANGE_EVENT)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received Port bitmap change event\r\n");
            IGMP_LOCK ();
            IgpMainHandleProxyEvents (IGP_PBMP_CHANGE_EVENT);
            IGMP_UNLOCK ();
        }

        /* This event will been sent by MFWD is enabled in the system */
        if (u4EventReceived & IGP_MFWD_ENABLE_EVENT)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received MFWD Enable event\r\n");
            IGMP_LOCK ();
            IgpMainHandleProxyEvents (IGP_MFWD_ENABLE_EVENT);
            IGMP_UNLOCK ();
        }

        /* This event will been sent by MFWD whne a port bitmap
         * change has occured */
        if (u4EventReceived & IGP_MFWD_DISABLE_EVENT)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received MFWD Enable event\r\n");
            IGMP_LOCK ();
            IgpMainHandleProxyEvents (IGP_MFWD_DISABLE_EVENT);
            IGMP_UNLOCK ();
        }
        if (u4EventReceived & IGMP_RM_PKT_EVENT)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received RM event\r\n");
            IGMP_LOCK ();
            IgmpRedHandleRmEvents ();
            IGMP_UNLOCK ();
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Processed RM event\r\n");
        }
        /* This event will been sent when a mld host report timer
         *          * get expired */
        if (u4EventReceived & MLD_REPORT_TMR)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                           "Received Port bitmap change event\r\n");
            IGMP_LOCK ();
            MLDProcessTmrEvt ();
            IGMP_UNLOCK ();
        }

        IGMP_LOCK ();
        IgmpRedSyncDynInfo ();
        IGMP_UNLOCK ();

    }
}

/*************************************************************************n
 * Function Name    :  IgmpProcessPkts
 *
 * Description      :  This function handles Packet reception event.
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Referred         :  None
 *                                               
 * Global Variables
 * Modified         :  None             
 *
 * Return(s)        :  None
 ****************************************************************************/
VOID
IgmpProcessPkts (VOID)
{
    tIgmpQMsg          *pQMsg = NULL;
    INT4                i4QMsgCount = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpProcessPkts\n");

    while (OsixQueRecv (gIgmpQId,
                        (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pQMsg == NULL)
            return;

        IGMP_LOCK ();

        switch (pQMsg->u4MsgType)
        {
            case IGMP_PKT_RECV_EVENT:
                /* This event will been sent by FSIP when IGMP packet is 
                 * received on a interface */
                IgmpInput (pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt);
                CRU_BUF_Release_MsgBufChain
                    (pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt, FALSE);
                break;

            case MLD_PKT_RECV_EVENT:
                MldInPktHandler (pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt);
                CRU_BUF_Release_MsgBufChain
                    (pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt, FALSE);
                break;

            case IGMP_IP_IF_CHG_EVENT:
                IgmpProcessIfChange (pQMsg->IgmpQMsgParam.IgmpIfStatusInfo);
                break;
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
            case MLD_IPADDR_CHG_EVENT:
                MldProcessIpAddrChange (pQMsg->IgmpQMsgParam.MldIpv6AddrInfo);
                break;
#endif
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:

                IgmpMbsmHandleEvent (pQMsg->IgmpQMsgParam.MbsmCardUpdate.
                                     pMbsmProtoMsg, pQMsg->u4MsgType);
                MEM_FREE (pQMsg->IgmpQMsgParam.MbsmCardUpdate.pMbsmProtoMsg);

                break;
#endif

            default:
                CRU_BUF_Release_MsgBufChain
                    (pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt, FALSE);
                break;
        }

        IGMP_UNLOCK ();
        IGMP_QMSG_FREE (pQMsg);

        i4QMsgCount++;
        if (i4QMsgCount >= IGMP_MAX_Q_MSGS)
        {
            if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
            {
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "Sending Input Queue Evt Failed\n");

            }
            else
            {
                IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                               IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                               "Relinquishing after Processing %d messages and "
                               "Posting Event to Input queue Succeeds\n",
                               i4QMsgCount);
            }
            break;
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpProcessPkts\n");
    return;
}

/*************************************************************************n
 * Function Name    :  IgmpHandleEnableEvent
 *
 * Description      :  This function handles IGMP global status enable event.
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Modified         :  gIgmpConfig.u1IgmpStatus             
 *
 * Return(s)        :  IGMP_SUCCESS/ IGMP_FAILURE
 ****************************************************************************/
INT4
IgmpHandleEnableEvent (VOID)
{
    tNetIpRegInfo       RegInfo;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    tIgmpIPvXAddr       ReporterAddrZero;
    UINT4               u4Count = IGMP_ZERO;
    UINT4               u4IPvXZero = IGMP_ZERO;
    INT4                i4MapStatus = IGMP_FALSE;
#if defined LNXIP4_WANTED && !defined NP_KERNEL_WANTED
    tNetIpMcRegInfo     NetIpMcRegInfo;
#endif
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpHandleEnableEvent\n");

    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));
    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

#if defined NPAPI_WANTED && defined IGMP_WANTED
    if (IgmpFsIgmpHwEnableIgmp () == FNP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       "Unable to create a filter for receiving IGMP packets \n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                              IGMP_NP_MODULE, IGMP_NAME,
                              IgmpSysErrString[NP_ENABLE_FILTER_FAIL],
                              "IGMP global status enable event\r\n");
        return IGMP_FAILURE;
    }
#endif /* L3_SWITCHING_WANTED */

    /* Create Socket for IGMP packets */
    if (IgmpCreateSocket () == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       "Unable to create a socket for packets \n");
        return IGMP_FAILURE;
    }

#ifdef LNXIP4_WANTED
    if (IgmpSetSendSocketOptions () == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       "Unable to set send socket options \n");
        /* Socket has to be closed */
        IgmpCloseSocket ();
        return IGMP_FAILURE;
    }

#ifndef ISS_WANTED
    /* Only for IGMP stack on Linux IP. Not for ISS */
    if (IgmpSetRecvSocketOptions () == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       "Unable to set receive socket options \n");
        /* Socket has to be closed */
        IgmpCloseSocket ();
        return IGMP_FAILURE;
    }

    /* Add to FD_SET */
    SelAddFd (gIgmpConfig.i4SocketId, IgmpNotifyPktArrivalEvent);
#else
#ifdef NP_KERNEL_WANTED
    /* Only for BCM Linux ip */
    /* Create Socket to receive IGMP packets */
    if (IgmpCreateRecvTask () == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       "Unable to create a socket to receive packets \n");
        IgmpCloseSocket ();
        return IGMP_FAILURE;
    }
#else
    /* All Linuxip other than BCM */
    /* Register for Multicast data packets from Netip */
    MEMSET (&NetIpMcRegInfo, 0, sizeof (tNetIpMcRegInfo));
    NetIpMcRegInfo.u4ProtocolId = IGMP_PROTID;
    NetIpMcRegInfo.pControlPktRcv = IgmpNetIpReceivePkt;
    NetIpMcRegInfo.pDataPktRcv = IgpPortHandleMcastDataPkt;
    NetIpv4RegisterMcPacket (&NetIpMcRegInfo);
#endif /* NP_KERNEL_WANTED */
#endif /* ISS_WANTED */
#else
    /* Add to FD_SET */
    if (OSIX_FAILURE ==
        SelAddFd (gIgmpConfig.i4SocketId, IgmpNotifyPktArrivalEvent))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "Failed to Add Callback function to Igmp SocketId \r\n");
    }

#endif /* LNXIP4_WANTED */

    gIgmpConfig.u1IgmpStatus = IGMP_ENABLE;

    /* Update the static interface configuration */
    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    while (pIfGetNextLink != NULL)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfaceNode->IfGetNextLink));

        IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_UP);

        IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfaceNode;

        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) & IgmpGrpEntry, NULL);

        while (pGrp != NULL)
        {
            if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
            {
                /* Group entry doesn't belong to
                 * specified interface index */
                break;
            }
            pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);

            if ((pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6) ||
                ((pGrp->u1StaticFlg == IGMP_TRUE) &&
                 (pGrp->u1EntryStatus != IGMP_ACTIVE)))
            {
                pGrp = pNextGrp;
                continue;
            }

            /* SrcList can be > 0 for IGMPv2 interface with SSM
             * mapping configured, in which case handle the sources
             * in UpdateMrp */
            if ((pIfaceNode->u1Version == IGMP_VERSION_2) &&
                (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO))
            {
                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    /* Call the PROXY routine to update the consolidate group
                     * membership data base for static group registration */
                    IgpGrpConsolidate (pGrp, IGMP_VERSION_2, IGMP_NEW_GROUP);
                }
                else
                {
                    /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
            }
            else
            {
                if (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO)
                {
                    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                    {
                        /* Call the PROXY routine to update the consolidate group
                         * membership data base for static group registration */
                        IgpGrpConsolidate (pGrp, IGMP_VERSION_3,
                                           IGMP_NEW_GROUP);

                        if (IgpUpIfFormAndSendV3Report (IGMP_ZERO)
                            == IGMP_FAILURE)
                        {
                            IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                                      IgmpGetModuleName (IGMP_CNTRL_MODULE),
                                      "IGMP PROXY: Sending "
                                      " upstream report failed \r\n");
                        }
                    }
                    else
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }

                    pGrp = pNextGrp;
                    continue;
                }

                pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);

                while (pSrc != NULL)
                {
                    pNextSrc =
                        (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList,
                                                      &pSrc->Link);

                    if ((pSrc->u1StaticFlg == IGMP_TRUE) &&
                        (pSrc->u1EntryStatus != IGMP_ACTIVE))
                    {
                        pSrc = pNextSrc;
                        continue;
                    }

                    if ((pSrc->u1FwdState == IGMP_TRUE) &&
                        (IGMP_PROXY_STATUS () == IGMP_DISABLE))
                    {
                        TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                        TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                    }
                    pSrc = pNextSrc;
                }

                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    /* Call the PROXY routine to update the consolidate group
                     * membership data base for static group registration */
                    IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_NEW_GROUP);

                    if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
                    {
                        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                                  "IGMP PROXY: Sending "
                                  " upstream report failed \r\n");
                    }
                    pGrp = pNextGrp;
                    continue;
                }

                if (pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)
                {
                    if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_JOIN);
                    }
                }
                else if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                {
                    if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_JOIN);
                    }
                    else
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }
                }
            }
            pGrp = pNextGrp;
        }
    }

    RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
    if (u4Count != IGMP_ZERO)
    {
        /* Map sources for static IGMP group entries */
        RBTreeCount (gIgmpGroup, &u4Count);
        if (u4Count != IGMP_ZERO)
        {
            pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
            while (pGrp != NULL)
            {
                pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                         (tRBElem *) pGrp,
                                                         NULL);
                /* check whether SSM mapping is configured for the group */
                pSSMMappedGrp =
                    IgmpUtilChkIfSSMMappedGroup (pGrp->u4Group, &i4MapStatus);
                if ((pSSMMappedGrp == NULL) && (i4MapStatus == IGMP_FALSE))
                {
                    pGrp = pNextGrp;
                    continue;
                }
                else
                {
                    /* SSM mapping is present for this group, 
                     * hence update MRP to create route */
                    for (pSrc = (tIgmpSource *)
                         TMO_SLL_First (&(pGrp->srcList));
                         pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
                    {
                        pNextSrcLink =
                            TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);

                        pSrc->u1SrcSSMMapped =
                            pSrc->u1SrcSSMMapped | IGMP_SRC_MAPPED;
                        if (pSrc->u1FwdState == IGMP_TRUE)
                        {
                            TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                            TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                         &pSrc->Link);
                        }
                    }

                    if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_INCLUDE);
                    }
                }
                for (u4Count = IGMP_ZERO; u4Count < MAX_IGMP_SSM_MAP_SRC;
                     u4Count++)
                {
                    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
                    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                                              (UINT1 *) &u4IPvXZero);
                    if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero,
                                                pSSMMappedGrp->
                                                SrcAddr[u4Count]) != IGMP_ZERO)
                    {
                        pSrc = IgmpSourceLookup (pGrp,
                                                 pSSMMappedGrp->
                                                 SrcAddr[u4Count],
                                                 ReporterAddrZero);
                        if ((pGrp->u1StaticFlg == IGMP_TRUE)
                            && ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                                || (pSrc != NULL)))
                        {
                            if ((IgmpAddSSMMappedSourcesForStaticGroup
                                 (pGrp, pSSMMappedGrp, pGrp->pIfNode,
                                  &(pSSMMappedGrp->SrcAddr[u4Count]),
                                  IGMP_CREATE_AND_GO,
                                  IGMP_FALSE)) == IGMP_NOT_OK)
                            {
                                IGMP_TRC (IGMP_TRC_FLAG, IGMP_GRP_MODULE,
                                          IgmpGetModuleName (IGMP_GRP_MODULE),
                                          "Failed to associate SSM mapping for static group\r\n");
                            }
                        }
                    }
                }
                pGrp = pNextGrp;
            }
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpHandleEnableEvent\n");

    return IGMP_SUCCESS;
}

/*************************************************************************n
 * Function Name    :  IgmpHandleDisableEvent
 *
 * Description      :  This function handles IGMP global status disable event.
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Modified         :  gIgmpConfig.u1IgmpStatus             
 *
 * Return(s)        :  IGMP_SUCCESS/ IGMP_FAILURE
 ****************************************************************************/
INT4
IgmpHandleDisableEvent (VOID)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
#if defined LNXIP4_WANTED && !defined NP_KERNEL_WANTED
    tNetIpMcastInfo     NetIpMcastInfo;
#endif
    tIgmpHostMcastGrpEntry *pMcastGrp = NULL;
    tIgmpHostMcastGrpEntry *pNextMcastGrp = NULL;
    tIgmpHostMcastGrpEntry IgmpMcastGrpEntry;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpHandleDisableEvent\n");
    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));
    MEMSET (&IgmpMcastGrpEntry, 0, sizeof (tIgmpHostMcastGrpEntry));

    if (IGMP_SSM_MAP_GLOBAL_STATUS () == IGMP_ENABLE)
    {
        /* remove all the SSM mapped entries */
        IgmpUtilClearAllSSMMappedGroups ();
    }

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    while (pIfGetNextLink != NULL)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfaceNode->IfGetNextLink));
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            /* clear querier related stuff */
            IgmpHdlTransitionToNonQuerier (pIfaceNode);
        }
        /* Clear all membership information */
        IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfaceNode;

        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) & IgmpGrpEntry, NULL);

        while (pGrp != NULL)
        {
            if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
            {
                /* Group entry doesn't belong to
                 * specified interface index */
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                           IgmpGetModuleName (IGMP_GRP_MODULE),
                           "Group entry doesn't belong to specified interface index %d \r\n",
                           pIfaceNode->u4IfIndex);

                break;
            }
            pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);

            if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                pGrp = pNextGrp;
                continue;
            }

            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);

            while (pSrc != NULL)
            {
                pNextSrc =
                    (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);
                /* If a source record has both static and dynamic flags enabled
                 * set the flag and retain the static record */
                if ((pSrc->u1StaticFlg == IGMP_TRUE)
                    && (pSrc->u1DynamicFlg == IGMP_TRUE))
                {
                    pSrc->u1DynamicFlg = IGMP_FALSE;
                    IGMPSTOPTIMER (pSrc->srcTimer);
                    pSrc->u1SrcModeFlag = IGMP_ADD_SOURCE;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pSrc->SrcDbNode);
                    IgmpRedSyncDynInfo ();
                    pSrc = pNextSrc;
                    continue;
                }
                else if (pSrc->u1StaticFlg == IGMP_TRUE)
                {
                    pSrc = pNextSrc;
                    continue;
                }
                pSrc->u1DelSyncFlag = IGMP_FALSE;
                IgmpDeleteSource (pGrp, pSrc);
                pSrc = pNextSrc;
            }

            /* If the group is learned dynamically and configured statically
             * update the flag and stop the timer and retain the static join record */
            if ((pGrp->u1StaticFlg == IGMP_TRUE)
                && (pGrp->u1DynamicFlg == IGMP_TRUE))
            {
                pGrp->u1DynamicFlg = IGMP_FALSE;
                IGMPSTOPTIMER (pGrp->Timer);
                IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
                pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
                IgmpRedSyncDynInfo ();
                pGrp = pNextGrp;
                continue;
            }
            else if (pGrp->u1StaticFlg == IGMP_TRUE)
            {
                pGrp = pNextGrp;
                continue;
            }
            pGrp->u1DelSyncFlag = IGMP_FALSE;
            IgmpDeleteGroup (pIfaceNode, pGrp, pGrp->u1StaticFlg);
            pGrp = pNextGrp;
        }
        /* Clear all IGMP Mcast group membership information */
        IgmpMcastGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
        pMcastGrp =
            (tIgmpHostMcastGrpEntry *) RBTreeGetNext (gIgmpHostMcastGrpTable,
                                                      (tRBElem *) &
                                                      IgmpMcastGrpEntry, NULL);
        while (pMcastGrp != NULL)
        {
            if (pMcastGrp->u4IfIndex != pIfaceNode->u4IfIndex)
            {
                /* Mcast Group entry doesn't belong to
                 * specified interface index */

                break;
            }
            pNextMcastGrp =
                (tIgmpHostMcastGrpEntry *)
                RBTreeGetNext (gIgmpHostMcastGrpTable, (tRBElem *) pMcastGrp,
                               NULL);
            if ((pMcastGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
            {
                IGMPSTOPTIMER (pMcastGrp->Timer);
            }
            if (RBTreeRemove (gIgmpHostMcastGrpTable, (tRBElem *) pMcastGrp) ==
                RB_FAILURE)
            {
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "IgmpHandleDisableEvent: "
                          "RB Tree node deletion failed \r\n");
                SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                                      IGMP_IO_MODULE, IGMP_NAME,
                                      IgmpSysErrString
                                      [SYS_LOG_RB_TREE_DEL_FAIL],
                                      "IGMP global status disable event\r\n");
            }
            else
            {
                MemReleaseMemBlock (IGMP_MCAST_GROUP_MEMPOOL_ID,
                                    (UINT1 *) pMcastGrp);
            }
            pMcastGrp = pNextMcastGrp;
        }
#if defined LNXIP4_WANTED && !defined NP_KERNEL_WANTED
        /* Disable Multicasting on VIFs added in NetIp. */
        MEMSET (&NetIpMcastInfo, 0, sizeof (NetIpMcastInfo));

        NetIpMcastInfo.u4IpPort = pIfaceNode->u4IfIndex;
        NetIpMcastInfo.u1McastProtocol = IGMP_PROTID;
        if (NetIpv4SetMcStatusOnPort (&NetIpMcastInfo, DISABLED)
            == NETIPV4_FAILURE)
        {
            return IGMP_FAILURE;
        }
#endif
    }

#ifdef LNXIP4_WANTED
#ifndef ISS_WANTED
    /* Only for IGMP stack on Linuxip. Not for ISS */
    /* Reset the socket option */
    IgmpResetSocketOption ();
    /* Socket has to be removed from FD_SET */
    SelRemoveFd (gIgmpConfig.i4SocketId);
#else
#ifdef NP_KERNEL_WANTED
    /* Only for BCM Linuxip */
    if (IgmpDeleteRecvTask () == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                       "Unable to delete recv task \n");
        return IGMP_FAILURE;
    }
#else
    /* Deregister for Multicast data packets from netip */
    NetIpv4DeRegisterMcPacket ((UINT4) IGMP_PROTID);
#endif /* NP_KERNEL_WANTED */
#endif /* ISS_WANTED */
#else
    /* Socket has to be removed from FD_SET */
    SelRemoveFd (gIgmpConfig.i4SocketId);
#endif /* LNXIP4_WANTED */

    /* Socket has to be closed */
    IgmpCloseSocket ();

#if defined NPAPI_WANTED && defined IGMP_WANTED
    if (IgmpFsIgmpHwDisableIgmp () == FNP_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       "Unable to remove filter from hardware\n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                              IGMP_NP_MODULE, IGMP_NAME,
                              IgmpSysErrString[NP_DISABLE_FILTER_FAIL],
                              "IGMP global status disable event\r\n");
        return IGMP_FAILURE;
    }
#endif /* L3_SWITCHING_WANTED */

    gIgmpConfig.u1IgmpStatus = IGMP_DISABLE;

    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        IgpMainStatusDisable ();
    }

    if (IGMP_PROXY_STATUS () == IGMP_DISABLE)
    {
        /* Inform PIM about the IGMP disable status */
        IgmpHandleProtocolDownStatus (IPVX_ADDR_FMLY_IPV4);
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting IgmpHandleDisableEvent\n");
    return IGMP_SUCCESS;
}

/*********************************************************************/
/* Function           :     IgmpGetIndexOfProtId                     */
/*                                                                   */
/* Description        :  This function returns the index in the      */
/*                       IgmpRegister given the MRP identifier       */
/*                                                                   */
/* Input(s)           :  MRP identifier                              */
/* Output(s)          :  None                                        */
/*                                                                   */
/* Returns            :  Index into the IgmpRegister.                */
/*                                                                   */
/*********************************************************************/
INT1
IgmpGetIndexOfProtId (UINT1 u1ProtId)
{
    UINT1               u1Index = IGMP_ZERO;
    for (u1Index = IGMP_ZERO; u1Index < IGMP_MAX_REGISTER; u1Index++)
    {
        if (gaIgmpRegister[u1Index].u1ProtId == u1ProtId)
        {
            return u1Index;
        }
    }
    return IGMP_NOT_OK;
}

/*****************************************************************************/
/* Function Name      : IgmpLock                                             */
/*                                                                           */
/* Description        : This function is used to take the IGMP  mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP,IgsMain                                        */
/*****************************************************************************/
INT4
IgmpLock ()
{
    if (OsixSemTake (gIgmpConfig.SemId) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Failed to Take IGMP Mutual Exclusion Sema4 \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgmpUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the IGMP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, IgsMain                                       */
/*****************************************************************************/
INT4
IgmpUnLock ()
{
    OsixSemGive (gIgmpConfig.SemId);
    return SNMP_SUCCESS;
}

/*************************************************************************n
 * Function Name    :  MldHandleEnableEvent
 *
 * Description      :  This function handles IGMP global status enable event.
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Modified         :  gIgmpConfig.u1IgmpStatus             
 *
 * Return(s)        :  IGMP_SUCCESS/ IGMP_FAILURE
 ****************************************************************************/
INT4
MldHandleEnableEvent (VOID)
{
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering MldHandleEnableEvent\n");

#if defined NPAPI_WANTED && defined MLD_WANTED
    if (MldFsMldHwEnableMld () == FNP_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                      IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                      "Unable to create a filter for receiving IGMP packets \n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG,
                              IGMP_NP_MODULE, IGMP_NAME,
                              IgmpSysErrString[NP_ENABLE_FILTER_FAIL],
                              "MLD global status enable event\r\n");
        return IGMP_FAILURE;
    }
#endif /* L3_SWITCHING_WANTED */

#ifdef LNXIP6_WANTED
    /* Create Socket to send and receive MLD packets */
    if (MldCreateSocket () == OSIX_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                      IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                      "Unable to create a socket for packets \n");
        return IGMP_FAILURE;
    }

    if (MLDSetSendSockOptions () == OSIX_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                      IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                      "Exiting MLDProtoInit:Mld Socket Options Settings "
                      "Failed\n ");
        return IGMP_FAILURE;
    }
#endif

    gIgmpConfig.u1MldStatus = IGMP_ENABLE;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting MldHandleEnableEvent\n");

    return IGMP_SUCCESS;
}

/************************************************************************/
/* Function Name      : ClearMldStat                                       */
/*                                                                      */
/* Description        : This routine is to clear the statistics         */
/*                      entries created.                                */
/*                                                                      */
/* Input(s)           : pIfaceNode - Interface entry                    */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

INT1
ClearMldStat (INT4 i4AddrType)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextAddrType = 0;
    UINT1               u1IsShowAll = TRUE;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    /* Clears MLD statistics info */

    if (nmhGetFirstIndexMgmdRouterInterfaceTable (&i4NextIfIndex, &i4AddrType)
        != SNMP_SUCCESS)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting ClearMldStat.\n");
        return CLI_FAILURE;
    }

    do
    {

        if (IgmpGetPortFromIfIndex (i4NextIfIndex,
                                    i4AddrType, &u4Port) == IGMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            return SNMP_FAILURE;
        }

        pIfaceNode = IgmpGetIfNode (u4Port, i4AddrType);
        CfaGetInterfaceNameFromIndex ((UINT4) i4NextIfIndex, au1InterfaceName);

        if ((pIfaceNode != NULL) && (i4AddrType == IPVX_ADDR_FMLY_IPV6))
        {
            pIfaceNode->u4InPkts = IGMP_ZERO;
            pIfaceNode->u4InJoins = IGMP_ZERO;
            pIfaceNode->u4InLeaves = IGMP_ZERO;
            pIfaceNode->u4InQueries = IGMP_ZERO;
            pIfaceNode->u4OutQueries = IGMP_ZERO;
            pIfaceNode->u4WrongVerQueries = IGMP_ZERO;
            pIfaceNode->u4ProcessedJoins = IGMP_ZERO;
            pIfaceNode->u4RxGenQueries = IGMP_ZERO;
            pIfaceNode->u4TxGenQueries = IGMP_ZERO;
            pIfaceNode->u4TxGrpQueries = IGMP_ZERO;
            pIfaceNode->u4TxGrpSrcQueries = IGMP_ZERO;
            pIfaceNode->u4RxGrpQueries = IGMP_ZERO;
            pIfaceNode->u4RxGrpSrcQueries = IGMP_ZERO;
            pIfaceNode->u4Txv2Leaves = IGMP_ZERO;
            pIfaceNode->u4CKSumError = IGMP_ZERO;
            pIfaceNode->u4PktLenError = IGMP_ZERO;;
            pIfaceNode->u4PktWithLocalIP = IGMP_ZERO;
            pIfaceNode->u4SubnetCheckFailure = IGMP_ZERO;;
            pIfaceNode->u4QryFromNonQuerier = IGMP_ZERO;;
            pIfaceNode->u4ReportVersionMisMatch = IGMP_ZERO;;
            pIfaceNode->u4QryVersionMisMatch = IGMP_ZERO;;
            pIfaceNode->u4UnknownMsgType = IGMP_ZERO;;
            pIfaceNode->u4InvalidV1Report = IGMP_ZERO;;
            pIfaceNode->u4InvalidV2Report = IGMP_ZERO;;
            pIfaceNode->u4InvalidV3Report = IGMP_ZERO;;
            pIfaceNode->u4RouterAlertCheckFailure = IGMP_ZERO;;
            pIfaceNode->u4BadScopeErr = IGMP_ZERO;
            pIfaceNode->u4MldRxv1Reports = IGMP_ZERO;
            pIfaceNode->u4MldRxv2Reports = IGMP_ZERO;
            pIfaceNode->u4MldTxv1Reports = IGMP_ZERO;
            pIfaceNode->u4MldTxv2Reports = IGMP_ZERO;
            pIfaceNode->u4MldInSSMPkts = IGMP_ZERO;
            pIfaceNode->u4MldInvalidSSMPkts = IGMP_ZERO;
            pIfaceNode->u4MldMalformedPkt = IGMP_ZERO;
            pIfaceNode->u4MldSocketErr = IGMP_ZERO;
            pIfaceNode->u4MldBadScopeErr = IGMP_ZERO;
            pIfaceNode->u4MldSubnetCheckFailure = IGMP_ZERO;
            pIfaceNode->u4MldInvalidIntf = IGMP_ZERO;
            pIfaceNode->u4InterfaceGroups = IGMP_ZERO;
#ifdef MULTICAST_SSM_ONLY
            pIfaceNode->u4DroppedASMIncomingPkts = IGMP_ZERO;
#endif
        }

        i4CurrIfIndex = i4NextIfIndex;
        i4NextAddrType = i4AddrType;
        if (nmhGetNextIndexMgmdRouterInterfaceTable (i4CurrIfIndex,
                                                     &i4NextIfIndex,
                                                     i4NextAddrType,
                                                     &i4AddrType)
            == SNMP_FAILURE)
        {
            u1IsShowAll = FALSE;
        }

    }
    while (u1IsShowAll);

    return CLI_SUCCESS;

}

/****************************************************************************
 *
 *     FUNCTION NAME    : ClearIgmpStat
 *
 *     DESCRIPTION      : Clears IGMP transmit and receive statistics
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
ClearIgmpStat (UINT1 u1AddrType)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT1               u1IsShowAll = TRUE;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    /* Clears IGMP statistics info */

    if (nmhGetFirstIndexIgmpInterfaceTable (&i4NextIfIndex) != SNMP_SUCCESS)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting ClearIgmpStats.\n");
        return CLI_FAILURE;
    }

    do
    {

        if (IgmpGetPortFromIfIndex (i4NextIfIndex,
                                    u1AddrType, &u4Port) == IGMP_FAILURE)
        {
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            return SNMP_FAILURE;
        }

        pIfaceNode = IgmpGetIfNode (u4Port, u1AddrType);
        CfaGetInterfaceNameFromIndex ((UINT4) i4NextIfIndex, au1InterfaceName);
        if (pIfaceNode != NULL)
        {
            pIfaceNode->u4InPkts = IGMP_ZERO;
            pIfaceNode->u4InJoins = IGMP_ZERO;
            pIfaceNode->u4InLeaves = IGMP_ZERO;
            pIfaceNode->u4InQueries = IGMP_ZERO;
            pIfaceNode->u4OutQueries = IGMP_ZERO;
            pIfaceNode->u4WrongVerQueries = IGMP_ZERO;
            pIfaceNode->u4ProcessedJoins = IGMP_ZERO;
            pIfaceNode->u4RxGenQueries = IGMP_ZERO;
            pIfaceNode->u4TxGenQueries = IGMP_ZERO;
            pIfaceNode->u4TxGrpQueries = IGMP_ZERO;
            pIfaceNode->u4TxGrpSrcQueries = IGMP_ZERO;
            pIfaceNode->u4RxGrpQueries = IGMP_ZERO;
            pIfaceNode->u4RxGrpSrcQueries = IGMP_ZERO;
            pIfaceNode->u4Txv2Leaves = IGMP_ZERO;
            pIfaceNode->u4CKSumError = IGMP_ZERO;
            pIfaceNode->u4PktLenError = IGMP_ZERO;;
            pIfaceNode->u4PktWithLocalIP = IGMP_ZERO;
            pIfaceNode->u4SubnetCheckFailure = IGMP_ZERO;;
            pIfaceNode->u4QryFromNonQuerier = IGMP_ZERO;;
            pIfaceNode->u4ReportVersionMisMatch = IGMP_ZERO;;
            pIfaceNode->u4QryVersionMisMatch = IGMP_ZERO;;
            pIfaceNode->u4UnknownMsgType = IGMP_ZERO;;
            pIfaceNode->u4InvalidV1Report = IGMP_ZERO;;
            pIfaceNode->u4InvalidV2Report = IGMP_ZERO;;
            pIfaceNode->u4RouterAlertCheckFailure = IGMP_ZERO;;
            pIfaceNode->u4BadScopeErr = IGMP_ZERO;
            pIfaceNode->u4IgmpRxv1v2Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpRxv3Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpTxv1v2Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpTxv3Reports = IGMP_ZERO;
            pIfaceNode->u4IgmpInSSMPkts = IGMP_ZERO;
            pIfaceNode->u4IgmpInvalidSSMPkts = IGMP_ZERO;
            pIfaceNode->u4IgmpMalformedPkt = IGMP_ZERO;
            pIfaceNode->u4IgmpSocketErr = IGMP_ZERO;
            pIfaceNode->u4InterfaceGroups = IGMP_ZERO;
        }

        i4CurrIfIndex = i4NextIfIndex;
        if (nmhGetNextIndexIgmpInterfaceTable (i4CurrIfIndex,
                                               &i4NextIfIndex) == SNMP_FAILURE)
        {
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll);

    return CLI_SUCCESS;
}

/*************************************************************************n
 * Function Name    :  MldHandleDisableEvent
 *
 * Description      :  This function handles MLD global status disable event.
 * Input(s)         :  None
 *
 * Output(s)        :  None
 *
 * Global Variables
 * Modified         :  gIgmpConfig.u1MldStatus             
 *
 * Return(s)        :  IGMP_SUCCESS/ IGMP_FAILURE
 ****************************************************************************/
INT4
MldHandleDisableEvent (VOID)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering MldHandleDisableEvent\n");
    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    while (pIfGetNextLink != NULL)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfaceNode->IfGetNextLink));
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            /* clear querier related stuff */
            IgmpHdlTransitionToNonQuerier (pIfaceNode);
        }
        /* Clear all membership information */
        IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfaceNode;

        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) & IgmpGrpEntry, NULL);

        while (pGrp != NULL)
        {
            if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
            {
                /* Group entry doesn't belong to
                 * specified interface index */
                break;
            }
            pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);

            if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pGrp = pNextGrp;
                continue;
            }

            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);

            while (pSrc != NULL)
            {
                pNextSrc =
                    (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);

                /* If a source record has both static and dynamic flags enabled
                 * set the flag and retain the static record */
                if ((pSrc->u1StaticFlg == IGMP_TRUE)
                    && (pSrc->u1DynamicFlg == IGMP_TRUE))
                {
                    pSrc->u1DynamicFlg = IGMP_FALSE;
                    IGMPSTOPTIMER (pSrc->srcTimer);
                    pSrc->u1SrcModeFlag = IGMP_ADD_SOURCE;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pSrc->SrcDbNode);
                    IgmpRedSyncDynInfo ();
                    pSrc = pNextSrc;
                    continue;
                }
                else if (pSrc->u1StaticFlg == IGMP_TRUE)
                {
                    pSrc = pNextSrc;
                    continue;
                }
                pSrc->u1DelSyncFlag = IGMP_FALSE;
                IgmpDeleteSource (pGrp, pSrc);
                pSrc = pNextSrc;
            }

            /* If the group is learned dynamically and configured statically
             * update the flag and stop the timer and retain the static join record */
            if ((pGrp->u1StaticFlg == IGMP_TRUE)
                && (pGrp->u1DynamicFlg == IGMP_TRUE))
            {
                pGrp->u1DynamicFlg = IGMP_FALSE;
                IGMPSTOPTIMER (pGrp->Timer);
                IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
                pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
                IgmpRedSyncDynInfo ();
                pGrp = pNextGrp;
                continue;
            }
            else if (pGrp->u1StaticFlg == IGMP_TRUE)
            {
                pGrp = pNextGrp;
                continue;
            }
            pGrp->u1DelSyncFlag = IGMP_FALSE;
            IgmpDeleteGroup (pIfaceNode, pGrp, pGrp->u1StaticFlg);
            pGrp = pNextGrp;
        }

    }

#ifdef LNXIP4_WANTED
    MldCloseSocket ();
#endif /* LNXIP4_WANTED */

#if defined NPAPI_WANTED && defined MLD_WANTED
    if (MldFsMldHwDisableMld () == FNP_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                      IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                      "Unable to remove filter from hardware\n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG,
                              IGMP_NP_MODULE, IGMP_NAME,
                              IgmpSysErrString[NP_DISABLE_FILTER_FAIL],
                              "MLD global status disable event\r\n");
        return IGMP_FAILURE;
    }
#endif /* L3_SWITCHING_WANTED */

    gIgmpConfig.u1MldStatus = IGMP_DISABLE;

    /* Inform PIM about the MLD disable status */
    IgmpHandleProtocolDownStatus (IPVX_ADDR_FMLY_IPV6);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting MldHandleDisableEvent\n");
    return IGMP_SUCCESS;
}
#endif
