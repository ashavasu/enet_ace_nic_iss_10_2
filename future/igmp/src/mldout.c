/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mldout.c,v 1.18 2015/09/25 07:09:05 siva Exp $
 *
 * Description:This file contains the routines required for
 *              sending the MLD packet
 *
 *******************************************************************/
#include "igmpinc.h"

/***************************************************************************/
/*                                                                         */
/*     Function Name :   MldOutSendQuery                                   */
/*                                                                         */
/*     Description   :   This function sends general/group specific        */
/*                       Query based on the group passed, on the given     */
/*                       interface                                         */
/*                                                                         */
/*     Input(s)      :   OutInterfaceId, GroupAddress.                     */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :   Returns OSIX_SUCCESS or OSIX_FAILURE              */
/*                                                                         */
/***************************************************************************/
INT1
MldOutSendQuery (tIgmpIface * pIfNode, tIPvXAddr * pGroup)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u2QueryPktSize = IGMP_ZERO;
    tMldPkt             MldPkt;    /* 24 */
    tMldV2Query         MldV2Query;    /* 28 */

    UINT4               u4CfaIfIndex = 0;
    UINT4               u4Mrc = 0;
    tNetIpv6IfInfo      Ip6Info;
    tIgmpIPvXAddr       u4IfAddr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldOutSendQuery.\n");
    if (pIfNode->u1Version == MLD_VERSION_2)
    {
        u2QueryPktSize = sizeof (tMldV2Query);
    }
    else
    {
        u2QueryPktSize = sizeof (tMldPkt);
    }
    pBuf = CRU_BUF_Allocate_MsgBufChain (u2QueryPktSize, 0);

    if (pBuf == NULL)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_BUFFER_MODULE | MLD_ALL_MODULES, "MLD",
                  "Failure, Buffer Empty.\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
         IgmpGetModuleName(IGMP_INIT_SHUT_MODULE),IgmpSysErrString[EVENT_SEND_FAIL],
		"because of buffer empty\r\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutSendQuery.\n");
        return (OSIX_FAILURE);
    }

    /*  Fill the Query */
    switch (pIfNode->u1Version)
    {

        case MLD_VERSION_1:
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE | MLD_ALL_MODULES, "MLD",
                      "Processing MLDv1 query\n");

            MEMSET (&MldPkt, 0, sizeof (tMldPkt));

            MldPkt.u1Type = MLD_QUERY;
            /* General query */
            if (IPVX_ADDR_COMPARE (gAllNodeV6McastAddr, (*pGroup)) == 0)
            {
                MldPkt.u2MaxRespDelay = (UINT2)
                    (pIfNode->u2MaxRespTime * MLDv2_HUNDRED); 
           
                /* For General Query MLD Hdr's Group field should be NULL */
                /*MEMSET(&MldPkt.McastAddr, 0, sizeof(MldPkt.McastAddr)); */
            }
            else                /* Group Specific Query */
            {
                MldPkt.u2MaxRespDelay =
                    (UINT2) (MLD_IF_LAST_MBR_QURY_IVAL (pIfNode) *
                           MLDv2_HUNDRED);
                /* For Specific Query MLD Hdr's Group field has the pGroup */
                MEMCPY (&MldPkt.McastAddr, pGroup->au1Addr,
                        sizeof (MldPkt.McastAddr));
            }
            MldPkt.u2MaxRespDelay = OSIX_HTONS (MldPkt.u2MaxRespDelay);
            MldPkt.u2ChkSum = 0;

            MldOutCopyMldPktHdr (pBuf, (UINT1 *) &MldPkt, sizeof (MldPkt),
                                 pGroup->au1Addr, pIfNode->u4IfAddr.au1Addr);
            break;

        case MLD_VERSION_2:

            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE | MLD_ALL_MODULES, "MLD",
                      "Processing MLDv2 query\n");

            MEMSET (&MldV2Query, 0, sizeof (MldV2Query));

            MldV2Query.u1Type = MLD_QUERY;
            u4Mrc = (UINT4)(pIfNode->u2MaxRespTime * MLDv2_HUNDRED);
            MldV2Query.u2MaxRespCode = (UINT2) u4Mrc;
            /* the Max. Response code */
            if (u4Mrc >= MLDV2_MAX_RESPONSE_CODE)
            {
                if (u4Mrc > MLDV2_MAX_ENCODE_VAL)
                {
                    MldV2Query.u2MaxRespCode = 0;
                    MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE | MLD_ALL_MODULES, "MLD",
                    "Max Response code value is too big. Hence assigned zero\n");
                }
                else
                {
                    MldV2Query.u2MaxRespCode = MldOutMrcEncode (u4Mrc);
                }
            }
            MldV2Query.u2MaxRespCode = OSIX_HTONS (MldV2Query.u2MaxRespCode);

            MEMSET (&MldV2Query.McastAddr, 0, sizeof (MldPkt.McastAddr));
            MldV2Query.u2CheckSum = 0;
            MldV2Query.u2NumSrcs = 0;

            MldV2Query.u1QQIC = (UINT1) pIfNode->u2QueryInterval;
            if (pIfNode->u2QueryInterval >= MLDV2_MAX_QQIC_VAL)
            {
                MLD_GET_QQIC_FROM_QIVAL (pIfNode->u2QueryInterval,
                                         MldV2Query.u1QQIC);
            }
            if (MLD_QUERIER_ROB_VARIABLE (pIfNode) > MLDV2_QRV_MAX_VALUE)
            {
                MldV2Query.u1ResvSupRV = 0;
            }
            else
            {
                MldV2Query.u1ResvSupRV = MLD_QUERIER_ROB_VARIABLE (pIfNode);
            }
            MldOutCopyMldPktHdr (pBuf, (UINT1 *) &MldV2Query,
                                 sizeof (MldV2Query),
                                 pGroup->au1Addr, pIfNode->u4IfAddr.au1Addr);
            break;

        default:
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE | MLD_ALL_MODULES, "MLD",
                      "Invalid Version\n");
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldOutSendQuery.\n");
            return OSIX_FAILURE;
    }
    DbgDumpIgmpPkt (pBuf, (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf),
                    MLD_QUERY, (UINT1) IGMP_PKT_FLOW_OUT, pIfNode->u4IfAddr,
                    *pGroup);
    IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   pBuf);
    IGMP_IP_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex, &u4CfaIfIndex);
    if (MLD_IP6_GET_IF_CONFIG_RECORD (u4CfaIfIndex, &Ip6Info) == OSIX_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE | MLD_ALL_MODULES, "MLD",
                  "If-Record not proper\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldOutSendQuery.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    IGMP_IPVX_ADDR_INIT_IPV6 (u4IfAddr, IPVX_ADDR_FMLY_IPV6,
                              (UINT1 *) Ip6Info.Ip6Addr.u1_addr);

    if (MldOutSendPktToIPv6 (pBuf, &u4IfAddr, pGroup,
                             u2QueryPktSize, pIfNode->u4IfIndex)
        == OSIX_FAILURE)
    {
        /* CRU buf would have been released by NETIP6 */
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldOutSendQuery.\n");
        return OSIX_FAILURE;
    }

    /* update statistics */
    if (IPVX_ADDR_COMPARE (gAllNodeV6McastAddr, (*pGroup)) == 0)
    {
        MLD_GEN_QUERY_TX (pIfNode);
    }
    else
    {
        MLD_GRP_QUERY_TX (pIfNode);
    }

    MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE, "MLD",
              "Processed MLD query\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldOutSendQuery.\n");
    return (OSIX_SUCCESS);
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldOutTxV1SchQuery                                 */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                      interface  on expiry of the scheduled timer        */
/*                                                                         */
/*     Input(s)      :  pIfNode   - Interface on which the query is to     */
/*                                    be scheduled                         */
/*                      pSchQuery   - the group address for which the query*/
/*                                   is to be scheduled                    */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  OSIX_SUCCESS/OSIX_FAILURE                          */
/*                                                                         */
/***************************************************************************/
INT4
MldOutTxV1SchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery)
{

    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMldPkt             MldV1Query;
    UINT2               u2QueryLength = 0;
    UINT4               u4CfaIfIndex = 0;
    tNetIpv6IfInfo      Ip6Info;
    tIgmpIPvXAddr       u4IfAddr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldOutTxV1SchQuery.\n");
    if (pIfNode->u1IfStatus != MLD_IFACE_UP)
    {

        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_INIT_SHUT_MODULE | MLD_ALL_MODULES, "MLD",
                  "Failure, The Intf is Down. Pkt Dropped.\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutTxV1SchQuery.\n");
        return OSIX_FAILURE;
    }

    u2QueryLength = sizeof (tMldPkt);
    pBuf = CRU_BUF_Allocate_MsgBufChain (u2QueryLength, 0);

    if (pBuf == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutTxV1SchQuery.\n");
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_BUFFER_MODULE | MLD_ALL_MODULES, "MLD",
                  "Failure, Buffer Empty.\n");
        return (OSIX_FAILURE);
    }

    MEMSET (&MldV1Query, 0, sizeof (tMldPkt));

    MldV1Query.u1Type = MLD_QUERY;
    MldV1Query.u2MaxRespDelay = (UINT2) 
           (MLD_IF_LAST_MBR_QURY_IVAL (pIfNode) * MLDv2_HUNDRED);
    MldV1Query.u2MaxRespDelay = OSIX_HTONS (MldV1Query.u2MaxRespDelay);
    MldV1Query.u2ChkSum = 0;
    MEMCPY (&MldV1Query.McastAddr, pSchQuery->u4Group.au1Addr,
            sizeof (MldV1Query.McastAddr));

    MldOutCopyMldPktHdr (pBuf, (UINT1 *) &MldV1Query, sizeof (MldV1Query),
                         pSchQuery->u4Group.au1Addr, pIfNode->u4IfAddr.au1Addr);

    /*  Get our IP address for this interface. */

    /* FSL: Dumping IGMP packet */
    DbgDumpIgmpPkt (pBuf, (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf),
                    MldV1Query.u1Type, (UINT1) IGMP_PKT_FLOW_OUT,
                    pIfNode->u4IfAddr, pSchQuery->u4Group);
    IGMP_DUMP (MLD_TRC_FLAG, IGMP_DUMP_TRC, "MLD - ",
                   pBuf);
    IGMP_IP_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex, &u4CfaIfIndex);
    if (MLD_IP6_GET_IF_CONFIG_RECORD (u4CfaIfIndex, &Ip6Info) == OSIX_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE | MLD_ALL_MODULES, "MLD",
                  "If-Record not proper\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutSendQuery.\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    IGMP_IPVX_ADDR_INIT_IPV6 (u4IfAddr, IPVX_ADDR_FMLY_IPV6,
                              (UINT1 *) Ip6Info.Ip6Addr.u1_addr);

    if (MldOutSendPktToIPv6 (pBuf, &u4IfAddr, &pSchQuery->u4Group,
                             u2QueryLength, pIfNode->u4IfIndex) == OSIX_FAILURE)
    {
        /* CRU buf would have been released by NETIP6 */
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutTxV1SchQuery.\n");
        return OSIX_FAILURE;
    }

    MLD_GRP_QUERY_TX (pIfNode);

    if (pSchQuery->u1RetrNum != 0)
    {
        pSchQuery->u1RetrNum--;
    }

    if (pSchQuery->u1RetrNum == 0)
    {
        /* add the node to the list of scheduled queries */
        TMO_SLL_Delete (&(pIfNode->pIfaceQry->schQueryList), &pSchQuery->Link);
        IgmpMemRelease (gIgmpMemPool.IgmpSchQueryId, (UINT1 *) pSchQuery);
    }
    else
    {
        IgmpReStartGrpSrcSchQryTmr (pIfNode, pSchQuery);
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldOutTxV1SchQuery.\n");
    return OSIX_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldOutTxV2SchQuery                                 */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer             */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*                                   be scheduled                          */
/*                      u4GroupAddr - the group addres for which the query */
/*                                    is to be scheduled                   */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
INT4
MldOutTxV2SchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery)
{
    UINT1              *pu1SrcsToPkt = NULL;
    tIgmpArrayStr      *pu4SrcsWithSBit = NULL;
    tIgmpArrayStr      *pu4SrcsWithoutSBit = NULL;
    tCRU_BUF_CHAIN_HEADER *pBufWithSBit = NULL;
    tCRU_BUF_CHAIN_HEADER *pBufWithoutSBit = NULL;
    tTMO_SLL_NODE      *pNextSchQrySrcNode = NULL;
    tMldV2Query         MldV2Query;
    tMldV2Query         MldV2QueryWithoutSBit;
    tSourceNode        *pSchQrySrcNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    UINT4               u4NoOfSources = 0;
    UINT4               u4NoOfSourcesWithSBit = 0;
    UINT4               u4NoOfSourcesWithoutSBit = 0;
    UINT4               u4index = 0;
    UINT4               u4RemTime = 0;
    UINT2               u2QueryLengthWithSBit = MLD_ZERO;
    UINT2               u2QueryLengthWithoutSBit = MLD_ZERO;
    UINT2               u2MaxRespCode = 0;
    UINT2               u2Offset = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4Mrc = 0;
    tNetIpv6IfInfo      Ip6Info;
    tIgmpIPvXAddr       u4IfAddr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldOutTxV2SchQuery.\n");

    IgmpFillMem (&MldV2Query, MLD_ZERO, sizeof (tMldV2Query));
    IgmpFillMem (&MldV2QueryWithoutSBit, MLD_ZERO, sizeof (tMldV2Query));

    if (pIfNode->u1IfStatus != MLD_IFACE_UP)
    {
        /* Without Iface Admin status = UP, Query cannot be sent out */
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_INIT_SHUT_MODULE | MLD_ALL_MODULES, "MLD",
                  "Failure, The Intf is Down. Pkt Dropped.\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutTxV2SchQuery.\n");
        return OSIX_FAILURE;
    }

    u4NoOfSources = TMO_SLL_Count (&(pSchQuery->SrcList));

    /*Setting the MLDv2 query header fields */

    MldV2Query.u1Type = MLD_QUERY;
    MldV2QueryWithoutSBit.u1Type = MLD_QUERY;

    MldV2Query.u2CheckSum = 0;
    MldV2QueryWithoutSBit.u2CheckSum = 0;

    u4Mrc = (MLD_IF_LAST_MBR_QURY_IVAL (pIfNode) * MLDv2_HUNDRED);
    MldV2Query.u2MaxRespCode = (UINT2) u4Mrc;
    MldV2QueryWithoutSBit.u2MaxRespCode = (UINT2) u4Mrc;

    if (u4Mrc >= MLDV2_MAX_RESPONSE_CODE)
    {
        if (u4Mrc > MLDV2_MAX_ENCODE_VAL)
        {
            u2MaxRespCode = 0;
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_QRY_MODULE | MLD_ALL_MODULES, "MLD",
            "Max Response code value is too big. Hence assigned zero\n");
        }
        else
        {
            u2MaxRespCode = MldOutMrcEncode (u4Mrc);
        }
        MldV2Query.u2MaxRespCode = u2MaxRespCode;
        MldV2QueryWithoutSBit.u2MaxRespCode = u2MaxRespCode;
    }
    MldV2Query.u2MaxRespCode = OSIX_HTONS (MldV2Query.u2MaxRespCode);
    MldV2QueryWithoutSBit.u2MaxRespCode =
        OSIX_HTONS (MldV2QueryWithoutSBit.u2MaxRespCode);

    MEMCPY (&MldV2Query.McastAddr, pSchQuery->u4Group.au1Addr,
            sizeof (MldV2Query.McastAddr));
    MEMCPY (&MldV2QueryWithoutSBit.McastAddr, pSchQuery->u4Group.au1Addr,
            sizeof (MldV2QueryWithoutSBit.McastAddr));

    MldV2Query.u1ResvSupRV = MLD_ZERO;
    MldV2QueryWithoutSBit.u1ResvSupRV = MLD_ZERO;

    if (pIfNode->u1Robustness < 8)
    {
        MldV2Query.u1ResvSupRV = pIfNode->u1Robustness;
        MldV2QueryWithoutSBit.u1ResvSupRV = pIfNode->u1Robustness;
    }

    MldV2Query.u1QQIC = (UINT1) pIfNode->u2QueryInterval;
    MldV2QueryWithoutSBit.u1QQIC = (UINT1) pIfNode->u2QueryInterval;
    if (pIfNode->u2QueryInterval >= MLDV2_MAX_QQIC_VAL)
    {
        MLD_GET_QQIC_FROM_QIVAL (pIfNode->u2QueryInterval, MldV2Query.u1QQIC);
        MLD_GET_QQIC_FROM_QIVAL (pIfNode->u2QueryInterval,
                                 MldV2QueryWithoutSBit.u1QQIC);
    }

    /*Getting the group structure object for the given group address */
    pGrp = IgmpGrpLookUp (pIfNode, pSchQuery->u4Group);

    /*For group specific queries */
    if (u4NoOfSources == MLD_ZERO)
    {
        /*Getting remaining time in Group Timer for scheduled group */
        if (pGrp != NULL)
        {
            if (TmrGetRemainingTime (gIgmpTimerListId,
                                     &(pGrp->Timer.TimerBlk.TimerNode),
                                     &u4RemTime) == TMR_SUCCESS)
            {
                /*Set S flag only if Group Timer > LMQT */
                if (u4RemTime > MLD_LMQT (pIfNode))
                {
                    MldV2Query.u1ResvSupRV |= MLD_S_FLAG;
                }
            }
        }
        /*Using pBufWithSBit for group specific queries */
        u2QueryLengthWithSBit = (UINT2) (sizeof (tMldV2Query));

        pBufWithSBit = CRU_BUF_Allocate_MsgBufChain (u2QueryLengthWithSBit,
                                                     MLD_ZERO);
        if (pBufWithSBit == NULL)
        {
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_BUFFER_MODULE, "MLD",
                      "Buffer Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_BUFFER_MODULE,
             IgmpGetModuleName(IGMP_BUFFER_MODULE),IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"for message buffer chain\n");
            return (MLD_NOT_OK);
        }
        MldOutCopyMldPktHdr (pBufWithSBit, (UINT1 *) &MldV2Query,
                             u2QueryLengthWithSBit, pSchQuery->u4Group.au1Addr,
                             pIfNode->u4IfAddr.au1Addr);
    }
    else
    {
        MldV2Query.u1ResvSupRV |= MLD_S_FLAG;

        /* Copy the sources addresses into the two buffers-one each for S set,clr */
        u4NoOfSourcesWithSBit = MLD_ZERO;
        u4NoOfSourcesWithoutSBit = MLD_ZERO;
        u2Offset = (UINT2) sizeof (tMldV2Query);

        if (IGMP_MEM_ALLOCATE
            (gIgmpMemPool.IgmpAddrStructArrayId, pu4SrcsWithSBit,
             tIgmpArrayStr) == NULL)
        {
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_OS_RES_MODULE, "MLD",
                      "Failure in allocating Memory block for Source with S bit!!\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_OS_RES_MODULE,
            IgmpGetModuleName(IGMP_OS_RES_MODULE),IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"for Source with S bit\n");
            return (MLD_NOT_OK);
        }
        if (IGMP_MEM_ALLOCATE
            (gIgmpMemPool.IgmpAddrStructArrayId, pu4SrcsWithoutSBit,
             tIgmpArrayStr) == NULL)
        {
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_OS_RES_MODULE, "MLD",
                      "Failure in allocating Memory block for Source without S bit!!\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_OS_RES_MODULE,
            IgmpGetModuleName(IGMP_OS_RES_MODULE),IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"for Source with S bit\n");
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);
            return (MLD_NOT_OK);
        }
        if (IGMP_MEM_ALLOCATE
            (gIgmpMemPool.IgmpU1SrcsToPktId, pu1SrcsToPkt, UINT1) == NULL)
        {
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_OS_RES_MODULE, "MLD",
                      "Memory Allocation failed for pu1SrcsToPkt.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_OS_RES_MODULE,
            IgmpGetModuleName(IGMP_OS_RES_MODULE),IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"for pu1SrcsToPkt\n");
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithoutSBit);
            return (MLD_NOT_OK);
        }

        MEMSET (pu4SrcsWithSBit, 0, sizeof (tIgmpArrayStr));
        MEMSET (pu4SrcsWithoutSBit, 0, sizeof (tIgmpArrayStr));
        MEMSET (pu1SrcsToPkt, 0,
                MAX_IGMP_SRCS_FOR_MRP_PER_GROUP * IPVX_IPV6_ADDR_LEN);

        for (pSchQrySrcNode =
             (tSourceNode *) TMO_SLL_First (&(pSchQuery->SrcList));
             pSchQrySrcNode != NULL;
             pSchQrySrcNode = (tSourceNode *) pNextSchQrySrcNode)
        {
            pNextSchQrySrcNode = TMO_SLL_Next (&(pSchQuery->SrcList),
                                               &pSchQrySrcNode->Link);

            /*Scanning the group's source list till the required source data */
            /*structure is found. Comparing source addresses. */
            pSrc = NULL;
            if (pGrp != NULL)
            {
                TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
                {
                    if (MLD_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                               pSchQrySrcNode->pSrc->
                                               u4SrcAddress) == MLD_ZERO)
                    {
                        break;
                    }
                }
            }

            u4RemTime = MLD_ZERO;
            if (pSrc != NULL)
            {
                (VOID) TmrGetRemainingTime (gIgmpTimerListId,
                                            &(pSrc->srcTimer.TimerBlk.
                                              TimerNode), &u4RemTime);
                /*If for a particular source, timer value > LMQT, add the */
                /*source to the S flag set array */
                if (u4RemTime > MLD_LMQT (pIfNode))
                {
                    MEMCPY (&
                            (pu4SrcsWithSBit->
                             au4SrcsSBit[u4NoOfSourcesWithSBit]),
                            &(pSrc->u4SrcAddress), sizeof (tIgmpIPvXAddr));
                    u4NoOfSourcesWithSBit++;
                }
                else
                {
                    /*If for a particular source, timer value <= LMQT, add the */
                    /*source to the S flag not set array */
                    MEMCPY (&
                            (pu4SrcsWithoutSBit->
                             au4SrcsSBit[u4NoOfSourcesWithoutSBit]),
                            &(pSrc->u4SrcAddress), sizeof (tIgmpIPvXAddr));
                    u4NoOfSourcesWithoutSBit++;
                }
            }
            /* The query has been sent the number of times it has to be sent.
             * No more queries for this source. Just let the node go.
             */
            pSchQrySrcNode->u1RetrNum--;
            if (pSchQrySrcNode->u1RetrNum == MLD_ZERO)
            {
                TMO_SLL_Delete (&(pSchQuery->SrcList), &pSchQrySrcNode->Link);
                IgmpMemRelease (gIgmpMemPool.IgmpSchQuerySourceId,
                                (UINT1 *) pSchQrySrcNode);
            }
        }
        /*Determine the packet lengths for both packets, alloc mem for buffers */
        u2QueryLengthWithSBit = (UINT2) (sizeof (tMldV2Query) +
                                         (u4NoOfSourcesWithSBit *
                                          (sizeof (tIp6Addr))));
        u2QueryLengthWithoutSBit =
            (UINT2) (sizeof (tMldV2Query) +
                     (u4NoOfSourcesWithoutSBit * (sizeof (tIp6Addr))));
        pBufWithSBit = CRU_BUF_Allocate_MsgBufChain (u2QueryLengthWithSBit, 0);
        if (pBufWithSBit == NULL)
        {
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_BUFFER_MODULE, "MLD",
                      "Buffer Allocation Failure.\n");
  	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_BUFFER_MODULE,
            IgmpGetModuleName(IGMP_BUFFER_MODULE),IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
			"for MLD message chain\n");
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithoutSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpU1SrcsToPktId, pu1SrcsToPkt);

            return (MLD_NOT_OK);
        }

        pBufWithoutSBit =
            CRU_BUF_Allocate_MsgBufChain (u2QueryLengthWithoutSBit, MLD_ZERO);

        if (pBufWithoutSBit == NULL)
        {
            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_BUFFER_MODULE, "MLD",
                      "Buffer Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_BUFFER_MODULE,
            IgmpGetModuleName(IGMP_BUFFER_MODULE),IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"for MLD message chain\n");
            CRU_BUF_Release_MsgBufChain (pBufWithSBit, MLD_ZERO);
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithoutSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpU1SrcsToPktId, pu1SrcsToPkt);

            return (MLD_NOT_OK);
        }

        MldV2Query.u2NumSrcs = OSIX_HTONS ((UINT2) u4NoOfSourcesWithSBit);
        MldV2QueryWithoutSBit.u2NumSrcs =
            OSIX_HTONS ((UINT2) u4NoOfSourcesWithoutSBit);

        /*Copy to pBufWithSBit the sources for which S bit is to be set */
        if (u4NoOfSourcesWithSBit != MLD_ZERO)
        {
            for (u4index = 0; u4index < u4NoOfSourcesWithSBit; u4index++)
            {
                MEMCPY (pu1SrcsToPkt + (u4index * IPVX_IPV6_ADDR_LEN),
                        pu4SrcsWithSBit->au4SrcsSBit[u4index].au1Addr,
                        IPVX_IPV6_ADDR_LEN);
            }
            /*Copying the source addresses into the buffer */
            CRU_BUF_Copy_OverBufChain (pBufWithSBit, pu1SrcsToPkt,
                                       u2Offset,
                                       (IPVX_IPV6_ADDR_LEN *
                                        u4NoOfSourcesWithSBit));
            /*Copying the header into the buffer */
            MldOutCopyMldPktHdr (pBufWithSBit, (UINT1 *) &MldV2Query, u2Offset,
                                 pSchQuery->u4Group.au1Addr,
                                 pIfNode->u4IfAddr.au1Addr);
        }
        /*Copy to pBufWithoutSBit the sources for which S bit is to be clear */
        if (u4NoOfSourcesWithoutSBit != MLD_ZERO)
        {
            for (u4index = 0; u4index < u4NoOfSourcesWithoutSBit; u4index++)
            {
                MEMCPY (pu1SrcsToPkt + (u4index * IPVX_IPV6_ADDR_LEN),
                        pu4SrcsWithoutSBit->au4SrcsSBit[u4index].au1Addr,
                        IPVX_IPV6_ADDR_LEN);
            }

            /*Copying the source addresses into the buffer */
            CRU_BUF_Copy_OverBufChain (pBufWithoutSBit, pu1SrcsToPkt,
                                       u2Offset, (IPVX_IPV6_ADDR_LEN *
                                                  u4NoOfSourcesWithoutSBit));
            /*Copying the header into the buffer */
            MldOutCopyMldPktHdr (pBufWithoutSBit,
                                 (UINT1 *) &MldV2QueryWithoutSBit,
                                 u2Offset, pSchQuery->u4Group.au1Addr,
                                 pIfNode->u4IfAddr.au1Addr);
        }
        IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                        (UINT1 *) pu4SrcsWithSBit);
        IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                        (UINT1 *) pu4SrcsWithoutSBit);
        IgmpMemRelease (gIgmpMemPool.IgmpU1SrcsToPktId, pu1SrcsToPkt);

    }

    /*For group specific query or S,G specific query with S bit set */
    if ((u4NoOfSourcesWithSBit != MLD_ZERO) || (u4NoOfSources == MLD_ZERO))
    {
        DbgDumpIgmpPkt (pBufWithSBit,
                        (UINT2) CRU_BUF_Get_ChainValidByteCount (pBufWithSBit),
                        MldV2Query.u1Type, (UINT1) IGMP_PKT_FLOW_OUT,
                        pIfNode->u4IfAddr, pSchQuery->u4Group);
    }
    /*For S,G specific query with S bit clear */
    if (u4NoOfSourcesWithoutSBit != MLD_ZERO)
    {
        DbgDumpIgmpPkt (pBufWithoutSBit,
                        (UINT2)
                        CRU_BUF_Get_ChainValidByteCount (pBufWithoutSBit),
                        MldV2QueryWithoutSBit.u1Type, (UINT1) IGMP_PKT_FLOW_OUT,
                        pIfNode->u4IfAddr, pSchQuery->u4Group);
    }
    IGMP_IP_GET_IFINDEX_FROM_PORT (pIfNode->u4IfIndex, &u4CfaIfIndex);
    if (MLD_IP6_GET_IF_CONFIG_RECORD (u4CfaIfIndex, &Ip6Info) == OSIX_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_INIT_SHUT_MODULE | MLD_ALL_MODULES, "MLD",
                  "If-Record not proper\n");
	SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_QRY_MODULE,
        IgmpGetModuleName(IGMP_QRY_MODULE),IgmpSysErrString[SYS_LOG_QRY_SEND_FAILURE],
		"for index %d\n",u4CfaIfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutSendQuery.\n");
        CRU_BUF_Release_MsgBufChain (pBufWithoutSBit, FALSE);
        CRU_BUF_Release_MsgBufChain (pBufWithSBit, FALSE);
        return OSIX_FAILURE;
    }
    IGMP_IPVX_ADDR_INIT_IPV6 (u4IfAddr, IPVX_ADDR_FMLY_IPV6,
                              (UINT1 *) Ip6Info.Ip6Addr.u1_addr);

    /*For group specific query or S,G specific query with S bit set */
    if ((u4NoOfSourcesWithSBit != MLD_ZERO) || (u4NoOfSources == MLD_ZERO))
    {
        if (MldOutSendPktToIPv6 (pBufWithSBit, &u4IfAddr,
                                 &pSchQuery->u4Group, u2QueryLengthWithSBit,
                                 pIfNode->u4IfIndex) == OSIX_FAILURE)
        {
            /* CRU buf would have been released by NETIP6 */
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutTxV2SchQuery.\n");
            CRU_BUF_Release_MsgBufChain (pBufWithoutSBit, FALSE);
            return OSIX_FAILURE;
        }
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBufWithSBit, FALSE);
    }

    if (u4NoOfSourcesWithoutSBit != MLD_ZERO)
    {
        if (MldOutSendPktToIPv6 (pBufWithoutSBit, &u4IfAddr,
                                 &pSchQuery->u4Group, u2QueryLengthWithoutSBit,
                                 pIfNode->u4IfIndex) == OSIX_FAILURE)
        {
            /* CRU buf would have been released by NETIP6 */
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldOutTxV2SchQuery.\n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBufWithoutSBit, FALSE);
    }

    /* Update the statistics */
    if ((MldV2Query.u2NumSrcs) || (MldV2QueryWithoutSBit.u2NumSrcs))
    {
        if ((MldV2Query.u2NumSrcs) && (MldV2QueryWithoutSBit.u2NumSrcs))
        {
            MLD_GRP_SRC_QUERY_TX (pIfNode);
        }
        MLD_GRP_SRC_QUERY_TX (pIfNode);
    }
    else
    {
        MLD_GRP_QUERY_TX (pIfNode);
    }

    if (pSchQuery->u1RetrNum != MLD_ZERO)
    {
        if (u4NoOfSources != MLD_ZERO)
        {
            MldV2QueryWithoutSBit.u2CheckSum = 0;
            MldV2QueryWithoutSBit.u2NumSrcs = 0;
            u2QueryLengthWithoutSBit = sizeof (tMldV2Query);

            pBufWithoutSBit =
                CRU_BUF_Allocate_MsgBufChain (u2QueryLengthWithoutSBit,
                                              MLD_ZERO);
            if (pBufWithoutSBit != NULL)
            {
                MldOutCopyMldPktHdr (pBufWithoutSBit,
                                     (UINT1 *) &MldV2QueryWithoutSBit,
                                     u2QueryLengthWithoutSBit,
                                     pSchQuery->u4Group.au1Addr,
                                     pIfNode->u4IfAddr.au1Addr);

                if (MldOutSendPktToIPv6 (pBufWithoutSBit, &u4IfAddr,
                                         &pSchQuery->u4Group,
                                         u2QueryLengthWithoutSBit,
                                         pIfNode->u4IfIndex) == OSIX_FAILURE)
                {
                    /* CRU buf would have been released by NETIP6 */
                    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			IgmpGetModuleName(IGMP_EXIT_MODULE), "Exiting MldOutTxV2SchQuery.\n");
                    return OSIX_FAILURE;
                }
                MLD_GRP_SRC_QUERY_TX (pIfNode);
            }
            else
            {
                MLD_DBG_INFO (MLD_DBG_FLAG, MLD_BUFFER_MODULE, "MLD",
                          "Buffer Allocation Failure.\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, MLD_DBG_FLAG, IGMP_BUFFER_MODULE,
                IgmpGetModuleName(IGMP_BUFFER_MODULE),IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"for MLD message chain\n");		
            }
        }
        pSchQuery->u1RetrNum--;
    }

    if ((pSchQuery->u1RetrNum == MLD_ZERO) &&
        (TMO_SLL_Count (&(pSchQuery->SrcList)) == MLD_ZERO))
    {
        /* add the node to the list of scheduled queries */
        TMO_SLL_Delete (&(pIfNode->pIfaceQry->schQueryList), &pSchQuery->Link);
        IgmpMemRelease (gIgmpMemPool.IgmpSchQueryId, (UINT1 *) pSchQuery);
    }
    else
    {
        IgmpReStartGrpSrcSchQryTmr (pIfNode, pSchQuery);

    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting MldOutTxV2SchQuery.\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  MldOutSendPktToIPv6
 Input       :  pBuf
                SrcAddr
                DstAddr
                u2Len
                u4IfIndex
 Output      :  NONE
 Description :  This function copies the Header of the MLD Packet in the CRU
                buffer.
 Returns     :  OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
INT4
MldOutSendPktToIPv6 (tCRU_BUF_CHAIN_HEADER * pBuf, tIPvXAddr * pSrcAddr,
                     tIPvXAddr * pDstAddr, UINT2 u2Len, UINT4 u4IfIndex)
{

    tHlToIp6Params      MldParams;
    INT4                i4RetVal = OSIX_SUCCESS;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldOutSendPktToIPv6.\n");
    MldParams.u1Cmd = IP6_LAYER4_DATA;
    MldParams.u1Hlim = 1;
    MldParams.u1Proto = NH_ICMP6;
    MldPortGetIp6IfIndexFromPort (u4IfIndex, &(MldParams.u4Index));
    MldParams.u4Len = u2Len;
    MEMCPY ((MldParams.Ip6SrcAddr.u1_addr), pSrcAddr->au1Addr, 16);
    MEMCPY ((MldParams.Ip6DstAddr.u1_addr), pDstAddr->au1Addr, 16);

    if (MldPortEnqueuePktToIpv6 (pBuf, &MldParams) == OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutSendPktToIPv6.\n");
    return i4RetVal;
}

/****************************************************************************
 Function    :  MldOutCopyMldPktHdr
 Input       :  pPktBuf
                pLinBuf
                u4Len
 Output      :  pMldHdr
 Description :  This function copies the Header of the MLD Packet in the CRU
                buffer.
 Returns     :  OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
INT4
MldOutCopyMldPktHdr (tCRU_BUF_CHAIN_HEADER * pPktBuf, UINT1 *pLinBuf,
                     UINT4 u4Len, UINT1 *pu1DstAddr, UINT1 *pu1SrcAddr)
{
    tMldPkt            *pMldPkt = (tMldPkt *) (VOID *) pLinBuf;
    INT4                i4RetVal = OSIX_SUCCESS;

    UNUSED_PARAM (pu1DstAddr);
    UNUSED_PARAM (pu1SrcAddr);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldOutCopyMldPktHdr.\n");
    pMldPkt->u2ChkSum = 0;

    if (CRU_BUF_Copy_OverBufChain (pPktBuf, pLinBuf, 0, u4Len) == CRU_FAILURE)
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_INIT_SHUT_MODULE | MLD_ALL_MODULES, "MLD",
                  "Failure, Buffer Empty.\n");
        i4RetVal = OSIX_FAILURE;
    }

    /* if (CRU_BUF_Copy_OverBufChain (pPktBuf, (UINT1 *) &u2CheckSum,
       MLD_CHKSUM_FIELD_OFFSET,
       MLD_CHKSUM_FIELD_SIZE) == CRU_FAILURE)
       {
       i4RetVal = OSIX_FAILURE;
       } */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutCopyMldPktHdr.\n");
    return i4RetVal;
}

/****************************************************************************
* Function Name    : MldOutCalcIpv6CheckSum                               
*                                                                          
* Description      : This function calculates checksum for the
*                    given buffer
*                                 
* Input (s)        : pu1Buf  - pointer to message buffer   
*                    u4Size  - length of the message
*                    pSrcAddr - Src Addr of the Packet
*                    pDestAddr - Dest Addr of the packet
*                    u1Proto - Protocol
*                           
* Output (s)       : None.
*                                                                          
* Returns          : Checksum value
***************************************************************************/
UINT2
MldOutCalcIpv6CheckSum (UINT1 *pu1Buf, UINT4 u4Size, UINT1 *pSrcAddr,
                        UINT1 *pDestAddr, UINT1 u1Proto)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT2              *pSrcPtr = NULL;
    UINT2              *pDstPtr = NULL;
    INT4                i4Cnt = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldOutCalcIpv6CheckSum.\n");
    pSrcPtr = (UINT2 *) (VOID *) pSrcAddr;
    pDstPtr = (UINT2 *) (VOID *) pDestAddr;

    for (i4Cnt = 0; i4Cnt < 8; i4Cnt++)
    {
        u4Sum += OSIX_HTONS (pSrcPtr[i4Cnt]);
        u4Sum += OSIX_HTONS (pDstPtr[i4Cnt]);
    }

    u4Sum += u4Size >> 16;
    u4Sum += u4Size & 0xffff;
    u4Sum += u1Proto;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
        u4Sum += OSIX_NTOHS (u2Tmp);
        pu1Buf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }
    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pu1Buf;
        u4Sum += OSIX_NTOHS (u2Tmp);
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xFFFF);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldOutCalcIpv6CheckSum.\n");
    return (((UINT2) OSIX_NTOHS (u2Tmp)));

}
/****************************************************************************
 * * Function Name    : MldOutMrcEncode
 * *
 * * Description      : This function converts MRC value to exponential and
 * *                    mantissa
 * *
 * * Input (s)        : code  - max response time in ms
 * *
 * * Output (s)       : None.
 * *
 * * Returns          : calculated mrc value
 * ***************************************************************************/

UINT2 MldOutMrcEncode (UINT4 u4Code)
{
   UINT2 u2Mrc = 0;
   UINT4 u4Mantissa = 0;
   UINT4 u4Exponent = 0;

   u4Mantissa = u4Code >> MLDv2_EXP_BITS;
   for (u4Exponent = 0; u4Exponent <= MLDv2_MAX_EXP_VAL; u4Exponent++)
   {
        if (u4Mantissa <= MLDv2_MAX_MANT_VAL)
        {
            break;
        }
        u4Mantissa >>= 1;
   }
   u2Mrc = (UINT2) (MLDV2_MAX_EXP_MANT_VAL | (u4Exponent << 12) | 
                   (u4Mantissa & 0x0FFF));
   return u2Mrc;
}
