#ifndef __MLDCLI_C__
#define __MLDCLI_C__
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldcli.c,v 1.33 2016/05/04 11:18:13 siva Exp $
 *
 * Description: this file handles routines for CLI SET/GET destined 
 *              for MLD in IGMP module                         
 *
 *******************************************************************/
#include "igmpinc.h"
#include "mldcli.h"
#include "mldcliprot.h"
#include "stdmgmwr.h"
#include "fsmgmdwr.h"
#include "stdmgmcli.h"
#include "fsmgmdcli.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule =  IGMP_MGMT_MODULE;
#endif

/***************************************************************************/
/*                                                                         */
/*     Function Name :  cli_process_mld_cmd                                */
/*                                                                         */
/*     Description   :  Calls appropriate command routine                  */
/*                                                                         */
/*     Input(s)      : .pInMsg    -Contains the command to be exicuted     */
/*                                                                         */
/*     Output(s)     : .ppRespMsg  -Output response message to be displayed*/
/*                                                                         */
/*     Returns       :    None                                             */
/*                                                                         */
/***************************************************************************/
INT1
cli_process_mld_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS];
    INT1                i1argno = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4Status = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst = 0;

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering cli_process_mld_cmd.\n");
    if (i4Inst != 0)
    {
        i4IfaceIndex = (UINT4) i4Inst;
    }
    else
    {
        i4IfaceIndex = CLI_GET_IFINDEX ();
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store 5 arguements at the max. This is because mld commands do not
     * take more than 5 inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == MLD_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();
    CLI_SET_ERR (0);

    switch (u4Command)
    {
        case MLD_CLI_STATUS:

            /* args[0] -> contains the MLD Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) ==
                 CLI_ENABLE) ? MLD_ENABLE : MLD_DISABLE;
            i4RetStatus = MldCliSetStatus (CliHandle, i4Status);
            break;

        case MLD_CLI_IFACE_STATUS:

            /* args[0] -> contains the MLD Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) ==
                 CLI_ENABLE) ? MLD_ENABLE : MLD_DISABLE;
            i4RetStatus =
                MldCliSetIfaceStatus (CliHandle, i4IfaceIndex, i4Status);
            break;

        case MLD_CLI_FAST_LEAVE:

            /* args[0] -> contains the MLD fast Enable/Disable Status */
            i4Status =
                (CLI_PTR_TO_I4 (args[0]) == CLI_ENABLE) ?
                MLD_FAST_LEAVE_ENABLE : MLD_FAST_LEAVE_DISABLE;
            i4RetStatus =
                MldCliSetFastLeave (CliHandle, i4IfaceIndex, i4Status);
            break;

        case MLD_CLI_VERSION:

            /* args[0] -> contains the MLD Version */
            i4RetStatus =
                MldCliSetMldVersion (CliHandle, i4IfaceIndex,
                                     CLI_PTR_TO_U4 (args[0]));
            break;
        case MLD_CLI_NO_VERSION:

            /* args[0] -> contains the MLD Version */
            i4RetStatus =
                MldCliSetNoMldVersion (CliHandle, i4IfaceIndex,
                                     CLI_PTR_TO_U4 (args[0]));
            break;

        case MLD_CLI_QRY_INTRVL:

            /* args[0] -> contains the MLD fast Enable/Disable Status */
            i4Status =
                (args[0] == NULL) ?
                MLD_NO_QUERY_INTERVAL : (*(INT4 *) (args[0]));

            i4RetStatus =
                MldCliSetQryInterval (CliHandle, i4IfaceIndex, i4Status);
            break;

        case MLD_CLI_QRY_MAXRESPTIME:

            /* args[0] -> contains the MLD Max response time */
            i4Status =
                (args[0] == NULL) ?
                MLD_NO_MAX_RESP_TIME : (*(INT4 *) (args[0]));

            i4RetStatus =
                MldCliSetQryMaxRespTime (CliHandle, i4IfaceIndex, i4Status);
            break;

        case MLD_CLI_ROBUSTNESS:

            /* args[0] -> contains the MLD Robustness value */
            i4Status =
                (args[0] == NULL) ?
                MLD_NO_ROB_VARIABLE : (*(INT4 *) (args[0]));

            i4RetStatus =
                MldCliSetRobustnessValue (CliHandle, i4IfaceIndex, i4Status);
            break;

        case MLD_CLI_LAST_MBRQRYINTRL:

            /* args[0] -> contains the MLD Last memb QIVAL */
            i4Status =
                (args[0] == NULL) ?
                MLD_NO_LAST_MEMBER_QIVAL : (*(INT4 *) (args[0]));

            i4RetStatus =
                MldCliSetLastMbrQryInterval (CliHandle, i4IfaceIndex, i4Status);
            break;

        case MLD_CLI_DEL_INTERFACE:

            i4RetStatus = MldCliDeleteInterface (CliHandle, i4IfaceIndex);
            break;

        case MLD_CLI_RATE_LIMIT:

            /* args[0] -> contains the rate limit value*/
            i4RetStatus =
                MldCliSetRateLimit (CliHandle, *(INT4 *)(VOID *)(args[0]));
            break;

        case MLD_CLI_NO_RATE_LIMIT:

            i4RetStatus =
                MldCliSetRateLimit (CliHandle,IGMP_DEFAULT_RATE_LIMIT);
            break;


        case MLD_CLI_IF_RATE_LIMIT:

            /* args[0] -> contains the rate limit value for interface */
            i4RetStatus =
                MldCliSetInterfaceRateLimit (CliHandle, i4IfaceIndex, 
						*(INT4 *)(VOID *)(args[0]));
            break;

        case MLD_CLI_IF_NO_RATE_LIMIT:

            i4RetStatus =
                MldCliSetInterfaceRateLimit (CliHandle, i4IfaceIndex, 
						IGMP_DEFAULT_INT_RATE_LIMIT);
            break;


        case MLD_CLI_SHOW_GBL_CONFIG:
            i4RetStatus = MldCliShowGlobalInfo (CliHandle);
            break;

        case MLD_CLI_SHOW_INTF_CONFIG:

            if (args[0] != NULL)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus = MldCliShowInterfaceInfo (CliHandle, i4IfaceIndex);
            }
            else
            {
                i4RetStatus =
                    MldCliShowInterfaceInfo (CliHandle, MLD_INVALID_INDEX);
            }
            break;

        case MLD_CLI_SHOW_GROUPS:
            i4RetStatus = MldCliShowGroupInfo (CliHandle);
            break;

        case MLD_CLI_SHOW_SOURCES:
            i4RetStatus = MldCliShowSourceInfo (CliHandle);
            break;

        case MLD_CLI_SHOW_STATS:

            if (args[0] != NULL)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus = MldCliShowStatistics (CliHandle, i4IfaceIndex);
            }
            else
            {
                i4RetStatus =
                    MldCliShowStatistics (CliHandle, MLD_INVALID_INDEX);
            }

            break;

        case MLD_CLI_CLEAR_STATS:
            if (args[0] != NULL)
            {
                i4IfaceIndex = (CLI_PTR_TO_I4 (args[0]));
                i4RetStatus = MldCliClearStats (CliHandle, i4IfaceIndex);
            }
            else
            {
                i4RetStatus =
                    MldCliClearStats (CliHandle, MLD_INVALID_INDEX);
            }

            break;

        case MLD_CLI_TRACE:
            /* args[0] contains Trace flag for the Modules */
            i4RetStatus = MldCliSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

	case MLD_CLI_DEBUG:
	     /* args[0] contains Trace flag for the Modules */
           i4RetStatus = MldCliSetDebug (CliHandle, CLI_PTR_TO_I4 (args[0]));
	   break;
        default:
            CliPrintf (CliHandle, "%% Unknown command \r\n");
            return CLI_ERROR;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_MLD_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", MldCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    IGMP_UNLOCK ();

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting cli_process_mld_cmd.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetStatus                                    */
/*                                                                         */
/*     Description   :  Enables/disables MLD globally                      */
/*                                                                         */
/*     INPUT         : i4Status - MLD Enable/ disable                      */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetStatus.\n");
    if (nmhTestv2FsMgmdMldGlobalStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
	
        MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
		 "MLD is globally disabled\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetStatus.\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMgmdMldGlobalStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
	MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to enable MLD globally\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetStatus.\n");
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetStatus.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetIfaceStatus                               */
/*                                                                         */
/*     Description   :  Enables/Disables Mld on the interface              */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Status - IGMP Enable/disable                      */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetIfaceStatus (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4IfStatus = 0;
    UINT1               u1Create = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetIfaceStatus.\n");
    if (i4Status != MLD_DISABLE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                     MLD_CREATE, &u1Create,
                                     MLD_FALSE) != CLI_SUCCESS)
        {
	    MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                     "Failed to update interaface table for interface %d\n", i4IfIndex);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting MldCliSetIfaceStatus.\n");
            return CLI_FAILURE;
        }
    }
    else 
    {
       if (nmhGetMgmdRouterInterfaceStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                         &i4IfStatus) == SNMP_FAILURE)
       {
           CLI_SET_ERR (CLI_MLD_INTERFACE_NOT_FOUND);
           MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting MldCliSetIfaceStatus.\n");
           return CLI_FAILURE;
       }
    }
    if (nmhTestv2FsMgmdInterfaceAdminStatus (&u4ErrorCode, i4IfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             i4Status) == SNMP_FAILURE)
    {
	MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Invalid admin status for index %d\n", i4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetIfaceStatus.\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMgmdInterfaceAdminStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                          i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
	MLD_DBG1(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to set admin status for interface %d\n", i4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetIfaceStatus.\n");
        return CLI_FAILURE;
    }

    if (u1Create == MLD_TRUE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_SET_ACTIVE, NULL,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
	    MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to update interaface table for interface %d\n", i4IfIndex);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetIfaceStatus.\n");
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetIfaceStatus.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetFastLeave                                 */
/*                                                                         */
/*     Description   :  Enables/Disables fast leave on the interface       */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Status - IGMP fast leave Enable/disable           */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetFastLeave (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4Status)
{
    UINT4               u4ErrorCode;
    INT4                i4IfStatus = 0;
    UINT1               u1Create = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetFastLeave.\n");
    if (i4Status != MLD_FAST_LEAVE_DISABLE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_CREATE, &u1Create,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
	    MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                     "Failed to update interaface table for interface %d\n", i4IfIndex);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                     "Exiting MldCliSetFastLeave.\n");
            return CLI_FAILURE;
        }
    }
    else
    {
       if (nmhGetMgmdRouterInterfaceStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                         &i4IfStatus) == SNMP_FAILURE)
       {
           CLI_SET_ERR (CLI_MLD_INTERFACE_NOT_FOUND);
           MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting MldCliSetFastLeave.\n");
           return CLI_FAILURE;
       }
    }
    if (nmhTestv2FsMgmdInterfaceFastLeaveStatus (&u4ErrorCode, i4IfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 i4Status) != SNMP_SUCCESS)
    {
        MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Invalid interface fast leave status for interface %d\n", i4IfIndex);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetFastLeave.\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMgmdInterfaceFastLeaveStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                              i4Status) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
	MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Unable to set interface first leave status for interface %d\n", i4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetFastLeave.\n");
        return CLI_FAILURE;
    }

    if (u1Create == MLD_TRUE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_SET_ACTIVE, NULL,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
	    MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to update interaface table for interface %d\n", i4IfIndex);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetFastLeave.\n");
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetFastLeave.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetMldVersion                                */
/*                                                                         */
/*     Description   :  Configures the MLD version for this interface      */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Version - IGMP version                            */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetMldVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetMldVersion.\n");
    if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                     MLD_CREATE, &u1Create,
                                     MLD_TRUE) != CLI_SUCCESS)
    {
        MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to update interaface table for interface %d\n", i4IfIndex);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetMldVersion.\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2MgmdRouterInterfaceVersion (&u4ErrorCode, i4IfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             u4Version) != SNMP_SUCCESS)
    {
	MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Invalid Router interface version for interface %d\n", i4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetMldVersion.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMgmdRouterInterfaceVersion (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                          u4Version) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
	MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to set Router interface version for interface %d\n", i4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetMldVersion.\n");
        return CLI_FAILURE;
    }

    if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                     MLD_SET_ACTIVE, NULL,
                                     MLD_FALSE) != CLI_SUCCESS)
    {
        MLD_DBG (IGMP_DBG_EXIT, "Exit : MldCliSetMldVersion.\n");
        return CLI_FAILURE;
    }

    MLD_DBG (IGMP_DBG_EXIT, "Exit : MldCliSetMldVersion.\n");
    return CLI_SUCCESS;
}


/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetNoMldVersion                                */
/*                                                                         */
/*     Description   :  Configures the MLD version for this interface      */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4Version - IGMP version                            */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetNoMldVersion (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Version)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT1                i1Status = SNMP_FAILURE;

    MLD_DBG (IGMP_DBG_ENTRY, "Entry : MldCliSetMldVersion.\n");
    
    i1Status = nmhGetMgmdRouterInterfaceStatus (i4IfIndex,
		                                IPVX_ADDR_FMLY_IPV6,
		                                &i4RowStatus);
    if (i1Status == SNMP_FAILURE)
    {
        MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to get Router interface status for interface %d\n", i4IfIndex);
	MLD_DBG (IGMP_DBG_EXIT, "Exit : MldCliSetMldVersion.\n");
	return CLI_FAILURE;
    }


    if (nmhTestv2MgmdRouterInterfaceVersion (&u4ErrorCode, i4IfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             u4Version) != SNMP_SUCCESS)
    {
	MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Invalid Router interface version for interface %d\n", i4IfIndex);
        MLD_DBG (IGMP_DBG_EXIT, "Exit : MldCliSetMldVersion.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMgmdRouterInterfaceVersion (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                          u4Version) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
	MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Failed to set Router interface version for interface %d\n", i4IfIndex);
        MLD_DBG (IGMP_DBG_EXIT, "Exit : MldCliSetMldVersion.\n");
        return CLI_FAILURE;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetMldVersion.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetQryInterval                               */
/*                                                                         */
/*     Description   :  Configures the IGMP query interval for this        */
/*                      interface                                          */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Interval - IGMP query interval                    */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetQryInterval (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetQryInterval.\n");
    if (u4Interval != MLD_NO_QUERY_INTERVAL)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_CREATE, &u1Create,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
	    MLD_DBG1(MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                    "Failed to  update interface tables for interface %d\n", i4IfIndex);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
	         	 "Exiting MldCliSetQryInterval.\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Interval = MLD_DEFAULT_QUERY_INTERVAL;
    }

    if (nmhTestv2MgmdRouterInterfaceQueryInterval (&u4ErrorCode, i4IfIndex,
                                                   IPVX_ADDR_FMLY_IPV6,
                                                   u4Interval) != SNMP_SUCCESS)
    {
        /*Query-interval between 1 to 10 should be configured
         *only for IGMP Version 1 */
        if (u4ErrorCode == IGMP_ONE)
        {
            CliPrintf (CliHandle,
                       "\r%%Query interval should be greater than 10"
                       " for MLD Version 1 and 2\r\n");
	    MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Invalid query Interval\n");

        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryInterval.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMgmdRouterInterfaceQueryInterval (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                                u4Interval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
	MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Unable to set  interface query Interval\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryInterval.\n");
        return CLI_FAILURE;
    }

    if (u1Create == MLD_TRUE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_SET_ACTIVE, NULL,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
	    MLD_DBG1(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "Unable to  update interface table for interface %d\n", i4IfIndex);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryInterval.\n");
            return CLI_FAILURE;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryInterval.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetQryMaxRespTime                            */
/*                                                                         */
/*     Description   :  Configures the MLD query mas response time for     */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Value - IGMP query max response time              */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetQryMaxRespTime (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetQryMaxRespTime.\n");
    if (u4Value != MLD_NO_MAX_RESP_TIME)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_CREATE, &u1Create,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
	    MLD_TRC (MGMD_TRC_FLAG, DATA_PATH_TRC, IgmpTrcGetModuleName(DATA_PATH_TRC),
                  "Updation of InterfaceTables failed.\n");
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting MldCliSetQryMaxRespTime.\n");
	    return CLI_FAILURE;
        }
    }
    else
    {
       u4Value = MLD_DEFAULT_MAX_RESP_TIME;
    }    

    if (nmhTestv2MgmdRouterInterfaceQueryMaxResponseTime
        (&u4ErrorCode, i4IfIndex, IPVX_ADDR_FMLY_IPV6, u4Value) == SNMP_FAILURE)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryMaxRespTime.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMgmdRouterInterfaceQueryMaxResponseTime
        (i4IfIndex, IPVX_ADDR_FMLY_IPV6, u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryMaxRespTime.\n");
        return CLI_FAILURE;
    }

    if (u1Create == MLD_TRUE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_SET_ACTIVE, NULL,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryMaxRespTime.\n");
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetQryMaxRespTime.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetRobustnessValue                           */
/*                                                                         */
/*     Description   :  Configures the IGMP robustness value for           */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Value - IGMP robustness value                     */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetRobustnessValue (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Value)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Status = 0;
    UINT1               u1Create = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetRobustnessValue.\n");
    if (u4Value != MLD_NO_ROB_VARIABLE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_CREATE, &u1Create,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
	        	 "Exiting MldCliSetRobustnessValue.\n");
            return CLI_FAILURE;
        }
    }
    else 
    {
       if (nmhGetMgmdRouterInterfaceStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                         &i4Status) == SNMP_FAILURE)
       {
           CLI_SET_ERR (CLI_MLD_INTERFACE_NOT_FOUND);
           MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting MldCliSetRobustnessValue.\n");
           return CLI_FAILURE;
       }

       u4Value = MLD_DEFAULT_ROB_VARIABLE;
    }
    
    if (nmhTestv2MgmdRouterInterfaceRobustness (&u4ErrorCode, i4IfIndex,
                                                IPVX_ADDR_FMLY_IPV6,
                                                u4Value) == SNMP_FAILURE)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetRobustnessValue.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMgmdRouterInterfaceRobustness (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                             u4Value) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetRobustnessValue.\n");
        return CLI_FAILURE;
    }

    if (u1Create == MLD_TRUE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_SET_ACTIVE, NULL,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetRobustnessValue.\n");
            return CLI_FAILURE;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetRobustnessValue.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetLastMbrQryInterval                        */
/*                                                                         */
/*     Description   :  Configures the MLD last member query interval for  */
/*                      this interface                                     */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : u4Interval - MLD last member query interval         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetLastMbrQryInterval (tCliHandle CliHandle, INT4 i4IfIndex,
                             UINT4 u4Interval)
{
    UINT4               u4ErrorCode = 0;
    UINT1               u1Create = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetLastMbrQryInterval.\n");
    if (u4Interval != MLD_NO_LAST_MEMBER_QIVAL)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_CREATE, &u1Create,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetLastMbrQryInterval.\n");
            return CLI_FAILURE;
        }
    }
    else 
    {
        u4Interval = MLD_DEFAULT_LAST_MEMBER_QIVAL;
    }
    
    if (nmhTestv2MgmdRouterInterfaceLastMemberQueryInterval
        (&u4ErrorCode, i4IfIndex, IPVX_ADDR_FMLY_IPV6,
         u4Interval) == SNMP_FAILURE)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetLastMbrQryInterval.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMgmdRouterInterfaceLastMemberQueryInterval
        (i4IfIndex, IPVX_ADDR_FMLY_IPV6, u4Interval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetLastMbrQryInterval.\n");
        return CLI_FAILURE;
    }

    if (u1Create == MLD_TRUE)
    {
        if (MldCliUpdateInterfaceTables (CliHandle, i4IfIndex,
                                         MLD_SET_ACTIVE, NULL,
                                         MLD_FALSE) != CLI_SUCCESS)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetLastMbrQryInterval.\n");
            return CLI_FAILURE;
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetLastMbrQryInterval.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliDeleteInterface                              */
/*                                                                         */
/*     Description   :  Deletes MLD capable interface                      */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliDeleteInterface (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4Status = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliDeleteInterface.\n");
    if (nmhGetMgmdRouterInterfaceStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                         &i4Status) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_MLD_INTERFACE_NOT_FOUND);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliDeleteInterface.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMgmdRouterInterfaceStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                         MLD_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliDeleteInterface.\n");
        return CLI_FAILURE;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliDeleteInterface.\n");
    return CLI_SUCCESS;
}


/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetRateLimit                                 */
/*                                                                         */
/*     Description   :  Configures the Global MLD rate limit for joins     */
/*                                                                packets  */
/*                                                                         */
/*     INPUT         : i4RateLimit - MLD interface join rate               */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetRateLimit (tCliHandle CliHandle, INT4 i4RateLimit)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMgmdJoinPktRate 
			(&u4ErrorCode,IPVX_ADDR_FMLY_IPV6, 
                         i4RateLimit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMgmdJoinPktRate (IPVX_ADDR_FMLY_IPV6,
                                          i4RateLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliSetInterfaceRateLimit                        */
/*                                                                         */
/*     Description   :  Configures the MLD rate limit for joins packets    */
/*                      in this interface                                  */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                   : i4RateLimit - MLD interface join rate               */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetInterfaceRateLimit (tCliHandle CliHandle, INT4 i4IfIndex,
                              INT4 i4RateLimit)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMgmdInterfaceJoinPktRate 
			(&u4ErrorCode, i4IfIndex,
                       	IPVX_ADDR_FMLY_IPV6, i4RateLimit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMgmdInterfaceJoinPktRate (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                          i4RateLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliShowGlobalInfo                               */
/*                                                                         */
/*     Description   :  Displays MLD global configuration                  */
/*                                                                         */
/*     INPUT         : None                                                */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliShowGlobalInfo (tCliHandle CliHandle)
{
    INT4                i4Status = 0;
    INT4                i4JoinPktRate = 0; 

    nmhGetFsMgmdMldGlobalStatus (&i4Status);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliShowGlobalInfo.\n");
    if (i4Status == MLD_ENABLE)
    {
        CliPrintf (CliHandle, "MLD is globally enabled\r\n");
	MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "MLD is globally enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "MLD is globally disabled\r\n");
	MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "MLD is globally disabled\n");
    }

    if (gIgmpConfig.i1MldProxyStatus == MLD_ENABLE)
    {
        CliPrintf (CliHandle, "MLD Proxy is globally"
                   " enabled in the system\r\n");
	MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "MLD Proxy is globally enabled in the system \r\n");
    }
    nmhGetFsMgmdJoinPktRate (IPVX_ADDR_FMLY_IPV6, &i4JoinPktRate);
    CliPrintf (CliHandle,
	    "MLD Global rate limit for joins packets : %d \r\n",
	    i4JoinPktRate);
    MLD_DBG1(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 "MLD Global rate limit for joins packets : %d \r\n",
		 i4JoinPktRate);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowGlobalInfo.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliShowIfInfoForIndex                           */
/*                                                                         */
/*     Description   :  Displays MLD interface configuration on specified  */
/*            interface                        */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
PRIVATE VOID
MldCliShowIfInfoForIndex (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tNetIpv6IfInfo      Ip6IfRecord;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Port = 0;
    UINT4               u4MaskBits = 0;
    UINT4               u4IfaceGroups = 0;
    UINT4               u4IfaceQryIntrval = 0;
    UINT4               u4IfaceVersion = 0;
    UINT4               u4IfaceQryMaxRespTime = 0;
    UINT4               u4RobustnessValue = 0;
    UINT4               u4IfaceLastMbrQryIntrval = 0;
    INT4                i4IfaceFastLeaveStatus = 0;
    INT4                i4IfaceOperStatus = 0;
    INT4                i4IfaceAdminStatus = 0;
    INT4                i4RateLimit = 0;
    tSNMP_OCTET_STRING_TYPE IfaceQuerier;
    tIp6Addr            Ip6Querier;
    UINT1               u1UpStream = MLD_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliShowIfInfoForIndex.\n");
    MEMSET (&Ip6IfRecord, MLD_ZERO, sizeof (tNetIpv6IfInfo));
    MEMSET (Ip6Querier.u1_addr, 0, IP6_ADDR_SIZE);
    IfaceQuerier.pu1_OctetList = Ip6Querier.u1_addr;
    IfaceQuerier.i4_Length = IP6_ADDR_SIZE;

    /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
    CfaGetInterfaceNameFromIndex (i4IfIndex, au1InterfaceName);
    /*    MLD_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, &u4Port); */
    MLD_IP6_GET_IF_CONFIG_RECORD (i4IfIndex, &Ip6IfRecord);

    if ((MLD_PROXY_STATUS () == MLD_ENABLE) &&
        (MldpUtilCheckUpIface (u4Port) == OSIX_SUCCESS))
    {
        u1UpStream = MLD_TRUE;
    }
    else
    {
        nmhGetMgmdRouterInterfaceQueryInterval (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                                &u4IfaceQryIntrval);
        nmhGetMgmdRouterInterfaceVersion (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                          &u4IfaceVersion);
        /* IGMPV2 = MLDv1 */
        if (u4IfaceVersion == IGMP_TWO)
        {
            u4IfaceVersion = IGMP_ONE;
        }
        /* IGMPV3 = MLDv2 */
        else if (u4IfaceVersion == IGMP_VERSION_THREE)
        {
            u4IfaceVersion = IGMP_TWO;
        }
        nmhGetMgmdRouterInterfaceQuerier (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                          &IfaceQuerier);
        nmhGetMgmdRouterInterfaceQueryMaxResponseTime (i4IfIndex,
                                                       IPVX_ADDR_FMLY_IPV6,
                                                       &u4IfaceQryMaxRespTime);
        nmhGetMgmdRouterInterfaceRobustness (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                             &u4RobustnessValue);
        nmhGetMgmdRouterInterfaceLastMemberQueryInterval (i4IfIndex,
                                                          IPVX_ADDR_FMLY_IPV6,
                                                          &u4IfaceLastMbrQryIntrval);
        nmhGetFsMgmdInterfaceFastLeaveStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                              &i4IfaceFastLeaveStatus);
        nmhGetMgmdRouterInterfaceGroups (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                         &u4IfaceGroups);
    }
    nmhGetFsMgmdInterfaceOperStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                     &i4IfaceOperStatus);
    nmhGetFsMgmdInterfaceAdminStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                      &i4IfaceAdminStatus);
    nmhGetFsMgmdInterfaceJoinPktRate (i4IfIndex, IPVX_ADDR_FMLY_IPV6, &i4RateLimit);

    CliPrintf (CliHandle, "%s,", au1InterfaceName);
    if (Ip6IfRecord.u4Oper == MLD_IFACE_OPER_UP)
    {
        CliPrintf (CliHandle, " line protocol is up\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " line protocol is down\r\n");
    }

    CliPrintf (CliHandle, " Internet Address is %s\n",
               Ip6PrintNtop (&(Ip6IfRecord.Ip6Addr)));

    u4MaskBits = 0;
    CliPrintf (CliHandle, " Prefix Length is %d\r\n", u4MaskBits);

    if ((i4IfaceOperStatus == MLD_IFACE_OPER_UP) &&
        (i4IfaceAdminStatus == MLD_IFACE_ADMIN_UP))
    {
        CliPrintf (CliHandle, " MLD is enabled on interface\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " MLD is disabled on interface\r\n");
    }

    if (u1UpStream == MLD_TRUE)
    {
        CliPrintf (CliHandle,
                   " Interface is configured as Upstream interface\r\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   " Current MLD router version is %d\r\n", u4IfaceVersion);
	CliPrintf (CliHandle,
                   " MLD query interval is %d seconds\r\n", u4IfaceQryIntrval);
        CliPrintf (CliHandle,
                   " Last member query response interval is %d seconds\r\n",
                   u4IfaceLastMbrQryIntrval);
	CliPrintf (CliHandle,
                   " MLD max query response time is %d milli-seconds\r\n",
                   u4IfaceQryMaxRespTime);
	CliPrintf (CliHandle,
                   " MLD rate limit for joins packets is %d\r\n",
                   i4RateLimit);
	CliPrintf (CliHandle, " Robustness value is %d\r\n", u4RobustnessValue);
        CliPrintf (CliHandle, " MLD querying router is ");
        CliPrintf (CliHandle, "%-16s",
                   Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                 IfaceQuerier.pu1_OctetList));

        if (Ip6AddrCompare (Ip6IfRecord.Ip6Addr, Ip6Querier) == 0)
        {
            CliPrintf (CliHandle, " (this system)\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }
        if (i4IfaceFastLeaveStatus == MLD_FAST_LEAVE_ENABLE)
        {
            CliPrintf (CliHandle,
                       " Fast leave is enabled on this interface\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Fast leave is disabled on this interface\r\n");
        }
        if (u4IfaceGroups)
        {
            CliPrintf (CliHandle,
                       " Number of multicast groups joined %d\r\n",
                       u4IfaceGroups);
        }
        else
        {
            CliPrintf (CliHandle, " No multicast groups joined\r\n");
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowIfaceInfoForIndex.\n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliShowInterfaceInfo                            */
/*                                                                         */
/*     Description   :  Displays MLD interface configuration               */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface index                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliShowInterfaceInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextAddrType = IGMP_ZERO;
    INT4                i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
    INT4                i4RowStatus = IGMP_ZERO;
    UINT1               u1IsShowAll = TRUE;
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4Status = IGMP_ZERO; 
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                 "Entering MldCliShowInterfaceInfo.\n");
    nmhGetFsMgmdMldGlobalStatus (&i4Status);

    if (i4Status == MLD_ENABLE)
    {
        CliPrintf (CliHandle, "MLD is globally enabled\r\n");
        MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                     "MLD is globally enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "MLD is globally disabled\r\n");
        MLD_DBG_INFO(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                     "MLD is globally disabled\n");
    }
    if (i4IfIndex != MLD_INVALID_INDEX)
    {
        i1Status = nmhGetMgmdRouterInterfaceStatus (i4IfIndex,
                                                    IPVX_ADDR_FMLY_IPV6,
                                                    &i4RowStatus);

        if ((i1Status != SNMP_SUCCESS) || (i4RowStatus != MLD_ACTIVE))
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowInterfaceInfo.\n");
            return CLI_SUCCESS;
        }
        MldCliShowIfInfoForIndex (CliHandle, i4IfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowInterfaceInfo.\n");
        return CLI_SUCCESS;
    }

    if (nmhGetFirstIndexMgmdRouterInterfaceTable
        (&i4NextIfIndex, &i4CurAddrType) != SNMP_SUCCESS)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowInterfaceInfo.\n");
        return CLI_SUCCESS;
    }
    do
    {
        MldCliShowIfInfoForIndex (CliHandle, i4NextIfIndex);

        i4CurrIfIndex = i4NextIfIndex;
        while (u1IsShowAll)
        {
            if (nmhGetNextIndexMgmdRouterInterfaceTable
                (i4CurrIfIndex, &i4NextIfIndex, i4CurAddrType,
                 &i4NextAddrType) == SNMP_FAILURE)
            {
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowInterfaceInfo.\n");
                return CLI_SUCCESS;
            }

            i4CurrIfIndex = i4NextIfIndex;

            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
                break;
            }
            i4CurAddrType = IPVX_ADDR_FMLY_IPV4;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        if (u4PagingStatus == CLI_FAILURE)
        {
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowInterfaceInfo.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliShowGroupInfo                                */
/*                                                                         */
/*     Description   :  Displays MLD group information                     */
/*                                                                         */
/*     INPUT         : None                                                */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliShowGroupInfo (tCliHandle CliHandle)
{
    UINT4               u4CurrentSysTime = 0;
    UINT4               u4UpTime = 0;
    CHR1                aTimeFormat[17] = "";
    CHR1                aUpTime[17] = "";
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT4               u4RemainingTime = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4IgmpIfIndex = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4IgmpInterfaceAdminStatus = 0;
    INT4                i4Flag = 0;

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliShowGroupInfo.\n");
    if (pIfGetNextLink == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowGroupInfo.\n");
        return CLI_SUCCESS;
    }
    do
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            continue;
        }

        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        i4IgmpIfIndex = (INT4) pIfaceNode->u4IfIndex;

        MLD_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IgmpIfIndex, &u4IfIndex);
        if (CfaGetInterfaceNameFromIndex (u4IfIndex, au1InterfaceName) ==
            OSIX_FAILURE)
        {
            continue;
        }
        if (nmhGetFsMgmdInterfaceAdminStatus (u4IfIndex,
                                              IPVX_ADDR_FMLY_IPV6,
                                              &i4IgmpInterfaceAdminStatus)
            == SNMP_FAILURE)
        {
            continue;
        }
        /*Check whether MLD is enabled on the VLAN */
        if (i4IgmpInterfaceAdminStatus != MLD_IFACE_ADMIN_DOWN)
        {
            MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

            IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
            IgmpGrpEntry.pIfNode = pIfaceNode;

            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) & IgmpGrpEntry,
                                                 NULL);
            while (pGrp != NULL)
            {
                if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
                {
                    /* Group entry doesn't belong to specified interface index */
		    MLD_DBG2(MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                 "Group entry %s doesn't belong to specified"
				 "interface index %d", IgmpPrintIPvxAddress(pGrp->u4Group), 
				  pIfaceNode->u4IfIndex );
                    break;
                }

                if (i4Flag == 0)
                {
                    CliPrintf (CliHandle,
                               "\nI - Include Mode,  E - Exclude Mode\r\n"
                               "S - Static Mbr,    D - Dynamic Mbr\r\n");

                    CliPrintf (CliHandle,
                               "\r\n%-16s%-4s%-13s%-17s%-17s%-16s\r\n",
                               "GroupAddress", "Flg", "Iface", "UpTime",
                               "ExpiryTime", "LastReporter");

                    CliPrintf (CliHandle, "%-16s%-4s%-13s%-17s%-17s%-16s\r\n",
                               "---------------", "---", "------------",
                               "----------------", "----------------",
                               "---------------");
                    i4Flag = 1;
                }

                CliPrintf (CliHandle, "%-16s",
                           IgmpPrintIPvxAddress (pGrp->u4Group));

                if (pIfaceNode->u1Version == MLD_VERSION_2)
                {
                    if (pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)
                    {
                        if (pGrp->u1StaticFlg == MLD_TRUE)
                        {
                            if (pGrp->u1DynamicFlg == MLD_TRUE)
                            {
                                CliPrintf (CliHandle, "%-4s", "ISD");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-4s", "IS");
                            }
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-4s", "ID");
                        }
                    }
                    else if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                    {
                        if (pGrp->u1StaticFlg == MLD_TRUE)
                        {
                            if (pGrp->u1DynamicFlg == MLD_TRUE)
                            {
                                CliPrintf (CliHandle, "%-4s", "ESD");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-4s", "ES");
                            }
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-4s", "ED");
                        }
                    }
                }
                else
                {
                    if ((pGrp->u1StaticFlg == MLD_TRUE) &&
                        (pGrp->u1DynamicFlg == MLD_TRUE))
                    {
                        CliPrintf (CliHandle, "%-4s", "SD");
                    }

                    if ((pGrp->u1StaticFlg == MLD_TRUE) &&
                        (pGrp->u1DynamicFlg == MLD_FALSE))
                    {
                        CliPrintf (CliHandle, "%-4s", "S");
                    }

                    if ((pGrp->u1StaticFlg == MLD_FALSE) &&
                        (pGrp->u1DynamicFlg == MLD_TRUE))
                    {
                        CliPrintf (CliHandle, "%-4s", "D");
                    }
                }

                CliPrintf (CliHandle, "%-13s", au1InterfaceName);

                OsixGetSysTime (&u4CurrentSysTime);

                u4UpTime = u4CurrentSysTime - pGrp->u4CacheUpTime;
                IgmpDispHhMmSs (u4UpTime, aUpTime);
                CliPrintf (CliHandle, "%-17s", aUpTime);

                IgmpDispHhMmSs (u4RemainingTime, aTimeFormat);
                if (pGrp->Timer.u1TmrStatus == IGMP_TMR_SET)
                {
                    if (TmrGetRemainingTime (gIgmpTimerListId,
                                             &(pGrp->Timer.TimerBlk.TimerNode),
                                             &u4RemainingTime) != TMR_SUCCESS)
                    {
                        MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, IGMP_NAME,
                                  "Tmr Get remaining time failure ! \n");
                    }

                    IgmpDispHhMmSs (u4RemainingTime, aTimeFormat);
                }

                CliPrintf (CliHandle, "%-17s", aTimeFormat);
                CliPrintf (CliHandle, "%-16s\r\n",
                           IgmpPrintIPvxAddress (pGrp->u4LastReporter));

                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);
            }
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while ((pIfGetNextLink = (TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                            &(pIfaceNode->IfGetNextLink)))) !=
           NULL);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowGroupInfo.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliShowSourceInfo                               */
/*                                                                         */
/*     Description   :  Displays MLD source information                    */
/*                                                                         */
/*     INPUT         : None                                                */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliShowSourceInfo (tCliHandle CliHandle)
{
    CHR1                aTimeFormat[17] = "";
    tIgmpIface         *pIfaceNode = NULL;
    tTMO_SLL_NODE      *pSrcLink = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    UINT4               u4RemainingTime = IGMP_ZERO;
    INT4                i4IgmpIfIndex = IGMP_ZERO;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    tIgmpIPvXAddr       LastReporter;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4IfIndex = 0;
    INT4                i4Flag = 0;
    INT4                i4IgmpInterfaceAdminStatus = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliShowSourceInfo.\n");
    IGMP_IPVX_ADDR_CLEAR (&LastReporter);

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);
    if (pIfGetNextLink == NULL)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowSourceInfo.\n");
        return CLI_SUCCESS;
    }
    do
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            continue;
        }
        /*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        i4IgmpIfIndex = (INT4) pIfaceNode->u4IfIndex;
        MLD_IP6_GET_IFINDEX_FROM_PORT ((UINT4) i4IgmpIfIndex, &u4IfIndex);
        if ((CfaGetInterfaceNameFromIndex (u4IfIndex,
                                           au1InterfaceName) == OSIX_FAILURE))
        {
            continue;
        }
        if (nmhGetFsMgmdInterfaceAdminStatus (u4IfIndex,
                                              IPVX_ADDR_FMLY_IPV6,
                                              &i4IgmpInterfaceAdminStatus)
            == SNMP_FAILURE)
        {
            continue;
        }

        if (i4IgmpInterfaceAdminStatus != MLD_IFACE_ADMIN_DOWN)
        {
            MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

            IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
            IgmpGrpEntry.pIfNode = pIfaceNode;

            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) & IgmpGrpEntry,
                                                 NULL);
            while (pGrp != NULL)
            {
                IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &(pGrp->u4Group));

                if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
                {
                    /* Group entry doesn't belong to specified interface index */
                    break;
                }
                if (i4Flag == 0)
                {
                    CliPrintf (CliHandle,
                               "\nI - Include Mode,  E - Exclude Mode\r\n"
                               "S - Static Mbr, D - Dynamic Mbr\n"
                               "F - Forward List, N - Non-Forward List\n");
                    CliPrintf (CliHandle,
                               "\r\n%-16s%-13s%-16s%-5s%-17s%-16s\r\n",
                               "GroupAddress", "Iface", "SrcAddress", "Flg",
                               "ExpiryTime", "LastReporter");

                    CliPrintf (CliHandle, "%-16s%-13s%-16s%-5s%-17s%-16s\r\n",
                               "---------------", "------------", "---------------",
                               "----", "----------------", "---------------");
                    i4Flag = 1;

                }
                TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
                {
                    pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);

                    CliPrintf (CliHandle, "%-16s",
                               IgmpPrintIPvxAddress (pGrp->u4Group));

                    CliPrintf (CliHandle, "%-13s", au1InterfaceName);

                    CliPrintf (CliHandle, "%-16s",
                               IgmpPrintIPvxAddress (pSrc->u4SrcAddress));

                    if (pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)
                    {
                        if (pSrc->u1StaticFlg == MLD_TRUE)
                        {
                            if (pSrc->u1DynamicFlg == MLD_TRUE)
                            {
                                if (pSrc->u1FwdState == MLD_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "ISDF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "ISDN");
                                }
                            }
                            else
                            {
                                if (pSrc->u1FwdState == MLD_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "ISF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "ISN");
                                }
                            }
                        }
                        else
                        {
                            if (pSrc->u1FwdState == MLD_TRUE)
                            {
                                CliPrintf (CliHandle, "%-5s", "IDF");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-5s", "IDN");
                            }
                        }
                    }
                    else if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                    {
                        if (pSrc->u1StaticFlg == MLD_TRUE)
                        {
                            if (pSrc->u1DynamicFlg == MLD_TRUE)
                            {
                                if (pSrc->u1FwdState == MLD_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "ESDF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "ESDN");
                                }
                            }
                            else
                            {
                                if (pSrc->u1FwdState == MLD_TRUE)
                                {
                                    CliPrintf (CliHandle, "%-5s", "ESF");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "%-5s", "ESN");
                                }
                            }
                        }
                        else
                        {
                            if (pSrc->u1FwdState == MLD_TRUE)
                            {
                                CliPrintf (CliHandle, "%-5s", "EDF");
                            }
                            else
                            {
                                CliPrintf (CliHandle, "%-5s", "EDN");
                            }
                        }
                    }

                    IgmpDispHhMmSs (u4RemainingTime, aTimeFormat);
                    if (pSrc->srcTimer.u1TmrStatus == IGMP_TMR_SET)
                    {
                        if (TmrGetRemainingTime (gIgmpTimerListId,
                                                 &(pSrc->srcTimer.TimerBlk.
                                                   TimerNode),
                                                 &u4RemainingTime) !=
                            TMR_SUCCESS)
                        {
                            MLD_DBG_INFO (MLD_DBG_FLAG, MLD_MGMT_MODULE, IGMP_NAME,
                                      "Tmr Get remaining time failure ! \n");
                        }

                        IgmpDispHhMmSs (u4RemainingTime, aTimeFormat);
                    }
                    else
                    {
                        u4RemainingTime = IGMP_ZERO;
                        IgmpDispHhMmSs (u4RemainingTime, aTimeFormat);
                    }
                    CliPrintf (CliHandle, "%-17s", aTimeFormat);

                    IGMP_IPVX_ADDR_COPY (&LastReporter,
                                         &(pSrc->u4LastReporter));
                    CliPrintf (CliHandle, "%-16s\r\n",
                               IgmpPrintIPvxAddress (LastReporter));
                }                /* End of Srclist scan */
                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);
            }
        }
        u4PagingStatus = CliPrintf (CliHandle, "\r");
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while ((pIfGetNextLink = (TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                            &(pIfaceNode->IfGetNextLink))))
           != NULL);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowSourceInfo.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldCliShowStatistics                               */
/*                                                                         */
/*     Description   :  Displays MLD statistics information                */
/*                                                                         */
/*     INPUT         : i4IfIndex - Interface Index                         */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliShowStatistics (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4RxGenQueries = 0;
    UINT4               u4RxGrpQueries = 0;
    UINT4               u4RxGrpSrcQueries = 0;
    UINT4               u4Rxv1Reports = 0;
    UINT4               u4Rxv2Reports = 0;
    UINT4               u4RxLeaves = 0;
    UINT4               u4TxGenQueries = 0;
    UINT4               u4TxGrpQueries = 0;
    UINT4               u4TxGrpSrcQueries = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Txv1Reports = 0;
    UINT4               u4Txv2Reports = 0;
    UINT4               u4TxLeaves = 0;
    UINT4               u4MldInSSMPkts = 0;
    UINT4               u4MldInvalidSSMPkts = 0;
    UINT4               u4MldInvalidV1Report = 0;
    UINT4               u4MldInvalidV2Report = 0;
    UINT4               u4MldMalformedPkts = 0;
    UINT4               u4MldInvalidIntf = 0;
    UINT4               u4MldSocketErr = 0;
    UINT4               u4MldBadScopeErr = 0;
    UINT4               u4MldSubnetCheckFailure = 0;
    UINT4               u4CKSumError = 0;
    UINT4               u4WrongVerQueries = 0;
    UINT4               u4PktLenError = 0;
    UINT4               u4UnknownMsgType = 0;
    UINT4               u4ReportVersionMisMatch = 0;
    UINT4               u4QryVersionMisMatch = 0;
    UINT1               u1IsShowAll = TRUE;
    INT4                i4IgmpInterfaceAdminStatus = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Status;
    tIgmpIface          *pIfNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliShowStatistics.\n");

    nmhGetFsMgmdMldGlobalStatus (&i4Status);
	if (i4Status == MLD_ENABLE)
	{
		MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
				"MLD is globally enabled\r\n");
	}
	else
	{
		CliPrintf (CliHandle, "MLD is globally disabled\r\n");
		MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
				"MLD is globally disabled\r\n");
	}

    if (nmhGetFirstIndexMgmdRouterInterfaceTable (&i4NextIfIndex, &i4AddrType)
        != SNMP_SUCCESS)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowStatistics.\n");
        return CLI_SUCCESS;
    }

    do
    {
        if (i4IfIndex != MLD_INVALID_INDEX)
        {
            if (i4IfIndex != i4NextIfIndex)
            {
                i4CurrIfIndex = i4NextIfIndex;

                if (nmhGetNextIndexMgmdRouterInterfaceTable
                    (i4CurrIfIndex, &i4NextIfIndex, IPVX_ADDR_FMLY_IPV6,
                     &i4AddrType) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			IgmpGetModuleName(IGMP_EXIT_MODULE), "Exiting MldCliShowStatistics.\n");
                    return CLI_SUCCESS;
                }
            }
            if (IgmpGetPortFromIfIndex (i4NextIfIndex,
                                        IPVX_ADDR_FMLY_IPV6,
                                        &u4Port) == IGMP_FAILURE)
            {
                CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
                return SNMP_FAILURE;
            }

	    pIfNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV6);
            if (pIfNode != NULL)
            {
                i4AddrType = pIfNode->u1AddrType;
            }
            if (i4AddrType != IPVX_ADDR_FMLY_IPV6)
            {
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
		IgmpGetModuleName(IGMP_EXIT_MODULE), "Exiting MldCliShowStatistics.\n");
                return CLI_SUCCESS;
            }
        }

        if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
	/*Call CfaUtil for getting IfAlias corresponding to IfIndex */
        CfaGetInterfaceNameFromIndex (i4NextIfIndex, au1InterfaceName);
        nmhGetFsMgmdInterfaceAdminStatus (i4NextIfIndex,
                                          IPVX_ADDR_FMLY_IPV6,
                                          &i4IgmpInterfaceAdminStatus);
        /*Check whether MLD is enabled on the VLAN
         * if enabled print the MLD statistics
         * else continue */
        if (i4IgmpInterfaceAdminStatus != MLD_IFACE_ADMIN_DOWN)
        {
            nmhGetFsMgmdInterfaceRxGenQueries (i4NextIfIndex,
                                               IPVX_ADDR_FMLY_IPV6,
                                               &u4RxGenQueries);
            nmhGetFsMgmdInterfaceRxGrpQueries (i4NextIfIndex,
                                               IPVX_ADDR_FMLY_IPV6,
                                               &u4RxGrpQueries);
            nmhGetFsMgmdInterfaceRxGrpAndSrcQueries (i4NextIfIndex,
                                                     IPVX_ADDR_FMLY_IPV6,
                                                     &u4RxGrpSrcQueries);
            nmhGetFsMgmdInterfaceRxMldv1Reports (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4Rxv1Reports);
            nmhGetFsMgmdInterfaceRxMldv2Reports (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4Rxv2Reports);
            nmhGetFsMgmdInterfaceTxGenQueries (i4NextIfIndex,
                                               IPVX_ADDR_FMLY_IPV6,
                                               &u4TxGenQueries);
            nmhGetFsMgmdInterfaceTxGrpQueries (i4NextIfIndex,
                                               IPVX_ADDR_FMLY_IPV6,
                                               &u4TxGrpQueries);
            nmhGetFsMgmdInterfaceTxGrpAndSrcQueries (i4NextIfIndex,
                                                     IPVX_ADDR_FMLY_IPV6,
                                                     &u4TxGrpSrcQueries);
            nmhGetFsMgmdInterfaceTxMldv1Reports (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4Txv1Reports);
            nmhGetFsMgmdInterfaceTxMldv2Reports (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4Txv2Reports);
            nmhGetFsMgmdInterfaceTxLeaves (i4NextIfIndex, IPVX_ADDR_FMLY_IPV6,
                                           &u4TxLeaves);
            nmhGetFsMgmdInterfaceIncomingLeaves (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4RxLeaves);
            nmhGetFsMgmdInterfaceIncomingSSMPkts (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4MldInSSMPkts);
            nmhGetFsMgmdInterfaceInvalidSSMPkts (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4MldInvalidSSMPkts);
            nmhGetFsMgmdInterfaceInvalidV1Report (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4MldInvalidV1Report);
            nmhGetFsMgmdInterfaceInvalidV2Report (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4MldInvalidV2Report);
            nmhGetFsMgmdInterfaceMalformedPkts (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4MldMalformedPkts);
            nmhGetFsMgmdInterfaceSocketErrors  (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4MldSocketErr);
            nmhGetFsMgmdInterfaceBadScopeErrors  (i4NextIfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 &u4MldBadScopeErr);
            nmhGetFsMgmdInterfaceSubnetCheckFailure (i4NextIfIndex,
                                                    IPVX_ADDR_FMLY_IPV6,
                                                    &u4MldSubnetCheckFailure);
            nmhGetFsMgmdInterfaceCKSumError  (i4NextIfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             &u4CKSumError);
            nmhGetFsMgmdInterfaceUnknownMsgType (i4NextIfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             &u4UnknownMsgType);
            nmhGetMgmdRouterInterfaceWrongVersionQueries (i4NextIfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             &u4WrongVerQueries);
            nmhGetFsMgmdInterfaceReportVersionMisMatch  (i4NextIfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             &u4ReportVersionMisMatch);
            nmhGetFsMgmdInterfaceQryVersionMisMatch (i4NextIfIndex,
                                             IPVX_ADDR_FMLY_IPV6,
                                             &u4QryVersionMisMatch);



	    CliPrintf (CliHandle, " MLD Statistics for %s              Incoming Pkts          OutGoing Pkts\r\n",
                       au1InterfaceName);
	    CliPrintf (CliHandle, "                                       -------------          -------------\r\n");
            CliPrintf (CliHandle,
                       " General queries %28d %25d\r\n",
                       u4RxGenQueries, u4TxGenQueries);
            CliPrintf (CliHandle,
                       " Group Specific queries %21d %25d\r\n",
                       u4RxGrpQueries, u4TxGrpQueries);
            CliPrintf (CliHandle,
                       " Group and Source Specific queries "
                       " %9d %25d\r\n", u4RxGrpSrcQueries, u4TxGrpSrcQueries);
            CliPrintf (CliHandle, " Mldv1 reports %30d\r\n",
                       u4Rxv1Reports);
            CliPrintf (CliHandle, " Mldv2 reports %30d\r\n",
                       u4Rxv2Reports);
            CliPrintf (CliHandle, " Mldv1 leaves  %30d\r\n",
                       u4RxLeaves);
            CliPrintf (CliHandle,
                       " Reports received in SSM range %14d\r\n",
                       u4MldInSSMPkts);
            CliPrintf (CliHandle,
                       " Invalid Reports received in SSM range %6d\r\n",
                       u4MldInvalidSSMPkts);
            CliPrintf (CliHandle,
                       " Invalid V1 Reports%26d\r\n",
                       u4MldInvalidV1Report);
            CliPrintf (CliHandle,
                       " Invalid V2 Reports%26d\r\n",
                       u4MldInvalidV2Report);
            CliPrintf (CliHandle, "\n Error Counters: \r\n");
            CliPrintf (CliHandle, " ===============\r\n");
            CliPrintf (CliHandle,
                       " Malformed Packets %26d\r\n",
                       u4MldMalformedPkts); 
            CliPrintf (CliHandle,
                       " Bad checksums %30d\r\n",u4CKSumError);
            CliPrintf (CliHandle,
                       " Data Length errors %25d\r\n",u4PktLenError);
            CliPrintf (CliHandle,
                       " Invalid interface %26d\r\n",u4MldInvalidIntf);
            CliPrintf (CliHandle,
                       " Invalid version %28d\r\n",
                       (u4WrongVerQueries + u4ReportVersionMisMatch + 
                        u4QryVersionMisMatch));
            CliPrintf (CliHandle,
                       " Subnet errors %30d\r\n",u4MldSubnetCheckFailure);
            CliPrintf (CliHandle,
                       " Socket errors %30d\r\n",u4MldSocketErr);
            CliPrintf (CliHandle,
                       " Scope errors %31d\r\n",u4MldBadScopeErr);
            CliPrintf (CliHandle,
                       " Unknown packet types %23d\r\n",u4UnknownMsgType);


	    if (MLD_PROXY_STATUS () == MLD_ENABLE)
            {
                CliPrintf (CliHandle,
                           " Mldv1 reports %42d\r\n",
                           u4Txv1Reports);
                CliPrintf (CliHandle,
                           " Mldv2 reports %42d\r\n",
                           u4Txv2Reports);
                CliPrintf (CliHandle,
                           " Mldv1 leaves %42d\r\n",
                           u4TxLeaves);
            }
        }
        }
        i4CurrIfIndex = i4NextIfIndex;
        while (u1IsShowAll)
        {
            if (nmhGetNextIndexMgmdRouterInterfaceTable
                (i4CurrIfIndex, &i4NextIfIndex, i4AddrType,
                 &i4NextAddrType) == SNMP_FAILURE)
            {
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting MldCliShowInterfaceInfo.\n");
                return CLI_SUCCESS;
            }

            i4CurrIfIndex = i4NextIfIndex;

            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                i4AddrType = IPVX_ADDR_FMLY_IPV6;
                break;
            }
            i4AddrType = IPVX_ADDR_FMLY_IPV4;
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        if ((u4PagingStatus == CLI_FAILURE) || (i4IfIndex != MLD_INVALID_INDEX))
        {
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliShowStatistics.\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : MldCliClearStats
 *
 *     DESCRIPTION      : Clears MLD transmit and receive statistics
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                      : i4IfIndex - Interface ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT1
MldCliClearStats (tCliHandle CliHandle,  INT4 i4IfIndex)
{
    tIgmpIface          *pIfaceNode = NULL;
    UINT4               u4Port = 0;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4AddrType = 0;
    UINT1               u1IsShowAll = TRUE;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    UNUSED_PARAM (CliHandle);

    /* Clears MLD statistics info */
 
    if (nmhGetFirstIndexMgmdRouterInterfaceTable (&i4NextIfIndex, &i4AddrType)
		    != SNMP_SUCCESS)
    {
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			    "Exiting MldCliClearStats.\n");
	    return CLI_FAILURE;
    }

    do
    {
        if (i4IfIndex != MLD_INVALID_INDEX)
        {
            if (i4IfIndex != i4NextIfIndex)
            {
                i4CurrIfIndex = i4NextIfIndex;

                if (nmhGetNextIndexMgmdRouterInterfaceTable
                    (i4CurrIfIndex, &i4NextIfIndex, IPVX_ADDR_FMLY_IPV6,
                     &i4AddrType) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                    IgmpGetModuleName(IGMP_EXIT_MODULE), "Exiting MldCliClearStats.\n");
                    return CLI_SUCCESS;
                }
            }
        }
	    if (IgmpGetPortFromIfIndex (i4NextIfIndex,
				    IPVX_ADDR_FMLY_IPV6,
				    &u4Port) == IGMP_FAILURE)
	    {
		    CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
		    return SNMP_FAILURE;
	    }

	    pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV6);
	    CfaGetInterfaceNameFromIndex ((UINT4)i4NextIfIndex, au1InterfaceName);

	    if (pIfaceNode != NULL)
	    {
		    pIfaceNode->u4InPkts = IGMP_ZERO;
		    pIfaceNode->u4InJoins = IGMP_ZERO;
		    pIfaceNode->u4InLeaves = IGMP_ZERO;
		    pIfaceNode->u4InQueries = IGMP_ZERO;
		    pIfaceNode->u4OutQueries = IGMP_ZERO;
		    pIfaceNode->u4WrongVerQueries = IGMP_ZERO;
		    pIfaceNode->u4ProcessedJoins = IGMP_ZERO;
		    pIfaceNode->u4RxGenQueries = IGMP_ZERO;
		    pIfaceNode->u4TxGenQueries = IGMP_ZERO;
		    pIfaceNode->u4TxGrpQueries = IGMP_ZERO;
		    pIfaceNode->u4TxGrpSrcQueries = IGMP_ZERO;
		    pIfaceNode->u4RxGrpQueries = IGMP_ZERO;
		    pIfaceNode->u4RxGrpSrcQueries = IGMP_ZERO;
		    pIfaceNode->u4Txv2Leaves = IGMP_ZERO;
		    pIfaceNode->u4CKSumError = IGMP_ZERO;
		    pIfaceNode->u4PktLenError = IGMP_ZERO;;
		    pIfaceNode->u4PktWithLocalIP = IGMP_ZERO;
		    pIfaceNode->u4SubnetCheckFailure = IGMP_ZERO;;
		    pIfaceNode->u4QryFromNonQuerier = IGMP_ZERO;;
		    pIfaceNode->u4ReportVersionMisMatch = IGMP_ZERO;;
		    pIfaceNode->u4QryVersionMisMatch = IGMP_ZERO;;
		    pIfaceNode->u4UnknownMsgType = IGMP_ZERO;;
		    pIfaceNode->u4InvalidV1Report = IGMP_ZERO;;
		    pIfaceNode->u4InvalidV2Report = IGMP_ZERO;;
		    pIfaceNode->u4RouterAlertCheckFailure = IGMP_ZERO;;
		    pIfaceNode->u4BadScopeErr = IGMP_ZERO;
		    pIfaceNode->u4MldRxv1Reports = IGMP_ZERO;
		    pIfaceNode->u4MldRxv2Reports = IGMP_ZERO;
		    pIfaceNode->u4MldTxv1Reports = IGMP_ZERO;
		    pIfaceNode->u4MldTxv2Reports = IGMP_ZERO;
		    pIfaceNode->u4MldInSSMPkts = IGMP_ZERO;
		    pIfaceNode->u4MldInvalidSSMPkts = IGMP_ZERO;
		    pIfaceNode->u4MldMalformedPkt = IGMP_ZERO;
		    pIfaceNode->u4MldSocketErr = IGMP_ZERO;
                    pIfaceNode->u4MldBadScopeErr = IGMP_ZERO;
                    pIfaceNode->u4MldSubnetCheckFailure = IGMP_ZERO;
                    pIfaceNode->u4MldInvalidIntf = IGMP_ZERO;
#ifdef MULTICAST_SSM_ONLY
            pIfaceNode->u4DroppedASMIncomingPkts = IGMP_ZERO;
#endif

		    i4CurrIfIndex = i4NextIfIndex;
		    if (nmhGetNextIndexMgmdRouterInterfaceTable (i4CurrIfIndex,
					    &i4NextIfIndex,
					    IPVX_ADDR_FMLY_IPV6,
					    &i4AddrType)
				    == SNMP_FAILURE)
		    {
			    u1IsShowAll = FALSE;
		    }

	    }
    }while (u1IsShowAll);

    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : MldCliSetTrace                                      */
/*                                                                         */
/*     Description   : Confiures MLD trace flag                            */
/*                                                                         */
/*     INPUT         : u4Trace - Trace flags                               */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetTrace (tCliHandle CliHandle, INT4 i4TraceStatus)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetTrace.\n");
    UNUSED_PARAM (CliHandle);
    if (i4TraceStatus >= IGMP_ZERO)
    {
        gIgmpConfig.u4MldTrcFlag = (UINT4) i4TraceStatus;
    }
    else
    {
        i4TraceStatus &= MLD_MAX_INT4;
        gIgmpConfig.u4MldTrcFlag &= (~i4TraceStatus);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetTrace.\n");
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : MldCliSetDebug                                      */
/*                                                                         */
/*     Description   : Confiures MLD debug flag                            */
/*                                                                         */
/*     INPUT         : u4Debug - Debug flags                               */
/*                                                                         */
/*     OUTPUT        : CliHandle - Contains error messages                 */
/*                                                                         */
/*     RETURNS       : CLI_SUCCESS/CLI_FAILURE                             */
/*                                                                         */
/***************************************************************************/
INT1
MldCliSetDebug (tCliHandle CliHandle, INT4 i4DebugStatus)
{
    UINT4               u4ErrorCode;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliSetDebug\n");
    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsMgmdMldDebugLevel (&u4ErrorCode, i4DebugStatus) ==
            SNMP_FAILURE)
    {
            return CLI_FAILURE;
    }
    if (nmhSetFsMgmdMldDebugLevel (i4DebugStatus) == SNMP_FAILURE)
    {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliSetDebug\n");
    return CLI_SUCCESS;
}
/*****************************************************************************/
/*     FUNCTION NAME    : MldCliUpdateInterfaceTable                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates or updates a interface entry */
/*                                                                           */
/*     INPUT            : i4IfIndex  - Interface index                       */
/*                        u1Status    - MLD_CREATE/MLD_SET_ACTIVE            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
MldCliUpdateInterfaceTables (tCliHandle CliHandle,
                             INT4 i4IfIndex, UINT1 u1Status,
                             UINT1 *pu1Create, UINT1 u1NotInservice)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;
    INT1                i1Status = SNMP_FAILURE;

    /* Check if the Interface already exists. If the Get
     * function returns Success, for CREATE and WAIT then a interface already
     * exists. Otherwise we need to create an entry.
     */

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldCliUpdateInterfaceTables.\n");
    if (u1Status == MLD_CREATE)
    {
        i1Status = nmhGetMgmdRouterInterfaceStatus (i4IfIndex,
                                                    IPVX_ADDR_FMLY_IPV6,
                                                    &i4RowStatus);
        if (i1Status == SNMP_FAILURE)
        {
            /* No Entry exists with this VlanId */
            if (nmhTestv2MgmdRouterInterfaceStatus (&u4ErrCode, i4IfIndex,
                                                    IPVX_ADDR_FMLY_IPV6,
                                                    MLD_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetMgmdRouterInterfaceStatus (i4IfIndex,
                                                 IPVX_ADDR_FMLY_IPV6,
                                                 MLD_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            /* set it to TRUE to indicate the interface is newly created */
            *pu1Create = MLD_TRUE;
        }
        else
        {
            /* u1NotInservice is used to identify whether the interface node 
             * can be set to not in service for the table objects,
             * only version and Interface status are allowed to put the 
             * row status to not in service */
            if (u1NotInservice == MLD_FALSE)
            {
                return (CLI_SUCCESS);
            }

            /* Entry already exists with this IfIndex, so make it to
             * not-in-service and proceed further.
             */

            if (nmhTestv2MgmdRouterInterfaceStatus (&u4ErrCode, i4IfIndex,
                                                    IPVX_ADDR_FMLY_IPV6,
                                                    MLD_NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetMgmdRouterInterfaceStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                                 MLD_NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
    }
    else
    {
        if (nmhTestv2MgmdRouterInterfaceStatus (&u4ErrCode, i4IfIndex,
                                                IPVX_ADDR_FMLY_IPV6,
                                                MLD_ACTIVE) == SNMP_FAILURE)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliUpdateInterfaceTables.\n");
            return (CLI_FAILURE);
        }

        if (nmhSetMgmdRouterInterfaceStatus (i4IfIndex, IPVX_ADDR_FMLY_IPV6,
                                             MLD_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliUpdateInterfaceTables.\n");
            return (CLI_FAILURE);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldCliUpdateInterfaceTables.\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MldShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the MLD  Configuration     */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
MldShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldShowRunningConfig.\n");
    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();
    
    IssMldShowDebugging (CliHandle);
    CliPrintf (CliHandle, "!\r\n");
    MldShowRunningConfigScalars (CliHandle);
    if (u4Module == ISS_MLD_SHOW_RUNNING_CONFIG)
    {
        MldShowRunningConfigInterface (CliHandle);
    }

    IGMP_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldShowRunningConfig.\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MldShowRunningConfigScalars                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Mld for   */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
MldShowRunningConfigScalars (tCliHandle CliHandle)
{
    INT4                i4ScalarObject = IGMP_ZERO;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldShowRunningConfigScalars.\n");
    nmhGetFsMgmdMldGlobalStatus (&i4ScalarObject);

    if (i4ScalarObject != MLD_DISABLE)
    {
        CliPrintf (CliHandle, "set ipv6 mld enable\r\n");
    }
    else
    {
	return;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldShowRunningConfigScalars.\n");
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MldShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays current interface specific  */
/*                        configurations in OSPF                             */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
MldShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    tIgmpIPvXAddr       CacheGroup;    /*for cache table */
    tIgmpIPvXAddr       SrcListGroup;    /*for src list table */
    tIgmpIPvXAddr       PrevGrpAddr;
    tIgmpIPvXAddr       SrcListSource;
    tIgmpIPvXAddr       PrevSrcAddr;
    UINT4               u4TableObject = IGMP_ZERO;
    UINT4               u4Version = IGMP_ZERO;
    UINT4               u4Port = IGMP_ZERO;
    INT4                i4Value = IGMP_ZERO;
    INT4                i4Result = IGMP_ZERO;
    INT1                ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1InterfaceName = NULL;

    pi1InterfaceName = ai1InterfaceName;

    MEMSET (ai1InterfaceName, IGMP_ZERO, CFA_MAX_PORT_NAME_LENGTH);

    IGMP_IPVX_ADDR_CLEAR (&CacheGroup);
    IGMP_IPVX_ADDR_CLEAR (&SrcListGroup);
    IGMP_IPVX_ADDR_CLEAR (&PrevGrpAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcListSource);
    IGMP_IPVX_ADDR_CLEAR (&PrevSrcAddr);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldShowRunningConfigInterfaceDetails.\n");

    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();

    i4Result = IgmpGetPortFromIfIndex ((UINT4) i4Index, IPVX_ADDR_FMLY_IPV6,
                                       &u4Port);
    if ((i4Result != NETIPV6_FAILURE) &&
        (MLD_PROXY_STATUS () == MLD_ENABLE) &&
        (MldpUtilCheckUpIface (u4Port) == OSIX_SUCCESS))
    {
        CliPrintf (CliHandle, "ipv6 mld router\r\n");
        IGMP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }
    if (nmhValidateIndexInstanceFsMgmdInterfaceTable
        (i4Index, IPVX_ADDR_FMLY_IPV6) == SNMP_SUCCESS)
    {
        nmhGetFsMgmdInterfaceAdminStatus (i4Index, IPVX_ADDR_FMLY_IPV6,
                                          &i4Value);
        CfaCliConfGetIfName ((UINT4) i4Index, pi1InterfaceName);
        CliPrintf (CliHandle, "interface %s\r\n", pi1InterfaceName);
        if (i4Value != MLD_DISABLE)
        {
            CliPrintf (CliHandle, "ipv6 mld router\r\n");
        }
        nmhGetFsMgmdInterfaceFastLeaveStatus (i4Index, IPVX_ADDR_FMLY_IPV6,
                                              &i4Value);
        if (i4Value != MLD_FAST_LEAVE_DISABLE)
        {
            CliPrintf (CliHandle, "ipv6 mld immediate-leave\r\n");
        }

        nmhGetMgmdRouterInterfaceVersion (i4Index, IPVX_ADDR_FMLY_IPV6,
                                          &u4Version);
#ifndef MULTICAST_SSM_ONLY
        if (u4Version != IGMP_TWO)
        {
            u4Version = IGMP_TWO;
            CliPrintf (CliHandle, "ipv6 mld version %d\r\n", u4Version);
	    MLD_DBG1 (MLD_DBG_FLAG, MLD_MGMT_MODULE, IGMP_NAME,
                                  "ipv6 mld version is %d\n", u4Version);
        }
#endif
        nmhGetMgmdRouterInterfaceQueryInterval (i4Index, IPVX_ADDR_FMLY_IPV6,
                                                &u4TableObject);
        if (u4TableObject != MLD_DEFAULT_QUERY_INTERVAL)
        {
            CliPrintf (CliHandle, "ipv6 mld query-interval  %d\r\n",
                       u4TableObject);
	    MLD_DBG1 (MLD_DBG_FLAG, MLD_QRY_MODULE, IgmpGetModuleName(MLD_QRY_MODULE),
                      "ipv6 mld query-interval: %d\n", u4TableObject);
        }

        nmhGetMgmdRouterInterfaceQueryMaxResponseTime (i4Index,
                                                       IPVX_ADDR_FMLY_IPV6,
                                                       &u4TableObject);
        if (u4TableObject != MLD_DEFAULT_MAX_RESP_TIME)
        {
            CliPrintf (CliHandle, "ipv6 mld query-max-response-time  %d\r\n",
                       u4TableObject);
	    MLD_DBG1 (MLD_DBG_FLAG, MLD_QRY_MODULE, IgmpGetModuleName(MLD_QRY_MODULE),
                      "ipv6 mld query-max-response-time: %d\n", u4TableObject);
        }

        nmhGetMgmdRouterInterfaceRobustness (i4Index, IPVX_ADDR_FMLY_IPV6,
                                             &u4TableObject);
        if (u4TableObject != MLD_DEFAULT_ROB_VARIABLE)
        {
            CliPrintf (CliHandle, "ipv6 mld robustness  %d\r\n", u4TableObject);
	    MLD_DBG1 (MLD_DBG_FLAG, MLD_MGMT_MODULE, IGMP_NAME ,
                      "ipv6 mld robustness: %d\n", u4TableObject);
        }

        nmhGetMgmdRouterInterfaceLastMemberQueryInterval (i4Index,
                                                          IPVX_ADDR_FMLY_IPV6,
                                                          &u4TableObject);
        if (u4TableObject != MLD_DEFAULT_LAST_MEMBER_QIVAL)
        {
            CliPrintf (CliHandle, "ipv6 mld last-member-query-interval %d\r\n",
                       u4TableObject);
	    MLD_DBG1 (MLD_DBG_FLAG, MLD_QRY_MODULE, IgmpGetModuleName(MLD_QRY_MODULE),
                      "ipv6 mld last-member-query-interval: %d\n", u4TableObject);
        }
        CliPrintf (CliHandle, "!\r\n");
    }

    IGMP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldShowRunningConfigInterfaceDetails.\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : MldShowRunningConfigInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
MldShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4ScalarObject = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               u1IsShowAll = TRUE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering MldShowRunningConfigInterface.\n");
    
    nmhGetFsMgmdMldGlobalStatus (&i4ScalarObject);

    if (i4ScalarObject != MLD_DISABLE)
    {
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			    "Entering MldShowRunningConfigInterface.\n");
    }
    else
    {
        return CLI_FAILURE;
    }

    if (nmhGetFirstIndexFsMgmdInterfaceTable (&i4NextIfIndex, &i4NextAddrType)
        == SNMP_SUCCESS)
    {
        do
        {

            if (i4NextAddrType == IPVX_ADDR_FMLY_IPV6)
            {

                IGMP_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                MldShowRunningConfigInterfaceDetails (CliHandle, i4NextIfIndex);

                CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
                IGMP_LOCK ();
            }

            i4IfIndex = i4NextIfIndex;
            i4AddrType = i4NextAddrType;

            if (nmhGetNextIndexFsMgmdInterfaceTable
                (i4IfIndex, &i4NextIfIndex, i4AddrType,
                 &i4NextAddrType) == SNMP_FAILURE)
            {
                u1IsShowAll = FALSE;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                u1IsShowAll = FALSE;
            }
        }
        while (u1IsShowAll == TRUE);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting MldShowRunningConfigInterface.\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssMldShowDebugging                                */
/*                                                                           */
/*     DESCRIPTION      : This function prints the MLD debug level           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssMldShowDebugging (tCliHandle CliHandle)
{

    INT4                i4MldTraceFlag = 0;
    INT4                i4MldTrace = 0;
    INT4                i4MldStatus = 0;
    INT4                i4MldDebugFlag = 0;
    INT4                i4MldDebug = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			 "Entering IssMldShowDebugging.\n");
    nmhGetFsMgmdMldGlobalStatus (&i4MldStatus);
    if (i4MldStatus != MLD_DISABLE)
    {
        nmhGetFsMgmdMldDebugLevel (&i4MldDebugFlag);
        nmhGetFsMgmdMldTraceLevel (&i4MldTraceFlag);

        if ((i4MldDebugFlag == 0) && (i4MldTraceFlag == 0))
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting IssMldShowDebugging.\n");
            return;
        }

        if ((i4MldDebugFlag != 0) | (i4MldTraceFlag != 0))
	{
		CliPrintf (CliHandle, "\r\n MLD :");
	}
        if ((i4MldDebugFlag & IGMP_INIT_SHUT_MODULE) != 0)
        {
            CliPrintf (CliHandle, "\r\n debug ipv6 mld init");
            i4MldDebug = 1;
        }

        if ((i4MldDebugFlag & IGMP_GRP_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld grp");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " grp");
            }
        }
        if ((i4MldDebugFlag & IGMP_QRY_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld qry");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " qry");
            }

        }
        if ((i4MldDebugFlag & IGMP_TMR_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld tmr");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " tmr");
            }
        }
        if ((i4MldDebugFlag & IGMP_OS_RES_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld resource");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " os_res");
            }
        }
        if ((i4MldDebugFlag & IGMP_IO_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld i/o");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " i/o");
            }
        }
        if ((i4MldDebugFlag & IGMP_NP_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld np");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " np");
            }
        }
        
        if ((i4MldDebugFlag & IGMP_BUFFER_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld buffer");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " buffer");
            }
        }
	if ((i4MldDebugFlag & IGMP_MGMT_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld mgmt");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " mgmt");
            }
        }

        if ((i4MldDebugFlag & IGMP_ENTRY_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld entry");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " entry");
            }
        }
        if ((i4MldDebugFlag & IGMP_EXIT_MODULE) != 0)
        {
            if (i4MldDebug == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld exit");
                i4MldDebug = 1;
            }
            else
            {
                CliPrintf (CliHandle, " exit");
            }
        }

	if ((i4MldTraceFlag & IGMP_DATA_MODULE) != 0)
        {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld data");
                i4MldTrace = 1;
        }
	if ((i4MldTraceFlag & IGMP_CNTRL_MODULE) != 0)
        {
            if (i4MldTrace == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ipv6 mld  control");
                i4MldTrace = 1;
            }
            else
            {
                CliPrintf (CliHandle, " control");
            }
        }
	if ((i4MldTraceFlag & IGMP_Rx_MODULE) != 0)
        {
            if (i4MldTrace == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp trace Rx");
                i4MldTrace = 1;
            }
            else
            {
                CliPrintf (CliHandle, " Rx");
            }
        }

	if ((i4MldTraceFlag & IGMP_Tx_MODULE) != 0)
        {
            if (i4MldTrace == 0)
            {
                CliPrintf (CliHandle, "\r\n debug ip igmp trace Tx");
                i4MldTrace = 1;
            }
            else
            {
                CliPrintf (CliHandle, " Tx");
            }
        }
        CliPrintf (CliHandle, "\r\n!\r\n");
    }
    else
    {
	return;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
			 "Exiting IssMldShowDebugging.\n");
    return;
}

#endif
