/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsipif.c,v 1.26 2017/02/06 10:45:27 siva Exp $
 *
 * Description:  This file contains functions some stub functions 
 *                  as functions provided by futureIP. This functions
 *                 are required in case of third party IP.         
 *
 ********************************************************************/

#include "igmpinc.h"
#include "stdigmcli.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE;
#endif
#ifdef IP_VX_WANTED
/**************************************************************************
 * Function Name          : IpifJoinMcastGroup
 *
 * Description            : This routine Joins for a particular multicast 
 *                          address in a particular interface.               
 * Global Variables 
 * Referred               : gSockRegister
 *
 * Gobal Variables 
 * Modified               : None
 *              
 * Input (s)              : None
 *
 * Output (s)             : None
 *
 * Returns                : SUCCESS or FAILURE
 ***************************************************************************/
INT4
IpifJoinMcastGroup (tIgmpIPvXAddr * pMcastGroupAddr, UINT4 u4IfIndex,
                    UINT1 u1ProtocolId)
{
    tCRU_INTERFACE      IfId;
    UNUSED_PARAM (u1ProtocolId);
    if (pMcastGroupAddr == NULL)
    {
        return FAILURE;
    }
    return SUCCESS;
}

 /*************************************************************************
 * Function Name          : IpifLeaveMcastGroup
 *
 * Description            : This routine leaves  for a particular multicast 
 *                          address in a particular interface.               
 * Global Variables 
 * Referred               : gSockRegister
 *
 * Gobal Variables 
 * Modified               : None
 *              
 * Input (s)              : None
 *
 * Output (s)             : None
 *
 * Returns                : SUCCESS or FAILURE
 **************************************************************************/

INT4
IpifLeaveMcastGroup (tIgmpIPvXAddr * pMcastGroupAddr, UINT4 u4IfIndex,
                     UINT1 u1ProtocolId)
{
    tCRU_INTERFACE      IfId;
    UNUSED_PARAM (u1ProtocolId);
    if (pMcastGroupAddr == NULL)
    {
        return FAILURE;
    }
    return SUCCESS;
}
#endif /* IP_VX_WANTED */

/*********************************************************************/
/* Function           : IgmpProcessIfChange                          */
/*                                                                   */
/* Description        : This functions handles i/f changes           */
/*                                                                   */
/* Input(s)           : IP buffer pointer                            */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
IgmpProcessIfChange (tIgmpIfStatusInfo IfStatusInfo)
{
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tNetIpv4IfInfo      IpIfRecord;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    INT4                i4RetValue = 0;
    UINT4               u4Port = IGMP_ZERO;
    UINT4               u4CfaIfIndex = IGMP_ZERO;
    UINT1               u1VerFlag = IGMP_ZERO;
    UINT1               u1MsgType = IGMP_ZERO;
    UINT1               u1AddrType = IPVX_ADDR_FMLY_IPV4;

    UNUSED_PARAM (i4RetValue);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                                "Entering IgmpProcessIfChange ()\r \n");

    /* Get the message type i.e Ucast Rt change or If status change */
    u1MsgType = IfStatusInfo.u1MsgType;
    u4Port = IfStatusInfo.u4IfIndex;
    u4CfaIfIndex = IfStatusInfo.u4CfaIfIndex;
    u1AddrType = IfStatusInfo.u1AddrType;

    pIfaceNode = IGMP_GET_IF_NODE (u4Port, u1AddrType);

    if (pIfaceNode == NULL)
    {
        IGMP_DBG1(IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
			"Received If Status change on interface %d\n", u4Port);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpProcessIfChange ()\r \n");
	return;
    }

    if(u1MsgType == IGMP_SEC_IFACE_DELETE)
    {
        IGMP_DBG1(IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
			"Received Secondary IP Delete on interface %d\n", u4Port);
		IgmpHandleSecondaryIPDelete(pIfaceNode, IfStatusInfo.u4SecIPAddr, IfStatusInfo.u4SecIPMask);
		return;
	}		

    if (u1MsgType == IGMP_DELETE_INTERFACE)
    {
        /* If IGMP Proxy is enabled and interface is configured as upstream 
         * interface delete the upstream interface information from the 
         * Proxy database */

        if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
            (IgpUtilCheckUpIface (pIfaceNode->u4IfIndex) == IGMP_TRUE))
        {
            if (IgpUpIfDeleteUpIfaceEntry (pIfaceNode->u4IfIndex, IGMP_TRUE)
                != IGMP_SUCCESS)
            {
		  IGMP_DBG1(IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
		    "Deletion of Upstream interface %d failed \r\n", pIfaceNode->u4IfIndex);
		  MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                    "Exiting IgmpProcessIfChange ()\r \n");
		  return;
            }
        }
        /* clear querier related stuff */
        IgmpHdlTransitionToNonQuerier (pIfaceNode);

        /* Clear all membership information in the interface node */
        MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

        IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfaceNode;

        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) & IgmpGrpEntry, NULL);
        while (pGrp != NULL)
        {
            IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &(pGrp->u4Group));
            if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
            {
                IGMP_DBG2(IGMP_DBG_FLAG, IGMP_GRP_MODULE, IgmpGetModuleName(IGMP_GRP_MODULE),
                                "Group entry %d  doesn't belog to this %d interface index \r\n", 
				pGrp->u4IfIndex, pIfaceNode->u4IfIndex );
                IGMP_TRC_ARG2(IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                        "Group entry %d  doesn't belog to this %d interface index \r\n", 
			pGrp->u4IfIndex, pIfaceNode->u4IfIndex );
		/* Group entry doesn't belong to 
                 * specified interface index */
                break;
            }
            if (pGrp->pIfNode->u1AddrType != pIfaceNode->u1AddrType)
            {
                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup, (tRBElem *) pGrp, NULL);
                continue;
            }

            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
            while (pSrc != NULL)
            {
                pNextSrc =
                    (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);
                
                #ifdef SNMP_3_WANTED
                MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
                SnmpNotifyInfo.pu4ObjectId = IgmpSrcListStatus;
                SnmpNotifyInfo.u4OidLen =
                    sizeof (IgmpSrcListStatus) / sizeof (UINT4);
                SnmpNotifyInfo.u4SeqNum = 0;
                SnmpNotifyInfo.u1RowStatus = TRUE;
                SnmpNotifyInfo.pLockPointer = IgmpLock;
                SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
                SnmpNotifyInfo.u4Indices = 3;
                SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %i %p %i",
                                  pGrp->u4Group, u4CfaIfIndex,
                                  pSrc->u4HostAddress, IGMP_DESTROY));
                #endif
	    	pSrc->u1DelSyncFlag = IGMP_FALSE;
                IgmpDeleteSource (pGrp, pSrc);
                pSrc = pNextSrc;
            }

            if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
            {
                u1VerFlag = IGMP_VERSION_2;
		IGMP_DBG1(IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                                "IGMP version is %d \r\n",u1VerFlag);
            }
            else
            {
                u1VerFlag = IGMP_VERSION_3;
		IGMP_DBG1(IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                                "IGMP version is %d \r\n",u1VerFlag);
            }

            u1VerFlag |= IGMP_V3REP_SEND_FLAG;

            MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
            SnmpNotifyInfo.pu4ObjectId = IgmpCacheStatus;
            SnmpNotifyInfo.u4OidLen = sizeof (IgmpCacheStatus) / sizeof (UINT4);
            SnmpNotifyInfo.u4SeqNum = 0;
            SnmpNotifyInfo.u1RowStatus = TRUE;
            SnmpNotifyInfo.pLockPointer = IgmpLock;
            SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
            SnmpNotifyInfo.u4Indices = 2;
            SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %i %i",
                              pGrp->u4Group, u4CfaIfIndex, IGMP_DESTROY));
	    pGrp->u1DelSyncFlag = IGMP_FALSE;
            IgmpDeleteGroup (pIfaceNode, pGrp, pGrp->u1StaticFlg);

            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) & IgmpGrpEntry,
                                                 NULL);
        }

/* For IGMP stack on Linux ip. Not for ISS */
#if defined (LNXIP4_WANTED) && !defined (ISS_WANTED)
        /* Delete the interface from the virtual interface table */
        if (IgmpSetMcastSocketOption (pIfaceNode, IGMP_DOWN) == IGMP_FAILURE)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG,
                      IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                      "Disabling multicast socket option on interafce %d"
		      " failed \n",pIfaceNode);
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpProcessIfChange ()\r \n");
	    return;
        }
#endif
        /* For MLD stack on Linux ip. Not for ISS */
#if defined (LNXIP6_WANTED) && !defined (ISS_WANTED)
        /* Delete the interface from the virtual interface table */
        if (MldSetMcastSocketOption (pIfaceNode->u4IfIndex, IGMP_DOWN) ==
            IGMP_FAILURE)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG,
                      IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                      "Disabling multicast socket option on interface %d"
		      "failed \n",pIfaceNode->u4IfIndex);
            return;
        }
#endif

#ifdef MFWD_WANTED
        if (gIgmpConfig.i4ProxyStatus == IGMP_ENABLE)
        {
            IgpMfwdDelInterface (pIfaceNode->u4IfIndex);
        }
#endif
#ifdef SNMP_3_WANTED
        MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
        SnmpNotifyInfo.pu4ObjectId = IgmpInterfaceStatus;
        SnmpNotifyInfo.u4OidLen = sizeof (IgmpInterfaceStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = IgmpLock;
        SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4CfaIfIndex, IGMP_DESTROY));
#endif
        IgmpDeleteInterfaceNode (pIfaceNode);
        return;
    }

    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetValue = IGMP_IP_GET_IF_CFG_RECORD (u4Port, &IpIfRecord);
        IGMP_IPVX_ADDR_INIT_IPV4 (pIfaceNode->u4IfAddr,
                                  IGMP_IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &(IpIfRecord.u4Addr));
        IPV4_MASK_TO_MASKLENX (pIfaceNode->u4IfMask, IpIfRecord.u4NetMask);
    }
    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MLD_IP6_GET_IF_CONFIG_RECORD (u4CfaIfIndex, &NetIpv6IfInfo);
        IGMP_IPVX_ADDR_INIT_IPV6 (pIfaceNode->u4IfAddr, IPVX_ADDR_FMLY_IPV6,
                                  (UINT1 *) &(NetIpv6IfInfo.Ip6Addr.u1_addr));
    }

    if (pIfaceNode->u1OperStatus != IfStatusInfo.u1IfStatus)
    {
        pIfaceNode->u1OperStatus = IfStatusInfo.u1IfStatus;

        if (pIfaceNode->u1AdminStatus == IGMP_UP)
        {
            IgmpProcessIfStatChg (pIfaceNode, IfStatusInfo.u1IfStatus);
        }
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpProcessIfChange ()\r \n");
    return;
}

/*********************************************************************/
/* Function           : IgmpProcessIfStatChg                         */
/*                                                                   */
/* Description        : This functions call appropriate i/f handlers */
/*                    : based on i/f status recv.                    */
/*                                                                   */
/* Input(s)           : IP buffer pointer                            */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
IgmpProcessIfStatChg (tIgmpIface * pIfNode, UINT1 u1IfStatus)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                                "Entering IgmpProcessIfStatChg ()\r \n");

    pIfNode->u1IfStatus = u1IfStatus;

    /* When the Interface is made up */
    if (u1IfStatus == IGMP_IFACE_UP)
    {
	IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                   "Interface status of index  %d is up\n",pIfNode->u4IfIndex);
        IgmpIfUpHdlr (pIfNode);
    }

    /* When the interface goes down */
    if (u1IfStatus == IGMP_IFACE_DOWN)
    {
	IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                   "Interface status of index  %d is down\n",pIfNode->u4IfIndex);
        IgmpIfDownHdlr (pIfNode);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpProcessIfStatChg ()\r \n");
    return;
}

/*********************************************************************/
/* Function           : IgmpIfUpHdlr                                 */
/*                                                                   */
/* Description        : This function handles i/f up status event    */
/*                                                                   */
/* Input(s)           : IP buffer pointer                            */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
IgmpIfUpHdlr (tIgmpIface * pIfaceNode)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                                "Entering IgmpIfUpHdlr ()\r \n");
    IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_UP);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpIfUpHdlr ()\r \n");
    return;
}

/*********************************************************************/
/* Function           : IgmpIfDownHdlr                               */
/*                                                                   */
/* Description        : This function handles i/f down status event  */
/*                                                                   */
/* Input(s)           : IP buffer pointer                            */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
IgmpIfDownHdlr (tIgmpIface * pIfaceNode)
{

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                                "Entering IgmpIfDownHdlr ()\r \n");

    IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_DOWN);

     MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpIfDownHdlr ()\r \n");
    return;
}


/*********************************************************************/
/* Function           : IgmpHandleSecondaryIPDelete                  */
/*                                                                   */
/* Description        : This function handles Secondary IP Delete    */
/*                                                                   */
/* Input(s)           : pIfaceNode - pointer to the interface node   */
/*                      u4SecIPAddr - sec IP addr being deleted      */
/*                      u4SecIPMask - subnet mask of the sec IP addr */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/

VOID
IgmpHandleSecondaryIPDelete (tIgmpIface *pIfaceNode, UINT4 u4SecIPAddr, UINT4 u4SecIPMask)
{
	tIgmpGroup         *pGrp = NULL;
	tIgmpGroup         *pNextGrp = NULL;
	tIgmpSource        *pNextSrc = NULL;
	tIgmpSource        *pSrc = NULL;
	tIgmpGroup          IgmpGrpEntry;
	UINT1               u1ExpireGroup = IGMP_FALSE;
	UINT1               u1ExpireSource = IGMP_FALSE;
	UINT4               u4SecNetwork = 0;
	UINT4               u4RepAddrRB = 0;
	tIgmpSrcReporter        *pDelReporter = NULL;
	tIgmpSrcReporter        *pNxtSecRepNode = NULL;
	tIgmpSrcReporter        SecIPReporter;
	tIgmpIPvXAddr      SrcAddr;

    MEMSET (&SrcAddr, 0, sizeof (tIgmpIPvXAddr));
    MEMSET (&SecIPReporter, 0, sizeof (tIgmpSrcReporter));
	u4SecNetwork = (u4SecIPAddr) & (u4SecIPMask);
	IGMP_IPVX_ADDR_INIT_IPV4 (SecIPReporter.u4SrcRepAddr, IPVX_ADDR_FMLY_IPV4, (UINT1 *) &u4SecNetwork);

    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));
	IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
	IgmpGrpEntry.pIfNode = pIfaceNode;

	pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
			(tRBElem *) & IgmpGrpEntry, NULL);
	while (pGrp != NULL)
	{
		pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
				(tRBElem *) pGrp,
				NULL); 		
		if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
		{
			IGMP_DBG2(IGMP_DBG_FLAG, IGMP_GRP_MODULE, IgmpGetModuleName(IGMP_GRP_MODULE),
					"Group entry %d  doesn't belong to this %d interface index \r\n", 
					pGrp->u4IfIndex, pIfaceNode->u4IfIndex );
			IGMP_TRC_ARG2(IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
					IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
					"Group entry %d  doesn't belong to this %d interface index \r\n", 
					pGrp->u4IfIndex, pIfaceNode->u4IfIndex );
			/* Group entry doesn't belong to 
			 * specified interface index or address type*/
			break;
		}
		
		if(pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
		{
			pGrp = pNextGrp; 
			continue;
		}
		pDelReporter = (tIgmpSrcReporter *) RBTreeGetNext(pGrp->GrpReporterTree, 
				(tRBElem *)&SecIPReporter, NULL);
		while(pDelReporter != NULL)
		{
			IGMP_IP_COPY_FROM_IPVX (&u4RepAddrRB, pDelReporter->u4SrcRepAddr, IGMP_IPVX_ADDR_FMLY_IPV4);
			if(((u4RepAddrRB)&(u4SecIPMask)) != (u4SecNetwork))
			{
				break;
			}
			else
			{
				u1ExpireGroup = IGMP_TRUE;
			}
			pNxtSecRepNode = RBTreeGetNext(pGrp->GrpReporterTree, (tRBElem *) pDelReporter, NULL);
			RBTreeRemove(pGrp->GrpReporterTree, (tRBElem *) pDelReporter);
			IGMP_DBG2 (IGMP_DBG_FLAG,
					IGMP_INIT_SHUT_MODULE, IGMP_NAME,
					"Reporter %s removed for Group %s \r\n",
					IgmpPrintIPvxAddress (pDelReporter->u4SrcRepAddr),
					IgmpPrintIPvxAddress (pDelReporter->GrpAddr));
			IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId, (UINT1 *) pDelReporter);
			pDelReporter = pNxtSecRepNode;
		}
		pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
		if(u1ExpireGroup == IGMP_TRUE)
		{
			if((pSrc == NULL)&&(IGMP_GET_NODE_STATUS() == RM_ACTIVE))
			{
                    IgmpSendGroupSpecificQuery (pIfaceNode, pGrp);
                    IgmpReStartGrpTmrToLmqTmr (pIfaceNode, pGrp);
			}
			u1ExpireGroup = IGMP_FALSE;
			pDelReporter = NULL;
		}
		while (pSrc != NULL)
		{
			pNextSrc =
				(tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);

			pDelReporter = (tIgmpSrcReporter *) RBTreeGetNext(pSrc->SrcReporterTree, 
					(tRBElem *)&SecIPReporter, NULL);
			while(pDelReporter != NULL)
			{
				IGMP_IP_COPY_FROM_IPVX (&u4RepAddrRB, pDelReporter->u4SrcRepAddr, IGMP_IPVX_ADDR_FMLY_IPV4);
				if(((u4RepAddrRB)&(u4SecIPMask)) != (u4SecNetwork))
				{
					break;
				}
				else
				{
					u1ExpireSource = IGMP_TRUE;
				}
				pNxtSecRepNode = RBTreeGetNext(pSrc->SrcReporterTree, (tRBElem *) pDelReporter, NULL);
				RBTreeRemove(pSrc->SrcReporterTree, (tRBElem *) pDelReporter);
				IGMP_DBG3 (IGMP_DBG_FLAG,
						IGMP_INIT_SHUT_MODULE, IGMP_NAME,
						"Reporter %s removed for Source %s for Group %s \r\n",
						IgmpPrintIPvxAddress (pDelReporter->u4SrcRepAddr),
						IgmpPrintIPvxAddress (pDelReporter->SourceAddr),
						IgmpPrintIPvxAddress (pDelReporter->GrpAddr));
				IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId, (UINT1 *) pDelReporter);
				pDelReporter = pNxtSecRepNode;
			}
			if(u1ExpireSource == IGMP_TRUE)
			{
				IGMP_IPVX_ADDR_COPY (&SrcAddr, &(pSrc->u4SrcAddress));
                if (IGMP_GET_NODE_STATUS() == RM_ACTIVE)
                {
                    IgmpSendGroupAndSourceSpecificQuery(pIfaceNode, pGrp, IGMP_ONE, &SrcAddr);
                    IgmpReStartSrcTmrToLmqTmr(pIfaceNode, pSrc);
                }
				u1ExpireSource = IGMP_FALSE;
			}
			pSrc = pNextSrc;
		}

		pGrp = pNextGrp; 
	}
	return;
}
#if defined (IP6_WANTED) || defined (LNXIP6_WANTED)
/*********************************************************************/
/* Function           : MldProcessIpAddrChange                               */
/*                                                                   */
/* Description        : This function handles i/f addr status event  */
/*                                                                   */
/* Input(s)           : Addr info                                    */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
MldProcessIpAddrChange (tMldIpv6AddrInfo MldIpv6AddrInfo)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = IGMP_ZERO;
    UINT1               u1MsgType = IGMP_ZERO;
    UINT1               u1AddrType = IPVX_ADDR_FMLY_IPV6;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering MldProcessIpAddrChange ()\r \n");

    u1MsgType = MldIpv6AddrInfo.u1MsgType;
    u4Port = MldIpv6AddrInfo.u4IfIndex;

    pIfaceNode = MLD_GET_IF_NODE (u4Port, u1AddrType);

    if (pIfaceNode == NULL)
    {
        MLD_TRC_ARG1 (MLD_TRC_FLAG, ALL_FAILURE_TRC, "MLD",
                    "MldProcessIpAddrChange:MLD not enabled on interface %d\n",
                    u4Port);

        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting MldProcessIpAddrChange ()\r \n");
        return;
    }
    if (u1MsgType == MLD_ADDRCHG_INTERFACE_ADD)
    {
        IPVX_ADDR_COPY (&(pIfaceNode->u4IfAddr), &(MldIpv6AddrInfo.Ip6Addr));
        pIfaceNode->u4IfAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
        if (pIfaceNode->pIfaceQry->u1Querier == TRUE)
        {
            IGMP_IPVX_ADDR_COPY (&(pIfaceNode->pIfaceQry->u4QuerierAddr), &(pIfaceNode->u4IfAddr));
	    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
			    &(pIfaceNode->pIfaceQry->QuerierDbNode));
	    IgmpRedSyncDynInfo ();
        }
    }
    if (u1MsgType == MLD_ADDRCHG_INTERFACE_DEL)
    {
        if (IPVX_ADDR_COMPARE (pIfaceNode->u4IfAddr, &(MldIpv6AddrInfo.Ip6Addr))
            != 0)
        {
            MLD_TRC (MLD_TRC_FLAG, MLD_CONTROL_PLANE_MODULE | MLD_ALL_MODULES,
                     "MLD", "No change in interface address.\n");

            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting MldProcessIpAddrChange ()\r \n");

            return;
        }
        MEMSET (&pIfaceNode->u4IfAddr, 0, sizeof (tIPvXAddr));
        if (pIfaceNode->pIfaceQry->u1Querier == TRUE)
        {
            MEMSET (&(pIfaceNode->pIfaceQry->u4QuerierAddr), 0, sizeof (tIgmpIPvXAddr));
	    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
			    &(pIfaceNode->pIfaceQry->QuerierDbNode));
	    IgmpRedSyncDynInfo ();
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting MldProcessIpAddrChange ()\r \n");

    return;
}
#endif
