/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdigset.c,v 1.22 2011/10/13 10:05:01 siva Exp $
 *
 * Description:This file contains Low level routines  for stdigmp.mib  
 *
 *******************************************************************/

#include "include.h"
#include "stdigcon.h"
#include "stdigogi.h"
#include "midconst.h"
#include "igmpinc.h"
#include "stdmgmcli.h"
#include "stdigmcli.h"
/* LOW LEVEL Routines for Table : IgmpInterfaceTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIgmpInterfaceQueryInterval
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceQueryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpInterfaceQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                  UINT4 u4SetValIgmpInterfaceQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceQueryInterval (i4IgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      u4SetValIgmpInterfaceQueryInterval);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceQueryInterval,
                                sizeof (MgmdRouterInterfaceQueryInterval) /
                                sizeof (UINT4), i4IgmpInterfaceIfIndex,
                                'u', &u4SetValIgmpInterfaceQueryInterval,
                                FALSE, IPVX_ADDR_FMLY_IPV4);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetIgmpInterfaceStatus
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpInterfaceStatus (INT4 i4IgmpInterfaceIfIndex,
                           INT4 i4SetValIgmpInterfaceStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceStatus (i4IgmpInterfaceIfIndex,
                                               IPVX_ADDR_FMLY_IPV4,
                                               i4SetValIgmpInterfaceStatus);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceStatus,
                                sizeof (MgmdRouterInterfaceStatus) /
                                sizeof (UINT4), i4IgmpInterfaceIfIndex,
                                'i', &i4SetValIgmpInterfaceStatus,
                                TRUE, IPVX_ADDR_FMLY_IPV4);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetIgmpInterfaceVersion
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpInterfaceVersion (INT4 i4IgmpInterfaceIfIndex,
                            UINT4 u4SetValIgmpInterfaceVersion)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceVersion (i4IgmpInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                u4SetValIgmpInterfaceVersion);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceVersion,
                                sizeof (MgmdRouterInterfaceVersion) /
                                sizeof (UINT4), i4IgmpInterfaceIfIndex,
                                'u', &u4SetValIgmpInterfaceVersion,
                                FALSE, IPVX_ADDR_FMLY_IPV4);
    }

    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetIgmpInterfaceQueryMaxResponseTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceQueryMaxResponseTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpInterfaceQueryMaxResponseTime (INT4 i4IgmpInterfaceIfIndex,
                                         UINT4
                                         u4SetValIgmpInterfaceQueryMaxResponseTime)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceQueryMaxResponseTime
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         u4SetValIgmpInterfaceQueryMaxResponseTime);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceQueryMaxResponseTime,
                                sizeof (MgmdRouterInterfaceQueryMaxResponseTime)
                                / sizeof (UINT4), i4IgmpInterfaceIfIndex, 'u',
                                &u4SetValIgmpInterfaceQueryMaxResponseTime,
                                FALSE, IPVX_ADDR_FMLY_IPV4);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetIgmpInterfaceProxyIfIndex
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceProxyIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpInterfaceProxyIfIndex (INT4 i4IgmpInterfaceIfIndex,
                                 INT4 i4SetValIgmpInterfaceProxyIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceProxyIfIndex (i4IgmpInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     i4SetValIgmpInterfaceProxyIfIndex);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceProxyIfIndex,
                                sizeof (MgmdRouterInterfaceProxyIfIndex) /
                                sizeof (UINT4), i4IgmpInterfaceIfIndex,
                                'i', &i4SetValIgmpInterfaceProxyIfIndex,
                                FALSE, IPVX_ADDR_FMLY_IPV4);
    }

    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetIgmpInterfaceRobustness
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceRobustness
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpInterfaceRobustness (INT4 i4IgmpInterfaceIfIndex,
                               UINT4 u4SetValIgmpInterfaceRobustness)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceRobustness (i4IgmpInterfaceIfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   u4SetValIgmpInterfaceRobustness);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceRobustness,
                                sizeof (MgmdRouterInterfaceRobustness) /
                                sizeof (UINT4), i4IgmpInterfaceIfIndex,
                                'u', &u4SetValIgmpInterfaceRobustness,
                                FALSE, IPVX_ADDR_FMLY_IPV4);
    }

    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetIgmpInterfaceLastMembQueryIntvl
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceLastMembQueryIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpInterfaceLastMembQueryIntvl (INT4 i4IgmpInterfaceIfIndex,
                                       UINT4
                                       u4SetValIgmpInterfaceLastMembQueryIntvl)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetIgmpInterfaceLastMembQueryIntvl
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         u4SetValIgmpInterfaceLastMembQueryIntvl);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIfTbl (MgmdRouterInterfaceLastMemberQueryInterval,
                                sizeof
                                (MgmdRouterInterfaceLastMemberQueryInterval) /
                                sizeof (UINT4), i4IgmpInterfaceIfIndex, 'u',
                                &u4SetValIgmpInterfaceLastMembQueryIntvl, FALSE,
                                IPVX_ADDR_FMLY_IPV4);
    }

    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIgmpCacheSelf
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                setValIgmpCacheSelf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpCacheSelf (UINT4 u4IgmpCacheAddress,
                     INT4 i4IgmpCacheIfIndex, INT4 i4SetValIgmpCacheSelf)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhSetIgmpCacheSelf (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                                         i4IgmpCacheIfIndex,
                                         i4SetValIgmpCacheSelf);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIgmpCacheTbl (IgmpCacheSelf, sizeof (IgmpCacheSelf) /
                                       sizeof (UINT4), u4IgmpCacheAddress,
                                       i4IgmpCacheIfIndex,
                                       i4SetValIgmpCacheSelf, FALSE);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetIgmpCacheStatus
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                setValIgmpCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpCacheStatus (UINT4 u4IgmpCacheAddress,
                       INT4 i4IgmpCacheIfIndex, INT4 i4SetValIgmpCacheStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhSetIgmpCacheStatus (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                                           i4IgmpCacheIfIndex,
                                           i4SetValIgmpCacheStatus);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIgmpCacheTbl (IgmpCacheStatus,
                                       sizeof (IgmpCacheStatus) /
                                       sizeof (UINT4), u4IgmpCacheAddress,
                                       i4IgmpCacheIfIndex,
                                       i4SetValIgmpCacheStatus, TRUE);
    }
    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIgmpSrcListStatus
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress

                The Object 
                setValIgmpSrcListStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIgmpSrcListStatus (UINT4 u4IgmpSrcListAddress,
                         INT4 i4IgmpSrcListIfIndex,
                         UINT4 u4IgmpSrcListHostAddress,
                         INT4 i4SetValIgmpSrcListStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddrX;
    tIgmpIPvXAddr       SrcListHostAddrX;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddrX);
    IGMP_IPVX_ADDR_CLEAR (&SrcListHostAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (SrcListAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (SrcListHostAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListHostAddress);

    i1Status =
        IgmpMgmtUtilNmhSetIgmpSrcListStatus (SrcListAddrX, IPVX_ADDR_FMLY_IPV4,
                                             i4IgmpSrcListIfIndex,
                                             SrcListHostAddrX,
                                             i4SetValIgmpSrcListStatus);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIgmpSrcTbl (IgmpSrcListStatus,
                                     sizeof (IgmpSrcListStatus) /
                                     sizeof (UINT4), u4IgmpSrcListAddress,
                                     i4IgmpSrcListIfIndex,
                                     u4IgmpSrcListHostAddress,
                                     i4SetValIgmpSrcListStatus, TRUE);
    }
    return i1Status;
}
