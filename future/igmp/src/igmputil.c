/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmputil.c,v 1.42 2017/05/30 11:13:43 siva Exp $
 *
 * Description:This file contains Low level routines  for stdigmp.mib  
 *
 *******************************************************************/

#include "include.h"
#include "stdigcon.h"
#include "stdigogi.h"
#include "midconst.h"
#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;
#endif
CHR1                gacIgmpAddrPrintBuf[3][50];

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpUtilUpdateMrps                            */
/*                                                                         */
/*     Description   :     This function updates the MRPs                  */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    GrpSrcInfo - variable of tGrpNoteMsg             */
/*                        pGrp - pointer to  tIgmpGroup                    */
/*                        u1JoinLeaveFlag - Join/Leave                     */
/*                        u1FilterMode - Include/Exclude                   */
/*                        u1NumOfSrcs - No of sources being reported.      */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpUtilCreateGroupEntry                    **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/

VOID
IgmpUtilUpdateMrps (tIgmpGroup * pGrp, UINT1 u1JoinLeaveFlag)
{
    UINT4               u4Count = IGMP_ZERO;
    tGrpNoteMsg         GrpSrcInfo;
    tIgmpSource        *pMrpSrcNode = NULL;
    tTMO_SLL_NODE      *pPrev = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tIgmpSource        *pTempSrc = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the funtion IgmpUtilUpdateMrps.\n");

    IgmpFillMem (&GrpSrcInfo, IGMP_ZERO, sizeof (tGrpNoteMsg));

    /* Updating MRP's */
    GrpSrcInfo.u4IfIndex = pGrp->pIfNode->u4IfIndex;
    IGMP_IP_COPY_FROM_IPVX (&(GrpSrcInfo.u4GrpAddr), pGrp->u4Group,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    GrpSrcInfo.u1Flag = u1JoinLeaveFlag;

    if (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) == IGMP_ZERO)
    {
        for (u4Count = IGMP_ZERO; u4Count < IGMP_MAX_REGISTER; u4Count++)
        {
            if (gaIgmpRegister[u4Count].u1ProtId != IGMP_ZERO)
            {
                gaIgmpRegister[u4Count].pLJNoteFuncPtr (GrpSrcInfo);
                IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                           "Informed MRP abt group: %s \n",
                           IgmpPrintIPvxAddress (pGrp->u4Group));
            }
        }
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "Exiting the funtion IgmpUtilUpdateMrps\n");
        return;
    }

    /* fill the sources for MRP updation */
    while ((pMrpSrcNode = (tIgmpSource *)
            TMO_SLL_First (&(pGrp->SrcListForMrpUpdate))) != NULL)
    {
        /* source 0.0.0.0 should not be notified to MRP */
        if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero,
                                    pMrpSrcNode->u4SrcAddress) != IGMP_ZERO)
        {
            IGMP_IP_COPY_FROM_IPVX (&(GrpSrcInfo.au4SrcAddr[u4Count]),
                                    (pMrpSrcNode->u4SrcAddress),
                                    IGMP_IPVX_ADDR_FMLY_IPV4);
            /* Update SSM Mapped status for each source */
            if (pMrpSrcNode->u1SrcSSMMapped & IGMP_SRC_MAPPED)
            {
                GrpSrcInfo.u1SrcSSMMapped |= (IGMP_ONE << u4Count);
            }
            u4Count++;
        }

        /* After this source is updated then it can become part of the
         * main source list of the group node.
         */
        TMO_SLL_Delete (&(pGrp->SrcListForMrpUpdate), &pMrpSrcNode->Link);
        /* Sort the sources and add into the list */
        TMO_SLL_Scan (&pGrp->srcList, pLstNode, tTMO_SLL_NODE *)
        {
            pTempSrc = (tIgmpSource *) pLstNode;
            if (IGMP_IPVX_ADDR_COMPARE
                (pTempSrc->u4SrcAddress, pMrpSrcNode->u4SrcAddress) < IGMP_ZERO)
            {
                pPrev = pLstNode;
            }
            else if (IGMP_IPVX_ADDR_COMPARE
                     (pTempSrc->u4SrcAddress,
                      pMrpSrcNode->u4SrcAddress) > IGMP_ZERO)
            {
                break;
            }
        }
        TMO_SLL_Insert (&(pGrp->srcList), pPrev, &(pMrpSrcNode->Link));
        pMrpSrcNode->u1UpdatedToMrp = IGMP_TRUE;

        if ((u4Count == IGMP_MAX_SOURCES) ||
            (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) == IGMP_ZERO))
        {
            GrpSrcInfo.u1NumOfSrcs = (UINT1) u4Count;
            for (u4Count = IGMP_ZERO; u4Count < IGMP_MAX_REGISTER; u4Count++)
            {
                if (gaIgmpRegister[u4Count].u1Flag == IGMP_UP)
                {
                    if (gaIgmpRegister[u4Count].pLJNoteFuncPtr)
                    {
                        gaIgmpRegister[u4Count].pLJNoteFuncPtr (GrpSrcInfo);
                        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_GRP_MODULE,
                                   IGMP_NAME,
                                   "Informed MRP abt group: %s and source : %s \n",
                                   IgmpPrintIPvxAddress (pGrp->u4Group),
                                   IgmpPrintIPvxAddress (pMrpSrcNode->
                                                         u4SrcAddress));
                    }
                }
            }
            u4Count = IGMP_ZERO;
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the funtion IgmpUtilUpdateMrps.\n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpToExclude                                   */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpToExclude (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    tIgmpIPvXAddr      *pOldSrcSet = gpu4SrcSetArray;
    UINT4               u4OldSrcCount = IGMP_ZERO;
    UINT4               u4Index = IGMP_ZERO;
    tIgmpIPvXAddr       SrcAddrX;
    UINT1              *pu1Sources = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;
    UINT4               u4NumSrc = IGMP_ZERO;
    tIgmpIPvXAddr      *pSrcSet = NULL;
    UINT1               u1SrcDelFlag = IGMP_FALSE;
    UINT4               u4SrcAddrPos = 0;
    UINT4               u4Count = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the funtion IgmpToExclude.\n");

    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    pGrp->u1FilterMode = IGMP_FMODE_EXCLUDE;
    if (u4NoOfSources == IGMP_ZERO)
    {
        /* Updating MRPs due to state change */
        if (IGMP_PROXY_STATUS () == IGMP_DISABLE)
        {
            IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                           "Updated to MRPs.\n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,
                      IgmpTrcGetModuleName (IGMP_DATA_MODULE),
                      "Updated to MRPs.\n");
        }
        if (IgmpAddV1V2Reporter (pGrp, u4Reporter) == IGMP_NOT_OK)
        {
            IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Reporter (%s) addition failed for Group %s added in i/f %d \n",
                       IgmpPrintIPvxAddress (u4Reporter),
                       IgmpPrintIPvxAddress (pGrp->u4Group),
                       pIfNode->u4IfIndex);
        }
    }

    /* Copy all the source records to the pOldSrcSet */
    TMO_SLL_Scan (&pGrp->srcList, pSrc, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_COPY (&(pOldSrcSet[u4Index++]), &(pSrc->u4SrcAddress));
    }
    u4OldSrcCount = TMO_SLL_Count (&(pGrp->srcList));

    for (u4Index = IGMP_ZERO; u4Index < u4NoOfSources; u4Index++)
    {
        IGMP_IPVX_ADDR_CLEAR (&SrcAddrX);

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                                      (pu1Sources +
                                       (IPVX_IPV4_ADDR_LEN * u4Index)));
        }
        else
        {
            u4SrcAddrPos = (UINT2) (u4Index * (UINT1) IPVX_IPV6_ADDR_LEN);
            IPVX_ADDR_INIT_FROMV6 (SrcAddrX,
                                   ((pIgmpFSMInfoNode->pu1Sources) +
                                    u4SrcAddrPos));
        }

        pSrc = IgmpAddSource (pGrp, SrcAddrX, u4Reporter, IGMP_TRUE);
        if (pSrc == NULL)
        {
            continue;
        }

        if (pSrc->u1DynamicFlg != IGMP_TRUE)
        {
            pSrc->u1DynamicFlg = IGMP_TRUE;
            pSrc->u1DynSyncFlag = IGMP_TRUE;
        }
        /*(B-A) = 0 */
        if (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                   pOldSrcSet) == IGMP_TRUE)
        {
            /* If the report has come with exclude mode change for the
             * source address already present in database, we need
             * to check whether there are other receivers for the same
             * source. Only if there are no other receivers the
             * forward state can be set as false and MRP should be 
             * informed to prune the source entry */
            RBTreeCount (pSrc->SrcReporterTree, &u4Count);
            if (u4Count == IGMP_ONE)
            {
                /* Stop the source timer; */
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                           IGMP_NAME,
                           "Source timer stopped for source address %s\n",
                           IgmpPrintIPvxAddress (pSrc->u4SrcAddress));
                if (pSrc->srcTimer.u1TmrStatus == IGMP_TMR_SET)
                {
                    IGMPSTOPTIMER (pSrc->srcTimer);
                    pSrc->u1FwdState = IGMP_FALSE;
                    pSrc->u1UpdatedToMrp = IGMP_FALSE;
                }
                if ((pSrc->u1UpdatedToMrp == IGMP_FALSE) &&
                    (IGMP_PROXY_STATUS () == IGMP_DISABLE))
                {
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                }
            }
        }
    }

    /*(B-A) = 0 */
    if ((TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) > IGMP_ZERO) &&
        (IGMP_PROXY_STATUS () == IGMP_DISABLE))
    {
        /* Updating MRPs */
        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
    }

    /* On receiving (EXCL (G,NONE) report add a source as 0.0.0.0
     * with the incoming reporter IP */
    if ((IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)) &&
        (u4NoOfSources == IGMP_ZERO))
    {
        IGMP_IPVX_ADDR_CLEAR (&SrcAddrX);
        if ((pSrc =
             IgmpAddSource (pGrp, SrcAddrX, u4Reporter, IGMP_FALSE)) != NULL)
        {
            IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
                            IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
        }
    }

    /* Delete (A-B) */
    for (u4Index = IGMP_ZERO; u4Index < u4OldSrcCount; u4Index++)
    {
        if (u4NoOfSources != IGMP_ZERO)
        {
            if (IgmpCheckSourceInArray (pOldSrcSet[u4Index], pu1Sources,
                                        u4NoOfSources) == IGMP_TRUE)
            {
                continue;
            }
        }
        /* delete (A-B) and informs MRP */
        if ((pSrc = IgmpGetSource (pGrp, pOldSrcSet[u4Index])) != NULL)
        {
            if (pSrc->u1StaticFlg == IGMP_FALSE)
            {
                if (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))
                {
                    /* Delete source only when the reporter that sent the
                     * request for (S,G) previously has now sent NONE,G */
                    IgmpDeleteReporter (pGrp->pIfNode, pSrc, u4Reporter);
                    if ((pSrc != NULL) && (pSrc->u1FwdState == IGMP_FALSE))
                    {
                        pSrc->u1FwdState = IGMP_TRUE;
                    }
                }
                else
                {
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
                    if (TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) !=
                        IGMP_ZERO)
                    {
                        /* Updating MRPs for A-B */
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                                       IGMP_NAME, "Updated to MRPs.\n");
                        IGMP_TRC (MGMD_TRC_FLAG, IGMP_DATA_MODULE,
                                  IgmpTrcGetModuleName (IGMP_DATA_MODULE),
                                  "Updated to MRPs.\n");
                    }
                    IgmpDeleteSource (pGrp, pSrc);
                }
            }
        }
    }

    if (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
    {
        /* Send Q(G, A*B) */
        for (pSrc = (tIgmpSource *)
             TMO_SLL_First (&(pGrp->srcList));
             pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
        {
            pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);
            u1SrcDelFlag = IGMP_FALSE;
            if ((IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                        pOldSrcSet) == TRUE) &&
                (IgmpCheckSourceInArray (pSrc->u4SrcAddress,
                                         pu1Sources, u4NoOfSources) == TRUE))
            {
                if (IGMP_IS_FAST_LEAVE_ENABLED (pIfNode))
                {
                    IgmpDeleteReporter (pIfNode, pSrc, u4Reporter);
                    u1SrcDelFlag = IGMP_TRUE;
                }
                if (u1SrcDelFlag == IGMP_FALSE)
                {
                    /* Source Timer lowered to LMQT */
                    IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                               IGMP_NAME,
                               "Source Timer lowered to LMQT for source address %s\n",
                               IgmpPrintIPvxAddress (pSrc->u4SrcAddress));
                    IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                    pSrc->u1QryFlag = IGMP_TRUE;
                    u4NumSrc++;
                }
            }
        }
    }

    if (u4NumSrc > IGMP_ZERO)
    {
        pSrcSet = gpu4SrcSetArray;
        u4NumSrc = IGMP_ZERO;
        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1QryFlag == IGMP_TRUE)
            {
                IGMP_IPVX_ADDR_COPY (&(pSrcSet[u4NumSrc++]),
                                     &(pSrc->u4SrcAddress));
                pSrc->u1QryFlag = IGMP_FALSE;
            }
        }
        if (u4NumSrc > IGMP_ZERO)
        {
            /* Sends GrpAndSrc Qry and also lowers src timer to LMQT */
            IgmpSendGroupAndSourceSpecificQuery (pIfNode, pGrp, u4NumSrc,
                                                 pSrcSet);
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the funtion IgmpToExclude.\n");

    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpBlockInclude                                */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpBlockInclude (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    UINT4               u4NumSrc = IGMP_ZERO;
    tIgmpIPvXAddr      *pSrcSet = NULL;
    UINT1              *pu1Sources = NULL;
    UINT1               u1SrcDelFlag = IGMP_FALSE;
    tGrpNoteMsg         GrpSrcInfo;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the funtion IgmpBlockInclude.\n");

    IgmpFillMem (&GrpSrcInfo, IGMP_ZERO, sizeof (tGrpNoteMsg));

    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    for (pSrc = (tIgmpSource *)
         TMO_SLL_First (&(pGrp->srcList));
         pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
    {
        pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);
        u1SrcDelFlag = IGMP_FALSE;
        /* send Q(G,A*B) */
        if (IgmpCheckSourceInArray (pSrc->u4SrcAddress, pu1Sources,
                                    u4NoOfSources) == TRUE)
        {
            if ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
                (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
            {
                IgmpDeleteReporter (pIfNode, pSrc, u4Reporter);
                u1SrcDelFlag = IGMP_TRUE;
            }

            if ((u1SrcDelFlag == IGMP_FALSE) ||
                (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))))
            {
                /* Source Timer lowered to LMQT */
                IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                pSrc->u1QryFlag = IGMP_TRUE;
                u4NumSrc++;
            }
        }
    }                            /* Scan for SourceNode - END */

    if (u4NumSrc > IGMP_ZERO)
    {
        pSrcSet = gpu4SrcSetArray;
        u4NumSrc = IGMP_ZERO;

        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1QryFlag == IGMP_TRUE)
            {
                IGMP_IPVX_ADDR_COPY (&(pSrcSet[u4NumSrc++]),
                                     &(pSrc->u4SrcAddress));
                pSrc->u1QryFlag = IGMP_FALSE;
            }
        }

        if (u4NumSrc > IGMP_ZERO)
        {
            /* Sends GrpAndSrc Qry */
            IgmpSendGroupAndSourceSpecificQuery (pIfNode, pGrp,
                                                 u4NumSrc, pSrcSet);
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                       IGMP_NAME,
                       "Group and source specific Query send for group %s on index %d\n",
                       IgmpPrintIPvxAddress (pGrp->u4Group),
                       pIfNode->u4IfIndex);
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the funtion IgmpBlockInclude.\n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpBlockExclude                                */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpBlockExclude (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tIgmpIPvXAddr      *pOldSrcSet = gpu4SrcSetArray;
    UINT4               u4Index = IGMP_ZERO;
    UINT4               u4NumSrc = IGMP_ZERO;
    UINT4               u4OldSrcCount = IGMP_ZERO;
    tIgmpIPvXAddr       SrcAddrX;
    tIgmpIPvXAddr      *pSrcSet = NULL;
    UINT1              *pu1Sources = NULL;
    UINT1               u1SrcDelFlag = IGMP_FALSE;
    tGrpNoteMsg         GrpSrcInfo;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;
    UINT4               u4SrcAddrPos = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the funtion IgmpBlockExclude.\n");

    IgmpFillMem (&GrpSrcInfo, IGMP_ZERO, sizeof (tGrpNoteMsg));

    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    /* Copy all the source records to the pOldSrcSet */
    TMO_SLL_Scan (&pGrp->srcList, pSrc, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_COPY (&(pOldSrcSet[u4Index++]), &(pSrc->u4SrcAddress));
    }
    u4OldSrcCount = TMO_SLL_Count (&(pGrp->srcList));

    /* add source to group source table */
    for (u4Index = IGMP_ZERO; u4Index < u4NoOfSources; u4Index++)
    {
        IGMP_IPVX_ADDR_CLEAR (&SrcAddrX);

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                                      (pu1Sources +
                                       (IPVX_IPV4_ADDR_LEN * u4Index)));
        }
        else
        {
            u4SrcAddrPos = (UINT2) (u4Index * (UINT1) IPVX_IPV6_ADDR_LEN);
            IPVX_ADDR_INIT_FROMV6 (SrcAddrX,
                                   ((pIgmpFSMInfoNode->pu1Sources) +
                                    u4SrcAddrPos));
        }

        pSrc = IgmpAddSource (pGrp, SrcAddrX, u4Reporter, IGMP_TRUE);
        if (pSrc == NULL)
        {
            continue;
        }

        if (pSrc->u1DynamicFlg != IGMP_TRUE)
        {
            pSrc->u1DynamicFlg = IGMP_TRUE;
            pSrc->u1DynSyncFlag = IGMP_TRUE;
        }
        /*(A-X-Y)=group_timer */
        if (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                   pOldSrcSet) == FALSE)
        {
            /* Start source timer with interval equal to 
             * Group Membership Interval 
             */
            IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
                            IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_TMR_MODULE,
                       IgmpGetModuleName (IGMP_TMR_MODULE),
                       "source timer started for %s with Interval value %d\n",
                       IgmpPrintIPvxAddress (pGrp->u4Group),
                       IGMP_IF_GROUP_TIMER (pIfNode));
            pSrc->u1FwdState = IGMP_TRUE;
            if (IGMP_PROXY_STATUS () == IGMP_DISABLE)
            {
                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
            }
        }
    }

    if ((TMO_SLL_Count (&(pGrp->SrcListForMrpUpdate)) > IGMP_ZERO) &&
        (IGMP_PROXY_STATUS () == IGMP_DISABLE))
    {
        /* Updating MRPs */
        IgmpUpdateMrps (pGrp, IGMP_JOIN);
    }

    /* send Q(G,A-Y) */
    u4NumSrc = IGMP_ZERO;
    for (pSrc = (tIgmpSource *)
         TMO_SLL_First (&(pGrp->srcList));
         pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
    {
        pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);
        u1SrcDelFlag = IGMP_FALSE;

        if ((IgmpCheckSourceInArray (pSrc->u4SrcAddress,
                                     pu1Sources, u4NoOfSources) == TRUE) &&
            (IgmpCheckSourceInList (pSrc->u4SrcAddress, u4OldSrcCount,
                                    pOldSrcSet) == FALSE))
        {
            /* one common list for fwd & non-fwd sources , the below chk is 
             * to send queries only related to non-fwd sources */
            if ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
                (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
            {
                IgmpDeleteReporter (pIfNode, pSrc, u4Reporter);
                u1SrcDelFlag = IGMP_TRUE;
            }
            if ((u1SrcDelFlag == IGMP_FALSE) ||
                (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode))))
            {
                /* Source Timer lowered to LMQT */
                IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                pSrc->u1QryFlag = IGMP_TRUE;
                u4NumSrc++;
            }
        }
    }

    if (u4NumSrc > IGMP_ZERO)
    {
        pSrcSet = gpu4SrcSetArray;
        u4NumSrc = IGMP_ZERO;
        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if (pSrc->u1QryFlag == IGMP_TRUE)
            {
                IGMP_IPVX_ADDR_COPY (&(pSrcSet[u4NumSrc]),
                                     &(pSrc->u4SrcAddress));
                pSrc->u1QryFlag = IGMP_FALSE;
                u4NumSrc++;
            }
        }
        if (u4NumSrc > IGMP_ZERO)
        {
            /* Sends GrpAndSrc Qry */
            IgmpSendGroupAndSourceSpecificQuery (pIfNode, pGrp, u4NumSrc,
                                                 pSrcSet);
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                       IGMP_NAME,
                       "Group and source specific Query send for group %s on index %d\n",
                       IgmpPrintIPvxAddress (pGrp->u4Group),
                       pIfNode->u4IfIndex);
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the funtion IgmpBlockExclude.\n");

    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpAction1                                     */
/*                                                                         */
/*     Description   :     This function process a group record with       */
/*                         record type ALLOW_NEW_SOURCES                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pMcast - Pointer to the multicast i/f block      */
/*                        u4NoOfSources - No of sources being reported.    */
/*                        pu1Sources - pointer to the list of sources.     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :    a pointer to the mcast iface block               */
/*                                                                         */
/***************************************************************************/
INT4
IgmpAction1 (tIgmpFSMInfo * pIgmpFSMInfoNode)
{
    tIgmpSource        *pSrc = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfNode = NULL;
    UINT4               u4Index = IGMP_ZERO;
    tIgmpIPvXAddr       SrcAddrX;
    UINT1              *pu1Sources = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    tIgmpIPvXAddr       u4Reporter;
    UINT4               u4SrcAddrPos = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the funtion IgmpAction1.\n");

    pu1Sources = pIgmpFSMInfoNode->pu1Sources;
    u4NoOfSources = pIgmpFSMInfoNode->u4NoOfSources;
    pIfNode = pIgmpFSMInfoNode->pIfNode;
    pGrp = pIgmpFSMInfoNode->pGrp;
    IGMP_IPVX_ADDR_CLEAR (&u4Reporter);
    IGMP_IPVX_ADDR_COPY (&u4Reporter, &(pIgmpFSMInfoNode->u4Reporter));

    /* Initialize the source */
    for (u4Index = IGMP_ZERO; u4Index < u4NoOfSources; u4Index++)
    {
        IGMP_IPVX_ADDR_CLEAR (&SrcAddrX);

        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                                      (pu1Sources +
                                       (IPVX_IPV4_ADDR_LEN * u4Index)));
        }
        else
        {
            u4SrcAddrPos = (UINT2) (u4Index * (UINT1) IPVX_IPV6_ADDR_LEN);
            IPVX_ADDR_INIT_FROMV6 (SrcAddrX,
                                   ((pIgmpFSMInfoNode->pu1Sources) +
                                    u4SrcAddrPos));
        }

        pSrc = IgmpAddSource (pGrp, SrcAddrX, u4Reporter, IGMP_TRUE);
        if (pSrc == NULL)
        {
            continue;
        }

        /* Start SourceTmr with interval equal to Group Membership Interval */
        if (pSrc->u1DynamicFlg != IGMP_TRUE)
        {
            pSrc->u1DynamicFlg = IGMP_TRUE;
            pSrc->u1DynSyncFlag = IGMP_TRUE;
        }
        if ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET)
        {
            IGMPSTOPTIMER (pSrc->srcTimer);
            if (pSrc->u1SrsBitFlag == IGMP_TRUE)
            {
                if ((pIfNode->u1Version == MLD_VERSION_2) ||
                    ((pIfNode->u1Version == IGMP_VERSION_3) &&
                     (pGrp->u1V2HostPresent == IGMP_FALSE)))
                {
                    IgmpDelSchQrySrcNode (pSrc);
                }
                else
                {
                    IgmpDelSchQryGrpNode (pSrc->pGrp);
                }
            }
        }
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_TMR_MODULE,
                   IgmpGetModuleName (IGMP_TMR_MODULE),
                   "source timer interval started for group %s with interval %d\r\n",
                   IgmpPrintIPvxAddress (pGrp->u4Group),
                   IGMP_IF_GROUP_TIMER (pIfNode));

        IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
                        IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);

        /* 
         * when the source was in non-fwd list, it was already updated to 
         * MRP and the u1UpdatedToMrp flag was set,
         * now if the source is added in fwd-list, it will be not updated
         * to MRP b'coz u1UpdatedToMrp was set. In order to update
         * this source, the u1UpdatedToMrp is reset 
         */
        if (pSrc->u1FwdState == IGMP_FALSE)
        {
            pSrc->u1UpdatedToMrp = IGMP_FALSE;
        }

        pSrc->u1FwdState = IGMP_TRUE;

        if ((pSrc->u1UpdatedToMrp == IGMP_FALSE) &&
            (IGMP_PROXY_STATUS () == IGMP_DISABLE))
        {
            TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
            TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
        }

        if ((TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO) &&
            (IGMP_PROXY_STATUS () == IGMP_DISABLE))
        {
            /* Updating MRPs */
            IgmpUpdateMrps (pGrp, IGMP_JOIN);
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the funtion IgmpAction1.\n");
    return IGMP_OK;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpGetModuleName                                          */
/*                                                                           */
/* Description  : Gets the Module name like IO,GRP,QRY,TMR                   */
/*                                                                           */
/* Input        : u2TraceModule    :                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returns the Module Name                                    */
/*                                                                           */
/*****************************************************************************/
const char         *
IgmpGetModuleName (UINT2 u2TraceModule)
{
    UINT1               au1ModName[11][7] =
        { "INIT", "RES", "TMR ", "GRP ", "QRY ",
        "I/O ", "NP ", "BUFFER ", "MGMT", "ENTRY ", "EXIT "
    };

    if ((gIgmpConfig.u4MldDbgFlag > 0) && (gIgmpConfig.u4IgmpDbgFlag > 0))
    {
        STRCPY (gIgmpConfig.au1IgmpTrcMode, "MGMD-");
    }
    else if (gIgmpConfig.u4IgmpDbgFlag > 0)
    {
        STRCPY (gIgmpConfig.au1IgmpTrcMode, "IGMP-");
    }
    else if (gIgmpConfig.u4MldDbgFlag > 0)
    {
        STRCPY (gIgmpConfig.au1IgmpTrcMode, "MLD:-");
    }
    MEMSET (gIgmpConfig.au1IgmpTrcMode + 5, 0, 7);
    switch (u2TraceModule)
    {
        case IGMP_INIT_SHUT_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[0], 4);
            break;

        case IGMP_OS_RES_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[1], 3);
            break;

        case IGMP_TMR_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[2], 3);
            break;

        case IGMP_GRP_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[3], 3);
            break;

        case IGMP_QRY_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[4], 3);
            break;

        case IGMP_IO_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[5], 3);
            break;

        case IGMP_NP_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[6], 2);
            break;

        case MLD_BUFFER_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[7], 6);
            break;

        case IGMP_MGMT_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[8], 4);
            break;

        case IGMP_ENTRY_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[9], 5);
            break;

        case IGMP_EXIT_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[10], 4);
            break;
        default:
            break;
    }
    return ((const char *) gIgmpConfig.au1IgmpTrcMode);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpTraceGetModuleName                                     */
/*                                                                           */
/* Description  : Gets the Module name like MGMT,DATA-PATH,CNTRL-PATH        */
/*                                                                           */
/* Input        : u2TraceModule                                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returns the Module Name                                    */
/*                                                                           */
/*****************************************************************************/

const char         *
IgmpTrcGetModuleName (UINT2 u2TraceModule)
{
    UINT1               au1ModName[4][5] = { "DATA", "CNTRL", "Rx", "Tx" };
    if ((gIgmpConfig.u4MldTrcFlag > 0) && (gIgmpConfig.u4IgmpTrcFlag > 0))
    {
        STRCPY (gIgmpConfig.au1IgmpTrcMode, "MGMD-");
    }
    else if (gIgmpConfig.u4IgmpTrcFlag > 0)
    {
        STRCPY (gIgmpConfig.au1IgmpTrcMode, "IGMP-");
    }
    else if (gIgmpConfig.u4MldTrcFlag > 0)
    {
        STRCPY (gIgmpConfig.au1IgmpTrcMode, "MLD:-");
    }
    MEMSET (gIgmpConfig.au1IgmpTrcMode + 5, 0, 7);
    switch (u2TraceModule)
    {
        case IGMP_DATA_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[0], 4);
            break;

        case IGMP_CNTRL_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[1], 5);
            break;

        case IGMP_Rx_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[2], 2);
            break;

        case IGMP_Tx_MODULE:
            STRNCPY (gIgmpConfig.au1IgmpTrcMode + 5, au1ModName[3], 2);
            break;

        default:
            MEMSET (gIgmpConfig.au1IgmpTrcMode + 5, 0, 7);
            break;
    }
    return ((const char *) gIgmpConfig.au1IgmpTrcMode);
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpUtilIsStaticEntry                           */
/*                                                                         */
/*     Description   :     This function checks whether the entry is a     */
/*                         static entry or not                             */
/*                                                                         */
/*     Input(s)      :     u4IpAddr - Group IpAddress                      */
/*                         u4IfIndex - Interface Index                     */
/*                         u1AddrType - Address Type                       */
/*                         u1CheckVersion - Flag to check interface version*/
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     TRUE/FALSE                                      */
/*                                                                         */
/***************************************************************************/
INT4
IgmpUtilIsStaticEntry (UINT4 u4IpAddr, UINT4 u4IfIndex, UINT1 u1AddrType,
                       UINT1 u1CheckVersion)
{
    tIgmpIPvXAddr       GrpAddr;
    tIgmpGroup         *pGrp = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    INT4                i4RetValue = 0;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpUtilIsStaticEntry()\r \n");
    UNUSED_PARAM (i4RetValue);
    MEMSET (&GrpAddr, 0, sizeof (tIgmpIPvXAddr));

    IGMP_IPVX_ADDR_INIT_IPV4 (GrpAddr, u1AddrType, (UINT1 *) &u4IpAddr);

    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetValue = IGMP_IP_GET_PORT_FROM_IFINDEX (u4IfIndex, &u4Port);
    }
    else
    {
        if (MldPortGetIpPortFrmIndex (u4IfIndex, &u4Port) == OSIX_FAILURE)
        {
            return FALSE;
        }
    }

    pIfaceNode = IGMP_GET_IF_NODE (u4Port, u1AddrType);
    if (pIfaceNode == NULL)
    {
        return FALSE;
    }

    pGrp = IgmpGrpLookUp (pIfaceNode, GrpAddr);
    if (pGrp == NULL)
    {
        return FALSE;
    }

    if (pGrp->u1StaticFlg == IGMP_TRUE)
    {
        if ((u1CheckVersion == IGMP_TRUE) &&
            (pIfaceNode->u1Version == IGMP_VERSION_3))
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpUtilIsStaticEntry()\r \n");
    return FALSE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpUtilIsStaticEntry                           */
/*                                                                         */
/*     Description   :     This function checks whether the entry is a     */
/*                         static entry or not                             */
/*                                                                         */
/*     Input(s)      :     u4IpAddr - Group IpAddress                      */
/*                         u4IfIndex - Interface Index                     */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     TRUE/FALSE                                      */
/*                                                                         */
/***************************************************************************/
INT4
IgmpUtilIsSourcePresent (tIgmpIPvXAddr u4IpAddr, UINT4 u4IfIndex,
                         UINT1 u1AddrType)
{
    INT4                i4RetValue = 0;
    UINT4               u4Port = IGMP_ZERO;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpUtilIsSourcePresent()\r \n");
    UNUSED_PARAM (i4RetValue);
    if (u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        i4RetValue = IGMP_IP_GET_PORT_FROM_IFINDEX (u4IfIndex, &u4Port);
    }
    else
    {
        if (MldPortGetIpPortFrmIndex (u4IfIndex, &u4Port) == OSIX_FAILURE)
        {
            return FALSE;
        }
    }

    pIfaceNode = IGMP_GET_IF_NODE (u4Port, u1AddrType);
    if (pIfaceNode == NULL)
    {
        return FALSE;
    }

    pGrp = IgmpGrpLookUp (pIfaceNode, u4IpAddr);
    if (pGrp == NULL)
    {
        return FALSE;
    }
    else if (((pSrc = IgmpISSourcePresentForGroup (pGrp)) != NULL) &&
             (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress, gIPvXZero) != 0))
    {
        return TRUE;
    }
    UNUSED_PARAM (pSrc);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpUtilIsSourcePresent()\r \n");
    return FALSE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IpIsRtrAlertPresent                             */
/*                                                                         */
/*     Description   :     This function checks whether the router         */
/*                         alert option is present in the packet           */
/*                                                                         */
/*     Input(s)      :     pIp - Pointer to the IP packet.                 */
/*                                                                         */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     TRUE/FALSE                                      */
/*                                                                         */
/***************************************************************************/
INT4
IpIsRtrAlertPresent (t_IP * pIp)
{
    INT2                i2Opt_len;
    INT4                i = 0, i1Opt;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IpIsRtrAlertPresent()\r \n");
    while ((i < pIp->u2Olen)
           && ((i1Opt = IP_OPT_NUMBER (pIp->au1Options[i])) != IP_OPT_EOL))
    {
        i2Opt_len =
            (i1Opt == IP_OPT_NOP) ? 1 : (UINT1) (pIp->au1Options[i + 1]);

        if (i2Opt_len < 2)
        {
            return FALSE;
        }

        switch (i1Opt)
        {
            case IP_OPT_ROUTERALERT:
                return TRUE;
                break;
            default:
                i += i2Opt_len;
                break;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IpIsRtrAlertPresent()\r \n");
    return FALSE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpGetPortFromIfIndex                          */
/*                                                                         */
/*     Description   :     This function gives the correspondig Port for   */
/*                         the specified Interface Index                   */
/*                                                                         */
/*     Input(s)      :     i4IfIndex  --> Interface Index                  */
/*               i4AddrType --> IPV4/IPV6 address type           */
/*                                                                         */
/*                                                                         */
/*     Output(s)     :     pu4Port --> Port no as output                   */
/*                                                                         */
/*     Returns       :     TRUE/FALSE                                      */
/*                                                                         */
/***************************************************************************/

INT1
IgmpGetPortFromIfIndex (INT4 i4IfIndex, INT4 i4AddrType, UINT4 *pu4Port)
{
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4IfIndex,
                                           pu4Port) == NETIPV4_FAILURE)
        {
            return IGMP_FAILURE;
        }
        return IGMP_SUCCESS;
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (MldPortGetIpPortFrmIndex ((UINT4) i4IfIndex,
                                      pu4Port) == OSIX_FAILURE)
        {
            return IGMP_FAILURE;
        }
        return IGMP_SUCCESS;
    }
    return IGMP_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpGetIfIndexFromPort                          */
/*                                                                         */
/*     Description   :     This function gives the correspondig Interface  */
/*                         for the specified Port                          */
/*                                                                         */
/*     Input(s)      :     u4Port  --> Port                                */
/*               i4AddrType --> IPV4/IPV6 address type           */
/*                                                                         */
/*                                                                         */
/*     Output(s)     :     pu4IfIndex --> Interface Index                  */
/*                                                                         */
/*     Returns       :     TRUE/FALSE                                      */
/*                                                                         */
/***************************************************************************/

INT1
IgmpGetIfIndexFromPort (UINT4 u4Port, INT4 i4AddrType, UINT4 *pu4IfIndex)
{
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) u4Port,
                                           pu4IfIndex) == NETIPV4_FAILURE)
        {
            return IGMP_FAILURE;
        }
        return IGMP_SUCCESS;
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (MLD_IP6_GET_IFINDEX_FROM_PORT ((UINT4) u4Port,
                                           pu4IfIndex) == OSIX_FAILURE)
        {
            return IGMP_FAILURE;
        }
        return IGMP_SUCCESS;
    }
    return IGMP_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpGetIfNode                                   */
/*                                                                         */
/*     Description   :     This function gives the correspondig IfNode     */
/*                         for the specified Port                          */
/*                                                                         */
/*     Input(s)      :     u4Port  --> Port                                */
/*                         i4AddrType --> IPv4/IPv6 Type                   */
/*                                                                         */
/*                                                                         */
/*     Output(s)     :     Pointer to the  Interface Node                  */
/*                                                                         */
/*     Returns       :     TRUE/FALSE                                      */
/*                                                                         */
/***************************************************************************/
tIgmpIface         *
IgmpGetIfNode (UINT4 u4Port, INT4 i4AddrType)
{
    return (IGMP_GET_IF_NODE (u4Port, (UINT1) i4AddrType));
}

/****************************************************************************/
/* Function Name         :   IgmpPrintAddress                               */
/*                                                                          */
/* Description           :   Converts the Input IpAddress into String and   */
/*                           Prints the IPAddress in the standard format    */
/*                                                                          */
/* Input (s)             :   pu1Addr: Ip Address                            */
/*                           u1Afi  : Ip Address Family                     */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : string containing the IpAddress in printable format*/
/****************************************************************************/

UINT1              *
IgmpPrintAddress (UINT1 *pu1Addr, UINT1 u1Afi)
{
    tIp6Addr            Ip6Addr;
    UINT4               u4Addr = 0;

    if (pu1Addr == NULL)
    {
        return NULL;
    }
    u4Addr = (UINT4) OSIX_NTOHL (*((UINT4 *) (VOID *) (pu1Addr)));
    MEMSET (&Ip6Addr, IGMP_ZERO, sizeof (tIp6Addr));
    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pu1Addr, &u4Addr, IPVX_IPV4_ADDR_LEN);
        MEMCPY (&Ip6Addr.u1_addr[IGMP_IPV6_THIRD_WORD_OFFSET], pu1Addr,
                IPVX_IPV4_ADDR_LEN);
    }
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr.u1_addr, pu1Addr, IPVX_IPV6_ADDR_LEN);
    }
    else
    {
        return NULL;
    }
    return (Ip6PrintNtopWithBuf (&Ip6Addr, gacIgmpAddrPrintBuf));
}

UINT1              *
IgmpPrintIPvxAddress (tIgmpIPvXAddr Addr)
{
    return (IgmpPrintAddress (Addr.au1Addr, Addr.u1Afi));
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpUpdateMrps                                */
/*                                                                         */
/*     Description   :     This function updates the MRPs                  */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    GrpSrcInfo - variable of tGrpNoteMsg             */
/*                        pGrp - pointer to  tIgmpGroup                    */
/*                        u1JoinLeaveFlag - Join/Leave                     */
/*                        u1FilterMode - Include/Exclude                   */
/*                        u1NumOfSrcs - No of sources being reported.      */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpUtilCreateGroupEntry                    **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpUpdateMrps (tIgmpGroup * pGrp, UINT1 u1JoinLeaveFlag)
{
    tIgmpSource        *pSrc = NULL;

    if ((pGrp->pIfNode == NULL) ||
        ((pGrp->pIfNode->u1OperStatus == IGMP_IFACE_OPER_DOWN) &&
         (pGrp->u1StaticFlg == IGMP_TRUE)))
    {
        while ((pSrc = (tIgmpSource *)
                TMO_SLL_First (&(pGrp->SrcListForMrpUpdate))) != NULL)
        {
            /* Since this source is not updated then it can become part of the
             * main source list of the group node.
             */
            TMO_SLL_Delete (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
            TMO_SLL_Add (&(pGrp->srcList), &pSrc->Link);
            pSrc->u1UpdatedToMrp = IGMP_FALSE;
        }
        return;
    }
    if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IgmpUtilUpdateMrps (pGrp, u1JoinLeaveFlag);
    }
    else if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MldPortUpdateMrps (pGrp, u1JoinLeaveFlag);
    }
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpUtilIncMsrForFsScalars                        */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                       FsIgmp and FsMld Scalars                          */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4SetVal        - Configured value that has to   */
/*                                          be notified                    */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
IgmpUtilIncMsrForFsScalars (UINT4 *pu4ObjectId, UINT4 u4OidLen, INT4 i4SetVal)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;

    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IgmpLock;
    SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
    SnmpNotifyInfo.u4Indices = IGMP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpUtilIncMsrForFsIgmpTbl                        */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                 FsIgmp  Table                                     */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4Index        - Interface Index where the entry */
/*                                  is added                        */
/*                        i4SetVal        - Configured value that has to   */
/*                                          be notified                    */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
IgmpUtilIncMsrForFsIgmpTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                            INT4 i4Index, INT4 i4SetVal)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;

    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IgmpLock;
    SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
    SnmpNotifyInfo.u4Indices = IGMP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", i4Index, IPVX_ADDR_FMLY_IPV4,
                      i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpUtilIncMsrForIfTbl                            */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                 Igmp Interface  Table                             */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        i4Index        - Interface Index where the entry */
/*                                  is added                        */
/*              cDataType      - Datatype of the configured value*/
/*                        pSetVal        - Configured value that has to    */
/*                                          be notified                    */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
IgmpUtilIncMsrForIfTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                        INT4 i4Index, CHR1 cDataType, VOID *pSetVal,
                        UINT1 u1RowStatus, INT4 i4AddrType)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;
    UINT4               u4SetVal = IGMP_ZERO;
    INT4                i4SetVal = IGMP_ZERO;

    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = u1RowStatus;
    SnmpNotifyInfo.pLockPointer = IgmpLock;
    SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
    SnmpNotifyInfo.u4Indices = IGMP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDataType)
    {
        case 'i':
            i4SetVal = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", i4Index, i4AddrType,
                              i4SetVal));
            break;

        case 'u':
            u4SetVal = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u", i4Index, i4AddrType,
                              u4SetVal));
            break;
        default:
            break;
    }
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpUtilIncMsrForIgmpCacheTbl                     */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                       Igmp Cache  Table                                 */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*              u4Addr         - Cache Address                   */
/*                        i4Index        - Interface Index where the entry */
/*                                         is added                        */
/*                        i4SetVal       - Configured value that has to    */
/*                                          be notified                    */
/*              u1RowStatus    - Rowstatus of Cache Table        */
/*                                (TRUE/FALSE)                   */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/

VOID
IgmpUtilIncMsrForIgmpCacheTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                               UINT4 u4Addr, INT4 i4Index, INT4 i4SetVal,
                               UINT1 u1RowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;

    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = u1RowStatus;
    SnmpNotifyInfo.pLockPointer = IgmpLock;
    SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
    SnmpNotifyInfo.u4Indices = IGMP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %i %i", u4Addr, i4Index,
                      i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpUtilIncMsrForIgmpSrcTbl                       */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                       Igmp Source Table                                 */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        u4CacheAddr    - Cache Address                   */
/*                        i4Index        - Interface Index where the entry */
/*                                         is added                        */
/*              u4SrcAddr      - Source Address                  */
/*                        i4SetVal       - Configured value that has to    */
/*                                          be notified                    */
/*                        u1RowStatus    - Rowstatus of SrcListTable       */
/*                                          (TRUE/FALSE)                   */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/

VOID
IgmpUtilIncMsrForIgmpSrcTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                             UINT4 u4CacheAddr, INT4 i4Index, UINT4 u4HostAddr,
                             INT4 i4SetVal, UINT1 u1RowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;

    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = u1RowStatus;
    SnmpNotifyInfo.pLockPointer = IgmpLock;
    SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
    SnmpNotifyInfo.u4Indices = IGMP_SRC_TBL_INDICES_COUNT;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i %u %i %i", u4CacheAddr, i4Index,
                      u4HostAddr, i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpUtilIncMsrForIgmpSSMMapTbl                    */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                       Igmp SSM Map Table                                 */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                            - Cache Address                   */
/*                        i4Index        - Interface Index where the entry */
/*                                         is added                        */
/*              u4SrcAddr      - Source Address                  */
/*                        i4SetVal       - Configured value that has to    */
/*                                          be notified                    */
/*                        u1RowStatus    - Rowstatus of SrcListTable       */
/*                                          (TRUE/FALSE)                   */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/

VOID
IgmpUtilIncMsrForIgmpSSMMapTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                UINT4 u4StartGrpAddr, UINT4 u4EndGrpAddr,
                                UINT4 u4SourceAddr, INT4 i4SetVal,
                                UINT1 u1RowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;

    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = u1RowStatus;
    SnmpNotifyInfo.pLockPointer = IgmpLock;
    SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
    SnmpNotifyInfo.u4Indices = IGMP_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %u %i", u4StartGrpAddr,
                      u4EndGrpAddr, u4SourceAddr, i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpUtilIncMsrForMldCacheTbl                      */
/*                                                                         */
/*     Description   :   This function sends notifications to MSR for the  */
/*                       Mld Cache Table                                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    pu4ObjectId    - OID                             */
/*                        u4OidLen       - Length of the OID               */
/*                        pAddr          - Cache Address                   */
/*                        i4Index        - Interface Index where the entry */
/*                                         is added                        */
/*                        i4SetVal       - Configured value that has to    */
/*                                          be notified                    */
/*                        u1RowStatus    - Rowstatus of SrcListTable       */
/*                                          (TRUE/FALSE)                   */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
IgmpUtilIncMsrForMldCacheTbl (UINT4 *pu4ObjectId, UINT4 u4OidLen,
                              tSNMP_OCTET_STRING_TYPE * pAddr, INT4 i4Index,
                              INT4 i4SetVal, UINT1 u1RowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;

    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = u1RowStatus;
    SnmpNotifyInfo.pLockPointer = IgmpLock;
    SnmpNotifyInfo.pUnLockPointer = IgmpUnLock;
    SnmpNotifyInfo.u4Indices = IGMP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i %i %i", pAddr, i4Index, i4SetVal));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpUtilGetMembershipInfo                     */
/*                                                                         */
/*     Description   :     This function gets the membership information   */
/*                    for the given grp, src, reporter & interface index   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    Grp Addr                                         */
/*                        Src Address                                      */
/*                        Reporter Address                                 */
/*                        Interface index                                  */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
INT1
IgmpUtilGetMembershipInfo (UINT4 u4IgmpGrpAddr, UINT4 u4IgmpSrcAddr,
                           UINT4 u4IgmpReporterAddr, INT4 i4IgmpIndex,
                           UINT4 *pu4IgmpGrpAddr, UINT4 *pu4IgmpSrcAddr,
                           UINT4 *pu4IgmpReporterAddr, INT4 *pi4IgmpIndex)
{
    tTMO_SLL_NODE      *pSrcLink = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup          IgmpGroupEntry;
    tIgmpSrcReporter   *pSrcReporter = NULL;
    tIgmpSrcReporter    FindReporter;
    tIgmpIPvXAddr       GrpAddr;
    tIgmpIPvXAddr       SrcAddr;
    tIgmpIPvXAddr       ReporterAddr;
    BOOL1               bIsGrpFound = IGMP_TRUE;
    BOOL1               b1EntryFound = IGMP_FALSE;
    INT1                i1Status = IGMP_FAILURE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpUtilGetMembershipInfo()\r \n");
    MEMSET (&FindReporter, 0, sizeof (tIgmpSrcReporter));
    MEMSET (&IgmpGroupEntry, 0, sizeof (tIgmpGroup));

    IGMP_IPVX_ADDR_CLEAR (&GrpAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcAddr);
    IGMP_IPVX_ADDR_CLEAR (&ReporterAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (GrpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpGrpAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (ReporterAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpReporterAddr);

    pIfNode = IgmpGetIfNode ((UINT4) i4IgmpIndex, IPVX_ADDR_FMLY_IPV4);
    if (pIfNode == NULL)
    {
        i1Status = IGMP_FAILURE;
        return i1Status;
    }

    IgmpGroupEntry.u4IfIndex = (UINT4) i4IgmpIndex;
    IgmpGroupEntry.pIfNode = pIfNode;
    IGMP_IPVX_ADDR_COPY (&(IgmpGroupEntry.u4Group), &GrpAddr);

    /* Get the group entry corresponding to the input given */
    pGrp = (tIgmpGroup *) RBTreeGet (gIgmpGroup, (tRBElem *) & IgmpGroupEntry);
    if (pGrp == NULL)
    {
        /* No entry exists for the given input, then get the
         * next valid group entry */
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                   IGMP_NAME,
                   "Group entry not exists for group address %s\n",
                   IgmpPrintIPvxAddress (GrpAddr));
        bIsGrpFound = IGMP_FALSE;
    }
    else
    {
        if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            /* Group entry found search for next valid source address */
            TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
            {
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                           IGMP_NAME,
                           "Group entry %s found search for next valid source address\n",
                           IgmpPrintIPvxAddress (GrpAddr));
                /* Scan for Source list */
                pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);
                if (IGMP_IPVX_ADDR_COMPARE (SrcAddr,
                                            pSrc->u4SrcAddress) == IGMP_ZERO)
                {
                    /* Given Source address found */
                    IGMP_IPVX_ADDR_COPY (&(FindReporter.u4SrcRepAddr),
                                         &ReporterAddr);
                    pSrcReporter =
                        (tIgmpSrcReporter *) RBTreeGet (pSrc->SrcReporterTree,
                                                        (tRBElem *) &
                                                        FindReporter);
                    if (pSrcReporter != NULL)
                    {
                        b1EntryFound = IGMP_TRUE;
                    }
                    if (b1EntryFound == IGMP_FALSE)
                    {
                        /* Continue with the next source address */
                        continue;
                    }
                }
                else if (IGMP_IPVX_ADDR_COMPARE (SrcAddr,
                                                 pSrc->u4SrcAddress) <
                         IGMP_ZERO)
                {
                    /* As we are getting the next source entry, we need to 
                     * return the first reporter in the list */
                    pSrcReporter =
                        (tIgmpSrcReporter *) RBTreeGetFirst (pSrc->
                                                             SrcReporterTree);
                    b1EntryFound = IGMP_TRUE;
                    break;
                }
                if (b1EntryFound == IGMP_TRUE)
                {
                    /* Next valid Entry found for given input,
                     * So avoid scanning the next entry  */
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                                   IGMP_NAME,
                                   "Next valid Entry found for given input\n");
                    break;
                }
            }
        }
        if (b1EntryFound == IGMP_FALSE)
        {
            /* there is no valid next entry for the given input
             * in the given group entry, so get the first entry from the 
             * next group entry */
            bIsGrpFound = IGMP_FALSE;
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                           IGMP_NAME,
                           "there is no valid next entry for the given input"
                           "in the given group entry\n");
        }
    }

    if (bIsGrpFound == IGMP_FALSE)
    {
        while (1)
        {
            pGrp = (tIgmpGroup *) RBTreeGetNext
                (gIgmpGroup, (tRBElem *) & IgmpGroupEntry, NULL);

            if (pGrp == NULL)
            {
                i1Status = IGMP_FAILURE;
                break;
            }

            if (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                pSrcLink = (tTMO_SLL_NODE *) TMO_SLL_First (&(pGrp->srcList));
                if (pSrcLink != NULL)
                {
                    pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);
                    pSrcReporter =
                        (tIgmpSrcReporter *) RBTreeGetFirst (pSrc->
                                                             SrcReporterTree);
                    b1EntryFound = IGMP_TRUE;
                    break;
                }
            }
            /* Continue to get the next valid entry for IPv4 type */
            IgmpGroupEntry.u4IfIndex = pGrp->pIfNode->u4IfIndex;
            IGMP_IPVX_ADDR_COPY (&(IgmpGroupEntry.u4Group), &(pGrp->u4Group));
        }
    }

    if ((b1EntryFound == IGMP_TRUE) && (pSrcReporter != NULL))
    {
        i1Status = IGMP_SUCCESS;
        IGMP_IP_COPY_FROM_IPVX (pu4IgmpGrpAddr,
                                pGrp->u4Group, IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (pu4IgmpSrcAddr,
                                pSrc->u4SrcAddress, IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (pu4IgmpReporterAddr,
                                pSrcReporter->u4SrcRepAddr,
                                IPVX_ADDR_FMLY_IPV4);
        *pi4IgmpIndex = pGrp->u4IfIndex;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpUtilGetMembershipInfo()\r \n");
    return i1Status;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :       IgmpUtilGetConfiguredQueryInterval            */
/*                                                                         */
/*     Description   :    This function gets the configured query interval */
/*                        for the given interface index                    */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    i4IgmpInterfaceIfIndex                           */
/*                                                                         */
/*     Output(s)     :    IgmpInterfaceConfiguredMembQueryIntvl            */
/*                                                                         */
/*     Returns       :    IGMP_SUCCESS or IGMP_FAILURE                     */
/*                                                                         */
/***************************************************************************/
INT1
IgmpUtilGetConfiguredQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                    UINT4 *pu4IgmpInterfaceConfiguredQueryIntvl)
{
    INT1                i1Status = IGMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpUtilGetConfiguredQueryInterval\n");
    if (IgmpGetPortFromIfIndex
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return IGMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface %d is configured as upstream interface\r\n",
                   u4Port);
        *pu4IgmpInterfaceConfiguredQueryIntvl = IGMP_ZERO;
        return IGMP_SUCCESS;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfaceNode != NULL)
    {

        *pu4IgmpInterfaceConfiguredQueryIntvl =
            (UINT4) pIfaceNode->u2ConfiguredQueryInterval;

        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "InterfaceConfiguredQuery Interval of i/f %d is %d \n",
                   i4IgmpInterfaceIfIndex,
                   *pu4IgmpInterfaceConfiguredQueryIntvl);
        i1Status = IGMP_SUCCESS;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the function IgmpUtilGetConfiguredQueryInterval\n");
    return (i1Status);
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    SortIgmpSSMMappedSources                         */
/*                                                                         */
/*     Description   :    This function sorts the IGMP SSM Mapped Sources  */
/*                        Array                                            */
/*                                                                         */
/*     Input(s)      :    pSrcAddr                                         */
/*                                                                         */
/*     Output(s)     :    pSrcAddr                                         */
/*                                                                         */
/*     Returns       :    VOID                                             */
/*                                                                         */
/***************************************************************************/
VOID
SortIgmpSSMMappedSources (tIgmpIPvXAddr * pSrcAddr)
{
    tIgmpIPvXAddr       TempIPvXAddr;
    UINT4               u4Index1 = IGMP_ZERO;
    UINT4               u4Index2 = IGMP_ZERO;
    UINT4               u4FirstAddr = IGMP_ZERO;
    UINT4               u4SecondAddr = IGMP_ZERO;
    UINT1               u1Swapped = IGMP_FALSE;

    MEMSET (&TempIPvXAddr, IGMP_ZERO, sizeof (tIgmpIPvXAddr));

    for (u4Index1 = IGMP_ZERO; u4Index1 < (MAX_IGMP_SSM_MAP_SRC - IGMP_ONE);
         u4Index1++)
    {
        u1Swapped = IGMP_FALSE;

        for (u4Index2 = IGMP_ZERO;
             u4Index2 < (MAX_IGMP_SSM_MAP_SRC - u4Index1 - IGMP_ONE);
             u4Index2++)
        {
            u4FirstAddr = u4SecondAddr = IGMP_ZERO;
            IGMP_IP_COPY_FROM_IPVX (&u4FirstAddr, pSrcAddr[u4Index2],
                                    IPVX_ADDR_FMLY_IPV4);
            IGMP_IP_COPY_FROM_IPVX (&u4SecondAddr,
                                    pSrcAddr[u4Index2 + IGMP_ONE],
                                    IPVX_ADDR_FMLY_IPV4);
            if (u4FirstAddr > u4SecondAddr)
            {
                MEMSET (&TempIPvXAddr, IGMP_ZERO, sizeof (tIgmpIPvXAddr));
                IGMP_IPVX_ADDR_COPY (&TempIPvXAddr, &pSrcAddr[u4Index2]);
                IGMP_IPVX_ADDR_COPY (&pSrcAddr[u4Index2],
                                     &pSrcAddr[u4Index2 + IGMP_ONE]);
                IGMP_IPVX_ADDR_COPY (&pSrcAddr[u4Index2 + IGMP_ONE],
                                     &TempIPvXAddr);
                u1Swapped = IGMP_TRUE;
            }
        }

        if (u1Swapped == IGMP_FALSE)
        {
            break;
        }
    }
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpUtilIsSourceConfigured                       */
/*                                                                         */
/*     Description   :    This function checks whether the source is       */
/*                        configured manually                              */
/*                                                                         */
/*     Input(s)      :    u4IgmpGrpAddress, u4IgmpSrcAddress, i4IfIndex,   */
/*                        i4AddrType                                       */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    IGMP_TRUE or IGMP_FALSE                          */
/*                                                                         */
/***************************************************************************/
INT4
IgmpUtilIsSourceConfigured (UINT4 u4IgmpGrpAddress,
                            UINT4 u4IgmpSrcAddress,
                            INT4 i4IfIndex, INT4 i4AddrType)
{
    INT4                i4Status = IGMP_FALSE;
    tIgmpIface         *pIfNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pSrcLink = NULL;
    tIgmpIPvXAddr       GrpAddrX;
    tIgmpIPvXAddr       SrcAddrX;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpUtilIsSourceSSMMapped routine entry\n");

    if (IgmpGetPortFromIfIndex (i4IfIndex, i4AddrType, &u4Port) == IGMP_FAILURE)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "IgmpUtilIsSourceSSMMapped routine exit\n");
        return IGMP_FALSE;
    }

    pIfNode = IgmpGetIfNode (u4Port, i4AddrType);

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    if (pIfNode == NULL)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "IgmpUtilIsSourceSSMMapped routine exit\n");
        return IGMP_FALSE;
    }

    IGMP_IPVX_ADDR_CLEAR (&GrpAddrX);
    IGMP_IPVX_ADDR_CLEAR (&SrcAddrX);

    IGMP_IPVX_ADDR_INIT_IPV4 (GrpAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpGrpAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcAddress);

    pGrp = IgmpGrpLookUp (pIfNode, GrpAddrX);
    if (pGrp != NULL)
    {
        TMO_SLL_Scan (&(pGrp->srcList), pSrcLink, tTMO_SLL_NODE *)
        {
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcLink);

            if ((IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                         SrcAddrX) == IGMP_ZERO) &&
                (pSrc->u1SrcSSMMapped & IGMP_SRC_CONF))
            {
                i4Status = IGMP_TRUE;
                break;
            }
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpUtilIsSourceSSMMapped routine exit\n");
    return i4Status;
}
