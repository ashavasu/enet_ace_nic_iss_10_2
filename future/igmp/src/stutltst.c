/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stutltst.c,v 1.25 2017/02/06 10:45:29 siva Exp $
 *
 * Description:This file contains Low level routines  for stdigmp.mib  
 *
 *******************************************************************/

#include "include.h"
#include "stdigcon.h"
#include "stdigogi.h"
#include "midconst.h"
#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE;
#endif

/* LOW LEVEL Routines for Table : IgmpInterfaceTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceQueryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval (UINT4 *pu4ErrorCode,
                                                 INT4 i4IgmpInterfaceIfIndex,
                                                 INT4
                                                 i4IgmpInterfaceQuerierType,
                                                 UINT4
                                                 u4TestValIgmpInterfaceQueryInterval)
{
    UINT4               u4IgmpVersion = 0;
    UINT4               u4MldMaxRespTime = 0;
    UINT4               u4Port = 0;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval()\r \n");
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceQuerierType,
                                &u4Port) == IGMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface index %d is configured as upstream interface\r\n",
		    i4IgmpInterfaceIfIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4)
    {
        if ((u4TestValIgmpInterfaceQueryInterval <
             IGMP_IFACE_MIN_QUERY_INTERVAL) ||
            (u4TestValIgmpInterfaceQueryInterval >
             IGMP_IFACE_MAX_QUERY_INTERVAL))
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set Query interval between %d and %d",
                           IGMP_IFACE_MIN_QUERY_INTERVAL,
                           IGMP_IFACE_MAX_QUERY_INTERVAL);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_QUERY_INTERVAL_RANGE_ERR);
            return SNMP_FAILURE;
        }
    }
    else if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
    {
        if ((u4TestValIgmpInterfaceQueryInterval <
             MLD_IFACE_MIN_QUERY_INTERVAL) ||
            (u4TestValIgmpInterfaceQueryInterval >
             MLD_IFACE_MAX_QUERY_INTERVAL))
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set Query interval between %d and %d",
                           MLD_IFACE_MIN_QUERY_INTERVAL,
                           MLD_IFACE_MAX_QUERY_INTERVAL);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_QUERY_INTERVAL_RANGE_ERR);
            return SNMP_FAILURE;
        }
    }

    /*Retrive the IGMP Version */
    if (IgmpMgmtUtilNmhGetIgmpInterfaceVersion
        (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
         &u4IgmpVersion) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((u4IgmpVersion == IGMP_VERSION_1) &&
        (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMPV1_QUERY_INTERVAL_ERR);
        return SNMP_FAILURE;
    }
    /*Retrive the MLD Max Response Tome */
    if (IgmpMgmtUtilNmhGetIgmpInterfaceQueryMaxResponseTime
        (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
         &u4MldMaxRespTime) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (u4TestValIgmpInterfaceQueryInterval <= (u4MldMaxRespTime / IGMP_TEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4)
        {
            CLI_SET_ERR (CLI_IGMP_QUERY_INTERVAL_ERR);
        }
        else if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
        {
            CLI_SET_ERR (CLI_MLD_QUERY_INTERVAL_ERR);
        }
        return SNMP_FAILURE;
    }

    /*Check whether the query-interval is less than or equal to ten
     * if yes,check for IGMP Version*/
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryInterval()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4IgmpInterfaceIfIndex,
                                          INT4 i4IgmpInterfaceQuerierType,
                                          INT4 i4TestValIgmpInterfaceStatus)
{
    INT1                i1RetStatus;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    i1RetStatus = SNMP_FAILURE;

    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    UNUSED_PARAM (i4IgmpInterfaceQuerierType);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus()\r \n");
    switch (i4TestValIgmpInterfaceStatus)
    {
        case IGMP_CREATE_AND_GO:
        case IGMP_CREATE_AND_WAIT:
        case IGMP_ACTIVE:
        case IGMP_DESTROY:
        case IGMP_NOT_IN_SERVICE:
            i1RetStatus = SNMP_SUCCESS;
            break;
        default:
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "RowStatus value ranges from 1 to 6\n");
            break;
    }

    if (i1RetStatus == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_ROWSTATUS_OUT_OF_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceStatus()\r \n");
    return (i1RetStatus);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion (UINT4 *pu4ErrorCode,
                                           INT4 i4IgmpInterfaceIfIndex,
                                           INT4 i4IgmpInterfaceQuerierType,
                                           UINT4 u4TestValIgmpInterfaceVersion)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion()\r \n");
    if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
    {
        if ((u4TestValIgmpInterfaceVersion < MLD_VERSION_1) ||
            (u4TestValIgmpInterfaceVersion > MLD_VERSION_2))
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set Version either %d or %d",
                           MLD_VERSION_1, MLD_VERSION_2);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_MLD_VERSION_ERR);
            return SNMP_FAILURE;
        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion()\r \n");
        return SNMP_SUCCESS;
    }
    else if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4)
    {
        if ((u4TestValIgmpInterfaceVersion < IGMP_VERSION_1) ||
            (u4TestValIgmpInterfaceVersion > IGMP_VERSION_3))
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set Version between %d and %d",
                           IGMP_VERSION_1, IGMP_VERSION_3);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_VERSION_ERR);
            return SNMP_FAILURE;
        }

        if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                    IPVX_ADDR_FMLY_IPV4,
                                    &u4Port) == IGMP_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            return SNMP_FAILURE;
        }

        pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);
        if (pIfaceNode != NULL)
        {
            if ((IGMP_IS_CHANNEL_TRACK_ENABLED (pIfaceNode)) &&
                ((u4TestValIgmpInterfaceVersion == IGMP_VERSION_1) ||
                 (u4TestValIgmpInterfaceVersion == IGMP_VERSION_2)))
            {
                CLI_SET_ERR (CLI_IGMP_DISABLE_TRACKING);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        
	    if ((pIfaceNode->u1Version == IGMP_VERSION_3) &&
                (u4TestValIgmpInterfaceVersion <= IGMP_VERSION_2))
            {  
                if (IgmpChkIfGrpSrcExists(pIfaceNode) == IGMP_TRUE)
                {
                   *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (CLI_IGMP_INTF_GRP_SRC_FOUND);
                    return SNMP_FAILURE;
                }
            }
        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion()\r \n");
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceVersion()\r \n");
    return SNMP_FAILURE;
}

/***************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceQueryMaxResponseTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime (UINT4 *pu4ErrorCode,
                                                        INT4
                                                        i4IgmpInterfaceIfIndex,
                                                        INT4
                                                        i4IgmpInterfaceQuerierType,
                                                        UINT4
                                                        u4TestValIgmpInterfaceQueryMaxResponseTime)
{
    UINT4               u4MldQueryInterval = 0;
    UINT4               u4IgmpVersion = 0;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime()\r \n");
    if (IgmpMgmtUtilNmhGetIgmpInterfaceVersion
        (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
         &u4IgmpVersion) != SNMP_SUCCESS)
    {
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime()\r \n");
        return SNMP_FAILURE;
    }

    if ((u4IgmpVersion != IGMP_VERSION_1) ||
        (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6))
    {
        /*Retrive the MLD Max Response Tome */
        if (IgmpMgmtUtilNmhGetIgmpInterfaceQueryInterval
            (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
             &u4MldQueryInterval) != SNMP_SUCCESS)
        {
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime()\r \n");
            return SNMP_FAILURE;
        }
        if ((u4TestValIgmpInterfaceQueryMaxResponseTime / IGMP_TEN) >=
            u4MldQueryInterval)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4)
            {
                CLI_SET_ERR (CLI_IGMP_MAX_RESP_ERR);
            }
            else if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
            {
                CLI_SET_ERR (CLI_MLD_MAX_RESP_ERR);
            }
            return SNMP_FAILURE;
        }
    }

    if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4)
    {
        if (u4TestValIgmpInterfaceQueryMaxResponseTime >
            IGMP_MAX_QRY_MAX_RESP_TIME)
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set MaxQueryResponseTime between %d and %d",
                           IGMP_MIN_QRY_MAX_RESP_TIME,
                           IGMP_MAX_QRY_MAX_RESP_TIME);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_MAX_RESP_RANGE_ERR);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
    {
        if (((INT4) u4TestValIgmpInterfaceQueryMaxResponseTime <
             MLD_MIN_QRY_MAX_RESP_TIME) ||
            ((INT4) u4TestValIgmpInterfaceQueryMaxResponseTime >
             MLD_MAX_QRY_MAX_RESP_TIME))
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set MaxQueryResponseTime between %d and %d",
                           MLD_MIN_QRY_MAX_RESP_TIME,
                           MLD_MAX_QRY_MAX_RESP_TIME);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_MAX_RESP_RANGE_ERR);
            return SNMP_FAILURE;
        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime()\r \n");
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceQueryMaxResponseTime()\r \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpInterfaceProxyIfIndex
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceProxyIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceProxyIfIndex (UINT4 *pu4ErrorCode,
                                                INT4 i4IgmpInterfaceIfIndex,
                                                INT4 i4IgmpInterfaceQuerierType,
                                                INT4
                                                i4TestValIgmpInterfaceProxyIfIndex)
{

    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    UNUSED_PARAM (i4TestValIgmpInterfaceProxyIfIndex);
    UNUSED_PARAM (i4IgmpInterfaceQuerierType);
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "This object not supported by IGMP as there is no proxy "
              "support\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceRobustness
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness (UINT4 *pu4ErrorCode,
                                              INT4 i4IgmpInterfaceIfIndex,
                                              INT4 i4IgmpInterfaceQuerierType,
                                              UINT4
                                              u4TestValIgmpInterfaceRobustness)
{
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    UNUSED_PARAM (i4IgmpInterfaceQuerierType);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness()\r \n");
    if ((u4TestValIgmpInterfaceRobustness < IGMP_MIN_ROB_VARIABLE) ||
        (u4TestValIgmpInterfaceRobustness > IGMP_MAX_ROB_VARIABLE))
    {
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                       "FAILED: Out of Range Value, "
                       "set  between %d and %d",
                       IGMP_MIN_QRY_MAX_RESP_TIME, IGMP_MAX_QRY_MAX_RESP_TIME);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_ROBUSTNESS_RANGE_ERR);
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceRobustness()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                testValIgmpInterfaceLastMembQueryIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl (UINT4 *pu4ErrorCode,
                                                      INT4
                                                      i4IgmpInterfaceIfIndex,
                                                      INT4
                                                      i4IgmpInterfaceQuerierType,
                                                      UINT4
                                                      u4TestValIgmpInterfaceLastMembQueryIntvl)
{
    UINT4               u4Version = 0;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
            "Entering IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl()\r\n");
    if (IgmpMgmtUtilNmhGetIgmpInterfaceVersion (i4IgmpInterfaceIfIndex,
                                                i4IgmpInterfaceQuerierType,
                                                &u4Version) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((u4Version == IGMP_VERSION_1) &&
        (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Error: Configuration not allowed for IGMPV1 interface");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMPV1_LAST_MEM_QUERY_INTVL_ERR);
        return SNMP_FAILURE;
    }

    if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4)
    {
        if ((u4TestValIgmpInterfaceLastMembQueryIntvl >
            IGMP_MAX_LAST_MEMBER_QIVAL) || (u4TestValIgmpInterfaceLastMembQueryIntvl == IGMP_ZERO))
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set  between %d and %d",
                           IGMP_MIN_LAST_MEMBER_QIVAL,
                           IGMP_MAX_LAST_MEMBER_QIVAL);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_LAST_MEM_QUERY_INTVL_RANGE_ERR);
            return SNMP_FAILURE;
        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl()\r\n");
        return SNMP_SUCCESS;
    }
    else if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
    {
        if (((INT4) u4TestValIgmpInterfaceLastMembQueryIntvl <
             MLD_MIN_LAST_MEMBER_QIVAL) ||
            (u4TestValIgmpInterfaceLastMembQueryIntvl >
             MLD_MAX_LAST_MEMBER_QIVAL))
        {
            IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "FAILED: Out of Range Value, "
                           "set  between %d and %d",
                           MLD_MIN_LAST_MEMBER_QIVAL,
                           MLD_MAX_LAST_MEMBER_QIVAL);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_IGMP_LAST_MEM_QUERY_INTVL_RANGE_ERR);
            return SNMP_FAILURE;
        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl()\r\n");
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpInterfaceLastMembQueryIntvl()\r\n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhDepv2IgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhDepv2IgmpInterfaceTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IgmpCacheTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpCacheSelf
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                testValIgmpCacheSelf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpCacheSelf (UINT4 *pu4ErrorCode,
                                    tIgmpIPvXAddr u4IgmpCacheAddress,
                                    INT4 i4IgmpCacheIfIndex,
                                    INT4 i4IgmpInterfaceQuerierType,
                                    INT4 i4TestValIgmpCacheSelf)
{
    UNUSED_PARAM (u4IgmpCacheAddress);
    UNUSED_PARAM (i4IgmpCacheIfIndex);
    UNUSED_PARAM (i4TestValIgmpCacheSelf);
    UNUSED_PARAM (i4IgmpInterfaceQuerierType);
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpCacheStatus
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                testValIgmpCacheStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpCacheStatus (UINT4 *pu4ErrorCode,
                                      tIgmpIPvXAddr u4IgmpCacheAddress,
                                      INT4 i4IgmpCacheIfIndex,
                                      INT4 i4IgmpCacheAddrType,
                                      INT4 i4TestValIgmpCacheStatus)
{
    INT1                i1Status;
    UINT1               u1GrpFound = 0;
    UINT4               u4Port = 0;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4TmpCacheAddress = IGMP_ZERO;
    tIp6Addr            Cache6Addr;
#ifdef MULTICAST_SSM_ONLY
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    INT4                i4Status = IGMP_FALSE;
#endif
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    i1Status = SNMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhTestv2IgmpCacheStatus()\r\n");
    if (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IGMP_IP_COPY_FROM_IPVX (&u4TmpCacheAddress, u4IgmpCacheAddress,
                                IPVX_ADDR_FMLY_IPV4);
        if ((u4TmpCacheAddress > IGMP_END_OF_MCAST_GRP_ADDR) ||
            (u4TmpCacheAddress < IGMP_START_OF_MCAST_GRP_ADDR))
        {
            CLI_SET_ERR (CLI_IGMP_INVALID_GRP_ADDRESS);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        IgmpFillMem (&Cache6Addr, IGMP_ZERO, sizeof (tIp6Addr));
        IGMP_IP_COPY_FROM_IPVX (&Cache6Addr, u4IgmpCacheAddress,
                                IPVX_ADDR_FMLY_IPV6);
        if (IS_ADDR_MULTI (Cache6Addr) == FALSE)
        {
            /* Invalid Group Destination Addres */
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Invalid Group.!!!!!!!!!!!!\n");
            CLI_SET_ERR (CLI_IGMP_INVALID_GRP_ADDRESS);
            return SNMP_FAILURE;
        }
    }
    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex,
                                i4IgmpCacheAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface is configured as upstream interface\r\n");
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    if (pIfaceNode == NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                       "Interface %d not present", i4IgmpCacheIfIndex);
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfaceNode, u4IgmpCacheAddress);
    if (pGrp != NULL)
    {
        u1GrpFound = IGMP_TRUE;
    }

    /* We do not support create and wait, not-in-service and active 
     * status for this table. 
     * only create-and-go and destroy
     */
    switch (i4TestValIgmpCacheStatus)
    {
        case IGMP_CREATE_AND_GO:
            if ((u1GrpFound == IGMP_TRUE)&& (pGrp->u1StaticFlg == IGMP_TRUE))
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                          "IGMP group cannot be added,"
                          " Group membership already learned \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else if ((u1GrpFound == IGMP_TRUE) && (pGrp->u1DynamicFlg == IGMP_TRUE))
            {
                /*User trying to configure static entry. */
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Dynamic entry already present\n");
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
            		 "Exiting  IgmpMgmtUtilNmhTestv2IgmpCacheStatus()\r \n");
                return SNMP_SUCCESS;

            }
            else
            {
#ifdef MULTICAST_SSM_ONLY
                if (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4)
                {
			    pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (u4IgmpCacheAddress, &i4Status);
                if (i4Status == IGMP_FALSE)
                {
				    if (IGMP_GLOBAL_STATUS() == IGMP_DISABLE)
				    {
					    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
							    "IGMP group cannot be added, "
							    "since IGMP status is disabled\n");
					    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
					    CLI_SET_ERR (CLI_IGMP_SSMMAP_IGMP_NOT_ENABLE);

				    }
				    else if (IGMP_SSM_MAP_GLOBAL_STATUS() == IGMP_DISABLE)
				    {
					    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
							    "IGMP group cannot be added, "
							    "since SSM Mapping is disabled\n");
					    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
					    CLI_SET_ERR (CLI_IGMP_SSMMAP_MCAST_SSM_NOT_ENABLE);

				    }
				    else
				    {
                    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                            "IGMP group cannot be added, "
                            "since no static SSM mapping is configured \n");
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    CLI_SET_ERR (CLI_IGMP_SSMMAP_MCAST_SSM_NO_STAR_G);
				    }
                    return SNMP_FAILURE;
                }
                }   
#endif
                return SNMP_SUCCESS;
            }
            break;
        case IGMP_CREATE_AND_WAIT:
            i1Status = SNMP_SUCCESS;
            break;
        case IGMP_ACTIVE:
        case IGMP_DESTROY:
            if ((u1GrpFound == IGMP_FALSE) || ((u1GrpFound == IGMP_TRUE) && 
                (pGrp->u1StaticFlg == IGMP_FALSE)))
            {
                CLI_SET_ERR (CLI_IGMP_NO_GROUP_ERR);
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }
            break;
        default:
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "RowStatus values are 4 & 6 only\n");
            break;
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpCacheStatus()\r \n");
    return (i1Status);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhDepv2IgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhDepv2IgmpCacheTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IgmpSrcListTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhTestv2IgmpSrcListStatus
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress

                The Object 
                testValIgmpSrcListStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhTestv2IgmpSrcListStatus (UINT4 *pu4ErrorCode,
                                        tIgmpIPvXAddr u4IgmpSrcListAddress,
                                        INT4 i4IgmpSrcListIfIndex,
                                        INT4 i4IgmpSrcListAddrType,
                                        tIgmpIPvXAddr u4IgmpSrcListHostAddress,
                                        INT4 i4TestValIgmpSrcListStatus)
{
    INT1                i1Status;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pCurGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    UINT4               u4Port = 0;
    UINT1               u1GrpFound = 0;
    UINT1               u1SrcFound = 0;
    UINT4               u4TmpSrcListAddress = 0;
    INT4                i4Status = 0;
    tIp6Addr            SrcList6Addr;
	UINT4 				u4Source = 0;
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    INT4                i4MapStatus = IGMP_FALSE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    i1Status = SNMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMgmtUtilNmhTestv2IgmpSrcListStatus()\r \n");
    if (IgmpGetPortFromIfIndex (i4IgmpSrcListIfIndex,
                                i4IgmpSrcListAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpSrcListAddrType);

    if ((pIfaceNode == NULL) || ((pIfaceNode->u1Version != IGMP_VERSION_3) &&
                                 (i4IgmpSrcListAddrType ==
                                  IPVX_ADDR_FMLY_IPV4)))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                       "Interface %d not present or not in version v3 mode",
                       i4IgmpSrcListIfIndex);
        return SNMP_FAILURE;
    }

    pCurGrp = IgmpGrpLookUp (pIfaceNode, u4IgmpSrcListAddress);
    if (pCurGrp != NULL)
    {
        u1GrpFound = IGMP_TRUE;
        pGrp = pCurGrp;
        TMO_SLL_Scan (&(pCurGrp->srcList), pSrc, tIgmpSource *)
        {
            if (IGMP_IPVX_ADDR_COMPARE (u4IgmpSrcListHostAddress,
                                        pSrc->u4SrcAddress) == IGMP_ZERO)
            {
                u1SrcFound = IGMP_TRUE;
                break;
            }
        }
    }

    /* Get the Port from the IfIndex */
    if (i4IgmpSrcListAddrType == IPVX_ADDR_FMLY_IPV4)
    {
		/* Validate Source address*/
	    IGMP_IP_COPY_FROM_IPVX (&u4Source, u4IgmpSrcListHostAddress,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        if(IgmpValidateV4SourceAddress (u4Source, IGMP_FALSE) == IGMP_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        	CLI_SET_ERR (CLI_IGMP_INVALID_SOURCE);
            return SNMP_FAILURE;
        }

        IGMP_IP_COPY_FROM_IPVX (&u4TmpSrcListAddress, u4IgmpSrcListAddress,
                                IPVX_ADDR_FMLY_IPV4);
        if ((u4TmpSrcListAddress > IGMP_END_OF_MCAST_GRP_ADDR) ||
            (u4TmpSrcListAddress < IGMP_START_OF_MCAST_GRP_ADDR) ||
	    (u4TmpSrcListAddress == IGMP_INVALID_SSM_GRP_ADDR))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	    pIfaceNode->u4IgmpInvalidSSMPkts++;
            CLI_SET_ERR (CLI_IGMP_MULT_ADDR_ERR);
            return SNMP_FAILURE;
        }

        /* check whether SSM mapping is configured for the group */
        pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (u4IgmpSrcListAddress, &i4MapStatus);

	IGMP_CHK_IF_SSM_RANGE (u4TmpSrcListAddress, i4Status);
        if (i4Status == IGMP_SSM_RANGE)
        {
           if((IGMP_IPVX_ADDR_COMPARE 
                    (u4IgmpSrcListHostAddress, gIPvXZero) == IGMP_ZERO) &&
                    ((i4MapStatus == IGMP_FALSE) || (pSSMMappedGrp == NULL)))
           {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                                "(*,G) Static Group creation is not allowed for SSM range" 
                                " without already configured IGMP SSM Mapping\n");
                pIfaceNode->u4IgmpInvalidSSMPkts++;
                CLI_SET_ERR (CLI_IGMP_SSM_RECORD_TYPE);
                return SNMP_FAILURE;    
           }  	 
           if ( pGrp != NULL)
           {
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Group %s in SSM Range \n",
		               IgmpPrintIPvxAddress(pGrp->u4Group));
                if (pGrp->u1StaticFlg != IGMP_TRUE)
                {
           pIfaceNode->u4IgmpInSSMPkts++;
        }
           }
        }
#ifdef MULTICAST_SSM_ONLY
        if ((i4TestValIgmpSrcListStatus == IGMP_CREATE_AND_GO) &&
            (u4Source == IGMP_ZERO) &&
            (i4MapStatus == IGMP_FALSE))
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                    "IGMP group cannot be added, "
                    "since no static SSM mapping is configured \n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IGMP_SSMMAP_MCAST_SSM_NO_STAR_G);
            return SNMP_FAILURE;
        }
#endif
    }
    else if (i4IgmpSrcListAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        IgmpFillMem (&SrcList6Addr, IGMP_ZERO, sizeof (tIp6Addr));
        IGMP_IP_COPY_FROM_IPVX (&SrcList6Addr, u4IgmpSrcListAddress,
                                IPVX_ADDR_FMLY_IPV6);
        if (IS_ADDR_MULTI (SrcList6Addr) == FALSE)
        {
            /* Invalid Group Destination Addres */
            if ( pGrp != NULL)
            {
                IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Invalid Group %s \n",IgmpPrintIPvxAddress(pGrp->u4Group));
            }
            CLI_SET_ERR (CLI_IGMP_INVALID_GRP_ADDRESS);
            return SNMP_FAILURE;
        }
    }

    switch (i4TestValIgmpSrcListStatus)
    {
        case IGMP_CREATE_AND_GO:
            if ((u1GrpFound == IGMP_TRUE) && (u1SrcFound == IGMP_TRUE) &&
                (pGrp->u1StaticFlg == IGMP_TRUE) && (pSrc->u1SrcSSMMapped & IGMP_SRC_CONF))
            {
		    if (u4Source == IGMP_ZERO)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
					    "IGMP Group membership already learned \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    CLI_SET_ERR (CLI_IGMP_GRP_ALREADY_LEARNED);
                return SNMP_FAILURE;
            }
		    else
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
					    "Configured IGMP group and source is already learned \n");
			    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    CLI_SET_ERR (CLI_IGMP_GRP_AND_SRC_ALREADY_LEARNED);
                return SNMP_FAILURE;
            }
            }
            i1Status = SNMP_SUCCESS;
            break;

        case IGMP_CREATE_AND_WAIT:
        case IGMP_ACTIVE:
        case IGMP_DESTROY:
            if ((u1GrpFound == IGMP_FALSE) || ((u1GrpFound == IGMP_TRUE) && 
                (pGrp->u1StaticFlg == IGMP_FALSE)))
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                          "IGMP Group membership does not exists\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_IGMP_NO_GROUP_ERR);
                return SNMP_FAILURE;
            }

            if ((u1GrpFound == IGMP_TRUE) && (u4Source != IGMP_ZERO) &&
                ((u1SrcFound == IGMP_FALSE) || ((u1SrcFound == IGMP_TRUE) && 
                (pSrc->u1StaticFlg == IGMP_FALSE))))
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                          "Configured IGMP Group and Source does not exists\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_IGMP_NO_GRP_AND_SRC);
                return SNMP_FAILURE;
            }
            i1Status = SNMP_SUCCESS;
            break;
        default:
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "RowStatus value ranges from 1 to 6\n");
            break;
    }

    if (i1Status == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMgmtUtilNmhTestv2IgmpSrcListStatus()\r \n");
    return (i1Status);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhDepv2IgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhDepv2IgmpSrcListTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
