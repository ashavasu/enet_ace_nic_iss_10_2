/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpsz.c,v 1.5 2015/02/13 11:13:52 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _IGMPSZ_C
#include "igmpinc.h"

extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
IgmpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpSizingMemCreateMemPools()\r \n");
    for (i4SizingId = 0; i4SizingId < IGMP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsIGMPSizingParams[i4SizingId].u4StructSize,
                              FsIGMPSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(IGMPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
            IGMP_TRC_NAME(IGMP_CNTRL_MODULE),IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
            "for sizing mem pools \r\n");
            IgmpSizingMemDeleteMemPools ();
            MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpSizingMemCreateMemPools()\r \n");
            return OSIX_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpSizingMemCreateMemPools()\r \n");
    return OSIX_SUCCESS;
}

INT4
IgmpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsIGMPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, IGMPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
IgmpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpSizingMemDeleteMemPools()\r \n");
    for (i4SizingId = 0; i4SizingId < IGMP_MAX_SIZING_ID; i4SizingId++)
    {
        if (IGMPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IGMPMemPoolIds[i4SizingId]);
            IGMPMemPoolIds[i4SizingId] = 0;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                                "Exiting IgmpSizingMemDeleteMemPools()\r \n");
    return;
}
