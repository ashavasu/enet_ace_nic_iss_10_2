/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *  $Id: igmpmem.c,v 1.17 2016/06/24 09:42:20 siva Exp $
 *
 * Description:File contains the routines required for memory related functions 
 *
 *******************************************************************/
#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE ;
#endif
/***************************************************************************/
/*                                                                         */
/* Function Name           :    IgmpGlobalMemInit                           */
/*                                                                         */
/* Description               : Allocates the memory for the                   */
/*                             Input Queue, group node, source node           */
/*                                                                       */
/* Input (s)                 : None                                          */
/*                                                                         */
/* Output (s)                : None                                         */
/*                                                                         */
/* Global Variables Referred : gpIgmpMemPool                               */
/*                                                                         */
/* Global Variables Modified : gpIgmpMemPool                               */
/*                                                                         */
/* Returns                   : IGMP_OK, if memory allocation successful  */
/*                                IGMP_NOT_OK, Otherwise                  */
/****************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGlobalMemInit                           **/
/** $$TRACE_PROCEDURE_LEVEL = INTMD                                       **/
/***************************************************************************/

INT1
IgmpGlobalMemInit (VOID)
{

   MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering IgmpGlobalMemInit routine Entry\n");

    if (IgmpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE, 
		       IgmpGetModuleName(IGMP_OS_RES_MODULE),
                       "Mem Pool Creation Failure for Schedule query Sources \n");
        SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                              IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
                              "for global mem init\r\n");
	return IGMP_NOT_OK;
    }

    gIgmpMemPool.IgmpQueryId = IGMPMemPoolIds[MAX_IGMP_QUERIES_SIZING_ID];
    gIgmpMemPool.IgmpIfId = IGMPMemPoolIds[MAX_IGMP_LOGICAL_IFACES_SIZING_ID];
    gIgmpMemPool.IgmpQPoolId = IGMPMemPoolIds[MAX_IGMP_Q_DEPTH_SIZING_ID];
    gIgmpMemPool.IgmpGroupId = IGMPMemPoolIds[MAX_IGMP_MCAST_GRPS_SIZING_ID];
    gIgmpMemPool.IgmpSourceId = IGMPMemPoolIds[MAX_IGMP_MCAST_SRCS_SIZING_ID];
    gIgmpMemPool.IgmpSrcSetArrayId = IGMPMemPoolIds
        [MAX_IGMP_SRCS_FOR_MRP_PER_GROUP_SIZING_ID];
    gIgmpMemPool.IgmpSrcReporterId = IGMPMemPoolIds
        [MAX_IGMP_MCAST_SRS_REPORTERS_SIZING_ID];
    gIgmpMemPool.IgmpSchQueryId = IGMPMemPoolIds
        [MAX_IGMP_MCAST_SCHED_QUERIES_SIZING_ID];
    gIgmpMemPool.IgmpSchQuerySourceId = IGMPMemPoolIds
        [MAX_IGMP_SCHED_QUERY_SRCS_SIZING_ID];
    gIgmpMemPool.IgmpAddrStructArrayId = IGMPMemPoolIds
        [MAX_IGMP_ADDR_ARRAY_STR_SIZING_ID];
    gIgmpMemPool.IgmpU4SrcsToPktId = IGMPMemPoolIds
        [MAX_SRCS_TO_PKT_BLKS_U4_SIZING_ID];
    gIgmpMemPool.IgmpU1SrcsToPktId = IGMPMemPoolIds
        [MAX_SRCS_TO_PKT_BLKS_U1_SIZING_ID];
    gIgmpMemPool.IgmpRecvBufId = IGMPMemPoolIds
        [MAX_IGMP_RECVBUF_BLKS_SIZING_ID];
    gIgmpMemPool.IgmpRmQueId = IGMPMemPoolIds[MAX_IGMP_RM_QUE_DEPTH_SIZING_ID];
    gIgmpMemPool.IgmpGrpLstPoolId =
        IGMPMemPoolIds[MAX_IGMP_GROUPLIST_NODES_SIZING_ID];
    gIgmpMemPool.IgmpHostMcastGrpId =
        IGMPMemPoolIds[MAX_IGMP_HOST_MCAST_GROUP_SIZING_ID];
    gIgmpMemPool.IgmpSSMMapGrpId =
        IGMPMemPoolIds[MAX_IGMP_SSM_MAPPINGS_SIZING_ID];
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting IgmpGlobalMemInit routine Exit\n");
    return IGMP_OK;

}

/****************************************************************************
* Function Name             : IgmpMemAllocate                               
*                                                                          
* Description               : Allocates a block of memory for a node from  
*                             the MemPool. If the MemPool exhausts, then   
*                             dynamic allocation is done using 'malloc'.   
*                                                                          
* Input (s)                 : MemPoolId   - MemPool struct 
*                                                                          
* Output (s)                : ppu1Block - Pointer to the node              
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : IGMP_OK, if the allocation successful,   
*                             IGMP_NOT_OK, Otherwise.                      
****************************************************************************/
INT4
IgmpMemAllocate (tMemPoolId MemPoolId, UINT1 **ppu1Block)
{

    UINT4               u4MemAllocateStatus;
    u4MemAllocateStatus = IGMP_OK;

   MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering IgmpMemAllocate routine Entry\n");

    /* Allocate a block from the MemPool.
     */

    if (MemAllocateMemBlock (MemPoolId, ppu1Block) == MEM_FAILURE)

    {

        IGMP_DBG1(IGMP_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
                  "-E- MemPool Exhausts. Cannot allocate memory for mempool Id %d\n",
                   MemPoolId);
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
                         "for pool id %d\r\n", MemPoolId);
	u4MemAllocateStatus = (UINT4) IGMP_NOT_OK;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting IgmpMemAllocate routine Exit\n");
    return u4MemAllocateStatus;

}                                /* End of the function - IgmpMemAllocate () */

/****************************************************************************/
/* Function Name             : IgmpMemRelease                                */
/*                                                                          */
/* Description               : Releases the block of memory from the        */
/*                             MemPool. If the allocation is not from the   */
/*                             MemPool, it does the 'freemem'.              */
/*                                                                          */
/* Input (s)                 : pMemPool     - Pointer to the MemPool struct */
/*                             pu1Block     - Pointer to node allocated     */
/*                                                                          */
/* Output (s)                : None                                         */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns                   : IGMP_OK, if release successful           */
/*                             IGMP_NOT_OK, Otherwise                       */
/****************************************************************************/

INT4
IgmpMemRelease (tMemPoolId MemPoolId, UINT1 *pu1Block)
{

    INT4                i4Status = IGMP_ZERO;
    UINT4               u4MemReleaseStatus = IGMP_ZERO;

   MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		"Entering IgmpMemRelease routine Entry\n");
    i4Status = IGMP_OK;

    /* Release the block to the MemPool. 
     */
    u4MemReleaseStatus = MemReleaseMemBlock (MemPoolId, pu1Block);

    if (u4MemReleaseStatus == MEM_FAILURE)
    {
        IGMP_DBG1(IGMP_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
                  "MemBlock Release is Failure for mempool Id %d\n", MemPoolId );
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_REL_FAIL],
                        "for pool id %d\r\n", MemPoolId);
	i4Status = IGMP_NOT_OK;
    }
    else
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_OS_RES_MODULE, 
		       IgmpGetModuleName(IGMP_OS_RES_MODULE),
                       "MemBlock Release is Successful\n");
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		"Exiting IgmpMemRelease routine Exit\n");

    return i4Status;

}                                /* End of the function - IgmpMemRelease () */



/****************************************************************************
* Function Name             : IgmpMemAllocMemBlk                               
*                                                                          
* Description               : Allocates a block of memory for a node from  
*                             the MemPool.    
*                                                                          
* Input (s)                 : MemPoolId   - MemPool struct 
*                                                                          
* Output (s)                : None              
*                                                                          
* Global Variables Referred : None                                         
*                                                                          
* Global Variables Modified : None                                         
*                                                                          
* Returns                   : Pointer to the allocated pBuf, or NULL if 
* 							  unsuccessful.  
****************************************************************************/


VOID * 
IgmpMemAllocMemBlk (tMemPoolId PoolId, UINT4 u4Size)
{
	UINT1 *pBuf = NULL;
	
	pBuf = (UINT1*) MemAllocMemBlk ((tMemPoolId)PoolId);
	if(pBuf != NULL)
	{
		MEMSET(pBuf, 0, u4Size);
	}
	else
	{
        IGMP_DBG1(IGMP_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
                  "-E- MemPool Exhausts. Cannot allocate memory for mempool Id %d\n",
                   PoolId);
	}
	return (VOID *) (pBuf);
}



