/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpout.c,v 1.21 2017/02/06 10:45:27 siva Exp $
 *
 * Description:This file contains the routines required for
 *             sending the IGMP output packets
 *
 *******************************************************************/

#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE ;
#endif

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpPutHdr                                         */
/*                                                                         */
/*     Description   : Fill the IP Buffer with IGMP packet fields.        */
/*                                                                         */
/*     Input(s)      :  Pointer to the igmppakcet structure.               */
/*                      Pointer to the IPBUF that is going to be sent out.*/
/*                                                                         */
/*     Output(s)     : Filled up IGMP_BUF to be transmitted.                */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpPutHdr                                  **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpPutHdr (tIgmpPkt * pIgmp, tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2QryLen)
{
    UINT2               u2CkSum = IGMP_ZERO;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIgmp, IGMP_IP_HDR_RTR_OPT_LEN,
                               IGMP_HEADER_LEN);
    u2CkSum = OSIX_HTONS (IgmpCalcCheckSum (pBuf, u2QryLen,
                                            IGMP_IP_HDR_RTR_OPT_LEN));
    CRU_BUF_Copy_OverBufChain (pBuf,
                               (UINT1 *) &u2CkSum,
                               IGMP_IP_HDR_RTR_OPT_LEN +
                               IGMP_PACKET_CHKSUM_OFFSET, sizeof (UINT2));
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpPutV3Hdr                                         */
/*                                                                         */
/*     Description   : Fill the IP Buffer with IGMP packet fields.        */
/*                                                                         */
/*     Input(s)      :  Pointer to the igmppakcet structure.               */
/*                      Pointer to the IPBUF that is going to be sent out.*/
/*                                                                         */
/*     Output(s)     : Filled up IGMP_BUF to be transmitted.                */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpPutV3Hdr                                  **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpPutV3Hdr (tIgmpV3Query * pIgmp, tCRU_BUF_CHAIN_HEADER * pBuf,
              UINT2 u2QryLen)
{
    UINT2               u2CkSum = IGMP_ZERO;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pIgmp, IGMP_IP_HDR_RTR_OPT_LEN,
                               sizeof (tIgmpV3Query));
    u2CkSum = OSIX_HTONS (IgmpCalcCheckSum (pBuf, u2QryLen,
                                            IGMP_IP_HDR_RTR_OPT_LEN));
    CRU_BUF_Copy_OverBufChain (pBuf,
                               (UINT1 *) &u2CkSum,
                               IGMP_IP_HDR_RTR_OPT_LEN +
                               IGMP_PACKET_CHKSUM_OFFSET, sizeof (UINT2));
}

/****************************************************************************/
/* Function           : IgmpFormIPHdr                                       */
/*  Description       : This function forms the IP header of IGMP Query     */
/*                    : packet.                                             */
/* Input(s)           : pBuf - IGMP buffer                                  */
/*                    : u4Src - Source IP address                           */
/*                    : u4Dest - Destination Ip address                     */
/*                    : u2Len - Length of PDU                               */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : None                                                */
/****************************************************************************/
VOID
IgmpFormIPHdr (tCRU_BUF_CHAIN_HEADER * pBuf,
               UINT4 u4Src, UINT4 u4Dest, UINT1 u1PktType, UINT2 u2Len)
{
    t_IP_HEADER         IpHdr;
    UINT1               u1DfFlag = FALSE;

    UNUSED_PARAM (u1DfFlag);
    MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));

    IpHdr.u1Ver_hdrlen = IGMP_IP_RTR_OPT_VERS_AND_HLEN;
    IpHdr.u1Tos = IGMP_TOS;
    IpHdr.u2Totlen = u2Len;
    IpHdr.u2Totlen = OSIX_HTONS (IpHdr.u2Totlen);
    IpHdr.u2Id = 0;
    IpHdr.u2Fl_offs = 0;
    IpHdr.u2Fl_offs = OSIX_HTONS (IpHdr.u2Fl_offs);
    IpHdr.u1Ttl = IGMP_PACKET_TTL;
    IpHdr.u1Proto = IGMP_PROTID;
    IpHdr.u2Cksum = 0;            /* Clear Checksum */
    IpHdr.u4Src = OSIX_HTONL (u4Src);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpFormIPHdr()\r \n");

    if (u1PktType == IGMP_PACKET_QUERY)
    {
        IpHdr.u4Dest = OSIX_HTONL (IGMP_ALL_HOSTS_GROUP);
    }
    else if (u1PktType == IGMP_PACKET_LEAVE)
    {
        IpHdr.u4Dest = OSIX_HTONL (IGMP_ALL_ROUTERS_GROUP);
    }
    else if (u1PktType == IGMP_PACKET_V3_REPORT)
    {
        IpHdr.u4Dest = OSIX_HTONL (IGMP_ALL_V3_ROUTERS_GROUP);
    }
    else
    {
        IpHdr.u4Dest = OSIX_HTONL (u4Dest);
    }

    /* Router alert option */
    IpHdr.u1Options[0] = 0x94;
    IpHdr.u1Options[1] = 0x04;
    IpHdr.u1Options[2] = 0x00;
    IpHdr.u1Options[3] = 0x00;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IpHdr,
                               IGMP_ZERO, IGMP_IP_HDR_RTR_OPT_LEN);

    IpHdr.u2Cksum = IgmpCalcCheckSum (pBuf, IGMP_IP_HDR_RTR_OPT_LEN, IGMP_ZERO);

    IpHdr.u2Cksum = OSIX_HTONS (IpHdr.u2Cksum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IpHdr.u2Cksum,
                               IGMP_TEN, IGMP_TWO);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpFormIPHdr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :        IgmpSendQuery                                */
/*                                                                         */
/*     Description   :      This function sends general/group specific     */
/*                         Query based on the group passed, on the given   */
/*                         interface                                       */
/*                                                                         */
/*     Input(s)      :    OutInterfaceId, GroupAddress.                    */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :   Returns IGMP_OK or IGMP_NOT_OK                    */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpSendQuery                               **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
INT1
IgmpSendQuery (tIgmpIface * pIfNode, UINT4 u4Group)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIgmpIPvXAddr       u4DestAddrX;
    UINT4               u4DestAddr = IGMP_ZERO;
#ifndef LNXIP4_WANTED
    UINT4               u4SrcAddr = IGMP_ZERO;
#endif
    UINT2               u2QueryLength = IGMP_ZERO;
    tIgmpPkt            Igmp;
    tIgmpV3Query        IgmpV3Query;
    tIgmpIPvXAddr       u4SrcAddrX;
    tIgmpIPvXAddr       u4GrpAddrX;
    tIgmpIPvXAddr       GroupIpvxAddr;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpSendQuery()\r \n");
    
    IGMP_IPVX_ADDR_CLEAR (&u4SrcAddrX);
#ifndef LNXIP4_WANTED
    IGMP_IPVX_ADDR_INIT_IPV4 (u4SrcAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
		    (UINT1 *) &u4SrcAddr);
    if (IGMP_GET_NODE_STATUS () != RM_ACTIVE)
    {
	/* Query should not be triggered from standby */
	return (IGMP_OK);
    }
    /* If source address is 0.0.0.0, do not initiate query since this will default
     * to queries being sent on VLAN 1 member ports even if IGMP is not enabled on
     * VLAN 1.
     */
    if ((IGMP_IPVX_ADDR_COMPARE (pIfNode->u4IfAddr, u4SrcAddrX)) == IGMP_ZERO)
    {
	    /* Check source address of associated interface in case of unnumbered
	     * interface. Required locks are taken inside the function so they
	     * are ignored here.
	     */
	    u4SrcAddr = IGMP_ZERO;
	    if (IpGetSrcAddressOnInterface ((UINT2) pIfNode->u4IfIndex,
				    (UINT4) IGMP_ZERO, &u4SrcAddr) == IP_SUCCESS)
	    {
		    if (u4SrcAddr == IP_ANY_ADDR)
		    {
			    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
					    "Source Address of Upstream interface is 0.0.0.0 "
					    "so Query will not be sent on this interface \r\n");
			    IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
					    IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
					    "Source Address of Upstream interface is 0.0.0.0 "
					    "so Query will not be sent on this interface \r\n");
			    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
					    "Exiting IgmpSendQuery() \r\n");
			    return (IGMP_OK);
		    }
	    }
	    else
	    {
		    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
				    "Source Address of Upstream interface is 0.0.0.0 "
				    "so Query will not be sent on this interface \r\n");
		    IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
				    IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
				    "Source Address of Upstream interface is 0.0.0.0 "
				    "so Query will not be sent on this interface \r\n");
		    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
				    "Exiting IgmpSendQuery() \r\n");
		    return (IGMP_OK);
	    }
    }
#endif

    IGMP_IPVX_ADDR_CLEAR (&u4GrpAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4GrpAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4Group);
    u4DestAddr = u4Group;
    IGMP_IPVX_ADDR_CLEAR (&u4DestAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4DestAddrX, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4Group);
    if (pIfNode->u1Version == IGMP_VERSION_3)
    {
        u2QueryLength = (UINT2) (sizeof (tIgmpV3Query) +
                                 IGMP_IP_HDR_RTR_OPT_LEN);
    }
    else
    {
        u2QueryLength = IGMP_HEADER_LEN + IGMP_IP_HDR_RTR_OPT_LEN;
    }
    pBuf = CRU_BUF_Allocate_MsgBufChain (u2QueryLength, IGMP_ZERO);

    if (pBuf == NULL)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_BUFFER_MODULE, IgmpGetModuleName(IGMP_BUFFER_MODULE),
                  	"Buffer Alloc Failure.\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Failure sending General Query\n");
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                         IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
                        "for u4Group %d\r\n", u4Group);
	return (IGMP_NOT_OK);
    }

    /*  Fill the Query */
    switch (pIfNode->u1Version)
    {
        case IGMP_VERSION_1:
            Igmp.u1PktType = IGMP_PACKET_QUERY;
            Igmp.u1MaxRespTime = IGMP_ZERO;
            u4Group = IGMP_NULL_GROUP;
            /* For Specific Query IGMP Header's Group field should u4Group */
            Igmp.u4GroupAddr = OSIX_HTONL (u4Group);
            Igmp.u2CheckSum = IGMP_ZERO;
            IgmpPutHdr (&Igmp, pBuf, IGMP_HEADER_LEN);
            break;

        case IGMP_VERSION_2:
            Igmp.u1PktType = IGMP_PACKET_QUERY;
            /* General query */
            if (u4Group == IGMP_ALL_HOSTS_GROUP)
            {
                Igmp.u1MaxRespTime = (UINT1) pIfNode->u2MaxRespTime;
            }
            else                /* Group Specific Query */
            {
                Igmp.u1MaxRespTime =
                    (UINT1) IGMP_IF_LAST_MEMBER_QIVAL (pIfNode);
            }

            /* For General Query IGMP Header's Group field should be NULL */
            if (u4Group == IGMP_ALL_HOSTS_GROUP)
            {
                u4Group = IGMP_NULL_GROUP;
            }
            /* For Specific Query IGMP Header's Group field should u4Group */
            Igmp.u4GroupAddr = OSIX_HTONL (u4Group);
            Igmp.u2CheckSum = IGMP_ZERO;
            IgmpPutHdr (&Igmp, pBuf, IGMP_HEADER_LEN);
            break;

        case IGMP_VERSION_3:
            IgmpV3Query.u1MsgType = IGMP_PACKET_QUERY;
            IgmpV3Query.u1MaxRespCode = (UINT1) pIfNode->u2MaxRespTime;
            /* the Max. Response code */
            if (pIfNode->u2MaxRespTime >= IGMPV3_MAX_RESPONSE_CODE)
            {
                IGMP_GET_CODE_FROM_TIME (pIfNode->u2MaxRespTime,
                                         IgmpV3Query.u1MaxRespCode);
            }
            u4Group = IGMP_NULL_GROUP;

            IgmpV3Query.u2CheckSum = IGMP_ZERO;
            /* For Specific Query IGMP Header's Group field should u4Group */
            IgmpV3Query.u4GroupAddr = OSIX_HTONL (u4Group);
            IgmpV3Query.u2NumSrcs = IGMP_ZERO;
            IgmpV3Query.u1QQIC = (UINT1) pIfNode->u2QueryInterval;
            if (pIfNode->u2QueryInterval >= IGMPV3_MAX_RESPONSE_CODE)
            {
                IGMP_GET_CODE_FROM_TIME (pIfNode->u2QueryInterval,
                                         IgmpV3Query.u1QQIC);
            }
            /* section: 4.1.6 */
            if (IGMP_IF_ROB_VARIABLE (pIfNode) > IGMP_QRV_MAX_VALUE)
            {
                IgmpV3Query.u1ResvSupRV = IGMP_ZERO;
            }
            else
            {
                IgmpV3Query.u1ResvSupRV = IGMP_IF_ROB_VARIABLE (pIfNode);
            }
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Query.u1ResvSupRV,
                                       IGMP_HEADER_LEN, IGMP_RESV_OFFSET);
            IgmpPutV3Hdr (&IgmpV3Query, pBuf,
                          (IGMP_HEADER_LEN + IGMP_RESV_OFFSET));
            break;

        default:
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "Wrong version of IGMP set on interface : %d\n",
                           pIfNode->u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return IGMP_NOT_OK;
    }
    /* 
     * Reset the u4Group field to IGMP_ALL_HOSTS_GROUP.
     * This the destination address to be put in the IP header
     * which means the Query should reach all IGMP hosts.
     */
    u4Group = IGMP_ALL_HOSTS_GROUP;
    IGMP_IPVX_ADDR_CLEAR (&GroupIpvxAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (GroupIpvxAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4Group);
    IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                   "Sending Query message on i/f %d for GroupAddr %s.\n",
                   pIfNode->u4IfIndex, IgmpPrintIPvxAddress (GroupIpvxAddr));

    /*
     * Stuff all the necessary information as parameters to the IP send
     * function.
     */
    /* FSL: Dumping IGMP packet */
    DbgDumpIgmpPkt (pBuf, (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf),
                    IGMP_PACKET_QUERY, (UINT1) IGMP_PKT_FLOW_OUT, u4SrcAddrX,
                    u4GrpAddrX);

    if (IgmpSendPktToIp (pBuf, pIfNode->u4IfAddr, u4DestAddrX, u2QueryLength,
                         (UINT2) pIfNode->u4IfIndex,
                         IGMP_PACKET_QUERY) == IGMP_NOT_OK)
    {
        return IGMP_NOT_OK;
    }

    /* update statistics */
    if (u4DestAddr == IGMP_ALL_HOSTS_GROUP)
    {
        IGMP_GEN_QUERY_TX (pIfNode);
    }
    else
    {
        IGMP_GRP_QUERY_TX (pIfNode);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpSendQuery()\r \n");
    return (IGMP_OK);
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpTxV2SchQuery                                */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer        */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*             be scheduled                         */
/*            u4GroupAddr - the group addres for which the query */
/*             is to be scheduled                    */
/*             u4NoOfSrcs - No. of sources                */
/*             pu1Sources - Source Addresses               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpTxSchQuery                         **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
INT4
IgmpTxV2SchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery)
{

    UINT2               u2QueryLength = IGMP_ZERO;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIgmpPkt            IgmpV2Query;
    tIgmpIPvXAddr       u4DestGroup;
    UINT4               u4GrpAddr = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpTxV2SchQuery()\r \n");
    IgmpFillMem (&IgmpV2Query, IGMP_ZERO, sizeof (tIgmpPkt));

    if (pIfNode->u1IfStatus != IGMP_IFACE_UP)
    {
        /* Without Iface Admin status = UP, Query cannot be sent out */
        IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "IGMP is disabled or interface %d is down."
                  "Can't send Query\n", pIfNode->u4IfIndex);
	SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_QRY_MODULE,
                        IGMP_NAME,IgmpSysErrString[SYS_LOG_QRY_SEND_FAILURE],
	"IGMP is disabled or interaface is down on interface %d ",pIfNode->u4IfIndex);
        return IGMP_NOT_OK;
    }

    u2QueryLength = (UINT2) (sizeof (tIgmpPkt) + IGMP_IP_HDR_RTR_OPT_LEN);
    pBuf = CRU_BUF_Allocate_MsgBufChain (u2QueryLength, IGMP_ZERO);

    if (pBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE, 
			IgmpGetModuleName(IGMP_BUFFER_MODULE),
                        "Buffer Allocation Failure.\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
			"for  message buffer chain\r\n");
        return (IGMP_NOT_OK);
    }

    IgmpV2Query.u1PktType = IGMP_PACKET_QUERY;
    IgmpV2Query.u1MaxRespTime = (UINT1) IGMP_IF_LAST_MEMBER_QIVAL (pIfNode);
    IgmpV2Query.u2CheckSum = IGMP_ZERO;
    IGMP_IP_COPY_FROM_IPVX (&u4GrpAddr, pSchQuery->u4Group,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    IgmpV2Query.u4GroupAddr = OSIX_HTONL (u4GrpAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV2Query, IGMP_ZERO,
                               IGMP_HEADER_LEN);
    IgmpPutHdr (&IgmpV2Query, pBuf, (UINT2) (IGMP_HEADER_LEN));
    IGMP_IPVX_ADDR_CLEAR (&u4DestGroup);
    IGMP_IPVX_ADDR_COPY (&u4DestGroup, &(pSchQuery->u4Group));

    IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                   "Sending Query message on i/f %d for %s.\n",
                   pIfNode->u4IfIndex, IgmpPrintIPvxAddress (u4DestGroup));

    /*  Get our IP address for this interface. */

    /* FSL: Dumping IGMP packet */
    DbgDumpIgmpPkt (pBuf, (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf),
                    IgmpV2Query.u1PktType, (UINT1) IGMP_PKT_FLOW_OUT,
                    pIfNode->u4IfAddr, u4DestGroup);

    IgmpSendPktToIp (pBuf, pIfNode->u4IfAddr, u4DestGroup,
                     u2QueryLength, (UINT2) pIfNode->u4IfIndex, IGMP_ZERO);

    IGMP_GRP_QUERY_TX (pIfNode);

    if (pSchQuery->u1RetrNum != IGMP_ZERO)
    {
        pSchQuery->u1RetrNum--;
    }

    if (pSchQuery->u1RetrNum == IGMP_ZERO)
    {
        /* add the node to the list of scheduled queries */
        TMO_SLL_Delete (&(pIfNode->pIfaceQry->schQueryList), &pSchQuery->Link);
        IgmpMemRelease (gIgmpMemPool.IgmpSchQueryId, (UINT1 *) pSchQuery);
    }
    else
    {
        IgmpReStartGrpSrcSchQryTmr (pIfNode, pSchQuery);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpTxV2SchQuery()\r \n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpTxSchQuery                                */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer        */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*             be scheduled                         */
/*            u4GroupAddr - the group addres for which the query */
/*             is to be scheduled                    */
/*             u4NoOfSrcs - No. of sources                */
/*             pu1Sources - Source Addresses               */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpTxSchQuery                         **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
INT4
IgmpTxSchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery)
{

    tIgmpArrayStr      *pu4SrcsWithSBit = NULL;
    tIgmpArrayStr      *pu4SrcsWithoutSBit = NULL;
    UINT2               u2QueryLengthWithSBit = IGMP_ZERO;
    UINT2               u2QueryLengthWithoutSBit = IGMP_ZERO;
    tCRU_BUF_CHAIN_HEADER *pBufWithSBit = NULL;
    tCRU_BUF_CHAIN_HEADER *pBufWithoutSBit = NULL;
    UINT4               u4NoOfSources = IGMP_ZERO;
    UINT4               u4NoOfSourcesWithSBit = IGMP_ZERO;
    UINT4               u4NoOfSourcesWithoutSBit = IGMP_ZERO;
    tIgmpV3Query        IgmpV3Query;
    tIgmpV3Query        IgmpV3QueryWithoutSBit;
    tIgmpIPvXAddr       u4DestGroupX;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    UINT2               u2Offset = IGMP_ZERO;
    tSourceNode        *pSchQrySrcNode = NULL;
    tTMO_SLL_NODE      *pNextSchQrySrcNode = NULL;
    UINT1               u1MaxRespCode = IGMP_ZERO;
    UINT4               u4GrpAddr = IGMP_ZERO;
    UINT4               u4SrcAddr = IGMP_ZERO;
    UINT4               u4Source = IGMP_ZERO;
    UINT4               u4index = 0;
    UINT4               u4RemTime = 0;
    UINT4              *pu4SrcsToPkt = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpTxSchQuery()\r \n");
    IgmpFillMem (&IgmpV3Query, IGMP_ZERO, sizeof (tIgmpV3Query));
    IgmpFillMem (&IgmpV3QueryWithoutSBit, IGMP_ZERO, sizeof (tIgmpV3Query));

    if (pIfNode->u1IfStatus != IGMP_IFACE_UP)
    {
        /* Without Iface Admin status = UP, Query cannot be sent out */
        IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "IGMP is disabled or interface %d is down"
                  "Can't send Query\n",pIfNode->u4IfIndex);
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_QRY_MODULE,
                IGMP_NAME,IgmpSysErrString[SYS_LOG_QRY_SEND_FAILURE],
		"IGMP is disabled or interaface is down on interface %d ",pIfNode->u4IfIndex);
	return IGMP_NOT_OK;
    }

    /* if the interface is non querier then ignore the message */
    if (IGMP_IPVX_ADDR_COMPARE (pIfNode->pIfaceQry->u4QuerierAddr,
                                pIfNode->u4IfAddr) != IGMP_ZERO)
    {
        return IGMP_OK;
    }

    u4NoOfSources = TMO_SLL_Count (&(pSchQuery->SrcList));

    IgmpV3Query.u1ResvSupRV = IGMP_ZERO;
    IgmpV3QueryWithoutSBit.u1ResvSupRV = IGMP_ZERO;

    if (pIfNode->u1Robustness < 8)
    {
        IgmpV3Query.u1ResvSupRV = pIfNode->u1Robustness;
        IgmpV3QueryWithoutSBit.u1ResvSupRV = pIfNode->u1Robustness;
    }

    IgmpV3Query.u1MsgType = IGMP_PACKET_QUERY;
    IgmpV3QueryWithoutSBit.u1MsgType = IGMP_PACKET_QUERY;
    u1MaxRespCode = (UINT1) IGMP_IF_LAST_MEMBER_QIVAL (pIfNode);
    IgmpV3Query.u1MaxRespCode = u1MaxRespCode;
    IgmpV3QueryWithoutSBit.u1MaxRespCode = u1MaxRespCode;

    if (u1MaxRespCode >= IGMPV3_MAX_RESPONSE_CODE)
    {
        IGMP_GET_CODE_FROM_TIME (u1MaxRespCode, IgmpV3Query.u1MaxRespCode);
        IGMP_GET_CODE_FROM_TIME (u1MaxRespCode,
                                 IgmpV3QueryWithoutSBit.u1MaxRespCode);
    }

    IgmpV3Query.u2CheckSum = IGMP_ZERO;
    IgmpV3QueryWithoutSBit.u2CheckSum = IGMP_ZERO;
    IGMP_IP_COPY_FROM_IPVX (&u4GrpAddr, pSchQuery->u4Group,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    IgmpV3Query.u4GroupAddr = OSIX_HTONL (u4GrpAddr);
    IgmpV3QueryWithoutSBit.u4GroupAddr = OSIX_HTONL (u4GrpAddr);

    if (pIfNode->u2QueryInterval < IGMPV3_MAX_RESPONSE_CODE)
    {
        IgmpV3Query.u1QQIC = (UINT1) pIfNode->u2QueryInterval;
        IgmpV3QueryWithoutSBit.u1QQIC = (UINT1) pIfNode->u2QueryInterval;
    }
    else
    {
        IGMP_GET_CODE_FROM_TIME (pIfNode->u2QueryInterval, IgmpV3Query.u1QQIC);
        IGMP_GET_CODE_FROM_TIME (pIfNode->u2QueryInterval,
                                 IgmpV3QueryWithoutSBit.u1QQIC);
    }

    /*Getting the group structure object for the given group address */
    pGrp = IgmpGrpLookUp (pIfNode, pSchQuery->u4Group);

    /*For group specific queries */
    if (u4NoOfSources == IGMP_ZERO)
    {
        /*Getting remaining time in Group Timer */
        if (pGrp != NULL)
        {
            if (TmrGetRemainingTime (gIgmpTimerListId,
                                     &(pGrp->Timer.TimerBlk.TimerNode),
                                     &u4RemTime) == TMR_SUCCESS)
            {
                /*Set S flag only if Group Timer > LMQT */
                if (u4RemTime > IGMP_LMQT (pIfNode))
                {
                    IgmpV3Query.u1ResvSupRV |= IGMP_S_FLAG;
                }
            }
        }
        /*Using pBufWithSBit for group specific queries */
        u2QueryLengthWithSBit = (UINT2) (sizeof (tIgmpV3Query) +
                                         IGMP_IP_HDR_RTR_OPT_LEN);
        pBufWithSBit = CRU_BUF_Allocate_MsgBufChain (u2QueryLengthWithSBit,
                                                     IGMP_ZERO);
        if (pBufWithSBit == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,IgmpGetModuleName(IGMP_BUFFER_MODULE),
                      "Buffer Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                    IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		     "for Message buffer chain \n");
            return (IGMP_NOT_OK);
        }
        IgmpPutV3Hdr (&IgmpV3Query, pBufWithSBit,
                      (UINT2) (IGMP_HEADER_LEN + IGMP_RESV_OFFSET));
    }

    else
    {
        /*Source group specific - Using IgmpV3Query for instances where S set */
        IgmpV3Query.u1ResvSupRV |= IGMP_S_FLAG;

        /* Copy the sources addresses into the two buffers-one each for S set,clr */
        u4NoOfSourcesWithSBit = IGMP_ZERO;
        u4NoOfSourcesWithoutSBit = IGMP_ZERO;
        u2Offset = IGMP_IP_HDR_RTR_OPT_LEN + IGMP_HEADER_LEN + IGMP_RESV_OFFSET;
        if (IGMP_MEM_ALLOCATE
            (gIgmpMemPool.IgmpAddrStructArrayId, pu4SrcsWithSBit,
             tIgmpArrayStr) == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
                      "Memory Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                         IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
			"for IGMP address Structure\n");
            return IGMP_NOT_OK;
        }
        if (IGMP_MEM_ALLOCATE
            (gIgmpMemPool.IgmpAddrStructArrayId, pu4SrcsWithoutSBit,
             tIgmpArrayStr) == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
                      "Memory  Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                              IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
				"for IGMP address Structure\n");
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);

            return IGMP_NOT_OK;
        }
        if (IGMP_MEM_ALLOCATE
            (gIgmpMemPool.IgmpU4SrcsToPktId, pu4SrcsToPkt, UINT4) == NULL)
        {
	    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE, IgmpGetModuleName(IGMP_OS_RES_MODULE),
                      "Memory Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
					"for IGMP Source to packet Id\n");
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithoutSBit);

            return IGMP_NOT_OK;
        }

        MEMSET (pu4SrcsWithSBit, 0, sizeof (tIgmpArrayStr));
        MEMSET (pu4SrcsWithoutSBit, 0, sizeof (tIgmpArrayStr));
        MEMSET (pu4SrcsToPkt, 0,
                MAX_IGMP_SRCS_FOR_MRP_PER_GROUP * sizeof (UINT4));

        for (pSchQrySrcNode =
             (tSourceNode *) TMO_SLL_First (&(pSchQuery->SrcList));
             pSchQrySrcNode != NULL;
             pSchQrySrcNode = (tSourceNode *) pNextSchQrySrcNode)
        {
            pNextSchQrySrcNode = TMO_SLL_Next (&(pSchQuery->SrcList),
                                               &pSchQrySrcNode->Link);
            IGMP_IP_COPY_FROM_IPVX (&u4SrcAddr,
                                    pSchQrySrcNode->pSrc->u4SrcAddress,
                                    IGMP_IPVX_ADDR_FMLY_IPV4);
            /*Scanning the group's source list till the required source data */
            /*structure is found. Comparing source addresses. */
            pSrc = NULL;
            if (pGrp != NULL)    /* Klocwork fix */
            {
                TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
                {
                    if (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                                pSchQrySrcNode->pSrc->
                                                u4SrcAddress) == IGMP_ZERO)
                    {
                        break;
                    }
                }
            }
            u4Source = OSIX_HTONL (u4SrcAddr);
            /* Either the source node is not NULL, or the grp node itself
             * is deleted */
            u4RemTime = IGMP_ZERO;
            if (pSrc != NULL)
            {
                (VOID) TmrGetRemainingTime (gIgmpTimerListId,
                                            &(pSrc->srcTimer.TimerBlk.
                                              TimerNode), &u4RemTime);
            }
            /*If for a particular source, timer value > LMQT, add the */
            /*source to the S flag set array */
            if (u4RemTime > IGMP_LMQT (pIfNode))
            {

                IGMP_IPVX_ADDR_INIT_IPV4 ((pu4SrcsWithSBit->
                                           au4SrcsSBit[u4NoOfSourcesWithSBit]),
                                          IGMP_IPVX_ADDR_FMLY_IPV4,
                                          (UINT1 *) &u4Source);
                u4NoOfSourcesWithSBit++;
            }
            else
            {
                /*If for a particular source, timer value <= LMQT, add the */
                /*source to the S flag not set array */

                IGMP_IPVX_ADDR_INIT_IPV4 ((pu4SrcsWithoutSBit->
                                           au4SrcsSBit
                                           [u4NoOfSourcesWithoutSBit]),
                                          IGMP_IPVX_ADDR_FMLY_IPV4,
                                          (UINT1 *) &u4Source);
                u4NoOfSourcesWithoutSBit++;
            }
            /* The query has been sent the number of times it has to be sent.
             * No more queries for this source. Just let the node go.
             */
            pSchQrySrcNode->u1RetrNum--;
            if (pSchQrySrcNode->u1RetrNum == IGMP_ZERO)
            {
                TMO_SLL_Delete (&(pSchQuery->SrcList), &pSchQrySrcNode->Link);
                IgmpMemRelease (gIgmpMemPool.IgmpSchQuerySourceId,
                                (UINT1 *) pSchQrySrcNode);
            }
        }
        /*Determine the packet lengths for both packets, alloc mem for buffer */
        u2QueryLengthWithSBit = (UINT2) ((sizeof (tIgmpV3Query) +
                                          (u4NoOfSourcesWithSBit) *
                                          sizeof (UINT4)) +
                                         IGMP_IP_HDR_RTR_OPT_LEN);
        u2QueryLengthWithoutSBit =
            (UINT2) ((sizeof (tIgmpV3Query) +
                      (u4NoOfSourcesWithoutSBit) * sizeof (UINT4)) +
                     IGMP_IP_HDR_RTR_OPT_LEN);

        pBufWithSBit = CRU_BUF_Allocate_MsgBufChain (u2QueryLengthWithSBit,
                                                     IGMP_ZERO);
        if (pBufWithSBit == NULL)
        {
	    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE, IgmpGetModuleName(IGMP_BUFFER_MODULE),
                      "Buffer Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                               IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
				"for Message buffer chain \n");
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithoutSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpU4SrcsToPktId,
                            (UINT1 *) pu4SrcsToPkt);

            return (IGMP_NOT_OK);
        }

        pBufWithoutSBit =
            CRU_BUF_Allocate_MsgBufChain (u2QueryLengthWithoutSBit, IGMP_ZERO);

        if (pBufWithoutSBit == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE, IgmpGetModuleName(IGMP_BUFFER_MODULE),
		                "Buffer Allocation Failure.\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                                 IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
				"for Message buffer chain \n");
            CRU_BUF_Release_MsgBufChain (pBufWithSBit, IGMP_ZERO);

            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                            (UINT1 *) pu4SrcsWithoutSBit);
            IgmpMemRelease (gIgmpMemPool.IgmpU4SrcsToPktId,
                            (UINT1 *) pu4SrcsToPkt);

            return (IGMP_NOT_OK);
        }

        IgmpV3Query.u2NumSrcs = OSIX_HTONS ((UINT2) u4NoOfSourcesWithSBit);
        IgmpV3QueryWithoutSBit.u2NumSrcs =
            OSIX_HTONS ((UINT2) u4NoOfSourcesWithoutSBit);

        /*Copy the sources for which S bit is to be set to pBufWithSBit */
        if (u4NoOfSourcesWithSBit != IGMP_ZERO)
        {
            for (u4index = 0; u4index < u4NoOfSourcesWithSBit; u4index++)
            {
                IGMP_IP_COPY_FROM_IPVX (pu4SrcsToPkt + u4index,
                                        pu4SrcsWithSBit->au4SrcsSBit[u4index],
                                        IGMP_IPVX_ADDR_FMLY_IPV4);
            }
            CRU_BUF_Copy_OverBufChain (pBufWithSBit, (UINT1 *) pu4SrcsToPkt,
                                       u2Offset,
                                       sizeof (UINT4) * u4NoOfSourcesWithSBit);
            IgmpPutV3Hdr (&IgmpV3Query, pBufWithSBit,
                          (UINT2) (IGMP_HEADER_LEN + IGMP_RESV_OFFSET +
                                   (sizeof (UINT4) * u4NoOfSourcesWithSBit)));
        }
        /*Copy the sources for which S bit is to be clear to pBufWithoutSBit */
        if (u4NoOfSourcesWithoutSBit != IGMP_ZERO)
        {
            for (u4index = 0; u4index < u4NoOfSourcesWithoutSBit; u4index++)
            {
                IGMP_IP_COPY_FROM_IPVX (pu4SrcsToPkt + u4index,
                                        pu4SrcsWithoutSBit->
                                        au4SrcsSBit[u4index],
                                        IGMP_IPVX_ADDR_FMLY_IPV4);
            }
            CRU_BUF_Copy_OverBufChain (pBufWithoutSBit, (UINT1 *) pu4SrcsToPkt,
                                       u2Offset,
                                       sizeof (UINT4) *
                                       u4NoOfSourcesWithoutSBit);
            IgmpPutV3Hdr (&IgmpV3QueryWithoutSBit, pBufWithoutSBit,
                          (UINT2) (IGMP_HEADER_LEN + IGMP_RESV_OFFSET +
                                   (sizeof (UINT4) *
                                    u4NoOfSourcesWithoutSBit)));
        }
        IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                        (UINT1 *) pu4SrcsWithSBit);
        IgmpMemRelease (gIgmpMemPool.IgmpAddrStructArrayId,
                        (UINT1 *) pu4SrcsWithoutSBit);
        IgmpMemRelease (gIgmpMemPool.IgmpU4SrcsToPktId, (UINT1 *) pu4SrcsToPkt);
    }
    IGMP_IPVX_ADDR_COPY (&u4DestGroupX, &(pSchQuery->u4Group));

    IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                   "Sending Query message(s) on i/f %d for %s.\n",
                   pIfNode->u4IfIndex, IgmpPrintIPvxAddress (u4DestGroupX));

    /*  Get our IP address for this interface. */

    /* FSL: Dumping IGMP packet */
    /*For group specific query or S,G specific query with S bit set */
    if ((u4NoOfSourcesWithSBit != IGMP_ZERO) || (u4NoOfSources == IGMP_ZERO))
    {
        DbgDumpIgmpPkt (pBufWithSBit,
                        (UINT2) CRU_BUF_Get_ChainValidByteCount (pBufWithSBit),
                        IgmpV3Query.u1MsgType, (UINT1) IGMP_PKT_FLOW_OUT,
                        pIfNode->u4IfAddr, u4DestGroupX);
    }
    /*For S,G specific query with S bit clear */
    if (u4NoOfSourcesWithoutSBit != IGMP_ZERO)
    {
        DbgDumpIgmpPkt (pBufWithoutSBit,
                        (UINT2)
                        CRU_BUF_Get_ChainValidByteCount (pBufWithoutSBit),
                        IgmpV3QueryWithoutSBit.u1MsgType,
                        (UINT1) IGMP_PKT_FLOW_OUT, pIfNode->u4IfAddr,
                        u4DestGroupX);
    }

    /*For group specific query or S,G specific query with S bit set */
    if ((u4NoOfSourcesWithSBit != IGMP_ZERO) || (u4NoOfSources == IGMP_ZERO))
    {
        /*Last param is IGMP_ZERO for specific queries */
        IgmpSendPktToIp (pBufWithSBit, pIfNode->u4IfAddr, u4DestGroupX,
                         u2QueryLengthWithSBit, (UINT2) pIfNode->u4IfIndex,
                         IGMP_ZERO);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBufWithSBit, FALSE);
    }

    /*For S,G specific query with S bit clear */
    if (u4NoOfSourcesWithoutSBit != IGMP_ZERO)
    {
        /*Last param is IGMP_ZERO for specific queries */
        IgmpSendPktToIp (pBufWithoutSBit, pIfNode->u4IfAddr, u4DestGroupX,
                         u2QueryLengthWithoutSBit, (UINT2) pIfNode->u4IfIndex,
                         IGMP_ZERO);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBufWithoutSBit, FALSE);
    }

    /* Update the statistics */
    if ((IgmpV3Query.u2NumSrcs) || (IgmpV3QueryWithoutSBit.u2NumSrcs))
    {
        if ((IgmpV3Query.u2NumSrcs) && (IgmpV3QueryWithoutSBit.u2NumSrcs))
        {
            IGMP_GRP_SRC_QUERY_TX (pIfNode);
        }
        IGMP_GRP_SRC_QUERY_TX (pIfNode);
    }
    else
    {
        IGMP_GRP_QUERY_TX (pIfNode);
    }

    if (pSchQuery->u1RetrNum != IGMP_ZERO)
    {
        if (u4NoOfSources != IGMP_ZERO)
        {
            IgmpV3QueryWithoutSBit.u2CheckSum = IGMP_ZERO;
            IgmpV3QueryWithoutSBit.u2NumSrcs = IGMP_ZERO;
            pBufWithoutSBit = CRU_BUF_Allocate_MsgBufChain ((IGMP_HEADER_LEN +
                                                             IGMP_RESV_OFFSET),
                                                            IGMP_ZERO);
            if (pBufWithoutSBit != NULL)
            {
                IgmpPutHdr ((tIgmpPkt *) & IgmpV3QueryWithoutSBit,
                            pBufWithoutSBit,
                            (IGMP_HEADER_LEN + IGMP_RESV_OFFSET));

                /*Last param is IGMP_ZERO for specific queries */
                IgmpSendPktToIp (pBufWithoutSBit, pIfNode->u4IfAddr,
                                 u4DestGroupX,
                                 (IGMP_HEADER_LEN + IGMP_RESV_OFFSET),
                                 (UINT2) pIfNode->u4IfIndex, IGMP_ZERO);

                IGMP_GRP_SRC_QUERY_TX (pIfNode);
            }
            else
            {
		IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_BUFFER_MODULE, IgmpGetModuleName(IGMP_BUFFER_MODULE),
                                        "Buffer Allocation Failure.\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
                                        "for Message buffer chain \n");
            }
        }
        pSchQuery->u1RetrNum--;
    }
    if ((pSchQuery->u1RetrNum == IGMP_ZERO) &&
        (TMO_SLL_Count (&(pSchQuery->SrcList)) == IGMP_ZERO))
    {
        /* add the node to the list of scheduled queries */
        TMO_SLL_Delete (&(pIfNode->pIfaceQry->schQueryList), &pSchQuery->Link);
        IgmpMemRelease (gIgmpMemPool.IgmpSchQueryId, (UINT1 *) pSchQuery);
    }
    else
    {
        IgmpReStartGrpSrcSchQryTmr (pIfNode, pSchQuery);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpTxSchQuery()\r \n");
    return IGMP_OK;
}
