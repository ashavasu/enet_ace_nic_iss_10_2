/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpmbsm.c,v 1.11 2015/02/17 10:16:51 siva Exp $
 *
 *******************************************************************/
#ifdef MBSM_WANTED
#include "igmpinc.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "igsnp.h"
#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE ;
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpMbsmPostMessage                                         */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the Line card          */
/*                change status to the IGMP Task                              */
/*                                                                           */
/*                                                                           */
/* Input        : pProtoMsg - Contains the Slot and Port Information         */
/*                i4Event  - Line card Up/Down status                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

INT4
IgmpMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tIgmpQMsg          *pQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMbsmPostMessage()\r \n");
    if (gIgmpConfig.u1IgmpStatus == IGMP_DISABLE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IGMP, "Igmp is Globally Disabled\n");
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        return MBSM_FAILURE;
    }

    pQMsg->u4MsgType = (UINT4) i4Event;

    /* mem alocation for pMbsmProtoMsg and memset pMbsmProtoMsg */
    pQMsg->IgmpQMsgParam.MbsmCardUpdate.pMbsmProtoMsg =
        MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg);
    MEMSET (pQMsg->IgmpQMsgParam.MbsmCardUpdate.pMbsmProtoMsg, 0,
            sizeof (tMbsmProtoMsg));

    MEMCPY (pQMsg->IgmpQMsgParam.MbsmCardUpdate.pMbsmProtoMsg,
            pProtoMsg, sizeof (tMbsmProtoMsg));

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IGMP,
                  "Enqueuing MBSM card status to IGMP - FAILED \n");
        MEM_FREE (pQMsg->IgmpQMsgParam.MbsmCardUpdate.pMbsmProtoMsg);
        IGMP_QMSG_FREE (pQMsg);
        return MBSM_FAILURE;
    }

    /* Send a EVENT to IGMP */
    if (OsixEvtSend (gIgmpTaskId, IGMP_INPUT_Q_EVENT) != OSIX_SUCCESS)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IGMP,
                  "Sending a event from MBSM to IGMP - FAILED \n");
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Sending a event from MBSM to IGMP - FAILED \n");
        return MBSM_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpMbsmPostMessage()\r \n");
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IgmpMbsmHandleEvent                                        */
/*                                                                           */
/* Description  : Install IGMP protol filter in the newly inserted line card */
/*                                                                           */
/*                                                                           */
/* Input        : pBuf - Buffer containing the protocol message information  */
/*                u4Cmd - Line card Status (UP/DOWN)                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
IgmpMbsmHandleEvent (tMbsmProtoMsg * pProtoMsg, UINT4 u4Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmProtoAckMsg    protoAckMsg;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpMbsmHandleEvent()\r \n");
    pSlotInfo = (tMbsmSlotInfo *) & (pProtoMsg->MbsmSlotInfo);
    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (u4Cmd == MBSM_MSG_CARD_INSERT)
        {
#if defined NPAPI_WANTED && defined IGMP_WANTED
            if (IgmpFsIgmpMbsmHwEnableIgmp (pSlotInfo) != FNP_SUCCESS)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                          "Card Insertion: IGMP filter installation failed!\n");
		SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_NP_MODULE,
                                        IGMP_NAME,IgmpSysErrString[NP_ENABLE_FILTER_FAIL],
                                  "Mbsm failure for cmd %d\r\n", u4Cmd);
                i4RetStatus = MBSM_FAILURE;
            }
#endif 
        }

        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            if (IgpFwdHandleMbsmEvent (pSlotInfo) != IGMP_SUCCESS)
            {
                MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                          "Card Insertion: IGMP Proxy forward entry creation failed!\n");
                i4RetStatus = MBSM_FAILURE;
            }
        }
    }
    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP */
    protoAckMsg.i4RetStatus = i4RetStatus;
    protoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
    protoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
    MbsmSendAckFromProto (&protoAckMsg);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
               "Exiting  IgmpMbsmHandleEvent()\r \n");
    return;
}
#endif
