/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmgmdlw.c,v 1.11 2017/02/06 10:45:27 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "igmpinc.h"

extern UINT4 FsMgmdMldTraceLevel[11];
extern UINT4 FsMgmdMldDebugLevel[11];

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMgmdIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsMgmdIgmpGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdIgmpGlobalStatus (INT4 *pi4RetValFsMgmdIgmpGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGlobalStatus (pi4RetValFsMgmdIgmpGlobalStatus,
                                              IPVX_ADDR_FMLY_IPV4);
    return i1Status;

}

/****************************************************************************
 Function    :  nmhGetFsMgmdIgmpTraceLevel
 Input       :  The Indices

                The Object 
                retValFsMgmdIgmpTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdIgmpTraceLevel (INT4 *pi4RetValFsMgmdIgmpTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpTraceLevel (pi4RetValFsMgmdIgmpTraceLevel,
                                            IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}


/****************************************************************************
 Function    :  nmhGetFsMgmdIgmpDebugLevel
 Input       :  The Indices
		The Object 
		retValFsMgmdIgmpDebugLevel 
 Output      :  The Get Low Lev Routine Take the Indices & 
	        store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/ 
INT1 
nmhGetFsMgmdIgmpDebugLevel (INT4 *pi4RetValFsMgmdIgmpDebugLevel)
{
 	INT1                i1Status = SNMP_FAILURE;
  	i1Status = 
		  IgmpMgmtUtilNmhGetFsIgmpDebugLevel (pi4RetValFsMgmdIgmpDebugLevel,
                                            IPVX_ADDR_FMLY_IPV4); 
  	return i1Status;
 }
/****************************************************************************
 Function    :  nmhGetFsMgmdMldGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsMgmdMldGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdMldGlobalStatus (INT4 *pi4RetValFsMgmdMldGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGlobalStatus (pi4RetValFsMgmdMldGlobalStatus,
                                              IPVX_ADDR_FMLY_IPV6);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdMldTraceLevel
 Input       :  The Indices

                The Object 
                retValFsMgmdMldTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdMldTraceLevel (INT4 *pi4RetValFsMgmdMldTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpTraceLevel (pi4RetValFsMgmdMldTraceLevel,
                                            IPVX_ADDR_FMLY_IPV6);
    return i1Status;
}


/****************************************************************************
 Function    :  nmhGetFsMgmdMldDebugLevel
 Input       :  The Indices

                The Object
                retValFsMgmdMldDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdMldDebugLevel (INT4 *pi4RetValFsMgmdMldDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpDebugLevel (pi4RetValFsMgmdMldDebugLevel,
                                            IPVX_ADDR_FMLY_IPV6);
    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMgmdIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsMgmdIgmpGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMgmdIgmpGlobalStatus (INT4 i4SetValFsMgmdIgmpGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus (i4SetValFsMgmdIgmpGlobalStatus,IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdIgmpTraceLevel
 Input       :  The Indices

                The Object 
                setValFsMgmdIgmpTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdIgmpTraceLevel (INT4 i4SetValFsMgmdIgmpTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpTraceLevel (i4SetValFsMgmdIgmpTraceLevel,
                                            IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhSetFsMgmdIgmpDebugLevel
 Input       :  The Indices
                The Object
                setValFsMgmdIgmpDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices & 
                 Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/ 
INT1 
nmhSetFsMgmdIgmpDebugLevel (INT4 i4SetValFsMgmdIgmpDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE; 
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpDebugLevel (i4SetValFsMgmdIgmpDebugLevel,
                                            IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdMldGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsMgmdMldGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdMldGlobalStatus (INT4 i4SetValFsMgmdMldGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpGlobalStatus (i4SetValFsMgmdMldGlobalStatus,
                                              IPVX_ADDR_FMLY_IPV6);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdMldTraceLevel
 Input       :  The Indices

                The Object 
                setValFsMgmdMldTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdMldTraceLevel (INT4 i4SetValFsMgmdMldTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpTraceLevel (i4SetValFsMgmdMldTraceLevel,
                                            IPVX_ADDR_FMLY_IPV6);
    if (i1Status == SNMP_SUCCESS)                                                                                                                                      
    {
	    IgmpUtilIncMsrForFsScalars (FsMgmdMldTraceLevel,
			    sizeof (FsMgmdMldTraceLevel) /
			    sizeof (UINT4), i4SetValFsMgmdMldTraceLevel);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdMldDebugLevel
 Input       :  The Indices

                The Object
                setValFsMgmdMldDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdMldDebugLevel (INT4 i4SetValFsMgmdMldDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpDebugLevel (i4SetValFsMgmdMldDebugLevel,
                                            IPVX_ADDR_FMLY_IPV6);

    if (i1Status == SNMP_SUCCESS)                                                                                                                                      
    {
	    IgmpUtilIncMsrForFsScalars (FsMgmdMldDebugLevel,
			    sizeof (FsMgmdMldDebugLevel) /
			    sizeof (UINT4), i4SetValFsMgmdMldDebugLevel);
    }
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMgmdIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsMgmdIgmpGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMgmdIgmpGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus (pu4ErrorCode,
                                                 i4TestValFsMgmdIgmpGlobalStatus,
                                                 IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdIgmpTraceLevel
 Input       :  The Indices

                The Object 
                testValFsMgmdIgmpTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdIgmpTraceLevel (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMgmdIgmpTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel (pu4ErrorCode,
                                               i4TestValFsMgmdIgmpTraceLevel,
                                               IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdIgmpDebugLevel
 Input       :  The Indices

                The Object
                testValFsMgmdIgmpDebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdIgmpDebugLevel (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMgmdIgmpDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel (pu4ErrorCode,
                                               i4TestValFsMgmdIgmpDebugLevel,
                                               IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdMldGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsMgmdMldGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdMldGlobalStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMgmdMldGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpGlobalStatus (pu4ErrorCode,
                                                 i4TestValFsMgmdMldGlobalStatus,
                                                 IPVX_ADDR_FMLY_IPV6);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdMldTraceLevel
 Input       :  The Indices

                The Object 
                testValFsMgmdMldTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdMldTraceLevel (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMgmdMldTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpTraceLevel (pu4ErrorCode,
                                               i4TestValFsMgmdMldTraceLevel,
                                               IPVX_ADDR_FMLY_IPV6);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdMldDebugLevel
 Input       :  The Indices

                The Object
                testValFsMgmdMldDebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdMldDebugLevel (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMgmdMldDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpDebugLevel (pu4ErrorCode,
                                               i4TestValFsMgmdMldDebugLevel,
                                               IPVX_ADDR_FMLY_IPV6);
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMgmdIgmpGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMgmdIgmpGlobalStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMgmdIgmpTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMgmdIgmpTraceLevel(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMgmdIgmpDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMgmdIgmpDebugLevel(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMgmdMldGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMgmdMldGlobalStatus (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMgmdMldTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMgmdMldTraceLevel(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMgmdMldDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMgmdMldDebugLevel(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMgmdInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMgmdInterfaceTable
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMgmdInterfaceTable (INT4 i4FsMgmdInterfaceIfIndex,
                                              INT4 i4FsMgmdInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpInterfaceTable
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMgmdInterfaceTable
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMgmdInterfaceTable (INT4 *pi4FsMgmdInterfaceIfIndex,
                                      INT4 *pi4FsMgmdInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    *pi4FsMgmdInterfaceAddrType = IGMP_ZERO;
    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable
        (pi4FsMgmdInterfaceIfIndex, pi4FsMgmdInterfaceAddrType);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMgmdInterfaceTable
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                nextFsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType
                nextFsMgmdInterfaceAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMgmdInterfaceTable (INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 *pi4NextFsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     INT4 *pi4NextFsMgmdInterfaceAddrType)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetNextIndexFsIgmpInterfaceTable
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pi4NextFsMgmdInterfaceIfIndex, pi4NextFsMgmdInterfaceAddrType);
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceAdminStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceAdminStatus (INT4 i4FsMgmdInterfaceIfIndex,
                                  INT4 i4FsMgmdInterfaceAddrType,
                                  INT4 *pi4RetValFsMgmdInterfaceAdminStatus)
{

    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceAdminStatus (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      pi4RetValFsMgmdInterfaceAdminStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceFastLeaveStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceFastLeaveStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceFastLeaveStatus (INT4 i4FsMgmdInterfaceIfIndex,
                                      INT4 i4FsMgmdInterfaceAddrType,
                                      INT4
                                      *pi4RetValFsMgmdInterfaceFastLeaveStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceFastLeaveStatus
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pi4RetValFsMgmdInterfaceFastLeaveStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceOperStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceOperStatus (INT4 i4FsMgmdInterfaceIfIndex,
                                 INT4 i4FsMgmdInterfaceAddrType,
                                 INT4 *pi4RetValFsMgmdInterfaceOperStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceOperStatus (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pi4RetValFsMgmdInterfaceOperStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceIncomingPkts
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceIncomingPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceIncomingPkts (INT4 i4FsMgmdInterfaceIfIndex,
                                   INT4 i4FsMgmdInterfaceAddrType,
                                   UINT4 *pu4RetValFsMgmdInterfaceIncomingPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingPkts (i4FsMgmdInterfaceIfIndex,
                                                       i4FsMgmdInterfaceAddrType,
                                                       pu4RetValFsMgmdInterfaceIncomingPkts);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceIncomingJoins
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceIncomingJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceIncomingJoins (INT4 i4FsMgmdInterfaceIfIndex,
                                    INT4 i4FsMgmdInterfaceAddrType,
                                    UINT4
                                    *pu4RetValFsMgmdInterfaceIncomingJoins)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingJoins
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceIncomingJoins);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceIncomingLeaves
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceIncomingLeaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceIncomingLeaves (INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsMgmdInterfaceIncomingLeaves)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingLeaves
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceIncomingLeaves);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceIncomingQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceIncomingQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceIncomingQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                      INT4 i4FsMgmdInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsMgmdInterfaceIncomingQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingQueries
        (i4FsMgmdInterfaceIfIndex, (UINT1) i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceIncomingQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceOutgoingQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceOutgoingQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceOutgoingQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                      INT4 i4FsMgmdInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsMgmdInterfaceOutgoingQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceOutgoingQueries
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceOutgoingQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRxGenQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRxGenQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceRxGenQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                   INT4 i4FsMgmdInterfaceAddrType,
                                   UINT4 *pu4RetValFsMgmdInterfaceRxGenQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGenQueries (i4FsMgmdInterfaceIfIndex,
                                                       i4FsMgmdInterfaceAddrType,
                                                       pu4RetValFsMgmdInterfaceRxGenQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRxGrpQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRxGrpQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceRxGrpQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                   INT4 i4FsMgmdInterfaceAddrType,
                                   UINT4 *pu4RetValFsMgmdInterfaceRxGrpQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpQueries (i4FsMgmdInterfaceIfIndex,
                                                       i4FsMgmdInterfaceAddrType,
                                                       pu4RetValFsMgmdInterfaceRxGrpQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRxGrpAndSrcQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRxGrpAndSrcQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceRxGrpAndSrcQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                         INT4 i4FsMgmdInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsMgmdInterfaceRxGrpAndSrcQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxGrpAndSrcQueries
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceRxGrpAndSrcQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRxIgmpv1v2Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRxIgmpv1v2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceRxIgmpv1v2Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                        INT4 i4FsMgmdInterfaceAddrType,
                                        UINT4
                                        *pu4RetValFsMgmdInterfaceRxIgmpv1v2Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceRxIgmpv1v2Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRxIgmpv3Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRxIgmpv3Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceRxIgmpv3Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                      INT4 i4FsMgmdInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsMgmdInterfaceRxIgmpv3Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      pu4RetValFsMgmdInterfaceRxIgmpv3Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRxMldv1Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRxMldv1Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceRxMldv1Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsMgmdInterfaceRxMldv1Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv1v2Reports
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceRxMldv1Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRxMldv2Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRxMldv2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceRxMldv2Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsMgmdInterfaceRxMldv2Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRxv3Reports (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      pu4RetValFsMgmdInterfaceRxMldv2Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxGenQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxGenQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceTxGenQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                   INT4 i4FsMgmdInterfaceAddrType,
                                   UINT4 *pu4RetValFsMgmdInterfaceTxGenQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGenQueries (i4FsMgmdInterfaceIfIndex,
                                                       i4FsMgmdInterfaceAddrType,
                                                       pu4RetValFsMgmdInterfaceTxGenQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxGrpQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxGrpQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceTxGrpQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                   INT4 i4FsMgmdInterfaceAddrType,
                                   UINT4 *pu4RetValFsMgmdInterfaceTxGrpQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpQueries (i4FsMgmdInterfaceIfIndex,
                                                       i4FsMgmdInterfaceAddrType,
                                                       pu4RetValFsMgmdInterfaceTxGrpQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxGrpAndSrcQueries
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxGrpAndSrcQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceTxGrpAndSrcQueries (INT4 i4FsMgmdInterfaceIfIndex,
                                         INT4 i4FsMgmdInterfaceAddrType,
                                         UINT4
                                         *pu4RetValFsMgmdInterfaceTxGrpAndSrcQueries)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxGrpAndSrcQueries
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceTxGrpAndSrcQueries);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxIgmpv1v2Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxIgmpv1v2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceTxIgmpv1v2Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                        INT4 i4FsMgmdInterfaceAddrType,
                                        UINT4
                                        *pu4RetValFsMgmdInterfaceTxIgmpv1v2Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceTxIgmpv1v2Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxIgmpv3Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxIgmpv3Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceTxIgmpv3Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                      INT4 i4FsMgmdInterfaceAddrType,
                                      UINT4
                                      *pu4RetValFsMgmdInterfaceTxIgmpv3Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      pu4RetValFsMgmdInterfaceTxIgmpv3Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxMldv1Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxMldv1Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*this is to be implemented in igmp struct*/
INT1
nmhGetFsMgmdInterfaceTxMldv1Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsMgmdInterfaceTxMldv1Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv1v2Reports
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceTxMldv1Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxMldv2Reports
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxMldv2Reports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*this is to be implemented in igmp struct*/
INT1
nmhGetFsMgmdInterfaceTxMldv2Reports (INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     UINT4
                                     *pu4RetValFsMgmdInterfaceTxMldv2Reports)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv3Reports (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      pu4RetValFsMgmdInterfaceTxMldv2Reports);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceTxLeaves
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceTxLeaves
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceTxLeaves (INT4 i4FsMgmdInterfaceIfIndex,
                               INT4 i4FsMgmdInterfaceAddrType,
                               UINT4 *pu4RetValFsMgmdInterfaceTxLeaves)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceTxv2Leaves (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceTxLeaves);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceGroupListId
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceGroupListId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceGroupListId (INT4 i4FsMgmdInterfaceIfIndex,
                                  INT4 i4FsMgmdInterfaceAddrType,
                                  UINT4 *pu4RetValFsMgmdInterfaceGroupListId)
{
    INT1                i1Status = SNMP_FAILURE;
    UNUSED_PARAM (i4FsMgmdInterfaceAddrType);
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceGroupListId (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      pu4RetValFsMgmdInterfaceGroupListId);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceLimit
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceLimit (INT4 i4FsMgmdInterfaceIfIndex,
                            INT4 i4FsMgmdInterfaceAddrType,
                            UINT4 *pu4RetValFsMgmdInterfaceLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    UNUSED_PARAM (i4FsMgmdInterfaceAddrType);
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceLimit (i4FsMgmdInterfaceIfIndex,
                                                i4FsMgmdInterfaceAddrType,
                                                pu4RetValFsMgmdInterfaceLimit);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceCurGrpCount
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceCurGrpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceCurGrpCount (INT4 i4FsMgmdInterfaceIfIndex,
                                  INT4 i4FsMgmdInterfaceAddrType,
                                  UINT4 *pu4RetValFsMgmdInterfaceCurGrpCount)
{
    INT1                i1Status = SNMP_FAILURE;
    UNUSED_PARAM (i4FsMgmdInterfaceAddrType);
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceCurGrpCount (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      pu4RetValFsMgmdInterfaceCurGrpCount);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceChannelTrackStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceChannelTrackStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdInterfaceChannelTrackStatus (INT4 i4FsMgmdInterfaceIfIndex,
                                         INT4 i4FsMgmdInterfaceAddrType,
                                         INT4
                                         *pi4RetValFsMgmdInterfaceChannelTrackStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceChannelTrackStatus
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pi4RetValFsMgmdInterfaceChannelTrackStatus);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceCKSumError
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceCKSumError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceCKSumError(INT4 i4FsMgmdInterfaceIfIndex , 
				INT4 i4FsMgmdInterfaceAddrType , 
				UINT4 *pu4RetValFsMgmdInterfaceCKSumError)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceCKSumError (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceCKSumError);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfacePktLenError
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfacePktLenError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfacePktLenError(INT4 i4FsMgmdInterfaceIfIndex , 
				 INT4 i4FsMgmdInterfaceAddrType , 
				 UINT4 *pu4RetValFsMgmdInterfacePktLenError)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfacePktLenError (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfacePktLenError);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfacePktsWithLocalIP
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfacePktsWithLocalIP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfacePktsWithLocalIP(INT4 i4FsMgmdInterfaceIfIndex , 
				     INT4 i4FsMgmdInterfaceAddrType , 
				     UINT4 *pu4RetValFsMgmdInterfacePktsWithLocalIP)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfacePktsWithLocalIP (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfacePktsWithLocalIP);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceSubnetCheckFailure
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceSubnetCheckFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceSubnetCheckFailure(INT4 i4FsMgmdInterfaceIfIndex , 
					INT4 i4FsMgmdInterfaceAddrType , 
					UINT4 *pu4RetValFsMgmdInterfaceSubnetCheckFailure)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceSubnetCheckFailure (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceSubnetCheckFailure);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceQryFromNonQuerier
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceQryFromNonQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceQryFromNonQuerier(INT4 i4FsMgmdInterfaceIfIndex , 
				       INT4 i4FsMgmdInterfaceAddrType , 
				       UINT4 *pu4RetValFsMgmdInterfaceQryFromNonQuerier)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceQryFromNonQuerier (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceQryFromNonQuerier);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceReportVersionMisMatch
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceReportVersionMisMatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceReportVersionMisMatch(INT4 i4FsMgmdInterfaceIfIndex , 
					   INT4 i4FsMgmdInterfaceAddrType , 
					   UINT4 *pu4RetValFsMgmdInterfaceReportVersionMisMatch)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceReportVersionMisMatch (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceReportVersionMisMatch);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceQryVersionMisMatch
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceQryVersionMisMatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceQryVersionMisMatch(INT4 i4FsMgmdInterfaceIfIndex , 
					INT4 i4FsMgmdInterfaceAddrType , 
					UINT4 *pu4RetValFsMgmdInterfaceQryVersionMisMatch)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceQryVersionMisMatch (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceQryVersionMisMatch);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceUnknownMsgType
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceUnknownMsgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceUnknownMsgType(INT4 i4FsMgmdInterfaceIfIndex , 
				    INT4 i4FsMgmdInterfaceAddrType , 
				    UINT4 *pu4RetValFsMgmdInterfaceUnknownMsgType)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceUnknownMsgType (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceUnknownMsgType);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceInvalidV1Report
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceInvalidV1Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceInvalidV1Report(INT4 i4FsMgmdInterfaceIfIndex , 
				     INT4 i4FsMgmdInterfaceAddrType , 
				     UINT4 *pu4RetValFsMgmdInterfaceInvalidV1Report)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV1Report (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceInvalidV1Report);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceInvalidV2Report
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceInvalidV2Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceInvalidV2Report(INT4 i4FsMgmdInterfaceIfIndex , 
				     INT4 i4FsMgmdInterfaceAddrType , 
				     UINT4 *pu4RetValFsMgmdInterfaceInvalidV2Report)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV2Report (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceInvalidV2Report);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceInvalidV3Report
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceInvalidV3Report
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceInvalidV3Report(INT4 i4FsMgmdInterfaceIfIndex , 
				     INT4 i4FsMgmdInterfaceAddrType , 
				     UINT4 *pu4RetValFsMgmdInterfaceInvalidV3Report)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidV3Report (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceInvalidV3Report);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceRouterAlertCheckFailure
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceRouterAlertCheckFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceRouterAlertCheckFailure(INT4 i4FsMgmdInterfaceIfIndex , 
					     INT4 i4FsMgmdInterfaceAddrType , 
					     UINT4 *pu4RetValFsMgmdInterfaceRouterAlertCheckFailure)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpInterfaceRouterAlertCheckFailure (i4FsMgmdInterfaceIfIndex,
                                                     i4FsMgmdInterfaceAddrType,
                                                     pu4RetValFsMgmdInterfaceRouterAlertCheckFailure);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceIncomingSSMPkts
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceIncomingSSMPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceIncomingSSMPkts (INT4 i4FsMgmdInterfaceIfIndex , 
				      INT4 i4FsMgmdInterfaceAddrType , 
				      UINT4 
				      *pu4RetValFsMgmdInterfaceIncomingSSMPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceIncomingSSMPkts
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceIncomingSSMPkts);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceInvalidSSMPkts
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceInvalidSSMPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceInvalidSSMPkts (INT4 i4FsMgmdInterfaceIfIndex , 
				     INT4 i4FsMgmdInterfaceAddrType , 
				     UINT4 *pu4RetValFsMgmdInterfaceInvalidSSMPkts)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceInvalidSSMPkts
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pu4RetValFsMgmdInterfaceInvalidSSMPkts);
    return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdJoinPktRate
 Input       :  The Indices
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdJoinPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdJoinPktRate (INT4 i4FsMgmdAddrType , 
			 INT4 *pi4RetValFsMgmdJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status = IgmpMgmtUtilNmhGetFsIgmpJoinPktRate
        (i4FsMgmdAddrType,
         pi4RetValFsMgmdJoinPktRate);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceJoinPktRate
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                retValFsMgmdInterfaceJoinPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdInterfaceJoinPktRate (INT4 i4FsMgmdInterfaceIfIndex , 
				  INT4 i4FsMgmdInterfaceAddrType , 
				  INT4 *pi4RetValFsMgmdInterfaceJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceJoinPktRate
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         pi4RetValFsMgmdInterfaceJoinPktRate);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceMalformedPkts
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object
                retValFsMgmdInterfaceMalformedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMgmdInterfaceMalformedPkts(INT4 i4FsMgmdInterfaceIfIndex , 
					INT4 i4FsMgmdInterfaceAddrType , 
					UINT4 *pu4RetValFsMgmdInterfaceMalformedPkts)
{
	INT1                i1Status = SNMP_FAILURE;
    	i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceMalformedPkts
        	   (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
                    pu4RetValFsMgmdInterfaceMalformedPkts);
  	return i1Status;

}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceSocketErrors
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object
                retValFsMgmdInterfaceSocketErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMgmdInterfaceSocketErrors(INT4 i4FsMgmdInterfaceIfIndex , 
				       INT4 i4FsMgmdInterfaceAddrType , 
			               UINT4 *pu4RetValFsMgmdInterfaceSocketErrors)
{
	INT1                i1Status = SNMP_FAILURE;
        i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceSocketErrors
                   (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
                    pu4RetValFsMgmdInterfaceSocketErrors);
        return i1Status;
}
/****************************************************************************
 Function    :  nmhGetFsMgmdInterfaceBadScopeErrors
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object
                retValFsMgmdInterfaceBadScopeErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMgmdInterfaceBadScopeErrors(INT4 i4FsMgmdInterfaceIfIndex , 
					 INT4 i4FsMgmdInterfaceAddrType , 
					 UINT4 *pu4RetValFsMgmdInterfaceBadScopeErrors)
{
	INT1                i1Status = SNMP_FAILURE;
        i1Status = IgmpMgmtUtilNmhGetFsIgmpInterfaceBadScopeErrors
                   (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
                    pu4RetValFsMgmdInterfaceBadScopeErrors);
        return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMgmdInterfaceAdminStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                setValFsMgmdInterfaceAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdInterfaceAdminStatus (INT4 i4FsMgmdInterfaceIfIndex,
                                  INT4 i4FsMgmdInterfaceAddrType,
                                  INT4 i4SetValFsMgmdInterfaceAdminStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceAdminStatus (i4FsMgmdInterfaceIfIndex,
                                                      i4FsMgmdInterfaceAddrType,
                                                      i4SetValFsMgmdInterfaceAdminStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdInterfaceFastLeaveStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                setValFsMgmdInterfaceFastLeaveStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdInterfaceFastLeaveStatus (INT4 i4FsMgmdInterfaceIfIndex,
                                      INT4 i4FsMgmdInterfaceAddrType,
                                      INT4
                                      i4SetValFsMgmdInterfaceFastLeaveStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceFastLeaveStatus
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         i4SetValFsMgmdInterfaceFastLeaveStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdInterfaceChannelTrackStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                setValFsMgmdInterfaceChannelTrackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdInterfaceChannelTrackStatus (INT4 i4FsMgmdInterfaceIfIndex,
                                         INT4 i4FsMgmdInterfaceAddrType,
                                         INT4
                                         i4SetValFsMgmdInterfaceChannelTrackStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         i4SetValFsMgmdInterfaceChannelTrackStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdInterfaceGroupListId
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                setValFsMgmdInterfaceGroupListId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdInterfaceGroupListId (INT4 i4FsMgmdInterfaceIfIndex,
                                  INT4 i4FsMgmdInterfaceAddrType,
                                  UINT4 u4SetValFsMgmdInterfaceGroupListId)
{
    INT1                i1Status = SNMP_FAILURE;
    UNUSED_PARAM (i4FsMgmdInterfaceAddrType);
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceGroupListId
        (i4FsMgmdInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         u4SetValFsMgmdInterfaceGroupListId);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdInterfaceLimit
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                setValFsMgmdInterfaceLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdInterfaceLimit (INT4 i4FsMgmdInterfaceIfIndex,
                            INT4 i4FsMgmdInterfaceAddrType,
                            UINT4 u4SetValFsMgmdInterfaceLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    UNUSED_PARAM (i4FsMgmdInterfaceAddrType);
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceLimit
        (i4FsMgmdInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         u4SetValFsMgmdInterfaceLimit);
    return i1Status;

}
/****************************************************************************
 Function    :  nmhSetFsMgmdJoinPktRate
 Input       :  The Indices
                FsMgmdAddrType

                The Object 
                setValFsMgmdJoinPktRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMgmdJoinPktRate (INT4 i4FsMgmdAddrType , 
			 INT4 i4SetValFsMgmdJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpJoinPktRate
        (i4FsMgmdAddrType,
         i4SetValFsMgmdJoinPktRate);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsMgmdInterfaceJoinPktRate
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                setValFsMgmdInterfaceJoinPktRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMgmdInterfaceJoinPktRate (INT4 i4FsMgmdInterfaceIfIndex , 
				  INT4 i4FsMgmdInterfaceAddrType , 
				  INT4 i4SetValFsMgmdInterfaceJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate
        (i4FsMgmdInterfaceIfIndex, i4FsMgmdInterfaceAddrType,
         i4SetValFsMgmdInterfaceJoinPktRate);
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMgmdInterfaceAdminStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                testValFsMgmdInterfaceAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdInterfaceAdminStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     INT4 i4TestValFsMgmdInterfaceAdminStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceAdminStatus (pu4ErrorCode,
                                                         i4FsMgmdInterfaceIfIndex,
                                                         i4FsMgmdInterfaceAddrType,
                                                         i4TestValFsMgmdInterfaceAdminStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdInterfaceFastLeaveStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                testValFsMgmdInterfaceFastLeaveStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdInterfaceFastLeaveStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMgmdInterfaceIfIndex,
                                         INT4 i4FsMgmdInterfaceAddrType,
                                         INT4
                                         i4TestValFsMgmdInterfaceFastLeaveStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceFastLeaveStatus (pu4ErrorCode,
                                                             i4FsMgmdInterfaceIfIndex,
                                                             i4FsMgmdInterfaceAddrType,
                                                             i4TestValFsMgmdInterfaceFastLeaveStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdInterfaceChannelTrackStatus
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                testValFsMgmdInterfaceChannelTrackStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdInterfaceChannelTrackStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMgmdInterfaceIfIndex,
                                            INT4 i4FsMgmdInterfaceAddrType,
                                            INT4
                                            i4TestValFsMgmdInterfaceChannelTrackStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceChannelTrackStatus (pu4ErrorCode,
                                                                i4FsMgmdInterfaceIfIndex,
                                                                i4FsMgmdInterfaceAddrType,
                                                                i4TestValFsMgmdInterfaceChannelTrackStatus);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdInterfaceGroupListId
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                testValFsMgmdInterfaceGroupListId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdInterfaceGroupListId (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMgmdInterfaceIfIndex,
                                     INT4 i4FsMgmdInterfaceAddrType,
                                     UINT4 u4TestValFsMgmdInterfaceGroupListId)
{
    INT1                i1Status = SNMP_FAILURE;
    UNUSED_PARAM (i4FsMgmdInterfaceAddrType);

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceGroupListId (pu4ErrorCode,
                                                         i4FsMgmdInterfaceIfIndex,
                                                         IPVX_ADDR_FMLY_IPV4,
                                                         u4TestValFsMgmdInterfaceGroupListId);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdInterfaceLimit
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                testValFsMgmdInterfaceLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdInterfaceLimit (UINT4 *pu4ErrorCode,
                               INT4 i4FsMgmdInterfaceIfIndex,
                               INT4 i4FsMgmdInterfaceAddrType,
                               UINT4 u4TestValFsMgmdInterfaceLimit)
{
    INT1                i1Status = SNMP_FAILURE;

    UNUSED_PARAM (i4FsMgmdInterfaceAddrType);
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceLimit (pu4ErrorCode,
                                                   i4FsMgmdInterfaceIfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   u4TestValFsMgmdInterfaceLimit);

    return i1Status;
}
/****************************************************************************
 Function    :  nmhTestv2FsMgmdJoinPktRate
 Input       :  The Indices
                FsMgmdAddrType

                The Object 
                testValJoinPktRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMgmdJoinPktRate (UINT4 *pu4ErrorCode , 
				     INT4 i4FsMgmdAddrType , 
				     INT4 i4TestValFsMgmdJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpJoinPktRate 
			(pu4ErrorCode,
                 	 i4FsMgmdAddrType,
			 i4TestValFsMgmdJoinPktRate);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhTestv2FsMgmdInterfaceJoinPktRate
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType

                The Object 
                testValFsMgmdInterfaceJoinPktRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMgmdInterfaceJoinPktRate (UINT4 *pu4ErrorCode , 
				     INT4 i4FsMgmdInterfaceIfIndex , 
				     INT4 i4FsMgmdInterfaceAddrType , 
				     INT4 i4TestValFsMgmdInterfaceJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpInterfaceJoinPktRate 
			(pu4ErrorCode, i4FsMgmdInterfaceIfIndex,
                 	 i4FsMgmdInterfaceAddrType,
			 i4TestValFsMgmdInterfaceJoinPktRate);
    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMgmdInterfaceTable
 Input       :  The Indices
                FsMgmdInterfaceIfIndex
                FsMgmdInterfaceAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMgmdInterfaceTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMgmdCacheTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMgmdCacheTable
 Input       :  The Indices
                FsMgmdCacheAddrType
                FsMgmdCacheAddress
                FsMgmdCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMgmdCacheTable (INT4 i4FsMgmdCacheAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMgmdCacheAddress,
                                          INT4 i4FsMgmdCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (IgmpCacheAddr, pFsMgmdCacheAddress,
                             i4FsMgmdCacheAddrType);

    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpCacheTable
        (i4FsMgmdCacheAddrType, IgmpCacheAddr, i4FsMgmdCacheIfIndex);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMgmdCacheTable
 Input       :  The Indices
                FsMgmdCacheAddrType
                FsMgmdCacheAddress
                FsMgmdCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMgmdCacheTable (INT4 *pi4FsMgmdCacheAddrType,
                                  tSNMP_OCTET_STRING_TYPE * pFsMgmdCacheAddress,
                                  INT4 *pi4FsMgmdCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddr);

    *pi4FsMgmdCacheAddrType = IGMP_ZERO;
    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexFsIgmpCacheTable (pi4FsMgmdCacheAddrType,
                                                      &IgmpCacheAddr,
                                                      pi4FsMgmdCacheIfIndex);

    if (i1Status == SNMP_SUCCESS)
    {
        IGMP_COPY_IPVX_TO_OCTET (pFsMgmdCacheAddress, IgmpCacheAddr,
                                 *pi4FsMgmdCacheAddrType);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMgmdCacheTable
 Input       :  The Indices
                FsMgmdCacheAddrType
                nextFsMgmdCacheAddrType
                FsMgmdCacheAddress
                nextFsMgmdCacheAddress
                FsMgmdCacheIfIndex
                nextFsMgmdCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMgmdCacheTable (INT4 i4FsMgmdCacheAddrType,
                                 INT4 *pi4NextFsMgmdCacheAddrType,
                                 tSNMP_OCTET_STRING_TYPE * pFsMgmdCacheAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextFsMgmdCacheAddress,
                                 INT4 i4FsMgmdCacheIfIndex,
                                 INT4 *pi4NextFsMgmdCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddr;
    tIgmpIPvXAddr       NextIgmpCacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddr);
    IGMP_IPVX_ADDR_CLEAR (&NextIgmpCacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (IgmpCacheAddr, pFsMgmdCacheAddress,
                             i4FsMgmdCacheAddrType);

    i1Status =
        IgmpMgmtUtilNmhGetNextIndexFsIgmpCacheTable (i4FsMgmdCacheAddrType,
                                                     pi4NextFsMgmdCacheAddrType,
                                                     IgmpCacheAddr,
                                                     &NextIgmpCacheAddr,
                                                     i4FsMgmdCacheIfIndex,
                                                     pi4NextFsMgmdCacheIfIndex);

    if (i1Status == SNMP_SUCCESS)
    {
        IGMP_COPY_IPVX_TO_OCTET (pNextFsMgmdCacheAddress, NextIgmpCacheAddr,
                                 *pi4NextFsMgmdCacheAddrType);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMgmdCacheGroupCompMode
 Input       :  The Indices
                FsMgmdCacheAddrType
                FsMgmdCacheAddress
                FsMgmdCacheIfIndex

                The Object 
                retValFsMgmdCacheGroupCompMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdCacheGroupCompMode (INT4 i4FsMgmdCacheAddrType,
                                tSNMP_OCTET_STRING_TYPE * pFsMgmdCacheAddress,
                                INT4 i4FsMgmdCacheIfIndex,
                                INT4 *pi4RetValFsMgmdCacheGroupCompMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddr;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddr);
    IGMP_COPY_OCTET_TO_IPVX (IgmpCacheAddr, pFsMgmdCacheAddress,
                             i4FsMgmdCacheAddrType);

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpCacheGroupCompMode (i4FsMgmdCacheAddrType,
                                                    IgmpCacheAddr,
                                                    i4FsMgmdCacheIfIndex,
                                                    pi4RetValFsMgmdCacheGroupCompMode);
    return i1Status;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMgmdGlobalLimit
 Input       :  The Indices

                The Object 
                retValFsMgmdGlobalLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdGlobalLimit (UINT4 *pu4RetValFsMgmdGlobalLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGlobalLimit (pu4RetValFsMgmdGlobalLimit,
                                             IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdGlobalCurGrpCount
 Input       :  The Indices

                The Object 
                retValFsMgmdGlobalCurGrpCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMgmdGlobalCurGrpCount (UINT4 *pu4RetValFsMgmdGlobalCurGrpCount)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpGlobalCurGrpCount
        (pu4RetValFsMgmdGlobalCurGrpCount, IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFsMgmdSSMMapStatus
 Input       :  The Indices

                The Object 
                retValFsMgmdSSMMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdSSMMapStatus(INT4 *pi4RetValFsMgmdSSMMapStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpSSMMapStatus (pi4RetValFsMgmdSSMMapStatus,
                                              IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMgmdGlobalLimit
 Input       :  The Indices

                The Object 
                setValFsMgmdGlobalLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMgmdGlobalLimit (UINT4 u4SetValFsMgmdGlobalLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpGlobalLimit
        (IPVX_ADDR_FMLY_IPV4, u4SetValFsMgmdGlobalLimit);
    return i1Status;
}


/****************************************************************************
 Function    :  nmhSetFsMgmdSSMMapStatus
 Input       :  The Indices

                The Object 
                setValFsMgmdSSMMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMgmdSSMMapStatus(INT4 i4SetValFsMgmdSSMMapStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus
                (i4SetValFsMgmdSSMMapStatus, IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMgmdGlobalLimit
 Input       :  The Indices

                The Object 
                testValFsMgmdGlobalLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMgmdGlobalLimit (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsMgmdGlobalLimit)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpGlobalLimit (pu4ErrorCode,
                                                u4TestValFsMgmdGlobalLimit,
                                                IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}


/****************************************************************************
 Function    :  nmhTestv2FsMgmdSSMMapStatus
 Input       :  The Indices

                The Object 
                testValFsMgmdSSMMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMgmdSSMMapStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsMgmdSSMMapStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpSSMMapStatus (pu4ErrorCode,
                                                 i4TestValFsMgmdSSMMapStatus,
                                                 IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}


/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMgmdGlobalLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMgmdGlobalLimit (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMgmdSSMMapStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhDepv2FsMgmdSSMMapStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}


/* LOW LEVEL Routines for Table : FsMgmdSSMMapGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMgmdSSMMapGroupTable
 Input       :  The Indices
                FsMgmdSSMMapStartGrpAddress
                FsMgmdSSMMapEndGrpAddress
                FsMgmdSSMMapSourceAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
nmhValidateIndexInstanceFsMgmdSSMMapGroupTable
                (UINT4 u4FsMgmdSSMMapStartGrpAddress , 
                 UINT4 u4FsMgmdSSMMapEndGrpAddress , 
                 UINT4 u4FsMgmdSSMMapSourceAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceFsIgmpSSMMapGroupTable
        (u4FsMgmdSSMMapStartGrpAddress, u4FsMgmdSSMMapEndGrpAddress, u4FsMgmdSSMMapSourceAddress);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMgmdSSMMapGroupTable
 Input       :  The Indices
                FsMgmdSSMMapStartGrpAddress
                FsMgmdSSMMapEndGrpAddress
                FsMgmdSSMMapSourceAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
nmhGetFirstIndexFsMgmdSSMMapGroupTable
                    (UINT4 *pu4FsMgmdSSMMapStartGrpAddress , 
                     UINT4 *pu4FsMgmdSSMMapEndGrpAddress , 
                     UINT4 *pu4FsMgmdSSMMapSourceAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexFsIgmpSSMMapGroupTable (pu4FsMgmdSSMMapStartGrpAddress,
                pu4FsMgmdSSMMapEndGrpAddress, pu4FsMgmdSSMMapSourceAddress);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMgmdSSMMapGroupTable
 Input       :  The Indices
                FsMgmdSSMMapStartGrpAddress
                nextFsMgmdSSMMapStartGrpAddress
                FsMgmdSSMMapEndGrpAddress
                nextFsMgmdSSMMapEndGrpAddress
                FsMgmdSSMMapSourceAddress
                nextFsMgmdSSMMapSourceAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
nmhGetNextIndexFsMgmdSSMMapGroupTable(UINT4 u4FsMgmdSSMMapStartGrpAddress ,
                    UINT4 *pu4NextFsMgmdSSMMapStartGrpAddress  , 
                    UINT4 u4FsMgmdSSMMapEndGrpAddress ,
                    UINT4 *pu4NextFsMgmdSSMMapEndGrpAddress  , 
                    UINT4 u4FsMgmdSSMMapSourceAddress ,
                    UINT4 *pu4NextFsMgmdSSMMapSourceAddress )
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetNextIndexFsIgmpSSMMapGroupTable (u4FsMgmdSSMMapStartGrpAddress,
                pu4NextFsMgmdSSMMapStartGrpAddress,
                u4FsMgmdSSMMapEndGrpAddress,
                pu4NextFsMgmdSSMMapEndGrpAddress,
                u4FsMgmdSSMMapSourceAddress,
                pu4NextFsMgmdSSMMapSourceAddress);
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMgmdSSMMapRowStatus
 Input       :  The Indices
                FsMgmdSSMMapStartGrpAddress
                FsMgmdSSMMapEndGrpAddress
                FsMgmdSSMMapSourceAddress

                The Object 
                retValFsMgmdSSMMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMgmdSSMMapRowStatus(UINT4 u4FsMgmdSSMMapStartGrpAddress , 
                            UINT4 u4FsMgmdSSMMapEndGrpAddress , 
                            UINT4 u4FsMgmdSSMMapSourceAddress , 
                            INT4 *pi4RetValFsMgmdSSMMapRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    i1Status =
        IgmpMgmtUtilNmhGetFsIgmpSSMMapRowStatus (u4FsMgmdSSMMapStartGrpAddress,
                u4FsMgmdSSMMapEndGrpAddress,
                u4FsMgmdSSMMapSourceAddress,
                pi4RetValFsMgmdSSMMapRowStatus);
    return i1Status;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMgmdSSMMapRowStatus
 Input       :  The Indices
                FsMgmdSSMMapStartGrpAddress
                FsMgmdSSMMapEndGrpAddress
                FsMgmdSSMMapSourceAddress

                The Object 
                setValFsMgmdSSMMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMgmdSSMMapRowStatus(UINT4 u4FsMgmdSSMMapStartGrpAddress , 
                            UINT4 u4FsMgmdSSMMapEndGrpAddress , 
                            UINT4 u4FsMgmdSSMMapSourceAddress , 
                            INT4 i4SetValFsMgmdSSMMapRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus
            (u4FsMgmdSSMMapStartGrpAddress, u4FsMgmdSSMMapEndGrpAddress,
            u4FsMgmdSSMMapSourceAddress, i4SetValFsMgmdSSMMapRowStatus);
    return i1Status;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMgmdSSMMapRowStatus
 Input       :  The Indices
                FsMgmdSSMMapStartGrpAddress
                FsMgmdSSMMapEndGrpAddress
                FsMgmdSSMMapSourceAddress

                The Object 
                testValFsMgmdSSMMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMgmdSSMMapRowStatus(UINT4 *pu4ErrorCode , UINT4 u4FsMgmdSSMMapStartGrpAddress , 
                            UINT4 u4FsMgmdSSMMapEndGrpAddress , 
                            UINT4 u4FsMgmdSSMMapSourceAddress , 
                            INT4 i4TestValFsMgmdSSMMapRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhTestv2FsIgmpSSMMapRowStatus (pu4ErrorCode,
                u4FsMgmdSSMMapStartGrpAddress,
                u4FsMgmdSSMMapEndGrpAddress,
                u4FsMgmdSSMMapSourceAddress,
                i4TestValFsMgmdSSMMapRowStatus);

    return i1Status;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMgmdSSMMapGroupTable
 Input       :  The Indices
                FsMgmdSSMMapStartGrpAddress
                FsMgmdSSMMapEndGrpAddress
                FsMgmdSSMMapSourceAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhDepv2FsMgmdSSMMapGroupTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
