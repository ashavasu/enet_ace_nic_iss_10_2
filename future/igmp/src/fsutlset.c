/******************************************************************************/
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsutlset.c,v 1.18 2017/02/06 10:45:27 siva Exp $
 *
 * Description: : Action routines for set/get objects in           
 *                           fsigmp.mib                           
 *
 *******************************************************************/

# include  "include.h"
# include  "fsigmcon.h"
# include  "fsigmogi.h"
# include  "midconst.h"
# include  "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE ;
#endif
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsIgmpGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpGlobalStatus (INT4 i4SetValFsIgmpGlobalStatus,
                                      INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
             "Entering IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (gIgmpConfig.u1IgmpStatus == i4SetValFsIgmpGlobalStatus)
        {
            return SNMP_SUCCESS;
        }

        if (i4SetValFsIgmpGlobalStatus == IGMP_ENABLE)
        {
            if (IgmpHandleEnableEvent () == IGMP_FAILURE)
            {
                CLI_SET_ERR (CLI_IGMP_NOT_ENABLE);
		IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                "Enabling IGMP failed");
		MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			     IgmpGetModuleName(IGMP_EXIT_MODULE),
                 	     "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
                return SNMP_FAILURE;
            }
        }
        else
        {
	    ClearIgmpStat (IPVX_ADDR_FMLY_IPV4);
            if (IgmpHandleDisableEvent () == IGMP_FAILURE)
            {
                CLI_SET_ERR (CLI_IGMP_NOT_DISABLE);
		IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                "Disabling IGMP failed");
		MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			     IgmpGetModuleName(IGMP_EXIT_MODULE),
                 	     "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (gIgmpConfig.u1MldStatus == i4SetValFsIgmpGlobalStatus)
        {
            return SNMP_SUCCESS;
        }
        if (i4SetValFsIgmpGlobalStatus == IGMP_ENABLE)
        {
            if (MldHandleEnableEvent () == IGMP_FAILURE)
            {
                CLI_SET_ERR (CLI_MLD_NOT_ENABLE);
		IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                "Enabling MLD failed");
		MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			     IgmpGetModuleName(IGMP_EXIT_MODULE),
                 	     "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
                return SNMP_FAILURE;
            }
        }
        else
        {
	    ClearMldStat (IPVX_ADDR_FMLY_IPV6);
            if (MldHandleDisableEvent () == IGMP_FAILURE)
            {
                CLI_SET_ERR (CLI_MLD_NOT_DISABLE);
		IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                "Disabling MLD failed");
		MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			     IgmpGetModuleName(IGMP_EXIT_MODULE),
                             "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpTraceLevel
 Input       :  The Indices

                The Object 
                setValFsIgmpTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpTraceLevel (INT4 i4SetValFsIgmpTraceLevel,
                                    INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpTraceLevel()\r \n");
    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
	if (i4SetValFsIgmpTraceLevel >= IGMP_ZERO)
    	{
        	gIgmpConfig.u4IgmpTrcFlag = (UINT4) i4SetValFsIgmpTraceLevel;
    	}
    	else
    	{
        	i4SetValFsIgmpTraceLevel &= IGMP_MAX_INT4;
        	gIgmpConfig.u4IgmpTrcFlag &= (UINT4) (~i4SetValFsIgmpTraceLevel);
    	}
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
        return SNMP_SUCCESS;
    }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if (i4SetValFsIgmpTraceLevel >= IGMP_ZERO)
        {
                gIgmpConfig.u4IgmpTrcFlag = (UINT4) i4SetValFsIgmpTraceLevel;
        }
        else
        {
                i4SetValFsIgmpTraceLevel &= IGMP_MAX_INT4;
                gIgmpConfig.u4IgmpTrcFlag &= (UINT4) (~i4SetValFsIgmpTraceLevel);
        }

        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n"); 
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpDebugLevel
 Input       :  The Indices

                The Object
                setValFsIgmpDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpDebugLevel (INT4 i4SetValFsIgmpDebugLevel,
                                    INT4 i4FsIgmpAddrType)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpDebugLevel()\r \n");
    if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (i4SetValFsIgmpDebugLevel >= IGMP_ZERO)
    	{
        	gIgmpConfig.u4IgmpDbgFlag = (UINT4) i4SetValFsIgmpDebugLevel;
    	}
    	else
    	{
        	i4SetValFsIgmpDebugLevel &= IGMP_MAX_INT4;
        	gIgmpConfig.u4IgmpDbgFlag &= (UINT4) (~i4SetValFsIgmpDebugLevel);
    	}
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
        return SNMP_SUCCESS;
    }
    else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
    {
	if (i4SetValFsIgmpDebugLevel >= IGMP_ZERO)
        {
                gIgmpConfig.u4MldDbgFlag = (UINT4) i4SetValFsIgmpDebugLevel;
        }
        else
        {
                i4SetValFsIgmpDebugLevel &= IGMP_MAX_INT4;
                gIgmpConfig.u4MldDbgFlag &= (UINT4) (~i4SetValFsIgmpDebugLevel);
        }
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpInterfaceAdminStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceAdminStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              INT4
                                              i4SetValFsIgmpInterfaceAdminStatus)
{
    tIgmpIface         *pIfaceNode = NULL;
    INT1                i1Status = SNMP_SUCCESS;
    UINT4               u4Port = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpDebugLevel()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Ifacenode not exist with index %d", i4FsIgmpInterfaceIfIndex);
	return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);

    if ((pIfaceNode != NULL) &&
        (pIfaceNode->u1AdminStatus != i4SetValFsIgmpInterfaceAdminStatus))
    {
        pIfaceNode->u1AdminStatus = (UINT1) i4SetValFsIgmpInterfaceAdminStatus;

        if (i4SetValFsIgmpInterfaceAdminStatus == IGMP_IFACE_ADMIN_DOWN)
        {
            mmi_printf("%% IGMP configurations associated with the interface is removed\n"); 
            /* admin status is set to down, so disable igmp on the i/f if 
             * it is enabled previously 
             */
            if (pIfaceNode->u1IfStatus == IGMP_UP)
            {
                /* Disable igmp on the interface */
                pIfaceNode->u1IfStatus = IGMP_DOWN;
                IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_DOWN);
            }
        }
        else                    /* IGMP_IFACE_ADMIN_UP */
        {
            /* admin status is set to up, so enable igmp on the i/f only if 
             * i/f Oper-state is up 
             */
            if ((pIfaceNode->u1IfStatus == IGMP_DOWN)
                && (pIfaceNode->u1OperStatus == IGMP_IFACE_OPER_UP))
            {
                /* Enable igmp on the interface */
                pIfaceNode->u1IfStatus = IGMP_UP;
                IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_UP);
            }
        }
    }
    else
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  " First Create the Interface !!!!!!!!!!! \n");
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
    return i1Status;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpInterfaceFastLeaveStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceFastLeaveStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceFastLeaveStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                                  INT4
                                                  i4FsIgmpInterfaceAddrType,
                                                  INT4
                                                  i4SetValFsIgmpInterfaceFastLeaveStatus)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpInterfaceAdminStatus()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Ifacenode not exist with index %d", i4FsIgmpInterfaceIfIndex);
	 MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Interface %d is configured as upstream interface\r\n", u4Port);
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
        return SNMP_FAILURE;
    }

    if ((pIfaceNode = IgmpGetIfNode (u4Port,
                                     i4FsIgmpInterfaceAddrType)) != NULL)
    {
        pIfaceNode->u1FastLeaveFlg = (UINT1)
            i4SetValFsIgmpInterfaceFastLeaveStatus;
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
        return SNMP_SUCCESS;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalStatus()\r \n");
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceChannelTrackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus (INT4
                                                     i4FsIgmpInterfaceIfIndex,
                                                     INT4
                                                     i4FsIgmpInterfaceAddrType,
                                                     INT4
                                                     i4SetValFsIgmpInterfaceChannelTrackStatus)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Ifacenode not exist with %d", i4FsIgmpInterfaceIfIndex);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
        return SNMP_FAILURE;
    }

    if ((pIfaceNode = IgmpGetIfNode (u4Port,
                                     i4FsIgmpInterfaceAddrType)) != NULL)
    {
        pIfaceNode->u1ChannelTrackFlg = (UINT1)
            i4SetValFsIgmpInterfaceChannelTrackStatus;
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
        return SNMP_SUCCESS;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpInterfaceLimit
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceLimit (INT4 i4FsIgmpInterfaceIfIndex,
                                        INT4 i4FsIgmpInterfaceAddrType,
                                        UINT4 u4SetValFsIgmpInterfaceLimit)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpInterfaceLimit()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Ifacenode not exist with index %d", i4FsIgmpInterfaceIfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
	return SNMP_FAILURE;
    }
    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);
    if ((pIfaceNode == NULL))
    {
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Ifacenode not exist with index %d", i4FsIgmpInterfaceIfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
	CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }
    if (((pIfaceNode->u4GrpLimit != IGMP_ZERO) &&
         (u4SetValFsIgmpInterfaceLimit == IGMP_ZERO)) ||
        (pIfaceNode->u4GrpLimit == IGMP_ZERO) ||
        (u4SetValFsIgmpInterfaceLimit < pIfaceNode->u4GrpCurCnt))
    {
        IgmpResetLimitFlag (pIfaceNode);
    }
    pIfaceNode->u4GrpLimit = u4SetValFsIgmpInterfaceLimit;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpInterfaceGroupListId
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceGroupListId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceGroupListId (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              UINT4
                                              u4SetValFsIgmpInterfaceGroupListId)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpInterfaceGroupListId()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Ifacenode not exist with index %d", i4FsIgmpInterfaceIfIndex);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
        return SNMP_FAILURE;
    }
    pIfaceNode = IgmpGetIfNode (u4Port, i4FsIgmpInterfaceAddrType);
    if ((pIfaceNode == NULL))
    {
        MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Ifacenode not exist with index %d", i4FsIgmpInterfaceIfIndex);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
	CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }
    pIfaceNode->u4GrpListId = u4SetValFsIgmpInterfaceGroupListId;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpJoinPktRate
 Input       :  The Indices

                The Object 
                setValFsIgmpInterfaceJoinPktRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpJoinPktRate (INT4 i4FsIgmpAddrType,
                                              INT4 i4SetValFsIgmpJoinPktRate)
{
        INT4 i4FsIgmpInterfaceIfIndex = 0;

	if (((i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4) 
		    && (gIgmpConfig.i4GlobalIgmpRateLimit == i4SetValFsIgmpJoinPktRate))
		|| ((i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
		    && (gIgmpConfig.i4GlobalMldRateLimit == i4SetValFsIgmpJoinPktRate))) 
	{
	    return SNMP_SUCCESS;
	}
#ifdef NPAPI_WANTED
	if (IgmpNpSetIfaceJoinRate (i4FsIgmpInterfaceIfIndex, 
				    i4FsIgmpAddrType,
                                    i4SetValFsIgmpJoinPktRate) == IGMP_FAILURE)
	{
            CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
	    return SNMP_FAILURE;
	}
#endif
	if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV4)
	{
       	    gIgmpConfig.i4GlobalIgmpRateLimit = i4SetValFsIgmpJoinPktRate;
        }
	else if (i4FsIgmpAddrType == IPVX_ADDR_FMLY_IPV6)
	{
            gIgmpConfig.i4GlobalMldRateLimit = i4SetValFsIgmpJoinPktRate;
        }
    UNUSED_PARAM (i4FsIgmpInterfaceIfIndex); 
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceJoinPktRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate (INT4 i4FsIgmpInterfaceIfIndex,
                                              INT4 i4FsIgmpInterfaceAddrType,
                                              INT4 i4SetValFsIgmpInterfaceJoinPktRate)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate()\r \n");
    /* Get the i/face node */
    if (IgmpGetPortFromIfIndex (i4FsIgmpInterfaceIfIndex,
                                i4FsIgmpInterfaceAddrType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
	MGMD_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface node not present for index %d", i4FsIgmpInterfaceIfIndex);
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate()\r \n");
        return SNMP_FAILURE;
    }

    if ((pIfaceNode = IgmpGetIfNode (u4Port,
                                     i4FsIgmpInterfaceAddrType)) != NULL)
    {
#ifdef NPAPI_WANTED
	if (IgmpNpSetIfaceJoinRate (i4FsIgmpInterfaceIfIndex, 
				    i4FsIgmpInterfaceAddrType,
                                    i4SetValFsIgmpInterfaceJoinPktRate) == IGMP_FAILURE)
	{
	    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate()\r \n");
            CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
	    return SNMP_FAILURE;
	}
#endif
	if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
	{
            pIfaceNode->u4IgmpJoinPktRate = 
			(UINT4) i4SetValFsIgmpInterfaceJoinPktRate;
	    
	    IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "IGMP join packet rate for interface index %d is %d", 
			i4FsIgmpInterfaceIfIndex, pIfaceNode->u4IgmpJoinPktRate );
	}
	else if (i4FsIgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV6)
	{
            pIfaceNode->u4MldJoinPktRate = 
			(UINT4) i4SetValFsIgmpInterfaceJoinPktRate;
	    
	    MLD_DBG2 (MLD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "MLD join packet rate for interface index %d is %d", 
			i4FsIgmpInterfaceIfIndex, pIfaceNode->u4MldJoinPktRate);
	}
	MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate()\r \n");
        return SNMP_SUCCESS;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate()\r \n");
    CLI_SET_ERR (CLI_IGMP_WRONG_INPUTS);
    return SNMP_FAILURE;
}


/****************************************************************************
 Function    : IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus 
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen

                The Object 
                setValFsIgmpGrpListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus (UINT4 u4FsIgmpGrpListId,
                                          UINT4 u4FsIgmpGrpIP,
                                          UINT4 u4FsIgmpGrpPrefixLen,
                                          INT4 i4AddfamilyType,
                                          INT4 i4SetValFsIgmpGrpListRowStatus)
{
    tIgmpGrpList       *pGrpListNode = NULL;
    tIgmpGrpList       *pGrpNode = NULL;
    tIgmpGrpList        GrpNode;
    tRBElem            *pRBElem = NULL;
    UINT4               u4EndAdr;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus()\r \n");
    UNUSED_PARAM (i4AddfamilyType);
    switch (i4SetValFsIgmpGrpListRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpGrpLstPoolId,
                               pGrpListNode, tIgmpGrpList);
            if (pGrpListNode == NULL)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                          "Memory allocation for GroupList Node failed\r\n");
		SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                                IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
			        "for GroupList Node for the Grp :%d\r\n", u4FsIgmpGrpIP);
                CLI_SET_ERR (CLI_IGMP_GRP_LIST_MEM_FAIL);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			     IgmpGetModuleName(IGMP_EXIT_MODULE),
                 	     "Exiting  IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus()\r \n");
		return SNMP_FAILURE;
            }
            GrpNode.u4GroupId = u4FsIgmpGrpListId;
            GrpNode.u4GroupIP = 0;
            GrpNode.u4PrefixLen = 0;
            pGrpNode =
                RBTreeGetNext (gIgmpGrpList, (tRBElem *) & GrpNode, NULL);
            while (pGrpNode != NULL)
            {
                if ((pGrpNode->u4GroupId != u4FsIgmpGrpListId))
                    break;
                u4EndAdr = (pGrpNode->u4GroupIP) | (~(pGrpNode->u4PrefixLen));
                if ((u4FsIgmpGrpIP >= pGrpNode->u4GroupIP) &&
                    (u4FsIgmpGrpIP <= u4EndAdr))
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                              "Summarised GroupList Record already exists\r\n");
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                                  "Summarised GroupList Record already exists"));
                    MemReleaseMemBlock (gIgmpMemPool.IgmpGrpLstPoolId,
                                        (UINT1 *) pGrpListNode);
                    CLI_SET_ERR (CLI_IGMP_GRP_LIST_SUMMARY_RECORD_EXISTS);
                    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                             IgmpGetModuleName(IGMP_EXIT_MODULE),
                             "Exiting  IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus()\r \n");
		    return SNMP_FAILURE;
                }
                pGrpNode =
                    RBTreeGetNext (gIgmpGrpList, (tRBElem *) pGrpNode, NULL);
            }
            pGrpListNode->u4GroupId = u4FsIgmpGrpListId;
            pGrpListNode->u4GroupIP = u4FsIgmpGrpIP;
            pGrpListNode->u4PrefixLen = u4FsIgmpGrpPrefixLen;
            if (RB_FAILURE ==
                RBTreeAdd (gIgmpGrpList, (tRBElem *) pGrpListNode))
            {
                MemReleaseMemBlock (gIgmpMemPool.IgmpGrpLstPoolId,
                                    (UINT1 *) pGrpListNode);
                CLI_SET_ERR (CLI_IGMP_GRP_LIST_RB_ADD_FAIL);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                             IgmpGetModuleName(IGMP_EXIT_MODULE),
                             "Exiting  IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus()\r \n");
		return SNMP_FAILURE;
            }
            if (CREATE_AND_WAIT == i4SetValFsIgmpGrpListRowStatus)
            {
                pGrpListNode->u4RowStatus = NOT_IN_SERVICE;
            }
            else
            {
                pGrpListNode->u4RowStatus = ACTIVE;
            }
            break;

        case NOT_IN_SERVICE:
        case ACTIVE:
            GrpNode.u4GroupId = u4FsIgmpGrpListId;
            GrpNode.u4GroupIP = u4FsIgmpGrpIP;
            GrpNode.u4PrefixLen = u4FsIgmpGrpPrefixLen;
            pGrpListNode =
                (tIgmpGrpList *) RBTreeGet (gIgmpGrpList,
                                            (tRBElem *) & GrpNode);
            if (pGrpListNode == NULL)
            {
                CLI_SET_ERR (CLI_IGMP_NO_GRP_LIST_RECORD_EXISTS);
		IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                              "Summarised GroupList Record already exists\r\n");
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, 
			     IgmpGetModuleName(IGMP_EXIT_MODULE),
                 	     "Exiting  IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus()\r \n");
		return SNMP_FAILURE;
            }
            pGrpListNode->u4RowStatus = (UINT4) i4SetValFsIgmpGrpListRowStatus;
            break;

        case DESTROY:
            GrpNode.u4GroupId = u4FsIgmpGrpListId;
            GrpNode.u4GroupIP = u4FsIgmpGrpIP;
            GrpNode.u4PrefixLen = u4FsIgmpGrpPrefixLen;
            pRBElem = RBTreeGet (gIgmpGrpList, (tRBElem *) & GrpNode);
            if (pRBElem == NULL)
            {
                CLI_SET_ERR (CLI_IGMP_NO_GRP_LIST_RECORD_EXISTS);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                             IgmpGetModuleName(IGMP_EXIT_MODULE),
                             "Exiting  IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus()\r \n");
		return SNMP_FAILURE;
            }
            RBTreeRemove (gIgmpGrpList, pRBElem);
            IgmpMemRelease (gIgmpMemPool.IgmpGrpLstPoolId, (UINT1 *) pRBElem);
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus()\r \n");
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpGlobalLimit
 Input       :  The Indices

                The Object 
                setValFsIgmpGlobalLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpGlobalLimit (INT4 i4FsIgmpAddrType,
                                     UINT4 u4SetValFsIgmpGlobalLimit)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpGlobalLimit()\r \n");
    UNUSED_PARAM (i4FsIgmpAddrType);
    if (((gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO) &&
         (u4SetValFsIgmpGlobalLimit == IGMP_ZERO)) ||
        (gIgmpConfig.u4GlobalGrpLimit == IGMP_ZERO) ||
        (u4SetValFsIgmpGlobalLimit < gIgmpConfig.u4GlobalCurCnt))
    {
        IgmpResetAllInterfaceLimitCounters ();
        gIgmpConfig.u4GlobalCurCnt = IGMP_ZERO;
    }
    gIgmpConfig.u4GlobalGrpLimit = u4SetValFsIgmpGlobalLimit;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpGlobalLimit()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus
 Input       :  The Indices

                The Object 
                i4SetValFsIgmpSSMMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus (INT4 i4SetValFsIgmpSSMMapStatus,
                                      INT4 i4FsIgmpAddrType)
{
    tIgmpIface              *pIfNode = NULL;
    tTMO_SLL_NODE           *pNextSrcLink = NULL;
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    tIgmpGroup              *pGrp = NULL;
    tIgmpGroup              *pNextGrp = NULL;
    tIgmpSource             *pSrc = NULL;
    tIgmpIPvXAddr            ReporterAddrZero;
    tTMO_SLL_NODE           *pSllNode = NULL;
    UINT4                    u4Count = IGMP_ZERO;
    UINT4                    u4IPvXZero = IGMP_ZERO;
    INT4                     i4MapStatus = IGMP_FALSE;
    UINT1                    u1ErrFlag = IGMP_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus()\r \n");
    UNUSED_PARAM (i4FsIgmpAddrType);

    if (IGMP_SSM_MAP_GLOBAL_STATUS() == (UINT1) i4SetValFsIgmpSSMMapStatus)
    {
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_ALL_MODULES, IgmpTrcGetModuleName(IGMP_ALL_MODULES),
                "IGMP SSM Map is already configured with the same global status.\r\n");
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting  IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus()\r \n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsIgmpSSMMapStatus == IGMP_DISABLE)
    {
        /* remove all the SSM mapped entries */
        IgmpUtilClearAllSSMMappedGroups ();
    }

    IGMP_SSM_MAP_GLOBAL_STATUS() = (UINT1) i4SetValFsIgmpSSMMapStatus;

    if (i4SetValFsIgmpSSMMapStatus == IGMP_ENABLE)
    {
        RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
        if (u4Count != IGMP_ZERO)
        {
            /* Map sources for static IGMP group entries */
            RBTreeCount (gIgmpGroup, &u4Count);
            if (u4Count != IGMP_ZERO)
            {
                MEMSET (&ReporterAddrZero, IGMP_ZERO, sizeof (tIgmpIPvXAddr));
                pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
                while (pGrp != NULL)
                {
                    pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                            (tRBElem *) pGrp, NULL);
                    /* check whether SSM mapping is configured for the group */
                    pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (pGrp->u4Group, &i4MapStatus);
                    if ((pSSMMappedGrp == NULL) && (i4MapStatus == IGMP_FALSE))
                    {
                        pGrp = pNextGrp;
                        continue;
                    }
                    else
                    {
                        /* SSM mapping is present for this group, 
                         * hence update MRP to create route */
                        for (pSrc = (tIgmpSource *)
                             TMO_SLL_First (&(pGrp->srcList));
                             pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
                        {
                            pNextSrcLink =
                                TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);

                            pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_MAPPED;
                            if (pSrc->u1FwdState == IGMP_TRUE)
                            {
                                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                             &pSrc->Link);
                            }
                        }

                        if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                        {
                            /* Updating MRPs */
                            IgmpUpdateMrps (pGrp, IGMP_INCLUDE);
                        }
                    }

                    for (u4Count = IGMP_ZERO; u4Count < MAX_IGMP_SSM_MAP_SRC; u4Count++)
                    {
                        IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
                        IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                                (UINT1 *) &u4IPvXZero);
                        if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero, pSSMMappedGrp->SrcAddr[u4Count]) != IGMP_ZERO)
                        {
                            pSrc = IgmpSourceLookup (pGrp, pSSMMappedGrp->SrcAddr[u4Count], ReporterAddrZero);
                            if ((pGrp->u1StaticFlg == IGMP_TRUE) &&
                                ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) || (pSrc != NULL)))
                            {
                                if ((IgmpAddSSMMappedSourcesForStaticGroup (pGrp, pSSMMappedGrp,
                                                pGrp->pIfNode, &(pSSMMappedGrp->SrcAddr[u4Count]),
                                                IGMP_CREATE_AND_GO, IGMP_FALSE)) == IGMP_NOT_OK)
                                {
                                    u1ErrFlag = IGMP_TRUE;
                                }
                            }
                        }
                    }
                    pGrp = pNextGrp;
                }
            }

            /* Send out query for dynamically learnt entries */
            TMO_SLL_Scan ((&gIgmpIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
            {
                pIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pSllNode);
                if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    IGMP_DBG_INFO(IGMP_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                            "Going to send IGMP Query Messages\r\n");
                    IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
                }
            }
        }
    }

    if (u1ErrFlag == IGMP_TRUE)
    {
        CLI_SET_ERR (CLI_IGMP_SSMMAP_STATIC_GRP_ASSOC);
        MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                "Exiting  IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus()\r \n");
        return SNMP_FAILURE;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus()\r \n");
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus
 Input       :  The Indices

                The Object 
                i4SetValFsIgmpSSMMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus (UINT4 u4FsIgmpStartGrpAddr, 
                                        UINT4 u4FsIgmpEndGrpAddr,
                                        UINT4 u4FsIgmpSourceAddr,
                                        INT4 i4SetValFsIgmpSSMMapRowStatus)
                                      
{
    tIgmpSSMMapGrpEntry     *pSSMMappedGrp = NULL;
    tIgmpIface              *pIfNode = NULL;
    tTMO_SLL_NODE           *pSllNode = NULL;
    tTMO_SLL_NODE           *pNextSrcLink = NULL;
    tIgmpSSMMapGrpEntry     *pNewSSMMappedGrp = NULL;
    tIgmpIPvXAddr           u4StartGrp;
    tIgmpIPvXAddr           u4EndGrp;
    tIgmpIPvXAddr           u4Source;
    tIgmpIPvXAddr           ReporterAddrZero;
    tIgmpGroup              *pGrp = NULL;
    tIgmpGroup              *pNextGrp = NULL;
    tIgmpSource             *pSrc = NULL;
    UINT4                   u4Index = IGMP_ZERO;
    UINT4                   u4IPvXZero = IGMP_ZERO;
    UINT4                   u4Count = IGMP_ZERO;
    INT4                    i4Status = IGMP_FALSE;
    INT1                    i1RetVal = SNMP_SUCCESS;
    UINT1                   u1AddFlag = IGMP_FALSE;
    UINT1                   u1ErrFlag = IGMP_FALSE;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                "Entering IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus()\r \n");

    IGMP_IPVX_ADDR_CLEAR (&u4StartGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4EndGrp);
    IGMP_IPVX_ADDR_CLEAR (&u4Source);
    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);

    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
            (UINT1 *) &u4Index);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4StartGrp, IPVX_ADDR_FMLY_IPV4,
                        (UINT1 *) &u4FsIgmpStartGrpAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4EndGrp, IPVX_ADDR_FMLY_IPV4,
                        (UINT1 *) &u4FsIgmpEndGrpAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (u4Source, IPVX_ADDR_FMLY_IPV4,
                        (UINT1 *) &u4FsIgmpSourceAddr);

    /* TODO: Handle for create_go, active, destroy.
     * query to be sent for the received group in both active & destroy */ 
    switch (i4SetValFsIgmpSSMMapRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        case ACTIVE:
            pSSMMappedGrp = IgmpUtilChkIfSSMMapExists (u4StartGrp, &i4Status);
            if ((IGMP_TRUE == i4Status) && (NULL != pSSMMappedGrp) &&
                ((IGMP_IPVX_ADDR_COMPARE (pSSMMappedGrp->StartGrpAddr,u4StartGrp) == IGMP_ZERO)
                && (IGMP_IPVX_ADDR_COMPARE (pSSMMappedGrp->EndGrpAddr,u4EndGrp) == IGMP_ZERO)))
            {
                for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
                {
                    if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero, pSSMMappedGrp->SrcAddr[u4Index]) == IGMP_ZERO)
                    {
                        IGMP_IPVX_ADDR_COPY(&(pSSMMappedGrp->SrcAddr[u4Index]),&u4Source);
                        pSSMMappedGrp->u4SrcCnt++;
                        SortIgmpSSMMappedSources (pSSMMappedGrp->SrcAddr);
                        /* Send out query for dynamically learnt entries */
                        TMO_SLL_Scan ((&gIgmpIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
                        {
                            pIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pSllNode);
                            if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                            {
                                IGMP_DBG_INFO(IGMP_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                                        "Going to send IGMP Query Messages\r\n");
                                IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
                            }
                        }
                        u1AddFlag = IGMP_TRUE;
                        break;
                    }
                }
            }
            else if ((IGMP_FALSE == i4Status) && (NULL == pSSMMappedGrp))
            {
                IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSSMMapGrpId, pNewSSMMappedGrp, tIgmpSSMMapGrpEntry);
                if (NULL == pNewSSMMappedGrp)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IgmpGetModuleName(IGMP_GRP_MODULE),
                            "Failure in allocate Memory block for SSM Map node \r\n");
                    CLI_SET_ERR (CLI_IGMP_SSMMAP_MAX_LIMIT_RCHD);
                    return SNMP_FAILURE;
                }
                for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
                {
                    IGMP_IPVX_ADDR_INIT_IPV4 (pNewSSMMappedGrp->SrcAddr[u4Index], IPVX_ADDR_FMLY_IPV4,
                            (UINT1 *) &u4IPvXZero);
                }
                IGMP_IPVX_ADDR_COPY(&(pNewSSMMappedGrp->StartGrpAddr),&u4StartGrp);
                IGMP_IPVX_ADDR_COPY(&(pNewSSMMappedGrp->EndGrpAddr),&u4EndGrp);
                IGMP_IPVX_ADDR_COPY(&(pNewSSMMappedGrp->SrcAddr[IGMP_ZERO]),&u4Source);
                pNewSSMMappedGrp->u4SrcCnt++;
                if (RB_FAILURE == RBTreeAdd
                        (gIgmpSSMMappedGrp,(tRBElem *) pNewSSMMappedGrp))
                {
                    IgmpMemRelease (gIgmpMemPool.IgmpSSMMapGrpId, (UINT1 *) pNewSSMMappedGrp);
                    CLI_SET_ERR (CLI_IGMP_SSMMAP_RB_ADD_FAIL);
                    i1RetVal = SNMP_FAILURE;
                }
                else
                {
                    TMO_SLL_Scan ((&gIgmpIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
                    {
                        pIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pSllNode);
                        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                        {
                            IGMP_DBG_INFO(IGMP_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                                    "Going to send IGMP Query Messages\r\n");
                            IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
                        }
                    }
                    u1AddFlag = IGMP_TRUE;
                }
            }
            if (u1AddFlag == IGMP_TRUE)
            {
                RBTreeCount (gIgmpGroup, &u4Count);
                if (u4Count != IGMP_ZERO)
                {
                    MEMSET (&ReporterAddrZero, IGMP_ZERO, sizeof (tIgmpIPvXAddr));
                    pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
                    while (pGrp != NULL)
                    {
                        pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                (tRBElem *) pGrp, NULL);
                        /* check whether SSM mapping is configured for the group */
                        pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (pGrp->u4Group, &i4Status);
                        if ((pSSMMappedGrp == NULL) && (i4Status == IGMP_FALSE))
                        {
                            pGrp = pNextGrp;
                            continue;
                        }
                        else
                        {
                            /* SSM mapping is present for this group, 
                             * hence update MRP to create route */
                            for (pSrc = (tIgmpSource *)
                                    TMO_SLL_First (&(pGrp->srcList));
                                    pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
                            {
                                pNextSrcLink =
                                    TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);

                                pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_MAPPED;
                                if (pSrc->u1FwdState == IGMP_TRUE)
                                {
                                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                                    TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                            &pSrc->Link);
                                }
                            }

                            if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
                            {
                                /* Updating MRPs */
                                IgmpUpdateMrps (pGrp, IGMP_INCLUDE);
                            }
                        }
                        if ((pGrp->u1StaticFlg == IGMP_TRUE) &&
                            ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) ||
                            ((pSrc = IgmpSourceLookup (pGrp, u4Source, ReporterAddrZero)) != NULL)))
                        {
                            if ((IgmpAddSSMMappedSourcesForStaticGroup (pGrp, pSSMMappedGrp,
                                            pGrp->pIfNode, &u4Source, IGMP_CREATE_AND_GO,
                                            IGMP_FALSE)) == IGMP_NOT_OK)
                            {
                                u1ErrFlag = IGMP_TRUE;
                            }
                        }
                        pGrp = pNextGrp;
                    }
                }
            }
            if (u1ErrFlag == IGMP_TRUE)
            {
                CLI_SET_ERR (CLI_IGMP_SSMMAP_STATIC_GRP_ASSOC);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus()\r \n");
                i1RetVal = SNMP_FAILURE;
            }
            break;
        case NOT_IN_SERVICE:
        case DESTROY:
            if (IGMP_FAILURE == IgmpUtilDeleteSSMMappedGroup (u4StartGrp,
                    u4EndGrp, u4Source))
            {
                CLI_SET_ERR (CLI_IGMP_SSMMAP_RB_REMOVE_FAIL);
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting  IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus()\r \n");
                i1RetVal = SNMP_FAILURE;
            }
            else
            {
                TMO_SLL_Scan ((&gIgmpIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
                {
                    pIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pSllNode);
                    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        IGMP_DBG_INFO(IGMP_DBG_FLAG, IGMP_QRY_MODULE, IgmpGetModuleName(IGMP_QRY_MODULE),
                                "Going to send IGMP Query Messages\r\n");
                        IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
                    }
                }
            }
            break;
        default:
            break;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                 "Exiting  IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus()\r \n");
    return i1RetVal;
}
