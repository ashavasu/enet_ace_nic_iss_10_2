/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmptmr.c,v 1.35 2017/05/30 11:13:43 siva Exp $
 *
 * Description:This file contains the routines to maintain IGMP Timers  
 *
 *******************************************************************/

#include "igmpinc.h"

extern UINT4        gu4MaxIgmpGrps;
extern UINT4        gu4MaxMldGrps;

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_TMR_MODULE;
#endif
/***************************************************************************/
/*     Function Name : IgmpTmrInitTmrDesc                                  */
/*                                                                         */
/*     Description   : This function is used to init timer decroptors      */
/*                                                                         */
/*     Input(s)      : None.                                               */
/*                                                                         */
/*     Output(s)     : None                                                */
/*                                                                         */
/*     Returns       : None.                                               */
/***************************************************************************/
VOID
IgmpTmrInitTmrDesc (VOID)
{
    /* IGMP start up query timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_STARTUP_QUERY_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpQuery, Timer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_STARTUP_QUERY_TIMER_ID].TmrExpFn =
        IgmpStartUpQryIntvlExpTmr;

    /* IGMP Query timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_QUERY_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpQuery, Timer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_QUERY_TIMER_ID].TmrExpFn = IgmpQryIntvlExpTmr;

    /* IGMP Other querier present timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_QUERIER_PRESENT_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpQuery, Timer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_QUERIER_PRESENT_TIMER_ID].TmrExpFn =
        IgmpOtherQuerierPresentExpTmr;

    /* IGMP Group timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_GROUP_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpGroup, Timer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_GROUP_TIMER_ID].TmrExpFn = IgmpGrpExpTmr;

    /* IGMP source timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_SOURCE_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpSource, srcTimer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_SOURCE_TIMER_ID].TmrExpFn = IgmpSrcExpTmr;

    /* IGMP Group and source query timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_QUERY_SCHEDULE_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpSchQuery, schTimer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_QUERY_SCHEDULE_TIMER_ID].TmrExpFn =
        IgmpGrpSrcSchQryExpTmr;

    /* IGMP Group Version 1 Host present timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_VERSION1_HOST_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpGroup, Version1HostTimer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_VERSION1_HOST_TIMER_ID].TmrExpFn =
        IgmpV1HostPresentExpTmr;

    /* IGMP Group Version 2 Host present timer */
    gIgmpConfig.aIgmpTmrDesc[IGMP_VERSION2_HOST_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpGroup, Version2HostTimer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_VERSION2_HOST_TIMER_ID].TmrExpFn =
        IgmpV2HostPresentExpTmr;

    /* IGMP Multicast Group timer */

    gIgmpConfig.aIgmpTmrDesc[IGMP_HOST_MCAST_GROUP_TIMER_ID].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpHostMcastGrpEntry, Timer);
    gIgmpConfig.aIgmpTmrDesc[IGMP_HOST_MCAST_GROUP_TIMER_ID].TmrExpFn =
        IgmpMcastGrpExpTmr;

    return;
}

/***************************************************************************/
/*     Function Name :     IgmpProcessTimerExpiryEvent                     */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpProcessTimerExpiryEvent                 **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpProcessTimerExpiryEvent (VOID)
{
    tIgmpTimer         *pExpiredTmr = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;
    INT2                i2TmrCnt = 0;
    IGMP_LOCK ();
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpProcessTimerExpiryEvent()\r \n");
    /* Get the expired timer from the list */
    while ((pExpiredTmr = (tIgmpTimer *)
            TmrGetNextExpiredTimer (gIgmpTimerListId)) != NULL)
    {
        /* Get the timer id and Offset */
        u1TimerId = ((tIgmpTimer *) pExpiredTmr)->TimerBlk.u1TimerId;
        i2Offset = gIgmpConfig.aIgmpTmrDesc[u1TimerId].i2Offset;

        /* Call the corresponding timer expiry handler to the
         * expiry of the timer */
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                   "Expired the timer for timer id %d\n", u1TimerId);
        (*(gIgmpConfig.aIgmpTmrDesc[u1TimerId].TmrExpFn))
            ((UINT1 *) pExpiredTmr - i2Offset);

        i2TmrCnt++;
        if (i2TmrCnt >= IGMP_MAX_TIMER_COUNTS)
        {
            if (OsixEvtSend (gIgmpTaskId, IGMP_TIMER_EXPIRY_EVENT)
                != OSIX_SUCCESS)
            {
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "Sending Timer Expiry Evt Failed\n");

            }
            else
            {
                IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                               IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                               "Relinquishing after Processing %d Timers and "
                               "Posting Timer Expiry event Succeeds\n",
                               i2TmrCnt);
            }
            break;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpProcessTimerExpiryEvent()\r \n");
    IGMP_UNLOCK ();
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpExpireGroup                                     */
/*                                                                         */
/*     Description   : This fuction removes the group membership forthe    */
/*                     given group.                                        */
/*     Input(s)      : pIfNode - Pointer to interface node                 */
/*                     pGrp - Pointer to the group record                  */
/*                     u1Version - IGMP version                            */
/*                     u1Consolidate - Flag to indicate the consolidation  */
/*                                                                         */
/*     Output(s)     : None                                                */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpExpireGroup                             **/
/** $$TRACE_PROCEDURE_LEVEL = INTMD                                       **/
/***************************************************************************/
VOID
IgmpExpireGroup (tIgmpIface * pIfNode, tIgmpGroup * pGrp,
                 UINT1 u1Version, UINT1 u1Consolidate, UINT1 u1Staticflag)
{
    tGrpNoteMsg         GrpNoteMsg;
    tRBElem            *pRBElem = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpExpireGroup()\r \n");
    IgmpFillMem (&GrpNoteMsg, IGMP_ZERO, sizeof (tGrpNoteMsg));

    if (pGrp->u1StaticFlg == IGMP_FALSE)
    {
        if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
        {
            IGMPSTOPTIMER (pGrp->Timer);
        }

        if (pGrp->u1V1HostPresent == IGMP_TRUE)
        {
            if ((pGrp->Version1HostTimer).u1TmrStatus == IGMP_TMR_SET)
            {
                IGMPSTOPTIMER (pGrp->Version1HostTimer);
            }
        }
        if (pGrp->u1V2HostPresent == IGMP_TRUE)
        {
            if ((pGrp->Version2HostTimer).u1TmrStatus == IGMP_TMR_SET)
            {
                IGMPSTOPTIMER (pGrp->Version2HostTimer);
            }
        }
        if ((pGrp->u1DynamicFlg == IGMP_TRUE)
            && (pGrp->u1DelSyncFlag == IGMP_TRUE))
        {
            pGrp->u1GrpModeFlag = IGMP_DELETE_GROUP;
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
            IgmpRedSyncDynInfo ();
            pGrp->u1DelSyncFlag = IGMP_FALSE;
        }
        /* IGMP Proxy is enabled so update the consolidated
         * group membership data base */
        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            if (u1Consolidate == IGMP_TRUE)
            {
                /* Call the PROXY routine to update the consolidate group
                 * membership data base for group */
                IgpGrpConsolidate (pGrp, u1Version, IGMP_GROUP_DELETION);
            }
        }
        else
        {
            /* Updating MRPs */
            IgmpUpdateMrps (pGrp, IGMP_LEAVE);
        }
        pRBElem = RBTreeGet (gIgmpGroup, (tRBElem *) pGrp);
        if (u1Staticflag == IGMP_FALSE)
        {
            if ((pGrp->u1LimitFlag & IGMP_GLOBAL_LIMIT_COUNTED))
            {
                if (gIgmpConfig.u4GlobalCurCnt > IGMP_ZERO)
                {
                    gIgmpConfig.u4GlobalCurCnt--;
                }
            }
            if ((pGrp->u1LimitFlag & IGMP_INTERFACE_LIMIT_COUNTED))
            {
                if (pIfNode->u4GrpCurCnt > IGMP_ZERO)
                {
                    pIfNode->u4GrpCurCnt--;
                }
            }
        }
        IgmpDeleteAllV1V2GrpReporters (pGrp);
        if (pGrp->GrpReporterTree != NULL)
        {
            RBTreeDelete (pGrp->GrpReporterTree);
            pGrp->GrpReporterTree = NULL;
        }
        RBTreeRemove (gIgmpGroup, pRBElem);
        IgmpMemRelease (gIgmpMemPool.IgmpGroupId, (UINT1 *) pGrp);
        pIfNode->u4InterfaceGroups--;
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                       IgmpGetModuleName (IGMP_GRP_MODULE),
                       "Group Node is deleted\n");
    }
    else
    {
        pGrp->u1DynamicFlg = IGMP_FALSE;
        IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
        IgmpDeleteAllV1V2GrpReporters (pGrp);
        pGrp->u1GrpModeFlag = IGMP_DELETE_GROUP;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
        IgmpRedSyncDynInfo ();
        /* IGMP Proxy is enabled so update the consolidated
         * group membership data base */
        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            if (u1Consolidate == IGMP_TRUE)
            {
                /* Call the PROXY routine to update the consolidate group
                 *                  * membership data base for group */
                IgpGrpConsolidate (pGrp, u1Version, IGMP_GROUP_DELETION);
            }
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpExpireGroup()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpExpireSource                                    */
/*                                                                         */
/*     Description   : This fuction removes the source membership forthe   */
/*                     given group and source                              */
/*     Input(s)      : Pointer to the Mcast Iface entry in Mcast Routetbl  */
/*                                                                         */
/*     Output(s)     : None                                                */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpExpireSource                            **/
/** $$TRACE_PROCEDURE_LEVEL = INTMD                                       **/
/***************************************************************************/
VOID
IgmpExpireSource (tIgmpSource * pSrc)
{
    tIgmpGroup         *pGrp = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpExpireSource()\r \n");

    pGrp = pSrc->pGrp;
    /* Get the Mcast Iface of this source */

    if ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pSrc->srcTimer);
    }

    IgmpDelSchQrySrcNode (pSrc);

    /* IGMP Proxy is enabled so update the consolidated
     * group membership data base */
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
        TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);

        if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
            (IGMP_IPVX_ADDR_COMPARE (gIPvXZero, pSrc->u4SrcAddress) !=
             IGMP_ZERO))
        {
            IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
        }
        else
        {
            IgmpUpdateMrps (pGrp, IGMP_LEAVE);
        }
    }
    /* yes, Release  the SourceNode */

    if (pSrc->u1DynamicFlg == IGMP_TRUE)
    {
        IgmpDeleteAllSrcReporters (pSrc);
    }
    if (pSrc->SrcReporterTree != NULL)
    {
        RBTreeDelete (pSrc->SrcReporterTree);
        pSrc->SrcReporterTree = NULL;
    }
    if (pSrc->u1DelSyncFlag == IGMP_TRUE)
    {
        pSrc->u1SrcModeFlag = IGMP_DELETE_SOURCE;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
        IgmpRedSyncDynInfo ();
    }
    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
    IgmpMemRelease (gIgmpMemPool.IgmpSourceId, (UINT1 *) pSrc);
    if (pGrp->u4Group.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        gu4MaxIgmpGrps--;
    }
    else
    {
        gu4MaxMldGrps--;
    }
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE,
                   IgmpGetModuleName (IGMP_GRP_MODULE),
                   "No reporters present deleting the source node");
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpExpireSource()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpStartUpQryIntvlExpTmr                         */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpStartUpQryIntvlExpTmr                   **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpStartUpQryIntvlExpTmr (VOID *pArg)
{
    tIgmpQuery         *pIgmp = pArg;
    tIgmpIface         *pIfNode = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpStartUpQryIntvlExpTmr()\r \n");
    pIfNode = pIgmp->pIfNode;

    /* Send General Query */
    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {

        IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
    }
    else
    {
        MldOutSendQuery (pIfNode, &gAllNodeV6McastAddr);
    }
    pIgmp->u1StartUpQCount++;
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_QRY_MODULE,
                   IgmpGetModuleName (IGMP_QRY_MODULE), "General Query Sent\n");

    if (pIgmp->u1StartUpQCount >= MAX_IF_STARTUP_QUERIES (pIfNode))
    {
        /* 
         * If the Startup Query count is exhausted
         * we need to reset the interval to regular
         * Query Timer Interval and set the timer ID
         * to the IGMP_QUERY_TIMER_ID
         */
        IGMPSTARTTIMER (pIgmp->Timer, IGMP_QUERY_TIMER_ID,
                        IGMP_IF_QUERY_INTERVAL (pIfNode), IGMP_ZERO);
    }
    else
    {
        /*
         * Until Startup Query count is exhausted
         * keep sending queries at a faster rate
         * defined by the IGMP_STARTUP_QIVAL
         */
        IGMPSTARTTIMER (pIgmp->Timer, IGMP_STARTUP_QUERY_TIMER_ID,
                        IGMP_STARTUP_QIVAL (pIfNode), IGMP_ZERO);
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpStartUpQryIntvlExpTmr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpQryIntvlExpTmr                              */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpQryIntvlExpTmr                          **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpQryIntvlExpTmr (VOID *pArg)
{
    tIgmpQuery         *pIgmp = pArg;
    tIgmpIface         *pIfNode = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpQryIntvlExpTmr()\r \n");
    pIfNode = pIgmp->pIfNode;

    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_QRY_MODULE,
                       IgmpGetModuleName (IGMP_QRY_MODULE),
                       "IGMP Query Messages are send\n");
        IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
    }
    else
    {
        MLD_DBG_INFO (MLD_DBG_FLAG, IGMP_QRY_MODULE,
                      IgmpGetModuleName (IGMP_QRY_MODULE),
                      "MLD Query Messages are send\n");
        MldOutSendQuery (pIfNode, &gAllNodeV6McastAddr);
    }
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                   "Timer started for Query Interval\n");
    IGMPSTARTTIMER (pIgmp->Timer, IGMP_QUERY_TIMER_ID,
                    IGMP_IF_QUERY_INTERVAL (pIfNode), IGMP_ZERO);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpQryIntvlExpTmr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpOtherQuerierPresentExpTmr                     */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpOtherQuerierPresentExpTmr               **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpOtherQuerierPresentExpTmr (VOID *pArg)
{
    tIgmpQuery         *pIgmp = pArg;
    tIgmpIface         *pIfNode = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpOtherQuerierPresentExpTmr()\r \n");
    pIfNode = pIgmp->pIfNode;

    /*
     * Gets the System time when the router becomes querier
     * And updates the querier address with router address. 
     */
    pIgmp->u1Querier = TRUE;
    IGMP_IPVX_ADDR_COPY (&(pIgmp->u4QuerierAddr), &(pIfNode->u4IfAddr));
    OsixGetSysTime (&pIgmp->u4QuerierUpTime);
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pIgmp->QuerierDbNode);
    IgmpRedSyncDynInfo ();

    if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {

        IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
    }
    else
    {
        MldOutSendQuery (pIfNode, &gAllNodeV6McastAddr);
    }

    IGMPSTARTTIMER (pIgmp->Timer, IGMP_QUERY_TIMER_ID,
                    pIfNode->u2QueryInterval, IGMP_ZERO);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpOtherQuerierPresentExpTmr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpGrpExpTmr                                   */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGrpExpTmr                               **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpGrpExpTmr (VOID *pArg)
{
    tIgmpGroup         *pGrp = pArg;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    UINT1               u1VerFlag = IGMP_ZERO;
    UINT1               u1Flag = IGMP_NOT_A_NEW_GROUP;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpGrpExpTmr()\r \n");
    /* 
     * If this timer expires then none of the hosts cared to report
     * for membership till this time. So this group membership
     * can be removed.
     */
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                   "IGMP Group timer expired\n");

    if ((pGrp->pIfNode->u1Version == IGMP_VERSION_3) ||
        (pGrp->pIfNode->u1Version == MLD_VERSION_2))
    {
        /* Filter mode is INCLUDE */
        if (pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)
        {
            for (pSrc = (tIgmpSource *)
                 TMO_SLL_First (&(pGrp->srcList));
                 pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
            {
                pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);
                /* Check whether the source is added statically */
                if (pSrc->u1StaticFlg == IGMP_FALSE)
                {
                    /* Release all the Source Node */
                    /* Delete all the host reported to this source */
                    IgmpDeleteAllSrcReporters (pSrc);
                    if (pSrc->SrcReporterTree != NULL)
                    {
                        RBTreeDelete (pSrc->SrcReporterTree);
                        pSrc->SrcReporterTree = NULL;
                    }
                    pSrc->u1SrcModeFlag = IGMP_DELETE_SOURCE;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pSrc->SrcDbNode);
                    IgmpRedSyncDynInfo ();
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    IgmpMemRelease (gIgmpMemPool.IgmpSourceId, (UINT1 *) pSrc);
                    if (pGrp->u4Group.u1Afi == IPVX_ADDR_FMLY_IPV4)
                    {
                        gu4MaxIgmpGrps--;
                    }
                    else
                    {
                        gu4MaxMldGrps--;
                    }
                }
                else
                {
                    pSrc->u1DynamicFlg = IGMP_FALSE;
                    IGMPSTOPTIMER (pSrc->srcTimer);
                    pSrc->u1SrcModeFlag = IGMP_ADD_SOURCE;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pSrc->SrcDbNode);
                    IgmpRedSyncDynInfo ();
                }
                /* MGMD_DBG (IGMP_DBG_MGMT, "MemReleased - Source Node..\n"); */
            }                    /* Scan for SourceNode - END */
            /* If static recoed exists for the same group
               Update the flg and stop the timer */
            if (pGrp->u1StaticFlg == IGMP_TRUE)
            {
                pGrp->u1DynamicFlg = IGMP_FALSE;
                IGMPSTOPTIMER (pGrp->Timer);
                IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
                pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
                IgmpRedSyncDynInfo ();
                return;
            }
            u1VerFlag = IGMP_VERSION_3 | IGMP_V3REP_SEND_FLAG;
            pGrp->u1DelSyncFlag = IGMP_TRUE;
            IgmpExpireGroup (pGrp->pIfNode, pGrp, IGMP_VERSION_3, IGMP_TRUE,
                             IGMP_FALSE);

        }
        else
        {
            for (pSrc = (tIgmpSource *)
                 TMO_SLL_First (&(pGrp->srcList));
                 pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
            {
                pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);

                /* Release all the Source Node */
                if (pSrc->u1FwdState == IGMP_FALSE)
                {
                    /* Delete all the host reported to this source */
                    IgmpDeleteAllSrcReporters (pSrc);
                    if (pSrc->SrcReporterTree != NULL)
                    {
                        RBTreeDelete (pSrc->SrcReporterTree);
                        pSrc->SrcReporterTree = NULL;
                    }
                    pSrc->u1SrcModeFlag = IGMP_DELETE_SOURCE;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pSrc->SrcDbNode);
                    IgmpRedSyncDynInfo ();
                    TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                    IgmpMemRelease (gIgmpMemPool.IgmpSourceId, (UINT1 *) pSrc);
                    if (pGrp->u4Group.u1AddrLen == IPVX_IPV4_ADDR_LEN)
                    {
                        gu4MaxIgmpGrps--;
                    }
                    else
                    {
                        gu4MaxMldGrps--;
                    }
                    /* MGMD_DBG (IGMP_DBG_MGMT, "MemReleased - Source Node..\n"); */
                }
            }                    /* Scan for SourceNode - END */

            if (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO)
            {
                /* If static recoed exists for the same group
                   Update the flg and stop the timer */
                if ((pGrp->u1StaticFlg == IGMP_TRUE)
                    && (pGrp->u1DynamicFlg == IGMP_TRUE))
                {
                    pGrp->u1DynamicFlg = IGMP_FALSE;
                    IGMPSTOPTIMER (pGrp->Timer);
                    IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
                    pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pGrp->GrpDbNode);
                    IgmpRedSyncDynInfo ();
                    return;
                }
                if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
                {
                    u1VerFlag = IGMP_VERSION_2;
                }
                else
                {
                    u1VerFlag = IGMP_VERSION_3;
                }

                u1VerFlag |= IGMP_V3REP_SEND_FLAG;
                pGrp->u1DelSyncFlag = IGMP_TRUE;
                IgmpExpireGroup (pGrp->pIfNode, pGrp, u1VerFlag, IGMP_TRUE,
                                 IGMP_FALSE);
            }
            else
            {
                (pGrp->Timer).u1TmrStatus = IGMP_TMR_RESET;
                pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
                if ((pGrp->u1StaticFlg == IGMP_TRUE)
                    && (pGrp->u1DynamicFlg == TRUE))
                {
                    pGrp->u1DynamicFlg = IGMP_FALSE;
                    IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
                    pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
                    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                             &pGrp->GrpDbNode);
                    IgmpRedSyncDynInfo ();
                }
                if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
                {
                    pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
                    u1Flag = IGMP_GROUP_UPGRADED;
                }

                if (pGrp->u1V1HostPresent == IGMP_TRUE)
                {
                    if ((pGrp->Version1HostTimer).u1TmrStatus == IGMP_TMR_SET)
                    {
                        IGMPSTOPTIMER (pGrp->Version1HostTimer);
                    }
                }
                if (pGrp->u1V2HostPresent == IGMP_TRUE)
                {
                    if ((pGrp->Version2HostTimer).u1TmrStatus == IGMP_TMR_SET)
                    {
                        IGMPSTOPTIMER (pGrp->Version2HostTimer);
                    }
                }

                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    /* IGMP Proxy is enabled so update the consolidated
                     * group membership data base */
                    IgpGrpConsolidate (pGrp, IGMP_VERSION_3, u1Flag);

                    if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
                    {
                        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE,
                                       IGMP_NAME, "IGMP PROXY: Sending "
                                       "upstream report failed \r\n");
                        IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                                  "IGMP PROXY: Sending "
                                  "upstream report failed \r\n");
                    }
                }
                else
                {
                    /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_INCLUDE);
                }
            }
        }
    }
    else
    {
        u1VerFlag = IGMP_VERSION_2 | IGMP_V3REP_SEND_FLAG;
        if (pGrp->u1StaticFlg == IGMP_TRUE)
        {
            pGrp->u1DynamicFlg = IGMP_FALSE;
            IGMPSTOPTIMER (pGrp->Timer);
            IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
            pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
            IgmpRedSyncDynInfo ();
            return;
        }
        pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpExpireGroup (pGrp->pIfNode, pGrp, u1VerFlag, IGMP_TRUE, IGMP_FALSE);
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpGrpExpTmr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpSrcExpTmr                                   */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpSrcExpTmr                               **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpSrcExpTmr (VOID *pArg)
{
    tIgmpSource        *pSrc = pArg;
    tIgmpGroup         *pGrp = NULL;
    tTMO_SLL_NODE      *pSrcNode = NULL;
    UINT1               u1VerFlag = IGMP_ZERO;
    INT4                i4Status = IGMP_ZERO;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpSrcExpTmr()\r \n");
    (pSrc->srcTimer).u1TmrStatus = IGMP_TIMER_RESET;

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                   "IGMP source timer expired\n");
    pGrp = pSrc->pGrp;
    /* If a source timer is expired then check whether
       same source record is configured statically
       If so, do not delete the record. Make dynamic flag FALSE
       and stop the source timer. */
    if ((pSrc->u1StaticFlg == IGMP_TRUE) && (pSrc->u1DynamicFlg == IGMP_TRUE))
    {
        pSrc->u1DynamicFlg = IGMP_FALSE;
        IGMPSTOPTIMER (pSrc->srcTimer);
        IgmpDelSchQrySrcNode (pSrc);
        IgmpDeleteAllSrcReporters (pSrc);
        IGMP_IPVX_ADDR_CLEAR (&(pSrc->u4LastReporter));
        IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
        pSrc->u1SrcModeFlag = IGMP_ADD_SOURCE;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
        IgmpRedSyncDynInfo ();
        TMO_SLL_Scan (&(pGrp->srcList), pSrcNode, tTMO_SLL_NODE *)
        {
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcNode);
            if (pSrc->u1DynamicFlg == IGMP_TRUE)
            {
                return;
            }
        }
        if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
        {
            u1VerFlag = IGMP_VERSION_2;
        }
        else
        {
            u1VerFlag = IGMP_VERSION_3;
        }

        u1VerFlag |= IGMP_V3REP_SEND_FLAG;

        /*  delete the group record */
        pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpExpireGroup (pGrp->pIfNode, pGrp, u1VerFlag, IGMP_TRUE, IGMP_FALSE);
        return;
    }

    if ((pGrp->u1FilterMode == IGMP_FMODE_INCLUDE) ||
        ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) &&
         (pGrp->u1StaticFlg == IGMP_TRUE) && (pGrp->u1DynamicFlg == IGMP_TRUE)
         && (IgmpUtilChkIfSSMMappedGroup (pGrp->u4Group, &i4Status) != NULL)))
    {
        IgmpExpireSource (pSrc);

        TMO_SLL_Scan (&(pGrp->srcList), pSrcNode, tTMO_SLL_NODE *)
        {
            pSrc = IGMP_GET_BASE_PTR (tIgmpSource, Link, pSrcNode);
            if (pSrc->u1DynamicFlg == IGMP_TRUE)
            {
                return;
            }
        }
        if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
        {
            u1VerFlag = IGMP_VERSION_2;
        }
        else
        {
            u1VerFlag = IGMP_VERSION_3;
        }

        u1VerFlag |= IGMP_V3REP_SEND_FLAG;

        /*  delete the group record */
        pGrp->u1DelSyncFlag = IGMP_TRUE;
        IgmpExpireGroup (pGrp->pIfNode, pGrp, u1VerFlag, IGMP_TRUE, IGMP_FALSE);
    }

    else if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
    {
        /* 
         * filter mode is exclude and source timer expires,
         * Don't delete source node. 
         * section 6.2.3 
         */
        (pSrc->srcTimer).u1TmrStatus = IGMP_TMR_RESET;

        pSrc->u1FwdState = IGMP_FALSE;
        pSrc->u1UpdatedToMrp = IGMP_FALSE;
        /* Updating to MRP also */
        IgmpDelSchQrySrcNode (pSrc);

        /* IGMP Proxy is enabled so update the consolidated
         * group membership data base */
        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            /* Call the PROXY routine to update the consolidate group
             * membership data base for group */
            IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_NOT_A_NEW_GROUP);

            if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                               "IGMP PROXY: Sending upstream report failed \r\n");
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "IGMP PROXY: Sending upstream report failed \r\n");
            }
        }
        else
        {
            /* Updating MRPs */
            TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
            TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
            IgmpUpdateMrps (pGrp, IGMP_LEAVE);
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpSrcExpTmr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpGrpSrcSchQryExpTmr                            */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGrpSrcSchQryExpTmr                      **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpGrpSrcSchQryExpTmr (VOID *pArg)
{
    tIgmpSchQuery      *pSchQuery = pArg;
    tIgmpIface         *pIfNode = NULL;

    pIfNode = pSchQuery->pIgmpQuery->pIfNode;

    IgmpMldSendSchQuery (pIfNode, pSchQuery);

    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpV1HostPresentExpTmr                           */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpV1HostPresentExpTmr                     **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpV1HostPresentExpTmr (VOID *pArg)
{
    tIgmpGroup         *pGrp = pArg;
    tIgmpIface         *pIfNode = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpV1HostPresentExpTmr()\r \n");

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                   "IGMPv1 Host present timer expired\n");
    pIfNode = pGrp->pIfNode;
    UNUSED_PARAM (pIfNode);
    /* The  router switches to GCM = IGMPv2 if Version2 
       timer is running, or the GCM = IGMPv3 */
    pGrp->u1V1HostPresent = IGMP_FALSE;

    if (pGrp->u1V2HostPresent == IGMP_TRUE)
    {
        pGrp->u4GroupCompMode = IGMP_GCM_IGMPV2;
    }
    else if (TMO_SLL_Count (&pGrp->srcList) != IGMP_ZERO)
    {
        pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
        if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
        {
            IGMPSTOPTIMER (pGrp->Timer);
        }

        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            /* Call the PROXY routine to update the consolidate group
             * membership data base for group */
            IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_GROUP_UPGRADED);

            if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                               "IGMP PROXY: Sending upstream report failed \r\n");
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "IGMP PROXY: Sending upstream report failed \r\n");
            }
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpV1HostPresentExpTmr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpV2HostPresentExpTmr                           */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpV2HostPresentExpTmr                     **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpV2HostPresentExpTmr (VOID *pArg)
{
    tIgmpGroup         *pGrp = pArg;
    tIgmpIface         *pIfNode = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpV2HostPresentExpTmr()\r \n");
    pIfNode = pGrp->pIfNode;

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                   "IGMPv2 Host present timer expired\n");
    UNUSED_PARAM (pIfNode);

    pGrp->u1V2HostPresent = IGMP_FALSE;

    if (pGrp->u1V1HostPresent == IGMP_TRUE)
    {
        pGrp->u4GroupCompMode = IGMP_GCM_IGMPV1;
    }
    else if (TMO_SLL_Count (&pGrp->srcList) != IGMP_ZERO)
    {
        pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
        pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;

        if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
        {
            IGMPSTOPTIMER (pGrp->Timer);
        }

        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
        {
            /* Call the PROXY routine to update the consolidate group
             * membership data base for group */
            IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_GROUP_UPGRADED);

            if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                               "IGMP PROXY: Sending upstream report failed \r\n");
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "IGMP PROXY: Sending upstream report failed \r\n");
            }
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpV2HostPresentExpTmr()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   IgmpMcastGrpExpTmr                                */
/*                                                                         */
/*     Description   :   This function is used to Process the timers       */
/*     Input(s)      :   None.                                             */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None.                                              */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpMcastGrpExpTmr                     **/
/** $$TRACE_PROCEDURE_LEVEL = LOW                                         **/
/***************************************************************************/
VOID
IgmpMcastGrpExpTmr (VOID *pArg)
{
    tIgmpHostMcastGrpEntry *pGrpEntry = (tIgmpHostMcastGrpEntry *) pArg;
    tIPvXAddr           IPvXGrpAddr;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMcastGrpExpTmr()\r \n");
    MEMSET (&IPvXGrpAddr, 0, sizeof (tIPvXAddr));
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE, IGMP_NAME,
                   "IGMP multicast group timer expired\n");
    if (pGrpEntry->u1Version != IGMP_VERSION_3)
    {
        MEMCPY (&IPvXGrpAddr, &(pGrpEntry->McastAddr), sizeof (tIPvXAddr));
        if (IgmpFormAndSendV1V2Report
            ((tIgmpIPvXAddr) IPvXGrpAddr, pGrpEntry->u1Version, IGMP_FALSE,
             pGrpEntry->u4IfIndex) == IGMP_FAILURE)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_TMR_MODULE | IGMP_ALL_MODULES,
                       IGMP_NAME,
                       "Sending of V1/V2 report to upstream failed for Mcast IP: %s\r\n",
                       IgmpPrintIPvxAddress (IPvXGrpAddr));
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "Exiting  IgmpMcastGrpExpTmr()\r \n");
            return;
        }
    }

    IGMPSTARTTIMER (pGrpEntry->Timer, IGMP_HOST_MCAST_GROUP_TIMER_ID,
                    IGMP_HOST_MCAST_JOIN_TIME,
                    (UINT4) SYS_NUM_OF_TIME_UNITS_IN_A_SEC *
                    IGMP_HOST_MCAST_JOIN_TIME);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMcastGrpExpTmr()\r \n");
    return;
}
