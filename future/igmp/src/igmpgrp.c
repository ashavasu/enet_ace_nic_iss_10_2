/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpgrp.c,v 1.55 2017/10/05 12:27:54 siva Exp $
 *
 * Description:This file contains the routines required for
 *                Group Membership Module                          
 *
 *******************************************************************/

#include "igmpinc.h"
#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;
#endif
/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpMatchGroupList                            */
/*                                                                         */
/*     Description   : Checks whther group(GrpAddr) matches in the 
 *                     grouplist mapped to an interface                    */
/*                                                                         */
/*     INPUT         : pIfNode - Interface Node 
 *                      GrpAddr - Grp Address                              */
/*     OUTPUT        :               */
/*                                                                         */
/*     RETURNS       : IGMP_GROUP_MATCHED/IGMP_NOT_GROUP_MATCHED                             */
/*                                                                         */
/***************************************************************************/
INT1
IgmpMatchGroupList (tIgmpIface * pIfNode, tIgmpIPvXAddr GrpAddr)
{
    UINT4               u4InGrpAdr;
    tIgmpGrpList       *pGrpList = NULL;
    tIgmpGrpList        GrpList;
    UINT4               u4EndAdr;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpMatchGroupList()\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpMatchGroupList"));
    MEMCPY (&u4InGrpAdr, GrpAddr.au1Addr, sizeof (UINT4));
    GrpList.u4GroupId = pIfNode->u4GrpListId;
    GrpList.u4GroupIP = 0;
    GrpList.u4PrefixLen = 0;
    pGrpList =
        (tIgmpGrpList *) RBTreeGetNext (gIgmpGrpList, (tRBElem *) & GrpList,
                                        NULL);
    while (pGrpList != NULL)
    {
        if (pGrpList->u4GroupId != pIfNode->u4GrpListId)
        {
            break;
        }
        u4EndAdr = (pGrpList->u4GroupIP) | (~(pGrpList->u4PrefixLen));
        if ((u4InGrpAdr >= pGrpList->u4GroupIP) && (u4InGrpAdr <= u4EndAdr))
        {
            return IGMP_GROUP_MATCHED;
        }
        pGrpList =
            (tIgmpGrpList *) RBTreeGetNext (gIgmpGrpList, (tRBElem *) pGrpList,
                                            NULL);
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpMatchGroupList"));
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpMatchGroupList()\r \n");
    return IGMP_GROUP_NOT_MACHED;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpCheckForLimit*/
/*                                                                         */
/*     Description   : Follows IGMP State Limit check. 
 * IGMP State limit Follows these checks through IgmpCheckForLimit, IgmpMatchGroupList
 * First checks for  global limit ,if it is reached, report will be ignored else 
 * checks for interface limit is configured or not. If not configured allow 
 * the report and increment the global counter  If global limit is configured. 
 * If interface limit is configured check for group matches in exclude group list.
 * 1) If group address matches in exclude group list then honor the report and 
 *    increment global counter.
 * 2)If group address does not matches in exclude group list and interface 
 * limit is not reached then honor the report, increment interface counter and 
 * global counter.
 * 3)If the group address does not match in the exclude group list and the 
 * interface limit has been reached, then ignore the join report.          */

/*     INPUT         : pIfNode - Interface Node 
 *                      GrpAddr - Grp Address                              */
/*     OUTPUT        :               */
/*                                                                         */
/*     RETURNS       :                                                     * 
 *       IGMP_IGNORE - ignore
 *       IGMP_GROUP_MATCHED - check Global limit configured if so increment*
 *                    Global Counter else do nothing.
 *       IGMP_INF_LIMIT_NOT_REACHED - check global limit configured if so  * 
 *               increment Globalcounter , interfaceLimitCounter           *
 *               else increment only interfacecounter alone.               *
 *       IGMP_INF_LIMIT_NOT_CONFIGURED - check global limit configured     *
 *          if so increment Globalcounter else do nothing                  * */
/***************************************************************************/

INT1
IgmpCheckForLimit (tIgmpIface * pIfNode, tIgmpIPvXAddr u4GrpAddr)
{
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpCheckForLimit()\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpCheckForLimit"));

    if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
    {
        return IGMP_ZERO;
    }

    if (gIgmpConfig.u4GlobalGrpLimit != 0)
    {
        if (gIgmpConfig.u4GlobalCurCnt >= gIgmpConfig.u4GlobalGrpLimit)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       " Membership report for group %s is ignored as Global Limit Reached \n",
                       IgmpPrintIPvxAddress (u4GrpAddr));
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                       IGMP_NAME,
                       " Membership report for group %s is ignored as Global Limit Reached \n",
                       IgmpPrintIPvxAddress (u4GrpAddr));
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                          " Membership report for group %s is ignored as Global Limit Reached",
                          IgmpPrintIPvxAddress (u4GrpAddr)));
            return IGMP_IGNORE;
        }
    }
    if (pIfNode->u4GrpLimit != 0)
    {
        if (pIfNode->u4GrpListId != 0)
        {
            if (IGMP_GROUP_MATCHED == IgmpMatchGroupList (pIfNode, u4GrpAddr))
            {
                return IGMP_GROUP_MATCHED;
            }
        }
        if (pIfNode->u4GrpLimit <= pIfNode->u4GrpCurCnt)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       " Membership report for group %s is ignored as Interface Limit Reached \n",
                       IgmpPrintIPvxAddress (u4GrpAddr));
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                       IGMP_NAME,
                       " Membership report for group %s is ignored as Interface Limit Reached \n",
                       IgmpPrintIPvxAddress (u4GrpAddr));
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                          " Membership report for group %s is ignored as Interface Limit Reached \n",
                          IgmpPrintIPvxAddress (u4GrpAddr)));
            return IGMP_IGNORE;
        }
        return IGMP_INF_LIMIT_NOT_REACHED;
    }
    else
    {
        return IGMP_INF_LIMIT_NOT_CONFIGURED;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpCheckForLimit"));
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpCheckForLimit()\r \n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     IgmpProcessGroup                                */
/*                                                                         */
/*     Description   :     This function is called whenever a Report       */
/*                         message is  received for a particular group     */
/*                         u4Group.                                        */
/*                                                                         */
/*     Input(s)      :    InterfaceId on which the report is received.     */
/*                        u4Group: Group Address for which the report      */
/*                        is received.                                     */
/*                        u1Version:  Version Number of IGMP Report.       */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    None                                             */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpProcessGroup                            **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpProcessGroup (tIgmpIface * pIfNode, tIgmpIPvXAddr u4GrpAddr,
                  UINT1 u1Version, tIgmpIPvXAddr u4HostAddr)
{
    tIgmpQuery         *pIgmp = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    UINT1               u1NewInterface = IGMP_NOT_A_NEW_GROUP;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpProcessGroup()\r \n");
    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE)
        && (IgpUtilCheckUpIface (pIfNode->u4IfIndex) == IGMP_TRUE))
    {
        /* IGMP Proxy is enabled and the interface is configures 
         * as an upstream interface. No reports should be processed 
         * on upstream interface. so return. */
        return;
    }

    /* Get the igmp query block */
    pIgmp = pIfNode->pIfaceQry;

    if ((pIfNode->u1Version == IGMP_VERSION_1) ||
        (pIfNode->u1Version == IGMP_VERSION_2) ||
        (pIfNode->u1Version == MLD_VERSION_1))
    {
        /* look for the group */
        pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
        if (pGrp == NULL)
        {
            /* group doesn't exists, add the group */
            pGrp = IgmpAddGroup (pIfNode, u4GrpAddr, IGMP_FALSE);
            if (pGrp == NULL)
            {
                /* failed to add group */
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                               IGMP_NAME, "Group node not created  \n");
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "Group node not created  \n");
                return;
            }
            u1NewInterface = IGMP_NEW_GROUP;

            pGrp->u1DynamicFlg = IGMP_TRUE;

            /* group added successfully */
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Membership for Group %s added in i/f %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);
            IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Membership for Group %s added in i/f %d \n",
                           IgmpPrintIPvxAddress (u4GrpAddr),
                           pIfNode->u4IfIndex);

            /* V1/V2 Join Report(*,G) */
            if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
            {
                /* Updating MRPs, this check is done for static group 
                 * addition since static configuration is allowed when IGMP
                 * is disabled */
                if ((gIgmpConfig.u1IgmpStatus == IGMP_ENABLE) ||
                    (gIgmpConfig.u1MldStatus == MLD_ENABLE))
                {
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
            }

        }
        else
        {
            pGrp->u1DynamicFlg = IGMP_TRUE;
            if (IgmpApplyLimitForGroup (pIfNode, pGrp) == IGMP_IGNORE)
            {
                pGrp->u1DynamicFlg = IGMP_FALSE;
                return;
            }
        }

        IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4HostAddr);
        IgmpReStartGrpTmr (pIfNode, pGrp);

        /* v2 router and recieved v1 report, start v1host present timer */
        if ((pIfNode->u1Version == IGMP_VERSION_2) &&
            (u1Version == IGMP_VERSION_1))
        {
            pGrp->u1V1HostPresent = IGMP_TRUE;
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV1;
            IGMPSTARTTIMER (pGrp->Version1HostTimer,
                            IGMP_VERSION1_HOST_TIMER_ID,
                            (UINT2) IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
        }

        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);
        }
        IgmpRedSyncDynInfo ();

        if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
            (u1NewInterface == IGMP_NEW_GROUP))
        {
            /* Call the PROXY routine to update the consolidate group
             * membership data base for group registration 
             * No need to update consolidated database, ifthe group is not
             * a new one */
            IgpGrpConsolidate (pGrp, u1Version, u1NewInterface);
        }
    }

    else if ((pIfNode->u1Version == IGMP_VERSION_3) ||
             (pIfNode->u1Version == MLD_VERSION_2))
    {
        pGrp = IgmpGrpLookUp (pIfNode, u4GrpAddr);
        if (pGrp == NULL)
        {
            pGrp = IgmpAddGroup (pIfNode, u4GrpAddr, IGMP_FALSE);
            if (pGrp == NULL)
            {
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "Group node not created  \n");
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                               "Group node not created  \n");

                return;
            }
            pGrp->u1DynamicFlg = IGMP_TRUE;
            u1NewInterface = IGMP_NEW_GROUP;

            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Membership for Group %s added in i/f %d \n",
                       IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);

            IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Membership for Group %s added in i/f %d \n",
                           IgmpPrintIPvxAddress (u4GrpAddr),
                           pIfNode->u4IfIndex);
        }
        else
        {
            pGrp->u1DynamicFlg = IGMP_TRUE;
            if (IgmpApplyLimitForGroup (pIfNode, pGrp) == IGMP_IGNORE)
            {
                pGrp->u1DynamicFlg = IGMP_FALSE;
                return;
            }
        }
        IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &u4HostAddr);

        /* V3Router recieved v1Report */
        if (u1Version == IGMP_VERSION_1)
        {
            pGrp->u1V1HostPresent = IGMP_TRUE;
            if ((u1NewInterface != IGMP_NEW_GROUP) &&
                (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV3))
            {
                u1NewInterface = IGMP_GROUP_DEGRADED;
            }
            pGrp->u4GroupCompMode = IGMP_GCM_IGMPV1;
            IGMPSTARTTIMER (pGrp->Version1HostTimer,
                            IGMP_VERSION1_HOST_TIMER_ID,
                            (UINT2) IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
        }
        /* V3Router recieved v2Report */
        if (u1Version == IGMP_VERSION_2)
        {
            if ((u1NewInterface != IGMP_NEW_GROUP) &&
                (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV3))
            {
                u1NewInterface = IGMP_GROUP_DEGRADED;
            }

            pGrp->u1V2HostPresent = IGMP_TRUE;
            if ((pGrp->u4GroupCompMode == IGMP_GCM_IGMPV3))
            {
                pGrp->u4GroupCompMode = IGMP_GCM_IGMPV2;
            }
            IGMPSTARTTIMER (pGrp->Version2HostTimer,
                            IGMP_VERSION2_HOST_TIMER_ID,
                            (UINT2) IGMP_IF_GROUP_TIMER (pIfNode), IGMP_ZERO);
        }

        /* Added for GCM , section: 7.3.2 */
        if ((pGrp->u4GroupCompMode == IGMP_GCM_IGMPV1) ||
            (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV2))
        {
            /* Convert the V1/V2 Report to IS_EX ({}) */
            IgmpGroupHandleIsExclude (pIfNode, pGrp->u4Group, IGMP_ZERO,
                                      NULL, u4HostAddr, IGMP_FALSE);
        }
        if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
            ((u1NewInterface == IGMP_GROUP_DEGRADED) ||
             (u1NewInterface == IGMP_NEW_GROUP)))
        {
            /* Call the PROXY routine to update the consolidate group
             * membership data base for group registration */
            IgpGrpConsolidate (pGrp, u1Version, u1NewInterface);
        }
    }

    else
    {
        return;
    }
    if (IgmpAddV1V2Reporter (pGrp, u4HostAddr) == IGMP_NOT_OK)
    {
        IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                   "Reporter (%s) addition failed for Group %s added in i/f %d \n",
                   IgmpPrintIPvxAddress (u4HostAddr),
                   IgmpPrintIPvxAddress (u4GrpAddr), pIfNode->u4IfIndex);

    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpProcessGroup()\r \n");
    UNUSED_PARAM (pIgmp);
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpApplyLimitForGroup                             */
/*                                                                         */
/*     Description   : Applies limit for the group.                        */
/*                                                                         */
/*     Input(s)      : pIfNode Igmp Interface Node
 *                     pGrp - GroupRecord                                  */
/*                       status.                                           */
/*     Output(s)    :    None                                              */
/*     Returns      : pointer to the interface block added to the mcast    */
/*                     router table.                                       */
/***************************************************************************/
INT1
IgmpApplyLimitForGroup (tIgmpIface * pIfNode, tIgmpGroup * pGrp)
{
    INT1                i1LimitSt;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpApplyLimitForGroup()\r \n");
    if (pGrp->u1LimitFlag != IGMP_ZERO)
    {
        return IGMP_OK;
    }
    i1LimitSt = IgmpCheckForLimit (pIfNode, pGrp->u4Group);
    if (((pGrp->u1StaticFlg == IGMP_TRUE) || (pGrp->u1LimitFlag != IGMP_ZERO))
        && (pGrp->u1DynamicFlg == IGMP_FALSE))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Group Limit Reached \n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       " Group Limit Reached \n");

        return IGMP_IGNORE;
    }
    if (IGMP_GROUP_MATCHED == i1LimitSt)
    {
        if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
        {
            gIgmpConfig.u4GlobalCurCnt++;
            pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
        }
    }
    else if (IGMP_INF_LIMIT_NOT_REACHED == i1LimitSt)
    {
        pIfNode->u4GrpCurCnt++;
        pGrp->u1LimitFlag |= IGMP_INTERFACE_LIMIT_COUNTED;
        if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
        {
            gIgmpConfig.u4GlobalCurCnt++;
            pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
        }
    }
    else if (IGMP_INF_LIMIT_NOT_CONFIGURED == i1LimitSt)
    {
        if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
        {
            gIgmpConfig.u4GlobalCurCnt++;
            pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpApplyLimitForGroup()\r \n");
    return IGMP_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  IgmpAddGroup                                       */
/*                                                                        */
/*     Description   : Adds the group membership on the interface IfId.   */
/*                                                                         */
/*     Input(s)      : InterfaceId on which group is to be added           */
/*                     groupaddress, index into the mcast route table,     */
/*                       status.                                           */
/*     Output(s)    :    None                                              */
/*     Returns      : pointer to the interface block added to the mcast    */
/*                     router table.                                       */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpAddGroup                                **/
/** $$TRACE_PROCEDURE_LEVEL = INTMD                                       **/
/***************************************************************************/
tIgmpGroup         *
IgmpAddGroup (tIgmpIface * pIfNode, tIgmpIPvXAddr u4Group, UINT1 u1Staticflag)
{
    UINT4               u4CacheUpTime = IGMP_ZERO;
    UINT4               u4Status = IGMP_ZERO;
    tIgmpGroup         *pGrp = NULL;
    INT1                i1LimitSt = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpAddGroup()\r \n");
    /* Added as a part of support for IGMP standard MIB RFC:2933
     * Handled failure in allocating Memblock and intialised
     * CacheSelf flag and V1HostPresent flags.
     */
    if (u1Staticflag == IGMP_FALSE)
    {
        i1LimitSt = IgmpCheckForLimit (pIfNode, u4Group);
    }
    if (i1LimitSt == IGMP_IGNORE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Limit Reached \n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IgmpGetModuleName (IGMP_OS_RES_MODULE),
                       "Limit Reached \n");
        return NULL;
    }

    IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpGroupId, pGrp, tIgmpGroup);
    if (pGrp == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Failure in allocate Memory block for Mcast group node \n");
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                       IgmpGetModuleName (IGMP_INIT_SHUT_MODULE),
                       "Failure in allocate Memory block for Mcast group node \n");

        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                         IGMP_OS_RES_MODULE, IGMP_NAME,
                         IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
                         "RBTree Addition for Group Entry for Interface Index :%d\r\n",
                         pIfNode->u4IfIndex);
        return NULL;
    }

    if (u1Staticflag == IGMP_FALSE)
    {
        if (IGMP_GROUP_MATCHED == i1LimitSt)
        {
            if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
            {
                gIgmpConfig.u4GlobalCurCnt++;
                pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
            }
        }
        else if (IGMP_INF_LIMIT_NOT_REACHED == i1LimitSt)
        {
            pIfNode->u4GrpCurCnt++;
            pGrp->u1LimitFlag |= IGMP_INTERFACE_LIMIT_COUNTED;
            if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
            {
                gIgmpConfig.u4GlobalCurCnt++;
                pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
            }
        }
        else if (IGMP_INF_LIMIT_NOT_CONFIGURED == i1LimitSt)
        {
            if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
            {
                gIgmpConfig.u4GlobalCurCnt++;
                pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
            }
        }
    }
    IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4Group));
    IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
    IGMP_IPVX_ADDR_COPY (&(pGrp->u4Group), &u4Group);
    pGrp->pIfNode = pIfNode;
    pGrp->u4IfIndex = pIfNode->u4IfIndex;
    pGrp->i1CacheSelfFlag = IGMP_FALSE;
    OsixGetSysTime (&u4CacheUpTime);
    pGrp->u4CacheUpTime = u4CacheUpTime;
    pGrp->u1EntryStatus = pIfNode->u1EntryStatus;
    pGrp->u1V1HostPresent = IGMP_FALSE;
    pGrp->u1V2HostPresent = IGMP_FALSE;
    pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
    pGrp->u4GroupCompMode = IGMP_GCM_IGMPV3;
    pGrp->u1StaticFlg = u1Staticflag;
    pGrp->u1DynamicFlg = IGMP_FALSE;
    pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
    pGrp->u1DelSyncFlag = IGMP_TRUE;
    pGrp->u1DynSyncFlag = IGMP_TRUE;
    /* Initialise the source linked list n the interface */
    TMO_SLL_Init (&(pGrp->srcList));
    TMO_SLL_Init (&(pGrp->SrcListForMrpUpdate));
    /* Initialise the Group DB Node */
    IgmpRedDbNodeInit (&(pGrp->GrpDbNode), IGMP_RED_GRP_INFO);
    /*Create Reporter RBTree */
    pGrp->GrpReporterTree = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tIgmpSrcReporter, RbNode),
         IgmpRBTreeGrpSrcReporterEntryCmp);
    if (pGrp->GrpReporterTree == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                   "RBTree Creation failed for Group Record %s\r\n",
                   IgmpPrintIPvxAddress (pGrp->u4Group));
        IgmpMemRelease (gIgmpMemPool.IgmpGroupId, (UINT1 *) pGrp);
        return NULL;
    }

    /* Add the group entry node to the global group RBTree */
    u4Status = RBTreeAdd (gIgmpGroup, (tRBElem *) pGrp);

    if (u4Status == RB_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "RBTree Addition Failed for Group Entry \r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                  "RBTree Addition Failed for Group Entry \r\n");
        SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,
                         IGMP_OS_RES_MODULE, IGMP_NAME,
                         IgmpSysErrString[SYS_LOG_RB_TREE_ADD_FAIL],
                         "RBTree Addition for Group Entry for Interface Index :%d\r\n",
                         pIfNode->u4IfIndex);
        IgmpMemRelease (gIgmpMemPool.IgmpGroupId, (UINT1 *) pGrp);
        return NULL;
    }

    /* Added support for IGMP standard MIB RFC:2933
     * For Mib objects : IgmpInterfaceGroups & IgmpCacheUpTime
     * Updating Number of groups for this interface,
     * cache up time and last reporter.
     */
    pIfNode->u4InterfaceGroups++;
    pIfNode->u4ProcessedJoins++;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpAddGroup()\r \n");
    return (pGrp);
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IGMP_Process_V2_Report                              */
/*                                                                         */
/*     Description   : Invokes the Common routine which processes the      */
/*                     reports and passes a flag denoting the version 2.   */
/*                     Prior to this it checks whether v2 is configured    */
/*                     in this interface.                                  */
/*                                                                         */
/*     Input(s)      : InterfaceId, Multicast Group Address                */
/*                                                                         */
/*     Output(s)     : None                                                */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpProcessV2Report                         **/
/** $$TRACE_PROCEDURE_LEVEL = INTMD                                       **/
/***************************************************************************/
VOID
IgmpProcessV2Report (tIgmpIface * pIfNode, tIgmpIPvXAddr u4Group,
                     tIgmpIPvXAddr u4HostAddr)
{
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpProcessV2Report()\r \n");
    /* Process the version two report only if IGMP version 2  or Version 3 
     * is enabled on  that interface.  
     */
    IgmpProcessGroup (pIfNode, u4Group, IGMP_VERSION_2, u4HostAddr);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpProcessV2Report()\r \n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpProcessLeave                                    */
/*                                                                         */
/*     Description   : Process the received leave message.                 */
/*                                                                         */
/*     Input(s)      : Interface Node, Multicast Group Address , SrcAddress*/
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpProcessLeave                            **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpProcessLeave (tIgmpIface * pIfNode, tIgmpIPvXAddr u4Group)
{
    tIgmpQuery         *pIgmp = NULL;
    tIgmpGroup         *pGrp = NULL;
    UINT1               u1VerFlag = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpProcessLeave()\r \n");
    /*
     * Process the leave report only if IGMP version 2/3 is enabled on
     * that interface.
     */
    pIgmp = pIfNode->pIfaceQry;
    if (pIgmp->u1Querier == FALSE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Non-Querier can't Process Leave Msg - Ignoring the Msg. \n");
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                  "Non-Querier can't Process Leave Msg - Ignoring the Msg. \n");

        return;
    }

    pGrp = IgmpGrpLookUp (pIfNode, u4Group);
    if (pGrp == NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                   " Group %s is not present. Ignoring the Msg\n",
                   IgmpPrintIPvxAddress (u4Group));
        return;
    }

    if (pGrp->u1V1HostPresent == IGMP_TRUE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       " Ver 1 Hosts is present. Ignoring the Leave Msg. \n");
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                  " Ver 1 Hosts is present. Ignoring the Leave Msg. \n");
        return;
    }
    /* If leave is received and a static group exits for the same record
       update the flag and stop the timer. DO not delete the group record */

    if ((pGrp->u1StaticFlg == IGMP_TRUE) && (pGrp->u1DynamicFlg == IGMP_TRUE))
    {
        pGrp->u1DynamicFlg = IGMP_FALSE;
        IGMPSTOPTIMER (pGrp->Timer);
        IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
        pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
        IgmpRedSyncDynInfo ();
        return;
    }
    if (pGrp->u1StaticFlg == IGMP_TRUE)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       " Static Entry for group is created Ignoring the Leave Msg\n");
        IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                  IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                  " Static Entry for group is created Ignoring the Leave Msg\n");
        return;
    }

    if ((pIfNode->u1Version == IGMP_VERSION_2) ||
        (pIfNode->u1Version == MLD_VERSION_1))
    {
        /*
         * If we are the Querier, we should send a Group Specific Query, 
         * because this could be last host in the interface for this Group.
         */
        if (pIgmp->u1Querier == TRUE)
        {
            if (IGMP_IS_FAST_LEAVE_ENABLED (pIfNode))
            {
                pGrp->u1DelSyncFlag = IGMP_TRUE;
                u1VerFlag = IGMP_VERSION_2 | IGMP_V3REP_SEND_FLAG;
                IgmpExpireGroup (pIfNode, pGrp, u1VerFlag, IGMP_TRUE,
                                 IGMP_FALSE);
            }
            else
            {
                IgmpReStartGrpTmrToLmqTmr (pIfNode, pGrp);
                IgmpSendGroupSpecificQuery (pIfNode, pGrp);
            }
        }
    }
    if ((pIfNode->u1Version == IGMP_VERSION_3) ||
        (pIfNode->u1Version == MLD_VERSION_2))
    {
        if (pGrp->u4GroupCompMode == IGMP_GCM_IGMPV2)
        {
            /* Convert the V2 Leave to TO_IN ({}) */
            IgmpGroupHandleToInclude (pIfNode, pGrp->u4Group, IGMP_ZERO,
                                      NULL, gIPvXZero);
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpProcessLeave()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpV3ProcessGroupRecord                            */
/*                                                                         */
/*     Description   : Process the received IGMPv3 Group Record            */
/*                                                                         */
/*     Input(s)      : pIfNode     - Interface Node                        */
/*                     GrpAddress  - Multicast Group Address               */
/*                     u1GrpRecType - Record Type as in RFC                */
/*                     pu1Sources   - Pointer to the Linear Src List       */
/*                     u2NoOfSources  - No of sources                      */
/*                     SenderAddr - Sender Address                         */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
VOID
IgmpV3ProcessGroupRecord (tIgmpIface * pIfNode, tIPvXAddr GrpAddress,
                          UINT1 u1GrpRecType, UINT1 *pu1Sources,
                          UINT2 u2NoOfSources, tIPvXAddr SenderAddr)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    INT4                i4MapStatus = IGMP_FALSE;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpV3ProcessGroupRecord()\r \n");
    switch (u1GrpRecType)
    {
        case IGMP_GRP_MODE_IS_INCLUDE:
            if (u2NoOfSources == IGMP_ZERO)
            {
                IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                           "Number of Sources is Zero, Group %s Ignored \n",
                           IgmpPrintIPvxAddress (GrpAddress));
                IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                               IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                               "Number of Sources is Zero, Group %s Ignored \n",
                               IgmpPrintIPvxAddress (GrpAddress));
                break;
            }
            IgmpGroupHandleIsInclude (pIfNode, GrpAddress,
                                      (UINT4) u2NoOfSources,
                                      pu1Sources, SenderAddr);
            break;

        case IGMP_GRP_MODE_IS_EXCLUDE:
            pSSMMappedGrp =
                IgmpUtilChkIfSSMMappedGroup (GrpAddress, &i4MapStatus);
            if ((u2NoOfSources == IGMP_ZERO) && (IGMP_TRUE == i4MapStatus))
            {
                /* Converting (* G) join to (S G) join with the source address
                 * configured through SSM mapping */
                IgmpChngStarGToSGJoinForSSMMappedGrp (pIfNode, pSSMMappedGrp,
                                                      GrpAddress, SenderAddr,
                                                      IGMP_JOIN);
            }
            else
            {
                IgmpGroupHandleIsExclude (pIfNode, GrpAddress,
                                          (UINT4) u2NoOfSources,
                                          pu1Sources, SenderAddr, IGMP_TRUE);
            }
            break;

        case IGMP_GRP_CHANGE_TO_INCLUDE_MODE:
            pSSMMappedGrp =
                IgmpUtilChkIfSSMMappedGroup (GrpAddress, &i4MapStatus);
            if ((u2NoOfSources == IGMP_ZERO) && (IGMP_TRUE == i4MapStatus))
            {
                /* Converting (* G) leave to (S G) leave with the source address
                 * configured through SSM mapping */
                IgmpChngStarGToSGJoinForSSMMappedGrp (pIfNode, pSSMMappedGrp,
                                                      GrpAddress, SenderAddr,
                                                      IGMP_LEAVE);
            }
            else
            {
                IgmpGroupHandleToInclude (pIfNode, GrpAddress,
                                          (UINT4) u2NoOfSources,
                                          pu1Sources, SenderAddr);
            }
            break;

        case IGMP_GRP_CHANGE_TO_EXCLUDE_MODE:
            pSSMMappedGrp =
                IgmpUtilChkIfSSMMappedGroup (GrpAddress, &i4MapStatus);
            if ((u2NoOfSources == IGMP_ZERO) && (IGMP_TRUE == i4MapStatus))
            {
                /* Converting (* G) join to (S G) join with the source address
                 * configured through SSM mapping */
                IgmpChngStarGToSGJoinForSSMMappedGrp (pIfNode, pSSMMappedGrp,
                                                      GrpAddress, SenderAddr,
                                                      IGMP_JOIN);
            }
            else
            {
                IgmpGroupHandleToExclude (pIfNode, GrpAddress,
                                          (UINT4) u2NoOfSources,
                                          pu1Sources, SenderAddr);
            }
            break;

        case IGMP_GRP_ALLOW_NEW_SOURCES:
            if (u2NoOfSources == IGMP_ZERO)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG,
                               IGMP_GRP_MODULE, IGMP_NAME,
                               "Number of Sources is Zero\n");
                IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "Number of Sources is Zero\n");
                break;
            }
            IgmpGroupHandleAllowNewSources (pIfNode, GrpAddress,
                                            (UINT4) u2NoOfSources,
                                            pu1Sources, SenderAddr, IGMP_TRUE);
            break;

        case IGMP_GRP_BLOCK_OLD_SOURCES:
            if (u2NoOfSources == IGMP_ZERO)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG,
                               IGMP_GRP_MODULE, IGMP_NAME,
                               "Number of Sources is Zero\n");
                IGMP_TRC (MGMD_TRC_FLAG,
                          IGMP_CNTRL_MODULE,
                          IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                          "Number of Sources is Zero\n");
                break;
            }
            IgmpGroupHandleBlockOldSources (pIfNode, GrpAddress,
                                            (UINT4) u2NoOfSources,
                                            pu1Sources, SenderAddr, IGMP_TRUE);
            break;

        default:
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "Invalid Group Record Type in Report");

    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpV3ProcessGroupRecord()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpProcessV3Report                                 */
/*                                                                         */
/*     Description   : Process the received V3 Report message.             */
/*                                                                         */
/*     Input(s)      : Interface Index, Message and The source address     */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpProcessV3Report                         **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpProcessV3Report (tIgmpIface * pIfNode, UINT1 *pu1BufPtr, UINT4 u4BufSize,
                     tIgmpIPvXAddr u4Source)
{
    tIgmpV3Report      *pReport = NULL;
    tIgmpGroupRec      *pGroupRec = NULL;
    tIgmpFilter         IgmpFilterPkt;
    tIgmpIPvXAddr       GrpIpvxAddr;
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    UINT4              *pu4Sources = NULL;
    UINT2               u2NoOfGrps = IGMP_ZERO;
    UINT2               u2Offset = IGMP_ZERO;
    UINT2               u2NoOfSources = IGMP_ZERO;
    UINT4               u4RecNum = IGMP_ZERO;
    UINT4               u4SrcAddress = IGMP_ZERO;
    UINT4               u4GrpAddress = IGMP_ZERO;
    INT4                i4Status = IGMP_ZERO;
    INT4                i4MapStatus = IGMP_FALSE;
    UINT1               u1GrpRecType = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpProcessV3Report()\r \n");
    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE)
        && (IgpUtilCheckUpIface (pIfNode->u4IfIndex) == IGMP_TRUE))
    {
        /* IGMP Proxy is enabled and the interface is configures 
         * as an upstream interface. No reports should be processed 
         * on upstream interface. so return. */
        return;
    }

    MEMSET (&IgmpFilterPkt, 0, sizeof (tIgmpFilter));

    pReport = (tIgmpV3Report *) (VOID *) pu1BufPtr;
    if (u4BufSize < sizeof (tIgmpV3Report))
    {
        pIfNode->u4InvalidV3Report++;
        return;
    }
    u4BufSize -= sizeof (tIgmpV3Report);

    u2NoOfGrps = OSIX_NTOHS (pReport->u2NumGrps);
    u2Offset = IGMP_HEADER_LEN;

    /* Processing all Group Records Present in the V3 Report */
    for (u4RecNum = IGMP_ZERO; u4RecNum < u2NoOfGrps; u4RecNum++)
    {
        pGroupRec = (tIgmpGroupRec *) (VOID *) (pu1BufPtr + u2Offset);
        if (u4BufSize < sizeof (tIgmpGroupRec))
        {
            break;
        }
        u2Offset += sizeof (tIgmpGroupRec);
        u4BufSize -= sizeof (tIgmpGroupRec);

        IGMP_IP_COPY_FROM_IPVX (&u4SrcAddress, u4Source,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        u4GrpAddress = OSIX_NTOHL (pGroupRec->u4GroupAddress);
        u2NoOfSources = OSIX_NTOHS (pGroupRec->u2NumSrcs);
        u1GrpRecType = pGroupRec->u1RecordType;

        /* Check for Group Address */
        if ((u4GrpAddress < IGMP_START_OF_MCAST_GRP_ADDR) ||
            (u4GrpAddress > IGMP_END_OF_MCAST_GRP_ADDR) ||
            (u4GrpAddress == IGMP_INVALID_SSM_GRP_ADDR))
        {
            /* Invalid Group Destination Addres */
            IGMP_IPVX_ADDR_CLEAR (&GrpIpvxAddr);
            IGMP_IPVX_ADDR_INIT_IPV4 (GrpIpvxAddr, IPVX_ADDR_FMLY_IPV4,
                                      (UINT1 *) &u4GrpAddress);
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "IGMP Invalid SSM Group %s \n",
                       IgmpPrintIPvxAddress (GrpIpvxAddr));
            IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "IGMP Invalid SSM Group %s \n",
                           IgmpPrintIPvxAddress (GrpIpvxAddr));
            pIfNode->u4InvalidV3Report++;
            pIfNode->u4IgmpInvalidSSMPkts++;
            u2Offset += (u2NoOfSources * sizeof (UINT4));
            u4BufSize -= (sizeof (UINT4) * u2NoOfSources);
            continue;
        }

        if ((u2NoOfSources != IGMP_ZERO) &&
            (u4BufSize < (sizeof (UINT4) * u2NoOfSources)))
        {
            return;
        }

        IGMP_CHK_IF_SSM_RANGE (u4GrpAddress, i4Status)
            IGMP_IPVX_ADDR_CLEAR (&GrpIpvxAddr);
        IGMP_IPVX_ADDR_INIT_IPV4 (GrpIpvxAddr, IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4GrpAddress);
        pSSMMappedGrp = IgmpUtilChkIfSSMMappedGroup (GrpIpvxAddr, &i4MapStatus);

        if ((i4Status == IGMP_SSM_RANGE) ||
            ((i4MapStatus == IGMP_TRUE) && (pSSMMappedGrp != NULL)))
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Group %s in SSM Range \n",
                       IgmpPrintIPvxAddress (GrpIpvxAddr));
            IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Group %s in SSM Range \n",
                           IgmpPrintIPvxAddress (GrpIpvxAddr));
            pIfNode->u4IgmpInSSMPkts++;
            /* Ignore a group record of either of the following mode type in SSM range dest address */
            if ((i4MapStatus == IGMP_FALSE) && (u2NoOfSources == IGMP_ZERO))
            {
                if ((u1GrpRecType == IGMP_GRP_CHANGE_TO_EXCLUDE_MODE) ||
                    (u1GrpRecType == IGMP_GRP_MODE_IS_EXCLUDE))
                {
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                                  "Ignored SSM range Exclude Null Join, "
                                  "Group %s not in SSM Mapped Range \n",
                                  IgmpPrintIPvxAddress (GrpIpvxAddr)));
                    IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                               "Ignored SSM range Exclude Null Join, Group %s not in SSM Mapped Range \n",
                               IgmpPrintIPvxAddress (GrpIpvxAddr));
                }
                else if (u1GrpRecType == IGMP_GRP_CHANGE_TO_INCLUDE_MODE)
                {
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                                  "Ignored SSM range (* G) Leave, Group %s not in SSM Mapped Range \n",
                                  IgmpPrintIPvxAddress (GrpIpvxAddr)));
                    IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                               "Ignored SSM range (* G) Leave, Group %s not in SSM Mapped Range \n",
                               IgmpPrintIPvxAddress (GrpIpvxAddr));
                }
                pIfNode->u4IgmpInvalidSSMPkts++;
                pIfNode->u4InvalidV3Report++;
                u2Offset += (UINT2) (u2NoOfSources * sizeof (UINT4));
                u4BufSize -= (sizeof (UINT4) * u2NoOfSources);
                continue;
            }
        }
#ifdef MULTICAST_SSM_ONLY
        else if ((i4Status != IGMP_SSM_RANGE) && (i4MapStatus == IGMP_FALSE) &&
                 (u2NoOfSources == IGMP_ZERO))
        {
            if ((u1GrpRecType == IGMP_GRP_CHANGE_TO_EXCLUDE_MODE) ||
                (u1GrpRecType == IGMP_GRP_MODE_IS_EXCLUDE))
            {
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                              "Ignored non SSM range Exclude Null Join, "
                              "Group %s not in SSM Mapped Range \n",
                              IgmpPrintIPvxAddress (GrpIpvxAddr)));
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                           "Ignored non SSM range Exclude Null Join, Group %s not in SSM Mapped Range \n",
                           IgmpPrintIPvxAddress (GrpIpvxAddr));
            }
            else if (u1GrpRecType == IGMP_GRP_CHANGE_TO_INCLUDE_MODE)
            {
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                              "Ignored non SSM range (* G) Leave, Group %s not in SSM Mapped Range \n",
                              IgmpPrintIPvxAddress (GrpIpvxAddr)));
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                           "Ignored non SSM range (* G) Leave, Group %s not in SSM Mapped Range \n",
                           IgmpPrintIPvxAddress (GrpIpvxAddr));
            }
            pIfNode->u4IgmpInvalidSSMPkts++;
            pIfNode->u4InvalidV3Report++;
            u2Offset += (u2NoOfSources * sizeof (UINT4));
            u4BufSize -= (sizeof (UINT4) * u2NoOfSources);
            continue;
        }
#endif
        IGMP_IPVX_ADDR_INIT_IPV4 (IgmpFilterPkt.u4SrcIpAddr,
                                  IGMP_IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4SrcAddress);
        IGMP_IPVX_ADDR_INIT_IPV4 (IgmpFilterPkt.u4GrpAddress,
                                  IGMP_IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4GrpAddress);
        IgmpFilterPkt.u4InIfIndex = pIfNode->u4IfIndex;
        IgmpFilterPkt.u1Version = IGMP_VERSION_2;

        if ((u1GrpRecType == IGMP_GRP_CHANGE_TO_INCLUDE_MODE) &&
            (u2NoOfSources == 0))
        {
            IgmpFilterPkt.u1PktType = IGMP_PACKET_LEAVE;
        }
        else
        {
            IgmpFilterPkt.u1PktType = IGMP_PACKET_V2_REPORT;
        }

        if (IgmpFilterPackets (&IgmpFilterPkt) == IGMP_DENY)
        {
            IGMP_IPVX_ADDR_CLEAR (&GrpIpvxAddr);
            IGMP_IPVX_ADDR_INIT_IPV4 (GrpIpvxAddr, IPVX_ADDR_FMLY_IPV4,
                                      (UINT1 *) &u4GrpAddress);
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "IGMP V3 report filtered for Group %s\n",
                       IgmpPrintIPvxAddress (GrpIpvxAddr));
            IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "IGMP V3 report filtered for Group %s\n",
                           IgmpPrintIPvxAddress (GrpIpvxAddr));
            continue;

        }

        u4BufSize -= (sizeof (UINT4) * u2NoOfSources);
        pu4Sources = (UINT4 *) (VOID *) (pu1BufPtr + u2Offset);
        u2Offset += (UINT2) (sizeof (UINT4) * u2NoOfSources);

        IgmpV3ProcessGroupRecord (pIfNode, IgmpFilterPkt.u4GrpAddress,
                                  u1GrpRecType, (UINT1 *) pu4Sources,
                                  u2NoOfSources, IgmpFilterPkt.u4SrcIpAddr);
    }

    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                           "IGMP PROXY: Sending upstream report failed \r\n");
            IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
                      IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                      "IGMP PROXY: Sending upstream report failed \r\n");
        }
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpProcessV3Report()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpGrpLookUp                                       */
/*                                                                         */
/*     Description   : Process the received V3 Report message.             */
/*                                                                         */
/*     Input(s)      : Interface Index, Message and The source address     */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpGrpLookUp                               **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
tIgmpGroup         *
IgmpGrpLookUp (tIgmpIface * pIfaceNode, tIgmpIPvXAddr u4GrpAddr)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpGrpLookUp()\r \n");
    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

    IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
    IgmpGrpEntry.pIfNode = pIfaceNode;
    IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &u4GrpAddr);

    pGrp = (tIgmpGroup *) RBTreeGet (gIgmpGroup, (tRBElem *) & IgmpGrpEntry);
    if (pGrp == NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_GRP_MODULE,
                   IGMP_NAME,
                   " Group %s is not present. Ignoring the Msg\n",
                   IgmpPrintIPvxAddress (u4GrpAddr));

        return NULL;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpGrpLookUp()\r \n");
    return pGrp;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpDeleteGroup                                     */
/*                                                                         */
/*     Description   : Deletes the received group entry from RBTree        */
/*                                                                         */
/*     Input(s)      : Group entry                                         */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       : None                                                */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpDeleteGroup                             **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
VOID
IgmpDeleteGroup (tIgmpIface * pIfNode, tIgmpGroup * pGrp, UINT1 u1Staticflag)
{
    tIgmpSource        *pSrcNode = NULL;
    tRBElem            *pRBElem = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpDeleteGroup()\r \n");
    if ((pGrp->Timer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pGrp->Timer);
    }

    if ((pGrp->Version1HostTimer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pGrp->Version1HostTimer);
    }
    if ((pGrp->Version2HostTimer).u1TmrStatus == IGMP_TMR_SET)
    {
        IGMPSTOPTIMER (pGrp->Version2HostTimer);
    }

    while ((pSrcNode = (tIgmpSource *)
            TMO_SLL_First (&(pGrp->srcList))) != NULL)
    {
        IgmpDeleteSource (pGrp, pSrcNode);
    }
    if (u1Staticflag == IGMP_FALSE)
    {
        if ((pGrp->u1LimitFlag & IGMP_GLOBAL_LIMIT_COUNTED))
        {
            if (gIgmpConfig.u4GlobalCurCnt > IGMP_ZERO)
            {
                gIgmpConfig.u4GlobalCurCnt--;
            }
        }
        if ((pGrp->u1LimitFlag & IGMP_INTERFACE_LIMIT_COUNTED))
        {
            if (pIfNode->u4GrpCurCnt > IGMP_ZERO)
            {
                pIfNode->u4GrpCurCnt--;
            }
        }
    }

    if (pGrp->u1DelSyncFlag == IGMP_TRUE)
    {
        pGrp->u1GrpModeFlag = IGMP_DELETE_GROUP;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
        IgmpRedSyncDynInfo ();
        pGrp->u1DelSyncFlag = IGMP_FALSE;
    }
    IgmpDeleteAllV1V2GrpReporters (pGrp);
    if (pGrp->GrpReporterTree != NULL)
    {
        RBTreeDelete (pGrp->GrpReporterTree);
        pGrp->GrpReporterTree = NULL;
    }
    pRBElem = RBTreeGet (gIgmpGroup, (tRBElem *) pGrp);
    RBTreeRemove (gIgmpGroup, pRBElem);
    IgmpMemRelease (gIgmpMemPool.IgmpGroupId, (UINT1 *) pGrp);
    if (pIfNode->u4InterfaceGroups != IGMP_ZERO)
    {
        pIfNode->u4InterfaceGroups--;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpDeleteGroup()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpRBTreeGroupEntryCmp                             */
/*                                                                         */
/*     Description   : This routine is used in Group Table for comparing   */
/*                     two keys used in RBTree functionality.              */
/*                                                                         */
/*     Input(s)      : GroupEntryNode - Key 1                              */
/*                     GroupEntryIn   - Key 2                              */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :  -1/1 -> when any key element is Less/Greater       */
/*                      0    -> when all key elements are Equal            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpRBTreeGroupEntryCmp                     **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/

INT4
IgmpRBTreeGroupEntryCmp (tRBElem * pGroupEntryNode, tRBElem * pGroupEntryIn)
{
    tIgmpGroup         *pIgmpGroup = NULL;
    tIgmpGroup         *pIgmpGroupIn = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpRBTreeGroupEntryCmp()\r \n");
    pIgmpGroup = (tIgmpGroup *) pGroupEntryNode;
    pIgmpGroupIn = (tIgmpGroup *) pGroupEntryIn;

    /* Compare the interface index for group */
    if (pIgmpGroup->u4IfIndex < pIgmpGroupIn->u4IfIndex)
    {
        return -1;
    }
    else if (pIgmpGroup->u4IfIndex > pIgmpGroupIn->u4IfIndex)
    {
        return 1;
    }

    /* Compare the interface address type for group */
    if (pIgmpGroup->pIfNode->u1AddrType < pIgmpGroupIn->pIfNode->u1AddrType)
    {
        return -1;
    }
    else if (pIgmpGroup->pIfNode->u1AddrType >
             pIgmpGroupIn->pIfNode->u1AddrType)
    {
        return 1;
    }

    /* Compare the Multicast group address */
    if (MEMCMP (pIgmpGroup->u4Group.au1Addr, pIgmpGroupIn->u4Group.au1Addr,
                pIgmpGroupIn->u4Group.u1AddrLen) < IGMP_ZERO)
    {
        return -1;
    }
    else if (MEMCMP (pIgmpGroup->u4Group.au1Addr, pIgmpGroupIn->u4Group.au1Addr,
                     pIgmpGroupIn->u4Group.u1AddrLen) > IGMP_ZERO)
    {
        return 1;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpRBTreeGroupEntryCmp()\r \n");
    return 0;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpRBTreeGroupListCmp                             */
/*                                                                         */
/*     Description   : This routine is used in GroupList Table for comparing */
/*                     two keys used in RBTree functionality.              */
/*                                                                         */
/*     Input(s)      : GroupEntryNode - Key 1                              */
/*                     GroupEntryIn   - Key 2                              */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :  -1/1 -> when any key element is Less/Greater       */
/*                      0    -> when all key elements are Equal            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpRBTreeGroupListCmp                     **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/

INT4
IgmpRBTreeGroupListCmp (tRBElem * pGroupEntryNode, tRBElem * pGroupEntryIn)
{
    tIgmpGrpList       *pIgmpGroup1 = NULL;
    tIgmpGrpList       *pIgmpGroup2 = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpRBTreeGroupListCmp()\r \n");
    pIgmpGroup1 = (tIgmpGrpList *) pGroupEntryNode;
    pIgmpGroup2 = (tIgmpGrpList *) pGroupEntryIn;
    /* Compare the GroupId of list */
    if (pIgmpGroup1->u4GroupId < pIgmpGroup2->u4GroupId)
    {
        return -1;
    }
    else if (pIgmpGroup1->u4GroupId > pIgmpGroup2->u4GroupId)
    {
        return 1;
    }

    /* Compare the GroupIP in the list */
    if (pIgmpGroup1->u4GroupIP < pIgmpGroup2->u4GroupIP)
    {
        return -1;
    }
    else if (pIgmpGroup1->u4GroupIP > pIgmpGroup2->u4GroupIP)
    {
        return 1;
    }

    /* Compare the mask of the group */
    if (pIgmpGroup1->u4PrefixLen < pIgmpGroup2->u4PrefixLen)
    {
        return -1;
    }
    else if (pIgmpGroup1->u4PrefixLen > pIgmpGroup2->u4PrefixLen)
    {
        return 1;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpRBTreeGroupListCmp()\r \n");
    return 0;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpResetAllInterfaceLimitCounters                  */
/*                                                                         */
/*     Description   : This routine is used to clear all interface limit 
 *                     counter and resets Limit Flag for the all groups    */
/*                                                                         */
/*     Input(s)      :  None                                               */
/*                                                                         */
/*     Output(s)     :  None                                              */
/*                                                                         */
/*     Returns       :  None                                               */
/***************************************************************************/
VOID
IgmpResetAllInterfaceLimitCounters (VOID)
{
    tIgmpGroup         *pGrp = NULL;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpResetAllInterfaceLimitCounters()\r \n");
    pGrp = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
    while (pGrp != NULL)
    {
        if ((pGrp->u1StaticFlg == IGMP_FALSE) &&
            (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            if ((gIgmpConfig.u4GlobalCurCnt != IGMP_ZERO))
            {
                gIgmpConfig.u4GlobalCurCnt--;
            }
            pGrp->u1LimitFlag = 0;
            pGrp->pIfNode->u4GrpCurCnt = IGMP_ZERO;
        }
        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) pGrp, NULL);
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpResetAllInterfaceLimitCounters()\r \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpResetLimitFlag                                  */
/*                                                                         */
/*     Description   : This routine is used to clear limit counter and 
 *                     resets Limit Flag for the groups on giver interface */
/*                                                                         */
/*     Input(s)      :  pIfaceNode - IgmpInterface Node                    */
/*                                                                         */
/*     Output(s)     :  None                                              */
/*                                                                         */
/*     Returns       :  None                                               */
/***************************************************************************/
VOID
IgmpResetLimitFlag (tIgmpIface * pIfaceNode)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;

    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));
    pIfaceNode->u4GrpCurCnt = IGMP_ZERO;
    IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
    IgmpGrpEntry.pIfNode = pIfaceNode;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpResetLimitFlag()\r \n");
    pGrp =
        (tIgmpGroup *) RBTreeGetNext (gIgmpGroup, (tRBElem *) & IgmpGrpEntry,
                                      NULL);
    while (pGrp != NULL)
    {
        if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
        {
            /* Group entry doesn't belong to 
             * specified interface index */
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Group entry doesn't belong to specified interface index %d\n",
                       pIfaceNode->u4IfIndex);
            break;
        }
        if ((pGrp->u1StaticFlg == IGMP_FALSE) &&
            (pGrp->pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4))
        {
            if ((pGrp->u1LimitFlag & IGMP_GLOBAL_LIMIT_COUNTED) &&
                (gIgmpConfig.u4GlobalCurCnt != IGMP_ZERO))
            {
                gIgmpConfig.u4GlobalCurCnt--;
            }
            pGrp->u1LimitFlag = 0;
        }
        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) pGrp, NULL);
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpResetLimitFlag()\r \n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpRBTreeMcastGroupEntryCmp                        */
/*                                                                         */
/*     Description   : This routine is used in gIgmpHostMcastGrpTable for  */
/*                     comparing two keys used in RBTree functionality.    */
/*                                                                         */
/*     Input(s)      : pMcastGroupEntryNode    - Key 1                     */
/*                     pMcastGroupEntryNodeIn  - Key 2                     */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :  -1/1 -> when any key element is Less/Greater       */
/*                      0    -> when all key elements are Equal            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpRBTreeMcastGroupEntryCmp                **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/

INT4
IgmpRBTreeMcastGroupEntryCmp (tRBElem * pMcastGroupEntryNode,
                              tRBElem * pMcastGroupEntryNodeIn)
{
    tIgmpHostMcastGrpEntry *pIgmpHostMcastGrpEntry = NULL;
    tIgmpHostMcastGrpEntry *pIgmpHostMcastGrpEntryIn = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpRBTreeMcastGroupEntryCmp()\r \n");

    pIgmpHostMcastGrpEntry = (tIgmpHostMcastGrpEntry *) pMcastGroupEntryNode;
    pIgmpHostMcastGrpEntryIn =
        (tIgmpHostMcastGrpEntry *) pMcastGroupEntryNodeIn;

    if (pIgmpHostMcastGrpEntry->u4IfIndex > pIgmpHostMcastGrpEntryIn->u4IfIndex)
    {
        return 1;
    }
    else if (pIgmpHostMcastGrpEntry->u4IfIndex <
             pIgmpHostMcastGrpEntryIn->u4IfIndex)
    {
        return -1;
    }
    if (MEMCMP
        (&(pIgmpHostMcastGrpEntry->McastAddr),
         &(pIgmpHostMcastGrpEntryIn->McastAddr), sizeof (tIPvXAddr)) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (&(pIgmpHostMcastGrpEntry->McastAddr),
              &(pIgmpHostMcastGrpEntryIn->McastAddr), sizeof (tIPvXAddr)) < 0)
    {
        return -1;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpRBTreeMcastGroupEntryCmp()\r \n");
    return 0;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpRBTreeGrpSrcReporterEntryCmp                    */
/*                                                                         */
/*     Description   : This routine is used in SrcReporterTree/               */
/*                        GrpReporterTree for comparing two keys used in      */
/*                        RBTree functionality.                               */
/*                                                                         */
/*     Input(s)      : pReporterNodeIn    - Key 1                          */
/*                     pReporterNodeRB    - Key 2                          */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :  -1/1 -> when any key element is Less/Greater       */
/*                      0    -> when all key elements are Equal            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpRBTreeGrpSrcReporterEntryCmp            **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/
INT4
IgmpRBTreeGrpSrcReporterEntryCmp (tRBElem * pReporterNodeIn,
                                  tRBElem * pReporterNodeRB)
{
    tIgmpSrcReporter   *pReporterIn;
                                   /*--IN--put node that is to be compared with the RB tree Node*/
    tIgmpSrcReporter   *pReporterRB;    /*node that is already present in the --RB--tree */
    UINT4               u4RepAddrRB = 0;
    UINT4               u4RepAddrIn = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering  IgmpRBTreeGrpSrcReporterEntryCmp()\r \n");

    pReporterIn = (tIgmpSrcReporter *) pReporterNodeIn;
    pReporterRB = (tIgmpSrcReporter *) pReporterNodeRB;

    /* Compare the reporter address for the group */
    IGMP_IP_COPY_FROM_IPVX (&u4RepAddrRB, pReporterIn->u4SrcRepAddr,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4RepAddrIn, pReporterRB->u4SrcRepAddr,
                            IGMP_IPVX_ADDR_FMLY_IPV4);

    if (u4RepAddrRB < u4RepAddrIn)
    {
        return -1;
    }
    else if (u4RepAddrRB > u4RepAddrIn)
    {
        return 1;
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting  IgmpRBTreeGrpSrcReporterEntryCmp()\r \n");
    return 0;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpAddV1V2Reporter                                 */
/*                                                                         */
/*     Description   : This routine is used to add V1/V2 reporter for a    */
/*                     multicast group.                                       */
/*                                                                            */
/*     Input(s)      :  pGrp - Multicast Group for which a new reporter is */
/*                         to be added                                        */
/*                      u4Reporter - IP address of the reporter            */
/*                                                                         */
/*     Output(s)     :  IGMP_NOT_OK(failure), IGMP_OK(success)             */
/*                                                                         */
/*     Returns       :  None                                               */
/***************************************************************************/
INT4
IgmpAddV1V2Reporter (tIgmpGroup * pGrp, tIgmpIPvXAddr u4Reporter)
{
    tIgmpSrcReporter   *pNewReporter = NULL;
    tIgmpSrcReporter   *pOldReporter = NULL;
    tIgmpSrcReporter    FindReporter;
    UINT4               u4Status = RB_FAILURE;

    if (pGrp->GrpReporterTree == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                   "Group Reporter addition failed as GrpReporterTree is NULL for group %s \r\n",
                   IgmpPrintIPvxAddress (pGrp->u4Group));
        return IGMP_NOT_OK;
    }

    MEMSET (&FindReporter, 0, sizeof (tIgmpSrcReporter));
    IGMP_IPVX_ADDR_COPY (&(FindReporter.u4SrcRepAddr), &u4Reporter);
    pOldReporter = (tIgmpSrcReporter *) RBTreeGet (pGrp->GrpReporterTree,
                                                   (tRBElem *) & FindReporter);
    if (pOldReporter != NULL)
    {
        IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &(u4Reporter));
        return IGMP_OK;
    }
    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpSrcReporterId,
                           pNewReporter, tIgmpSrcReporter) != NULL)
    {
        pNewReporter->u4IfIndex = pGrp->u4IfIndex;
        IGMP_IPVX_ADDR_COPY (&(pNewReporter->u4SrcRepAddr), &u4Reporter);
        IGMP_IPVX_ADDR_COPY (&(pNewReporter->GrpAddr), &(pGrp->u4Group));
        IGMP_IPVX_ADDR_CLEAR (&(pNewReporter->SourceAddr));
        pNewReporter->u1RepModeFlag = IGMP_ADD_REPORTER;
        pNewReporter->u1GrpSrcFlag = IGMP_GROUP_REPORTER;
        u4Status = RBTreeAdd (pGrp->GrpReporterTree, (tRBElem *) pNewReporter);
        if (u4Status == RB_FAILURE)
        {
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                       "Reporter Node %s is already present in RBtree of group %s \r\n",
                       IgmpPrintIPvxAddress (u4Reporter),
                       IgmpPrintIPvxAddress (pGrp->u4Group));
            IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId,
                            (UINT1 *) pNewReporter);
            return IGMP_NOT_OK;
        }
        IGMP_IPVX_ADDR_COPY (&(pGrp->u4LastReporter), &(u4Reporter));
        IgmpRedDbNodeInit (&(pNewReporter->SrcReporterDbNode),
                           IGMP_RED_SRC_REPORTER_INFO);
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &pNewReporter->SrcReporterDbNode);
        IgmpRedSyncDynInfo ();
        return IGMP_OK;
    }

    IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
               "Reporter %s for group %s is ignored as the threshold (%d) for reporters is reached \r\n",
               IgmpPrintIPvxAddress (u4Reporter),
               IgmpPrintIPvxAddress (pGrp->u4Group),
               MAX_IGMP_MCAST_SRS_REPORTERS);

    return IGMP_NOT_OK;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpDeleteV1V2GrpReporter                           */
/*                                                                         */
/*     Description   : This routine is used to del V1/V2 reporter for a    */
/*                     multicast group.                                       */
/*                                                                            */
/*     Input(s)      :  pGrp - Multicast Group for which the reporter is   */
/*                         to be deleted                                        */
/*                      u4Reporter - IP address of the reporter            */
/*                                                                         */
/*     Output(s)     :  IGMP_NOT_OK(failure), IGMP_OK(success)             */
/*                                                                         */
/*     Returns       :  None                                               */
/***************************************************************************/
INT4
IgmpDeleteV1V2GrpReporter (tIgmpGroup * pGrp, tIgmpIPvXAddr u4Reporter)
{
    tIgmpSrcReporter    FindReporter;
    tIgmpSrcReporter   *pDelReporter = NULL;

    if (pGrp->GrpReporterTree == NULL)
    {
        return IGMP_OK;
    }

    MEMSET (&FindReporter, 0, sizeof (tIgmpSrcReporter));
    IGMP_IPVX_ADDR_COPY (&(FindReporter.u4SrcRepAddr), &u4Reporter);
    pDelReporter = (tIgmpSrcReporter *) RBTreeGet (pGrp->GrpReporterTree,
                                                   (tRBElem *) & FindReporter);
    if (pDelReporter == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                   "Reporter Node %s not present in RBTree of group %s \r\n",
                   IgmpPrintIPvxAddress (u4Reporter),
                   IgmpPrintIPvxAddress (pGrp->u4Group));
        return IGMP_NOT_OK;
    }

    pDelReporter->u1RepModeFlag = IGMP_DELETE_REPORTER;
    pDelReporter->u1GrpSrcFlag = IGMP_GROUP_REPORTER;
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                             &pDelReporter->SrcReporterDbNode);
    IgmpRedSyncDynInfo ();
    if (RBTreeRemove (pGrp->GrpReporterTree, (tRBElem *) pDelReporter) ==
        RB_FAILURE)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                   "Reporter Node %s deletion failed in group %s \r\n",
                   IgmpPrintIPvxAddress (u4Reporter),
                   IgmpPrintIPvxAddress (pGrp->u4Group));
        return IGMP_NOT_OK;
    }
    else
    {
        IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId, (UINT1 *) pDelReporter);
        return IGMP_OK;
    }
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpDeleteAllV1V2GrpReporters                       */
/*                                                                         */
/*     Description   : This routine is used to del all V1/V2 reporter for  */
/*                     the multicast group.                               */
/*                                                                            */
/*     Input(s)      :  pGrp - Multicast Group for which the reporters are */
/*                         to be deleted                                      */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/***************************************************************************/
VOID
IgmpDeleteAllV1V2GrpReporters (tIgmpGroup * pGrp)
{
    tIgmpSrcReporter   *pDelReporter = NULL;
    tIgmpSrcReporter   *pNxtReporter = NULL;

    if (pGrp->GrpReporterTree == NULL)
    {
        return;
    }

    pDelReporter = (tIgmpSrcReporter *) RBTreeGetFirst (pGrp->GrpReporterTree);

    while (pDelReporter != NULL)
    {
        pNxtReporter =
            RBTreeGetNext (pGrp->GrpReporterTree, (tRBElem *) pDelReporter,
                           NULL);
        pDelReporter->u1RepModeFlag = IGMP_DELETE_REPORTER;
        pDelReporter->u1GrpSrcFlag = IGMP_GROUP_REPORTER;
        RBTreeRemove (pGrp->GrpReporterTree, (tRBElem *) pDelReporter);
        IgmpMemRelease (gIgmpMemPool.IgmpSrcReporterId, (UINT1 *) pDelReporter);
        pDelReporter = pNxtReporter;
        /*sync not sent to standby as this func() is called only on group delete */
    }
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the funtion IgmpDeleteAllV1V2GrpReporters.\n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpSSMMapRBTreeGroupEntryCmp                       */
/*                                                                         */
/*     Description   : This routine is used in SSM Map Group Table for     */
/*                     comparing two keys used in RBTree functionality.    */
/*                                                                         */
/*     Input(s)      : GroupEntryNode - Key 1                              */
/*                     GroupEntryIn   - Key 2                              */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :  -1/1 -> when any key element is Less/Greater       */
/*                      0    -> when all key elements are Equal            */
/*                                                                         */
/***************************************************************************/
/** $$TRACE_PROCEDURE_NAME  = IgmpSSMMapRBTreeGroupEntryCmp               **/
/** $$TRACE_PROCEDURE_LEVEL = MAIN                                        **/
/***************************************************************************/

INT4
IgmpSSMMapRBTreeGroupEntryCmp (tRBElem * pSSMMapGrpNodeIn,
                               tRBElem * pSSMMapGrpNodeRB)
{
    tIgmpSSMMapGrpEntry *pIgmpSSMGroupIn = NULL;
    tIgmpSSMMapGrpEntry *pIgmpSSMGroupRB = NULL;
    UINT4               u4RepAddrIn = 0;
    UINT4               u4RepAddrRB = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpSSMMapRBTreeGroupEntryCmp()\r \n");

    pIgmpSSMGroupIn = (tIgmpSSMMapGrpEntry *) pSSMMapGrpNodeIn;
    pIgmpSSMGroupRB = (tIgmpSSMMapGrpEntry *) pSSMMapGrpNodeRB;

    /* Compare the reporter address for the group */
    IGMP_IP_COPY_FROM_IPVX (&u4RepAddrIn, pIgmpSSMGroupIn->StartGrpAddr,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4RepAddrRB, pIgmpSSMGroupRB->StartGrpAddr,
                            IGMP_IPVX_ADDR_FMLY_IPV4);

    if (u4RepAddrIn < u4RepAddrRB)
    {
        return -1;
    }
    else if (u4RepAddrIn > u4RepAddrRB)
    {
        return 1;
    }

    u4RepAddrIn = u4RepAddrRB = 0;
    /* Compare the reporter address for the group */
    IGMP_IP_COPY_FROM_IPVX (&u4RepAddrIn, pIgmpSSMGroupIn->EndGrpAddr,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4RepAddrRB, pIgmpSSMGroupRB->EndGrpAddr,
                            IGMP_IPVX_ADDR_FMLY_IPV4);

    if (u4RepAddrIn < u4RepAddrRB)
    {
        return -1;
    }
    else if (u4RepAddrIn > u4RepAddrRB)
    {
        return 1;
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "Exiting the funtion IgmpSSMMapRBTreeGroupEntryCmp.\n");
    return IGMP_ZERO;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpUtilDeleteSSMMappedGroup                     */
/*                                                                         */
/*     Description   :    This function deletes SSM mapping configured     */
/*                        for the given group and source address.          */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    u4StartGroup, EndGroup, Source                   */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    IGMP_SUCCESS/IGMP_FAILURE                        */
/*                                                                         */
/***************************************************************************/
INT1
IgmpUtilDeleteSSMMappedGroup (tIgmpIPvXAddr u4StartGrp, tIgmpIPvXAddr u4EndGrp,
                              tIgmpIPvXAddr u4Source)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tIgmpSSMMapGrpEntry SSMMappedGrp;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpIface          IfNode;
#ifdef MULTICAST_SSM_ONLY
    tRBElem            *pRBElem = NULL;
#endif
    UINT4               u4Index = IGMP_ZERO;
    UINT4               u4Count = IGMP_ZERO;
    UINT4               u4IPvXZero = IGMP_ZERO;
    UINT4               u4StartGrpAddr = IGMP_ZERO;
    UINT4               u4EndGrpAddr = IGMP_ZERO;
    UINT4               u4GroupIn = IGMP_ZERO;
    INT4                i4Status = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpUtilDeleteSSMMappedGroup \n");

    MEMSET (&SSMMappedGrp, IGMP_ZERO, sizeof (tIgmpSSMMapGrpEntry));
    MEMSET (&IgmpGrpEntry, IGMP_ZERO, sizeof (tIgmpGroup));
    MEMSET (&IfNode, IGMP_ZERO, sizeof (tIgmpIface));

    RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
    if (u4Count == IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "No Group entries found in the SSM mapped group database\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_ENTRY_MODULE),
                      "Exiting the function IgmpUtilDeleteSSMMappedGroup \n");
        return IGMP_FAILURE;
    }

    IGMP_IPVX_ADDR_COPY (&(SSMMappedGrp.StartGrpAddr), &u4StartGrp);
    IGMP_IPVX_ADDR_COPY (&(SSMMappedGrp.EndGrpAddr), &u4EndGrp);

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGet
        (gIgmpSSMMappedGrp, (tRBElem *) & SSMMappedGrp);

    if (NULL == pSSMMappedGrp)
    {
        return IGMP_SUCCESS;
    }

    for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
    {
        if (IGMP_IPVX_ADDR_COMPARE (u4Source, pSSMMappedGrp->SrcAddr[u4Index])
            == IGMP_ZERO)
        {
            /* Mapping entry for the given group address present */
            IGMP_IPVX_ADDR_CLEAR (&pSSMMappedGrp->SrcAddr[u4Index]);
            IGMP_IPVX_ADDR_INIT_IPV4 (pSSMMappedGrp->SrcAddr[u4Index],
                                      IPVX_ADDR_FMLY_IPV4,
                                      (UINT1 *) &u4IPvXZero);
            pSSMMappedGrp->u4SrcCnt--;

            /* Remove the Grp-Src entry from IGMP membership table
             * that are learnt through this SSM mapped source */
            IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group), &u4StartGrp);
            IgmpGrpEntry.pIfNode = &IfNode;
            pGrp = (tIgmpGroup *) RBTreeGetNext
                (gIgmpGroup, (tRBElem *) & IgmpGrpEntry, NULL);
            while (pGrp != NULL)
            {
                pNextGrp =
                    (tIgmpGroup *) RBTreeGetNext (gIgmpGroup, (tRBElem *) pGrp,
                                                  NULL);
                u4StartGrpAddr = u4EndGrpAddr = u4GroupIn = IGMP_ZERO;
                IGMP_IP_COPY_FROM_IPVX (&u4StartGrpAddr, u4StartGrp,
                                        IGMP_IPVX_ADDR_FMLY_IPV4);
                IGMP_IP_COPY_FROM_IPVX (&u4EndGrpAddr, u4EndGrp,
                                        IGMP_IPVX_ADDR_FMLY_IPV4);
                IGMP_IP_COPY_FROM_IPVX (&u4GroupIn, pGrp->u4Group,
                                        IGMP_IPVX_ADDR_FMLY_IPV4);

                if ((u4GroupIn < u4StartGrpAddr) || (u4GroupIn > u4EndGrpAddr))
                {
                    pGrp = pNextGrp;
                    continue;
                }

                pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
                while (pSrc != NULL)
                {
                    pNextSrc =
                        (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList,
                                                      &pSrc->Link);

                    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
                    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                                              (UINT1 *) &u4IPvXZero);
                    if ((pSSMMappedGrp->u4SrcCnt == IGMP_ZERO) &&
                        (IGMP_IPVX_ADDR_COMPARE (gIPvXZero,
                                                 pSrc->u4SrcAddress) ==
                         IGMP_ZERO))
                    {
                        pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
                        pSrc->u1DelSyncFlag = IGMP_FALSE;
                        IgmpExpireSource (pSrc);
                        pSrc = pNextSrc;
                        continue;
                    }
                    if ((IGMP_IPVX_ADDR_COMPARE (u4Source,
                                                 pSrc->u4SrcAddress) ==
                         IGMP_ZERO) && (pSrc->u1SrcSSMMapped & IGMP_SRC_MAPPED))
                    {
                        if ((pSrc->u1SrcSSMMapped & IGMP_SRC_CONF) ||
                            (pSrc->u1DynJoinFlag == IGMP_TRUE))
                        {
                            pSrc->u1SrcSSMMapped =
                                (UINT1) (pSrc->
                                         u1SrcSSMMapped & (~IGMP_SRC_MAPPED));
                            IGMP_CHK_IF_SSM_RANGE (u4GroupIn,
                                                   i4Status) if (i4Status !=
                                                                 IGMP_SSM_RANGE)
                            {
                                /* IGMP Leave should be given only for non SSM range groups */
                                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                             &pSrc->Link);

                                if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                                {
                                    IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                                }
                                else
                                {
                                    IgmpUpdateMrps (pGrp, IGMP_LEAVE);
                                }
                            }
                        }
                        else
                        {
                            pSrc->u1DelSyncFlag = IGMP_FALSE;
                            IgmpExpireSource (pSrc);
                        }
#ifdef MULTICAST_SSM_ONLY
                        if ((TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO) &&
                            (pGrp->Timer.u1TmrStatus != IGMP_TMR_SET))
                        {
                            pGrp->u1DelSyncFlag = IGMP_FALSE;
                            IgmpExpireGroup (pGrp->pIfNode, pGrp,
                                             IGMP_VERSION_3, IGMP_TRUE,
                                             IGMP_FALSE);
                            if (pGrp->u1StaticFlg == IGMP_TRUE)
                            {
                                pGrp->pIfNode->u4InterfaceGroups--;
                                pRBElem =
                                    RBTreeGet (gIgmpGroup, (tRBElem *) pGrp);
                                IgmpDeleteAllV1V2GrpReporters (pGrp);
                                if (pGrp->GrpReporterTree != NULL)
                                {
                                    RBTreeDelete (pGrp->GrpReporterTree);
                                    pGrp->GrpReporterTree = NULL;
                                }
                                RBTreeRemove (gIgmpGroup, pRBElem);
                                IgmpMemRelease (gIgmpMemPool.IgmpGroupId,
                                                (UINT1 *) pGrp);

                                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                                               IgmpGetModuleName
                                               (IGMP_GRP_MODULE),
                                               "Group Node is deleted\n");
                            }
                        }
#endif
                    }
                    else if ((pSSMMappedGrp->u4SrcCnt == IGMP_ZERO) &&
                             (pSrc->u1SrcSSMMapped & IGMP_SRC_MAPPED))
                    {
                        pSrc->u1SrcSSMMapped =
                            (UINT1) (pSrc->u1SrcSSMMapped & (~IGMP_SRC_MAPPED));
                        IGMP_CHK_IF_SSM_RANGE (u4GroupIn,
                                               i4Status) if (i4Status !=
                                                             IGMP_SSM_RANGE)
                        {
                            /* IGMP Leave should be given only for non SSM range groups */
                            TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                            TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                         &pSrc->Link);

                            if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                            {
                                IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                            }
                            else
                            {
                                IgmpUpdateMrps (pGrp, IGMP_LEAVE);
                            }
                        }
                    }
                    pSrc = pNextSrc;
                }
                pGrp = pNextGrp;
            }                    /* Grp end */
        }                        /* Mapped source - end */
    }

    if (pSSMMappedGrp->u4SrcCnt == IGMP_ZERO)
    {
        IGMP_TRC_ARG2 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                       IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                       "SSM Mapping for Group range (%s - %s) is removed \r\n",
                       IgmpPrintIPvxAddress (u4StartGrp),
                       IgmpPrintIPvxAddress (u4EndGrp));
        RBTreeRemove (gIgmpSSMMappedGrp, (tRBElem *) pSSMMappedGrp);
        IgmpMemRelease (gIgmpMemPool.IgmpSSMMapGrpId, (UINT1 *) pSSMMappedGrp);
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting the function IgmpUtilDeleteSSMMappedGroup\n");
    return IGMP_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpUtilClearAllSSMMappedGroups                  */
/*                                                                         */
/*     Description   :    This function deletes all SSM mapping configured */
/*                        and removes the learnt group and source          */
/*                        entries learnt via SSM mapping                   */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    None                                             */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    None                                             */
/*                                                                         */
/***************************************************************************/
VOID
IgmpUtilClearAllSSMMappedGroups (VOID)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tIgmpSSMMapGrpEntry *pNextSSMMappedGrp = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpIface          IfNode;
    tIgmpIface         *pIfNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
#ifdef MULTICAST_SSM_ONLY
    tRBElem            *pRBElem = NULL;
#endif
    UINT4               u4Count = IGMP_ZERO;
    UINT4               u4StartGrpAddr = IGMP_ZERO;
    UINT4               u4EndGrpAddr = IGMP_ZERO;
    UINT4               u4GroupIn = IGMP_ZERO;
    UINT4               u4IPvXZero = IGMP_ZERO;
    INT4                i4Status = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpUtilClearAllSSMMappedGroups\n");

    MEMSET (&IgmpGrpEntry, IGMP_ZERO, sizeof (tIgmpGroup));

    RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
    if ((IGMP_SSM_MAP_GLOBAL_STATUS () == IGMP_DISABLE)
        || (u4Count == IGMP_ZERO))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "No Group entries found in the SSM mapped group database\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_ENTRY_MODULE),
                      "Exiting the function IgmpUtilClearAllSSMMappedGroups \n");
        return;
    }

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetFirst (gIgmpSSMMappedGrp);

    while (pSSMMappedGrp != NULL)
    {
        pNextSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetNext
            (gIgmpSSMMappedGrp, (tRBElem *) pSSMMappedGrp, NULL);

        /* Remove the Grp-Src entry from IGMP membership table
         * that are learnt through this SSM mapped source */
        IGMP_IPVX_ADDR_COPY (&(IgmpGrpEntry.u4Group),
                             &(pSSMMappedGrp->StartGrpAddr));
        MEMSET (&IfNode, IGMP_ZERO, sizeof (tIgmpIface));
        IgmpGrpEntry.pIfNode = &IfNode;
        pGrp = (tIgmpGroup *) RBTreeGetNext
            (gIgmpGroup, (tRBElem *) & IgmpGrpEntry, NULL);
        while (pGrp != NULL)
        {
            pNextGrp = (tIgmpGroup *) RBTreeGetNext
                (gIgmpGroup, (tRBElem *) pGrp, NULL);

            u4StartGrpAddr = u4EndGrpAddr = u4GroupIn = IGMP_ZERO;
            IGMP_IP_COPY_FROM_IPVX (&u4StartGrpAddr,
                                    pSSMMappedGrp->StartGrpAddr,
                                    IGMP_IPVX_ADDR_FMLY_IPV4);
            IGMP_IP_COPY_FROM_IPVX (&u4EndGrpAddr, pSSMMappedGrp->EndGrpAddr,
                                    IGMP_IPVX_ADDR_FMLY_IPV4);
            IGMP_IP_COPY_FROM_IPVX (&u4GroupIn, pGrp->u4Group,
                                    IGMP_IPVX_ADDR_FMLY_IPV4);

            if ((u4GroupIn < u4StartGrpAddr) || (u4GroupIn > u4EndGrpAddr))
            {
                pGrp = pNextGrp;
                continue;
            }

            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
            while (pSrc != NULL)
            {
                pNextSrc =
                    (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);

                IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
                IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                                          (UINT1 *) &u4IPvXZero);
                if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero,
                                            pSrc->u4SrcAddress) == IGMP_ZERO)
                {
                    pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
                    pSrc->u1DelSyncFlag = IGMP_FALSE;
                    IgmpExpireSource (pSrc);
                    pSrc = pNextSrc;
                    continue;
                }

                if (pSrc->u1SrcSSMMapped & IGMP_SRC_MAPPED)
                {
                    if ((pSrc->u1SrcSSMMapped & IGMP_SRC_CONF) ||
                        (pSrc->u1DynJoinFlag == IGMP_TRUE))
                    {
                        pSrc->u1SrcSSMMapped =
                            (UINT1) (pSrc->u1SrcSSMMapped & (~IGMP_SRC_MAPPED));
                        IGMP_CHK_IF_SSM_RANGE (u4GroupIn,
                                               i4Status) if (i4Status !=
                                                             IGMP_SSM_RANGE)
                        {
                            /* IGMP Leave should be given only for non SSM range groups */
                            TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                            TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                         &pSrc->Link);

                            if (pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                            {
                                IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                            }
                            else
                            {
                                IgmpUpdateMrps (pGrp, IGMP_LEAVE);
                            }
                        }
                    }
                    else
                    {
                        pSrc->u1DelSyncFlag = IGMP_FALSE;
                        IgmpExpireSource (pSrc);
                    }
#ifdef MULTICAST_SSM_ONLY
                    if ((TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO) &&
                        (pGrp->Timer.u1TmrStatus != IGMP_TMR_SET))
                    {
                        pGrp->u1DelSyncFlag = IGMP_FALSE;
                        IgmpExpireGroup (pGrp->pIfNode, pGrp, IGMP_VERSION_3,
                                         IGMP_TRUE, IGMP_FALSE);
                        if (pGrp->u1StaticFlg == IGMP_TRUE)
                        {
                            pGrp->pIfNode->u4InterfaceGroups--;
                            pRBElem = RBTreeGet (gIgmpGroup, (tRBElem *) pGrp);
                            IgmpDeleteAllV1V2GrpReporters (pGrp);
                            if (pGrp->GrpReporterTree != NULL)
                            {
                                RBTreeDelete (pGrp->GrpReporterTree);
                                pGrp->GrpReporterTree = NULL;
                            }
                            RBTreeRemove (gIgmpGroup, pRBElem);
                            IgmpMemRelease (gIgmpMemPool.IgmpGroupId,
                                            (UINT1 *) pGrp);

                            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                                           IgmpGetModuleName (IGMP_GRP_MODULE),
                                           "Group Node is deleted\n");
                        }
                    }
#endif
                }
                pSrc = pNextSrc;
            }                    /* End of Source List loop */
            pGrp = pNextGrp;
        }                        /* End of Group Table loop */
        pSSMMappedGrp = pNextSSMMappedGrp;
    }                            /* End of SSM Map Table loop */

    /* Send out query for dynamically learny entries */
    TMO_SLL_Scan ((&gIgmpIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pSllNode);
        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_QRY_MODULE,
                           IgmpGetModuleName (IGMP_QRY_MODULE),
                           "Going to send IGMP Query Messages\r\n");
            IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting the function IgmpUtilClearAllSSMMappedGroups \n");
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpGrpLookUpInSSMMapTable                       */
/*                                                                         */
/*     Description   :    This function looks for SSM mapping entry        */
/*                        for the given group and source address.          */
/*                                                                         */
/*     Input(s)      :    StartGrp, EndGrp, Source Addr                    */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    pSSMMappedGrp                                    */
/*                                                                         */
/***************************************************************************/
tIgmpSSMMapGrpEntry *
IgmpGrpLookUpInSSMMapTable (tIgmpIPvXAddr u4StartGrp, tIgmpIPvXAddr u4EndGrp,
                            tIgmpIPvXAddr u4Source)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tIgmpSSMMapGrpEntry SSMMappedGrp;
    UINT4               u4Index = IGMP_ZERO;
    UINT4               u4Count = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpGrpLookUpInSSMMapTable \n");

    MEMSET (&SSMMappedGrp, IGMP_ZERO, sizeof (tIgmpSSMMapGrpEntry));

    RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
    if (u4Count == IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "No Group entries found in the SSM mapped group database\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_ENTRY_MODULE),
                      "Exiting the function IgmpGrpLookUpInSSMMapTable \n");
        return NULL;
    }

    IGMP_IPVX_ADDR_COPY (&(SSMMappedGrp.StartGrpAddr), &u4StartGrp);
    IGMP_IPVX_ADDR_COPY (&(SSMMappedGrp.EndGrpAddr), &u4EndGrp);

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGet
        (gIgmpSSMMappedGrp, (tRBElem *) & SSMMappedGrp);

    if (NULL == pSSMMappedGrp)
    {
        return NULL;
    }

    for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
    {
        if (IGMP_IPVX_ADDR_COMPARE (u4Source, pSSMMappedGrp->SrcAddr[u4Index])
            == IGMP_ZERO)
        {
            /* Mapping entry for the given group address present */
            IGMP_TRC_ARG3 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "SSM Mapping for Group Range (%s - %s)"
                           " with Source %s is retrieved \r\n",
                           IgmpPrintIPvxAddress (pSSMMappedGrp->StartGrpAddr),
                           IgmpPrintIPvxAddress (pSSMMappedGrp->EndGrpAddr),
                           IgmpPrintIPvxAddress (pSSMMappedGrp->
                                                 SrcAddr[u4Index]));

            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_ENTRY_MODULE),
                          "Exiting the function IgmpGrpLookUpInSSMMapTable\n");
            return pSSMMappedGrp;
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting the function IgmpGrpLookUpInSSMMapTable\n");
    return NULL;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpChngStarGToSGJoinForSSMMappedGrp             */
/*                                                                         */
/*     Description   :    This function converts the (* G) join to         */
/*                        (S G) join with the source address configured    */
/*                        in the SSM mapping table for the given group.    */
/*                                                                         */
/*     Input(s)      :    SSMMapEntry, Group Addr, Host Addr               */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    IGMP_SUCCESS/IGMP_FAILURE                        */
/*                                                                         */
/***************************************************************************/
INT1
IgmpChngStarGToSGJoinForSSMMappedGrp (tIgmpIface * pIfNode,
                                      tIgmpSSMMapGrpEntry * pSSMMappedGrp,
                                      tIgmpIPvXAddr GroupAddr,
                                      tIgmpIPvXAddr HostAddr, UINT1 u1JoinFlag)
{
    tIgmpGroup         *pIgmpGrpEntry = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    tIgmpIPvXAddr      *pSrcSet = NULL;
    UINT4               u4Index = IGMP_ZERO;
    UINT4               u4IPvXZero = IGMP_ZERO;
    UINT4               u4SrcAddr = IGMP_ZERO;
    UINT4               u4TmpSrcAddr = IGMP_ZERO;
    UINT4               au4Sources[MAX_IGMP_SSM_MAP_SRC];
    INT4                i4Index = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpChngStarGToSGJoinForSSMMappedGrp \n");

    MEMSET (&au4Sources, IGMP_ZERO, sizeof (au4Sources));

    for (u4Index = IGMP_ZERO; u4Index < MAX_IGMP_SSM_MAP_SRC; u4Index++)
    {
        IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
        IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4IPvXZero);
        if (IGMP_IPVX_ADDR_COMPARE (gIPvXZero,
                                    pSSMMappedGrp->SrcAddr[u4Index]) !=
            IGMP_ZERO)
        {
            u4SrcAddr = IGMP_ZERO;
            IGMP_IP_COPY_FROM_IPVX (&u4SrcAddr, pSSMMappedGrp->SrcAddr[u4Index],
                                    IPVX_ADDR_FMLY_IPV4);
            u4TmpSrcAddr = OSIX_HTONL (u4SrcAddr);
            MEMCPY (&au4Sources[i4Index++], &u4TmpSrcAddr, IPVX_IPV4_ADDR_LEN);
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                          "Converting (*, %s) to (%s, %s) using SSM Mapping",
                          IgmpPrintIPvxAddress (GroupAddr),
                          IgmpPrintIPvxAddress (pSSMMappedGrp->
                                                SrcAddr[u4Index]),
                          IgmpPrintIPvxAddress (GroupAddr)));
        }
    }

    if (u1JoinFlag == IGMP_JOIN)
    {
        if (IgmpGroupHandleAllowNewSources (pIfNode, GroupAddr,
                                            pSSMMappedGrp->u4SrcCnt,
                                            (UINT1 *) au4Sources, HostAddr,
                                            IGMP_FALSE) != IGMP_OK)
        {
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                          "Failed to convert (*, %s) to (S, %s) for "
                          "one or more sources using SSM Mapping",
                          IgmpPrintIPvxAddress (GroupAddr),
                          IgmpPrintIPvxAddress (GroupAddr)));
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_ENTRY_MODULE),
                          "Exiting the function IgmpChngStarGToSGJoinForSSMMappedGrp \n");
            return IGMP_FAILURE;
        }
    }
    else
    {
        /* If both static and dynamic flag is set then handle dynamic leave
         * similar to IgmpBlockInclude since this is specific to SSM Mapping */
        pIgmpGrpEntry = IgmpGrpLookUp (pIfNode, GroupAddr);
        if ((pIgmpGrpEntry != NULL) && (pIgmpGrpEntry->u1StaticFlg == IGMP_TRUE)
            && (pIgmpGrpEntry->u1DynamicFlg == IGMP_TRUE))
        {
            u4Index = IGMP_ZERO;
            pSrcSet = gpu4SrcSetArray;
            for (pSrc = (tIgmpSource *)
                 TMO_SLL_First (&(pIgmpGrpEntry->srcList));
                 pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
            {
                pNextSrcLink =
                    TMO_SLL_Next (&(pIgmpGrpEntry->srcList), &pSrc->Link);
                if ((IgmpCheckSourceInArray
                     (pSrc->u4SrcAddress, (UINT1 *) au4Sources,
                      pSSMMappedGrp->u4SrcCnt))
                    && (pSrc->u1SrcSSMMapped & IGMP_SRC_MAPPED))
                {
                    if ((IGMP_IS_FAST_LEAVE_ENABLED (pIfNode)) ||
                        (IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
                    {
                        IgmpDeleteReporter (pIfNode, pSrc, HostAddr);
                    }
                    else if (!(IGMP_IS_CHANNEL_TRACK_ENABLED (pIfNode)))
                    {
                        /* Source Timer lowered to LMQT */
                        IgmpReStartSrcTmrToLmqTmr (pIfNode, pSrc);
                        IGMP_IPVX_ADDR_COPY (&(pSrcSet[u4Index++]),
                                             &(pSrc->u4SrcAddress));
                    }
                }
            }
            if (u4Index > IGMP_ZERO)
            {
                /* Sends GrpAndSrc Qry */
                IgmpSendGroupAndSourceSpecificQuery (pIfNode, pIgmpGrpEntry,
                                                     u4Index, pSrcSet);
                IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_GRP_MODULE,
                           IGMP_NAME,
                           "Group and source specific Query send for group %s on index %d\n",
                           IgmpPrintIPvxAddress (pIgmpGrpEntry->u4Group),
                           pIfNode->u4IfIndex);
            }
            return IGMP_SUCCESS;
        }

        if (IgmpGroupHandleBlockOldSources (pIfNode, GroupAddr,
                                            pSSMMappedGrp->u4SrcCnt,
                                            (UINT1 *) au4Sources, HostAddr,
                                            IGMP_FALSE) != IGMP_OK)
        {
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                          "Failed to convert (*, %s) to (S, %s) for "
                          "one or more sources using SSM Mapping",
                          IgmpPrintIPvxAddress (GroupAddr),
                          IgmpPrintIPvxAddress (GroupAddr)));
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_ENTRY_MODULE),
                          "Exiting the function IgmpChngStarGToSGJoinForSSMMappedGrp \n");
            return IGMP_FAILURE;
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting the function IgmpChngStarGToSGJoinForSSMMappedGrp \n");
    return IGMP_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpUtilChkIfSSMMappedGroup                      */
/*                                                                         */
/*     Description   :    This function checks whether SSM mapping is      */
/*                        configured for the given group address.          */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    u4Group                                          */
/*                                                                         */
/*     Output(s)     :    Status - Present or not                          */
/*                                                                         */
/*     Returns       :    SSMMapped Group entry                            */
/*                                                                         */
/***************************************************************************/
tIgmpSSMMapGrpEntry *
IgmpUtilChkIfSSMMappedGroup (tIgmpIPvXAddr u4Group, INT4 *pi4Status)
{
    UINT4               u4Count = IGMP_ZERO;
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpUtilChkIfSSMMappedGroup\n");

    RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
    if ((IGMP_GLOBAL_STATUS () == IGMP_DISABLE) ||
        (IGMP_SSM_MAP_GLOBAL_STATUS () == IGMP_DISABLE)
        || (u4Count == IGMP_ZERO))
    {
        *pi4Status = IGMP_FALSE;
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "No Group entries found in the SSM mapped group database\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_ENTRY_MODULE),
                      "Exiting the function IgmpUtilChkIfSSMMappedGroup\n");
        return NULL;
    }

    pSSMMappedGrp = IgmpUtilChkIfSSMMapExists (u4Group, pi4Status);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting the function IgmpUtilChkIfSSMMappedGroup\n");
    return pSSMMappedGrp;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpUtilChkIfSSMMapExists                        */
/*                                                                         */
/*     Description   :    This function checks whether SSM mapping is      */
/*                        configured for the given group address.          */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    u4Group                                          */
/*                                                                         */
/*     Output(s)     :    Status - Present or not                          */
/*                                                                         */
/*     Returns       :    SSMMapped Group entry                            */
/*                                                                         */
/***************************************************************************/
tIgmpSSMMapGrpEntry *
IgmpUtilChkIfSSMMapExists (tIgmpIPvXAddr u4Group, INT4 *pi4Status)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    UINT4               u4StartGrpAddr = IGMP_ZERO;
    UINT4               u4EndGrpAddr = IGMP_ZERO;
    UINT4               u4GroupIn = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpUtilChkIfSSMMapExists\n");

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetFirst (gIgmpSSMMappedGrp);

    while (pSSMMappedGrp != NULL)
    {
        u4StartGrpAddr = u4EndGrpAddr = u4GroupIn = IGMP_ZERO;
        IGMP_IP_COPY_FROM_IPVX (&u4StartGrpAddr, pSSMMappedGrp->StartGrpAddr,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (&u4EndGrpAddr, pSSMMappedGrp->EndGrpAddr,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (&u4GroupIn, u4Group, IGMP_IPVX_ADDR_FMLY_IPV4);

        if ((u4GroupIn >= u4StartGrpAddr) && (u4GroupIn <= u4EndGrpAddr))
        {
            /* Mapping entry for the given group address present */
            *pi4Status = IGMP_TRUE;
            IGMP_TRC_ARG1 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "SSM Mapping for Group %s is present \r\n",
                           IgmpPrintIPvxAddress (u4Group));
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_ENTRY_MODULE),
                          "Exiting the function IgmpUtilChkIfSSMMapExists\n");
            return pSSMMappedGrp;
        }
        pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetNext
            (gIgmpSSMMappedGrp, (tRBElem *) pSSMMappedGrp, NULL);
    }
    *pi4Status = IGMP_FALSE;
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting the function IgmpUtilChkIfSSMMapExists\n");
    return NULL;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :    IgmpUtilChkIfSuperSetOfSSMMappedGroup            */
/*                                                                         */
/*     Description   :    This function checks whether input group range   */
/*                        is a superset of existing group range.           */
/*                                                                         */
/*     Input(s)      :    StartAddrIn, EndAddrIn                           */
/*                                                                         */
/*     Output(s)     :    None                                             */
/*                                                                         */
/*     Returns       :    IGMP_TRUE, if superset                           */
/*                        IGMP_FALSE, otherwise                            */
/*                                                                         */
/***************************************************************************/
INT4
IgmpUtilChkIfSuperSetOfSSMMappedGroup (tIgmpIPvXAddr StartAddrIn,
                                       tIgmpIPvXAddr EndAddrIn)
{
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    UINT4               u4Count = IGMP_ZERO;
    UINT4               u4StartGrpAddr = IGMP_ZERO;
    UINT4               u4EndGrpAddr = IGMP_ZERO;
    UINT4               u4StartAddrIn = IGMP_ZERO;
    UINT4               u4EndAddrIn = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering the function IgmpUtilChkIfSuperSetOfSSMMappedGroup\n");

    RBTreeCount (gIgmpSSMMappedGrp, &u4Count);
    if (u4Count == IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_IO_MODULE, IGMP_NAME,
                       "No Group entries found in the SSM mapped group database\r\n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_ENTRY_MODULE),
                      "Exiting the function IgmpUtilChkIfSuperSetOfSSMMappedGroup\n");
        return (IGMP_FALSE);
    }

    IGMP_IP_COPY_FROM_IPVX (&u4StartAddrIn, StartAddrIn,
                            IGMP_IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4EndAddrIn, EndAddrIn, IGMP_IPVX_ADDR_FMLY_IPV4);

    pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetFirst (gIgmpSSMMappedGrp);

    while (pSSMMappedGrp != NULL)
    {
        u4StartGrpAddr = u4EndGrpAddr = IGMP_ZERO;
        IGMP_IP_COPY_FROM_IPVX (&u4StartGrpAddr, pSSMMappedGrp->StartGrpAddr,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (&u4EndGrpAddr, pSSMMappedGrp->EndGrpAddr,
                                IGMP_IPVX_ADDR_FMLY_IPV4);

        if ((u4StartGrpAddr > u4StartAddrIn) && (u4EndGrpAddr < u4EndAddrIn))
        {
            IGMP_TRC_ARG4 (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE,
                           IgmpTrcGetModuleName (IGMP_CNTRL_MODULE),
                           "Configured group range %x - %x is a superset of an existing SSM Mapping group range"
                           "%x - %x \r\n", u4StartAddrIn, u4EndAddrIn,
                           u4StartGrpAddr, u4EndGrpAddr);
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_ENTRY_MODULE),
                          "Exiting the function IgmpUtilChkIfSuperSetOfSSMMappedGroup\n");
            return (IGMP_TRUE);
        }
        pSSMMappedGrp = (tIgmpSSMMapGrpEntry *) RBTreeGetNext
            (gIgmpSSMMappedGrp, (tRBElem *) pSSMMappedGrp, NULL);
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting the function IgmpUtilChkIfSuperSetOfSSMMappedGroup\n");
    return (IGMP_FALSE);
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IgmpChkIfGrpSrcExists                               */
/*                                                                         */
/*     Description   : Checks if static group and source entry is          */
/*                     present for the given interface node                */
/*                                                                         */
/*     Input(s)      : Pointer to Interface node                           */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       : IGMP_TRUE/IGMP_FALSE                                */
/*                                                                         */
/***************************************************************************/

INT1
IgmpChkIfGrpSrcExists (tIgmpIface * pIfNode)
{
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
    UINT4               u4IPvXZero = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Entering IgmpChkIfGrpSrcExists()\r \n");

    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

    IgmpGrpEntry.u4IfIndex = pIfNode->u4IfIndex;
    IgmpGrpEntry.pIfNode = pIfNode;
    pGrp = (tIgmpGroup *)
        RBTreeGetNext (gIgmpGroup, (tRBElem *) & IgmpGrpEntry, NULL);
    while (pGrp != NULL)
    {
        if (pGrp->pIfNode->u4IfIndex > pIfNode->u4IfIndex)
        {
            break;
        }

        if (pGrp->pIfNode->u1AddrType != pIfNode->u1AddrType)
        {
            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) pGrp, NULL);
            continue;
        }

        pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);

        while (pSrc != NULL)
        {
            IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
            IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                                      (UINT1 *) &u4IPvXZero);
            if ((IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                         gIPvXZero) > IGMP_ZERO) &&
                (pSrc->u1StaticFlg == IGMP_TRUE) &&
                (pSrc->u1SrcSSMMapped & IGMP_SRC_CONF))
            {
                IGMP_DBG3 (MGMD_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
                           "Group %s and Source %s is present in interface index %d.\n",
                           IgmpPrintIPvxAddress (pSrc->u4GrpAddress),
                           IgmpPrintIPvxAddress (pSrc->u4SrcAddress),
                           pIfNode->u4IfIndex);
                MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                              IgmpGetModuleName (IGMP_ENTRY_MODULE),
                              "Exiting IgmpChkIfGrpSrcExists()\r \n");
                return IGMP_TRUE;
            }
            pSrc = (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);
        }
        pGrp = (tIgmpGroup *) RBTreeGetNext
            (gIgmpGroup, (tRBElem *) pGrp, NULL);
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "Exiting IgmpChkIfGrpSrcExists()\r \n");
    return IGMP_FALSE;
}
