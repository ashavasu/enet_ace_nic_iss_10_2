/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mldstub.c,v 1.2 2010/12/17 12:45:19 siva Exp $
 *
 * Description:This file contains the routines required for
 *              porting MLD on different targets
 *
 *******************************************************************/

#include "igmpinc.h"

/*********************************************************************/
/* Function           : MldInPktHandler                              */
/*                                                                   */
/* Description        : Entry point for the incoming MLD  packets.   */
/*                      Decodes the packet and invokes the           */
/*                      appropriate routine.                         */
/*                                                                   */
/* Input(s)           : MLD buffer pointer,                          */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
MldInPktHandler (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM (pBuf);
    return;
}

INT4
MldPortGetIpPortFrmIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4Port);

    return OSIX_FAILURE;
}

INT4
MldPortGetIp6IfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (pu4IfIndex);
    UNUSED_PARAM (u4Port);

    return OSIX_FAILURE;
}

INT4
MldPortIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);

    return OSIX_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :   MldOutSendQuery                                   */
/*                                                                         */
/*     Description   :   This function sends general/group specific        */
/*                       Query based on the group passed, on the given     */
/*                       interface                                         */
/*                                                                         */
/*     Input(s)      :   OutInterfaceId, GroupAddress.                     */
/*                                                                         */
/*     Output(s)     :   None                                              */
/*                                                                         */
/*     Returns       :   Returns OSIX_SUCCESS or OSIX_FAILURE              */
/*                                                                         */
/***************************************************************************/
INT1
MldOutSendQuery (tIgmpIface * pIfNode, tIPvXAddr * pGroup)
{
    UNUSED_PARAM (pIfNode);
    UNUSED_PARAM (pGroup);

    return OSIX_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldOutTxV1SchQuery                                 */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                      interface  on expiry of the scheduled timer        */
/*                                                                         */
/*     Input(s)      :  pIfNode   - Interface on which the query is to     */
/*                                    be scheduled                         */
/*                      pSchQuery   - the group address for which the query*/
/*                                   is to be scheduled                    */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  OSIX_SUCCESS/OSIX_FAILURE                          */
/*                                                                         */
/***************************************************************************/
INT4
MldOutTxV1SchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery)
{
    UNUSED_PARAM (pIfNode);
    UNUSED_PARAM (pSchQuery);

    return OSIX_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :  MldOutTxV2SchQuery                                 */
/*                                                                         */
/*     Description   :  This function Txmts. a  scheduled query on a       */
/*                 interface  on expiry of the scheduled timer             */
/*                                                                         */
/*     Input(s)      :  u4IfIndex - Interface on which the query is to     */
/*                                   be scheduled                          */
/*                      u4GroupAddr - the group addres for which the query */
/*                                    is to be scheduled                   */
/*                                                                         */
/*     Output(s)     :  None                                               */
/*                                                                         */
/*     Returns       :  None                                               */
/*                                                                         */
/***************************************************************************/
INT4
MldOutTxV2SchQuery (tIgmpIface * pIfNode, tIgmpSchQuery * pSchQuery)
{
    UNUSED_PARAM (pIfNode);
    UNUSED_PARAM (pSchQuery);

    return OSIX_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :      MldPortUpdateMrps                              */
/*                                                                         */
/*     Description   :     This function updates the MRPs                  */
/*                                                                         */
/*                                                                         */
/*     Input(s)      :    GrpSrcInfo - variable of tGrpNoteMsg             */
/*                        pGrp - pointer to  tIgmpGroup                    */
/*                        u1JoinLeaveFlag - Join/Leave                     */
/*                        u1FilterMode - Include/Exclude                   */
/*                        u1NumOfSrcs - No of sources being reported.      */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     None                                            */
/*                                                                         */
/***************************************************************************/
VOID
MldPortUpdateMrps (tIgmpGroup * pGrp, UINT1 u1JoinLeaveFlag)
{
    UNUSED_PARAM (pGrp);
    UNUSED_PARAM (u1JoinLeaveFlag);

    return;
}
