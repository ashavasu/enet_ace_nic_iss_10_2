/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stutlset.c,v 1.32 2017/05/30 11:13:43 siva Exp $
 *
 * Description:This file contains Low level routines  for stdigmp.mib  
 *
 *******************************************************************/

#include "include.h"
#include "stdigcon.h"
#include "stdigogi.h"
#include "midconst.h"
#include "igmpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE;
#endif

/* LOW LEVEL Routines for Table : IgmpInterfaceTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpInterfaceQueryInterval
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceQueryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                              INT4 i4IgmpInterfaceQuerierType,
                                              UINT4
                                              u4SetValIgmpInterfaceQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = 0;
    tIgmpIface         *pIfaceNode = NULL;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpInterfaceQueryIntvl SET routine Entry\n");
    if (IgmpGetPortFromIfIndex
        (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
         &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface is configured as upstream interface\r\n");
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    /* Get the interface node */
    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);

    if (pIfaceNode == NULL)
    {
        /* Not found, it's a read-create object, so creating the record */
        if (IgmpMgmtUtilNmhSetIgmpInterfaceStatus
            (i4IgmpInterfaceIfIndex, (UINT1) i4IgmpInterfaceQuerierType,
             IGMP_CREATE_AND_GO) == SNMP_FAILURE)
        {
            i1Status = SNMP_FAILURE;
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "IgmpInterfaceQueryIntvl SET routine" "Exit\n");
            return (i1Status);
        }
        pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);

        if (pIfaceNode == NULL)
        {
            i1Status = SNMP_FAILURE;
            return (i1Status);
        }
    }

    /* set the query-interval value */
    if (u4SetValIgmpInterfaceQueryInterval > (UINT4)
        (IGMP_IF_QUERY_RESPONSE_INTERVAL (pIfaceNode) / IGMP_TEN))
    {
        pIfaceNode->u2ConfiguredQueryInterval = (UINT2)
            u4SetValIgmpInterfaceQueryInterval;
        pIfaceNode->u2QueryInterval = (UINT2)
            u4SetValIgmpInterfaceQueryInterval;
        IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Set Query Interval on i/f %d to %d Secs \n",
                   i4IgmpInterfaceIfIndex, u4SetValIgmpInterfaceQueryInterval);
    }
    else
    {
        i1Status = SNMP_FAILURE;
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Query Interval must be GREATER then Query Response Interval \n");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "IgmpInterfaceQueryIntvl SET routine" "Exit\n");
        return (i1Status);
    }

    i1Status = SNMP_SUCCESS;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpInterfaceQueryIntvl SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpInterfaceStatus
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceStatus (INT4 i4IgmpInterfaceIfIndex,
                                       INT4 i4IgmpInterfaceQuerierType,
                                       INT4 i4SetValIgmpInterfaceStatus)
{

    INT1                i1Status = SNMP_FAILURE;
    UINT4               u4Port = 0;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGroupEntry;
    UINT1               u1VerFlag = IGMP_ZERO;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpInterfaceStatus SET routine Entry\n");

    /* Create the Interface Node by setting the InterfaceStatus value. If the
     * interface node is already created, then the interfaceStatus is ACTIVE.
     * If the interface node doesn't exist, then it should be created by setting
     * the interfaceStatus CREATE_AND_GO.
     */
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceQuerierType,
                                &u4Port) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface %d is configured as IGMP Proxy upstream interface\n",
                   i4IgmpInterfaceIfIndex);
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);

    if ((i4SetValIgmpInterfaceStatus != IGMP_CREATE_AND_GO) &&
        (i4SetValIgmpInterfaceStatus != IGMP_CREATE_AND_WAIT))
    {
        if (pIfaceNode == NULL)
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "IgmpInterfaceStatus SET routine Exit\n");
            CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pIfaceNode != NULL)
        {
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                           IGMP_NAME,
                           "Interface Node for IGMP/MLD already exists \n");
            CLI_SET_ERR (CLI_IGMP_INTERFACE_EXIST);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIgmpInterfaceStatus)
    {
        case IGMP_ACTIVE:
            pIfaceNode->u1EntryStatus = IGMP_ACTIVE;
            pIfaceNode->u1AdminStatus = IGMP_IFACE_ADMIN_UP;

            if (pIfaceNode->u1OperStatus == IGMP_IFACE_OPER_UP)
            {
                pIfaceNode->u1IfStatus = IGMP_UP;
                IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_UP);
#if defined (LNXIP4_WANTED) && !defined (ISS_WANTED)
                /* Add the interface to the virtual interface table */
                if (IgmpSetMcastSocketOption (pIfaceNode, IGMP_UP)
                    == IGMP_FAILURE)
                {
                    IGMP_DBG_INFO (MGMD_DBG_FLAG,
                                   IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                                   IGMP_NAME,
                                   "Enabling multicast socket option failed \n");
                    CLI_SET_ERR (CLI_IGMP_ENABLE_SOCK_FAILURE);
                    return SNMP_FAILURE;
                }
#endif
#if defined (LNXIP6_WANTED) && !defined (ISS_WANTED)
                /* Add the interface to the virtual interface table */
                if (MldSetMcastSocketOption (pIfaceNode->u4IfIndex, IGMP_UP)
                    == IGMP_FAILURE)
                {
                    IGMP_DBG1 (IGMP_DBG_FLAG,
                               IGMP_MGMT_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                               "Enabling multicast socket option on interface %d failed\n",
                               pIfaceNode->u4IfIndex);
                    CLI_SET_ERR (CLI_IGMP_ENABLE_SOCK_FAILURE);
                    return SNMP_FAILURE;
                }
#endif

#ifdef MFWD_WANTED
                if (gIgmpConfig.i4ProxyStatus == IGMP_ENABLE)
                {
                    IgpMfwdAddInterface (pIfaceNode->u4IfIndex);
                }
#endif
            }
            i1Status = SNMP_SUCCESS;
            break;

        case IGMP_CREATE_AND_GO:
            /* FALLTHROUGH */
        case IGMP_CREATE_AND_WAIT:
            i4SetValIgmpInterfaceStatus =
                (i4SetValIgmpInterfaceStatus == IGMP_CREATE_AND_GO) ?
                IGMP_ACTIVE : IGMP_CREATE_AND_WAIT;
            pIfaceNode =
                IgmpCreateInterfaceNode (u4Port,
                                         (UINT1) i4IgmpInterfaceQuerierType,
                                         (UINT1) i4SetValIgmpInterfaceStatus);
            if (pIfaceNode == NULL)
            {
                IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "ERROR: Not able to create i/f %d ",
                           i4IgmpInterfaceIfIndex);
                MGMD_DBG (IGMP_DBG_FAILURE,
                          "Failure in creating the interface\n");
            }
            else
            {
                if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV4)
                {
#if defined (LNXIP4_WANTED) && !defined (ISS_WANTED)
                    /* Add the interface to the virtual interface table */
                    if ((i4SetValIgmpInterfaceStatus == IGMP_ACTIVE) &&
                        (pIfaceNode->u1IfStatus == IGMP_IFACE_UP))
                    {
                        if (IgmpSetMcastSocketOption (pIfaceNode, IGMP_UP)
                            == IGMP_FAILURE)
                        {
                            IGMP_DBG_INFO (MGMD_DBG_FLAG,
                                           IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                                           IGMP_NAME,
                                           "Enabling multicast socket option failed \n");
                            CLI_SET_ERR (CLI_IGMP_ENABLE_SOCK_FAILURE);
                            return SNMP_FAILURE;
                        }
                    }
#endif
                    if (pIfaceNode->u1IfStatus == IGMP_IFACE_UP)
                    {
                        IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_UP);
                    }

#ifdef MFWD_WANTED
                    if ((gIgmpConfig.i4ProxyStatus == IGMP_ENABLE) &&
                        (i4SetValIgmpInterfaceStatus == IGMP_ACTIVE))
                    {
                        IgpMfwdAddInterface (pIfaceNode->u4IfIndex);
                    }
#endif

                    i1Status = SNMP_SUCCESS;
                }
                else if (i4IgmpInterfaceQuerierType == IPVX_ADDR_FMLY_IPV6)
                {
#if defined (LNXIP6_WANTED) && !defined (ISS_WANTED)
                    /* Add the interface to the virtual interface table */
                    if ((i4SetValIgmpInterfaceStatus == IGMP_ACTIVE) &&
                        (pIfaceNode->u1IfStatus = IGMP_IFACE_UP))
                    {
                        if (MldSetMcastSocketOption (pIfaceNode->u4IfIndex,
                                                     IGMP_UP) == IGMP_FAILURE)
                        {
                            IGMP_DBG_INFO (IGMP_DBG_FLAG,
                                           IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                                           IGMP_NAME,
                                           "Enabling multicast socket option failed \n");
                            CLI_SET_ERR (CLI_IGMP_ENABLE_SOCK_FAILURE);
                            return SNMP_FAILURE;
                        }
                    }
#endif
                    if (pIfaceNode->u1IfStatus == IGMP_IFACE_UP)
                    {
                        IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_UP);
                    }
                    i1Status = SNMP_SUCCESS;
                }

            }
            break;

        case IGMP_DESTROY:
#if defined (LNXIP4_WANTED) && !defined (ISS_WANTED)
            /* Delete the interface from the virtual interface table */
            if (IgmpSetMcastSocketOption (pIfaceNode, IGMP_DOWN)
                == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG,
                               IGMP_MGMT_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                               "Disabling multicast socket option failed \n");
                CLI_SET_ERR (CLI_IGMP_DISABLE_SOCK_FAILURE);
                return SNMP_FAILURE;
            }
#endif
#if defined (LNXIP6_WANTED) && !defined (ISS_WANTED)
            /* Delete the interface from the virtual interface table */
            if (MldSetMcastSocketOption (pIfaceNode->u4IfIndex, IGMP_DOWN)
                == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG,
                               IGMP_MGMT_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                               "Disabling multicast socket option failed \n");
                CLI_SET_ERR (CLI_IGMP_DISABLE_SOCK_FAILURE);
                return SNMP_FAILURE;
            }
#endif
#ifdef MFWD_WANTED
            if (gIgmpConfig.i4ProxyStatus == IGMP_ENABLE)
            {
                IgpMfwdDelInterface (pIfaceNode->u4IfIndex);
            }
#endif
            /* If IGMP Proxy is enabled and interface is configured as upstream 
             * interface delete the upstream interface information from the 
             * Proxy database */

            if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
                (IgpUtilCheckUpIface (pIfaceNode->u4IfIndex) == IGMP_TRUE))
            {
                if (IgpUpIfDeleteUpIfaceEntry
                    (pIfaceNode->u4IfIndex, IGMP_TRUE) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                   "Deletion of Upstream interface failed \r\n");
                    return SNMP_FAILURE;
                }
            }
            else
            {
                MEMSET (&IgmpGroupEntry, IGMP_ZERO, sizeof (tIgmpGroup));
                /* clear querier related stuff */
                IgmpHdlTransitionToNonQuerier (pIfaceNode);

                /* Clear all membership information */
                IgmpGroupEntry.u4IfIndex = pIfaceNode->u4IfIndex;
                IgmpGroupEntry.pIfNode = pIfaceNode;

                pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) &
                                                     IgmpGroupEntry, NULL);
                while (pGrp != NULL)
                {
                    if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
                    {
                        /* Group entry doesn't belong to 
                         * specified interface index */
                        break;
                    }

                    if (pGrp->pIfNode->u1AddrType != i4IgmpInterfaceQuerierType)
                    {
                        /* Group entry doesn't belong to specified type */
                        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                             (tRBElem *) pGrp,
                                                             NULL);
                        continue;
                    }
                    IGMP_IPVX_ADDR_COPY (&(IgmpGroupEntry.u4Group),
                                         &(pGrp->u4Group));

                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
                        {
                            u1VerFlag = IGMP_VERSION_2;
                        }
                        else
                        {
                            u1VerFlag = IGMP_VERSION_3;
                        }
                    }
                    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        if (pGrp->u4GroupCompMode != MLD_GCM_MLDV2)
                        {
                            u1VerFlag = MLD_VERSION_1;
                        }
                        else
                        {
                            u1VerFlag = MLD_VERSION_2;
                        }
                    }

                    u1VerFlag |= IGMP_V3REP_SEND_FLAG;

                    /* IGMP Proxy is enabled so update the consolidated
                     * group membership data base */
                    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                    {
                        /* Call the PROXY routine to update the consolidate group
                         * membership data base for group */
                        IgpGrpConsolidate (pGrp, u1VerFlag,
                                           IGMP_GROUP_DELETION);
                    }
                    else
                    {
                        /* Updating MRPs */
                        IgmpUpdateMrps (pGrp, IGMP_LEAVE);
                    }
                    pGrp->u1DelSyncFlag = IGMP_FALSE;
                    IgmpDeleteGroup (pIfaceNode, pGrp, pGrp->u1StaticFlg);
                    pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                         (tRBElem *) &
                                                         IgmpGroupEntry, NULL);
                }
            }
            IgmpDeleteInterfaceNode (pIfaceNode);

            i1Status = SNMP_SUCCESS;
            break;

        case IGMP_NOT_IN_SERVICE:
            /* Since if Proxy is enabled and the interface is configured 
             * as an upstrema interface we do not allow any router configuration
             * on this interface */
            if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
                (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                               "Interface is configured as upstream interface\r\n");
                CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
                return SNMP_FAILURE;
            }

            pIfaceNode->u1EntryStatus = IGMP_NOT_IN_SERVICE;
            if (pIfaceNode->u1IfStatus == IGMP_UP)
            {
#if defined (LNXIP4_WANTED) && !defined (ISS_WANTED)
                /* Delete the interface from the virtual interface table */
                if (IgmpSetMcastSocketOption (pIfaceNode, IGMP_DOWN)
                    == IGMP_FAILURE)
                {
                    IGMP_DBG_INFO (MGMD_DBG_FLAG,
                                   IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                                   IGMP_NAME,
                                   "Disabling multicast socket option failed \n");
                    CLI_SET_ERR (CLI_IGMP_DISABLE_SOCK_FAILURE);
                    return SNMP_FAILURE;
                }
#endif
#if defined (LNXIP6_WANTED) && !defined (ISS_WANTED)
                /* Delete the interface from the virtual interface table */
                if (MldSetMcastSocketOption (pIfaceNode -.u4IfIndex, IGMP_DOWN)
                    == IGMP_FAILURE)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG,
                                   IGMP_MGMT_MODULE | IGMP_ALL_MODULES,
                                   IGMP_NAME,
                                   "Disabling multicast socket option failed \n");
                    CLI_SET_ERR (CLI_IGMP_DISABLE_SOCK_FAILURE);
                    return SNMP_FAILURE;
                }
#endif
#ifdef MFWD_WANTED
                if (gIgmpConfig.i4ProxyStatus == IGMP_ENABLE)
                {
                    IgpMfwdDelInterface (pIfaceNode->u4IfIndex);
                }
#endif
                IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_DOWN);
            }
            pIfaceNode->u1AdminStatus = IGMP_IFACE_ADMIN_DOWN;
            pIfaceNode->u1IfStatus = IGMP_DOWN;
            i1Status = SNMP_SUCCESS;
            break;

        default:
            break;

    }                            /* switch - END */

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpInterfaceStatus SET routine Exit\n");
    return (i1Status);
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpInterfaceVersion
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceVersion (INT4 i4IgmpInterfaceIfIndex,
                                        INT4 i4IgmpInterfaceQuerierType,
                                        UINT4 u4SetValIgmpInterfaceVersion)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpInterfaceVersion SET routine Entry\n");

    /* As this mib object is read-Create object,
     * if the interface table entry is not avaiable
     * Create the interface and update the query interval
     */
    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceQuerierType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);
    if (pIfaceNode == NULL)
    {
        if (IgmpMgmtUtilNmhSetIgmpInterfaceStatus
            (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
             IGMP_CREATE_AND_GO) == SNMP_FAILURE)
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "IgmpInterfaceVersion SET routine" "Exit\n");
            return (SNMP_FAILURE);
        }
        pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);
    }
    /* if the igmp-version differs, clear everything and 
     * update MRPs.  send general query to re-establish 
     * the Group membership states.
     */
    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface is configured as upstream interface\r\n");
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }
    if (NULL == pIfaceNode)
    {
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    if (pIfaceNode->u1Version != (UINT1) u4SetValIgmpInterfaceVersion)
    {
        pIfaceNode->u1Version = (UINT1) u4SetValIgmpInterfaceVersion;
        if (pIfaceNode->u1IfStatus == IGMP_UP)
        {
            IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_DOWN);
            IgmpSetInterfaceAdminStatus (pIfaceNode, IGMP_UP);
        }
    }

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpInterfaceVersion SET routine Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpInterfaceQueryMaxResponseTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceQueryMaxResponseTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceQueryMaxResponseTime (INT4
                                                     i4IgmpInterfaceIfIndex,
                                                     INT4
                                                     i4IgmpInterfaceQuerierType,
                                                     UINT4
                                                     u4SetValIgmpInterfaceQueryMaxResponseTime)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpInterfaceQueryMaxResponseTime SET routine Entry\n");

    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceQuerierType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Interface %d is configured as upstream interface\r\n",
                   u4Port);
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);

    if (pIfaceNode == NULL)
    {
        if (IgmpMgmtUtilNmhSetIgmpInterfaceStatus
            (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
             IGMP_CREATE_AND_GO) == SNMP_FAILURE)
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "IgmpInterfaceQueryMaxResponseTime SET"
                          "routine Exit\n");
            return (SNMP_FAILURE);
        }
        pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);
    }
    if (NULL == pIfaceNode)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "No Such Interface Node routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    /* QueryMaxResponseTime must be < QueryInterval */
    /* Section: 8.3 */
    if ((u4SetValIgmpInterfaceQueryMaxResponseTime / IGMP_TEN) >
        IGMP_IF_QUERY_INTERVAL (pIfaceNode))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "ERROR: Query-Max-Resp_Time must be less than Query Interval");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "IgmpInterfaceQueryMaxResponseTime SET routine Exit\n");
        CLI_SET_ERR (CLI_IGMP_QUERY_INTERVAL_ERR);
        return SNMP_FAILURE;
    }

    pIfaceNode->u2MaxRespTime = (UINT2)
        u4SetValIgmpInterfaceQueryMaxResponseTime;
    IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
               "Max Query Response time for i/f %d set to %d in "
               "tenths of secs \n", i4IgmpInterfaceIfIndex,
               pIfaceNode->u2MaxRespTime);
    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpInterfaceQueryMaxResponseTime SET routine Exit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpInterfaceProxyIfIndex
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceProxyIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceProxyIfIndex (INT4 i4IgmpInterfaceIfIndex,
                                             INT4 i4IgmpInterfaceQuerierType,
                                             INT4
                                             i4SetValIgmpInterfaceProxyIfIndex)
{
    UNUSED_PARAM (i4IgmpInterfaceIfIndex);
    UNUSED_PARAM (i4SetValIgmpInterfaceProxyIfIndex);
    UNUSED_PARAM (i4IgmpInterfaceQuerierType);
    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "This object not supported by IGMP as there is no proxy"
                   "support\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpInterfaceRobustness
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceRobustness
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceRobustness (INT4 i4IgmpInterfaceIfIndex,
                                           INT4 i4IgmpInterfaceQuerierType,
                                           UINT4
                                           u4SetValIgmpInterfaceRobustness)
{
    tIgmpIface         *pIfaceNode = NULL;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpInterfaceRobustness SET routine Entry\n");

    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceQuerierType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface is configured as upstream interface\r\n");
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);

    if (pIfaceNode == NULL)
    {
        if (IgmpMgmtUtilNmhSetIgmpInterfaceStatus
            (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
             IGMP_CREATE_AND_GO) == SNMP_FAILURE)
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "IgmpInterfaceRobustness SET routine" "Exit\n");
            return (SNMP_FAILURE);
        }
        pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);
    }
    if (NULL == pIfaceNode)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "No Such Interface Node" "routine Exit\n");
        return (SNMP_FAILURE);
    }

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    pIfaceNode->u1Robustness = (UINT1) (u4SetValIgmpInterfaceRobustness);

    IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
               "Interface Robustness for i/f %d set to %d secs \n",
               i4IgmpInterfaceIfIndex, pIfaceNode->u1Robustness);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpInterfaceRobustness SET routine Exit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpInterfaceLastMembQueryIntvl
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                setValIgmpInterfaceLastMembQueryIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpInterfaceLastMembQueryIntvl (INT4 i4IgmpInterfaceIfIndex,
                                                   INT4
                                                   i4IgmpInterfaceQuerierType,
                                                   UINT4
                                                   u4SetValIgmpInterfaceLastMembQueryIntvl)
{
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSchQuery      *pSchQuery = NULL;
    tIgmpGroup          IgmpGroupEntry;
    UINT4               u4Duration = IGMP_ZERO;
    UINT4               u4RemTime = IGMP_ZERO;
    UINT4               u4PrevTime = IGMP_ZERO;
    UINT4               u4Port = 0;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpInterfaceLastMembQueryIntvl SET routine Entry\n");
    MEMSET (&IgmpGroupEntry, IGMP_ZERO, sizeof (tIgmpGroup));

    if (IgmpGetPortFromIfIndex (i4IgmpInterfaceIfIndex,
                                i4IgmpInterfaceQuerierType,
                                &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface is configured as upstream interface\r\n");
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);

    if (pIfaceNode == NULL)
    {
        if (IgmpMgmtUtilNmhSetIgmpInterfaceStatus
            (i4IgmpInterfaceIfIndex, i4IgmpInterfaceQuerierType,
             IGMP_CREATE_AND_GO) == SNMP_FAILURE)
        {
            MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                          IgmpGetModuleName (IGMP_EXIT_MODULE),
                          "IgmpInterfaceLastMembQueryIntvl SET routine Exit\n");
            return (SNMP_FAILURE);
        }
        pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpInterfaceQuerierType);
    }
    if (NULL == pIfaceNode)
    {
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "No Such Interface Node" "routine Exit\n");
        return (SNMP_FAILURE);
    }

    /* Validate the IgmpInterfaceIfIndex in the IgmpInterface Table */
    /* Value of this object is irrelevant if igmpInterfaceVersion
     * is one
     */
    if (pIfaceNode->u1Version == IGMP_VERSION_1)
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Error: Irrelevant on IGMPV1 interface");
        MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                      IgmpGetModuleName (IGMP_EXIT_MODULE),
                      "IgmpInterfaceLastMembQueryIntvl SET routine Exit\n");
        CLI_SET_ERR (CLI_IGMPV1_LAST_MEM_QUERY_INTVL_ERR);
        return SNMP_FAILURE;
    }

    /*Restarting the source timer as per the updated
     *Last member query interval  */
    IgmpGroupEntry.u4IfIndex = pIfaceNode->u4IfIndex;
    IgmpGroupEntry.pIfNode = pIfaceNode;

    pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                         (tRBElem *) & IgmpGroupEntry, NULL);
    while (pGrp != NULL)
    {
        if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
        {
            /* Group entry doesn't belong to
             * specified interface index */
            break;
        }
        TMO_SLL_Scan (&(pGrp->srcList), pSrc, tIgmpSource *)
        {
            if ((pSrc != NULL) &&
                (pSrc->u1SrsBitFlag == IGMP_TRUE) &&
                ((pSrc->srcTimer).u1TmrStatus == IGMP_TMR_SET))
            {
                pSchQuery = IgmpGetSchQueryGrp (pIfaceNode->pIfaceQry,
                                                pGrp->u4Group);
                if (pSchQuery != NULL)
                {
                    if (TmrGetRemainingTime (gIgmpTimerListId,
                                             &(pSrc->srcTimer.TimerBlk.
                                               TimerNode),
                                             &u4RemTime) == TMR_SUCCESS)
                    {
                        u4PrevTime = ((u4RemTime / 100) %
                                      (pIfaceNode->u4LastMemberQueryIntvl /
                                       IGMP_TEN));
                        u4Duration =
                            (u4PrevTime +
                             ((pSchQuery->u1RetrNum) *
                              (u4SetValIgmpInterfaceLastMembQueryIntvl /
                               IGMP_TEN)));
                        IGMPSTOPTIMER (pSrc->srcTimer);
                        IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
                                        u4Duration, (u4RemTime % 100));
                    }
                }
            }
        }
        pGrp =
            (tIgmpGroup *) RBTreeGetNext (gIgmpGroup, (tRBElem *) pGrp, NULL);
    }

    pIfaceNode->u4LastMemberQueryIntvl =
        (UINT4) (u4SetValIgmpInterfaceLastMembQueryIntvl);

    IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
               "Last member query interval for i/f %d set to %d secs\n",
               i4IgmpInterfaceIfIndex, pIfaceNode->u4LastMemberQueryIntvl);

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpInterfaceLastMembQueryIntvl SET routine Exit\n");

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpCacheSelf
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                setValIgmpCacheSelf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpCacheSelf (tIgmpIPvXAddr u4IgmpCacheAddress,
                                 INT4 i4IgmpCacheAddrType,
                                 INT4 i4IgmpCacheIfIndex,
                                 INT4 i4SetValIgmpCacheSelf)
{
    UNUSED_PARAM (u4IgmpCacheAddress);
    UNUSED_PARAM (i4IgmpCacheIfIndex);
    UNUSED_PARAM (i4SetValIgmpCacheSelf);
    UNUSED_PARAM (i4IgmpCacheAddrType);

    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "This object is not supported");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpCacheStatus
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                setValIgmpCacheStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpCacheStatus (tIgmpIPvXAddr u4IgmpCacheAddress,
                                   INT4 i4IgmpCacheAddrType,
                                   INT4 i4IgmpCacheIfIndex,
                                   INT4 i4SetValIgmpCacheStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    INT1                i1RetVal = SNMP_SUCCESS;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tTMO_SLL_NODE      *pNextSrcLink = NULL;
    tIgmpTimer         *pTimer = NULL;
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    tIgmpIPvXAddr       ReporterAddrZero;
    UINT4               u4IpGrp = IGMP_ZERO;
    UINT4               u4Port = 0;
    UINT1               u1VerFlag = IGMP_ZERO;
    INT4                i4Stat = IGMP_ZERO;
    INT4                i4MapStatus = IGMP_FALSE;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpCacheStatus SET routine Entry\n");

    MEMSET (&ReporterAddrZero, IGMP_ZERO, sizeof (tIgmpIPvXAddr));

    if (IgmpGetPortFromIfIndex (i4IgmpCacheIfIndex,
                                i4IgmpCacheAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface is configured as upstream interface\r\n");
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpCacheAddrType);

    if (pIfaceNode == NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                   "Interface %d not present", i4IgmpCacheIfIndex);
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pGrp = IgmpGrpLookUp (pIfaceNode, u4IgmpCacheAddress);

    if (((i4SetValIgmpCacheStatus == IGMP_DESTROY) ||
         (i4SetValIgmpCacheStatus == IGMP_ACTIVE)) && (pGrp == NULL))
    {
        return SNMP_FAILURE;
    }

    if (i4IgmpCacheAddrType == IGMP_IPVX_ADDR_FMLY_IPV4)
    {
        /* check whether SSM mapping is configured for the group */
        pSSMMappedGrp =
            IgmpUtilChkIfSSMMappedGroup (u4IgmpCacheAddress, &i4MapStatus);
    }

    switch (i4SetValIgmpCacheStatus)
    {
        case IGMP_CREATE_AND_WAIT:
            /* FALL Through */
        case IGMP_CREATE_AND_GO:
            if ((pIfaceNode->u1Version != IGMP_VERSION_3)
                && (i4IgmpCacheAddrType == IGMP_IPVX_ADDR_FMLY_IPV4))
            {
                IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4IgmpCacheAddress,
                                        IGMP_IPVX_ADDR_FMLY_IPV4);

                IGMP_CHK_IF_SSM_RANGE (u4IpGrp, i4Stat)
                    if (((i4Stat == IGMP_SSM_RANGE)
                         && (i4MapStatus == IGMP_FALSE))
                        || (u4IpGrp == IGMP_INVALID_SSM_GRP_ADDR))
                {
                    IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                               "Group %s in SSM Range , Ignored !!!!!! \n",
                               IgmpPrintIPvxAddress (u4IgmpCacheAddress));
                    IGMP_DBG (IGMP_DBG_EXIT, "Exiting IgmpInput\n");
                    CLI_SET_ERR (CLI_IGMP_MULTICAST_RANGE_SSM);
                    pIfaceNode->u4IgmpInvalidSSMPkts++;
                    return SNMP_FAILURE;
                }
            }
            if (pGrp == NULL)
            {
                pGrp = IgmpAddGroup (pIfaceNode, u4IgmpCacheAddress, IGMP_TRUE);
                if (pGrp == NULL)
                {
                    CLI_SET_ERR (CLI_IGMP_MAX_GROUPS);
                    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                   "Group node not created  \n");
                    return SNMP_FAILURE;
                }
                IGMP_DBG2 (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "Membership for Group %s added in i/f %d \n",
                           IgmpPrintIPvxAddress (u4IgmpCacheAddress),
                           pIfaceNode->u4IfIndex);

                IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
            }
            pGrp->u1FilterMode = IGMP_FMODE_EXCLUDE;
            pGrp->u1StaticFlg = IGMP_TRUE;
            if (i4MapStatus == IGMP_TRUE)
            {
                pSrc = IgmpAddSource (pGrp, gIPvXZero, gIPvXZero, IGMP_FALSE);
                if (pSrc != NULL)
                {
                    pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_CONF;
                    pSrc->u1StaticFlg = IGMP_TRUE;
                    pSrc->u1FwdState = IGMP_TRUE;
                }
                if (IgmpAddSSMMappedSourcesForStaticGroup (pGrp, pSSMMappedGrp,
                                                           pGrp->pIfNode, NULL,
                                                           IGMP_CREATE_AND_GO,
                                                           IGMP_FALSE) ==
                    IGMP_NOT_OK)
                {
                    CLI_SET_ERR (CLI_IGMP_SSMMAP_STATIC_GRP_ASSOC);
                    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                                  "Exiting  IgmpMgmtUtilNmhSetIgmpCacheStatus()\r \n");
                    return SNMP_FAILURE;
                }
            }

            if (i4SetValIgmpCacheStatus == IGMP_CREATE_AND_GO)
            {
                pGrp->u1EntryStatus = IGMP_ACTIVE;

                /* Updating MRPs, this check is done for static group 
                 * addition since static configuration is allowed when IGMP
                 * is disabled */
                if ((gIgmpConfig.u1IgmpStatus == IGMP_DISABLE) &&
                    (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    return i1RetVal;
                }
                else if ((gIgmpConfig.u1MldStatus == MLD_DISABLE) &&
                         (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    return i1RetVal;
                }

                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    if (pIfaceNode->u1Version == IGMP_VERSION_3)
                    {
                        /* Call the PROXY routine to update the consolidate group
                         * membership data base for static group registration */
                        IgpGrpConsolidate (pGrp, IGMP_VERSION_3,
                                           IGMP_NEW_GROUP);

                        if (IgpUpIfFormAndSendV3Report (IGMP_ZERO)
                            == IGMP_FAILURE)
                        {
                            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,
                                           IGMP_NAME, "IGMP PROXY: Sending "
                                           " upstream report failed \r\n");
                        }
                    }
                    else
                    {
                        /* Call the PROXY routine to update the consolidate group
                         * membership data base for static group registration */
                        IgpGrpConsolidate (pGrp, pIfaceNode->u1Version,
                                           IGMP_NEW_GROUP);
                    }

                    return i1RetVal;
                }

                if ((gIgmpConfig.u1IgmpStatus == IGMP_ENABLE) &&
                    (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    if (pIfaceNode->u1Version == IGMP_VERSION_3)
                    {
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }
                    else
                    {
                        IgmpUpdateMrps (pGrp, IGMP_JOIN);
                    }
                }
                else if ((gIgmpConfig.u1MldStatus == MLD_ENABLE) &&
                         (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    if (pIfaceNode->u1Version == MLD_VERSION_2)
                    {
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }
                    else
                    {
                        IgmpUpdateMrps (pGrp, IGMP_JOIN);
                    }
                }

            }
            else
            {
                pGrp->u1EntryStatus = IGMP_NOT_READY;
            }

            i1Status = i1RetVal;
            break;

        case IGMP_ACTIVE:
            if (pGrp != NULL)
            {
                pGrp->u1EntryStatus = IGMP_ACTIVE;

                /* Updating MRPs, this check is done for static group 
                 * addition since static configuration is allowed when IGMP
                 * is disabled */
                if ((gIgmpConfig.u1IgmpStatus == IGMP_DISABLE) &&
                    (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    return SNMP_SUCCESS;
                }
                else if ((gIgmpConfig.u1MldStatus == MLD_DISABLE) &&
                         (i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    return SNMP_SUCCESS;
                }

                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    if (pIfaceNode->u1Version == IGMP_VERSION_3)
                    {
                        /* Call the PROXY routine to update the consolidate group
                         * membership data base for static group registration */
                        IgpGrpConsolidate (pGrp, IGMP_VERSION_3,
                                           IGMP_NEW_GROUP);

                        if (IgpUpIfFormAndSendV3Report (IGMP_ZERO)
                            == IGMP_FAILURE)
                        {
                            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,
                                           IGMP_NAME, "IGMP PROXY: Sending "
                                           " upstream report failed \r\n");
                        }
                    }
                    else
                    {
                        /* Call the PROXY routine to update the consolidate group
                         * membership data base for static group registration */
                        IgpGrpConsolidate (pGrp, pIfaceNode->u1Version,
                                           IGMP_NEW_GROUP);
                    }

                    return SNMP_SUCCESS;
                }

                if (((pIfaceNode->u1Version == MLD_VERSION_2) ||
                     (pIfaceNode->u1Version == IGMP_VERSION_3)))
                {
                    if (TMO_SLL_Count (&pGrp->srcList) == 0)
                    {
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }
                    else
                    {
                        for (pSrc = (tIgmpSource *)
                             TMO_SLL_First (&(pGrp->srcList));
                             pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
                        {
                            pNextSrcLink =
                                TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);

                            if ((pSrc->u1StaticFlg == IGMP_TRUE) &&
                                (pSrc->u1EntryStatus != IGMP_ACTIVE))
                            {
                                continue;
                            }

                            if (pSrc->u1FwdState == IGMP_TRUE)
                            {
                                TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                                TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                             &pSrc->Link);
                            }
                        }

                        if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate)
                            > IGMP_ZERO)
                        {
                            /* Updating MRPs */
                            IgmpUpdateMrps (pGrp, IGMP_JOIN);
                        }
                    }
                }
                else
                {
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
            }

            i1Status = SNMP_SUCCESS;
            break;

        case IGMP_DESTROY:
            if (TMO_SLL_Count (&(pGrp->srcList)) == IGMP_ZERO)
            {
                if (pGrp->u1StaticFlg == IGMP_FALSE)
                {
                    i1Status = SNMP_SUCCESS;
                    break;
                }
                pGrp->u1StaticFlg = IGMP_FALSE;
                pTimer = &(pGrp->Timer);
                if (pTimer->u1TmrStatus == IGMP_TIMER_SET)
                {
                    i1Status = SNMP_SUCCESS;
                    break;
                }
                if ((pIfaceNode->u1Version == IGMP_VERSION_1) ||
                    (pIfaceNode->u1Version == IGMP_VERSION_2))
                {
                    pGrp->u1DelSyncFlag = IGMP_FALSE;
                    IgmpExpireGroup (pIfaceNode, pGrp,
                                     IGMP_VERSION_2, IGMP_TRUE, IGMP_TRUE);
                }

                else if (pIfaceNode->u1Version == MLD_VERSION_1)
                {
                    pGrp->u1DelSyncFlag = IGMP_FALSE;
                    IgmpExpireGroup (pIfaceNode, pGrp,
                                     MLD_VERSION_1, IGMP_TRUE, IGMP_TRUE);
                }
                if (((pIfaceNode->u1Version == IGMP_VERSION_3) ||
                     (pIfaceNode->u1Version == MLD_VERSION_2)))
                {
                    /* Convert the  Leave to TO_IN ({}) */
                    pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
                    IgmpGroupHandleToInclude (pIfaceNode, pGrp->u4Group,
                                              IGMP_ZERO, NULL,
                                              pIfaceNode->u4IfAddr);
                }

                i1Status = SNMP_SUCCESS;
            }
            else
            {
                for (pSrc = (tIgmpSource *)
                     TMO_SLL_First (&(pGrp->srcList));
                     pSrc != NULL; pSrc = (tIgmpSource *) pNextSrcLink)
                {
                    pNextSrcLink = TMO_SLL_Next (&(pGrp->srcList), &pSrc->Link);

                    if (pSrc->u1StaticFlg == IGMP_TRUE)
                    {
                        pSrc->u1StaticFlg = IGMP_FALSE;
                        /* If timer is running, dynamic record also available */
                        if (pSrc->u1DynamicFlg == IGMP_TRUE)
                        {
                            continue;
                        }

                        IgmpExpireSource (pSrc);
                    }
                }

                pGrp->u1StaticFlg = IGMP_FALSE;
                pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;

                if (TMO_SLL_Count ((&pGrp->srcList)) == IGMP_ZERO)
                {
                    if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        if (pGrp->u4GroupCompMode != IGMP_GCM_IGMPV3)
                        {
                            u1VerFlag = IGMP_VERSION_2;
                        }
                        else
                        {
                            u1VerFlag = IGMP_VERSION_3;
                        }
                    }
                    else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        if (pGrp->u4GroupCompMode != MLD_GCM_MLDV2)
                        {
                            u1VerFlag = MLD_VERSION_1;
                        }
                        else
                        {
                            u1VerFlag = MLD_VERSION_2;
                        }
                    }

                    u1VerFlag |= IGMP_V3REP_SEND_FLAG;
                    if (pGrp->u1DynamicFlg != IGMP_TRUE)
                    {
                        pGrp->u1DelSyncFlag = IGMP_FALSE;
                        IgmpExpireGroup (pIfaceNode, pGrp, u1VerFlag, IGMP_TRUE,
                                         IGMP_TRUE);
                    }
                }
            }
            i1Status = SNMP_SUCCESS;
            break;

        default:
            break;

    }                            /* switch - END */

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpCacheStatus SET routine Exit\n");
    return (i1Status);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  IgmpMgmtUtilNmhSetIgmpSrcListStatus
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress

                The Object 
                setValIgmpSrcListStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IgmpMgmtUtilNmhSetIgmpSrcListStatus (tIgmpIPvXAddr u4IgmpSrcListAddress,
                                     INT4 i4IgmpSrcListAddrType,
                                     INT4 i4IgmpSrcListIfIndex,
                                     tIgmpIPvXAddr u4IgmpSrcListHostAddress,
                                     INT4 i4SetValIgmpSrcListStatus)
{
    INT4                i4MapStatus = IGMP_FALSE;
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pCurrGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpIPvXAddr       HostAddrX;
    tIgmpSSMMapGrpEntry *pSSMMappedGrp = NULL;
    UINT1               u1NewGrpFlag = IGMP_FALSE;
    UINT1               u1GrpFound = IGMP_FALSE;
    UINT1               u1SrcFound = IGMP_FALSE;
    UINT1               u1DelSrcFlag = IGMP_FALSE;
    UINT4               u4Port = 0;
    UINT4               u4TmpSrcListAddress = IGMP_ZERO;
    UINT4               u4TmpAddr = IGMP_ZERO;
    UINT4               u4SrcAddr = IGMP_ZERO;
    UINT4               u4IPvXZero = IGMP_ZERO;
    tIp6Addr            SrcList6Addr;

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_ENTRY_MODULE,
                  IgmpGetModuleName (IGMP_ENTRY_MODULE),
                  "IgmpSrcListStatus SET routine Entry\n");

    if (IgmpGetPortFromIfIndex (i4IgmpSrcListIfIndex,
                                i4IgmpSrcListAddrType, &u4Port) == IGMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((IGMP_PROXY_STATUS () == IGMP_ENABLE) &&
        (IgpUtilCheckUpIface (u4Port) == IGMP_TRUE))
    {
        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface is configured as upstream interface\r\n");
        CLI_SET_ERR (CLI_IGMP_PROXY_UPSTREAM);
        return SNMP_FAILURE;
    }

    pIfaceNode = IgmpGetIfNode (u4Port, i4IgmpSrcListAddrType);

    if (pIfaceNode == NULL)
    {
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                   "Interface %d not present", i4IgmpSrcListIfIndex);
        CLI_SET_ERR (CLI_IGMP_INTERFACE_NOT_FOUND);
        return SNMP_FAILURE;
    }

    pCurrGrp = IgmpGrpLookUp (pIfaceNode, u4IgmpSrcListAddress);
    if (pCurrGrp != NULL)
    {
        u1GrpFound = IGMP_TRUE;
        pGrp = pCurrGrp;
        /* Only if no dynamic record exists clear the reporters */
        if (pGrp->u1DynamicFlg == IGMP_FALSE)
        {
            IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
        }
        TMO_SLL_Scan (&(pCurrGrp->srcList), pSrc, tIgmpSource *)
        {
            if (IGMP_IPVX_ADDR_COMPARE (u4IgmpSrcListHostAddress,
                                        pSrc->u4SrcAddress) == IGMP_ZERO)
            {
                u1SrcFound = IGMP_TRUE;
                break;
            }
        }
    }

    if (((i4SetValIgmpSrcListStatus == IGMP_DESTROY) ||
         (i4SetValIgmpSrcListStatus == IGMP_ACTIVE)) && (pSrc == NULL))
    {
        if (pSrc == NULL)
        {
            CLI_SET_ERR (CLI_IGMP_NO_GROUP_ERR);
        }
        return SNMP_FAILURE;
    }

    if (u1GrpFound == IGMP_FALSE)
    {
        /* Check for Group Address */
        if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            IGMP_IP_COPY_FROM_IPVX (&u4TmpSrcListAddress, u4IgmpSrcListAddress,
                                    IPVX_ADDR_FMLY_IPV4);
            if ((u4TmpSrcListAddress < IGMP_START_OF_MCAST_GRP_ADDR) ||
                (u4TmpSrcListAddress > IGMP_END_OF_MCAST_GRP_ADDR))
            {
                /* Invalid Group Destination Addres */
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                               "Invalid Group.!!!!!!!!!!!!\n");
                CLI_SET_ERR (CLI_IGMP_INVALID_GRP_ADDRESS);
                return SNMP_FAILURE;
            }
        }
        else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            IgmpFillMem (&SrcList6Addr, IGMP_ZERO, sizeof (tIp6Addr));
            IGMP_IP_COPY_FROM_IPVX (&SrcList6Addr, u4IgmpSrcListAddress,
                                    IPVX_ADDR_FMLY_IPV6);
            if (IS_ADDR_MULTI (SrcList6Addr) == FALSE)
            {
                /* Invalid Group Destination Addres */
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                               "Invalid Group.!!!!!!!!!!!!\n");
                CLI_SET_ERR (CLI_IGMP_INVALID_GRP_ADDRESS);
                return SNMP_FAILURE;
            }
        }

        pGrp = IgmpAddGroup (pIfaceNode, u4IgmpSrcListAddress, IGMP_TRUE);
        if (pGrp == NULL)
        {
            CLI_SET_ERR (CLI_IGMP_MAX_GROUPS);
            MGMD_DBG (IGMP_DBG_FAILURE, "Failure in creating the Group\n");
            return SNMP_FAILURE;
        }
        IGMP_IPVX_ADDR_CLEAR (&(pGrp->u4LastReporter));
        u1NewGrpFlag = IGMP_TRUE;
    }

    IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
    IGMP_IPVX_ADDR_INIT_IPV4 (gIPvXZero, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IPvXZero);

    switch (i4SetValIgmpSrcListStatus)
    {
        case IGMP_CREATE_AND_WAIT:
            /* FALL Through */
        case IGMP_CREATE_AND_GO:

            if ((u1GrpFound == IGMP_TRUE) && (u1SrcFound == IGMP_TRUE))
            {
                if (pGrp->u1DynamicFlg == IGMP_TRUE)
                {
                    pGrp->u1StaticFlg = IGMP_TRUE;
                    pSrc->u1StaticFlg = IGMP_TRUE;
                    pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_CONF;
                    if (pGrp->u1FilterMode != IGMP_FMODE_EXCLUDE)
                    {
                        pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
                    }
                    return SNMP_SUCCESS;
                }
            }

            if (pSrc == NULL)
            {
                if (i4IgmpSrcListAddrType == IGMP_IPVX_ADDR_FMLY_IPV4)
                {
                    /* check whether SSM mapping is configured for the group */
                    pSSMMappedGrp =
                        IgmpUtilChkIfSSMMappedGroup (u4IgmpSrcListAddress,
                                                     &i4MapStatus);
                }
                if (IGMP_IPVX_ADDR_COMPARE (u4IgmpSrcListHostAddress,
                                            gIPvXZero) == IGMP_ZERO)
                {
                    pGrp->u1FilterMode = IGMP_FMODE_EXCLUDE;
                    /* Add a 0.0.0.0 source node to indicate that the (*,G) is
                     * learnt via IGMPv3 interface */
                    pSrc =
                        IgmpAddSource (pGrp, gIPvXZero, gIPvXZero, IGMP_FALSE);
                    if (pSrc == NULL)
                    {
                        if (u1NewGrpFlag == IGMP_TRUE)
                        {
                            pGrp->u1StaticFlg = IGMP_FALSE;
                            pGrp->u1DelSyncFlag = IGMP_FALSE;
                            IgmpDeleteGroup (pIfaceNode, pGrp, IGMP_TRUE);
                        }
                        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES,
                                       IGMP_NAME,
                                       "Source node cannot be added \n");
                        return SNMP_FAILURE;
                    }

                    pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_CONF;

                    if (pGrp->u1DynamicFlg == IGMP_TRUE)
                    {
                        pGrp->u1StaticFlg = IGMP_TRUE;
                    }
                    pSrc->u1StaticFlg = IGMP_TRUE;

                    if (pSrc->u1FwdState == IGMP_FALSE)
                    {
                        pSrc->u1UpdatedToMrp = IGMP_FALSE;
                    }
                    pSrc->u1FwdState = IGMP_TRUE;

                    if (u1NewGrpFlag == IGMP_TRUE)
                    {
                        pGrp->u1EntryStatus = IGMP_ACTIVE;
                    }

                    /* Handle for static SSM Mapping */
                    if (i4MapStatus == IGMP_FALSE)
                    {
                        if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                        {
                            /* Call the PROXY routine to update the consolidate group
                             * membership data base for static group registration */
                            IgpGrpConsolidate (pGrp, IGMP_VERSION_3,
                                               IGMP_NEW_GROUP);

                            if (IgpUpIfFormAndSendV3Report (IGMP_ZERO)
                                == IGMP_FAILURE)
                            {
                                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,
                                               IGMP_NAME, "IGMP PROXY: Sending "
                                               " upstream report failed \r\n");
                            }
                        }

                        if (pGrp->u1EntryStatus == IGMP_ACTIVE)
                        {
                            if ((TMO_SLL_Count (&pGrp->srcList) == 0) ||
                                (IGMP_IPVX_ADDR_COMPARE (pSrc->u4SrcAddress,
                                                         gIPvXZero) ==
                                 IGMP_ZERO))
                            {
                                IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                            }
                        }

                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        if ((IgmpAddSSMMappedSourcesForStaticGroup
                             (pGrp, pSSMMappedGrp, pIfaceNode, NULL,
                              i4SetValIgmpSrcListStatus,
                              u1NewGrpFlag)) == IGMP_NOT_OK)
                        {
                            CLI_SET_ERR (CLI_IGMP_SSMMAP_STATIC_GRP_ASSOC);
                            return SNMP_FAILURE;
                        }
                        else
                        {
                            return SNMP_SUCCESS;
                        }
                    }
                }
/* todo: check this macro*/
                if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    IGMP_IP_COPY_FROM_IPVX (&u4SrcAddr,
                                            u4IgmpSrcListHostAddress,
                                            IPVX_ADDR_FMLY_IPV4);
                    IGMP_IPVX_ADDR_CLEAR (&HostAddrX);
                    u4TmpAddr = OSIX_HTONL (u4SrcAddr);
                    IGMP_IPVX_ADDR_INIT_IPV4 (HostAddrX,
                                              IGMP_IPVX_ADDR_FMLY_IPV4,
                                              (UINT1 *) &u4TmpAddr);

                }
                else if (pIfaceNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    IGMP_IPVX_ADDR_COPY (&HostAddrX, &u4IgmpSrcListHostAddress);
                }
                pSrc = IgmpAddSource (pGrp, HostAddrX, gIPvXZero, IGMP_TRUE);
                if (pSrc == NULL)
                {
                    if (u1NewGrpFlag == IGMP_TRUE)
                    {
                        pGrp->u1StaticFlg = IGMP_FALSE;
                        pGrp->u1DelSyncFlag = IGMP_FALSE;
                        IgmpDeleteGroup (pIfaceNode, pGrp, IGMP_TRUE);
                    }
                    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                                   "Source node cannot be added \n");
                    return SNMP_FAILURE;
                }
                pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_CONF;
                if (pGrp->u1DynamicFlg == IGMP_TRUE)
                {
                    pGrp->u1StaticFlg = IGMP_TRUE;
                }
                pSrc->u1StaticFlg = IGMP_TRUE;

                if (pSrc->u1FwdState == IGMP_FALSE)
                {
                    pSrc->u1UpdatedToMrp = IGMP_FALSE;
                }

                pSrc->u1FwdState = IGMP_TRUE;
                /* If the group is already learned in EXCLUDE filter mode
                   then retain the EXCLUDE mode even if INCLUDE for the same group is received */
                if (pGrp->u1FilterMode != IGMP_FMODE_EXCLUDE)
                {
                    pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
                }

                if (u1NewGrpFlag == IGMP_TRUE)
                {
                    pGrp->u1EntryStatus = IGMP_ACTIVE;
                }
            }
            else if (pSrc->u1SrcSSMMapped & IGMP_SRC_MAPPED)
            {
                pSrc->u1SrcSSMMapped = pSrc->u1SrcSSMMapped | IGMP_SRC_CONF;
            }
            else
            {
                IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "Static Source %s already present",
                           IgmpPrintIPvxAddress (u4IgmpSrcListHostAddress));
                return SNMP_FAILURE;
            }

            if (i4SetValIgmpSrcListStatus == IGMP_CREATE_AND_GO)
            {
                pSrc->u1EntryStatus = IGMP_ACTIVE;
                if ((gIgmpConfig.u1IgmpStatus == IGMP_DISABLE) &&
                    (i4IgmpSrcListAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    return SNMP_SUCCESS;
                }
                else if ((gIgmpConfig.u1MldStatus == MLD_DISABLE) &&
                         (i4IgmpSrcListAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    return SNMP_SUCCESS;
                }

                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    /* Call the PROXY routine to update the consolidate group
                     * membership data base for static group registration */
                    IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_NEW_GROUP);

                    if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
                    {
                        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE,
                                       IGMP_NAME, "IGMP PROXY: Sending "
                                       " upstream report failed \r\n");
                    }

                    return SNMP_SUCCESS;
                }

                if (pGrp->u1EntryStatus == IGMP_ACTIVE)
                {
                    if (TMO_SLL_Count (&pGrp->srcList) == 0)
                    {
                        IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                    }
                    else
                    {
                        if (pGrp->pIfNode->u1OperStatus == IGMP_IFACE_OPER_UP)
                        {
                            TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                            TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate),
                                         &pSrc->Link);

                            IgmpUpdateMrps (pGrp, IGMP_JOIN);
                        }
                    }
                }
            }
            else
            {
                pSrc->u1EntryStatus = IGMP_NOT_READY;
            }

            i1Status = SNMP_SUCCESS;
            break;

        case IGMP_ACTIVE:
            if (pSrc != NULL)
            {
                pSrc->u1EntryStatus = IGMP_ACTIVE;
                if (pGrp->u1EntryStatus != IGMP_ACTIVE)
                {
                    return SNMP_SUCCESS;
                }
                if ((gIgmpConfig.u1IgmpStatus == IGMP_DISABLE) &&
                    (i4IgmpSrcListAddrType == IPVX_ADDR_FMLY_IPV4))
                {
                    return SNMP_SUCCESS;
                }
                else if ((gIgmpConfig.u1MldStatus == MLD_DISABLE) &&
                         (i4IgmpSrcListAddrType == IPVX_ADDR_FMLY_IPV6))
                {
                    return SNMP_SUCCESS;
                }

                if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
                {
                    /* Call the PROXY routine to update the consolidate group
                     * membership data base for static group registration */
                    IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_NEW_GROUP);

                    if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
                    {
                        IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE,
                                       IGMP_NAME, "IGMP PROXY: Sending "
                                       " upstream report failed \r\n");
                    }

                    return SNMP_SUCCESS;
                }

                if (TMO_SLL_Count (&pGrp->srcList) == 0)
                {
                    IgmpUpdateMrps (pGrp, IGMP_EXCLUDE);
                }
                else
                {
                    if (pGrp->pIfNode->u1OperStatus == IGMP_IFACE_OPER_UP)
                    {
                        TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
                        TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);

                        IgmpUpdateMrps (pGrp, IGMP_JOIN);
                    }
                }
            }

            i1Status = SNMP_SUCCESS;
            break;

        case IGMP_DESTROY:

            pSSMMappedGrp =
                IgmpUtilChkIfSSMMapExists (pGrp->u4Group, &i4MapStatus);
            if (pSSMMappedGrp != NULL)
            {
                if (IgmpGrpLookUpInSSMMapTable
                    (pSSMMappedGrp->StartGrpAddr,
                     pSSMMappedGrp->EndGrpAddr, pSrc->u4SrcAddress) == NULL)
                {
                    u1DelSrcFlag = IGMP_TRUE;
                }
            }
            else
            {
                u1DelSrcFlag = IGMP_TRUE;
            }
            pSrc->u1SrcSSMMapped =
                (UINT1) (pSrc->u1SrcSSMMapped & (~IGMP_SRC_CONF));
            if ((pGrp->u1FilterMode == IGMP_FMODE_INCLUDE)
                || ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE)
                    && (u1DelSrcFlag == IGMP_TRUE)))
            {
                pSrc->u1StaticFlg = IGMP_FALSE;
                if (pSrc->u1DynamicFlg == IGMP_FALSE)
                {
                    if ((pSrc->srcTimer).u1TmrStatus == IGMP_TIMER_RESET)
                    {
                        pSrc->u1FwdState = IGMP_FALSE;
                    }
                    pSrc->u1DelSyncFlag = IGMP_FALSE;
                    IgmpExpireSource (pSrc);
                }
            }
            if (IGMP_IPVX_ADDR_COMPARE (u4IgmpSrcListHostAddress,
                                        gIPvXZero) == IGMP_ZERO)
            {
                pGrp->u1FilterMode = IGMP_FMODE_INCLUDE;
            }
            if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
            {
                /* Call the PROXY routine to update the consolidate group
                 * membership data base for static group registration */
                IgpGrpConsolidate (pGrp, IGMP_VERSION_3, IGMP_NOT_A_NEW_GROUP);

                if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
                {
                    IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                                   "IGMP PROXY: Sending upstream report failed \r\n");
                }
            }

            pGrp->u1StaticFlg = IGMP_FALSE;

            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
            while (pSrc != NULL)
            {
                pNextSrc = (tIgmpSource *) TMO_SLL_Next
                    (&pGrp->srcList, &pSrc->Link);

                if (pSrc->u1StaticFlg == IGMP_TRUE)
                {
                    if ((IGMP_IPVX_ADDR_COMPARE (u4IgmpSrcListHostAddress,
                                                 gIPvXZero) == IGMP_ZERO) &&
                        (pSrc->u1SrcSSMMapped & IGMP_SRC_MAPPED))
                    {
                        if (pSrc->u1SrcSSMMapped & IGMP_SRC_CONF)
                        {
                            pGrp->u1StaticFlg = IGMP_TRUE;
                            pSrc = pNextSrc;
                            continue;
                        }

                        if (pSrc->u1DynamicFlg == IGMP_FALSE)
                        {
                            pSrc->u1DelSyncFlag = IGMP_FALSE;
                            IgmpExpireSource (pSrc);
                        }
                        else
                        {
                            pSrc->u1StaticFlg = IGMP_FALSE;
                        }
                    }
                    else if (pSrc->u1SrcSSMMapped & IGMP_SRC_CONF)
                    {
                        pGrp->u1StaticFlg = IGMP_TRUE;
                    }
                }
                pSrc = pNextSrc;
            }

            if (TMO_SLL_Count ((&pGrp->srcList)) == IGMP_ZERO)
            {
                pGrp->u1DelSyncFlag = IGMP_FALSE;
                IgmpDeleteGroup (pIfaceNode, pGrp, IGMP_TRUE);
            }

            i1Status = SNMP_SUCCESS;
            break;
        default:
            break;

    }                            /* switch - END */

    MGMD_DBG_EXT (MGMD_DBG_FLAG, IGMP_EXIT_MODULE,
                  IgmpGetModuleName (IGMP_EXIT_MODULE),
                  "IgmpsrcListStatus SET routine Exit\n");
    return (i1Status);

}
