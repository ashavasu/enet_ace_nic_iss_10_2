/******************************************************************************/
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsigmset.c,v 1.14 2016/06/24 09:42:19 siva Exp $
 *
 * Description: : Action routines for set/get objects in           
 *                           fsigmp.mib                           
 *
 *******************************************************************/

# include  "include.h"
# include  "fsigmcon.h"
# include  "fsigmogi.h"
# include  "midconst.h"
# include  "igmpinc.h"
# include  "fsmgmdcli.h"
# include  "fsigmpcli.h"

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIgmpGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsIgmpGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpGlobalStatus (INT4 i4SetValFsIgmpGlobalStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpGlobalStatus (i4SetValFsIgmpGlobalStatus,
                                              IPVX_ADDR_FMLY_IPV4);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsScalars (FsMgmdIgmpGlobalStatus,
                                    sizeof (FsMgmdIgmpGlobalStatus) /
                                    sizeof (UINT4), i4SetValFsIgmpGlobalStatus);

    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpTraceLevel
 Input       :  The Indices

                The Object 
                setValFsIgmpTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpTraceLevel (INT4 i4SetValFsIgmpTraceLevel)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpTraceLevel (i4SetValFsIgmpTraceLevel,
                                            IPVX_ADDR_FMLY_IPV4);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsScalars (FsMgmdIgmpTraceLevel,
                                    sizeof (FsMgmdIgmpTraceLevel) /
                                    sizeof (UINT4), i4SetValFsIgmpTraceLevel);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpDebugLevel
 Input       :  The Indices

                The Object
                setValFsIgmpDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpDebugLevel (INT4 i4SetValFsIgmpDebugLevel)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpDebugLevel (i4SetValFsIgmpDebugLevel,
                                            IPVX_ADDR_FMLY_IPV4);

   if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsScalars (FsMgmdIgmpDebugLevel,
                                    sizeof (FsMgmdIgmpDebugLevel) /
                                    sizeof (UINT4), i4SetValFsIgmpDebugLevel);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpInterfaceAdminStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpInterfaceAdminStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4SetValFsIgmpInterfaceAdminStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceAdminStatus (i4FsIgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      i4SetValFsIgmpInterfaceAdminStatus);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsIgmpTbl (FsMgmdInterfaceAdminStatus,
                                    sizeof (FsMgmdInterfaceAdminStatus) /
                                    sizeof (UINT4), i4FsIgmpInterfaceIfIndex,
                                    i4SetValFsIgmpInterfaceAdminStatus);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpInterfaceFastLeaveStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceFastLeaveStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpInterfaceFastLeaveStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                      INT4
                                      i4SetValFsIgmpInterfaceFastLeaveStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceFastLeaveStatus
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         i4SetValFsIgmpInterfaceFastLeaveStatus);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsIgmpTbl (FsMgmdInterfaceFastLeaveStatus,
                                    sizeof (FsMgmdInterfaceFastLeaveStatus) /
                                    sizeof (UINT4), i4FsIgmpInterfaceIfIndex,
                                    i4SetValFsIgmpInterfaceFastLeaveStatus);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpInterfaceGroupListId
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceGroupListId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpInterfaceGroupListId (INT4 i4FsIgmpInterfaceIfIndex,
                                  UINT4 u4SetValFsIgmpInterfaceGroupListId)
{
    INT1                i1Status = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;
    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceGroupListId
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         u4SetValFsIgmpInterfaceGroupListId);
    if (i1Status == SNMP_SUCCESS)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMgmdInterfaceGroupListId,
                              u4SeqNum, FALSE, IgmpLock, IgmpUnLock, 2,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u", i4FsIgmpInterfaceIfIndex,
                          IPVX_ADDR_FMLY_IPV4,
                          u4SetValFsIgmpInterfaceGroupListId));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpInterfaceChannelTrackStatus
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceChannelTrackStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpInterfaceChannelTrackStatus (INT4 i4FsIgmpInterfaceIfIndex,
                                         INT4
                                         i4SetValFsIgmpInterfaceChannelTrackStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceChannelTrackStatus
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         i4SetValFsIgmpInterfaceChannelTrackStatus);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsIgmpTbl (FsMgmdInterfaceChannelTrackStatus,
                                    sizeof (FsMgmdInterfaceChannelTrackStatus) /
                                    sizeof (UINT4), i4FsIgmpInterfaceIfIndex,
                                    i4SetValFsIgmpInterfaceChannelTrackStatus);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpInterfaceLimit
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpInterfaceLimit (INT4 i4FsIgmpInterfaceIfIndex,
                            UINT4 u4SetValFsIgmpInterfaceLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;
    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceLimit
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         u4SetValFsIgmpInterfaceLimit);
    if (i1Status == SNMP_SUCCESS)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMgmdInterfaceLimit, u4SeqNum,
                              FALSE, IgmpLock, IgmpUnLock, 2, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %u", i4FsIgmpInterfaceIfIndex,
                          IPVX_ADDR_FMLY_IPV4, u4SetValFsIgmpInterfaceLimit));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpJoinPktRate
 Input       :  The Indices

                The Object 
                setValFsIgmpJoinPktRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpJoinPktRate (INT4 i4SetValFsIgmpJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4FsIgmpInterfaceIfIndex = IGMP_ZERO;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpJoinPktRate
        (IPVX_ADDR_FMLY_IPV4,
         i4SetValFsIgmpJoinPktRate);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsIgmpTbl (FsMgmdInterfaceJoinPktRate,
                                    sizeof (FsMgmdInterfaceJoinPktRate) /
                                    sizeof (UINT4), i4FsIgmpInterfaceIfIndex,
                                    i4SetValFsIgmpJoinPktRate);
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpInterfaceJoinPktRate
 Input       :  The Indices
                FsIgmpInterfaceIfIndex

                The Object 
                setValFsIgmpInterfaceJoinPktRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpInterfaceJoinPktRate (INT4 i4FsIgmpInterfaceIfIndex,
                                  INT4 i4SetValFsIgmpInterfaceJoinPktRate)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpInterfaceJoinPktRate
        (i4FsIgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         i4SetValFsIgmpInterfaceJoinPktRate);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsIgmpTbl (FsMgmdInterfaceJoinPktRate,
                                    sizeof (FsMgmdInterfaceJoinPktRate) /
                                    sizeof (UINT4), i4FsIgmpInterfaceIfIndex,
                                    i4SetValFsIgmpInterfaceJoinPktRate);
    }
    return i1Status;
}


/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIgmpGrpListRowStatus
 Input       :  The Indices
                FsIgmpGrpListId
                FsIgmpGrpIP
                FsIgmpGrpPrefixLen

                The Object 
                setValFsIgmpGrpListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpGrpListRowStatus (UINT4 u4FsIgmpGrpListId,
                              UINT4 u4FsIgmpGrpIP,
                              UINT4 u4FsIgmpGrpPrefixLen,
                              INT4 i4SetValFsIgmpGrpListRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;
    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpGrpListRowStatus (u4FsIgmpGrpListId,
                                                  u4FsIgmpGrpIP,
                                                  u4FsIgmpGrpPrefixLen,
                                                  IPVX_ADDR_FMLY_IPV4,
                                                  i4SetValFsIgmpGrpListRowStatus);
    if (i1Status == SNMP_SUCCESS)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsIgmpGrpListRowStatus, u4SeqNum,
                              TRUE, IgmpLock, IgmpUnLock, 3, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %p %p %i", u4FsIgmpGrpListId,
                          u4FsIgmpGrpIP, u4FsIgmpGrpPrefixLen,
                          i4SetValFsIgmpGrpListRowStatus));
    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpGlobalLimit
 Input       :  The Indices

                The Object 
                setValFsIgmpGlobalLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpGlobalLimit (UINT4 u4SetValFsIgmpGlobalLimit)
{
    INT1                i1Status = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = IGMP_ZERO;
    MEMSET (&SnmpNotifyInfo, IGMP_ZERO, sizeof (tSnmpNotifyInfo));
    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpGlobalLimit
        (IPVX_ADDR_FMLY_IPV4, u4SetValFsIgmpGlobalLimit);
    if (i1Status == SNMP_SUCCESS)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMgmdGlobalLimit, u4SeqNum,
                              FALSE, IgmpLock, IgmpUnLock, 0, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u ", u4SetValFsIgmpGlobalLimit));

    }
    return i1Status;
}

/****************************************************************************
 Function    :  nmhSetFsIgmpSSMMapStatus
 Input       :  The Indices

                The Object 
                setValFsIgmpSSMMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsIgmpSSMMapStatus(INT4 i4SetValFsIgmpSSMMapStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpSSMMapStatus 
                (i4SetValFsIgmpSSMMapStatus, IPVX_ADDR_FMLY_IPV4);
    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForFsScalars (FsMgmdSSMMapStatus,
                                    sizeof (FsMgmdSSMMapStatus) /
                                    sizeof (UINT4), i4SetValFsIgmpSSMMapStatus);

    }
    return i1Status;
}


/****************************************************************************
 Function    :  nmhSetFsIgmpSSMMapRowStatus
 Input       :  The Indices
                FsIgmpSSMMapStartGrpAddress
                FsIgmpSSMMapEndGrpAddress
                FsIgmpSSMMapSourceAddress

                The Object 
                setValFsIgmpSSMMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsIgmpSSMMapRowStatus(UINT4 u4FsIgmpSSMMapStartGrpAddress , 
                            UINT4 u4FsIgmpSSMMapEndGrpAddress , 
                            UINT4 u4FsIgmpSSMMapSourceAddress , 
                            INT4 i4SetValFsIgmpSSMMapRowStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhSetFsIgmpSSMMapRowStatus
            (u4FsIgmpSSMMapStartGrpAddress, u4FsIgmpSSMMapEndGrpAddress,
            u4FsIgmpSSMMapSourceAddress, i4SetValFsIgmpSSMMapRowStatus);

    if (i1Status == SNMP_SUCCESS)
    {
        IgmpUtilIncMsrForIgmpSSMMapTbl (FsMgmdSSMMapRowStatus,
                                    sizeof (FsMgmdSSMMapRowStatus) /
                                    sizeof (UINT4), u4FsIgmpSSMMapStartGrpAddress,
                                    u4FsIgmpSSMMapEndGrpAddress,
                                    u4FsIgmpSSMMapSourceAddress,
                                    i4SetValFsIgmpSSMMapRowStatus, IGMP_TRUE);
    }
    return i1Status;
}

