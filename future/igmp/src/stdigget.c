/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdigget.c,v 1.14 2017/02/06 10:45:28 siva Exp $
 *
 * Description:This file contains Low level routines  for stdigmp.mib  
 *
 *******************************************************************/

#include "include.h"
#include "stdigcon.h"
#include "stdigogi.h"
#include "midconst.h"
#include "igmpinc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceIgmpInterfaceTable (INT4 i4IgmpInterfaceIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpInterfaceTable
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIgmpInterfaceTable (INT4 *pi4IgmpInterfaceIfIndex)
{
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpInterfaceTable (pi4IgmpInterfaceIfIndex,
                                                        &i4AddrType);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIgmpInterfaceTable
 Input       :  The Indices
                IgmpInterfaceIfIndex
                nextIgmpInterfaceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIgmpInterfaceTable (INT4 i4IgmpInterfaceIfIndex,
                                   INT4 *pi4NextIgmpInterfaceIfIndex)
{
    INT4                i4IgmpInterfaceAddrType = IGMP_ZERO;
    INT4                i4CurAddrType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4NextIndex = IGMP_ZERO;
    INT1                i1Status = SNMP_FAILURE;
    UINT1               u1IsTrue = 1;

    while (u1IsTrue)
    {
        i1Status =
            IgmpMgmtUtilNmhGetNextIndexIgmpInterfaceTable
            (i4IgmpInterfaceIfIndex, &i4NextIndex, i4CurAddrType,
             &i4IgmpInterfaceAddrType);

        if (i1Status == SNMP_FAILURE
            || i4IgmpInterfaceAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            break;
        }
        i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
        i4IgmpInterfaceIfIndex = i4NextIndex;
    }
    *pi4NextIgmpInterfaceIfIndex = i4NextIndex;
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceQueryInterval
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceQueryInterval (INT4 i4IgmpInterfaceIfIndex,
                                  UINT4 *pu4RetValIgmpInterfaceQueryInterval)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQueryInterval (i4IgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pu4RetValIgmpInterfaceQueryInterval);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpinterfaceStatus
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceStatus (INT4 i4IgmpInterfaceIfIndex,
                           INT4 *pi4RetValIgmpInterfaceStatus)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceStatus (i4IgmpInterfaceIfIndex,
                                               IPVX_ADDR_FMLY_IPV4,
                                               pi4RetValIgmpInterfaceStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceVersion
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceVersion (INT4 i4IgmpInterfaceIfIndex,
                            UINT4 *pu4RetValIgmpInterfaceVersion)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceVersion (i4IgmpInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                pu4RetValIgmpInterfaceVersion);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceQuerier
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQuerier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceQuerier (INT4 i4IgmpInterfaceIfIndex,
                            UINT4 *pu4RetValIgmpInterfaceQuerier)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpQuerierAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpQuerierAddrX);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerier (i4IgmpInterfaceIfIndex,
                                                IPVX_ADDR_FMLY_IPV4,
                                                &IgmpQuerierAddrX);

    IGMP_IP_COPY_FROM_IPVX (pu4RetValIgmpInterfaceQuerier, IgmpQuerierAddrX,
                            IPVX_ADDR_FMLY_IPV4);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceQueryMaxResponseTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQueryMaxResponseTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceQueryMaxResponseTime (INT4 i4IgmpInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValIgmpInterfaceQueryMaxResponseTime)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQueryMaxResponseTime
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValIgmpInterfaceQueryMaxResponseTime);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceQuerierUpTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQuerierUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceQuerierUpTime (INT4 i4IgmpInterfaceIfIndex,
                                  UINT4 *pu4RetValIgmpInterfaceQuerierUpTime)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerierUpTime (i4IgmpInterfaceIfIndex,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      pu4RetValIgmpInterfaceQuerierUpTime);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceQuerierExpiryTime
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceQuerierExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceQuerierExpiryTime (INT4 i4IgmpInterfaceIfIndex,
                                      UINT4
                                      *pu4RetValIgmpInterfaceQuerierExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceQuerierExpiryTime
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValIgmpInterfaceQuerierExpiryTime);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceVersion1QuerierTimer
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceVersion1QuerierTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceVersion1QuerierTimer (INT4 i4IgmpInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValIgmpInterfaceVersion1QuerierTimer)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceVersion1QuerierTimer
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValIgmpInterfaceVersion1QuerierTimer);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceWrongVersionQueries
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceWrongVersionQueries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceWrongVersionQueries (INT4 i4IgmpInterfaceIfIndex,
                                        UINT4
                                        *pu4RetValIgmpInterfaceWrongVersionQueries)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceWrongVersionQueries
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValIgmpInterfaceWrongVersionQueries);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceJoins
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceJoins
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceJoins (INT4 i4IgmpInterfaceIfIndex,
                          UINT4 *pu4RetValIgmpInterfaceJoins)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceJoins (i4IgmpInterfaceIfIndex,
                                              IPVX_ADDR_FMLY_IPV4,
                                              pu4RetValIgmpInterfaceJoins);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceProxyIfIndex
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceProxyIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceProxyIfIndex (INT4 i4IgmpInterfaceIfIndex,
                                 INT4 *pi4RetValIgmpInterfaceProxyIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceProxyIfIndex (i4IgmpInterfaceIfIndex,
                                                     IPVX_ADDR_FMLY_IPV4,
                                                     pi4RetValIgmpInterfaceProxyIfIndex);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceGroups
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceGroups
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceGroups (INT4 i4IgmpInterfaceIfIndex,
                           UINT4 *pu4RetValIgmpInterfaceGroups)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceGroups (i4IgmpInterfaceIfIndex,
                                               IPVX_ADDR_FMLY_IPV4,
                                               pu4RetValIgmpInterfaceGroups);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceRobustness
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceRobustness
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceRobustness (INT4 i4IgmpInterfaceIfIndex,
                               UINT4 *pu4RetValIgmpInterfaceRobustness)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceRobustness (i4IgmpInterfaceIfIndex,
                                                   IPVX_ADDR_FMLY_IPV4,
                                                   pu4RetValIgmpInterfaceRobustness);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceLastMembQueryIntvl
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceLastMembQueryIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceLastMembQueryIntvl (INT4 i4IgmpInterfaceIfIndex,
                                       UINT4
                                       *pu4RetValIgmpInterfaceLastMembQueryIntvl)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceLastMembQueryIntvl
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValIgmpInterfaceLastMembQueryIntvl);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpInterfaceVersion2QuerierTimer
 Input       :  The Indices
                IgmpInterfaceIfIndex

                The Object 
                retValIgmpInterfaceVersion2QuerierTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpInterfaceVersion2QuerierTimer (INT4 i4IgmpInterfaceIfIndex,
                                         UINT4
                                         *pu4RetValIgmpInterfaceVersion2QuerierTimer)
{
    INT1                i1Status = SNMP_FAILURE;

    i1Status =
        IgmpMgmtUtilNmhGetIgmpInterfaceVersion2QuerierTimer
        (i4IgmpInterfaceIfIndex, IPVX_ADDR_FMLY_IPV4,
         pu4RetValIgmpInterfaceVersion2QuerierTimer);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIgmpCacheTable (UINT4 u4IgmpCacheAddress,
                                        INT4 i4IgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpCacheTable (IPVX_ADDR_FMLY_IPV4,
                                                            IgmpCacheAddrX,
                                                            i4IgmpCacheIfIndex);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexIgmpCacheTable (UINT4 *pu4IgmpCacheAddress,
                                INT4 *pi4IgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);

    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpCacheTable (&i4AddrType,
                                                    &IgmpCacheAddrX,
                                                    pi4IgmpCacheIfIndex);

    if ((i4AddrType == IPVX_ADDR_FMLY_IPV4) && (i1Status == SNMP_SUCCESS))
    {
        IGMP_IP_COPY_FROM_IPVX (pu4IgmpCacheAddress, IgmpCacheAddrX,
                                IPVX_ADDR_FMLY_IPV4);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIgmpCacheTable
 Input       :  The Indices
                IgmpCacheAddress
                nextIgmpCacheAddress
                IgmpCacheIfIndex
                nextIgmpCacheIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexIgmpCacheTable (UINT4 u4IgmpCacheAddress,
                               UINT4 *pu4NextIgmpCacheAddress,
                               INT4 i4IgmpCacheIfIndex,
                               INT4 *pi4NextIgmpCacheIfIndex)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4CurAddrType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4IgmpCacheAddrType = IGMP_ZERO;
    tIgmpIPvXAddr       IgmpCacheAddrX;
    tIgmpIPvXAddr       NextIgmpCacheAddrX;
    UINT1               u1IsTrue = 1;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_CLEAR (&NextIgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    while (u1IsTrue)
    {
    i1Status =
            IgmpMgmtUtilNmhGetNextIndexIgmpCacheTable (i4CurAddrType,
                                                   &i4IgmpCacheAddrType,
                                                   IgmpCacheAddrX,
                                                   &NextIgmpCacheAddrX,
                                                   i4IgmpCacheIfIndex,
                                                   pi4NextIgmpCacheIfIndex);

        if (i1Status == SNMP_FAILURE
                || i4IgmpCacheAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            break;
        }
        i4CurAddrType = IPVX_ADDR_FMLY_IPV6;
        i4IgmpCacheIfIndex = *pi4NextIgmpCacheIfIndex;
        IGMP_IPVX_ADDR_COPY (&IgmpCacheAddrX, &NextIgmpCacheAddrX);
    }
    IGMP_IP_COPY_FROM_IPVX (pu4NextIgmpCacheAddress, NextIgmpCacheAddrX,
                            IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIgmpCacheSelf
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheSelf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheSelf (UINT4 u4IgmpCacheAddress, INT4 i4IgmpCacheIfIndex,
                     INT4 *pi4RetValIgmpCacheSelf)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheSelf (IPVX_ADDR_FMLY_IPV4, IgmpCacheAddrX,
                                         i4IgmpCacheIfIndex,
                                         pi4RetValIgmpCacheSelf);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpCacheLastReporter
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheLastReporter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheLastReporter (UINT4 u4IgmpCacheAddress,
                             INT4 i4IgmpCacheIfIndex,
                             UINT4 *pu4RetValIgmpCacheLastReporter)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;
    tIgmpIPvXAddr       LastReporterX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_CLEAR (&LastReporterX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheLastReporter (IPVX_ADDR_FMLY_IPV4,
                                                 IgmpCacheAddrX,
                                                 i4IgmpCacheIfIndex,
                                                 &LastReporterX);

    IGMP_IP_COPY_FROM_IPVX (pu4RetValIgmpCacheLastReporter, LastReporterX,
                            IPVX_ADDR_FMLY_IPV4);
    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpCacheUpTime
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheUpTime (UINT4 u4IgmpCacheAddress,
                       INT4 i4IgmpCacheIfIndex, UINT4 *pu4RetValIgmpCacheUpTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheUpTime (IPVX_ADDR_FMLY_IPV4, IgmpCacheAddrX,
                                           i4IgmpCacheIfIndex,
                                           pu4RetValIgmpCacheUpTime);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpCacheExpiryTime
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheExpiryTime (UINT4 u4IgmpCacheAddress,
                           INT4 i4IgmpCacheIfIndex,
                           UINT4 *pu4RetValIgmpCacheExpiryTime)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheExpiryTime (IPVX_ADDR_FMLY_IPV4,
                                               IgmpCacheAddrX,
                                               i4IgmpCacheIfIndex,
                                               pu4RetValIgmpCacheExpiryTime);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpCacheStatus
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheStatus (UINT4 u4IgmpCacheAddress,
                       INT4 i4IgmpCacheIfIndex, INT4 *pi4RetValIgmpCacheStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheStatus (IPVX_ADDR_FMLY_IPV4, IgmpCacheAddrX,
                                           i4IgmpCacheIfIndex,
                                           pi4RetValIgmpCacheStatus);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpCacheVersion1HostTimer
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheVersion1HostTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheVersion1HostTimer (UINT4 u4IgmpCacheAddress,
                                  INT4 i4IgmpCacheIfIndex,
                                  UINT4 *pu4RetValIgmpCacheVersion1HostTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheVersion1HostTimer (IPVX_ADDR_FMLY_IPV4,
                                                      IgmpCacheAddrX,
                                                      i4IgmpCacheIfIndex,
                                                      pu4RetValIgmpCacheVersion1HostTimer);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpCacheVersion2HostTimer
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheVersion2HostTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheVersion2HostTimer (UINT4 u4IgmpCacheAddress,
                                  INT4 i4IgmpCacheIfIndex,
                                  UINT4 *pu4RetValIgmpCacheVersion2HostTimer)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheVersion2HostTimer (IPVX_ADDR_FMLY_IPV4,
                                                      IgmpCacheAddrX,
                                                      i4IgmpCacheIfIndex,
                                                      pu4RetValIgmpCacheVersion2HostTimer);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetIgmpCacheSourceFilterMode
 Input       :  The Indices
                IgmpCacheAddress
                IgmpCacheIfIndex

                The Object 
                retValIgmpCacheSourceFilterMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpCacheSourceFilterMode (UINT4 u4IgmpCacheAddress,
                                 INT4 i4IgmpCacheIfIndex,
                                 INT4 *pi4RetValIgmpCacheSourceFilterMode)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       IgmpCacheAddrX;

    IGMP_IPVX_ADDR_CLEAR (&IgmpCacheAddrX);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgmpCacheAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpCacheAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpCacheSourceFilterMode (IPVX_ADDR_FMLY_IPV4,
                                                     IgmpCacheAddrX,
                                                     i4IgmpCacheIfIndex,
                                                     pi4RetValIgmpCacheSourceFilterMode);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIgmpSrcListTable (UINT4 u4IgmpSrcListAddress,
                                          INT4 i4IgmpSrcListIfIndex,
                                          UINT4 u4IgmpSrcListHostAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddrX;
    tIgmpIPvXAddr       HostAddrX;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddrX);
    IGMP_IPVX_ADDR_CLEAR (&HostAddrX);

    IGMP_IPVX_ADDR_INIT_IPV4 (SrcListAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (HostAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListHostAddress);

    i1Status =
        IgmpMgmtUtilNmhValidateIndexInstanceIgmpSrcListTable
        (IPVX_ADDR_FMLY_IPV4, SrcListAddrX, i4IgmpSrcListIfIndex, HostAddrX);

    return i1Status;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIgmpSrcListTable (UINT4 *pu4IgmpSrcListAddress,
                                  INT4 *pi4IgmpSrcListIfIndex,
                                  UINT4 *pu4IgmpSrcListHostAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4IgmpSrcAddrType = IGMP_ZERO;

    tIgmpIPvXAddr       SrcListAddrX;
    tIgmpIPvXAddr       HostAddrX;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddrX);
    IGMP_IPVX_ADDR_CLEAR (&HostAddrX);


    i1Status =
        IgmpMgmtUtilNmhGetFirstIndexIgmpSrcListTable (&i4IgmpSrcAddrType,
                                                      &SrcListAddrX,
                                                      pi4IgmpSrcListIfIndex,
                                                      &HostAddrX);
    if ((i4IgmpSrcAddrType == IPVX_ADDR_FMLY_IPV4) &&
        (i1Status == SNMP_SUCCESS))
    {
        IGMP_IP_COPY_FROM_IPVX (pu4IgmpSrcListAddress, SrcListAddrX,
                                IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (pu4IgmpSrcListHostAddress, HostAddrX,
                                IPVX_ADDR_FMLY_IPV4);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIgmpSrcListTable
 Input       :  The Indices
                IgmpSrcListAddress
                nextIgmpSrcListAddress
                IgmpSrcListIfIndex
                nextIgmpSrcListIfIndex
                IgmpSrcListHostAddress
                nextIgmpSrcListHostAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIgmpSrcListTable (UINT4 u4IgmpSrcListAddress,
                                 UINT4 *pu4NextIgmpSrcListAddress,
                                 INT4 i4IgmpSrcListIfIndex,
                                 INT4 *pi4NextIgmpSrcListIfIndex,
                                 UINT4 u4IgmpSrcListHostAddress,
                                 UINT4 *pu4NextIgmpSrcListHostAddress)
{
    INT1                i1Status = SNMP_FAILURE;
    INT4                i4SrcAddrType = IPVX_ADDR_FMLY_IPV4;
    INT4                i4NextSrcAddrType = IGMP_ZERO;
    tIgmpIPvXAddr       SrcListAddrX;
    tIgmpIPvXAddr       HostAddrX;
    tIgmpIPvXAddr       NextSrcListAddrX;
    tIgmpIPvXAddr       NextHostAddrX;
    UINT1               u1IsTrue = IGMP_TRUE;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddrX);
    IGMP_IPVX_ADDR_CLEAR (&NextSrcListAddrX);
    IGMP_IPVX_ADDR_CLEAR (&HostAddrX);
    IGMP_IPVX_ADDR_CLEAR (&NextHostAddrX);
     
                            
    IGMP_IPVX_ADDR_INIT_IPV4 (SrcListAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (HostAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListHostAddress);
    while (u1IsTrue)
    {
	    i1Status =
		    IgmpMgmtUtilNmhGetNextIndexIgmpSrcListTable (i4SrcAddrType,
				    &i4NextSrcAddrType,
				    SrcListAddrX,
				    &NextSrcListAddrX,
				    i4IgmpSrcListIfIndex,
				    pi4NextIgmpSrcListIfIndex,
				    HostAddrX, &NextHostAddrX);
	    if (i1Status == SNMP_FAILURE  || i4NextSrcAddrType == IPVX_ADDR_FMLY_IPV4)
	    {
		    break;
	    }
	    i4SrcAddrType = i4NextSrcAddrType;
	    i4NextSrcAddrType = IPVX_ADDR_FMLY_IPV6;
	    i4IgmpSrcListIfIndex = *pi4NextIgmpSrcListIfIndex;
	    SrcListAddrX = NextSrcListAddrX;
	    HostAddrX = NextHostAddrX;
	    continue;
       
    }
      

  
    IGMP_IP_COPY_FROM_IPVX (pu4NextIgmpSrcListAddress, NextSrcListAddrX,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (pu4NextIgmpSrcListHostAddress, NextHostAddrX,
                            IPVX_ADDR_FMLY_IPV4);
           

    return i1Status;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIgmpSrcListStatus
 Input       :  The Indices
                IgmpSrcListAddress
                IgmpSrcListIfIndex
                IgmpSrcListHostAddress

                The Object 
                retValIgmpSrcListStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIgmpSrcListStatus (UINT4 u4IgmpSrcListAddress,
                         INT4 i4IgmpSrcListIfIndex,
                         UINT4 u4IgmpSrcListHostAddress,
                         INT4 *pi4RetValIgmpSrcListStatus)
{
    INT1                i1Status = SNMP_FAILURE;
    tIgmpIPvXAddr       SrcListAddrX;
    tIgmpIPvXAddr       HostAddrX;

    IGMP_IPVX_ADDR_CLEAR (&SrcListAddrX);
    IGMP_IPVX_ADDR_CLEAR (&HostAddrX);

    IGMP_IPVX_ADDR_INIT_IPV4 (SrcListAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListAddress);
    IGMP_IPVX_ADDR_INIT_IPV4 (HostAddrX, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4IgmpSrcListHostAddress);

    i1Status =
        IgmpMgmtUtilNmhGetIgmpSrcListStatus (IPVX_ADDR_FMLY_IPV4, SrcListAddrX,
                                             i4IgmpSrcListIfIndex, HostAddrX,
                                             pi4RetValIgmpSrcListStatus);

    return i1Status;
}
