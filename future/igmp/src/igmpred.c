/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: igmpred.c,v 1.34 2017/02/06 10:45:28 siva Exp $ 
 *
 * Description: This file contains IGMP Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __IGMPRED_C
#define __IGMPRED_C

#ifdef IGMPPRXY_WANTED
#include "igpinc.h"
#else
#include "igmpinc.h"
#endif

#ifdef IGMPPRXY_WANTED
PRIVATE INT4        IgpRedUtilGetFwdEntry (tIgmpIPvXAddr u4GrpAddr,
                                           tIgmpIPvXAddr u4SrcAddr,
                                           tIgpRedTable ** ppRetIgpRedInfo);
PRIVATE INT4        IgpRedUtilBuildForwardEntry (tIgmpIPvXAddr GrpAddr,
                                                 tIgmpIPvXAddr SrcAddr,
                                                 UINT4 u4IfIndex,
                                                 tPortListExt OifPortList,
                                                 tIgmpPrxyFwdEntry **
                                                 pRetIgpForwardEntry);
#endif

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE ;
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDynDataDescInit                           */
/*                                                                           */
/*    Description         : This function will initialize dynamic info       */
/*                          data descriptor for the different DB nodes.      */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pIgmpGrpDynDataDesc = NULL;
    tDbDataDescInfo    *pIgmpQueryDynDataDesc = NULL;
    tDbDataDescInfo    *pIgmpSrcDynDataDesc = NULL;
    tDbDataDescInfo    *pIgmpSrcReporterDynDataDesc = NULL;
#ifdef IGMPPRXY_WANTED
    tDbDataDescInfo    *pIgmpUpIntDynDataDesc = NULL;
    tDbDataDescInfo    *pIgmpFwdDynDataDesc = NULL;
    tDbDataDescInfo    *pIgmpPrxyInfoDynDataDesc = NULL;
#endif

    /* Group info DB Node */
    pIgmpGrpDynDataDesc = &(gaIgmpDynDataDescList[IGMP_RED_GRP_INFO]);
    pIgmpGrpDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.
     */
    /*
     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pIgmpGrpDynDataDesc->u4DbDataSize =
        sizeof (tIgmpGroup) - FSAP_OFFSETOF (tIgmpGroup, u4IfIndex);
    pIgmpGrpDynDataDesc->pDbDataOffsetTbl = gaIgmpGrpOffsetTbl;

    /* Source info DB Node */
    pIgmpSrcDynDataDesc = &(gaIgmpDynDataDescList[IGMP_RED_SRC_INFO]);
    pIgmpSrcDynDataDesc->pDbDataHandleFunction = NULL;

    pIgmpSrcDynDataDesc->u4DbDataSize =
        sizeof (tIgmpSource) - FSAP_OFFSETOF (tIgmpSource, u4IfIndex);
    pIgmpSrcDynDataDesc->pDbDataOffsetTbl = gaIgmpSrcOffsetTbl;

    /* Source Reporter info DB Node */
    pIgmpSrcReporterDynDataDesc =
        &(gaIgmpDynDataDescList[IGMP_RED_SRC_REPORTER_INFO]);
    pIgmpSrcReporterDynDataDesc->pDbDataHandleFunction = NULL;

    pIgmpSrcReporterDynDataDesc->u4DbDataSize =
        sizeof (tIgmpSrcReporter) - FSAP_OFFSETOF (tIgmpSrcReporter, u4IfIndex);
    pIgmpSrcReporterDynDataDesc->pDbDataOffsetTbl = gaIgmpSrcReporterOffsetTbl;

    /* Query info DB Node */
    pIgmpQueryDynDataDesc = &(gaIgmpDynDataDescList[IGMP_RED_QUERY_INFO]);
    pIgmpQueryDynDataDesc->pDbDataHandleFunction = NULL;

    pIgmpQueryDynDataDesc->u4DbDataSize =
        sizeof (tIgmpQuery) - FSAP_OFFSETOF (tIgmpQuery, u4IfIndex);
    pIgmpQueryDynDataDesc->pDbDataOffsetTbl = gaIgmpQueryOffsetTbl;

#ifdef IGMPPRXY_WANTED
    /* Upstream interface info DB Node */
    pIgmpUpIntDynDataDesc = &(gaIgmpDynDataDescList[IGP_RED_UP_INT_INFO]);
    pIgmpUpIntDynDataDesc->pDbDataHandleFunction = NULL;

    pIgmpUpIntDynDataDesc->u4DbDataSize =
        sizeof (tIgmpPrxyUpIface) - FSAP_OFFSETOF (tIgmpPrxyUpIface, u4IfIndex);
    pIgmpUpIntDynDataDesc->pDbDataOffsetTbl = gaIgmpUpIntOffsetTbl;

    /* Forwarding Entry DB Node */
    pIgmpFwdDynDataDesc = &(gaIgmpDynDataDescList[IGP_RED_FWDENTRY_INFO]);
    pIgmpFwdDynDataDesc->pDbDataHandleFunction = NULL;

    pIgmpFwdDynDataDesc->u4DbDataSize =
        sizeof (tIgmpPrxyFwdEntry) - FSAP_OFFSETOF (tIgmpPrxyFwdEntry,
                                                    u4IfIndex);
    pIgmpFwdDynDataDesc->pDbDataOffsetTbl = gaIgmpFwdOffsetTbl;

    /* Igmp Proxy Info DB Node */
    pIgmpPrxyInfoDynDataDesc = &(gaIgmpDynDataDescList[IGP_RED_INFO]);
    pIgmpPrxyInfoDynDataDesc->pDbDataHandleFunction = NULL;

    pIgmpPrxyInfoDynDataDesc->u4DbDataSize =
        sizeof (tIgmpProxyInfo) - FSAP_OFFSETOF (tIgmpProxyInfo, u2PktOffset);
    pIgmpPrxyInfoDynDataDesc->pDbDataOffsetTbl = gaIgmpPrxyInfoOffsetTbl;
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Igmp descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDescrTblInit (VOID)
{
    tDbDescrParams      IgmpDescrParams;
    MEMSET (&IgmpDescrParams, 0, sizeof (tDbDescrParams));

    IgmpDescrParams.u4ModuleId = RM_IGMP_APP_ID;

    IgmpDescrParams.pDbDataDescList = gaIgmpDynDataDescList;

    DbUtilTblInit (&gIgmpDynInfoList, &IgmpDescrParams);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Igmp db node.      */
/*                                                                           */
/*    Input(s)            : DBNode - Igmp data strucutres DBNode             */
/*                    u4Type - Dynamic info type                       */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDbNodeInit (tDbTblNode * pDBNode, UINT4 u4Type)
{
    DbUtilNodeInit (pDBNode, u4Type);

    return;
}

/************************************************************************
 *  Function Name   : IgmpRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the IGMP module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
IgmpRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE), 
			"Entering IgmpRedInitGlobalInfo\r \n");

#ifdef IGMPPRXY_WANTED
    gIgpRedTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tIgpRedTable, RbNode), IgmpRBTreeRedEntryCmp);

    if (gIgpRedTable == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "RB Tree creation for Redundancy table failed\r\n");
        return OSIX_FAILURE;
    }
#endif

    /* Create the RM Packet arrival Q */

    if (OsixQueCrt ((UINT1 *) IGMP_RM_PKT_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    IGMP_RM_QUE_DEPTH, &gIgmpRmPktQId) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "INIT: Queue Creation Failed\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_IGMP_APP_ID;
    RmRegParams.pFnRcvPkt = IgmpRedRmCallBack;

    /* Registering IGMP with RM */
    if (IgmpRedRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {

        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedInitGlobalInfo: Registration with RM failed\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedInitGlobalInfo: Registration with RM failed"));
        return OSIX_FAILURE;
    }
    IGMP_GET_NODE_STATUS () = RM_INIT;
    IGMP_NUM_STANDBY_NODES () = 0;
    gIgmpRedGblInfo.bBulkReqRcvd = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE), 
			"Exiting IgmpRedInitGlobalInfo\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : IgmpRedRmRegisterProtocols                        
 *                                                                      
 *  Description     : This function is invoked by the IGMP module to    
 *                    register itself with the RM module. This          
 *                    registration is required to send/receive peer     
 *                    synchronization messages and control events       
 *                                                                      
 *  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       
 *                                                                      
 *  Output(s)       : None.                                             
 *                                                                      
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       
 ************************************************************************/

PUBLIC INT4
IgmpRedRmRegisterProtocols (tRmRegParams * pRmReg)
{

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE), 
			"Entering IgmpRedRmRegisterProtocols\r \n");


    /* Registering IGMP protocol with RM */
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedRmRegisterProtocols: Registration with RM failed");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedRmRegisterProtocols: Registration with RM failed"));
        return OSIX_FAILURE;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE), 
		    "Exiting IgmpRedRmRegisterProtocols\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : IgmpRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the IGMP module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register IGMP with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
IgmpRedDeInitGlobalInfo (VOID)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			 "Entering IgmpRedDeInitGlobalInfo\r \n");

    if (gIgmpRmPktQId != IGMP_ZERO)
    {
        OsixQueDel (gIgmpRmPktQId);
        gIgmpRmPktQId = IGMP_ZERO;
    }
#ifdef IGMPPRXY_WANTED
    if (gIgpRedTable != NULL)
    {
        RBTreeDelete (gIgpRedTable);
        gIgpRedTable = NULL;
    }
#endif
    /* Deregister from SYSLOG */
    if (RmDeRegisterProtocols (RM_IGMP_APP_ID) == OSIX_FAILURE)
    {

        SYS_LOG_DEREGISTER (gu4IgmpSysLogId);
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedDeInitGlobalInfo: De-Registration with RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedDeInitGlobalInfo: De-Registration with RM failed"));
        return OSIX_FAILURE;
    }

    SYS_LOG_DEREGISTER (gu4IgmpSysLogId);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedDeInitGlobalInfo\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : IgmpRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to IGMP        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
IgmpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tIgmpRmMsg         *pMsg = NULL;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
			 "Entering IgmpRedRmCallBack\r \n");

    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedRmCallBack:This event is not associated with RM \r\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedRmCallBack: Queue Message associated with the event "
                  "is not sent by RM \r\n");
        return OSIX_FAILURE;
    }

    if ((pMsg =
         (tIgmpRmMsg *) MemAllocMemBlk ((tMemPoolId) IGMP_RM_MSG_MEMPOOL_ID)) ==
        NULL)
    {

        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedRmCallBack: Queue message allocation failure\r \n");

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            IgmpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tIgmpRmMsg));

    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    /* Send the message associated with the event to IGMP module in a queue */
    if (OsixQueSend (IGMP_RM_PKT_Q_ID,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) IGMP_RM_MSG_MEMPOOL_ID,
                            (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            IgmpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedRmCallBack: Q send failure\r \n");
        return OSIX_FAILURE;
    }

    /* Post a event to IGMP to process RM events */
    OsixEvtSend (gIgmpTaskId, IGMP_RM_PKT_EVENT);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedRmCallBack\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : IgmpRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the IGMP module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the IGMP Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
IgmpRedHandleRmEvents (VOID)
{
    tIgmpRmMsg         *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;
    INT4                i4RmMsgCount = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedHandleRmEvents\r \n");
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixQueRecv (IGMP_RM_PKT_Q_ID,
                        (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
	if (pMsg == NULL)
	{
	    return;
	}
        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "IgmpRedHandleRmEvents:Received GO_ACTIVE event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                              "IgmpRedHandleRmEvents: Received GO_ACTIVE event"));
                IgmpRedHandleGoActive ();
                break;
            case GO_STANDBY:
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "IgmpRedHandleRmEvents:Received GO_STANDBY event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                              "IgmpRedHandleRmEvents:Received GO_STANDBY event"));
                IgmpRedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                return;
            case RM_STANDBY_UP:
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "IgmpRedHandleRmEvents: Received RM_STANDBY_UP"
                          " event \r\n");
		
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                              "IgmpRedHandleRmEvents: Received RM_STANDBY_UP"
                              " event"));
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                IGMP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                IgmpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (IGMP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    IGMP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gIgmpRedGblInfo.u1BulkUpdStatus = IGMP_HA_UPD_NOT_STARTED;
                    IgmpRedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "IgmpRedHandleRmEvents: Received RM_STANDBY_DOWN "
                          "event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                              "IgmpRedHandleRmEvents: Received RM_STANDBY_DOWN "
                              "event"));
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                IGMP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                IgmpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "IgmpRedHandleRmEvents:Received RM_MESSAGE event\r\n");
                ProtoAck.u4AppId = RM_IGMP_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (IGMP_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    /* Process the message at active */
                    IgmpRedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
                {
                    /* Process the message at standby */
                    IgmpRedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                    pMsg->RmCtrlMsg.u2DataLen);
                }
                else
                {
                    /* Message is received at the idle node so ignore */
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                              "IgmpRedHandleRmEvents: Sync-up message received"
                              " at Idle Node!!!!\r\n");
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                                  "IgmpRedHandleRmEvents: Sync-up message "
                                  "received at Idle Node"));
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the IGMP status is changed 
                 * from idle to standby */
                if (IGMP_GET_NODE_STATUS () == RM_INIT)
                {
                    if (IGMP_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        IgmpRedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

   			if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
    			{
        			IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 			 "Failed to Inform RM about the Standby event Processed \r\n");
    			}
                 }
                }
                break;
            case L2_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "IgmpRedHandleRmEvents: Received "
                          "L2_INITIATE_BULK_UPDATES \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                              "IgmpRedHandleRmEvents: Received "
                              "L2_INITIATE_BULK_UPDATES"));
                IgmpRedSendBulkReqMsg ();
                break;
            default:
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "IgmpRedHandleRmEvents:Invalid RM event received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                              "IgmpRedHandleRmEvents: Invalid RM event received"));
                break;

        }
        MemReleaseMemBlock (IGMP_RM_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
	
        i4RmMsgCount++;
	if (i4RmMsgCount >= IGMP_MAX_RM_MSGS)
	{
	    if (OsixEvtSend (gIgmpTaskId, IGMP_RM_PKT_EVENT)
		    != OSIX_SUCCESS)
	    {
		IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
			IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
			"Send RM packet event Failed\n");

	    }
	    else
	    {
		IGMP_TRC_ARG1 (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
			IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
			"Relinquishing after Processing %d RM messages and "
			"Posting RM packet Event Succeeds\n", i4RmMsgCount);
	    }
	    break;
	}
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedHandleRmEvents\r \n");
    return;
}

/************************************************************************
 * Function Name      : IgmpRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the IGMP upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
IgmpRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedHandleGoActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedHandleGoActive"));
    
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_IGMP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gIgmpRedGblInfo.u1BulkUpdStatus = IGMP_HA_UPD_NOT_STARTED;

    if (IGMP_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedHandleGoActive: GO_ACTIVE event reached when node "
                  "is already active \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedHandleGoActive: GO_ACTIVE event reached when "
                      "node is already active"));
        return;
    }
    if (IGMP_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IgmpRedHandleGoActive: Idle to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedHandleGoActive: Idle to Active transition"));

        IgmpRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the arp entries 
         * and change the state to active */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IgmpRedHandleGoActive: Standby to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedHandleGoActive: Standby to Active transition"));
        IgmpRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (IGMP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        IGMP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gIgmpRedGblInfo.u1BulkUpdStatus = IGMP_HA_UPD_NOT_STARTED;
        IgmpRedSendBulkUpdMsg ();
    }
    if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to Inform RM about the acknowledgement of GO_ACTIVE \r\n");
    }	

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedHandleGoActive"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE), 
		"Exiting IgmpRedHandleGoActive\r \n");
    return;
}

/************************************************************************
 * Function Name      : IgmpRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the IGMP upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
IgmpRedHandleGoStandby (tIgmpRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedHandleGoStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedHandleGoStandby"));
    ProtoEvt.u4AppId = RM_IGMP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
    MemReleaseMemBlock (IGMP_RM_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
    gIgmpRedGblInfo.u1BulkUpdStatus = IGMP_HA_UPD_NOT_STARTED;

    if (IGMP_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IgmpRedHandleGoStandby: GO_STANDBY event reached when "
                  "node is already in standby \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already in standby"));
        return;
    }
    if (IGMP_GET_NODE_STATUS () == RM_INIT)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IgmpRedHandleGoStandby: GO_STANDBY event reached when node "
                  "is already idle \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already idle"));

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IgmpRedHandleGoStandby: Active to Standby transition..\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedHandleGoStandby: Active to Standby transition"));
        IgmpRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
         if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
        {
        	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                	  "Failed to Inform RM about the Standby event Processed \r\n");
        }

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedHandleGoStandby"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedHandleGoStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleIdleToActive (VOID)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedHandleIdleToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedHandleIdleToActive"));

    /* Node status is set to active and the number of standby nodes 
     * are updated */
    IGMP_GET_NODE_STATUS () = RM_ACTIVE;
    IGMP_RM_GET_NUM_STANDBY_NODES_UP ();

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedHandleIdleToActive"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedHandleIdleToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleIdleToStandby (VOID)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE), 
		"Entering IgmpRedHandleIdleToStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedHandleIdleToStandby"));

    /* the node status is set to standby and no of standby nodes is set to 0 */
    IGMP_GET_NODE_STATUS () = RM_STANDBY;
    IGMP_NUM_STANDBY_NODES () = 0;

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "IgmpRedHandleIdleToStandby: Node Status Idle to Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "IgmpRedHandleIdleToStandby: Node Status Idle to Standby"));

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedHandleIdleToStandby"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedHandleIdleToStandby\r \n");

    return;
}

/************************************************************************/
/* Function Name      : IgmpStartTimers                            */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion timers processing.         */
/*                      The following action are                  */
/*                      performed during this transistion.              */
/*                      1.Start the timers for the query node, group    */
/*                          node & source node.                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpStartTimers (VOID)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfNode = NULL;
    tIgmpQuery         *pIgmpQuery = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
	INT1				i1SrcFlag = IGMP_FALSE;
	INT1                i1LimitSt = 0;
	UINT2				u2QueryMaxResponseTime = 0;
	UINT4				u4Duration = IGMP_MAX_GRP_RESTART_TIME;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpStartTimers\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpStartTimers"));

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);
    /* Scan though the interface list */
    while (pIfGetNextLink != NULL)
    {
        pIfNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pIfGetNextLink);
        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfNode->IfGetNextLink));
        if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            if (gIgmpConfig.u1IgmpStatus == IGMP_DISABLE)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                        "IGMP is globally disabled . \n");
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting IgmpStartTimers\r \n");
                continue;
            }
        }
        else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            if (gIgmpConfig.u1MldStatus == IGMP_DISABLE)
            {
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_TMR_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                        "MLD is globally disabled . \n");
                MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
                        "Exiting IgmpStartTimers\r \n");
                continue;
            }
        }
        /* Query should be sent only in igmp enabled interface */
        if (pIfNode->u1AdminStatus != IGMP_IFACE_ADMIN_UP)
        {
            continue;
        }
        /* Find the querier interface node & start the query timer */
        pIgmpQuery = pIfNode->pIfaceQry;

        /* queries are sent */
		if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV4)
        {
			u2QueryMaxResponseTime = pIfNode->u2MaxRespTime;
			pIfNode->u2MaxRespTime = IGMP_MAX_QRY_MAX_RESP_TIME;
            IgmpSendQuery (pIfNode, IGMP_ALL_HOSTS_GROUP);
			pIfNode->u2MaxRespTime = u2QueryMaxResponseTime;
        }
        else if (pIfNode->u1AddrType == IPVX_ADDR_FMLY_IPV6)
        {
			u2QueryMaxResponseTime = pIfNode->u2MaxRespTime;
			pIfNode->u2MaxRespTime = IGMP_MAX_QRY_MAX_RESP_TIME;
            MldOutSendQuery (pIfNode, &gAllNodeV6McastAddr);
			pIfNode->u2MaxRespTime = u2QueryMaxResponseTime;
        }

        if (pIgmpQuery->u1Querier == TRUE)
        {
            OsixGetSysTime (&(pIgmpQuery->u4QuerierUpTime));
            /* update the Igmp query block with querier address */
            IGMP_IPVX_ADDR_COPY (&(pIgmpQuery->u4QuerierAddr),
                                 &(pIfNode->u4IfAddr));
            IGMPSTARTTIMER (pIgmpQuery->Timer, IGMP_QUERY_TIMER_ID,
                            IGMP_TWENTY, IGMP_ZERO);
        }
        else
        {
            IGMPSTARTTIMER (pIgmpQuery->Timer, IGMP_QUERIER_PRESENT_TIMER_ID,
                            (UINT2)
                            IGMP_IF_OTHER_QUERIER_PRESENT_TIMER (pIfNode),
                            IGMP_ZERO);
        }

        MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));
        IgmpGrpEntry.u4IfIndex = pIfNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfNode;

        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) & IgmpGrpEntry, NULL);
		while (pGrp != NULL)
		{
				if ((pGrp->u4IfIndex != pIfNode->u4IfIndex) ||
					(pGrp->pIfNode->u1AddrType != pIfNode->u1AddrType))
				{
						/* Group entry doesn't belong to 
						 * specified interface index */
						break;
				}

				pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
				while (pSrc != NULL)
				{
						i1SrcFlag = IGMP_TRUE;
						pNextSrc =
								(tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);
						/* Timers need not be started for static source */
						if (pSrc->u1StaticFlg == IGMP_FALSE)
						{
							IGMPSTARTTIMER (pSrc->srcTimer, IGMP_SOURCE_TIMER_ID,
									u4Duration, IGMP_ZERO);
						}
						if ((pSrc->u1FwdState == IGMP_TRUE) &&
										(IGMP_PROXY_STATUS () == IGMP_DISABLE))
						{
								TMO_SLL_Delete (&(pGrp->srcList), &pSrc->Link);
								TMO_SLL_Add (&(pGrp->SrcListForMrpUpdate), &pSrc->Link);
						}
						pSrc = pNextSrc;
				}
				/* Timers need not be started for static group */
				if (pGrp->u1StaticFlg == IGMP_FALSE)
				{
						i1LimitSt = IgmpCheckForLimit (pIfNode, pGrp->u4Group);
						if (IGMP_GROUP_MATCHED == i1LimitSt)
						{
								if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
								{
										gIgmpConfig.u4GlobalCurCnt++;
										pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
								}
						}
						else if (IGMP_INF_LIMIT_NOT_REACHED == i1LimitSt)
						{
								pIfNode->u4GrpCurCnt++;
								pGrp->u1LimitFlag |= IGMP_INTERFACE_LIMIT_COUNTED;
								if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
								{
										gIgmpConfig.u4GlobalCurCnt++;
										pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
								}
						}
						else if (IGMP_INF_LIMIT_NOT_CONFIGURED == i1LimitSt)
						{
								if (gIgmpConfig.u4GlobalGrpLimit != IGMP_ZERO)
								{
										gIgmpConfig.u4GlobalCurCnt++;
										pGrp->u1LimitFlag |= IGMP_GLOBAL_LIMIT_COUNTED;
								}
						}
						if ((pGrp->u1FilterMode == IGMP_FMODE_EXCLUDE) || (i1SrcFlag == IGMP_FALSE))
						{
							IGMPSTARTTIMER (pGrp->Timer, IGMP_GROUP_TIMER_ID,
										u4Duration,
										IGMP_ZERO);
						}

				}
            if (TMO_SLL_Count (&pGrp->SrcListForMrpUpdate) > IGMP_ZERO)
            {
                    /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
            else
            {
                if ((gIgmpConfig.u1IgmpStatus == IGMP_ENABLE) ||
                    (gIgmpConfig.u1MldStatus == MLD_ENABLE))
                {
                    /* Updating MRPs */
                    IgmpUpdateMrps (pGrp, IGMP_JOIN);
                }
            }
            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) pGrp, NULL);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpStartTimers\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpStartTimers"));

    return;
}

#ifdef IGMPPRXY_WANTED
/************************************************************************/
/* Function Name      : IgmpProxyStartTimers                        */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion timers processing.             */
/*                      The following action are                        */
/*                      performed during this transistion.              */
/*                      1.Start the timers for the query node, group    */
/*                        node & source node.                           */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpProxyStartTimers (VOID)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    tTMO_SLL_NODE      *pIgpUpIfaceNode = NULL;
    tTMO_SLL_NODE      *pIgpNextUpIfaceNode = NULL;
    UINT4               u4HashIndex = IGMP_ZERO;
    UINT4               u4RetValue = 0;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UNUSED_PARAM (u4RetValue);

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpProxyStartTimers\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpProxyStartTimers"));

    if (gIgmpProxyInfo.pUpIfaceTbl == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Upstream interfaces are not present \r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
		  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Upstream interfaces are not present \r\n");
        return;
    }

    TMO_HASH_Scan_Table (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex)
    {
        pIgpUpIfaceNode =
            TMO_HASH_Get_First_Bucket_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                            u4HashIndex);
        while (pIgpUpIfaceNode)
        {
            pIgpNextUpIfaceNode =
                TMO_HASH_Get_Next_Bucket_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                               u4HashIndex, pIgpUpIfaceNode);

            /* Start the upstream interface timer */
            pIgpUpIfaceEntry = (tIgmpPrxyUpIface *) pIgpUpIfaceNode;

            u4RetValue = TmrStart (gIgmpProxyInfo.ProxyTmrListId,
                                   &(pIgpUpIfaceEntry->IfacePurgeTimer.
                                     TimerBlk), IGP_RTR_IFACE_TIMER,
                                   pIgpUpIfaceEntry->u2RtrPurgeInt, 0);

            pIgpUpIfaceNode = pIgpNextUpIfaceNode;
        }
    }
    if (gIgmpProxyInfo.ForwardEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "No Multicasting forwarding entries are not present\r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "No Multicasting forwarding entries are not present\r\n");
        return;
    }

    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    /* Scan thro the forwarding entries */
    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        /* start the forward entry timer */
        u4RetValue = TmrStart (gIgmpProxyInfo.ProxyTmrListId,
                               &(pIgpForwardEntry->EntryTimer.TimerBlk),
                               IGP_FWD_ENTRY_TIMER, IGP_FWD_ENTRY_TIMER_VAL, 0);

        pRBElem = pRBNextElem;
    }

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpProxyStartTimers\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpProxyStartTimers"));
    return;
}
#endif
/************************************************************************/
/* Function Name      : IgmpRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleStandbyToActive (VOID)
{

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedHandleStandbyToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedHandleStandbyToActive"));

#ifdef IGMPPRXY_WANTED
    /* Hardware audit is done and the hardware and software entries are 
     * checked for synchronization */
    IgmpRedHwAudit ();
#endif

    IGMP_GET_NODE_STATUS () = RM_ACTIVE;
    IGMP_RM_GET_NUM_STANDBY_NODES_UP ();

    IgmpStartTimers ();

#ifdef IGMPPRXY_WANTED
    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        IgmpProxyStartTimers ();
    }
#endif

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "IgmpRedHandleStandbyToActive: Node Status Standby to "
              "Active\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "IgmpRedHandleStandbyToActive: Node Status Standby to Active"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedHandleStandbyToActive"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedHandleStandbyToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedHandleActiveToStandby (VOID)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
#ifdef IGMPPRXY_WANTED
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyFwdEntry  *pNextIgpForwardEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
#endif

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedHandleActiveToStandby\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedHandleActiveToStandby"));

    /* Delete all the dynamically learnt entries
     * and flush those memories. These entries will be learnt from the new
     * active node through bulk updated */

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    while (pIfGetNextLink != NULL)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfaceNode->IfGetNextLink));

        /* clear querier related stuff */
        IgmpHdlTransitionToNonQuerier (pIfaceNode);

        /* Clear all membership information */
        MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

        IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfaceNode;

	IgmpClearStats(pIfaceNode);
        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) & IgmpGrpEntry, NULL);
        while (pGrp != NULL)
        {
            if ((pGrp->u4IfIndex != pIfaceNode->u4IfIndex) ||
                (pGrp->pIfNode->u1AddrType != pIfaceNode->u1AddrType))
            {
                /* Group entry doesn't belong to 
                 * specified interface index */
                break;
            }
            pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                     (tRBElem *) pGrp, NULL);
	    /* Static Group entries needs to be retained */
	    if ((pGrp->u1StaticFlg == IGMP_TRUE) && 
		    (pGrp->u1DynamicFlg == IGMP_FALSE))
	    {
		pGrp = pNextGrp;
		continue;
	    }
            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
            while (pSrc != NULL)
            {
                pNextSrc =
                    (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);
		if (pSrc->u1StaticFlg == IGMP_TRUE)
		{
			    if (pSrc->u1DynamicFlg == IGMP_TRUE)
			    {
				    pSrc->u1DynamicFlg = IGMP_FALSE;
				    IGMPSTOPTIMER (pSrc->srcTimer);
				    IgmpDelSchQrySrcNode (pSrc);
				    IgmpDeleteAllSrcReporters (pSrc);
				    IGMP_IPVX_ADDR_CLEAR (&(pSrc->u4LastReporter));
			    }
		    pSrc = pNextSrc;
		    continue;
		}
		pSrc->u1DelSyncFlag = IGMP_FALSE;
                IgmpDeleteSource (pGrp, pSrc);
                pSrc = pNextSrc;
            }
            pGrp->u1DelSyncFlag = IGMP_FALSE;
	    IgmpExpireGroup (pGrp->pIfNode, pGrp, pGrp->pIfNode->u1Version,
			    IGMP_TRUE, pGrp->u1StaticFlg);
            pGrp = pNextGrp;
        }
    }
#ifdef IGMPPRXY_WANTED
    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        /* Delete all the Multicast forwarding entries */
        if (gIgmpProxyInfo.ForwardEntry != NULL)
        {
            pIgpForwardEntry =
                (tIgmpPrxyFwdEntry *) RBTreeGetFirst (gIgmpProxyInfo.
                                                      ForwardEntry);

            /* Scan thro the forwarding entries */
            while (pIgpForwardEntry != NULL)
            {
                pNextIgpForwardEntry = (tIgmpPrxyFwdEntry *)
                    RBTreeGetNext (gIgmpProxyInfo.ForwardEntry,
                                   (tRBElem *) pIgpForwardEntry, NULL);

                /* Stop the Forwarding entry timer  */
                TmrStop (gIgmpProxyInfo.ProxyTmrListId,
                         &(pIgpForwardEntry->EntryTimer.TimerBlk));

                /* Delete the Oif nodes */
                while ((pIgpOifNode = (tIgmpPrxyOif *)
                        TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
                {
                    TMO_SLL_Delete (&pIgpForwardEntry->OifList,
                                    &pIgpOifNode->NextOifNode);
                    IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
                }
                RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                              (tRBElem *) pIgpForwardEntry);

                IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);

                pIgpForwardEntry = pNextIgpForwardEntry;
            }
        }
        /* Delete all the Group entries */
        IgpGrpDeleteGroupEntries ();
    }
#endif
    /*update the statistics */
    IGMP_GET_NODE_STATUS () = RM_STANDBY;
    IGMP_RM_GET_NUM_STANDBY_NODES_UP ();

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "IgmpRedHandleActiveToStandby: Node Status Active to "
              "Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "IgmpRedHandleActiveToStandby: Node Status Active to Standby"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedHandleActiveToStandby"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedHandleActiveToStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : IgmpClearStats                                  */
/*                                                                      */
/* Description        : This routine is to clear the statistics         */
/*                      entries created.                                */
/*                                                                      */
/* Input(s)           : pIfaceNode - Interface entry                    */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpClearStats (tIgmpIface * pIfaceNode)
{

        pIfaceNode->u4InPkts = IGMP_ZERO;
        pIfaceNode->u4InJoins = IGMP_ZERO;
        pIfaceNode->u4InLeaves = IGMP_ZERO;
        pIfaceNode->u4InQueries = IGMP_ZERO;
        pIfaceNode->u4OutQueries = IGMP_ZERO;
        pIfaceNode->u4WrongVerQueries = IGMP_ZERO;
        pIfaceNode->u4ProcessedJoins = IGMP_ZERO;
        pIfaceNode->u4RxGenQueries = IGMP_ZERO;
        pIfaceNode->u4TxGenQueries = IGMP_ZERO;
        pIfaceNode->u4TxGrpQueries = IGMP_ZERO;
        pIfaceNode->u4TxGrpSrcQueries = IGMP_ZERO;
        pIfaceNode->u4RxGrpQueries = IGMP_ZERO;
        pIfaceNode->u4RxGrpSrcQueries = IGMP_ZERO;
        pIfaceNode->u4Txv2Leaves = IGMP_ZERO;
        pIfaceNode->u4CKSumError = IGMP_ZERO;
        pIfaceNode->u4PktLenError = IGMP_ZERO;;
        pIfaceNode->u4PktWithLocalIP = IGMP_ZERO;
        pIfaceNode->u4SubnetCheckFailure = IGMP_ZERO;;
        pIfaceNode->u4QryFromNonQuerier = IGMP_ZERO;;
        pIfaceNode->u4ReportVersionMisMatch = IGMP_ZERO;;
        pIfaceNode->u4QryVersionMisMatch = IGMP_ZERO;;
        pIfaceNode->u4UnknownMsgType = IGMP_ZERO;;
        pIfaceNode->u4InvalidV1Report = IGMP_ZERO;;
        pIfaceNode->u4InvalidV2Report = IGMP_ZERO;;
        pIfaceNode->u4InvalidV3Report = IGMP_ZERO;;
        pIfaceNode->u4RouterAlertCheckFailure = IGMP_ZERO;;
        pIfaceNode->u4BadScopeErr = IGMP_ZERO;
        pIfaceNode->u4IgmpRxv1v2Reports = IGMP_ZERO;
        pIfaceNode->u4IgmpRxv3Reports = IGMP_ZERO;
        pIfaceNode->u4IgmpTxv1v2Reports = IGMP_ZERO;
        pIfaceNode->u4IgmpTxv3Reports = IGMP_ZERO;
        pIfaceNode->u4IgmpInSSMPkts = IGMP_ZERO;
        pIfaceNode->u4IgmpInvalidSSMPkts = IGMP_ZERO;
        pIfaceNode->u4IgmpMalformedPkt = IGMP_ZERO;
        pIfaceNode->u4IgmpSocketErr = IGMP_ZERO;
        pIfaceNode->u4MldRxv1Reports = IGMP_ZERO;
        pIfaceNode->u4MldRxv2Reports = IGMP_ZERO;
        pIfaceNode->u4MldTxv1Reports = IGMP_ZERO;
	pIfaceNode->u4MldTxv2Reports = IGMP_ZERO;
        pIfaceNode->u4MldInSSMPkts = IGMP_ZERO;
        pIfaceNode->u4MldInvalidSSMPkts = IGMP_ZERO;
        pIfaceNode->u4MldMalformedPkt = IGMP_ZERO;
        pIfaceNode->u4MldSocketErr = IGMP_ZERO;
#ifdef MULTICAST_SSM_ONLY
        pIfaceNode->u4DroppedASMIncomingPkts = IGMP_ZERO;
#endif

	return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;
   
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
   
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedProcessPeerMsgAtActive\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedProcessPeerMsgAtActive"));
    ProtoEvt.u4AppId = RM_IGMP_APP_ID;

    IGMP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    IGMP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
         if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
	 {
        	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                	  "Failed to Inform RM about Process failure at Active Node \r\n");
         }

    }

    if (u2Length != IGMP_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

         if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
	 {
        	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Failed to Inform RM about Process failure at Active Node\r\n");
         }

    }

    if (u1MsgType == IGMP_RED_BULK_REQ_MESSAGE)
    {
        gIgmpRedGblInfo.u1BulkUpdStatus = IGMP_HA_UPD_NOT_STARTED;
        if (!IGMP_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gIgmpRedGblInfo.bBulkReqRcvd = OSIX_TRUE;

            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "IgmpRedProcessPeerMsgAtActive:Bulk request message "
                      "before RM_STANDBY_UP\r \n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                          "IgmpRedProcessPeerMsgAtActive:Bulk request message "
                          "before RM_STANDBY_UP"));
            return;
        }
        IgmpRedSendBulkUpdMsg ();
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedProcessPeerMsgAtActive"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE), 
		"Exiting IgmpRedProcessPeerMsgAtActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2ExtractMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedProcessPeerMsgAtStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedProcessPeerMsgAtStandby"));
    ProtoEvt.u4AppId = RM_IGMP_APP_ID;
    u2MinLen = IGMP_RED_TYPE_FIELD_SIZE + IGMP_RED_LEN_FIELD_SIZE;

    while (u4OffSet < u2DataLen)
    {
        u2ExtractMsgLen = (UINT2) u4OffSet;
        IGMP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

        IGMP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u4OffSet += u2Length;
            continue;
        }
        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen += u2Length;

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* If there is a length mismatch between the message and the given 
             * length, ignore the message and send error to RM */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

    	    if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
	    {
        	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                	  "Failed to Inform RM about Process failure at Standby Node\r\n");
    	    }
           IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                      "IgmpRedProcessPeerMsgAtStandby:RM_PROCESS Failure\r \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                          "IgmpRedProcessPeerMsgAtStandby:RM_PROCESS Failure"));
            return;
        }

        switch (u1MsgType)
        {
            case IGMP_RED_BULK_UPD_TAIL_MESSAGE:
                IgmpRedProcessBulkTailMsg (pMsg, &u4OffSet);
                break;
            case IGMP_RED_GRP_INFO:
                IgmpRedProcessGrpInfo (pMsg, &u4OffSet);
                break;
            case IGMP_RED_SRC_INFO:
                IgmpRedProcessSourceInfo (pMsg, &u4OffSet);
                break;
            case IGMP_RED_SRC_REPORTER_INFO:
                IgmpRedProcessSrcReporterInfo (pMsg, &u4OffSet);
                break;
            case IGMP_RED_QUERY_INFO:
                IgmpRedProcessQueryInfo (pMsg, &u4OffSet);
                break;
#ifdef IGMPPRXY_WANTED
            case IGP_RED_UP_INT_INFO:
                IgpRedProcessUpIntInfo (pMsg, &u4OffSet);
                break;
            case IGP_RED_FWDENTRY_INFO:
                IgpRedProcessFwdEntryInfo (pMsg, &u4OffSet);
                break;
            case IGP_RED_INFO:
                IgpRedProcessInfo (pMsg, &u4OffSet);
                break;
#endif
        }
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedProcessPeerMsgAtStandby"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedProcessPeerMsgAtStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the IGMP module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
IgmpRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedRmReleaseMemoryForMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedRmReleaseMemoryForMsg"));
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedRmReleaseMemoryForMsg:Failure in releasing allocated"
                  " memory\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedRmReleaseMemoryForMsg:Failure in releasing "
                      "allocated memory"));
        return OSIX_FAILURE;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedRmReleaseMemoryForMsg"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedRmReleaseMemoryForMsg\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : IgmpRedSendMsgToRm                        */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
IgmpRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedSendMsgToRm\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedSendMsgToRm"));
    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_IGMP_APP_ID, RM_IGMP_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedSendMsgToRm:Freememory due to message send "
                  "failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedSendMsgToRm:Freememory due to message send"
                      " failure"));
        return OSIX_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedSendMsgToRm"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedSendMsgToRm\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : IgmpRedSendBulkReqMsg                            */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedSendBulkReqMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedSendBulkReqMsg"));
    ProtoEvt.u4AppId = RM_IGMP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    IGMP Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (IGMP_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedSendBulkReqMsg: RM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedSendBulkReqMsg: RM Memory allocation failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
         if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
	 {
       		 IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                 	  "Failed to Inform RM about Memory Allocation failure \r\n");
         }

        return;
    }

    IGMP_RM_PUT_1_BYTE (pMsg, &u4OffSet, IGMP_RED_BULK_REQ_MESSAGE);
    IGMP_RM_PUT_2_BYTE (pMsg, &u4OffSet, IGMP_RED_BULK_REQ_MSG_SIZE);

    if (IgmpRedSendMsgToRm (pMsg, u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
         if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
	 {
        	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                	  "Failed to Inform RM about Sendto failure for Bulk Request Message \r\n");
         } 

        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedSendBulkReqMsg: Send message to RM failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedSendBulkReqMsg: Send message to RM failed"));
        return;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedSendBulkReqMsg"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedSendBulkReqMsg\r \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pIgmpDataDesc - This is Igmp sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pIgmpDbNode - This is db node defined in the IGMP*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedDbUtilAddTblNode (tDbTblDescriptor * pIgmpDataDesc,
                         tDbTblNode * pIgmpDbNode)
{
    if ((IGMP_IS_STANDBY_UP () == OSIX_FALSE) ||
        (RmGetNodeState () != RM_ACTIVE))
    {
        return;
    }

    DbUtilAddTblNode (pIgmpDataDesc, pIgmpDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate IGMP dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
IgmpRedSyncDynInfo (VOID)
{
    if ((IGMP_IS_STANDBY_UP () == OSIX_FALSE) ||
        (IGMP_GET_NODE_STATUS () != RM_ACTIVE))
    {
        return;
    }

    DbUtilSyncModuleNodes (&gIgmpDynInfoList);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IgmpRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

VOID
IgmpRedAddAllNodeInDbTbl (VOID)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpQuery         *pIgmpQuery = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tIgmpSource        *pSrc = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpSrcReporter   *pSrcReporter = NULL;
    tIgmpSrcReporter   *pNextSrcReporter = NULL;
    tIgmpSrcReporter   *pGrpReporter = NULL;
    tIgmpSrcReporter   *pNextGrpReporter = NULL;
#ifdef IGMPPRXY_WANTED
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    tTMO_SLL_NODE      *pIgpUpIfaceNode = NULL;
    tTMO_SLL_NODE      *pIgpNextUpIfaceNode = NULL;
    UINT4               u4HashIndex = IGMP_ZERO;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
#endif

    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    while (pIfGetNextLink != NULL)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);
        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfaceNode->IfGetNextLink));

        /* Fetch the querier interface node */
        pIgmpQuery = pIfaceNode->pIfaceQry;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pIgmpQuery->QuerierDbNode);

        /* Fetch the group info node */
        IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
        IgmpGrpEntry.pIfNode = pIfaceNode;

        pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                             (tRBElem *) & IgmpGrpEntry, NULL);
        while (pGrp != NULL)
        {
            if ((pGrp->u4IfIndex != pIfaceNode->u4IfIndex) || (pGrp->pIfNode->u1AddrType != pIfaceNode->u1AddrType))
            {
                /* Group entry doesn't belong to
                 * specified interface index */
                break;
            }
	    /* Static Group entries will be retained, so need not be synced */
	    if ((pGrp->u1StaticFlg == IGMP_TRUE) && 
		    (pGrp->u1DynamicFlg == IGMP_FALSE))
	    {
            	pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                (tRBElem *) pGrp, NULL);
		continue;
	    }
	    pGrp->u1GrpModeFlag = IGMP_ADD_GROUP;
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pGrp->GrpDbNode);
            pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
			if(pSrc == NULL)
			{
				pGrpReporter =
					(tIgmpSrcReporter *) RBTreeGetFirst(pGrp->GrpReporterTree);;
				while (pGrpReporter != NULL)
				{
					pGrpReporter->u1RepModeFlag = IGMP_ADD_REPORTER;
					pGrpReporter->u1GrpSrcFlag = IGMP_GROUP_REPORTER;
					IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
							&pGrpReporter->SrcReporterDbNode);
					pNextGrpReporter =
						(tIgmpSrcReporter *) RBTreeGetNext (pGrp->GrpReporterTree, 
								(tRBElem *) pGrpReporter, NULL);
					pGrpReporter = pNextGrpReporter;
				}
			}
            while (pSrc != NULL)
			{
                if ((pSrc->u1StaticFlg == IGMP_TRUE) &&
		    (pSrc->u1DynamicFlg == IGMP_FALSE))
				{
					pNextSrc =
						(tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);
					pSrc = pNextSrc;
					continue;
				}
				IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pSrc->SrcDbNode);

				pSrcReporter =
					(tIgmpSrcReporter *) RBTreeGetFirst(pSrc->SrcReporterTree);;
				while (pSrcReporter != NULL)
				{
					pSrcReporter->u1RepModeFlag = IGMP_ADD_REPORTER;
					pSrcReporter->u1GrpSrcFlag = IGMP_SOURCE_REPORTER;
					IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
							&pSrcReporter->SrcReporterDbNode);
					pNextSrcReporter =
						(tIgmpSrcReporter *) RBTreeGetNext (pSrc->SrcReporterTree, 
								(tRBElem *) pSrcReporter, NULL);
					pSrcReporter = pNextSrcReporter;
				}
				pNextSrc =
					(tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);

				pSrc = pNextSrc;
			}
            pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) pGrp, NULL);
        }
    }

#ifdef IGMPPRXY_WANTED

    if (gIgmpProxyInfo.pUpIfaceTbl != NULL)
    {
        /* Fetch the upstream interface node */
        TMO_HASH_Scan_Table (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex)
        {
            pIgpUpIfaceNode =
                TMO_HASH_Get_First_Bucket_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                                u4HashIndex);
            while (pIgpUpIfaceNode)
            {
                pIgpNextUpIfaceNode =
                    TMO_HASH_Get_Next_Bucket_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                                   u4HashIndex,
                                                   pIgpUpIfaceNode);

                pIgpUpIfaceEntry = (tIgmpPrxyUpIface *) pIgpUpIfaceNode;

                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgpUpIfaceEntry->UpIntDbNode);

                pIgpUpIfaceNode = pIgpNextUpIfaceNode;
            }
        }
    }

    /* Fetch the forwarding entry node */
    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    /* Scan thro the forwarding entries */
    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &pIgpForwardEntry->FwdDbNode);
        pRBElem = pRBNextElem;
    }

    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
#endif

    return;
}

/************************************************************************/
/* Function Name      : IgmpRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedSendBulkUpdMsg (VOID)
{

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),\
		 "Entering IgmpRedSendBulkUpdMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedSendBulkUpdMsg"));
    if (IGMP_IS_STANDBY_UP () == OSIX_FALSE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedSendBulkUpMsg:IGMP Standby up failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedSendBulkUpMsg:IGMP stand by up failure"));
        return;
    }

    if (gIgmpRedGblInfo.u1BulkUpdStatus == IGMP_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        IgmpRedAddAllNodeInDbTbl ();

        IgmpRedSyncDynInfo ();

        gIgmpRedGblInfo.u1BulkUpdStatus = IGMP_HA_UPD_COMPLETED;
        IgmpRedSendBulkUpdTailMsg ();
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedSendBulkUpdMsg"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedSendBulkUpdMsg\r \n");
    return;

}

/************************************************************************/
/* Function Name      : IgmpRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the IGMP offers an IP address         */
/*                      to the IGMP client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pIgmpBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
IgmpRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedSendBulkUpdTailMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedSendBulkUpdTailMsg"));
    ProtoEvt.u4AppId = RM_IGMP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * IGMP_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (IGMP_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedSendBulkUpdTailMsg: RM Memory allocation"
                  " failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedSendBulkUpdTailMsg: RM Memory allocation "
                      "failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
         if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
	 {
        	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                	  "Failed to Inform RM about Memory Allocation failure \r\n");
         }

        return;
    }

    u4OffSet = 0;

    IGMP_RM_PUT_1_BYTE (pMsg, &u4OffSet, IGMP_RED_BULK_UPD_TAIL_MESSAGE);
    IGMP_RM_PUT_2_BYTE (pMsg, &u4OffSet, IGMP_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (IgmpRedSendMsgToRm (pMsg, u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
         if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
	 {
        	IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                	  "Failed to Inform RM about Sendto failure for Bulk Update Tail Message \r\n");
         }

        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IgmpRedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4IgmpSysLogId,
                      "IgmpRedSendBulkUpdTailMsg:Send message to RM failed"));
        return;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedSendBulkUpdTailMsg"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
		 "Exiting IgmpRedSendBulkUpdTailMsg\r \n");
    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
IgmpRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_IGMP_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
		 "Entering IgmpRedProcessBulkTailMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Entering IgmpRedProcessBulkTailMsg"));

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
              "IgmpRedProcessBulkTailMsg: Bulk Update Tail Message received"
              " at Standby node.\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "IgmpRedProcessBulkTailMsg: Bulk Update Tail Message"
                  " received at Standby node"));

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

     if (RM_FAILURE == RmApiHandleProtocolEvent (&ProtoEvt))
     {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to Inform RM on Bulk Protocol  Update Completion \r\n");
     }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4IgmpSysLogId,
                  "Exiting IgmpRedProcessBulkTailMsg"));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),\
		 "Exiting IgmpRedProcessBulkTailMsg\r \n");

    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessGrpInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node group node.                 */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessGrpInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tIgmpIface         *pIfNode = NULL;
    tIgmpIPvXAddr       GrpAddr;
    tIgmpIPvXAddr       u4LastReporter;
    tIgmpGroup         *pGrp = NULL;
    UINT4               u4IfIndex = IGMP_ZERO;
    UINT4               u4GrpCompMode = IGMP_ZERO;
	UINT4               u4TmpOffset = IGMP_ZERO;
    UINT1               u1GrpMode = IGMP_ZERO;
    UINT1               au1TempPad[IGMP_SIX];

#ifdef IGMPPRXY_WANTED
    tIgmPxySrcBmp       InclSrcBmap;
    tIgmPxySrcBmp       ExclSrcBmap;

    MEMSET (InclSrcBmap, IGMP_ZERO, sizeof (tIgmPxySrcBmp));
    MEMSET (ExclSrcBmap, IGMP_ZERO, sizeof (tIgmPxySrcBmp));
#endif
	u4TmpOffset = *pu4OffSet;
    IGMP_IPVX_ADDR_CLEAR (&GrpAddr);
    IGMP_IPVX_ADDR_CLEAR (&u4LastReporter);
    IGMP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    IGMP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4GrpCompMode);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &GrpAddr, sizeof (tIgmpIPvXAddr));
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &u4LastReporter, sizeof (tIgmpIPvXAddr));
    pIfNode = IGMP_GET_IF_NODE (u4IfIndex, GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
		*(pu4OffSet) = u4TmpOffset + IGMP_GROUP_DB_SIZE;
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Interface not up - pkt dropped!!!\n");
        IGMP_DBG (IGMP_DBG_EXIT, "Exiting IgmpInput\n");
        return;
    }
#ifdef IGMPPRXY_WANTED
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, InclSrcBmap, sizeof (tIgmPxySrcBmp));
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, ExclSrcBmap, sizeof (tIgmPxySrcBmp));
#endif

    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1GrpMode);

    if (u1GrpMode == IGMP_ADD_GROUP)
    {
        /* look for the group */
        pGrp = IgmpGrpLookUp (pIfNode, GrpAddr);
        if (pGrp == NULL)
        {
            /* group doesn't exists, add the group */
            pGrp = IgmpAddGroup (pIfNode, GrpAddr, IGMP_FALSE);
            if (pGrp == NULL)
            {
                /* failed to add group */
				*(pu4OffSet) = u4TmpOffset + IGMP_GROUP_DB_SIZE;
                IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES,
                          IGMP_NAME, "Group node not created  \n");
                return;
            }
#ifdef IGMPPRXY_WANTED
            /* Copy the include and exclude source bitmaps */
            MEMCPY (pGrp->InclSrcBmp, InclSrcBmap, IGP_SRC_LIST_SIZE);
            MEMCPY (pGrp->ExclSrcBmp, ExclSrcBmap, IGP_SRC_LIST_SIZE);

            if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
            {
                IgpGrpConsolidate (pGrp, pIfNode->u1Version, IGMP_NEW_GROUP);
            }
#endif
        }
        /* Update the entries to the group */
        pGrp->u4GroupCompMode = u4GrpCompMode;
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1FilterMode);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1V2HostPresent);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1V1HostPresent);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1DynamicFlg);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1StaticFlg);
        IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, au1TempPad, IGMP_SIX);
        IGMP_IPVX_ADDR_COPY (&pGrp->u4LastReporter, &u4LastReporter); 
    }
    else
    {
        /* look for the group */
        pGrp = IgmpGrpLookUp (pIfNode, GrpAddr);
        if (pGrp == NULL)
        {
			*(pu4OffSet) = u4TmpOffset + IGMP_GROUP_DB_SIZE;
            IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES,
                      IGMP_NAME, "Group node not exists  \n");
            return;
        }
        /* Update the entries to the group */
        pGrp->u4GroupCompMode = u4GrpCompMode;
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1FilterMode);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1V2HostPresent);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1V1HostPresent);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1DynamicFlg);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pGrp->u1StaticFlg);
        IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, au1TempPad, IGMP_SIX);
	pGrp->u1DelSyncFlag = IGMP_FALSE;
        IgmpExpireGroup (pGrp->pIfNode, pGrp, pGrp->pIfNode->u1Version,
                         IGMP_TRUE, pGrp->u1StaticFlg);
    }

    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessSourceInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node source node.                */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessSourceInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tIgmpIface         *pIfNode = NULL;
    tIgmpIPvXAddr       GrpAddr;
    tIgmpIPvXAddr       SrcAddr;
    tIgmpIPvXAddr       u4SrcAddr;
    tIgmpIPvXAddr       ReporterAddr;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    UINT4               u4IfIndex = IGMP_ZERO;
    UINT4               u4TmpAddr = IGMP_ZERO;
    UINT4               u4Source = IGMP_ZERO;
	UINT4               u4TmpOffset = IGMP_ZERO;
    UINT1               u1SrcMode = IGMP_ZERO;
    UINT1               u1DynJoinFlag = IGMP_ZERO;
	u4TmpOffset = *pu4OffSet;
    UINT1               au1TempPad[IGMP_FOUR];
    IGMP_IPVX_ADDR_CLEAR (&GrpAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcAddr);
    IGMP_IPVX_ADDR_CLEAR (&u4SrcAddr);   
    IGMP_IPVX_ADDR_CLEAR (&ReporterAddr);

    IGMP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1DynJoinFlag);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, au1TempPad, IGMP_THREE);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &GrpAddr, sizeof (tIgmpIPvXAddr));

    pIfNode = IGMP_GET_IF_NODE (u4IfIndex, GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
		*(pu4OffSet) = u4TmpOffset + IGMP_SOURCE_DB_SIZE;
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Interface %d not up - pkt dropped!!!\n", u4IfIndex);
        IGMP_DBG (IGMP_DBG_EXIT, "Exiting IgmpInput\n");
        return;
    }

    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &SrcAddr, sizeof (tIgmpIPvXAddr));
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &ReporterAddr, sizeof (tIgmpIPvXAddr));
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1SrcMode);

    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
	IGMP_IP_COPY_FROM_IPVX (&u4Source, SrcAddr, IPVX_ADDR_FMLY_IPV4);
    	IGMP_IPVX_ADDR_CLEAR (&u4SrcAddr);
    	u4TmpAddr = OSIX_NTOHL (u4Source);
    	IGMP_IPVX_ADDR_INIT_IPV4 (u4SrcAddr, IPVX_ADDR_FMLY_IPV4,
        	                      (UINT1 *) &u4TmpAddr);
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
	IGMP_IPVX_ADDR_COPY (&u4SrcAddr, &SrcAddr);
    }

    /* look for the group */
    pGrp = IgmpGrpLookUp (pIfNode, GrpAddr);
    if (pGrp == NULL)
    {
		*(pu4OffSet) = u4TmpOffset + IGMP_SOURCE_DB_SIZE;
        IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES,
                  IGMP_NAME, "Group node %d not present \n", pIfNode);
        return;
    }

    if (u1SrcMode == IGMP_ADD_SOURCE)
    {
        pSrc = IgmpAddSource (pGrp, u4SrcAddr, ReporterAddr, IGMP_FALSE);
        if (pSrc == NULL)
        {
			*(pu4OffSet) = u4TmpOffset + IGMP_SOURCE_DB_SIZE;
            IGMP_DBG1 (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES,
                      IGMP_NAME, "Source node  with source address %s addition failed \n",
		       IgmpPrintIPvxAddress(u4SrcAddr));
            return;
        }
#ifdef IGMPPRXY_WANTED
        IgpUtilUpdateSourceInfo (pGrp);
#endif
        /* Update the entries to the source */
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1FwdState);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1UpdatedToMrp);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1QryFlag);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1DynamicFlg);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1StaticFlg);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1DynSyncFlag);
        IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1SrcSSMMapped);
	pSrc->u1DynJoinFlag = u1DynJoinFlag;
    }
    else
    {
        pSrc = IgmpSourceLookup (pGrp, u4SrcAddr, ReporterAddr);
        if (pSrc != NULL)
        {
            /* Update the entries to the source */
            IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1FwdState);
            IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1UpdatedToMrp);
            IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1QryFlag);
            IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1DynamicFlg);
            IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1StaticFlg);
            IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1DynSyncFlag);
            IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pSrc->u1SrcSSMMapped);
	    pSrc->u1DynJoinFlag = u1DynJoinFlag;
            IgmpExpireSource (pSrc);
        }
    }

    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessSrcReporterInfo                   */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node source node.                */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessSrcReporterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tIgmpIface         *pIfNode = NULL;
    tIgmpIPvXAddr       GrpAddr;
    tIgmpIPvXAddr       u4SrcAddr;
    tIgmpIPvXAddr       SrcAddr;
    tIgmpIPvXAddr       ReporterAddr;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    UINT4               u4IfIndex = IGMP_ZERO;
    UINT4               u4TmpAddr = IGMP_ZERO;
    UINT4               u4Source = IGMP_ZERO;
	UINT4               u4TmpOffset = IGMP_ZERO;
    UINT1               u1RepMode = IGMP_ZERO;
    UINT1               u1GrpSrcFlag = IGMP_SOURCE_REPORTER;
    UINT1               au1TempPad[IGMP_TWO];

	u4TmpOffset = *pu4OffSet;
    IGMP_IPVX_ADDR_CLEAR (&GrpAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcAddr);
    IGMP_IPVX_ADDR_CLEAR (&ReporterAddr);

    IGMP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &GrpAddr, sizeof (tIgmpIPvXAddr));

    pIfNode = IGMP_GET_IF_NODE (u4IfIndex, GrpAddr.u1Afi);
    if (pIfNode == NULL)
    {
		*(pu4OffSet) = u4TmpOffset + IGMP_REPORTER_DB_SIZE;
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Interface %d not up - pkt dropped!!!\n", u4IfIndex);
        IGMP_DBG (IGMP_DBG_EXIT, "Exiting IgmpInput\n");
        return;
    }

    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &SrcAddr, sizeof (tIgmpIPvXAddr));
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &ReporterAddr, sizeof (tIgmpIPvXAddr));
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1RepMode);
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1GrpSrcFlag);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, au1TempPad, IGMP_TWO);

    /* look for the group */
    pGrp = IgmpGrpLookUp (pIfNode, GrpAddr);
    if (pGrp == NULL)
    {
		pGrp = IgmpAddGroup (pIfNode, GrpAddr, IGMP_FALSE);
		if(pGrp == NULL)
		{
			*(pu4OffSet) = u4TmpOffset + IGMP_REPORTER_DB_SIZE;
			IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_ALL_MODULES,
					IGMP_NAME, "Group node not present \n");
			IGMP_TRC (MGMD_TRC_FLAG, IGMP_CNTRL_MODULE,
					IgmpTrcGetModuleName(IGMP_CNTRL_MODULE), "Group node not present \n");
			return;
		}
    }

    if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
	IGMP_IP_COPY_FROM_IPVX (&u4Source, SrcAddr, IPVX_ADDR_FMLY_IPV4);
    	IGMP_IPVX_ADDR_CLEAR (&u4SrcAddr);
    	u4TmpAddr = OSIX_NTOHL (u4Source);
    	IGMP_IPVX_ADDR_INIT_IPV4 (u4SrcAddr, IPVX_ADDR_FMLY_IPV4,
        	                      (UINT1 *) &u4TmpAddr);
    }
    else if (SrcAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
	IGMP_IPVX_ADDR_COPY (&u4SrcAddr, &SrcAddr);
    }

    if (u1RepMode == IGMP_ADD_REPORTER)
    {
		if(u1GrpSrcFlag == IGMP_SOURCE_REPORTER)
		{
			pSrc = IgmpAddSource (pGrp, u4SrcAddr, ReporterAddr, IGMP_FALSE);
			if (pSrc == NULL)
			{
				*(pu4OffSet) = u4TmpOffset + IGMP_REPORTER_DB_SIZE;
				IGMP_DBG_INFO (MGMD_DBG_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES,
						IGMP_NAME, "Source node addition failed \n");
				IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
						"Source node addition failed \n");
				return;
			}
		}
		else if(u1GrpSrcFlag == IGMP_GROUP_REPORTER)
		{
			if(IgmpAddV1V2Reporter(pGrp, ReporterAddr) == IGMP_NOT_OK)
			{
				IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_GRP_MODULE, IGMP_NAME,
						"Reporter (%s) addition failed for Group %s added in i/f %d \n",
						IgmpPrintIPvxAddress (ReporterAddr), IgmpPrintIPvxAddress (pGrp->u4Group),
						pIfNode->u4IfIndex);
			}
		}
    }
    else
    {
		if(u1GrpSrcFlag == IGMP_SOURCE_REPORTER)
        {
			pSrc = IgmpGetSource (pGrp, SrcAddr);
        	if (pSrc != NULL)
        	{
            	IgmpDeleteReporter (pIfNode, pSrc, ReporterAddr);
        	}
		}
		else if(u1GrpSrcFlag == IGMP_GROUP_REPORTER)
		{
			IgmpDeleteV1V2GrpReporter(pGrp, ReporterAddr);
		}
    }

    return;
}

/************************************************************************/
/* Function Name      : IgmpRedProcessQueryInfo                         */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node query node.                 */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgmpRedProcessQueryInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tIgmpIface         *pIfNode = NULL;
    tIgmpIPvXAddr       QuerierAddr;
    tIgmpQuery         *pIgmpQuery = NULL;
    UINT4               u4IfIndex = IGMP_ZERO;
	UINT4               u4TmpOffset = IGMP_ZERO;
    UINT1               au1TempPad[IGMP_THREE];

	u4TmpOffset = *pu4OffSet;
    IGMP_IPVX_ADDR_CLEAR (&QuerierAddr);

    IGMP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &QuerierAddr, sizeof (tIgmpIPvXAddr));

    pIfNode = IGMP_GET_IF_NODE (u4IfIndex, QuerierAddr.u1Afi);
    if (pIfNode == NULL)
    {
		*(pu4OffSet) = u4TmpOffset + IGMP_QUERY_DB_SIZE;
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Interface %d not up - pkt dropped!!!\n", u4IfIndex);
        IGMP_DBG (IGMP_DBG_EXIT, "Exiting IgmpInput\n");
        return;
    }

    pIgmpQuery = pIfNode->pIfaceQry;
    /* update querier address */

    IGMP_IPVX_ADDR_COPY (&(pIgmpQuery->u4QuerierAddr), &QuerierAddr);
    /* Update the entries to the querier node */
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pIgmpQuery->u1Querier);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, au1TempPad, IGMP_THREE);

    return;
}

#ifdef IGMPPRXY_WANTED
/************************************************************************/
/* Function Name      : IgpRedProcessUpIntInfo                          */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node upstream interface node.    */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgpRedProcessUpIntInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    UINT4               u4IfIndex = IGMP_ZERO;
    UINT1               au1TempPad[IGMP_THREE];

    IGMP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    if (gIgmpProxyInfo.pUpIfaceTbl == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Upstream interfaces are not present \r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Upstream interfaces are not present \r\n");
        return;
    }

    if (IgpUpIfGetUpIfaceEntry (u4IfIndex, &pIgpUpIfaceEntry) != IGMP_SUCCESS)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Upstream interface %d entry not present\r\n", u4IfIndex);
        return;
    }

    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, pIgpUpIfaceEntry->u1OperVersion);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, au1TempPad, IGMP_THREE);

    return;
}

/*****************************************************************************/
/* Function Name      : IgpRedProcessInfo                                    */
/*                                                                           */
/* Description        : This function decodes the sync-up message from       */
/*            Active node and updates the binding information      */
/*             at the Standby node global info.             */
/*                                                                           */
/* Input(s)           : pMsg - RM Sync-up message                            */
/*                      u2RemMsgLen - Length of value of Message             */
/*                                                                           */
/* Output(s)          : u4OffSet - OffSet value                              */
/*                                                                           */
/* Return Value(s)    : None                                         */
/*****************************************************************************/

VOID
IgpRedProcessInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2PktOffset = IGMP_ZERO;
    UINT2               u2MaxUpIfCount = IGMP_ZERO;
    UINT1               u1V1UpstrmCnt = IGMP_ZERO;
    UINT1               u1V2UpstrmCnt = IGMP_ZERO;
    UINT1               u1V3UpstrmCnt = IGMP_ZERO;
    UINT1               u1NoOfGrpRecords = IGMP_ZERO;

    IGMP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2PktOffset);
    IGMP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2MaxUpIfCount);
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1V1UpstrmCnt);
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1V2UpstrmCnt);
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1V3UpstrmCnt);
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1NoOfGrpRecords);

    gIgmpProxyInfo.u2MaxUpIfCount = u2MaxUpIfCount;
    gIgmpProxyInfo.u2PktOffset = u2PktOffset;
    gIgmpProxyInfo.u1V1UpstrmCnt = u1V1UpstrmCnt;
    gIgmpProxyInfo.u1V2UpstrmCnt = u1V2UpstrmCnt;
    gIgmpProxyInfo.u1V3UpstrmCnt = u1V3UpstrmCnt;
    gIgmpProxyInfo.u1NoOfGrpRecords = u1NoOfGrpRecords;

    return;
}

/*****************************************************************************/
/* Function Name      : IgpRedUtilGetFwdEntry                                */
/*                                                                           */
/* Description        : This function gets the multicast forwarding entry    */
/*                                                                           */
/* Input(s)           : u4GrpAddr  - Group IP address                        */
/*                      u4SrcAddr  - Source IP address                       */
/*                                                                           */
/* Output(s)          : pointer to the forwarding entry or NULL              */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
PRIVATE INT4
IgpRedUtilGetFwdEntry (tIgmpIPvXAddr u4GrpAddr, tIgmpIPvXAddr u4SrcAddr,
                       tIgpRedTable ** ppRetIgpRedInfo)
{
    tRBElem            *pRBElem = NULL;
    tIgpRedTable        IgpRedInfo;

    MEMSET (&IgpRedInfo, 0, sizeof (tIgpRedTable));

    IGMP_IPVX_ADDR_COPY (&(IgpRedInfo.SrcAddr), &u4SrcAddr);
    IGMP_IPVX_ADDR_COPY (&(IgpRedInfo.GrpAddr), &u4GrpAddr);

    pRBElem = RBTreeGet (gIgpRedTable, (tRBElem *) & IgpRedInfo);

    if (pRBElem == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Multicast forwarding Entry not found\r\n");
        IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                  "Multicast forwarding Entry not found\r\n");
        return IGMP_FAILURE;
    }
    else
    {
        *ppRetIgpRedInfo = (tIgpRedTable *) pRBElem;
    }

    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpRedUtilBuildForwardEntry                          */
/*                                                                           */
/* Description        : This function builds the multicast forwarding entry  */
/*                                                                           */
/* Input(s)           : u4GrpAddr  - Group IP address                        */
/*                      u4SrcAddr  - Source IP address                       */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pointer to the forwarding entry or NULL              */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
PRIVATE INT4
IgpRedUtilBuildForwardEntry (tIgmpIPvXAddr GrpAddr, tIgmpIPvXAddr SrcAddr,
                             UINT4 u4IfIndex, tPortListExt OifPortList,
                             tIgmpPrxyFwdEntry ** pRetIgpForwardEntry)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    UINT4               u4Status = IGMP_ZERO;
    UINT2               u2Count = IGMP_ZERO;
    BOOL1               bResult = OSIX_FALSE;

    if (IgpFwdGetForwardEntry (GrpAddr, SrcAddr,
                               &pIgpForwardEntry) != IGMP_SUCCESS)
    {
        /* Multicast forwarding entry not found, so create a new one */
        if (IGMP_MEM_ALLOCATE (IGP_FWD_MEMPOOL_ID,
                               pIgpForwardEntry, tIgmpPrxyFwdEntry) == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
			   IgmpGetModuleName(IGMP_OS_RES_MODULE),
                           "Memory allocation for multicast forward"
                           "entry creation failed \r\n");
            IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
		      IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "Memory allocation for multicast forward"
                      " entry creation failed \r\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                             IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
			    "for mempool id");
            return IGMP_FAILURE;
        }

        MEMSET (pIgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));
        MEMSET (pIgpForwardEntry->McFwdPortList, IGMP_ZERO,
                sizeof (tPortListExt));
        /* Update the fields of the forwarding entry */
        IGMP_IPVX_ADDR_COPY (&(pIgpForwardEntry->u4SrcIpAddr), &SrcAddr);
        IGMP_IPVX_ADDR_COPY (&(pIgpForwardEntry->u4GrpIpAddr), &GrpAddr);
        OsixGetSysTime (&(pIgpForwardEntry->u4RouteUpTime));    /*Store the uptime */
        TMO_SLL_Init (&pIgpForwardEntry->OifList);
        /* Initialise the forwarding entry DB Node */
        IgmpRedDbNodeInit (&(pIgpForwardEntry->FwdDbNode),
                           IGP_RED_FWDENTRY_INFO);

        u4Status = RBTreeAdd (gIgmpProxyInfo.ForwardEntry,
                              (tRBElem *) pIgpForwardEntry);

        if (u4Status == RB_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "RBTree Addition Failed for forwarding Entry \r\n");
            IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                      "RBTree Addition Failed for forwarding Entry \r\n");
	    SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                                IGMP_NAME,IgmpSysErrString[SYS_LOG_RB_TREE_ADD_FAIL],
				"for forwarding Entry\r\n");
            IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
            return IGMP_FAILURE;
        }
    }

    pIgpForwardEntry->u4IfIndex = u4IfIndex;

    /* Delete the Oif nodes */
    while ((pIgpOifNode = (tIgmpPrxyOif *)
            TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
    {
        TMO_SLL_Delete (&pIgpForwardEntry->OifList, &pIgpOifNode->NextOifNode);
        IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
    }

    /* Add the Oif nodes */
    for (u2Count = 1; u2Count < (8 * sizeof (tPortListExt)); u2Count++)
    {
        OSIX_BITLIST_IS_BIT_SET (OifPortList, (UINT2) (u2Count),
                                 sizeof (tPortListExt), bResult);

        if (bResult == OSIX_TRUE)
        {
            if (IgpFwdUpdateOifNode (pIgpForwardEntry, (UINT4) (u2Count - 1),
                                     IGP_ADD_OIF) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Addition of Oif to existing route"
                          " entry failed\r\n");
                SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_CNTRL_MODULE,
                                  IGMP_NAME,IgmpSysErrString[SYS_LOG_UPD_ROUTE_FAIL],
				"while adding Oif\r\n");
		RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                              (tRBElem *) pIgpForwardEntry);
                IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
                return IGMP_FAILURE;
            }
        }
    }
    *pRetIgpForwardEntry = pIgpForwardEntry;

    return IGMP_SUCCESS;
}

/************************************************************************/
/* Function Name      : IgpRedProcessFwdEntryInfo                       */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node forward entry node.         */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
IgpRedProcessFwdEntryInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tIgpRedTable       *pIgpRedInfo = NULL;
    tIgmpIPvXAddr       GrpAddr;
    tIgmpIPvXAddr       SrcAddr;
    tPortListExt       *pOifPortList = NULL;
    UINT4               u4IfIndex = IGMP_ZERO;
    UINT4               u4Status = IGMP_ZERO;
    UINT1               u1HwStatus = IGMP_ZERO;
    UINT1               u1RouteStatus = IGMP_ZERO;
    UINT1               au1TempPad[IGMP_TWO];

    IGMP_IPVX_ADDR_CLEAR (&GrpAddr);
    IGMP_IPVX_ADDR_CLEAR (&SrcAddr);

    pOifPortList = (tPortListExt *) FsUtilAllocBitList (sizeof (tPortListExt));
    if (pOifPortList == NULL)
    {
        return;
    }
    MEMSET (*pOifPortList, 0, sizeof (tPortListExt));

    IGMP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &SrcAddr, sizeof (tIgmpIPvXAddr));
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, &GrpAddr, sizeof (tIgmpIPvXAddr));
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, (*pOifPortList),
                        sizeof (tPortListExt));
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1HwStatus);
    IGMP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1RouteStatus);
    IGMP_RM_GET_N_BYTE (pMsg, pu4OffSet, au1TempPad, IGMP_TWO);

    if (u1HwStatus == NP_PRESENT)
    {
        if (IgpRedUtilGetFwdEntry (GrpAddr, SrcAddr,
                                   &pIgpRedInfo) == IGMP_SUCCESS)
        {
            RBTreeRem (gIgpRedTable, pIgpRedInfo);
            MemReleaseMemBlock (IGP_DYN_MSG_MEMPOOL_ID, (UINT1 *) pIgpRedInfo);
        }
        if (u1RouteStatus == IGP_ADD_ROUTE)
        {
            u4Status = IgpRedUtilBuildForwardEntry (GrpAddr, SrcAddr, u4IfIndex,
                                                    *pOifPortList,
                                                    &pIgpForwardEntry);
            if (u4Status == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Failed for forwarding Entry creation\r\n");
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "Failed for forwarding Entry creation\r\n");
                FsUtilReleaseBitList ((UINT1 *) pOifPortList);
                return;
            }
            pIgpForwardEntry->u1RouteStatus = u1RouteStatus;
            pIgpForwardEntry->u1HwStatus = u1HwStatus;
        }
        else
        {
            if ((IgpFwdGetForwardEntry (GrpAddr, SrcAddr, &pIgpForwardEntry))
                != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "multicast forward entry not present \r\n");
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "multicast forward entry not present \r\n");
                FsUtilReleaseBitList ((UINT1 *) pOifPortList);
                return;
            }
            /* Delete the Oif nodes */
            while ((pIgpOifNode = (tIgmpPrxyOif *)
                    TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
            {
                TMO_SLL_Delete (&pIgpForwardEntry->OifList,
                                &pIgpOifNode->NextOifNode);
                IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
            }

            RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                          (tRBElem *) pIgpForwardEntry);
            IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
            gIgmpProxyInfo.u2MaxHwEntryCnt--;
        }
    }

    else if (u1HwStatus == NP_NOT_PRESENT)
    {
        if (IgpRedUtilGetFwdEntry (GrpAddr, SrcAddr,
                                   &pIgpRedInfo) != IGMP_SUCCESS)
        {
            /* Multicast forwarding entry not found, so create a new one */
            if (IGMP_MEM_ALLOCATE (IGP_DYN_MSG_MEMPOOL_ID,
                                   pIgpRedInfo, tIgpRedTable) == NULL)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
				IgmpGetModuleName(IGMP_OS_RES_MODULE),
                          "Memory allocation for multicast forward"
                          " entry creation failed \r\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                                      IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
					"for multicast forward entry\r\n");
                FsUtilReleaseBitList ((UINT1 *) pOifPortList);
                return;
            }

            MEMSET (pIgpRedInfo, IGMP_ZERO, sizeof (tIgpRedTable));
            MEMSET (pIgpRedInfo->McFwdPortList, IGMP_ZERO,
                    sizeof (tPortListExt));

            /* Update the fields of the forwarding entry */
            IGMP_IPVX_ADDR_COPY (&(pIgpRedInfo->SrcAddr), &SrcAddr);
            IGMP_IPVX_ADDR_COPY (&(pIgpRedInfo->GrpAddr), &GrpAddr);

            u4Status = RBTreeAdd (gIgpRedTable, (tRBElem *) pIgpRedInfo);

            if (u4Status == RB_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "RBTree Addition Failed for forwarding Entry \r\n");
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "RBTree Addition Failed for forwarding Entry \r\n");
                SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_CNTRL_MODULE,
                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_RB_TREE_ADD_FAIL],
					"for multicast forward entry\r\n");
		IGP_RELEASE_MEM_BLOCK (IGP_DYN_MSG_MEMPOOL_ID, pIgpRedInfo);
                FsUtilReleaseBitList ((UINT1 *) pOifPortList);
                return;
            }
        }
        pIgpRedInfo->u4IfIndex = u4IfIndex;
        pIgpRedInfo->u1RouteStatus = u1RouteStatus;
        MEMCPY ((pIgpRedInfo->McFwdPortList), *pOifPortList,
                sizeof (tPortListExt));
    }
    FsUtilReleaseBitList ((UINT1 *) pOifPortList);
    return;
}

/************************************************************************
 * Function Name      : IgmpRedHwAudit                                  
 *                                                                      
 * Description        : This function does the hardware audit in the    
 *                      following approach.                             
 *                                                                      
 *                      When there is a transaction between standby and 
 *                      active node, the IgmpRedTable is walked, if     
 *                      there  are any entries in the table, they are   
 *                      verified with the hardware, if the entry is     
 *                      present in the hardware, then the entry is      
 *                      added to the sofware. If not, the entry is      
 *                      deleted.                                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/
VOID
IgmpRedHwAudit (VOID)
{
#if NPAPI_WANTED
    tIgpRedTable       *pIgpRedInfo = NULL;
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tMcRtEntry          rtEntry;
    UINT4               u4Status = IGMP_ZERO;
    UINT4               u4IfIndex = 0;
    UINT1               u1NpRetStatus = FNP_FAILURE;

    pIgpRedInfo = RBTreeGetFirst (gIgpRedTable);
    while (pIgpRedInfo != NULL)
    {
        /* Query NP to retrieve the corresponding route */
        if (IgpNpGetFwdEntry (pIgpRedInfo) != IGP_FAILURE)
        {
            u1NpRetStatus = FNP_SUCCESS;
        }
        if ((pIgpRedInfo->u1RouteStatus == IGP_ADD_ROUTE) &&
            (u1NpRetStatus == FNP_SUCCESS))
        {
            u4Status = IgpRedUtilBuildForwardEntry (pIgpRedInfo->GrpAddr,
                                                    pIgpRedInfo->SrcAddr,
                                                    pIgpRedInfo->u4IfIndex,
                                                    pIgpRedInfo->McFwdPortList,
                                                    &pIgpForwardEntry);

            if (u4Status != IGMP_FAILURE)
            {
                pIgpForwardEntry->u1HwStatus = NP_PRESENT;
            }
            else
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Failed for forwarding Entry creation\r\n");
                IGMP_TRC (IGMP_TRC_FLAG, IGMP_CNTRL_MODULE, 
			  IgmpTrcGetModuleName(IGMP_CNTRL_MODULE),
                          "Failed for forwarding Entry creation\r\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                                        IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
					"for multicast forward entry\r\n");
                MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));
                FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

                IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpRedInfo->u4IfIndex,
                                               &u4IfIndex);
                rtEntry.u2RpfIf = (UINT2) u4IfIndex;
                IgpUtilGetVlanIdFromIfIndex (pIgpRedInfo->u4IfIndex,
                                             &rtEntry.u2VlanId);

                IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4SrcAddr),
                                        pIgpRedInfo->SrcAddr,
                                        IPVX_ADDR_FMLY_IPV4);
                IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4GrpAddr),
                                        pIgpRedInfo->GrpAddr,
                                        IPVX_ADDR_FMLY_IPV4);
                NetIpMcRouteInfo.u4Iif = pIgpRedInfo->u4IfIndex;

                IpmcFsNpIpv4McDelRouteEntry (IGMP_PROXY_ID,
                                         NetIpMcRouteInfo.u4GrpAddr,
                                         FNP_ALL_BITS_SET,
                                         NetIpMcRouteInfo.u4SrcAddr,
                                         FNP_ALL_BITS_SET, rtEntry, FNP_ZERO,
                                         NULL);
            }
        }
        else if ((pIgpRedInfo->u1RouteStatus == IGP_DELETE_ROUTE) &&
                 (u1NpRetStatus == FNP_FAILURE))
        {
            if ((IgpFwdGetForwardEntry
                 (pIgpRedInfo->GrpAddr, pIgpRedInfo->SrcAddr,
                  &pIgpForwardEntry)) == IGMP_SUCCESS)
            {
                /* Delete the Oif nodes */
                while ((pIgpOifNode = (tIgmpPrxyOif *)
                        TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
                {
                    TMO_SLL_Delete (&pIgpForwardEntry->OifList,
                                    &pIgpOifNode->NextOifNode);
                    IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
                }

                RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                              (tRBElem *) pIgpForwardEntry);
                IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
            }
        }
        RBTreeRem (gIgpRedTable, pIgpRedInfo);
        MemReleaseMemBlock (IGP_DYN_MSG_MEMPOOL_ID, (UINT1 *) pIgpRedInfo);
        pIgpRedInfo = RBTreeGetFirst (gIgpRedTable);
        u1NpRetStatus = FNP_FAILURE;
    }

#endif
    return;
}

/*-------------------------------------------------------------------+
 * Function           : IgmpRBTreeRedEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : IGMP_RB_LESSER  if pRBElem <  pRBElemIn
 *                      IGMP_RB_GREATER if pRBElem >  pRBElemIn
 *                      IGMP_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy cache entries in lexicographic
 * order of the indices of the Redundancy cache entry.
+-------------------------------------------------------------------*/

INT4
IgmpRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tIgpRedTable       *pIgpRed = pRBElem;
    tIgpRedTable       *pIgpRedIn = pRBElemIn;
    INT4                i4RetVal = IGMP_ZERO;

    /* key = GrpAddr + SrcAddr */

    /* The Group address of the nodes are compared */
    i4RetVal = IPVX_ADDR_COMPARE (pIgpRed->GrpAddr, pIgpRedIn->GrpAddr);
    if (i4RetVal > IGMP_ZERO)
    {
        return IGMP_RB_GREATER;
    }
    else if (i4RetVal < IGMP_ZERO)
    {
        return IGMP_RB_LESSER;
    }
    /* As the Grp addresses are the same, The Src address of the
     *        nodes are compared */
    i4RetVal = IPVX_ADDR_COMPARE (pIgpRed->SrcAddr, pIgpRedIn->SrcAddr);
    if (i4RetVal > IGMP_ZERO)
    {
        return IGMP_RB_GREATER;
    }
    else if (i4RetVal < IGMP_ZERO)
    {
        return IGMP_RB_LESSER;
    }

    return IGMP_RB_EQUAL;
}

#endif

#endif
