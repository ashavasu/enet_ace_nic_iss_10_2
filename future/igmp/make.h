#
# Copyright (C) 2010 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.8 2014/12/18 12:12:54 siva Exp $            
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 11 December 2005                              |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the warning options    |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+


#Project base directories
PROJECT_NAME  = FutureIGMP
PROJECT_BASE_DIR        = ${BASE_DIR}/igmp
PROJECT_INC_DIR         = $(PROJECT_BASE_DIR)/inc
PROJECT_MFWD_DIR        = $(BASE_DIR)/mfwd
IGMP_MAKE_H              = make.h
IGMP_MAKEFILE            = Makefile

PROJECT_OBJECT_DIR     = $(PROJECT_BASE_DIR)/obj
PROJECT_SOURCE_DIR     = $(PROJECT_BASE_DIR)/src

PROJECT_FINAL_OBJ  = $(PROJECT_NAME).o
IGMP_INCLUDE_DIRS = -I$(PROJECT_INC_DIR)

ifeq (${IGMPROXY}, YES)
PROJECT_NAME       = FutureIGP
PROXY_BASE_DIR        = ${PROJECT_BASE_DIR}/proxy
PROXY_INC_DIR         = $(PROXY_BASE_DIR)/inc
PROXY_SOURCE_DIR      = $(PROXY_BASE_DIR)/src
PROXY_OBJECT_DIR      = $(PROXY_BASE_DIR)/obj
PROXY_INCLUDE_DIRS    = -I$(PROJECT_INC_DIR) -I$(PROXY_INC_DIR) \
                        -I$(PROJECT_MFWD_DIR)
PROJECT_FINAL_OBJ     = $(PROJECT_NAME).o
endif


# List of include files of IGMP

IGMP_INC_FILES = $(PROJECT_INC_DIR)/igmpdefs.h \
   $(PROJECT_INC_DIR)/igmpextn.h \
   $(PROJECT_INC_DIR)/igmpglob.h \
   $(PROJECT_INC_DIR)/igmpinc.h \
   $(PROJECT_INC_DIR)/igmpred.h \
   $(PROJECT_INC_DIR)/igmpipdf.h \
   $(PROJECT_INC_DIR)/igmpport.h \
   $(PROJECT_INC_DIR)/igmpprot.h  \
   $(PROJECT_INC_DIR)/igmptdfs.h  \
   $(PROJECT_INC_DIR)/fsigmlow.h  \
   $(PROJECT_INC_DIR)/stdigmlow.h  \
   $(PROJECT_INC_DIR)/fsmgmdlw.h  \
   $(PROJECT_INC_DIR)/stdmgmlw.h  \
   $(PROJECT_INC_DIR)/igmptrc.h  \
   $(PROJECT_INC_DIR)/igmpsz.h

IGMP_SRC_FILES = $(PROJECT_SOURCE_DIR)/fsipif.c     \
   $(PROJECT_SOURCE_DIR)/igmpmain.c   \
   $(PROJECT_SOURCE_DIR)/igmpfast.c   \
   $(PROJECT_SOURCE_DIR)/igmpfilt.c   \
   $(PROJECT_SOURCE_DIR)/igmpfsm.c    \
   $(PROJECT_SOURCE_DIR)/igmpgrp.c    \
   $(PROJECT_SOURCE_DIR)/igmpinit.c   \
   $(PROJECT_SOURCE_DIR)/igmpmem.c    \
   $(PROJECT_SOURCE_DIR)/igmpport.c   \
   $(PROJECT_SOURCE_DIR)/igmpqury.c   \
   $(PROJECT_SOURCE_DIR)/igmpsrc.c    \
   $(PROJECT_SOURCE_DIR)/igmptmr.c    \
   $(PROJECT_SOURCE_DIR)/fsigmset.c   \
   $(PROJECT_SOURCE_DIR)/fsigmtst.c   \
   $(PROJECT_SOURCE_DIR)/stutlget.c   \
   $(PROJECT_SOURCE_DIR)/stutlset.c   \
   $(PROJECT_SOURCE_DIR)/stutltst.c   \
   $(PROJECT_SOURCE_DIR)/fsutlget.c   \
   $(PROJECT_SOURCE_DIR)/fsutlset.c   \
   $(PROJECT_SOURCE_DIR)/fsutltst.c   \
   $(PROJECT_SOURCE_DIR)/stdigget.c   \
   $(PROJECT_SOURCE_DIR)/stdigset.c   \
   $(PROJECT_SOURCE_DIR)/stdigtst.c   \
   $(PROJECT_SOURCE_DIR)/stdmgmlw.c   \
   $(PROJECT_SOURCE_DIR)/fsmgmdlw.c   \
   $(PROJECT_SOURCE_DIR)/igmputil.c   \
   $(PROJECT_SOURCE_DIR)/fsigmget.c   \
   $(PROJECT_SOURCE_DIR)/igmpdbg.c   \
   $(PROJECT_SOURCE_DIR)/igmpout.c   \
   $(PROJECT_SOURCE_DIR)/igmpin.c   \
   $(PROJECT_SOURCE_DIR)/mldport.c   \
   $(PROJECT_SOURCE_DIR)/igmpsz.c 

IGMP_OBJ_LIST = $(PROJECT_OBJECT_DIR)/igmpmain.o    \
  $(PROJECT_OBJECT_DIR)/igmpmem.o     \
  $(PROJECT_OBJECT_DIR)/igmpport.o    \
  $(PROJECT_OBJECT_DIR)/igmpqury.o    \
  $(PROJECT_OBJECT_DIR)/igmpsrc.o     \
  $(PROJECT_OBJECT_DIR)/igmptmr.o     \
  $(PROJECT_OBJECT_DIR)/igmputil.o    \
  $(PROJECT_OBJECT_DIR)/igmpfast.o    \
  $(PROJECT_OBJECT_DIR)/igmpfilt.o    \
  $(PROJECT_OBJECT_DIR)/igmpfsm.o     \
  $(PROJECT_OBJECT_DIR)/igmpgrp.o     \
  $(PROJECT_OBJECT_DIR)/igmpinit.o    \
  $(PROJECT_OBJECT_DIR)/fsipif.o      \
  $(PROJECT_OBJECT_DIR)/fsigmget.o    \
  $(PROJECT_OBJECT_DIR)/fsigmset.o    \
  $(PROJECT_OBJECT_DIR)/fsigmtst.o    \
  $(PROJECT_OBJECT_DIR)/stutlget.o    \
  $(PROJECT_OBJECT_DIR)/stutlset.o    \
  $(PROJECT_OBJECT_DIR)/stutltst.o    \
  $(PROJECT_OBJECT_DIR)/fsutlget.o    \
  $(PROJECT_OBJECT_DIR)/fsutlset.o    \
  $(PROJECT_OBJECT_DIR)/fsutltst.o    \
  $(PROJECT_OBJECT_DIR)/stdigget.o    \
  $(PROJECT_OBJECT_DIR)/stdigset.o    \
  $(PROJECT_OBJECT_DIR)/stdigtst.o    \
  $(PROJECT_OBJECT_DIR)/stdmgmlw.o    \
  $(PROJECT_OBJECT_DIR)/fsmgmdlw.o    \
  $(PROJECT_OBJECT_DIR)/igmpdbg.o    \
  $(PROJECT_OBJECT_DIR)/igmpout.o  \
  $(PROJECT_OBJECT_DIR)/igmpin.o  \
  $(PROJECT_OBJECT_DIR)/igmpsz.o
  #$(PROJECT_OBJECT_DIR)/mldport.o  \
  $(PROJECT_OBJECT_DIR)/igmpsz.o
                
MLD_SRC_FILES = $(PROJECT_SOURCE_DIR)/mldport.c    \
   $(PROJECT_SOURCE_DIR)/mldin.c   \
   $(PROJECT_SOURCE_DIR)/mldout.c \
   $(PROJECT_SOURCE_DIR)/stdmllow.c \
   $(PROJECT_SOURCE_DIR)/stdmldwr.c \
   $(PROJECT_SOURCE_DIR)/fsmldlow.c \
   $(PROJECT_SOURCE_DIR)/fsmldwr.c

MLD_OBJ_LIST = $(PROJECT_OBJECT_DIR)/mldport.o    \
  $(PROJECT_OBJECT_DIR)/mldin.o  \
  $(PROJECT_OBJECT_DIR)/mldout.o \
  $(PROJECT_OBJECT_DIR)/stdmllow.o \
  $(PROJECT_OBJECT_DIR)/stdmldwr.o \
  $(PROJECT_OBJECT_DIR)/fsmldlow.o \
  $(PROJECT_OBJECT_DIR)/fsmldwr.o

MLD_INC_FILES = $(PROJECT_INC_DIR)/mlddefs.h \
   $(PROJECT_INC_DIR)/mldprot.h \
   $(PROJECT_INC_DIR)/stdmllow.h  \
   $(PROJECT_INC_DIR)/stdmldwr.h  \
   $(PROJECT_INC_DIR)/fsmldlow.h  \
   $(PROJECT_INC_DIR)/fsmldwr.h

ifeq (${IGMPROXY}, YES)
PROXY_INC_FILES = $(PROXY_INC_DIR)/igpdefn.h  \
    $(PROXY_INC_DIR)/igpglob.h  \
    $(PROXY_INC_DIR)/igpinc.h   \
    $(PROXY_INC_DIR)/igpprot.h  \
    $(PROXY_INC_DIR)/igptdfs.h  \
    $(PROXY_INC_DIR)/igpmacs.h  \
    $(PROXY_INC_DIR)/fsigplw.h  \
    $(PROXY_INC_DIR)/igpsz.h  
                 
PROXY_SRC_FILES = $(PROXY_SOURCE_DIR)/igpfwd.c       \
    $(PROXY_SOURCE_DIR)/igpgrp.c       \
    $(PROXY_SOURCE_DIR)/igpmain.c      \
    $(PROXY_SOURCE_DIR)/igpupif.c      \
    $(PROXY_SOURCE_DIR)/igpport.c      \
    $(PROXY_SOURCE_DIR)/igptmr.c       \
    $(PROXY_SOURCE_DIR)/igputil.c      \
    $(PROXY_SOURCE_DIR)/fsigplw.c      \
    $(PROXY_SOURCE_DIR)/igpsz.c      

PROXY_OBJ_LIST = $(PROXY_OBJECT_DIR)/igpfwd.o        \
   $(PROXY_OBJECT_DIR)/igpgrp.o        \
   $(PROXY_OBJECT_DIR)/igpmain.o       \
   $(PROXY_OBJECT_DIR)/igpupif.o       \
   $(PROXY_OBJECT_DIR)/igpport.o       \
   $(PROXY_OBJECT_DIR)/igptmr.o        \
   $(PROXY_OBJECT_DIR)/igputil.o       \
   $(PROXY_OBJECT_DIR)/fsigplw.o       \
   $(PROXY_OBJECT_DIR)/igpsz.o         
endif

ifeq (${L2RED}, YES)
IGMP_SRC_FILES += $(PROJECT_SOURCE_DIR)/igmpred.c
IGMP_OBJ_LIST +=  $(PROJECT_OBJECT_DIR)/igmpred.o
else
IGMP_OBJ_LIST +=  $(PROJECT_OBJECT_DIR)/igmpstb.o
endif

ifeq (${IGMPROXY}, NO)
PROXY_OBJ_LIST += $(PROJECT_OBJECT_DIR)/igpstb.o     
endif

ifeq ($(MBSM), YES)
IGMP_SRC_FILES+= $(PROJECT_SOURCE_DIR)/igmpmbsm.c
IGMP_OBJ_LIST+= $(PROJECT_OBJECT_DIR)/igmpmbsm.o 
IGMP_CMN_INC_FILES+=         \
    $(BASE_DIR)/inc/mbsm.h
endif

ifeq (${CLI}, YES)
IGMP_CMN_SRC_FILES+=\
    $(PROJECT_SOURCE_DIR)/igmpcli.c

IGMP_OBJ_LIST+=\
    $(PROJECT_OBJECT_DIR)/igmpcli.o

CLI_INC = $(BASE_DIR)/inc/cli

IGMP_CMN_INC_FILES+=         \
    $(PROJECT_INC_DIR)/igmpcliprot.h \
    $(CLI_INC)/igmpcli.h
    
ifeq (${IGMPROXY}, YES)
IGMP_CMN_SRC_FILES+=\
    $(PROXY_SOURCE_DIR)/igpcli.c
IGMP_OBJ_LIST+=\
    $(PROXY_OBJECT_DIR)/igpcli.o
IGMP_CMN_INC_FILES+=         \
    $(PROJECT_INC_DIR)/igpclipt.h \
    $(CLI_INC)/igpcli.h
endif

IGMP_CMN_SRC_FILES+=\
    $(PROJECT_SOURCE_DIR)/mldcli.c

IGMP_OBJ_LIST+=\
    $(PROJECT_OBJECT_DIR)/mldcli.o

CLI_INC = $(BASE_DIR)/inc/cli

IGMP_CMN_INC_FILES+=         \
    $(PROJECT_INC_DIR)/mldcliprot.h \
    $(CLI_INC)/mldcli.h
endif

ifeq (${NPAPI}, YES)
ifeq (${IGMP}, YES)
IGMP_SRC_FILES+= $(PROJECT_SOURCE_DIR)/igmpnpapi.c
IGMP_OBJ_LIST+= $(PROJECT_OBJECT_DIR)/igmpnpapi.o 
IGMP_SRC_FILES+= $(PROJECT_SOURCE_DIR)/igmpnpwr.c
IGMP_OBJ_LIST+= $(PROJECT_OBJECT_DIR)/igmpnpwr.o 
endif
ifeq (${IGMPROXY}, YES)
PROXY_SRC_FILES += $(PROXY_SOURCE_DIR)/igpnpapi.c
PROXY_OBJ_LIST += $(PROXY_OBJECT_DIR)/igpnpapi.o
endif
endif

ifeq (${NPAPI}, YES)
ifeq (${MLD}, YES)
MLD_SRC_FILES+= $(PROJECT_SOURCE_DIR)/mldnpapi.c
MLD_OBJ_LIST+= $(PROJECT_OBJECT_DIR)/mldnpapi.o 
MLD_SRC_FILES+= $(PROJECT_SOURCE_DIR)/mldnpwr.c
MLD_OBJ_LIST+= $(PROJECT_OBJECT_DIR)/mldnpwr.o 
endif
endif

ifeq (${MFWD}, YES)
ifeq (${IGMPROXY}, YES)
PROXY_SRC_FILES += $(PROXY_SOURCE_DIR)/igpmfwd.c
PROXY_OBJ_LIST += $(PROXY_OBJECT_DIR)/igpmfwd.o
endif
endif

ifeq (${MRI}, YES)
ifeq (${IGMPROXY}, YES)
PROXY_SRC_FILES += $(PROXY_SOURCE_DIR)/igpapi.c
PROXY_OBJ_LIST += $(PROXY_OBJECT_DIR)/igpapi.o
endif
endif

                        
ifeq (${SNMP}, YES)
IGMP_OBJ_LIST +=  $(PROJECT_OBJECT_DIR)/stdigmid.o \
                  $(PROJECT_OBJECT_DIR)/fsigmmid.o
ifeq (${IGMPROXY}, YES)
PROXY_OBJ_LIST +=  $(PROXY_OBJECT_DIR)/fsigpmid.o
endif
endif

ifeq (${SNMP_2}, YES)
IGMP_OBJ_LIST +=  $(PROJECT_OBJECT_DIR)/stdigmwr.o \
                  $(PROJECT_OBJECT_DIR)/fsigmpwr.o
ifeq (${IGMPROXY}, YES)
PROXY_OBJ_LIST +=  $(PROXY_OBJECT_DIR)/fsigpwr.o
endif
IGMP_OBJ_LIST +=  $(PROJECT_OBJECT_DIR)/stdmgmwr.o \
                  $(PROJECT_OBJECT_DIR)/fsmgmdwr.o
endif

ifeq (DIGMP_TEST_WANTED, $(findstring DIGMP_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
    IGMP_TEST_BASE_DIR  = ${BASE_DIR}/igmp/test
    IGMP_TEST_OBJ_DIR  = ${BASE_DIR}/igmp/test/obj
    IGMP_OBJ_LIST += ${IGMP_TEST_OBJ_DIR}/FutureIgmpTest.o
endif
                             
PROJECT_FINAL_INCLUDES_DIRS   = $(IGMP_INCLUDE_DIRS) \
                                $(COMMON_INCLUDE_DIRS)
        
ifeq (${IGMPROXY}, YES)
PROJECT_FINAL_INCLUDES_DIRS   += $(PROXY_INCLUDE_DIRS)
endif

PROJECT_OBJECT_LIST = $(IGMP_OBJ_LIST) \
                      $(PROXY_OBJ_LIST) \
                      $(MLD_OBJ_LIST)

#dependencies
PROJECT_COMMON_DEPENDENCIES =   $(IGMP_INC_FILES) 
        
ifeq (${IGMPROXY}, YES)
PROJECT_COMMON_DEPENDENCIES +=   $(PROXY_INC_FILES) 
endif

PROJECT_COMMON_DEPENDENCIES +=   $(MLD_INC_FILES) 
        
PROJECT_DEPENDENCIES        =   $(PROJECT_COMMON_DEPENDENCIES) \
                                $(COMMON_DEPENDENCIES)
        

PROJECT_COMPILATION_SWITCHES =-DDEBUG_WANTED
PROJECT_COMPILATION_SWITCHES += $(PROJECT_DEF_COMPILATION_SWITCHES)

