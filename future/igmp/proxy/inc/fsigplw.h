/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsigplw.h,v 1.4 2013/01/23 11:23:20 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpProxyStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIgmpProxyForwardingTblEntryCnt ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIgmpProxyStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIgmpProxyStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIgmpProxyStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIgmpProxyRtrIfaceTable. */
INT1
nmhValidateIndexInstanceFsIgmpProxyRtrIfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpProxyRtrIfaceTable  */

INT1
nmhGetFirstIndexFsIgmpProxyRtrIfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIgmpProxyRtrIfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpProxyRtrIfaceOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpProxyRtrIfaceCfgOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpProxyRtrIfacePurgeInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIgmpProxyRtrIfaceUpTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpProxyRtrIfaceExpiryTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsIgmpProxyRtrIfaceRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIgmpProxyRtrIfaceCfgOperVersion ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIgmpProxyRtrIfacePurgeInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIgmpProxyRtrIfaceRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIgmpProxyRtrIfaceCfgOperVersion ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIgmpProxyRtrIfacePurgeInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIgmpProxyRtrIfaceRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIgmpProxyRtrIfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIgmpProxyMrouteTable. */
INT1
nmhValidateIndexInstanceFsIgmpProxyMrouteTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpProxyMrouteTable  */

INT1
nmhGetFirstIndexFsIgmpProxyMrouteTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIgmpProxyMrouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpProxyMRouteIifIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsIgmpProxyMRouteUpTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsIgmpProxyMRouteExpiryTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsIgmpProxyNextHopTable. */
INT1
nmhValidateIndexInstanceFsIgmpProxyNextHopTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpProxyNextHopTable  */

INT1
nmhGetFirstIndexFsIgmpProxyNextHopTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIgmpProxyNextHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpProxyNextHopState ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIgmpProxyQuerierIfIndex ARG_LIST((INT4 *));

INT1
nmhGetFsIgmpProxyQuerierAddress ARG_LIST((UINT4 *));
