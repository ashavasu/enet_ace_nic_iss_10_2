#ifndef _IGPGLOB_H
#define _IGPGLOB_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : igpglob.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Globals                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains global variables            */
/*                            for IGMP Proxy modules                         */
/* $Id: igpglob.h,v 1.5 2013/01/23 11:23:20 siva Exp $                    */
/*---------------------------------------------------------------------------*/

tIgmpProxyInfo    gIgmpProxyInfo;      /* Global IGMP Proxy Info */
tIgmpPrxySrcInfo  gaIgpSrcInfo[IGP_MAX_MCAST_SRCS]; /* Array of source address 
                                                    for which registrations 
                                                    (Include /Exclude) have 
                                                    been for reported for this 
                                                    group */
tIgmpPrxyMemPool  gIgmpPrxyMemPool;       /* Global memory pool info */
UINT1         gau1RepSendBuffer[IGP_REP_BUF_MEMBLK_SIZE];
UINT1         *gpv3RepSendBuffer = gau1RepSendBuffer;  /* Misc buffer for contru                                                         cting  and sending IGMP                                                         v3 reports */ 

UINT1         *gpSrcInfoBuffer = NULL;    /* Src buffer for having 
                                             source information */  

tIgmPxySrcBmp    gIgpNullSrcBmp;
tIgmpIPvXAddr    gIgpIPvXZero;
tRBTree   gIgpRedTable;

/* Offset table for Upstream interface DB Nodes */
tDbOffsetTemplate gaIgmpUpIntOffsetTbl[] =
   {{(FSAP_OFFSETOF(tIgmpPrxyUpIface, u4IfIndex) -
      FSAP_OFFSETOF(tIgmpPrxyUpIface, UpIntDbNode) - sizeof(tDbTblNode)), 4},
    {-1, 0}};

/* Offset table for Forward entry DB Nodes */
tDbOffsetTemplate gaIgmpFwdOffsetTbl[] =
   {{(FSAP_OFFSETOF(tIgmpPrxyFwdEntry, u4IfIndex) -
      FSAP_OFFSETOF(tIgmpPrxyFwdEntry, FwdDbNode) - sizeof(tDbTblNode)), 4},
    {-1, 0}};

/* Offset table for Igmp Proxy info DB Nodes */
tDbOffsetTemplate gaIgmpPrxyInfoOffsetTbl[] =
   {{(FSAP_OFFSETOF(tIgmpProxyInfo, u2PktOffset) -
      FSAP_OFFSETOF(tIgmpProxyInfo, IgpInfoDbNode) - sizeof(tDbTblNode)), 2},
    {(FSAP_OFFSETOF(tIgmpProxyInfo, u2MaxUpIfCount) -
      FSAP_OFFSETOF(tIgmpProxyInfo, IgpInfoDbNode) - sizeof(tDbTblNode)), 2},
    {-1, 0}};

#endif /* _IGPGLOB_H */
