/* $Id: fsigpwr.h,v 1.3 2013/01/23 11:23:20 siva Exp $*/
#ifndef _FSIGPWR_H
#define _FSIGPWR_H

VOID RegisterFSIGP(VOID);

VOID UnRegisterFSIGP(VOID);
INT4 FsIgmpProxyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyForwardingTblEntryCntGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsIgmpProxyRtrIfaceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIgmpProxyRtrIfaceOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceCfgOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfacePurgeIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceCfgOperVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfacePurgeIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceCfgOperVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfacePurgeIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyRtrIfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsIgmpProxyMrouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIgmpProxyMRouteIifIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyMRouteUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyMRouteExpiryTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsIgmpProxyNextHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsIgmpProxyNextHopStateGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyQuerierIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsIgmpProxyQuerierAddressGet(tSnmpIndex *, tRetVal *);
#endif /* _FSIGPWR_H */
