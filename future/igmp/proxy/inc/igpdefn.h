#ifndef _IGPDEFN_H
#define _IGPDEFN_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: igpdefn.h,v 1.9 2013/12/18 12:10:29 siva Exp $                 */
/*****************************************************************************/
/*    FILE  NAME            : igpdefn.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Definitions                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Definitions                 */
/*                            for IGMP prxoy module                          */
/*---------------------------------------------------------------------------*/


#define   IGP_MAX_MCAST_SRCS          MAX_IGP_MCAST_SRCS
#define   IGP_MAX_HOSTS                10  
#define   IGP_SRC_BUF_MEMBLK_SIZE     1500

#define   IGP_MAX_MCAST_GROUPS         FsIGPSizingParams\
                                      [MAX_IGP_MCAST_GRPS_SIZING_ID].\
                                       u4PreAllocatedUnits 

#define   IGP_MAX_FWD_ENTRIES          FsIGPSizingParams\
                                       [MAX_IGP_FWD_ENTRIES_SIZING_ID].\
                                       u4PreAllocatedUnits

#define   IGP_MAX_RTR_IFACE            FsIGPSizingParams\
                                       [MAX_IGP_RTR_IFACES_SIZING_ID].\
                                       u4PreAllocatedUnits

#define   IGP_MAX_OIF                  FsIGPSizingParams\
                                       [MAX_IGP_MAX_OIFS_SIZING_ID].\
                                       u4PreAllocatedUnits


#define   IGP_ADD_ROUTE   1
#define   IGP_DELETE_ROUTE  2
/* IGMP Proxy Timer related definitions */
#define   IGP_FWD_ENTRY_TIMER              10
#define   IGP_RTR_IFACE_TIMER              11

/* Values for the different timer intervals */
#define   IGP_MIN_RTR_IFACE_INTERVAL     60 
#define   IGP_MAX_RTR_IFACE_INTERVAL     600
#define   IGP_DEF_RTR_IFACE_INTERVAL     125
         
/* Values used while sending packet out */
#define   IGP_LEAVE_MESSAGE              1
#define   IGP_SEND_V1V2_REPORT           2
#define   IGP_DONT_SEND_V1V2_REPORT      3
#define   IGP_SEND_V3_REPORT             4
#define   IGP_DONT_SEND_V3_REPORT        5
         
#ifdef NPAPI_WANTED
#define   IGP_FWD_ENTRY_TIMER_VAL        75
#else
#define   IGP_FWD_ENTRY_TIMER_VAL        150
#endif


#define   IGMP_PROXY_ID                  257
#define   IGP_IP_ADDR_SIZE               sizeof (UINT4)
#define   IGP_IP6_ADDR_SIZE              16
#define   IGP_OBJ_NAME_LEN               257
#define   IGP_TRAP_OID_LEN               10

/* NPAPI Defintions */
#define   IGP_CREATE_FWD_ENTRY           1
#define   IGP_DELETE_FWD_ENTRY           2
#define   IGP_ADD_OIF                    3
#define   IGP_DEL_OIF                    4
#define   IGP_FWD_TMR_FIRST_HALF         1
#define   IGP_FWD_TMR_SECOND_HALF        2

/* MFWD Definitions */
#define  IGP_MFWD_DELIVER_MDP      1
#define  IGP_MFWD_DONT_DELIVER_MDP 2
#define  IGP_MFWD_DELIVER_FULL_MDP 3
#define  IGP_OIF_DELETED           1
#define  IGP_OIF_FORWARDING        2
#define  IGP_STATE_FORWARDING    1

/* Statistics definitions */          
enum {
IGP_GEN_QRY_RCVD = 1,
IGP_GRP_QRY_RCVD,
IGP_GRP_SRC_QRY_RCVD,
IGP_V1V2REP_RCVD,
IGP_V3REP_RCVD,
IGP_GEN_QRY_SENT,
IGP_GRP_QRY_SENT,
IGP_GRP_SRC_QRY_SENT,
IGP_V1V2REP_SENT,
IGP_V3REP_SENT,
};

#define  IGP_IANA_RT_PROTOCOL_ID  1

#define  IGP_MAX_INT4             0x7FFFFFFF
#define  IGP_DEFAULT_SOURCE_MASK  0xFFFFFFFF

#ifdef MRI_WANTED

#define  IGP_MIN_INTERFACE_TTL    MRI_MIN_INTERFACE_TTL
#define  IGP_MAX_INTERFACE_TTL    MRI_MAX_INTERFACE_TTL

#define  IGP_MIN_RATELIMIT_IN_KBPS    MRI_MIN_RATELIMIT_IN_KBPS
#define  IGP_MAX_RATELIMIT_IN_KBPS    MRI_MAX_RATELIMIT_IN_KBPS

#define  IGP_ROUTE_TYPE_MULTICAST  MRI_ROUTE_TYPE_MULTICAST

#define  IGP_OIF_STATE_PRUNED           MRI_OIF_STATE_PRUNED
#define  IGP_OIF_STATE_FORWARDING       MRI_OIF_STATE_FORWARDING

#endif /* MRI_WANTED */

#ifdef RM_WANTED
#define   IGP_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? OSIX_TRUE : OSIX_FALSE)
#else
#define   IGP_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#endif

#endif /* _IGPDEFN_H */
