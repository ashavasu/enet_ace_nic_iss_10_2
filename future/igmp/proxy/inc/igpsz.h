/* $Id: igpsz.h,v 1.3 2013/01/23 11:23:20 siva Exp $*/
enum {
    MAX_IGP_DYN_MSG_SIZING_ID,
    MAX_IGP_FWD_ENTRIES_SIZING_ID,
    MAX_IGP_MAX_OIFS_SIZING_ID,
    MAX_IGP_MCAST_GRPS_SIZING_ID,
    MAX_IGP_MCAST_SRCS_SIZING_ID,
    MAX_IGP_RTR_IFACES_SIZING_ID,
    IGP_MAX_SIZING_ID
};


#ifdef  _IGPSZ_C
tMemPoolId IGPMemPoolIds[ IGP_MAX_SIZING_ID];
INT4  IgpSizingMemCreateMemPools(VOID);
VOID  IgpSizingMemDeleteMemPools(VOID);
INT4  IgpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _IGPSZ_C  */
extern tMemPoolId IGPMemPoolIds[ ];
extern INT4  IgpSizingMemCreateMemPools(VOID);
extern VOID  IgpSizingMemDeleteMemPools(VOID);
extern INT4  IgpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif /*  _IGPSZ_C  */


#ifdef  _IGPSZ_C
tFsModSizingParams FsIGPSizingParams [] = {
{ "tIgpRedTable", "MAX_IGP_DYN_MSG", sizeof(tIgpRedTable),MAX_IGP_DYN_MSG, MAX_IGP_DYN_MSG,0 },
{ "tIgmpPrxyFwdEntry", "MAX_IGP_FWD_ENTRIES", sizeof(tIgmpPrxyFwdEntry),MAX_IGP_FWD_ENTRIES, MAX_IGP_FWD_ENTRIES,0 },
{ "tIgmpPrxyOif", "MAX_IGP_MAX_OIFS", sizeof(tIgmpPrxyOif),MAX_IGP_MAX_OIFS, MAX_IGP_MAX_OIFS,0 },
{ "tIgmpPrxyGrpInfo", "MAX_IGP_MCAST_GRPS", sizeof(tIgmpPrxyGrpInfo),MAX_IGP_MCAST_GRPS, MAX_IGP_MCAST_GRPS,0 },
{ "tIgmpSrcMemBlk", "MAX_IGP_MCAST_SRCS", sizeof(tIgmpSrcMemBlk),MAX_IGP_MCAST_SRCS, MAX_IGP_MCAST_SRCS,0 },
{ "tIgmpPrxyUpIface", "MAX_IGP_RTR_IFACES", sizeof(tIgmpPrxyUpIface),MAX_IGP_RTR_IFACES, MAX_IGP_RTR_IFACES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _IGPSZ_C  */
extern tFsModSizingParams FsIGPSizingParams [];
#endif /*  _IGPSZ_C  */


