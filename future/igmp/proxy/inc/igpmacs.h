#ifndef _IGPMACS_H
#define _IGPMACS_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : igpmacs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy module Macros                       */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains macro definitions           */
/*                            for IGMP Proxy module                          */
/* $Id: igpmacs.h,v 1.7 2013/01/23 11:23:20 siva Exp $                   */
/*---------------------------------------------------------------------------*/


/* Functions to convert between IP VLAN interface index and IP Port numbers */
#define IGMP_PROXY_GET_PORT_FROM_IFINDEX    NetIpv4GetPortFromIfIndex
#define IGMP_PROXY_GET_IFINDEX_FROM_PORT    NetIpv4GetCfaIfIndexFromPort

/* Memory pool related defintions */
#define   IGP_GRP_MEMBLK_SIZE          sizeof(tIgmpPrxyGrpInfo)
#define   IGP_GRP_MEMPOOL_ID           gIgmpPrxyMemPool.GrpEntryPoolId

#define   IGP_FWD_MEMBLK_SIZE          sizeof(tIgmpPrxyFwdEntry)
#define   IGP_FWD_MEMPOOL_ID           gIgmpPrxyMemPool.FwdEntryPoolId

#define   IGP_DYN_MEMBLK_SIZE          sizeof(tIgpRedTable)
#define   IGP_DYN_MSG_MEMPOOL_ID       gIgmpPrxyMemPool.DynMsgPoolId

#define   IGP_RTR_MEMBLK_SIZE          sizeof(tIgmpPrxyUpIface)
#define   IGP_RTR_MEMPOOL_ID           gIgmpPrxyMemPool.UpIfacePoolId

#define   IGP_OIF_MEMBLK_SIZE          sizeof(tIgmpPrxyOif)
#define   IGP_OIF_MEMPOOL_ID           gIgmpPrxyMemPool.OifPoolId

#define   IGP_REP_BUF_MEMBLK_SIZE      MAX_IGP_REP_BUF_MEMBLK_SIZE
#define   IGP_REP_BUF_MEMBLK_COUNT     1
#define   IGP_REP_MEMPOOL_ID           gIgmpPrxyMemPool.ReportPoolId

#define   IGP_SRC_BUF_MEMBLK_SIZE      1500
#define   IGP_SRC_BUF_MEMBLK_COUNT     1
#define   IGP_SRC_MEMPOOL_ID           gIgmpPrxyMemPool.SourcePoolId


/* MEM Pool Block Allocation Macros */
#define  IGP_ALLOC_MEM_BLOCK(PoolId, ppu1Msg) \
         MemAllocateMemBlock(PoolId, (UINT1 **)ppu1Msg)

#define  IGP_RELEASE_MEM_BLOCK(PoolId, pu1Msg) \
{\
         MemReleaseMemBlock(PoolId, (UINT1 *) pu1Msg);\
         pu1Msg = NULL;\
}

#define  IGP_MFWD_REGISTER_MRP(u2RtrId, i4Status) \
{ \
    tMrpRegnInfo  regninfo; \
    regninfo.u2OwnerId = u2RtrId; \
    regninfo.u4MaxGroups = IGP_MAX_MCAST_GROUPS;\
    regninfo.u4MaxSrcs = IGP_MAX_MCAST_SRCS;\
    regninfo.u1Mode = 1;\
    regninfo.u4MaxIfaces = IGMP_MAX_LOGICAL_INTERFACES;\
    regninfo.u1ProtoId = IGMP_PROTID; \
    regninfo.MrpHandleMcastDataPkt = \
      (VOID (*) (struct CRU_BUF_CHAIN_DESC *))IgpPortHandleMcastDataPkt;\
    regninfo.MrpHandleV6McastDataPkt = NULL; \
    regninfo.MrpInformMfwdStatus = (INT4 (*) (UINT4))IgpPortMfwdHandleStatus; \
    i4Status = MfwdInputHandleRegistration (regninfo); \
    i4Status = (i4Status == MFWD_SUCCESS)?IGMP_SUCCESS:IGMP_FAILURE; \
}

#define  IGP_MFWD_DEREGISTER_MRP(u2RtrId) \
{ \
    MfwdInputHandleDeRegistration (u2RtrId); \
}

/* Enqueue the message to MFWD module */ 
#define  MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD(pMrtUpdBuf, i4Status) \
{\
    if (MfwdPostMrpMsgToMfwd (pMrtUpdBuf) == MFWD_FAILURE)\
    {\
       i4Status = IGMP_FAILURE;\
    }\
}

#define  IGP_REGISTER_WITH_IP_FOR_MDP(RegnId, u4Status) \
{\
   u4Status = IpRegHLProtocolForMCastPkts(IgpPortHandleMcastDataPkt);\
   if (u4Status == FAILURE)\
   {\
      u4Status = IGMP_FAILURE;\
   }\
   else\
   {\
     RegnId = u4Status;\
     u4Status = IGMP_SUCCESS;\
   }\
}

#define  IGP_DEREGISTER_WITH_IP_FOR_MDP(RegnId) \
         IpDeRegHLProtocolForMCastPkts (RegnId)
         

/* Source information macros */
#define IGP_IS_SOURCE_PRESENT(u4SrcAddr, bResult, u2SrcIndex) \
{ \
    UINT2   u2SourceCount = 0; \
    bResult = IGMP_FALSE;\
    u2SrcIndex = 0xffff; \
    for (u2SourceCount = 0;u2SourceCount < IGP_MAX_MCAST_SRCS;u2SourceCount++)\
    {\
       if (IGMP_IPVX_ADDR_COMPARE (u4SrcAddr, gaIgpSrcInfo[u2SourceCount].u4SrcIpAddr)\
           == IGMP_ZERO)\
       {\
          u2SrcIndex = u2SourceCount;\
          bResult = IGMP_TRUE;\
   break;\
       }\
    }\
}

#define IGP_ADD_TO_SRC_BMP(SrcIndex, SrcBitmap)\
{\
   UINT2       u2TempSrcIndex = 0;\
   u2TempSrcIndex = (UINT2) (SrcIndex + 1);\
   OSIX_BITLIST_SET_BIT(SrcBitmap, u2TempSrcIndex, IGP_SRC_LIST_SIZE);\
}

#define IGP_DEL_FROM_SRC_BMP(SrcIndex, SrcBitmap)\
{\
   UINT2       u2TempSrcIndex = 0;\
   u2TempSrcIndex = (UINT2) (SrcIndex + 1);\
   OSIX_BITLIST_RESET_BIT(SrcBitmap, u2TempSrcIndex, IGP_SRC_LIST_SIZE);\
}

/* This macro will make the Include and Exclude bitmap mutually exclusive */
#define IGP_UPDATE_SRC_INFO(InclSrcBmp, ExclSrcBmp)\
{\
   UINT2 u2Index = 0; \
   BOOL1 bResult = OSIX_FALSE;\
   for (u2Index = 1; u2Index <= IGP_MAX_MCAST_SRCS; u2Index++) \
   {\
      OSIX_BITLIST_IS_BIT_SET (InclSrcBmp, u2Index,\
                                 IGP_SRC_LIST_SIZE, bResult);\
      if (bResult == OSIX_TRUE)\
       {\
          OSIX_BITLIST_RESET_BIT (ExclSrcBmp, u2Index, IGP_SRC_LIST_SIZE);\
       }\
   }\
}

#define IGP_OR_SRC_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   for (u2ByteIndex = 0; u2ByteIndex < IGP_SRC_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] |= SrcBmp[u2ByteIndex];\
   }\
}

#define IGP_AND_SRC_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < IGP_SRC_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] &= SrcBmp[u2ByteIndex];\
   }\
}

#define IGP_XOR_SRC_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < IGP_SRC_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] ^= SrcBmp[u2ByteIndex];\
   }\
}

#define IGP_GET_UPIF_HASHINDEX(u4IfIndex, u4HashIndex) \
     u4HashIndex = (u4IfIndex % IGP_MAX_RTR_IFACE)

#endif /* _IGPMACS_H */
