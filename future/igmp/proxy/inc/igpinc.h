#ifndef IGPINC_H
#define IGPINC_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : igpinc.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                 */
/*    SUBSYSTEM NAME        : IGMP Proxy                                      */
/*    MODULE NAME           : IGMP Proxy Header files                         */
/*    LANGUAGE              : C                                               */
/*    TARGET ENVIRONMENT    : Any                                             */
/*    DATE OF FIRST RELEASE :                                                 */
/*    AUTHOR                : Aricent Inc.                                 */
/*    DESCRIPTION           : This file contains all header files that are to */
/*                            be included in the source files                 */
/*                            for IGMP Proxy module                           */
/*---------------------------------------------------------------------------*/


#include "igmpinc.h"
#include "bridge.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "snp.h"
#include "utilipvx.h"
#include "fssnmp.h"

#include "igpdefn.h"
#include "igptdfs.h"
#include "igpmacs.h"
#include "igpprot.h"
#include "igpextn.h"
#include "fsigplw.h"

#ifdef MFWD_WANTED
#include "mfmrp.h"
#endif

#ifdef DVMRP_WANTED
#include "dvmrp.h"
#endif

#ifdef PIM_WANTED
#include "pim.h"
#endif

#ifdef NPAPI_WANTED 
#include "ipnp.h"
#include "ipmcnp.h"
#endif

#include "igpsz.h"
#endif /* IGPINC_H */
