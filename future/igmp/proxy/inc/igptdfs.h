#ifndef _IGPTDFS_H
#define _IGPTDFS_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : igptdfs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Data Structures                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Data Structures             */
/*                            for IGMP Proxy module                          */
/* $Id: igptdfs.h,v 1.7 2013/11/22 11:37:21 siva Exp $                   */
/*---------------------------------------------------------------------------*/


/* Upstream interface record. This record is used for maintaing information on
 * the upstream interface configured */
typedef struct _IgmpPrxyUpIface
{
    tTMO_SLL_NODE     UpIfHashLink;           
    tIgmpTimer        IfacePurgeTimer; /* Indicates the timer for  
                                          changing the operating version
                                          of IGMP on the upstream interface */
    UINT4             u4RtrUpTime;     /* Indicates the time since the IGMP
                                          router has sent an IGMP query on the 
                                          upstream interface */
    INT4              i4IfTtlTreshold; /* Ttl Treshold of the interface */

    INT4              i4IfRateLimit;   /* Ratelimite value of the interface
                                          in kilobits per second */

    UINT2             u2RtrPurgeInt;   /* Indicates the time interval for which
                                          the upstream interface timer is
                                          started */
    UINT1             u1CfgVersion;    /* Indicates the configured  
                                          version of IGMP on the upstream 
                                          interface */ 
    UINT1             u1RowStatus;     /* Indicates the Upstream interface
                                          ROW status */ 
/* The dynamic entries that are to be synced should be added after the
 *  UpIntDbNode. And both the halves should be padded accordingly */
    tDbTblNode        UpIntDbNode;      /* Data Base node to sync upstream interface
       node dynamic information */
    UINT4             u4IfIndex;       /* Interface index of the upstream 
                                          interface, it corresponds to 
                                          IP port number */
    UINT1             u1OperVersion;   /* Indicates the operating  
                                          version of IGMP on the upstream 
                                          interface */ 
    UINT1             au1Reserved[3];  /* Reserved */
}tIgmpPrxyUpIface; 


/* Outgoing interface node. This node indicates the list of interfaces on which
 * multicast registrations has been received */
typedef struct _IgmpPrxyOif
{
    tTMO_SLL_NODE     NextOifNode;     /* Pointer to the next oif node */
    UINT4             u4IfIndex;       /* Outgoing interface index, 
                                          corresponds to IP port number */
    UINT4             u4UpTime;        /* Interface UpTime */
}tIgmpPrxyOif;

/* Multicast Forwarding information. This datastructure maintains the multicast
 * forwarding information and is populated when multicast data packet is 
 * received */
typedef struct _IgmpPrxyFwdEntry
{
    tRBNodeEmbd       RbNode;
    tTMO_SLL          OifList;         /* List of outgoing interface of type
                                          tIgmpPrxyOif */
    tIgmpTimer        EntryTimer;      /* Forwarding entry timer */
    UINT4             u4RouteUpTime;   /* The system time at which 
                                          the entry is created*/
    UINT1             u1TmrStatus;     /* Indicates the timer is running for
                                          FIRST HALF/SECOND HALF */
    UINT1       au1Pad[3];       /* Pad */
/* The dynamic entries that are to be synced should be added after the
 *  FwdDbNode. And both the halves should be padded accordingly */
    tDbTblNode        FwdDbNode;        /* Data Base node to sync forward entry
       node dynamic information */
    UINT4             u4IfIndex;       /* Incoming interface index  
                                          corresponds to IP port number */
    tIgmpIPvXAddr     u4SrcIpAddr;     /* Unicast source IP address */
    tIgmpIPvXAddr     u4GrpIpAddr;     /* Multicast group IP address */
    tPortListExt         McFwdPortList;   /* OIF List */
    UINT1       u1HwStatus;      /* Hardware status whether NP_PRESENT
      or NP_NOT_PRESENT*/
    UINT1       u1RouteStatus; /* Flag to indicate ADD_ROUTE/DELETE_ROUTE*/ 
    UINT1             au1Reserved[2];  /* Reserved */ 
}tIgmpPrxyFwdEntry;


/* Consolidated group membership database. This database maintains the 
 * consolidated  information for the group record */
typedef struct _IgmpPrxyGrpInfo
{
    tRBNodeEmbd       RbNode;
    tIgmPxySrcBmp     InclSrcBmp;      /* Indicates the set of sources for 
                                          which include registrations are 
                                          received for this group record */
    tIgmPxySrcBmp     ExclSrcBmp;      /* Indicates the set of sources for 
                                          which exclude registrations are 
                                          received for this group record */
    tIgmpIPvXAddr         u4GrpIpAddr;     /* Multicast group IP address */ 
    UINT4             u4IfaceCount;    /* Indicates the total number of 
                                          interfaces on which multicast 
                                          registrations are received */
    UINT4             u4v1v2IfaceCnt;  /* Indicates the total number of 
                                          interfaces on which v1/v2  
                                          registrations are received */
    UINT1             u1FilterMode;    /* Filter mode (INCLUDE/EXCLUDE) */
    UINT1             au1Reserved[3];  /* Reserved */
}tIgmpPrxyGrpInfo;

/* Source infomation */
typedef struct _IgmpPrxySrcInfo
{
    tIgmpIPvXAddr             u4SrcIpAddr;     /* Unicast source IP address */ 
    UINT2             u2GrpCount;      /* Number of groups using this source */ 
    UINT2             u2Reserved;      /* Reserved */ 
}tIgmpPrxySrcInfo;

/* Data structure for sending traps */
typedef struct _IgmpPrxyTrap
{
  UINT4          u4IfIndex;                     /* Downstream interface index */ 
  tIgmpIPvXAddr  u4QuerierAddress;              /* IP address of the router that
                                           has sent a query */
}tIgmpPrxyTrap;

/* Global IGMP Proxy infomation */
typedef struct _IgmpProxyInfo
{
    tTMO_HASH_TABLE   *pUpIfaceTbl;    /* Pointer to upstream interface 
                                          table */
    tRBTree           GroupEntry;      /* Pointer to consolidated group 
                                          membership table */
    tRBTree           ForwardEntry;    /* Pointer to multicast 
                                          forward entry */
    tTimerListId      ProxyTmrListId;  /* Timer List ID */
    tIgmpPrxyTrap     TrapInfo;        /* IGMP Proxy Trap inforamtion */
    tOsixQId          IgpMdpQId;       /* Queue for receiving mcast packets */
    UINT2             u2SourceCount;   /* Number of sources for which 
                                          Include /Exclude) have 
                                          been reported */
    UINT2             u2MaxHwEntryCnt; /* Maximum number of harware entries
                                          supported in the system */
    UINT1             u1UpIfaceCnt;    /* Indicates the number of upstream 
                                          interfaces */
    UINT1             u1MfwdStatus;    /* MFWD Status */
    UINT1             u1IPRegnId;      /* Registration ID from IP */
    UINT1             u1Reserved;      /* reserved */ 
/* The dynamic entries that are to be synced should be added after the
 *  IgpInfoDbNode. And both the halves should be padded accordingly */
    tDbTblNode       IgpInfoDbNode;   /* Database node to sync the
      global igmp proxy info */
    UINT2             u2PktOffset;     /* Offset on global upstream packet */
    UINT2             u2MaxUpIfCount;  /* Maximum number of upstream 
                                          interfaces supported in the 
                                          system */
    UINT1             u1V1UpstrmCnt;   /* Indicates the number of V1 
                                          routers present in the upstream */
    UINT1             u1V2UpstrmCnt;   /* Indicates the number of V2 
                                          routers present in the upstream */
    UINT1             u1V3UpstrmCnt;   /* Indicates the number of V3 
                                          routers present in the upstream */
    UINT1             u1NoOfGrpRecords;/* Number of group records in the 
                                          upstream packet */
}tIgmpProxyInfo;


/* Memory pool information for IGMP Proxy */
typedef struct _IgmpPrxyMemPool
{
    tMemPoolId        UpIfacePoolId;    /* Upstream interface memory 
                                           pool id */   
    tMemPoolId        GrpEntryPoolId;   /* Consolidated Group entry memory 
                                           pool id */
    tMemPoolId        FwdEntryPoolId;   /* Multicast forwarding entry memory 
                                           pool id */
    tMemPoolId        ReportPoolId;     /* Report memroy pool id for forming and 
                                           sending IGMP v3 report */
    tMemPoolId        SourcePoolId;     /* Source memroy pool id for 
                                           forming and sending IGMP v3 report */
    tMemPoolId        OifPoolId;        /* Outgoing interfaces memory pool id */
    tMemPoolId        DynMsgPoolId;      /* Igmp proxy dynamic message pol id 
      for redundancy */
}tIgmpPrxyMemPool;

typedef struct _IgmpSrcMemBlk
{
        UINT1      au1SrcMemBlk[IGP_SRC_BUF_MEMBLK_SIZE];
}
tIgmpSrcMemBlk;

typedef struct _IgpRedTable {
    tRBNodeEmbd   RbNode;   /* RbNode for the entry */
    tIgmpIPvXAddr       SrcAddr;    /* Unicast source IP address */
    tIgmpIPvXAddr       GrpAddr;    /* Multicast group IP address */ 
    UINT4         u4IfIndex;    /* Interface index */
    tPortListExt  McFwdPortList;  /* OIF List */  
    UINT1  u1RouteStatus;  /* Flag indicating ADD_ROUTE/
       DELETE_ROUTE */
    UINT1  au1Pad[3]; 
}tIgpRedTable;

#endif /* _IGPTDFS_H */
