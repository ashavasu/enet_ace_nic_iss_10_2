#ifndef _IGPEXTN_H
#define _IGPEXTN_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : igpextn.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy module external Global variables    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains extern global variables     */
/*                            for IGMP Proxy module                          */
/* $Id: igpextn.h,v 1.4 2013/01/23 11:23:20 siva Exp $                   */
/*---------------------------------------------------------------------------*/
extern tIgmpProxyInfo     gIgmpProxyInfo;
extern tIgmpPrxyMemPool   gIgmpPrxyMemPool;
extern tIgmpPrxySrcInfo   gaIgpSrcInfo [IGP_MAX_MCAST_SRCS]; 
extern tIgmPxySrcBmp      gIgpNullSrcBmp;
extern UINT1              *gpv3RepSendBuffer;  
extern UINT1              *gpSrcInfoBuffer;  
extern tIgmpIPvXAddr       gIgpIPvXZero;
extern tRBTree             gIgpRedTable;
#ifdef IGMPPRXY_WANTED
extern tDbOffsetTemplate   gaIgmpUpIntOffsetTbl[];
extern tDbOffsetTemplate   gaIgmpFwdOffsetTbl[];
extern tDbOffsetTemplate   gaIgmpPrxyInfoOffsetTbl[];
#endif
#endif /* _IGPEXTN_H */
