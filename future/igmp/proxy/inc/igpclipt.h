/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                              */
/*****************************************************************************/
/* |  FILE NAME             : igpclipt.h                                     */ 
/* |  PRINCIPAL AUTHOR      :                                                */ 
/* |  SUBSYSTEM NAME        : CLI                                            */ 
/* |  MODULE NAME           : IGMP Proxy configuration                       */ 
/* |  LANGUAGE              : C                                              */ 
/* |  TARGET ENVIRONMENT    :                                                */ 
/* |  DATE OF FIRST RELEASE :                                                */ 
/* |  DESCRIPTION           : This file includes all the IGMP Proxy CLI      */
/*                            related prototype declarations.                */ 
/* |                                                                         */ 
/*****************************************************************************/
#ifndef IGPCLI_H
#define IGPCLI_H

INT4
IgpCliSetModuleStatus (tCliHandle CliHandle, INT4 i4IgpModuleStatus);

INT4      
IgpCliUpIfConfigure (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4UpIfStatus);

INT4
IgpCliConfigureUpIfPurgeInterval (tCliHandle CliHandle, UINT4 u4IfIndex,
                                  INT4 i4Interval);

INT4
IgpCliConfigureUpIfVersion (tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4Version);
INT4
IgpCliShowUpIfTable (tCliHandle CliHandle, INT4 i4IfIndex);

INT4
IgpCliShowMrouteTable (tCliHandle CliHandle, UINT4 u4IfIndex, 
                       UINT4 u4IpAddress, INT4 i4Flag);

INT4
IgmpProxyShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module);

INT4
IgmpProxyShowRunningConfigUpIface (tCliHandle CliHandle);

VOID
IgmpProxyShowRunningConfigUpIfaceDetails (tCliHandle CliHandle, INT4 i4Index);
#endif
