/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsigpdb.h,v 1.4 2013/01/23 11:23:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSIGPDB_H
#define _FSIGPDB_H

UINT1 FsIgmpProxyRtrIfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIgmpProxyMrouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsIgmpProxyNextHopTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsigp [] ={1,3,6,1,4,1,2076,124};
tSNMP_OID_TYPE fsigpOID = {8, fsigp};


UINT4 FsIgmpProxyStatus [ ] ={1,3,6,1,4,1,2076,124,1,1};
UINT4 FsIgmpProxyForwardingTblEntryCnt [ ] ={1,3,6,1,4,1,2076,124,7,1};
UINT4 FsIgmpProxyRtrIfaceIndex [ ] ={1,3,6,1,4,1,2076,124,2,1,1,1};
UINT4 FsIgmpProxyRtrIfaceOperVersion [ ] ={1,3,6,1,4,1,2076,124,2,1,1,2};
UINT4 FsIgmpProxyRtrIfaceCfgOperVersion [ ] ={1,3,6,1,4,1,2076,124,2,1,1,3};
UINT4 FsIgmpProxyRtrIfacePurgeInterval [ ] ={1,3,6,1,4,1,2076,124,2,1,1,4};
UINT4 FsIgmpProxyRtrIfaceUpTime [ ] ={1,3,6,1,4,1,2076,124,2,1,1,5};
UINT4 FsIgmpProxyRtrIfaceExpiryTime [ ] ={1,3,6,1,4,1,2076,124,2,1,1,6};
UINT4 FsIgmpProxyRtrIfaceRowStatus [ ] ={1,3,6,1,4,1,2076,124,2,1,1,7};
UINT4 FsIgmpProxyMRouteSource [ ] ={1,3,6,1,4,1,2076,124,3,1,1,1};
UINT4 FsIgmpProxyMRouteGroup [ ] ={1,3,6,1,4,1,2076,124,3,1,1,2};
UINT4 FsIgmpProxyMRouteIifIndex [ ] ={1,3,6,1,4,1,2076,124,3,1,1,3};
UINT4 FsIgmpProxyMRouteUpTime [ ] ={1,3,6,1,4,1,2076,124,3,1,1,4};
UINT4 FsIgmpProxyMRouteExpiryTime [ ] ={1,3,6,1,4,1,2076,124,3,1,1,5};
UINT4 FsIgmpProxyNextHopSource [ ] ={1,3,6,1,4,1,2076,124,3,2,1,1};
UINT4 FsIgmpProxyNextHopGroup [ ] ={1,3,6,1,4,1,2076,124,3,2,1,2};
UINT4 FsIgmpProxyNextHopIndex [ ] ={1,3,6,1,4,1,2076,124,3,2,1,3};
UINT4 FsIgmpProxyNextHopState [ ] ={1,3,6,1,4,1,2076,124,3,2,1,4};
UINT4 FsIgmpProxyQuerierIfIndex [ ] ={1,3,6,1,4,1,2076,124,5,1};
UINT4 FsIgmpProxyQuerierAddress [ ] ={1,3,6,1,4,1,2076,124,5,2};


tMbDbEntry fsigpMibEntry[]= {

{{10,FsIgmpProxyStatus}, NULL, FsIgmpProxyStatusGet, FsIgmpProxyStatusSet, FsIgmpProxyStatusTest, FsIgmpProxyStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsIgmpProxyRtrIfaceIndex}, GetNextIndexFsIgmpProxyRtrIfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIgmpProxyRtrIfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpProxyRtrIfaceOperVersion}, GetNextIndexFsIgmpProxyRtrIfaceTable, FsIgmpProxyRtrIfaceOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIgmpProxyRtrIfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpProxyRtrIfaceCfgOperVersion}, GetNextIndexFsIgmpProxyRtrIfaceTable, FsIgmpProxyRtrIfaceCfgOperVersionGet, FsIgmpProxyRtrIfaceCfgOperVersionSet, FsIgmpProxyRtrIfaceCfgOperVersionTest, FsIgmpProxyRtrIfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIgmpProxyRtrIfaceTableINDEX, 1, 0, 0, "3"},

{{12,FsIgmpProxyRtrIfacePurgeInterval}, GetNextIndexFsIgmpProxyRtrIfaceTable, FsIgmpProxyRtrIfacePurgeIntervalGet, FsIgmpProxyRtrIfacePurgeIntervalSet, FsIgmpProxyRtrIfacePurgeIntervalTest, FsIgmpProxyRtrIfaceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIgmpProxyRtrIfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpProxyRtrIfaceUpTime}, GetNextIndexFsIgmpProxyRtrIfaceTable, FsIgmpProxyRtrIfaceUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsIgmpProxyRtrIfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpProxyRtrIfaceExpiryTime}, GetNextIndexFsIgmpProxyRtrIfaceTable, FsIgmpProxyRtrIfaceExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsIgmpProxyRtrIfaceTableINDEX, 1, 0, 0, NULL},

{{12,FsIgmpProxyRtrIfaceRowStatus}, GetNextIndexFsIgmpProxyRtrIfaceTable, FsIgmpProxyRtrIfaceRowStatusGet, FsIgmpProxyRtrIfaceRowStatusSet, FsIgmpProxyRtrIfaceRowStatusTest, FsIgmpProxyRtrIfaceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIgmpProxyRtrIfaceTableINDEX, 1, 0, 1, NULL},

{{12,FsIgmpProxyMRouteSource}, GetNextIndexFsIgmpProxyMrouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpProxyMrouteTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpProxyMRouteGroup}, GetNextIndexFsIgmpProxyMrouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpProxyMrouteTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpProxyMRouteIifIndex}, GetNextIndexFsIgmpProxyMrouteTable, FsIgmpProxyMRouteIifIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIgmpProxyMrouteTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpProxyMRouteUpTime}, GetNextIndexFsIgmpProxyMrouteTable, FsIgmpProxyMRouteUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsIgmpProxyMrouteTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpProxyMRouteExpiryTime}, GetNextIndexFsIgmpProxyMrouteTable, FsIgmpProxyMRouteExpiryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsIgmpProxyMrouteTableINDEX, 2, 0, 0, NULL},

{{12,FsIgmpProxyNextHopSource}, GetNextIndexFsIgmpProxyNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpProxyNextHopTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpProxyNextHopGroup}, GetNextIndexFsIgmpProxyNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIgmpProxyNextHopTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpProxyNextHopIndex}, GetNextIndexFsIgmpProxyNextHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIgmpProxyNextHopTableINDEX, 3, 0, 0, NULL},

{{12,FsIgmpProxyNextHopState}, GetNextIndexFsIgmpProxyNextHopTable, FsIgmpProxyNextHopStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIgmpProxyNextHopTableINDEX, 3, 0, 0, NULL},

{{10,FsIgmpProxyQuerierIfIndex}, NULL, FsIgmpProxyQuerierIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIgmpProxyQuerierAddress}, NULL, FsIgmpProxyQuerierAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIgmpProxyForwardingTblEntryCnt}, NULL, FsIgmpProxyForwardingTblEntryCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsigpEntry = { 20, fsigpMibEntry };

#endif /* _FSIGPDB_H */

