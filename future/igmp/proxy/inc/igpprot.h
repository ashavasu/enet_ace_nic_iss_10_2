#ifndef _IGPPT_H
#define _IGPPT_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: igpprot.h,v 1.7 2013/11/22 11:37:21 siva Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : igpprot.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Prototype definitions               */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains ProtoType definitions       */
/*                            for IGMP Proxy module                          */
/*---------------------------------------------------------------------------*/


/* igpmain.c */
INT4 IgpMainStatusEnable (VOID);
INT4 IgpMainMemPoolCreate (VOID);
VOID IgpMainMemPoolDelete (VOID);
INT4 IgpMainCreateProxyInfo (VOID);
VOID IgpMainDeleteProxyInfo (VOID);
INT4 IgpMainMcastDataPktHandler (tCRU_BUF_CHAIN_HEADER *pBuffer);
VOID IgpMainForwardMulticastData (tIgmpPrxyFwdEntry *, 
                                  tCRU_BUF_CHAIN_HEADER *);

/* igpnpapi.c */
INT1 IgpNpHwEnableMcastRouting (VOID);
INT1 IgpNpHwDisableMcastRouting (VOID);
INT4 IgpNpCreateFwdEntry (tIgmpPrxyFwdEntry *pIgpFwdEntry);
INT4 IgpNpDeleteFwdEntry (tIgmpPrxyFwdEntry *pIgpFwdEntry);
INT4 IgpNpAddOif (tIgmpPrxyFwdEntry *pIgpFwdEntry, UINT4 u4OifIndex);
INT4 IgpNpDeleteOif (tIgmpPrxyFwdEntry *pIgpFwdEntry, UINT4 u4OifIndex);
UINT4 IgpNpGetMcastFwdPortList (tIgmpIPvXAddr u4GrpAddr, tIgmpIPvXAddr u4SrcAddr, 
                                UINT4 u4IfIndex,UINT2 u2VlanId, 
    tPortListExt McFwdPortList, 
                                tPortListExt UntagPortList);
VOID IgpNpHandlePortBmpChange (tMacAddr GrpMacAddr, UINT2 u2VlanId);
INT4 IgpNpGetFwdEntry (tIgpRedTable * pIgpRedInfo);

INT4 IgpNpGetMRoutePktCount (tIgmpPrxyFwdEntry *, UINT4 *);
INT4 IgpNpGetMRouteDifferentInIfPktCount (tIgmpPrxyFwdEntry *, UINT4 *);
INT4 IgpNpGetMRouteOctetCount (tIgmpPrxyFwdEntry *, UINT4 *);
INT4 IgpNpGetMRouteHCOctetCount (tIgmpPrxyFwdEntry *, tSNMP_COUNTER64_TYPE *);
INT4 IgpNpGetMNextHopPktCount (tIgmpPrxyFwdEntry *, INT4, UINT4 *);
INT4 IgpNpGetMIfInOctetCount (INT4, UINT4 *);
INT4 IgpNpGetMIfOutOctetCount (INT4 i4IfIndex, UINT4 *pu4OutOctets);
INT4 IgpNpGetMIfHCInOctetCount (INT4, tSNMP_COUNTER64_TYPE *pu8HCInOctets);
INT4 IgpNpGetMIfHCOutOctetCount (INT4, tSNMP_COUNTER64_TYPE *);
INT4 IgpNpSetMIfaceTtlTreshold (INT4, INT4);
INT4 IgpNpSetMIfaceRateLimit (INT4, INT4);


/* igpfwd.c */
INT4 IgpFwdUpdateFwdTable (tIgmpIPvXAddr u4GrpIpAddr, tIgmpIPvXAddr u4SrcIpAddr,
                           UINT4 u4IfIndex, UINT2 u2SrcIndex,
                           UINT1 u1EventType,
                           tIgmpPrxyFwdEntry **ppRetIgpFwdEntry);
INT4 IgpFwdGetForwardEntry (tIgmpIPvXAddr u4GrpAddr, tIgmpIPvXAddr u4SrcAddr, 
                            tIgmpPrxyFwdEntry ** ppRetIgpFwdEntry);
VOID 
IgpFwdDeleteFwdEntries (tIgmpIPvXAddr u4GrpAddr);
VOID IgpFwdGetOifList (tIgmpIPvXAddr u4GrpAddr, UINT2 u2SrcIndex, 
                       tIgmpPrxyFwdEntry *pIgpForwardEntry);
INT4 IgpFwdUpdateOifNode (tIgmpPrxyFwdEntry *pIgpForwardEntry,
                          UINT4 u4OifIndex, UINT1 u1Action);
VOID
IgpFwdUpIfEntryDeletion (UINT4 u4IfIndex);
VOID IgpFwdSummarizeForwardTable (tIgmpPrxyGrpInfo *, tIgmpGroup *);
VOID IgpFwdSummarizeOifList (tIgmpGroup *, tIgmpPrxyFwdEntry *, UINT2);

/* igpupif.c */
INT4
IgpUpIfCreateUpIfaceEntry (UINT4 u4IfIndex, tIgmpIface *pIgmpIfNode,  
                           tIgmpPrxyUpIface **ppRetIgpUpIfaceEntry);
INT4 IgpUpIfGetUpIfaceEntry (UINT4 u4IfIndex, 
                             tIgmpPrxyUpIface **ppRetIgpUpIfaceEntry);
VOID IgpUpIfDeleteUpIfaceEntries(VOID);
INT4
IgpUpIfGetFirstUpIfaceEntry (tIgmpPrxyUpIface **ppRetIgpUpIfaceEntry);
INT4
IgpUpIfGetNextUpIfaceEntry (UINT4 u4CurrentIndex,
                            tIgmpPrxyUpIface **ppRetIgpUpIfaceEntry);
INT4
IgpUpIfConstructUpstreamReport (UINT1 u1OldFilterMode,
                                tIgmPxySrcBmp OldInclSrcBmp,
                                tIgmPxySrcBmp OldExclSrcBmp,
                                tIgmpPrxyGrpInfo *pIgmpPrxyGrpInfo,
                                tIgmpIPvXAddr u4GrpAddr, UINT1 u1V1V2SendFlag, 
                                UINT1 u1V3SendFlag);
INT4 IgpUpIfFormAndSendV1V2Report (tIgmpIPvXAddr, UINT1, UINT1, UINT4);
INT4 IgpUpIfProcessGeneralQuery (tIgmpPrxyUpIface *);
INT4 IgpUpIfProcessGrpSpecificQuery (tIgmpPrxyUpIface *, tIgmpIPvXAddr);
INT4 IgpUpIfProcessGrpAndSrcSpfQuery (tIgmpPrxyUpIface *, tIgmpIPvXAddr, UINT4,
                                      UINT4 *);
INT4 IgpUpIfSendUpstreamReport (tCRU_BUF_CHAIN_HEADER *, 
                                UINT4, tIgmpIPvXAddr, UINT2, UINT1);

VOID
IgpUpIfUpdateVersionCount(tIgmpPrxyUpIface *pIgpUpIfaceEntry, 
                          UINT1 u1Version);

/* igputil.c */
INT4 IgpUtilRBTreeGroupEntryCmp (tRBElem *pGroupEntryNode, 
                                 tRBElem *pGroupEntryIn);
INT4 IgpUtilRBTreeFwdEntryCmp (tRBElem *pForwardEntryNode, 
                               tRBElem *pForwardEntryIn);
INT4 IgpUtilRBTreeGroupEntryFree (tRBElem *pGroupEntryNode);
INT4 IgpUtilRBTreeFwdEntryFree (tRBElem *pForwadrEntryNode);
INT4 IgpUtilGetVlanIdFromIfIndex (UINT4 u4Port, UINT2 *pu2VlanId);
INT4
IgpUtilCreateGroupRecord (UINT1 u1OldFilterMode,
                          tIgmPxySrcBmp OldInclSrcBmp,
                          tIgmPxySrcBmp OldExclSrcBmp,
                          tIgmpPrxyGrpInfo *pIgmpPrxyGrpInfo,
                          tIgmpIPvXAddr u4GrpIpAddr,
                          UINT2 *pu2PktLength);
VOID
IgpUtilFillGroupRecord (UINT1 u1RecordType, tIgmpIPvXAddr u4GrpAddress, 
                        tIgmPxySrcBmp IgmPxySrcBmp, UINT2 *u2Offset);
VOID
IgpUtilUpdateGroupRefCount (tIgmPxySrcBmp OldInclSrcBmp, 
                            tIgmPxySrcBmp OldExclSrcBmp,
                            tIgmPxySrcBmp NewInclSrcBmp,
                            tIgmPxySrcBmp NewExclSrcBmp);
INT4
IgpUtilAddToSourceList (tIgmpIPvXAddr u4SrcAddr, UINT2 *pu2SrcIndex);
tSNMP_OID_TYPE     *
IgpUtilMakeObjIdFromDotNew (INT1 *pi1TextStr);
VOID 
IgpUtilFreeVarBindList (tSNMP_VAR_BIND * pVarBindLst);
VOID 
IgpUtilBuildProxyDatabase (VOID);
INT4
IgpUtilCreateGrpRecord4Xsisting (tIgmpPrxyGrpInfo *pIgmpPrxyGrpInfo,
                                UINT2 *pu2PktLength, tIgmpIPvXAddr u4GrpAddress,
                                tIgmPxySrcBmp IgmPxySrcBmp);
INT4
IgpUtilCreateGrpRecord4Change (UINT1 u1OldFilterMode,
                          tIgmPxySrcBmp OldInclSrcBmp,
                          tIgmPxySrcBmp OldExclSrcBmp,
                          tIgmpPrxyGrpInfo *pIgmpPrxyGrpInfo,
                          tIgmpIPvXAddr u4GrpIpAddr,
                          UINT2 *pu2PktLength);
INT4 IgpUtilUpdateSourceInfo (tIgmpGroup *pIgmpGroupEntry);
VOID IgpUtilGetOifBitListFrmRtOifList (tIgmpPrxyFwdEntry  *pIgpForwardEntry);

/* igpgrp.c */
INT4
IgpGrpCreateGroupEntry (tIgmpGroup *pIgmpGrpEntry, UINT1 u1Version, 
                        tIgmpPrxyGrpInfo **ppRetIgpGroupEntry);
INT4 IgpGrpDeleteGroupEntry (tIgmpIPvXAddr u4GrpIpAddr);
INT4 IgpGrpGetGroupEntry (tIgmpIPvXAddr u4GrpIpAddr, 
                          tIgmpPrxyGrpInfo **ppRetIgpGroupEntry);
VOID IgpGrpDeleteGroupEntries (VOID);

VOID
IgpGrpConsolidateSrcInfo (UINT1 u1OldFilterMode, tIgmPxySrcBmp OldInclBmp, 
                          tIgmPxySrcBmp OldExclBmp, 
                          tIgmpPrxyGrpInfo * pIgpGroupEntry, 
     UINT1 u1V1V2SendFlag, UINT1 u1V3SendFlag);
INT4
IgpGrpDeleteGroupOnIface (tIgmpGroup *pIgmpGroupEntry, UINT1 u1Version);

/* igpmfwd.c */
INT4 IgpMfwdCreateRouteEntry (tIgmpPrxyFwdEntry *pIgpForwardEntry);
INT4 IgpMfwdDeleteRouteEntry (tIgmpPrxyFwdEntry *pIgpForwardEntry);
INT4 IgpMfwdAddOif (tIgmpPrxyFwdEntry *pIgpForwardEntry, UINT4 u4OifIndex);
INT4 IgpMfwdDelOif (tIgmpPrxyFwdEntry *pIgpForwardEntry, UINT4 u4OifIndex);
VOID IgpMfwdHandleEnable (VOID);
VOID IgpMfwdHandleDisable (VOID);

/* igpport.c */
INT4 IgpPortMfwdHandleStatus (UINT4 u4Event);
INT4
IgpPortSocketSend (tCRU_BUF_CHAIN_HEADER *pBuffer, UINT4  u4Src, 
                   UINT4 u4Dest, UINT2 u2Len, UINT1 u1CntlPkt);

/* igptmr.c */
VOID IgpTmrInitTmrDesc (VOID);
VOID IgpTmrUpIfaceTmrExpiry (VOID *pArg);
VOID IgpTmrForwardTmrExpiry (VOID *pArg);
VOID IgpTmrExpiryHandler (VOID);

#endif /* _IGPPT_H */
