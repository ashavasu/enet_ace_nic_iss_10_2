/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: igpcmd.def,v 1.8 2014/11/04 13:14:02 siva Exp $                                                         
*                                                                    
*********************************************************************/
DEFINE GROUP : IGP_GBL_CMDS
COMMAND : ip igmp proxy-service
ACTION  : cli_process_igp_cmd(CliHandle, IGMP_PROXY_STATUS_ENABLE, NULL);
SYNTAX  : ip igmp proxy-service 
PRVID   : 15
HELP    : Enables IGMP Proxy service in the system.
CXT_HELP : ip Configures IP related protocol|
           igmp IGMP related configuration|
           proxy-service IGMP Proxy service related configuration|
           <CR> Enables IGMP Proxy service in the system

COMMAND : no ip igmp proxy-service
ACTION  : cli_process_igp_cmd(CliHandle, IGMP_PROXY_STATUS_DISABLE, NULL);
SYNTAX  : no ip igmp proxy-service 
PRVID   : 15
HELP    : Disables IGMP Proxy service in the system.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
           ip IP related configuration|
           igmp IGMP related configuration|
           proxy-service IGMP Proxy service related configuration|
           <CR> Disables IGMP Proxy service in the system
END GROUP

DEFINE GROUP : IGP_IFACE_CMDS
COMMAND : ip igmp-proxy mrouter 
ACTION  : cli_process_igp_cmd(CliHandle, IGMP_PROXY_CLI_UPIFACE, 
                              NULL, CLI_ENABLE);
SYNTAX  : ip igmp-proxy mrouter
PRVID   : 15
HELP    : Configures the interface as an upstream interface
CXT_HELP : ip Configures IP related protocol|
           igmp-proxy IGMP proxy related configuration|
           mrouter Mrouter related configuration|
           <CR> Configures the interface as an upstream interface

COMMAND : no ip igmp-proxy mrouter 
ACTION  : cli_process_igp_cmd(CliHandle, IGMP_PROXY_CLI_UPIFACE, 
                              NULL, CLI_DISABLE); 
SYNTAX  : no ip igmp-proxy mrouter
PRVID   : 15
HELP    : Removes the interface from the upstream interface list
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
           ip IP related configuration|
           igmp-proxy IGMP proxy related configuration|
           mrouter Mrouter related configuration|
           <CR> Removes the interface from the upstream interface list

COMMAND : ip igmp-proxy mrouter-time-out <short(60-600)>
ACTION  : cli_process_igp_cmd(CliHandle, IGMP_PROXY_CLI_UPIFACE_INT, NULL, $3);
SYNTAX  : ip igmp-proxy mrouter-time-out <(60 - 600) seconds> 
PRVID   : 15
HELP    : Configures the upstream interface purge interval, after which the 
          IGMP version on upstream interface will switch back to configured 
          version
CXT_HELP: ip Configures IP related protocol|
          igmp-proxy IGMP proxy related configuration|
          mrouter-time-out Upstream interface purge interval configuration|
          (60-600) Interval value after which the IGMP version switches back to the configured version|
          <CR> Configures the upstream interface purge interval, after which theIGMP version on upstream interface will switch back to configured version
          
COMMAND : ip igmp-proxy mrouter-version { "1" | "2" | "3" }
ACTION  : 
        {
          if( $3 != NULL)
          {
            cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_VERSION,
                                  NULL ,CLI_IGMP_V1);
          }
          else if( $4 != NULL)
          {
            cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_VERSION,
                                  NULL, CLI_IGMP_V2);
          }
          else
          {
            cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_VERSION,
                                  NULL, CLI_IGMP_V3);
          }
        }
SYNTAX  : ip igmp-proxy mrouter-version { 1 | 2 | 3 }
PRVID   : 15
HELP    : Configures the version of IGMP on upstream interface
CXT_HELP : ip Configures IP related protocol|
           igmp-proxy IGMP proxy related configuration|
           mrouter-version Upstream interface IGMP version configuration|
            1 IGMP Version 1| 
            2 IGMP Version 2| 
            3 IGMP Version 3|
            <CR> Configures the version of IGMP on upstream interface

END GROUP

DEFINE GROUP : IGP_SHOW_CMDS
COMMAND : show ip igmp-proxy mrouter [Vlan <vlan_vfi_id>] 
ACTION  :
         {
           UINT4   u4IfIndex = 0; 
           if (($4 != NULL) && ($5 != NULL))
           {
             if (CfaCliGetIfIndex ((INT1 *) $4,(INT1 *)  $5, &u4IfIndex) == CLI_FAILURE)
             {
               CliPrintf (CliHandle, "%% Invalid Interface \r\n");
               return (CLI_FAILURE);
             }

              cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_SHOW_UPIFACE, 
                                   NULL, u4IfIndex);
           }
           else
           {
              cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_SHOW_UPIFACE, 
                                   NULL, NULL);
           }
         }                       
SYNTAX  : show ip igmp-proxy mrouter [Vlan <vlan-id/vfi-id>] 
PRVID   : 1
HELP    : Displays the upstream interface configuration of IGMP Proxy device
CXT_HELP : show Displays the configuration/statistics/general information|
           ip IP related configuration|
           igmp-proxy IGMP proxy related configuration|
           mrouter Mrouter related configuration|
           Vlan Vlan related configuration|
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI|
           <CR> Displays the upstream interface configuration of IGMP Proxy device

COMMAND : show ip igmp-proxy forwarding-database [{ Vlan <vlan_vfi_id> |  group <mcast_addr> | source <ucast_addr> }]
ACTION  :
         {
           UINT4   u4IfIndex = 0; 
           INT4   i4ShowObject = 0;
           if (($4 != NULL) && ($5 != NULL))
           {
             i4ShowObject = IGMP_PROXY_CLI_SHOW_MROUTES_VID;  
             if (CfaCliGetIfIndex ((INT1 *) $4, (INT1 *) $5, &u4IfIndex) == CLI_FAILURE)
             {
               CliPrintf (CliHandle, "%% Invalid Interface \r\n");
               return (CLI_FAILURE);
             }
             cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_SHOW_MROUTES, 
                                  NULL, u4IfIndex, i4ShowObject);
           }
           else if (($6 != NULL) && ($7 != NULL))
           {
             i4ShowObject = IGMP_PROXY_CLI_SHOW_MROUTES_GRP;
             cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_SHOW_MROUTES,
                                  NULL, $7, i4ShowObject);
           }
           else if (($8 != NULL) && ($9 != NULL))
           {
             i4ShowObject = IGMP_PROXY_CLI_SHOW_MROUTES_SRC;
             cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_SHOW_MROUTES,
                                  NULL, $9, i4ShowObject);
           }
           else
           {
              cli_process_igp_cmd (CliHandle, IGMP_PROXY_CLI_SHOW_MROUTES, 
                                   NULL, NULL, i4ShowObject);
           }
         } 
SYNTAX  : show ip igmp-proxy forwarding-database [{ Vlan <vlan-id/vfi-id> | group <group-address> | source <source-address> }]
PRVID   : 1
HELP    : Displays the multicast forwarding information
CXT_HELP : show Displays the configuration/statistics/general information|
           ip IP related configuration|
           igmp-proxy IGMP proxy related configuration|
           forwarding-database Multicast forwarding information|
           Vlan Vlan related configuration|
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI|
           group Multicast group address|
           A.B.C.D(<mcast_addr>) Multicast group address| 
           source Multicast source address|
           A.B.C.D(<ucast_addr>) Unicast IP address|
           <CR> Displays the multicast forwarding information
END GROUP
