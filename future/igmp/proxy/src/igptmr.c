/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 * $Id: igptmr.c,v 1.9 2015/02/13 11:13:54 siva Exp $
 *
 ********************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : igptmr.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Timer Module                        */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains timer related functions     */
/*                            for IGMP Proxy module                          */
/*---------------------------------------------------------------------------*/
#include "igpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_TMR_MODULE;
#endif

/*****************************************************************************/
/* Function Name      : IgpTmrInitTmrDesc                                    */
/*                                                                           */
/* Description        : This function will init the timer desrciptors        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpTmrInitTmrDesc (VOID)
{
    gIgmpConfig.aIgmpTmrDesc[IGP_RTR_IFACE_TIMER].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpPrxyUpIface, IfacePurgeTimer);
    gIgmpConfig.aIgmpTmrDesc[IGP_RTR_IFACE_TIMER].TmrExpFn =
        IgpTmrUpIfaceTmrExpiry;
    gIgmpConfig.aIgmpTmrDesc[IGP_FWD_ENTRY_TIMER].i2Offset =
        (INT2) IGMP_OFFSET (tIgmpPrxyFwdEntry, EntryTimer);
    gIgmpConfig.aIgmpTmrDesc[IGP_FWD_ENTRY_TIMER].TmrExpFn =
        IgpTmrForwardTmrExpiry;
    return;
}

/*****************************************************************************/
/* Function Name      : IgpTmrExpiryHandler                                  */
/*                                                                           */
/* Description        : This function will handle the timer expiry events    */
/*                      related to IGMP Proxy module. Different timer expiry */
/*                      handlers are invoked based upon the Timer Type       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpTmrExpiryHandler (VOID)
{
    tIgmpTimer         *pExpiredTmr = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2OffSet = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpTmrExpiryHandler()\r \n");
    /* Get the expired timer from the list */
    while ((pExpiredTmr = (tIgmpTimer *)
            TmrGetNextExpiredTimer (gIgmpProxyInfo.ProxyTmrListId)) != NULL)
    {
        /* Get the timer id and Offset */
        u1TimerId = ((tIgmpTimer *) pExpiredTmr)->TimerBlk.u1TimerId;
        i2OffSet = gIgmpConfig.aIgmpTmrDesc[u1TimerId].i2Offset;

        /* Call the corresponding timer expiry handler to the
         * expiry of the timer */
        (*(gIgmpConfig.aIgmpTmrDesc[u1TimerId].TmrExpFn))
            ((UINT1 *) pExpiredTmr - i2OffSet);

    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpTmrExpiryHandler()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpTmrUpIfaceTmrExpiry                               */
/*                                                                           */
/* Description        : This function will handle the upstream interface     */
/*                      timer expiry                                         */
/*                                                                           */
/* Input(s)           : pArg  - Pointer to the upstream interface structure  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpTmrUpIfaceTmrExpiry (VOID *pArg)
{
    tIgmpPrxyUpIface   *pIgpUpIfacEntry = pArg;

    /* If any IGMP version is configured on the upstream interface set the
     * operating version to the configured version of IGMP on the upstream 
     * interface else configure the operating version to default version 
     * that is IGMP v3 */
    if (pIgpUpIfacEntry->u1OperVersion != pIgpUpIfacEntry->u1CfgVersion)
    {
        IgpUpIfUpdateVersionCount (pIgpUpIfacEntry,
                                   pIgpUpIfacEntry->u1CfgVersion);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IgpTmrForwardTmrExpiry                               */
/*                                                                           */
/* Description        : This function will handle  multicast forward entry   */
/*                      timer expiry                                         */
/*                                                                           */
/* Input(s)           : pArg  - Pointer to the forwadr entry structure       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpTmrForwardTmrExpiry (VOID *pArg)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = pArg;
    INT4                i4FwdTmrVal = 0;
#ifdef MFWD_WANTED
    tIPvXAddr           SrcIpAddr;
    tIPvXAddr           GrpIpAddr;
    UINT4               u4SrcIpAddr;
    UINT4               u4GrpIpAddr;
    UINT4               u4CrtTime = 0;
    UINT4               u4LastFwdTime = 0;
#endif
#ifdef FS_NPAPI
    UINT4               u4HitBitStatus = FALSE;
    UINT4               u4Iif = 0;
    UINT2               u2VlanId;
#endif

    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpTmrForwardTmrExpiry()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, (pIgpForwardEntry->u4SrcIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
#ifdef NPAPI_WANTED
    IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpForwardEntry->u4IfIndex, &u4Iif);

    if (CFA_FAILURE == CfaGetVlanId (u4Iif, &u2VlanId))
    {
        return;
    }

    /* Get the hitbit status for the forwarding entry to check if
     * the entry is being used in the data plane */
    IpmcFsNpIpv4McGetHitStatus (u4IpSrc,
                            u4IpGrp,
                            pIgpForwardEntry->u4IfIndex,
                            u2VlanId, &u4HitBitStatus);

    /* If the entry is used in the data plane then restart the
     * forward entry timer and entry timer status to First Half */
    if (u4HitBitStatus == TRUE)
    {
        pIgpForwardEntry->u1TmrStatus = IGP_FWD_TMR_FIRST_HALF;
        i4FwdTmrVal = IGP_FWD_ENTRY_TIMER_VAL;
    }

    /* If the entry is not used in the data plane and the timer status is
     * first half set the timer status to second half and restart the 
     * forward entry timer */
    else if (pIgpForwardEntry->u1TmrStatus == IGP_FWD_TMR_FIRST_HALF)
    {
        pIgpForwardEntry->u1TmrStatus = IGP_FWD_TMR_SECOND_HALF;
        i4FwdTmrVal = IGP_FWD_ENTRY_TIMER_VAL;
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "First half of the forward entry timer expired"
                       " for source 0x%x and group 0x%x\r\n", u4IpSrc, u4IpGrp);
    }
    /* Second half of the timer is also expierd so delete the entry */
    else
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "Second half of the forward entry timer expired"
                       " for source 0x%x and group 0x%x\r\n", u4IpSrc, u4IpGrp);
    }
#endif
#ifdef MFWD_WANTED
    /* If MFWD is disable directly delete the entry */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        i4FwdTmrVal = 0;
    }
    else
    {
        u4SrcIpAddr = OSIX_HTONL (u4IpSrc);
        u4GrpIpAddr = OSIX_HTONL (u4IpGrp);

        IPVX_ADDR_INIT_IPV4 (SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &u4SrcIpAddr);
        IPVX_ADDR_INIT_IPV4 (GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                             (UINT1 *) &u4GrpIpAddr);

        /* Get the last time at which the data packet was forwarded */
        MfwdMrtGetLastFwdTime (IGMP_PROXY_ID,
                               SrcIpAddr, GrpIpAddr, &u4LastFwdTime);

        OsixGetSysTime (&u4CrtTime);
        i4FwdTmrVal = (u4CrtTime - u4LastFwdTime);

        IGMP_GET_TIME_IN_SEC (i4FwdTmrVal);

        i4FwdTmrVal = (IGP_FWD_ENTRY_TIMER_VAL - i4FwdTmrVal);
        if (i4FwdTmrVal > IGP_FWD_ENTRY_TIMER_VAL)
        {
            i4FwdTmrVal = IGP_FWD_ENTRY_TIMER_VAL;
        }
    }
#endif

    /* Restart the forward entry timer if the forward timer value is 
     * greater that 0 */
    if (i4FwdTmrVal > 0)
    {
        if (TmrStart (gIgmpProxyInfo.ProxyTmrListId,
                      &(pIgpForwardEntry->EntryTimer.TimerBlk),
                      IGP_FWD_ENTRY_TIMER, i4FwdTmrVal, 0) == TMR_FAILURE)
        {

            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Forward Entry timer restart failed\r\n");
        }
    }
    /* Delete the forwarding entry */
    else
    {
        if (IgpFwdUpdateFwdTable (pIgpForwardEntry->u4GrpIpAddr,
                                  pIgpForwardEntry->u4SrcIpAddr,
                                  pIgpForwardEntry->u4IfIndex,
                                  0, IGP_DELETE_FWD_ENTRY,
                                  NULL) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Forward entry deletion on timer expiry failed\r\n");
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpTmrForwardTmrExpiry()\r \n");
    return;
}
