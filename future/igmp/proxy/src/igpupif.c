/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igpupif.c,v 1.19 2015/05/23 10:51:56 siva Exp $
 *
 ******************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : igpupif.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Upstream interface module           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains routines related to         */
/*                            updating the upstream interfaces               */
/*---------------------------------------------------------------------------*/

#include "igpinc.h"
#include "stdigmcli.h"
#include "utilrand.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_QRY_MODULE;
#endif

/*****************************************************************************/
/* Function Name      : IgpUpIfCreateUpIfaceEntry                            */
/*                                                                           */
/* Description        : This function creates the upstream interface entry   */
/*                                                                           */
/* Input(s)           : u4IfIndex        - Upstream interface index          */
/*                                                                           */
/* Output(s)          : pointer to the upstream interface entry              */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpUpIfCreateUpIfaceEntry (UINT4 u4IfIndex, tIgmpIface * pIgmpIfNode,
                           tIgmpPrxyUpIface ** ppRetIgpUpIfaceEntry)
{
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pGrp = NULL;
    tIgmpSource        *pSrc = NULL;
    tIgmpGroup         *pNextGrp = NULL;
    tIgmpSource        *pNextSrc = NULL;
    tIgmpGroup          IgmpGrpEntry;
    tSnmpNotifyInfo     GrpSnmpNotifyInfo;
    tSnmpNotifyInfo     SrcSnmpNotifyInfo;
    UINT4               u4HashIndex = IGMP_ZERO;
    UINT4               u4CfaIfIndex = IGMP_ZERO;
    UINT4               u4IpGrp = IGMP_ZERO;
    UINT4               u4IpSrc = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfCreateUpIfaceEntry()\r \n");
    if (gIgmpProxyInfo.u2MaxUpIfCount == IGP_MAX_RTR_IFACE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Upstream interface configuration reached the maximum"
                  " supported by the system\r\n");
        return IGMP_FAILURE;
    }

    /* clear querier related stuff */
    IgmpHdlTransitionToNonQuerier (pIgmpIfNode);

    pIfaceNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);
    if (pIfaceNode == NULL)
    {
        return IGMP_FAILURE;
    }

    IGMP_IP_GET_IFINDEX_FROM_PORT (u4IfIndex, &u4CfaIfIndex);

    MEMSET (&GrpSnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    MEMSET (&SrcSnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    SNMP_SET_NOTIFY_INFO (GrpSnmpNotifyInfo, IgmpCacheStatus, 0,
                          TRUE, IgmpLock, IgmpUnLock, 2, SNMP_SUCCESS);

    SNMP_SET_NOTIFY_INFO (SrcSnmpNotifyInfo, IgmpSrcListStatus, 0,
                          TRUE, IgmpLock, IgmpUnLock, 3, SNMP_SUCCESS);

    MEMSET (&IgmpGrpEntry, 0, sizeof (tIgmpGroup));

    /* Clear all membership information in the given interface index */
    IgmpGrpEntry.u4IfIndex = pIfaceNode->u4IfIndex;
    IgmpGrpEntry.pIfNode = pIfaceNode;

    pGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                         (tRBElem *) & IgmpGrpEntry, NULL);
    while (pGrp != NULL)
    {
        if (pGrp->u4IfIndex != pIfaceNode->u4IfIndex)
        {
            /* Group entry doesn't belong to
             * specified interface index */
            break;
        }

        pNextGrp = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                 (tRBElem *) pGrp, NULL);

        pSrc = (tIgmpSource *) TMO_SLL_First (&pGrp->srcList);
        IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, pGrp->u4Group, IPVX_ADDR_FMLY_IPV4);

        /* For version 3 with null source, notification is sent here */
        if ((pSrc == NULL) && (pIfaceNode != NULL) /* Klocwork fix */  &&
            (pIfaceNode->u1Version == IGMP_VERSION_3) &&
            (pGrp->u1StaticFlg == IGMP_TRUE))
        {
            SNMP_NOTIFY_CFG ((SrcSnmpNotifyInfo, "%p %i %p %i",
                              pGrp->u4Group, u4CfaIfIndex, 0, IGMP_DESTROY));
        }
        while (pSrc != NULL)
        {
            pNextSrc =
                (tIgmpSource *) TMO_SLL_Next (&pGrp->srcList, &pSrc->Link);

            IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, pSrc->u4SrcAddress,
                                    IPVX_ADDR_FMLY_IPV4);
            if (pSrc->u1StaticFlg == IGMP_TRUE)
            {
                /* For valid sources, the destroy notificationi is sent here */
                SNMP_NOTIFY_CFG ((SrcSnmpNotifyInfo, "%p %i %p %i",
                                  u4IpGrp, u4CfaIfIndex,
                                  u4IpSrc, IGMP_DESTROY));
            }
	    pSrc->u1DelSyncFlag = IGMP_FALSE;
            IgmpDeleteSource (pGrp, pSrc);
            pSrc = pNextSrc;
        }

        /* For v1 and v2 group, destroy notification is sent here */
        if (pGrp->u1StaticFlg == IGMP_TRUE)
        {
            SNMP_NOTIFY_CFG ((GrpSnmpNotifyInfo, "%p %i %i",
                              u4IpGrp, u4CfaIfIndex, IGMP_DESTROY));
        }
        IgpGrpDeleteGroupEntry (pGrp->u4Group);
	pGrp->u1DelSyncFlag = IGMP_FALSE;
        IgmpDeleteGroup (pIgmpIfNode, pGrp, pGrp->u1StaticFlg);

        pGrp = pNextGrp;
    }

    /* Create the upstream interface entry */
    if (IGMP_MEM_ALLOCATE (IGP_RTR_MEMPOOL_ID,
                           pIgpUpIfaceEntry, tIgmpPrxyUpIface) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Memory allocation failed for creting upstream interface"
                  " entry\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
	      IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"for creting upstream interface entry\n");
        return IGMP_FAILURE;
    }

    MEMSET (pIgpUpIfaceEntry, IGMP_ZERO, sizeof (tIgmpPrxyUpIface));

    /* Initialise and add the Interface node to the Interface list */

    pIgpUpIfaceEntry->u4IfIndex = u4IfIndex;
    pIgpUpIfaceEntry->u1CfgVersion = IGMP_VERSION_3;
    pIgpUpIfaceEntry->u1OperVersion = IGMP_VERSION_3;
    pIgpUpIfaceEntry->u2RtrPurgeInt = IGP_DEF_RTR_IFACE_INTERVAL;

    /* Initialise the upstream interface DB Node */
    IgmpRedDbNodeInit (&(pIgpUpIfaceEntry->UpIntDbNode), IGP_RED_UP_INT_INFO);

    IGP_GET_UPIF_HASHINDEX (u4IfIndex, u4HashIndex);
    TMO_HASH_Add_Node (gIgmpProxyInfo.pUpIfaceTbl,
                       &pIgpUpIfaceEntry->UpIfHashLink,
                       u4HashIndex, (UINT1 *) &u4IfIndex);

    *ppRetIgpUpIfaceEntry = pIgpUpIfaceEntry;

    /* Increment the number of upstream interfaces in the system */
    gIgmpProxyInfo.u2MaxUpIfCount++;
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfCreateUpIfaceEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfDeleteUpIfaceEntry                            */
/*                                                                           */
/* Description        : This function deletes the upstream interface entry   */
/*                                                                           */
/* Input(s)           : u4IfIndex        - Upstream interface index          */
/*                      u1Flag           - Flag to indicate whter to delete  */
/*                                         the upstream interface            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpUpIfDeleteUpIfaceEntry (UINT4 u4IfIndex, UINT1 u1Flag)
{
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    tIgmpQuery         *pIgmpQryNode = NULL;
    tIgmpIface         *pIgmpIfNode = NULL;
    UINT4               u4HashIndex = IGMP_ZERO;
    UINT4               u4RandomTime = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfDeleteUpIfaceEntry()\r \n");
    if (IgpUpIfGetUpIfaceEntry (u4IfIndex, &pIgpUpIfaceEntry) != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Upstream interface entry not present\r\n");
        return IGMP_FAILURE;
    }

    /* Update the forwarding table for Upstream interface deletion */
    IgpFwdUpIfEntryDeletion (u4IfIndex);

    if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_3)
    {
        /* Decrement the Upstream count for version IGMP v3 */
        gIgmpProxyInfo.u1V3UpstrmCnt--;
    }
    else if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_2)
    {
        /* Decrement the Upstream count for version IGMP v2 */
        gIgmpProxyInfo.u1V2UpstrmCnt--;
    }
    else
    {
        /* Decrement the Upstream count for version IGMP v1 */
        gIgmpProxyInfo.u1V1UpstrmCnt--;
    }

    /* Stop the upstream interface timer if it is running */
    TmrStop (gIgmpProxyInfo.ProxyTmrListId,
             &(pIgpUpIfaceEntry->IfacePurgeTimer.TimerBlk));

    pIgpUpIfaceEntry->u1RowStatus = IGMP_NOT_IN_SERVICE;
    /* This flag will be set to true only when the upstream interface 
     * needs to be deleted from the from the proxy database */

    if (u1Flag == IGMP_FALSE)
    {
        return IGMP_SUCCESS;
    }

    IGP_GET_UPIF_HASHINDEX (pIgpUpIfaceEntry->u4IfIndex, u4HashIndex);
    TMO_HASH_Delete_Node (gIgmpProxyInfo.pUpIfaceTbl,
                          &pIgpUpIfaceEntry->UpIfHashLink, u4HashIndex);
    IGP_RELEASE_MEM_BLOCK (IGP_RTR_MEMPOOL_ID, pIgpUpIfaceEntry);

    /* Decrement the number of upstream interfaces in the system */
    gIgmpProxyInfo.u2MaxUpIfCount--;

    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();

    /* Make the interface  as a downstream interface and 
     * start the Query timer */
    pIgmpIfNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);
    if (NULL == pIgmpIfNode)
    {
        return IGMP_FAILURE;
    }
    pIgmpQryNode = pIgmpIfNode->pIfaceQry;

    OsixGetSysTime (&(pIgmpQryNode->u4QuerierUpTime));

    /* update the Igmp query block with querier address */
    IGMP_IPVX_ADDR_COPY (&(pIgmpQryNode->u4QuerierAddr),
                         &(pIgmpIfNode->u4IfAddr));
    pIgmpQryNode->u1Querier = TRUE;
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pIgmpQryNode->QuerierDbNode);
    IgmpRedSyncDynInfo ();

    IGMP_GET_RANDOM_IN_RANGE (IGMP_MAX_RAND, u4RandomTime);
    /* Startup queries are sent randomly(0-5secs) */
    IGMPSTARTTIMER (pIgmpQryNode->Timer,
                    IGMP_STARTUP_QUERY_TIMER_ID, u4RandomTime, IGMP_ZERO);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfDeleteUpIfaceEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfGetUpIfaceEntry                               */
/*                                                                           */
/* Description        : This function gets the upstream interface entry      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Upstream interface index                 */
/*                                                                           */
/* Output(s)          : pointer to the upstream interface entry or NULL      */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpUpIfGetUpIfaceEntry (UINT4 u4IfIndex,
                        tIgmpPrxyUpIface ** ppRetIgpUpIfaceEntry)
{
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    UINT4               u4HashIndex = IGMP_ZERO;
    UINT1               u1EntryFound = IGMP_FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfGetUpIfaceEntry()\r \n");
    IGP_GET_UPIF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex,
                          pIgpUpIfaceEntry, tIgmpPrxyUpIface *)
    {
        if (pIgpUpIfaceEntry->u4IfIndex == u4IfIndex)
        {
            u1EntryFound = IGMP_TRUE;
            break;
        }
    }

    if (u1EntryFound == IGMP_FALSE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Upstream interface entry not found\r\n");
        return IGMP_FAILURE;
    }
    else
    {
        *ppRetIgpUpIfaceEntry = pIgpUpIfaceEntry;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfGetUpIfaceEntry()\r\n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfDeleteUpIfaceEntries                          */
/*                                                                           */
/* Description        : This function deletes all the upstream interfaces    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
IgpUpIfDeleteUpIfaceEntries (VOID)
{
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    tTMO_SLL_NODE      *pIgpUpIfaceNode = NULL;
    tTMO_SLL_NODE      *pIgpNextUpIfaceNode = NULL;
    tIgmpQuery         *pIgmpQryNode = NULL;
    tIgmpIface         *pIgmpIfNode = NULL;
    UINT4               u4HashIndex = IGMP_ZERO;
    UINT4               u4IfIndex = IGMP_ZERO;
    UINT4               u4RandomTime = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfDeleteUpIfaceEntries()\r \n");
    /* Scan through the upstream interface table and delete the 
     * upstream interface nodes */

    if (gIgmpProxyInfo.pUpIfaceTbl == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Upstream interfaces are not present \r\n");
        return;
    }

    TMO_HASH_Scan_Table (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex)
    {
        pIgpUpIfaceNode =
            TMO_HASH_Get_First_Bucket_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                            u4HashIndex);

        while (pIgpUpIfaceNode)
        {
            pIgpNextUpIfaceNode =
                TMO_HASH_Get_Next_Bucket_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                               u4HashIndex, pIgpUpIfaceNode);

            /* Stop the upstream interface timer if it is running */
            pIgpUpIfaceEntry = (tIgmpPrxyUpIface *) pIgpUpIfaceNode;
            TmrStop (gIgmpProxyInfo.ProxyTmrListId,
                     &(pIgpUpIfaceEntry->IfacePurgeTimer.TimerBlk));

            if (gIgmpConfig.u1IgmpStatus == IGMP_ENABLE)
            {
                u4IfIndex = pIgpUpIfaceEntry->u4IfIndex;

                /* Make the interface  as a downstream interface and 
                 * start the Query timer */
                pIgmpIfNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);
                if (NULL == pIgmpIfNode)
                {
                    continue;
                }
                pIgmpQryNode = pIgmpIfNode->pIfaceQry;

                OsixGetSysTime (&(pIgmpQryNode->u4QuerierUpTime));

                /* update the Igmp query block with querier address */
                IGMP_IPVX_ADDR_COPY (&(pIgmpQryNode->u4QuerierAddr),
                                     &(pIgmpIfNode->u4IfAddr));
                pIgmpQryNode->u1Querier = TRUE;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgmpQryNode->QuerierDbNode);
                IgmpRedSyncDynInfo ();

                IGMP_GET_RANDOM_IN_RANGE (IGMP_MAX_RAND, u4RandomTime);
                /* Startup queries are sent randomly(0-5secs) */
                IGMPSTARTTIMER (pIgmpQryNode->Timer,
                                IGMP_STARTUP_QUERY_TIMER_ID, u4RandomTime,
                                IGMP_ZERO);
            }

            TMO_HASH_Delete_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                  &pIgpUpIfaceEntry->UpIfHashLink, u4HashIndex);

            IGP_RELEASE_MEM_BLOCK (IGP_RTR_MEMPOOL_ID, pIgpUpIfaceEntry);

            pIgpUpIfaceNode = pIgpNextUpIfaceNode;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfDeleteUpIfaceEntries()\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfGetFirstUpIfaceEntry                          */
/*                                                                           */
/* Description        : This function gets the first upstream interface entry*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : ppRetIgpUpIfaceEntry - pointer to pointer to the     */
/*                      upstream interface entry                             */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpUpIfGetFirstUpIfaceEntry (tIgmpPrxyUpIface ** ppRetIgpUpIfaceEntry)
{
    UINT4               u4HashIndex = IGMP_ZERO;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIfaceFirst = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfGetFirstUpIfaceEntry()\r \n");
    /* Return the least node in the hash table */
    TMO_HASH_Scan_Table (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex)
    {
        pIgmpPrxyUpIface = (tIgmpPrxyUpIface *)
            TMO_HASH_Get_First_Bucket_Node (gIgmpProxyInfo.pUpIfaceTbl,
                                            u4HashIndex);
        if (pIgmpPrxyUpIface != NULL)
        {
            if ((pIgmpPrxyUpIfaceFirst == NULL) ||
                (pIgmpPrxyUpIface->u4IfIndex <
                 pIgmpPrxyUpIfaceFirst->u4IfIndex))
            {
                pIgmpPrxyUpIfaceFirst = pIgmpPrxyUpIface;
            }
        }
    }

    if (pIgmpPrxyUpIfaceFirst == NULL)
    {
        return IGMP_FAILURE;
    }

    *ppRetIgpUpIfaceEntry = pIgmpPrxyUpIfaceFirst;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfGetFirstUpIfaceEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfGetNextUpIfaceEntry                           */
/*                                                                           */
/* Description        : This function gets the first upstream interface entry*/
/*                                                                           */
/* Input(s)           : u4CurrentIndex - Upstream interface index (We have   */
/*                      to find next node to node with this index)           */
/*                                                                           */
/* Output(s)          : ppRetIgpUpIfaceEntry - pointer to pointer to the     */
/*                      upstream interface entry                             */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpUpIfGetNextUpIfaceEntry (UINT4 u4CurrentIndex,
                            tIgmpPrxyUpIface ** ppRetIgpUpIfaceEntry)
{
    UINT4               u4HashIndex = IGMP_ZERO;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIfaceNext = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfGetNextUpIfaceEntry()\r \n");
    TMO_HASH_Scan_Table (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex,
                              pIgmpPrxyUpIface, tIgmpPrxyUpIface *)
        {
            if ((pIgmpPrxyUpIface->u4IfIndex > u4CurrentIndex) &&
                ((pIgmpPrxyUpIfaceNext == NULL) ||
                 (pIgmpPrxyUpIface->u4IfIndex <
                  pIgmpPrxyUpIfaceNext->u4IfIndex)))
            {
                pIgmpPrxyUpIfaceNext = pIgmpPrxyUpIface;
                break;
            }
        }
    }

    if (pIgmpPrxyUpIfaceNext == NULL)
    {
        return IGMP_FAILURE;
    }

    *ppRetIgpUpIfaceEntry = pIgmpPrxyUpIfaceNext;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfGetNextUpIfaceEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfFormAndSendV3Report                     */
/*                                                                           */
/* Description        : This function will encode and send the v3 report to  */
/*                      the upstream                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Upstream interface index. If this value  */
/*                      is 0, send to all V3 upstream interfaces,            */
/*                                                                           */
/*                      gIgmpProxyInfo.u2PktOffset - Offset on               */
/*                                                   gpv3RepSendBuffer       */
/*                      gIgmpProxyInfo.u1NoOfGrpRecords - No of group        */
/*                                            records in the current packet  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfFormAndSendV3Report (UINT4 u4IfIndex)
{
    UINT2               u2PktSize = IGMP_ZERO;
    UINT2               u2IgmpPktSize = IGMP_ZERO;
    UINT2               u2NoOfGrpRecords = IGMP_ZERO;
    tIgmpV3Report       IgmpV3Hdr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfFormAndSendV3Report()\r \n");
    if (gIgmpProxyInfo.u2PktOffset == IGMP_ZERO)
    {
        return IGMP_SUCCESS;
    }

    MEMSET (&IgmpV3Hdr, IGMP_ZERO, sizeof (tIgmpV3Report));

    u2PktSize = (UINT2) (IGMP_DATA_START_OFFSET + gIgmpProxyInfo.u2PktOffset);

    pBuf = CRU_BUF_Allocate_MsgBufChain (u2PktSize, IGMP_ZERO);
    if (pBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Buffer allocation failure in forming"
                  "V3 report messages\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"in forming V3 report messages\r\n");
        return IGMP_FAILURE;
    }

    IgmpV3Hdr.u1MsgType = IGMP_PACKET_V3_REPORT;
    IgmpV3Hdr.u1Resvd = IGMP_ZERO;
    IgmpV3Hdr.u2CheckSum = IGMP_ZERO;
    IgmpV3Hdr.u2Resvd = IGMP_ZERO;
    u2NoOfGrpRecords = gIgmpProxyInfo.u1NoOfGrpRecords;
    IgmpV3Hdr.u2NumGrps = OSIX_HTONS (u2NoOfGrpRecords);

    /* copy the IGMPv3 header */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr,
                               IGMP_IP_HDR_RTR_OPT_LEN, IGMP_V3_HDR_LEN);

    /* copy all the group records */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) gpv3RepSendBuffer,
                               IGMP_DATA_START_OFFSET,
                               gIgmpProxyInfo.u2PktOffset);

    u2IgmpPktSize = (UINT2) (gIgmpProxyInfo.u2PktOffset + IGMP_V3_HDR_LEN);

    IgmpV3Hdr.u2CheckSum =
        IgmpCalcCheckSum (pBuf, (UINT4) u2IgmpPktSize, IGMP_IP_HDR_RTR_OPT_LEN);

    IgmpV3Hdr.u2CheckSum = OSIX_HTONS (IgmpV3Hdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr.u2CheckSum,
                               IGMP_IP_HDR_RTR_OPT_LEN + IGMP_TWO, IGMP_TWO);

    if (IgpUpIfSendUpstreamReport (pBuf, u4IfIndex, gIgpIPvXZero,
                                   u2PktSize, IGMP_VERSION_3) == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Sending of v3 report packet failed\r\n");
        return IGMP_FAILURE;
    }

    MEMSET (gpv3RepSendBuffer, IGMP_ZERO, IGP_REP_BUF_MEMBLK_SIZE);
    gIgmpProxyInfo.u2PktOffset = IGMP_ZERO;
    gIgmpProxyInfo.u1NoOfGrpRecords = IGMP_ZERO;

    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfFormAndSendV3Report()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfConstructUpstreamReport                       */
/*                                                                           */
/* Description        : This function will construct the upstream interface  */
/*                      report                                               */
/*                                                                           */
/* Input(s)           : u1OldFilterMode - Old filtermode of the group entry  */
/*                      OldInclSrcBmp - Old include bitmap of the group entry*/
/*                      OldExclSrcBmp - Old exclude bitmap of the group entry*/
/*                      pIgmpPrxyGrpInfo - Pointer to the updated group entry*/
/*                      u4GrpAddr - group address, used only while processing*/
/*                                  Leave                                    */
/*                      u1V1V2SendFlag - Flag can have different values      */
/*                       IGP_LEAVE_MESSAGE - the reoprt to be send is a leave*/
/*                       IGP_DONT_SEND_V1V2_REPORT - Dont send v1/v2 reports */
/*                       IGP_SEND_V1V2_REPORT - send v1/v2 reports           */
/*                                                                           */
/*                      u1V3SendFlag - Flag can have different values        */
/*                       IGP_DONT_SEND_V3_REPORT - Dont send v3 reports      */
/*                       IGP_SEND_V3_REPORT - send v1/v2 reports             */
/*                                                                           */
/* Global(s)          : gpv3RepSendBuffer - Global buffer for keeping the v3 */
/*                                          report to be sent                */
/*                      gpSrcInfoBuffer - Contains group records, that should*/
/*                                        be sent upstream                   */
/*                      gIgmpProxyInfo.u2PktOffset - Offset on               */
/*                                                   gpv3RepSendBuffer       */
/*                      gIgmpProxyInfo.u1NoOfGrpRecords - No of group        */
/*                                            records in the current packet  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfConstructUpstreamReport (UINT1 u1OldFilterMode,
                                tIgmPxySrcBmp OldInclSrcBmp,
                                tIgmPxySrcBmp OldExclSrcBmp,
                                tIgmpPrxyGrpInfo * pIgmpPrxyGrpInfo,
                                tIgmpIPvXAddr u4GrpAddr, UINT1 u1V1V2SendFlag,
                                UINT1 u1V3SendFlag)
{
    UINT2               u2PktLength = IGMP_ZERO;
    UINT2               u2OldNoOfGrpRecords = gIgmpProxyInfo.u1NoOfGrpRecords;
    UINT1               u1Diff = IGMP_ZERO;
    UINT1               u1Leave = IGMP_FALSE;
    INT4                i4RetStatus = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfConstructUpstreamReport()\r \n");
    if ((gIgmpProxyInfo.u1V1UpstrmCnt != IGMP_ZERO) &&
        (u1V1V2SendFlag == IGP_SEND_V1V2_REPORT))
    {
        /* Send V1 report to all upstream interfaces, having their operating 
         * version as V1 */
        if (IgpUpIfFormAndSendV1V2Report (u4GrpAddr, IGMP_VERSION_1,
                                          u1Leave, IGMP_ZERO) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Fatal Error: sending of V1 report failed\r\n");
            i4RetStatus = IGMP_FAILURE;
        }
    }

    /* Send V2 report to all upstream interfaces, having their operating 
     * version as V2 */
    if ((gIgmpProxyInfo.u1V2UpstrmCnt != IGMP_ZERO) &&
        (u1V1V2SendFlag != IGP_DONT_SEND_V1V2_REPORT))
    {
        if (u1V1V2SendFlag == IGP_LEAVE_MESSAGE)
        {
            u1Leave = IGMP_TRUE;
        }
        if (IgpUpIfFormAndSendV1V2Report (u4GrpAddr, IGMP_VERSION_2,
                                          u1Leave, IGMP_ZERO) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Fatal Error: sending of V2 report failed\r\n");
            i4RetStatus = IGMP_FAILURE;
        }
    }

    if (gIgmpProxyInfo.u1V3UpstrmCnt != IGMP_ZERO)
    {
        /* u1V3UpstrmFlag indicates, whether any V3 upstream 
         * interface is present */
        /* Create a Group record for the changed group */
        if (IgpUtilCreateGrpRecord4Change (u1OldFilterMode, OldInclSrcBmp,
                                           OldExclSrcBmp, pIgmpPrxyGrpInfo,
                                           u4GrpAddr,
                                           &u2PktLength) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Group record creation failed\r\n");
            return IGMP_FAILURE;
        }

        if (u2PktLength + gIgmpProxyInfo.u2PktOffset >= IGMP_MAX_PKT_SIZE)
        {
            /* If total packet size is greater than allowed, send the 
             * packet withoutthe new group records*/

            u1Diff =
                (UINT1) (gIgmpProxyInfo.u1NoOfGrpRecords - u2OldNoOfGrpRecords);
            gIgmpProxyInfo.u1NoOfGrpRecords = (UINT1) u2OldNoOfGrpRecords;

            if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Fatal Error: Encoding of V3 report failed\r\n");
                return IGMP_FAILURE;
            }
            gIgmpProxyInfo.u1NoOfGrpRecords += u1Diff;
        }

        MEMCPY (gpv3RepSendBuffer + gIgmpProxyInfo.u2PktOffset,
                gpSrcInfoBuffer, u2PktLength);
        MEMSET (gpSrcInfoBuffer, IGMP_ZERO, IGP_SRC_BUF_MEMBLK_SIZE);

        gIgmpProxyInfo.u2PktOffset += u2PktLength;

        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &gIgmpProxyInfo.IgpInfoDbNode);
        IgmpRedSyncDynInfo ();
        /* Send V3 report only when the flag is set */
        if (u1V3SendFlag == IGP_SEND_V3_REPORT)
        {
            if (IgpUpIfFormAndSendV3Report (IGMP_ZERO) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Fatal Error: Encoding of V3 report failed\r\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,IGMP_CNTRL_MODULE,
		      IGMP_NAME,IgmpSysErrString[SYS_LOG_V3_ENCODING_FAIL],
			"Fatal Error\n");
                return IGMP_FAILURE;
            }
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfConstructUpstreamReport()\r \n");
    return i4RetStatus;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfFormAndSendV1V2Report                   */
/*                                                                           */
/* Description        : This function will construct and send the V1/V2      */
/*                      upstream                                             */
/*                                                                           */
/* Input(s)           : u4GrpAddr - Ip address of the group                  */
/*                      u1Version  - IGMP version (V1/V2)                    */
/*                      u1Leave - Whether the report to be sent is a leave   */
/*                      u4IfIndex - Send report to only this interface. If   */
/*                      this value is 0, send to all upstream interfaces,    */
/*                      having the same version                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfFormAndSendV1V2Report (tIgmpIPvXAddr u4GrpAddr, UINT1 u1Version,
                              UINT1 u1Leave, UINT4 u4IfIndex)
{
    UINT4               u4PktSize = IGMP_ZERO;
    tIgmpPkt            IgmpPkt;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IpGrp = 0;
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddr, IPVX_ADDR_FMLY_IPV4);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfFormAndSendV1V2Report()\r \n");
    u4PktSize = IGMP_IP_HDR_RTR_OPT_LEN + IGMP_PACKET_SIZE;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, IGMP_ZERO);

    if (pBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Buffer allocation failure in forming"
                  "V1/V2 report messages\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
	      IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"in forming V1/V2 report messages\r\n");
        return IGMP_FAILURE;
    }

    IgmpPkt.u1MaxRespTime = IGMP_ZERO;
    IgmpPkt.u2CheckSum = IGMP_ZERO;
    IgmpPkt.u4GroupAddr = OSIX_HTONL (u4IpGrp);

    if (u1Leave == IGMP_TRUE)
    {
        IgmpPkt.u1PktType = IGMP_PACKET_LEAVE;
    }
    else if (u1Version == IGMP_VERSION_1)
    {
        IgmpPkt.u1PktType = IGMP_PACKET_V1_REPORT;
    }
    else
    {
        IgmpPkt.u1PktType = IGMP_PACKET_V2_REPORT;
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpPkt,
                               IGMP_IP_HDR_RTR_OPT_LEN, IGMP_PACKET_SIZE);

    IgmpPkt.u2CheckSum = IgmpCalcCheckSum (pBuf, IGMP_PACKET_SIZE,
                                           IGMP_IP_HDR_RTR_OPT_LEN);

    IgmpPkt.u2CheckSum = OSIX_HTONS (IgmpPkt.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpPkt.u2CheckSum,
                               IGMP_IP_HDR_RTR_OPT_LEN + IGMP_TWO, IGMP_TWO);

    /* Send the packet upstream after filling IP header */
    if (IgpUpIfSendUpstreamReport (pBuf, u4IfIndex, u4GrpAddr,
                                   (UINT2) u4PktSize,
                                   u1Version) == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Sending of V3 report to upstream failed\r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfFormAndSendV1V2Report()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfProcessQuery                                  */
/*                                                                           */
/* Description        : This function will process upstream queries and send */
/*                      reports accordingly                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index of upstream router       */
/*                      u4GrpAddress - IP address of the group               */
/*                      u4NoOfSrcs - Number of sources in the query          */
/*                      pu4Sources - List of IP address of sources           */
/*                      u1Version - Version of the incoming query            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfProcessQuery (UINT4 u4IfIndex, tIgmpIPvXAddr u4GrpAddress,
                     UINT4 u4NoOfSrcs, UINT4 *pu4Sources, UINT1 u1Version)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfProcessQuery()\r \n");
    if (IgpUpIfGetUpIfaceEntry (u4IfIndex, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface %d configured as an upstream interface\r\n",
                       u4IfIndex);
        return IGMP_FAILURE;
    }
    if (pIgmpPrxyUpIface->u1RowStatus != IGMP_ACTIVE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Interface %d is not active\r\n", u4IfIndex);
        return IGMP_FAILURE;
    }

    /* Update the operating version of upstream interface and increment the 
     * Upstream count when the version is changed to IGMP v3 and decrement 
     * the count when the version is changed to IGMP v1 or v2 from IGMP v3 */
    if (pIgmpPrxyUpIface->u1OperVersion != u1Version)
    {
        IgpUpIfUpdateVersionCount (pIgmpPrxyUpIface, u1Version);
    }

    /* Re-start the router purge timer */
    if (TmrStart (gIgmpProxyInfo.ProxyTmrListId,
                  &(pIgmpPrxyUpIface->IfacePurgeTimer.TimerBlk),
                  IGP_RTR_IFACE_TIMER,
                  pIgmpPrxyUpIface->u2RtrPurgeInt, 0) == TMR_FAILURE)
    {

        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Router purge interval timer start failed\r\n");
    }

    /* General query processing */
    if ((IGMP_IPVX_ADDR_COMPARE (u4GrpAddress, gIgpIPvXZero) == 0) &&
        (u4NoOfSrcs == 0))
    {
        /* This function will be called for IGMP V1/V2/V3 general queries */
        if (IgpUpIfProcessGeneralQuery (pIgmpPrxyUpIface) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Fatal error: Processing of general query failed\r\n");
            SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_PACKET_MODULE,
	      IGMP_NAME,IgmpSysErrString[SYS_LOG_PROCESS_GEN_QUERY_FAIL],
		       "Fatal error\n");
	    return IGMP_FAILURE;
        }
    }
    else if (u4NoOfSrcs == 0)    /* Group specific query processing */
    {
        /* This function will be called for IGMP V2/V3 group 
         * specific queries */
        if (IgpUpIfProcessGrpSpecificQuery (pIgmpPrxyUpIface,
                                            u4GrpAddress) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Fatal error: Processing of group specific"
                      "query failed\r\n");
	    SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_PACKET_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_PROCESS_GROUP_QUERY_FAIL],
	 	"for group %s\n",IgmpPrintIPvxAddress (u4GrpAddress));
            return IGMP_FAILURE;
        }
    }
    else
    {
        /* This function will be called for IGMP V3 group 
         * and source specific queries */
        if (IgpUpIfProcessGrpAndSrcSpfQuery (pIgmpPrxyUpIface,
                                             u4GrpAddress,
                                             u4NoOfSrcs,
                                             pu4Sources) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Fatal error: Processing of group and source"
                      "specific query failed\r\n");
	     SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_PACKET_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_PROCESS_GROUP_SRC_QUERY_FAIL],
		"for group %s\n",IgmpPrintIPvxAddress (u4GrpAddress));
            return IGMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfProcessQuery()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfProcessGeneralQuery                           */
/*                                                                           */
/* Description        : This function will process upstream general queries  */
/*                      and send reports for all the groups in the group     */
/*                      membership database based on the operating version   */
/*                      of IGMP on the upstream interface.                   */
/*                                                                           */
/* Input(s)           : pIgmpPrxyUpIface - Upstream Interface entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfProcessGeneralQuery (tIgmpPrxyUpIface * pIgmpPrxyUpIface)
{
    tIgmpPrxyGrpInfo   *pIgmpPrxyGrpInfo = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    UINT2               u2PktLength = IGMP_ZERO;

    gIgmpProxyInfo.u1NoOfGrpRecords = IGMP_ZERO;
    gIgmpProxyInfo.u2PktOffset = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfProcessGeneralQuery()\r \n");
    if (gIgmpProxyInfo.GroupEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "No Group entries found in the group database\r\n");
        return IGMP_FAILURE;
    }

    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.GroupEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext = RBTreeGetNext (gIgmpProxyInfo.GroupEntry, pRBElem, NULL);

        pIgmpPrxyGrpInfo = (tIgmpPrxyGrpInfo *) pRBElem;

        /* Form and send IGMP v1 report if the operating version is 
         * IGMP v1 */
        if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_1)
        {
            if (IgpUpIfFormAndSendV1V2Report
                (pIgmpPrxyGrpInfo->u4GrpIpAddr, IGMP_VERSION_1, IGMP_FALSE,
                 pIgmpPrxyUpIface->u4IfIndex) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Fatal error: Construction of V1 report failed\r\n");
		SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,IGMP_PACKET_MODULE,
		      IGMP_NAME,IgmpSysErrString[SYS_LOG_V1_CONSTRUCT_FAIL],
			"on interaface %d\n", pIgmpPrxyUpIface->u4IfIndex);
                return IGMP_FAILURE;
            }
        }
        /* Form and send IGMP v2 report if the operating version is 
         * IGMP v2 */
        else if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_2)
        {
            if (IgpUpIfFormAndSendV1V2Report
                (pIgmpPrxyGrpInfo->u4GrpIpAddr, IGMP_VERSION_2, IGMP_FALSE,
                 pIgmpPrxyUpIface->u4IfIndex) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Fatal error: Construction of V2 report failed\r\n");
		SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,IGMP_PACKET_MODULE,
                      IGMP_NAME,IgmpSysErrString[SYS_LOG_V2_CONSTRUCT_FAIL],
			"on interaface %d\n", pIgmpPrxyUpIface->u4IfIndex);
                return IGMP_FAILURE;
            }
        }
        /* Form and send IGMP v3 report if the operating version is 
         * IGMP v3 */
        else
        {
            /* create a group record for the current group entry */
            IgpUtilCreateGrpRecord4Xsisting (pIgmpPrxyGrpInfo, &u2PktLength,
                                             gIgpIPvXZero, NULL);

            if (u2PktLength + gIgmpProxyInfo.u2PktOffset >= IGMP_MAX_PKT_SIZE)
            {
                (gIgmpProxyInfo.u1NoOfGrpRecords)--;
                if (IgpUpIfFormAndSendV3Report
                    (pIgmpPrxyUpIface->u4IfIndex) == IGMP_FAILURE)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Encoding of V3 report failed\r\n");
		    SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,IGMP_PACKET_MODULE,
                      IGMP_NAME,IgmpSysErrString[SYS_LOG_V3_ENCODING_FAIL],
			"on interaface %d\n", pIgmpPrxyUpIface->u4IfIndex);
                    return IGMP_FAILURE;
                }

                gIgmpProxyInfo.u1NoOfGrpRecords++;
            }

            MEMCPY (gpv3RepSendBuffer + gIgmpProxyInfo.u2PktOffset,
                    gpSrcInfoBuffer, u2PktLength);

            MEMSET (gpSrcInfoBuffer, IGMP_ZERO, IGP_SRC_BUF_MEMBLK_SIZE);

            gIgmpProxyInfo.u2PktOffset += u2PktLength;
        }
        pRBElem = pRBElemNext;
    }
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();
    if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_3)
    {
        if (IgpUpIfFormAndSendV3Report
            (pIgmpPrxyUpIface->u4IfIndex) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Encoding of V3 report failed\r\n");
	    SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG,IGMP_PACKET_MODULE,
                      IGMP_NAME,IgmpSysErrString[SYS_LOG_V3_ENCODING_FAIL],
			"on interaface %d\n", pIgmpPrxyUpIface->u4IfIndex);
            return IGMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfProcessGeneralQuery()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfProcessGrpSpecificQuery                       */
/*                                                                           */
/* Description        : This function will process upstream group specific   */
/*                      query and send reports accordingly                   */
/*                                                                           */
/* Input(s)           : pIgmpPrxyUpIface - Upstream Interface entry          */
/*                      u4GrpAddress - IP address of the group               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfProcessGrpSpecificQuery (tIgmpPrxyUpIface * pIgmpPrxyUpIface,
                                tIgmpIPvXAddr u4GrpAddress)
{
    tIgmpPrxyGrpInfo   *pIgmpPrxyGrpInfo = NULL;
    UINT2               u2PktLength = IGMP_ZERO;
    UINT4               u4IpGrp = 0;
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddress, IPVX_ADDR_FMLY_IPV4);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfProcessGrpSpecificQuery()\r \n");
    /* There is no IGMPv1 group specific query */
    if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_1)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "There is no IGMPv1 group specific query\r\n");
        return IGMP_FAILURE;
    }
    /* Get the group entry from the consolidated database */
    if (IgpGrpGetGroupEntry (u4GrpAddress, &pIgmpPrxyGrpInfo) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "Group Entry not present for group address %x\r\n",
                       u4IpGrp);
        return IGMP_SUCCESS;
    }

    if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_2)
    {
        if (IgpUpIfFormAndSendV1V2Report (u4GrpAddress, IGMP_VERSION_2,
                                          IGMP_FALSE,
                                          pIgmpPrxyUpIface->u4IfIndex)
            == IGMP_FAILURE)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                           "IGMP v2 Form and send Failed for group %x\r\n",
                           u4IpGrp);
            return IGMP_FAILURE;
        }
    }
    else
    {
        gIgmpProxyInfo.u1NoOfGrpRecords = IGMP_ZERO;
        gIgmpProxyInfo.u2PktOffset = IGMP_ZERO;

        /* create group record */
        IgpUtilCreateGrpRecord4Xsisting (pIgmpPrxyGrpInfo, &u2PktLength,
                                         gIgpIPvXZero, NULL);

        MEMCPY (gpv3RepSendBuffer + gIgmpProxyInfo.u2PktOffset,
                gpSrcInfoBuffer, u2PktLength);
        MEMSET (gpSrcInfoBuffer, IGMP_ZERO, IGP_SRC_BUF_MEMBLK_SIZE);

        gIgmpProxyInfo.u2PktOffset += u2PktLength;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &gIgmpProxyInfo.IgpInfoDbNode);
        IgmpRedSyncDynInfo ();
        if (IgpUpIfFormAndSendV3Report (pIgmpPrxyUpIface->u4IfIndex)
            == IGMP_FAILURE)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                           "IGMP v3 Form and send Failed for group %x\r\n",
                           u4IpGrp);
            return IGMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfProcessGrpSpecificQuery()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfProcessGrpAndSrcSpfQuery                      */
/*                                                                           */
/* Description        : This function will process upstream group and source */
/*                      specific query and send reports accordingly          */
/*                                                                           */
/* Input(s)           : pIgmpPrxyUpIface - Upstream Interface entry          */
/*                      u4GrpAddress - IP address of the group               */
/*                      u4NoOfSrcs - Number of sources in the query          */
/*                      pu4Sources - List of IP address of sources           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUpIfProcessGrpAndSrcSpfQuery (tIgmpPrxyUpIface * pIgmpPrxyUpIface,
                                 tIgmpIPvXAddr u4GrpAddress, UINT4 u4NoOfSrcs,
                                 UINT4 *pu4Sources)
{
    tIgmPxySrcBmp       IgmPxySrcBmp;
    tIgmpPrxyGrpInfo   *pIgmpPrxyGrpInfo = NULL;
    UINT4               u4LoopVar = IGMP_ZERO;
    UINT4               u4Source = IGMP_ZERO;
    UINT2               u2PktLength = IGMP_ZERO;
    UINT2               u2SrcIndex = IGMP_ZERO;
    BOOL1               bResult = IGMP_FALSE;
    UINT4               u4IpGrp = 0;
    tIgmpIPvXAddr       SrcAddr;
    IGMP_IPVX_ADDR_CLEAR (&SrcAddr);
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddress, IPVX_ADDR_FMLY_IPV4);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfProcessGrpAndSrcSpfQuery()\r \n");
    MEMSET (IgmPxySrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);

    if (pIgmpPrxyUpIface->u1OperVersion != IGMP_VERSION_3)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Group and source specific query is only"
                  "supported in V3\r\n");
        return IGMP_FAILURE;
    }

    if (IgpGrpGetGroupEntry (u4GrpAddress, &pIgmpPrxyGrpInfo) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "Group Entry not present for group address %x\r\n",
                       u4IpGrp);
        return IGMP_SUCCESS;
    }

    gIgmpProxyInfo.u1NoOfGrpRecords = IGMP_ZERO;
    gIgmpProxyInfo.u2PktOffset = IGMP_ZERO;

    for (; u4LoopVar < u4NoOfSrcs; u4LoopVar++)
    {
        u4Source = 0;
        u4Source = OSIX_NTOHL (pu4Sources[u4LoopVar]);
        IGMP_IPVX_ADDR_INIT_IPV4 (SrcAddr, IPVX_ADDR_FMLY_IPV4,
                                  (UINT1 *) &u4Source);

        IGP_IS_SOURCE_PRESENT (SrcAddr, bResult, u2SrcIndex);

        if (bResult == IGMP_TRUE)
        {
            u2SrcIndex = (UINT2) (u2SrcIndex + 1);

            if (pIgmpPrxyGrpInfo->u1FilterMode == IGMP_FMODE_EXCLUDE)
            {
                /* If the filter mode is EXCLUDE, and if the source is in 
                 * Exclude list it is not needed */
                OSIX_BITLIST_IS_BIT_SET (pIgmpPrxyGrpInfo->ExclSrcBmp,
                                         u2SrcIndex, IGP_SRC_LIST_SIZE,
                                         bResult);
                if (bResult == OSIX_TRUE)
                {
                    continue;
                }
            }
            else
            {
                /* If the filter mode is INCLUDE and if the source is not 
                 * present in the Include list, the source is not needed */
                OSIX_BITLIST_IS_BIT_SET (pIgmpPrxyGrpInfo->InclSrcBmp,
                                         u2SrcIndex, IGP_SRC_LIST_SIZE,
                                         bResult);
                if (bResult == OSIX_FALSE)
                {
                    continue;
                }
            }
        }
        else
        {
            /* if the source is not found, in the global source array,
             * it will be needed only when the filtermode is exclud */
            if (pIgmpPrxyGrpInfo->u1FilterMode == IGMP_FMODE_INCLUDE)
            {
                continue;
            }
        }

        OSIX_BITLIST_SET_BIT (IgmPxySrcBmp, u2SrcIndex, IGP_SRC_LIST_SIZE);
    }

    /* If there are no sources continue needed for the group no need 
     * to respond to the group and source specific query */
    if (MEMCMP (IgmPxySrcBmp, gIgpNullSrcBmp, IGP_SRC_LIST_SIZE) == IGMP_ZERO)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "Sources are not present for group address %x\r\n",
                       u4IpGrp);
        return IGMP_SUCCESS;
    }

    IgpUtilCreateGrpRecord4Xsisting (NULL, &u2PktLength,
                                     pIgmpPrxyGrpInfo->u4GrpIpAddr,
                                     IgmPxySrcBmp);

    MEMCPY (gpv3RepSendBuffer + gIgmpProxyInfo.u2PktOffset,
            gpSrcInfoBuffer, u2PktLength);
    MEMSET (gpSrcInfoBuffer, IGMP_ZERO, IGP_SRC_BUF_MEMBLK_SIZE);

    gIgmpProxyInfo.u2PktOffset += u2PktLength;
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();
    if (IgpUpIfFormAndSendV3Report (pIgmpPrxyUpIface->u4IfIndex)
        == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "IGMP v3 Form and send Failed for group %x\r\n",
                       u4IpGrp);
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfProcessGrpAndSrcSpfQuery()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfSendUpstreamReport                            */
/*                                                                           */
/* Description        : This function sends out the upstream report to the   */
/*                      specified index; If the given index is zero, send    */
/*                      the report to all upstream interfaces having the     */
/*                      given version                                        */
/*                                                                           */
/* Input(s)           : pBuffer  - Pointer to the packet buffer              */
/*                      u4IfIndex - index of upstream interface              */
/*                      u4GrpAddress - group for which, report is being sent */
/*                      u2Len - length of the packet                         */
/*                      u1Version - Version of report being sendt            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpUpIfSendUpstreamReport (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4IfIndex,
                           tIgmpIPvXAddr u4GrpAddress, UINT2 u2Len,
                           UINT1 u1Version)
{
    tIgmpIface         *pIgmpIfNode = NULL;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Dest = IGMP_ZERO;
    UINT4               u4Src = IGMP_ZERO;
    UINT4               u4HashIndex = IGMP_ZERO;
    UINT1               u1Leave = IGMP_ZERO;
    UINT4               u4IpGrp = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfSendUpstreamReport()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddress, IPVX_ADDR_FMLY_IPV4);

    if (CRU_BUF_Copy_FromBufChain (pBuffer, &u1Leave, IGMP_IP_HDR_RTR_OPT_LEN,
                                   IGMP_ONE) == CRU_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Getting the packet type from buffer failed\r\n");
        return IGMP_FAILURE;
    }

    if (u1Leave == IGMP_PACKET_LEAVE)
    {
        u4Dest = IGMP_ALL_ROUTERS_GROUP;
    }
    else if (u1Version != IGMP_VERSION_3)
    {
        u4Dest = u4IpGrp;
    }
    else
    {
        u4Dest = IGMP_ALL_V3_ROUTERS_GROUP;
    }

    if (u4IfIndex != IGMP_ZERO)
    {
        /* Upstream interface is given. send packet to only this interface */
        pIgmpIfNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);
        if (NULL == pIgmpIfNode)
        {
            CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
            return IGMP_FAILURE;
        }
        IGMP_IP_COPY_FROM_IPVX (&u4Src, pIgmpIfNode->u4IfAddr,
                                IPVX_ADDR_FMLY_IPV4);
        /* form the IP header and attach it to packet */
        IgmpFormIPHdr (pBuffer, u4Src, u4Dest, IGMP_ZERO, u2Len);

        if (IgpPortSocketSend (pBuffer, u4Src,
                               u4Dest, u2Len, IGMP_TRUE) == IGMP_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
            return IGMP_FAILURE;
        }

        /* Update the statistics */
        if (u1Version != IGMP_VERSION_3)
        {
            IGMP_V1V2REP_TX (pIgmpIfNode);
        }
        else
        {
            IGMP_V3REP_TX (pIgmpIfNode);
        }

        CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
        return IGMP_SUCCESS;
    }

    TMO_HASH_Scan_Table (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex,
                              pIgmpPrxyUpIface, tIgmpPrxyUpIface *)
        {
            if ((pIgmpPrxyUpIface->u1RowStatus == IGMP_ACTIVE) &&
                (pIgmpPrxyUpIface->u1OperVersion == u1Version))
            {
                pIgmpIfNode = IgmpGetIfNode (pIgmpPrxyUpIface->u4IfIndex,
                                             IPVX_ADDR_FMLY_IPV4);
                if (NULL == pIgmpIfNode)
                {
                    continue;
                }
                IGMP_IP_COPY_FROM_IPVX (&u4Src, pIgmpIfNode->u4IfAddr,
                                        IPVX_ADDR_FMLY_IPV4);

                /* form the IP header and attach it to packet */
                IgmpFormIPHdr (pBuffer, u4Src, u4Dest, IGMP_ZERO, u2Len);

                if (IgpPortSocketSend (pBuffer, u4Src, u4Dest,
                                       u2Len, IGMP_TRUE) == IGMP_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
                    return IGMP_FAILURE;
                }
                /* Update the statistics */
                if (u1Version != IGMP_VERSION_3)
                {
                    IGMP_V1V2REP_TX (pIgmpIfNode);
                }
                else
                {
                    IGMP_V3REP_TX (pIgmpIfNode);
                }

            }
        }
    }

    CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfSendUpstreamReport()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfUpdateUpIface                                 */
/*                                                                           */
/* Description        : This function will update the Upstream interface     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUpIfUpdateUpIface (UINT4 u4IfIndex)
{
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    UINT4               u4HashIndex = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUpIfUpdateUpIface()\r \n");
    /* Get the Interface node from the hash table */
    IGP_GET_UPIF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex,
                          pIgpUpIfaceEntry, tIgmpPrxyUpIface *)
    {
        if (pIgpUpIfaceEntry->u4IfIndex == u4IfIndex)
        {
            break;
        }
    }

    if (NULL == pIgpUpIfaceEntry)
    {
        return;
    }
    pIgpUpIfaceEntry->u1RowStatus = IGMP_ACTIVE;

    /* Increment the Upstream interface version count directly */
    if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_3)
    {
        gIgmpProxyInfo.u1V3UpstrmCnt++;
    }
    else if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_2)
    {
        gIgmpProxyInfo.u1V2UpstrmCnt++;
    }
    else
    {
        gIgmpProxyInfo.u1V1UpstrmCnt++;
    }
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();

    if (IgpFwdUpdateFwdTable (gIgpIPvXZero, gIgpIPvXZero,
                              pIgpUpIfaceEntry->u4IfIndex,
                              IGMP_ZERO, IGP_ADD_OIF, NULL) != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Upstream interface additon to forwarding table"
                  " failed\r\n");
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUpIfUpdateUpIface()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUpIfUpdateVersionCount                            */
/*                                                                           */
/* Description        : This function will update the Upstream interface     */
/*                      and update the version counts for various IGMP       */
/*                      version                                              */
/*                                                                           */
/* Input(s)           : pIgpUpIfaceEntry  - Pointer to Upstream Iface        */
/*                      u1Version - IGMP version                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUpIfUpdateVersionCount (tIgmpPrxyUpIface * pIgpUpIfaceEntry, UINT1 u1Version)
{
    switch (u1Version)
    {
        case IGMP_VERSION_1:

            if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_2)
            {
                gIgmpProxyInfo.u1V2UpstrmCnt--;
            }
            else if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_3)
            {
                gIgmpProxyInfo.u1V3UpstrmCnt--;
            }

            pIgpUpIfaceEntry->u1OperVersion = IGMP_VERSION_1;
            gIgmpProxyInfo.u1V1UpstrmCnt++;

            break;

        case IGMP_VERSION_2:

            if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_1)
            {
                gIgmpProxyInfo.u1V1UpstrmCnt--;
            }
            else if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_3)
            {
                gIgmpProxyInfo.u1V3UpstrmCnt--;
            }

            pIgpUpIfaceEntry->u1OperVersion = IGMP_VERSION_2;
            gIgmpProxyInfo.u1V2UpstrmCnt++;

            break;

        case IGMP_VERSION_3:

            if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_1)
            {
                gIgmpProxyInfo.u1V1UpstrmCnt--;
            }
            else if (pIgpUpIfaceEntry->u1OperVersion == IGMP_VERSION_2)
            {
                gIgmpProxyInfo.u1V2UpstrmCnt--;
            }

            pIgpUpIfaceEntry->u1OperVersion = IGMP_VERSION_3;
            gIgmpProxyInfo.u1V3UpstrmCnt++;

            break;

        default:
            break;
    }

    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pIgpUpIfaceEntry->UpIntDbNode);
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();

    return;
}
