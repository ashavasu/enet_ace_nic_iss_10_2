/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igpapi.c,v 1.6 2015/02/17 10:16:59 siva Exp $
 *
 * Description:This file contains the APIs provided to MRI Module
 *
 *******************************************************************/

/************************************************************/
/* Copyright (C) Aricent Technologies(Holdings)Ltd,2008-2009*/
/* Licensee Future Communications Software, 2008-2009       */
/*                                                          */
/*  FILE NAME             :  igpapi.c                       */
/*  PRINCIPAL AUTHOR      :  Aricent                        */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :  IGMP Proxy                     */
/*  LANGUAGE              :  C                              */
/*  TARGET ENVIRONMENT    :  Any                            */
/*  DATE OF FIRST RELEASE :  --                             */
/*  AUTHOR                :  Aricent                        */
/*  DESCRIPTION           :  This files containd the APIs   */
/*                           provided to MRI module         */
/************************************************************/
/*  Change History                                          */
/*  Version               :--                               */
/*  Date(DD/MM/YYYY)      :--                               */
/*  Modified by           :--                               */
/*  Description of change :--                               */
/************************************************************/

#include "igpinc.h"
#include "mri.h"

PRIVATE INT4        IgmpProxyUtilRbCompare (tRBElem *, tRBElem *);

static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;

/***************************************************************/
/*  Function Name   : IgmpProxyUtilRbCompare                   */
/*  Description     : Utillity function used to compare the    */
/*                    two RB-Tree elements                     */
/*  Input(s)        : pForwardEntryNode, pForwardEntryIn       */
/*  Output(s)       : -1/0/1                                   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  None                                    */
/***************************************************************/

INT4
IgmpProxyUtilRbCompare (tRBElem * pForwardEntryNode, tRBElem * pForwardEntryIn)
{
    /* Compare the Group IP address of the forwarding entry */
    if (IGMP_IPVX_ADDR_COMPARE
        ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4GrpIpAddr),
         (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4GrpIpAddr)) < IGMP_ZERO)
    {
        return -1;
    }
    else if (IGMP_IPVX_ADDR_COMPARE
             ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4GrpIpAddr),
              (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4GrpIpAddr)) >
             IGMP_ZERO)
    {
        return 1;
    }

    /* Compare the source IP address of the forwarding entry */
    if (IGMP_IPVX_ADDR_COMPARE
        ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4SrcIpAddr),
         (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4SrcIpAddr)) < IGMP_ZERO)
    {
        return -1;
    }
    else if (IGMP_IPVX_ADDR_COMPARE
             ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4SrcIpAddr),
              (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4SrcIpAddr)) >
             IGMP_ZERO)
    {
        return 1;
    }

    return 0;
}

/***************************************************************/
/*  Function Name   :  IgmpProxyValidateRouteTblIndex          */
/*  Description     :  Function validate the IGMP Proxy Mroute */
/*                     index                                   */
/*  Input(s)        :  u4GroupAddr - Group Address             */
/*                     u4SrcAddr - Source Address              */
/*                     u4SrcMask - Source Mask                 */
/*  Output(s)       :                                          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
/*As MRI use this func, it is U4 specific in stead of VX*/
INT4
IgmpProxyValidateRouteTblIndex (UINT4 u4GroupAddr, UINT4 u4SrcAddr,
                                UINT4 u4SrcMask)
{
    tIgmpPrxyFwdEntry   IgpForwardEntry;

    UNUSED_PARAM (u4SrcMask);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyValidateRouteTblIndex()\r \n");
    IgmpLock ();

    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }
    /*It is Mask specific in stead of Masklength */
    if (u4SrcMask != IGP_DEFAULT_SOURCE_MASK)
    {
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));

    IGMP_IPVX_ADDR_INIT_IPV4 ((IgpForwardEntry.u4SrcIpAddr),
                              IPVX_ADDR_FMLY_IPV4, (UINT1 *) &u4SrcAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 ((IgpForwardEntry.u4GrpIpAddr),
                              IPVX_ADDR_FMLY_IPV4, (UINT1 *) &u4GroupAddr);

    if (RBTreeGet (gIgmpProxyInfo.ForwardEntry,
                   (tRBElem *) & IgpForwardEntry) == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast forwarding entry not found for"
                       " source %x group %x \r\n", u4SrcAddr, u4GroupAddr);
        IgmpUnLock ();
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyValidateRouteTblIndex()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   :  IgmpProxyGetNextRouteIndex              */
/*  Description     :  Gives the next route entry index of the */
/*                     given index.                            */
/*  Input(s)        :  u4GroupAddr - Group Address             */
/*                     u4SrcAddr - Source Address              */
/*                     u4SrcMask - Source Mask                 */
/*  Output(s)       :  u4GroupAddr - Next Group Address        */
/*                     u4SrcAddr - Next Source Address         */
/*                     u4SrcMask - Next Source Mask            */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
/*as MRI use this func, this is U4 specific in stead VX */
INT4
IgmpProxyGetNextRouteIndex (UINT4 u4GroupAddr, UINT4 u4SrcAddr,
                            UINT4 u4SrcMask, UINT4 *pu4NextGroupAddr,
                            UINT4 *pu4NextSrcAddr, UINT4 *pu4NextSrcMask)
{
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyGetNextRouteIndex()\r \n");
    UNUSED_PARAM (u4SrcMask);

    IgmpLock ();
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    IGMP_IPVX_ADDR_INIT_IPV4 ((IgpForwardEntry.u4GrpIpAddr),
                              IPVX_ADDR_FMLY_IPV4, (UINT1 *) &u4GroupAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 ((IgpForwardEntry.u4SrcIpAddr),
                              IPVX_ADDR_FMLY_IPV4, (UINT1 *) &u4SrcAddr);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGetNext (gIgmpProxyInfo.ForwardEntry,
                       (tRBElem *) & IgpForwardEntry, IgmpProxyUtilRbCompare);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Multicast forwarding entry not found\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }
    IGMP_IP_COPY_FROM_IPVX (pu4NextGroupAddr, pIgmpPrxyFwdEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (pu4NextSrcAddr, pIgmpPrxyFwdEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    *pu4NextSrcMask = IGP_DEFAULT_SOURCE_MASK;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyGetNextRouteIndex()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgmpProxyGetRouteEntryObjectByName       */
/*  Description     : The function returns the object value of */
/*                    multicast route dependig upon the given  */
/*                    index and object type                    */
/*  Input(s)        : u4GroupAddr - Group Address              */
/*                    u4SrcAddr - Source Address               */
/*                    u4SrcMask - Source Mask                  */
/*                    u4ObjectType - Object type               */
/*  Output(s)       : pu8ObjectValue - Object value            */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
/*as MRI use this func, it is U4 specific in stead of VX */
INT4
IgmpProxyGetRouteEntryObjectByName (UINT4 u4GroupAddr,
                                    UINT4 u4SrcAddr,
                                    UINT4 u4SrcMask, UINT4 u4ObjectType,
                                    tSNMP_COUNTER64_TYPE * pu8ObjectValue)
{

    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    UINT4               u4IfIndex = 0;
    UINT4               u4CurrentSysTime = 0;
    UINT4               u4RemainingTime = 0;
#ifdef FS_NPAPI
    UINT4               u4Count = 0;
#endif

    UNUSED_PARAM (u4SrcMask);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyGetRouteEntryObjectByName()\r \n");
    pu8ObjectValue->msn = 0;
    pu8ObjectValue->lsn = 0;

    IgmpLock ();
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4GroupAddr);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4SrcAddr);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast forwarding entry not found for"
                       " source %x group %x \r\n", u4SrcAddr, u4GroupAddr);
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    switch (u4ObjectType)
    {

        case IGP_IIFACE:

            if (IGMP_IP_GET_IFINDEX_FROM_PORT (pIgmpPrxyFwdEntry->u4IfIndex,
                                               &u4IfIndex) == NETIPV4_FAILURE)

            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, "Failed to get "
                          "CFA Index number from to IP Port number\r\n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }

            pu8ObjectValue->lsn = u4IfIndex;
            break;

        case IGP_UPTIME:

            if (OsixGetSysTime (&u4CurrentSysTime) != OSIX_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting current system time \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }

            pu8ObjectValue->lsn =
                (u4CurrentSysTime - pIgmpPrxyFwdEntry->u4RouteUpTime);
            break;

        case IGP_EXPIRY_TIME:

            if (TmrGetRemainingTime (gIgmpProxyInfo.ProxyTmrListId,
                                     &(pIgmpPrxyFwdEntry->EntryTimer.TimerBlk.
                                       TimerNode),
                                     &u4RemainingTime) == TMR_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the remaining time for "
                          "forward entry Timer\r\n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }

            pu8ObjectValue->lsn = u4RemainingTime;
            break;

        case IGP_FWD_PACKETS:

#ifdef FS_NPAPI
            if (IgpNpGetMRoutePktCount (pIgmpPrxyFwdEntry, &u4Count)
                != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the route packet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
            pu8ObjectValue->lsn = u4Count;
#else
            pu8ObjectValue->lsn = 0;
#endif
            break;

        case IGP_DIFF_IIFACE:

#ifdef FS_NPAPI
            if (IgpNpGetMRouteDifferentInIfPktCount
                (pIgmpPrxyFwdEntry, &u4Count) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the DifferentInIfPktCount \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
            pu8ObjectValue->lsn = u4Count;
#else
            /* Get from NPAPI */
            pu8ObjectValue->lsn = 0;
#endif
            break;

        case IGP_FWD_OCTETS:

#ifdef FS_NPAPI
            if (IgpNpGetMRouteOctetCount (pIgmpPrxyFwdEntry, &u4Count)
                != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the route Octet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
            pu8ObjectValue->lsn = u4Count;
#else
            pu8ObjectValue->lsn = 0;
#endif
            break;

        case IGP_MCAST_PROTOCOL:

            pu8ObjectValue->lsn = IGMP_IANA_MPROTOCOL_ID;
            break;

        case IGP_RT_PROTOCOL:
            /* No Protocol ID for IGMP Proxy
               So used Other(1) option
             */
            pu8ObjectValue->lsn = IGP_IANA_RT_PROTOCOL_ID;
            break;

        case IGP_ROUTE_TYPE:
            /* Always Muticast */
            pu8ObjectValue->lsn = IGP_ROUTE_TYPE_MULTICAST;
            break;

        case IGP_FWD_HCOCTETS:

#ifdef FS_NPAPI
            if (IgpNpGetMRouteHCOctetCount (pIgmpPrxyFwdEntry, pu8ObjectValue)
                != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the route HCOctet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
#else
            pu8ObjectValue->lsn = 0;
            pu8ObjectValue->msn = 0;
#endif
            break;

        default:
            IgmpUnLock ();
            return IGP_FAILURE;

    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyGetRouteEntryObjectByName()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgmpProxyValidateNextHopTblIndex         */
/*  Description     : Finction validate the next-hop index     */
/*  Input(s)        : pIgpNextHop - Next-Hop Index structure   */
/*  Output(s)       :                                          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS or IGP_FAILURE               */
/***************************************************************/
INT4
IgmpProxyValidateNextHopTblIndex (tIgpNextHopTblIndex * pIgpNextHop)
{

    UINT4               u4Port = IGMP_ZERO;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyValidateNextHopTblIndex()\r \n");
    IgmpLock ();
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));
/* this structure UINT4 specific */
    /* Support for IP muticast MIB  refer RFC#2932 */
    if ((pIgpNextHop->u4NextHopAddr != pIgpNextHop->u4GrpAddr) ||
        (pIgpNextHop->u4SrcMask != IGP_DEFAULT_SOURCE_MASK))
    {
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(pIgpNextHop->u4GrpAddr));
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(pIgpNextHop->u4SrcAddr));

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry != NULL)
    {
        if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) pIgpNextHop->i4OIfIndex,
                                           &u4Port) == NETIPV4_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Failed to get IP Port number from to CFA index\r\n");
            IgmpUnLock ();
            return IGP_FAILURE;
        }

        TMO_SLL_Scan (&(pIgmpPrxyFwdEntry->OifList),
                      pIgpOifNode, tIgmpPrxyOif *)
        {
            if (pIgpOifNode->u4IfIndex == u4Port)
            {
                IgmpUnLock ();
                return IGP_SUCCESS;
            }
            else if (pIgpOifNode->u4IfIndex > u4Port)
            {
                break;
            }
        }
    }

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "Next hop table validation failed\r\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyValidateNextHopTblIndex()\r \n");
    IgmpUnLock ();
    return IGP_FAILURE;
}

/***************************************************************/
/*  Function Name   : IgmpProxyGetNextNextHopIndex             */
/*  Description     : Function gives the next next-hop index of*/
/*                    the given index                          */
/*  Input(s)        : pIgpNextHop - Next-Hop Index structure   */
/*  Output(s)       : pIgpNextNextHop - Next next-Hop index    */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
INT4
IgmpProxyGetNextNextHopIndex (tIgpNextHopTblIndex * pIgpNextHop,
                              tIgpNextHopTblIndex * pIgpNextNextHop)
{
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    INT4                i4IfIndex = IGMP_ZERO;
    INT4                i4TempIndex = IGP_MAX_INT4;
    INT1                i1Found = FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyGetNextNextHopIndex()\r \n");
    IgmpLock ();
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(pIgpNextHop->u4GrpAddr));
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(pIgpNextHop->u4SrcAddr));

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry != NULL)
    {
        TMO_SLL_Scan (&(pIgmpPrxyFwdEntry->OifList), pIgpOifNode,
                      tIgmpPrxyOif *)
        {
            if (IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpOifNode->u4IfIndex,
                                               (UINT4 *) &i4IfIndex)
                == NETIPV4_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failed to get Port number from to CFA index\r\n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }

            if ((pIgpNextHop->i4OIfIndex < i4IfIndex) &&
                (i4IfIndex < i4TempIndex))
            {
                /* At least got one greater index */
                i1Found = TRUE;
                i4TempIndex = i4IfIndex;
            }
        }
    }

    if (i1Found == FALSE)
    {
        i4TempIndex = IGP_MAX_INT4;
        /*Get the next pIgmpPrxyFwdEntry */
        pIgmpPrxyFwdEntry =
            (tIgmpPrxyFwdEntry *) RBTreeGetNext (gIgmpProxyInfo.ForwardEntry,
                                                 (tRBElem *) & IgpForwardEntry,
                                                 IgmpProxyUtilRbCompare);

        if (pIgmpPrxyFwdEntry == NULL)
        {
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, "Multicast "
                           "forwarding entry not found for source %d and "
                           "group %x\r\n", pIgpNextHop->u4SrcAddr,
                           pIgpNextHop->u4GrpAddr);
            IgmpUnLock ();
            return IGP_FAILURE;
        }

        TMO_SLL_Scan (&(pIgmpPrxyFwdEntry->OifList), pIgpOifNode,
                      tIgmpPrxyOif *)
        {
            if (IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpOifNode->u4IfIndex,
                                               (UINT4 *) &i4IfIndex)
                == NETIPV4_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failed to get Port number from to CFA index\r\n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }

            if (i4IfIndex < i4TempIndex)
            {
                i4TempIndex = i4IfIndex;
            }
        }
    }

    IGMP_IP_COPY_FROM_IPVX (&(pIgpNextNextHop->u4GrpAddr),
                            pIgmpPrxyFwdEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&(pIgpNextNextHop->u4SrcAddr),
                            pIgmpPrxyFwdEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    pIgpNextNextHop->u4SrcMask = IGP_DEFAULT_SOURCE_MASK;
    pIgpNextNextHop->i4OIfIndex = i4TempIndex;
    IGMP_IP_COPY_FROM_IPVX (&(pIgpNextNextHop->u4NextHopAddr),
                            pIgmpPrxyFwdEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);

    IgmpUnLock ();
    return IGP_SUCCESS;

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "No entries are found in the NextHopTable\r\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyGetNextNextHopIndex()\r \n");
    IgmpUnLock ();
    return IGP_FAILURE;
}

/***************************************************************/
/*  Function Name   : IgmpProxyGetNextHopEntryObjectByName     */
/*  Description     : The function returns the object value of */
/*                    multicast next-hop dependig upon the     */
/*                    given index and object type              */
/*  Input(s)        : pIgpNextHop - Next-Hop Index structure   */
/*                    u4ObjectType - Object type               */
/*  Output(s)       : pu4ObjectValue - Object value            */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
INT4
IgmpProxyGetNextHopEntryObjectByName (tIgpNextHopTblIndex * pIgpNextHop,
                                      UINT4 u4ObjectType, UINT4 *pu4ObjectValue)
{
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    UINT4               u4Port = IGMP_ZERO;
    UINT4               u4RemainingTime = 0;
    UINT4               u4CurrentSysTime = 0;

    *pu4ObjectValue = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyGetNextHopEntryObjectByName()\r \n");
    IgmpLock ();
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(pIgpNextHop->u4GrpAddr));
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(pIgpNextHop->u4SrcAddr));

    pIgmpPrxyFwdEntry =
        (tIgmpPrxyFwdEntry *) RBTreeGet (gIgmpProxyInfo.ForwardEntry,
                                         (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry != NULL)
    {
        if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) pIgpNextHop->i4OIfIndex,
                                           &u4Port) == NETIPV4_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Failed to get IP Port number from to CFA index\r\n");
            IgmpUnLock ();
            return IGP_FAILURE;
        }

        TMO_SLL_Scan (&(pIgmpPrxyFwdEntry->OifList), pIgpOifNode,
                      tIgmpPrxyOif *)
        {
            if (pIgpOifNode->u4IfIndex == u4Port)
            {
                break;
            }

            if (pIgpOifNode->u4IfIndex > u4Port)
            {
                IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                               "Multicast entry not found for source %x and Group %x "
                               "and OIF index %d\r\n", pIgpNextHop->u4SrcAddr,
                               pIgpNextHop->u4GrpAddr, pIgpNextHop->i4OIfIndex);
                IgmpUnLock ();
                return IGP_FAILURE;
            }
        }
    }

    if (pIgpOifNode == NULL)
    {
        IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast entry not found for source %x and Group %x "
                       "and OIF index %d\r\n", pIgpNextHop->u4SrcAddr,
                       pIgpNextHop->u4GrpAddr, pIgpNextHop->i4OIfIndex);
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    switch (u4ObjectType)
    {

        case IGP_STATE:

            *pu4ObjectValue = IGP_OIF_FORWARDING;

            break;
        case IGP_UPTIME:

            if (OsixGetSysTime (&u4CurrentSysTime) != OSIX_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting current system time \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }

            *pu4ObjectValue = (u4CurrentSysTime - pIgpOifNode->u4UpTime);

            break;
        case IGP_EXPIRY_TIME:

            if (TmrGetRemainingTime (gIgmpProxyInfo.ProxyTmrListId,
                                     &(pIgmpPrxyFwdEntry->EntryTimer.TimerBlk.
                                       TimerNode),
                                     &u4RemainingTime) == TMR_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the remaining time for "
                          "forward entry Timer\r\n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }

            *pu4ObjectValue = u4RemainingTime;

            break;
        case IGP_FWD_PACKETS:
            /* Get from NPAPI */

#ifdef FS_NPAPI
            if (IgpNpGetMNextHopPktCount (pIgmpPrxyFwdEntry,
                                          (INT4) pIgpOifNode->u4IfIndex,
                                          pu4ObjectValue) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the route Octet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
#else
            *pu4ObjectValue = 0;
#endif

            break;
        case IGP_MCAST_PROTOCOL:

            *pu4ObjectValue = IGMP_IANA_MPROTOCOL_ID;

            break;
        default:

            *pu4ObjectValue = 0;
            IgmpUnLock ();
            return IGP_FAILURE;

    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyGetNextHopEntryObjectByName()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgmpProxyValidateInterfaceTblIndex       */
/*  Description     : Function validates the given interface   */
/*                    index                                    */
/*  Input(s)        : i4Ifindex - Interface Index              */
/*  Output(s)       :                                          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
INT4
IgmpProxyValidateInterfaceTblIndex (INT4 i4Ifindex)
{

    UINT4               u4Port = IGMP_ZERO;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyValidateInterfaceTblIndex()\r \n");
    IgmpLock ();
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4Ifindex, &u4Port)
        == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Validation of FsIgmpProxyRtrIfaceTable for "
                       "interface index%d failed\r\n", i4Ifindex);
        IgmpUnLock ();
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyValidateInterfaceTblIndex()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgmpProxyGetNextInterfaceIndex           */
/*  Description     : Function gives the next Interface index  */
/*                    of the given index                       */
/*  Input(s)        : i4Ifindex -Interface index               */
/*  Output(s)       : pi4NextIfIndex -Interface index          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS or IGP_FAILURE               */
/***************************************************************/
INT4
IgmpProxyGetNextInterfaceIndex (INT4 i4Ifindex, INT4 *pi4NextIfIndex)
{
    UINT4               u4HashIndex = IGMP_ZERO;
    UINT4               u4IgpIndex = IGMP_ZERO;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;

    *pi4NextIfIndex = IGP_MAX_INT4;

    IgmpLock ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyGetNextInterfaceIndex()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    TMO_HASH_Scan_Table (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex,
                              pIgmpPrxyUpIface, tIgmpPrxyUpIface *)
        {
            IGMP_IP_GET_IFINDEX_FROM_PORT ((pIgmpPrxyUpIface->u4IfIndex),
                                           &u4IgpIndex);
            if (((INT4) u4IgpIndex > i4Ifindex) &&
                ((INT4) u4IgpIndex) < *pi4NextIfIndex)
            {
                *pi4NextIfIndex = (INT4) u4IgpIndex;
            }
        }
    }

    if (*pi4NextIfIndex == IGP_MAX_INT4)
    {
        *pi4NextIfIndex = 0;
        IgmpUnLock ();
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyGetNextInterfaceIndex()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgmpProxyGetInterfaceEntryObjetByName    */
/*  Description     : The function returns the object value of */
/*                    multicast interface dependig upon the    */
/*                    given index and object type              */
/*  Input(s)        : i4Ifindex - Interface index              */
/*                    u4ObjectType - Object type               */
/*  Output(s)       : pu8ObjectVal - Object value              */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
INT4
IgmpProxyGetInterfaceEntryObjetByName (INT4 i4Ifindex, UINT4 u4ObjectType,
                                       tSNMP_COUNTER64_TYPE * pu8ObjectVal)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
#ifdef FS_NPAPI
    UINT4               u4Count = 0;
#endif

    pu8ObjectVal->msn = 0;
    pu8ObjectVal->lsn = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyGetInterfaceEntryObjetByName()\r \n");
    IgmpLock ();

    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4Ifindex, &u4Port)
        == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, "Upstream Iface "
                       "entry not found for interface index %d\r\n", i4Ifindex);
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    switch (u4ObjectType)
    {

        case IGP_IFACE_TTL:

            pu8ObjectVal->lsn = pIgmpPrxyUpIface->i4IfTtlTreshold;
            break;

        case IGP_IFACE_RLIMIT:

            pu8ObjectVal->lsn = pIgmpPrxyUpIface->i4IfRateLimit;
            break;

        case IGP_IFACE_PROTO:

            pu8ObjectVal->lsn = IGMP_IANA_MPROTOCOL_ID;
            break;

        case IGP_IFACE_IN_MOCTETS:

            /* Get From NP */
#ifdef FS_NPAPI
            if (IgpNpGetMIfInOctetCount ((INT4) pIgmpPrxyUpIface->u4IfIndex,
                                         &u4Count) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the Iface In Octet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
            pu8ObjectVal->lsn = u4Count;
#else
            pu8ObjectVal->lsn = 0;
#endif
            break;

        case IGP_IFACE_OUT_MOCTETS:

            /* Get From NP */
#ifdef FS_NPAPI
            if (IgpNpGetMIfOutOctetCount ((INT4) pIgmpPrxyUpIface->u4IfIndex,
                                          &u4Count) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the Iface Out Octet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
            pu8ObjectVal->lsn = u4Count;
#else
            pu8ObjectVal->lsn = 0;
#endif
            break;

        case IGP_IFACE_HC_IN_MOCTETS:

            /* Get From NP */
#ifdef FS_NPAPI
            if (IgpNpGetMIfHCInOctetCount ((INT4) pIgmpPrxyUpIface->u4IfIndex,
                                           pu8ObjectVal) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the Iface HcIn Octet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
#else
            pu8ObjectVal->lsn = 0;
            pu8ObjectVal->msn = 0;
#endif
            break;

        case IGP_IFACE_HC_OUT_MOCTETS:

            /* Get From NP */
#ifdef FS_NPAPI
            if (IgpNpGetMIfHCOutOctetCount ((INT4) pIgmpPrxyUpIface->u4IfIndex,
                                            pu8ObjectVal) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failure in getting the Iface HCOut Octet count \n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
#else
            pu8ObjectVal->lsn = 0;
            pu8ObjectVal->msn = 0;
#endif
            break;

        default:
            /* Control should not reach here */
            IgmpUnLock ();
            return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyGetInterfaceEntryObjetByName()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgmpProxySetInterfaceEntryObjectByName   */
/*  Description     : The function sets the value for object   */
/*                    multicast interface dependig upon the    */
/*                    given index and object type              */
/*  Input(s)        : i4Ifindex - Interface index              */
/*                    u4ObjectType - Object type               */
/*                    u4ObjectVal - Object value               */
/*  Output(s)       :                                          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         :  IGP_SUCCESS or IGP_FAILURE              */
/***************************************************************/
INT4
IgmpProxySetInterfaceEntryObjectByName (INT4 i4Ifindex, UINT4 u4ObjectType,
                                        INT4 i4ObjectValue)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxySetInterfaceEntryObjectByName()\r \n");
    IgmpLock ();
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4Ifindex, &u4Port)
        == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, "Upstream Iface "
                       "entry not found for interface index %d\r\n", i4Ifindex);
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    switch (u4ObjectType)
    {

        case IGP_IFACE_TTL:

            /* Call NPAPI. If NPAPI returns success store the value */
#ifdef FS_NPAPI
            if (IgpNpSetMIfaceTtlTreshold ((INT4) pIgmpPrxyUpIface->u4IfIndex,
                                           i4ObjectValue) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, "NP Failed Set "
                          "the muticast interface Ttl Treshold \r\n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
#endif

            pIgmpPrxyUpIface->i4IfTtlTreshold = i4ObjectValue;
            break;

        case IGP_IFACE_RLIMIT:

#ifdef FS_NPAPI
            if (IgpNpSetMIfaceRateLimit ((INT4) pIgmpPrxyUpIface->u4IfIndex,
                                         i4ObjectValue) != IGP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, "NP Failed Set "
                          "the muticast Rate Limit \r\n");
                IgmpUnLock ();
                return IGP_FAILURE;
            }
#endif
            /* Call NPAPI. If NPAPI returns success store the value */
            pIgmpPrxyUpIface->i4IfRateLimit = i4ObjectValue;
            break;

        default:
            /* Control should not reach here */
            IgmpUnLock ();
            return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxySetInterfaceEntryObjectByName()\r \n");
    IgmpUnLock ();
    return IGP_SUCCESS;
}

/*******************************************************************/
/*  Function Name   : IgmpProxyTestInterfaceEntryObjectValueByName */
/*  Description     : Functions validate the object value depending*/
/*                    upon the given index and ibject type         */
/*  Input(s)        : i4Ifindex - Interface index                  */
/*                    u4ObjectType - Object type                   */
/*                    i4ObjectValue - Object type                  */
/*  Output(s)       : pu4ErrorCode - Error value                   */
/*  <OPTIONAL Fields>:--                                           */
/*  Global Variables Referred :   --                               */
/*  Global variables Modified :   --                               */
/*  Exceptions or Operating System Error Handling :--              */
/*  Use of Recursion : NO                                          */
/*  Returns         :  None                                        */
/*******************************************************************/
INT4
IgmpProxyTestInterfaceEntryObjectValueByName (UINT4 *pu4ErrorCode,
                                              INT4 i4Ifindex,
                                              UINT4 u4ObjectType,
                                              INT4 i4ObjectValue)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyTestInterfaceEntryObjectValueByName()\r \n");
    IgmpLock ();

    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        IgmpUnLock ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return IGP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4Ifindex, &u4Port)
        == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        IgmpUnLock ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IGP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME, "Upstream Iface "
                       "entry not found for interface index %d\r\n", i4Ifindex);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        IgmpUnLock ();
        return IGP_FAILURE;
    }

    switch (u4ObjectType)
    {

        case IGP_IFACE_TTL:

            if ((i4ObjectValue >= IGP_MIN_INTERFACE_TTL) &&
                (i4ObjectValue <= IGP_MAX_INTERFACE_TTL))
            {
                IgmpUnLock ();
                return IGP_SUCCESS;
            }
            break;

        case IGP_IFACE_RLIMIT:

            if ((i4ObjectValue >= IGP_MIN_RATELIMIT_IN_KBPS) &&
                (i4ObjectValue <= IGP_MAX_RATELIMIT_IN_KBPS))
            {
                IgmpUnLock ();
                return IGP_SUCCESS;
            }
            break;

        default:
            /* Control should not reach here */
            IgmpUnLock ();
            return IGP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyTestInterfaceEntryObjectValueByName()\r \n");
    IgmpUnLock ();
    return IGP_FAILURE;
}
