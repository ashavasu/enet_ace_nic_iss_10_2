/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igputil.c,v 1.23 2015/02/13 11:13:54 siva Exp $
 *
 ******************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : igputil.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy utilities                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains utilities                   */
/*                            for IGMP Proxy module                          */
/*---------------------------------------------------------------------------*/

#include "igpinc.h"
#include "fsigpwr.h"
#include "snmputil.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE;
#endif

extern UINT1       *EoidGetEnterpriseOid (void);

/*****************************************************************************/
/* Function Name      : IgpUtilRBTreeGroupEntryCmp                           */
/*                                                                           */
/* Description        : This routine is used in Group Table for comparing    */
/*                      two keys used in RBTree functionality.               */
/*                                                                           */
/* Input(s)           : GroupEntryNode - Key 1                               */
/*                      GroupEntryIn   - Key 2                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
IgpUtilRBTreeGroupEntryCmp (tRBElem * pGroupEntryNode, tRBElem * pGroupEntryIn)
{
    /* Compare the Multicast group address */
    if (IGMP_IPVX_ADDR_COMPARE
        ((((tIgmpPrxyGrpInfo *) pGroupEntryNode)->u4GrpIpAddr),
         (((tIgmpPrxyGrpInfo *) pGroupEntryIn)->u4GrpIpAddr)) < IGMP_ZERO)
    {
        return -1;
    }
    else if (IGMP_IPVX_ADDR_COMPARE
             ((((tIgmpPrxyGrpInfo *) pGroupEntryNode)->u4GrpIpAddr),
              (((tIgmpPrxyGrpInfo *) pGroupEntryIn)->u4GrpIpAddr)) > IGMP_ZERO)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : IgpUtilRBTreeFwdEntryCmp                             */
/*                                                                           */
/* Description        : This routine is used in Mcast Forwarding Table       */
/*                      for comparing two keys used in RBTree functionality. */
/*                                                                           */
/* Input(s)           : ForwadrEntryNode -  Key 1                            */
/*                      ForwadrEntryIn - Key 2                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
IgpUtilRBTreeFwdEntryCmp (tRBElem * pForwardEntryNode,
                          tRBElem * pForwardEntryIn)
{
    /* Compare the source IP address of the forwarding entry */
    if (IGMP_IPVX_ADDR_COMPARE
        ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4SrcIpAddr),
         (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4SrcIpAddr)) < IGMP_ZERO)
    {
        return -1;
    }
    else if (IGMP_IPVX_ADDR_COMPARE
             ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4SrcIpAddr),
              (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4SrcIpAddr)) >
             IGMP_ZERO)
    {
        return 1;
    }

    /* Compare the Group IP address of the forwarding entry */
    if (IGMP_IPVX_ADDR_COMPARE
        ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4GrpIpAddr),
         (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4GrpIpAddr)) < IGMP_ZERO)
    {
        return -1;
    }
    else if (IGMP_IPVX_ADDR_COMPARE
             ((((tIgmpPrxyFwdEntry *) pForwardEntryNode)->u4GrpIpAddr),
              (((tIgmpPrxyFwdEntry *) pForwardEntryIn)->u4GrpIpAddr)) >
             IGMP_ZERO)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : IgpUtilRBTreeGroupEntryFree                          */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for Group table.                           */
/*                                                                           */
/* Input(s)           : GroupEntryNode - Pointer to Group table.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
IgpUtilRBTreeGroupEntryFree (tRBElem * pGroupEntryNode)
{
    if (pGroupEntryNode != NULL)
    {
        IGP_RELEASE_MEM_BLOCK (IGP_GRP_MEMPOOL_ID, pGroupEntryNode);
    }
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUtilRBTreeFwdEntryFree                            */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for forward table.                         */
/*                                                                           */
/* Input(s)           : ForwadrEntryNode - Pointer to IP forward Node        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
IgpUtilRBTreeFwdEntryFree (tRBElem * pForwadrEntryNode)
{
    if (pForwadrEntryNode != NULL)
    {
        IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pForwadrEntryNode);
    }
    return IGMP_SUCCESS;
}

/****************************************************************************/
/* Function Name             : IgpUtilGetVlanIdFromIfIndex                  */
/*                                                                          */
/* Description               : This function returns the VLAN ID for the    */
/*                             given IP interface index                     */
/*                                                                          */
/* Input (s)                 : u4Port   - IP Interface index                */
/*                                                                          */
/* Output (s)                : pu2VlanId - VLAN ID of the IP interface      */
/*                                                                          */
/* Returns                   : IGMP_SUCCESS/IGMP_FAILURE                    */
/****************************************************************************/
INT4
IgpUtilGetVlanIdFromIfIndex (UINT4 u4Port, UINT2 *pu2VlanId)
{
    tIgmpIface         *pIfaceNode = NULL;

    pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);

    if (pIfaceNode == NULL)
    {
        return IGMP_FAILURE;
    }

    *pu2VlanId = pIfaceNode->u2VlanId;
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUtilAddToSourceList                               */
/*                                                                           */
/* Description        : This function will update the source information in  */
/*                      the global array; It returns Failure if the array is */
/*                      full. This function will return the index of the     */
/*                      source if already present else retrun the first      */
/*                      empty slot in the source array                       */
/*                                                                           */
/* Input(s)           : u4SrcAddr - Source to be added in to global array    */
/*                                                                           */
/* Global(s)          : gaIgpSrcInfo - Global array of source information    */
/*                                                                           */
/* Output(s)          : pu2SrcIndex - index on global array, where source is */
/*                                    added                                  */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUtilAddToSourceList (tIgmpIPvXAddr u4SrcAddr, UINT2 *pu2SrcIndex)
{
    UINT2               u2Index;
    *pu2SrcIndex = IGP_MAX_MCAST_SRCS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilAddToSourceList()\r \n");
    for (u2Index = 0; u2Index < IGP_MAX_MCAST_SRCS; u2Index++)
    {
        if (IGMP_IPVX_ADDR_COMPARE (gaIgpSrcInfo[u2Index].u4SrcIpAddr,
                                    u4SrcAddr) == IGMP_ZERO)
        {
            *pu2SrcIndex = u2Index;
            break;
        }
        else if ((*pu2SrcIndex == IGP_MAX_MCAST_SRCS) &&
                 (IGMP_IPVX_ADDR_COMPARE (gaIgpSrcInfo[u2Index].u4SrcIpAddr,
                                          gIgpIPvXZero) == IGMP_ZERO))
        {
            *pu2SrcIndex = u2Index;
        }
    }
    if (*pu2SrcIndex >= IGP_MAX_MCAST_SRCS)
    {
        return IGMP_FAILURE;
    }
    IGMP_IPVX_ADDR_COPY (&(gaIgpSrcInfo[*pu2SrcIndex].u4SrcIpAddr), &u4SrcAddr);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilAddToSourceList()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUtilUpdateSourceInfo                              */
/*                                                                           */
/* Description        : This function will update the source information in  */
/*                      the global array and update the bitmaps of the given */
/*                      group entry                                          */
/*                                                                           */
/* Input(s)           : pIgmpGroupEntry - Pointer to the group entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUtilUpdateSourceInfo (tIgmpGroup * pIgmpGroupEntry)
{
    tIgmpSource        *pIgmpSource = NULL;
    INT4                i4Status = IGMP_SUCCESS;
    UINT2               u2SrcIndex = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilUpdateSourceInfo()\r \n");
    MEMSET (pIgmpGroupEntry->InclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
    MEMSET (pIgmpGroupEntry->ExclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);

    TMO_SLL_Scan (&(pIgmpGroupEntry->srcList), pIgmpSource, tIgmpSource *)
    {
        IGMP_IPVX_ADDR_CLEAR (&gIPvXZero);
        if (IGMP_IPVX_ADDR_COMPARE
            (pIgmpSource->u4SrcAddress, gIPvXZero) == IGMP_ZERO)
        {
            /* Src Address 0.0.0.0 is added for EXCL NONE report.
             * If 0.0.0.0 address is added to MRP, other sources
             * need not be added, since EXCL NONE is superset */
            MEMSET (pIgmpGroupEntry->ExclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
            break;
        }

        if (IgpUtilAddToSourceList (pIgmpSource->u4SrcAddress,
                                    &u2SrcIndex) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Number of sources supported by the system reached"
                      " the maximum\r\n");
            /* Do not return failure because, there may be other sources, 
             * which are already present in sourceList, and can be processed */
            i4Status = IGMP_FAILURE;
        }

        /* If u1FwdState = IGMP_TRUE, timer is running and hence it is in 
         * include state. If u1FwdState = IGMP_FALSE, timer is 
         * not running (Hence filter mode is exclude) and source should be
         * blocked */

        if (pIgmpSource->u1FwdState == IGMP_FALSE)
        {
            IGP_ADD_TO_SRC_BMP (u2SrcIndex, pIgmpGroupEntry->ExclSrcBmp);
        }
        else
        {
            IGP_ADD_TO_SRC_BMP (u2SrcIndex, pIgmpGroupEntry->InclSrcBmp);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilUpdateSourceInfo()\r \n");
    return i4Status;
}

/*****************************************************************************/
/* Function Name      : IgpUtilCreateGrpRecord4Xsisting                       */
/*                                                                           */
/* Description        : This function is called for any single group entry   */
/*                      in the consolidated Database. This will create the   */
/*                      group record, based on the group entry               */
/*                                                                           */
/* Input(s)           : pIgmpPrxyGrpInfo - Pointer to the updated group entry*/
/*                      IgmPxySrcBmp - Sorce bitmap. If pIgmpPrxyGrpInfo is  */
/*                      group record will be created based on IgmPxySrcBmp   */
/*                                                                           */
/* Output(s)          : pu2PktLength - increase in length of packet          */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUtilCreateGrpRecord4Xsisting (tIgmpPrxyGrpInfo * pIgmpPrxyGrpInfo,
                                 UINT2 *pu2PktLength,
                                 tIgmpIPvXAddr u4GrpAddress,
                                 tIgmPxySrcBmp IgmPxySrcBmp)
{
    UINT2               u2Offset = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilCreateGrpRecord4Xsisting()\r \n");
    if (pIgmpPrxyGrpInfo == NULL)
    {
        IgpUtilFillGroupRecord (IGMP_GRP_MODE_IS_INCLUDE, u4GrpAddress,
                                IgmPxySrcBmp, &u2Offset);
    }
    else if (pIgmpPrxyGrpInfo->u1FilterMode == IGMP_FMODE_INCLUDE)
    {
        IgpUtilFillGroupRecord (IGMP_GRP_MODE_IS_INCLUDE,
                                pIgmpPrxyGrpInfo->u4GrpIpAddr,
                                pIgmpPrxyGrpInfo->InclSrcBmp, &u2Offset);
    }
    else
    {
        IgpUtilFillGroupRecord (IGMP_GRP_MODE_IS_EXCLUDE,
                                pIgmpPrxyGrpInfo->u4GrpIpAddr,
                                pIgmpPrxyGrpInfo->ExclSrcBmp, &u2Offset);
    }
    gIgmpProxyInfo.u1NoOfGrpRecords++;
    *pu2PktLength = u2Offset;
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilCreateGrpRecord4Xsisting()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUtilCreateGrpRecord4Change                        */
/*                                                                           */
/* Description        : This function is called for any single group entry   */
/*                      in the consolidated Database. This will create the   */
/*                      group record, based on the changes happened to the   */
/*                      group entry                                          */
/*                                                                           */
/* Input(s)           : u1OldFilterMode - Old filtermode of the group entry  */
/*                      OldInclSrcBmp - Old include bitmap of the group entry*/
/*                      OldExclSrcBmp - Old exclude bitmap of the group entry*/
/*                      pIgmpPrxyGrpInfo - Pointer to the updated group entry*/
/*                      u4GrpIpAddr - group address, used only while         */
/*                                    processing Leave                       */
/*                                                                           */
/* Output(s)          : pu2PktLength - increase in length of packet          */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpUtilCreateGrpRecord4Change (UINT1 u1OldFilterMode,
                               tIgmPxySrcBmp OldInclSrcBmp,
                               tIgmPxySrcBmp OldExclSrcBmp,
                               tIgmpPrxyGrpInfo * pIgmpPrxyGrpInfo,
                               tIgmpIPvXAddr u4GrpIpAddr, UINT2 *pu2PktLength)
{
    tIgmPxySrcBmp       IgmPrxyXorBmp;
    tIgmPxySrcBmp       IgmPrxyInclAllowBmp;
    tIgmPxySrcBmp       IgmPrxyExclBlockBmp;
    UINT2               u2Offset = 0;
    UINT1               u1RecordType = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilCreateGrpRecord4Change()\r \n");
    MEMSET (IgmPrxyXorBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
    MEMSET (IgmPrxyInclAllowBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
    MEMSET (IgmPrxyExclBlockBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);

    if (pIgmpPrxyGrpInfo == NULL)
    {
        /* if pIgmpPrxyGrpInfo == NULL, that means, the group has been 
         * deleted from the consolidated database. So send TO_INCL () */
        IgpUtilFillGroupRecord (IGMP_GRP_CHANGE_TO_INCLUDE_MODE, u4GrpIpAddr,
                                IgmPrxyInclAllowBmp, &u2Offset);
        /* Total number of grouprecords increased by one */
        gIgmpProxyInfo.u1NoOfGrpRecords++;
        /* total packet length increased by the length of group record */
        *pu2PktLength = u2Offset;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &gIgmpProxyInfo.IgpInfoDbNode);
        IgmpRedSyncDynInfo ();
        return IGMP_SUCCESS;
    }

    if (pIgmpPrxyGrpInfo->u1FilterMode == u1OldFilterMode)
    {
        /* Filtermode has not changed */
        if (u1OldFilterMode == IGMP_FMODE_INCLUDE)
        {
            /* Change is from INCLUDE (A) to INCLUDE (B).
             * So change will be only in include bitmap*/
            MEMCPY (IgmPrxyXorBmp, OldInclSrcBmp, IGP_SRC_LIST_SIZE);
            MEMCPY (IgmPrxyExclBlockBmp, OldInclSrcBmp, IGP_SRC_LIST_SIZE);
            MEMCPY (IgmPrxyInclAllowBmp, pIgmpPrxyGrpInfo->InclSrcBmp,
                    IGP_SRC_LIST_SIZE);

            /* XOR between old bitmap and new bitmap, will highlight 
             * the difference (of sources) between new and old bitmaps */
            IGP_XOR_SRC_BMP (IgmPrxyXorBmp, pIgmpPrxyGrpInfo->InclSrcBmp);

            /* calculate ALLOW (B-A) */
            IGP_AND_SRC_BMP (IgmPrxyInclAllowBmp, IgmPrxyXorBmp);
            if (MEMCMP (&IgmPrxyInclAllowBmp,
                        &gIgpNullSrcBmp, IGP_SRC_LIST_SIZE) != 0)
            {
                u1RecordType += IGMP_GRP_ALLOW_NEW_SOURCES;
            }

            /* calculate BLOCK (A-B) */
            IGP_AND_SRC_BMP (IgmPrxyExclBlockBmp, IgmPrxyXorBmp);
            if (MEMCMP (&IgmPrxyExclBlockBmp,
                        &gIgpNullSrcBmp, IGP_SRC_LIST_SIZE) != 0)
            {
                u1RecordType += IGMP_GRP_BLOCK_OLD_SOURCES;
            }
        }
        else if (u1OldFilterMode == IGMP_FMODE_EXCLUDE)
        {
            /* change is from exclude mode to exclude mode.
             * So change will be only in exclude bitmap*/
            MEMCPY (IgmPrxyXorBmp, OldExclSrcBmp, IGP_SRC_LIST_SIZE);
            MEMCPY (IgmPrxyInclAllowBmp, OldExclSrcBmp, IGP_SRC_LIST_SIZE);
            MEMCPY (IgmPrxyExclBlockBmp, pIgmpPrxyGrpInfo->ExclSrcBmp,
                    IGP_SRC_LIST_SIZE);

            /* XOR between old bitmap and new bitmap, will highlight 
             * the difference (of sources) between new and old bitmaps */
            IGP_XOR_SRC_BMP (IgmPrxyXorBmp, pIgmpPrxyGrpInfo->ExclSrcBmp);

            /* calculate ALLOW (A-B) */
            IGP_AND_SRC_BMP (IgmPrxyInclAllowBmp, IgmPrxyXorBmp);
            if (MEMCMP (&IgmPrxyInclAllowBmp,
                        &gIgpNullSrcBmp, IGP_SRC_LIST_SIZE) != 0)
            {
                u1RecordType += IGMP_GRP_ALLOW_NEW_SOURCES;
            }

            /* calculate BLOCK (B-A) */
            IGP_AND_SRC_BMP (IgmPrxyExclBlockBmp, IgmPrxyXorBmp);
            if (MEMCMP (&IgmPrxyExclBlockBmp,
                        &gIgpNullSrcBmp, IGP_SRC_LIST_SIZE) != 0)
            {
                u1RecordType += IGMP_GRP_BLOCK_OLD_SOURCES;
            }
        }
        else
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Invalid filtermode in IgpUtilUpdateGroupRecord\r\n");
            return IGMP_FAILURE;
        }
    }
    else
    {
        if (u1OldFilterMode == IGMP_FMODE_INCLUDE)
        {
            /* Filter mode changed to EXCLUDE (B) 
             * So send a TO_EX (B)*/
            MEMCPY (IgmPrxyExclBlockBmp, pIgmpPrxyGrpInfo->ExclSrcBmp,
                    IGP_SRC_LIST_SIZE);
            u1RecordType = IGMP_GRP_CHANGE_TO_EXCLUDE_MODE;
        }
        else if (u1OldFilterMode == IGMP_FMODE_EXCLUDE)
        {
            /* Filter mode changed to INCLUDE (B)
             * So send a TO_IN (B)*/
            MEMCPY (IgmPrxyInclAllowBmp, pIgmpPrxyGrpInfo->InclSrcBmp,
                    IGP_SRC_LIST_SIZE);
            u1RecordType = IGMP_GRP_CHANGE_TO_INCLUDE_MODE;
        }
        else
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Invalid filtermode in IgpUtilUpdateGroupRecord\r\n");
            return IGMP_FAILURE;

        }
    }

    if ((u1RecordType == IGMP_GRP_ALLOW_NEW_SOURCES) ||
        (u1RecordType == IGMP_GRP_CHANGE_TO_INCLUDE_MODE))
    {
        IgpUtilFillGroupRecord (u1RecordType, pIgmpPrxyGrpInfo->u4GrpIpAddr,
                                IgmPrxyInclAllowBmp, &u2Offset);
        /* Total number of grouprecords increased by one */
        gIgmpProxyInfo.u1NoOfGrpRecords++;
    }
    else if ((u1RecordType == IGMP_GRP_BLOCK_OLD_SOURCES) ||
             (u1RecordType == IGMP_GRP_CHANGE_TO_EXCLUDE_MODE))
    {
        IgpUtilFillGroupRecord (u1RecordType, pIgmpPrxyGrpInfo->u4GrpIpAddr,
                                IgmPrxyExclBlockBmp, &u2Offset);
        /* Total number of grouprecords increased by one */
        gIgmpProxyInfo.u1NoOfGrpRecords++;
    }
    else if (u1RecordType != IGMP_ZERO)
    {
        /* Need to send both ALLOW and BLOCK */
        u1RecordType = IGMP_GRP_ALLOW_NEW_SOURCES;
        IgpUtilFillGroupRecord (u1RecordType, pIgmpPrxyGrpInfo->u4GrpIpAddr,
                                IgmPrxyInclAllowBmp, &u2Offset);

        u1RecordType = IGMP_GRP_BLOCK_OLD_SOURCES;
        IgpUtilFillGroupRecord (u1RecordType, pIgmpPrxyGrpInfo->u4GrpIpAddr,
                                IgmPrxyExclBlockBmp, &u2Offset);
        /* Total number of grouprecords increased by two */
        gIgmpProxyInfo.u1NoOfGrpRecords += IGMP_TWO;
    }
    *pu2PktLength = u2Offset;
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &gIgmpProxyInfo.IgpInfoDbNode);
    IgmpRedSyncDynInfo ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilCreateGrpRecord4Change()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUtilFillGroupRecord                               */
/*                                                                           */
/* Description        : This function is called for creation of a            */
/*                      group record, based on the given record type and     */
/*                      bitmap                                               */
/*                                                                           */
/* Input(s)           : u1RecordType - Record type of group record           */
/*                      u4GrpAddress - Group address for which record is     */
/*                                     being created                         */
/*                      IgmPxySrcBmp - source bitmap                         */
/*                      pu2Offset - offset on global gpSrcInfoBuffer;        */
/*                      Indicates from where the new group record needs to   */
/*                      filled                                               */
/*                                                                           */
/* global variables   : gpSrcInfoBuffer - Contains group records, that should*/
/*                                        be sent upstream                   */
/*                                                                           */
/* Output(s)          : pu2Offset - offset on global gpSrcInfoBuffer; returns*/
/*                      the point upto which, the grouprecord is filled      */
/*                                                                           */
/* Return Value(s)    : Nothing                                              */
/*****************************************************************************/
VOID
IgpUtilFillGroupRecord (UINT1 u1RecordType, tIgmpIPvXAddr u4GrpAddress,
                        tIgmPxySrcBmp IgmPxySrcBmp, UINT2 *pu2Offset)
{
    UINT1               u1AuxDataLen = IGMP_ZERO;
    UINT4               u4IpAddress = IGMP_ZERO;
    UINT2               u2Count = IGMP_ZERO;
    UINT2               u2SrcCount = IGMP_ZERO;
    UINT2               u2TmpOffset = *pu2Offset;
    BOOL1               bResult = OSIX_FALSE;
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilFillGroupRecord()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddress, IPVX_ADDR_FMLY_IPV4);

    MEMCPY (gpSrcInfoBuffer + u2TmpOffset, &u1RecordType, IGMP_ONE);
    u2TmpOffset = (UINT2) (u2TmpOffset + IGMP_ONE);

    MEMCPY (gpSrcInfoBuffer + u2TmpOffset, &u1AuxDataLen, IGMP_ONE);
    u2TmpOffset = (UINT2) (u2TmpOffset + IGMP_ONE);

    /* We are not filling the number of sources now */
    u2TmpOffset = (UINT2) (u2TmpOffset + IGMP_TWO);

    u4IpAddress = OSIX_HTONL (u4IpGrp);
    MEMCPY (gpSrcInfoBuffer + u2TmpOffset, &u4IpAddress, IGP_IP_ADDR_SIZE);
    u2TmpOffset = (UINT2) (u2TmpOffset + IGP_IP_ADDR_SIZE);

    if (MEMCMP (IgmPxySrcBmp, gIgpNullSrcBmp, IGP_SRC_LIST_SIZE) != IGMP_ZERO)
    {
        for (u2Count = 0; u2Count < IGP_MAX_MCAST_SRCS; u2Count++)
        {
            OSIX_BITLIST_IS_BIT_SET (IgmPxySrcBmp, (UINT2) (u2Count + IGMP_ONE),
                                     IGP_SRC_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                IGMP_IP_COPY_FROM_IPVX (&u4IpSrc,
                                        gaIgpSrcInfo[u2Count].u4SrcIpAddr,
                                        IPVX_ADDR_FMLY_IPV4);
                u4IpAddress = OSIX_HTONL (u4IpSrc);

                MEMCPY (gpSrcInfoBuffer + u2TmpOffset, &u4IpAddress,
                        IGP_IP_ADDR_SIZE);

                u2TmpOffset = (UINT2) (u2TmpOffset + IGP_IP_ADDR_SIZE);
                u2SrcCount++;
            }
        }
    }
    /* fill the number of sources */
    u2SrcCount = OSIX_HTONS (u2SrcCount);
    MEMCPY (gpSrcInfoBuffer + (*pu2Offset) + IGMP_TWO, &u2SrcCount, IGMP_TWO);

    /* update the offset, to include this group record too */
    *pu2Offset = u2TmpOffset;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilFillGroupRecord()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUtilCheckUpIface                                  */
/*                                                                           */
/* Description        : This function will check whether the interface is    */
/*                      configured as upstream interface and also updates    */
/*                      the configured version of IGMP on the interface      */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interfcae Index                     */
/*                      u1Version      - IGMP Version                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_TRUE/IGMP_FALSE                                 */
/*****************************************************************************/
UINT1
IgpUtilCheckUpIface (UINT4 u4IfIndex)
{
    tIgmpPrxyUpIface   *pIgpUpIfaceEntry = NULL;
    UINT4               u4HashIndex = 0;

    /* Get the Interface node from the hash table */
    IGP_GET_UPIF_HASHINDEX (u4IfIndex, u4HashIndex);

    TMO_HASH_Scan_Bucket (gIgmpProxyInfo.pUpIfaceTbl, u4HashIndex,
                          pIgpUpIfaceEntry, tIgmpPrxyUpIface *)
    {
        if (pIgpUpIfaceEntry->u4IfIndex == u4IfIndex)
        {
            return IGMP_TRUE;
        }
    }

    return IGMP_FALSE;
}

/*****************************************************************************/
/* Function Name      : IgpUtilUpdateGroupRefCount                           */
/*                                                                           */
/* Description        : This function will update the group refernece count  */
/*                      for the source information                           */
/*                                                                           */
/* Input(s)           : OldInclSrcBmp - Old INCL source bitmap               */
/*                      OldExclSrcBmp - Old EXCL source bitmap               */
/*                      TempExclSrcBmp - New EXCL source bitmap              */
/*                      pIgpGroupEntry - Pointer to IGMP Proxy group entry   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUtilUpdateGroupRefCount (tIgmPxySrcBmp OldInclSrcBmp,
                            tIgmPxySrcBmp OldExclSrcBmp,
                            tIgmPxySrcBmp NewInclSrcBmp,
                            tIgmPxySrcBmp NewExclSrcBmp)
{
    tIgmPxySrcBmp       OldSrcBmp;
    tIgmPxySrcBmp       NewSrcBmp;
    UINT2               u2Count = 0;
    UINT2               u2SrcIndex = 0;
    BOOL1               bResult = OSIX_FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilUpdateGroupRefCount()\r \n");
    MEMSET (OldSrcBmp, 0, IGP_SRC_LIST_SIZE);
    MEMSET (NewSrcBmp, 0, IGP_SRC_LIST_SIZE);

    /* OR the old INCL and EXCL source bitmaps */
    IGP_OR_SRC_BMP (OldSrcBmp, OldInclSrcBmp);
    IGP_OR_SRC_BMP (OldSrcBmp, OldExclSrcBmp);

    /* OR the New INCL and EXCL source bitmaps */
    IGP_OR_SRC_BMP (NewSrcBmp, NewInclSrcBmp);
    IGP_OR_SRC_BMP (NewSrcBmp, NewExclSrcBmp);

    /* There is no change in the IGMP Source bitmaps */
    if (MEMCMP (OldSrcBmp, NewSrcBmp, IGP_SRC_LIST_SIZE) == 0)
    {
        return;
    }

    for (u2Count = 0; u2Count < IGP_MAX_MCAST_SRCS; u2Count++)
    {
        if (IGMP_IPVX_ADDR_COMPARE (gaIgpSrcInfo[u2Count].u4SrcIpAddr,
                                    gIPvXZero) == IGMP_ZERO)
        {
            continue;
        }

        u2SrcIndex = (UINT2) (u2Count + IGMP_ONE);

        /* If the source is not present in the Old Source bitmap but it is
         * present in the new source bitmap */

        OSIX_BITLIST_IS_BIT_SET (OldSrcBmp, u2SrcIndex,
                                 IGP_SRC_LIST_SIZE, bResult);

        if (bResult == OSIX_FALSE)
        {
            OSIX_BITLIST_IS_BIT_SET (NewSrcBmp, u2SrcIndex,
                                     IGP_SRC_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                gaIgpSrcInfo[u2Count].u2GrpCount++;
            }
        }
        else
        {
            /* Source is present in the Old source bitmap and it is
             * not present in the new bitmap then decrement the 
             * group ref count */

            OSIX_BITLIST_IS_BIT_SET (NewSrcBmp, u2SrcIndex,
                                     IGP_SRC_LIST_SIZE, bResult);

            if (bResult == OSIX_FALSE)
            {
                gaIgpSrcInfo[u2Count].u2GrpCount--;

                if (gaIgpSrcInfo[u2Count].u2GrpCount == 0)
                {
                    IGMP_IPVX_ADDR_CLEAR (&(gaIgpSrcInfo[u2Count].u4SrcIpAddr));
                }
            }
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilUpdateGroupRefCount()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUtilSendQuerierTrap                               */
/*                                                                           */
/* Description        : This function will send an trap to the administrator */
/*                      when a query with lower IP address is received on    */
/*                      downstream interface                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                      u4QuerierAddr - Address of the querier which has     */
/*                                      sent a query                         */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
/*The following function has U4 specific in stead of IPvX */
INT4
IgpUtilSendQuerierTrap (UINT4 u4Port, tIgmpIPvXAddr u4QuerierAddr)
{
#ifdef SNMP_2_WANTED
    tIgmpPrxyTrap       IgmpPrxyTrap;
    tSNMP_OID_TYPE     *pEnterpriseOid;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[IGP_OBJ_NAME_LEN];
    UINT4               u4GenTrapType;
    UINT4               u4SpecTrapType;
    UINT4               u4IfIndex = IGMP_INVALID_INDEX;
    UINT4               u4TempAddr;
    UINT4               u4IpAddr = 0;
    INT1                IGP_TRAPS_OID[SNMP_MAX_OID_LENGTH];
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilSendQuerierTrap()\r \n");
    MEMSET (IGP_TRAPS_OID, '\0', SNMP_MAX_OID_LENGTH);
    SPRINTF ((CHR1 *) IGP_TRAPS_OID, "1.3.6.1.4.1.%s.124.5.0",
             EoidGetEnterpriseOid ());

    if (u4QuerierAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        if (IGMP_IP_GET_IFINDEX_FROM_PORT (u4Port, &u4IfIndex))
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Failed to convert IP port number in to IP VLAN index\r\n");
            return IGMP_FAILURE;
        }
    }
    else if (u4QuerierAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        if (MLD_IP6_GET_IFINDEX_FROM_PORT (u4Port, &u4IfIndex))
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Failed to convert IP6 port number in to IP6 VLAN index\r\n");
            return IGMP_FAILURE;
        }
    }
    /* Copy the Interface index and Querier address
     * to the trap data structure */

    IgmpPrxyTrap.u4IfIndex = u4IfIndex;
    IGMP_IPVX_ADDR_COPY (&(IgmpPrxyTrap.u4QuerierAddress), &(u4QuerierAddr));

    gIgmpProxyInfo.TrapInfo.u4IfIndex = u4IfIndex;
    IGMP_IPVX_ADDR_COPY (&(gIgmpProxyInfo.TrapInfo.u4QuerierAddress),
                         &u4QuerierAddr);

    u4GenTrapType = ENTERPRISE_SPECIFIC;
    u4SpecTrapType = 1;

    pEnterpriseOid = SNMP_AGT_GetOidFromString (IGP_TRAPS_OID);
    if (pEnterpriseOid == NULL)
    {
        return IGMP_FAILURE;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    SPRINTF ((char *) au1Buf, "fsIgmpProxyQuerierIfIndex");
    pOid = IgpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return IGMP_FAILURE;
    }

    pVbList =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              (INT4) IgmpPrxyTrap.u4IfIndex,
                              NULL, NULL, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        return IGMP_FAILURE;
    }

    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, "fsIgmpProxyQuerierAddress");
    pOid = IgpUtilMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        IgpUtilFreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return IGMP_FAILURE;
    }
    if (u4QuerierAddr.u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        IGMP_IP_COPY_FROM_IPVX (&u4IpAddr, IgmpPrxyTrap.u4QuerierAddress,
                                IPVX_ADDR_FMLY_IPV4);
        u4TempAddr = OSIX_HTONL (u4IpAddr);
        pOstring =
            SNMP_AGT_FormOctetString ((UINT1 *) &u4TempAddr,
                                      (INT4) IGP_IP_ADDR_SIZE);
    }
    else if (u4QuerierAddr.u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        pOstring =
            SNMP_AGT_FormOctetString (u4QuerierAddr.au1Addr,
                                      (INT4) IGP_IP6_ADDR_SIZE);
    }

    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        IgpUtilFreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return IGMP_FAILURE;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        IgpUtilFreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return IGMP_FAILURE;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (pEnterpriseOid, u4GenTrapType,
                              u4SpecTrapType, pStartVb);

#else
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4QuerierAddr);
#endif
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilSendQuerierTrap()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpUtilMakeObjIdFromDotNew                           */
/*                                                                           */
/* Description        : This function will return the OID from name of the   */
/*                      object                                               */
/*                                                                           */
/* Input(s)           : pi1TextStr - Object Name                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pOidPtr  or NULL                                     */
/*****************************************************************************/
tSNMP_OID_TYPE     *
IgpUtilMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    INT1               *pTemp = NULL, *pDot = NULL;
    UINT1               au1tempBuffer[IGP_OBJ_NAME_LEN];
    tSNMP_OID_TYPE      IfIndexPrefix;
    tSNMP_OID_TYPE      IfIndexSuffix;
    tSNMP_OID_TYPE     *pIfIndex = NULL;
    tSNMP_OID_TYPE      QuerierAddrPrefix;
    tSNMP_OID_TYPE      QuerierAddrSuffix;
    tSNMP_OID_TYPE     *pQuerierAddr = NULL;
    UINT4               au4IfIndexPrefix[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4IfIndexSuffix[] = { 124, 5, 1 };
    UINT4               au4QuerierAddrPrefix[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4QuerierAddrSuffix[] = { 124, 5, 2 };
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpUtilMakeObjIdFromDotNew()\r \n");
    if ((pIfIndex = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Memory Allocation Failure.\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
	      IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"happened\n");

        return (NULL);
    }
    if ((pQuerierAddr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Memory Allocation Failure.\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"happened\n");
        free_oid (pIfIndex);
        return (NULL);
    }

    MEMSET (pIfIndex->pu4_OidList, 0, SNMP_MAX_OID_LENGTH * sizeof (UINT4));
    MEMSET (pQuerierAddr->pu4_OidList, 0, SNMP_MAX_OID_LENGTH * sizeof (UINT4));

    IfIndexPrefix.pu4_OidList = au4IfIndexPrefix;
    IfIndexPrefix.u4_Length = sizeof (au4IfIndexPrefix) / (sizeof (UINT4));

    IfIndexSuffix.pu4_OidList = au4IfIndexSuffix;
    IfIndexSuffix.u4_Length = sizeof (au4IfIndexSuffix) / (sizeof (UINT4));

    pIfIndex->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&IfIndexPrefix, &IfIndexSuffix,
                              pIfIndex) == OSIX_FAILURE)
    {
        free_oid (pQuerierAddr);
        free_oid (pIfIndex);
        return NULL;
    }

    QuerierAddrPrefix.pu4_OidList = au4QuerierAddrPrefix;
    QuerierAddrPrefix.u4_Length =
        sizeof (au4QuerierAddrPrefix) / (sizeof (UINT4));

    QuerierAddrSuffix.pu4_OidList = au4QuerierAddrSuffix;
    QuerierAddrSuffix.u4_Length =
        sizeof (au4QuerierAddrSuffix) / (sizeof (UINT4));

    pQuerierAddr->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&QuerierAddrPrefix, &QuerierAddrSuffix,
                              pQuerierAddr) == OSIX_FAILURE)
    {
        free_oid (pQuerierAddr);
        free_oid (pIfIndex);
        return NULL;
    }

    /* Is there an alpha descriptor at begining ?? */
    if (isalpha (*pi1TextStr))
    {
        pDot = ((INT1 *) STRCHR ((char *) pi1TextStr, '.'));

        /* if no dot, point to end of string */
        if (pDot == NULL)
        {
            pDot = ((INT1 *) (pi1TextStr + STRLEN ((char *) pi1TextStr)));
        }
        pTemp = ((INT1 *) pi1TextStr);
        MEMSET (au1tempBuffer, 0, IGP_OBJ_NAME_LEN);
        STRNCPY (au1tempBuffer, pTemp, (pDot - pTemp));
        au1tempBuffer[(pDot - pTemp)] = '\0';

        if ((pOid = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
        {
            free_oid (pQuerierAddr);
            free_oid (pIfIndex);
            return (NULL);
        }

        if ((STRCMP ("fsIgmpProxyQuerierIfIndex", (INT1 *) au1tempBuffer) == 0))
        {
            MEMCPY (pOid->pu4_OidList, pIfIndex->pu4_OidList,
                    pIfIndex->u4_Length * sizeof (UINT4));
            pOid->u4_Length = pIfIndex->u4_Length;
            free_oid (pQuerierAddr);
            free_oid (pIfIndex);
            return (pOid);

        }
        else if ((STRCMP ("fsIgmpProxyQuerierAddress",
                          (INT1 *) au1tempBuffer) == 0))
        {
            MEMCPY (pOid->pu4_OidList, pQuerierAddr->pu4_OidList,
                    pQuerierAddr->u4_Length * sizeof (UINT4));
            pOid->u4_Length = pQuerierAddr->u4_Length;
            free_oid (pQuerierAddr);
            free_oid (pIfIndex);
            return (pOid);

        }
        free_oid (pOid);
    }
    free_oid (pQuerierAddr);
    free_oid (pIfIndex);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpUtilMakeObjIdFromDotNew()\r \n");
    return NULL;
}

/*****************************************************************************/
/* Function Name      : IgpUtilFreeVarBindList                               */
/*                                                                           */
/* Description        : This function will free the memory allocated for     */
/*                      SNMP VAR Bind list                                   */
/*                                                                           */
/* Input(s)           : pVarBindLst - Pointer to the VAR Bind list           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUtilFreeVarBindList (tSNMP_VAR_BIND * pVarBindLst)
{
    tSNMP_VAR_BIND     *pVarBndNext = NULL;
    tSNMP_VAR_BIND     *pVarBind = NULL;

    for (pVarBind = pVarBindLst; pVarBind != NULL;)
    {
        pVarBndNext = pVarBind->pNextVarBind;
        SNMP_FreeOid (pVarBind->pObjName);
        SNMP_FreeOid (pVarBind->ObjValue.pOidValue);
        free_octetstring (pVarBind->ObjValue.pOctetStrValue);
        MEM_FREE (pVarBind);
        pVarBind = NULL;
        pVarBind = pVarBndNext;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUtilBuildProxyDatabase                            */
/*                                                                           */
/* Description        : This function will build the proxy group member data */
/*                      base and send report to upstream interface for the   */
/*                      groups learnt before IGMP Proxy is enable            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUtilBuildProxyDatabase (VOID)
{
    tIgmpGroup         *pIgmpGrpEntry = NULL;

    pIgmpGrpEntry = (tIgmpGroup *) RBTreeGetFirst (gIgmpGroup);
    while (pIgmpGrpEntry != NULL)
    {
        IgpGrpConsolidate (pIgmpGrpEntry,
                           pIgmpGrpEntry->pIfNode->u1Version, IGMP_NEW_GROUP);

        pIgmpGrpEntry = (tIgmpGroup *) RBTreeGetNext (gIgmpGroup,
                                                      (tRBElem *) pIgmpGrpEntry,
                                                      NULL);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : IgpUtilRegisterFsIgpMib                              */
/*                                                                           */
/* Description        : This function will register the fsigp.mib            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUtilRegisterFsIgpMib ()
{
    RegisterFSIGP ();
    return;
}

/*****************************************************************************/
/* Function Name      : IgpUtilGetOifBitListFrmRtOifList                     */
/*                                                                           */
/* Description        : This function will get the OIF Port list             */
/*                     from the route entry OIF node.       */
/*                                                                           */
/* Input(s)           : pIgpForwardEntry                                     */
/*                                                                           */
/* Output(s)          : pIgpForwardEntry                                     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpUtilGetOifBitListFrmRtOifList (tIgmpPrxyFwdEntry * pIgpForwardEntry)
{
    tIgmpPrxyOif       *pOifNode = NULL;

    TMO_SLL_Scan (&pIgpForwardEntry->OifList, pOifNode, tIgmpPrxyOif *)
    {
        /* index starts from 0 and hence the +1 */
        OSIX_BITLIST_SET_BIT ((pIgpForwardEntry->McFwdPortList),
                              (pOifNode->u4IfIndex + 1), sizeof (tPortListExt));
    }

    return;
}
