/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igpsz.c,v 1.5 2015/02/13 11:13:54 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _IGPSZ_C
#include "igpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
IgpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpSizingMemCreateMemPools()\r \n");
    for (i4SizingId = 0; i4SizingId < IGP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsIGPSizingParams[i4SizingId].u4StructSize,
                                     FsIGPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(IGPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            IgpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpSizingMemCreateMemPools()\r \n");
    return OSIX_SUCCESS;
}

INT4
IgpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsIGPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, IGPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
IgpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpSizingMemDeleteMemPools()\r \n");

    for (i4SizingId = 0; i4SizingId < IGP_MAX_SIZING_ID; i4SizingId++)
    {
        if (IGPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IGPMemPoolIds[i4SizingId]);
            IGPMemPoolIds[i4SizingId] = 0;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpSizingMemDeleteMemPools()\r \n");
    return;
}
