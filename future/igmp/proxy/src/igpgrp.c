/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igpgrp.c,v 1.12 2015/02/13 11:13:54 siva Exp $
 *
 ******************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : igpgrp.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Group module                        */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains processing routines         */
/*                            for IGMP Proxy group module                    */
/*---------------------------------------------------------------------------*/

#include "igpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;
#endif

static tIgmPxySrcBmp TempOldExclBmp;
/*****************************************************************************/
/* Function Name      : IgpGrpCreateGroupEntry                               */
/*                                                                           */
/* Description        : This function creates a new group entry in the       */
/*                      consolidated data base                               */
/*                                                                           */
/* Input(s)           : pIgmpGrpEntry - Pointer to IGMP Group entry          */
/*                      u1Version - IGMP version                             */
/*                                                                           */
/* Output(s)          : ppRetIgpGroupEntry - pointer to the group entry      */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpGrpCreateGroupEntry (tIgmpGroup * pIgmpGrpEntry, UINT1 u1Version,
                        tIgmpPrxyGrpInfo ** ppRetIgpGroupEntry)
{
    tIgmpPrxyGrpInfo   *pIgpGroupEntry = NULL;
    UINT4               u4Status = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpCreateGroupEntry()\r \n");
    if (IGMP_MEM_ALLOCATE (IGP_GRP_MEMPOOL_ID,
                           pIgpGroupEntry, tIgmpPrxyGrpInfo) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Memory allocation failed for creation of Group Entry\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                        IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
			"for group entry\n");
        return IGMP_FAILURE;
    }

    MEMSET (pIgpGroupEntry, 0, sizeof (tIgmpPrxyGrpInfo));

    /* Set the values for the various oblects */
    IGMP_IPVX_ADDR_COPY (&(pIgpGroupEntry->u4GrpIpAddr),
                         &(pIgmpGrpEntry->u4Group));
    pIgpGroupEntry->u1FilterMode = pIgmpGrpEntry->u1FilterMode;

    /* Increment the v1/v2 interface count if the create group entry is 
     * triggered because of IGMP v1/v2 reports */
    if (u1Version != IGMP_VERSION_3)
    {
        pIgpGroupEntry->u4v1v2IfaceCnt++;
    }

    /* Increment the global increment count */
    pIgpGroupEntry->u4IfaceCount++;

    /* Copy the include and exclude source bitmaps */
    MEMCPY (pIgpGroupEntry->InclSrcBmp,
            pIgmpGrpEntry->InclSrcBmp, IGP_SRC_LIST_SIZE);
    MEMCPY (pIgpGroupEntry->ExclSrcBmp,
            pIgmpGrpEntry->ExclSrcBmp, IGP_SRC_LIST_SIZE);

    /* Add Node */
    u4Status =
        RBTreeAdd (gIgmpProxyInfo.GroupEntry, (tRBElem *) pIgpGroupEntry);

    if (u4Status == RB_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "RBTree Addition Failed for Group Entry \r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                     IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		     "during RBTree Addition for group Entry\n");
        IGP_RELEASE_MEM_BLOCK (IGP_GRP_MEMPOOL_ID, pIgpGroupEntry);
        return IGMP_FAILURE;
    }

    *ppRetIgpGroupEntry = pIgpGroupEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpCreateGroupEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpGrpDeleteGroupEntry                               */
/*                                                                           */
/* Description        : This function deletes a group entry from the         */
/*                      consolidated table                                   */
/*                                                                           */
/* Input(s)           : u4GrpIpAddr - Group IP address                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpGrpDeleteGroupEntry (tIgmpIPvXAddr u4GrpIpAddr)
{
    tIgmpPrxyGrpInfo   *pIgpGroupEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tIgmpPrxyGrpInfo    IgpGroupEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpDeleteGroupEntry()\r \n");
    MEMSET (&IgpGroupEntry, 0, sizeof (tIgmpPrxyGrpInfo));

    IGMP_IPVX_ADDR_COPY (&(IgpGroupEntry.u4GrpIpAddr), &u4GrpIpAddr);

    pRBElem =
        RBTreeGet (gIgmpProxyInfo.GroupEntry, (tRBElem *) & IgpGroupEntry);

    if (pRBElem == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "Group Entry not found for Group %s\r\n", IgmpPrintIPvxAddress(u4GrpIpAddr));
        return IGMP_FAILURE;
    }
    else
    {
        pIgpGroupEntry = (tIgmpPrxyGrpInfo *) pRBElem;

        /* Delete all the multicast forwarding entries for the
         * group entry */
        IgpFwdDeleteFwdEntries (pIgpGroupEntry->u4GrpIpAddr);

        RBTreeRemove (gIgmpProxyInfo.GroupEntry, pRBElem);
        IGP_RELEASE_MEM_BLOCK (IGP_GRP_MEMPOOL_ID, pIgpGroupEntry);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpDeleteGroupEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpGrpGetGroupEntry                                  */
/*                                                                           */
/* Description        : This function gets the group entry from group        */
/*                      membership data base                                 */
/*                                                                           */
/* Input(s)           : u4GrpIpAddr - Group IP address                       */
/*                                                                           */
/* Output(s)          : pointer to the group entry or NULL                   */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpGrpGetGroupEntry (tIgmpIPvXAddr u4GrpIpAddr,
                     tIgmpPrxyGrpInfo ** ppRetIgpGroupEntry)
{
    tRBElem            *pRBElem = NULL;
    tIgmpPrxyGrpInfo    IgpGroupEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpGetGroupEntry()\r \n");
    MEMSET (&IgpGroupEntry, 0, sizeof (tIgmpPrxyGrpInfo));

    IGMP_IPVX_ADDR_COPY (&(IgpGroupEntry.u4GrpIpAddr), &u4GrpIpAddr);

    pRBElem =
        RBTreeGet (gIgmpProxyInfo.GroupEntry, (tRBElem *) & IgpGroupEntry);

    if (pRBElem == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                       "Group Entry not found for Group %s\r\n", IgmpPrintIPvxAddress(u4GrpIpAddr));
        return IGMP_FAILURE;
    }
    else
    {
        *ppRetIgpGroupEntry = (tIgmpPrxyGrpInfo *) pRBElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpGetGroupEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpGrpDeleteGroupEntries                             */
/*                                                                           */
/* Description        : This function deletes all the group entries in the   */
/*                      consolidated data base                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpGrpDeleteGroupEntries (VOID)
{
    tIgmpPrxyGrpInfo   *pIgpGroupEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpDeleteGroupEntries()\r \n");
    if (gIgmpProxyInfo.GroupEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "No Group entries found for this instance\r\n");
        return;
    }

    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.GroupEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext = RBTreeGetNext (gIgmpProxyInfo.GroupEntry, pRBElem, NULL);

        pIgpGroupEntry = (tIgmpPrxyGrpInfo *) pRBElem;
        RBTreeRemove (gIgmpProxyInfo.GroupEntry, (tRBElem *) pIgpGroupEntry);
        IGP_RELEASE_MEM_BLOCK (IGP_GRP_MEMPOOL_ID, pIgpGroupEntry);
        pRBElem = pRBElemNext;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpDeleteGroupEntries()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpGrpConsolidate                                    */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      membership data base for a single group and also     */
/*                      updates the source bitmap of per interface IGMP      */
/*                      group entry                                          */
/*                                                                           */
/* Input(s)           : pIgmpGroupEntry - Pointer to the group entry         */
/*                      u1VerFlag  - IGMP version Flag                       */
/*                      u1GrpIfaceStat  - Indicates the status of the group  */
/*                      on the IGMP capable interface                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Nothing                                              */
/*****************************************************************************/
VOID
IgpGrpConsolidate (tIgmpGroup * pIgmpGroupEntry, UINT1 u1VerFlag,
                   UINT1 u1GrpIfaceStat)
{
    UINT1               u1Version = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpConsolidate()\r \n");
    u1Version = (u1VerFlag & IGMP_VERSION_FLAG);

    if (u1GrpIfaceStat != IGMP_GROUP_DELETION)
    {
        /* If IGMP v1/v2 report is received set the filter mode to EXCL and
         * INCL source bitmap and EXCL source bitmap to NULL */
        if ((u1Version == IGMP_VERSION_1) || (u1Version == IGMP_VERSION_2))
        {
            pIgmpGroupEntry->u1FilterMode = IGMP_FMODE_EXCLUDE;
            MEMSET (pIgmpGroupEntry->ExclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
            MEMSET (pIgmpGroupEntry->InclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
        }
        /* Update source bitmap information for the group entry */
        else if (IgpUtilUpdateSourceInfo (pIgmpGroupEntry) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Updation of source bitmap of the interface "
                      "and global source array failed\r\n");
            return;
        }

        /* update the consolidated table based on the change in 
         * per interface IGMP group entry */
        if (IgpGrpUpdateGroupTable (pIgmpGroupEntry, u1Version,
                                    u1GrpIfaceStat) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Updation of consolidated database failed "
                      "on IgpGrpUpdateGroupTable\r\n");
            return;
        }
    }
    else
    {
        /* A group is deleted in a per interface IGMP table
         * So handle accordingly */
        pIgmpGroupEntry->u1FilterMode = IGMP_FMODE_INCLUDE;
        MEMSET (pIgmpGroupEntry->InclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);

        if (IgpGrpDeleteGroupOnIface (pIgmpGroupEntry,
                                      u1VerFlag) == IGMP_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Updation of consolidated database failed "
                      "on IgpGrpDeleteGroupOnIface\r\n");
            return;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpConsolidate()\r \n");
}


/*****************************************************************************/
/* Function Name      : IgpGrpDeleteGroupOnIface                             */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      membership data base, when a group is deleted on any */
/*                      interface                                            */
/*                                                                           */
/* Input(s)           : pIgmpGroupEntry - Pointer to the group entry         */
/*                      u1VerFlag  - IGMP version Flag for report received   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpGrpDeleteGroupOnIface (tIgmpGroup * pIgmpGroupEntry, UINT1 u1VerFlag)
{
    tIgmpPrxyGrpInfo   *pIgpGroupEntry = NULL;
    tIgmPxySrcBmp       OldExclSrcBmp;
    tIgmPxySrcBmp       OldInclSrcBmp;
    UINT1               u1OldFilterMode = 0;
    UINT1               u1V3SendFlag = IGP_DONT_SEND_V3_REPORT;
    UINT1               u1Version = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpDeleteGroupOnIface()\r \n");
    u1Version = (u1VerFlag & IGMP_VERSION_FLAG);

    if (IgpGrpGetGroupEntry (pIgmpGroupEntry->u4Group,
                             &pIgpGroupEntry) != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Group entry does not exist in the consolidated database"
                  "Fatal Error\r\n");
        return IGMP_FAILURE;
    }

    if (u1VerFlag & IGMP_V3REP_SEND_FLAG)
    {
        u1V3SendFlag = IGP_SEND_V3_REPORT;
    }

    /* decrement the interface count */
    (pIgpGroupEntry->u4IfaceCount)--;

    /* If there are no receivers in the LAN delete the group entry
     * and send Leave or TO_INCL () on to the upstream interfaces */
    if (pIgpGroupEntry->u4IfaceCount == 0)
    {
        /* Call the routine to update the group reference count in 
         * the Global source array */
        IgpUtilUpdateGroupRefCount (pIgpGroupEntry->InclSrcBmp,
                                    pIgpGroupEntry->ExclSrcBmp,
                                    gIgpNullSrcBmp, gIgpNullSrcBmp);

        IgpGrpDeleteGroupEntry (pIgmpGroupEntry->u4Group);
        IgpUpIfConstructUpstreamReport (IGMP_ZERO, NULL, NULL, NULL,
                                        pIgmpGroupEntry->u4Group,
                                        IGP_LEAVE_MESSAGE, u1V3SendFlag);
        return IGMP_SUCCESS;
    }

    if ((u1Version == IGMP_VERSION_1) || (u1Version == IGMP_VERSION_2))
    {
        pIgpGroupEntry->u4v1v2IfaceCnt--;

        if (pIgpGroupEntry->u4v1v2IfaceCnt > 0)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Atleast one v1/v2 receiver exists; So no change in "
                      "consolidated database\r\n");

            /* Summarize the forwarding table */
            IgpFwdSummarizeForwardTable (pIgpGroupEntry, pIgmpGroupEntry);
            return IGMP_SUCCESS;
        }
    }

    u1OldFilterMode = pIgpGroupEntry->u1FilterMode;
    MEMCPY (OldInclSrcBmp, pIgpGroupEntry->InclSrcBmp, IGP_SRC_LIST_SIZE);
    MEMCPY (OldExclSrcBmp, pIgpGroupEntry->ExclSrcBmp, IGP_SRC_LIST_SIZE);

    /* Consolidate the Source information */
    IgpGrpConsolidateSrcInfo (u1OldFilterMode, OldInclSrcBmp,
                              OldExclSrcBmp, pIgpGroupEntry,
                              IGP_DONT_SEND_V1V2_REPORT, u1V3SendFlag);

    /* Summarize the forwarding table */
    IgpFwdSummarizeForwardTable (pIgpGroupEntry, pIgmpGroupEntry);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpDeleteGroupOnIface()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpGrpUpdateGroupTable                               */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      membership database for changes in per interface     */
/*                      IGMP group entry                                     */
/*                                                                           */
/* Input(s)           : pIgmpGroupEntry - Pointer to the group entry         */
/*                      u1Version  - IGMP version of report received         */
/*                      u1NewInterface  - It can take the following values   */
/*                      IGMP_NOT_A_NEW_GROUP - Already existing group        */
/*                      IGMP_NEW_GROUP - New group registration              */
/*                      IGMP_GROUP_DEGRADED - Group compatibiltiy mode chaneg*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS/IGMP_FAILURE                            */
/*****************************************************************************/
INT4
IgpGrpUpdateGroupTable (tIgmpGroup * pIgmpGroupEntry, UINT1 u1Version,
                        UINT1 u1NewInterface)
{
    tIgmpPrxyGrpInfo   *pIgpGroupEntry = NULL;
    tIgmPxySrcBmp       OldExclSrcBmp;
    tIgmPxySrcBmp       OldInclSrcBmp;
    UINT1               u1OldFilterMode = 0;
    UINT1               u1V3SendFlag = IGP_SEND_V3_REPORT;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpUpdateGroupTable()\r \n");
    MEMSET (OldExclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
    MEMSET (OldInclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);

    /* Check if the group entry is present */
    if (IgpGrpGetGroupEntry (pIgmpGroupEntry->u4Group,
                             &pIgpGroupEntry) != IGMP_SUCCESS)
    {
        /* Group record not present, create a new group record and 
         * update the Filter mode and source information */
        if (IgpGrpCreateGroupEntry (pIgmpGroupEntry, u1Version,
                                    &pIgpGroupEntry) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Creation of group entry in the consolidated"
                      " group member database failed\r\n");
            return IGMP_FAILURE;
        }

        if (u1Version == IGMP_VERSION_3)
        {
            /* Call the routine to update the group reference count in 
             * the Global source array */
            IgpUtilUpdateGroupRefCount (gIgpNullSrcBmp, gIgpNullSrcBmp,
                                        pIgpGroupEntry->InclSrcBmp,
                                        pIgpGroupEntry->ExclSrcBmp);
            MEMCPY (TempOldExclBmp, pIgpGroupEntry->ExclSrcBmp,
                    IGP_SRC_LIST_SIZE);
            u1V3SendFlag = IGP_DONT_SEND_V3_REPORT;
        }

        /* Send the group record on to the upstream interfaces based upon 
         * the version of IGMP. We need to send TO_EXCL or TO_INCL; so 
         * call the function with old filtermode, as just opposite value */
        if (pIgmpGroupEntry->u1FilterMode == IGMP_FMODE_EXCLUDE)
        {
            IgpUpIfConstructUpstreamReport (IGMP_FMODE_INCLUDE, OldInclSrcBmp,
                                            OldExclSrcBmp, pIgpGroupEntry,
                                            pIgmpGroupEntry->u4Group,
                                            IGP_SEND_V1V2_REPORT, u1V3SendFlag);
        }
        else
        {
            IgpUpIfConstructUpstreamReport (IGMP_FMODE_EXCLUDE, OldInclSrcBmp,
                                            OldExclSrcBmp, pIgpGroupEntry,
                                            pIgmpGroupEntry->u4Group,
                                            IGP_SEND_V1V2_REPORT, u1V3SendFlag);
        }
    }
    else
    {
        /* Copy the old filter mode, include and exclude source bitmap
         * which is required for consolidation */
        u1OldFilterMode = pIgpGroupEntry->u1FilterMode;
        MEMCPY (OldInclSrcBmp, pIgpGroupEntry->InclSrcBmp, IGP_SRC_LIST_SIZE);
        MEMCPY (OldExclSrcBmp, pIgpGroupEntry->ExclSrcBmp, IGP_SRC_LIST_SIZE);

        /* Increment the global interface count */
        if (u1NewInterface == IGMP_NEW_GROUP)
            pIgpGroupEntry->u4IfaceCount++;

        /* Group is already present in the data base 
         * If a join was received from the IGMP v1/v2 host set EXCLUDE bitmap to
         * NULL and update the v1v2interface count for the group entry if the
         * join has come on a new interface */

        if (u1Version != IGMP_VERSION_3)
        {
            pIgpGroupEntry->u1FilterMode = IGMP_FMODE_EXCLUDE;
            MEMSET (pIgpGroupEntry->ExclSrcBmp, 0, IGP_SRC_LIST_SIZE);
            MEMSET (pIgpGroupEntry->InclSrcBmp, 0, IGP_SRC_LIST_SIZE);

            if ((u1NewInterface == IGMP_NEW_GROUP) ||
                (u1NewInterface == IGMP_GROUP_DEGRADED))
            {
                pIgpGroupEntry->u4v1v2IfaceCnt++;
            }

            /* Send a IGMP report upstream based upon the version of
             * IGMP on the upstream interface .Since this is not a
             * new group, there is no need to send V1, V2 reports*/

            IgpUpIfConstructUpstreamReport (u1OldFilterMode, OldInclSrcBmp,
                                            OldExclSrcBmp, pIgpGroupEntry,
                                            pIgpGroupEntry->u4GrpIpAddr,
                                            IGP_DONT_SEND_V1V2_REPORT,
                                            u1V3SendFlag);

            /* Summarize the forwarding table */
            IgpFwdSummarizeForwardTable (pIgpGroupEntry, pIgmpGroupEntry);

            /* MAKE sure not to send a report if no change occured, 
             * ie dont send reportfor second join on same group */
        }
        else
        {
            if (u1NewInterface == IGMP_GROUP_UPGRADED)
            {
                pIgpGroupEntry->u4v1v2IfaceCnt--;
            }

            if (pIgpGroupEntry->u4v1v2IfaceCnt == IGMP_ZERO)
            {
                /* Consolidate the group and source information */
                IgpGrpConsolidateSrcInfo (u1OldFilterMode, OldInclSrcBmp,
                                          OldExclSrcBmp, pIgpGroupEntry,
                                          IGP_DONT_SEND_V1V2_REPORT,
                                          IGP_DONT_SEND_V3_REPORT);
            }

            /* Summarize the forwarding table */
            IgpFwdSummarizeForwardTable (pIgpGroupEntry, pIgmpGroupEntry);
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpUpdateGroupTable()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpGrpConsolidateSrcInfo                             */
/*                                                                           */
/* Description        : This function will consolidate the source information*/
/*                      for a group record                                   */
/*                                                                           */
/* Input(s)           : OldFilterMode - Old Filter Mode                      */
/*                      OldInclSrcBmp - Old INCL source bitmap               */
/*                      OldExclSrcBmp - Old EXCL source bitmap               */
/*                      pIgpGroupEntry - Pointer to the group entry          */
/*                      u1V1V2SendFlag - Flag to indicate the transmission of*/
/*                                       V1/V2 reports                       */
/*                      u1V3SendFlag   - Flag to indicate the transmission of*/
/*                                       V3 reports                          */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpGrpConsolidateSrcInfo (UINT1 u1OldFilterMode, tIgmPxySrcBmp OldInclBmp,
                          tIgmPxySrcBmp OldExclBmp,
                          tIgmpPrxyGrpInfo * pIgpGroupEntry,
                          UINT1 u1V1V2SendFlag, UINT1 u1V3SendFlag)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pIgmpGrpEntry = NULL;
    tIgmPxySrcBmp       TempExclBmp;
    tIgmPxySrcBmp       TempSrcBmp;
    MEMSET (pIgpGroupEntry->InclSrcBmp, 0, IGP_SRC_LIST_SIZE);
    MEMSET (pIgpGroupEntry->ExclSrcBmp, 0, IGP_SRC_LIST_SIZE);
    MEMSET (TempExclBmp, 0, IGP_SRC_LIST_SIZE);
    MEMSET (TempSrcBmp, 0xff, IGP_SRC_LIST_SIZE);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpGrpConsolidateSrcInfo()\r \n");
    pIgpGroupEntry->u1FilterMode = IGMP_FMODE_INCLUDE;

    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    while (pIfGetNextLink != NULL)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfaceNode->IfGetNextLink));

        /* Get the group entry for the given group address */
        pIgmpGrpEntry = IgmpGrpLookUp (pIfaceNode, pIgpGroupEntry->u4GrpIpAddr);

        if (pIgmpGrpEntry == NULL)
        {
            /* Group entry does not exists for the 
             * specified group address */
            continue;
        }

        /* source bitmap updation for the group record */
        IGP_OR_SRC_BMP (pIgpGroupEntry->InclSrcBmp, pIgmpGrpEntry->InclSrcBmp);

        /* source bitmap updation for the group record */
        IGP_OR_SRC_BMP (TempExclBmp, pIgmpGrpEntry->ExclSrcBmp);

        if (pIgmpGrpEntry->u1FilterMode == IGMP_FMODE_EXCLUDE)
        {
            IGP_AND_SRC_BMP (TempSrcBmp, pIgmpGrpEntry->ExclSrcBmp);
            pIgpGroupEntry->u1FilterMode = IGMP_FMODE_EXCLUDE;
        }
    }
    if (pIgpGroupEntry->u1FilterMode == IGMP_FMODE_EXCLUDE)
    {
        MEMCPY (pIgpGroupEntry->ExclSrcBmp, TempSrcBmp, IGP_SRC_LIST_SIZE);
    }
    else
    {
        MEMSET (pIgpGroupEntry->ExclSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
    }

    /* Make the include and exclude bitmap mutually exclusive */
    IGP_UPDATE_SRC_INFO (pIgpGroupEntry->InclSrcBmp,
                         pIgpGroupEntry->ExclSrcBmp);

    /* Update for any filter mode change in the group record if there is any 
     * IGMP v1/v2 receivers are present */
    if (pIgpGroupEntry->u4v1v2IfaceCnt)
    {
        pIgpGroupEntry->u1FilterMode = IGMP_FMODE_EXCLUDE;
    }

    /* Send a IGMP report upstream based upon the version of
     * IGMP on the upstream interface .Since it is not a
     * new group, there is no need to send V1, V2 reports*/
    IgpUpIfConstructUpstreamReport (u1OldFilterMode, OldInclBmp, OldExclBmp,
                                    pIgpGroupEntry,
                                    pIgpGroupEntry->u4GrpIpAddr,
                                    u1V1V2SendFlag, u1V3SendFlag);

    /* Call the routine to update the group reference count in 
     * the Global source array */
    IgpUtilUpdateGroupRefCount (OldInclBmp, TempOldExclBmp,
                                pIgpGroupEntry->InclSrcBmp, TempExclBmp);

    /* Copy the latest TempExclBitmap to TempOldExclBmp so that this
     * will be used during the next update for the group reference count 
     */
    MEMCPY (TempOldExclBmp, TempExclBmp, IGP_SRC_LIST_SIZE);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
          "Exiting  IgpGrpConsolidateSrcInfo()\r \n");
    return;
}
