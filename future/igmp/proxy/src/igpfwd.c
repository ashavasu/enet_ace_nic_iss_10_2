/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igpfwd.c,v 1.17 2015/02/13 11:13:54 siva Exp $
 *
 * Description : This file contains the routines that are         
 *                           called at the time of IGMP initialization        
 *                                                                            
 ******************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : igpfwd.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Forwarding table                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains routines related to         */
/*                            updating the forwarding entries                */
/*---------------------------------------------------------------------------*/

#include "igpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;
#endif

/*****************************************************************************/
/* Function Name      : IgpFwdUpdateFwdTable                                 */
/*                                                                           */
/* Description        : This function updates the multicast forwarding table */
/*                                                                           */
/* Input(s)           : u4GrpIpAddr      - Group IP address                  */
/*                      u4SrcIpAddr      - Source Ip address                 */
/*                      u4IfIndex        - Iif/Oif interface index           */
/*                      u2SrcIndex       - Index for source array            */
/*                      u1EventType      - CREATE/DELETE entry               */
/*                                         ADD/DELETE oif                    */
/*                                                                           */
/* Output(s)          : ppRetIgpFwdEntry - Pointer to the forwarding entry   */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpFwdUpdateFwdTable (tIgmpIPvXAddr u4GrpIpAddr, tIgmpIPvXAddr u4SrcIpAddr,
                      UINT4 u4IfIndex, UINT2 u2SrcIndex,
                      UINT1 u1EventType, tIgmpPrxyFwdEntry ** ppRetIgpFwdEntry)
{
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT4               u4Status;

    MEMSET (&IgpForwardEntry, 0, sizeof (tIgmpPrxyFwdEntry));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
               "Entering IgpFwdUpdateFwdTable()\r \n");
    IGMP_IPVX_ADDR_COPY (&(IgpForwardEntry.u4SrcIpAddr), &u4SrcIpAddr);
    IGMP_IPVX_ADDR_COPY (&(IgpForwardEntry.u4GrpIpAddr), &u4GrpIpAddr);

    switch (u1EventType)
    {
        case IGP_CREATE_FWD_ENTRY:

            pRBElem = RBTreeGet (gIgmpProxyInfo.ForwardEntry,
                                 (tRBElem *) & IgpForwardEntry);
            if (pRBElem != NULL)
            {
                pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;
            }

            /* Multicast forwarding entry not found, so create a new one */
            if (IGMP_MEM_ALLOCATE (IGP_FWD_MEMPOOL_ID,
                                   pIgpForwardEntry, tIgmpPrxyFwdEntry) == NULL)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Memory allocation for multicast forward"
                          " entry creation failed \r\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                            IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
				"for multicast forward entry\n");
                return IGMP_FAILURE;
            }

            MEMSET (pIgpForwardEntry, 0, sizeof (tIgmpPrxyFwdEntry));

            /* Update the fields of the forwarding entry */
            pIgpForwardEntry->u4IfIndex = u4IfIndex;
            IGMP_IPVX_ADDR_COPY (&(pIgpForwardEntry->u4SrcIpAddr),
                                 &u4SrcIpAddr);
            IGMP_IPVX_ADDR_COPY (&(pIgpForwardEntry->u4GrpIpAddr),
                                 &u4GrpIpAddr);
            OsixGetSysTime (&(pIgpForwardEntry->u4RouteUpTime));    /*Store the uptime */
            TMO_SLL_Init (&pIgpForwardEntry->OifList);

            IgpFwdGetOifList (u4GrpIpAddr, u2SrcIndex, pIgpForwardEntry);

            IgpUtilGetOifBitListFrmRtOifList (pIgpForwardEntry);

            pIgpForwardEntry->u1HwStatus = NP_NOT_PRESENT;
            pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
            /* Initialise the forwarding entry DB Node */
            IgmpRedDbNodeInit (&(pIgpForwardEntry->FwdDbNode),
                               IGP_RED_FWDENTRY_INFO);

            u4Status = RBTreeAdd (gIgmpProxyInfo.ForwardEntry,
                                  (tRBElem *) pIgpForwardEntry);

            if (u4Status == RB_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "RBTree Addition Failed for forwarding Entry \r\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                             IGMP_NAME,IgmpSysErrString[SYS_LOG_RB_TREE_ADD_FAIL],
				"for forwarding entry\n");
                IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
                return IGMP_FAILURE;
            }

            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                     &pIgpForwardEntry->FwdDbNode);
            IgmpRedSyncDynInfo ();
#ifdef MFWD_WANTED
            if (IgpMfwdCreateRouteEntry (pIgpForwardEntry) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Creation of forwarding entry to" " MFWD failed\r\n");
                /* Delete the Oif nodes */
                while ((pIgpOifNode = (tIgmpPrxyOif *)
                        TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
                {
                    TMO_SLL_Delete (&pIgpForwardEntry->OifList,
                                    &pIgpOifNode->NextOifNode);
                    IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
                }

                RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                              (tRBElem *) pIgpForwardEntry);
                IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
                return IGMP_FAILURE;
            }
#endif

#ifdef NPAPI_WANTED
            if (IgpNpCreateFwdEntry (pIgpForwardEntry) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Creation of forwarding entry to"
                          " hardware failed\r\n");
                /* Delete the Oif nodes */
                while ((pIgpOifNode = (tIgmpPrxyOif *)
                        TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
                {
                    TMO_SLL_Delete (&pIgpForwardEntry->OifList,
                                    (tTMO_SLL_NODE *) pIgpOifNode);
                    IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
                }

                RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                              (tRBElem *) pIgpForwardEntry);
                IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
                return IGMP_FAILURE;
            }
#endif

            pIgpForwardEntry->u1HwStatus = NP_PRESENT;
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                     &pIgpForwardEntry->FwdDbNode);
            IgmpRedSyncDynInfo ();
            /* Start forward entry timer */
            if (TmrStart (gIgmpProxyInfo.ProxyTmrListId,
                          &(pIgpForwardEntry->EntryTimer.TimerBlk),
                          IGP_FWD_ENTRY_TIMER, IGP_FWD_ENTRY_TIMER_VAL,
                          0) == TMR_FAILURE)
            {

                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Forward Entry timer start failed\r\n");
            }
            pIgpForwardEntry->u1TmrStatus = IGP_FWD_TMR_FIRST_HALF;
            gIgmpProxyInfo.u2MaxHwEntryCnt++;
            *ppRetIgpFwdEntry = pIgpForwardEntry;
            break;

        case IGP_DELETE_FWD_ENTRY:

            pRBElem = RBTreeGet (gIgmpProxyInfo.ForwardEntry,
                                 (tRBElem *) & IgpForwardEntry);

            pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

            if (pRBElem == NULL)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Multicast forwarding entry not found"
                          " for deletion\r\n");
                return IGMP_FAILURE;
            }

            /* Stop the forwarding entry timer */
            TmrStop (gIgmpProxyInfo.ProxyTmrListId,
                     &(pIgpForwardEntry->EntryTimer.TimerBlk));

            /* Delete the Oif nodes */
            while ((pIgpOifNode = (tIgmpPrxyOif *)
                    TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
            {
                TMO_SLL_Delete (&pIgpForwardEntry->OifList,
                                &pIgpOifNode->NextOifNode);
                IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
            }

            pIgpForwardEntry->u1HwStatus = NP_NOT_PRESENT;
            pIgpForwardEntry->u1RouteStatus = IGP_DELETE_ROUTE;
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                     &pIgpForwardEntry->FwdDbNode);
            IgmpRedSyncDynInfo ();
#ifdef MFWD_WANTED
            if (IgpMfwdDeleteRouteEntry (pIgpForwardEntry) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Deletion of forwarding entry to" " MFWD failed\r\n");
                return IGMP_FAILURE;
            }
#endif

#ifdef NPAPI_WANTED
            if (IgpNpDeleteFwdEntry (pIgpForwardEntry) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Deletion of forwarding entry to "
                          "hardware failed\r\n");
                return IGMP_FAILURE;
            }
#endif

            pIgpForwardEntry->u1HwStatus = NP_PRESENT;
            pIgpForwardEntry->u1RouteStatus = IGP_DELETE_ROUTE;
            IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                     &pIgpForwardEntry->FwdDbNode);
            IgmpRedSyncDynInfo ();
            RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                          (tRBElem *) pIgpForwardEntry);
            IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
            gIgmpProxyInfo.u2MaxHwEntryCnt--;
            break;

        case IGP_ADD_OIF:

            /* Get the forwarding entries and add the oif if the group address
             * match is found */
            pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

            while (pRBElem != NULL)
            {
                pRBNextElem = RBTreeGetNext (gIgmpProxyInfo.ForwardEntry,
                                             pRBElem, NULL);

                pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

                /* Check if the addition of OIF is specific to (S,G) entry 
                 * If the addition is specific to (S,G) check for the 
                 * source and group IP address. Continue if the entry
                 * does not match else add the OIF for the matched entries */
                if ((IGMP_IPVX_ADDR_COMPARE (u4GrpIpAddr, gIgpIPvXZero) != 0) &&
                    (IGMP_IPVX_ADDR_COMPARE (u4SrcIpAddr, gIgpIPvXZero) != 0))
                {
                    if ((IGMP_IPVX_ADDR_COMPARE
                         ((pIgpForwardEntry->u4GrpIpAddr),
                          u4GrpIpAddr) != IGMP_ZERO)
                        &&
                        (IGMP_IPVX_ADDR_COMPARE
                         ((pIgpForwardEntry->u4SrcIpAddr),
                          u4SrcIpAddr) != IGMP_ZERO))
                    {
                        pRBElem = pRBNextElem;
                        continue;
                    }
                }
                /* Check if the addition of OIF is specific to (*,G) entry 
                 * If the addition is specific to (*,G) check for the 
                 * group IP address alone and continue if the entry
                 * does not match else add the OIF from the matched entries*/
                else if ((IGMP_IPVX_ADDR_COMPARE (u4GrpIpAddr, gIgpIPvXZero) !=
                          0)
                         && (IGMP_IPVX_ADDR_COMPARE (u4SrcIpAddr, gIgpIPvXZero)
                             == 0))

                {
                    if (IGMP_IPVX_ADDR_COMPARE (pIgpForwardEntry->u4GrpIpAddr,
                                                u4GrpIpAddr) != IGMP_ZERO)
                    {
                        pRBElem = pRBNextElem;
                        continue;
                    }
                }

                /* Update forward table is called for the addition of 
                 * Upstream interface to the OIF list if the above checks
                 * fail so add the interface to the OIF list of all
                 * the forwarding entries */

                /* If the OIF is equal to IIF dont add the OIF */
                if (pIgpForwardEntry->u4IfIndex == u4IfIndex)
                {
                    pRBElem = pRBNextElem;
                    continue;
                }

                /* Add the Oif to the existing forwarding entry  and update 
                 * hardware */
                if (IgpFwdUpdateOifNode (pIgpForwardEntry, u4IfIndex,
                                         IGP_ADD_OIF) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Addition of Oif to existing route"
                              " entry failed\r\n");
                    return IGMP_FAILURE;
                }

                pIgpForwardEntry->u1HwStatus = NP_NOT_PRESENT;
                pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgpForwardEntry->FwdDbNode);
                IgmpRedSyncDynInfo ();
#ifdef MFWD_WANTED
                if (IgpMfwdAddOif (pIgpForwardEntry, u4IfIndex) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Addiion of Oif to existing route"
                              " entry in MFWD failed\r\n");
                    IgpFwdUpdateOifNode (pIgpForwardEntry, u4IfIndex,
                                         IGP_DEL_OIF);
                    return IGMP_FAILURE;
                }
#endif

#ifdef NPAPI_WANTED
                if (IgpNpAddOif (pIgpForwardEntry, u4IfIndex) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Addiion of Oif to existing route"
                              " entry in hardware failed\r\n");
                    IgpFwdUpdateOifNode (pIgpForwardEntry, u4IfIndex,
                                         IGP_DEL_OIF);
                    return IGMP_FAILURE;
                }
#endif
                pIgpForwardEntry->u1HwStatus = NP_PRESENT;
                pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgpForwardEntry->FwdDbNode);
                IgmpRedSyncDynInfo ();
                pRBElem = pRBNextElem;
            }

            break;

        case IGP_DEL_OIF:

            pRBElem = RBTreeGet (gIgmpProxyInfo.ForwardEntry,
                                 (tRBElem *) & IgpForwardEntry);

            pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

            if (pRBElem == NULL)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Multicast forwarding entry not found"
                          " for deletion\r\n");
                return IGMP_FAILURE;
            }

            /* Delete the Oif to the existing forwarding entry and update 
             * hardware */
            if (IgpFwdUpdateOifNode (pIgpForwardEntry, u4IfIndex,
                                     IGP_DEL_OIF) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Deletion of Oif to existing route"
                          " entry failed\r\n");
                return IGMP_FAILURE;
            }

            /* Oif List is empty directly delete the entry from the 
             * multicast forwarding data base */
            if (TMO_SLL_Count (&(pIgpForwardEntry->OifList)) == IGMP_ZERO)
            {
                pIgpForwardEntry->u1HwStatus = NP_NOT_PRESENT;
                pIgpForwardEntry->u1RouteStatus = IGP_DELETE_ROUTE;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgpForwardEntry->FwdDbNode);
                IgmpRedSyncDynInfo ();
#ifdef MFWD_WANTED
                if (IgpMfwdDeleteRouteEntry (pIgpForwardEntry) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Deletion of forwarding entry to"
                              " MFWD failed\r\n");
                    return IGMP_FAILURE;
                }
#endif
#ifdef NPAPI_WANTED
                if (IgpNpDeleteFwdEntry (pIgpForwardEntry) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Deletion of forwarding entry to "
                              "hardware failed\r\n");
                    return IGMP_FAILURE;
                }
#endif
                pIgpForwardEntry->u1HwStatus = NP_PRESENT;
                pIgpForwardEntry->u1RouteStatus = IGP_DELETE_ROUTE;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgpForwardEntry->FwdDbNode);
                IgmpRedSyncDynInfo ();
                /* Stop the forwarding entry timer */
                TmrStop (gIgmpProxyInfo.ProxyTmrListId,
                         &(pIgpForwardEntry->EntryTimer.TimerBlk));

                RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                              (tRBElem *) pIgpForwardEntry);
                IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
                gIgmpProxyInfo.u2MaxHwEntryCnt--;
            }
            else
            {
                pIgpForwardEntry->u1HwStatus = NP_NOT_PRESENT;
                pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgpForwardEntry->FwdDbNode);
                IgmpRedSyncDynInfo ();
                /* Delete the OIF alone fron the data plane */
#ifdef MFWD_WANTED
                if (IgpMfwdDelOif (pIgpForwardEntry, u4IfIndex) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Deletion of Oif to existing route"
                              " entry in MFWD failed\r\n");
                    return IGMP_FAILURE;
                }
#endif
#ifdef NPAPI_WANTED
                if (IgpNpDeleteOif (pIgpForwardEntry,
                                    u4IfIndex) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                              "Deletion of Oif to existing route"
                              " entry in hardware failed\r\n");
                    return IGMP_FAILURE;
                }
#endif
                pIgpForwardEntry->u1HwStatus = NP_PRESENT;
                pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
                IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                         &pIgpForwardEntry->FwdDbNode);
                IgmpRedSyncDynInfo ();
            }
            break;

        default:
            break;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdUpdateFwdTable()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpFwdGetForwardEntry                                */
/*                                                                           */
/* Description        : This function gets the multicast forwarding entry    */
/*                                                                           */
/* Input(s)           : u4GrpAddr  - Group IP address                        */
/*                      u4SrcAddr  - Source IP address                       */
/*                                                                           */
/* Output(s)          : pointer to the forwarding entry or NULL              */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpFwdGetForwardEntry (tIgmpIPvXAddr u4GrpAddr, tIgmpIPvXAddr u4SrcAddr,
                       tIgmpPrxyFwdEntry ** ppRetIgpFwdEntry)
{
    tRBElem            *pRBElem = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;

    MEMSET (&IgpForwardEntry, 0, sizeof (tIgmpPrxyFwdEntry));
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdGetForwardEntry()\r \n");
    IGMP_IPVX_ADDR_COPY (&(IgpForwardEntry.u4SrcIpAddr), &u4SrcAddr);
    IGMP_IPVX_ADDR_COPY (&(IgpForwardEntry.u4GrpIpAddr), &u4GrpAddr);

    pRBElem =
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pRBElem == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Multicast forwarding Entry not found\r\n");
        return IGMP_FAILURE;
    }
    else
    {
        *ppRetIgpFwdEntry = (tIgmpPrxyFwdEntry *) pRBElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdGetForwardEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpFwdDeleteFwdEntries                               */
/*                                                                           */
/* Description        : This function deletes the multicast forwarding       */
/*                      entries                                              */
/*                                                                           */
/* Input(s)           : u4GrpAddr - Group IP address                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpFwdDeleteFwdEntries (tIgmpIPvXAddr u4GrpAddr)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdDeleteFwdEntries()\r \n");
    if (gIgmpProxyInfo.ForwardEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "No Multicasting forwarding entries are not present\r\n");
        return;
    }

    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    /* Scan thro the forwarding entries */
    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        if ((IGMP_IPVX_ADDR_COMPARE (u4GrpAddr, gIgpIPvXZero) != IGMP_ZERO) &&
            (IGMP_IPVX_ADDR_COMPARE (pIgpForwardEntry->u4GrpIpAddr, u4GrpAddr)
             != IGMP_ZERO))
        {
            pRBElem = pRBNextElem;
            continue;
        }

        /* Stop the Forwarding entry timer  */
        TmrStop (gIgmpProxyInfo.ProxyTmrListId,
                 &(pIgpForwardEntry->EntryTimer.TimerBlk));

        /* Delete the Oif nodes */
        while ((pIgpOifNode = (tIgmpPrxyOif *)
                TMO_SLL_First (&pIgpForwardEntry->OifList)) != NULL)
        {
            TMO_SLL_Delete (&pIgpForwardEntry->OifList,
                            &pIgpOifNode->NextOifNode);
            IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
        }

        pIgpForwardEntry->u1HwStatus = NP_NOT_PRESENT;
        pIgpForwardEntry->u1RouteStatus = IGP_DELETE_ROUTE;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &pIgpForwardEntry->FwdDbNode);
        IgmpRedSyncDynInfo ();

#ifdef MFWD_WANTED
        if (IgpMfwdDeleteRouteEntry (pIgpForwardEntry) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Deletion of forwarding entry to MFWD failed\r\n");
            return;
        }
#endif

#ifdef NPAPI_WANTED
        if (IgpNpDeleteFwdEntry (pIgpForwardEntry) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Deletion of forwarding entry to hardware failed\r\n");
            return;
        }
#endif
        pIgpForwardEntry->u1HwStatus = NP_PRESENT;
        pIgpForwardEntry->u1RouteStatus = IGP_DELETE_ROUTE;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &pIgpForwardEntry->FwdDbNode);
        IgmpRedSyncDynInfo ();
        RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                      (tRBElem *) pIgpForwardEntry);

        IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);

        gIgmpProxyInfo.u2MaxHwEntryCnt--;

        pRBElem = pRBNextElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdDeleteFwdEntries()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpFwdGetOifList                                     */
/*                                                                           */
/* Description        : This function will update the Oiflist of the         */
/*                      multicast forwarding entry                           */
/*                                                                           */
/* Input(s)           : u4GrpAddr    - Group IP address                      */
/*                      u2SrcIndex   - Source index to the array             */
/*                      pIgpForwardEntry - Pointer to the forwarding entry   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpFwdGetOifList (tIgmpIPvXAddr u4GrpAddr, UINT2 u2SrcIndex,
                  tIgmpPrxyFwdEntry * pIgpForwardEntry)
{
    tTMO_SLL_NODE      *pIfGetNextLink = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    tIgmpGroup         *pIgmpGrpEntry = NULL;
    BOOL1               bResult = OSIX_FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdGetOifList()\r \n");
    /* Scan thro the interfaces for the group records */
    pIfGetNextLink = TMO_SLL_First (&gIgmpIfInfo.IfGetNextList);

    while (pIfGetNextLink != NULL)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink,
                                        pIfGetNextLink);

        pIfGetNextLink = TMO_SLL_Next (&gIgmpIfInfo.IfGetNextList,
                                       &(pIfaceNode->IfGetNextLink));

        /* If the interface is configured as upstream interface add the
         * interface directly checking for IIF != OIF */
        if ((IgpUtilCheckUpIface (pIfaceNode->u4IfIndex) == IGMP_TRUE) &&
            (pIgpForwardEntry->u4IfIndex != pIfaceNode->u4IfIndex))
        {
            IgpFwdUpdateOifNode (pIgpForwardEntry, pIfaceNode->u4IfIndex,
                                 IGP_ADD_OIF);
        }
        else
        {
            /* Get the group entry for the given group address */
            pIgmpGrpEntry = IgmpGrpLookUp (pIfaceNode, u4GrpAddr);

            if (pIgmpGrpEntry == NULL)
            {
                /* Group entry does not exists for the 
                 * specified group address */
                continue;
            }
            /* If the group record exists for the group and filter mode is 
             * INCLUDE add the interface to the Oif List if the source 
             * is present in the include bitmap for the group record */
            if (pIgmpGrpEntry->u1FilterMode == IGMP_FMODE_INCLUDE)
            {
                OSIX_BITLIST_IS_BIT_SET (pIgmpGrpEntry->InclSrcBmp,
                                         u2SrcIndex, IGP_SRC_LIST_SIZE,
                                         bResult);

                if (bResult == OSIX_TRUE)
                {
                    IgpFwdUpdateOifNode (pIgpForwardEntry,
                                         pIfaceNode->u4IfIndex, IGP_ADD_OIF);
                }
            }
            else
            {
                /* If the group record exists for the group and filter mode is 
                 * EXCLUDE add the interface to the OIF List if the source 
                 * is not present in the global source array, but registration 
                 * (either IGMPv1/v2 or EXCLUDE registration for some other 
                 * source) has been received on this interface */
                if (u2SrcIndex == 0xffff)
                {
                    IgpFwdUpdateOifNode (pIgpForwardEntry,
                                         pIfaceNode->u4IfIndex, IGP_ADD_OIF);
                }
                else
                {
                    /* If the source information is present add the interface 
                     * to the OIF list if there is no EXCLUDE registration 
                     * received for this source on this interface */

                    OSIX_BITLIST_IS_BIT_SET (pIgmpGrpEntry->ExclSrcBmp,
                                             u2SrcIndex,
                                             IGP_SRC_LIST_SIZE, bResult);

                    if (bResult == OSIX_FALSE)
                    {
                        IgpFwdUpdateOifNode (pIgpForwardEntry,
                                             pIfaceNode->u4IfIndex,
                                             IGP_ADD_OIF);
                    }
                }
            }
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdGetOifList()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpFwdUpdateOifNode                                  */
/*                                                                           */
/* Description        : This function will add/delete Oif for the existing   */
/*                      multicast forwarding entry                           */
/*                                                                           */
/* Input(s)           : pIgpForwardEntry - pointer to the forwarding entry   */
/*                      u4OifIndex   -  Outgoing interface index             */
/*                      u1Action     - ADD/DEL Oif                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpFwdUpdateOifNode (tIgmpPrxyFwdEntry * pIgpForwardEntry,
                     UINT4 u4OifIndex, UINT1 u1Action)
{
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tIgmpPrxyOif       *pIgpPrevOifNode = NULL;
    UINT1               u1OifFound = IGMP_FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdUpdateOifNode()\r \n");
    if (u1Action == IGP_ADD_OIF)
    {
        /* Oif Entry not found so allocate a node and add to the Oif List in
         * the existing forwarding entry */
        TMO_SLL_Scan (&(pIgpForwardEntry->OifList), pIgpOifNode, tIgmpPrxyOif *)
        {
            if (pIgpOifNode->u4IfIndex < u4OifIndex)
            {
                pIgpPrevOifNode = pIgpOifNode;
            }
            else if (pIgpOifNode->u4IfIndex == u4OifIndex)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Oif is already present in the Oif list of "
                          " forwarding entry\r\n");
                return IGMP_SUCCESS;
            }
            else
            {
                break;
            }
        }
        if (IGMP_MEM_ALLOCATE (IGP_OIF_MEMPOOL_ID,
                               pIgpOifNode, tIgmpPrxyOif) == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Memory allocation for Oif node failed\r\n");
	    SYSLOG_IGMP_MSG (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                           IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
			"for Oif node %d\n", pIgpOifNode->u4IfIndex);
            return IGMP_FAILURE;
        }

        TMO_SLL_Init_Node (&pIgpOifNode->NextOifNode);
        pIgpOifNode->u4IfIndex = u4OifIndex;
        OsixGetSysTime (&(pIgpOifNode->u4UpTime));    /*Store the uptime */

        TMO_SLL_Insert (&(pIgpForwardEntry->OifList),
                        &pIgpPrevOifNode->NextOifNode,
                        &pIgpOifNode->NextOifNode);
        /*update the Oif Portlist with input u4OifIndex */
        OSIX_BITLIST_SET_BIT ((pIgpForwardEntry->McFwdPortList),
                              (u4OifIndex + 1), sizeof (tPortListExt));
    }
    else                        /* Called for deletion of Oif from the forwarding entry */
    {
        TMO_SLL_Scan (&(pIgpForwardEntry->OifList), pIgpOifNode, tIgmpPrxyOif *)
        {
            if (pIgpOifNode->u4IfIndex == u4OifIndex)
            {
                u1OifFound = IGMP_TRUE;
                break;
            }
        }

        /*update the Oif Portlist with input u4OifIndex */
        OSIX_BITLIST_RESET_BIT ((pIgpForwardEntry->McFwdPortList),
                                (u4OifIndex + 1), sizeof (tPortListExt));
        if (u1OifFound == IGMP_TRUE)
        {
            /* Oif Entry found so delete the Oif Node from the Oif List of
             * the existing forwarding entry */
            TMO_SLL_Delete (&(pIgpForwardEntry->OifList),
                            &pIgpOifNode->NextOifNode);
            IGP_RELEASE_MEM_BLOCK (IGP_OIF_MEMPOOL_ID, pIgpOifNode);
        }
        else
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Oif is not present in the Oif list of "
                      " forwarding entry\r\n");
            return IGMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdUpdateOifNode()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpFwdUpIfEntryDeletion                              */
/*                                                                           */
/* Description        : This function will update the forwarding table for   */
/*                      upstream interface deletion                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Upstream interface index                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpFwdUpIfEntryDeletion (UINT4 u4IfIndex)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdUpIfEntryDeletion()\r \n");
    if (gIgmpProxyInfo.ForwardEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "No Multicasting forwarding entries are not present\r\n");
        return;
    }

    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    /* Scan thro the forwarding entries */
    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        /* If the upstream interface index matches the IIF delete
         * the forwarding entry */
        if (pIgpForwardEntry->u4IfIndex == u4IfIndex)
        {
            if (IgpFwdUpdateFwdTable (pIgpForwardEntry->u4GrpIpAddr,
                                      pIgpForwardEntry->u4SrcIpAddr,
                                      pIgpForwardEntry->u4IfIndex,
                                      IGMP_ZERO, IGP_DELETE_FWD_ENTRY,
                                      NULL) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Multicast forward deletion for upstream interface"
                          " deletion failed\r\n");
            }
        }
        else
        {
            /* If the upstream interface index does not match the IIF delete
             * Upstream interface from OIF list of the forwarding entry */
            if (IgpFwdUpdateFwdTable (pIgpForwardEntry->u4GrpIpAddr,
                                      pIgpForwardEntry->u4SrcIpAddr,
                                      u4IfIndex, IGMP_ZERO, IGP_DEL_OIF,
                                      NULL) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                          "Multicast forward deletion for upstream interface"
                          " deletion failed\r\n");
            }
        }

        pRBElem = pRBNextElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdUpIfEntryDeletion()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpFwdSummarizeForwardTable                          */
/*                                                                           */
/* Description        : This function will summarize the forwarding table    */
/*                                                                           */
/* Input(s)           : u4GrpIpAddr - Multicast group IP address             */
/*                      u1FilterMode - Filter Mode                           */
/*                      InclSrcBmp   - INCLUDE source bitmap                 */
/*                      ExclSrcBmp   - EXCLUDE source bitmap                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpFwdSummarizeForwardTable (tIgmpPrxyGrpInfo * pIgpGroupEntry,
                             tIgmpGroup * pIgmpGroupEntry)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT2               u2SrcIndex = 0;
    BOOL1               bResult = IGMP_FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdSummarizeForwardTable()\r \n");
    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        if (IGMP_IPVX_ADDR_COMPARE (pIgpForwardEntry->u4GrpIpAddr,
                                    pIgpGroupEntry->u4GrpIpAddr) != IGMP_ZERO)
        {
            pRBElem = pRBNextElem;
            continue;
        }

        /* If entry is found get the source index for the source 
         * IP address */
        IGP_IS_SOURCE_PRESENT (pIgpForwardEntry->u4SrcIpAddr, bResult,
                               u2SrcIndex);

        /* If the source index is not equal to 0xffff */
        if (u2SrcIndex != 0xffff)
        {
            u2SrcIndex = (UINT2) (u2SrcIndex + 1);
            /* Filter Mode is include */
            if (pIgpGroupEntry->u1FilterMode == IGMP_FMODE_INCLUDE)
            {
                /* If the source is not present in the INCLUDE 
                 * Bitmap delete the forwarding entry */
                OSIX_BITLIST_IS_BIT_SET (pIgpGroupEntry->InclSrcBmp, u2SrcIndex,
                                         IGP_SRC_LIST_SIZE, bResult);
                if (bResult == OSIX_FALSE)
                {
                    IgpFwdUpdateFwdTable (pIgpForwardEntry->u4GrpIpAddr,
                                          pIgpForwardEntry->u4SrcIpAddr,
                                          pIgpForwardEntry->u4IfIndex,
                                          u2SrcIndex, IGP_DELETE_FWD_ENTRY,
                                          NULL);

                    pRBElem = pRBNextElem;
                    continue;
                }
            }
            else
            {
                /* If the source is present in the EXCLUDE 
                 * Bitmap delete the forwarding entry */
                OSIX_BITLIST_IS_BIT_SET (pIgpGroupEntry->ExclSrcBmp, u2SrcIndex,
                                         IGP_SRC_LIST_SIZE, bResult);
                if (bResult == OSIX_TRUE)
                {
                    IgpFwdUpdateFwdTable (pIgpForwardEntry->u4GrpIpAddr,
                                          pIgpForwardEntry->u4SrcIpAddr,
                                          pIgpForwardEntry->u4IfIndex,
                                          u2SrcIndex, IGP_DELETE_FWD_ENTRY,
                                          NULL);

                    pRBElem = pRBNextElem;
                    continue;
                }
            }
        }
        else
        {
            /* Source index is 0xffff */
            if (pIgpGroupEntry->u1FilterMode == IGMP_FMODE_INCLUDE)
            {
                IgpFwdUpdateFwdTable (pIgpForwardEntry->u4GrpIpAddr,
                                      pIgpForwardEntry->u4SrcIpAddr,
                                      pIgpForwardEntry->u4IfIndex,
                                      u2SrcIndex, IGP_DELETE_FWD_ENTRY, NULL);

                pRBElem = pRBNextElem;
                continue;
            }
        }

        /* Update the outgoing interface list */
        IgpFwdSummarizeOifList (pIgmpGroupEntry, pIgpForwardEntry, u2SrcIndex);

        pRBElem = pRBNextElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdSummarizeForwardTable()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpFwdSummarizeOifList                               */
/*                                                                           */
/* Description        : This function will update the Outgoing interface list*/
/*                      of given forward entry according to the IGMP group   */
/*                      entry given                                          */
/*                                                                           */
/* Input(s)           : pIgmpGroupEntry - pointer to IGMP group entry        */
/*                      pIgpForwardEntry - pointer to forwarding entry       */
/*                      u4SrcIndex   - Index of source in global source array*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpFwdSummarizeOifList (tIgmpGroup * pIgmpGroupEntry,
                        tIgmpPrxyFwdEntry * pIgpForwardEntry, UINT2 u2SrcIndex)
{
    UINT4               u4OifIndex = IGMP_ZERO;
    UINT1               u1Flag;
    BOOL1               bResult = IGMP_FALSE;

    u4OifIndex = pIgmpGroupEntry->pIfNode->u4IfIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdSummarizeOifList()\r \n");
    if (u2SrcIndex != 0xffff)
    {
        if (pIgmpGroupEntry->u1FilterMode == IGMP_FMODE_INCLUDE)
        {
            /* If filter mode is include, and the bit is set, it means,
             * the interface needs the (S, G) entry */
            OSIX_BITLIST_IS_BIT_SET (pIgmpGroupEntry->InclSrcBmp, u2SrcIndex,
                                     IGP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_FALSE)
            {
                u1Flag = IGP_DEL_OIF;
            }
            else
            {
                u1Flag = IGP_ADD_OIF;
            }
        }
        else
        {
            /* If filter mode is exclude, and the bit is set, it means,
             * the interface does NOT needs the (S, G) entry */
            OSIX_BITLIST_IS_BIT_SET (pIgmpGroupEntry->ExclSrcBmp, u2SrcIndex,
                                     IGP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u1Flag = IGP_DEL_OIF;
            }
            else
            {
                u1Flag = IGP_ADD_OIF;
            }
        }
    }
    else
    {
        /* if the source is not in the global array, then the interface will 
         * be needing the (S,G) entry, only if the filter mode is exclude */
        if (pIgmpGroupEntry->u1FilterMode == IGMP_FMODE_INCLUDE)
        {
            u1Flag = IGP_DEL_OIF;
        }
        else
        {
            u1Flag = IGP_ADD_OIF;
        }
    }

    /* Add or delete from outgoing interface list */
    if (IgpFwdUpdateOifNode (pIgpForwardEntry,
                             u4OifIndex, u1Flag) == IGMP_FAILURE)
    {
        return;
    }

    pIgpForwardEntry->u1HwStatus = NP_NOT_PRESENT;
    pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
    IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList, &pIgpForwardEntry->FwdDbNode);
    IgmpRedSyncDynInfo ();
    /* If Oif List is empty directly delete the entry from the
     * multicast forwarding data base */
    if (TMO_SLL_Count (&(pIgpForwardEntry->OifList)) == IGMP_ZERO)
    {
#ifdef MFWD_WANTED
        if (IgpMfwdDeleteRouteEntry (pIgpForwardEntry) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Deletion of forwarding entry to" " MFWD failed\r\n");
        }
#endif

#ifdef NPAPI_WANTED
        if (IgpNpDeleteFwdEntry (pIgpForwardEntry) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Deletion of forwarding entry to " "hardware failed\r\n");
        }
#endif
        pIgpForwardEntry->u1HwStatus = NP_PRESENT;
        pIgpForwardEntry->u1RouteStatus = IGP_DELETE_ROUTE;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &pIgpForwardEntry->FwdDbNode);
        IgmpRedSyncDynInfo ();
        /* Stop the forwarding entry timer */
        TmrStop (gIgmpProxyInfo.ProxyTmrListId,
                 &(pIgpForwardEntry->EntryTimer.TimerBlk));

        RBTreeRemove (gIgmpProxyInfo.ForwardEntry,
                      (tRBElem *) pIgpForwardEntry);
        IGP_RELEASE_MEM_BLOCK (IGP_FWD_MEMPOOL_ID, pIgpForwardEntry);
        gIgmpProxyInfo.u2MaxHwEntryCnt--;
    }
    else if (u1Flag == IGP_ADD_OIF)
    {
#ifdef MFWD_WANTED
        if (IgpMfwdAddOif (pIgpForwardEntry, u4OifIndex) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Addiion of Oif to existing route"
                      " entry in MFWD failed\r\n");
        }
#endif

#ifdef NPAPI_WANTED
        if (IgpNpAddOif (pIgpForwardEntry, u4OifIndex) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Addiion of Oif to existing route"
                      " entry in hardware failed\r\n");
        }
        pIgpForwardEntry->u1HwStatus = NP_PRESENT;
        pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &pIgpForwardEntry->FwdDbNode);
        IgmpRedSyncDynInfo ();
#endif
    }
    else
    {
#ifdef MFWD_WANTED
        if (IgpMfwdDelOif (pIgpForwardEntry, u4OifIndex) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Deletion of Oif to existing route"
                      " entry in MFWD failed\r\n");
        }
#endif

#ifdef NPAPI_WANTED
        if (IgpNpDeleteOif (pIgpForwardEntry, u4OifIndex) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Deletion of Oif to existing route"
                      " entry in hardware failed\r\n");
        }
        pIgpForwardEntry->u1HwStatus = NP_PRESENT;
        pIgpForwardEntry->u1RouteStatus = IGP_ADD_ROUTE;
        IgmpRedDbUtilAddTblNode (&gIgmpDynInfoList,
                                 &pIgpForwardEntry->FwdDbNode);
        IgmpRedSyncDynInfo ();
#endif
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdSummarizeOifList()\r \n");
    return;
}

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : IgpFwdHandleMbsmEvent                                */
/*                                                                           */
/* Description        : This function will add all the multicast forwarding  */
/*                      entries in the new line card inserted                */
/*                                                                           */
/* Input(s)           : pSlotInfo - pointer to the Line card information     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpFwdHandleMbsmEvent (tMbsmSlotInfo * pSlotInfo)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tIgmpPrxyFwdEntry  *pIgpFwdEntry = NULL;
#ifdef NPAPI_WANTED
    tMcRtEntry          rtEntry;
    tMcDownStreamIf    *pDsIf = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tIgmpIface         *pIfNode = NULL;
    tIgmpPrxyOif       *pOifNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4OifCnt = 0;
    UINT4               u4GrpIpAddr = 0;
    UINT4               u4SrcIpAddr = 0;
#endif
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpFwdHandleMbsmEvent()\r \n");
    if (gIgmpProxyInfo.ForwardEntry == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IGMP,
                  "No Multicasting forwarding entries are present\r\n");
        return IGMP_SUCCESS;
    }

    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    /* Scan thro the forwarding entries and installl all the entries 
     * in the new Line card inserted */
    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpFwdEntry = (tIgmpPrxyFwdEntry *) pRBElem;

#ifdef NPAPI_WANTED
        MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));

        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

        /* Pass the CFA ifindex to NP, ir-respective of whether the interface
         * exists or not. This will be used in case of older NPAPI, in which  
         * the input CFA ifindex is used to retrieve the VLAN id. 
         */

        IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpFwdEntry->u4IfIndex, &u4IfIndex);
        rtEntry.u2RpfIf = (UINT2) u4IfIndex;

        if (IgpUtilGetVlanIdFromIfIndex (pIgpFwdEntry->u4IfIndex,
                                         &rtEntry.u2VlanId) == IGMP_FAILURE)
        {
            return IGMP_FAILURE;
        }

        /* Get the portlist for incoming VLAN id from IGS or VLAN
         * module */
        if (IgpNpGetMcastFwdPortList (pIgpFwdEntry->u4GrpIpAddr,
                                      pIgpFwdEntry->u4SrcIpAddr,
                                      pIgpFwdEntry->u4IfIndex,
                                      rtEntry.u2VlanId,
                                      rtEntry.McFwdPortList,
                                      rtEntry.UntagPortList) != IGMP_SUCCESS)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                           "Failure in getting the portlist for incoming VLAN %d\r\n",
                           rtEntry.u2VlanId);
            return IGMP_FAILURE;
        }

        if (TMO_SLL_Count (&(pIgpFwdEntry->OifList)) != 0)
        {
            pDsIf = MEM_MALLOC ((sizeof (tMcDownStreamIf) *
                                 TMO_SLL_Count (&(pIgpFwdEntry->OifList))),
                                tMcDownStreamIf);
            if (pDsIf == NULL)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                          "Failure allocating memory for installing entry in NP\r\n");
                return IGMP_FAILURE;
            }
        }

        pTmpDsIf = pDsIf;
        TMO_SLL_Scan (&(pIgpFwdEntry->OifList), pOifNode, tIgmpPrxyOif *)
        {
            /* If the OIF is to incoming interface continue
             * with the next in the list */
            if (pIgpFwdEntry->u4IfIndex == pOifNode->u4IfIndex)
            {
                continue;
            }

            IGMP_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4IfIndex,
                                           &pTmpDsIf->u4IfIndex);

            pIfNode =
                IGMP_GET_IF_NODE (pOifNode->u4IfIndex, IPVX_ADDR_FMLY_IPV4);
            pTmpDsIf->u2TtlThreshold = 255;
            pTmpDsIf->u4Mtu = pIfNode->u4Mtu;

            if (IgpUtilGetVlanIdFromIfIndex (pOifNode->u4IfIndex,
                                             &pTmpDsIf->u2VlanId) ==
                IGMP_FAILURE)
            {
                MEM_FREE (pDsIf);
                return IGMP_FAILURE;
            }

            if (IgpNpGetMcastFwdPortList (pIgpFwdEntry->u4GrpIpAddr,
                                          pIgpFwdEntry->u4SrcIpAddr,
                                          pIgpFwdEntry->u4IfIndex,
                                          pTmpDsIf->u2VlanId,
                                          pTmpDsIf->McFwdPortList,
                                          pTmpDsIf->UntagPortList) !=
                IGMP_SUCCESS)
            {
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                               "Failure in getting the portlist for outgoing VLAN %d\r\n",
                               pTmpDsIf->u2VlanId);
                MEM_FREE (pDsIf);
                return IGMP_FAILURE;
            }

            pTmpDsIf++;
            u4OifCnt++;
        }

        IGMP_IP_COPY_FROM_IPVX (&u4GrpIpAddr, pIgpFwdEntry->u4GrpIpAddr,
                                IPVX_ADDR_FMLY_IPV4);
        IGMP_IP_COPY_FROM_IPVX (&u4SrcIpAddr, pIgpFwdEntry->u4SrcIpAddr,
                                IPVX_ADDR_FMLY_IPV4);
        if ((IpmcFsNpIpv4MbsmMcAddRouteEntry (IGMP_PROXY_ID,
                                          u4GrpIpAddr,
                                          FNP_ALL_BITS_SET,
                                          u4SrcIpAddr,
                                          FNP_ALL_BITS_SET, IPMC_MRP, rtEntry,
                                          u4OifCnt, pDsIf,
                                          pSlotInfo)) == FNP_FAILURE)
        {
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                           "Failure in installing IPMC entry in NP for"
                           " (0x%x, 0x%x)\r\n", pIgpFwdEntry->u4SrcIpAddr,
                           pIgpFwdEntry->u4GrpIpAddr);
            MEM_FREE (pDsIf);
            return IGMP_FAILURE;
        }
        else
        {
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                           "Installed entry (0x%x, 0x%x) in NP\r\n",
                           pIgpFwdEntry->u4SrcIpAddr,
                           pIgpFwdEntry->u4GrpIpAddr);
        }

        if (pDsIf != NULL)
        {
            MEM_FREE (pDsIf);
        }
#endif
        pRBElem = pRBNextElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpFwdHandleMbsmEvent()\r \n");
    return IGMP_SUCCESS;
}
#endif
