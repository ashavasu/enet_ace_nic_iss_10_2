#ifndef _IGPCLI_C
#define _IGPCLI_C

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved     */
/* Licensee Aricent Inc., 2006                              */
/* $Id: igpcli.c,v 1.18 2016/04/04 10:00:27 siva Exp $   */
/*****************************************************************************/
/*    FILE  NAME            : igpcli.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    MODULE NAME           : IGMP Proxy                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains the CLI routines            */
/*                            for IGMP Proxy module                          */
/*---------------------------------------------------------------------------*/

#include "igpinc.h"
#include "igpcli.h"
#include "igpclipt.h"
#include "fsigpwr.h"
#include "fsigpcli.h"
/*****************************************************************************/
/*     FUNCTION NAME    : cli_process_igp_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for IGMP Proxy module as  */
/*                        defined in igpcmd.def                              */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
INT4
cli_process_igp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4Inst = 0;
    INT4                i4RetStatus = CLI_SUCCESS;

    va_start (ap, u4Command);

    if ((IGMP_PROXY_STATUS () != IGMP_ENABLE) &&
        (u4Command != IGMP_PROXY_STATUS_ENABLE) &&
        (u4Command != IGMP_PROXY_STATUS_DISABLE))
    {
        CliPrintf (CliHandle, "\r %% IGMP Proxy is disabled in the system\r\n");
        va_end (ap);
        return CLI_FAILURE;
    }

    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }
    else
    {
        u4IfIndex = CLI_GET_IFINDEX ();
    }

    /* Walk through the rest of the arguements and store in args array.
     * Store CLI_MAX_ARGS arguements at the max. */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();
    CLI_SET_ERR (IGMP_ZERO);

    switch (u4Command)
    {
        case IGMP_PROXY_STATUS_ENABLE:

            i4RetStatus =
                IgpCliSetModuleStatus (CliHandle, IGMP_PROXY_STATUS_ENABLE);
            break;

        case IGMP_PROXY_STATUS_DISABLE:

            i4RetStatus =
                IgpCliSetModuleStatus (CliHandle, IGMP_PROXY_STATUS_DISABLE);
            break;

        case IGMP_PROXY_CLI_UPIFACE:

            /* args[0] will have the configuration constant ENABLE/DISABLE */
            i4RetStatus =
                IgpCliUpIfConfigure (CliHandle, u4IfIndex,
                                     CLI_PTR_TO_I4 (args[0]));
            break;

        case IGMP_PROXY_CLI_UPIFACE_INT:

            /* args[0] will be pointing to the upstream interface 
             * purge interval */
            i4RetStatus =
                IgpCliConfigureUpIfPurgeInterval (CliHandle, u4IfIndex,
                                                  (INT4) (*args[0]));
            break;

        case IGMP_PROXY_CLI_VERSION:

            /* args[0] will be pointing to the upstream interface 
             * purge interval */
            i4RetStatus =
                IgpCliConfigureUpIfVersion (CliHandle, u4IfIndex,
                                            CLI_PTR_TO_I4 (args[0]));
            break;

        case IGMP_PROXY_CLI_SHOW_UPIFACE:
            /* args[0] will be pointing to the upstream interface index */
            if (args[0] != NULL)
            {
                i4RetStatus =
                    IgpCliShowUpIfTable (CliHandle, CLI_PTR_TO_I4 (args[0]));
            }
            else
            {
                i4RetStatus = IgpCliShowUpIfTable (CliHandle, IGMP_ZERO);
            }
            break;

        case IGMP_PROXY_CLI_SHOW_MROUTES:
            /* args[0] will be pointing to the upstream interface index or
             * Group or source address */
            /* args[1] will be pointing to the show flag */
            if (args[0] == NULL)
            {
                i4RetStatus = IgpCliShowMrouteTable (CliHandle, IGMP_ZERO,
                                                     IGMP_ZERO, IGMP_ZERO);
            }
            else if (CLI_PTR_TO_I4 (args[1]) == IGMP_PROXY_CLI_SHOW_MROUTES_VID)
            {
                i4RetStatus =
                    IgpCliShowMrouteTable (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                           IGMP_ZERO,
                                           IGMP_PROXY_CLI_SHOW_MROUTES_VID);
            }
            else
            {
                u4IpAddr = *(UINT4 *) (args[0]);

                if (CLI_PTR_TO_I4 (args[1]) == IGMP_PROXY_CLI_SHOW_MROUTES_GRP)
                {
                    IgpCliShowMrouteTable (CliHandle, IGMP_ZERO, u4IpAddr,
                                           IGMP_PROXY_CLI_SHOW_MROUTES_GRP);
                }
                else
                {
                    IgpCliShowMrouteTable (CliHandle, IGMP_ZERO, u4IpAddr,
                                           IGMP_PROXY_CLI_SHOW_MROUTES_SRC);
                }
            }
            break;

        default:

            CliPrintf (CliHandle, "\r%%CLI ERROR: Unexpected event\r\n");
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_IGMP_PROXY_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", IgpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    IGMP_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgpcliSetModuleStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function Enables/Disables IGMP Proxy module   */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        i4IgpModuleStatus -  IGMP Proxy Module Status      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgpCliSetModuleStatus (tCliHandle CliHandle, INT4 i4IgpModuleStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsIgmpProxyStatus (&u4ErrCode,
                                    i4IgpModuleStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpProxyStatus (i4IgpModuleStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgpCliUpIfConfigure                                */
/*                                                                           */
/*     DESCRIPTION      : This function Creates/Deletes an upstream interface*/
/*                                                                           */
/*     INPUT            : CliHandle    -  CLIHandler                         */
/*                        u4IfIndex    - IP VLAN interface index             */
/*                        i4UpIfStatus - Variable based on which interface is*/
/*                                       created or deleted                  */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgpCliUpIfConfigure (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4UpIfStatus)
{
    UINT4               u4ErrCode = IGMP_ZERO;
    INT4                i4RowStatus = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpCliUpIfConfigure()\r \n");
    nmhGetFsIgmpProxyRtrIfaceRowStatus (i4IfIndex, &i4RowStatus);

    if ((i4UpIfStatus == CLI_ENABLE) && (i4RowStatus == IGMP_ZERO))
    {
        if (nmhTestv2FsIgmpProxyRtrIfaceRowStatus (&u4ErrCode, i4IfIndex,
                                                   IGMP_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsIgmpProxyRtrIfaceRowStatus (i4IfIndex,
                                                IGMP_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsIgmpProxyRtrIfaceRowStatus (i4IfIndex,
                                                IGMP_ACTIVE) == SNMP_FAILURE)
        {
            if (nmhSetFsIgmpProxyRtrIfaceRowStatus (i4IfIndex, IGMP_DESTROY) ==
                                                    SNMP_FAILURE)
            { 
            return CLI_FAILURE;
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    else if ((i4UpIfStatus == CLI_DISABLE) && (i4RowStatus != IGMP_ZERO))
    {
        if (nmhTestv2FsIgmpProxyRtrIfaceRowStatus (&u4ErrCode, i4IfIndex,
                                                   IGMP_DESTROY) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsIgmpProxyRtrIfaceRowStatus (i4IfIndex,
                                                IGMP_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpCliUpIfConfigure()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgpCliConfigureUpIfPurgeInterval                   */
/*                                                                           */
/*     DESCRIPTION      : This function Creates/Deletes an upstream interface*/
/*                                                                           */
/*     INPUT            : CliHandle    -  CLIHandler                         */
/*                        u4IfIndex    - IP VLAN interface index             */
/*                        i4Interval   - Purge interval                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgpCliConfigureUpIfPurgeInterval (tCliHandle CliHandle, UINT4 u4IfIndex,
                                  INT4 i4Interval)
{
    UINT4               u4ErrCode = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpCliConfigureUpIfPurgeInterval()\r \n");
    if (nmhTestv2FsIgmpProxyRtrIfacePurgeInterval (&u4ErrCode, (INT4) u4IfIndex,
                                                   i4Interval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIgmpProxyRtrIfaceRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                               IGMP_NOT_IN_SERVICE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpProxyRtrIfaceRowStatus ((INT4) u4IfIndex,
                                            IGMP_NOT_IN_SERVICE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpProxyRtrIfacePurgeInterval ((INT4) u4IfIndex,
                                                i4Interval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpProxyRtrIfaceRowStatus ((INT4) u4IfIndex,
                                            IGMP_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpCliConfigureUpIfPurgeInterval()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgpCliConfigureUpIfVersion                         */
/*                                                                           */
/*     DESCRIPTION      : This function Configures the version of upstream   */
/*                        interface                                          */
/*                                                                           */
/*     INPUT            : CliHandle    - CLIHandler                          */
/*                        u4IfIndex    - IP VLAN interface index             */
/*                        i4Version    - Version to be configured            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgpCliConfigureUpIfVersion (tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4Version)
{
    UINT4               u4ErrCode = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpCliConfigureUpIfVersion()\r \n");
    if (nmhTestv2FsIgmpProxyRtrIfaceCfgOperVersion (&u4ErrCode,
                                                    (INT4) u4IfIndex,
                                                    i4Version) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIgmpProxyRtrIfaceRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                               IGMP_NOT_IN_SERVICE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpProxyRtrIfaceRowStatus ((INT4) u4IfIndex,
                                            IGMP_NOT_IN_SERVICE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpProxyRtrIfaceCfgOperVersion ((INT4) u4IfIndex,
                                                 i4Version) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsIgmpProxyRtrIfaceRowStatus ((INT4) u4IfIndex,
                                            IGMP_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpCliConfigureUpIfVersion()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgpCliShowUpIfTable                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays upstream interface table    */
/*                                                                           */
/*     INPUT            : CliHandle    -  CLIHandler                         */
/*                        u4IfIndex    - IP VLAN interface index             */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgpCliShowUpIfTable (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4OperVersion = 0;
    INT4                i4CfgVersion = 0;
    INT4                i4Interval = 0;
    UINT4               u4UpTime = 0;
    UINT4               u4ExpTime = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1IsShowAll = IGMP_TRUE;
    INT1                ai1TmStr[17];
    const CHR1         *au1Version[] = {
        "IGMPv1",
        "IGMPv2",
        "IGMPv3"
    };
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpCliShowUpIfTable()\r \n");
    if (nmhGetFirstIndexFsIgmpProxyRtrIfaceTable (&i4NextIfIndex)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\n%6s%1s%4s%15s%12s%8s%1s%7s%12s\r\n",
               "IfName", "/", "IfId", "OperVersion", "CfgVersion",
               "UpTime", "/", "VersionExpiryTime", "PurgeIntvl");
    CliPrintf (CliHandle, "%8s%1s%4s%13s%12s%10s%1s%3s%12s\r\n",
               "--------", "-", "----", "-----------", "----------",
               "--------", "-", "---------------", "----------");
    do
    {
        if (i4IfIndex)
        {
            if (i4IfIndex != i4NextIfIndex)
            {
                i4CurrIfIndex = i4NextIfIndex;

                if (nmhGetNextIndexFsIgmpProxyRtrIfaceTable
                    (i4CurrIfIndex, &i4NextIfIndex) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
        }

        CfaGetInterfaceNameFromIndex (i4NextIfIndex, au1InterfaceName);

        nmhGetFsIgmpProxyRtrIfaceOperVersion (i4NextIfIndex, &i4OperVersion);
        nmhGetFsIgmpProxyRtrIfaceCfgOperVersion (i4NextIfIndex, &i4CfgVersion);
        nmhGetFsIgmpProxyRtrIfacePurgeInterval (i4NextIfIndex, &i4Interval);
        nmhGetFsIgmpProxyRtrIfaceUpTime (i4NextIfIndex, &u4UpTime);
        IgmpDispHhMmSs (u4UpTime, (CHR1 *) ai1TmStr);

        nmhGetFsIgmpProxyRtrIfaceExpiryTime (i4NextIfIndex, &u4ExpTime);
        if (u4ExpTime)
        {
            IGMP_GET_TIME_IN_SEC (u4ExpTime);
        }
        else
        {
            u4ExpTime = IGMP_ZERO;
        }

        u4PagingStatus =
            CliPrintf (CliHandle, "%-8s%1s%-4d%8s%13s%22s%1s%-3d%9d\r\n",
                       au1InterfaceName, "/", i4NextIfIndex,
                       au1Version[i4OperVersion - 1],
                       ((i4CfgVersion !=
                         0) ? au1Version[i4CfgVersion - 1] : "None  "),
                       ai1TmStr, "/", u4ExpTime, i4Interval);

        if ((u4PagingStatus == CLI_FAILURE) || (i4IfIndex != IGMP_ZERO))
        {
            break;
        }

        i4CurrIfIndex = i4NextIfIndex;

        if (nmhGetNextIndexFsIgmpProxyRtrIfaceTable (i4CurrIfIndex,
                                                     &i4NextIfIndex)
            != SNMP_SUCCESS)
        {
            u1IsShowAll = IGMP_FALSE;
        }

    }
    while (u1IsShowAll == IGMP_TRUE);

    CliPrintf (CliHandle, "\r\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpCliShowUpIfTable()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgpCliShowMrouteTable                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays multicast forwarding table  */
/*                                                                           */
/*     INPUT            : CliHandle    -  CLIHandler                         */
/*                        u4IfIndex    - IP VLAN interface index             */
/*                        u4IpAddress  - Group or source address             */
/*                        i4Flag       - display flag                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgpCliShowMrouteTable (tCliHandle CliHandle, UINT4 u4IfIndex,
                       UINT4 u4IpAddress, INT4 i4Flag)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyOif       *pIgpOifEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4UpTime = 0;
    UINT4               u4ExpTime = 0;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4MaxHwEntryCnt = 0;
    INT4                i4RetValue = 0;
    UINT1               au1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT1                ai1TmStr[17];
    CHR1               *pu1String = NULL;
    UINT4               u4Port = IGMP_ZERO;
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    UINT1               u1EntryFound = IGMP_FALSE;
    
    UNUSED_PARAM (i4RetValue);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpCliShowMrouteTable()\r \n");
    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);
    if (pRBElem == NULL)
    {
        return CLI_SUCCESS;
    }

    nmhGetFsIgmpProxyForwardingTblEntryCnt (&i4MaxHwEntryCnt);
    CliPrintf (CliHandle, "\r\nTotal No of Multicast Route Entries : %d\r\n",
               i4MaxHwEntryCnt);
    CliPrintf (CliHandle, "\r\nIGMP Proxy Multicast Routing table\r\n");
    CliPrintf (CliHandle, "----------------------------------\r\n");
    CliPrintf (CliHandle, "(Source, Group) , Uptime/Expires(seconds)\r\n");
    CliPrintf (CliHandle, "Incoming Interface: Interface\r\n");
    CliPrintf (CliHandle, "Outgoing Interface: \nInterface, State\r\n\n");

    if (i4Flag == IGMP_PROXY_CLI_SHOW_MROUTES_VID)
    {
        i4RetValue = IGMP_IP_GET_PORT_FROM_IFINDEX (u4IfIndex, &u4Port);
    }

    /* Scan thro the forwarding entries */
    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        switch (i4Flag)
        {
            case IGMP_PROXY_CLI_SHOW_MROUTES_VID:
                if (pIgpForwardEntry->u4IfIndex == u4Port)
                {
                    u1EntryFound = IGMP_TRUE;
                }
                break;

            case IGMP_PROXY_CLI_SHOW_MROUTES_GRP:
                IGMP_IP_COPY_FROM_IPVX (&u4IpGrp,
                                        pIgpForwardEntry->u4GrpIpAddr,
                                        IPVX_ADDR_FMLY_IPV4);
                if (u4IpGrp == u4IpAddress)
                {
                    u1EntryFound = IGMP_TRUE;
                }
                break;

            case IGMP_PROXY_CLI_SHOW_MROUTES_SRC:
                IGMP_IP_COPY_FROM_IPVX (&u4IpSrc,
                                        pIgpForwardEntry->u4SrcIpAddr,
                                        IPVX_ADDR_FMLY_IPV4);
                if (u4IpSrc == u4IpAddress)
                {
                    u1EntryFound = IGMP_TRUE;
                }
                break;

            default:
                u1EntryFound = IGMP_TRUE;
                break;
        }

        if (u1EntryFound == IGMP_TRUE)
        {
            IGMP_IP_COPY_FROM_IPVX (&u4IpSrc,
                                    pIgpForwardEntry->u4SrcIpAddr,
                                    IPVX_ADDR_FMLY_IPV4);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpSrc);
            CliPrintf (CliHandle, "(%s, ", pu1String);
            IGMP_IP_COPY_FROM_IPVX (&u4IpGrp,
                                    pIgpForwardEntry->u4GrpIpAddr,
                                    IPVX_ADDR_FMLY_IPV4);

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpGrp);
            CliPrintf (CliHandle, "%s)", pu1String);

            nmhGetFsIgmpProxyMRouteUpTime (u4IpSrc, u4IpGrp, &u4UpTime);

            nmhGetFsIgmpProxyMRouteExpiryTime (u4IpSrc, u4IpGrp, &u4ExpTime);

            IGMP_GET_TIME_IN_SEC (u4ExpTime);
            IgmpDispHhMmSs (u4UpTime, (CHR1 *) ai1TmStr);

            CliPrintf (CliHandle, " ,%s/ %d\r\n", ai1TmStr, u4ExpTime);

            IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpForwardEntry->u4IfIndex,
                                           &u4CfaIfIndex);
            CfaGetInterfaceNameFromIndex (u4CfaIfIndex, au1InterfaceName);
            CliPrintf (CliHandle, "  Incoming Interface : %s\r\n",
                       au1InterfaceName);

            CliPrintf (CliHandle, "  Outgoing InterfaceList :\r\n");

            TMO_SLL_Scan (&(pIgpForwardEntry->OifList), pIgpOifEntry,
                          tIgmpPrxyOif *)
            {
                IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpOifEntry->u4IfIndex,
                                               &u4CfaIfIndex);
                CfaGetInterfaceNameFromIndex (u4CfaIfIndex, au1InterfaceName);

                CliPrintf (CliHandle, "    %s,  Forwarding\r\n",
                           au1InterfaceName);
            }

            u4PagingStatus = CliPrintf (CliHandle, "\r\n");

            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }
            u1EntryFound = IGMP_FALSE;
        }
        pRBElem = pRBNextElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpCliShowMrouteTable()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IgmpProxyShowRunningConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the IGMP Proxy             */
/*                        configurations                                     */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IgmpProxyShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    INT4                i4ScalarObject;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyShowRunningConfig()\r \n");
    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();

    nmhGetFsIgmpProxyStatus (&i4ScalarObject);
    if (i4ScalarObject != IGMP_DISABLE)
    {
        CliPrintf (CliHandle, "ip igmp proxy-service\r\n");
    }


    if (u4Module == ISS_IGP_SHOW_RUNNING_CONFIG)
    {
        IgmpProxyShowRunningConfigUpIface (CliHandle);
    }

    IGMP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyShowRunningConfig()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgmpProxyShowRunningConfigUpIface                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays current configuration       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
IgmpProxyShowRunningConfigUpIface (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex;
    UINT1               u1isShowAll = TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyShowRunningConfigUpIface()\r \n");
    if (nmhGetFirstIndexFsIgmpProxyRtrIfaceTable (&i4NextIfIndex)
        != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        IGMP_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        IgmpProxyShowRunningConfigUpIfaceDetails (CliHandle, i4NextIfIndex);

        CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
        IGMP_LOCK ();

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        i4IfIndex = i4NextIfIndex;

        if (nmhGetNextIndexFsIgmpProxyRtrIfaceTable (i4IfIndex, &i4NextIfIndex)
            == SNMP_FAILURE)
        {
            u1isShowAll = FALSE;
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit 
             * 
             * Setting the Count to -1 will result in not displaying the
             * count of the entries.
             */
            u1isShowAll = FALSE;
        }
    }
    while (u1isShowAll == TRUE);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyShowRunningConfigUpIface()\r \n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IgmpProxyShowRunningConfigUpIfaceDetails           */
/*                                                                           */
/*     DESCRIPTION      : This function displays current interface specific  */
/*                        configurations in IGMP Proxy                       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index  - Specified interface for                 */
/*                        configuration                                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IgmpProxyShowRunningConfigUpIfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4TableObject;
    INT1                ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1InterfaceName = NULL;
    UINT1               u1BangStatus = FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgmpProxyShowRunningConfigUpIfaceDetails()\r \n");
    MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    CliRegisterLock (CliHandle, IgmpLock, IgmpUnLock);
    IGMP_LOCK ();

    if (nmhValidateIndexInstanceFsIgmpProxyRtrIfaceTable (i4Index) !=
        SNMP_SUCCESS)
    {
        IGMP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    pi1InterfaceName = &ai1InterfaceName[0];

    CfaCliConfGetIfName ((UINT4) i4Index, pi1InterfaceName);
    CliPrintf (CliHandle, "interface %s\r\n", pi1InterfaceName);
    
    u1BangStatus = TRUE;  
   
    CliPrintf (CliHandle, "ip igmp-proxy mrouter\r\n");

    nmhGetFsIgmpProxyRtrIfacePurgeInterval (i4Index, &i4TableObject);
    if (i4TableObject != IGP_DEF_RTR_IFACE_INTERVAL)
    {
        CliPrintf (CliHandle, "ip igmp-proxy mrouter-time-out %d\r\n",
                   i4TableObject);
    }

    nmhGetFsIgmpProxyRtrIfaceCfgOperVersion (i4Index, &i4TableObject);
    if (i4TableObject != IGMP_VERSION_3)
    {
        CliPrintf (CliHandle, "ip igmp-proxy mrouter-version %d\r\n",
                   i4TableObject);
    }
    
    if (u1BangStatus == TRUE)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    IGMP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgmpProxyShowRunningConfigUpIfaceDetails()\r \n");
    return;
}

#endif                                          /*_IGPCLI_C */
