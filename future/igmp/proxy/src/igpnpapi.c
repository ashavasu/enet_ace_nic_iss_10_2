/*****************************************************************************/
/*    FILE  NAME            : igpnpapi.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy                                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains functions that interface    */
/*                            with hardware                                  */
/* $Id: igpnpapi.c,v 1.16 2015/10/01 11:32:40 siva Exp $                  */
/*---------------------------------------------------------------------------*/

#include "igpinc.h"

static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;
/*****************************************************************************/
/* Function Name      : IgpNpHwEnableMcastRouting                            */
/*                                                                           */
/* Description        : This function enables IP multicast routing in the    */
/*                      hardware                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/

INT1
IgpNpHwEnableMcastRouting (VOID)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpHwEnableMcastRouting()\r \n");
    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    /* Enable Multicast routing in the hardware */
    if (IpmcFsNpIgmpProxyInit () == FNP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Unable to enable IP multicast routing in the hardware\r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpHwEnableMcastRouting()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpNpHwDisableMcastRouting                           */
/*                                                                           */
/* Description        : This function disables IP multicast routing          */
/*                      in the hardware                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT1
IgpNpHwDisableMcastRouting (VOID)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpHwDisableMcastRouting()\r \n");
    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    /* Disable IP multicast routing in hardware */
    if (IpmcFsNpIgmpProxyDeInit () == FNP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Unable to disable IP multicast routing in the hardware\r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpHwDisableMcastRouting()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpNpCreateFwdEntry                                  */
/*                                                                           */
/* Description        : This function creates a multicast forwarding entry   */
/*                      in the hardware                                      */
/*                                                                           */
/* Input(s)           : *pIgpFwdEntry -  Pointer to the multicast forward    */
/*                                       entry                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpNpCreateFwdEntry (tIgmpPrxyFwdEntry * pIgpFwdEntry)
{
    tMcRtEntry          rtEntry;
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tNetIpOif          *pOifList = NULL;
    tMcDownStreamIf    *pDsIf = NULL;
    tMcDownStreamIf    *pTmpDsIf = NULL;
    tIgmpIface         *pIfNode = NULL;
    tIgmpPrxyOif       *pOifNode = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4OifCnt = 0;
    INT4                i4RetVal = IGMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpCreateFwdEntry()\r \n");
    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */

    IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpFwdEntry->u4IfIndex, &u4IfIndex);
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (IgpUtilGetVlanIdFromIfIndex (pIgpFwdEntry->u4IfIndex,
                                     &rtEntry.u2VlanId) == IGMP_FAILURE)
    {
        return IGMP_FAILURE;
    }

    /* Get the portlist for incoming VLAN id from IGS or VLAN
     * module */
    if (IgpNpGetMcastFwdPortList (pIgpFwdEntry->u4GrpIpAddr,
                                  pIgpFwdEntry->u4SrcIpAddr,
                                  u4IfIndex,
                                  rtEntry.u2VlanId,
                                  rtEntry.McFwdPortList,
                                  rtEntry.UntagPortList) != IGMP_SUCCESS)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Failure in getting the portlist for incoming VLAN %d\r\n",
                       rtEntry.u2VlanId);
        return IGMP_FAILURE;
    }

    if (TMO_SLL_Count (&(pIgpFwdEntry->OifList)) != 0)
    {
        pDsIf = MEM_MALLOC ((sizeof (tMcDownStreamIf) *
                             TMO_SLL_Count (&(pIgpFwdEntry->OifList))),
                            tMcDownStreamIf);
        if (pDsIf == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                      "Failure allocating memory for installing entry in NP\r\n");
            return IGMP_FAILURE;
        }

        pOifList = MEM_MALLOC (sizeof (tNetIpOif) *
                               (TMO_SLL_Count (&(pIgpFwdEntry->OifList))),
                               tNetIpOif);
        if (pOifList == NULL)
        {
            MEM_FREE (pDsIf);
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                      "Failure allocating memory for installing entry in NP\r\n");
            return IGMP_FAILURE;
        }
    }

    pTmpDsIf = pDsIf;
    TMO_SLL_Scan (&(pIgpFwdEntry->OifList), pOifNode, tIgmpPrxyOif *)
    {
        /* If the OIF is to incoming interface continue
         * with the next in the list */
        if (pIgpFwdEntry->u4IfIndex == pOifNode->u4IfIndex)
        {
            continue;
        }

        IGMP_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4IfIndex,
                                       &pTmpDsIf->u4IfIndex);

        pIfNode = IgmpGetIfNode (pOifNode->u4IfIndex, IPVX_ADDR_FMLY_IPV4);
        pTmpDsIf->u2TtlThreshold = 255;
        pTmpDsIf->u4Mtu = pIfNode->u4Mtu;

        pOifList[u4OifCnt].u2TtlThreshold = pTmpDsIf->u2TtlThreshold;
        pOifList[u4OifCnt].u4IfIndex = pTmpDsIf->u4IfIndex;

        if (IgpUtilGetVlanIdFromIfIndex (pOifNode->u4IfIndex,
                                         &pTmpDsIf->u2VlanId) == IGMP_FAILURE)
        {
            MEM_FREE (pDsIf);
            MEM_FREE (pOifList);
            return IGMP_FAILURE;
        }

        if (IgpNpGetMcastFwdPortList (pIgpFwdEntry->u4GrpIpAddr,
                                      pIgpFwdEntry->u4SrcIpAddr,
                                      u4IfIndex,
                                      pTmpDsIf->u2VlanId,
                                      pTmpDsIf->McFwdPortList,
                                      pTmpDsIf->UntagPortList) != IGMP_SUCCESS)
        {
            IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                           "Failure in getting the portlist for outgoing VLAN %d\r\n",
                           pTmpDsIf->u2VlanId);
            MEM_FREE (pDsIf);
            MEM_FREE (pOifList);
            return IGMP_FAILURE;
        }

        pTmpDsIf++;
        u4OifCnt++;
    }

    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4SrcAddr),
                            pIgpFwdEntry->u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4GrpAddr),
                            pIgpFwdEntry->u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4);
    NetIpMcRouteInfo.u4Iif = pIgpFwdEntry->u4IfIndex;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;
    NetIpMcRouteInfo.pOIf = pOifList;

    /* Add the route entry into LinuxIp. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        != NETIPV4_SUCCESS)
    {
        if (pDsIf != NULL)
        {
            MEM_FREE (pDsIf);
            MEM_FREE (pOifList);
        }
        return IGMP_FAILURE;
    }

    if ((IpmcFsNpIpv4McAddRouteEntry (IGMP_PROXY_ID,
                                  NetIpMcRouteInfo.u4GrpAddr,
                                  FNP_ALL_BITS_SET,
                                  NetIpMcRouteInfo.u4SrcAddr,
                                  FNP_ALL_BITS_SET, IPMC_MRP, rtEntry,
                                  u4OifCnt, pDsIf)) == FNP_FAILURE)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Failure in installing IPMC entry in NP for"
                       " (0x%x, 0x%x)\r\n", pIgpFwdEntry->u4SrcIpAddr,
                       pIgpFwdEntry->u4GrpIpAddr);

        i4RetVal = IGMP_FAILURE;
    }
    else
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Installed entry (0x%x, 0x%x) in NP\r\n",
                       pIgpFwdEntry->u4SrcIpAddr, pIgpFwdEntry->u4GrpIpAddr);

        i4RetVal = IGMP_SUCCESS;
    }

    if (pDsIf != NULL)
    {
        MEM_FREE (pDsIf);
        MEM_FREE (pOifList);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpCreateFwdEntry()\r \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IgpNpDeleteFwdEntry                                  */
/*                                                                           */
/* Description        : This function deletes the multicast forwarding entry */
/*                      in the hardware                                      */
/*                                                                           */
/* Input(s)           : *pIgpFwdEntry -  Pointer to the multicast forward    */
/*                                       entry                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpNpDeleteFwdEntry (tIgmpPrxyFwdEntry * pIgpFwdEntry)
{
    tMcRtEntry          rtEntry;
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    UINT4               u4IfIndex = 0;

    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpDeleteFwdEntry()\r \n");

    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */
    IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpFwdEntry->u4IfIndex, &u4IfIndex);
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (IgpUtilGetVlanIdFromIfIndex (pIgpFwdEntry->u4IfIndex,
                                     &rtEntry.u2VlanId) == IGMP_FAILURE)
    {
        return IGMP_FAILURE;
    }

    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4SrcAddr),
                            pIgpFwdEntry->u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4GrpAddr),
                            pIgpFwdEntry->u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4);
    NetIpMcRouteInfo.u4Iif = pIgpFwdEntry->u4IfIndex;

    /* delete route entry from linux ip. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_DELETE_ROUTE)
        != NETIPV4_SUCCESS)
    {
        return IGMP_FAILURE;
    }

    if ((IpmcFsNpIpv4McDelRouteEntry (IGMP_PROXY_ID,
                                  NetIpMcRouteInfo.u4GrpAddr,
                                  FNP_ALL_BITS_SET,
                                  NetIpMcRouteInfo.u4SrcAddr,
                                  FNP_ALL_BITS_SET,
                                  rtEntry, FNP_ZERO, NULL)) == FNP_FAILURE)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Failure in deleting IPMC entry in NP for"
                       " (0x%x, 0x%x)\r\n", pIgpFwdEntry->u4SrcIpAddr,
                       pIgpFwdEntry->u4GrpIpAddr);

        return IGMP_FAILURE;
    }
    else
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Deleted Route (0x%x, 0x%x) from the NP\n",
                       pIgpFwdEntry->u4SrcIpAddr, pIgpFwdEntry->u4GrpIpAddr);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpDeleteFwdEntry()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpNpAddOif                                          */
/*                                                                           */
/* Description        : This function adds the outgoing interface to the     */
/*                      existing multicast forwarding entry                  */
/*                                                                           */
/* Input(s)           : pIgpFwdEntry - Pointer to the multicast forward entry*/
/*                      u4OifIndex - Outgoing interface index                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpNpAddOif (tIgmpPrxyFwdEntry * pIgpFwdEntry, UINT4 u4OifIndex)
{
    tMcRtEntry          rtEntry;
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tMcDownStreamIf     DsIf;
    tIgmpIface         *pIfNode = NULL;
    tNetIpOif          *pOifList = NULL;
    tIgmpPrxyOif       *pOifNode = NULL;
    UINT4               u4OifCnt = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = IGMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpAddOif()\r \n");
    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    MEMSET (&DsIf, 0, sizeof (DsIf));
    MEMSET (&NetIpMcRouteInfo, 0, sizeof (tNetIpMcRouteInfo));

    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    if (pIgpFwdEntry->u4IfIndex == u4OifIndex)
    {
        return IGMP_SUCCESS;
    }

    pIfNode = IgmpGetIfNode (u4OifIndex, IPVX_ADDR_FMLY_IPV4);
    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpFwdEntry->u4IfIndex, &u4IfIndex);
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (IgpUtilGetVlanIdFromIfIndex (pIgpFwdEntry->u4IfIndex,
                                     &rtEntry.u2VlanId) == IGMP_FAILURE)
    {
        return IGMP_FAILURE;
    }

    DsIf.u2TtlThreshold = 255;
    DsIf.u4Mtu = pIfNode->u4Mtu;

    IGMP_IP_GET_IFINDEX_FROM_PORT (u4OifIndex, &DsIf.u4IfIndex);

    if (IgpUtilGetVlanIdFromIfIndex (u4OifIndex,
                                     &DsIf.u2VlanId) == IGMP_FAILURE)
    {
        return IGMP_FAILURE;
    }

    if (IgpNpGetMcastFwdPortList (pIgpFwdEntry->u4GrpIpAddr,
                                  pIgpFwdEntry->u4SrcIpAddr,
                                  u4IfIndex,
                                  DsIf.u2VlanId,
                                  DsIf.McFwdPortList,
                                  DsIf.UntagPortList) != IGMP_SUCCESS)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Failure in getting the portlist for VLAN %d\r\n",
                       DsIf.u2VlanId);
        return IGMP_FAILURE;
    }

    if (TMO_SLL_Count (&(pIgpFwdEntry->OifList)) != 0)
    {
        pOifList = MEM_MALLOC (sizeof (tNetIpOif) *
                               (TMO_SLL_Count (&(pIgpFwdEntry->OifList))),
                               tNetIpOif);
        if (pOifList == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                      "Failure allocating memory for installing entry in NP\r\n");
            return IGMP_FAILURE;
        }
    }

    TMO_SLL_Scan (&(pIgpFwdEntry->OifList), pOifNode, tIgmpPrxyOif *)
    {
        /* If the OIF is to incoming interface continue
         * with the next in the list */
        if (pIgpFwdEntry->u4IfIndex == pOifNode->u4IfIndex)
        {
            continue;
        }

        IGMP_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4IfIndex,
                                       &(pOifList[u4OifCnt].u4IfIndex));

        pOifList[u4OifCnt].u2TtlThreshold = 255;
        u4OifCnt++;
    }

    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4SrcAddr),
                            pIgpFwdEntry->u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4GrpAddr),
                            pIgpFwdEntry->u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4);
    NetIpMcRouteInfo.u4Iif = pIgpFwdEntry->u4IfIndex;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;
    NetIpMcRouteInfo.pOIf = pOifList;

    /* Add the route entry into LinuxIp. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        != NETIPV4_SUCCESS)
    {
        if (pOifList != NULL)
        {
            MEM_FREE (pOifList);
        }
        return IGMP_FAILURE;
    }

    if ((IpmcFsNpIpv4McAddRouteEntry (IGMP_PROXY_ID,
                                  NetIpMcRouteInfo.u4GrpAddr, FNP_ALL_BITS_SET,
                                  NetIpMcRouteInfo.u4SrcAddr, FNP_ALL_BITS_SET,
                                  IPMC_MRP, rtEntry,
                                  IGMP_ONE, &DsIf)) == FNP_FAILURE)
    {

        IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Failure in adding OIF %d for IPMC entry in NP for"
                       " (0x%x, 0x%x)\r\n", u4OifIndex,
                       pIgpFwdEntry->u4SrcIpAddr, pIgpFwdEntry->u4GrpIpAddr);

        i4RetVal = IGMP_FAILURE;
    }
    else
    {
        IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Installed oif 0x%x for entry (0x%x, 0x%x) in NP.\n",
                       u4OifIndex, pIgpFwdEntry->u4SrcIpAddr,
                       pIgpFwdEntry->u4GrpIpAddr);

        i4RetVal = IGMP_SUCCESS;
    }

    if (pOifList != NULL)
    {
        MEM_FREE (pOifList);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpAddOif()\r \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IgpNpDeleteOif                                       */
/*                                                                           */
/* Description        : This function deletes the outgoing interface from the*/
/*                      existing multicast forwarding entry                  */
/*                                                                           */
/* Input(s)           : pIgpFwdEntry - Pointer to the multicast forward entry*/
/*                      u4OifIndex - Outgoing interface index                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpNpDeleteOif (tIgmpPrxyFwdEntry * pIgpFwdEntry, UINT4 u4OifIndex)
{
    tMcRtEntry          rtEntry;
    tMcDownStreamIf     DsIf;
    tNetIpMcRouteInfo   NetIpMcRouteInfo;
    tNetIpOif          *pOifList = NULL;
    tIgmpPrxyOif       *pOifNode = NULL;
    UINT4               u4OifCnt = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = IGMP_FAILURE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpDeleteOif()\r \n");
    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));
    MEMSET (&DsIf, 0, sizeof (tMcDownStreamIf));

    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

    /* Pass the CFA ifindex to NP, ir-respective of whether the interface
     * exists or not. This will be used in case of older NPAPI, in which  
     * the input CFA ifindex is used to retrieve the VLAN id. 
     */
    IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpFwdEntry->u4IfIndex, &u4IfIndex);
    rtEntry.u2RpfIf = (UINT2) u4IfIndex;

    if (IgpUtilGetVlanIdFromIfIndex (pIgpFwdEntry->u4IfIndex,
                                     &rtEntry.u2VlanId) == IGMP_FAILURE)
    {
        return IGMP_FAILURE;
    }

    IGMP_IP_GET_IFINDEX_FROM_PORT (u4OifIndex, &(DsIf.u4IfIndex));

    if (IgpUtilGetVlanIdFromIfIndex (u4OifIndex,
                                     &DsIf.u2VlanId) == IGMP_FAILURE)
    {
        return IGMP_FAILURE;
    }

    /* Update the portlist for this MC group address */
    if (IgpNpGetMcastFwdPortList (pIgpFwdEntry->u4GrpIpAddr,
                                  pIgpFwdEntry->u4SrcIpAddr,
                                  u4IfIndex,
                                  DsIf.u2VlanId,
                                  DsIf.McFwdPortList,
                                  DsIf.UntagPortList) != IGMP_SUCCESS)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Failure in getting the portlist for VLAN %d\r\n",
                       DsIf.u2VlanId);
        return IGMP_FAILURE;
    }

    if (TMO_SLL_Count (&(pIgpFwdEntry->OifList)) != 0)
    {
        pOifList = MEM_MALLOC (sizeof (tNetIpOif) *
                               (TMO_SLL_Count (&(pIgpFwdEntry->OifList))),
                               tNetIpOif);
        if (pOifList == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                      "Failure allocating memory for installing entry in NP\r\n");
            return IGMP_FAILURE;
        }
    }

    TMO_SLL_Scan (&(pIgpFwdEntry->OifList), pOifNode, tIgmpPrxyOif *)
    {
        /* If the OIF is to incoming interface continue
         * with the next in the list */
        if (pIgpFwdEntry->u4IfIndex == pOifNode->u4IfIndex)
        {
            continue;
        }

        IGMP_IP_GET_IFINDEX_FROM_PORT (pOifNode->u4IfIndex,
                                       &(pOifList[u4OifCnt].u4IfIndex));

        pOifList[u4OifCnt].u2TtlThreshold = 255;
        u4OifCnt++;
    }

    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4SrcAddr),
                            pIgpFwdEntry->u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&(NetIpMcRouteInfo.u4GrpAddr),
                            pIgpFwdEntry->u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4);
    NetIpMcRouteInfo.u4Iif = pIgpFwdEntry->u4IfIndex;
    NetIpMcRouteInfo.u4OifCnt = u4OifCnt;
    NetIpMcRouteInfo.pOIf = pOifList;

    /* Add the route entry into LinuxIp. */
    if (NetIpv4McastRouteUpdate (&NetIpMcRouteInfo, NETIPV4_ADD_ROUTE)
        != NETIPV4_SUCCESS)
    {
        if (pOifList != NULL)
        {
            MEM_FREE (pOifList);
        }
        return IGMP_FAILURE;
    }

    if ((IpmcFsNpIpv4McDelRouteEntry (IGMP_PROXY_ID,
                                  NetIpMcRouteInfo.u4GrpAddr,
                                  FNP_ALL_BITS_SET,
                                  NetIpMcRouteInfo.u4SrcAddr,
                                  FNP_ALL_BITS_SET, rtEntry,
                                  IGMP_ONE, &DsIf)) == FNP_FAILURE)
    {
        IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Failure in deleting OIF %d for IPMC entry in NP for"
                       " (0x%x, 0x%x)\r\n", DsIf.u2VlanId,
                       pIgpFwdEntry->u4SrcIpAddr, pIgpFwdEntry->u4GrpIpAddr);

        i4RetVal = IGMP_FAILURE;
    }
    else
    {
        IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                       "Deleted Oif 0x%x from entry (0x%x, 0x%x) in NP.\n",
                       u4OifIndex,
                       pIgpFwdEntry->u4SrcIpAddr, pIgpFwdEntry->u4GrpIpAddr);

        i4RetVal = IGMP_SUCCESS;
    }

    if (pOifList != NULL)
    {
        MEM_FREE (pOifList);
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpDeleteOif()\r \n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IgpNpGetMcastFwdPortList                             */
/*                                                                           */
/* Description        : This function gets the portlist from IGS for the     */
/*                      multicast forwarding entry                           */
/*                                                                           */
/* Input(s)           : u4GrpAddr - Multicast group IP address               */
/*                      u4SrcAddr - Source IP address                        */
/*                      u2VlanId - Incoming VLAN Identifier                  */
/*                      McFwdPortList - Multicast Foward port list           */
/*                      UntagPortList - Untagged  port list                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
UINT4
IgpNpGetMcastFwdPortList (tIgmpIPvXAddr u4GrpAddr, tIgmpIPvXAddr u4SrcAddr,
                          UINT4 u4IfIndex, UINT2 u2VlanId,
                          tPortListExt McFwdPortList,
                          tPortListExt UntagPortList)
{
    tMacAddr            au1MacAddr;
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMcastFwdPortList()\r \n");
    /* Convert GroupAddr to MacAddr */
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, u4GrpAddr, IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, u4SrcAddr, IPVX_ADDR_FMLY_IPV4);
    UtilConvMcastIP2Mac (u4IpGrp, au1MacAddr);

    MEMSET (McFwdPortList, 0, sizeof (tPortListExt));
    MEMSET (UntagPortList, 0, sizeof (tPortListExt));

    if (CfaIsPhysicalInterface (u4IfIndex) == CFA_TRUE)
    {
        OSIX_BITLIST_SET_BIT (McFwdPortList, u4IfIndex, sizeof (McFwdPortList));
        u2VlanId = 0;
        return IGMP_SUCCESS;
    }
    if (u2VlanId != 0)
    {
#ifdef IGS_WANTED
        /* Call IGS func here to update PortList. It will inturn call
         * VlanIgsGetTxPortList based on Igs forwarding mode
         */
        if (SNOOP_FAILURE ==
            SnoopUtilGetMcIgsFwdPorts (u2VlanId, u4IpGrp, u4IpSrc,
                                       McFwdPortList, UntagPortList))
#else
        if (VLAN_FAILURE == VlanGetVlanMemberPorts (u2VlanId,
                                                    McFwdPortList,
                                                    UntagPortList))
#endif
        {
            return IGMP_FAILURE;
        }
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMcastFwdPortList()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpNpHandlePortBmpChange                             */
/*                                                                           */
/* Description        : This function will update the IPMC table in the      */
/*                      hardware for bimap change event from IGS/VLAN        */
/*                                                                           */
/* Input(s)           : GrpMacAddr - Multicast group MAC address             */
/*                      u2VlanId - VLAN Identifier                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpNpHandlePortBmpChange (tMacAddr GrpMacAddr, UINT2 u2VlanId)
{
    tIgmpIface         *pIfNode;
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tMcRtEntry          rtEntry;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tMcDownStreamIf     downStreamIf;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex;
    UINT4               u4Iif;
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    tMacAddr            MacAddr;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpHandlePortBmpChange()\r \n");
    MEMSET (&downStreamIf, 0, sizeof (tMcDownStreamIf));
    MEMSET (&rtEntry, 0, sizeof (tMcRtEntry));

    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return;
    }

    /* Get the CFA interface index from the VLAN id */
    u4IfIndex = CfaGetVlanInterfaceIndex (u2VlanId);
    if (CFA_INVALID_INDEX == u4IfIndex)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Invalid VlanID passed for the PBMP change\r\n");
        return;
    }

    IGMP_IP_GET_PORT_FROM_IFINDEX (u4IfIndex, &u4Port);

    pIfNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);
    if (pIfNode == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "PBMP change received for a IGMP unware VLAN interface\r\n");
        return;
    }

    if (gIgmpProxyInfo.ForwardEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "No Multicast forwarding entries are not present\r\n");
        return;
    }

    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    /* Scan thro the forwarding entries */
    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        /* Check if the MAC address of the Groups match */
        IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                                IPVX_ADDR_FMLY_IPV4);

        UtilConvMcastIP2Mac (u4IpGrp, MacAddr);
        if (MEMCMP (MacAddr, GrpMacAddr, MAC_ADDR_LEN) != 0)
        {
            pRBElem = pRBNextElem;
            continue;
        }

        FNP_SET_DEFAULTS_IN_ROUTE_ENTRY (rtEntry);

        /* If the port bitmap change is for incoming VLAN interface */
        if (u4Port == pIgpForwardEntry->u4IfIndex)
        {
            rtEntry.u2RpfIf = (UINT2) u4IfIndex;
            /* Validate the incoming VLAN interface */
            if (IgpUtilGetVlanIdFromIfIndex (u4Port,
                                             &rtEntry.u2VlanId) == IGMP_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                          "Unable to get the VLAN ID from IP Port Number\r\n");
                return;
            }

            /* Get the MCAST port list from IGS/VLAN */
            if (IgpNpGetMcastFwdPortList (pIgpForwardEntry->u4GrpIpAddr,
                                          pIgpForwardEntry->u4SrcIpAddr,
                                          pIgpForwardEntry->u4IfIndex,
                                          rtEntry.u2VlanId,
                                          rtEntry.McFwdPortList,
                                          rtEntry.UntagPortList) !=
                IGMP_SUCCESS)
            {
                IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                               "Failure in getting the portlist for VLAN %d\r\n",
                               rtEntry.u2VlanId);
                return;
            }

            L2IwfGetVlanEgressPorts (rtEntry.u2VlanId,
                                     rtEntry.VlanEgressPortList);

            IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                                    IPVX_ADDR_FMLY_IPV4);
            IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, (pIgpForwardEntry->u4SrcIpAddr),
                                    IPVX_ADDR_FMLY_IPV4);
            IpmcFsNpIpv4McUpdateIifVlanEntry (0, u4IpGrp, u4IpSrc, rtEntry);
        }
        else
        {
            TMO_SLL_Scan (&(pIgpForwardEntry->OifList), pIgpOifNode,
                          tIgmpPrxyOif *)
            {
                if (pIgpOifNode->u4IfIndex != u4Port)
                {
                    continue;
                }
                else
                {
                    IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpForwardEntry->u4IfIndex,
                                                   &u4Iif);
                    rtEntry.u2RpfIf = (UINT2) u4Iif;

                    downStreamIf.u4IfIndex = u4IfIndex;
                    downStreamIf.u2TtlThreshold = 255;
                    downStreamIf.u4Mtu = pIfNode->u4Mtu;

                    /* Validate for the outgoing VLAN interface */
                    if ((IgpUtilGetVlanIdFromIfIndex
                         (pIgpForwardEntry->u4IfIndex,
                          &rtEntry.u2VlanId) == IGMP_FAILURE)
                        ||
                        (IgpUtilGetVlanIdFromIfIndex
                         (u4Port, &downStreamIf.u2VlanId) == IGMP_FAILURE))
                    {
                        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                                  "Unable to get the VLAN ID from"
                                  " IP Port Number\r\n");
                        return;
                    }

                    if (IgpNpGetMcastFwdPortList (pIgpForwardEntry->u4GrpIpAddr,
                                                  pIgpForwardEntry->u4SrcIpAddr,
                                                  pIgpForwardEntry->u4IfIndex,
                                                  downStreamIf.u2VlanId,
                                                  downStreamIf.McFwdPortList,
                                                  downStreamIf.UntagPortList) !=
                        IGMP_SUCCESS)
                    {
                        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                                       "Failure in getting the portlist for VLAN %d\r\n",
                                       rtEntry.u2VlanId);
                        return;
                    }

                    L2IwfGetVlanEgressPorts (downStreamIf.u2VlanId,
                                             downStreamIf.VlanEgressPortList);
                    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp,
                                            (pIgpForwardEntry->u4GrpIpAddr),
                                            IPVX_ADDR_FMLY_IPV4);
                    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc,
                                            (pIgpForwardEntry->u4SrcIpAddr),
                                            IPVX_ADDR_FMLY_IPV4);

                    IpmcFsNpIpv4McUpdateOifVlanEntry (0,
                                                  u4IpGrp,
                                                  u4IpSrc,
                                                  rtEntry, downStreamIf);
                }
            }
        }

        pRBElem = pRBNextElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpHandlePortBmpChange()\r \n");
    return;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMRoutePktCount                   */
/*  Description     : The function get the number of packets   */
/*                    router has received from source and      */
/*                    multicast group.                         */
/*  Input(s)        : pIgpForwardEntry - Route node            */
/*  Output(s)       : pu4IpMRoutePkts - packets received       */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMRoutePktCount (tIgmpPrxyFwdEntry * pIgpForwardEntry,
                        UINT4 *pu4IpMRoutePkts)
{
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMRoutePktCount()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, (pIgpForwardEntry->u4SrcIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
#ifndef L3_SWITCHING_WANTED

    if (NetIpv4McGetRouteStats (u4IpGrp,
                                u4IpSrc,
                                NETIPMC_ROUTE_PKTS, pu4IpMRoutePkts)
        == NETIPV4_SUCCESS)
    {
        return IGP_SUCCESS;
    }
    else
    {
        return IGP_FAILURE;
    }
#endif

    if (IpmcFsNpIpv4GetMRouteStats (IGMP_PROXY_ID, u4IpGrp,
                                u4IpSrc,
                                IPV4MROUTE_PKTS, pu4IpMRoutePkts)
        == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMRoutePktCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMRouteDifferentInIfPktCount      */
/*  Description     : This function gets the packets which     */
/*                    router has received from source and      */
/*                    multicast group which were dropped       */
/*                    because they were not received on the    */
/*                    expected incomming interface             */
/*  Input(s)        : pIgpForwardEntry - Route node            */
/*  Output(s)       : pu4DifferentInIfPackets -                */
/*                    Diffrent if in pkts.                     */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMRouteDifferentInIfPktCount (tIgmpPrxyFwdEntry * pIgpForwardEntry,
                                     UINT4 *pu4DifferentInIfPackets)
{
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMRouteDifferentInIfPktCount()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, (pIgpForwardEntry->u4SrcIpAddr),
                            IPVX_ADDR_FMLY_IPV4);

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetRouteStats (u4IpGrp,
                                u4IpSrc,
                                NETIPMC_ROUTE_DIFF_IF_IN_PKTS,
                                pu4DifferentInIfPackets) == NETIPV4_SUCCESS)
    {
        return IGP_SUCCESS;
    }
    else
    {
        return IGP_FAILURE;
    }
#endif

    if (IpmcFsNpIpv4GetMRouteStats (IGMP_PROXY_ID, u4IpGrp,
                                u4IpSrc,
                                IPV4MROUTE_DIFF_IF_IN_PKTS,
                                pu4DifferentInIfPackets) == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMRouteDifferentInIfPktCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMRouteOctetCount                 */
/*  Description     : The function get the number of Octets    */
/*                    router has received from source and      */
/*                    multicast group.                         */
/*  Input(s)        : pIgpForwardEntry - Route node            */
/*  Output(s)       : pu4IpMRouteOctets - Octets received      */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMRouteOctetCount (tIgmpPrxyFwdEntry * pIgpForwardEntry,
                          UINT4 *pu4IpMRouteOctets)
{
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMRouteOctetCount()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, (pIgpForwardEntry->u4SrcIpAddr),
                            IPVX_ADDR_FMLY_IPV4);

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetRouteStats (u4IpGrp,
                                u4IpSrc,
                                NETIPMC_ROUTE_OCTETS, pu4IpMRouteOctets)
        == NETIPV4_SUCCESS)
    {
        return IGP_SUCCESS;
    }
    else
    {
        return IGP_FAILURE;
    }
#endif

    if (IpmcFsNpIpv4GetMRouteStats (IGMP_PROXY_ID, u4IpGrp,
                                u4IpSrc,
                                IPV4MROUTE_OCTETS, pu4IpMRouteOctets)
        == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMRouteOctetCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMRouteHCOctetCount               */
/*  Description     : The function get the number of Octets    */
/*                    router has received from source and      */
/*                    multicast group. This is 64-bit counter  */
/*  Input(s)        : pIgpForwardEntry - Route node            */
/*  Output(s)       : pu4IpMRouteHCOctets - Octets received.   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMRouteHCOctetCount (tIgmpPrxyFwdEntry * pIgpForwardEntry,
                            tSNMP_COUNTER64_TYPE * pu8IpMRouteHCOctets)
{
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMRouteHCOctetCount()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, (pIgpForwardEntry->u4SrcIpAddr),
                            IPVX_ADDR_FMLY_IPV4);

    if (IpmcFsNpIpv4GetMRouteHCStats (IGMP_PROXY_ID, u4IpGrp,
                                  u4IpSrc,
                                  IPV4MROUTE_HCOCTETS, pu8IpMRouteHCOctets)
        == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMRouteHCOctetCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMNextHopPktCount                 */
/*  Description     : The function gets number of packets      */
/*                    which have been forwarded on this route  */
/*                    on particular interface                  */
/*  Input(s)        : pIgpForwardEntry - Route node            */
/*                    i4OifIndex -Out interface index          */
/*  Output(s)       : pu4NextHopPkts - Packets received.       */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMNextHopPktCount (tIgmpPrxyFwdEntry * pIgpForwardEntry, INT4 i4OifIndex,
                          UINT4 *pu4NextHopPkts)
{
    UINT4               i4IfIndex;
    UINT4               u4IpGrp = 0;
    UINT4               u4IpSrc = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMNextHopPktCount()\r \n");
    IGMP_IP_COPY_FROM_IPVX (&u4IpGrp, (pIgpForwardEntry->u4GrpIpAddr),
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4IpSrc, (pIgpForwardEntry->u4SrcIpAddr),
                            IPVX_ADDR_FMLY_IPV4);

    IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4OifIndex, (UINT4 *) &i4IfIndex);

    if (IpmcFsNpIpv4GetMNextHopStats (IGMP_PROXY_ID, u4IpGrp,
                                  u4IpSrc,
                                  i4IfIndex, IPV4MNEXTHOP_PKTS,
                                  pu4NextHopPkts) == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMNextHopPktCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMIfOutOctetCount                 */
/*  Description     : Gets number of octets of multicast pkts  */
/*                    that have arrived on the interface       */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4InOctets - Octets received on Iface   */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMIfInOctetCount (INT4 i4IfIndex, UINT4 *pu4InOctets)
{
    UINT4               i4CfaIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMIfInOctetCount()\r \n");
    IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetIfaceStats (i4CfaIndex, NETIPMC_IFACE_IN_OCTETS,
                                pu4InOctets) == NETIPV4_SUCCESS)
    {
        return IGP_SUCCESS;
    }
    else
    {
        return IGP_FAILURE;
    }
#endif

    if (IpmcFsNpIpv4GetMIfaceStats (IGMP_PROXY_ID, i4CfaIndex,
                                IPV4MIFACE_IN_MCAST_PKTS,
                                pu4InOctets) == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMIfInOctetCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMIfOutOctetCount                 */
/*  Description     : Gets number of octets of multicast pkts  */
/*                    that have sent on the interface          */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4OutOctets - Octets sent on Iface      */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMIfOutOctetCount (INT4 i4IfIndex, UINT4 *pu4OutOctets)
{
    UINT4               i4CfaIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMIfOutOctetCount()\r \n");
    IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

#ifndef L3_SWITCHING_WANTED
    if (NetIpv4McGetIfaceStats (i4CfaIndex, NETIPMC_IFACE_OUT_OCTETS,
                                pu4OutOctets) == NETIPV4_SUCCESS)
    {
        return IGP_SUCCESS;
    }
    else
    {
        return IGP_FAILURE;
    }
#endif

    if (IpmcFsNpIpv4GetMIfaceStats (IGMP_PROXY_ID, i4CfaIndex,
                                IPV4MIFACE_OUT_MCAST_PKTS,
                                pu4OutOctets) == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMIfOutOctetCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMIfHCInOctetCount                */
/*  Description     : Gets number of octets of  multicast pkts */
/*                    that have arrived on the interface       */
/*                    This is 64-bit counter.                  */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4InHCOctets - Octets received on Iface */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMIfHCInOctetCount (INT4 i4IfIndex, tSNMP_COUNTER64_TYPE * pu8HCInOctets)
{
    UINT4               i4CfaIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMIfHCInOctetCount()\r \n");
    IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4GetMIfaceHCStats (IGMP_PROXY_ID, i4CfaIndex,
                                  IPV4MIFACE_HCIN_MCAST_PKTS,
                                  pu8HCInOctets) == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetMIfHCInOctetCount()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpGetMIfHCOutOctetCount               */
/*  Description     : The function get number of octets of     */
/*                    multicast packets that have sent on the  */
/*                    interface. This is 64-bit counter.       */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : pu4OutHCOctets - Octets sent on interface*/
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpGetMIfHCOutOctetCount (INT4 i4IfIndex,
                            tSNMP_COUNTER64_TYPE * pu8HCOutOctets)
{
    UINT4               i4CfaIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetMIfHCOutOctetCount()\r \n");
    IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4GetMIfaceHCStats (IGMP_PROXY_ID, i4CfaIndex,
                                  IPV4MIFACE_HCOUT_MCAST_PKTS,
                                  pu8HCOutOctets) == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpSetMIfaceTtlTreshold                */
/*  Description     : This function sets the multicast TTL     */
/*                    treshold of the interface               */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : i4TtlTreshold -TTL treshold of the Iface*/
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpSetMIfaceTtlTreshold (INT4 i4IfIndex, INT4 i4TtlTreshold)
{
    UINT4               i4CfaIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpSetMIfaceTtlTreshold()\r \n");
    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4SetMIfaceTtlTreshold (IGMP_PROXY_ID, i4CfaIndex, i4TtlTreshold)
        == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpSetMIfaceTtlTreshold()\r \n");
    return IGP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IgpNpSetMIfaceRateLimit                  */
/*  Description     : This function sets the multicast rate    */
/*                    limit value for interface                */
/*  Input(s)        : i4IfIndex - Interface index              */
/*  Output(s)       : i4RateLimit - Rate limit value.          */
/*  <OPTIONAL Fields>:--                                       */
/*  Global Variables Referred :   --                           */
/*  Global variables Modified :   --                           */
/*  Exceptions or Operating System Error Handling :--          */
/*  Use of Recursion : NO                                      */
/*  Returns         : IGP_SUCCESS/IGP_FAILURE                  */
/***************************************************************/
INT4
IgpNpSetMIfaceRateLimit (INT4 i4IfIndex, INT4 i4RateLimit)
{
    UINT4               i4CfaIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpSetMIfaceRateLimit()\r \n");
    if (IGP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_FALSE)
    {
        return IGMP_SUCCESS;
    }

    IGMP_IP_GET_IFINDEX_FROM_PORT ((UINT4) i4IfIndex, (UINT4 *) &i4CfaIndex);

    if (IpmcFsNpIpv4SetMIfaceRateLimit (IGMP_PROXY_ID, i4CfaIndex, i4RateLimit)
        == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpSetMIfaceRateLimit()\r \n");
    return IGP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpNpGetFwdEntry                                     */
/*                                                                           */
/* Description        : This function gets the  multicast forwarding entry   */
/*                      in the hardware                                      */
/*                                                                           */
/* Input(s)           : *pIgpRedInfo  -  Pointer to the multicast forward    */
/*                                       entry                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpNpGetFwdEntry (tIgpRedTable * pIgpRedInfo)
{
    tNpL3McastEntry     NpL3McastEntry;
    UINT4               u4IfIndex = 0;
    UINT2               u2VlanId = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpNpGetFwdEntry()\r \n");
    MEMSET (&NpL3McastEntry, IGMP_ZERO, sizeof (tNpL3McastEntry));

    IGMP_IP_GET_IFINDEX_FROM_PORT (pIgpRedInfo->u4IfIndex, &u4IfIndex);

    /* For VLAN cases Vlan ID is retrieved.
     * For router port cases vlan id will be 0 */
    if (IgpUtilGetVlanIdFromIfIndex (pIgpRedInfo->u4IfIndex,
                                     &u2VlanId) == IGMP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MEMCPY (NpL3McastEntry.au1GrpAddr, pIgpRedInfo->GrpAddr.au1Addr,
            sizeof (NpL3McastEntry.au1GrpAddr));
    MEMCPY (NpL3McastEntry.au1SrcAddr, pIgpRedInfo->SrcAddr.au1Addr,
            sizeof (NpL3McastEntry.au1SrcAddr));
    NpL3McastEntry.u1Afi = pIgpRedInfo->GrpAddr.u1Afi;
    NpL3McastEntry.u2VlanId = u2VlanId;

    if (IpmcFsNpIpvXHwGetMcastEntry (&NpL3McastEntry) == FNP_FAILURE)
    {
        return IGP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpNpGetFwdEntry()\r \n");
    return IGP_SUCCESS;
}
