/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: igpmain.c,v 1.23 2015/02/13 11:13:54 siva Exp $
 * *
 * * Description: <File description>
 * *
 * *******************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : igpmain.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Main Module                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains init, deinit funtions       */
/*                            for IGMP Proxy module                          */
/*---------------------------------------------------------------------------*/
#include "igpinc.h"
#include "igpglob.h"

#ifdef SNMP_2_WANTED
#include "fsigpwr.h"
#endif /*SNMP_2_WANTED */

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE;
#endif

/*****************************************************************************/
/* Function Name      : IgpMainStatusEnable                                  */
/*                                                                           */
/* Description        : This function will enable IGMP Proxy functionality   */
/*                      in the router                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpMainStatusEnable (VOID)
{
#if defined (IP_WANTED) || defined (MFWD_WANTED)
    INT4                i4Status = 0;
#endif
#ifdef MFWD_WANTED
    tTMO_SLL_NODE      *pSllNode = NULL;
    tIgmpIface         *pIfaceNode = NULL;
#endif
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainStatusEnable()\r \n");
    /* Check if IGMP Proxy status is already enabled */
    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "IGMP Proxy is already enabled in the system\r\n");
        return IGMP_SUCCESS;
    }

    MEMSET (&gIgmpProxyInfo, 0, sizeof (gIgmpProxyInfo));

    /* Create Q for Receiving multicast data packets */
    if (OsixQueCrt ((UINT1 *) IGP_MDP_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    IGP_MDP_QUEUE_DEPTH,
                    &gIgmpProxyInfo.IgpMdpQId) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Creation of Queue for multicast data packet"
                  " reception failed\r\n");
        IgpMainStatusDisable ();
        return IGMP_FAILURE;
    }

    /* Create timer list for IGMP Proxy */
    if (TmrCreateTimerList ((const UINT1 *) IGMP_TASK_NAME,
                            IGP_TIMER_EXP_EVENT,
                            NULL, &gIgmpProxyInfo.ProxyTmrListId)
        == TMR_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE,
                  IGMP_NAME, "Timer list creation for IGMP Proxu Failed !!!\n");
        IgpMainStatusDisable ();
        return IGMP_FAILURE;
    }

    /* Init the timer descriptors */
    IgpTmrInitTmrDesc ();

    /*  Memory pool creation for IGMP Proxy data structure */
    if (IgpMainMemPoolCreate () != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Memory pool creation for IGMP Proxy data"
                  " structure failed\r\n");
        IgpMainStatusDisable ();
        return IGMP_FAILURE;
    }

    /*  Create RB Tree, Hash table and buffers fo IGMP Proxy data structure */
    if (IgpMainCreateProxyInfo () != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Memory pool creation for IGMP Proxy data"
                  " structure failed\r\n");
        IgpMainStatusDisable ();
        return IGMP_FAILURE;
    }

    IGMP_IPVX_ADDR_CLEAR (&gIgpIPvXZero);
#ifdef MFWD_WANTED
    IGP_MFWD_REGISTER_MRP (IGMP_PROXY_ID, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Failure in Registering with MFWD for"
                  " multicast data packets\r\n");
        IgpMainStatusDisable ();
        return IGMP_FAILURE;
    }
    gIgmpProxyInfo.u1MfwdStatus = MFWD_STATUS_ENABLED;

    TMO_SLL_Scan ((&gIgmpIfInfo.IfGetNextList), pSllNode, tTMO_SLL_NODE *)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pSllNode);
        if (pIfaceNode->u1EntryStatus == IGMP_ACTIVE)
        {
            IgpMfwdAddInterface (pIfaceNode->u4IfIndex);
        }
    }
#endif

#ifdef NPAPI_WANTED
    /* if NPAPI is enabled the multicast packet will be received 
     * through IP else packets are received through MFWD */

    /* Enable IP multicast routing in the harware */
    if (IgpNpHwEnableMcastRouting () != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Enabling IP multicast routing in hardware failed\r\n");
        IgpMainStatusDisable ();
        return IGMP_FAILURE;
    }
#ifdef IP_WANTED
    /* Register with IP */
    IGP_REGISTER_WITH_IP_FOR_MDP (gIgmpProxyInfo.u1IPRegnId, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Unable to register with FSIP for multicast data"
                  " Data packets \n");
        IgpMainStatusDisable ();
        return IGMP_FAILURE;
    }
#endif
#endif

    /* Initialise the IGMP Proxy info DB Node */
    IgmpRedDbNodeInit (&(gIgmpProxyInfo.IgpInfoDbNode), IGP_RED_INFO);

    IGMP_PROXY_STATUS () = IGMP_ENABLE;
    MEMSET (&gIgpNullSrcBmp, IGMP_ZERO, IGP_SRC_LIST_SIZE);
    /* Build the proxy group member data base and report to upstream for
     * the IGMP groups learned before proxy is enabled */
    IgpUtilBuildProxyDatabase ();
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMainStatusEnable()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpMainStatusDisable                                 */
/*                                                                           */
/* Description        : This function will disable IGMP Proxy functionality  */
/*                      in the router                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
VOID
IgpMainStatusDisable (VOID)
{
    tIgmpQMsg          *pIgpQMsg = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainStatusDisable()\r \n");
    /* Delete all the Multicast forwarding entries */
    IgpFwdDeleteFwdEntries (gIgpIPvXZero);

    /* Delete all the Group entries */
    IgpGrpDeleteGroupEntries ();

    /* Delete all the upstream interface entries */
    IgpUpIfDeleteUpIfaceEntries ();

#ifdef MFWD_WANTED
    IGP_MFWD_DEREGISTER_MRP (IGMP_PROXY_ID);
#endif

#ifdef NPAPI_WANTED
    /* Enable IP multicast routing in the harware */
    if (IgpNpHwDisableMcastRouting () != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Disabling IP multicast routing in hardware failed\r\n");
    }

#ifndef LNXIP4_WANTED
    /* De Register with IP for receiving mcast data packtes */
    IGP_DEREGISTER_WITH_IP_FOR_MDP (gIgmpProxyInfo.u1IPRegnId);
#endif
#endif

    /*  RBTree and Hash table deletion for IGMP Proxy data structure */
    IgpMainDeleteProxyInfo ();

    /*  Memory pool Deletion for IGMP Proxy data structure */
    IgpSizingMemDeleteMemPools ();

    gIgmpConfig.aIgmpTmrDesc[IGP_RTR_IFACE_TIMER].i2Offset = IGMP_ZERO;
    gIgmpConfig.aIgmpTmrDesc[IGP_RTR_IFACE_TIMER].TmrExpFn = NULL;
    gIgmpConfig.aIgmpTmrDesc[IGP_FWD_ENTRY_TIMER].i2Offset = IGMP_ZERO;
    gIgmpConfig.aIgmpTmrDesc[IGP_FWD_ENTRY_TIMER].TmrExpFn = NULL;

    /* Delete the timer list for IGMP Proxy */
    if (gIgmpProxyInfo.ProxyTmrListId)
    {
        if (TmrDeleteTimerList (gIgmpProxyInfo.ProxyTmrListId) != TMR_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                      "IGMP Proxy Timer list deletion failed !!!\r\n");
        }
    }

    if (gIgmpProxyInfo.IgpMdpQId)
    {
        /* Release all the multicast data packets from the MDP queue */
        while (OsixQueRecv (gIgmpProxyInfo.IgpMdpQId,
                            (UINT1 *) &pIgpQMsg,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            if (CRU_BUF_Release_MsgBufChain
                (pIgpQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt,
                 FALSE) != CRU_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                          "CRU buf release failed in incoming data packet "
                          "processing\r\n");
		SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
           		IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_REL_FAIL],
			" in incoming data packet\n");
            }

            IGP_RELEASE_MEM_BLOCK (gIgmpMemPool.IgmpQPoolId, pIgpQMsg);
        }
        /* Delete Queue for Receiving multicast data packets */
        OsixQueDel (gIgmpProxyInfo.IgpMdpQId);
    }

    MEMSET (&gIgmpProxyInfo, IGMP_ZERO, sizeof (tIgmpProxyInfo));

    IGMP_PROXY_STATUS () = IGMP_DISABLE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting IgpMainStatusDisable()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpMainMemPoolCreate                                 */
/*                                                                           */
/* Description        : This function will create memory pools for IGMP Proxy*/
/*                      data structures                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpMainMemPoolCreate (VOID)
{
    /* Create memory pool for consolidated group membership information */
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainMemPoolCreate()\r \n");
    if (IgpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Memory Pool creation failed " " source information\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_FAILURE],
		"for source information\n");	
        return IGMP_FAILURE;
    }

    gIgmpPrxyMemPool.GrpEntryPoolId =
        IGPMemPoolIds[MAX_IGP_MCAST_GRPS_SIZING_ID];

    gIgmpPrxyMemPool.FwdEntryPoolId =
        IGPMemPoolIds[MAX_IGP_FWD_ENTRIES_SIZING_ID];

    gIgmpPrxyMemPool.UpIfacePoolId =
        IGPMemPoolIds[MAX_IGP_RTR_IFACES_SIZING_ID];

    gIgmpPrxyMemPool.OifPoolId = IGPMemPoolIds[MAX_IGP_MAX_OIFS_SIZING_ID];

    gIgmpPrxyMemPool.SourcePoolId = IGPMemPoolIds[MAX_IGP_MCAST_SRCS_SIZING_ID];

    gIgmpPrxyMemPool.DynMsgPoolId = IGPMemPoolIds[MAX_IGP_DYN_MSG_SIZING_ID];
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMainMemPoolCreate()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpMainCreateProxyInfo                               */
/*                                                                           */
/* Description        : This function will create RBTree, hash table and     */
/*                      buffer allocations for the data structures           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpMainCreateProxyInfo (VOID)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainCreateProxyInfo()\r \n");
    /* RBTree creation for Consolidated Group Database */
    gIgmpProxyInfo.GroupEntry =
        RBTreeCreateEmbedded (0, IgpUtilRBTreeGroupEntryCmp);
    if (gIgmpProxyInfo.GroupEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "RB Tree creation for Consoldiated Group Entry failed\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IGMP_NAME,IgmpSysErrString[SYS_LOG_RB_TREE_CREATE_FAIL],
			"for Consoldiated Group Entry\n");
        return IGMP_FAILURE;
    }

    /* RBTree creation for multicast forward entries */
    gIgmpProxyInfo.ForwardEntry =
        RBTreeCreateEmbedded (0, IgpUtilRBTreeFwdEntryCmp);

    if (gIgmpProxyInfo.ForwardEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "RB Tree creation for IP Mcast Forward Entry " "failed\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IGMP_NAME,IgmpSysErrString[SYS_LOG_RB_TREE_CREATE_MCAST_FRWD_FAIL],
			"for IP Mcast Forward Entry\n");
        return IGMP_FAILURE;
    }

    gIgmpProxyInfo.pUpIfaceTbl =
        TMO_HASH_Create_Table (IGP_MAX_RTR_IFACE, IgmpIfNodeAddCriteria, TRUE);

    if (gIgmpProxyInfo.pUpIfaceTbl == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Hast table creation for Upstream interface failed\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
                       IGMP_NAME,IgmpSysErrString[SYS_LOG_HASH_TBL_CREATE_UPSTRM_FAIL],
			"for Upstream interface\n");
        return IGMP_FAILURE;
    }

    /* Memory allocation for IGMPv3 report buffer which is used for 
     * contructing IGMPv3 reports */
    MEMSET (gpv3RepSendBuffer, IGMP_ZERO, IGP_REP_BUF_MEMBLK_SIZE);

    /* Memory allocation for source information which is used for
     * constructing source information */
    if (IGP_ALLOC_MEM_BLOCK (IGP_SRC_MEMPOOL_ID,
                             &gpSrcInfoBuffer) != MEM_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Memory allocation for source information failed\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_OS_RES_MODULE,
               IGMP_NAME,IgmpSysErrString[SYS_LOG_MEM_ALLOC_SRC_INFO_FAIL],
		"for source information\n");
        return IGMP_FAILURE;
    }
    MEMSET (gpSrcInfoBuffer, IGMP_ZERO, IGP_SRC_BUF_MEMBLK_SIZE);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMainCreateProxyInfo()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpMainDeleteProxyInfo                               */
/*                                                                           */
/* Description        : This function will delete the RBTree, Hash tables    */
/*                      and buffer allocated for IGMP Proxy structures       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
VOID
IgpMainDeleteProxyInfo (VOID)
{
    UINT2               u2Index = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainDeleteProxyInfo()\r \n");
    /* Release source information buffer */
    if (gpSrcInfoBuffer != NULL)
    {
        IGP_RELEASE_MEM_BLOCK (IGP_SRC_MEMPOOL_ID, gpSrcInfoBuffer);
    }

    /* Reset the global list of sources */
    for (u2Index = 0; u2Index < IGP_MAX_MCAST_SRCS; u2Index++)
    {
        MEMSET (&(gaIgpSrcInfo[u2Index]), 0, sizeof (tIgmpPrxySrcInfo));
    }

    /* Reset the global source count */
    gIgmpProxyInfo.u2SourceCount = 0;

    /* RBTree deletion for IP multicast forward entries */
    if (gIgmpProxyInfo.ForwardEntry != NULL)
    {
        RBTreeDestroy (gIgmpProxyInfo.ForwardEntry,
                       (tRBKeyFreeFn) IgpUtilRBTreeFwdEntryFree, 0);
        gIgmpProxyInfo.ForwardEntry = NULL;
    }

    /* RBTree deletion for Group Entries */
    if (gIgmpProxyInfo.GroupEntry != NULL)
    {
        RBTreeDestroy (gIgmpProxyInfo.GroupEntry,
                       (tRBKeyFreeFn) IgpUtilRBTreeGroupEntryFree, 0);
        gIgmpProxyInfo.GroupEntry = NULL;
    }

    /* Deleting Hash Table for upstrem interface */
    if (gIgmpProxyInfo.pUpIfaceTbl != NULL)
    {
        TMO_HASH_Delete_Table (gIgmpProxyInfo.pUpIfaceTbl, NULL);
        gIgmpProxyInfo.pUpIfaceTbl = NULL;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMainDeleteProxyInfo()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpMainMcastDataPktHandler                           */
/*                                                                           */
/* Description        : This function processes the incoming multicast       */
/*                      data packet                                          */
/*                                                                           */
/* Input(s)           : *pBuffer   - Pointer to the packet buffer            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : IGMP_SUCCESS / IGMP_FAILURE                          */
/*****************************************************************************/
INT4
IgpMainMcastDataPktHandler (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tIgmpPrxyGrpInfo   *pIgpGroupEntry = NULL;
    tIgmpPrxyUpIface   *pIgpUpIfacEntry = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    t_IP                IpPktInfo;
#ifdef MFWD_WANTED
    tMfwdMrpMDPMsg      MfwdMsgHdr;
#endif
    UINT4               u4IfIndex = 0;
    tIgmpIPvXAddr       u4SrcAddr;
    tIgmpIPvXAddr       u4GrpAddr;
    UINT2               u2SrcIndex = 0;
    UINT1               u1SGEntry = IGMP_FALSE;
    BOOL1               bResult = OSIX_FALSE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainMcastDataPktHandler()\r \n");
    MEMSET (&IpPktInfo, 0, sizeof (t_IP));

    /* Check if the forwarding entries in the hardware table is full */
    if (gIgmpProxyInfo.u2MaxHwEntryCnt == IGP_MAX_FWD_ENTRIES)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Forwaring entries count reached the maximum supported"
                  " by the system - packet dropped!!!\n");
        return IGMP_FAILURE;
    }

#ifdef MFWD_WANTED
    CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &MfwdMsgHdr,
                               IGMP_ZERO, sizeof (tMfwdMrpMDPMsg));
    CRU_BUF_Move_ValidOffset (pBuffer, sizeof (tMfwdMrpMDPMsg));
    u4IfIndex = MfwdMsgHdr.u4Iif;
#endif /* MFWD_WANTED */

#ifdef NPAPI_WANTED
    /* Get the port from Buffer */
    GET_IFINDEX_FROMBUF (pBuffer, u4IfIndex);
#endif /* FS_NPAPI */

    /* Get the upstream interface node from the table */
    if (IgpUpIfGetUpIfaceEntry (u4IfIndex, &pIgpUpIfacEntry) != IGMP_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Interface not configured as upstream interface -"
                  " packet dropped!!!\n");
        return IGMP_FAILURE;
    }
    if (pIgpUpIfacEntry->u1RowStatus != IGMP_ACTIVE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Interface not active - packet dropped!!!\n");
        return IGMP_FAILURE;
    }

    /* Get the interface for the purpose of interface parameters */
    pIfaceNode = IgmpGetIfNode (u4IfIndex, IPVX_ADDR_FMLY_IPV4);
    if (pIfaceNode == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "IGMP Interface not found - pkt dropped!!!\n");
        return IGMP_FAILURE;
    }

    /* Extract the IP header from the buffer */
    if (IGMP_EXTRACT_IP_HEADER (&IpPktInfo, pBuffer) == IP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Unable extract IP header from data packet\r\n");
        return IGMP_FAILURE;
    }

    IGMP_IPVX_ADDR_INIT_IPV4 (u4SrcAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(IpPktInfo.u4Src));
    IGMP_IPVX_ADDR_INIT_IPV4 (u4GrpAddr, IGMP_IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &(IpPktInfo.u4Dest));

    /* Get the group entry */
    if (IgpGrpGetGroupEntry (u4GrpAddr, &pIgpGroupEntry) != IGMP_SUCCESS)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                       "Group entry not present for group %s\r\n", IgmpPrintIPvxAddress(u4GrpAddr));
        return IGMP_FAILURE;
    }

    /* Check for (S,G) entry */
    if (IgpFwdGetForwardEntry (u4GrpAddr, u4SrcAddr,
                               &pIgpForwardEntry) == IGMP_SUCCESS)
    {

        if (pIgpForwardEntry->u4IfIndex != u4IfIndex)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                      "Multicast data packet is received on wrong incoming"
                      " interface \r\n");
            return IGMP_FAILURE;
        }

        /* Restart the multicast forwarding entry timer */
        if (TmrRestart (gIgmpProxyInfo.ProxyTmrListId,
                        &(pIgpForwardEntry->EntryTimer.TimerBlk),
                        IGP_FWD_ENTRY_TIMER, IGP_FWD_ENTRY_TIMER_VAL,
                        0) == TMR_FAILURE)
        {

            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "Forward Entry timer restart failed\r\n");
        }

        IgpMainForwardMulticastData (pIgpForwardEntry, pBuffer);
        return IGMP_SUCCESS;
    }

    /* Check if source is present */
    IGP_IS_SOURCE_PRESENT (u4SrcAddr, bResult, u2SrcIndex);

    /* source is not present and filter mode is include it is
     * an unregistered packet */
    if ((bResult == IGMP_FALSE) &&
        (pIgpGroupEntry->u1FilterMode == IGMP_FMODE_INCLUDE))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Group entry filter mode is include and source address"
                  " is not registered\r\n");
        return IGMP_FAILURE;
    }

    if (u2SrcIndex != 0xffff)
    {
        u2SrcIndex = (UINT2) (u2SrcIndex + 1);
    }

    /* If the filter mode is include and if the source is present in
     * the include bitmap create (S,G) entry */
    if (pIgpGroupEntry->u1FilterMode == IGMP_FMODE_INCLUDE)
    {
        OSIX_BITLIST_IS_BIT_SET (pIgpGroupEntry->InclSrcBmp, u2SrcIndex,
                                 IGP_SRC_LIST_SIZE, bResult);
        if (bResult == OSIX_TRUE)
        {
            u1SGEntry = IGMP_TRUE;
        }
    }
    else
    {
        /* If filter mode is exclude and source is not present and there
         * are v1/v2 receivers create an (S, G) entry */
        if (u2SrcIndex == 0xffff)
        {
            u1SGEntry = IGMP_TRUE;
        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (pIgpGroupEntry->ExclSrcBmp,
                                     u2SrcIndex, IGP_SRC_LIST_SIZE, bResult);

            /* If the bit is not set in exclude bitmap */
            if (bResult == OSIX_FALSE)
            {
                u1SGEntry = IGMP_TRUE;
            }
            else
            {
                IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                               "Source %s is present in the exclude list"
                               " for the group %s\r\n", IgmpPrintIPvxAddress(u4SrcAddr),IgmpPrintIPvxAddress(u4GrpAddr));
                return IGMP_FAILURE;
            }
        }
    }

    if (u1SGEntry == IGMP_TRUE)
    {
        /* Create a new (S, G) entry */
        if (IgpFwdUpdateFwdTable (u4GrpAddr, u4SrcAddr, u4IfIndex,
                                  u2SrcIndex, IGP_CREATE_FWD_ENTRY,
                                  &pIgpForwardEntry) != IGMP_SUCCESS)
        {
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                           "Forwarding entry creation failed for "
                           " Source %s and group %s\r\n", IgmpPrintIPvxAddress(u4SrcAddr), IgmpPrintIPvxAddress(u4GrpAddr));
            return IGMP_FAILURE;
        }

        /* Forward the multicast packet to the VLANs on which receivers 
         * are connected */
        IgpMainForwardMulticastData (pIgpForwardEntry, pBuffer);
    }
    else
    {
        /* Just return failure, buffer will get freed 
         * at the calling place since it is an unregistered packet*/
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_ALL_MODULES, IGMP_NAME,
                  "Unregisterd multicast data packe received\r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMainMcastDataPktHandler()\r \n");
    return IGMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgpMainHandleProxyEvents                             */
/*                                                                           */
/* Description        : This function is used to handle all the events       */
/*                      related to IGMP Proxy implementation                 */
/*                                                                           */
/* Input(s)           : u4Event                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IgpMainHandleProxyEvents (UINT4 u4Event)
{
    tIgmpQMsg          *pQMsg = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainHandleProxyEvents()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        return;
    }

    switch (u4Event)
    {
        case IGP_TIMER_EXP_EVENT:
            IgpTmrExpiryHandler ();
            break;

        case IGP_MCAST_DATA_EVENT:
        case IGP_PBMP_CHANGE_EVENT:

            while (OsixQueRecv (gIgmpProxyInfo.IgpMdpQId,
                                (UINT1 *) &pQMsg,
                                OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
            {
                if (pQMsg->u4MsgType == IGP_MCAST_DATA_EVENT)
                {
                    /* Receive from the multicast data queue and call the IGMP
                     * Proxy Mcast data handler */
                    if (IgpMainMcastDataPktHandler
                        (pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt) != IGMP_SUCCESS)
                    {
                        /* Release the packet buffer here only when it is 
                         * failure */
                        CRU_BUF_Release_MsgBufChain (pQMsg->IgmpQMsgParam.
                                                     IgmpPktInfo.pPkt, FALSE);
                    }
                }
                else
                {
#ifdef NPAPI_WANTED
                    /* Call the port bitmap change event handler */
                    IgpNpHandlePortBmpChange
                        (pQMsg->IgmpQMsgParam.IgpPbmpChangeMsg.MacAddr,
                         pQMsg->IgmpQMsgParam.IgpPbmpChangeMsg.u2VlanId);
#endif
                }

                /* Release the Q Msg */
                IGMP_QMSG_FREE (pQMsg);
            }
            break;

#ifdef MFWD_WANTED
        case IGP_MFWD_ENABLE_EVENT:
            IgpMfwdHandleEnable ();
            break;

        case IGP_MFWD_DISABLE_EVENT:
            IgpMfwdHandleDisable ();
            break;
#endif
        default:
            break;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMainHandleProxyEvents()\r \n");
    return;
}

/*****************************************************************************/
/* Function Name      : IgpMainForwardMulticastData                          */
/*                                                                           */
/* Description        : This function forwards the multicast data packet     */
/*                      to the OIF list                                      */
/*                                                                           */
/* Input(s)           : pIgpForwardEntry - Pointer to the forwarding entry   */
/*                      pBuffer  - Pointer to the packet buffer              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpMainForwardMulticastData (tIgmpPrxyFwdEntry * pIgpForwardEntry,
                             tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    UINT4               u4Dest = 0;
    UINT4               u4Src = 0;
    tIgmpIface         *pIgmpIfNode = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    UINT2               u2Len = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMainForwardMulticastData()\r \n");
    CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &u2Len, 2, 2);

    u2Len = OSIX_NTOHS (u2Len);

    IGMP_IP_COPY_FROM_IPVX (&u4Dest, (pIgpForwardEntry->u4GrpIpAddr),
                            IPVX_ADDR_FMLY_IPV4);

    /* Scan through the outgoing interface list and send the multicast packet
     * on each OIF */
    TMO_SLL_Scan (&(pIgpForwardEntry->OifList), pIgpOifNode, tIgmpPrxyOif *)
    {
        pIgmpIfNode = IgmpGetIfNode (pIgpOifNode->u4IfIndex,
                                     IPVX_ADDR_FMLY_IPV4);
        if (NULL == pIgmpIfNode)
        {
            continue;
        }
        IGMP_IP_COPY_FROM_IPVX (&u4Src, pIgmpIfNode->u4IfAddr,
                                IGMP_IPVX_ADDR_FMLY_IPV4);
        if (IgpPortSocketSend (pBuffer, u4Src,
                               u4Dest, u2Len, IGMP_FALSE) == IGMP_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
            return;
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuffer, IGMP_ZERO);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMainForwardMulticastData()\r \n");
    return;
}
