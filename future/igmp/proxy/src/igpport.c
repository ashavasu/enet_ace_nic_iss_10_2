/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: igpport.c,v 1.15 2015/02/13 11:13:54 siva Exp $
 * 
 ********************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : igpport.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy Porting Module                      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains porting functions for       */
/*                            for IGMP Proxy module                          */
/*---------------------------------------------------------------------------*/
#include "igpinc.h"
#include "fssocket.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_IO_MODULE;
#endif
/*gau1IGPRawPkt is used to hold the IGMP packet info being sent out all IGMP capable interfaces*/
PRIVATE UINT1       gau1IGPRawPkt[IGMP_MAX_MTU_SIZE];

/*****************************************************************************/
/* Function Name      : IgpPortHandleMcastDataPkt                            */
/*                                                                           */
/* Description        : This function will post a multicast data packet      */
/*                      receive event to the IGMP task                       */
/*                                                                           */
/* Input(s)           : *pBuffer - Pointer to the packet buffer              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgpPortHandleMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuffer)
{
    tIgmpQMsg          *pQMsg = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpPortHandleMcastDataPkt()\r \n");
    if (IGMP_PROXY_STATUS () == IGMP_DISABLE)
    {
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        return;
    }

    pQMsg->u4MsgType = IGP_MCAST_DATA_EVENT;
    pQMsg->IgmpQMsgParam.IgmpPktInfo.pPkt = pBuffer;

    /* Enqueue the buffer to IGMP task */
    if (OsixQueSend (gIgmpProxyInfo.IgpMdpQId,
                     (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE | IGMP_ALL_MODULES, IGMP_NAME,
                  "Enqueuing Multicast data packet from IP to IGMP - FAILED \n");
        /* Free the CRU buffer and QMSG */
        CRU_BUF_Release_MsgBufChain (pBuffer, FALSE);
        IGMP_QMSG_FREE (pQMsg);
        return;
    }

    /* Send a EVENT to IGMP Task */
    OsixEvtSend (gIgmpTaskId, IGP_MCAST_DATA_EVENT);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpPortHandleMcastDataPkt()\r \n");
    return;
}

#ifdef MFWD_WANTED
/*****************************************************************************/
/* Function Name      : IgpPortMfwdHandleStatus                              */
/*                                                                           */
/* Description        : This function will handle the enable/disable event   */
/*                      form MFWD                                            */
/*                                                                           */
/* Input(s)           : u4Status - MFWD_STATUS_DISABLED/ MFWD_STATUS_ENABLED */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
IgpPortMfwdHandleStatus (UINT4 u4Event)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpPortMfwdHandleStatus()\r \n");
    if (u4Event == MFWD_TO_MRP_STATUS_ENABLED_EVENT)
    {
        u4Event = IGP_MFWD_ENABLE_EVENT;
    }
    else
    {
        u4Event = IGP_MFWD_DISABLE_EVENT;
    }

    /* Send a EVENT to IGMP Task */
    OsixEvtSend (gIgmpTaskId, u4Event);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpPortMfwdHandleStatus()\r \n");
    return SUCCESS;
}
#endif

/****************************************************************************/
/* Function           : IgpPortSocketSend                                   */
/*                    :                                                     */
/* Descriptio         : This function will send IGMP control and data       */
/*                      packets on to IGMP capable interfaces               */
/*                                                                          */
/* Input(s)           : pBuffer - pointer to packet buffer                  */
/*                    : u4Src - Source IP address                           */
/*                    : u4Dest - Destination Ip address                     */
/*                    : u2Len - Length of PDU                               */
/*                    : u1CntlPkt - Whether the packet is a control packet  */
/*                                  or not                                  */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : IGMP_SUCCESS/IGMP_FAILURE                           */
/****************************************************************************/
INT4
IgpPortSocketSend (tCRU_BUF_CHAIN_HEADER * pBuffer, UINT4 u4Src,
                   UINT4 u4Dest, UINT2 u2Len, UINT1 u1CntlPkt)
{
    struct sockaddr_in  DestAddr;
    struct in_addr      IfAddr;
#ifdef LNXIP4_WANTED
    UINT1               u1OptVal = IGMP_TRUE;
#endif
    INT4                i4OptVal = IGMP_TRUE;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpPortSocketSend()\r \n");
    MEMSET (&(DestAddr), IGMP_ZERO, sizeof (struct sockaddr_in));

    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (u4Dest);

    IfAddr.s_addr = OSIX_HTONL (u4Src);

    MEMSET (&gau1IGPRawPkt, IGMP_ZERO, IGMP_MAX_MTU_SIZE);

    if (CRU_BUF_Copy_FromBufChain (pBuffer, (UINT1 *) &gau1IGPRawPkt,
                                   IGMP_ZERO, u2Len) == CRU_FAILURE)
    {
        return IGMP_FAILURE;
    }

#ifdef LNXIP4_WANTED
    if (u1CntlPkt == IGMP_TRUE)
    {
        /* for control packets, set TTL option */
        if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                               IP_MULTICAST_TTL, &u1OptVal,
                               sizeof (u1OptVal)) < IGMP_ZERO)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                      "setsockopt failed for IP_MULTICAST_TTL\r\n");
            return IGMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u1CntlPkt);
#endif
    /* Do not append IP header, since we are providing complete packet, 
     * including IP header */
    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           IP_HDRINCL, &i4OptVal,
                           sizeof (i4OptVal)) < IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Setsockopt failed for IP_HDRINCL\r\n");
        return IGMP_FAILURE;
    }

    if (IGMP_SET_SOCK_OPT (gIgmpConfig.i4SocketId, IPPROTO_IP,
                           IP_MULTICAST_IF, (char *) &IfAddr,
                           sizeof (struct in_addr)) < IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Setsockopt Failed for IP_MULTICAST_IF\n");
        return IGMP_FAILURE;
    }

    if (IGMP_SEND_TO (gIgmpConfig.i4SocketId, &gau1IGPRawPkt, u2Len, IGMP_ZERO,
                      (struct sockaddr *) &DestAddr,
                      sizeof (struct sockaddr_in)) < IGMP_ZERO)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Sendto failed while attempting to" "send control packet \n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpPortSocketSend()\r \n");
    return IGMP_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function     : IgpPortBmpChgNotify                                        */
/*                                                                           */
/* Description  : This function will post an event to IGMP task indicating   */
/*                the port bitmap change for a Group MAC address and VLAN ID */
/*                                                                           */
/* Input        : GrpMacAddr   - Multicast group MAC address                 */
/*                u2VlanId     - VLAN Identifier                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
IgpPortBmpChgNotify (tMacAddr GrpMacAddr, UINT2 u2VlanId)
{
    tIgmpPrxyPbmpChg    IgpPbmpChange;
    tIgmpQMsg          *pQMsg = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpPortBmpChgNotify()\r \n");
    /* Validate the CFA interface index for the given VLAN id */
    if (CfaGetVlanInterfaceIndex (u2VlanId) == CFA_INVALID_INDEX)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Invalid VLAN ID passed for send port bitmap chaneg event\r\n");
        return;
    }

    /* Copy the Group MAC address and VLAN ID */
    MEMCPY (IgpPbmpChange.MacAddr, GrpMacAddr, (sizeof (tMacAddr)));
    IgpPbmpChange.u2VlanId = u2VlanId;

    if (IGMP_MEM_ALLOCATE (gIgmpMemPool.IgmpQPoolId, pQMsg, tIgmpQMsg) == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Memory allocation for IGMP Q for posting bitmap"
                  " change event failed\r\n");
        return;
    }

    pQMsg->u4MsgType = IGP_PBMP_CHANGE_EVENT;
    pQMsg->IgmpQMsgParam.IgpPbmpChangeMsg = IgpPbmpChange;

    if (OsixQueSend (gIgmpProxyInfo.IgpMdpQId,
                     (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Enqueing port bitmap change event to IGMP task failed\r\n");
        IGP_RELEASE_MEM_BLOCK (gIgmpMemPool.IgmpQPoolId, pQMsg);
        return;
    }

    if (OsixEvtSend (gIgmpTaskId, IGP_PBMP_CHANGE_EVENT) != OSIX_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "Send Event failed for port bitmap change\r\n");
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpPortBmpChgNotify()\r \n");
    return;
}
#endif

/*****************************************************************************/
/* Function Name      : IgpPortIsIgmpPrxyEnabled                             */
/*                                                                           */
/* Description        : This function will check whether the source          */
/*                      information is used across groups                    */
/*                                                                           */
/* Input(s)           : u2SrcIndex     - Source Index                        */
/*                                                                           */
/* Output(s)          : pu1EntryUsed  - IGMP_TRUE/ IGMP_FALSE                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
UINT1
IgpPortIsIgmpPrxyEnabled (VOID)
{
    if (IGMP_PROXY_STATUS () == IGMP_ENABLE)
    {
        return 1;
    }
    return 0;
}
