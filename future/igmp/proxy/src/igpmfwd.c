/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igpmfwd.c,v 1.8 2015/02/13 11:13:54 siva Exp $
 * 
 ********************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : igpmfwd.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : IGMP Proxy                                     */
/*    MODULE NAME           : IGMP Proxy MFWD interaction                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains routines related to         */
/*                            IGMP Proxy and MFWD interaction                */
/*---------------------------------------------------------------------------*/

#include "igpinc.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_GRP_MODULE;
#endif

/****************************************************************************/
/* Function Name         : IgpMfwdHandleEnable                              */
/*                                                                          */
/* Description           : This function installs all the forwading entries */
/*                         in MFWD                                          */
/*                                                                          */
/* Input (s)             : None                                             */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : None                                             */
/****************************************************************************/
VOID
IgpMfwdHandleEnable (VOID)
{
    tMrpUpdMesgHdr      MsgHdr;
    tIgmpIface         *pIfaceNode = NULL;
    tTMO_SLL_NODE      *pTempIfNode = NULL;
    tIgmpPrxyFwdEntry  *pIgpForwardEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT4               u4Offset;
    UINT4               u4IfCount;
    UINT4               i4Status = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMfwdHandleEnable()\r \n");
    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));

    /* Check if MFWD is already enabled */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_ENABLED)
    {
        return;
    }

    gIgmpProxyInfo.u1MfwdStatus = MFWD_STATUS_ENABLED;

    MsgHdr.u2OwnerId = IGMP_PROXY_ID;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIF_CMD;

    /* Update all the IGMP capable interfaces to MFWD */
    u4IfCount = TMO_SLL_Count (&gIgmpIfInfo.IfGetNextList);

    if (u4IfCount == 0)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "No IGMP cpapable inetrfaces are available\r\n");
        return;
    }

    /* Allocate CRU buffer to the size of (MFWD Header + 
     * Number of interfaces + Interface count * sizeof (UINT4)) */

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) + sizeof (UINT4) *
                                              u4IfCount, 0);
    if (pIfUpdBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Buffer alloction failed to intimate MFWD on"
                  " entry creation\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
	      IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"to intimate MFWD on entry creation\r\n");
        return;
    }

    /* Copy the MFWD message Header */
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));

    /* Copy the interface count */
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                               sizeof (tMrpUpdMesgHdr), sizeof (UINT4));

    u4Offset = sizeof (tMrpUpdMesgHdr) + sizeof (UINT4);

    /* Scan through the interface list and update all the interface indices */
    TMO_SLL_Scan (&gIgmpIfInfo.IfGetNextList, pTempIfNode, tTMO_SLL_NODE *)
    {
        pIfaceNode = IGMP_GET_BASE_PTR (tIgmpIface, IfGetNextLink, pTempIfNode);

        CRU_BUF_Copy_OverBufChain (pIfUpdBuf,
                                   (UINT1 *) &pIfaceNode->u4IfIndex,
                                   u4Offset, sizeof (UINT4));
        u4Offset += sizeof (UINT4);
    }

    MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Enqueing IGMP interface information to MFWD failed \r\n");
        return;
    }

    /* Update MFWD for the route entries to be reinstalled */
    if (gIgmpProxyInfo.ForwardEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "No Multicasting forwarding entries are not present\r\n");
        return;
    }

    /* Install all the forwarding entries to MFWD */
    pRBElem = RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry, pRBElem, NULL);

        pIgpForwardEntry = (tIgmpPrxyFwdEntry *) pRBElem;

        /* Create the forwarding entry in the MFWD module */
        if (IgpMfwdCreateRouteEntry (pIgpForwardEntry) != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                      "Creation of forwarding entry to MFWD failed\r\n");
            return;
        }

        pRBElem = pRBNextElem;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMfwdHandleEnable()\r \n");
    return;
}

/****************************************************************************/
/* Function Name         : IgpMfwdHandleDisable                             */
/*                                                                          */
/* Description           : This function handles MFWD module disable        */
/*                                                                          */
/* Input (s)             : None                                             */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : None                                             */
/****************************************************************************/
VOID
IgpMfwdHandleDisable (VOID)
{
    gIgmpProxyInfo.u1MfwdStatus = MFWD_STATUS_DISABLED;
    return;
}

/****************************************************************************/
/* Function Name         : IgpMfwdCreateRouteEntry                          */
/*                                                                          */
/* Description           : This function posts a message to MFWD to create  */
/*                         a multicast forwarding entry                     */
/*                                                                          */
/* Input (s)             : pIgpForwardEntry - Pointer to the forwarding     */
/*                         entry                                            */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : IGMP_SUCCESS/IGMP_FAILURE                        */
/****************************************************************************/
INT4
IgpMfwdCreateRouteEntry (tIgmpPrxyFwdEntry * pIgpForwardEntry)
{
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tMrpOifNode         UpdOifNode;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    UINT4               u4SrcIpAddr;
    UINT4               u4GrpIpAddr;
    UINT4               u4SrcTmp = 0;
    UINT4               u4GrpTmp = 0;
    UINT4               u4OifCount = 0;
    UINT4               u4Count = 0;
    INT4                i4Status = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMfwdCreateRouteEntry()\r \n");
    MEMSET (&RtData, 0, sizeof (tMrtUpdData));
    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));

    /* Check if MFWD is enabled in the system */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "MFWD is disabled in the system\r\n");
        return IGMP_SUCCESS;
    }

    /* fill the MFWD update message header */
    MsgHdr.u2OwnerId = IGMP_PROXY_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_CREATE_CMD;
    MsgHdr.u1NumEntries = 1;

    IGMP_IP_COPY_FROM_IPVX (&u4SrcTmp, pIgpForwardEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4GrpTmp, pIgpForwardEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    u4SrcIpAddr = OSIX_HTONL (u4SrcTmp);
    u4GrpIpAddr = OSIX_HTONL (u4GrpTmp);
    /* Fill the data required for the creation of entry */
    IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4SrcIpAddr);
    IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4GrpIpAddr);

    RtData.u1ChkRpfFlag = 1;
    RtData.u4Iif = pIgpForwardEntry->u4IfIndex;
    RtData.u4GrpMaskLen = (UINT4) pIgpForwardEntry->u4GrpIpAddr.u1AddrLen;

    u4OifCount = TMO_SLL_Count (&(pIgpForwardEntry->OifList));
    RtData.u4OifCnt = u4OifCount;

    RtData.u1DeliverMDP = IGP_MFWD_DONT_DELIVER_MDP;

    /* Allocate the buffer to send a create entry message to MFWD */
    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               (u4OifCount *
                                                sizeof (tMrpOifNode)), 0);
    if (pMrtUpdBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Buffer alloction failed to intimate MFWD on"
                  " entry creation\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"to intimate MFWD on entry creation\r\n");
        return IGMP_FAILURE;
    }

    /* Copy the MFWD header information */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));

    /* Copy the Route information */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    /* Add all the oifs into the update message for the MFWD */
    u4Count = 0;
    TMO_SLL_Scan (&(pIgpForwardEntry->OifList), pIgpOifNode, tIgmpPrxyOif *)
    {
        UpdOifNode.u4OifIndex = pIgpOifNode->u4IfIndex;
        UpdOifNode.u4NextHopState = IGP_OIF_FORWARDING;
        CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdOifNode,
                                   sizeof (tMrpUpdMesgHdr) +
                                   sizeof (tMrtUpdData) +
                                   (u4Count * sizeof (tMrpOifNode)),
                                   sizeof (tMrpOifNode));

        u4Count++;
    }

    /* Enqueue the message to MFWD */
    MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Enqueing creation of forwarding entry"
                  " to MFWD failed \r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMfwdCreateRouteEntry()\r \n");
    return IGMP_SUCCESS;
}

/****************************************************************************/
/* Function Name         : IgpMfwdDeleteRouteEntry                          */
/*                                                                          */
/* Description           : This function posts a message to MFWD to Delete  */
/*                         a multicast forwarding entry                     */
/*                                                                          */
/* Input (s)             : pIgpForwardEntry - Pointer to the forwarding     */
/*                         entry                                            */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : IGMP_SUCCESS/IGMP_FAILURE                        */
/****************************************************************************/
INT4
IgpMfwdDeleteRouteEntry (tIgmpPrxyFwdEntry * pIgpForwardEntry)
{
    tMrpUpdMesgHdr      MsgHdr;
    tMrtUpdData         RtData;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    UINT4               u4SrcIpAddr;
    UINT4               u4GrpIpAddr;
    UINT4               u4SrcTmp = 0;
    UINT4               u4GrpTmp = 0;
    INT4                i4Status = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMfwdDeleteRouteEntry()\r \n");
    MEMSET (&RtData, 0, sizeof (tMrtUpdData));
    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));

    /* Check if MFWD is enabled in the system */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "MFWD is disabled in the system\r\n");
        return IGMP_SUCCESS;
    }

    /* fill the MFWD update message header */
    MsgHdr.u2OwnerId = IGMP_PROXY_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_DELETE_CMD;
    MsgHdr.u1NumEntries = 1;

    IGMP_IP_COPY_FROM_IPVX (&u4SrcTmp, pIgpForwardEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4GrpTmp, pIgpForwardEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    u4SrcIpAddr = OSIX_HTONL (u4SrcTmp);
    u4GrpIpAddr = OSIX_HTONL (u4GrpTmp);
    /* Fill the data required for the creation of entry */
    IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4SrcIpAddr);
    IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4GrpIpAddr);

    RtData.u4Iif = pIgpForwardEntry->u4IfIndex;

    /* Allocate the buffer to send a create entry message to MFWD */
    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               MsgHdr.u1NumEntries, 0);
    if (pMrtUpdBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Buffer alloction failed to intimate MFWD on"
                  " entry deletion\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"to intimate MFWD on entry deletion\r\n");
        return IGMP_FAILURE;
    }

    /* Copy the MFWD header information */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));

    /* Copy the Route information */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    /* Enqueue the message to MFWD */
    MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Enqueing deletion of forwarding entry"
                  " to MFWD failed \r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMfwdDeleteRouteEntry()\r \n");
    return IGMP_SUCCESS;
}

/****************************************************************************/
/* Function Name         : IgpMfwdAddOif                                    */
/*                                                                          */
/* Description           : This function posts a message to MFWD to add an  */
/*                         Oif to a existing multicast forwarding entry     */
/*                                                                          */
/* Input (s)             : pIgpForwardEntry - Pointer to the forwarding     */
/*                         entry                                            */
/*                         u4OifIndex - Outgoing interface index            */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : IGMP_SUCCESS/IGMP_FAILURE                        */
/****************************************************************************/
INT4
IgpMfwdAddOif (tIgmpPrxyFwdEntry * pIgpForwardEntry, UINT4 u4OifIndex)
{
    tMrpUpdMesgHdr      MsgHdr;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    tMrtUpdData         RtData;
    tMrpOifNode         UpdOifNode;
    UINT4               u4SrcIpAddr;
    UINT4               u4GrpIpAddr;
    UINT4               u4SrcTmp = 0;
    UINT4               u4GrpTmp = 0;
    INT4                i4Status = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMfwdAddOif()\r \n");
    MEMSET (&RtData, 0, sizeof (tMrtUpdData));
    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));
    MEMSET (&UpdOifNode, 0, sizeof (tMrpOifNode));

    /* Check if MFWD is enabled */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "MFWD is disabled in the system\r\n");
        return IGMP_SUCCESS;
    }

    MsgHdr.u2OwnerId = IGMP_PROXY_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_ADD_OIF_CMD;
    MsgHdr.u1NumEntries = 1;
    IGMP_IP_COPY_FROM_IPVX (&u4SrcTmp, pIgpForwardEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4GrpTmp, pIgpForwardEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    u4SrcIpAddr = OSIX_HTONL (u4SrcTmp);
    u4GrpIpAddr = OSIX_HTONL (u4GrpTmp);
    /* Fill the data required for the creation of entry */
    IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4SrcIpAddr);
    IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4GrpIpAddr);

    RtData.u4Iif = pIgpForwardEntry->u4IfIndex;
    RtData.u4OifCnt = 1;

    pMrtUpdBuf =
        CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                      sizeof (tMrtUpdData) +
                                      (RtData.u4OifCnt *
                                       sizeof (tMrpOifNode)), 0);

    if (pMrtUpdBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Buffer alloction failed to intimate MFWD "
                  " for Oif Addition\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"to intimate MFWD for Oif Addition\r\n");
        return IGMP_FAILURE;
    }

    /* Copy the MFWD message header */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));

    /* Copy the Forward entry information */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    /* update Oif params */
    UpdOifNode.u4NextHopState = IGP_OIF_FORWARDING;
    UpdOifNode.u4OifIndex = u4OifIndex;

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdOifNode,
                               sizeof (tMrpUpdMesgHdr) + sizeof (tMrtUpdData),
                               sizeof (tMrpOifNode));

    /* Enqueue the message to MFWD */
    MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Enqueing OIF addition to forwarding entry"
                  " to MFWD failed \r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMfwdAddOif()\r \n");
    return IGMP_SUCCESS;
}

/****************************************************************************/
/* Function Name         : IgpMfwdDelOif                                    */
/*                                                                          */
/* Description           : This function posts a message to MFWD to delete  */
/*                         an Oif from the existing forwarding entry        */
/*                                                                          */
/* Input (s)             : pIgpForwardEntry - Pointer to the forwarding     */
/*                         entry                                            */
/*                         u4OifIndex - Outgoing interface index            */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : IGMP_SUCCESS/IGMP_FAILURE                        */
/****************************************************************************/
INT4
IgpMfwdDelOif (tIgmpPrxyFwdEntry * pIgpForwardEntry, UINT4 u4OifIndex)
{
    tMrpUpdMesgHdr      MsgHdr;
    tCRU_BUF_CHAIN_HEADER *pMrtUpdBuf = NULL;
    tMrtUpdData         RtData;
    tMrpOifNode         UpdOifNode;
    UINT4               u4SrcIpAddr;
    UINT4               u4GrpIpAddr;
    UINT4               u4SrcTmp = 0;
    UINT4               u4GrpTmp = 0;
    INT4                i4Status = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMfwdDelOif()\r \n");
    MEMSET (&RtData, 0, sizeof (tMrtUpdData));
    MEMSET (&MsgHdr, 0, sizeof (tMrpUpdMesgHdr));
    MEMSET (&UpdOifNode, 0, sizeof (tMrpOifNode));

    /* Check if MFWD is enabled */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_INIT_SHUT_MODULE, IGMP_NAME,
                  "MFWD is disabled in the system\r\n");
        return IGMP_SUCCESS;
    }

    MsgHdr.u2OwnerId = IGMP_PROXY_ID;
    MsgHdr.u1UpdType = MFWD_MRP_MRT_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ENTRY_DELETE_OIF_CMD;
    MsgHdr.u1NumEntries = 1;
    IGMP_IP_COPY_FROM_IPVX (&u4SrcTmp, pIgpForwardEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (&u4GrpTmp, pIgpForwardEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    u4SrcIpAddr = OSIX_HTONL (u4SrcTmp);
    u4GrpIpAddr = OSIX_HTONL (u4GrpTmp);
    /* Fill the data required for the creation of entry */
    IPVX_ADDR_INIT_IPV4 (RtData.SrcAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4SrcIpAddr);
    IPVX_ADDR_INIT_IPV4 (RtData.GrpAddr, IPVX_ADDR_FMLY_IPV4,
                         (UINT1 *) &u4GrpIpAddr);

    RtData.u4Iif = pIgpForwardEntry->u4IfIndex;
    RtData.u4OifCnt = 1;

    pMrtUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                               sizeof (tMrtUpdData) +
                                               (RtData.u4OifCnt *
                                                sizeof (tMrpOifNode)), 0);
    if (pMrtUpdBuf == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Buffer alloction failed to intimate MFWD "
                  " for Oif deletion\r\n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"to intimate MFWD for Oif Addition\r\n");
        return IGMP_FAILURE;
    }

    /* Copy the MFWD message header */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &MsgHdr, 0,
                               sizeof (tMrpUpdMesgHdr));

    /* Copy the Forward entry information */
    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &RtData,
                               sizeof (tMrpUpdMesgHdr), sizeof (tMrtUpdData));

    /* update Oif params */
    UpdOifNode.u4NextHopState = IGP_OIF_DELETED;
    UpdOifNode.u4OifIndex = u4OifIndex;

    CRU_BUF_Copy_OverBufChain (pMrtUpdBuf, (UINT1 *) &UpdOifNode,
                               sizeof (tMrpUpdMesgHdr) + sizeof (tMrtUpdData),
                               sizeof (tMrpOifNode));

    /* Enqueue the message to MFWD */
    MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD (pMrtUpdBuf, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Enqueing OIF deletion to forwarding entry"
                  " to MFWD failed \r\n");
        return IGMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMfwdDelOif()\r \n");
    return IGMP_SUCCESS;
}

/****************************************************************************/
/* Function Name         : IgpMfwdAddIface                                  */
/*                                                                          */
/* Description           : This function creates a update buffer for adding */
/*                         an interface to MFWD                             */
/*                                                                          */
/* Input (s)             : u4IfIndex - Interface Index                      */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : IGMP_SUCCESS/IGMP_FAILURE                        */
/****************************************************************************/
INT4
IgpMfwdAddInterface (UINT4 u4IfIndex)
{
    tMrpUpdMesgHdr      MsgHdr;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;
    UINT4               u4IfCount = 0;
    INT4                i4Status = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMfwdAddInterface()\r \n");
    /* Check if MFWD is enabled */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "MFWD is disabled in the system\r\n");
        return IGMP_SUCCESS;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = IGMP_PROXY_ID;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_ADD_OWNERIF_CMD;
    MsgHdr.u1NumEntries = IGMP_ONE;
    u4IfCount = IGMP_ONE;

    /* Fill basic data required for the IF Update */

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) +
                                              sizeof (UINT4) * u4IfCount,
                                              IGMP_ZERO);
    if (pIfUpdBuf == NULL)
    {
        /* Check if Allocation success */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Failure in MFWD Interface Update Message "
                  "buffer Allocation \n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"for MFWD Interface Update Message\n");
        return IGMP_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, IGMP_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                               sizeof (tMrpUpdMesgHdr), sizeof (UINT4));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                               sizeof (tMrpUpdMesgHdr) + sizeof (UINT4),
                               sizeof (UINT4));

    MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Enqueing IGMP interface information to MFWD failed \r\n");
        return i4Status;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMfwdAddInterface()\r \n");
    return i4Status;
}

/****************************************************************************/
/* Function Name         : IgpMfwdDelInterface                              */
/*                                                                          */
/* Description           : This function Deletes the specified interface    */
/*                         from the MFWD                                    */
/*                                                                          */
/* Input (s)             : u4IfIndex - Interface Index                      */
/*                                                                          */
/* Output (s)            : None                                             */
/*                                                                          */
/* Returns               : None                                             */
/****************************************************************************/
INT4
IgpMfwdDelInterface (UINT4 u4IfIndex)
{
    tMrpUpdMesgHdr      MsgHdr;
    tCRU_BUF_CHAIN_HEADER *pIfUpdBuf = NULL;
    UINT4               u4IfCount;
    INT4                i4Status = IGMP_SUCCESS;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering IgpMfwdDelInterface()\r \n");
    /* Check if MFWD is enabled */
    if (gIgmpProxyInfo.u1MfwdStatus == MFWD_STATUS_DISABLED)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "MFWD is disabled in the system\r\n");
        return IGMP_SUCCESS;
    }

    /* fill the update message header */
    MsgHdr.u2OwnerId = IGMP_PROXY_ID;
    MsgHdr.u1UpdType = MFWD_MRP_OIM_UPDATE;
    MsgHdr.u1UpdCmd = MFWD_DELETE_OWNERIF_CMD;
    MsgHdr.u1NumEntries = IGMP_ONE;
    u4IfCount = IGMP_ONE;

    pIfUpdBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tMrpUpdMesgHdr) +
                                              sizeof (UINT4) +
                                              sizeof (UINT4) * u4IfCount,
                                              IGMP_ZERO);
    if (pIfUpdBuf == NULL)
    {
        /* Check if Allocation success */
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_CNTRL_MODULE, IGMP_NAME,
                  "Failure in MFWD Interface Update Message "
                  "buffer Allocation \n");
	SYSLOG_IGMP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, IGMP_DBG_FLAG, IGMP_BUFFER_MODULE,
              IGMP_NAME,IgmpSysErrString[SYS_LOG_BUFFER_MEM_ALLOC_FAIL],
		"for MFWD Interface Update Message\n");
        return IGMP_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &MsgHdr, IGMP_ZERO,
                               sizeof (tMrpUpdMesgHdr));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfCount,
                               sizeof (tMrpUpdMesgHdr), sizeof (UINT4));
    CRU_BUF_Copy_OverBufChain (pIfUpdBuf, (UINT1 *) &u4IfIndex,
                               sizeof (tMrpUpdMesgHdr) + sizeof (UINT4),
                               sizeof (UINT4));

    MFWD_ENQUEUE_IGP_UPDATE_TO_MFWD (pIfUpdBuf, i4Status);

    if (i4Status == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_DATA_MODULE, IGMP_NAME,
                  "Enqueing IGMP interface information to MFWD failed \r\n");
        return i4Status;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  IgpMfwdDelInterface()\r \n");
    return i4Status;
}
