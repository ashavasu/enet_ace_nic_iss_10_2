/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsigplw.c,v 1.25 2015/02/28 12:17:45 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "igpinc.h"
# include  "igpcli.h"

#ifdef TRACE_WANTED
static UINT2        u2IgmpTrcModule = IGMP_MGMT_MODULE;
#endif

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIgmpProxyStatus
 Input       :  The Indices

                The Object 
                retValFsIgmpProxyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyStatus (INT4 *pi4RetValFsIgmpProxyStatus)
{
    *pi4RetValFsIgmpProxyStatus = IGMP_PROXY_STATUS ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyForwardingTblEntryCnt
 Input       :  The Indices

                The Object 
                retValFsIgmpProxyForwardingTblEntryCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyForwardingTblEntryCnt (INT4
                                        *pi4RetValFsIgmpProxyForwardingTblEntryCnt)
{
    UINT4               u4Count = IGMP_ZERO;

    *pi4RetValFsIgmpProxyForwardingTblEntryCnt = IGMP_ZERO;

    RBTreeCount (gIgmpProxyInfo.ForwardEntry, &u4Count);
    *pi4RetValFsIgmpProxyForwardingTblEntryCnt = (INT4) u4Count;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsIgmpProxyStatus
 Input       :  The Indices

                The Object 
                setValFsIgmpProxyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpProxyStatus (INT4 i4SetValFsIgmpProxyStatus)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhSetFsIgmpProxyStatus()\r \n");
    if (gIgmpConfig.u1IgmpStatus == IGMP_DISABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP is globally disabled in the system\r\n");
        CLI_SET_ERR (CLI_IGP_IGMP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (IGMP_PROXY_STATUS () == i4SetValFsIgmpProxyStatus)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP Proxy is already enabled/disabled in the"
                  " system\r\n");
        return SNMP_SUCCESS;
    }

    if (i4SetValFsIgmpProxyStatus == IGMP_ENABLE)
    {
#ifdef PIM_WANTED
        if (PimIsPimEnabled () == PIMSM_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "PIM is enabled;"
                      "IGMP Proxy and PIM cannot co-exist \r\n");
            CLI_SET_ERR (CLI_IGP_PIM_DVMRP_ERR);
            return SNMP_FAILURE;
        }
#endif /* PIM_WANTED */

#ifdef DVMRP_WANTED
        if (DvmrpIsDvmrpEnabled () == DVMRP_STATUS_ENABLED)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "DVMRP is enabled;"
                      "IGMP Proxy and DVMRP cannot co-exist \r\n");
            CLI_SET_ERR (CLI_IGP_PIM_DVMRP_ERR);
            return SNMP_FAILURE;
        }
#endif /* DVMRP_WANTED */
    }

    if (i4SetValFsIgmpProxyStatus == IGMP_ENABLE)
    {
        if (IgpMainStatusEnable () != IGMP_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Enabling of IGMP Proxy Module failed \r\n");
            return SNMP_FAILURE;
        }
    }
    else
    {
        IgpMainStatusDisable ();
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhSetFsIgmpProxyStatus()\r \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIgmpProxyStatus
 Input       :  The Indices

                The Object 
                testValFsIgmpProxyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpProxyStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsIgmpProxyStatus)
{
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhTestv2FsIgmpProxyStatus()\r \n");
    if (gIgmpConfig.u1IgmpStatus == IGMP_DISABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP is globally disabled in the system\r\n");
        CLI_SET_ERR (CLI_IGP_IGMP_DISABLED_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (IGMP_PROXY_STATUS () == i4TestValFsIgmpProxyStatus)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP Proxy is already enabled/disabled in the"
                  " system\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_SUCCESS;
    }

    if (i4TestValFsIgmpProxyStatus == IGMP_ENABLE)
    {
#ifdef PIM_WANTED
        if (PimIsPimEnabled () == PIMSM_SUCCESS)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "PIM is enabled;"
                      "IGMP Proxy and PIM cannot co-exist \r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IGP_PIM_DVMRP_ERR);
            return SNMP_FAILURE;
        }
#endif /* PIM_WANTED */

#ifdef DVMRP_WANTED
        if (DvmrpIsDvmrpEnabled () == DVMRP_STATUS_ENABLED)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "DVMRP is enabled;"
                      "IGMP Proxy and DVMRP cannot co-exist \r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_IGP_PIM_DVMRP_ERR);
            return SNMP_FAILURE;
        }
#endif /* DVMRP_WANTED */
    }

    if ((i4TestValFsIgmpProxyStatus != IGMP_ENABLE) &&
        (i4TestValFsIgmpProxyStatus != IGMP_DISABLE))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Invalid value for IGMP Proxy Status \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhTestv2FsIgmpProxyStatus()\r \n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIgmpProxyStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpProxyStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIgmpProxyRtrIfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIgmpProxyRtrIfaceTable
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIgmpProxyRtrIfaceTable (INT4
                                                  i4FsIgmpProxyRtrIfaceIndex)
{
    UINT4               u4Port = IGMP_ZERO;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
              "Entering nmhValidateIndexInstanceFsIgmpProxyRtrIfaceTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Validation of FsIgmpProxyRtrIfaceTable for"
                       " interface index%d failed\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhValidateIndexInstanceFsIgmpProxyRtrIfaceTable()\r \n");
    return SNMP_SUCCESS;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpProxyRtrIfaceTable
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsIgmpProxyRtrIfaceTable (INT4 *pi4FsIgmpProxyRtrIfaceIndex)
{
    UINT4               u4IfIndex = IGMP_ZERO;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFirstIndexFsIgmpProxyRtrIfaceTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetFirstUpIfaceEntry (&pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Get First for FsIgmpProxyRtrIfaceTabl \r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_IFINDEX_FROM_PORT (pIgmpPrxyUpIface->u4IfIndex, &u4IfIndex))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get CFA Index number from to IP Port number\r\n");
        return SNMP_FAILURE;
    }

    *pi4FsIgmpProxyRtrIfaceIndex = (INT4) u4IfIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFirstIndexFsIgmpProxyRtrIfaceTable()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpProxyRtrIfaceTable
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex
                nextFsIgmpProxyRtrIfaceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIgmpProxyRtrIfaceTable (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                         INT4 *pi4NextFsIgmpProxyRtrIfaceIndex)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    UINT4               u4IfIndex = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetNextIndexFsIgmpProxyRtrIfaceTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to convert IP VLAN index in to IP port number\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetNextUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == SNMP_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed in getting the next Upstream Iface entry \r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_IFINDEX_FROM_PORT ((pIgmpPrxyUpIface->u4IfIndex),
                                       &u4IfIndex))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to convert IP port number in to CFA index\r\n");
        return SNMP_FAILURE;
    }

    *pi4NextFsIgmpProxyRtrIfaceIndex = (INT4) u4IfIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetNextIndexFsIgmpProxyRtrIfaceTable()\r \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIgmpProxyRtrIfaceOperVersion
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                retValFsIgmpProxyRtrIfaceOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyRtrIfaceOperVersion (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                      INT4
                                      *pi4RetValFsIgmpProxyRtrIfaceOperVersion)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyRtrIfaceOperVersion()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValFsIgmpProxyRtrIfaceOperVersion =
        (INT4) pIgmpPrxyUpIface->u1OperVersion;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyRtrIfaceOperVersion()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyRtrIfaceCfgOperVersion
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                retValFsIgmpProxyRtrIfaceCfgOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyRtrIfaceCfgOperVersion (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                         INT4
                                         *pi4RetValFsIgmpProxyRtrIfaceCfgOperVersion)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyRtrIfaceCfgOperVersion()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from to CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)

    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValFsIgmpProxyRtrIfaceCfgOperVersion =
        (INT4) pIgmpPrxyUpIface->u1CfgVersion;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyRtrIfaceCfgOperVersion()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyRtrIfacePurgeInterval
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                retValFsIgmpProxyRtrIfacePurgeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyRtrIfacePurgeInterval (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                        INT4
                                        *pi4RetValFsIgmpProxyRtrIfacePurgeInterval)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyRtrIfacePurgeInterval()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValFsIgmpProxyRtrIfacePurgeInterval =
        (INT4) pIgmpPrxyUpIface->u2RtrPurgeInt;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyRtrIfacePurgeInterval()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyRtrIfaceUpTime
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                retValFsIgmpProxyRtrIfaceUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyRtrIfaceUpTime (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                 UINT4 *pu4RetValFsIgmpProxyRtrIfaceUpTime)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    UINT4               u4CurrentSysTime = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyRtrIfaceUpTime()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    OsixGetSysTime (&u4CurrentSysTime);

    *pu4RetValFsIgmpProxyRtrIfaceUpTime =
        (u4CurrentSysTime - pIgmpPrxyUpIface->u4RtrUpTime);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyRtrIfaceUpTime()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyRtrIfaceExpiryTime
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                retValFsIgmpProxyRtrIfaceExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyRtrIfaceExpiryTime (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                     UINT4
                                     *pu4RetValFsIgmpProxyRtrIfaceExpiryTime)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4RemainingTime = IGMP_ZERO;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyRtrIfaceExpiryTime()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from to CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    if (TmrGetRemainingTime (gIgmpProxyInfo.ProxyTmrListId,
                             &(pIgmpPrxyUpIface->IfacePurgeTimer.TimerBlk.
                               TimerNode), &u4RemainingTime) != TMR_SUCCESS)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Unable to get the remaining time for Upstream Interface\r\n");
        u4RemainingTime = IGMP_ZERO;
    }

    *pu4RetValFsIgmpProxyRtrIfaceExpiryTime = u4RemainingTime;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyRtrIfaceExpiryTime()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyRtrIfaceRowStatus
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                retValFsIgmpProxyRtrIfaceRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyRtrIfaceRowStatus (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                    INT4 *pi4RetValFsIgmpProxyRtrIfaceRowStatus)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyRtrIfaceRowStatus()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from to CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValFsIgmpProxyRtrIfaceRowStatus =
        (INT4) pIgmpPrxyUpIface->u1RowStatus;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyRtrIfaceRowStatus()\r \n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsIgmpProxyRtrIfaceCfgOperVersion
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                setValFsIgmpProxyRtrIfaceCfgOperVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpProxyRtrIfaceCfgOperVersion (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                         INT4
                                         i4SetValFsIgmpProxyRtrIfaceCfgOperVersion)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhSetFsIgmpProxyRtrIfaceCfgOperVersion()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)

    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    pIgmpPrxyUpIface->u1CfgVersion =
        (UINT1) i4SetValFsIgmpProxyRtrIfaceCfgOperVersion;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhSetFsIgmpProxyRtrIfaceCfgOperVersion()\r \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIgmpProxyRtrIfacePurgeInterval
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                setValFsIgmpProxyRtrIfacePurgeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpProxyRtrIfacePurgeInterval (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                        INT4
                                        i4SetValFsIgmpProxyRtrIfacePurgeInterval)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhSetFsIgmpProxyRtrIfacePurgeInterval()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        return SNMP_FAILURE;
    }

    pIgmpPrxyUpIface->u2RtrPurgeInt =
        (UINT2) i4SetValFsIgmpProxyRtrIfacePurgeInterval;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhSetFsIgmpProxyRtrIfacePurgeInterval()\r \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIgmpProxyRtrIfaceRowStatus
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                setValFsIgmpProxyRtrIfaceRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIgmpProxyRtrIfaceRowStatus (INT4 i4FsIgmpProxyRtrIfaceIndex,
                                    INT4 i4SetValFsIgmpProxyRtrIfaceRowStatus)
{
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    tIgmpIface         *pIfaceNode = NULL;
    INT4                i4RetValue = 0;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhSetFsIgmpProxyRtrIfaceRowStatus()\r \n");
    UNUSED_PARAM (i4RetValue);
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        return SNMP_FAILURE;
    }

    /* Check if IGMP is enabled on the interface */
    pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);

    if (pIfaceNode == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Enable IGMP on interface %d \r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        CLI_SET_ERR (CLI_IGP_IGMP_INT_ERR);
        return SNMP_FAILURE;
    }

    i4RetValue = IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface);

    if (pIgmpPrxyUpIface != NULL)
    {
        if (pIgmpPrxyUpIface->u1RowStatus ==
            (UINT1) i4SetValFsIgmpProxyRtrIfaceRowStatus)
        {
            return SNMP_SUCCESS;
        }

        if ((i4SetValFsIgmpProxyRtrIfaceRowStatus == IGMP_CREATE_AND_WAIT) ||
            (i4SetValFsIgmpProxyRtrIfaceRowStatus == IGMP_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if ((i4SetValFsIgmpProxyRtrIfaceRowStatus == IGMP_DESTROY) ||
            (i4SetValFsIgmpProxyRtrIfaceRowStatus == IGMP_ACTIVE) ||
            (i4SetValFsIgmpProxyRtrIfaceRowStatus == IGMP_NOT_IN_SERVICE))
        {
            CLI_SET_ERR (CLI_IGP_UPSTREAM_ERR);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFsIgmpProxyRtrIfaceRowStatus)
    {
        case IGMP_CREATE_AND_WAIT:
        case IGMP_CREATE_AND_GO:

            if (IgpUpIfCreateUpIfaceEntry (u4Port, pIfaceNode,
                                           &pIgmpPrxyUpIface) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Upstream interface creation failed\r\n");
                return SNMP_FAILURE;
            }

            if (i4SetValFsIgmpProxyRtrIfaceRowStatus == IGMP_CREATE_AND_GO)
            {
                pIgmpPrxyUpIface->u1RowStatus = IGMP_ACTIVE;

                OsixGetSysTime (&(pIgmpPrxyUpIface->u4RtrUpTime));

                /* Update the operating version to the configured version */
                pIgmpPrxyUpIface->u1OperVersion =
                    pIgmpPrxyUpIface->u1CfgVersion;

                /* Increment the Upstream interface version count directly */
                if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_3)
                {
                    gIgmpProxyInfo.u1V3UpstrmCnt++;
                }
                else if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_2)
                {
                    gIgmpProxyInfo.u1V2UpstrmCnt++;
                }
                else
                {
                    gIgmpProxyInfo.u1V1UpstrmCnt++;
                }

                if (IgpFwdUpdateFwdTable (gIgpIPvXZero, gIgpIPvXZero,
                                          pIgmpPrxyUpIface->u4IfIndex,
                                          IGMP_ZERO, IGP_ADD_OIF,
                                          NULL) != IGMP_SUCCESS)
                {
                    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                              "Upstream interface additon to forwarding table"
                              " failed\r\n");
                    return SNMP_FAILURE;
                }
            }
            else
            {
                pIgmpPrxyUpIface->u1RowStatus = IGMP_NOT_READY;
            }
            break;

        case IGMP_ACTIVE:

            /* Update the operating version to the configured version */
            pIgmpPrxyUpIface->u1OperVersion = pIgmpPrxyUpIface->u1CfgVersion;
            /* Increment the Upstream interface version count directly */
            if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_3)
            {
                gIgmpProxyInfo.u1V3UpstrmCnt++;
            }
            else if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_2)
            {
                gIgmpProxyInfo.u1V2UpstrmCnt++;
            }
            else
            {
                gIgmpProxyInfo.u1V1UpstrmCnt++;
            }

            /* Add the interface to the forwadring table */
            if (IgpFwdUpdateFwdTable (gIgpIPvXZero, gIgpIPvXZero,
                                      pIgmpPrxyUpIface->u4IfIndex,
                                      IGMP_ZERO, IGP_ADD_OIF,
                                      NULL) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Upstream interface additon to forwarding table"
                          " failed\r\n");
                return SNMP_FAILURE;
            }

            OsixGetSysTime (&(pIgmpPrxyUpIface->u4RtrUpTime));

            pIgmpPrxyUpIface->u1RowStatus = IGMP_ACTIVE;
            break;

        case IGMP_NOT_IN_SERVICE:
            if (pIgmpPrxyUpIface->u1RowStatus == IGMP_NOT_READY)
            {
                return SNMP_FAILURE;
            }

            if (pIgmpPrxyUpIface->u1RowStatus == IGMP_ACTIVE)
            {
                /* Decrement the Upstream interface version count directly */
                if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_3)
                {
                    gIgmpProxyInfo.u1V3UpstrmCnt--;
                }
                else if (pIgmpPrxyUpIface->u1OperVersion == IGMP_VERSION_2)
                {
                    gIgmpProxyInfo.u1V2UpstrmCnt--;
                }
                else
                {
                    gIgmpProxyInfo.u1V1UpstrmCnt--;
                }
            }

            IgpFwdUpIfEntryDeletion (pIgmpPrxyUpIface->u4IfIndex);

            pIgmpPrxyUpIface->u1RowStatus = IGMP_NOT_IN_SERVICE;
            break;

        case IGMP_DESTROY:
            if (IgpUpIfDeleteUpIfaceEntry (u4Port, IGMP_TRUE) != IGMP_SUCCESS)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failed to delete upstream interface entry\r\n");
                return SNMP_FAILURE;
            }
            break;

        default:
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Invalid value for FsIgmpProxyRtrIfaceRowStatus\r\n");
            break;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhSetFsIgmpProxyRtrIfaceRowStatus()\r \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsIgmpProxyRtrIfaceCfgOperVersion
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                testValFsIgmpProxyRtrIfaceCfgOperVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpProxyRtrIfaceCfgOperVersion (UINT4 *pu4ErrorCode,
                                            INT4 i4FsIgmpProxyRtrIfaceIndex,
                                            INT4
                                            i4TestValFsIgmpProxyRtrIfaceCfgOperVersion)
{

    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhTestv2FsIgmpProxyRtrIfaceCfgOperVersion()\r \n");
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)

    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGP_UPSTREAM_NOT_FOUND);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIgmpProxyRtrIfaceCfgOperVersion < IGMP_VERSION_1) ||
        (i4TestValFsIgmpProxyRtrIfaceCfgOperVersion > IGMP_VERSION_3))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Invalid value for IGMP Version \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhTestv2FsIgmpProxyRtrIfaceCfgOperVersion()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpProxyRtrIfacePurgeInterval
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                testValFsIgmpProxyRtrIfacePurgeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpProxyRtrIfacePurgeInterval (UINT4 *pu4ErrorCode,
                                           INT4 i4FsIgmpProxyRtrIfaceIndex,
                                           INT4
                                           i4TestValFsIgmpProxyRtrIfacePurgeInterval)
{

    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    UINT4               u4Port = IGMP_ZERO;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhTestv2FsIgmpProxyRtrIfacePurgeInterval()\r \n");
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface) == IGMP_FAILURE)

    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Upstream Iface entry not found for interface index %d\r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_IGP_UPSTREAM_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIgmpProxyRtrIfacePurgeInterval
         < IGP_MIN_RTR_IFACE_INTERVAL) ||
        (i4TestValFsIgmpProxyRtrIfacePurgeInterval
         > IGP_MAX_RTR_IFACE_INTERVAL))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Invalid value for IGMP upstream purge interval\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhTestv2FsIgmpProxyRtrIfacePurgeInterval()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIgmpProxyRtrIfaceRowStatus
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex

                The Object 
                testValFsIgmpProxyRtrIfaceRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIgmpProxyRtrIfaceRowStatus (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIgmpProxyRtrIfaceIndex,
                                       INT4
                                       i4TestValFsIgmpProxyRtrIfaceRowStatus)
{
    INT1                i1RetStatus = SNMP_SUCCESS;
    INT4                i4RetValue = 0;
    UINT4               u4Port = IGMP_ZERO;
    tIgmpPrxyUpIface   *pIgmpPrxyUpIface = NULL;
    tIgmpIface         *pIfaceNode = NULL;

    UNUSED_PARAM (i4RetValue);
    UNUSED_PARAM (i4FsIgmpProxyRtrIfaceIndex);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhTestv2FsIgmpProxyRtrIfaceRowStatus()\r \n");
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyRtrIfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from CFA index\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if IGMP is enabled on the interface */
    pIfaceNode = IgmpGetIfNode (u4Port, IPVX_ADDR_FMLY_IPV4);

    if (pIfaceNode == NULL)
    {
        IGMP_DBG1 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Enable IGMP on interface %d \r\n",
                       i4FsIgmpProxyRtrIfaceIndex);
        CLI_SET_ERR (CLI_IGP_IGMP_INT_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    i4RetValue = IgpUpIfGetUpIfaceEntry (u4Port, &pIgmpPrxyUpIface);

    if (pIgmpPrxyUpIface != NULL)
    {
        if (pIgmpPrxyUpIface->u1RowStatus ==
            (UINT1) i4TestValFsIgmpProxyRtrIfaceRowStatus)
        {
            return SNMP_SUCCESS;
        }

        if ((i4TestValFsIgmpProxyRtrIfaceRowStatus == IGMP_CREATE_AND_WAIT) ||
            (i4TestValFsIgmpProxyRtrIfaceRowStatus == IGMP_CREATE_AND_GO))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if ((i4TestValFsIgmpProxyRtrIfaceRowStatus == IGMP_DESTROY) ||
            (i4TestValFsIgmpProxyRtrIfaceRowStatus == IGMP_ACTIVE) ||
            (i4TestValFsIgmpProxyRtrIfaceRowStatus == IGMP_NOT_IN_SERVICE))
        {
            CLI_SET_ERR (CLI_IGP_UPSTREAM_ERR);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    switch (i4TestValFsIgmpProxyRtrIfaceRowStatus)
    {
        case IGMP_CREATE_AND_GO:
        case IGMP_CREATE_AND_WAIT:
        case IGMP_ACTIVE:
        case IGMP_DESTROY:
        case IGMP_NOT_IN_SERVICE:
            break;

        default:
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Invalid value for Upstream interface row status \r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetStatus = SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhTestv2FsIgmpProxyRtrIfaceRowStatus()\r \n");
    return i1RetStatus;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIgmpProxyRtrIfaceTable
 Input       :  The Indices
                FsIgmpProxyRtrIfaceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIgmpProxyRtrIfaceTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIgmpProxyMrouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIgmpProxyMrouteTable
 Input       :  The Indices
                FsIgmpProxyMRouteSource
                FsIgmpProxyMRouteGroup
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIgmpProxyMrouteTable (UINT4 u4FsIgmpProxyMRouteSource,
                                                UINT4 u4FsIgmpProxyMRouteGroup)
{
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhValidateIndexInstanceFsIgmpProxyMrouteTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteGroup);

    if (RBTreeGet (gIgmpProxyInfo.ForwardEntry,
                   (tRBElem *) & IgpForwardEntry) == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast forwarding entry not found for"
                       " source %x group %x \r\n",
                       u4FsIgmpProxyMRouteSource, u4FsIgmpProxyMRouteGroup);
        return SNMP_FAILURE;
    }
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhValidateIndexInstanceFsIgmpProxyMrouteTable()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpProxyMrouteTable
 Input       :  The Indices
                FsIgmpProxyMRouteSource
                FsIgmpProxyMRouteGroup
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIgmpProxyMrouteTable (UINT4 *pu4FsIgmpProxyMRouteSource,
                                        UINT4 *pu4FsIgmpProxyMRouteGroup)
{
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFirstIndexFsIgmpProxyMrouteTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    pIgmpPrxyFwdEntry =
        (tIgmpPrxyFwdEntry *) RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Multicast forwarding entry not found\r\n");
        return SNMP_FAILURE;
    }
    IGMP_IP_COPY_FROM_IPVX (pu4FsIgmpProxyMRouteGroup,
                            pIgmpPrxyFwdEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (pu4FsIgmpProxyMRouteSource,
                            pIgmpPrxyFwdEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFirstIndexFsIgmpProxyMrouteTable()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpProxyMrouteTable
 Input       :  The Indices
                FsIgmpProxyMRouteSource
                nextFsIgmpProxyMRouteSource
                FsIgmpProxyMRouteGroup
                nextFsIgmpProxyMRouteGroup
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIgmpProxyMrouteTable (UINT4 u4FsIgmpProxyMRouteSource,
                                       UINT4 *pu4NextFsIgmpProxyMRouteSource,
                                       UINT4 u4FsIgmpProxyMRouteGroup,
                                       UINT4 *pu4NextFsIgmpProxyMRouteGroup)
{
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetNextIndexFsIgmpProxyMrouteTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteGroup);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGetNext (gIgmpProxyInfo.ForwardEntry,
                       (tRBElem *) & IgpForwardEntry, NULL);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Multicast forwarding entry not found\r\n");
        return SNMP_FAILURE;
    }
    IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpProxyMRouteGroup,
                            pIgmpPrxyFwdEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpProxyMRouteSource,
                            pIgmpPrxyFwdEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetNextIndexFsIgmpProxyMrouteTable()\r \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyMRouteIifIndex
 Input       :  The Indices
                FsIgmpProxyMRouteSource
                FsIgmpProxyMRouteGroup

                The Object 
                retValFsIgmpProxyMRouteIifIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyMRouteIifIndex (UINT4 u4FsIgmpProxyMRouteSource,
                                 UINT4 u4FsIgmpProxyMRouteGroup,
                                 INT4 *pi4RetValFsIgmpProxyMRouteIifIndex)
{
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    UINT4               u4IfIndex = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyMRouteIifIndex()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, 0, sizeof (tIgmpPrxyFwdEntry));
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteGroup);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast forwarding entry not found for"
                       " source %x group %x \r\n",
                       u4FsIgmpProxyMRouteSource, u4FsIgmpProxyMRouteGroup);
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_IFINDEX_FROM_PORT (pIgmpPrxyFwdEntry->u4IfIndex,
                                       &u4IfIndex))
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get CFA Index number from to IP Port number\r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFsIgmpProxyMRouteIifIndex = u4IfIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyMRouteIifIndex()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyMRouteUpTime
 Input       :  The Indices
                FsIgmpProxyMRouteSource
                FsIgmpProxyMRouteGroup

                The Object 
                retValFsIgmpProxyMRouteUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyMRouteUpTime (UINT4 u4FsIgmpProxyMRouteSource,
                               UINT4 u4FsIgmpProxyMRouteGroup,
                               UINT4 *pu4RetValFsIgmpProxyMRouteUpTime)
{
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    UINT4               u4CurrentSysTime = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyMRouteUpTime()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, 0, sizeof (tIgmpPrxyFwdEntry));

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteGroup);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast forwarding entry not found for"
                       " source %x group %x \r\n",
                       u4FsIgmpProxyMRouteSource, u4FsIgmpProxyMRouteGroup);
        return SNMP_FAILURE;
    }

    OsixGetSysTime (&u4CurrentSysTime);

    *pu4RetValFsIgmpProxyMRouteUpTime =
        (u4CurrentSysTime - pIgmpPrxyFwdEntry->u4RouteUpTime);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyMRouteUpTime()\r \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyMRouteExpiryTime
 Input       :  The Indices
                FsIgmpProxyMRouteSource
                FsIgmpProxyMRouteGroup

                The Object 
                retValFsIgmpProxyMRouteExpiryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyMRouteExpiryTime (UINT4 u4FsIgmpProxyMRouteSource,
                                   UINT4 u4FsIgmpProxyMRouteGroup,
                                   UINT4 *pu4RetValFsIgmpProxyMRouteExpiryTime)
{
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    UINT4               u4RemainingTime = 0;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyMRouteExpiryTime()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, 0, sizeof (tIgmpPrxyFwdEntry));

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyMRouteGroup);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast forwarding entry not found for"
                       " source %x group %x \r\n",
                       u4FsIgmpProxyMRouteSource, u4FsIgmpProxyMRouteGroup);
        return SNMP_FAILURE;
    }

    if (TmrGetRemainingTime (gIgmpProxyInfo.ProxyTmrListId,
                             &(pIgmpPrxyFwdEntry->EntryTimer.TimerBlk.
                               TimerNode), &u4RemainingTime) == TMR_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failure in getting the remaining time for forward entry"
                  " TImer\r\n");
        return SNMP_FAILURE;
    }

    *pu4RetValFsIgmpProxyMRouteExpiryTime = u4RemainingTime;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyMRouteExpiryTime()\r \n");
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIgmpProxyNextHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIgmpProxyNextHopTable
 Input       :  The Indices
                FsIgmpProxyNextHopSource
                FsIgmpProxyNextHopGroup
                FsIgmpProxyNextHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIgmpProxyNextHopTable (UINT4
                                                 u4FsIgmpProxyNextHopSource,
                                                 UINT4
                                                 u4FsIgmpProxyNextHopGroup,
                                                 INT4 i4FsIgmpProxyNextHopIndex)
{
    UINT4               u4Port = IGMP_ZERO;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhValidateIndexInstanceFsIgmpProxyNextHopTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));

    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyNextHopSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyNextHopGroup);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry != NULL)
    {
        if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyNextHopIndex,
                                           &u4Port) == NETIPV4_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Failed to get IP Port number from to CFA index\r\n");
            return SNMP_FAILURE;
        }

        TMO_SLL_Scan (&(pIgmpPrxyFwdEntry->OifList),
                      pIgpOifNode, tIgmpPrxyOif *)
        {
            if (pIgpOifNode->u4IfIndex == u4Port)
            {
                return SNMP_SUCCESS;
            }
            else if (pIgpOifNode->u4IfIndex > u4Port)
            {
                break;
            }
        }
    }

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "Next hop table validation failed\r\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhValidateIndexInstanceFsIgmpProxyNextHopTable()\r \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIgmpProxyNextHopTable
 Input       :  The Indices
                FsIgmpProxyNextHopSource
                FsIgmpProxyNextHopGroup
                FsIgmpProxyNextHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIgmpProxyNextHopTable (UINT4 *pu4FsIgmpProxyNextHopSource,
                                         UINT4 *pu4FsIgmpProxyNextHopGroup,
                                         INT4 *pi4FsIgmpProxyNextHopIndex)
{
    UINT4               u4IfIndex = IGMP_ZERO;
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFirstIndexFsIgmpProxyNextHopTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    pIgmpPrxyFwdEntry =
        (tIgmpPrxyFwdEntry *) RBTreeGetFirst (gIgmpProxyInfo.ForwardEntry);

    if (pIgmpPrxyFwdEntry != NULL)
    {
        pIgpOifNode =
            (tIgmpPrxyOif *) TMO_SLL_First (&(pIgmpPrxyFwdEntry->OifList));

        if (pIgpOifNode != NULL)
        {
            if (IGMP_IP_GET_IFINDEX_FROM_PORT
                (pIgpOifNode->u4IfIndex, &u4IfIndex) == NETIPV4_FAILURE)
            {
                IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                          "Failed to get IP Port number from to CFA index\r\n");
                return SNMP_FAILURE;
            }

            IGMP_IP_COPY_FROM_IPVX (pu4FsIgmpProxyNextHopGroup,
                                    pIgmpPrxyFwdEntry->u4GrpIpAddr,
                                    IPVX_ADDR_FMLY_IPV4);
            IGMP_IP_COPY_FROM_IPVX (pu4FsIgmpProxyNextHopSource,
                                    pIgmpPrxyFwdEntry->u4SrcIpAddr,
                                    IPVX_ADDR_FMLY_IPV4);

            *pi4FsIgmpProxyNextHopIndex = (INT4) u4IfIndex;
            return SNMP_SUCCESS;
        }
    }

    IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
              "No entries are found in the NextHopTable\r\n");
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFirstIndexFsIgmpProxyNextHopTable()\r \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIgmpProxyNextHopTable
 Input       :  The Indices
                FsIgmpProxyNextHopSource
                nextFsIgmpProxyNextHopSource
                FsIgmpProxyNextHopGroup
                nextFsIgmpProxyNextHopGroup
                FsIgmpProxyNextHopIndex
                nextFsIgmpProxyNextHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIgmpProxyNextHopTable (UINT4 u4FsIgmpProxyNextHopSource,
                                        UINT4 *pu4NextFsIgmpProxyNextHopSource,
                                        UINT4 u4FsIgmpProxyNextHopGroup,
                                        UINT4 *pu4NextFsIgmpProxyNextHopGroup,
                                        INT4 i4FsIgmpProxyNextHopIndex,
                                        INT4 *pi4NextFsIgmpProxyNextHopIndex)
{
    UINT4               u4Port = IGMP_ZERO;
    UINT4               u4IfIndex = IGMP_ZERO;
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetNextIndexFsIgmpProxyNextHopTable()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyNextHopSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyNextHopGroup);

    pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
        RBTreeGet (gIgmpProxyInfo.ForwardEntry, (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry == NULL)
    {
        IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                       "Multicast forwarding entry not found for source %d and "
                       "group %x\r\n", u4FsIgmpProxyNextHopSource,
                       u4FsIgmpProxyNextHopGroup);
        return SNMP_FAILURE;
    }

    if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyNextHopIndex,
                                       &u4Port) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get IP Port number from to CFA index\r\n");
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pIgmpPrxyFwdEntry->OifList), pIgpOifNode, tIgmpPrxyOif *)
    {
        if (pIgpOifNode->u4IfIndex > u4Port)
        {
            /* Since the vlan port numbers are stored in ascending order,
             * the first greater entry can be returned */
            break;
        }
    }

    if (pIgpOifNode == NULL)
    {
        /* pIgpOifNode == NULL means that we ding get any IP Port, in the 
         * current pIgmpPrxyFwdEntry, which is greater than the given IP Port
         * So get the next pIgmpPrxyFwdEntry */
        pIgmpPrxyFwdEntry = (tIgmpPrxyFwdEntry *)
            RBTreeGetNext (gIgmpProxyInfo.ForwardEntry,
                           (tRBElem *) pIgmpPrxyFwdEntry, NULL);

        if (pIgmpPrxyFwdEntry == NULL)
        {
            IGMP_DBG2 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                           "Multicast forwarding entry not found for source %d and "
                           "group %x\r\n", u4FsIgmpProxyNextHopSource,
                           u4FsIgmpProxyNextHopGroup);
            return SNMP_FAILURE;
        }

        /* There will be atleast one entry in the Out-going Interface List */
        pIgpOifNode =
            (tIgmpPrxyOif *) TMO_SLL_First (&(pIgmpPrxyFwdEntry->OifList));

        if (pIgpOifNode == NULL)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "ERROR Oif List is empty\r\n");
            return SNMP_FAILURE;
        }
    }

    if (IGMP_IP_GET_IFINDEX_FROM_PORT
        (pIgpOifNode->u4IfIndex, &u4IfIndex) == NETIPV4_FAILURE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "Failed to get CFA IfIndex from IP Port number\r\n");
        return SNMP_FAILURE;
    }
    IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpProxyNextHopGroup,
                            pIgmpPrxyFwdEntry->u4GrpIpAddr,
                            IPVX_ADDR_FMLY_IPV4);
    IGMP_IP_COPY_FROM_IPVX (pu4NextFsIgmpProxyNextHopSource,
                            pIgmpPrxyFwdEntry->u4SrcIpAddr,
                            IPVX_ADDR_FMLY_IPV4);

    *pi4NextFsIgmpProxyNextHopIndex = (INT4) u4IfIndex;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetNextIndexFsIgmpProxyNextHopTable()\r \n");
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsIgmpProxyNextHopState
 Input       :  The Indices
                FsIgmpProxyNextHopSource
                FsIgmpProxyNextHopGroup
                FsIgmpProxyNextHopIndex

                The Object 
                retValFsIgmpProxyNextHopState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyNextHopState (UINT4 u4FsIgmpProxyNextHopSource,
                               UINT4 u4FsIgmpProxyNextHopGroup,
                               INT4 i4FsIgmpProxyNextHopIndex,
                               INT4 *pi4RetValFsIgmpProxyNextHopState)
{
    UINT4               u4Port = IGMP_ZERO;
    tIgmpPrxyFwdEntry  *pIgmpPrxyFwdEntry = NULL;
    tIgmpPrxyFwdEntry   IgpForwardEntry;
    tIgmpPrxyOif       *pIgpOifNode = NULL;
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_ENTRY_MODULE, IgmpGetModuleName(IGMP_ENTRY_MODULE),
                      "Entering nmhGetFsIgmpProxyNextHopState()\r \n");
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                  "IGMP proxy is disabled in the system\r\n");
        return SNMP_FAILURE;
    }

    MEMSET (&IgpForwardEntry, IGMP_ZERO, sizeof (tIgmpPrxyFwdEntry));
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4SrcIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyNextHopSource);
    IGMP_IPVX_ADDR_INIT_IPV4 (IgpForwardEntry.u4GrpIpAddr, IPVX_ADDR_FMLY_IPV4,
                              (UINT1 *) &u4FsIgmpProxyNextHopGroup);

    pIgmpPrxyFwdEntry =
        (tIgmpPrxyFwdEntry *) RBTreeGet (gIgmpProxyInfo.ForwardEntry,
                                         (tRBElem *) & IgpForwardEntry);

    if (pIgmpPrxyFwdEntry != NULL)
    {
        if (IGMP_IP_GET_PORT_FROM_IFINDEX ((UINT4) i4FsIgmpProxyNextHopIndex,
                                           &u4Port) == NETIPV4_FAILURE)
        {
            IGMP_DBG_INFO (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                      "Failed to get IP Port number from to CFA index\r\n");
            return SNMP_FAILURE;
        }

        TMO_SLL_Scan (&(pIgmpPrxyFwdEntry->OifList), pIgpOifNode,
                      tIgmpPrxyOif *)
        {
            if (pIgpOifNode->u4IfIndex == u4Port)
            {
                *pi4RetValFsIgmpProxyNextHopState = IGP_STATE_FORWARDING;
                return SNMP_SUCCESS;
            }
            if (pIgpOifNode->u4IfIndex > u4Port)
            {
                break;
            }
        }
    }

    IGMP_DBG3 (IGMP_DBG_FLAG, IGMP_MGMT_MODULE, IGMP_NAME,
                   "Multicast entry not found for source %x and Group %x and OIF"
                   " index %d\r\n", u4FsIgmpProxyNextHopSource,
                   u4FsIgmpProxyNextHopGroup, i4FsIgmpProxyNextHopIndex);
    MGMD_DBG_EXT(MGMD_DBG_FLAG, IGMP_EXIT_MODULE, IgmpGetModuleName(IGMP_EXIT_MODULE),
             "Exiting  nmhGetFsIgmpProxyNextHopState()\r \n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyQuerierIfIndex
 Input       :  The Indices

                The Object 
                retValFsIgmpProxyQuerierIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyQuerierIfIndex (INT4 *pi4RetValFsIgmpProxyQuerierIfIndex)
{
    *pi4RetValFsIgmpProxyQuerierIfIndex = gIgmpProxyInfo.TrapInfo.u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIgmpProxyQuerierAddress
 Input       :  The Indices

                The Object 
                retValFsIgmpProxyQuerierAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIgmpProxyQuerierAddress (UINT4 *pu4RetValFsIgmpProxyQuerierAddress)
{
    if (IGMP_PROXY_STATUS () != IGMP_ENABLE)
    {
        IGMP_IP_COPY_FROM_IPVX (pu4RetValFsIgmpProxyQuerierAddress,
                                gIgmpProxyInfo.TrapInfo.u4QuerierAddress,
                                IPVX_ADDR_FMLY_IPV4);
    }
    return SNMP_SUCCESS;
}
