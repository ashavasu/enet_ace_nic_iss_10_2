#
# mktool.pl
#
# $Id: mktool.pl,v 1.1 2007/10/06 12:57:02 iss Exp $

use File::Path;
use File::Find;
use File::Copy;
use Cwd;

($prog = $0) =~ s/.*\///;

SWITCH:
{
    $op = shift;
    mktool_md(@ARGV); last SWITCH;
}

exit 0;

sub mktool_md
{
    my($dir);

    foreach $dir (@_) {
	$dir =~ s!/+$!!;
	eval { mkpath($dir) };
	if ($@) {
	    die "$prog $op: failed to make directory $dir: $@\n";
        }
    }
}



