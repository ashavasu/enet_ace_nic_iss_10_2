/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: lrmain.c,v 1.345.2.1 2018/03/05 13:16:01 siva Exp $
*
* Description:
*
*******************************************************************/
#ifndef LRMAIN_C
#define LRMAIN_C
#include "lr.h"
#ifdef ISSSZ_WANTED
#include "sizereg.h"
#endif
#include "fsbuddy.h"
#include "cfa.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif /* MBSM WANTED */
#include "iss.h"

#ifdef NAT_WANTED
#include "nat.h"
#endif
#include "ip.h"
#include "arp.h"
#include "lnxip.h"
#ifdef SNMP_WANTED
#include "snmcport.h"
#endif
#ifdef SNMP_2_WANTED
#include "fssnmp.h"
#endif
#include "trie.h"
#include "bridge.h"
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h"
#include "issu.h"
#include "garp.h"
#include "mrp.h"
#include "rstp.h"
#include "snp.h"
#include "eoam.h"
#include "eoamfm.h"
#include "fm.h"
#include "cmipif.h"
#ifdef RSNA_WANTED
#include "rsna.h"
#endif
#ifdef WPS_WANTED
#include "wps.h"
#endif
#include "pnac.h"
#include "la.h"
#include "ecfm.h"
#include "ospf.h"
#include "ospfte.h"
#include "tcp.h"
#include "utilipvx.h"
#include "snmputil.h"
#include "radius.h"
#include "rsvp.h"
#include "pim.h"
#include "msdp.h"
#include "igmp.h"
#include "dvmrp.h"
#include "ftp.h"
#include "dhcp.h"
#include "bgp.h"
#include "ipv6.h"
#include "dhcp6.h"
#include "rmon.h"
#include "rmon2.h"
#include "dsmon.h"
#include "enm.h"
#include "rmgr.h"
#ifdef ICCH_WANTED
#include "icch.h"
#endif
#include "hb.h"
#include "rip.h"
#include "mpls.h"
#include "tlm.h"
#include "size.h"
#include "mfwd.h"
#include "vrrp.h"
#include "ripv6.h"
#include "mld.h"
#include "cli.h"
#include "fpam.h"
#include "fssyslog.h"
#include "cust.h"
#ifdef PPP_WANTED
#include "ppp.h"
#endif /* PPP_WANTED */
#include "vcm.h"
#include "evcpro.h"
#include "lcm.h"
#include "elm.h"
#include "lldp.h"
#include "tac.h"
#include "pbbte.h"
#include "elps.h"
#include "erps.h"
#include "dns.h"
#include "fips.h"
#include "secv6.h"
#ifdef CN_WANTED
#include "cn.h"
#endif
#ifdef RBRG_WANTED
#include "rbridge.h"
#endif
#ifdef DCBX_WANTED
#include "dcbx.h"
#endif
#ifdef FSB_WANTED
#include "fsb.h"
#endif
#ifdef MEF_WANTED
#include "fsmef.h"
#endif

#ifdef Y1564_WANTED
#include "y1564.h"
#endif
#ifdef RFC2544_WANTED
#include "r2544.h"
#endif

#ifdef RFC6374_WANTED
#include "r6374.h"
#endif

#include "l2ds.h"
#include "ipdb.h"

#ifdef ISIS_WANTED
#include "isis.h"
#endif
#ifdef FIREWALL_WANTED
#include "firewall.h"
#endif

#ifdef LSPP_WANTED
#include "lspp.h"
#endif

#ifdef BFD_WANTED
#include "bfd.h"
#endif

#include "pbbte.h"
#include "beepsrv.h"
#include "beepclt.h"
#include "fsntp.h"
#ifdef BGP_WANTED
#include "bgp.h"
#endif /* BGP_WANTED */

#ifdef ISS_WANTED
#include "msr.h"
#endif /* ISS_WANTED */

#include "secv4.h"
#include "fsike.h"

#ifdef POE_WANTED
#include "poe.h"
#endif /* POE_WANTED */
#ifdef VPN_WANTED
#include "vpn.h"
#endif
#ifdef CLKIWF_WANTED
PUBLIC VOID ClkMainInit PROTO ((INT1 *pArg));
#endif

#ifdef PTP_WANTED
#include "ptp.h"
#endif

#ifdef SYNCE_WANTED
#include "synce.h"
#endif

#ifdef KERNEL_WANTED
#include <linux/version.h>
#include "chrdev.h"
#include "netdev.h"
#ifdef KERNEL_2_6
#include "signal.h"
#endif
#endif

#if defined (LNXIP4_WANTED) || defined (IP_WANTED)
#include <fcntl.h>
#include <sys/mount.h>
#endif

#ifdef HOTSPOT2_WANTED
#include "hs.h"
#endif /* HOTSPOT2_WANTED */

#ifdef CAPWAP_WANTED
#include "capwap.h"
#endif

#ifdef RFMGMT_WANTED
#include "rfmgmt.h"
#endif

#ifdef WSS_WANTED
#include "wsssta.h"
#include "wsspm.h"
#include "aphdlr.h"
#include "wlchdlr.h"
#ifdef WSSUSER_WANTED
#include "user.h"
#endif
#ifdef BCNMGR_WANTED
#include "bcnmgr.h"
#endif
#endif

#ifdef SMOD_WANTED
#include "secmod.h"
#endif /* SMOD_WANTED */

#ifdef OPENFLOW_WANTED
#include "ofcl.h"
#endif

#include "utilalgo.h"
#ifdef IDS_WANTED
#include "ids.h"
#endif /* IDS_WANTED */
#ifdef WEBNM_WANTED
#include "fswebnm.h"
#endif
#ifdef L2RED_WANTED
#include "rednp.h"
#ifdef SNMP_3_WANTED
extern VOID         SnmpRedTaskMain (INT1 *);
#endif
#endif

#ifdef VXLAN_WANTED
#include "fsvxlan.h"
#endif
#ifdef NETCONF_WANTED
#include "netconf.h"
#endif
#ifdef KERNEL_WANTED
extern VOID         NpCallBackHandler (INT1 *pi1TaskId);
#endif
#ifdef OSPF3_WANTED
extern VOID         V3OSPFTaskMain (VOID);
#endif
#ifdef TACACS_WANTED
extern VOID         TacacsClientMain (INT1 *);
#endif
UINT1               gau1BitMaskMap[BITS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};

extern tOsixSemId   gu4SemNameSemId;

/* Below Global Variable can take ISS_CTRL_PLANE_STACKING_MODEL 
   or ISS_DATA_PLANE_STACKING_MODEL as values . 
      
  ISS_CTRL_PLANE_STACKING_MODEL - Stacking model Independent of 
                                  Hardware. 
  ISS_DATA_PLANE_STACKING_MODEL - Stacking  model Dependent on Hw 
                                  Stack Task */
#ifdef MBSM_WANTED
UINT4               gu4StackingModel = ISS_CTRL_PLANE_STACKING_MODEL;
#else
UINT4               gu4StackingModel = ISS_STACKING_MODEL_NONE;
#endif
#ifdef QOSX_WANTED
PUBLIC VOID         QoSInit (INT1 *i1Params);
#endif

#ifdef SLI_WANTED
extern VOID SliInit PROTO ((VOID));
#else
/* FS SLI IS INCLUDED FOR LNXIP-FSIP6 COMBINATION TO SUPPORT UP6 SOCKET */
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED) && !defined (LNXIP6_WANTED)
extern VOID SliInit PROTO ((VOID));
#endif
#endif

#ifdef BRIDGE_WANTED
extern INT1         nmhSetDot1dBaseBridgeStatus (INT4);
#endif

extern INT4         TrieInit (void);

#ifdef IPVX_WANTED
PUBLIC VOID         IpvxMain (INT1 *pArg);
#endif

#ifdef ROUTEMAP_WANTED
extern INT4         RMapInit (VOID);
#endif

#ifdef SNMP_3_WANTED
extern VOID SnmpONMain PROTO ((VOID));
#endif

typedef enum
{
    TSK_BEFORE_INIT = 0,
    TSK_INIT_SUCCESS,
    TSK_INIT_FAILURE
}
tTskStatus;

typedef struct TskInitTable
{
    char               *pu1TskName;
    UINT4               u4TskPriority;
    UINT4               u4SchedPolicy;
    UINT4               u4TskStack;
    VOID                (*pTskEntryFn) (INT1 *);
    VOID                (*pTskShutDnFn) (VOID);
}
tTskInitTable;
UINT1               gu1ResDefConfig = OSIX_FALSE;

extern INT4         SNMPIndexMangerListInit (VOID);
extern tMemPoolId   gBitListPoolId;
extern tMemPoolId   gLocalPortListPoolId;
extern tMemPoolId   gVlanListSizePoolId;
extern tMemPoolId   gVlanIdSizePoolId;

/* As a general rule Layer 2 protocols like CFA/LA/STP/VLAN/GARP
* are assigned priorities from the following band 0 ~ 50
* Layer 3 forwarding modules like IP,IP6,ARP use the 50 ~ 100 band
* Layer 3 Routing protocols/Applications like OSPF/RADIUS use
* the 100 ~ 150 band
* The configurations Managers use the 150 ~ 200 Band
* Please note that Scheduling Policy is relevent only for
* Linux Pthreads environment */

tTskInitTable       gaInitTable[] = {
#ifdef L2IWF_WANTED
    {NULL, 0, 0, 0, (void *) L2IwfInit, NULL},
#endif
#ifdef CLKIWF_WANTED
    {NULL, 0, 0, 0, (void *) ClkMainInit, NULL},
#endif
#ifdef EVCPRO_WANTED
    {"EvcT", 210, 0, 0, (void *) EvcProTaskMain, NULL},
#endif
#ifdef LCM_WANTED
    {NULL, 0, 0, 0, (void *) LcmInitialize, NULL},
#endif
#ifdef CFA_WANTED
    /*Creating task in CFA for transmitting  packets periodically */
    {"PKTTX", 5, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, CfaPeriodicPktTxTsk,
     NULL},
#endif
#ifdef HB_WANTED
    {"HBTS", 15, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, HbTaskMain, NULL},
#endif
#ifdef RM_WANTED
    {"RMGR", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, RmTaskMain, NULL},
#endif
#ifdef ISSU_WANTED
    {NULL, 0, 0, 0, (void *) IssuModuleInit, NULL},
#endif
#ifdef ICCH_WANTED
    {"ICCH", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, IcchTaskMain, NULL},
#endif
#ifdef VCM_WANTED
    /* If Vcm priority is lower, then local port numbers will not be restored during
     * MSR restoration. This is because Vcm stores the generated local port numbers for
     * ports only if the node is active. For Standby it uses syncup values. */
    {"VcmT", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, VcmTaskMain, NULL},
#endif
#ifdef SYSLOG_WANTED
    {"SMT", 200, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE, SysLogSmtpMain,
     NULL},
#endif
#ifdef BEEP_SERVER_WANTED
    {"BPSR", 200, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, BpSrvTaskMain,
     NULL},
#endif
#ifdef CFA_WANTED
    {"CFA", 10, OSIX_SCHED_RR, CFA_STACKSIZE_VALUE, CfaMain, NULL},
    {"IPDB", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, IPDBMain, NULL},
    {"L2DS", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, L2DSMain, NULL},
    {"CFATX", 10, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, CfaPacketTxTask,
     NULL},

#endif
#ifdef EOAM_WANTED
    {"ELMT", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, EoamLmMainTask, NULL},
    {"EOAT", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, EoamMainTask, NULL},
#ifdef CMIPINTF_WANTED
    {"CMIT", 200, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE, CmifMainTask,
     NULL},
#endif /* CMIPINTF_WANTED */
#ifdef EOAM_FM_WANTED
    {"FMGT", 200, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE, FmMainTask, NULL},
#endif /* EOAM_FM_WANTED */
#endif /* EOAM_WANTED */
#ifndef MBSM_WANTED
#ifdef CFA_WANTED
    {NULL, 0, 0, 0, (void *) CfaIfmBringupAllInterfaces, NULL},
#endif
#endif /* MBSM_WANED */
/*Added for PBB*/
#ifdef PBB_WANTED
    {"PBB", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, PbbMain, NULL},
#endif
#ifdef RSTP_WANTED
    {"AstT", 30, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     AstTaskMain,
     NULL},
#endif
#ifdef ELMI_WANTED
    {"ElmT", 100, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     ElmTaskMain,
     NULL},
#endif
#ifdef PNAC_WANTED
    {"PIf", 40, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     PnacInterfaceTaskMain, NULL},
#endif
#ifdef RSNA_WANTED
    {"RsnaT", 40, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     RsnaTaskMain, NULL},
#endif
#ifdef WPS_WANTED
    {"WpsT", 40, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     WpsTaskMain, NULL},
#endif

#ifdef LA_WANTED
    {"LaTT", 20, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE), LaMain,
     NULL},
#endif
#ifdef POE_WANTED
    {"PoeT", 50, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     PoeTaskMain,
     NULL},
#endif
#ifdef ICCH_WANTED
    {"MCAG", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, McagMain, NULL},
#endif
#ifdef VLAN_WANTED
    {"VLAN", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, VlanMain, NULL},
#ifdef GARP_WANTED
    {"GARP", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, GarpMain, NULL},
#endif
#endif
#ifdef HOTSPOT2_WANTED
    {"HSTK", 220, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, HsMainTask, NULL},
#endif

#if defined(IGS_WANTED) || defined(MLDS_WANTED)
    {"SnpT", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, SnoopMain, NULL},
#endif
#ifdef MRP_WANTED
    {"MRP", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, MrpMainTask, NULL},
#endif /* MRP_WANTED */
#ifdef ELPS_WANTED
    {"ELPT", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, ElpsMainTask, NULL},
    {"APST", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, ElpsMainApsTxTask,
     NULL},
#endif /* ELPS_WANTED */
#ifdef ERPS_WANTED
    {"ERPT", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, ErpsMainTask, NULL},
    {"RTXT", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, ErpsMainRApsTxTask,
     NULL},
#endif /* ERPS_WANTED */
#ifdef RSTP_WANTED
    {NULL, 0, 0, 0, AstModuleEnable, NULL},
#endif /*RSTP_WANTED */
#ifdef PBBTE_WANTED
    {"PBBTE", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) PbbTeInit, NULL},
#endif
#ifdef QOSX_WANTED
    {"QOS", 210, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, QoSInit, NULL},
#endif /* QOSX_WANTED */
#ifdef ISS_WANTED
    {NULL, 0, 0, 0, (void *) IssInit, NULL},
    {"SMGT", 200, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) IssSysMainTask, NULL},
#endif
#ifdef MEF_WANTED
    {"MEF", 210, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, MefTask, NULL},
#endif
#ifdef SLI_WANTED
    {NULL, 0, 0, 0, (void *) SliInit, NULL},
#endif
#ifdef FIPS_WANTED
    {"FIPT", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) FipsInitialize, NULL},
#endif
#ifdef IP6_WANTED
    {"RT6", 80, OSIX_SCHED_RR, 0x8000, (void *) Rtm6TaskMain, NULL},
#ifdef LNXIP6_WANTED
    {"LI6T", 50, OSIX_SCHED_RR, 0x10000, (void *) Lip6MainInit, NULL},
#if defined (MLD_WANTED) || defined (PIMV6_WANTED)
    {"L6MC", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) LnxIp6McastTaskMain, NULL},
#endif
#else
    {"IP6", 50, OSIX_SCHED_RR, 0x10000, (void *) Ip6TaskMain, NULL},
#endif
    {"PNG6", 100, OSIX_SCHED_OTHER, 0x10000, (void *) Ping6TaskMain, NULL},
#endif
#ifdef LNXIP4_WANTED
    {"LNXTAP", 60, OSIX_SCHED_RR, 0x8000, (void *) LnxIpMain, NULL},
#if  defined (IGMP_WANTED) || defined (PIM_WANTED) || defined (DVMRP_WANTED)
    {"LXMC", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) LnxIpMcastTaskMain, NULL},
#endif
#endif
#if defined (LNXIP4_WANTED) || defined (IP_WANTED)
    {"RTM", 80, OSIX_SCHED_RR, 0x8000, RtmTaskMain, NULL},
#endif /* defined (LNXIP4_WANTED) || defined (IP_WANTED) */
#ifdef IP_WANTED
    {"IPFW", 60, OSIX_SCHED_RR, 0x8000, IPFWDTaskMain, NULL},
    {"UDP", 100, OSIX_SCHED_RR, 0x8000, Ip_Udp_Task_Main, NULL},
#endif
#ifdef ARP_WANTED
#if defined (LNXIP4_WANTED) || defined (IP_WANTED)
    {"ARP", 60, OSIX_SCHED_RR, 0x8000, ArpTaskMain, NULL},
#endif
#endif /* ARP_WANTED */
#if  defined (IP_WANTED) || defined(LNXIP4_WANTED)
    {"PNG", 100, OSIX_SCHED_OTHER, 0x8000, PingTaskMain, NULL},
#endif
#ifdef SMOD_WANTED
    {NULL, 0, 0, 0, (void *) SecMainInit, NULL},
#ifndef SECURITY_KERNEL_WANTED
    {"SECMOD", 60, OSIX_SCHED_RR, 0x8000, (void *) SecMainTask, NULL},
#endif
#endif /* SMOD_WANTED */
#ifdef IPSECv4_WANTED
    {NULL, 0, 0, 0, (void *) Secv4Initialize, NULL},
#endif /* SMOD_WANTED */
#ifdef IPSECv6_WANTED
    {NULL, 0, 0, 0, (void *) Secv6Initialize, NULL},
#endif /* SMOD_WANTED */
#ifdef NAT_WANTED
    {NULL, 0, 0, 0, (void *) NatInit, NULL},
#endif
#ifdef FIREWALL_WANTED
    {NULL, 0, 0, 0, (void *) FwlAclInit, NULL},
#endif
#ifdef PPP_WANTED
    {"PPP", PPP_TASK_PRIORITY, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) PPPMain, NULL},
#endif /* PPP_WANTED */
#ifdef MFWD_WANTED
    {"MFW", 60, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, MfwdTaskMain, NULL},
#endif
#ifdef RIP_WANTED
    {"RIP", 150, OSIX_SCHED_RR, 0x8000, RIPTaskMain, NULL},
#endif
#ifdef ISIS_WANTED
    {"ISIS", 130, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE, IsisTaskMain,
     NULL},
#endif
#ifdef SNMP_3_WANTED
    {"SAT", 200, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     SnmpMain,
     NULL},
#endif
#if defined(SNMP_3_WANTED) && defined(L2RED_WANTED)
    {"SRED", 200, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE, SnmpRedTaskMain,
     NULL},
#endif

#ifdef OSPF_WANTED
    {"OSPF", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, OSPFTaskMain,
     NULL},
#ifdef OSPFTE_WANTED
    {"OSPT", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, OspfTeTaskMain,
     NULL},
#endif
#endif
#ifdef OSPF3_WANTED
    {"OSV3", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) V3OSPFTaskMain, NULL},
#endif
#ifdef TCP_WANTED
    {"TCP", 100, OSIX_SCHED_RR, 20000, TcpTaskMain, NULL},
#endif
#ifdef FTP_SRV_WANTED
    {"FTP", 200, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE, FtpTaskMain, NULL},
#endif
#ifdef RADIUS_WANTED
    {"RAD", 150, OSIX_SCHED_RR, (OSIX_DEFAULT_STACK_SIZE * 2), RadiusMain,
     NULL},
#endif
#ifdef TACACS_WANTED
    {"TACT", 150, OSIX_SCHED_RR, (OSIX_DEFAULT_STACK_SIZE * 2),
     TacacsClientMain, NULL},
#endif
#ifdef RIP6_WANTED
/* FS SLI IS INCLUDED FOR LNXIP-FSIP6 COMBINATION TO SUPPORT UP6 SOCKET*/
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED) && !defined (LNXIP6_WANTED)
    {NULL, 0, 0, 0, (void *) SliInit, NULL},
#endif
    {"RIP6", 150, OSIX_SCHED_RR, 0x10000, (void *) Rip6TaskMain, NULL},
    {"RR6", 150, OSIX_SCHED_RR, 0x10000, (void *) Rip6DataNotify, NULL},
#endif

#ifdef BEEP_CLIENT_WANTED
    {"BPCL", 200, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, BpCltTaskMain,
     NULL},
#endif

#ifdef DHCP_SRV_WANTED
    {"DHS", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, DhcpTaskMain, NULL},
#endif
#ifdef DHCP_RLY_WANTED
    {"DHRL", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, DhcpRelayMain,
     NULL},
#endif
#ifdef DHCPC_WANTED
    {"DHC", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, DhcpCTaskMain,
     NULL},
    {"DCS", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     DhcpClntSelectTaskMain, NULL},
#endif
#ifdef DHCP6_SRV_WANTED
    {"D6SR", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, D6SrMainTask, NULL},
#endif
#ifdef DHCP6_CLNT_WANTED
    {"D6CL", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, D6ClMainTask, NULL},
#endif
#ifdef DHCP6_RLY_WANTED
    {"D6RL", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, D6RlMainTask, NULL},
#endif
#if defined (IGMP_WANTED) || defined (MLD_WANTED)
    {"IGMP", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, IgmpTaskMain,
     NULL},
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
    {"PIM", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, SparsePimMain,
     NULL},
#if defined PIM_NP_HELLO_WANTED
    {"PIMH", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, SparsePimHelloMain,
     NULL},
#endif
#endif
#ifdef MSDP_WANTED
    {"MSDP", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (VOID *) MsdpTaskSpawnMsdpTask, NULL},
#endif
#ifdef DVMRP_WANTED
    {"DVM", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, DvmrpTaskMain,
     NULL},
#endif
#ifdef BGP_WANTED
    {"Bgp", 150, OSIX_SCHED_RR, (1024 * 25), Bgp4TaskMain, NULL},
#endif

#ifdef TLM_WANTED
    {"TLM", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, TlmTaskMain,
     NULL},
#endif

#ifdef MPLS_WANTED
    {"MFWD", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, MplsFmMain, NULL},
    {"VPNT", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, L2VpnTaskMain,
     NULL},
#ifdef MPLS_SIG_WANTED
    {"LDPT", 150, OSIX_SCHED_RR, (UINT4) (3 * OSIX_DEFAULT_STACK_SIZE),
     LdpTaskMain,
     NULL},
    {"RPTE", 150, OSIX_SCHED_RR, (UINT4) (3 * OSIX_DEFAULT_STACK_SIZE),
     RpteRsvpTaskMain, NULL},
#endif
#ifdef MPLS_L3VPN_WANTED
    {"L3VP", 150, OSIX_SCHED_RR, (UINT4) (3 * OSIX_DEFAULT_STACK_SIZE),
     L3vpnTaskSpawnL3vpnTask, NULL},
#endif
#ifdef LSPP_WANTED
    {"LSPP", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     LsppTaskSpawnLsppTask, NULL},
#endif
#ifdef RFC6374_WANTED
    {"RFC6374", 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, R6374TaskSpawn,
     NULL},
#endif
#endif

#ifdef BFD_WANTED
    {"BFD", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, BfdTaskSpawnBfdTask,
     NULL},
#endif
#ifdef VXLAN_WANTED
    {"VXLA", 150, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     VxlanTaskSpawnVxlanTask, NULL},
#endif
#ifdef WEBNM_WANTED
    {"HST", 200, OSIX_SCHED_RR, (UINT4) (3 * OSIX_DEFAULT_STACK_SIZE),
     HttpMain,
     NULL},
#endif
#ifdef RMON_WANTED
    {"RMON", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, RmonMain, NULL},
#endif
#ifdef RMON2_WANTED
    {"RMN2", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, Rmon2MainTask, NULL},
#ifdef DSMON_WANTED
    {"DSMN", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, DsmonMainTaskMain,
     NULL},
#endif /* DSMON_WANTED */
#endif /* RMON2_WANTED */
#ifdef VRRP_WANTED
    {"VRRP", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, VrrpMain, NULL},
#endif
#ifdef FPAM_WANTED
    {NULL, 0, 0, 0, (void *) FpamInit, NULL},
#endif
#ifdef CLI_WANTED
    {"CLIC", 200, OSIX_SCHED_RR, (UINT4) (1.2 * OSIX_DEFAULT_STACK_SIZE),
     CliTaskStart, NULL},
#ifdef RM_WANTED
    {"CLRM", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     CliRmMain, NULL},
#endif
#if defined (BSDCOMP_SLI_WANTED) || (defined (SLI_WANTED) && defined (TCP_WANTED) )
    {"CTS", 200, OSIX_SCHED_OTHER, (UINT4) (1.2 * OSIX_DEFAULT_STACK_SIZE),
     CliTelnetTaskMain, NULL},
#endif
#ifdef SSH_WANTED
    {"SSH", 200, OSIX_SCHED_OTHER, (UINT4) (1.2 * OSIX_DEFAULT_STACK_SIZE),
     CliSshTaskMain, NULL},
#endif
#endif
#ifdef NETCONF_WANTED
    {"NETCONF", 200, OSIX_SCHED_OTHER, (UINT4) (1.2 * OSIX_DEFAULT_STACK_SIZE),
     NetConfTaskMain, NULL},
#endif
#ifdef KERNEL_WANTED
    {"NPCB", 10, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, NpCallBackHandler,
     NULL},
#endif

#if defined(L2RED_WANTED) && defined(NPAPI_WANTED)
    {"NPRD", 100, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, NpRedTaskMain,
     NULL},
#endif
#ifdef MBSM_WANTED
    {"MBSM", 190, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, MbsmMain, NULL},
#endif
    /* If MSR restoration flag is not set, then default vlan will not be 
     * created for default context. 
     * VlanCreateDefaultVlan creates default vlan for default context, 
     * if MSR restoration flag is not set. */
#ifdef VLAN_WANTED
    {NULL, 0, 0, 0, VlanCreateDefaultVlan, NULL},
#endif

#ifdef LLDP_WANTED
    {"LLDP", 45, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, LldpMainTask, NULL},
#endif /* LLDP_WANTED */
#ifdef ECFM_WANTED
    {"LBLT", 50, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, EcfmLbLtMain, NULL},
    {"CCHK", 30, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, EcfmCcMain, NULL},
#endif /* ECFM_WANTED */
#ifdef CN_WANTED
    {"CNTK", 210, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, CnMainTask, NULL},
#endif

#ifdef DCBX_WANTED
    {"DCBT", 210, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, DcbxMainTask, NULL},
#endif
#ifdef FSB_WANTED
    {"FsbT", 45, OSIX_SCHED_RR, (UINT4) (1.5 * OSIX_DEFAULT_STACK_SIZE),
     FsbTaskMain, NULL},
#endif
#ifdef RBRG_WANTED
    {"RBRG", 210, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, RbrgTaskSpawnRbrgTask,
     NULL},
#endif

#ifdef Y1564_WANTED
    {"Y1564", 60, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, Y1564Task, NULL},
#endif

#ifdef RFC2544_WANTED
    {"R2544", 60, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, R2544Task, NULL},
#endif

#ifdef HSEC_WANTED
    {NULL, 0, 0, 0, (void *) UtilInit, NULL},
#endif
#ifdef TAC_WANTED
    {NULL, 0, 0, 0, (void *) TACMain, NULL},
#endif
#ifdef IPVX_WANTED
    {NULL, 0, 0, 0, (void *) IpvxMain, NULL},

#endif
#ifdef ISS_WANTED
    {NULL, 0, 0, 0, TgtCustomStartup, NULL},
#endif
#ifdef PTP_WANTED
    {"PTP", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, PtpMainTask, NULL},
#endif
#ifdef SYNCE_WANTED
    {"SYNCE", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, SynceMainTask, NULL},
#endif
#ifdef SNTP_WANTED
    {SNTP_TASK_NAME, SNTP_TASK_PRIORITY, OSIX_SCHED_OTHER,
     OSIX_DEFAULT_STACK_SIZE, SntpMain, NULL},
#endif
#ifdef VPN_WANTED
#ifndef SECURITY_KERNEL_WANTED
    {VPN_TASK_NAME, 40, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) VpnMain,
     NULL},
#else
    {NULL, 0, 0, 0, (void *) VpnKernelInit, NULL},
#endif
#endif
#ifdef IKE_WANTED
    {"ike", 18, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, (void *) IkeTaskMain,
     NULL},
#endif
#ifdef DNS_WANTED
    {"DNSR", 20, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE, DnsResolverMainTask,
     NULL},
#endif
#ifdef IDS_WANTED
    {"IDSIPC", 60, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) SecIdsTaskMain,
     NULL},
#endif
#ifdef WTP_WANTED
#ifdef RFMGMT_WANTED
    {"RFMAPT", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) RfMgmtWtpMainTask,
     NULL},
#endif
    {"PMST", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) PmTaskInit,
     NULL},
    {"APH", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) ApHandlerMainTaskInit,
     NULL},
#ifdef BCNMGR_WANTED
    /* Currently Becaon mgr is used from HW driver only, 
     * This will be uncommented when it is implemented */
    {"BCNMGR", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) BcnMgrMainTask,
     NULL},
#endif
#endif

#ifdef WLC_WANTED
#ifdef RFMGMT_WANTED
    {"RFMACT", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) RfMgmtWlcMainTask,
     NULL},
#endif
    {"WLCH", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) WlcHandlerMainTaskInit,
     NULL},
    {"WMAIN", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) WssStaWebAuthRecvMainTaskInit,
     NULL},
    {"WPROC", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) WssStaWebAuthProcessTask,
     NULL},
    {"PMST", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) PmTaskInit,
     NULL},
#ifdef WSSUSER_WANTED
    {"WUser", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) WssUserInitModuleStart,
     NULL},
#endif

#endif

#ifdef CAPWAP_WANTED
    {"CAPR", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) CapwapRecvMainTaskInit,
     NULL},
    {"CAPS", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) CapwapServiceTaskInit,
     NULL},
    {"CAPD", 100, OSIX_SCHED_OTHER, OSIX_DEFAULT_STACK_SIZE,
     (void *) CapwapDiscTaskMain,
     NULL},
#endif

#ifdef OPENFLOW_WANTED
    {"OFCL", 200, OSIX_SCHED_RR, OSIX_DEFAULT_STACK_SIZE,
     (void *) OfcTaskSpawnOfcTask, NULL},
#endif
    {NULL, 0, 0, 0, NULL, NULL}
};

#define ENTRY_FN(gu4CurIndex)     (gaInitTable[gu4CurIndex].pTskEntryFn)
#define SHUTDN_FN(gu4CurIndex)    (gaInitTable[gu4CurIndex].pTskShutDnFn)

#define ISS_FILE_NAME_LENGTH                  30
#ifdef ISSSZ_WANTED
static VOID         UpdateDefaultSystemSizingInfo (VOID);
char                gau1SizingFileName[ISS_FILE_NAME_LENGTH + 1];
#endif

VOID                issSystemStart (INT1 *);

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
extern INT4         setns (int, int);
#define NETWORK_NS_RUN_DIR "/var/run/netns"
#define NETNS_ETC_DIR "/etc/netns"
#ifndef CLONE_NEWNET
#define CLONE_NEWNET 0x40000000    /* New network namespace (lo, device, names sockets, etc) */
#endif

#ifndef MNT_DETACH
#define MNT_DETACH      0x00000002    /* Just detach from the tree */
#endif /* MNT_DETACH */

#ifndef CLONE_NEWNS
#define CLONE_NEWNS  0x00020000    /* Just detach from the tree */
#endif /* MNT_DETACH */

UINT1               gau1NamespaceList[MAX_NETNS_PATH];
PUBLIC UINT4        IssNetnsAdd (char *name);
PUBLIC UINT4        IssNetnsDelete (char *name);
PUBLIC VOID         IssNetnsAddAll (VOID);
PUBLIC VOID         IssNetnsDeleteAll (VOID);
#endif

static tOsixCfg     LrOsixCfg;
static tTimerCfg    LrTimerCfg;
static tMemPoolCfg  LrMemPoolCfg;
static tBufConfig   LrBufConfig;

tSystemSize         gSystemSize;

/* Assumption: by default, system does not have mgmt port */
UINT4               gu4MgmtPort = FALSE;

/* Assumption: by default, system will not have 
   routing  over oob/(linux/fsip) vlan interface */
UINT4               gu4MgmtIntfRouting = FALSE;

UINT4               gu4SysTimeTicks;

static char        *gpPimMode;
static UINT4        gu4PimMode;

static UINT4        gu4CurIndex = 0;
static tTskStatus   gCurTskStatus = TSK_BEFORE_INIT;

tOsixTaskId         gu4RootTaskId;
tOsixSemId          gSeqSemId;

#ifdef KERNEL_WANTED
#ifdef OS_RTLINUX
#define DIAG_SHELL_TASK         "tDIA"
#define DIAG_SHELL_TASK_PRIORITY 50
/* refer : system.c for BCMCLI stack size */
#define DIAG_STACK_SIZE           (128*1024)

tOsixTaskId         u4DiagTaskId = 0;
extern int          bmw_init;
extern VOID         InitializeBcm (INT1 *pi1Param);
#endif
INT4                gi4DevFd;    /* Interface between ISS and MUX Module */
INT4                gi4NetDevFd;    /* Interface between ISS ans ISS Netdevice
                                       Module */

#endif
#if ( (defined(OS_VXWORKS) || defined(OS_VX2PTH_WRAP) || defined(OS_CPSS_MAIN_OS)|| defined (LION) || defined (LION_DB) || defined (MRVLLS)) \
    || defined (EXTERNAL_MAIN_WANTED) || defined(PETRA_WANTED)|| defined (SECURITY_KERNEL_MAKE_WANTED))
int
LrMain (int argc, char *argv[])
#else
int
main (int argc, char *argv[])
#endif
{
    UINT4               u4PeerNodeId = 0;
    UINT4               u4SelfNodeId = SELF;
    INT1                ai1Log[256];
    INT4                i4RetStatus = 0;

#ifdef ISSSZ_WANTED
    FILE               *fp = NULL;
#endif

#define  XSTR(str) STR(str)
#define STR(str) #str
#if _BullseyeCoverage
    cov_file (XSTR (COVF));
#endif

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    IssNetnsAddAll ();

    int                 i4Netns;
    i4Netns = open ("/proc/1/ns/net", O_RDONLY);
    if (i4Netns < 0)
    {
        perror ("Cannot open network namespace");
        return NETIPV4_FAILURE;
    }
    i4RetStatus = (INT4) setns (i4Netns, CLONE_NEWNET);
    if (i4RetStatus < 0)
    {
        perror ("seting the network namespace failed");
        return NETIPV4_FAILURE;
    }

#endif

    MEMSET (ai1Log, 0, sizeof (ai1Log));

#ifdef KERNEL_WANTED
#ifdef KERNEL_2_6
    sigset_t            signal_mask;    /* signals to block */
    sigemptyset (&signal_mask);
    sigaddset (&signal_mask, SIGPIPE);
    pthread_sigmask (SIG_SETMASK, &signal_mask, NULL);
#endif
#endif
    UNUSED_PARAM (u4PeerNodeId);
    UNUSED_PARAM (argc);
    UNUSED_PARAM (argv);
    UNUSED_PARAM (gpPimMode);
#ifndef ISS_WANTED
    UNUSED_PARAM (gu4PimMode);
#endif /* !ISS_WANTED */
#ifdef SMOD_WANTED
    if (0 == STRCMP (argv[0], "-sk"))
    {
        SecSetSysOperMode (SEC_KERN);
    }
    else if (0 == STRCMP (argv[0], "-su"))
    {
        SecSetSysOperMode (SEC_KERN_USER);
    }
    else
    {
        SecSetSysOperMode (SEC_USER);
    }
#endif
#ifdef ISSSZ_WANTED
    /* Register Sizing Information for all modules */
    IssSzRegisterAllModuleSizingParams ();
    /* Update the System Sizing structure with Default Values */
    UpdateDefaultSystemSizingInfo ();

    /* Update Sizing information for all modules */
    STRNCPY (gau1SizingFileName, ISS_SIZING_FILE_NAME, ISS_FILE_NAME_LENGTH);
    fp = IssSzOpenFile (gau1SizingFileName);
    if (fp == NULL)
    {
        UtlTrcPrint (" Unable to open sizing file system.size \n");
        IssSzUpdateDefaultSizingInfo ();
        UtlTrcPrint
            (" System Size Data Structure updated with Default Values \n");
    }
    else
    {
        IssSzUpdateSizingParams (fp);
        fclose (fp);
    }
#endif

#ifdef ISS_WANTED
    /* If the CUSTOM_STARTUP switch is defined, the CustomStartup function
     * should be implemented suitably. eg., reading parameters from
     * flash or file system etc.
     */
    CustomStartup (argc, argv);

    gu4PimMode = IssGetPimModeFromNvRam ();

    UtlTrcLog (1, 1, "LR",
               "\r\n      This switch software is implemented using Open "
               "sources from OpenSSL,OpenSSH,"
#ifdef NETCONF_WANTED
               "OpenYuma Netconf "
#endif
               "and other open source community."
               "\r\nTo see a full description, type "
               "\"show system acknowledgement\"\r\n");

#endif /* ISS_WANTED */

    gu4SysTimeTicks = OsixGetTps ();
    /* Initialize Osix */
    LrOsixCfg.u4MaxTasks = SYS_MAX_TASKS;
    LrOsixCfg.u4MaxQs = SYS_MAX_QUEUES;
    LrOsixCfg.u4MaxSems = SYS_MAX_SEMS;
    LrOsixCfg.u4MyNodeId = u4SelfNodeId;
    LrOsixCfg.u4TicksPerSecond = SYS_TIME_TICKS_IN_A_SEC;
    LrOsixCfg.u4SystemTimingUnitsPerSecond = SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

#if defined(BCM53415_WANTED)
    /*This is to make sure SYS_TIME_TICKS_IN_A_SEC and gu4SysTimeTicks are same */
    gu4SysTimeTicks = SYS_TIME_TICKS_IN_A_SEC;
#endif

    if (OsixInit (&LrOsixCfg) != OSIX_SUCCESS)
    {
        SNPRINTF ((CHR1 *) ai1Log, sizeof (ai1Log),
                  "OsixInit Fails !!!!!!!!!!!!!!!!!!!!!!");
        UtlTrcPrint ((CHR1 *) ai1Log);
        return (1);
    }
#ifdef SMOD_WANTED
    if (SecGetSysOperMode () == SEC_KERN)
    {
        /* Initialize MemPool manager */
        LrMemPoolCfg.u4MaxMemPools = SEC_KERN_MAX_MEMPOOLS;
    }
    else
#endif
    {
        /* Initialize MemPool manager */
        LrMemPoolCfg.u4MaxMemPools = SYS_MAX_MEMPOOLS;
    }
    LrMemPoolCfg.u4NumberOfMemTypes = 0;
    LrMemPoolCfg.MemTypes[0].u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrMemPoolCfg.MemTypes[0].u4NumberOfChunks = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4StartAddr = 0;
    LrMemPoolCfg.MemTypes[0].MemChunkCfg[0].u4Length = 0;

    if (MemInitMemPool (&LrMemPoolCfg) != MEM_SUCCESS)
    {
        UtlTrcLog (1, 1, "LR", "MemInitMemPool Fails !!!!!!!!!!!!!!!!!!!!!!");

        return (1);
    }

    /* Initialize Buffer manager */
    LrBufConfig.u4MemoryType = MEM_DEFAULT_MEMORY_TYPE;
    LrBufConfig.u4MaxChainDesc = SYS_MAX_BUF_DESC;
    LrBufConfig.u4MaxDataBlockCfg = 1;
    LrBufConfig.DataBlkCfg[0].u4BlockSize = SYS_MAX_BUF_BLOCK_SIZE;
    LrBufConfig.DataBlkCfg[0].u4NoofBlocks = SYS_MAX_NUM_OF_BUF_BLOCK;

    if (BufInitManager (&LrBufConfig) != CRU_SUCCESS)
    {
        UtlTrcLog (1, 1, "LR", "BufInitManager Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }
    /* Initialize the Timer Manager */
    LrTimerCfg.u4MaxTimerLists = SYS_MAX_NUM_OF_TIMER_LISTS;
    if (TmrTimerInit (&LrTimerCfg) != TMR_SUCCESS)
    {
        UtlTrcLog (1, 1, "LR", "TmrTimerInit Fails !!!!!!!!!!!!!!!!!!!!!!");
        return (1);
    }

    if (IndexManagerUtilInit () != INDEX_SUCCESS)
    {
        return (1);
    }

    /* Semaphore is created here for LOCK being used in 
     * RBTree creation for unique sem name*/

    if (OsixCreateSem ((const UINT1 *) "RBSEM", 1, 0, &gu4SemNameSemId) ==
        OSIX_FAILURE)
    {
        return (1);
    }
#ifdef SNMP_3_WANTED
    SnmpONMain ();
#endif
    /* Set the system time */
    OsixSetLocalTime ();

#ifdef SNMP_3_WANTED
    /* Initilization of the SNMP Mib Registration Indexmanager Init list */
    SNMPIndexMangerListInit ();
#else
#ifdef ISS_WANTED
    /* If SNMP is not defined, atleast give memory for snmp utils */
    if (UtlSnmpMemInit () == OSIX_FAILURE)
    {
        return (1);
    }
#endif
#endif

#define BUDDY_MAX_INST 30
    if (MemBuddyInit (BUDDY_MAX_INST) == BUDDY_FAILURE)
    {
        UtlTrcLog (1, 1, "LR", "MemBuddyInit Fails !!!!!!!!!!!!!!!!!!!!!!");

        return (1);
    }
#ifdef ISS_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if (IssGetStackPortCountFromNvRam () == 1)
        {
            /* only one higig port for stacking purpose
             * remaining higig ports will be used for 
             * normal switching/routing */
            i4RetStatus = system ("cp rc.soc_stk_1 rc.soc");
        }
        else if (IssGetStackPortCountFromNvRam () == 2)
        {
            i4RetStatus = system ("cp rc.soc_stk_2 rc.soc");
        }
        else if (IssGetStackPortCountFromNvRam () == 3)
        {
            i4RetStatus = system ("cp rc.soc_stk_3 rc.soc");
        }
        else if (IssGetStackPortCountFromNvRam () == 4)
        {
            i4RetStatus = system ("cp rc.soc_stk_4 rc.soc");
        }
        else
        {
            i4RetStatus = system ("cp rc.soc_stk rc.soc");
        }
    }
#endif
#ifdef KERNEL_WANTED
#ifdef OS_RTLINUX
#ifdef BCMX_WANTED
    if (OsixTskCrt
        ((UINT1 *) DIAG_SHELL_TASK, DIAG_SHELL_TASK_PRIORITY,
         DIAG_STACK_SIZE, (OsixTskEntry) InitializeBcm, 0,
         &u4DiagTaskId) != OSIX_SUCCESS)
    {
        return 0;
    }

    /* BCM diag shell waits for ISS Command to toggle bmw_init */
    while (!bmw_init)
    {
        sleep (2);
    }
#endif /* BCMX_WANTED */
#endif /* OS_RTLINUX */

    /* Load the ISS Kernel Module into the system */
    i4RetStatus = system ("insmod " KERNEL_MODULE_NAME);

    /* Open a Device for communicating between the user space ISS module and
     * the kernel module in ISS. This device is common for both BCM and Marvell
     * 6095 platform. This device will be used for all communication including
     * packet Rx/Tx, netdevice management, kernel database management
     * between user space and kernel space in BCM. In Marvell case 
     * this device is used only for protocol registration and packet Tx/Rx. 
     * A separate device is used for netdevice creation, management, deletion 
     * in Marvell 6095 case */

    /* Define the major & minor numbers based on the number of ioctl request */
    i4RetStatus = system ("/bin/mknod " GDD_DEVICE_FILE_NAME " c 100 0");

    gi4DevFd = FileOpen ((const UINT1 *) GDD_DEVICE_FILE_NAME, OSIX_FILE_RO);

    if (gi4DevFd < 0)
    {
        UtlTrcLog (1, 1, "LR", "Can't open device file: <%s>\n",
                   GDD_DEVICE_FILE_NAME);

        return 0;
    }
#ifdef MUX_WANTED
    /* Load the ISS NetDevice Kernel Module into the system */

    i4RetStatus = system ("insmod " NETDEV_MODULE_NAME);

    /* Create the Character Device between ISS <-> ISS NetDevice Module */
    i4RetStatus = system ("/bin/mknod " NET_DEVICE_FILE_NAME " c 105 0");

    gi4NetDevFd = FileOpen ((const UINT1 *) NET_DEVICE_FILE_NAME, OSIX_FILE_RO);

    if (gi4NetDevFd < 0)
    {
        UtlTrcLog (1, 1, "LR", "Can't open device file: <%s>\n",
                   NET_DEVICE_FILE_NAME);
        return 0;
    }
#endif /* MUX_WANTED */
#endif /* KERNEL_WANTED */

    if (OsixTskCrt
        ((UINT1 *) "ROOT", (0 | OSIX_SCHED_RR), OSIX_DEFAULT_STACK_SIZE,
         (OsixTskEntry) issSystemStart, 0, &gu4RootTaskId) == OSIX_SUCCESS)
    {
        Fsap2Start ();
    }
    else
    {
        return (1);
    }
    UNUSED_PARAM (i4RetStatus);
    return (0);
}

VOID
issSystemStart (INT1 *pi1Param)
{
    INT4                i4Count = 0;
    INT4                i4RetVal = 0;
    INT4                i4RetStatus = 0;

#ifdef MSR_WANTED
    UINT4               u4Event = 0;
#endif
   /***/
    tOsixTaskId         TskId = 0;

    INT4                (*pModEntFn) (INT1 *) = NULL;
    INT1                i1Param = 0;
    tOsixMemStatus      HealthStatus;

    UNUSED_PARAM (pi1Param);

    OsixTskIdSelf (&gu4RootTaskId);

    TrieInit ();
    TrieLibInit ();

    if (TrieLibMemPoolInit () == OSIX_FAILURE)
    {
        UtlTrcLog (1, 1, "LR", "ERROR: TrieLibMemPoolInit failed!!\n");

        return;
    }
    /*Creation of MemPools required by UTIL module */
    if (UtilSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        UtlTrcLog (1, 1, "LR", "ERROR: UtilSizingMemCreateMemPools failed!!\n");

        return;
    }
    /*Assigning mempool Id's */
    UtlShMemInitPoolIds ();
    gBitListPoolId = UTILMemPoolIds[MAX_UTIL_BITLIST_COUNT_SIZING_ID];
    gLocalPortListPoolId =
        UTILMemPoolIds[MAX_UTIL_LOCAL_PORTLIST_COUNT_SIZING_ID];
    gVlanListSizePoolId =
        UTILMemPoolIds[MAX_UTIL_VLAN_LIST_SIZE_COUNT_SIZING_ID];
    gVlanIdSizePoolId = UTILMemPoolIds[MAX_UTIL_VLAN_ID_SIZE_COUNT_SIZING_ID];

    FsUtilBitListInit (MAX_UTIL_BITLIST_COUNT);
    UtilPlstInitLocalPortList (MAX_UTIL_LOCAL_PORTLIST_COUNT);
/* Initializing the MEMPOOLS for VLAN_LIST_SIZE(512) of count 20. */
    UtilVlanInitVlanListSize (MAX_UTIL_VLAN_LIST_SIZE_COUNT);
/* Initializing the MEMPOOLS for VLAN_LIST_SIZE(4096) of count 20. */
    UtilVlanInitVlanIdSize (MAX_UTIL_VLAN_ID_SIZE_COUNT);

#ifdef ROUTEMAP_WANTED
    /* Initialise the Route Map module */
    RMapInit ();
#endif

    /* The "SEQ" semaphore is used to ensure that each protocol / module
     * is initialized before the next protocol / module initialization 
     * is invoked.
     */
    if (OsixCreateSem ((CONST UINT1 *) "SEQ", 0, 0, &gSeqSemId) != OSIX_SUCCESS)
    {
        return;
    }

   /***/

    /* As long as there are protocols / modules to be initialized
     * do the following:
     */
    while (ENTRY_FN (gu4CurIndex) != NULL)
    {
        gCurTskStatus = TSK_BEFORE_INIT;

        /* When a module is to be initialized, the Task Name for the
         * module will be set to NULL. This is to ensure that the 
         * module initialization function is invoked directly.
         */
        if (gaInitTable[gu4CurIndex].pu1TskName == NULL)
        {
            pModEntFn = (INT4 (*)(INT1 *)) (ENTRY_FN (gu4CurIndex));
            pModEntFn (&i1Param);
            /* Module reports failure by lrInitComplete
               Since, every module has different return type */
            i4RetVal = OSIX_SUCCESS;
        }
        else
            /* When a protocol is to be intialized, the protocol task is 
             * spawned with the parameters from the InitTable. The protocol
             * task will perform all the initializations related to the 
             * protocol and relinquish the "SEQ" semaphore before waiting
             * for events / messages.
             */
        {
            i4RetVal =
                (INT4) OsixTskCrt ((UINT1 *) gaInitTable[gu4CurIndex].
                                   pu1TskName,
                                   (gaInitTable[gu4CurIndex].
                                    u4TskPriority |
                                    gaInitTable[gu4CurIndex].u4SchedPolicy),
                                   gaInitTable[gu4CurIndex].u4TskStack,
                                   (OsixTskEntry) ENTRY_FN (gu4CurIndex),
                                   0, &TskId);
        }

        /* When either the module initialization failed or the protocol
         * task could not be spawned, break from the initialization sequence.
         */

        if (i4RetVal == OSIX_FAILURE)
        {

            UtlTrcLog (1, 1, "LR", "ERROR: %s - Task failed, exiting!!\n",
                       gaInitTable[gu4CurIndex].pu1TskName);

            break;
        }

        /* After the module init has been invoked or protocol task has been
         * spawned, wait on the "SEQ" semaphore. When the semaphore is given
         * back by the protocol, check if the initialization was successful.
         */
        if ((OsixSemTake (gSeqSemId) != OSIX_SUCCESS) ||
            (gCurTskStatus != TSK_INIT_SUCCESS))
        {

            UtlTrcLog (1, 1, "LR", "Task Init Failed for <%s> \n",
                       gaInitTable[gu4CurIndex].pu1TskName);
            i4RetVal = OSIX_FAILURE;
            break;
        }
        gu4CurIndex++;
    }
    /* If the module / protocol was not initialized properly, then shutdown
     * all the modules / protocols initialized so far by invoking the related
     * shutdown routines (when one is present). The modules / protocols will 
     * be shut down in the reverse order of initialization, i.e., the last
     * task successfully initialized will be shutdown first and so on.
     */
    if (ENTRY_FN (gu4CurIndex) != NULL)
    {
        for (i4Count = gu4CurIndex; i4Count >= 0; i4Count--)
        {
            if (SHUTDN_FN ((UINT4) i4Count) != NULL)
            {
                (SHUTDN_FN ((UINT4) i4Count)) ();
            }
        }
        gu4CurIndex = 0;
    }
    else
    {
        /* When all the modules/protocols have been initialized
         * successfully, indicate CFA to start packet RX.
         */
#ifdef CFA_WANTED
        if (CfaNotifyStartPktProcessEvent () != OSIX_SUCCESS)
        {
            UtlTrcLog (1, 1, "LR",
                       "CFA_PACKET_PROCESS_START_EVENT send event failed.\n");
            return;
        }
#endif

#ifdef MSR_WANTED
        /* MSR task should be spawned only after CFA handles CFA_IP_UP_EVENT */
        OsixEvtRecv (gu4RootTaskId, ISS_SPAWN_MSR_TASK, OSIX_WAIT, &u4Event);

        i1Param = ISS_SPAWN_MSR_TASK;
        /* Hook added to allow customer specific configurations, after
         * IP_UP_EVENT is processed by CFA which ensures H/W initialisation
         * is complete now.
         */
        TgtCustomStartup (&i1Param);

        /* Decremented the priority of MSR so that it is less than
         * the L2 modules, and they handle the interface - context mapping
         * before the protocol specific configurations are restored. */

        if (OsixTskCrt
            ((UINT1 *) "MSR", (200 | OSIX_SCHED_RR),
             OSIX_DEFAULT_STACK_SIZE, (OsixTskEntry) MibSaveTaskMain,
             0, &TskId) != OSIX_SUCCESS)
        {
            UtlTrcLog (1, 1, "LR", "MSR Task creation failed.\n");

            return;
        }
#ifdef MBSM_WANTED
        /* After successful creation of MSR Task,Send an event Cfa.
         * Cfa will trigger MSR, only after receving this event.
         * */
        if (CfaNotifyStartBootupEvent () != OSIX_SUCCESS)
        {

            UtlTrcLog (1, 1, "LR",
                       "CFA_BOOTUP_START_EVENT send event failed.\n");

            return;
        }
#endif /* MBSM_WANTED */
#else

#ifdef ICCH_WANTED
        /* ICCH should proceed with it's work after all tasks are spawned */
        /* If MSR is not defined, ICCH interface to be created from lrmain.c */
        HbIcchResumeProtocolOperation ();
#endif /* ICCH_WANTED */

        /* Since MSR is not there, Relinquish the "SEQ" semaphore and return */
        if (OsixSemGive (gSeqSemId) != OSIX_SUCCESS)
        {

            UtlTrcLog (1, 1, "LR", "SEQ sem give failed\n");

            return;
        }
#endif

#ifdef RM_WANTED
        /* RM should proceed with it's work only after all tasks are
         * spawned
         */
        if (OsixSemTake (gSeqSemId) != OSIX_SUCCESS)
        {

            UtlTrcLog (1, 1, "LR", "SEQ sem take failed\n");

            return;
        }
        RmInformOtherModulesBootUp (NULL);
        /* Since MSR is not there, Relinquish the "SEQ" semaphore and return */
        if (OsixSemGive (gSeqSemId) != OSIX_SUCCESS)
        {
            UtlTrcLog (1, 1, "LR", "SEQ sem give failed\n");

            return;
        }

#endif /* RM_WANTED */
    }
    /* Delete the "SEQ" semaphore */
    OsixSemDel (gSeqSemId);

    if (i4RetVal == OSIX_FAILURE)
    {
        /* When there is a Task init failure, do not proceed.
         * When task init failed occur, log isshealthstatus
         * as non recoverable failure occured */

        HealthStatus.u1ErrorStatus = DOWN_NONRECOVERABLE_ERR;
        HealthStatus.u1MemAllocErrReason = TASK_INIT_FAILURE;
        OsixSetHealthStatus (&HealthStatus);

        UtlTrcLog (2, 2, "LR",
                   "ISS is down due to occurence of TaskInitializationFailure\n");
        return;
    }

    /* Set the Bridge admin status, only if there is no MSR restore option */
#ifdef ISS_WANTED
    if (IssGetRestoreOptionFromNvRam () == ISS_CONFIG_NO_RESTORE)
#endif /* ISS_WANTED */
    {
        /* Enable Bridge by default */
#ifdef BRIDGE_WANTED
        if (nmhSetDot1dBaseBridgeStatus (UP) != SNMP_SUCCESS)
        {
            PRINTF ("ERROR: Enabling STP. Failed!!\n\r");
        }
#endif
        LrSetRestoreDefConfigFlag (OSIX_TRUE);
    }
#if (defined(OS_VX2PTH_WRAP) || defined(OS_CPSS_MAIN_OS))
    OsixTskDel (gu4RootTaskId);
    gu4RootTaskId = 0;
#endif

#ifdef WEBNM_WANTED
    WebPagePrivInit ();
#endif
#ifdef ISS_WANTED
    ISSSetHealthChkStatus (ISS_UP_AND_RUNNING);
#endif
#if (defined(LNXIP4_WANTED) || defined (LNXIP6_WANTED))
    i4RetStatus = system ("killall -9 -q avahi-daemon");
    i4RetStatus = system ("killall -9 -q NetworkManager");
#endif
    UNUSED_PARAM (i4RetStatus);
    return;
}

VOID
lrInitComplete (UINT4 u4Status)
{
    /* Depending on the status returned, set the Task Init Status
     * for the current task.
     */
    if (u4Status == OSIX_SUCCESS)
    {
        gCurTskStatus = TSK_INIT_SUCCESS;
    }
    else
    {
        gCurTskStatus = TSK_INIT_FAILURE;
    }

    /* Relinquish the "SEQ" semaphore and return */
    if (OsixSemGive (gSeqSemId) != OSIX_SUCCESS)
    {
        return;
    }
}

#ifdef ISSSZ_WANTED
VOID
UpdateDefaultSystemSizingInfo (VOID)
{
    gSystemSize.CfaSystemSize.u4SystemMaxIface = SYS_DEF_MAX_INTERFACES;
    gSystemSize.CfaSystemSize.u4SystemMaxPhysicalIface =
        SYS_DEF_MAX_PHYSICAL_INTERFACES;
    gSystemSize.CfaSystemSize.u4SystemMaxFFMEntries = SYS_DEF_MAX_FFM_ENTRIES;
    gSystemSize.CfaSystemSize.u4SystemMaxTunlIface = SYS_DEF_MAX_TUNL_IFACES;

#ifdef IP6_WANTED
    gSystemSize.Ip6SystemSize.u4Ip6MaxRoutes = MAX_RTM6_ROUTE_TABLE_ENTRIES;
    gSystemSize.Rtm6SystemSize.u4Rtm6MaxRoutes =
        gSystemSize.Ip6SystemSize.u4Ip6MaxRoutes;
    gSystemSize.Ip6SystemSize.i4Ip6MaxLogicalIfaces = MAX_IP6_INTERFACES;
    gSystemSize.Ip6SystemSize.u4Ip6MaxTunnelIfaces = MAX_IP6_TUNNEL_INTERFACES;
    gSystemSize.Ip6SystemSize.u4Ip6MaxAddr = MAX_IP6_UNICAST_ADDRESS;
    gSystemSize.Rtm6SystemSize.u4Rtm6MaxRoutes +=
        gSystemSize.Ip6SystemSize.u4Ip6MaxAddr;
    gSystemSize.Ip6SystemSize.u4Ip6MaxFragReasmList = MAX_IP6_REASM_ENTRIES;
    gSystemSize.Ip6SystemSize.u4Ip6MaxCacheEntries = MAX_IP6_ND6_CACHE_ENTRIES;
    gSystemSize.Ip6SystemSize.u4Ip6MaxPmtuEntries = MAX_IP6_PMTU_ENTRIES;
#endif

#ifdef RIP6_WANTED
    gSystemSize.Rip6SystemSize.u4Rip6MaxPeerEntries = MAX_RIP6_PEERS;
    gSystemSize.Rip6SystemSize.u4Rip6MaxRoutes = MAX_RIP6_ROUTE_ENTRIES;
#ifdef IP6_WANTED
    gSystemSize.Rtm6SystemSize.u4Rtm6MaxRoutes +=
        gSystemSize.Rip6SystemSize.u4Rip6MaxRoutes;
#endif /* IP6_WANTED */
#endif /* RIP6_WANTED */

#ifdef OSPF3_WANTED
#ifdef IP6_WANTED
    gSystemSize.Rtm6SystemSize.u4Rtm6MaxRoutes +=
        MAX_OSPFV3_INTERNAL_ROUTES + OSPFV3_MAX_LEARNT_EXT_ROUTES;
#endif /* IP6_WANTED */
#endif /* OSPF3_WANTED */

#ifdef MPLS_WANTED
    gSystemSize.MplsSystemSize.u4MplsMaxTunnels = MPLS_DEF_MAX_TUNNELS;
    gSystemSize.MplsSystemSize.u4MplsMaxPwId = MAX_L2VPN_PWID_ENTRIES;
#endif
#ifdef RIP_WANTED
    gSystemSize.RipSystemSize.u4RipMaxPeerEntries = MAX_RIP_PEERS;
    gSystemSize.RipSystemSize.u4RipMaxRoutes = MAX_RIP_ROUTE_ENTRIES;
#endif

#ifdef TCP_WANTED
    gSystemSize.TcpSystemSize.u4TcpMaxNumOfTCB = TCP_DEF_MAX_NUM_OF_TCB;
#endif

#ifdef MLD_WANTED
    gSystemSize.MldSystemSize.u4MldMaxCacheEntries = MLD_DEF_MAX_CACHE_ENTRIES;
    gSystemSize.MldSystemSize.u4MldMaxRoutingProtocol =
        MLD_DEF_MAX_ROUTINGPROTO;
#endif

#ifdef IPOA_WANTED
    gSystemSize.IpoaSystemSize.u4IpoaMaxATEntries = IPOA_DEF_MAX_AT_ENTRIES;
    gSystemSize.IpoaSystemSize.u4IpoaMaxIpoaConnections =
        IPOA_DEF_MAX_IPOA_CONNECTIONS;
    gSystemSize.IpoaSystemSize.u4IpoaAarpMaxCacheEntries =
        IPOA_DEF_MAX_AARP_CACHE_ENTRIES;
#endif

#ifdef NAT_WANTED
    gSystemSize.NatSystemSize.u4NatTypicalNumOfEntries =
        NAT_DEF_MAX_TYPICAL_ENTRIES;
#endif

#ifdef FIREWALL_WANTED
    gSystemSize.FwlAclSystemSize.u4FwlMaxNumOfFilters = FWL_DEF_MAX_FILTERS;
    gSystemSize.FwlAclSystemSize.u4FwlMaxNumOfRules = FWL_DEF_MAX_RULES;
#endif

#ifdef MFWD_WANTED
    gSystemSize.MfwdSystemSize.u2MfwdMaxMrps = MFWD_DEF_MAX_MRPS;
#endif

#ifdef QoS_WANTED
    gSystemSize.QoSSystemSize.u4QoSMaxNbrOfClassifiers = QoS_DEF_MAX_CLFR;
    gSystemSize.QoSSystemSize.u4QoSMaxNbrOfMeters = QoS_DEF_MAX_METER;
    gSystemSize.QoSSystemSize.u4QoSMaxNbrOfActions = QoS_DEF_MAX_ACTION;
    gSystemSize.QoSSystemSize.u4QoSMaxNbrOfBuffers = QoS_DEF_MAX_BUFF;
#ifdef QoS_INTSRV_WANTED
    gSystemSize.QoSSystemSize.u4QoSMaxNbrOfFlows = QoS_DEF_MAX_FLOWS;
#endif
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
    gSystemSize.PimSystemSize.u4PimMaxSources = PIM_DEF_MAX_SOURCES;
    gSystemSize.PimSystemSize.u4PimMaxRps = PIM_DEF_MAX_RPS;
#endif

#ifdef RADIUS_WANTED
    gSystemSize.RadiusSystemSize.u4RadiusMaxUserEntries = MAX_RAD_USER_ENTRIES;
#endif
#ifdef OSPF_WANTED
    gSystemSize.OspfSystemSize.u4OspfMaxAreas = MAX_OSPF_AREAS;
    gSystemSize.OspfSystemSize.u4OspfMaxLSAperArea = OSPF_DEF_MAX_LSA_PER_AREA;
    gSystemSize.OspfSystemSize.u4OspfMaxExtLSAs = OSPF_DEF_MAX_EXT_LSAS;
    gSystemSize.OspfSystemSize.u4OspfMaxSelfOrgLSAs = MAX_OSPF_LSA_DESCRIPTORS;
    gSystemSize.OspfSystemSize.u4OspfMaxRoutes = MAX_OSPF_RT_ENTRIES;
#endif

#ifdef BRIDGE_WANTED
    gSystemSize.BridgeSystemSize.u4BridgeMaxFdbEntries = BRIDGE_DEF_MAX_FDB;
    gSystemSize.BridgeSystemSize.u4BridgeMaxFilterEntries =
        BRIDGE_DEF_MAX_FILTERS;
#endif

#ifdef RSVP_WANTED
    gSystemSize.RsvpSystemSize.u4RsvpMaxSessionEntries = RSVP_DEF_MAX_SESSION;
    gSystemSize.RsvpSystemSize.u4RsvpMaxSenderEntries = RSVP_DEF_MAX_SENDER;
    gSystemSize.RsvpSystemSize.u4RsvpMaxRsbEntries = RSVP_DEF_MAX_RSB;
    gSystemSize.RsvpSystemSize.u4RsvpMaxTcsbEntries = RSVP_DEF_MAX_TCSB;
    gSystemSize.RsvpSystemSize.u4RsvpMaxResvFwdEntries = RSVP_DEF_MAX_RESV_FWD;
    gSystemSize.RsvpSystemSize.u4RsvpFlowDescPoolSize = RSVP_DEF_MAX_FLOW_DESCR;
    gSystemSize.RsvpSystemSize.u4RsvpTmrParamPoolSize = RSVP_DEF_MAX_TMR_PARAM;
#endif

#if defined(IPSECv4_WANTED) || defined(IPSECv6_WANTED)
    gSystemSize.SecSystemSize.u4SecMaxNoSA = MAX_IPSEC6_MAX_SA;
    gSystemSize.SecSystemSize.u4SecMaxContextTableSize = IPSEC_DEF_MAX_TBL_SIZE;
#endif
}
#endif

/************************************************************************
  Function Name   : Functions to Get/Set System Sizing information.   
Description     : Takes a pointer to a module specific structure,   
and gets or sets it accordingly.
Input(s)        :                                                  
: pParams - A protocol specific structure 
containing sizing information for that
module. For the SetXXXSizingParams functions
carries the new values.
Output(s)       :                                        
: pParams - Filled sizing information - relevant only
to GetXXXSizingParams class of functions.
Returns         : VOID                                     
 ************************************************************************/
#ifdef BGP_WANTED
VOID
GetBgpSizingParams (tBgpSystemSize * pParams)
{
    *pParams = gSystemSize.BgpSystemSize;
}

VOID
SetBgpSizingParams (tBgpSystemSize * pParams)
{
    gSystemSize.BgpSystemSize = *pParams;
}
#endif

#ifdef CFA_WANTED
VOID
GetCfaSizingParams (tCfaSystemSize * pParams)
{
    *pParams = gSystemSize.CfaSystemSize;
}

VOID
SetCfaSizingParams (tCfaSystemSize * pParams)
{
    gSystemSize.CfaSystemSize = *pParams;
}
#endif

#ifdef DNS_RELAY_WANTED
VOID
GetDnsRelaySizingParams (tDnsRelaySystemSize * pParams)
{
    *pParams = gSystemSize.DnsRelaySystemSize;
}

VOID
SetDnsRelaySizingParams (tDnsRelaySystemSize * pParams)
{
    gSystemSize.DnsRelaySystemSize = *pParams;
}
#endif

#ifdef DVMRP_WANTED
VOID
GetDvmrpSizingParams (tDvmrpSystemSize * pParams)
{
    *pParams = gSystemSize.DvmrpSystemSize;
}

VOID
SetDvmrpSizingParams (tDvmrpSystemSize * pParams)
{
    gSystemSize.DvmrpSystemSize = *pParams;
}
#endif

#ifdef FWLACL_WANTED
VOID
GetFwlAclSizingParams (tFwlAclSystemSize * pParams)
{
    *pParams = gSystemSize.FwlAclSystemSize;
}

VOID
SetFwlAclSizingParams (tFwlAclSystemSize * pParams)
{
    gSystemSize.FwlAclSystemSize = *pParams;
}
#endif

#ifdef IP_WANTED
VOID
GetIpSizingParams (tIpSystemSize * pParams)
{
    *pParams = gSystemSize.IpSystemSize;
}

VOID
SetIpSizingParams (tIpSystemSize * pParams)
{
    gSystemSize.IpSystemSize = *pParams;
}
#endif
VOID
GetRtmSizingParams (tRtmSystemSize * pParams)
{
    *pParams = gSystemSize.RtmSystemSize;
}

#ifdef IP6_WANTED
VOID
GetIp6SizingParams (tIp6SystemSize * pParams)
{
    *pParams = gSystemSize.Ip6SystemSize;
}

VOID
SetIp6SizingParams (tIp6SystemSize * pParams)
{
    gSystemSize.Ip6SystemSize = *pParams;
}

VOID
GetRtm6SizingParams (tRtm6SystemSize * pParams)
{
    *pParams = gSystemSize.Rtm6SystemSize;
}
#endif

#ifdef RIP6_WANTED
VOID
GetRip6SizingParams (tRip6SystemSize * pParams)
{
    *pParams = gSystemSize.Rip6SystemSize;
}

VOID
SetRip6SizingParams (tRip6SystemSize * pParams)
{
    gSystemSize.Rip6SystemSize = *pParams;
}
#endif

#ifdef IPOA_WANTED
VOID
GetIpoaSizingParams (tIpoaSystemSize * pParams)
{
    *pParams = gSystemSize.IpoaSystemSize;
}

VOID
SetIpoaSizingParams (tIpoaSystemSize * pParams)
{
    gSystemSize.IpoaSystemSize = *pParams;
}
#endif

#ifdef MFWD_WANTED
VOID
GetMfwdSizingParams (tMfwdSystemSize * pParams)
{
    *pParams = gSystemSize.MfwdSystemSize;
}

VOID
SetMfwdSizingParams (tMfwdSystemSize * pParams)
{
    gSystemSize.MfwdSystemSize = *pParams;
}
#endif

#ifdef MPLS_WANTED
VOID
GetMplsSizingParams (tMplsSystemSize * pParams)
{
    *pParams = gSystemSize.MplsSystemSize;
}

VOID
SetMplsSizingParams (tMplsSystemSize * pParams)
{
    gSystemSize.MplsSystemSize = *pParams;
}
#endif

#ifdef NAT_WANTED
VOID
GetNatSizingParams (tNatSystemSize * pParams)
{
    *pParams = gSystemSize.NatSystemSize;
}

VOID
SetNatSizingParams (tNatSystemSize * pParams)
{
    gSystemSize.NatSystemSize = *pParams;
}
#endif

#ifdef OSPF_WANTED
VOID
GetOspfSizingParams (tOspfSystemSize * pParams)
{
    *pParams = gSystemSize.OspfSystemSize;
}

VOID
SetOspfSizingParams (tOspfSystemSize * pParams)
{
    gSystemSize.OspfSystemSize = *pParams;
}
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
VOID
GetPimSizingParams (tPimSystemSize * pParams)
{
    *pParams = gSystemSize.PimSystemSize;
}

VOID
SetPimSizingParams (tPimSystemSize * pParams)
{
    gSystemSize.PimSystemSize = *pParams;
}
#endif

#ifdef QOS_WANTED
VOID
GetQoSSizingParams (tQoSSystemSize * pParams)
{
    *pParams = gSystemSize.QoSSystemSize;
}

VOID
SetQoSSizingParams (tQoSSystemSize * pParams)
{
    gSystemSize.QoSSystemSize = *pParams;
}
#endif

#ifdef RADIUS_WANTED
VOID
GetRadiusSizingParams (tRadiusSystemSize * pParams)
{
    *pParams = gSystemSize.RadiusSystemSize;
}

VOID
SetRadiusSizingParams (tRadiusSystemSize * pParams)
{
    gSystemSize.RadiusSystemSize = *pParams;
}
#endif

#ifdef RIP_WANTED
VOID
GetRipSizingParams (tRipSystemSize * pParams)
{
    *pParams = gSystemSize.RipSystemSize;
}

VOID
SetRipSizingParams (tRipSystemSize * pParams)
{
    gSystemSize.RipSystemSize = *pParams;
}
#endif

#ifdef RSVP_WANTED
VOID
GetRsvpSizingParams (tRsvpSystemSize * pParams)
{
    *pParams = gSystemSize.RsvpSystemSize;
}

VOID
SetRsvpSizingParams (tRsvpSystemSize * pParams)
{
    gSystemSize.RsvpSystemSize = *pParams;
}
#endif

#ifdef TCP_WANTED
VOID
GetTcpSizingParams (tTcpSystemSize * pParams)
{
    *pParams = gSystemSize.TcpSystemSize;
}

VOID
SetTcpSizingParams (tTcpSystemSize * pParams)
{
    gSystemSize.TcpSystemSize = *pParams;
}
#endif

#ifdef MLD_WANTED
VOID
GetMldSizingParams (tMldSystemSize * pParams)
{
    *pParams = gSystemSize.MldSystemSize;
}

VOID
SetMldSizingParams (tMldSystemSize * pParams)
{
    gSystemSize.MldSystemSize = *pParams;
}
#endif

#ifdef IPSECv4_WANTED
VOID
GetSecSizingParams (tSecSystemSize * pParams)
{
    *pParams = gSystemSize.SecSystemSize;
}

VOID
SetSecSizingParams (tSecSystemSize * pParams)
{
    gSystemSize.SecSystemSize = *pParams;
}
#else
#ifdef IPSECv6_WANTED
VOID
GetSecSizingParams (tSecSystemSize * pParams)
{
    *pParams = gSystemSize.SecSystemSize;
}

VOID
SetSecSizingParams (tSecSystemSize * pParams)
{
    gSystemSize.SecSystemSize = *pParams;
}
#endif /* IPSECv6_WANTED */
#endif /* IPSECv4_WANTED */

UINT1
LrGetRestoreDefConfigFlag (VOID)
{
    return (gu1ResDefConfig);
}

VOID
LrSetRestoreDefConfigFlag (UINT1 u1Status)
{
    gu1ResDefConfig = u1Status;
#ifdef ISS_WANTED
    IssCustConfigRestore ();
#endif
}

#ifdef MSR_WANTED
UINT4
LrSendEventToLrTask (UINT4 u4Event)
{
    OsixEvtSend (gu4RootTaskId, u4Event);
    return OSIX_SUCCESS;
}
#endif

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
PUBLIC UINT4
IssNetnsAdd (char *name)
{
    CHR1                c1netw_name_sp_path[MAX_NETNS_PATH];
    INT4                i4Netns_file_desc;

    SNPRINTF (c1netw_name_sp_path, sizeof (c1netw_name_sp_path), "%s/%s",
              NETWORK_NS_RUN_DIR, name);

    /* Create the base netns directory if it doesn't exist */
    mkdir (NETWORK_NS_RUN_DIR, S_IXGRP | S_IRWXU | S_IXOTH | S_IRGRP | S_IROTH);

    /* Create the filesystem state */
    i4Netns_file_desc =
        (INT4) FileOpen ((const UINT1 *) c1netw_name_sp_path,
                         O_EXCL | O_CREAT | O_RDONLY);
    if (i4Netns_file_desc < 0)
    {
        UtlTrcPrint ("Could not create netw_name_sp_path");
        return OSIX_FAILURE;
    }
    FileClose (i4Netns_file_desc);
    if (unshare (CLONE_NEWNET) < 0)
    {
        UtlTrcPrint ("Failed to create a new network namespace");
        IssNetnsDelete (name);
        return OSIX_FAILURE;
    }

    if (mount ("/proc/self/ns/net", c1netw_name_sp_path, "none", MS_BIND, NULL)
        < 0)
    {
        UtlTrcPrint ("Failed in bind");
        IssNetnsDelete (name);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
PUBLIC UINT4
IssNetnsDelete (char *name)
{
    CHR1                netns_path[MAX_NETNS_PATH];
    SNPRINTF ((CHR1 *) netns_path, sizeof (netns_path), "%s/%s",
              NETWORK_NS_RUN_DIR, name);
    umount2 (netns_path, MNT_DETACH);
    if (unlink (netns_path) < 0)
    {
        perror ("Cannot remove netns_path");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC VOID
IssNetnsAddAll ()
{
    UINT4               u4Index;
    for (u4Index = 1; u4Index < SYS_DEF_MAX_NUM_CONTEXTS; u4Index++)
    {
        SPRINTF ((CHR1 *) gau1NamespaceList, "ns%d", u4Index);
        IssNetnsAdd ((CHR1 *) gau1NamespaceList);
    }
}

PUBLIC VOID
IssNetnsDeleteAll ()
{
    UINT4               u4Index;
    for (u4Index = 1; u4Index < SYS_DEF_MAX_NUM_CONTEXTS; u4Index++)
    {
        SPRINTF ((CHR1 *) gau1NamespaceList, "ns%d", u4Index);
        IssNetnsDelete ((CHR1 *) gau1NamespaceList);
    }
}
#endif

#endif /* LRMAIN_C */
