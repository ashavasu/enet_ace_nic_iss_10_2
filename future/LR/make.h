######################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved                          #
# ------------------------------------------                          #
# $Id: make.h,v 1.138.2.1 2018/03/05 13:16:01 siva Exp $                   #
#    DESCRIPTION            : Specifies the options and modules to be #
#                             including for building the Linux Router #
#                             Environment.                            #
#######################################################################

.SILENT:

ifeq (${BASE_DIR},)
   BASE_DIR                      = ${PWD}
endif

# BUILD_TYPE can be ISS/STACK
ifeq (${BUILD_TYPE},)
   BUILD_TYPE                   = ISS
endif

# PRODUCT_TYPE can be ISS/WLC/WTP
ifeq (${PRODUCT_TYPE},)
   PRODUCT_TYPE                 = ISS
endif   

# For STACK builds edit the file pkg/make.stack
# to choose individual modules for compilation.
#
# For ISS/WLC/WTP build choose a package.

# PACKAGE can be WORKGROUP/ENTERPRISE/METRO/METRO_E/WSS
ifeq (${PACKAGE},)
   PACKAGE                      = METRO_E
endif

# TARGET_OS can be OS_LINUX_PTHREADS/OS_VXWORKS/OS_VX2PTH_WRAP/OS_TMO/OS_CPSS_MAIN_OS/OS_QNX
ifeq (${TARGET_OS},)
   TARGET_OS                    = OS_LINUX_PTHREADS
endif

# TARGET_ASIC can be BCMX/PETRA/SWITCHCORE/DX260/DX285/DX5128/DX167/XCAT/XCAT3/MRVLLS/LION/LION_DB/NPSIM/FULCRUM/NONE/MS/EZCHIP/WINTEGRA/ALTERA/QCA/WASP/LNXWIRELESS/QORIQ/VTSS_SERVAL_1/VTSS_SERVAL_2/INTEL_ONS/QCAX/XP_WM/XP_HW
ifeq (${TARGET_ASIC},)
   TARGET_ASIC                  = ENET_ADAPTOR
endif

# TARGET_TYPE can be PIZZABOX/CHASSIS/LM_AS_PIZZABOX
ifeq (${TARGET_TYPE},)
   TARGET_TYPE                  = PIZZABOX
endif

# TARGET_CPU  can be MIPS/PPC_82XX/PPC_85XX/PPC_8XX/ARM/PPC_405/PPC_4XX/x86
ifeq (${TARGET_CPU},)
   TARGET_CPU                   = PPC_82XX
endif

# TARGET_CPU_BIT can be 32BIT/64BIT
ifeq (${TARGET_CPU_BIT},)
   TARGET_CPU_BIT               = 64BIT
endif

# TARGET_IP can be LINUXIP/FSIP
ifeq (${TARGET_IP},)
   TARGET_IP                    = LINUXIP
endif

# COMPILER_TYPE can be DIAB
ifeq (${COMPILER_TYPE},)
   COMPILER_TYPE                    =GCC
endif

# Environment / Package specific options
include ${BASE_DIR}/LR/make.env
