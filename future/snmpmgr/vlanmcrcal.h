#define nmhGetFsDot1qVlanStaticRowStatus(i1,i2,v) CliSnmpMngrCommonOp(1,(unsigned char *)"1.3.6.1.4.1.2076.116.7.1.4.4.1.2",(tSnmpIndex *)ConvertToMultiIndex(2,2,i1,66,i2,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)

#define nmhSetFsDot1qVlanStaticRowStatus(i1,i2,v) CliSnmpMngrCommonOp(2,(unsigned char *)"1.3.6.1.4.1.2076.116.7.1.4.4.1.2",(tSnmpIndex *)ConvertToMultiIndex(2,2,i1,66,i2,0),(tRetVal *)ConvertToSetMultiData(2,v),0, 0)

#define nmhSetFsDot1qVlanStaticPort(i1,i2,i3,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.2076.116.7.1.4.5.1.1",(tSnmpIndex *)ConvertToMultiIndex(3,2,i1,2,i2,2,i3,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)

#define nmhGetDot1qVlanStaticUntaggedPorts(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.2.1.17.7.1.4.3.1.4", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(4), 4, v)


#define nmhSetDot1qPvid(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.2.1.17.7.1.4.5.1.1",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)

#define nmhGetDot1qPvid(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.2.1.17.7.1.4.5.1.1", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66), 66, v)
