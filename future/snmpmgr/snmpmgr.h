
#define MAX_PORT_COUNT         48 

/* VLAN*/
char issnmhSetDot1qVlanStaticRowStatus ( long i1, unsigned long i2, long v);
char issnmhSetFsDot1qVlanStaticPort (long i1, unsigned long i2, long i3, long v);
char issnmhGetDot1qVlanStaticUntaggedPorts(unsigned long i1, tSNMP_OCTET_STRING_TYPE *v);
char issnmhSetDot1qPvid(unsigned long i1, unsigned long v);
char issnmhGetDot1qPvid(unsigned long i1, unsigned long *v);

/* QOS */
char issnmhSetFsQoSQStatus (unsigned long i1, long i2, unsigned long v);
char issnmhGetFsQoSQStatus (unsigned long i1, long i2, unsigned long *v);
char issnmhSetFsQoSQShapeId (unsigned long i1, long i2, unsigned long v);
char issnmhSetFsQoSShapeTemplateCBS(unsigned long i1, unsigned long v);
char issnmhSetFsQoSShapeTemplateCIR(unsigned long i1, unsigned long v);
char issnmhSetFsQoSShapeTemplateEIR(unsigned long i1, unsigned long v);
char issnmhSetFsQoSShapeTemplateStatus(unsigned long i1, unsigned long v);
char issnmhGetFsQoSShapeTemplateCIR(unsigned long i1, unsigned long *v);

char issnmhSetFsQoSMeterStatus (unsigned long i1, long v);
char issnmhGetFsQoSMeterStatus (unsigned long i1, long *v);
char issnmhSetFsQoSMeterType (unsigned long i1, long v);
char issnmhSetFsQoSMeterCIR (unsigned long i1, unsigned long v);
char issnmhSetFsQoSMeterCBS (unsigned long i1, unsigned long v);
char issnmhSetFsQoSMeterEIR (unsigned long i1, unsigned long v);
char issnmhSetFsQoSMeterEBS (unsigned long i1, unsigned long v);
char issnmhSetFsQoSMeterColorMode (unsigned long i1, long v);

char issnmhSetIssAclL2FilterStatus (long i1, long v);
char issnmhGetIssAclL2FilterStatus (long i1, long *v);
char issnmhSetIssAclL2FilterPriority (long i1, long v);
char issnmhSetIssExtL2FilterVlanPriority (long i1, long v);
char issnmhSetIssAclL2FilterVlanId (long i1, long v);
char issnmhSetIssAclL2FilterDirection (long i1, long v);
char issnmhSetIssAclL2FilterInPortList (long i1, tSNMP_OCTET_STRING_TYPE  *v);
char issnmhGetIssAclL2FilterInPortList (long i1, tSNMP_OCTET_STRING_TYPE  *v);

char issnmhSetFsQoSClassMapStatus (unsigned long i1, long v);
char issnmhGetFsQoSClassMapStatus (unsigned long i1, long *v);
char issnmhSetFsQoSClassMapL2FilterId (unsigned long i1, unsigned long v);
char issnmhSetFsQoSClassMapCLASS (unsigned long i1, unsigned long v);

char issnmhSetFsQoSPolicyMapStatus (unsigned long i1, long v);
char issnmhGetFsQoSPolicyMapStatus (unsigned long i1, long *v);
char issnmhSetFsQoSPolicyMapMeterTableId (unsigned long i1, unsigned long v);
char issnmhSetFsQoSPolicyMapOutProfileActionFlag (unsigned long i1, tSNMP_OCTET_STRING_TYPE  *v);
char issnmhSetFsQoSPolicyMapCLASS (unsigned long i1, unsigned long v);

/* LA */
char issnmhSetFsLaStatus(long);
char issnmhSetIfMainRowStatus (long ,long );
char issnmhSetIfAlias (long ,long);
char issnmhSetIfMainType (long ,long );
char issnmhSetIfMainAdminStatus (long ,long );
char issnmhSetDot3adAggPortActorAdminKey (long , long );
char issnmhSetDot3adAggPortActorActivity (long, long);
char issnmhSetDot3adAggPortActorTimeout (long, long);
char issnmhSetDot3adAggPortActorAdminState (long, long);
char issnmhSetFsLaPortMode(long , long );
char issnmhSetIfAdminStatus(long , long );
