/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved
 *   
 * $Id: smgrutil.h,v 1.1 2017/08/24 12:03:41 siva Exp $
 * 
 * Description: This file contains typedef, structure and function 
 *              prototype for snmp manager utilities.
 ********************************************************************/
#ifndef _SNMMGR_UTILS_H
#define _SNMMGR_UTILS_H

#include "smgrcmn.h"

/* List of MACRO used in snmp manager for utilities */ 
#define NO_INDEX  2
#define NO_INDEX_LEN_MATCH 3 
#define MAX_STR_OCTET_VAL         256 

/* Errors */
#define SNMP_ERR_NO_CREATION                                   11

/* Max allowd Integer */
#define SNMP_MAX_INT 0x7fffffff

#define IS_SCALAR(x) ((x != NULL) && (x->u4NoIndices == 0))
#define IS_TABLE(x) ((x != NULL) && (x->u4NoIndices > 0))

/* List of function prortotype used in snmp manager for utilities */
tSNMP_OID_TYPE     *
SNMP_MGR_GetOidFromString (INT1 *pi1_str);
tSnmpIndex * ConvertToMultiIndex(UINT4 u4IndexNo,...);
tRetVal * ConvertToSetMultiData(INT4 i4Type,...);
tRetVal * ConvertToGetMultiData(INT4 i4Type);
VOID ConvertStringToDottedString (UINT1 * pu1ConvertedString, UINT1 * pu1ToBeConvertString);
tRetVal * ConvertToGetNextMultiData(VOID);
tSNMP_VAR_BIND *SnmpCreateVarBind(tSNMP_OID_TYPE * pOid,tRetVal *pMultiData);
VOID SnmpFormOidWithIndex(tSnmpIndex *pMultiIndex,UINT1 *pu1TempOid);
INT4 SNMPMGRGetIndices (tSNMP_OID_TYPE OID, UINT4 u4Len,
                                    const tMbDbEntry * pDbEntry,
                                    tSnmpIndex ** pIndex);
INT4 SnmpMgrConvertStringToOctet (tSNMP_OCTET_STRING_TYPE * pOctetStrValue,
                           UINT1 *pu1Data);
tSNMP_VAR_BIND * SNMP_MGR_FormVarBind (tSNMP_OID_TYPE * pOid, INT2 i2Type, UINT4 u4Value,
                      INT4 i4Value, tSNMP_OCTET_STRING_TYPE * pOctet,
                      tSNMP_OID_TYPE * pOidType, tSNMP_COUNTER64_TYPE u8Value);
INT1
GetMgrMbDbEntryForName (CHR1 *, tSnmpMgrDbEntry *);
INT1
GetMgrMbDbEntryForOid (CHR1 *, tSnmpMgrDbEntry *);
INT1
SnmpMgrConvertToMultidata (UINT1 , UINT1 *, tRetVal *);
INT1
SnmpMgrSetValueForName (UINT4 *, UINT4, CHR1 *, CHR1 *, tSnmpMgrDbEntry *, UINT1 *);
INT1
SnmpMgrSetValueForOid (UINT4 *, CHR1 *, UINT4, CHR1 *, tSnmpMgrDbEntry *, UINT1 *);
INT1
SnmpMgrSendPacketForSet(UINT4 *, UINT1 *, INT4);
INT1
SnmpMgrEncodePacket(tSNMP_VAR_BIND *, UINT1 **, INT4 *, CHR1 *, INT2);
INT1
SnmpMgrGetValueForName (UINT4 *, CHR1 *, UINT4, CHR1 *, tSnmpMgrDbEntry *, UINT1 *,
                                                        UINT1 *, UINT4);
INT1
SnmpMgrSendPacketForGet(UINT4 *, UINT1 *, INT4 , INT4 , UINT1 *,UINT1 *, UINT4);
VOID
SnmpMgrConvertDataToString (tSNMP_MULTI_DATA_TYPE *, UINT1 *, INT4);
INT4
SnmpMgrSockInit (INT4 *pi4Socket);
INT4
SnmpMgrMemInit(VOID);
INT1
SnmpMgrGetValueForOid (UINT4 *,CHR1 *, UINT4, CHR1 *, tSnmpMgrDbEntry *, UINT1 *, UINT1 *, UINT4);
#endif /* _SNMMGR_UTILS_H */
