/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved
 *
 * $Id: smgrcons.h,v 1.1 2017/08/24 12:03:41 siva Exp $
 *
 * Description: This file contains constants required for the module.
 *******************************************************************/
#ifndef _SMGR_CONS_H_
#define _SMGR_CONS_H_

#include "fssnmp.h"

/* Snmp version Macro */
#define VERSION1 0
#define VERSION2 1
#define VERSION3 3

/* SNMP Operation */
#define SNMP_GET               1
#define SNMP_SET               2
#define SNMP_TEST              3
#define SNMP_GET_FIRST         4
#define SNMP_GET_NEXT          5

/* Timeout Macro */
#define SNMP_MGR_TIMEOUT       5  

#define SNMP_MGR_PORT          162

#define SNMP_MGR_PUBLIC_COMMUNITY_LEN 6
#define SNMP_MGR_MAX_IP_ADD_LEN  16
#define SNMP3_BYTE_LEN         8
#define SNMP3_MAX_NAME_LEN     1024
#define MAX_ARRAY_SIZE 1024
#define MAX_VARBIND               50
#define SNMP_MINUS_ONE   -1
#define SNMP_ZERO   0
#define SNMP_ONE    1
#define SNMP_TWO    2
#define SNMP_THREE  3
#define SNMP_FOUR   4
#define SNMP_EIGHT  8
#define SNMP_OK     0
#define SNMP_NOT_OK -1
#define MAX_OID_LENGTH            SNMP_MAX_OID_LENGTH 
#define SNMP_ONELONG 1L
/* sizing parameters */
#define MAX_PKT_LENGTH            1472
#define SNMP_GET_LAST_BYTE(x) (UINT1)(0xff & x);

#define SNMP_PDU_TYPE_GET_REQUEST            0xA0
#define SNMP_PDU_TYPE_GET_NEXT_REQUEST       0xA1
#define SNMP_PDU_TYPE_GET_RESPONSE           0xA2
#define SNMP_PDU_TYPE_SET_REQUEST            0xA3
#define SNMP_PDU_TYPE_TRAP                   0xA4
#define SNMP_PDU_TYPE_GET_BULK_REQUEST       0xA5
#define SNMP_PDU_TYPE_V2_INFORM_REQUEST      0xA6
#define SNMP_PDU_TYPE_SNMPV2_TRAP            0xA7
#define SNMP_PDU_TYPE_GET_REPORT             0xA8
#define SNMP_PDU_TYPE_V2_INFORM_RESPONSE     0xA2

#define SNMP_EXCEPTION_NO_SUCH_OBJECT       0x80
#define SNMP_EXCEPTION_NO_SUCH_INSTANCE     0x81
#define SNMP_EXCEPTION_END_OF_MIB_VIEW      0x82
#define SNMP_EXCEPTION_NO_DEF_VAL         0x83

#define SNMP_IPADDR_LEN   4
#define SNMP_UNSIGNED    0
#define SNMP_SIGNED      1

#define SNMPMGR_MAX_ARGS 11

#define MAX_SNMP_PKT_LEN 1024

/* List of MACRO used in snmp manager for utilities */ 
#define NO_INDEX  2
#define NO_INDEX_LEN_MATCH 3 
#define MAX_STR_OCTET_VAL         256 

/* Errors */
#define SNMP_ERR_NO_CREATION                                   11

/* Max allowd Integer */
#define SNMP_MAX_INT 0x7fffffff

#define IS_SCALAR(x) ((x != NULL) && (x->u4NoIndices == 0))
#define IS_TABLE(x) ((x != NULL) && (x->u4NoIndices > 0))

#endif /* _SMGR_CONS_H_ */
