/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved
 *
 * $Id: smgrcli.h,v 1.1 2017/08/24 12:03:40 siva Exp $
 *
 * Description: This file contains macros to the CLI commands and 
 * structures for the SNMPMGR module.
 *******************************************************************/
#ifndef _SNMPMGR_CLI_H
#define _SNMPMGR_CLI_H

#include <smgrcmn.h>
#include <cli.h>
#include <snmp3cli.h>

/* Function Prototype */
INT4 cli_process_snmpmgr_cmd (tCliHandle , UINT4 , ...);
INT4 SnmpMgrCliSetValueForName(tCliHandle , UINT4 *, UINT4, CHR1 *, CHR1 *, UINT1 *);
INT4 SnmpMgrCliSetValueForOid(tCliHandle , UINT4 *, UINT4, CHR1 *, CHR1 *, UINT1 *);
INT4 SnmpMgrCliGetValueForOid (tCliHandle , UINT4 *, UINT4, CHR1 *, CHR1 *, UINT4 );
INT4 SnmpMgrCliGetValueForName (tCliHandle , UINT4 *, UINT4, CHR1 *, CHR1 *, UINT4 );
INT4 SnmpMgrCliNameWalk (tCliHandle, UINT4 *, UINT4, CHR1 *, CHR1 * );
INT4 SnmpMgrCliOidWalk (tCliHandle, UINT4 *, UINT4, CHR1 *, CHR1 *);
#endif /* _SNMPMGRCLI_H */
