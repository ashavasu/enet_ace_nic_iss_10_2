/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved
 *   
 * $Id: smgrcmn.h,v 1.1 2017/08/24 12:03:40 siva Exp $
 * 
 * Description: This file contains common typedef, structure and 
 *              function prototype for snmp manager.
 ********************************************************************/
#ifndef _SNMMNGR_CMN_H
#define _SNMMNGR_CMN_H

#include "fssnmp.h"

/* Snmp version Macro */
#define VERSION1 0
#define VERSION2 1
#define VERSION3 3

/* SNMP Operation */
#define SNMP_GET               1
#define SNMP_SET               2
#define SNMP_TEST              3
#define SNMP_GET_FIRST         4
#define SNMP_GET_NEXT          5

#define SNMP3_BYTE_LEN         8
#define                   SNMP3_MAX_NAME_LEN     1024
#define MAX_ARRAY_SIZE 1024
#define MAX_VARBIND               50
#define SNMP_MINUS_ONE   -1
#define SNMP_ZERO   0
#define SNMP_ONE    1
#define SNMP_TWO    2
#define SNMP_THREE  3
#define SNMP_FOUR   4
#define SNMP_EIGHT  8
#define SNMP_OK     0
#define SNMP_NOT_OK -1
#define MAX_OID_LENGTH            SNMP_MAX_OID_LENGTH 
#define SNMP_ONELONG 1L
/* sizing parameters */
#define MAX_PKT_LENGTH            1472
#define SNMP_GET_LAST_BYTE(x) (UINT1)(0xff & x);

#define SNMP_PDU_TYPE_GET_REQUEST            0xA0
#define SNMP_PDU_TYPE_GET_NEXT_REQUEST       0xA1
#define SNMP_PDU_TYPE_GET_RESPONSE           0xA2
#define SNMP_PDU_TYPE_SET_REQUEST            0xA3
#define SNMP_PDU_TYPE_TRAP                   0xA4
#define SNMP_PDU_TYPE_GET_BULK_REQUEST       0xA5
#define SNMP_PDU_TYPE_V2_INFORM_REQUEST      0xA6
#define SNMP_PDU_TYPE_SNMPV2_TRAP            0xA7
#define SNMP_PDU_TYPE_GET_REPORT             0xA8
#define SNMP_PDU_TYPE_V2_INFORM_RESPONSE     0xA2

#define SNMP_EXCEPTION_NO_SUCH_OBJECT       0x80
#define SNMP_EXCEPTION_NO_SUCH_INSTANCE     0x81
#define SNMP_EXCEPTION_END_OF_MIB_VIEW      0x82
#define SNMP_EXCEPTION_NO_DEF_VAL         0x83

#define SNMP_IPADDR_LEN   4
#define SNMP_UNSIGNED    0
#define SNMP_SIGNED      1

#define SNMPMGR_MAX_ARGS 11

#define MAX_SNMP_PKT_LEN 1024

typedef struct MgrDb {
    INT4     i4OidLength;
    CONST CHR1    *i1ObjName;
    CONST CHR1    *i1Oid;
    INT4     i4NoOfIndex;
    INT4     i4IndexType[30];
    INT4     i4ObjType;
} tSnmpMgrDbEntry;

typedef struct SNMP_TRAP_PDU{
        UINT4                   u4_Version;
        UINT4                   u4_TimeStamp;
        INT4                    i4_GenericTrap;
        INT4                    i4_SpecificTrap;
        tSNMP_OCTET_STRING_TYPE *pCommunityStr;
        tSNMP_OID_TYPE          *pEnterprise;
        tSNMP_OCTET_STRING_TYPE *pAgentAddr;
        tSNMP_VAR_BIND          *pVarBindList;
        tSNMP_VAR_BIND          *pVarBindEnd;
        INT2                    i2_PduType;
        INT2                    i2Padding;
}tSNMP_TRAP_PDU;

typedef struct SNMP_NORMAL_PDU{
        UINT4                   u4_Version;
        UINT4                   u4_RequestID;
        INT4                    i4_ErrorStatus;
        INT4                    i4_ErrorIndex;
        INT4                    i4_NonRepeaters;
        INT4                    i4_MaxRepetitions;
        tSNMP_OCTET_STRING_TYPE *pCommunityStr;
        tSNMP_VAR_BIND          *pVarBindList;
        tSNMP_VAR_BIND          *pVarBindEnd;
        INT2                    i2_PduType;
        INT2                    i2Padding;
}tSNMP_NORMAL_PDU;

typedef struct MemBlock {
 tSNMP_VAR_BIND VarBind;
 tSNMP_OID_TYPE Oid;
 tSNMP_OID_TYPE DataOid;
 tSNMP_OCTET_STRING_TYPE Octet;
 UINT4 OidList[MAX_OID_LENGTH];
 UINT1 DataList[MAX_OID_LENGTH*SNMP_FOUR];
 struct MemBlock *pNext;
}tMemBlock;

/* Extern declartions */
extern INT4
SnmpONGetNameFromOid (char *pNumber, char *pNameIdx);
extern INT4
SnmpONGetOidFromName (char *pName, char *pNumberIdx);
extern INT4
WebnmConvertStringToOctet (tSNMP_OCTET_STRING_TYPE * pOctetStrValue,
                           UINT1 *pu1Data);
/************************************************************/

#endif /* _SNMMNGR_CMN_H */
