/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved
 *   
 * $Id: smgrcode.h,v 1.1 2017/08/24 12:03:41 siva Exp $
 * 
 * Description: This file contains typedef, structure and function 
 *              prototype for snmp manager packet coding.
 ********************************************************************/
#ifndef _SNMMGR_CODE_H
#define _SNMMGR_CODE_H

#include "smgrcmn.h"

extern unsigned int gu4AgentAddr;

/*tMemBlock gaMemPoolMgr[MAX_VARBIND];
tMemBlock *pCurMgr = &(gaMemPoolMgr[0]);
*/

/*UINT1               gau1OutDatFirst[MAX_PKT_LENGTH];
UINT1               gau1RevDatFirst[MAX_PKT_LENGTH];
UINT1               gau1CommunityDataFirst[SNMP_MAX_OCTETSTRING_SIZE];
UINT1               gau1AgentAddressFirst[SNMP_MAX_OCTETSTRING_SIZE];
tSNMP_OCTET_STRING_TYPE CommunityFirst;
*/
#if defined(__SMGR_API_C__)
tSNMP_OID_TYPE                  gapOidThird[SNMP_MAX_INDICES_2];
tSNMP_OCTET_STRING_TYPE         gapOctateThird[SNMP_MAX_INDICES_2];
tSnmpIndex                      gapMultiIndexThird;
tSNMP_MULTI_DATA_TYPE           gapIndexThird[SNMP_MAX_INDICES_2];
#else
extern tSNMP_OID_TYPE                  gapOidThird[SNMP_MAX_INDICES_2];
extern tSNMP_OCTET_STRING_TYPE         gapOctateThird[SNMP_MAX_INDICES_2];
extern tSNMP_MULTI_DATA_TYPE           gapIndexThird[SNMP_MAX_INDICES_2];
#endif

/*tSNMP_TRAP_PDU  gV1TrapPduFirst;
tSNMP_OCTET_STRING_TYPE AgentAddressFirst;
tSNMP_OID_TYPE EnterpriseOidFirst;
UINT4 EnterpriseOidListMngr[MAX_OID_LENGTH];

tSNMP_NORMAL_PDU    gRxPduMngr;
*/

/* Function prototypes */
INT1               *
SNMPMGRDecodePacket (UINT1 *pu1PktPtr, INT4 i4PktLen, INT2 *pPduType);
INT4
SNMPMGREstimateVarbindLength (tSNMP_VAR_BIND * pVbPtr);
INT4
SNMPMGREstimateNormalPacketLength (tSNMP_NORMAL_PDU * pMsgPtr);
VOID
SNMPMGRFreeV1TrapMsg (tSNMP_TRAP_PDU * pPdu);
INT4
SNMPMGREncodeGetPacket (tSNMP_NORMAL_PDU * pMsgPtr, UINT1 **ppu1DataOut,
                                         INT4 *pi4PktLen, INT4 i4TypeOfMsg);
VOID
SNMPMGRFreeVarbindList (tSNMP_VAR_BIND * pVbPtr);
INT2 SNMPMGRAddVarbindList (tSNMP_NORMAL_PDU *msg_ptr,
                            tSNMP_VAR_BIND * vb_ptr);
VOID SNMPMGRFreeNormalMsg (tSNMP_NORMAL_PDU *msg_ptr);
tSNMP_OCTET_STRING_TYPE *
SNMPMGRFormOctetString (UINT1 *u1_string,INT4 i4_length,
                             tSNMP_OCTET_STRING_TYPE *octetstring);
INT4 SNMPMGREstimateOidLength (tSNMP_OID_TYPE *oid_ptr);
INT4 SNMPMGREstimateOctetstringLength (tSNMP_OCTET_STRING_TYPE *os_ptr);
INT4 SNMPMGRWriteGetMsg (UINT1 **ppu1_curr_ptr, 
                              tSNMP_NORMAL_PDU * msg_ptr, INT4 i4TypeOfMsg);
INT4 SNMPMGRWriteLength (UINT1 **ppu1_curr_ptr,INT4 i4_length);
INT4 SNMPMGRWriteOctetstring (UINT1 **ppu1_curr_ptr,
                              tSNMP_OCTET_STRING_TYPE * os_ptr, INT2 i2_type);
INT4 SNMPMGRWriteUnsignedint (UINT1 **ppu1_curr_ptr, UINT4 u4_value,
                              INT2 i2_type);
INT4 SNMPMGREstimateUnsignedintLength (UINT4 u4_value);
INT4 SNMPMGREstimateLengthLength (INT4 i4_length);
INT4 SNMPMGRFindLength (INT4 i4_len);
INT4 SNMPMGREstimateCounter64Length (tSNMP_COUNTER64_TYPE u8_value);
INT4 SNMPMGREstimateSignedintLength (INT4 i4_value);
VOID SNMPMGRReverseVarBindList (tSNMP_VAR_BIND **pVarBindList);
INT4 SNMPMGRWriteVarbind (UINT1 **ppu1_curr_ptr,tSNMP_VAR_BIND * vb_ptr);
INT4 SNMPMGRWriteCounter64 (UINT1 **ppu1_curr_ptr,
                            tSNMP_COUNTER64_TYPE u8_value, INT2 i2_type);
INT4 SNMPMGRWriteSignedint (UINT1 **pu1_curr, INT4 i4_value, INT2 i2_type);
INT4 SNMPMGRWriteOid (UINT1 **ppu1_curr_ptr, tSNMP_OID_TYPE * oid_ptr);
VOID SNMPMGRWriteNull (UINT1 **ppu1_curr_ptr);
VOID SNMPMGRWriteException (UINT1 **ppu1_curr_ptr, INT2 i2_DataType);
INT4 SNMPMGRDecodeSequence (UINT1 **ppu1_curr_ptr,UINT1 * pu1_end_ptr);
INT4 SNMPMGRDecodeTypeLen (UINT1 **ppu1_curr_ptr, const UINT1 *pu1_end_ptr, 
                          INT2 *i2_type);
INT4 SNMPMGRDecodeInteger (UINT1 **pu1_curr, UINT1 *pu1_end, INT2 *i2_type,
                           INT2 i2_unsign_or_sign);
tSNMP_OCTET_STRING_TYPE *
SNMPMGRDecodeOctetstring (UINT1 **ppu1_curr_ptr, UINT1 *pu1_end_ptr, 
                               INT2 *i2_type,tSNMP_OCTET_STRING_TYPE * os_ptr);
tSNMP_OID_TYPE * SNMPMGRDecodeOid (UINT1 **ppu1_curr_ptr, 
                       UINT1 *pu1_end_ptr, tSNMP_OID_TYPE *oid_ptr);
tSNMP_VAR_BIND *
SNMPMGRDecodeVarbind (UINT1 **ppu1_curr_ptr,UINT1 * pu1_end_ptr);
VOID SNMPMGRDecodeCounter64Value (UINT1 **pu1_curr, UINT1 *pu1_end, 
      INT2 *i2_type,tSNMP_COUNTER64_TYPE * u8_value);
INT2 SNMPMGRDecodeNull (UINT1 **ppu1_curr_ptr,UINT1 * pu1_end_ptr,
                        INT2 * pi2_type);
INT4 SNMPMGRReversePacket (UINT1 *pu1_in_ptr, UINT1 **ppu1_out_ptr,
                           INT4  i4_length);
INT1
SNMPMGRConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value);
INT1
SNMPMGRHexConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value);
VOID
SNMPMGRFreeVarBind (tSNMP_VAR_BIND * pPtr);
INT4
SNMPMGRVersionCheck (UINT1 *pu1Pkt, INT4 i4PktSize);
INT4
SNMPMGRConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data, UINT1 u1HexFlag);
VOID
SNMPMGRConvertcolonstrToOidString (UINT1 *pCStr, UINT1 *pDStr);

#if defined(__SMGR_API_C__) || defined(__SMGR_EXE__)
/* List of function prortotype used in snmp manager */
INT1 
SnmpSetRequest(UINT1 *u1Oid,tSnmpIndex *pMultiIndex,tRetVal *pMultiData);
INT1 
SnmpGetRequest(UINT1 *u1Oid,tSnmpIndex *pMultiIndex,
                                      tRetVal *pMultiData,INT4 i4Type, va_list args);
INT1
SnmpGetFirstIndex(UINT1 *u1Oid, INT4 i4IndexNo, va_list args);
INT1
SnmpGetNextIndex(UINT1 *u1ObjName, INT4 i4IndexNo, va_list args);
INT1
CliSnmpMngrCommonOp(INT4 i4Operation, unsigned char *u1Oidorname, tSnmpIndex *pMultiIndex,
                             tRetVal *pMultiData, INT4 i4Type, ...);
#endif

#endif /* _SNMMGR_CODE_H */
