/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved
 *
 * $Id: smgrinc.h,v 1.1 2017/08/24 12:03:41 siva Exp $
 *
 * Description: This file contains global variable and global structure.
 *******************************************************************/
#ifndef _SMGR_INC_H_
#define _SMGR_INC_H_

#include "smgrcons.h"
#include "smgrcode.h"
#include "fssocket.h"
#include "smgrutil.h"

#endif /* _SMGR_INC_H_ */
