/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved
 *
 * $Id: snmpmgr.c,v 1.1 2017/08/24 12:03:40 siva Exp $
 * 
 * Description: Routines for snmp manager
 ********************************************************************/

#include <stdlib.h>
#include <string.h>

#include "inc/smgrinc.h"
#include "vlanmcrcal.h"
#include "qosxmcrcal.h"
#include "lamcrcal.h"
#include "snmpmgr.h"

#ifndef TARGET_NONE

#define ISS_PORTLIST_LEN1 12
unsigned int  gu4AgentAddr = 0x7f000001;

INT1 CliSnmpMngrCommonOp(INT4 i4Operation, unsigned char *u1Oidorname_char, tSnmpIndex *pMultiIndex,
                             tRetVal *pMultiData, INT4 i4Type, ...)
{
  va_list ap;
  INT1 i1Result = SNMP_FAILURE;
  INT4 i4IndexNo = 0;
  UINT1 *u1Oidorname = (UINT1 *)u1Oidorname_char;
  gu4AgentAddr = 0x7f000001;

  /* Appropriate function will be
     called depending on
     * the operation to be
     performed */
  switch (i4Operation)
    {
    case SNMP_GET:
      va_start (ap, i4Type);
      i1Result
	= SnmpGetRequest (u1Oidorname, pMultiIndex, pMultiData, i4Type, ap);
      va_end (ap);
      break;
    case SNMP_SET:
      i1Result = SnmpSetRequest (u1Oidorname, pMultiIndex, pMultiData);
      break;
    case SNMP_TEST:
      i1Result = SNMP_SUCCESS;
      break;
    case SNMP_GET_FIRST:
      va_start (ap, i4Type);
      va_arg (ap, INT4);
      i4IndexNo = va_arg (ap, INT4);
      i1Result = SnmpGetFirstIndex (u1Oidorname, i4IndexNo, ap);
      va_end (ap);
      break;
    case SNMP_GET_NEXT:
      va_start (ap, i4Type);
      va_arg (ap, INT4);
      i4IndexNo = va_arg (ap, INT4);
      i1Result = SnmpGetNextIndex (u1Oidorname, i4IndexNo, ap);
      va_end (ap);
      break;
    default:
        printf("\nInvalid snmp operation\n");
        break;
    }
  return i1Result;
}


char issnmhGetDot1qVlanStaticUntaggedPorts(unsigned long i1, tSNMP_OCTET_STRING_TYPE  *v)
{
    char retval=0;	
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT1 au1PortList[ISS_PORTLIST_LEN1];
    
    memset (au1PortList, 0, ISS_PORTLIST_LEN1);

    PortList.i4_Length = ISS_PORTLIST_LEN1;
    PortList.pu1_OctetList = &au1PortList[0];
 
    retval=nmhGetDot1qVlanStaticUntaggedPorts(i1, &PortList);
    
    memcpy (v->pu1_OctetList, PortList.pu1_OctetList, ISS_PORTLIST_LEN1);
    v->i4_Length = PortList.i4_Length;
    return retval;
}

char issnmhSetDot1qPvid(unsigned long i1, unsigned long v)
{
    return nmhSetDot1qPvid(i1,v);
}

char issnmhSetDot1qVlanStaticRowStatus (long i1, unsigned long i2, long v)
{
    return nmhSetFsDot1qVlanStaticRowStatus(i1,i2,v);
}

char issnmhSetFsDot1qVlanStaticPort (long i1, unsigned long i2, long i3, long v)
{
    return nmhSetFsDot1qVlanStaticPort(i1,i2,i3,v);
}

char issnmhGetDot1qPvid(unsigned long i1, unsigned long *v)
{
    return nmhGetDot1qPvid(i1,v);
}

/*QOS*/
char issnmhSetFsQoSQStatus (unsigned long i1, long i2, unsigned long v)
{
    return nmhSetFsQoSQStatus(i1,i2,v);
}

char issnmhSetFsQoSQShapeId (unsigned long i1, long i2, unsigned long v)
{
    return nmhSetFsQoSQShapeId(i1,i2,v);
}

char issnmhSetFsQoSShapeTemplateStatus (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSShapeTemplateStatus(i1, v);
}

char issnmhSetFsQoSShapeTemplateCIR (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSShapeTemplateCIR(i1, v);
}

char issnmhSetFsQoSShapeTemplateEIR (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSShapeTemplateEIR(i1, v);
}

char issnmhSetFsQoSShapeTemplateCBS (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSShapeTemplateCBS(i1, v);
}

char issnmhGetFsQoSShapeTemplateCIR (unsigned long i1, unsigned long *v)
{
    return nmhGetFsQoSShapeTemplateCIR(i1, v);
}


char issnmhSetFsLaStatus (long v)
{
    return nmhSetFsLaStatus(v);
}

char issnmhSetIfMainRowStatus (long i1, long v)
{
    return nmhSetIfMainRowStatus(i1, v);
}

char issnmhSetIfAlias (long i1,  long v)
{
    tSNMP_OCTET_STRING_TYPE Alias;
  
    char str[10];
 
    sprintf(str,  "po%ld", v);
    Alias.pu1_OctetList = (UINT1 *)str;
    Alias.i4_Length = (INT4) (strlen ((const char *)Alias.pu1_OctetList));
    return nmhSetIfAlias(i1, &Alias);
}

char issnmhSetIfMainType (long i1, long v)
{
    return nmhSetIfMainType(i1, v);
}

char issnmhSetIfMainAdminStatus (long i1, long v)
{
    return nmhSetIfMainAdminStatus(i1, v);
}

char issnmhSetDot3adAggPortActorAdminKey (long i1, long v)
{
   return nmhSetDot3adAggPortActorAdminKey(i1, v);
}
 
char issnmhSetFsLaPortMode (long i1, long v)
{
    return nmhSetFsLaPortMode(i1, v);
}

char issnmhSetIfAdminStatus (long i1, long v)
{
    return nmhSetIfAdminStatus(i1, v);
}

char issnmhSetDot3adAggPortActorActivity (long i1, long v)
{
    tSNMP_OCTET_STRING_TYPE portAdminState;
    unsigned char u1SetActivity;
    u1SetActivity = 0x80;
    portAdminState.i4_Length = 1;
    portAdminState.pu1_OctetList = &u1SetActivity;
    if (v == 0)
    {
        return nmhSetDot3adAggPortActorAdminState (i1, &portAdminState);
    }
    else
    {
        return nmhSetFsLaPortActorResetAdminState (i1, &portAdminState);
    }
}

char issnmhSetDot3adAggPortActorTimeout (long i1, long v)
{
    tSNMP_OCTET_STRING_TYPE portAdminState;
    unsigned char u1SetActivity;
    u1SetActivity = 0x40;
    portAdminState.i4_Length = 1;
    portAdminState.pu1_OctetList = &u1SetActivity;
    if (v == 0)
    {
        return nmhSetDot3adAggPortActorAdminState (i1, &portAdminState);
    }
    else
    {
        return nmhSetFsLaPortActorResetAdminState (i1, &portAdminState);
    }
}

char issnmhSetFsQoSMeterCIR (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSMeterCIR(i1, v);
}

char issnmhSetFsQoSMeterCBS (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSMeterCBS(i1, v);
}

char issnmhSetFsQoSMeterEIR (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSMeterEIR(i1, v);
}

char issnmhSetFsQoSMeterEBS (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSMeterEBS(i1, v);
}

char issnmhSetFsQoSMeterStatus (unsigned long i1, long v)
{
    return nmhSetFsQoSMeterStatus(i1, v);
}

char issnmhSetFsQoSMeterType (unsigned long i1, long v)
{
    return nmhSetFsQoSMeterType(i1, v);
}

char issnmhSetFsQoSMeterColorMode (unsigned long i1, long v)
{
    return nmhSetFsQoSMeterColorMode(i1, v);
}

char issnmhSetIssAclL2FilterStatus (long i1, long v)
{
    return nmhSetIssAclL2FilterStatus(i1, v);
}

char issnmhGetIssAclL2FilterStatus (long i1, long *v)
{
    return nmhGetIssAclL2FilterStatus(i1, v);
}

char issnmhSetIssAclL2FilterPriority (long i1, long v)
{
    return nmhSetIssAclL2FilterPriority(i1, v);
}

char issnmhSetIssAclL2FilterVlanId (long i1, long v)
{
    return nmhSetIssAclL2FilterVlanId(i1, v);
}

char issnmhSetIssAclL2FilterDirection (long i1, long v)
{
    return nmhSetIssAclL2FilterDirection(i1, v);
}

char issnmhSetIssAclL2FilterInPortList (long i1, tSNMP_OCTET_STRING_TYPE  *v)
{
    return nmhSetIssAclL2FilterInPortList(i1, v);
}

char issnmhGetIssAclL2FilterInPortList (long i1, tSNMP_OCTET_STRING_TYPE  *v)
{
    return nmhGetIssAclL2FilterInPortList(i1, v);
}

char issnmhSetIssExtL2FilterVlanPriority (long i1, long v)
{
    return nmhSetIssExtL2FilterVlanPriority(i1, v);
}

char issnmhSetFsQoSPolicyMapStatus (unsigned long i1, long v)
{
    return nmhSetFsQoSPolicyMapStatus(i1, v);
}

char issnmhGetFsQoSPolicyMapStatus (unsigned long i1, long *v)
{
    return nmhGetFsQoSPolicyMapStatus(i1, v);
}

char issnmhSetFsQoSPolicyMapMeterTableId (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSPolicyMapMeterTableId(i1, v);
}

char issnmhSetFsQoSPolicyMapCLASS (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSPolicyMapCLASS(i1, v);
}

char issnmhSetFsQoSPolicyMapOutProfileActionFlag (unsigned long i1, tSNMP_OCTET_STRING_TYPE  *v)
{
    return nmhSetFsQoSPolicyMapOutProfileActionFlag(i1, v);
}

char issnmhSetFsQoSClassMapStatus (unsigned long i1, long v)
{
    return nmhSetFsQoSClassMapStatus(i1, v);
}

char issnmhGetFsQoSClassMapStatus (unsigned long i1, long *v)
{
    return nmhGetFsQoSClassMapStatus(i1, v);
}

char issnmhSetFsQoSClassMapL2FilterId (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSClassMapL2FilterId(i1, v);
}

char issnmhSetFsQoSClassMapCLASS (unsigned long i1, unsigned long v)
{
    return nmhSetFsQoSClassMapCLASS(i1, v);
}


#else  /*TARGET_NONE*/
int mainsnmp () { return SNMP_SUCCESS; }
long issSnmpMgrMemInit() { return SNMP_SUCCESS; }

/* VLAN */
char issnmhSetDot1qVlanStaticRowStatus ( long i1, unsigned long i2, long v) { return SNMP_SUCCESS; }
char issnmhSetFsDot1qVlanStaticPort (long i1, unsigned long i2, long i3, long v) { return SNMP_SUCCESS; }
char issnmhGetDot1qVlanStaticUntaggedPorts(unsigned long i1, tSNMP_OCTET_STRING_TYPE  *v) { return SNMP_SUCCESS; }
char issnmhSetDot1qPvid(unsigned long i1, unsigned long v)  { return SNMP_SUCCESS; }
char issnmhGetDot1qPvid(unsigned long i1, unsigned long *v) { return SNMP_SUCCESS; }


/* QOS */
char issnmhSetFsQoSQStatus (unsigned long i1, long i2, unsigned long v) { return SNMP_SUCCESS; }
char issnmhGetFsQoSQStatus (unsigned long i1, long i2, unsigned long *v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSQShapeId (unsigned long i1, long i2, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSShapeTemplateCBS(unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSShapeTemplateCIR(unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSShapeTemplateEIR(unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSShapeTemplateStatus(unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhGetFsQoSShapeTemplateCIR(unsigned long i1, unsigned long *v) { return SNMP_SUCCESS; }

char issnmhSetFsQoSMeterStatus (unsigned long i1, long v) { return SNMP_SUCCESS; }
char issnmhGetFsQoSMeterStatus (unsigned long i1, long *v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSMeterType (unsigned long i1, long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSMeterCIR (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSMeterCBS (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSMeterEIR (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSMeterEBS (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSMeterColorMode (unsigned long i1, long v) { return SNMP_SUCCESS; }


char issnmhSetIssAclL2FilterStatus (long i1, long v) { return SNMP_SUCCESS; }
char issnmhGetIssAclL2FilterStatus (long i1, long *v) { return SNMP_SUCCESS; }
char issnmhSetIssAclL2FilterPriority (long i1, long v) { return SNMP_SUCCESS; }
char issnmhSetIssExtL2FilterVlanPriority (long i1, long v) { return SNMP_SUCCESS; }
char issnmhSetIssAclL2FilterVlanId (long i1, long v) { return SNMP_SUCCESS; }
char issnmhSetIssAclL2FilterDirection (long i1, long v) { return SNMP_SUCCESS; }
char issnmhSetIssAclL2FilterInPortList (long i1, tSNMP_OCTET_STRING_TYPE  *v) { return SNMP_SUCCESS; }
char issnmhGetIssAclL2FilterInPortList (long i1, tSNMP_OCTET_STRING_TYPE  *v) { return SNMP_SUCCESS; }


char issnmhSetFsQoSClassMapStatus (unsigned long i1, long v) { return SNMP_SUCCESS; }
char issnmhGetFsQoSClassMapStatus (unsigned long i1, long *v);
char issnmhSetFsQoSClassMapL2FilterId (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSClassMapCLASS (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }

char issnmhSetFsQoSPolicyMapStatus (unsigned long i1, long v) { return SNMP_SUCCESS; }
char issnmhGetFsQoSPolicyMapStatus (unsigned long i1, long *v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSPolicyMapMeterTableId (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; } 
char issnmhSetFsQoSPolicyMapOutProfileActionFlag (unsigned long i1, tSNMP_OCTET_STRING_TYPE  *v) { return SNMP_SUCCESS; }
char issnmhSetFsQoSPolicyMapCLASS (unsigned long i1, unsigned long v) { return SNMP_SUCCESS; }


/* LA */
char issnmhSetFsLaStatus(long) { return SNMP_SUCCESS; }
char issnmhSetIfMainRowStatus (long ,long ) { return SNMP_SUCCESS; }
char issnmhSetIfAlias (long ,long) { return SNMP_SUCCESS; }
char issnmhSetIfMainType (long ,long ) { return SNMP_SUCCESS; }
char issnmhSetIfMainAdminStatus (long ,long ) { return SNMP_SUCCESS; }
char issnmhSetDot3adAggPortActorAdminKey (long , long ) { return SNMP_SUCCESS; }
char issnmhSetDot3adAggPortActorActivity (long, long) { return SNMP_SUCCESS; }
char issnmhSetDot3adAggPortActorTimeout (long, long) { return SNMP_SUCCESS; }
char issnmhSetDot3adAggPortActorAdminState (long, long) { return SNMP_SUCCESS; }
char issnmhGetDot3adAggPortActorAdminState (long,  tSNMP_OCTET_STRING_TYPE *) { return SNMP_SUCCESS; }
char issnmhSetFsLaPortMode(long , long ) { return SNMP_SUCCESS; }
char issnmhSetIfAdminStatus(long , long ) { return SNMP_SUCCESS; }
#endif /*TARGET_NONE*/
