/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved 
 *
 * $Id: smgrcode.c,v 1.1 2017/08/24 12:03:42 siva Exp $
 *
 * Description: Contains utility routines for packet coding
 *******************************************************************/

#ifndef __SNMMGR_CODE_C__
#define __SNMMGR_CODE_C__

#include "smgrcode.h"

/* Global allocation for varbind */
tSNMP_VAR_BIND          gVarBindCode;
tSNMP_OID_TYPE          gObjNameCode;
UINT4                   gu4_OidListCode[MAX_ARRAY_SIZE];
UINT1                   gau1_OctetCode[MAX_ARRAY_SIZE];
UINT4                   gau4_oidListCode[MAX_ARRAY_SIZE];
tSNMP_OCTET_STRING_TYPE gtOctateCode;
tSNMP_OID_TYPE          gtOidCode;

/***************************/

/*********************************************************************
*  Function Name :SNMPMGRAddVarbindList
*  Description   :This procedure makes a varbind list.It i
*                 connects the incoming pVbPtr to the end of the list.
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT2
SNMPMGRAddVarbindList (tSNMP_NORMAL_PDU * pMsgPtr, tSNMP_VAR_BIND * pVbPtr)
{
    if (pVbPtr == NULL)
    {
        return (SNMP_NOT_OK);
    }
    if ((pMsgPtr->pVarBindList == NULL) || (pMsgPtr->pVarBindEnd == NULL))
    {
        pMsgPtr->pVarBindList = pVbPtr;
        pMsgPtr->pVarBindEnd = pVbPtr;
        return (SNMP_OK);
    }
    /* Add varbind to the beginning */
    pMsgPtr->pVarBindEnd->pNextVarBind = pVbPtr;
    pMsgPtr->pVarBindEnd = pVbPtr;
    pVbPtr->pNextVarBind = NULL;
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRFreeNormalMsg
*  Description   :This procedure Frees the data structure of 
*                 type tSNMP_NORMAL_PDU allocated.
*
*  Parameter(s)  : 
*
*  Return Values : VOID
*********************************************************************/

VOID
SNMPMGRFreeNormalMsg (tSNMP_NORMAL_PDU * pMsgPtr)
{
    if (pMsgPtr != NULL)
    {
        SNMPMGRFreeVarbindList (pMsgPtr->pVarBindList);
    }
}

/*********************************************************************
*  Function Name :SNMPMGRFreeV1TrapMsg
*  Description   :This procedure Frees the data structure of 
*                 type tSNMP_TRAP_PDU allocated.
*
*  Parameter(s)  : 
*
*  Return Values : VOID
*********************************************************************/
VOID
SNMPMGRFreeV1TrapMsg (tSNMP_TRAP_PDU * pPdu)
{
    if (pPdu != NULL)
    {
        SNMPMGRFreeVarbindList (pPdu->pVarBindList);
    }
}

/*********************************************************************
*  Function Name nSNMPMGRFormOctetString 
*  Description   :This procedure creates the internal datastructure of type 
*            OctetString and puts the values in to it.
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/
tSNMP_OCTET_STRING_TYPE *
SNMPMGRFormOctetString (UINT1 *pu1String, INT4 i4Length,
                     tSNMP_OCTET_STRING_TYPE * pOctetstring)
{
    if (i4Length > SNMP_MAX_OCTETSTRING_SIZE)
    {
        i4Length = SNMP_MAX_OCTETSTRING_SIZE;
    }
    if (pOctetstring == NULL)
    {
        return (NULL);
    }
    pOctetstring->i4_Length = i4Length;
    if (i4Length != SNMP_ZERO)
    {
        MEMCPY ((VOID *) pOctetstring->pu1_OctetList,
                (VOID *) pu1String, i4Length);
    }
    return (pOctetstring);
}

/*********************************************************************
*  Function Name :SNMPMGRFreeVarbindList
*  Description   :This procedure Frees the data structure of type  tSNMP_VAR_BIND
*                       allocated
*
*
*  Parameter(s)  :
*
*  Return Values : VOID
*********************************************************************/
VOID
SNMPMGRFreeVarbindList (tSNMP_VAR_BIND * pVbPtr)
{
    tSNMP_VAR_BIND     *pCur = pVbPtr, *pNext = NULL;
    while (pCur != NULL)
    {
        pNext = pCur->pNextVarBind;
        SNMPMGRFreeVarBind (pCur);
        pCur = pNext;
    }
}

/*********************************************************************
*  Function Name :SNMPMGREncodeGetPacket
*  Description   : This procedure encodes the GET-Response Message in to BER 
*           encoded format.
*           It writes the opcodes at relevant places to distinguish the type 
*           of objects.As the SNMP Message does not have fixed length format
*           so it allows for encoding variable size data and variable number
*           of objects.The lengths of the values precede the value in the
*           data packet. The encoding is done from the last object and put 
*           in the array    backwards.Later on the entire array is reversed.
*
*  Parameter(s)  :
*
*  Return Values :returns pdu ptr
*********************************************************************/

INT4
SNMPMGREncodeGetPacket (tSNMP_NORMAL_PDU * pMsgPtr, UINT1 **ppu1DataOut,
                     INT4 *pi4PktLen, INT4 i4TypeOfMsg)
{
    INT4                i4MsgLen = 0;
    INT4                i4TotLen = 0;
    UINT1              *pu1DataPtr = NULL;
    UINT1              *pu1StartPtr = NULL;
    INT4                i4EstLen = 0;
    UINT1               gau1OutDatFirst[MAX_PKT_LENGTH];

    if (pMsgPtr == NULL)
    {
        return (SNMP_NOT_OK);
    }
    i4EstLen = SNMPMGREstimateNormalPacketLength (pMsgPtr);

    if (i4EstLen > MAX_PKT_LENGTH)
    {
        *pi4PktLen = i4EstLen;
        /* Return a too big Error status,
         * when asked for a larger sized response that exceeds the 
         * maximum allowed packet length.
         */
        pMsgPtr->i4_ErrorStatus = SNMP_ERR_TOO_BIG;
        return SNMP_NOT_OK;
    }
    else
        pu1DataPtr = gau1OutDatFirst;

    pu1StartPtr = pu1DataPtr;
    if (SNMPMGRWriteGetMsg (&pu1DataPtr, pMsgPtr, i4TypeOfMsg) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    i4MsgLen = pu1DataPtr - pu1StartPtr;
    if (SNMPMGRWriteLength (&pu1DataPtr, i4MsgLen) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }

    *pu1DataPtr = SNMP_GET_LAST_BYTE (i4TypeOfMsg);
    /*  *pu1DataPtr = (UINT1) (0xff & i4TypeOfMsg); */
    pu1DataPtr++;
    if (SNMPMGRWriteOctetstring (&pu1DataPtr, pMsgPtr->pCommunityStr,
                              SNMP_DATA_TYPE_OCTET_PRIM) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (SNMPMGRWriteUnsignedint (&pu1DataPtr, pMsgPtr->u4_Version,
                              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    i4TotLen = pu1DataPtr - pu1StartPtr;
    if (SNMPMGRWriteLength (&pu1DataPtr, i4TotLen) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    *pu1DataPtr = SNMP_DATA_TYPE_SEQUENCE;
    pu1DataPtr++;
    *pi4PktLen = pu1DataPtr - pu1StartPtr;
    SNMPMGRReversePacket (pu1StartPtr, ppu1DataOut, *pi4PktLen);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGREstimateNormalPacketLength
*  Description   :This function estimates the length of tSNMP_NORMAL_PDU Message
*
*
*  Parameter(s)  :pdu ptr
*
*  Return Values :length of the pdu
*********************************************************************/

INT4
SNMPMGREstimateNormalPacketLength (tSNMP_NORMAL_PDU * pMsgPtr)
{
    INT4                i4VbLen = 0, i4MsgLen = 0;
    INT4                i4Counter = 0;
    i4VbLen = SNMPMGREstimateVarbindLength (pMsgPtr->pVarBindList);
    i4Counter = i4VbLen + SNMPMGREstimateLengthLength (i4VbLen);
    i4Counter++;                /* SNMP_DATA_TYPE_SEQUENCE  */
    i4Counter += SNMPMGREstimateSignedintLength (pMsgPtr->i4_ErrorIndex);
    i4Counter += SNMPMGREstimateSignedintLength (pMsgPtr->i4_ErrorStatus);
    i4Counter =
        (INT4) (i4Counter +
                SNMPMGREstimateSignedintLength ((INT4) pMsgPtr->u4_RequestID));
    i4MsgLen = i4Counter;
    i4Counter += SNMPMGREstimateLengthLength (i4MsgLen);
    i4Counter++;                /* (0xff & type_of_message)  */
    if (pMsgPtr->pCommunityStr != NULL)
    {
        i4Counter += SNMPMGREstimateOctetstringLength (pMsgPtr->pCommunityStr);
    }
    i4Counter += SNMPMGREstimateUnsignedintLength (pMsgPtr->u4_Version);
    i4Counter += SNMPMGREstimateLengthLength (i4Counter);
    i4Counter++;                /* SNMP_DATA_TYPE_SEQUENCE  */
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPMGREstimateVarbindLength
*  Description   :This procedure estimates the length of the tSNMP_VAR_BIND 
*
*
*  Parameter(s)  : variable ptr
*
*  Return Values : length
*********************************************************************/

INT4
SNMPMGREstimateVarbindLength (tSNMP_VAR_BIND * pVbPtr)
{
    INT4                i4VbLen = 0;
    INT4                i4Counter = 0;

    i4Counter = 0;
    while (pVbPtr != NULL)
    {
        i4VbLen = i4Counter;
        switch (pVbPtr->ObjValue.i2_DataType)
        {
            case SNMP_DATA_TYPE_COUNTER32:
            case SNMP_DATA_TYPE_GAUGE32:
                /* this is moved here since TimeTicks is an unsigned value */
            case SNMP_DATA_TYPE_TIME_TICKS:
                i4Counter += SNMPMGREstimateUnsignedintLength
                    (pVbPtr->ObjValue.u4_ULongValue);
                break;

            case SNMP_DATA_TYPE_COUNTER64:
                i4Counter += SNMPMGREstimateCounter64Length
                    (pVbPtr->ObjValue.u8_Counter64Value);
                break;
            case SNMP_EXCEPTION_NO_SUCH_OBJECT:
            case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
            case SNMP_EXCEPTION_END_OF_MIB_VIEW:
                i4Counter += SNMP_TWO;
                break;

            case SNMP_DATA_TYPE_INTEGER32:
                i4Counter += SNMPMGREstimateSignedintLength
                    (pVbPtr->ObjValue.i4_SLongValue);
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                i4Counter += SNMPMGREstimateOidLength (pVbPtr->ObjValue.pOidValue);
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
                i4Counter += SNMPMGREstimateOctetstringLength
                    (pVbPtr->ObjValue.pOctetStrValue);
                break;

            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                pVbPtr->ObjValue.pOctetStrValue->i4_Length = SNMP_IPADDR_LEN;
                i4Counter += SNMPMGREstimateOctetstringLength
                    (pVbPtr->ObjValue.pOctetStrValue);
                break;
            case SNMP_DATA_TYPE_NULL:
                i4Counter += SNMP_TWO;    /* 0x00  SNMP_DATA_TYPE_NULL */
                break;
            default:
                return (SNMP_NOT_OK);
        }                        /* end of switch */
        i4Counter += SNMPMGREstimateOidLength (pVbPtr->pObjName);
        i4VbLen = i4Counter - i4VbLen;
        i4Counter += SNMPMGREstimateLengthLength (i4VbLen);
        i4Counter++;            /*  SNMP_DATA_TYPE_SEQUENCE */
        pVbPtr = pVbPtr->pNextVarBind;
    }
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPMGREstimateOidLength
*  Description   :This procedure estimates the length of OID
*
*
*  Parameter(s)  :oid ptr
*
*  Return Values : length of the OID
*********************************************************************/

INT4
SNMPMGREstimateOidLength (tSNMP_OID_TYPE * pOidPtr)
{
    INT4                i4Seq = 0, i4Counter = 0;
    UINT4               i4Check = 0;
    INT4                i4Len = 0, i4Count = 0;

    if (pOidPtr->u4_Length > SNMP_TWO)
    {
        for (i4Seq = (INT4) pOidPtr->u4_Length - SNMP_ONE; i4Seq > SNMP_ONE;
             i4Seq--)
        {
            i4Counter++;
            i4Count = SNMP_ONELONG;
            i4Check = 0x80;
            while ((pOidPtr->pu4_OidList[i4Seq] >= (UINT4) i4Check) &&
                   (i4Count < 5))
            {
                i4Check <<= 7;
                i4Counter++;
                i4Count++;
            }
        }
    }
    i4Counter++;
    i4Len = i4Counter;
    i4Counter += SNMPMGREstimateLengthLength (i4Len);
    i4Counter++;
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPMGREstimateOctetstringLength
*  Description   :This procedure eestimates the length of OctetString
*
*
*  Parameter(s)  : octet string ptr
*
*  Return Values : length
*********************************************************************/

INT4
SNMPMGREstimateOctetstringLength (tSNMP_OCTET_STRING_TYPE * pOctetString)
{
    INT4                i4Seq = 0, i4Counter = 0;
    for (i4Seq = pOctetString->i4_Length - SNMP_ONE;
         i4Seq >= SNMP_ZERO; i4Seq--)
    {
        i4Counter++;
    }
    i4Counter += SNMPMGREstimateLengthLength (pOctetString->i4_Length);
    i4Counter++;
    return (i4Counter);
}

/*********************************************************************
*  Function Name :SNMPMGRWriteGetMsg
*  Description   :This procedure encodes the fields corresponding to 
*                 GET-Response  Message. 
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRWriteGetMsg (UINT1 **ppu1CurrPtr, tSNMP_NORMAL_PDU * pMsgPtr,
                 INT4 i4TypeOfMsg)
{
    UINT1              *pu1StartPtr = NULL;
    INT4                i4VbLen = 0;

    pu1StartPtr = *ppu1CurrPtr;

    /*
     * Before Writing VarBind Lists, Reverse the VarBinds
     * in Place, so that the PDU Reversal later would encode
     * the VarBind in the Correct Order.
     */
    SNMPMGRReverseVarBindList (&(pMsgPtr->pVarBindList));
    SNMPMGRWriteVarbind (ppu1CurrPtr, pMsgPtr->pVarBindList);
    i4VbLen = *ppu1CurrPtr - pu1StartPtr;
    if (SNMPMGRWriteLength (ppu1CurrPtr, i4VbLen) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_SEQUENCE;
    if ((pMsgPtr->i2_PduType == SNMP_PDU_TYPE_GET_BULK_REQUEST) &&
        (i4TypeOfMsg != SNMP_PDU_TYPE_GET_RESPONSE))
    {
        if ((SNMPMGRWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_MaxRepetitions,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
            ||
            (SNMPMGRWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_NonRepeaters,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK))
        {
            return (SNMP_NOT_OK);
        }
    }
    else
    {

        if ((SNMPMGRWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_ErrorIndex,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
            ||
            (SNMPMGRWriteSignedint
             (ppu1CurrPtr, pMsgPtr->i4_ErrorStatus,
              SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK))
        {
            return (SNMP_NOT_OK);
        }
    }
    if (SNMPMGRWriteSignedint (ppu1CurrPtr, (INT4) pMsgPtr->u4_RequestID,
                            SNMP_DATA_TYPE_INTEGER32) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    SNMPMGRReverseVarBindList (&(pMsgPtr->pVarBindList));
    return (SNMP_OK);

}

/*********************************************************************
*  Function Name :SNMPMGRWriteLength
*  Description   :This procedure encodes the length of the values
*          The length is written in the data according to the BER format.
*          The values are written backwards.
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRWriteLength (UINT1 **ppu1CurrPtr, INT4 i4Len)
{
    INT4                i4LenOfLen = SNMP_NOT_OK, i4Seq = 0;

    i4LenOfLen = (INT4) SNMPMGRFindLength (i4Len);
    if (i4LenOfLen == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (i4LenOfLen == SNMP_ONE)
    {
        *(*ppu1CurrPtr)++ = (UINT1) i4Len;
        return (SNMP_OK);
    }
    for (i4Seq = SNMP_ONE; i4Seq < i4LenOfLen; i4Seq++)
    {
        *(*ppu1CurrPtr)++ = (UINT1) (((UINT4) i4Len >>
                                      (SNMP_EIGHT *
                                       ((UINT4) i4Seq - SNMP_ONE))) & 0x0FF);
    }
    *(*ppu1CurrPtr)++ = (UINT1) ((UINT1) 0x80 + (UINT1) i4LenOfLen - SNMP_ONE);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRWriteOctetstring
*  Description   :This procedure encodes the OctetString structure
*          The values are written backwards
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRWriteOctetstring (UINT1 **ppu1CurrPtr,
                      tSNMP_OCTET_STRING_TYPE * pOctetString, INT2 i2Type)
{
    INT4                i4Seq = 0;
    for (i4Seq = pOctetString->i4_Length - SNMP_ONE;
         i4Seq >= SNMP_ZERO; i4Seq--)
    {
        *(*ppu1CurrPtr)++ = pOctetString->pu1_OctetList[i4Seq];
    }
    SNMPMGRWriteLength (ppu1CurrPtr, pOctetString->i4_Length);
    *(*ppu1CurrPtr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRWriteUnsignedint
*  Description   : This procedure encodes the unsigned integers
*           The values are written backwards.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/
INT4
SNMPMGRWriteUnsignedint (UINT1 **ppu1CurrPtr, UINT4 u4Value, INT2 i2Type)
{
    UINT4               u4Count = 0;
    UINT4               u4Check = 0, u4TempValue = 0;

    u4Count = SNMP_ONELONG;
    u4Check = 0xff;
    /* WINSNMP Warnigs */
    *(*ppu1CurrPtr)++ = (UINT1) u4Value & 0xff;
    u4TempValue = u4Value & 0xff;
    while ((u4TempValue != u4Value) && (u4Count < SNMP_FOUR))
    {
        /* WINSNMP Warnigs */
        *(*ppu1CurrPtr)++ = (UINT1) (u4Value >> SNMP_EIGHT * u4Count) & 0xff;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count)) & u4Value;
        u4Count++;
    }
    if (((u4Value >> SNMP_EIGHT * (u4Count - SNMP_ONE)) & 0x80) != 0)
    {
        *(*ppu1CurrPtr)++ = SNMP_ZERO;
        u4Count++;
    }
    SNMPMGRWriteLength (ppu1CurrPtr, (INT4) u4Count);
    *(*ppu1CurrPtr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGREstimateUnsignedintLength
*  Description   :This procedure estimates the length of unsigned integers.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGREstimateUnsignedintLength (UINT4 u4Value)
{
    INT4                i4Counter = 0;
    UINT4               u4Check = 0, u4TempValue = 0;
    UINT4               i4Count = 0;
    i4Count = SNMP_ONELONG;
    u4Check = 0xff;
    i4Counter++;
    u4TempValue = u4Value & 0xff;
    while ((u4TempValue != u4Value) && (i4Count < SNMP_FOUR))
    {
        i4Counter++;
        u4TempValue |= (u4Check << (SNMP_EIGHT * i4Count)) & u4Value;
        i4Count++;
    }
    if (((u4Value >> SNMP_EIGHT * (i4Count - SNMP_ONE)) & 0x80) != 0)
    {
        i4Counter++;
        i4Count++;
    }
    i4Counter = (INT4) (i4Counter + SNMPMGREstimateLengthLength ((INT4) i4Count));
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPMGREstimateLengthLength
*  Description   :This procedure estimates the length of the 
*                 length of the values.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGREstimateLengthLength (INT4 i4Len)
{
    INT4                i4LenOfLen, i4Seq, i4Counter = 0;

    i4LenOfLen = (INT4) SNMPMGRFindLength (i4Len);
    if (i4LenOfLen == SNMP_ONE)
    {
        i4Counter++;
        return (i4Counter);
    }
    for (i4Seq = SNMP_ONE; i4Seq < i4LenOfLen; i4Seq++)
    {
        i4Counter++;
    }
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPMGRFindLength
*  Description   :This procedure finds the length of the values
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRFindLength (INT4 i4Len)
{
    /* single octet */
    if (i4Len < 128)
    {
        return (SNMP_ONE);
    }
    if (i4Len < 0x0100)
    {
        return (SNMP_TWO);
    }
    if (i4Len < 0x010000)
    {
        return (3);
    }
    if (i4Len < 0x01000000)
    {
        return (SNMP_FOUR);
    }
    return (SNMP_FOUR);
}

/*********************************************************************
*  Function Name :SNMPMGREstimateCounter64Length 
*  Description   :This procedure estimates the length of Counter64
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGREstimateCounter64Length (tSNMP_COUNTER64_TYPE u8Value)
{
    INT4                i4Count = 0, i4Counter = 0;
    UINT4               u4Check = 0, u4TempValue = 0;
    UINT4               u4Count2 = 0, u4Count1 = 0;

    u4Count1 = SNMP_ONELONG;
    u4Check = 0xff;
    i4Counter++;
    u4TempValue = u8Value.lsn & 0xff;
    while ((u4TempValue != u8Value.lsn) && (u4Count1 < SNMP_FOUR))
    {
        i4Counter++;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count1)) & u8Value.lsn;
        u4Count1++;
    }

    u4Count2 = 0L;

    if (u8Value.msn > SNMP_ZERO)
    {
        u4Count2 = SNMP_ONELONG;
        u4Check = 0xff;
        i4Counter++;
        u4TempValue = u8Value.msn & 0xff;
        while ((u4TempValue != u8Value.msn) && (u4Count2 < SNMP_FOUR))
        {
            i4Counter++;
            u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count2)) & u8Value.msn;
            u4Count2++;
        }
    }

    if (((u8Value.msn >> SNMP_EIGHT * (u4Count2 - SNMP_ONE)) & 0x80) !=
        SNMP_ZERO)
    {
        i4Counter++;
        u4Count2++;
    }
    else
    {
        if (((u8Value.lsn >> SNMP_EIGHT * (u4Count1 - SNMP_ONE)) & 0x80) !=
            SNMP_ZERO)
        {
            i4Counter++;
            u4Count2++;
        }
    }

    i4Count = (INT4) (u4Count1 + u4Count2);

    i4Counter += SNMPMGREstimateLengthLength (i4Count);
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPMGREstimateSignedintLength
*  Description   :This procedure estimates the length of signed integers
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGREstimateSignedintLength (INT4 i4Value)
{
    UINT4               u4Count = 0, u4Times = 0;
    INT4                i4Counter = 0;
    INT4                i4TempValue = 0;
    INT4                i4Temp = 0;
    UINT4               u4Check = 0;

    u4Count = SNMP_ONE;
    u4Check = 0xff;

    if ((((UINT4) i4Value & 0xff000000) == 0xff000000)
        && (i4Value & 0x00800000))
    {
        u4Times = 3;
    }
    else
    {
        u4Times = SNMP_FOUR;
    }
    i4Counter++;
    i4TempValue = (INT4) ((UINT4) i4Value & u4Check);
    i4Temp = SNMP_ZERO;
    while ((i4TempValue != i4Value) && (u4Count < u4Times))
    {
        i4Counter++;
        i4TempValue =
            (INT4) ((UINT4) i4TempValue |
                    ((u4Check << (SNMP_EIGHT * u4Count)) & (UINT4) i4Value));
        u4Count++;
        i4Temp = (i4Value >> SNMP_EIGHT * u4Count) & (INT4) u4Check;
    }
    if ((i4Temp == SNMP_ZERO) && (u4Count != SNMP_FOUR))
    {
        if (((i4Value >> (SNMP_EIGHT * (u4Count - SNMP_ONE))) & 0x80) !=
            SNMP_ZERO)
        {
            i4Counter++;
            u4Count++;
        }
    }
    i4Counter += SNMPMGREstimateLengthLength ((INT4) u4Count);
    i4Counter++;
    return (i4Counter);

}

/*********************************************************************
*  Function Name :SNMPMGRReverseVarBindList 
*  Description   :This Procedure Reverses the Var-Bind Lists present
*          in the Output Message
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

VOID
SNMPMGRReverseVarBindList (tSNMP_VAR_BIND ** pVarBindList)
{
    tSNMP_VAR_BIND     *pTmp = NULL, *pStart = NULL, *pNewStart = NULL;

    pNewStart = NULL;
    pStart = *pVarBindList;

    while (pStart != NULL)
    {
        pTmp = pStart;
        pStart = pStart->pNextVarBind;
        if (pNewStart == NULL)
        {
            pNewStart = pTmp;
            pNewStart->pNextVarBind = NULL;
        }
        else
        {
            pTmp->pNextVarBind = pNewStart;
            pNewStart = pTmp;
        }
    }
    *pVarBindList = pNewStart;
}

/*********************************************************************
*  Function Name :SNMPMGRWriteVarbind
*  Description   :This procedure encodes the tSNMP_VAR_BIND structure.
*          The values are written backwards.    
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRWriteVarbind (UINT1 **ppu1CurrPtr, tSNMP_VAR_BIND * pVbPtr)
{
    INT4                u4RetVal = 0;
    UINT1              *pu1StartPtr = NULL;
    INT4                i4VbLen = 0;
    while (pVbPtr != NULL)
    {
        pu1StartPtr = *ppu1CurrPtr;
        switch (pVbPtr->ObjValue.i2_DataType)
        {
            case SNMP_DATA_TYPE_COUNTER32:
            case SNMP_DATA_TYPE_GAUGE32:
            case SNMP_DATA_TYPE_TIME_TICKS:
                u4RetVal = SNMPMGRWriteUnsignedint (ppu1CurrPtr,
                                                 pVbPtr->ObjValue.u4_ULongValue,
                                                 pVbPtr->ObjValue.i2_DataType);
                break;

            case SNMP_DATA_TYPE_COUNTER64:
                SNMPMGRWriteCounter64 (ppu1CurrPtr,
                                    pVbPtr->ObjValue.u8_Counter64Value,
                                    pVbPtr->ObjValue.i2_DataType);
                break;

            case SNMP_DATA_TYPE_INTEGER32:
                u4RetVal = SNMPMGRWriteSignedint (ppu1CurrPtr,
                                               pVbPtr->ObjValue.i4_SLongValue,
                                               pVbPtr->ObjValue.i2_DataType);
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                if (SNMPMGRWriteOid (ppu1CurrPtr, pVbPtr->ObjValue.pOidValue)
                    == SNMP_NOT_OK)
                {
                    return (SNMP_NOT_OK);
                }
                break;
            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
                u4RetVal = SNMPMGRWriteOctetstring (ppu1CurrPtr,
                                                 pVbPtr->ObjValue.
                                                 pOctetStrValue,
                                                 pVbPtr->ObjValue.i2_DataType);
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                pVbPtr->ObjValue.u4_ULongValue =
                    OSIX_HTONL (pVbPtr->ObjValue.u4_ULongValue);
                memcpy (pVbPtr->ObjValue.pOctetStrValue->pu1_OctetList,
                        &(pVbPtr->ObjValue.u4_ULongValue), SNMP_FOUR);
                pVbPtr->ObjValue.pOctetStrValue->i4_Length = SNMP_FOUR;
                u4RetVal = SNMPMGRWriteOctetstring (ppu1CurrPtr,
                                                 pVbPtr->ObjValue.
                                                 pOctetStrValue,
                                                 pVbPtr->ObjValue.i2_DataType);
                break;
            case SNMP_DATA_TYPE_NULL:
                SNMPMGRWriteNull (ppu1CurrPtr);
                break;

            case SNMP_EXCEPTION_NO_SUCH_OBJECT:
            case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
            case SNMP_EXCEPTION_END_OF_MIB_VIEW:
                SNMPMGRWriteException (ppu1CurrPtr, pVbPtr->ObjValue.i2_DataType);
                break;

            default:
                return (SNMP_NOT_OK);
        }
        /* end of switch */

        if (SNMPMGRWriteOid (ppu1CurrPtr, pVbPtr->pObjName) == SNMP_NOT_OK)
        {
            return (SNMP_NOT_OK);
        }
        i4VbLen = *ppu1CurrPtr - pu1StartPtr;
        if (SNMPMGRWriteLength (ppu1CurrPtr, i4VbLen) == SNMP_NOT_OK)
        {
            return (SNMP_NOT_OK);
        }
        *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_SEQUENCE;
        pVbPtr = pVbPtr->pNextVarBind;
    }
    UNUSED_PARAM (u4RetVal);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRWriteCounter64
*  Description   :This procedure encodes the Counter64 Value
*          The values are written backwards
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRWriteCounter64 (UINT1 **ppu1CurrPtr,
                    tSNMP_COUNTER64_TYPE u8Value, INT2 i2Type)
{
    INT4                i4Count = 0;
    UINT4               u4Count1 = 0, u4Count2 = 0;
    UINT4               u4Check = 0, u4TempValue = 0;

    u4Count1 = SNMP_ONELONG;
    u4Check = 0xff;
    *(*ppu1CurrPtr)++ = (UINT1) u8Value.lsn & 0xff;
    u4TempValue = u8Value.lsn & 0xff;
    while (u4Count1 < SNMP_FOUR)
    {
        *(*ppu1CurrPtr)++ =
            (UINT1) (u8Value.lsn >> SNMP_EIGHT * u4Count1) & 0xff;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count1)) & u8Value.lsn;
        u4Count1++;
    }

    u4Count2 = 0L;

    if (u8Value.msn > SNMP_ZERO)
    {
        u4Count2 = SNMP_ONELONG;
        u4Check = 0xff;
        *(*ppu1CurrPtr)++ = (UINT1) u8Value.msn & 0xff;
        u4TempValue = u8Value.msn & 0xff;
        while (u4Count2 < SNMP_FOUR)
        {
            *(*ppu1CurrPtr)++ =
                (UINT1) (u8Value.msn >> SNMP_EIGHT * u4Count2) & 0xff;
            u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count2)) & u8Value.msn;
            u4Count2++;
        }
    }

    if ((((u8Value.msn >> SNMP_EIGHT) * (u4Count2 - SNMP_ONE)) & 0x80) !=
        SNMP_ZERO)
    {
        *(*ppu1CurrPtr)++ = SNMP_ZERO;
        u4Count2++;
    }
    else
    {
        if (((u8Value.lsn >> SNMP_EIGHT * (u4Count1 - SNMP_ONE)) & 0x80) !=
            SNMP_ZERO)
        {
            *(*ppu1CurrPtr)++ = SNMP_ZERO;
            u4Count2++;
        }
    }

    i4Count = (INT4) (u4Count1 + u4Count2);

    SNMPMGRWriteLength (ppu1CurrPtr, i4Count);
    *(*ppu1CurrPtr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRWriteSignedint
*  Description   :This procedure encodes the signed integers.The values 
*          are written backwards
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRWriteSignedint (UINT1 **pu1Curr, INT4 i4Value, INT2 i2Type)
{
    UINT4               u4Count = 0, u4Times = 0;
    UINT4               u4TempValue = 0;
    INT4                i4Temp = 0;
    UINT4               u4Check = 0;

    u4Count = SNMP_ONE;
    u4Check = 0xff;

    /*
     * Checking Whether the Number has the First 9 Bytes as One's or
     * Zero's. If so then the First Octet (which is 0xFF) is not
     * needed.  So if First 9 Bytes are One's then We Encode only
     * 3 Bytes and leave the First Octet (which is 0xFF) else we
     * encode all the 4 bytes.
     */
    if ((((UINT4) i4Value & 0xff000000) == 0xff000000)
        && (i4Value & 0x00800000))
    {
        u4Times = 3;
    }
    else
    {
        u4Times = SNMP_FOUR;
    }
    *(*pu1Curr)++ = (UINT1) i4Value & 0xff;
    u4TempValue = (UINT4) i4Value & u4Check;
    i4Temp = SNMP_ZERO;
    while ((u4TempValue != (UINT4) i4Value) && (u4Count < u4Times))
    {
        *(*pu1Curr)++ = (UINT1) (i4Value >> SNMP_EIGHT * u4Count) & 0xff;
        u4TempValue |= (u4Check << (SNMP_EIGHT * u4Count)) & (UINT4) i4Value;
        u4Count++;
        i4Temp = (i4Value >> SNMP_EIGHT * u4Count) & (INT4) u4Check;
    }
    if ((i4Temp == SNMP_ZERO) && (u4Count != SNMP_FOUR))
    {
        if (((i4Value >> SNMP_EIGHT * (u4Count - SNMP_ONE)) & 0x80) !=
            SNMP_ZERO)
        {
            *(*pu1Curr)++ = 00;
            u4Count++;
        }
    }
    SNMPMGRWriteLength (pu1Curr, (INT4) u4Count);
    *(*pu1Curr)++ = (UINT1) (0xff & i2Type);
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRWriteOid
*  Description   :This procedure encodes the OID structure.
*          The values are written backwards.     
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRWriteOid (UINT1 **ppu1CurrPtr, tSNMP_OID_TYPE * pOidPtr)
{
    UINT1              *pu1StartPtr = NULL;
    UINT4               u4Cnt = 0, u4Check = 0, u4Count = 0;
    INT4                i4Len = 0;

    pu1StartPtr = *ppu1CurrPtr;
    if (pOidPtr->u4_Length > SNMP_TWO)
    {
        for (u4Cnt = pOidPtr->u4_Length - SNMP_ONE; u4Cnt > SNMP_ONE; u4Cnt--)
        {
            *(*ppu1CurrPtr)++ = (UINT1) pOidPtr->pu4_OidList[u4Cnt] & 0x7f;
            u4Count = SNMP_ONELONG;
            u4Check = 0x80;
            while ((pOidPtr->pu4_OidList[u4Cnt] >= u4Check) && (u4Count < 5))
            {
                u4Check <<= 7;
                *(*ppu1CurrPtr)++ = (UINT1) (((pOidPtr->pu4_OidList[u4Cnt]) >>
                                              7 * (u4Count)) | 0x80);
                u4Count++;
            }
        }
    }
    if (pOidPtr->u4_Length < SNMP_TWO)
    {
        *(*ppu1CurrPtr)++ = (UINT1) (pOidPtr->pu4_OidList[SNMP_ZERO] * 40);
    }
    else
    {
        *(*ppu1CurrPtr)++ = (UINT1) ((pOidPtr->pu4_OidList[SNMP_ZERO] * 40) +
                                     pOidPtr->pu4_OidList[SNMP_ONE]);
    }
    i4Len = *ppu1CurrPtr - pu1StartPtr;
    if (SNMPMGRWriteLength (ppu1CurrPtr, i4Len) == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_OBJECT_ID;
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRWriteNull
*  Description   : This procedure encodes the NULL type values
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

VOID
SNMPMGRWriteNull (UINT1 **ppu1CurrPtr)
{
    *(*ppu1CurrPtr)++ = 0x00;
    *(*ppu1CurrPtr)++ = SNMP_DATA_TYPE_NULL;
}

/*********************************************************************
*  Function Name :SNMPMGRWriteException
*  Description   :This procedure encodes the NULL type values
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

VOID
SNMPMGRWriteException (UINT1 **ppu1CurrPtr, INT2 i2DataType)
{
    *(*ppu1CurrPtr)++ = 0x00;
    *(*ppu1CurrPtr)++ = (UINT1) (i2DataType & 0xff);
}

/*********************************************************************
*  Function Name :SNMPMGRDecodePacket
*  Description   :This procedure decodes the raw data that comes 
*                 to the SNMP Agent. This looks at the opcode and 
*                 the length of the data and decodes the packet 
*                 accordingly. The decoded packet is put in the 
*                 internal datastructure.
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT1               *
SNMPMGRDecodePacket (UINT1 *pu1PktPtr, INT4 i4PktLen, INT2 *pPduType)
{
    tSNMP_TRAP_PDU     *pV1Trap = NULL;
    tSNMP_NORMAL_PDU   *pMsgPtr = NULL;
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1EndPtr = NULL;
    UINT1              *pu1NewEndPtr = NULL;
    UINT4               u4Version = 0;
    INT4                i4TotalLength = 0;
    INT4                i4Len = 0;
    INT2                i2Type = 0;
    INT2                i2MsgType = 0;
    UINT1               gau1CommunityDataFirst[SNMP_MAX_OCTETSTRING_SIZE];
    UINT1               gau1AgentAddressFirst[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE CommunityFirst;
    tSNMP_TRAP_PDU  gV1TrapPduFirst;
    tSNMP_OCTET_STRING_TYPE AgentAddressFirst;
    tSNMP_OID_TYPE EnterpriseOidFirst;
    UINT4 EnterpriseOidListMngr[MAX_OID_LENGTH];
    tSNMP_NORMAL_PDU    gRxPduMngr;

    pu1CurrPtr = pu1PktPtr;
    pu1EndPtr = pu1CurrPtr + i4PktLen;
    i4TotalLength = SNMPMGRDecodeSequence (&pu1CurrPtr, pu1EndPtr);
    if (i4TotalLength == SNMP_NOT_OK)
    {
        return (NULL);
    }
    pu1NewEndPtr = pu1CurrPtr + i4TotalLength;
    if (pu1NewEndPtr != pu1EndPtr)
    {
        pu1EndPtr = pu1NewEndPtr;
    }
    u4Version = (UINT4) SNMPMGRDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                           &i2Type, SNMP_UNSIGNED);
    if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        return (NULL);
    }
    CommunityFirst.pu1_OctetList = gau1CommunityDataFirst;
    MEMSET (gau1CommunityDataFirst, SNMP_ZERO, sizeof (gau1CommunityDataFirst));
    if ((SNMPMGRDecodeOctetstring (&pu1CurrPtr, pu1EndPtr,
                                &i2Type, &CommunityFirst)) == NULL)
    {
        return (NULL);
    }
    if (i2Type != SNMP_DATA_TYPE_OCTET_PRIM)
    {
        return (NULL);
    }
    i4Len = SNMPMGRDecodeTypeLen (&pu1CurrPtr, pu1EndPtr, &i2MsgType);

    if ((i4Len == SNMP_NOT_OK) || (i4Len == SNMP_ZERO))
    {
        return (NULL);
    }

    if (i2MsgType == SNMP_NOT_OK)
    {
        return (NULL);
    }
    /*condition detects pointer overflow and wrap around */
    if ((pu1CurrPtr + i4Len > pu1EndPtr) || (pu1CurrPtr + i4Len < pu1CurrPtr))    /*silvercreek */
    {
        return (NULL);
    }

    *pPduType = (INT2) i2MsgType;

    switch ((INT4) i2MsgType)
    {
        case SNMP_PDU_TYPE_GET_REQUEST:
        case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
        case SNMP_PDU_TYPE_SET_REQUEST:
        case SNMP_PDU_TYPE_GET_BULK_REQUEST:
        case SNMP_PDU_TYPE_GET_RESPONSE:
        case SNMP_PDU_TYPE_SNMPV2_TRAP:
        case SNMP_PDU_TYPE_V2_INFORM_REQUEST:

            pMsgPtr = &gRxPduMngr;
            pMsgPtr->pCommunityStr = &CommunityFirst;
            pMsgPtr->pCommunityStr->pu1_OctetList = gau1CommunityDataFirst;
            pMsgPtr->pVarBindList = NULL;
            pMsgPtr->pVarBindEnd = NULL;
            pMsgPtr->i4_ErrorStatus = 0;
            pMsgPtr->i4_ErrorIndex = 0;
            pMsgPtr->u4_RequestID =
                (UINT4) SNMPMGRDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                           &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPMGRFreeNormalMsg (pMsgPtr);
                return (NULL);
            }
            pMsgPtr->i4_ErrorStatus =
                (INT4) SNMPMGRDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                          &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPMGRFreeNormalMsg (pMsgPtr);
                return (NULL);
            }

            if (i2MsgType == SNMP_PDU_TYPE_GET_BULK_REQUEST)
            {
                pMsgPtr->i4_NonRepeaters = pMsgPtr->i4_ErrorStatus;
                pMsgPtr->i4_ErrorStatus = SNMP_ZERO;
            }

            pMsgPtr->i4_ErrorIndex = SNMPMGRDecodeInteger (&pu1CurrPtr, pu1EndPtr,
                                                        &i2Type, SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPMGRFreeNormalMsg (pMsgPtr);
                return (NULL);
            }

            if (i2MsgType == SNMP_PDU_TYPE_GET_BULK_REQUEST)
            {
                pMsgPtr->i4_MaxRepetitions = pMsgPtr->i4_ErrorIndex;
                pMsgPtr->i4_ErrorIndex = SNMP_ZERO;
            }

            if (SNMPMGRDecodeSequence (&pu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
            {
                SNMPMGRFreeNormalMsg (pMsgPtr);
                return (NULL);
            }
            pMsgPtr->u4_Version = u4Version;
            pMsgPtr->pCommunityStr = &CommunityFirst;
            pMsgPtr->i2_PduType = i2MsgType;
            break;
            /* V1 Trap */
        case SNMP_PDU_TYPE_TRAP:
            MEMSET (&gV1TrapPduFirst, 0, sizeof (gV1TrapPduFirst));
            pV1Trap = &gV1TrapPduFirst;
            pV1Trap->pAgentAddr = &AgentAddressFirst;
            pV1Trap->pAgentAddr->pu1_OctetList = gau1AgentAddressFirst;
            pV1Trap->pEnterprise = &EnterpriseOidFirst;
            pV1Trap->pEnterprise->pu4_OidList = EnterpriseOidListMngr;

            if (NULL ==
                SNMPMGRDecodeOid (&pu1CurrPtr, pu1EndPtr, pV1Trap->pEnterprise))
            {
                SNMPMGRFreeV1TrapMsg (pV1Trap);
                return NULL;
            }

            if (NULL == SNMPMGRDecodeOctetstring (&pu1CurrPtr, pu1EndPtr,
                                               &i2Type, pV1Trap->pAgentAddr))
            {
                SNMPMGRFreeV1TrapMsg (pV1Trap);
                return NULL;
            }

            pV1Trap->i4_GenericTrap =
                (INT4) SNMPMGRDecodeInteger (&pu1CurrPtr, pu1EndPtr, &i2Type,
                                          SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPMGRFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }

            pV1Trap->i4_SpecificTrap =
                (INT4) SNMPMGRDecodeInteger (&pu1CurrPtr, pu1EndPtr, &i2Type,
                                          SNMP_SIGNED);
            if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
            {
                SNMPMGRFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }

            pV1Trap->u4_TimeStamp =
                (UINT4) SNMPMGRDecodeInteger (&pu1CurrPtr, pu1EndPtr, &i2Type,
                                           SNMP_UNSIGNED);
            if ((i2Type == SNMP_NOT_OK)
                || (i2Type != SNMP_DATA_TYPE_TIME_TICKS))
            {
                SNMPMGRFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }

            if (SNMPMGRDecodeSequence (&pu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
            {
                SNMPMGRFreeV1TrapMsg (pV1Trap);
                return (NULL);
            }
            pV1Trap->u4_Version = u4Version;
            pV1Trap->pCommunityStr = &CommunityFirst;
            pV1Trap->i2_PduType = i2MsgType;
            break;

        default:
            return (NULL);
    }                            /* end of switch(msg_type) */

    while (pu1CurrPtr < pu1EndPtr)
    {
        pVarBindPtr = SNMPMGRDecodeVarbind (&pu1CurrPtr, pu1EndPtr);
        if (pVarBindPtr == NULL)
        {
            if (pMsgPtr)
                SNMPMGRFreeNormalMsg (pMsgPtr);
            else if (pV1Trap)
                SNMPMGRFreeV1TrapMsg (pV1Trap);

            return (NULL);
        }
        if (pMsgPtr)
        {
            if (pMsgPtr->pVarBindList == NULL)
            {
                pMsgPtr->pVarBindList = pVarBindPtr;
                pMsgPtr->pVarBindEnd = pVarBindPtr;
            }
            else
            {
                pMsgPtr->pVarBindEnd->pNextVarBind = pVarBindPtr;
                pMsgPtr->pVarBindEnd = pVarBindPtr;
            }
        }
        else if (pV1Trap)
        {
            if (pV1Trap->pVarBindList == NULL)
            {
                pV1Trap->pVarBindList = pVarBindPtr;
                pV1Trap->pVarBindEnd = pVarBindPtr;
            }
            else
            {
                if (NULL != pV1Trap->pVarBindEnd)
                {
                    pV1Trap->pVarBindEnd->pNextVarBind = pVarBindPtr;
                    pV1Trap->pVarBindEnd = pVarBindPtr;
                }
            }
        }
        pVarBindPtr = NULL;
    }

    if (pMsgPtr)
        return ((INT1 *) pMsgPtr);
    else
        return ((INT1 *) pV1Trap);
}

/*********************************************************************
*  Function Name :SNMPMGRDecodeSequence
*  Description   :This procedure decodes the Sequence Tags in the SNMP Message.
*
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRDecodeSequence (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr)
{
    INT4                i4Len = SNMP_NOT_OK;
    INT2                i2Type = 0;

    i4Len = SNMPMGRDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, &i2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (i2Type != SNMP_DATA_TYPE_SEQUENCE)
    {
        return (SNMP_NOT_OK);
    }
    return (i4Len);
}

/*********************************************************************
*  Function Name :SNMPMGRDecodeTypeLen
*  Description   :This procedure decodes the Type of the tag in the 
*                 SNMP Message.The length of the value, is present 
*                 before the value in the BER encoded data.
*                 This length of the value following is decoded.
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRDecodeTypeLen (UINT1 **ppu1CurrPtr, const UINT1 *pu1EndPtr, INT2 *i2Type)
{
    INT4                i4Len = 0;
    INT4                i4Lenlen = 0;
    INT4                i4Seq = 0;

    if (*ppu1CurrPtr > pu1EndPtr)    /*silver creek */
    {
        return (SNMP_NOT_OK);
    }
    *i2Type = *(*ppu1CurrPtr)++;

    if (*ppu1CurrPtr > pu1EndPtr)    /*silver creek */
    {
        return (SNMP_NOT_OK);
    }
    i4Len = (INT4) *(*ppu1CurrPtr)++;
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        return (SNMP_NOT_OK);
    }
    switch (*i2Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
        case SNMP_DATA_TYPE_NULL:
        case SNMP_DATA_TYPE_OBJECT_ID:
        case SNMP_DATA_TYPE_SEQUENCE:
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
        case SNMP_DATA_TYPE_COUNTER32:

        case SNMP_DATA_TYPE_COUNTER64:

        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
        case SNMP_PDU_TYPE_GET_REQUEST:
        case SNMP_PDU_TYPE_GET_NEXT_REQUEST:
        case SNMP_PDU_TYPE_GET_RESPONSE:
        case SNMP_PDU_TYPE_SET_REQUEST:
        case SNMP_PDU_TYPE_TRAP:

        case SNMP_PDU_TYPE_GET_BULK_REQUEST:
        case SNMP_PDU_TYPE_SNMPV2_TRAP:
        case SNMP_PDU_TYPE_V2_INFORM_REQUEST:
        case SNMP_PDU_TYPE_GET_REPORT:

            break;
        default:
            return (SNMP_NOT_OK);
    }
    if (i4Len < 0x80)
    {
        return (i4Len);
    }
    i4Lenlen = i4Len & 0x7f;
    if ((i4Lenlen > SNMP_FOUR) || (i4Lenlen < SNMP_ONE))
    {
        return (SNMP_NOT_OK);
    }
    i4Len = SNMP_ZERO;
    for (i4Seq = SNMP_ZERO; i4Seq < i4Lenlen; i4Seq++)
    {
        i4Len = (i4Len << SNMP_EIGHT) + *(*ppu1CurrPtr)++;
    }
    /*Check is added to avoid wrap around */
    if (i4Len > (pu1EndPtr - (*ppu1CurrPtr)))
    {
        return (SNMP_NOT_OK);
    }

    /* For Negative Values */
    if (i4Len < SNMP_ZERO)
    {
        return (SNMP_NOT_OK);
    }
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        return (SNMP_NOT_OK);
    }
    return (i4Len);
}                                /* end of Decodei2Typelen */

/*********************************************************************
*  Function Name :SNMPMGRDecodeInteger
*  Description   :This procedure decodes the Integer type data 
*                 in the SNMP Message
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRDecodeInteger (UINT1 **ppu1Curr, UINT1 *pu1End, INT2 *i2Type,
                   INT2 i2UnsignOrSign)
{
    INT4                i4Value = 0;
    INT4                i4Len = 0;
    INT4                i4Seq = 0;
    UINT4               u4Sign = 0;
    i4Value = 0L;
    i4Len = SNMPMGRDecodeTypeLen (ppu1Curr, pu1End, i2Type);
    if ((i4Len == SNMP_NOT_OK) || (i4Len == SNMP_ZERO))
    {
        *i2Type = (INT2) SNMP_NOT_OK;
        return (SNMP_OK);
    }
    if (i2UnsignOrSign == SNMP_ONE)
    {
        if (i4Len > SNMP_FOUR)
        {
            *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
            return (SNMP_OK);
        }
    }
    else
    {
        if ((i4Len > 5) || ((i4Len > SNMP_FOUR) && (*(*ppu1Curr) != 0x00)))
        {
            *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
            return (SNMP_OK);
        }
    }
    if (i2UnsignOrSign == SNMP_ONE)
    {
        u4Sign = ((*(*ppu1Curr) & 0x80) == 0x00) ? 0x00 : 0xff;
    }

    for (i4Seq = SNMP_ZERO; i4Seq < i4Len; i4Seq++)
    {
        i4Value = (i4Value << SNMP_EIGHT) + (INT4) *(*ppu1Curr)++;
    }
    /*
     * now fill in the upper bits with the appropriate sign extension.
     */
    if (i2UnsignOrSign == SNMP_ONE)
    {
        for (i4Seq = i4Len; i4Seq < SNMP_FOUR; i4Seq++)
        {
            i4Value = i4Value + ((INT4) u4Sign << (i4Seq * SNMP_EIGHT));
        }
    }
    if (*ppu1Curr > pu1End)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (SNMP_OK);
    }
    return (i4Value);
}

/*********************************************************************
*  Function Name :SNMPMGRDecodeOctetstring
*  Description   :This procedure decodes the OctetString type data in the SNMP
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

tSNMP_OCTET_STRING_TYPE *
SNMPMGRDecodeOctetstring (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr,
                       INT2 *i2Type, tSNMP_OCTET_STRING_TYPE * pOctetString)
{
    INT4                i4Len = SNMP_NOT_OK;

    if (((i4Len = SNMPMGRDecodeTypeLen (ppu1CurrPtr, pu1EndPtr,
                                     i2Type)) == SNMP_NOT_OK) ||
        (*i2Type == SNMP_NOT_OK))
    {
        return (NULL);
    }
    if ((((*ppu1CurrPtr) + i4Len) > pu1EndPtr) || ((*ppu1CurrPtr) + i4Len) < (*ppu1CurrPtr))    /*silvercreek */
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (NULL);
    }

    if ((SNMPMGRFormOctetString (*ppu1CurrPtr, i4Len, pOctetString)) == NULL)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (NULL);
    }

    (*ppu1CurrPtr) += i4Len;

    if (*ppu1CurrPtr > pu1EndPtr)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (NULL);
    }
    return (pOctetString);
}                                /* end of Decodeoctetstring() */

/*********************************************************************
*  Function Name :SNMPMGRDecodeOid
*  Description   :This procedure decodes the OIDs of the SNMP Message
*  Parameter(s)  :
*  Return Values :
*********************************************************************/
tSNMP_OID_TYPE     *
SNMPMGRDecodeOid (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr, tSNMP_OID_TYPE * pOidPtr)
{
    INT4                i4Len = SNMP_NOT_OK, i4Seq = 0;
    INT2                i2Type = 0;
    i4Len = SNMPMGRDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, &i2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        return (NULL);
    }
    if (i2Type != SNMP_DATA_TYPE_OBJECT_ID)
    {
        return (NULL);
    }
    if (i4Len == SNMP_ZERO)
    {
        pOidPtr->u4_Length = SNMP_ZERO;
        return (pOidPtr);
    }

    if ((((*ppu1CurrPtr) + i4Len) > pu1EndPtr) || ((*ppu1CurrPtr) + i4Len) < *ppu1CurrPtr)    /*silvercreek */
    {
        return (NULL);
    }

    pOidPtr->pu4_OidList[SNMP_ZERO] = (UINT4) (*(*ppu1CurrPtr) / 40);
    pOidPtr->pu4_OidList[SNMP_ONE] = (UINT4) (*(*ppu1CurrPtr)++ -
                                              (pOidPtr->pu4_OidList[SNMP_ZERO] *
                                               40));
    pOidPtr->u4_Length = SNMP_TWO;
    pOidPtr->pu4_OidList[SNMP_TWO] = SNMP_ZERO;
    for (i4Seq = SNMP_ZERO; i4Seq < i4Len - SNMP_ONE; i4Seq++)
    {
        if (pOidPtr->u4_Length < MAX_OID_LENGTH)
        {
            pOidPtr->pu4_OidList[pOidPtr->u4_Length] =
                (pOidPtr->pu4_OidList[pOidPtr->u4_Length] << 7) +
                (*(*ppu1CurrPtr) & 0x7F);
            if ((*(*ppu1CurrPtr)++ & 0x80) == SNMP_ZERO)
            {
                pOidPtr->u4_Length++;
                if (i4Seq < i4Len - SNMP_TWO)
                {
                    pOidPtr->pu4_OidList[pOidPtr->u4_Length] = SNMP_ZERO;
                }
            }
        }
        else
        {
            /*OIDs sub-identifier length exceeds MAX_OID_LENGTH
             * drop the request*/
            return (NULL);
        }
    }
    if (*ppu1CurrPtr > pu1EndPtr)
    {

        return (NULL);
    }
    return (pOidPtr);
}

/*********************************************************************
*  Function Name :SNMPMGRDecodeVarbind
*  Description   :This procedure decodes the varbind part of the 
*                 SNMP Message The data is stored in the 
*                 internal datastructure of type tSNMP_VAR_BIND.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

tSNMP_VAR_BIND     *
SNMPMGRDecodeVarbind (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr)
{
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;

    if (SNMPMGRDecodeSequence (ppu1CurrPtr, pu1EndPtr) == SNMP_NOT_OK)
    {
        return (NULL);
    }

    MEMSET (gau1_OctetCode, '\0', sizeof (gau1_OctetCode)); 
    MEMSET (gu4_OidListCode, 0, sizeof (gu4_OidListCode)); 
    MEMSET (gau4_oidListCode, 0, sizeof (gau4_oidListCode)); 
    gtOctateCode.pu1_OctetList = gau1_OctetCode;
    gtOidCode.pu4_OidList = gu4_OidListCode;
    gVarBindCode.ObjValue.pOctetStrValue = &gtOctateCode;
    gVarBindCode.ObjValue.pOidValue = &gtOidCode;
    gObjNameCode.pu4_OidList = (UINT4 *)&gau4_oidListCode;
    gVarBindCode.pNextVarBind = NULL;
    gVarBindCode.pObjName = &gObjNameCode;
    pVarBindPtr = &gVarBindCode;

    if (pVarBindPtr == NULL)
    {
        return NULL;
    }
    if ((SNMPMGRDecodeOid (ppu1CurrPtr, pu1EndPtr, pVarBindPtr->pObjName)) == NULL)
    {
        return (NULL);
    }

    switch (*(*ppu1CurrPtr))
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
            pVarBindPtr->ObjValue.u4_ULongValue =
                (UINT4) SNMPMGRDecodeInteger (ppu1CurrPtr, pu1EndPtr,
                                           &pVarBindPtr->ObjValue.
                                           i2_DataType, SNMP_UNSIGNED);
            if (pVarBindPtr->ObjValue.i2_DataType == SNMP_NOT_OK)
            {
                return (NULL);
            }
            break;

        case SNMP_DATA_TYPE_COUNTER64:
            SNMPMGRDecodeCounter64Value (ppu1CurrPtr,
                                      pu1EndPtr,
                                      &pVarBindPtr->ObjValue.
                                      i2_DataType,
                                      &(pVarBindPtr->ObjValue.
                                        u8_Counter64Value));
            if (pVarBindPtr->ObjValue.i2_DataType == SNMP_NOT_OK)
            {
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_INTEGER32:    /* handle signed integers */
            pVarBindPtr->ObjValue.i4_SLongValue =
                (INT4) SNMPMGRDecodeInteger (ppu1CurrPtr, pu1EndPtr,
                                          &pVarBindPtr->ObjValue.
                                          i2_DataType, SNMP_SIGNED);
            if (pVarBindPtr->ObjValue.i2_DataType == SNMP_NOT_OK)
            {
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            pVarBindPtr->ObjValue.i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
            if ((SNMPMGRDecodeOid (ppu1CurrPtr,
                                pu1EndPtr,
                                pVarBindPtr->ObjValue.pOidValue)) == NULL)
            {
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            if ((SNMPMGRDecodeOctetstring (ppu1CurrPtr, pu1EndPtr,
                                        &pVarBindPtr->ObjValue.
                                        i2_DataType,
                                        pVarBindPtr->ObjValue.
                                        pOctetStrValue)) == NULL)
            {
                return (NULL);
            }
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            if ((SNMPMGRDecodeOctetstring (ppu1CurrPtr, pu1EndPtr,
                                        &pVarBindPtr->ObjValue.
                                        i2_DataType,
                                        pVarBindPtr->ObjValue.
                                        pOctetStrValue)) == NULL)
            {
                return (NULL);
            }
            memcpy (&(pVarBindPtr->ObjValue.u4_ULongValue),
                    pVarBindPtr->ObjValue.pOctetStrValue->pu1_OctetList,
                    SNMP_FOUR);
            pVarBindPtr->ObjValue.u4_ULongValue =
                OSIX_HTONL (pVarBindPtr->ObjValue.u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_NULL:
            if (SNMPMGRDecodeNull (ppu1CurrPtr, pu1EndPtr,
                                &pVarBindPtr->ObjValue.i2_DataType) ==
                SNMP_NOT_OK)
            {
                return (NULL);
            }
            break;

        case SNMP_EXCEPTION_NO_SUCH_OBJECT:
        case SNMP_EXCEPTION_NO_SUCH_INSTANCE:
        case SNMP_EXCEPTION_END_OF_MIB_VIEW:
            pVarBindPtr->ObjValue.i2_DataType = (*(*ppu1CurrPtr));
            *ppu1CurrPtr = *ppu1CurrPtr + SNMP_TWO;
            break;

        default:
            return (NULL);
    }                            /* end of switch */
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        return (NULL);
    }
    return (pVarBindPtr);
}

/*********************************************************************
*  Function Name :SNMPMGRDecodeCounter64Value
*  Description   :This procedure decodes the Counter64 type data in 
*            the SNMP Message.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

VOID
SNMPMGRDecodeCounter64Value (UINT1 **ppu1Curr, UINT1 *pu1End,
                          INT2 *i2Type, tSNMP_COUNTER64_TYPE * pu8Value)
{
    tSNMP_COUNTER64_TYPE u8_zero_value;
    INT4                i4Len = 0;
    INT4                i4Seq = 0;

    pu8Value->lsn = 0L;
    pu8Value->msn = 0L;

    u8_zero_value.lsn = 0L;
    u8_zero_value.msn = 0L;

    i4Len = SNMPMGRDecodeTypeLen (ppu1Curr, pu1End, i2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        pu8Value->msn = u8_zero_value.msn;
        pu8Value->lsn = u8_zero_value.lsn;
        return;
    }
    if ((i4Len > 9) || ((i4Len > 8) && (*(*ppu1Curr) != 0x00)))
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        pu8Value->msn = u8_zero_value.msn;
        pu8Value->lsn = u8_zero_value.lsn;
        return;
    }

    if (i4Len > SNMP_FOUR)
    {
        for (i4Seq = SNMP_ZERO; i4Seq < (i4Len - SNMP_FOUR); i4Seq++)
        {
            pu8Value->msn =
                (pu8Value->msn << SNMP_EIGHT) + (UINT4) *(*ppu1Curr)++;
        }
        for (i4Seq = i4Len - SNMP_FOUR; i4Seq < i4Len; i4Seq++)
        {
            pu8Value->lsn =
                (pu8Value->lsn << SNMP_EIGHT) + (UINT4) *(*ppu1Curr)++;
        }
    }
    else
    {
        for (i4Seq = SNMP_ZERO; i4Seq < i4Len; i4Seq++)
        {
            pu8Value->lsn =
                (pu8Value->lsn << SNMP_EIGHT) + (UINT4) *(*ppu1Curr)++;
        }
    }

    if (*ppu1Curr > pu1End)
    {
        *i2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        pu8Value->msn = u8_zero_value.msn;
        pu8Value->lsn = u8_zero_value.lsn;
        return;
    }
    return;
}

/*********************************************************************
*  Function Name :SNMPMGRDecodeNull
*  Description   :This procedure decodes the NULL Type values of the 
*            SNMP Message.
*  Parameter(s)  :
*  Return Values :
*********************************************************************/

INT2
SNMPMGRDecodeNull (UINT1 **ppu1CurrPtr, UINT1 *pu1EndPtr, INT2 *pi2Type)
{
    INT4                i4Len = SNMP_NOT_OK;

    i4Len = SNMPMGRDecodeTypeLen (ppu1CurrPtr, pu1EndPtr, pi2Type);
    if (i4Len == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    if (*pi2Type != SNMP_DATA_TYPE_NULL)
    {
        *pi2Type = (INT2) SNMP_NOT_OK;    /*kloc */
        return (SNMP_NOT_OK);
    }
    *ppu1CurrPtr = *ppu1CurrPtr + i4Len;
    if (*ppu1CurrPtr > pu1EndPtr)
    {
        return (SNMP_NOT_OK);
    }
    return (SNMP_OK);
}

/*********************************************************************
*  Function Name :SNMPMGRReversePacket
*  Description   :This procedure reverses the encoded packet
*
*  Parameter(s)  :
*
*  Return Values :
*********************************************************************/

INT4
SNMPMGRReversePacket (UINT1 *pu1InPtr, UINT1 **ppu1OutPtr, INT4 i4Len)
{
    INT4                i4Seq = 0;
    UINT1               gau1RevDatFirst[MAX_PKT_LENGTH];

    *ppu1OutPtr = gau1RevDatFirst;
    for (i4Seq = i4Len - SNMP_ONE; i4Seq >= SNMP_ZERO; i4Seq--)
    {
        (*ppu1OutPtr)[i4Len - (i4Seq + SNMP_ONE)] = pu1InPtr[i4Seq];
    }
    return (SNMP_OK);
}



/*********************************************************************
*  Function Name : SNMPMGRVersionCheck
*  Description   : Function to Check the version of incomming SNMP 
*                  Request
*  Parameter(s)  : pu1Pkt - Pointer to Packet Content
*                  i4PktSize - Packet Length in Bytes  
*  Return Values : if Success returns Version Number otherwise 
*                  returns SNMP_NOT_OK
*********************************************************************/
INT4
SNMPMGRVersionCheck (UINT1 *pu1Pkt, INT4 i4PktSize)
{
    UINT1              *pu1Start = NULL, *pu1End = NULL;
    INT4                i4Length = SNMP_ZERO, i4Version = SNMP_ZERO;
    INT2                i2Type = SNMP_ZERO;
    pu1Start = pu1Pkt;
    pu1End = pu1Start + i4PktSize;
    i4Length = SNMPMGRDecodeSequence (&pu1Start, pu1End);
    if (i4Length == SNMP_NOT_OK)
    {
        return (SNMP_NOT_OK);
    }
    i4Version = (INT4) SNMPMGRDecodeInteger (&pu1Start, pu1End,
                                          &i2Type, SNMP_UNSIGNED);
    if ((i2Type == SNMP_NOT_OK) || (i2Type != SNMP_DATA_TYPE_INTEGER32))
    {
        return (SNMP_NOT_OK);
    }
    return i4Version;
}

/*********************************************************************
*  Function Name : SNMPMGRConvertStringToOid
*  Description   : Function convert string to OID format.
*  Parameter(s)  : pOid - OID
*                  pu1Data
*                  u1HexFlag
*  Return Values : if Success returns formatted OID.
*********************************************************************/
INT4
SNMPMGRConvertStringToOid (tSNMP_OID_TYPE * pOid, UINT1 *pu1Data, UINT1 u1HexFlag)
{
    UINT4               u4Count = 0, u4Dot = 0;
    UINT4               u4Value = 0;
    UINT1              *pu1String = pu1Data;
    INT1                i1RetVal = 0;

    pOid->u4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
            u4Dot++;
    }
    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (isdigit ((INT4) *pu1String))
        {
            if (u1HexFlag == 0)
                i1RetVal = SNMPMGRHexConvertSubOid (&pu1String, &u4Value);
            else
                i1RetVal = SNMPMGRConvertSubOid (&pu1String, &u4Value);
            if (i1RetVal == -1)
            {
                return SNMP_FAILURE;
            }
            pOid->pu4_OidList[u4Count] = u4Value;
            pOid->u4_Length++;
        }
        else
        {
            return SNMP_FAILURE;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SNMPMGRConvertSubOid 
 *  Description     : Convert String pointer to decemial integer32
 *  Input           : ppu1String - String Pointer
 *  Output          : pu4Value - integer equivalent of ppu1String
 *  Returns         : 0 or -1
 ************************************************************************/

INT1
SNMPMGRConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value)
{
    UINT4               u4Count = 0, u4Value = 0;
    UINT1               u1Value = 0;
    for (u4Count = 0; ((u4Count < 11) && (**ppu1String != '.') &&
                       (**ppu1String != '\0')); u4Count++)
    {
        if (!isdigit (**ppu1String))
        {
            return -1;
        }
        MEMCPY (&u1Value, *ppu1String, 1);
        u4Value = (u4Value * 10) + (0x0f & (UINT4) u1Value);
        (*ppu1String)++;
    }
    *pu4Value = u4Value;

    return 0;
}

/************************************************************************
 *  Function Name   : SNMPMGRHexConvertSubOid 
 *  Description     : Convert String pointer to Hexa integer32
 *  Input           : ppu1String - String Pointer
 *  Output          : pu4Value - integer equivalent of ppu1String
 *  Returns         : 0 or -1
 ************************************************************************/

INT1
SNMPMGRHexConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value)
{
    UINT4               u4Count = 0, u4Value = 0;
    UINT1               u1Value = 0;
    for (u4Count = 0; ((u4Count < 11) && (**ppu1String != '.') &&
                       (**ppu1String != '\0')); u4Count++)
    {
        if (!((**ppu1String >= 'a' && **ppu1String < 'g')
              || isdigit (**ppu1String)))
        {
            return -1;
        }
        else
        {
            if (!isdigit (**ppu1String))
            {
                **ppu1String = (UINT1) (10 + ((**ppu1String) - 'a'));
            }
        }
        MEMCPY (&u1Value, *ppu1String, 1);
        u4Value = (u4Value * 16) + (0x0f & (UINT4) u1Value);
        (*ppu1String)++;
    }
    *pu4Value = u4Value;

    return 0;
}


/************************************************************************
 *  Function Name   : SNMPMGRFreeVarBind 
 *  Description     : Free the given Variable Bind pointer from Variable
 *                    bind pool
 *  Input           : None
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/

VOID
SNMPMGRFreeVarBind (tSNMP_VAR_BIND * pPtr)
{
    tMemBlock          *pNew = NULL;
    tMemBlock gaMemPoolMgr[MAX_VARBIND];
    tMemBlock *pCurMgr = &(gaMemPoolMgr[0]);

    if (pPtr == NULL)
    {
        return;
    }
    pPtr->pNextVarBind = NULL;
    pNew = (tMemBlock *) pPtr;
    pNew->pNext = pCurMgr;
    pCurMgr = pNew;
    pPtr = NULL;
}

#endif /* __SNMMGR_CODE_C__ */
