/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved 
 *
 * $Id: smgrcli.c,v 1.1 2017/08/24 12:03:42 siva Exp $
 *
 * Description: Contains utility routines for packet coding
 *******************************************************************/

#ifndef __SMGR_CLI_C__
#define __SMGR_CLI_C__

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : smgrcli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                         |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : SNMPMGR                                          |
 * |                                                                           |
 * |  MODULE NAME           : SNMP MANAGER                                     |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : File contains snmp manager CLI handling.         |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */

#include "smgrcli.h"
#include "smgrinc.h"

INT4
cli_process_snmpmgr_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list            ap;
    UINT4              *args[SNMPMGR_MAX_ARGS];
    INT4               i4RetStatus = CLI_SUCCESS;
    INT1               argno = 0;
    UINT4              u4Flag = 0;

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array.
     * Store 16 arguements at the max. This is because vlan commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give
     * second input. In that case first arg will be null and second arg only
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == SNMPMGR_MAX_ARGS)
            break;
    }
    va_end (ap);
    
    switch (u4Command)
    {
        case CLI_SNMP3_SET_NAME_VALUE:
            i4RetStatus = SnmpMgrCliSetValueForName (CliHandle,
                                                    (UINT4 *) args[0],
                                                    (UINT4 ) args[1],
                                                    (CHR1 *) args[2],
                                                    (CHR1 *) args[3],
                                                    (UINT1 *) args[4]);
            break;
        case CLI_SNMP3_SET_OID_VALUE:
            i4RetStatus = SnmpMgrCliSetValueForOid (CliHandle,
                                                   (UINT4 *) args[0],
                                                   (UINT4 ) args[1],
                                                   (CHR1 *) args[2],
                                                   (CHR1 *) args[3],
                                                   (UINT1 *) args[4]);
            break;
        case CLI_SNMP3_GET_NAME:
            u4Flag = SNMP_PDU_TYPE_GET_REQUEST;
            i4RetStatus = SnmpMgrCliGetValueForName (CliHandle,
                                                    (UINT4 *) args[0],
                                                    (UINT4 ) args[1],
                                                    (CHR1 *) args[2],
                                                    (CHR1 *) args[3], u4Flag);
            break;
        case CLI_SNMP3_GET_OID:
            u4Flag = SNMP_PDU_TYPE_GET_REQUEST;
            i4RetStatus = SnmpMgrCliGetValueForOid (CliHandle,
                                                   (UINT4 *) args[0],
                                                   (UINT4 ) args[1],
                                                   (CHR1 *) args[2],
                                                   (CHR1 *) args[3], u4Flag);
            break;
        case CLI_SNMP3_GETNEXT_NAME:
            u4Flag = SNMP_PDU_TYPE_GET_NEXT_REQUEST;
            i4RetStatus = SnmpMgrCliGetValueForName (CliHandle,
                                                    (UINT4 *) args[0],
                                                    (UINT4 ) args[1],
                                                    (CHR1 *) args[2],
                                                    (CHR1 *) args[3], u4Flag);
            break;
        case CLI_SNMP3_GETNEXT_OID:
            u4Flag = SNMP_PDU_TYPE_GET_NEXT_REQUEST;
            i4RetStatus = SnmpMgrCliGetValueForOid (CliHandle,
                                                   (UINT4 *) args[0],
                                                   (UINT4 ) args[1],
                                                   (CHR1 *) args[2],
                                                   (CHR1 *) args[3], u4Flag);
            break;
        case CLI_SNMP3_NAME_WALK:
            i4RetStatus = SnmpMgrCliNameWalk (CliHandle,
                                                    (UINT4 *) args[0],
                                                    (UINT4 ) args[1],
                                                    (CHR1 *) args[2],
                                                    (CHR1 *) args[3]);
            break;
        case CLI_SNMP3_OID_WALK:
            i4RetStatus = SnmpMgrCliOidWalk (CliHandle,
                                                    (UINT4 *) args[0],
                                                    (UINT4 ) args[1],
                                                    (CHR1 *) args[2],
                                                    (CHR1 *) args[3]);

    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : SnmpMgrCliSetValueForName                  */
/*  Description     : This function is used to set the         */
/*                    value for the specified mib name         */
/*  Input(s)        : pName - mib object name,                 */
/*                    pu1Val - value to be set                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
SnmpMgrCliSetValueForName (tCliHandle CliHandle, UINT4 *u4IpAddr, UINT4 u4Port, 
                                   CHR1 *pCommunity, CHR1 * pName, UINT1 *pu1Val)
{
    UINT4               u4Val = 0;
    tSnmpMgrDbEntry     tMgrDbEntry;
    CHR1                au1NumberIdx[SNMP_MAX_OID_LENGTH];
    CHR1                aTemp[SNMP_MAX_OID_LENGTH];
    CHR1                aName[SNMP_MAX_OID_LENGTH];
    INT1                i1Index = 0;

    MEMSET (au1NumberIdx, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (aTemp, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (aName, '\0', SNMP_MAX_OID_LENGTH);

    if (pName == NULL)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (aTemp, pName, STRLEN (pName));
    while (aTemp[i1Index] != '\0')
    {
        if (aTemp[i1Index] == '.')
        {
            break;
        }
        else
        {
            aName[i1Index] = aTemp[i1Index];
            i1Index++;
        }
    }
    
    u4Val = (UINT4) SnmpONGetOidFromName (aName, (char *) &au1NumberIdx);
    if (u4Val == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid mib object name\r\n");
        return CLI_FAILURE;
    }
    if (GetMgrMbDbEntryForName((CHR1 *)&au1NumberIdx,
                   (tSnmpMgrDbEntry *)&tMgrDbEntry) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Db entry found\r\n");
        return CLI_FAILURE;
    }
    
    if (SnmpMgrSetValueForName((UINT4 *)u4IpAddr, u4Port, pCommunity, 
              (CHR1 *) pName, (tSnmpMgrDbEntry *)&tMgrDbEntry, pu1Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Db entry found\r\n");
        return CLI_FAILURE;
    }


    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : SnmpMgrCliSetValueForOid                   */
/*  Description     : This function is used to set the value   */
/*                    for the specified mib object.            */
/*  Input(s)        : pOid - OID                               */
/*                    pu1Val - value to be set                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
SnmpMgrCliSetValueForOid (tCliHandle CliHandle, UINT4 *u4IpAddr, UINT4 u4Port, 
                                       CHR1 *pCommunity, CHR1 * pOid, UINT1 *pu1Val)
{
    tSnmpMgrDbEntry     tMgrDbEntry;
 
    if (GetMgrMbDbEntryForOid(pOid, &tMgrDbEntry) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Db entry found\r\n");
        return CLI_FAILURE;
    }
    if (SnmpMgrSetValueForOid(u4IpAddr, pOid, u4Port, 
                      pCommunity, &tMgrDbEntry, pu1Val) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Set operation failed for OID\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : SnmpMgrCliGetValueForName                  */
/*  Description     : This function is used to get the         */
/*                    value for the specified mib name         */
/*  Input(s)        : pName - mib object name,                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
SnmpMgrCliGetValueForName (tCliHandle CliHandle, UINT4 *u4IpAddr, UINT4 u4Port, 
                                       CHR1 *pCommunity, CHR1 * pName, UINT4 u4PduReq)
{
    UINT4               u4Val = 0;
    tSnmpMgrDbEntry     tMgrDbEntry;
    CHR1                au1NumberIdx[SNMP_MAX_OID_LENGTH];
    CHR1                aTemp[SNMP_MAX_OID_LENGTH];
    CHR1                aName[SNMP_MAX_OID_LENGTH];
    INT1                i1Index = 0;
    UINT1               au1Output[SNMP_MAX_OID_LENGTH];
    UINT1               au1GetNextOid[SNMP_MAX_OID_LENGTH];

    MEMSET (au1NumberIdx, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1GetNextOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Output, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (aTemp, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (aName, '\0', SNMP_MAX_OID_LENGTH);

    if (pName == NULL)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (aTemp, pName, STRLEN (pName));
    while (aTemp[i1Index] != '\0')
    {
        if (aTemp[i1Index] == '.')
        {
            break;
        }
        else
        {
            aName[i1Index] = aTemp[i1Index];
            i1Index++;
        }
    }
    
    u4Val = (UINT4) SnmpONGetOidFromName (aName, (char *) &au1NumberIdx);
    if (u4Val == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid mib object name\r\n");
        return CLI_FAILURE;
    }
    if (GetMgrMbDbEntryForName((CHR1 *)&au1NumberIdx, 
                   (tSnmpMgrDbEntry *)&tMgrDbEntry) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Db entry found\r\n");
        return CLI_FAILURE;
    }
    
    if (SnmpMgrGetValueForName((UINT4 *) u4IpAddr, (CHR1 *) pName, u4Port, pCommunity, 
                (tSnmpMgrDbEntry *)&tMgrDbEntry,(UINT1 *) &au1Output, 
                                          au1GetNextOid, u4PduReq) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Get operation failed for name\r\n");
        return CLI_FAILURE;
    }
    /* If it is get-next request then next object name should be printed */
    if (u4PduReq == SNMP_PDU_TYPE_GET_NEXT_REQUEST)
    {
        if((UINT4) SnmpONGetNameFromOid ((CHR1 *) au1GetNextOid, (CHR1 *) pName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Entry not present\r\n");
            return CLI_FAILURE;
        }
    }
    CliPrintf (CliHandle, "%s =: %s\r\n", pName, au1Output);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : SnmpMgrCliGetValueForOid                 */
/*  Description     : This function is used to get the         */
/*                    value for the specified mib oid          */
/*  Input(s)        : pName - mib object name,                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
SnmpMgrCliGetValueForOid (tCliHandle CliHandle, UINT4 *u4IpAddr, UINT4 u4Port,
                                        CHR1 *pCommunity, CHR1 * pOid, UINT4 u4PduReq)
{
    tSnmpMgrDbEntry     tMgrDbEntry;
    UINT1               au1Output[SNMP_MAX_OID_LENGTH];
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    UINT1               au1GetNextOid[SNMP_MAX_OID_LENGTH];
    
    MEMSET (au1Output, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1GetNextOid, '\0', SNMP_MAX_OID_LENGTH);

    if (GetMgrMbDbEntryForOid(pOid, &tMgrDbEntry) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Db entry found\r\n");
        return CLI_FAILURE;
    }
    if (SnmpMgrGetValueForOid(u4IpAddr, pOid, u4Port, pCommunity, &tMgrDbEntry,                                                   au1Output, au1GetNextOid, u4PduReq) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Get operation for Oid failed\r\n");
        return CLI_FAILURE;
    }
    if (u4PduReq == SNMP_PDU_TYPE_GET_REQUEST)
    {
        if((UINT4) SnmpONGetNameFromOid (pOid, (char *) &au1Name) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Entry not present\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /* If it is get-next request then next object name should be printed */
        if((UINT4) SnmpONGetNameFromOid ((CHR1 *) au1GetNextOid, 
                                    (CHR1 *) &au1Name) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Entry not present\r\n");
            return CLI_FAILURE;
        }
       
    }
    
    CliPrintf (CliHandle, "%s =: %s\r\n", au1Name, au1Output);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : SnmpMgrCliNameWalk                       */
/*  Description     : This function is used to get the         */
/*  Input(s)        : pName - mib object name,                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
SnmpMgrCliNameWalk (tCliHandle CliHandle, UINT4 *u4IpAddr, UINT4 u4Port, 
                                       CHR1 *pCommunity, CHR1 * pName)
{
    UINT1               au1TempGetNextOid[SNMP_MAX_OID_LENGTH];
    UINT1               au1TempOidTwo[SNMP_MAX_OID_LENGTH];
    tSnmpMgrDbEntry     tMgrDbEntryFirst;
    tSnmpMgrDbEntry     tMgrDbEntrySecond;
    UINT4               u4Val = 0;
    tSnmpMgrDbEntry     tMgrDbEntry;
    CHR1                au1NumberIdx[SNMP_MAX_OID_LENGTH];
    CHR1                aTemp[SNMP_MAX_OID_LENGTH];
    CHR1                aName[SNMP_MAX_OID_LENGTH];
    INT1                i1Index = 0;
    UINT1               au1Output[SNMP_MAX_OID_LENGTH];
    UINT1               au1GetNextOid[SNMP_MAX_OID_LENGTH];
    UINT1               au1TempOid[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE      pOidAct;
    tSNMP_OID_TYPE      *pOid = NULL;
    tSNMP_OID_TYPE      *pOidRes = NULL;
    INT4                i4Loop = 0;
    INT4                i4Flag = 0;
    UINT4               pu4List[SNMP_MAX_OID_LENGTH];
    INT4                i4Iter = 0;
    INT4                i4BrkFlag = 0; 

    MEMSET (au1NumberIdx, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1GetNextOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1TempGetNextOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1TempOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1TempOidTwo, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1Output, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (pu4List, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (aTemp, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (aName, '\0', SNMP_MAX_OID_LENGTH);

    if (pName == NULL)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (aTemp, pName, STRLEN (pName));
    while (aTemp[i1Index] != '\0')
    {
        if (aTemp[i1Index] == '.')
        {
            break;
        }
        else
        {
            aName[i1Index] = aTemp[i1Index];
            i1Index++;
        }
    }
 
    u4Val = (UINT4) SnmpONGetOidFromName (aName, (char *) &au1NumberIdx);
    if (u4Val == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid mib object name\r\n");
        return CLI_FAILURE;
    }
    if (SnmpONGetOidFromName ((char *)pName,
                          (char *)&au1TempOid)== SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* If registered MIB object name is directly specified call 
     * get for that object */
    if (GetMgrMbDbEntryForName((CHR1 *)&au1NumberIdx, 
                   (tSnmpMgrDbEntry *)&tMgrDbEntry) != SNMP_FAILURE)
    {
        if (SnmpMgrGetValueForName((UINT4 *) u4IpAddr, (CHR1 *) pName, u4Port, pCommunity, 
                    (tSnmpMgrDbEntry *)&tMgrDbEntry,(UINT1 *) &au1Output, 
                                  au1GetNextOid, SNMP_PDU_TYPE_GET_REQUEST) != SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%s =: %s\r\n", pName, au1Output);
        }
        else
        {
            /* If get is not successful we have to try getnext for the oid
            because get will only be success for exact oid value. So copy the 
            inputed object oid value for get next */
            STRCPY (au1GetNextOid, au1TempOid);
        }
    }
    else
    {
        /* If it is a table oid then get of DB will be fail.Then we can do get-next
         * for table oid itself. It will return first valid entry of the table. 
         * So copy table oid for  get next */
        STRCPY (au1GetNextOid, au1TempOid);
    }

    pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)au1TempOid);
    for (i4Iter = 0; i4Iter <(INT4) pOid->u4_Length; i4Iter ++)
    {
        pu4List[i4Iter] = pOid->pu4_OidList[i4Iter];
    }
    pOidAct.pu4_OidList = pu4List;
    pOidAct.u4_Length = pOid->u4_Length;

    /* Gext-next for all the objet of  same table */
    while(1)
    {
        MEMSET (au1TempOidTwo, '\0', SNMP_MAX_OID_LENGTH);
        STRCPY (au1TempOidTwo, au1GetNextOid); 
        if (SnmpMgrGetValueForOid((UINT4 *) u4IpAddr, (CHR1 *) au1GetNextOid, u4Port, pCommunity, 
                    (tSnmpMgrDbEntry *)&tMgrDbEntry,(UINT1 *) &au1Output, 
                                  au1GetNextOid, SNMP_PDU_TYPE_GET_NEXT_REQUEST) == SNMP_FAILURE)
        {
            break;
        }
        else
        {/* If a table is having only one entry get-next will return same object again. So
           * exit from loop without printing if same entry is returned */

            if ((GetMgrMbDbEntryForOid((CHR1 *) au1TempOidTwo, &tMgrDbEntryFirst) != SNMP_FAILURE)           && (GetMgrMbDbEntryForOid((CHR1 *) au1GetNextOid, &tMgrDbEntrySecond) != SNMP_FAILURE)
           && i4BrkFlag != 0)
            {
                if (STRCMP (tMgrDbEntryFirst.i1ObjName, tMgrDbEntrySecond.i1ObjName)== 0)
                {
                    if( tMgrDbEntryFirst.i4NoOfIndex == 0)
                    {
                        break;
                    }
                }
            }
           
        }

        /* After each get-next one verification is required to validate that returned 
        * next object belongs to the same tree. */
        pOidRes = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)au1GetNextOid);
        i4Flag = 0;
        i4BrkFlag ++;
        for (i4Loop = 0; i4Loop<(INT4)pOidAct.u4_Length; i4Loop++)
        {
            if (pOidAct.pu4_OidList[i4Loop] != pOidRes->pu4_OidList[i4Loop])
            {
                i4Flag = 1;
                break;
            }
        }
        if (i4Flag != 0)
        {
            break;
        }
        MEMSET (au1TempGetNextOid, '\0', SNMP_MAX_OID_LENGTH);
        STRCPY (au1TempGetNextOid, au1GetNextOid);
        if((UINT4) SnmpONGetNameFromOid ((CHR1 *) au1TempGetNextOid,
                                  (CHR1 *) pName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Entry not present\r\n");
            return CLI_FAILURE;
        }
       
        CliPrintf (CliHandle, "%s =: %s\r\n", pName, au1Output);
    } 

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : SnmpMgrCliOidWalk                        */
/*  Description     : This function is used to                 */
/*                    walk for the specified mib oid           */
/*  Input(s)        : pOid - mib object name,                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
SnmpMgrCliOidWalk (tCliHandle CliHandle, UINT4 *u4IpAddr, UINT4 u4Port,
                                 CHR1 *pCommunity, CHR1 * pOid)
{
    tSnmpMgrDbEntry     tMgrDbEntry;
    tSnmpMgrDbEntry     tMgrDbEntryFirst;
    tSnmpMgrDbEntry     tMgrDbEntrySecond;
    UINT1               au1Output[SNMP_MAX_OID_LENGTH];
    UINT1               au1Name[SNMP_MAX_OID_LENGTH];
    UINT1               au1GetNextOid[SNMP_MAX_OID_LENGTH];
    UINT1               au1TempGetNextOid[SNMP_MAX_OID_LENGTH];
    UINT1               au1TempOid[SNMP_MAX_OID_LENGTH];
    UINT1               au1TempOidTwo[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE      pOidAct;
    tSNMP_OID_TYPE      *pOidTmp = NULL;
    tSNMP_OID_TYPE      *pOidRes = NULL;
    INT4                i4Loop = 0;
    INT4                i4Flag = 0;
    UINT4               pu4List[SNMP_MAX_OID_LENGTH];
    INT4                i4Iter = 0;
    INT4                i4BrkFlag = 0; 
    MEMSET (au1Output, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1GetNextOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1TempGetNextOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1TempOid, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (au1TempOidTwo, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (pu4List, 0, SNMP_MAX_OID_LENGTH);
     
    STRCPY (au1TempOid, pOid);

    /* If registered MIB object name is directly specified call 
     * get for that object */
    if (GetMgrMbDbEntryForOid(pOid, &tMgrDbEntry) != SNMP_FAILURE)
    {
        if (SnmpMgrGetValueForOid((UINT4 *) u4IpAddr, (CHR1 *) pOid, u4Port, pCommunity, 
                    (tSnmpMgrDbEntry *)&tMgrDbEntry,(UINT1 *) &au1Output, 
                                  au1GetNextOid, SNMP_PDU_TYPE_GET_REQUEST) != SNMP_FAILURE)
        {
            if((UINT4) SnmpONGetNameFromOid (pOid, (char *) au1Name) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Entry not present\r\n");
                return CLI_FAILURE;
            }
    
            CliPrintf (CliHandle, "%s =: %s\r\n", au1Name, au1Output);
        }
        else
        {   /* If get is not successful we have to try getnext for the oid
             because get will only be success for exact oid value. So copy the 
            inputed object oid value for get next */
            STRCPY (au1GetNextOid, pOid);
        }
    }
    else
    {   /* If it is a table oid then get of DB will be fail.Then we can do get-next
         * for table oid itself. It will return first valid entry of the table. 
         * So copy table oid for  get next */
        STRCPY (au1GetNextOid, pOid);
    }

    pOidTmp = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)au1TempOid);
    for (i4Iter = 0; i4Iter <(INT4) pOidTmp->u4_Length; i4Iter ++)
    {
        pu4List[i4Iter] = pOidTmp->pu4_OidList[i4Iter];
    }
    pOidAct.pu4_OidList = pu4List;
    pOidAct.u4_Length = pOidTmp->u4_Length;

    /* Gext-next for all the objet of  same table */
    while(1)
    {
        MEMSET (au1TempOidTwo, '\0', SNMP_MAX_OID_LENGTH);
        STRCPY (au1TempOidTwo, au1GetNextOid); 
        if (SnmpMgrGetValueForOid((UINT4 *) u4IpAddr, (CHR1 *) au1GetNextOid, u4Port, pCommunity, 
                    (tSnmpMgrDbEntry *)&tMgrDbEntry,(UINT1 *) &au1Output, 
                                  au1GetNextOid, SNMP_PDU_TYPE_GET_NEXT_REQUEST) == SNMP_FAILURE)
        {
            break;
        }
        else
        { /* If a table is having only one entry get-next will return same object again. So
           * exit from loop without printing if same entry is returned */
            if ((GetMgrMbDbEntryForOid((CHR1 *) au1TempOidTwo, &tMgrDbEntryFirst) != SNMP_FAILURE)           && (GetMgrMbDbEntryForOid((CHR1 *) au1GetNextOid, &tMgrDbEntrySecond) != SNMP_FAILURE)
           && i4BrkFlag != 0)
            {
                if (STRCMP (tMgrDbEntryFirst.i1ObjName, tMgrDbEntrySecond.i1ObjName)== 0)
                {
                    if( tMgrDbEntryFirst.i4NoOfIndex == 0)
                    {
                        break;
                    }
                }
            }
           
        }

        /* After each get-next one verification is required to validate that returned 
        * next object belongs to the same tree. */
        pOidRes = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)au1GetNextOid);
        i4Flag = 0;
        i4BrkFlag ++;
        for (i4Loop = 0; i4Loop<(INT4)pOidAct.u4_Length; i4Loop++)
        {
            if (pOidAct.pu4_OidList[i4Loop] != pOidRes->pu4_OidList[i4Loop])
            {
                i4Flag = 1;
                break;
            }
        }
        if (i4Flag != 0)
        {
            break;
        }
        MEMSET (au1TempGetNextOid, '\0', SNMP_MAX_OID_LENGTH);
        STRCPY (au1TempGetNextOid, au1GetNextOid);
        if((UINT4) SnmpONGetNameFromOid ((CHR1 *)au1TempGetNextOid, 
                               (CHR1 *) &au1Name) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Entry not present\r\n");
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "%s =: %s\r\n", au1Name, au1Output);
    } 

    return CLI_SUCCESS;
}

#endif /* __SMGR_CLI_C__ */
