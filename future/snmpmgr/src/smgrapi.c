/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved 
 *
 * $Id: smgrapi.c,v 1.1 2017/08/24 12:03:41 siva Exp $
 *
 * Description: Routines for snmp manager
 *******************************************************************/

#ifndef __SMGR_API_C__
#define __SMGR_API_C__

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : smgrapi.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : ISS TEAM                                         |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : SNMPMGR                                          |
 * |                                                                           |
 * |  MODULE NAME           : SNMP MANAGER                                     |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : File contains snmp manager handling routines.    |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */

#include "cli.h"
#include "smgrinc.h"
#include "smgrutil.h"
#include "fssocket.h"
#include <sys/types.h>
#include <stdarg.h>

extern UINT1               gau1OutDat[MAX_PKT_LENGTH];
extern UINT1               gau1RevDat[MAX_PKT_LENGTH];
extern UINT1               gau1CommunityData[SNMP_MAX_OCTETSTRING_SIZE];
extern tSNMP_OCTET_STRING_TYPE Community;

INT4 gi4Sock = SNMP_ZERO;

#define MgmtUnLock() {}
#define MgmtLock()   {}   

/************************************************************************
 *  Function Name   : SnmpSetRequest
 *  Description     : This function send request for set operation and 
 *                    process received response..
 *  Input           : u1Oid - OID of the Object to be set.
 *                    pMultiIndex - Contains index values.
 *                    pMultiData - Contains value to be set.
 *  Output          : NONE
 *  Returns         : SNMP_SUCCESS / SNMP_FAILURE
 ************************************************************************/

INT1 SnmpSetRequest(UINT1 *u1Oid,tSnmpIndex *pMultiIndex,tRetVal *pMultiData)
{   
    fd_set                   Socks;
    struct timeval           tTimeout = {SNMP_MGR_TIMEOUT,0};
    INT4                     i4RetVal = SNMP_ZERO;
    CHR1                     *pu1IpAddr; 
    UINT1                    *pu1_OctetList = NULL;
    tSNMP_NORMAL_PDU         *pPtr = NULL;  
    INT2                     i2_PduType = SNMP_ZERO;
    VOID                     *pDecodedPtr = NULL;
    INT4                     i4PktSize = SNMP_ZERO;
    UINT1                    *pu1PktArray = NULL;
    tSNMP_VAR_BIND           *pVbList = NULL;
    tSNMP_COUNTER64_TYPE     SnmpCounter64Type;
    tSNMP_OID_TYPE           *pOid = NULL;
    tSNMP_NORMAL_PDU         pPdu;
    UINT4                    *pu4Temp = NULL;    
    UINT4                    u4Count = 0; 
    INT4                     i4Result = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE  pCommunityStr;
    tSNMP_OCTET_STRING_TYPE  pParseIpAddr;
    INT4                     i4OutLen;
    UINT1                    **pOut;
    UINT1                    *pu1SendPkt = NULL;
    struct sockaddr_in	     SnmpMgrAddr;
    UINT4                    u4Len = SNMP_ZERO;
    CHR1                     *i1Temp;
    INT4                     Socket = SNMP_ZERO;
    UINT1                    *pu1TempOid;

    /* Memory allocation */
    pu1TempOid = (UINT1 *)calloc(SNMP_MAX_OID_LENGTH, sizeof(UINT1)); 
    i1Temp = (CHR1 *)calloc(SNMP_MAX_OID_LENGTH, sizeof(CHR1));
    pu4Temp = (UINT4 *)calloc(SNMP_MAX_OCTETSTRING_SIZE , sizeof(UINT4));
    pu1SendPkt = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1)); 
    pu1PktArray = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1)); 
    pu1_OctetList = (UINT1 *)calloc(SNMP_MAX_OCTETSTRING_SIZE, sizeof(UINT1)); 
    pParseIpAddr.pu1_OctetList = (UINT1 *)calloc(SNMP_MGR_MAX_IP_ADD_LEN, sizeof(UINT1)); 
    pu1IpAddr = (CHR1 *)calloc(SNMP_MGR_MAX_IP_ADD_LEN, sizeof(CHR1)); 
  
    MEMSET (pu1TempOid, '\0', SNMP_MAX_OID_LENGTH * sizeof (UINT1));
    MEMSET (i1Temp, '\0', SNMP_MAX_OID_LENGTH * sizeof (INT1));
    MEMSET (pu4Temp, 0, SNMP_MAX_OCTETSTRING_SIZE * sizeof (UINT4));
    MEMSET (pu1SendPkt,'\0', MAX_PKT_LENGTH * sizeof (UINT1));
    MEMSET (pu1PktArray,'\0',MAX_PKT_LENGTH * sizeof (UINT1));
    MEMSET (pu1_OctetList,'\0', SNMP_MAX_OCTETSTRING_SIZE * sizeof (UINT1));
    MEMSET (pParseIpAddr.pu1_OctetList, '\0', SNMP_MGR_MAX_IP_ADD_LEN * sizeof (UINT1));
    MEMSET (pu1IpAddr, '\0', SNMP_MGR_MAX_IP_ADD_LEN * sizeof (UINT1));
   
    MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
    SnmpMgrAddr.sin_family = AF_INET;
    SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
    SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SnmpMgrAddr);
    
    FD_ZERO(&Socks);
 
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    pOut = (UINT1 **)&pu1SendPkt;
    i4OutLen = 0;

    STRCPY(pu1TempOid,u1Oid);

    pCommunityStr.pu1_OctetList = gau1CommunityData;
    MEMSET (gau1CommunityData, SNMP_ZERO, sizeof (gau1CommunityData));
    STRCPY(pCommunityStr.pu1_OctetList,"PUBLIC");
    pCommunityStr.i4_Length = SNMP_MGR_PUBLIC_COMMUNITY_LEN;

    /* Construct full oid from actual oid and mutiindex */  
    if(pMultiIndex->u4No == 0)
    {
        SPRINTF((CHR1 *)pu1TempOid,"%s.0",pu1TempOid);
    }
    else
    {
        for (u4Count = 0; u4Count<pMultiIndex->u4No; u4Count++)
        {
            switch (pMultiIndex->pIndex[u4Count].i2_DataType)
            {
                case SNMP_DATA_TYPE_INTEGER32:                
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",
                           pu1TempOid,pMultiIndex->pIndex[u4Count].i4_SLongValue);
                    break;
                case SNMP_DATA_TYPE_UNSIGNED32:                
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%u",
                           pu1TempOid,pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                    CLI_CONVERT_IPADDR_TO_STR(i1Temp, pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%s",pu1TempOid,i1Temp);
                    break;
                case SNMP_DATA_TYPE_OCTET_PRIM:
                    ConvertStringToDottedString (pu1_OctetList,
                                    pMultiIndex->pIndex[u4Count].pOctetStrValue->pu1_OctetList);
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d%s",pu1TempOid,
                    pMultiIndex->pIndex[u4Count].pOctetStrValue->i4_Length, pu1_OctetList);
                    break;
                case SNMP_DATA_TYPE_COUNTER32:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid,
                                  pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_COUNTER64:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_TIME_TICKS:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
		default:
                    break;
            }
        }
    }
    pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)pu1TempOid);
    
    /* Form Varbind */
    switch(pMultiData->i2_DataType)
    {
       case SNMP_DATA_TYPE_INTEGER32:
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_INTEGER32,
                                  0, pMultiData->i4_SLongValue, NULL, NULL, 
                                  SnmpCounter64Type);
           break;
       case SNMP_DATA_TYPE_UNSIGNED32:
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_UNSIGNED32, 
                                  pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                  SnmpCounter64Type);
          break;
       case SNMP_DATA_TYPE_IP_ADDR_PRIM:
           CLI_CONVERT_IPADDR_TO_STR(pu1IpAddr,
                                  pMultiData->u4_ULongValue);
           SnmpMgrConvertStringToOctet (&pParseIpAddr, (UINT1 *)pu1IpAddr);
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_IP_ADDR_PRIM, 
                                  0, 0, &pParseIpAddr, NULL, 
                                  SnmpCounter64Type);
          break;
       case SNMP_DATA_TYPE_OCTET_PRIM:
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_OCTET_PRIM, 
                                  0, 0, pMultiData->pOctetStrValue, NULL, 
                                  SnmpCounter64Type);
          break;
       case SNMP_DATA_TYPE_COUNTER32:
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_COUNTER32, 
                                  pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                  SnmpCounter64Type);
          break;
       case SNMP_DATA_TYPE_COUNTER64:
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_COUNTER64, 
                                  0, 0, NULL, NULL, 
                                  SnmpCounter64Type);
          break;
       case SNMP_DATA_TYPE_TIME_TICKS:
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_TIME_TICKS, 
                                  pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                  SnmpCounter64Type);
          break;
       default:
          break;
    }
    
    /* Fill the PDU */
    pPdu.pVarBindList = pVbList;
    pPdu.pVarBindEnd = pVbList;
    pPdu.u4_Version = VERSION1;
    pPdu.i4_ErrorStatus = SNMP_ZERO;
    pPdu.i4_ErrorIndex = SNMP_ZERO;
    pPdu.i4_NonRepeaters = SNMP_ZERO;
    pPdu.i4_MaxRepetitions = SNMP_ZERO;
    pPdu.i2Padding = SNMP_ZERO;
    pPdu.pCommunityStr = &pCommunityStr;
    pPdu.i2_PduType = SNMP_PDU_TYPE_SET_REQUEST;

    /* Encode the packet */
    i4Result = SNMPMGREncodeGetPacket(&pPdu, pOut, &i4OutLen,
                                (UINT4)SNMP_PDU_TYPE_SET_REQUEST);
    if (i4Result == SNMP_NOT_OK)
    {
        i4Result = SNMP_FAILURE;
    }
    else
    {
        i4Result = SNMP_SUCCESS;
    }
    
    /* Send the encoded packet to the destined 
    *  SNMP manager for set operation */
    if (pu1SendPkt != NULL)
    {
       if ( gi4Sock <= SNMP_ZERO)
       {
       if (SnmpMgrSockInit(&gi4Sock) == SNMP_FAILURE)
       {
           return SNMP_FAILURE;
       }
       }
       Socket = gi4Sock;

       SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL(gu4AgentAddr);
       SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_PORT);

       /* Unlocking management lock as it will be automatically taken by SNMP */
       MgmtUnLock();

       if (sendto (Socket, pu1SendPkt, i4OutLen, SNMP_ZERO,
                  (struct sockaddr *) &SnmpMgrAddr,
                  (INT4) u4Len) == SNMP_NOT_OK)
       {

          /* Get the lock back */
          printf("\n Sendto failed in setRequest\n");
          MgmtLock();
          return SNMP_FAILURE;
          
       }
       /* Handling for response packet */
       MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
       SnmpMgrAddr.sin_family = AF_INET;
       SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
       SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
       u4Len = sizeof (SnmpMgrAddr);

        /* Call select() for setting timeout */
        FD_SET(Socket,&Socks);
        i4RetVal = select (Socket +1, &Socks, NULL, NULL, &tTimeout);

        /* Get the lock back */
        MgmtLock();

        if(i4RetVal == -1)
        {
            return SNMP_FAILURE;
        }
        else if (i4RetVal)
        { 
            if ((i4PktSize = recvfrom (Socket, pu1PktArray,
                                    MAX_SNMP_PKT_LEN, 0,
                                    (struct sockaddr *) &SnmpMgrAddr,
                                    (socklen_t *) & u4Len)) > 0)
            {
               /* Received responce packet. Now decode the packet */
               pDecodedPtr =
                    (VOID *) SNMPMGRDecodePacket (pu1PktArray, 
                                   (INT4) i4PktSize, &i2_PduType);
               if (pDecodedPtr == NULL)
               {
                   return SNMP_FAILURE;
               }

               pPtr = (tSNMP_NORMAL_PDU *) pDecodedPtr;
               if (pPtr == NULL)
               {
                   return SNMP_FAILURE;
               }
            
               if (pPtr->i4_ErrorStatus == SNMP_ERR_NO_ERROR)
               {
                   printf("\nSet operation success for %s\n", pu1TempOid);
               }
               else
               { 
                    printf("\nSet opration failed for %s %s \n", u1Oid, pu1TempOid);
                   return SNMP_FAILURE;
               }
                   
           } 
       }
       else
       {
           printf("\nResponse timeout for %s\n", pu1TempOid);
           return SNMP_FAILURE; 
       }
    }

    return SNMP_SUCCESS; 
}

/************************************************************************
 *  Function Name   : SnmpGetRequest
 *  Description     : This function send request for Get operation and 
 *                    process received response.
 *  Input           : u1Oid - OID of the Object to be set.
 *                    pMultiIndex - Contains index values.
 *                    pMultiData - Empty container for Get values.
 *  Output          : NONE
 *  Returns         : SNMP_SUCCESS / SNMP_FAILURE
 ************************************************************************/
INT1 SnmpGetRequest(UINT1 *u1Oid,tSnmpIndex *pMultiIndex,
                             tRetVal *pMultiData,INT4 i4Type,va_list args)
{   
    tSNMP_OCTET_STRING_TYPE  *pRcvOctate = NULL;
    fd_set                   Socks; 
    struct timeval           tTimeout={5,0};
    CHR1                     *pu1IpAddr; 
    INT4                     *i4_SLongValue = NULL;
    UINT4                    *u4_SLongValue = NULL;
    UINT1                    *pu1_OctetList = NULL;
    va_list                  ap;
    tSNMP_NORMAL_PDU         *pPtr = NULL;  
    INT2                     i2_PduType = SNMP_ZERO;
    VOID                     *pDecodedPtr = NULL;
    INT4                     i4PktSize = SNMP_ZERO;
    UINT1                    *pu1PktArray = NULL;
    tSNMP_VAR_BIND           *pVbList = NULL;
    tSNMP_COUNTER64_TYPE     SnmpCounter64Type;
    tSNMP_OID_TYPE           *pOid = NULL;
    tSNMP_NORMAL_PDU         pPdu;
    UINT4                    *pu4Temp = NULL;    
    UINT4                    u4Count = 0; 
    INT4                     i4Result = SNMP_SUCCESS;
    tSNMP_OCTET_STRING_TYPE  pCommunityStr;
    tSNMP_OCTET_STRING_TYPE  pParseIpAddr;
    INT4                     i4OutLen;
    UINT1                    **pOut;
    UINT1                    *pu1SendPkt = NULL;
    struct sockaddr_in       SnmpMgrAddr;
    UINT4                    u4Len = SNMP_ZERO;
    CHR1                     *i1Temp;
    INT4                     Socket = SNMP_ZERO;
    UINT1                    *pu1TempOid;
    tSNMP_MULTI_DATA_TYPE    *pGetMultiData = NULL;
    INT4                     i4RetVal = 0;

     
    /* Memory allocation */
    pu1TempOid = (UINT1 *)calloc(SNMP_MAX_OID_LENGTH, sizeof(UINT1)); 
    i1Temp = (CHR1 *)calloc(SNMP_MAX_OID_LENGTH, sizeof(CHR1));
    pu4Temp = (UINT4 *)calloc(SNMP_MAX_OCTETSTRING_SIZE , sizeof(UINT4));
    pu1SendPkt = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1)); 
    pu1PktArray = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1)); 
    pu1_OctetList = (UINT1 *)calloc(SNMP_MAX_OCTETSTRING_SIZE, sizeof(UINT1)); 
    pParseIpAddr.pu1_OctetList = (UINT1 *)calloc(SNMP_MGR_MAX_IP_ADD_LEN, sizeof(UINT1)); 
    pu1IpAddr = (CHR1 *)calloc(SNMP_MGR_MAX_IP_ADD_LEN, sizeof(CHR1)); 
  
    MEMSET (pu1TempOid, '\0', SNMP_MAX_OID_LENGTH * sizeof (UINT1));
    MEMSET (i1Temp, '\0', SNMP_MAX_OID_LENGTH * sizeof (INT1));
    MEMSET (pu4Temp, 0, SNMP_MAX_OCTETSTRING_SIZE * sizeof (UINT4));
    MEMSET (pu1SendPkt,'\0', MAX_PKT_LENGTH * sizeof (UINT1));
    MEMSET (pu1PktArray,'\0',MAX_PKT_LENGTH * sizeof (UINT1));
    MEMSET (pu1_OctetList,'\0', SNMP_MAX_OCTETSTRING_SIZE * sizeof (UINT1));
    MEMSET (pParseIpAddr.pu1_OctetList, '\0', SNMP_MGR_MAX_IP_ADD_LEN * sizeof (UINT1));
    MEMSET (pu1IpAddr, '\0', SNMP_MGR_MAX_IP_ADD_LEN * sizeof (UINT1));

   
    MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
    SnmpMgrAddr.sin_family = AF_INET;
    SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
    SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SnmpMgrAddr);
 
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;
  
    FD_ZERO(&Socks); 

    pOut = (UINT1 **)&pu1SendPkt;
    i4OutLen = 0;

    STRCPY(pu1TempOid,u1Oid);

    pCommunityStr.pu1_OctetList = gau1CommunityData;
    MEMSET (gau1CommunityData, SNMP_ZERO, sizeof (gau1CommunityData));
    STRCPY(pCommunityStr.pu1_OctetList,"PUBLIC");
    pCommunityStr.i4_Length = SNMP_MGR_PUBLIC_COMMUNITY_LEN;

    /* Construct full oid from actual oid and mutiindex */  
    if(pMultiIndex->u4No == 0)
    {
        SPRINTF((CHR1 *)pu1TempOid,"%s.0",pu1TempOid);
    }
    else
    {
        for (u4Count = 0; u4Count<pMultiIndex->u4No; u4Count++)
        {
            switch (pMultiIndex->pIndex[u4Count].i2_DataType)
            {
                case SNMP_DATA_TYPE_INTEGER32:                
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",
                          pu1TempOid,pMultiIndex->pIndex[u4Count].i4_SLongValue);
                    break;
                case SNMP_DATA_TYPE_UNSIGNED32:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                      pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                    CLI_CONVERT_IPADDR_TO_STR(i1Temp, pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%s",pu1TempOid,i1Temp);
                    break;
                case SNMP_DATA_TYPE_OCTET_PRIM:
                    ConvertStringToDottedString (pu1_OctetList,
                                    pMultiIndex->pIndex[u4Count].pOctetStrValue->pu1_OctetList);
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d%s",
                              pu1TempOid, pMultiIndex->pIndex[u4Count].pOctetStrValue->i4_Length, 
                              pu1_OctetList);
                    break;
                case SNMP_DATA_TYPE_COUNTER32:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                    pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_COUNTER64:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                    pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_TIME_TICKS:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                    pMultiIndex->pIndex[u4Count].u4_ULongValue);
                    break;
		default:
                    break;
            }  
        }
    }
    pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)pu1TempOid);
    
    /* Form Varbind */
    switch(pMultiData->i2_DataType)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_INTEGER32,
                                   0, pMultiData->i4_SLongValue, NULL, NULL, 
                                   SnmpCounter64Type);
            break;
        case SNMP_DATA_TYPE_UNSIGNED32:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_UNSIGNED32, 
                                   pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
           CLI_CONVERT_IPADDR_TO_STR(pu1IpAddr,
                                  pMultiData->u4_ULongValue);
           SnmpMgrConvertStringToOctet (&pParseIpAddr, (UINT1 *) pu1IpAddr);
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_IP_ADDR_PRIM, 
                                  0, 0, &pParseIpAddr, NULL, 
                                  SnmpCounter64Type);
          break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_OCTET_PRIM, 
                                   0, 0, pMultiData->pOctetStrValue, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_COUNTER32:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_COUNTER, 
                                   pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_COUNTER64:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_COUNTER64, 
                                   0, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_TIME_TICKS:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_TIME_TICKS, 
                                   pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;  
	default:
           break;  
    }
    
    /* Fill the PDU */
    pPdu.pVarBindList = pVbList;
    pPdu.pVarBindEnd = pVbList;
    pPdu.u4_Version = VERSION1;
    pPdu.i4_ErrorStatus = SNMP_ZERO;
    pPdu.i4_ErrorIndex = SNMP_ZERO;
    pPdu.i4_NonRepeaters = SNMP_ZERO;
    pPdu.i4_MaxRepetitions = SNMP_ZERO;
    pPdu.i2Padding = SNMP_ZERO;
    pPdu.pCommunityStr = &pCommunityStr;
    pPdu.i2_PduType = SNMP_PDU_TYPE_GET_REQUEST;

    /* Encode the packet */
    i4Result = SNMPMGREncodeGetPacket(&pPdu, pOut, &i4OutLen,
                                (UINT4)SNMP_PDU_TYPE_GET_REQUEST);
    if (i4Result == SNMP_NOT_OK)
    {
        i4Result = SNMP_FAILURE;
    }
    else
    {
        i4Result = SNMP_SUCCESS;
    }

    
    /* Send the encoded packet to the destined 
    *  SNMP manager for set operation */
    if (pu1SendPkt != NULL)
    {
        if ( gi4Sock <= SNMP_ZERO)
        {
        if (SnmpMgrSockInit(&gi4Sock) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        }
        Socket = gi4Sock;

        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (gu4AgentAddr);
        SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_PORT);

        /* Get the lock back */
        MgmtUnLock();

        if (sendto (Socket, pu1SendPkt, i4OutLen, SNMP_ZERO,
                   (struct sockaddr *) &SnmpMgrAddr,
                   (INT4) u4Len) == SNMP_NOT_OK)
        {  
           /* Get the lock back */
           MgmtLock(); 
           return SNMP_FAILURE; 
        }
         
        /* Handling for response packet */
        MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
        SnmpMgrAddr.sin_family = AF_INET;
        SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
        u4Len = sizeof (SnmpMgrAddr);
 
        /* Call select() for setting timeout */
 
        FD_SET(Socket,&Socks);
        i4RetVal = select (Socket +1, &Socks, NULL, NULL, &tTimeout);

        /* Get the lock back */
        MgmtLock();

        if(i4RetVal == -1)
        {
            return SNMP_FAILURE;
        }
        else if (i4RetVal)
        {
           if ((i4PktSize = recvfrom (Socket, pu1PktArray,
                                   MAX_SNMP_PKT_LEN, 0,
                                   (struct sockaddr *) &SnmpMgrAddr,
                                   (socklen_t *) & u4Len)) > 0)
           {
                  /* Received responce packet. Now decode the packet */
                  pDecodedPtr =
                       (VOID *) SNMPMGRDecodePacket (pu1PktArray, 
                                      (INT4) i4PktSize, &i2_PduType);
                  if (pDecodedPtr == NULL)
                  {
                      return SNMP_FAILURE;
                  }
 
                  pPtr = (tSNMP_NORMAL_PDU *) pDecodedPtr;
                  if (pPtr == NULL)
                  {
                      return SNMP_FAILURE;
                  }
               
                  if (pPtr->i4_ErrorStatus != SNMP_ERR_NO_ERROR)
                  {
                      return SNMP_FAILURE;
                  }
                  else
                  {
                  /* Copy the value form received response data to call routine
                   * variable. */ 
                  pGetMultiData = &pPtr->pVarBindList->ObjValue;
                  __va_copy(ap, args);
                  switch(i4Type)
                  {
                      case SNMP_DATA_TYPE_INTEGER32:
                          i4_SLongValue = va_arg(ap,INT4*);
                          *i4_SLongValue = pGetMultiData->i4_SLongValue;
                          break;
                      case SNMP_DATA_TYPE_UNSIGNED32:
                          u4_SLongValue = va_arg(ap,UINT4*);
                          *u4_SLongValue = pGetMultiData->u4_ULongValue;
                          break;
                      case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                          u4_SLongValue = va_arg(ap,UINT4*);
                          *u4_SLongValue = pGetMultiData->u4_ULongValue;
                          break;
                      case SNMP_DATA_TYPE_OCTET_PRIM:
                          pRcvOctate = va_arg(ap,tSNMP_OCTET_STRING_TYPE *);
                          *pRcvOctate = *pGetMultiData->pOctetStrValue;
                          break;
                      case SNMP_DATA_TYPE_COUNTER32:
                          u4_SLongValue = va_arg(ap,UINT4*);
                          *u4_SLongValue = pGetMultiData->u4_ULongValue;
                          break;
                      case SNMP_DATA_TYPE_COUNTER64:
                          u4_SLongValue = va_arg(ap,UINT4*);
                          *u4_SLongValue = pGetMultiData->u4_ULongValue;
                          break;
                      case SNMP_DATA_TYPE_TIME_TICKS:
                          u4_SLongValue = va_arg(ap,UINT4*);
                          *u4_SLongValue = pGetMultiData->u4_ULongValue;
                          break;
		      default:
			  break;  
                  }     
                  }
           } 
        }
        else
        { 
            return SNMP_FAILURE; 
        } 
     }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpGetFirstIndex
 *  Description     : This function send request for getting first index
 *                    and process the received response.
 *  Input           : u1Oid - OID of the Object to be set.
 *                    i4IndexNo - No of index.
 *  Output          : NONE
 *  Returns         : SNMP_SUCCESS / SNMP_FAILURE
 ************************************************************************/
INT1
SnmpGetFirstIndex(UINT1 *u1Oid, INT4 i4IndexNo, va_list args)
{
    tSNMP_OCTET_STRING_TYPE  *pRcvOctate = NULL;
    fd_set                   Socks;
    struct timeval           tTimeout={SNMP_MGR_TIMEOUT,0};
    INT4                     i4RetVal;
    INT4                     i4Loop = SNMP_ZERO;
    INT4                     i4Flag = SNMP_ZERO;
    tMbDbEntry               pDbEntry;
    UINT1                    pu1IndexType[SNMP_MAX_INDICES_2];
    UINT1                    *pu1TempIndexType;
    tSnmpIndex               pMultiIndex;
    tSnmpIndex               *pParseMultiIndex = NULL;
    tSNMP_OCTET_STRING_TYPE  pOctateSecond[SNMP_MAX_INDICES_2];
    tSNMP_OID_TYPE           pOidSecond[SNMP_MAX_INDICES_2];
    tSNMP_MULTI_DATA_TYPE    pIndex[SNMP_MAX_INDICES_2];
    UINT1                    *pu1PktArray = NULL;
    tSNMP_OID_TYPE           *pOid = NULL; 
    tSNMP_OID_TYPE           tResOid; 
    INT4                     i4PktSize = SNMP_ZERO;
    tSNMP_VAR_BIND           *pVarBindPtr = NULL;
    tSNMP_NORMAL_PDU         pPdu; 
    tSNMP_OCTET_STRING_TYPE  pCommunityStr;
    INT4                     i4OutLen = SNMP_ZERO;
    UINT1                    **pOut;
    UINT1                    *pu1SendPkt = NULL;
    struct sockaddr_in       SnmpMgrAddr;
    INT4                     Socket = SNMP_ZERO;
    UINT4                    u4Len = SNMP_ZERO;
    INT4                     i4Result = SNMP_SUCCESS;
    tSNMP_COUNTER64_TYPE     SnmpCounter64Type;
    va_list                  ap;
    tSNMP_NORMAL_PDU         *pPtr = NULL;
    VOID                     *pDecodedPtr = NULL;
    INT2                     i2_PduType = SNMP_ZERO;
    INT4                     *i4_SLongValue = NULL;
    UINT4                    *u4_SLongValue = NULL;
    UINT1                    *pu1_OctetList = NULL;
    INT4                     i4Type = SNMP_ZERO;
    INT4                     i4Count = SNMP_ZERO;
    UINT4                     u4Count = SNMP_ZERO;    

    /* Memory allocation */
    pu1_OctetList = (UINT1 *)calloc(SNMP_MAX_OCTETSTRING_SIZE, sizeof(UINT1));
    pu1SendPkt = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1));
    pu1PktArray = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1));

    MEMSET (pu1SendPkt, '\0', MAX_PKT_LENGTH * sizeof(UINT1));
    MEMSET (pu1IndexType, '\0', SNMP_MAX_INDICES_2);
    MEMSET (pu1PktArray,'\0', MAX_PKT_LENGTH * sizeof (UINT1));
    MEMSET (pu1_OctetList, '\0', SNMP_MAX_OCTETSTRING_SIZE * sizeof(UINT1));

    for(u4Count = SNMP_ZERO; u4Count<SNMP_MAX_INDICES_2; u4Count++)
    {
        pOctateSecond[u4Count].pu1_OctetList = 
                    (UINT1 *)calloc(SNMP_MAX_OCTETSTRING_SIZE ,sizeof(UINT1));
        MEMSET (pOctateSecond[u4Count].pu1_OctetList, '\0', SNMP_MAX_OCTETSTRING_SIZE);
        pIndex[u4Count].pOctetStrValue = &pOctateSecond[u4Count];
        pIndex[u4Count].pOidValue = &pOidSecond[u4Count];
    }

    pMultiIndex.pIndex = (tSNMP_MULTI_DATA_TYPE *)&pIndex;
    pParseMultiIndex = &pMultiIndex;
    pOut = &pu1SendPkt;
    pDbEntry.pu1IndexType = (UINT1 *)&pu1IndexType;
    i4OutLen = 0;

    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    FD_ZERO(&Socks);
	
    MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
    SnmpMgrAddr.sin_family = AF_INET;
    SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
    SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SnmpMgrAddr);

    pCommunityStr.pu1_OctetList = gau1CommunityData;
    MEMSET (gau1CommunityData, SNMP_ZERO, sizeof (gau1CommunityData));
    STRCPY(pCommunityStr.pu1_OctetList,"PUBLIC");
    pCommunityStr.i4_Length = SNMP_MGR_PUBLIC_COMMUNITY_LEN;

    pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)u1Oid);

    pVarBindPtr = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_NULL, 
                                  0, 0, NULL, NULL, 
                                  SnmpCounter64Type);
    /* Fill the PDU */
    pPdu.pVarBindList = pVarBindPtr;
    pPdu.pVarBindEnd = pVarBindPtr;
    pPdu.u4_Version = VERSION1;
    pPdu.i4_ErrorStatus = SNMP_ZERO;
    pPdu.i4_ErrorIndex = SNMP_ZERO;
    pPdu.i4_NonRepeaters = SNMP_ZERO;
    pPdu.i4_MaxRepetitions = SNMP_ZERO;
    pPdu.i2Padding = SNMP_ZERO;
    pPdu.pCommunityStr = &pCommunityStr;
    pPdu.i2_PduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;

    /* Encode the packet */
    i4Result = SNMPMGREncodeGetPacket(&pPdu, pOut, &i4OutLen,
                                (UINT4)SNMP_PDU_TYPE_GET_NEXT_REQUEST);
    if (i4Result == SNMP_NOT_OK)
    {
        i4Result = SNMP_FAILURE;
    }
    else
    {
        i4Result = SNMP_SUCCESS;
    }
    
    /* Send the encoded packet to the destined 
    *  SNMP manager for set operation */
    if (pu1SendPkt != NULL)
    {
        if ( gi4Sock <= SNMP_ZERO)
        {
        if (SnmpMgrSockInit(&gi4Sock) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        }
        Socket = gi4Sock;

        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL(gu4AgentAddr);
	SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_PORT);

        /* Unlocking management lock as it will be automatically taken by SNMP */
        MgmtUnLock(); 

        if (sendto (Socket, pu1SendPkt, i4OutLen, SNMP_ZERO,
                   (struct sockaddr *) &SnmpMgrAddr,
                   (INT4) u4Len) == SNMP_NOT_OK)
        {
           MgmtLock();
           return SNMP_FAILURE;       
        }
     
        /* Handling for response packet */
        MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
        SnmpMgrAddr.sin_family = AF_INET;
        SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
        u4Len = sizeof (SnmpMgrAddr);
        /* Call select() for setting timeout */

        FD_SET(Socket,&Socks);
        i4RetVal = select (Socket +1, &Socks, NULL, NULL, &tTimeout);

	/* Get the lock back */
        MgmtLock();

        if(i4RetVal == -1)
        {
            return SNMP_FAILURE;
        }
        else if (i4RetVal)
        { 
            if ((i4PktSize = recvfrom (Socket, pu1PktArray,
                                    MAX_SNMP_PKT_LEN, 0,
                                    (struct sockaddr *) &SnmpMgrAddr,
                                    (socklen_t *) & u4Len)) > 0)
            {
                /* Received responce packet. Now decode the packet */
                pDecodedPtr =
                     (VOID *) SNMPMGRDecodePacket (pu1PktArray, 
                                    (INT4) i4PktSize, &i2_PduType);
                if (pDecodedPtr == NULL)
                {
                    return SNMP_FAILURE;
                }

                pPtr = (tSNMP_NORMAL_PDU *) pDecodedPtr;
                if (pPtr == NULL)
                {
                    return SNMP_FAILURE;
                }
                
                if (pPtr->i4_ErrorStatus != SNMP_ERR_NO_ERROR)
                {
                    printf("\n Get opration failed \n");
                    return SNMP_FAILURE;
                }
                else
                {
                    /* Need to fill mandatory parameters before passing it for
                    * getting index */
                    pDbEntry.u4NoIndices = (UINT4)i4IndexNo;
                    __va_copy(ap, args);
                    pu1TempIndexType = pDbEntry.pu1IndexType;
                    for (i4Count = 0; i4Count<i4IndexNo; i4Count++)
                    {
                        i4Type = va_arg(ap, INT4);
                        switch(i4Type)
                        {
                            case SNMP_DATA_TYPE_INTEGER32:
                                *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_INTEGER32;
                                va_arg(ap, INT4*);
                                break;
                            case SNMP_DATA_TYPE_UNSIGNED32:
                                *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_UNSIGNED32;
                                va_arg(ap, UINT4*);
                                break;
                            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                                *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                                va_arg(ap, UINT4*);
                                break;
                            case SNMP_DATA_TYPE_OCTET_PRIM:
                                *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_OCTET_PRIM;
                                va_arg(ap,tSNMP_OCTET_STRING_TYPE *);
                                break;
                            case SNMP_DATA_TYPE_COUNTER32:
                                *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_COUNTER32;
                                va_arg(ap, UINT4*);
                                break;
                            case SNMP_DATA_TYPE_COUNTER64:
                                *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_COUNTER64;
                                va_arg(ap, UINT4*);
                                break;
                            case SNMP_DATA_TYPE_TIME_TICKS:
                                *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_TIME_TICKS;
                                va_arg(ap, UINT4*);
                                break;
			    default:
                                break;
                        }
                        pDbEntry.pu1IndexType ++;
                        
                    } 
                    pDbEntry.pu1IndexType = pu1TempIndexType;
                    i4Result = SNMPMGRGetIndices (*pPtr->pVarBindList->pObjName,
                                                         pOid->u4_Length + 2,
                                                        &pDbEntry, &pParseMultiIndex);
                    if (i4Result == SNMP_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }

                    /* Copy the value form received response data to call routine
                     * variable. */ 
                    __va_copy(ap, args);
                    for (i4Count = 0; i4Count<i4IndexNo; i4Count++)
                    {
                        i4Type = va_arg(ap, INT4);
                        switch(i4Type)
                        {
                            case SNMP_DATA_TYPE_INTEGER32:
                                i4_SLongValue = va_arg(ap,INT4*);
                                *i4_SLongValue = pParseMultiIndex->pIndex[i4Count].i4_SLongValue;
                                break;
                            case SNMP_DATA_TYPE_UNSIGNED32:
                                u4_SLongValue = va_arg(ap,UINT4*);
                                *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                break;
                            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                                u4_SLongValue = va_arg(ap,UINT4*);
                                *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                break;
                            case SNMP_DATA_TYPE_OCTET_PRIM:
                                pRcvOctate = va_arg(ap,tSNMP_OCTET_STRING_TYPE *);
                                *pRcvOctate = *pParseMultiIndex->pIndex[i4Count].pOctetStrValue; 
                                break;
                            case SNMP_DATA_TYPE_COUNTER32:
                                u4_SLongValue = va_arg(ap,UINT4*);
                                *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                break;
                            case SNMP_DATA_TYPE_COUNTER64:
                                u4_SLongValue = va_arg(ap,UINT4*);
                                *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                break;
                            case SNMP_DATA_TYPE_TIME_TICKS:
                                u4_SLongValue = va_arg(ap,UINT4*);
                                *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                break;
			    default:
                                break;
                        }
                    } 
                }
                    
            } 
        }
        else 
        {
            printf("\nResponse timeout\n");
            return SNMP_FAILURE;
        }

        /* We need to return FAILURE if the response not comming for object belongs 
         * to the same table which is getting passed. Below checking will validate 
         * if it is fetching get-first for only the object which belong to the table
         *  getting passed to this routine via parameter */
        
        if (pPtr != NULL)
        {
            pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)u1Oid);
        
            tResOid = *pPtr->pVarBindList->pObjName;
            i4Flag = 0;
            for (i4Loop = 0; i4Loop<(INT4)pOid->u4_Length; i4Loop++)
            {
                if (pOid->pu4_OidList[i4Loop] != tResOid.pu4_OidList[i4Loop])
                {
                    i4Flag = 1;
                    break;
                }
            }
            if (i4Flag != 0)
            {
                return SNMP_FAILURE;
            }
        }

    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : SnmpGetNextIndex
 *  Description     : This function send request for getting next index
 *                    with respect to inputted index and 
 *                    process the received response.
 *  Input           : u1ObjName - Name of the object for which entry 
 *                    will be present in the corresponding table.
 *                    i4IndexNo - No of index.
 *  Output          : NONE
 *  Returns         : SNMP_SUCCESS / SNMP_FAILURE
 ************************************************************************/
INT1
SnmpGetNextIndex(UINT1 *u1ObjName, INT4 i4IndexNo, va_list args)
{
    tSNMP_OCTET_STRING_TYPE  *pRcvOctate = NULL;
    fd_set                   Socks;
    struct timeval           tTimeout={SNMP_MGR_TIMEOUT,0};
    INT4                     i4RetVal = SNMP_ZERO;
    INT4                     i4Flag = SNMP_ZERO;
    INT4                     i4Loop = SNMP_ZERO;
    UINT4                    u4OidLength = SNMP_ZERO;
    tMbDbEntry               pDbEntry;
    UINT1                    pu1IndexType[SNMP_MAX_INDICES_2];
    UINT1                    *pu1TempIndexType;
    tSnmpIndex               pMultiIndex;
    tSnmpIndex               *pParseMultiIndex = NULL;
    tSNMP_OCTET_STRING_TYPE  pOctateSecond[SNMP_MAX_INDICES_2];
    tSNMP_OID_TYPE           pOidSecond[SNMP_MAX_INDICES_2];
    tSNMP_MULTI_DATA_TYPE    pIndex[SNMP_MAX_INDICES_2];
    UINT1                    *pu1PktArray = NULL;
    tSNMP_OID_TYPE           *pOid = NULL; 
    tSNMP_OID_TYPE           tResOid; 
    INT4                     i4PktSize = SNMP_ZERO;
    tSNMP_VAR_BIND           *pVarBindPtr = NULL;
    tSNMP_NORMAL_PDU         pPdu; 
    tSNMP_OCTET_STRING_TYPE  pCommunityStr;
    INT4                     i4OutLen = SNMP_ZERO;
    UINT1                    **pOut;
    UINT1                    *pu1SendPkt = NULL;
    struct sockaddr_in       SnmpMgrAddr;
    INT4                     Socket = SNMP_ZERO;
    UINT4                    u4Len = SNMP_ZERO;
    INT4                     i4Result = SNMP_SUCCESS;
    tSNMP_COUNTER64_TYPE     SnmpCounter64Type;
    va_list                  ap;
    tSNMP_NORMAL_PDU         *pPtr = NULL;
    VOID                     *pDecodedPtr = NULL;
    INT2                     i2_PduType = SNMP_ZERO;
    INT4                     *i4_SLongValue = NULL;
    UINT4                    *u4_SLongValue = NULL;
    UINT1                    *pu1_OctetList = NULL;
    INT4                     i4Type = SNMP_ZERO;
    INT4                     i4Count = SNMP_ZERO;
    UINT4                    u4Count = SNMP_ZERO;
    UINT1                    *pu1TempOidStr = NULL;
    UINT4                    u4Iter = 0;

    /* Memory allocation */
    pu1_OctetList = (UINT1 *)calloc(SNMP_MAX_OCTETSTRING_SIZE, sizeof(UINT1));
    pu1SendPkt = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1));
    pu1PktArray = (UINT1 *)calloc(MAX_PKT_LENGTH, sizeof(UINT1));
    pu1TempOidStr = (UINT1 *)calloc(SNMP_MAX_OID_LENGTH, sizeof(UINT1));

    MEMSET (pu1SendPkt, '\0', MAX_PKT_LENGTH * sizeof(UINT1));
    MEMSET (pu1IndexType, '\0', SNMP_MAX_INDICES_2);
    MEMSET (pu1PktArray,'\0', MAX_PKT_LENGTH * sizeof (UINT1));
    MEMSET (pu1_OctetList, '\0', SNMP_MAX_OCTETSTRING_SIZE * sizeof(UINT1));
    MEMSET (pu1TempOidStr,'\0', SNMP_MAX_OID_LENGTH * sizeof(UINT1));

    for(u4Count = SNMP_ZERO; u4Count<SNMP_MAX_INDICES_2; u4Count++)
    {
        pOctateSecond[u4Count].pu1_OctetList = 
                    (UINT1 *)calloc(SNMP_MAX_OCTETSTRING_SIZE ,sizeof(UINT1));
        MEMSET (pOctateSecond[u4Count].pu1_OctetList, '\0', SNMP_MAX_OCTETSTRING_SIZE);
        pIndex[u4Count].pOctetStrValue = &pOctateSecond[u4Count];
        pIndex[u4Count].pOidValue = &pOidSecond[u4Count];
    }
 
    for(u4Count=0;u4Count<SNMP_MAX_INDICES_2;u4Count++)
    {
        MEMSET(gapOctateThird[u4Count].pu1_OctetList, '\0', 1024);
        MEMSET(gapOidThird[u4Count].pu4_OidList, '\0', 1024);

    }  

    gapMultiIndexThird.pIndex = (tSNMP_MULTI_DATA_TYPE *) &gapIndexThird;

    pMultiIndex.pIndex = (tSNMP_MULTI_DATA_TYPE *)&pIndex;
    pParseMultiIndex = &pMultiIndex;
    pOut = &pu1SendPkt;
    pDbEntry.pu1IndexType = (UINT1 *)&pu1IndexType;
    i4OutLen = 0;

    FD_ZERO(&Socks);

    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;
	
    MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
    SnmpMgrAddr.sin_family = AF_INET;
    SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
    SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SnmpMgrAddr);

    pCommunityStr.pu1_OctetList = gau1CommunityData;
    MEMSET (gau1CommunityData, SNMP_ZERO, sizeof (gau1CommunityData));
    STRCPY(pCommunityStr.pu1_OctetList,"PUBLIC");
    pCommunityStr.i4_Length = SNMP_MGR_PUBLIC_COMMUNITY_LEN;

    if (SnmpONGetOidFromName ((char *)u1ObjName, (char *)pu1TempOidStr)== SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
     
    __va_copy(ap, args);
    for(u4Count=0;u4Count<(UINT4)i4IndexNo;u4Count++)
    {
       i4Type = va_arg(ap, INT4);
       switch(i4Type)
       {
           case SNMP_DATA_TYPE_INTEGER32:
               gapMultiIndexThird.pIndex[u4Iter].i4_SLongValue = va_arg(ap,INT4);
               gapMultiIndexThird.pIndex[u4Iter].i2_DataType = 0x02;
               va_arg(ap,INT4*);
               break;
           case SNMP_DATA_TYPE_UNSIGNED32:
               gapMultiIndexThird.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndexThird.pIndex[u4Iter].i2_DataType = 0x42;
               va_arg(ap,UINT4*);
               break;
           case SNMP_DATA_TYPE_IP_ADDR_PRIM:
               gapMultiIndexThird.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndexThird.pIndex[u4Iter].i2_DataType = 0x40;
               va_arg(ap,UINT4*);
               break;
           case SNMP_DATA_TYPE_OCTET_PRIM:
               gapMultiIndexThird.pIndex[u4Iter].pOctetStrValue =
                                              va_arg(ap,tSNMP_OCTET_STRING_TYPE*);
               /*gapMultiIndexThird.pIndex[u4Iter].pOctetStrValue->i4_Length =
                 (INT4) STRLEN (gapMultiIndexThird.pIndex[u4Iter].pOctetStrValue->pu1_OctetList);*/
               gapMultiIndexThird.pIndex[u4Iter].i2_DataType = 0x04;
               va_arg(ap,tSNMP_OCTET_STRING_TYPE*);
               break;
           case SNMP_DATA_TYPE_COUNTER32:
               gapMultiIndexThird.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndexThird.pIndex[u4Iter].i2_DataType = 0x41;
               va_arg(ap,UINT4*);
               break;
           case SNMP_DATA_TYPE_COUNTER64:
               gapMultiIndexThird.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndexThird.pIndex[u4Iter].i2_DataType = 0x46;
               va_arg(ap,UINT4*);
               break;
           case SNMP_DATA_TYPE_TIME_TICKS:
               gapMultiIndexThird.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndexThird.pIndex[u4Iter].i2_DataType = 0x43;
               va_arg(ap,UINT4*);
               break;  
	   default:
	       break;  
       }
       u4Iter++;
    }

    gapMultiIndexThird.u4No = u4Iter;
  
    pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)pu1TempOidStr);
    u4OidLength = pOid->u4_Length;
    
    SnmpFormOidWithIndex(&gapMultiIndexThird, pu1TempOidStr);

    pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)pu1TempOidStr);
    
    pVarBindPtr = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_NULL, 
                                  0, 0, NULL, NULL, 
                                  SnmpCounter64Type);
    /* Fill the PDU */
    pPdu.pVarBindList = pVarBindPtr;
    pPdu.pVarBindEnd = pVarBindPtr;
    pPdu.u4_Version = VERSION1;
    pPdu.i4_ErrorStatus = SNMP_ZERO;
    pPdu.i4_ErrorIndex = SNMP_ZERO;
    pPdu.i4_NonRepeaters = SNMP_ZERO;
    pPdu.i4_MaxRepetitions = SNMP_ZERO;
    pPdu.i2Padding = SNMP_ZERO;
    pPdu.pCommunityStr = &pCommunityStr;
    pPdu.i2_PduType = SNMP_PDU_TYPE_GET_NEXT_REQUEST;

    /* Encode the packet */
    i4Result = SNMPMGREncodeGetPacket(&pPdu, pOut, &i4OutLen,
                                (UINT4)SNMP_PDU_TYPE_GET_NEXT_REQUEST);
    if (i4Result == SNMP_NOT_OK)
    {
        i4Result = SNMP_FAILURE;
    }
    else
    {
        i4Result = SNMP_SUCCESS;
    }

    /* Send the encoded packet to the destined 
    *  SNMP manager for set operation */
    if (pu1SendPkt != NULL)
    {
        if ( gi4Sock <= SNMP_ZERO)
        {
        if (SnmpMgrSockInit(&gi4Sock) == SNMP_FAILURE)
        {   
            return SNMP_FAILURE;
        }
        }
        Socket = gi4Sock;
        
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (gu4AgentAddr);        
	SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_PORT);

        /* Unlocking management lock as it will be automatically taken by SNMP */
        MgmtUnLock(); 

        if (sendto (Socket, pu1SendPkt, i4OutLen, SNMP_ZERO,
                   (struct sockaddr *) &SnmpMgrAddr,
                   (INT4) u4Len) == SNMP_NOT_OK)
        {
           /* Get the lock back */
           MgmtLock();
           return SNMP_FAILURE; 
        }
     
        /* Handling for response packet */
        MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
        SnmpMgrAddr.sin_family = AF_INET;
        SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
        u4Len = sizeof (SnmpMgrAddr);
        /* Call select() for setting timeout */

        FD_SET(Socket,&Socks);
        i4RetVal = select (Socket +1, &Socks, NULL, NULL, &tTimeout);

	/* Get the lock back */
        MgmtLock();

        if(i4RetVal == -1)
        {
            return SNMP_FAILURE;
        }
        else if (i4RetVal)
        { 
            if ((i4PktSize = recvfrom (Socket, pu1PktArray,
                                    MAX_SNMP_PKT_LEN, 0,
                                    (struct sockaddr *) &SnmpMgrAddr,
                                    (socklen_t *) & u4Len)) > 0)
            {
                    /* Received responce packet. Now decode the packet */
                    pDecodedPtr =
                         (VOID *) SNMPMGRDecodePacket (pu1PktArray, 
                                        (INT4) i4PktSize, &i2_PduType);
                    if (pDecodedPtr == NULL)
                    {
                        return SNMP_FAILURE;
                    }

                    pPtr = (tSNMP_NORMAL_PDU *) pDecodedPtr;
                    if (pPtr == NULL)
                    {
                        return SNMP_FAILURE;
                    }
                 
                    if (pPtr->i4_ErrorStatus != SNMP_ERR_NO_ERROR)
                    {
                        printf("\n Get opration failed \n");
                        return SNMP_FAILURE;
                    }
                    else
                    {
                        /* Need to fill mandatory parameters before passing it for
                        * getting index */
                        pDbEntry.u4NoIndices = (UINT4)i4IndexNo;
                        __va_copy(ap, args);
                        pu1TempIndexType = pDbEntry.pu1IndexType;
                        for (i4Count = 0; i4Count<i4IndexNo; i4Count++)
                        {
                            i4Type = va_arg(ap, INT4);
                            switch(i4Type)
                            {
                                case SNMP_DATA_TYPE_INTEGER32:
                                    *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_INTEGER32;
                                    va_arg(ap, INT4*);
                                    va_arg(ap, INT4*);
                                    break;
                                case SNMP_DATA_TYPE_UNSIGNED32:
                                    *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_UNSIGNED32;
                                    va_arg(ap, UINT4*);
                                    va_arg(ap, UINT4*);
                                    break;
                                case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                                    *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
                                    va_arg(ap, UINT4*);
                                    va_arg(ap, UINT4*);
                                    break;
                                case SNMP_DATA_TYPE_OCTET_PRIM:
                                    *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_OCTET_PRIM;
                                    va_arg(ap,tSNMP_OCTET_STRING_TYPE *);
                                    va_arg(ap,tSNMP_OCTET_STRING_TYPE *);
                                    break;
                                case SNMP_DATA_TYPE_COUNTER32:
                                    *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_COUNTER32;
                                    va_arg(ap, UINT4*);
                                    va_arg(ap, UINT4*);
                                    break;
                                case SNMP_DATA_TYPE_COUNTER64:
                                    *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_COUNTER64;
                                    va_arg(ap, UINT4*);
                                    va_arg(ap, UINT4*);
                                    break;
                                case SNMP_DATA_TYPE_TIME_TICKS:
                                    *pDbEntry.pu1IndexType = SNMP_DATA_TYPE_TIME_TICKS;
                                    va_arg(ap, UINT4*);
                                    va_arg(ap, UINT4*);
                                    break;
				default:
				    break;  
                            }
                            pDbEntry.pu1IndexType ++;
                            
                        } 
                        pDbEntry.pu1IndexType = pu1TempIndexType;
                        i4Result = SNMPMGRGetIndices (*pPtr->pVarBindList->pObjName,
                                                             u4OidLength,
                                                            &pDbEntry, &pParseMultiIndex);
                        if (i4Result == SNMP_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        
                        /* Copy the value form received response data to call routine
                         * variable. */ 
                        __va_copy(ap, args);
                        for (i4Count = 0; i4Count<i4IndexNo; i4Count++)
                        {
                            i4Type = va_arg(ap, INT4);
                            switch(i4Type)
                            {
                                case SNMP_DATA_TYPE_INTEGER32:
                                    va_arg(ap, INT4);
                                    i4_SLongValue = va_arg(ap,INT4*);
                                    *i4_SLongValue = pParseMultiIndex->pIndex[i4Count].i4_SLongValue;
                                    break;
                                case SNMP_DATA_TYPE_UNSIGNED32:
                                    va_arg(ap, UINT4);
                                    u4_SLongValue = va_arg(ap,UINT4*);
                                    *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                    break;
                                case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                                    va_arg(ap, UINT4);
                                    u4_SLongValue = va_arg(ap,UINT4*);
                                    *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                    break;
                                case SNMP_DATA_TYPE_OCTET_PRIM:
                                    va_arg(ap, tSNMP_OCTET_STRING_TYPE *);
                                    pRcvOctate = va_arg(ap,tSNMP_OCTET_STRING_TYPE *);
                                    *pRcvOctate = *pParseMultiIndex->pIndex[i4Count].pOctetStrValue;
                                    break;
                                case SNMP_DATA_TYPE_COUNTER32:
                                    va_arg(ap, UINT4);
                                    u4_SLongValue = va_arg(ap,UINT4*);
                                    *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                    break;
                                case SNMP_DATA_TYPE_COUNTER64:
                                    va_arg(ap, UINT4);
                                    u4_SLongValue = va_arg(ap,UINT4*);
                                    *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                    break;
                                case SNMP_DATA_TYPE_TIME_TICKS:
                                    va_arg(ap, UINT4);
                                    u4_SLongValue = va_arg(ap,UINT4*);
                                    *u4_SLongValue = pParseMultiIndex->pIndex[i4Count].u4_ULongValue;
                                    break;
				default:
				    break;  
                            }
                        } 
                    }
                    
            } 
        }
        else
        {   
            printf("\nResponse timeout\n"); 
            return SNMP_FAILURE;
        }
        
        /* We need to stop while get-next reach end otherwise it will agin start 
         * fetching get-next for next object. But we only need index so for one 
         * object first entry to last entry walking will be sufficient. Below 
         * checking will validate if it is fetching get-next for only the object 
         * which is getting passed to this routine via parameter */
        if(pPtr != NULL)
        {
            if (SnmpONGetOidFromName ((char *)u1ObjName,
                          (char *)pu1TempOidStr)== SNMP_FAILURE)
            {   
                return SNMP_FAILURE;
            }
    
            pOid = (tSNMP_OID_TYPE *) SNMP_MGR_GetOidFromString ((INT1 *)pu1TempOidStr);
        
            tResOid = *pPtr->pVarBindList->pObjName;
            i4Flag = 0;
            for (i4Loop = 0; i4Loop<(INT4)pOid->u4_Length; i4Loop++)
            {
                if (pOid->pu4_OidList[i4Loop] != tResOid.pu4_OidList[i4Loop])
                {
                    i4Flag = 1;
                    break;
                }
            }
            if (i4Flag != 0)
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

#endif /* __SMGR_API_C__ */
