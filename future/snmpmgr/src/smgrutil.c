/********************************************************************
 * Copyright (C) 2017 Aricent Inc . All Rights Reserved 
 *
 * $Id: smgrutil.c,v 1.1 2017/08/24 12:03:43 siva Exp $
 *
 * Description: Contains utility routines for snmp manager
 *******************************************************************/

#ifndef __SNMMGR_UTILS_C__
#define __SNMMGR_UTILS_C__

#include "smgrinc.h"
#include "cli.h"
#include "smgrdb.h"
#include "fssocket.h"

/******************Global allocation************************/
INT4 gisMemInit = FALSE;
extern UINT1            gau1CommunityData[SNMP_MAX_OCTETSTRING_SIZE];
UINT4                   gu4_EntRetOidList[MAX_ARRAY_SIZE];
/* Global allocation for varbind */
UINT1               gau1OutDatSecond[MAX_PKT_LENGTH];
tSNMP_VAR_BIND          gVarBind;
tSNMP_OID_TYPE          gObjName;
UINT4                   gu4_OidList[MAX_ARRAY_SIZE];
UINT1                   gau1_Octet[MAX_ARRAY_SIZE];
UINT4                   gau4_oidList[MAX_ARRAY_SIZE];
tSNMP_OCTET_STRING_TYPE gtOctate;
tSNMP_OID_TYPE          gtOid;
tSNMP_OID_TYPE          gtOidMgr;
/**********************************/

/* Extern declaration */
/*extern INT1
SNMPMGRConvertSubOid (UINT1 **ppu1String, UINT4 *pu4Value);
extern INT4
SNMPMGREncodeGetPacket (tSNMP_NORMAL_PDU * pMsgPtr, UINT1 **ppu1DataOut,
                     INT4 *pi4PktLen, INT4 i4TypeOfMsg);
extern INT1               *
SNMPMGRDecodePacket (UINT1 *pu1PktPtr, INT4 i4PktLen, INT2 *pPduType);
*/extern INT4
SNMP_AGT_ConvSubOIDToLong (UINT1 **ppu1TempPtr);
/************************************************************************/

/* Global allocation for multiindex and multidata */
tRetVal             gapMultiData;
tSnmpIndex          gapMultiIndex;
UINT1                   gapu1_OctetFirst[1024];
UINT4                   gapu4_oidListFirst[1024];
tSNMP_OCTET_STRING_TYPE gapOctateFirst;
tSNMP_OID_TYPE          gapOidFirst;
tSNMP_OID_TYPE                  gapOidSecond[SNMP_MAX_INDICES_2];
tSNMP_OCTET_STRING_TYPE         gapOctateSecond[SNMP_MAX_INDICES_2];
UINT4                           gapu4_oidListSecond[1024];
UINT1                           gapu1_OctetSecond[1024];
tSNMP_MULTI_DATA_TYPE           gapIndex[SNMP_MAX_INDICES_2];
/**************************************************/

/************************************************************************
 *  Function Name   : ConvertToMultiIndex
 *  Description     : This function form MultiIndex Structure with the 
 *                    received index values.
 *  Input           : u4IndexNo - No of index.
 *  Output/Returns  : gapMultiIndex - MultiIndex structure.
 ************************************************************************/
tSnmpIndex * ConvertToMultiIndex(UINT4 u4IndexNo,...)
{
    va_list                         ap;
    UINT4                           u4Count = 0;
    INT4                            i4Type = 0;
    UINT4                           u4Iter = 0;
  
    if ( gisMemInit == FALSE)
    {
        SnmpMgrMemInit();
        gisMemInit = TRUE; 
    }   
    for(u4Count=0;u4Count<SNMP_MAX_INDICES_2;u4Count++)
    {
        MEMSET(gapOctateSecond[u4Count].pu1_OctetList, '\0', 1024);
        MEMSET(gapOidSecond[u4Count].pu4_OidList, '\0', 1024);
    }  

    gapMultiIndex.pIndex = (tSNMP_MULTI_DATA_TYPE *) &gapIndex;
 
    va_start (ap, u4IndexNo);
    while(TRUE)
    {
       i4Type = va_arg(ap, INT4);
       if (i4Type == 0)
       {
           break;
       }
       switch(i4Type)
       {
           case SNMP_DATA_TYPE_INTEGER32:
               gapMultiIndex.pIndex[u4Iter].i4_SLongValue = va_arg(ap,INT4);
               gapMultiIndex.pIndex[u4Iter].i2_DataType = SNMP_DATA_TYPE_INTEGER32;
               break;
           case SNMP_DATA_TYPE_UNSIGNED32:
               gapMultiIndex.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndex.pIndex[u4Iter].i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
               break;
           case SNMP_DATA_TYPE_IP_ADDR_PRIM:
               gapMultiIndex.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndex.pIndex[u4Iter].i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
               break;
           case SNMP_DATA_TYPE_OCTET_PRIM:
               gapMultiIndex.pIndex[u4Iter].pOctetStrValue =
                                              va_arg(ap,tSNMP_OCTET_STRING_TYPE*);
               gapMultiIndex.pIndex[u4Iter].pOctetStrValue->i4_Length =
                 (INT4) STRLEN (gapMultiIndex.pIndex[u4Iter].pOctetStrValue->pu1_OctetList);
               gapMultiIndex.pIndex[u4Iter].i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
               break;
           case SNMP_DATA_TYPE_COUNTER32:
               gapMultiIndex.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndex.pIndex[u4Iter].i2_DataType = SNMP_DATA_TYPE_COUNTER32;
               break;
           case SNMP_DATA_TYPE_COUNTER64:
               gapMultiIndex.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndex.pIndex[u4Iter].i2_DataType = SNMP_DATA_TYPE_COUNTER64;
               break;
           case SNMP_DATA_TYPE_TIME_TICKS:
               gapMultiIndex.pIndex[u4Iter].u4_ULongValue = va_arg(ap,UINT4);
               gapMultiIndex.pIndex[u4Iter].i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
               break;  
           default:
               break;  
       }
       u4Iter++;
    }

    va_end (ap);

    gapMultiIndex.u4No = u4Iter;
    return &gapMultiIndex;
}

/************************************************************************
 *  Function Name   : ConvertToSetMultiData
 *  Description     : This function form MultiData Structure for set 
 *                    operation with the received parameters.
 *  Input           : i4Type - Type of set value.
 *  Output/Returns  : gapMultiData - MultiData structure.
 ************************************************************************/
tRetVal * ConvertToSetMultiData(INT4 i4Type,...)
{
    va_list                 ap;
 
    MEMSET (gapu1_OctetFirst, '\0', SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (gapu4_oidListFirst, '\0', SNMP_MAX_OID_LENGTH);
    
    gapOctateFirst.pu1_OctetList = gapu1_OctetFirst;
    gapOidFirst.pu4_OidList = gapu4_oidListFirst;
    gapMultiData.pOctetStrValue = &gapOctateFirst;
    gapMultiData.pOidValue = &gapOidFirst;
    
    va_start (ap, i4Type);
    switch(i4Type)
    {
       case SNMP_DATA_TYPE_INTEGER32:
           gapMultiData.i4_SLongValue = va_arg(ap,INT4);
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_INTEGER32;
           break;
       case SNMP_DATA_TYPE_UNSIGNED32:
           gapMultiData.u4_ULongValue = va_arg(ap,UINT4);
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
           break;
       case SNMP_DATA_TYPE_IP_ADDR_PRIM:
           gapMultiData.u4_ULongValue = va_arg(ap,UINT4); 
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
           break;
       case SNMP_DATA_TYPE_OCTET_PRIM:
           gapMultiData.pOctetStrValue = va_arg(ap,tSNMP_OCTET_STRING_TYPE*);
           gapMultiData.pOctetStrValue->i4_Length =
             (INT4) STRLEN (gapMultiData.pOctetStrValue->pu1_OctetList);
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
           break;
       case SNMP_DATA_TYPE_COUNTER32:
           gapMultiData.u4_ULongValue = va_arg(ap,UINT4);
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_COUNTER32;
           break;
       case SNMP_DATA_TYPE_COUNTER64:
           gapMultiData.u4_ULongValue = va_arg(ap,UINT4);
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_COUNTER64;
           break;
       case SNMP_DATA_TYPE_TIME_TICKS:
           gapMultiData.u4_ULongValue = va_arg(ap,UINT4);
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
           break;  
       default:
           break;  
      }
      
      va_end (ap);
 
      return &gapMultiData; 
}

/************************************************************************
 *  Function Name   : ConvertToGetMultiData
 *  Description     : This function form MultiData Structure for Get 
 *                    operation with the default values.
 *  Input           : i4Type - Type of set value.
 *  Output/ Returns : gapMultiData - MultiData structure.
 ************************************************************************/
tRetVal * ConvertToGetMultiData(INT4 i4Type)
{
    MEMSET (gapu1_OctetFirst, '\0', SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (gapu4_oidListFirst, '\0', SNMP_MAX_OID_LENGTH);
    
    gapOctateFirst.pu1_OctetList = gapu1_OctetFirst;
    gapOidFirst.pu4_OidList = gapu4_oidListFirst;
    gapMultiData.pOctetStrValue = &gapOctateFirst;
    gapMultiData.pOidValue = &gapOidFirst;
    
    switch(i4Type)
    {
       case SNMP_DATA_TYPE_INTEGER32:
           gapMultiData.i4_SLongValue = 0;
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_INTEGER32;
           break;
       case SNMP_DATA_TYPE_UNSIGNED32:
           gapMultiData.u4_ULongValue = 0;
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
           break;
       case SNMP_DATA_TYPE_IP_ADDR_PRIM:
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
           gapMultiData.u4_ULongValue = 0;
           break;
       case SNMP_DATA_TYPE_OCTET_PRIM:
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
           break;
       case SNMP_DATA_TYPE_COUNTER32:
           gapMultiData.u4_ULongValue = 0;
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_COUNTER32;
           break;
       case SNMP_DATA_TYPE_COUNTER64:
           gapMultiData.u4_ULongValue = 0;
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_COUNTER64;
           break;
       case SNMP_DATA_TYPE_TIME_TICKS:
           gapMultiData.u4_ULongValue = 0;
           gapMultiData.i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
           break;  
       default:
           break;  
      }
   return &gapMultiData;
}

/************************************************************************
 *  Function Name   : ConvertStringToDottedString
 *  Description     : This function construct dotted string for inputed string.
 *  Input           : pu1ToBeConvertString - Inputted string.
 *  Output          : pu1ConvertedString - Containes resultant dotted string
 *  Returns         : NONE
 ************************************************************************/
VOID 
ConvertStringToDottedString (UINT1 * pu1ConvertedString, UINT1 * pu1ToBeConvertString)
{
    INT4           i4Count = 0;
    
    while(pu1ToBeConvertString[i4Count] != '\0')
    {
        SPRINTF((CHR1 *)pu1ConvertedString,"%s.%d",pu1ConvertedString,
                                    pu1ToBeConvertString[i4Count]);
        i4Count ++;
    }
    
}

/************************************************************************
 *  Function Name   : ConvertToGetNextMultiData
 *  Description     : This function construct multidata structure for 
 *                    getNext operation.
 *  Input           : None.
 *  Output          : None
 *  Returns         : Multidata structure.
 ************************************************************************/
tRetVal * ConvertToGetNextMultiData(VOID)
{
    MEMSET (gapu1_OctetFirst, '\0', SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (gapu4_oidListFirst, '\0', SNMP_MAX_OID_LENGTH);
    
    gapOctateFirst.pu1_OctetList = gapu1_OctetFirst;
    gapOidFirst.pu4_OidList = gapu4_oidListFirst;
    gapMultiData.pOctetStrValue = &gapOctateFirst;
    gapMultiData.pOidValue = &gapOidFirst;
    return &gapMultiData;
}

/************************************************************************
 *  Function Name   : SnmpCreateVarBind
 *  Description     : This function construct varbind structure.
 *  Input           : pOid - OID.
 *                    pMultiData - Multidata structure.
 *  Output          : NONE
 *  Returns         : Varbind structure.
 ************************************************************************/
tSNMP_VAR_BIND *
SnmpCreateVarBind(tSNMP_OID_TYPE * pOid,tRetVal *pMultiData)
{
    CHR1                    *pu1IpAddr;
    UINT1                    u1IpAddr[SNMP_MGR_MAX_IP_ADD_LEN];
    UINT1                    u1IpAddrSecond[SNMP_MGR_MAX_IP_ADD_LEN];
    tSNMP_OCTET_STRING_TYPE  pParseIpAddr;     
    tSNMP_COUNTER64_TYPE     SnmpCounter64Type;
    tSNMP_VAR_BIND           *pVbList = NULL;

    MEMSET (u1IpAddr, '\0', SNMP_MGR_MAX_IP_ADD_LEN);

    pParseIpAddr.pu1_OctetList = (UINT1 *)&u1IpAddr;
    pu1IpAddr = (CHR1 *)&u1IpAddrSecond;    

    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    /* Form Varbind */
    switch(pMultiData->i2_DataType)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_INTEGER32,
                                   0, pMultiData->i4_SLongValue, NULL, NULL, 
                                   SnmpCounter64Type);
            break;
        case SNMP_DATA_TYPE_UNSIGNED32:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_UNSIGNED32, 
                                   pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
           CLI_CONVERT_IPADDR_TO_STR(pu1IpAddr,
                                  pMultiData->u4_ULongValue);
           SnmpMgrConvertStringToOctet (&pParseIpAddr, (UINT1 *)pu1IpAddr);
           pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                  SNMP_DATA_TYPE_IP_ADDR_PRIM, 
                                  0, 0, &pParseIpAddr, NULL, 
                                  SnmpCounter64Type);
          break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_OCTET_PRIM, 
                                   0, 0, pMultiData->pOctetStrValue, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_COUNTER32:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_COUNTER32, 
                                   pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_COUNTER64:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_COUNTER64, 
                                   0, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;
        case SNMP_DATA_TYPE_TIME_TICKS:
            pVbList = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid, 
                                   SNMP_DATA_TYPE_TIME_TICKS, 
                                   pMultiData->u4_ULongValue, 0, NULL, NULL, 
                                   SnmpCounter64Type);
           break;  
	default:
           break;  
    }
    return pVbList;

}

/************************************************************************
 *  Function Name   : SnmpFormOidWithIndex
 *  Description     : This function constructs full OID with the indexes.
 *  Input           : pMultiIndex - MultiIndex data
 *                    pu1TempOid - Actual OID  
 *  Output          : pu1TempOid - Full OID 
 *  Returns         : NONE
 ************************************************************************/
VOID
SnmpFormOidWithIndex(tSnmpIndex *pMultiIndex,UINT1 *pu1TempOid)
{
    UINT1                    *pu1_OctetList = NULL;
    UINT1                    u1_OctetList[SNMP_MAX_OCTETSTRING_SIZE];
    UINT4                    u4Count = SNMP_ZERO;
    CHR1                     *i1Temp;
    INT1                     i1TempArray[SNMP_MGR_MAX_IP_ADD_LEN];

    MEMSET (i1TempArray, '\0', SNMP_MGR_MAX_IP_ADD_LEN);
    MEMSET (u1_OctetList, '\0', SNMP_MAX_OCTETSTRING_SIZE);

    i1Temp = (CHR1 *)&i1TempArray;
    pu1_OctetList = (UINT1 *)&u1_OctetList;

    /* Construct full oid from actual oid and mutiindex */  
    if(pMultiIndex->u4No == 0)
    {
        SPRINTF((CHR1 *)pu1TempOid,"%s.0",pu1TempOid);
    }
    else
    {
        for (u4Count = 0; u4Count<pMultiIndex->u4No; u4Count++)
        {
            switch (pMultiIndex->pIndex->i2_DataType)
            {
                case SNMP_DATA_TYPE_INTEGER32:                
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",
                          pu1TempOid,pMultiIndex->pIndex->i4_SLongValue);
                    break;
                case SNMP_DATA_TYPE_UNSIGNED32:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                      pMultiIndex->pIndex->u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                    CLI_CONVERT_IPADDR_TO_STR(i1Temp, pMultiIndex->pIndex->u4_ULongValue);
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%s",pu1TempOid,i1Temp);
                    break;
                case SNMP_DATA_TYPE_OCTET_PRIM:
                    ConvertStringToDottedString (pu1_OctetList,
                                    pMultiIndex->pIndex->pOctetStrValue->pu1_OctetList);
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d%s",pu1TempOid,
                      pMultiIndex->pIndex->pOctetStrValue->i4_Length, pu1_OctetList);
                    break;
                case SNMP_DATA_TYPE_COUNTER32:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                    pMultiIndex->pIndex->u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_COUNTER64:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                    pMultiIndex->pIndex->u4_ULongValue);
                    break;
                case SNMP_DATA_TYPE_TIME_TICKS:
                    SPRINTF((CHR1 *)pu1TempOid,"%s.%d",pu1TempOid, 
                                    pMultiIndex->pIndex->u4_ULongValue);
                    break;
		default:
		    break;  
            }  
            pMultiIndex->pIndex++;     
        }
    }

}

/************************************************************************
 *  Function Name   : SNMPMGRGetIndices 
 *  Description     : Get Index in a Index array from OID
 *  Input           : OID - Received Oid
 *                    u4Len - Length From index fetching starts
 *                    pDbEntry - DB entry where the actual Oid is available
 *  Output          : pIndex - Index array where index to be stored  
 *  Returns         : SUCCESS,NOINDEX or FAILURE 
 ************************************************************************/
INT4
SNMPMGRGetIndices (tSNMP_OID_TYPE OID, UINT4 u4Len,
                const tMbDbEntry * pDbEntry, tSnmpIndex ** pIndex)
{
    UINT4               u4Count = 0, u4I = 0;
    tSNMP_MULTI_DATA_TYPE *pMulti = NULL;
    UINT4               u4IpAddress = 0;
    UINT4               u4CountTemp = 0;
    UINT4               u4WrongValue = SNMP_FAILURE;

    (*pIndex)->u4No = pDbEntry->u4NoIndices;
    /*----------------------------------------
     *  if the MbDb Entry passed is a Scalar
     *  and the length of OID is not greater than
     *  MBDB Entry OID lenth by One 
     *  the last instance is not Zero
     *  return SNMP_FAILURE
     *  The instance ID is not stored in MBDB
     *  Hence the length will differ by one
     *---------------------------------------------- */
    if (IS_SCALAR (pDbEntry))
    {
        if ((u4Len != OID.u4_Length - SNMP_ONE) ||
            (OID.pu4_OidList[u4Len] != SNMP_ZERO))
        {
            return SNMP_FAILURE;
        }
        else
        {
            return NO_INDEX;
        }
    }
    /*--------If table to take care of the first instance---*/
    if (IS_TABLE (pDbEntry) && (u4Len >= OID.u4_Length))
    {
        return NO_INDEX;
    }
    /*-------------Decoding the indices (to be refined )---*/
    for (u4Count = 0, u4CountTemp = 0; u4CountTemp < (*pIndex)->u4No;
         u4Count++, u4CountTemp++)
    {
        if (u4Len >= OID.u4_Length)
        {
            (*pIndex)->u4No = u4Count;
        }
        (*pIndex)->pIndex[u4CountTemp].i2_DataType =
            pDbEntry->pu1IndexType[u4Count];
        /* Select the Appropriate Index to update */
        pMulti = (tSNMP_MULTI_DATA_TYPE *) & ((*pIndex)->pIndex[u4CountTemp]);

        switch (pDbEntry->pu1IndexType[u4Count])
        {
            case SNMP_DATA_TYPE_UNSIGNED32:
            case SNMP_DATA_TYPE_COUNTER:
            case SNMP_DATA_TYPE_TIME_TICKS:
                pMulti->u4_ULongValue = OID.pu4_OidList[u4Len++];
                break;
            case SNMP_DATA_TYPE_INTEGER32:
                if (OID.pu4_OidList[u4Len] > SNMP_MAX_INT)
                {
                    OID.pu4_OidList[u4Len] = SNMP_MAX_INT;
                    u4WrongValue = SNMP_SUCCESS;
                }
                pMulti->i4_SLongValue = (INT4) (OID.pu4_OidList[u4Len++]);
                break;

            case SNMP_DATA_TYPE_OCTET_PRIM:
            case SNMP_DATA_TYPE_OPAQUE:
                pMulti->pOctetStrValue->i4_Length =
                    (INT4) (OID.pu4_OidList[u4Len++]);
                /* work around for Fixedlen octet str illegal 
                 * input case.
                 */
                if ((pMulti->pOctetStrValue->i4_Length < 0) ||
                    (pMulti->pOctetStrValue->i4_Length >
                     SNMP_MAX_OCTETSTRING_SIZE))
                    /*Invalid length str 
                     * The maximum allowable length of octet string is
                     * SNMP_MAX_OCTETSTRING_SIZE */
                {
                    return NO_INDEX_LEN_MATCH;
                }

                if ((UINT4) (pMulti->pOctetStrValue->i4_Length) >
                    (OID.u4_Length - u4Len))
                {
                    return NO_INDEX_LEN_MATCH;
                }
                for (u4I = 0; u4I < (UINT4) pMulti->pOctetStrValue->i4_Length;
                     u4I++)
                {
                    if (u4Len > OID.u4_Length)
                    {
                        /* Length cannot be more than OID's length */
                        break;
                    }
                    if (OID.pu4_OidList[u4Len] > MAX_STR_OCTET_VAL)
                    {
                        OID.pu4_OidList[u4Len] = MAX_STR_OCTET_VAL;
                        u4WrongValue = SNMP_SUCCESS;
                    }
                    pMulti->pOctetStrValue->pu1_OctetList[u4I]
                        = (UINT1) OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_DATA_TYPE_IMP_OCTET_PRIM:

                if (OID.u4_Length < u4Len)    /*Invalid length str */
                    return NO_INDEX_LEN_MATCH;

                pMulti->pOctetStrValue->i4_Length =
                    (INT4) (OID.u4_Length - u4Len);
                if (pMulti->pOctetStrValue->i4_Length >
                    SNMP_MAX_OCTETSTRING_SIZE)
                {
                    return SNMP_FAILURE;
                }
                for (u4I = SNMP_ZERO;
                     u4I < (UINT4) pMulti->pOctetStrValue->i4_Length; u4I++)
                {
                    pMulti->pOctetStrValue->pu1_OctetList[u4I]
                        = (UINT1) OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_DATA_TYPE_IP_ADDR_PRIM:
                for (u4I = u4Len; u4I < u4Len + 4; u4I++)
                {
                    if (u4Len > OID.u4_Length)
                    {
                        /* Length cannot be more than OID's length */
                        break;
                    }
                    if (OID.pu4_OidList[u4I] >= MAX_STR_OCTET_VAL)
                    {
                        OID.pu4_OidList[u4I] = MAX_STR_OCTET_VAL;
                        u4WrongValue = SNMP_SUCCESS;
                    }
                }
                u4IpAddress = OID.pu4_OidList[u4Len++];
                u4IpAddress =
                    (u4IpAddress << SNMP_EIGHT) | OID.pu4_OidList[u4Len++];
                u4IpAddress =
                    (u4IpAddress << SNMP_EIGHT) | OID.pu4_OidList[u4Len++];
                u4IpAddress =
                    (u4IpAddress << SNMP_EIGHT) | OID.pu4_OidList[u4Len++];
                pMulti->u4_ULongValue = u4IpAddress;
                break;
            case SNMP_DATA_TYPE_OBJECT_ID:
                pMulti->pOidValue->u4_Length = OID.pu4_OidList[u4Len++];
                if (pMulti->pOidValue->u4_Length > MAX_OID_LENGTH)
                {
                    return NO_INDEX_LEN_MATCH;
                }
                for (u4I = SNMP_ZERO;
                     u4I < (UINT4) pMulti->pOidValue->u4_Length; u4I++)
                {
                    pMulti->pOidValue->pu4_OidList[u4I]
                        = OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_DATA_TYPE_IMP_OBJECT_ID:
                pMulti->pOidValue->u4_Length = OID.u4_Length - u4Len;
                if (pMulti->pOidValue->u4_Length > MAX_OID_LENGTH)
                {
                    return SNMP_FAILURE;
                }

                for (u4I = SNMP_ZERO;
                     u4I < (UINT4) pMulti->pOidValue->u4_Length; u4I++)
                {
                    pMulti->pOidValue->pu4_OidList[u4I]
                        = OID.pu4_OidList[u4Len++];
                }
                break;
            case SNMP_FIXED_LENGTH_OCTET_STRING:
                /* To Handle Fixed OctetString
                 *  Get the length from protocol db.h (generated 
                 *  using midgen tool)
                 *  Copy only the specified no of octets
                 *  (length as in db.h file) 
                 *  Read Next Index    */
                pMulti->pOctetStrValue->i4_Length =
                    pDbEntry->pu1IndexType[u4Count + 1];
                for (u4I = 0; u4I < (UINT4) pMulti->pOctetStrValue->i4_Length;
                     u4I++)
                {
                    if (u4Len > OID.u4_Length)
                    {
                        /* Length cannot be more than OID's length */
                        break;
                    }
                    if (OID.pu4_OidList[u4Len] > MAX_STR_OCTET_VAL)
                    {
                        OID.pu4_OidList[u4Len] = MAX_STR_OCTET_VAL;
                        u4WrongValue = SNMP_SUCCESS;
                    }
                    pMulti->pOctetStrValue->pu1_OctetList[u4I]
                        = (UINT1) OID.pu4_OidList[u4Len++];
                }
                u4Count++;
                break;
            default:
                return SNMP_FAILURE;
                break;
        }
    }
    /* Wrong Values are given so we have updated with the Max Values for these
     * and returning NO_INDEX_LEN_MATCH.
     */
    if (u4WrongValue == SNMP_SUCCESS)
    {
        return NO_INDEX_LEN_MATCH;
    }
    /*---------------------------------------------------
      In case of Table after decoding 
      if there are still OID left out length
      or if thr processed length is more
      than OID length then the OID is invalid
      ----------------------------------------------------*/
    if (u4Len != OID.u4_Length)
    {
        return SNMP_ERR_NO_CREATION;
    }
    return SNMP_SUCCESS;
}


/************************************************************************
 *  Function Name   : SNMP_MGR_FormVarBind 
 *  Description     : Function to form variable bind for the give oid and
 *                    value  
 *  Input           : pOid - Pointer to oid to be assigned in varbind
 *                    i2Type - Data type in the varbind value
 *                    u4Value - value of unsigned32 if the type is unsigned
 *                    i4Value - value of signed32 if the type is signed32
 *                    pOctet - pointer to octet string if the type is
 *                    octet string
 *                    pOidType - Pointer to oid if the type is oid
 *                    u8Value - Conter64 value if the type is conter64 
 *  Output          : None 
 *  Returns         : Variable bind pointer or null
 ************************************************************************/
tSNMP_VAR_BIND     *
SNMP_MGR_FormVarBind (tSNMP_OID_TYPE * pOid, INT2 i2Type, UINT4 u4Value,
                      INT4 i4Value, tSNMP_OCTET_STRING_TYPE * pOctet,
                      tSNMP_OID_TYPE * pOidType, tSNMP_COUNTER64_TYPE u8Value)
{
    tSNMP_VAR_BIND     *pVarBindPtr = NULL;
     
    pVarBindPtr = &gVarBind;
    
    MEMSET (pVarBindPtr, 0, sizeof (tSNMP_VAR_BIND));
    pVarBindPtr->pObjName = pOid;
    pVarBindPtr->ObjValue.i2_DataType = i2Type;
    pVarBindPtr->ObjValue.u4_ULongValue = 0;
    pVarBindPtr->ObjValue.i4_SLongValue = 0;
    pVarBindPtr->ObjValue.pOidValue = NULL;
    pVarBindPtr->ObjValue.pOctetStrValue = NULL;
    pVarBindPtr->ObjValue.u8_Counter64Value.lsn = 0;
    pVarBindPtr->ObjValue.u8_Counter64Value.msn = 0;
    pVarBindPtr->pNextVarBind = NULL;
    switch (i2Type)
    {
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
            pVarBindPtr->ObjValue.u4_ULongValue = u4Value;
            break;
        case SNMP_DATA_TYPE_INTEGER32:
            pVarBindPtr->ObjValue.i4_SLongValue = i4Value;
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            if (pOidType == NULL)
            {
                   return NULL;
            }
            pVarBindPtr->ObjValue.pOidValue = pOidType;
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            pVarBindPtr->ObjValue.pOctetStrValue = pOctet;
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            pVarBindPtr->ObjValue.pOctetStrValue = pOctet;
	    MEMCPY (&(pVarBindPtr->ObjValue.u4_ULongValue),
			    pOctet->pu1_OctetList, pOctet->i4_Length);

	    pVarBindPtr->ObjValue.u4_ULongValue =
		    OSIX_HTONL (pVarBindPtr->ObjValue.u4_ULongValue);
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            pVarBindPtr->ObjValue.u8_Counter64Value.lsn = u8Value.lsn;
            pVarBindPtr->ObjValue.u8_Counter64Value.msn = u8Value.msn;
            break;
        case SNMP_DATA_TYPE_NULL:
            break;
        default:
            return NULL;
    }
    return pVarBindPtr;
}

/************************************************************************
 *  Function Name   : SnmpMgrConvertStringToOctet 
 *  Description     : Convert a string to octet string
 *  Input           : pu1Data - String to be converted to octet 
 *  Output          : pOctetStrValue - Pointer to octet string
 *  Returns         : SUCCESS or FAILURE 
 ************************************************************************/

INT4
SnmpMgrConvertStringToOctet (tSNMP_OCTET_STRING_TYPE * pOctetStrValue,
                           UINT1 *pu1Data)
{
    UINT4               u4Count = 0, u4Dot = 0;
    UINT4               u4Value = 0;
    UINT1              *pu1String = pu1Data;
    INT1                i1RetVal = 0;
    pOctetStrValue->i4_Length = 0;
    for (u4Count = 0; pu1String[u4Count] != '\0'; u4Count++)
    {
        if (pu1String[u4Count] == '.')
            u4Dot++;
    }
    if (u4Dot == 0)
    {
        return SNMP_FAILURE;
    }
    for (u4Count = 0; u4Count < (u4Dot + 1); u4Count++)
    {
        if (isdigit ((INT4) *pu1String))
        {
            i1RetVal = SNMPMGRConvertSubOid (&pu1String, &u4Value);
            if (i1RetVal == -1)
            {
                return SNMP_FAILURE;
            }
            pOctetStrValue->pu1_OctetList[u4Count] = (UINT1) u4Value;
            pOctetStrValue->i4_Length++;
        }
        else
        {
            return SNMP_FAILURE;
        }
        if (*pu1String == '.')
        {
            pu1String++;
        }
        else if (*pu1String != '\0')
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name : SNMP_MGR_GetOidFromString                             */
/* Description   : To get Oid from Octet string form                     */
/* Input(s)      : Oid in Octet string form                              */
/* Output(s)     : Pointer to Oid                                        */
/* Returns       : pOid, in case of success                              */
/*                 NULL, in  failure cases                               */
/*************************************************************************/
tSNMP_OID_TYPE     *
SNMP_MGR_GetOidFromString (INT1 *pi1_str)
{
    INT4                i4DotCount;
    INT4                i4Int;
    UINT1              *pu1TempPtr = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    i4DotCount = 0;
    
    MEMSET(gau4_oidList, '\0', SNMP_MAX_OID_LENGTH);
    gtOidMgr.pu4_OidList = gau4_oidList;
    pOid = &gtOidMgr;

    for (i4Int = 0; pi1_str[i4Int] != '\0'; i4Int++)
    {
        if (pi1_str[i4Int] == '.')
            i4DotCount++;
    }

    if (i4DotCount > SNMP_MAX_OID_LENGTH)
    {
        return NULL;
    }

    pOid->u4_Length = 0;
    pu1TempPtr = (UINT1 *) pi1_str;

    for (i4Int = 0; i4Int < i4DotCount + 1; i4Int++)
    {
        if (isdigit ((INT4) *pu1TempPtr))
            pOid->pu4_OidList[i4Int] =
                (UINT4) SNMP_AGT_ConvSubOIDToLong (&pu1TempPtr);
        else
        {
            return (NULL);
        }
        if (*pu1TempPtr == '.')
            pu1TempPtr++;
        else if (*pu1TempPtr != '\0')
        {
            return (NULL);
        }
    }

    pOid->u4_Length = (UINT4) (++i4DotCount);
    return (pOid);
}

/*************************************************************************/
/* Function Name : GetMgrMbDbEntryForName                                */
/* Description   : To get DbEntry for given Oid in case of name          */
/* Input(s)      : pu1NumberIdx - Oid                                    */
/* Output(s)     : Pointer to DbEntry                                    */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
GetMgrMbDbEntryForName (CHR1 * pu1NumberIdx, tSnmpMgrDbEntry * pMgrDbEntry)
{
    INT4                      i4Count = 0;
    INT4                      i4Flag = 0;

    while(fsMgrMibEntry[i4Count].i1Oid != '\0')
    {
        if (STRCMP (pu1NumberIdx, fsMgrMibEntry[i4Count].i1Oid) == 0)
        {
            i4Flag = 1;
            break;
        }
        i4Count ++;
    }
    
    if (i4Flag == 1)
    {
        *pMgrDbEntry = fsMgrMibEntry[i4Count];
    }
    else 
    {
        return SNMP_FAILURE;
    }
    
    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name : SnmpMgrSetValueForOid                                 */
/* Description   : Send set request and receive the responce             */
/* Input(s)      : pName - Object name                                   */
/*                 tMgrDbEntry - DbEntry for object                      */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrSetValueForOid (UINT4 * u4IpAddr, CHR1 * pObjOid, UINT4 u4Port, CHR1 *pCommunity, 
                               tSnmpMgrDbEntry *tMgrDbEntry, UINT1 * pu1Val)
{
    CHR1                    au1NumberIdx[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE          *pOid = NULL; 
    tRetVal                 pMulti;
    UINT4                   u4_OidList[SNMP_MAX_OID_LENGTH];
    UINT1                   au1_Octet[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE tOctate;
    tSNMP_OID_TYPE          tOid;
    tSNMP_VAR_BIND          *tVar;
    UINT1                   au1SendPacket[MAX_PKT_LENGTH];
    UINT1                   *pu1SendPacket = NULL;
    INT4                    i4OutLen = 0;
 
    UNUSED_PARAM (u4Port);
    
    MEMSET (au1NumberIdx, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (u4_OidList, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1_Octet, '\0', SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1SendPacket, '\0', MAX_PKT_LENGTH);

    tOctate.pu1_OctetList = au1_Octet;
    tOid.pu4_OidList = u4_OidList;
    pMulti.pOctetStrValue = &tOctate;
    pMulti.pOidValue = &tOid;
    pu1SendPacket = au1SendPacket;

    /* Convert string type OID to OIDType */
    pOid = SNMP_MGR_GetOidFromString((INT1 *)pObjOid);
    if (pOid == NULL)
    {
        return SNMP_FAILURE;
    }
   
    /* Fill multidata */
    if (SnmpMgrConvertToMultidata ((UINT1)tMgrDbEntry->i4ObjType, 
                                            pu1Val, &pMulti) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    
    /* Form Varbind with the inputted data */
    tVar = SnmpCreateVarBind(pOid, &pMulti);
    if (tVar == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Encode packet */ 
    if (SnmpMgrEncodePacket(tVar, &pu1SendPacket, &i4OutLen, pCommunity,
                       SNMP_PDU_TYPE_SET_REQUEST) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    
    /* Send packet */
    if (SnmpMgrSendPacketForSet(u4IpAddr, pu1SendPacket, i4OutLen) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
     
}

/*************************************************************************/
/* Function Name : GetMgrMbDbEntryForOid                                 */
/* Description   : To get DbEntry for given Oid                          */
/* Input(s)      : pu1NumberIdx - Oid                                    */
/* Output(s)     : Pointer to DbEntry                                    */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
GetMgrMbDbEntryForOid (CHR1 * pu1NumberIdx, tSnmpMgrDbEntry * pMgrDbEntry)
{   
    tSNMP_OID_TYPE           *pOidFirst = NULL, tOid;
    tSNMP_OID_TYPE           *pOidSecond = NULL;
    INT4                     i4Count = 0;
    INT4                     i4Flag = 0;
    INT4                     i4Loop = 0;
    UINT4                    u4OidArray[SNMP_MAX_OID_LENGTH];
    CHR1                     au1TmpMgrMib[SNMP_MAX_OID_LENGTH];

    MEMSET(u4OidArray, 0, SNMP_MAX_OID_LENGTH);
    MEMSET(au1TmpMgrMib, '\0', SNMP_MAX_OID_LENGTH);

    pOidFirst = SNMP_MGR_GetOidFromString((INT1 *)pu1NumberIdx);
    if (pOidFirst == NULL)
    {
        return SNMP_FAILURE;
    }
    for (i4Loop = 0; i4Loop<(INT4)pOidFirst->u4_Length; i4Loop++)
    {
        u4OidArray[i4Loop] = pOidFirst->pu4_OidList[i4Loop];
    }

    tOid.u4_Length = pOidFirst->u4_Length;
    tOid.pu4_OidList = u4OidArray;

    while(fsMgrMibEntry[i4Count].i1Oid != '\0')
    {
        MEMSET(au1TmpMgrMib, '\0', SNMP_MAX_OID_LENGTH);
        STRCPY(au1TmpMgrMib, fsMgrMibEntry[i4Count].i1Oid);
        pOidSecond = SNMP_MGR_GetOidFromString((INT1 *)au1TmpMgrMib);
        if (pOidSecond != NULL)
        {
            i4Flag = 0;
            for (i4Loop = 0; i4Loop<(INT4)pOidSecond->u4_Length; i4Loop++)
            {
                if (pOidSecond->pu4_OidList[i4Loop] != tOid.pu4_OidList[i4Loop])
                {   
                    i4Flag = 1;
                    break;
                }
            }
            if (i4Flag == 0)
            { 
                *pMgrDbEntry = fsMgrMibEntry[i4Count];
                return SNMP_SUCCESS;
            }
             
        } 
        i4Count ++;
    }
    return SNMP_FAILURE;
}

/*************************************************************************/
/* Function Name : SnmpMgrSetValueForName                                */
/* Description   : Send set request and receive the responce             */
/* Input(s)      : pName - Object name                                   */
/*                 tMgrDbEntry - DbEntry for object                      */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrSetValueForName (UINT4 * u4IpAddr, UINT4 u4Port, CHR1 *pCommunity, CHR1 * pName, 
               tSnmpMgrDbEntry *tMgrDbEntry, UINT1 * pu1Val)
{
    CHR1                    au1NumberIdx[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE          *pOid = NULL; 
    tRetVal                 pMulti;
    UINT4                   u4_OidList[SNMP_MAX_OID_LENGTH];
    UINT1                   au1_Octet[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE tOctate;
    tSNMP_OID_TYPE          tOid;
    tSNMP_VAR_BIND          *tVar;
    UINT1                   au1SendPacket[MAX_PKT_LENGTH];
    UINT1                   *pu1SendPacket = NULL;
    INT4                    i4OutLen = 0;

    UNUSED_PARAM(u4Port);
 
    MEMSET (au1NumberIdx, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (u4_OidList, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1_Octet, '\0', SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1SendPacket, '\0', MAX_PKT_LENGTH);

    tOctate.pu1_OctetList = au1_Octet;
    tOid.pu4_OidList = u4_OidList;
    pMulti.pOctetStrValue = &tOctate;
    pMulti.pOidValue = &tOid;
    pu1SendPacket = au1SendPacket;
        
    /* Get the OID from the name */
    if (SnmpONGetOidFromName (pName, (char *) &au1NumberIdx) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Convert string type OID to OIDType */
    pOid = SNMP_MGR_GetOidFromString((INT1 *)au1NumberIdx);
    if (pOid == NULL)
    {
        return SNMP_FAILURE;
    }
   
    /* Fill multidata */
    if (SnmpMgrConvertToMultidata ((UINT1)tMgrDbEntry->i4ObjType, 
                                            pu1Val, &pMulti) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    
    /* Form Varbind with the inputted data */
    tVar = SnmpCreateVarBind(pOid, &pMulti);
    if (tVar == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Encode packet */ 
    if (SnmpMgrEncodePacket(tVar, &pu1SendPacket, &i4OutLen, pCommunity, 
                            SNMP_PDU_TYPE_SET_REQUEST) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    
    /* Send packet */
    if (SnmpMgrSendPacketForSet(u4IpAddr, pu1SendPacket, i4OutLen) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
     
}

/*************************************************************************/
/* Function Name : SnmpMgrConvertToMultidata                             */
/* Description   : Form multidata for snmp manager                       */
/* Input(s)      : u1Type - Object type                                  */
/*                 pu1Val - Value to be set                              */
/*                 tMgrDbEntry - DbEntry for object                      */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrConvertToMultidata (UINT1 u1Type, UINT1 * pu1Val, tRetVal *pData)
{  
    switch (u1Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            pData->i4_SLongValue = ATOI(pu1Val);
            pData->i2_DataType = SNMP_DATA_TYPE_INTEGER32;
            break;
        case SNMP_DATA_TYPE_UNSIGNED32:
            pData->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pData->u4_ULongValue = PTR_TO_U4(pu1Val);
            break;
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            SnmpMgrConvertStringToOctet (pData->pOctetStrValue, pu1Val);
            MEMCPY (&pData->u4_ULongValue, pData->pOctetStrValue->pu1_OctetList, 4);
            pData->u4_ULongValue = OSIX_NTOHL (pData->u4_ULongValue);           
            pData->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
            STRCPY (pData->pOctetStrValue->pu1_OctetList, pu1Val);
            pData->pOctetStrValue->i4_Length = (INT4) STRLEN (pu1Val);
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        case SNMP_DATA_TYPE_COUNTER32:
            pData->u4_ULongValue = PTR_TO_U4(pu1Val);
            pData->i2_DataType = SNMP_DATA_TYPE_COUNTER32;
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            pData->u4_ULongValue = PTR_TO_U4(pu1Val);
            pData->i2_DataType = SNMP_DATA_TYPE_COUNTER64;
            break; 
        case SNMP_DATA_TYPE_TIME_TICKS:
            pData->u4_ULongValue = PTR_TO_U4(pu1Val);
            pData->i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
            break; 
        default:
           return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name : SnmpMgrEncodePacket                                   */
/* Description   : Encode the packet to be send                          */
/* Input(s)      : pVar - Varbind containing data                        */
/* Output(s)     : pOut - Encoded packet                                 */
/*                 i4OutLen - Encoded packet length                      */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrEncodePacket(tSNMP_VAR_BIND * pVar, UINT1 **pOut, INT4 *i4OutLen, 
                                           CHR1 *pCommunity, INT2 i2PduType)
{
    tSNMP_NORMAL_PDU          pPdu;
    tSNMP_OCTET_STRING_TYPE   pCommunityStr;
    INT4                      i4Result = 0;

    pCommunityStr.pu1_OctetList = gau1CommunityData;
    MEMSET (gau1CommunityData, SNMP_ZERO, sizeof (gau1CommunityData));
    
    /* Fill the community details. If community is getting received as parameter
     * fill community details with that else fill default community as PUBLIC */
    if (pCommunity == NULL)
    {
        STRCPY(pCommunityStr.pu1_OctetList,"PUBLIC");
        pCommunityStr.i4_Length = 6;
    }
    else
    {
        STRCPY(pCommunityStr.pu1_OctetList, pCommunity);
        pCommunityStr.i4_Length = STRLEN (pCommunity);
    }

 
    /* Fill the PDU */
    pPdu.pVarBindList = pVar;
    pPdu.pVarBindEnd = pVar;
    pPdu.u4_Version = VERSION2;
    pPdu.i4_ErrorStatus = SNMP_ZERO;
    pPdu.i4_ErrorIndex = SNMP_ZERO;
    pPdu.i4_NonRepeaters = SNMP_ZERO;
    pPdu.i4_MaxRepetitions = SNMP_ZERO;
    pPdu.i2Padding = SNMP_ZERO;
    pPdu.pCommunityStr = &pCommunityStr;
    pPdu.i2_PduType = i2PduType;

    /* Encode the packet */
    i4Result = SNMPMGREncodeGetPacket(&pPdu, pOut, i4OutLen,
                                (UINT4)i2PduType);
    if (i4Result == SNMP_NOT_OK)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name : SnmpMgrSendPacketForSet                               */
/* Description   : Send set request and process the response             */
/* Input(s)      : u4IpAddr - Agent IP address                           */
/*                 pu1SendPkt - Packet to be send                        */
/*                 i4OutLen - Packet length                              */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrSendPacketForSet(UINT4 *u4IpAddr, UINT1 *pu1SendPkt, INT4 i4OutLen)
{
    fd_set                   Socks;
    struct timeval           tTimeout = {5,0};
    INT4                     i4RetVal = SNMP_ZERO;
    INT4                     i4Sock = SNMP_ZERO;
    INT4                     i4PktSize = SNMP_ZERO;
    struct sockaddr_in       SnmpMgrAddr;
    UINT4                    u4Len = SNMP_ZERO;
    UINT1                    *pu1PktArray = NULL;
    UINT1                    u1PktArray[MAX_PKT_LENGTH];
    INT2                     i2_PduType = SNMP_ZERO;
    VOID                     *pDecodedPtr = NULL;
    tSNMP_NORMAL_PDU         *pPtr = NULL;
    INT4                     Socket = 0;

    MEMSET (u1PktArray, '\0', MAX_PKT_LENGTH); 
    pu1PktArray = u1PktArray; 

    MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
    SnmpMgrAddr.sin_family = AF_INET;
    SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
    SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SnmpMgrAddr);
    
    if (pu1SendPkt != NULL)
    {
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL(*u4IpAddr);
        SnmpMgrAddr.sin_port = OSIX_HTONS (161);
        if (SnmpMgrSockInit(&i4Sock) == SNMP_FAILURE)
        {
            close (i4Sock);
            return SNMP_FAILURE;
        }
        Socket = i4Sock;

        if (sendto (Socket, pu1SendPkt, i4OutLen, SNMP_ZERO,
                   (struct sockaddr *) &SnmpMgrAddr,
                   (INT4) u4Len) == SNMP_NOT_OK)
        {
           close (i4Sock);
           return SNMP_FAILURE;
 
        }
        /* Handling for response packet */
        MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
        SnmpMgrAddr.sin_family = AF_INET;
        SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
        u4Len = sizeof (SnmpMgrAddr);
        /* Call select() for setting timeout */
        FD_ZERO(&Socks);
        FD_SET(Socket,&Socks);
        i4RetVal = select (Socket +1, &Socks, NULL, NULL, &tTimeout);
        if(i4RetVal == -1)
        {
            close (i4Sock);
            return SNMP_FAILURE;
        }
        else if (i4RetVal)
        {

          if ((i4PktSize = recvfrom (Socket, pu1PktArray,
                                  MAX_SNMP_PKT_LEN, 0,
                                  (struct sockaddr *) &SnmpMgrAddr,
                                  (socklen_t *) & u4Len)) > 0)
          {
               /* Received responce packet. Now process the packet */
               pDecodedPtr =
                    (VOID *) SNMPMGRDecodePacket (pu1PktArray,
                                   (INT4) i4PktSize, &i2_PduType);
               if (pDecodedPtr == NULL)
               {
                   close (i4Sock);
                   return SNMP_FAILURE;
               }
   
               pPtr = (tSNMP_NORMAL_PDU *) pDecodedPtr;
               if (pPtr == NULL)
               {
                   close (i4Sock);
                   return SNMP_FAILURE;
               }
   
               if (pPtr->i4_ErrorStatus == SNMP_ERR_NO_ERROR)
               {
                   printf("\nSet operation is successful\n");
               }
               else
               {
                   printf("\nSet opration failed\n");
                   close (i4Sock);
                   return SNMP_FAILURE;
               }

          }
       }
       else
       {
           printf("\nResponse timeout\n");
           close (i4Sock);
           return SNMP_FAILURE;
       }

    }
    close (i4Sock);
    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name : SnmpMgrGetValueForName                                */
/* Description   : Send set request and receive the responce             */
/* Input(s)      : pName - Object name                                   */
/*                 tMgrDbEntry - DbEntry for object                      */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrGetValueForName (UINT4 * u4IpAddr, CHR1 * pName, UINT4 u4Port,  CHR1 *pCommunity, 
          tSnmpMgrDbEntry *tMgrDbEntry, UINT1 *au1Output, UINT1 * au1GetNextOid, UINT4 u4PduReq)
{
    CHR1                    au1NumberIdx[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE          *pOid = NULL; 
    UINT4                   u4_OidList[SNMP_MAX_OID_LENGTH];
    UINT1                   au1_Octet[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_VAR_BIND          *tVar;
    UINT1                   au1SendPacket[MAX_PKT_LENGTH];
    UINT1                   *pu1SendPacket = NULL;
    INT4                    i4OutLen = 0;
    tSNMP_COUNTER64_TYPE     SnmpCounter64Type;    
 
    UNUSED_PARAM(u4Port); 

    MEMSET (au1NumberIdx, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (u4_OidList, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1_Octet, '\0', SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1SendPacket, '\0', MAX_PKT_LENGTH);


    pu1SendPacket = au1SendPacket;

    /* Counter initialization */    
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    /* Get the OID from the name */
    if (SnmpONGetOidFromName (pName, (char *) &au1NumberIdx) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Convert string type OID to OIDType */
    pOid = SNMP_MGR_GetOidFromString((INT1 *)au1NumberIdx);
    if (pOid == NULL)
    {
        return SNMP_FAILURE;
    }
   
    /* Create Varbind structure */ 
    tVar = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid,
                                  SNMP_DATA_TYPE_NULL,
                                  0, 0, NULL, NULL,
                                  SnmpCounter64Type);

    /* Encode packet */ 
    if (SnmpMgrEncodePacket(tVar, &pu1SendPacket, &i4OutLen, pCommunity, 
                             u4PduReq) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    
    /* Send packet */
    if (SnmpMgrSendPacketForGet(u4IpAddr, pu1SendPacket, 
                               i4OutLen, tMgrDbEntry->i4ObjType, 
                                       au1Output, au1GetNextOid, u4PduReq) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
     
}

/*************************************************************************/
/* Function Name : SnmpMgrSendPacketForGet                               */
/* Description   : Send get request and process the response             */
/* Input(s)      : u4IpAddr - Agent IP address                           */
/*                 pu1SendPkt - Packet to be send                        */
/*                 i4OutLen - Packet length                              */
/*                 i4Type - Object Type                                  */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrSendPacketForGet(UINT4 *u4IpAddr, UINT1 *pu1SendPkt, INT4 i4OutLen,
                                   INT4 i4Type, UINT1 * au1Output,
                                      UINT1 * au1GetNextOid, UINT4 u4PduReq)
{
    fd_set                   Socks;
    struct timeval           tTimeout = {5,0};
    INT4                     i4RetVal = SNMP_ZERO;
    INT4                     i4Sock = SNMP_ZERO;
    INT4                     i4PktSize = SNMP_ZERO;
    struct sockaddr_in       SnmpMgrAddr;
    UINT4                    u4Len = SNMP_ZERO;
    UINT1                    *pu1PktArray = NULL;
    UINT1                    u1PktArray[MAX_PKT_LENGTH];
    INT2                     i2_PduType = SNMP_ZERO;
    VOID                     *pDecodedPtr = NULL;
    tSNMP_NORMAL_PDU         *pPtr = NULL;
    INT4                     Socket = 0;
    tSNMP_MULTI_DATA_TYPE    *pGetMultiData = NULL;
    tSnmpMgrDbEntry          tTempMgrDbEntry;

    MEMSET (u1PktArray, '\0', MAX_PKT_LENGTH); 
    pu1PktArray = u1PktArray; 

    MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
    SnmpMgrAddr.sin_family = AF_INET;
    SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
    SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
    u4Len = sizeof (SnmpMgrAddr);
    
    if (pu1SendPkt != NULL)
    {
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL(*u4IpAddr);
        SnmpMgrAddr.sin_port = OSIX_HTONS (161);
        if (SnmpMgrSockInit(&i4Sock) == SNMP_FAILURE)
        {
            close (i4Sock);
            return SNMP_FAILURE;
        }
        Socket = i4Sock;

        if (sendto (Socket, pu1SendPkt, i4OutLen, SNMP_ZERO,
                   (struct sockaddr *) &SnmpMgrAddr,
                   (INT4) u4Len) == SNMP_NOT_OK)
        {
           close (i4Sock);
           return SNMP_FAILURE;
 
        }
        /* Handling for response packet */
        MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
        SnmpMgrAddr.sin_family = AF_INET;
        SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ZERO);
        SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL (INADDR_ANY);
        u4Len = sizeof (SnmpMgrAddr);
        /* Call select() for setting timeout */
        FD_ZERO(&Socks);
        FD_SET(Socket,&Socks);
        i4RetVal = select (Socket +1, &Socks, NULL, NULL, &tTimeout);
        if(i4RetVal == -1)
        {
            close (i4Sock);
            return SNMP_FAILURE;
        }
        else if (i4RetVal)
        {

           if ((i4PktSize = recvfrom (Socket, pu1PktArray,
                                   MAX_SNMP_PKT_LEN, 0,
                                   (struct sockaddr *) &SnmpMgrAddr,
                                   (socklen_t *) & u4Len)) > 0)
           {
              /* Received responce packet. Now process the packet */
              pDecodedPtr =
                  (VOID *) SNMPMGRDecodePacket (pu1PktArray,
                                      (INT4) i4PktSize, &i2_PduType);
              if (pDecodedPtr == NULL)
              {
                  close (i4Sock);
                  return SNMP_FAILURE;
              }

              pPtr = (tSNMP_NORMAL_PDU *) pDecodedPtr;
              if (pPtr == NULL)
              {
                  close (i4Sock);
                  return SNMP_FAILURE;
              }

              if (pPtr->i4_ErrorStatus != SNMP_ERR_NO_ERROR)
              {
                  printf("\nGet operation is not successful\n");
                  close (i4Sock);
                  return SNMP_FAILURE;
              }
              else
              {
                  /* Copy the value form received respons */
                  pGetMultiData = &pPtr->pVarBindList->ObjValue;
                  if((pGetMultiData->i2_DataType == SNMP_EXCEPTION_NO_SUCH_INSTANCE)||
                     (pGetMultiData->i2_DataType == SNMP_EXCEPTION_NO_SUCH_OBJECT)) 
                  {
                      close (i4Sock);
                      return SNMP_FAILURE;
                  }
                  /* Convert Multidata to string for display. If it is get-next
                   * then next object type should be fetched first. */
                  if (u4PduReq == SNMP_PDU_TYPE_GET_NEXT_REQUEST)
                  {
                      SNMPGetOidString (pPtr->pVarBindList->pObjName->pu4_OidList,
                                pPtr->pVarBindList->pObjName->u4_Length, au1GetNextOid);
                      if (GetMgrMbDbEntryForOid((CHR1 *)au1GetNextOid,
                                         &tTempMgrDbEntry) == SNMP_FAILURE)
                      {
                          return CLI_FAILURE;
                      }
                      i4Type = tTempMgrDbEntry.i4ObjType;
                      SnmpMgrConvertDataToString(pGetMultiData, au1Output, i4Type);
                  }
                  else
                  {
                      SnmpMgrConvertDataToString(pGetMultiData, au1Output, i4Type);
                  }
              }
 
           }
        }
        else
        {
            printf("\nResponse timeout\n");
            close (i4Sock);
            return SNMP_FAILURE;
        }

    }
    close (i4Sock);
    return SNMP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : SnmpMgrConvertDataToString              */
/*  Description     : This function is used to convert the     */
/*                    multidata to string.                     */
/*  Input(s)        : pData - Pointer to multidata             */
/*                    u1Type - Type of element in multidata    */
/*  Output(s)       : pu1String - pointer to a string where the*/
/*                    result will be written                   */
/*  Returns         : None                                     */
/***************************************************************/

VOID
SnmpMgrConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                             UINT1 *pu1String, INT4 i4Type)
{
    UINT4               u4Count = 0, u4Value = 0, u4Temp = 0;
    UINT1               au1Temp[SNMP_MAX_OID_LENGTH];

    pu1String[0] = '\0';
    MEMSET (au1Temp, 0, sizeof (au1Temp));

    switch (i4Type)
    {
        case SNMP_DATA_TYPE_INTEGER32:

            SPRINTF ((CHR1 *) pu1String, "%d", pData->i4_SLongValue);

            break;

        case SNMP_DATA_TYPE_UNSIGNED32:
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_TIME_TICKS:

            SPRINTF ((CHR1 *) pu1String, "%u", pData->u4_ULongValue);
            break;

        case SNMP_DATA_TYPE_COUNTER64:

            SPRINTF ((CHR1 *) pu1String, "%u.", pData->u8_Counter64Value.msn);
            SPRINTF ((CHR1 *) au1Temp, "%u", pData->u8_Counter64Value.lsn);
            STRCAT (pu1String, au1Temp);
            break;

        case SNMP_DATA_TYPE_IP_ADDR_PRIM:

            u4Value = pData->u4_ULongValue;

            for (u4Count = 0; u4Count < SNMP_MAX_IPADDR_LEN - 1; u4Count++)
            {
                u4Temp = (u4Value & 0xff000000);
                u4Temp = u4Temp >> 24;
                SPRINTF ((CHR1 *) au1Temp, "%u.", u4Temp);
                STRCAT (pu1String, au1Temp);
                u4Value = u4Value << SNMP3_BYTE_LEN;
            }

            u4Temp = (u4Value & 0xff000000);
            u4Temp = u4Temp >> 24;
            SPRINTF ((CHR1 *) au1Temp, "%u", u4Temp);
            STRCAT (pu1String, au1Temp);
            break;

        case SNMP_DATA_TYPE_OBJECT_ID:

            for (u4Count = 0; u4Count < pData->pOidValue->u4_Length; u4Count++)
            {
                if (u4Count == pData->pOidValue->u4_Length - 1)
                {
                    SPRINTF ((CHR1 *) au1Temp, "%u",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                else
                {
                    SPRINTF ((CHR1 *) au1Temp, "%u.",
                             pData->pOidValue->pu4_OidList[u4Count]);
                }
                STRCAT (pu1String, au1Temp);
            }
            break;
        case SNMP_DATA_TYPE_MAC_ADDRESS:
            /*Intentional fall through */
        case SNMP_DATA_TYPE_OCTET_PRIM:

            for (u4Count = 0; u4Count <
                 (UINT4) (pData->pOctetStrValue->i4_Length); u4Count++)
            {
                if (u4Count == (UINT4) (pData->pOctetStrValue->i4_Length - 1))
                {
                    SPRINTF ((CHR1 *) au1Temp, "%02x",
                             pData->pOctetStrValue->pu1_OctetList[u4Count]);
                    if (STRLEN (pu1String) + STRLEN (au1Temp) <
                        SNMP3_MAX_NAME_LEN)
                    {
                        STRCAT (pu1String, au1Temp);
                    }
                }
                else
                {
                    SPRINTF ((CHR1 *) au1Temp, "%02x",
                             pData->pOctetStrValue->pu1_OctetList[u4Count]);
                    if (STRLEN (pu1String) + STRLEN (au1Temp) + 1 <
                        SNMP3_MAX_NAME_LEN)
                    {
                        STRCAT (pu1String, au1Temp);
                        STRCAT (pu1String, ":");
                    }
                }
            }
            break;

        default:
            break;
    }
    return;
}

/*************************************************************************/
/* Function Name : SnmpMgrGetValueForOid                                 */
/* Description   : Send set request and receive the responce             */
/* Input(s)      : pName - Object name                                   */
/*                 tMgrDbEntry - DbEntry for object                      */
/* Returns       : SNMP_SUCCESS/SNMP_FAILURE                             */
/*************************************************************************/
INT1
SnmpMgrGetValueForOid (UINT4 * u4IpAddr, CHR1 * pObjOid, UINT4 u4Port, CHR1 *pCommunity, 
             tSnmpMgrDbEntry *tMgrDbEntry, UINT1 * pu1Val, UINT1 * au1GetNextOid, UINT4 u4PduReq)
{
    CHR1                    au1NumberIdx[SNMP_MAX_OID_LENGTH];
    tSNMP_OID_TYPE          *pOid = NULL; 
    UINT4                   u4_OidList[SNMP_MAX_OID_LENGTH];
    UINT1                   au1_Octet[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_VAR_BIND          *tVar;
    UINT1                   au1SendPacket[MAX_PKT_LENGTH];
    UINT1                   *pu1SendPacket = NULL;
    INT4                    i4OutLen = 0;
    tSNMP_COUNTER64_TYPE    SnmpCounter64Type;
    
    UNUSED_PARAM (u4Port);
 
    MEMSET (au1NumberIdx, '\0', SNMP_MAX_OID_LENGTH);
    MEMSET (u4_OidList, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1_Octet, '\0', SNMP_MAX_OCTETSTRING_SIZE);
    MEMSET (au1SendPacket, '\0', MAX_PKT_LENGTH);

    pu1SendPacket = au1SendPacket;
    
    SnmpCounter64Type.msn = 0;
    SnmpCounter64Type.lsn = 0;

    /* Convert string type OID to OIDType */
    pOid = SNMP_MGR_GetOidFromString((INT1 *)pObjOid);
    if (pOid == NULL)
    {
        return SNMP_FAILURE;
    }
   
    /* Create Varbind structure */ 
    tVar = (tSNMP_VAR_BIND *)SNMP_MGR_FormVarBind (pOid,
                                  SNMP_DATA_TYPE_NULL,
                                  0, 0, NULL, NULL,
                                  SnmpCounter64Type);

    /* Encode packet */ 
    if (SnmpMgrEncodePacket(tVar, &pu1SendPacket, &i4OutLen, pCommunity,
                                                   u4PduReq) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Send packet */
    if (SnmpMgrSendPacketForGet(u4IpAddr, pu1SendPacket, 
                               i4OutLen, tMgrDbEntry->i4ObjType, 
                                       pu1Val, au1GetNextOid, u4PduReq) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
     
}

INT4
SnmpMgrSockInit (INT4 *pi4Socket)
{
    struct sockaddr_in SnmpMgrAddr;
    INT4               i4Sock = SNMP_ZERO;

    MEMSET (&SnmpMgrAddr, SNMP_ZERO, sizeof (SnmpMgrAddr));
    SnmpMgrAddr.sin_family = AF_INET;
    SnmpMgrAddr.sin_addr.s_addr = OSIX_HTONL(INADDR_ANY);
    SnmpMgrAddr.sin_port = OSIX_HTONS (162);

    i4Sock = socket (AF_INET, SOCK_DGRAM, SNMP_ZERO);
    if (i4Sock < SNMP_ZERO)
    {
        printf ("SNMP:Unable to Open Socket\n");
        return SNMP_FAILURE;
    }
    *pi4Socket = i4Sock;
    if (bind (i4Sock, (struct sockaddr *) &SnmpMgrAddr, sizeof (SnmpMgrAddr))
        < SNMP_ZERO)
    {
        SnmpMgrAddr.sin_port = OSIX_HTONS (SNMP_ALT_PORT);
        if (bind (i4Sock, (struct sockaddr *) &SnmpMgrAddr, sizeof (SnmpMgrAddr))
            < SNMP_ZERO)
        {
            *pi4Socket = -1;
            close (i4Sock);
            printf ("SNMP:Unable to Bind Socket\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
SnmpMgrMemInit(VOID)
{
    UINT4                           u4Count = 0;
    for(u4Count=0;u4Count<SNMP_MAX_INDICES_2;u4Count++)
    {
        gapOctateSecond[u4Count].pu1_OctetList = (UINT1 *)calloc(1024,sizeof(UINT1));
        gapIndex[u4Count].pOctetStrValue = &gapOctateSecond[u4Count];
        gapIndex[u4Count].pOidValue = &gapOidSecond[u4Count];
        gapOidSecond[u4Count].pu4_OidList = (UINT4 *)calloc(1024,sizeof(UINT1));
    }

    for(u4Count=0;u4Count<SNMP_MAX_INDICES_2;u4Count++)
    {
        gapOctateThird[u4Count].pu1_OctetList = (UINT1 *)calloc(1024,sizeof(UINT1));
        gapIndexThird[u4Count].pOctetStrValue = &gapOctateThird[u4Count];
        gapIndexThird[u4Count].pOidValue = &gapOidThird[u4Count];
        gapOidThird[u4Count].pu4_OidList = (UINT4 *)calloc(1024,sizeof(UINT1));
    }  
   return SNMP_SUCCESS;
}
#endif /* __SNMMGR_UTILS_C__ */
