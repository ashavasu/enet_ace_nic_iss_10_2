#include "fssnmp.h"

#define nmhSetFsQoSShapeTemplateCIR(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.3.1.3",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhSetFsQoSShapeTemplateCBS(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.3.1.4",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhSetFsQoSShapeTemplateEIR(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.3.1.5",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhSetFsQoSShapeTemplateEBS(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.3.1.6",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhSetFsQoSShapeTemplateStatus(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.3.1.7",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetFsQoSShapeTemplateCIR(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.3.1.3", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66), 66, v)

#define nmhSetFsQoSQStatus(i1,i2,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.5.1.7",(tSnmpIndex *)ConvertToMultiIndex(2,2,i1,66,i2,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhSetFsQoSQShapeId(i1,i2,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.4.5.1.6",(tSnmpIndex *)ConvertToMultiIndex(2,2,i1,66,i2,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)

/* CLASS MAP TABLE */
#define nmhSetFsQoSClassMapStatus(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.9",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(2,v),  0, 0)
#define nmhGetFsQoSClassMapStatus(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.9", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2,v)
#define nmhSetFsQoSClassMapName(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.2",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(4,v), 0, 0)
#define nmhGetFsQoSClassMapName(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.2", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(4), 4, v)
#define nmhSetFsQoSClassMapL2FilterId(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.3",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)

#define nmhGetFsQoSClassMapL2FilterId(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.3", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),                        (tRetVal *)ConvertToGetMultiData(66), 66, v)
#define nmhSetFsQoSClassMapCLASS(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.6",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v),  0, 0)
#define nmhGetFsQoSClassMapCLASS(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.2.2.1.6", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66),   66, v)

/* POLICY MAP TABLE */
#define nmhSetFsQoSPolicyMapStatus(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.37",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(2,v),0, 0)
#define nmhGetFsQoSPolicyMapStatus(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.37", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetFsQoSPolicyMapCLASS(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.4",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhGetFsQoSPolicyMapCLASS(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.4", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66),  66, v)
#define nmhSetFsQoSPolicyMapMeterTableId(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.7",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),                      (tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhGetFsQoSPolicyMapMeterTableId(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.7", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),                     (tRetVal *)ConvertToGetMultiData(66), 66, v)

#define nmhSetFsQoSPolicyMapOutProfileActionFlag(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.28",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(4,v), 0, 0)
#define nmhGetFsQoSPolicyMapOutProfileActionFlag(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.28", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(4), 4, v)
#define nmhSetFsQoSPolicyMapOutProfileActionId(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.29",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhGetFsQoSPolicyMapOutProfileActionId(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.2.1.29", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66), 66, v)



/* METER TABLE */
#define nmhSetFsQoSMeterStatus(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.11",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetFsQoSMeterStatus(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.11", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetFsQoSMeterType(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.3",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetFsQoSMeterType(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.3", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetFsQoSMeterColorMode(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.5",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(2,v),  0, 0)
#define nmhGetFsQoSMeterColorMode(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.5", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2,v)
#define nmhSetFsQoSMeterCIR(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.6",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhGetFsQoSMeterCIR(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.6", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66), 66, v)
#define nmhSetFsQoSMeterCBS(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.7",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhGetFsQoSMeterCBS(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.7", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66), 66, v)
#define nmhSetFsQoSMeterEIR(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.8",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhGetFsQoSMeterEIR(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.8", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66), 66, v)
#define nmhSetFsQoSMeterEBS(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.9",(tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToSetMultiData(66,v), 0, 0)
#define nmhGetFsQoSMeterEBS(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.6.1.3.1.1.9", (tSnmpIndex *)ConvertToMultiIndex(1,66,i1,0),(tRetVal *)ConvertToGetMultiData(66), 66, v)

/* L2 FILTER TABLE */
#define nmhSetIssAclL2FilterPriority(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.2",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetIssAclL2FilterPriority(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.2", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetIssAclL2FilterVlanId(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.7",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetIssAclL2FilterVlanId(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.7", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetIssAclL2FilterInPortList(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.8",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(4,v), 0, 0)
#define nmhGetIssAclL2FilterInPortList(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.8", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(4), 4, v)
#define nmhSetIssAclL2FilterAction(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.9",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetIssAclL2FilterAction(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.9", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetIssAclL2FilterStatus(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.11",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetIssAclL2FilterStatus(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.11", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetIssAclL2FilterOutPortList(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.12",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(4,v), 0, 0)

#define nmhGetIssAclL2FilterOutPortList(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.12", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(4), 4, v)
#define nmhSetIssAclL2FilterDirection(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.13",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetIssAclL2FilterDirection(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.29601.2.21.2.1.1.13", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
#define nmhSetIssExtL2FilterVlanPriority(i1,v) CliSnmpMngrCommonOp(2, (unsigned char *)"1.3.6.1.4.1.2076.81.8.2.1.1.8",(tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToSetMultiData(2,v), 0, 0)
#define nmhGetIssExtL2FilterVlanPriority(i1,v) CliSnmpMngrCommonOp(1, (unsigned char *)"1.3.6.1.4.1.2076.81.8.2.1.1.8", (tSnmpIndex *)ConvertToMultiIndex(1,2,i1,0),(tRetVal *)ConvertToGetMultiData(2), 2, v)
