Please find the code developed for the macro substitution of nmh to SNMP Manager requests in the attachment (macro.tgz).
Kindly follow the below procedure to generate the macro definitions.

Prerequisite
All required MIB objects must be registered for it to be reflected in generated files.

Files
1.	mibawk.h -> Header file to be kept in code/future/snmpv3 and included in snmpmbdb.c file.
2.	snmpmbdb_new.c -> Function SNMPYangValidate (& related functions if any) and inclusion of mibawk.h should be merged in the file snmpmbdb.c.

Procedure
1.	Compile the ISS code along with this function in a package (preferably METRO_E where most of the MIBs are registered by default – Check and add any unregistered MIBs).
2.	Call the function SNMPYangValidate to get the required generated files.
a.	This function can also be included & invoked from CLI