/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* $Id: syeprotg.h,v 1.3 2016/07/06 11:05:10 siva Exp $ 
*
* This file contains prototypes for functions defined in Synce.
*********************************************************************/


#include "cli.h"

INT4 SynceCliSetFsSynceGlobalSysCtrl (tCliHandle CliHandle,UINT4 *pFsSynceGlobalSysCtrl);


INT4 SynceCliSetFsSynceTable (tCliHandle CliHandle,tSynceFsSynceEntry * pSynceSetFsSynceEntry,tSynceIsSetFsSynceEntry * pSynceIsSetFsSynceEntry);

INT4 SynceCliSetFsSynceIfTable (tCliHandle CliHandle,tSynceFsSynceIfEntry * pSynceSetFsSynceIfEntry,tSynceIsSetFsSynceIfEntry * pSynceIsSetFsSynceIfEntry);


PUBLIC INT4 SynceUtlCreateRBTree PROTO ((VOID));
PUBLIC INT4 SynceUtlDestroyRBTree PROTO ((VOID));

PUBLIC INT4 SynceLock (VOID);

PUBLIC INT4 SynceUnLock (VOID);
PUBLIC INT4 FsSynceTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 SynceFsSynceTableCreate PROTO ((VOID));

tSynceFsSynceEntry * SynceFsSynceTableCreateApi PROTO ((tSynceFsSynceEntry *));

PUBLIC INT4 FsSynceIfTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 SynceFsSynceIfTableCreate PROTO ((VOID));

tSynceFsSynceIfEntry * SynceFsSynceIfTableCreateApi PROTO ((tSynceFsSynceIfEntry *));
INT4 SynceGetFsSynceGlobalSysCtrl PROTO ((INT4 *));
INT4 SynceGetAllFsSynceTable PROTO ((tSynceFsSynceEntry *));
INT4 SynceGetAllUtlFsSynceTable PROTO((tSynceFsSynceEntry *, tSynceFsSynceEntry *));
INT4 SynceGetAllFsSynceIfTable PROTO ((tSynceFsSynceIfEntry *));
INT4 SynceGetAllUtlFsSynceIfTable PROTO((tSynceFsSynceIfEntry *, tSynceFsSynceIfEntry *));
INT4 SynceSetFsSynceGlobalSysCtrl PROTO ((INT4 ));
INT4 SynceSetAllFsSynceTable PROTO ((tSynceFsSynceEntry *, tSynceIsSetFsSynceEntry *, INT4 , INT4 ));
INT4 SynceSetAllFsSynceIfTable PROTO ((tSynceFsSynceIfEntry *, tSynceIsSetFsSynceIfEntry *, INT4 , INT4 ));

INT4 SynceInitializeFsSynceTable PROTO ((tSynceFsSynceEntry *));

INT4 SynceInitializeMibFsSynceTable PROTO ((tSynceFsSynceEntry *));

INT4 SynceInitializeFsSynceIfTable PROTO ((tSynceFsSynceIfEntry *));

INT4 SynceInitializeMibFsSynceIfTable PROTO ((tSynceFsSynceIfEntry *));
INT4 SynceTestFsSynceGlobalSysCtrl PROTO ((UINT4 *,INT4 ));
INT4 SynceTestAllFsSynceTable PROTO ((UINT4 *, tSynceFsSynceEntry *, tSynceIsSetFsSynceEntry *, INT4 , INT4));
INT4 SynceTestAllFsSynceIfTable PROTO ((UINT4 *, tSynceFsSynceIfEntry *, tSynceIsSetFsSynceIfEntry *, INT4 , INT4));
tSynceFsSynceEntry * SynceGetFsSynceTable PROTO ((tSynceFsSynceEntry *));
tSynceFsSynceIfEntry * SynceGetFsSynceIfTable PROTO ((tSynceFsSynceIfEntry *));
tSynceFsSynceEntry * SynceGetFirstFsSynceTable PROTO ((VOID));
tSynceFsSynceIfEntry * SynceGetFirstFsSynceIfTable PROTO ((VOID));
tSynceFsSynceEntry * SynceGetNextFsSynceTable PROTO ((tSynceFsSynceEntry *));
tSynceFsSynceIfEntry * SynceGetNextFsSynceIfTable PROTO ((tSynceFsSynceIfEntry *));
INT4 FsSynceTableFilterInputs PROTO ((tSynceFsSynceEntry *, tSynceFsSynceEntry *, tSynceIsSetFsSynceEntry *));
INT4 FsSynceIfTableFilterInputs PROTO ((tSynceFsSynceIfEntry *, tSynceFsSynceIfEntry *, tSynceIsSetFsSynceIfEntry *));
INT4 SynceUtilUpdateFsSynceTable PROTO ((tSynceFsSynceEntry *, tSynceFsSynceEntry *, tSynceIsSetFsSynceEntry *));
INT4 SynceUtilUpdateFsSynceIfTable PROTO ((tSynceFsSynceIfEntry *, tSynceFsSynceIfEntry *, tSynceIsSetFsSynceIfEntry *));
INT4 SynceSetAllFsSynceTableTrigger PROTO ((tSynceFsSynceEntry *, tSynceIsSetFsSynceEntry *, INT4));
INT4 SynceSetAllFsSynceIfTableTrigger PROTO ((tSynceFsSynceIfEntry *, tSynceIsSetFsSynceIfEntry *, INT4));

INT4
SynceSetFsSynceIfCntrs (UINT4 u4FsSynceIfIndex, UINT4 u4PktsCount, UINT4 u4CntrType);
INT4
SynceTestFsSynceIfCntrs (UINT4 *pu4ErrorCode, UINT4 u4FsSynceIfIndex, UINT4 u4PktsCount);

INT4
SynceIsSetForceSwitchMode (UINT4 u4ContextId);

void
SynceResetSwitchMode (UINT4 u4ContextId, UINT4 u4SelectedInterface);
