/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
* $Id: syesrdef.h,v 1.3 2016/07/06 11:05:10 siva Exp $
*
* Description: This file contains the default values for Synce module.
*********************************************************************/


#ifndef __SYNCESRCDEFN_H__
#define __SYNCESRCDEFN_H__


#define  SYNCE_SUCCESS                    (OSIX_SUCCESS)
#define  SYNCE_FAILURE                    (OSIX_FAILURE)
#define  SYNCE_TASK_PRIORITY              100
#define  SYNCE_TASK_NAME                  "SYNCE"
#define  SYNCE_QUEUE_NAME                 (const UINT1 *) "SYNCEQ"
#define  SYNCE_MUT_EXCL_SEM_NAME          (const UINT1 *) "SYNCEM"
#define  SYNCE_QUEUE_DEPTH                MAX_SYNCE_PORTS
#define  SYNCE_SEM_CREATE_INIT_CNT        1

#define  SYNCE_TIMER_EVENT                0x00000001
#define  SYNCE_QUEUE_EVENT                0x00000002
#define  SYNCE_SYSTEM_QL_EVENT            0x00000004

/*********** macros for scalar objects*********************/

#define SYNCE_DEF_FSSYNCEGLOBALSYSCTRL             1

/*********** macros for tabular objects*********************/

#define SYNCE_DEF_FSSYNCETRACEOPTION             512
#define SYNCE_DEF_FSSYNCEQLMODE                  1
#define SYNCE_DEF_FSSYNCEQLVALUE                 5
#define SYNCE_DEF_FSSYNCESSMOPTIONMODE           1
#define SYNCE_DEF_FSSYNCEIFSYNCEMODE             0
#define SYNCE_DEF_FSSYNCEIFESMCMODE              1
#define SYNCE_DEF_FSSYNCEIFPRIORITY              0
#define SYNCE_DEF_FSSYNCEIFQLVALUE               5
#define SYNCE_DEF_FSSYNCEIFLOCKOUTSTATUS         0
#define SYNCE_DEF_FSSYNCEIFCONFIGSWITCH          0

#endif  /* __SYNCEDEFN_H__*/
/*-----------------------------------------------------------------------*/
/*                       End of the file syncedefn.h                      */
/*-----------------------------------------------------------------------*/

