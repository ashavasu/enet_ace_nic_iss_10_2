/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syetdfsg.h,v 1.2 2016/07/06 11:05:10 siva Exp $ 
*
* Description: This file contains data structures defined for Synce module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsSynceEntry */

#ifndef __SYNCETDFSG_H__
#define __SYNCETDFSG_H__

typedef struct
{
 BOOL1  bFsSynceContextId;
 BOOL1  bFsSynceTraceOption;
 BOOL1  bFsSynceQLMode;
 BOOL1  bFsSynceSSMOptionMode;
 BOOL1  bFsSynceContextRowStatus;
} tSynceIsSetFsSynceEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsSynceIfEntry */

typedef struct
{
 BOOL1  bFsSynceIfIndex;
 BOOL1  bFsSynceIfSynceMode;
 BOOL1  bFsSynceIfEsmcMode;
 BOOL1  bFsSynceIfPriority;
 BOOL1  bFsSynceIfQLValue;
 BOOL1  bFsSynceIfLockoutStatus;
 BOOL1  bFsSynceIfConfigSwitch;
 BOOL1  bFsSynceIfRowStatus;
} tSynceIsSetFsSynceIfEntry;



/* Structure used by Synce protocol for FsSynceEntry */

typedef struct
{
 tRBNodeEmbd  FsSynceTableNode;
 UINT4 u4FsSynceTraceOption;
 UINT4 u4FsSynceQLValue;
 INT4 i4FsSynceContextId;
 INT4 i4FsSynceQLMode;
 INT4 i4FsSynceSSMOptionMode;
 INT4 i4FsSynceSelectedInterface;
 INT4 i4FsSynceContextRowStatus;
    UINT1 u1Padding[4];
} tSynceMibFsSynceEntry;

/* Structure used by Synce protocol for FsSynceIfEntry */

typedef struct
{
 tRBNodeEmbd  FsSynceIfTableNode;
 UINT4 u4FsSynceIfPktsTx;
 UINT4 u4FsSynceIfPktsRx;
 UINT4 u4FsSynceIfPktsRxDropped;
 UINT4 u4FsSynceIfPktsRxErrored;
 INT4 i4FsSynceIfIndex;
 INT4 i4FsSynceIfSynceMode;
 INT4 i4FsSynceIfEsmcMode;
 INT4 i4FsSynceIfPriority;
    UINT4 u4FsSynceIfQLValue;
 INT4 i4FsSynceIfIsRxQLForced;
 INT4 i4FsSynceIfLockoutStatus;
 INT4 i4FsSynceIfSignalFail;
 INT4 i4FsSynceIfRowStatus;
 INT4 i4FsSynceIfConfigSwitch;
} tSynceMibFsSynceIfEntry;

#endif
