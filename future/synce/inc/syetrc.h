/********************************************************************
 * Copyright (C) 2013  Aricent Inc . All Rights Reserved 
 *
 * $Id: syetrc.h,v 1.1 2013/03/23 12:41:36 siva Exp $
 *
 * Description:This file contains procedures and definitions
 *             used for debugging.
 *******************************************************************/


#ifndef __SYNCETRC_H__
#define __SYNCETRC_H__

#define  SYNCE_GBL_TRC_FLAG  gSynceGlobals.u4GblTrc
#define  SYNCE_CTX_TRC_FLAG  gau4SynceTraceOptions[SYNCE_DEFAULT_CXT_ID]
#define  SYNCE_MOD_NAME      (const CHR1 *)"SYNCE"

#ifdef SYSLOG_WANTED
#define  SYNCE_LOG(x)       SYS_LOG_MSG( x );
#else
#define  SYNCE_LOG(x)
#endif

#ifdef TRACE_WANTED
#define  SYNCE_FN_ENTRY() MOD_FN_ENTRY (SYNCE_CTX_TRC_FLAG, SYNCE_FN_ENTRY_TRC, \
                                         SYNCE_MOD_NAME)

#define  SYNCE_FN_EXIT()  MOD_FN_EXIT (SYNCE_CTX_TRC_FLAG, SYNCE_FN_EXIT_TRC, \
                                        SYNCE_MOD_NAME)


#define  SYNCE_TRC(x)     SynceTrcPrint( __FUNCTION__, __LINE__, SynceTrc x)

#define  SYNCE_TRC_DUMP(x) SynceTrcWrite(SynceTrc x);

#else /* TRACE_WANTED */

#define  SYNCE_FN_ENTRY(x)
#define  SYNCE_FN_EXIT(x)
#define  SYNCE_TRC(x)
#define  SYNCE_TRC_DUMP(x)

#endif /* TRACE_WANTED */


#endif /* _SYNCETRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  syncetrc.h                      */
/*-----------------------------------------------------------------------*/
