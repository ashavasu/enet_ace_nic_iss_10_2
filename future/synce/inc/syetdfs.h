/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syetdfs.h,v 1.3 2016/07/06 11:05:10 siva Exp $ 
*
* Description: This file contains type definitions for Synce module.
*********************************************************************/
#ifndef __SYNCETDFS_H__
#define __SYNCETDFS_H__

#include "syesm.h"
#include "syetmr.h"

typedef struct
{

    tRBTree   FsSynceTable;
    tRBTree   FsSynceIfTable;
    INT4 i4FsSynceGlobalSysCtrl;
    UINT1     u1Padding[4];
} tSynceGlbMib;

typedef struct _SynceFsSynceEntry
{
    tSynceMibFsSynceEntry  MibObject;
    eSynceSmState          smState;
    UINT1                  u1Padding[4];
} tSynceFsSynceEntry;

typedef struct
{
    tSynceMibFsSynceIfEntry  MibObject;
    tTmrBlk                  aTimers[SYNCE_MAX_TMRS];
    /* flag that govern if event pdu need to be send on this interface */
    BOOL1                    b1TxEventPdu;
    UINT1                    u1Padding[7];
} tSynceFsSynceIfEntry;

/** NOTE : DO NOT CHANGE THE VALUE OF THESE ENUM
 *  THEY WILL MAKE THIS PROGRAM CRASH
 *  THESE VALUE ARE USED FOR DISPLAY PURPOSE IN CLI
 */
typedef enum
{
    SYNCE_SIGNAL_RESTORE,
    SYNCE_SIGNAL_FAIL
}eSynceSignalFailStatus;

typedef enum
{
    SYNCE_IF_PKT_RCV,
    SYNCE_IF_CREATE,
    SYNCE_IF_DELETE,
    SYNCE_IF_OPER_ST_CHG
} eSynceMsgType;

typedef enum
{
    SYNCE_QL_MODE_DISABLE,
    SYNCE_QL_MODE_ENABLE
} eSynceQlMode;

/* Switch Modes */
typedef enum
{
    SYNCE_NO_SWITCH_MODE,
    SYNCE_FORCED_SWITCH_MODE,
    SYNCE_MANUAL_SWITCH_MODE
} eSynceSwtichMode;

/* Esmc Modes */
typedef enum
{
    SYNCE_ESMC_MODE_NONE = 0,
    SYNCE_ESMC_MODE_RX = 1,
    SYNCE_ESMC_MODE_TX = 2
} eSynceEsmcMode;

/* SSM option modes */
typedef enum
{
    SYNCE_SSM_OPTION1 = 1,
    SYNCE_SSM_OPTION2GEN1 = 2,
    SYNCE_SSM_OPTION2GEN2 = 3,
    SYNCE_SSM_OPTION_MAX = 3
} eSynceSsmOption;

/* Clock QL Values
 * These values are mapped to SyncE MIB
 * assigned values to the symbols does not represent SSM code.
 * SSM code value will be different according to the selected SSM option.
 * */
typedef enum
{
    SYNCE_QL_PRC    = 1,
    SYNCE_QL_SSU_A  = 2,
    SYNCE_QL_SSU_B  = 3,
    SYNCE_QL_SEC    = 4,
    SYNCE_QL_DNU    = 5,
    SYNCE_QL_PRS    = 6,
    SYNCE_QL_STU    = 7,
    SYNCE_QL_ST2    = 8,
    SYNCE_QL_TNC    = 9,
    SYNCE_QL_ST3E   = 10,
    SYNCE_QL_ST3    = 11,
    SYNCE_QL_SMC    = 12,
    SYNCE_QL_RES    = 13,
    SYNCE_QL_PROV   = 14,
    SYNCE_QL_DUS    = 15,
    SYNCE_QL_MAX    = 16,
    SYNCE_QL_UNDEF  = 0,
} eSynceClockQl;

/* trap types */
typedef enum
{
    SYNCE_SIGNAL_FAIL_TRAP,
    SYNCE_QL_CHANGE_TRAP
} eSynceTrapTypes;

/* QL source */
typedef enum
{
    SYNCE_QL_SOURCE_USER,
    SYNCE_QL_SOURCE_PDU
} eSynceQlSource;

/* module start/shutdown */
typedef enum
{
    SYNCE_MODULE_START = 1,
    SYNCE_MODULE_SHUTDOWN = 2
} eSynceModuleStatus;

/* -------------------------------------------------------
 *
 *                SYNCE   Data Structures
 *
 * ------------------------------------------------------*/

typedef struct SYNCE_GLOBALS
{
    tSynceTmrDesc       aTmrDesc[SYNCE_MAX_TMRS];
    tSynceGlbMib        glbMib;
    UINT1               au1TaskSemName[8];
    tTimerListId        tmrList;
    tOsixSemId          taskSemId;
    tOsixQId            taskQueId;
    tOsixTaskId         taskId;
    tMemPoolId          contextPoolId;
    tMemPoolId          ifPoolId;
    tMemPoolId          msgQPoolId;
    tMemPoolId          pktPoolId;
    UINT4               u4GblTrc;
    UINT4               u4SysLogId;
    UINT2               u2TrapControl;
    UINT1               u1Padding[2];
} tSynceGlobals;

/* Structure used by callback function for storing message */
typedef struct
{
    union
    {
         tCRU_BUF_CHAIN_DESC  *pSyncePdu;
         VOID *pSynceMsg;
    } uSynceMsgParam;
    UINT4 u4ContextId;
    UINT4 u4MsgType;
    UINT4 u4IfIndex;
    UINT4 u4PktLen;
    UINT1 u1IfOperStatus;
    UINT1 u1Padding[7];
} tSynceQMsg;

typedef struct
{
    UINT1 *pu1Symbol;
    UINT1 u1QlCode; /* this is the symbolic numbers to be used while
                       configuration using CLI or SNMP */
    BOOL1 b1IsValid;
    UINT1 u1Padding[6];
} tSynceSsmCodeInfo;

/* structure to hold ESMC packet */
typedef struct
{
    tSynceFsSynceIfEntry *pSynceIfEntry; /* pointer to interface structure */
    UINT1   *pu1PduBuf; /* pointer to the pdu buffer*/
    UINT4    u4PduLen; /* Length of the received PDU */
    UINT1    u4Padding[4];
} tSyncePduParams;

typedef struct
{
    UINT1   u1SsmCode;
    UINT1   u1EventFlag;
    UINT1   u1Padding[6];
} tSynceSsmParams;

#endif  /* __SYNCETDFS_H__ */
/*-----------------------------------------------------------------------*/


