/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syemacs.h,v 1.2 2013/04/25 11:36:31 siva Exp $
 *
 * Description: This file contains macro definitions for synce module.
 *******************************************************************/

#ifndef __SYNCEMACS_H__
#define __SYNCEMACS_H__

/*********************** Macro Definition ********************************/
#define SYNCE_GET_OFFSET(StructType,Member)  (UINT2)(&(((StructType*)0)->Member))

#define SYNCE_LOCK  SynceApiLock ()
#define SYNCE_UNLOCK  SynceApiUnLock ()

/** Current Context related macros */
#define SYNCE_SELECT_CONTEXT(u4ContextId)\
        SynceUtilSelectContext (u4ContextId)
#define SYNCE_RELEASE_CONTEXT()\
        SynceUtilReleseContext()
#define SYNCE_CURR_CONTEXT_INFO() gpSynceContextInfo
#define SYNCE_CURR_CONTEXT_ID() (SYNCE_CURR_CONTEXT_INFO())->MibObject.i4FsSynceContextId

/** buffer related macros */
#define SYNCE_READ_2_BYTES(pBuf, u2Value) \
{\
    u2Value =  (UINT2) (*pBuf++ << 8);\
    u2Value =  (UINT2) (u2Value | *pBuf++);\
}

#define SYNCE_WRITE_N_BYTES(pBuf, pValue, u2Len) \
{\
    MEMCPY (pBuf, pValue, u2Len);\
    pBuf += u2Len;\
}

#define SYNCE_WRITE_2_BYTES(pBuf, u2Value) \
{\
    UINT2 u2TmpVal = OSIX_HTONS(u2Value);\
    MEMCPY (pBuf, &u2TmpVal, 2);\
    pBuf += 2;\
}

/** CRU macros **/
#define SYNCE_CRU_GET_1_BYTE(pMsg, u4Offset, u1Value) \
   CRU_BUF_Copy_FromBufChain(pMsg, &u1Value, u4Offset, 1)

#define SYNCE_CRU_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}

#define SYNCE_CRU_GET_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *) &u4Value), u4Offset, 4);\
   u4Value = OSIX_NTOHL (u4Value);\
}

#define SYNCE_CRU_GET_STRING(pBufChain,pu1_StringMemArea,u4Offset,u4_StringLength) \
{\
   CRU_BUF_Copy_FromBufChain(pBufChain, pu1_StringMemArea, u4Offset, u4_StringLength);\
}


#endif /* __SYNCEMACS_H__ */
