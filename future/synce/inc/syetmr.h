/********************************************************************
 * Copyright (C) 2013  Aricent Inc . All Rights Reserved
 *
 * $Id: syetmr.h,v 1.1 2013/03/23 12:41:36 siva Exp $
 *
 * Description: This file contains definitions for synce Timer
 *******************************************************************/

#ifndef __SYNCETMR_H__
#define __SYNCETMR_H__

/* constants for timer types */
typedef enum {
    SYNCE_RX_TMR = 0,
    SYNCE_TX_TMR,
    SYNCE_MAX_TMRS
} eSynceTmrType;

/* timer default interval */
enum {
    SYNCE_DEF_SIGNAL_FAIL_TIME = 5,
    SYNCE_DEF_PACKET_TX_TIME = 1
};

typedef enum {
    SYNCE_MILLISEC,
    SYNCE_SECOND,
    SYNCE_MINUTE
} eSynceTmrUnit;

typedef struct {
    UINT4 u4MinTime;
    UINT4 u4MaxTime;
    eSynceTmrUnit tmrUnit;
} tSynceTimerRange;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _SYNCE_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad[3];
                            /* Included for 8-byte Alignment
                             */
} tSynceTmrDesc;

/* external declarations*/
extern tSynceTimerRange gaSynceTmrRange[];
extern UINT4 gaSynceTmrDefInterval[];

/***************** Function Prototypes **********************************/
VOID SynceTmrSignalFailHandler(VOID *);
VOID SynceTmrTxTimerHandler(VOID *);

#endif  /* __SYNCETMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  syncetmr.h                      */
/*-----------------------------------------------------------------------*/
