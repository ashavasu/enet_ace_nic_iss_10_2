/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syedefg.h,v 1.3 2016/07/06 11:05:10 siva Exp $ 
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/


#define SYNCE_FSSYNCETABLE_POOLID   SYNCEMemPoolIds[MAX_SYNCE_FSSYNCETABLE_SIZING_ID]
#define SYNCE_FSSYNCETABLE_ISSET_POOLID   SYNCEMemPoolIds[MAX_SYNCE_FSSYNCETABLE_ISSET_SIZING_ID]
#define SYNCE_FSSYNCEIFTABLE_POOLID   SYNCEMemPoolIds[MAX_SYNCE_FSSYNCEIFTABLE_SIZING_ID]
#define SYNCE_FSSYNCEIFTABLE_ISSET_POOLID   SYNCEMemPoolIds[MAX_SYNCE_FSSYNCEIFTABLE_ISSET_SIZING_ID]
#define SYNCE_FSSYNCEMESSAGEQUEUE_POOLID   SYNCEMemPoolIds[MAX_SYNCE_MESSAGEQUEUE_SIZING_ID]
#define SYNCE_ESMC_PKT_POOLID   SYNCEMemPoolIds[MAX_SYNCE_ESMC_PKT_SIZING_ID]

/* Macro used to fill the CLI structure for FsSynceEntry 
 using the input given in def file */

#define  SYNCE_FILL_FSSYNCETABLE_ARGS(pSynceFsSynceEntry,\
   pSynceIsSetFsSynceEntry,\
   pau1FsSynceContextId,\
   pau1FsSynceTraceOption,\
   pau1FsSynceQLMode,\
   pau1FsSynceSSMOptionMode,\
   pau1FsSynceContextRowStatus)\
  {\
  if (pau1FsSynceContextId != NULL)\
  {\
   pSynceFsSynceEntry->MibObject.i4FsSynceContextId = *(INT4 *) (pau1FsSynceContextId);\
   pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_FALSE;\
  }\
  if (pau1FsSynceTraceOption != NULL)\
  {\
   pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption = *(UINT4 *) (pau1FsSynceTraceOption);\
   pSynceIsSetFsSynceEntry->bFsSynceTraceOption = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceEntry->bFsSynceTraceOption = OSIX_FALSE;\
  }\
  if (pau1FsSynceQLMode != NULL)\
  {\
   pSynceFsSynceEntry->MibObject.i4FsSynceQLMode = *(INT4 *) (pau1FsSynceQLMode);\
   pSynceIsSetFsSynceEntry->bFsSynceQLMode = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceEntry->bFsSynceQLMode = OSIX_FALSE;\
  }\
  if (pau1FsSynceSSMOptionMode != NULL)\
  {\
   pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode = *(INT4 *) (pau1FsSynceSSMOptionMode);\
   pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode = OSIX_FALSE;\
  }\
  if (pau1FsSynceContextRowStatus != NULL)\
  {\
   pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus = *(INT4 *) (pau1FsSynceContextRowStatus);\
   pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsSynceIfEntry 
 using the input given in def file */

#define  SYNCE_FILL_FSSYNCEIFTABLE_ARGS(pSynceFsSynceIfEntry,\
   pSynceIsSetFsSynceIfEntry,\
   pau1FsSynceIfIndex,\
   pau1FsSynceIfSynceMode,\
   pau1FsSynceIfEsmcMode,\
   pau1FsSynceIfPriority,\
   pau1FsSynceIfQLValue,\
   pau1FsSynceIfLockoutStatus,\
   pau1FsSynceIfConfigSwitch,\
   pau1FsSynceIfRowStatus)\
  {\
  if (pau1FsSynceIfIndex != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = *(INT4 *) (pau1FsSynceIfIndex);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsSynceIfSynceMode != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode = *(INT4 *) (pau1FsSynceIfSynceMode);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode = OSIX_FALSE;\
  }\
  if (pau1FsSynceIfEsmcMode != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode = *(INT4 *) (pau1FsSynceIfEsmcMode);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode = OSIX_FALSE;\
  }\
  if (pau1FsSynceIfPriority != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority = *(INT4 *) (pau1FsSynceIfPriority);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority = OSIX_FALSE;\
  }\
  if (pau1FsSynceIfQLValue != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue = *(UINT4 *) (pau1FsSynceIfQLValue);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue = OSIX_FALSE;\
  }\
  if (pau1FsSynceIfLockoutStatus != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus = *(INT4 *) (pau1FsSynceIfLockoutStatus);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus = OSIX_FALSE;\
  }\
  if (pau1FsSynceIfConfigSwitch != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch = *(INT4 *) (pau1FsSynceIfConfigSwitch);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch = OSIX_FALSE;\
  }\
  if (pau1FsSynceIfRowStatus != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus = *(INT4 *) (pau1FsSynceIfRowStatus);\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsSynceEntry 
 using the input given in def file */

#define  SYNCE_FILL_FSSYNCETABLE_INDEX(pSynceFsSynceEntry,\
   pau1FsSynceContextId)\
  {\
  if (pau1FsSynceContextId != NULL)\
  {\
   pSynceFsSynceEntry->MibObject.i4FsSynceContextId = *(INT4 *) (pau1FsSynceContextId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsSynceIfEntry 
 using the input given in def file */

#define  SYNCE_FILL_FSSYNCEIFTABLE_INDEX(pSynceFsSynceIfEntry,\
   pau1FsSynceIfIndex)\
  {\
  if (pau1FsSynceIfIndex != NULL)\
  {\
   pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = *(INT4 *) (pau1FsSynceIfIndex);\
  }\
 }
/* Macro used to convert the input 
  to required datatype for FsSynceGlobalSysCtrl*/

#define  SYNCE_FILL_FSSYNCEGLOBALSYSCTRL(i4FsSynceGlobalSysCtrl,\
   pau1FsSynceGlobalSysCtrl)\
  {\
  if (pau1FsSynceGlobalSysCtrl != NULL)\
  {\
   i4FsSynceGlobalSysCtrl = *(INT4 *) (pau1FsSynceGlobalSysCtrl);\
  }\
  }


