/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fssyncdb.h,v 1.3 2016/07/06 11:05:10 siva Exp $ 
*
* Description: Protocol Mib Data base
*********************************************************************/


#ifndef _FSSYNCDB_H
#define _FSSYNCDB_H

UINT1 FsSynceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsSynceIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fssync [] ={1,3,6,1,4,1,29601,2,79};
tSNMP_OID_TYPE fssyncOID = {9, fssync};


UINT4 FsSynceGlobalSysCtrl [ ] ={1,3,6,1,4,1,29601,2,79,1,1,1};
UINT4 FsSynceContextId [ ] ={1,3,6,1,4,1,29601,2,79,1,1,2,1,1};
UINT4 FsSynceTraceOption [ ] ={1,3,6,1,4,1,29601,2,79,1,1,2,1,2};
UINT4 FsSynceQLMode [ ] ={1,3,6,1,4,1,29601,2,79,1,1,2,1,3};
UINT4 FsSynceQLValue [ ] ={1,3,6,1,4,1,29601,2,79,1,1,2,1,4};
UINT4 FsSynceSSMOptionMode [ ] ={1,3,6,1,4,1,29601,2,79,1,1,2,1,5};
UINT4 FsSynceSelectedInterface [ ] ={1,3,6,1,4,1,29601,2,79,1,1,2,1,6};
UINT4 FsSynceContextRowStatus [ ] ={1,3,6,1,4,1,29601,2,79,1,1,2,1,7};
UINT4 FsSynceIfIndex [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,1};
UINT4 FsSynceIfSynceMode [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,2};
UINT4 FsSynceIfEsmcMode [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,3};
UINT4 FsSynceIfPriority [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,4};
UINT4 FsSynceIfQLValue [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,5};
UINT4 FsSynceIfIsRxQLForced [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,6};
UINT4 FsSynceIfLockoutStatus [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,7};
UINT4 FsSynceIfSignalFail [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,8};
UINT4 FsSynceIfPktsTx [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,9};
UINT4 FsSynceIfPktsRx [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,10};
UINT4 FsSynceIfPktsRxDropped [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,11};
UINT4 FsSynceIfPktsRxErrored [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,12};
UINT4 FsSynceIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,13};
UINT4 FsSynceIfConfigSwitch [ ] ={1,3,6,1,4,1,29601,2,79,1,2,1,1,14};




tMbDbEntry fssyncMibEntry[]= {
{{12,FsSynceGlobalSysCtrl}, NULL, FsSynceGlobalSysCtrlGet, FsSynceGlobalSysCtrlSet, FsSynceGlobalSysCtrlTest, FsSynceGlobalSysCtrlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{14,FsSynceContextId}, GetNextIndexFsSynceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSynceTableINDEX, 1, 0, 0, ""},

{{14,FsSynceTraceOption}, GetNextIndexFsSynceTable, FsSynceTraceOptionGet, FsSynceTraceOptionSet, FsSynceTraceOptionTest, FsSynceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSynceTableINDEX, 1, 0, 0, "64"},

{{14,FsSynceQLMode}, GetNextIndexFsSynceTable, FsSynceQLModeGet, FsSynceQLModeSet, FsSynceQLModeTest, FsSynceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceTableINDEX, 1, 0, 0, "1"},

{{14,FsSynceQLValue}, GetNextIndexFsSynceTable, FsSynceQLValueGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsSynceTableINDEX, 1, 0, 0, "15"},

{{14,FsSynceSSMOptionMode}, GetNextIndexFsSynceTable, FsSynceSSMOptionModeGet, FsSynceSSMOptionModeSet, FsSynceSSMOptionModeTest, FsSynceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceTableINDEX, 1, 0, 0, "1"},

{{14,FsSynceSelectedInterface}, GetNextIndexFsSynceTable, FsSynceSelectedInterfaceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSynceTableINDEX, 1, 0, 0, ""},

{{14,FsSynceContextRowStatus}, GetNextIndexFsSynceTable, FsSynceContextRowStatusGet, FsSynceContextRowStatusSet, FsSynceContextRowStatusTest, FsSynceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceTableINDEX, 1, 0, 1, ""},

{{14,FsSynceIfIndex}, GetNextIndexFsSynceIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSynceIfTableINDEX, 1, 0, 0, ""},

{{14,FsSynceIfSynceMode}, GetNextIndexFsSynceIfTable, FsSynceIfSynceModeGet, FsSynceIfSynceModeSet, FsSynceIfSynceModeTest, FsSynceIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceIfTableINDEX, 1, 0, 0, "2"},

{{14,FsSynceIfEsmcMode}, GetNextIndexFsSynceIfTable, FsSynceIfEsmcModeGet, FsSynceIfEsmcModeSet, FsSynceIfEsmcModeTest, FsSynceIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceIfTableINDEX, 1, 0, 0, "1"},

{{14,FsSynceIfPriority}, GetNextIndexFsSynceIfTable, FsSynceIfPriorityGet, FsSynceIfPrioritySet, FsSynceIfPriorityTest, FsSynceIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSynceIfTableINDEX, 1, 0, 0, "0"},

{{14,FsSynceIfQLValue}, GetNextIndexFsSynceIfTable, FsSynceIfQLValueGet, FsSynceIfQLValueSet, FsSynceIfQLValueTest, FsSynceIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceIfTableINDEX, 1, 0, 0, "5"},

{{14,FsSynceIfIsRxQLForced}, GetNextIndexFsSynceIfTable, FsSynceIfIsRxQLForcedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSynceIfTableINDEX, 1, 0, 0, ""},

{{14,FsSynceIfLockoutStatus}, GetNextIndexFsSynceIfTable, FsSynceIfLockoutStatusGet, FsSynceIfLockoutStatusSet, FsSynceIfLockoutStatusTest, FsSynceIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceIfTableINDEX, 1, 0, 0, "2"},

{{14,FsSynceIfSignalFail}, GetNextIndexFsSynceIfTable, FsSynceIfSignalFailGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSynceIfTableINDEX, 1, 0, 0, ""},

{{14,FsSynceIfPktsTx}, GetNextIndexFsSynceIfTable, FsSynceIfPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsSynceIfTableINDEX, 1, 0, 0, ""},

{{14,FsSynceIfPktsRx}, GetNextIndexFsSynceIfTable, FsSynceIfPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsSynceIfTableINDEX, 1, 0, 0, ""},

{{14,FsSynceIfPktsRxDropped}, GetNextIndexFsSynceIfTable, FsSynceIfPktsRxDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsSynceIfTableINDEX, 1, 0, 0, ""},

{{14,FsSynceIfPktsRxErrored}, GetNextIndexFsSynceIfTable, FsSynceIfPktsRxErroredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsSynceIfTableINDEX, 1, 0, 0, ""},

{{14,FsSynceIfRowStatus}, GetNextIndexFsSynceIfTable, FsSynceIfRowStatusGet, FsSynceIfRowStatusSet, FsSynceIfRowStatusTest, FsSynceIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceIfTableINDEX, 1, 0, 1, ""},

{{14,FsSynceIfConfigSwitch}, GetNextIndexFsSynceIfTable, FsSynceIfConfigSwitchGet, FsSynceIfConfigSwitchSet, FsSynceIfConfigSwitchTest, FsSynceIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSynceIfTableINDEX, 1, 0, 0, "0"},
};
tMibData fssyncEntry = { 22, fssyncMibEntry };

#endif /* _FSSYNCDB_H */


