/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syewrg.h,v 1.3 2016/07/06 11:05:10 siva Exp $ 
*
* Description: This file contains the prototype(wr) for Synce 
*********************************************************************/
#ifndef _SYNCEWR_H
#define _SYNCEWR_H


INT4 GetNextIndexFsSynceTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsSynceIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSynceGlobalSysCtrlGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceQLModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceQLValueGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceSSMOptionModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceSelectedInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceContextRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfSynceModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfEsmcModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfQLValueGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfIsRxQLForcedGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfLockoutStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfSignalFailGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsTxGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxDroppedGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxErroredGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfConfigSwitchGet(tSnmpIndex *, tRetVal *);
INT4 FsSynceGlobalSysCtrlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceQLModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceSSMOptionModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceContextRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfSynceModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfEsmcModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfQLValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfLockoutStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfConfigSwitchTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceGlobalSysCtrlSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceQLModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceSSMOptionModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceContextRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfSynceModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfEsmcModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfQLValueSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfLockoutStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfConfigSwitchSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceGlobalSysCtrlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSynceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsSynceIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 FsSynceIfPktsTxSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxDroppedSet(tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxErroredSet(tSnmpIndex *, tRetVal *);


VOID  RegisterFSSYNC PROTO ((VOID));
VOID  UnRegisterFSSYNC PROTO ((VOID));

INT4 FsSynceIfPktsTxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxDroppedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSynceIfPktsRxErroredTest(UINT4 *, tSnmpIndex *, tRetVal *);
#endif /* _SYNCEWR_H */
