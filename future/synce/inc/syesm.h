/******************************************************************************
 *  Copyright (C) 2013  Aricent Inc . All Rights Reserved
 *
 *  $Id: syesm.h,v 1.3 2016/07/29 09:53:56 siva Exp $
 *
 *  Description : This file contains the states, events and transition function
 *                used by the Synce State Machine Handler Sub Module.
 *
 *****************************************************************************/

#ifndef SYNCE_SM_H
#define SYNCE_SM_H

/* States of State Machine */
typedef enum {
    QL_ENABLED_NORMAL_STATE,
    QL_ENABLED_MANUAL_STATE,
    QL_ENABLED_FORCED_STATE,
    QL_DISABLED_NORMAL_STATE,
    QL_DISABLED_MANUAL_STATE,
    QL_DISABLED_FORCED_STATE,
    SYNCE_MAX_STATE,
} eSynceSmState;

enum {
    SYNCE_NO_CHANGE_STATE = SYNCE_MAX_STATE + 1
};

/* Events of State Machine */
typedef enum {
    PRIORITY_CHANGED_EVENT,
    QL_MODE_CHANGED_EVENT,
    QL_CHANGED_EVENT,
    SIGNAL_STATUS_CHANGED_EVENT,
    ENABLE_LOCKOUT_EVENT,
    CLEAR_LOCKOUT_EVENT,
    SWITCH_MODE_FORCED_EVENT,
    SWITCH_MODE_MANUAL_EVENT,
    CLEAR_SWITCH_EVENT,
    SYNCE_MODE_CHANGED_EVENT,
    SYNCE_MAX_EVENT
} eSynceSmEvent;

/* Error in SM */
typedef enum
{
    SYNCE_SM_ERR_WRONG_EVENT_OR_STATE = 1,
    SYNCE_SM_ERR_MAX_ERROR
} eSynceSmError;

typedef void * tSynceSmStateData;

typedef eSynceSmState (*SynceSmTransitionFn)(tSynceSmStateData);

/*forward declaration */
struct _SynceFsSynceEntry;

typedef struct _SynceStateData
{
    struct _SynceFsSynceEntry *pSynceEntry;
    UINT1      *pu1Data;
    UINT4      u4IfIndex;
    UINT4      u4IfContextId;
    UINT4      u4DataLen;
    UINT1      u1ErrNo;
    UINT1      u1Padding[3];
} tSynceStateData;

/* State Functions */

/* State Functions for QL_ENABLED_NORMAL_STATE */
eSynceSmState SynceSmFnQlEnNoSwPriorityCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnNoSwQlModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnNoSwQlCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnNoSwSignalCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnNoSwLockoutEnable(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnNoSwClearLockout(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnNoSwSynceModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnForceSwEn(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnManualSwEnable(tSynceSmStateData data);

/* State Functions for QL_DISABLED_NORMAL_STATE */
eSynceSmState SynceSmFnQlDisNoSwPriorityCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisNoSwQlModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisNoSwQlCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisNoSwLockoutEnable(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisNoSwClearLockout(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisNoSwSynceModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisNoSwSignalCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisForceSwEn(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisManualSwEnable(tSynceSmStateData data);

/* State Functions for QL_ENABLED_MANUAL_STATE */

eSynceSmState SynceSmFnQlEnManSwPriorityCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnManSwQlModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnManSwQlCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnSwLockoutEn(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnManSwClearLock(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnSwClearSw(tSynceSmStateData data);

/* State Functions for QL_ENABLED_FORCED_STATE */

eSynceSmState SynceSmFnQlEnForceSwPriorityCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnForceSwQlModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnForceSwNoCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlEnForceSwQlCh(tSynceSmStateData data);

/* State Functions for QL_DISABLED_MANUAL_STATE */

eSynceSmState SynceSmFnQlDisManSwPriorityCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisManSwQlModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisManSwSignalCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisManSwLockoutEn(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisManSwClearLock(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisSwClearSw(tSynceSmStateData data);

/* State Functions fro QL_DISABLED_FORCED_STATE */ 
eSynceSmState SynceSmFnQlDisForceSwPriorityCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisForceSwQlModeCh(tSynceSmStateData data);
eSynceSmState SynceSmFnQlDisForceSwLockoutEn(tSynceSmStateData data);

INT4 SynceSmUpdateQLValue (UINT4 u4ContextId,
             UINT4 u4CurBestQlValue);

PUBLIC INT4
SynceSmTriggerEvent(UINT4 u4ContextId, eSynceSmEvent smEvent,
                    tSynceStateData *smData);


eSynceSmState
SynceSmFnInvalidStateOrEvent(tSynceSmStateData data);

#endif
