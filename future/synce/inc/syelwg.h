/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syelwg.h,v 1.3 2016/07/06 11:05:10 siva Exp $ 
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsSynceTable. */
INT1
nmhValidateIndexInstanceFsSynceTable ARG_LIST((INT4 ));

/* Proto Validate Index Instance for FsSynceIfTable. */
INT1
nmhValidateIndexInstanceFsSynceIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSynceTable  */

INT1
nmhGetFirstIndexFsSynceTable ARG_LIST((INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsSynceIfTable  */

INT1
nmhGetFirstIndexFsSynceIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSynceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSynceIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceGlobalSysCtrl ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceTraceOption ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceQLMode ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceQLValue ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceSSMOptionMode ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceSelectedInterface ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceContextRowStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfConfigSwitch ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSynceIfSynceMode ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfEsmcMode ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfPriority ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfQLValue ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfIsRxQLForced ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfLockoutStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfSignalFail ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfPktsTx ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfPktsRx ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfPktsRxDropped ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfPktsRxErrored ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSynceIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceGlobalSysCtrl ARG_LIST((INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceTraceOption ARG_LIST((INT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceQLMode ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceSSMOptionMode ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceContextRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsSynceIfConfigSwitch ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSynceIfSynceMode ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceIfEsmcMode ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceIfPriority ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceIfQLValue ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSynceIfLockoutStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */


INT1
nmhSetFsSynceIfPktsTx ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsSynceIfPktsRx ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsSynceIfPktsRxDropped ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsSynceIfPktsRxErrored ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsSynceIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceGlobalSysCtrl ARG_LIST((UINT4 * , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceTraceOption ARG_LIST((UINT4 * , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceQLMode ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceSSMOptionMode ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceContextRowStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsSynceIfConfigSwitch ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSynceIfSynceMode ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceIfEsmcMode ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceIfPriority ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceIfQLValue ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSynceIfLockoutStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsSynceIfPktsTx ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsSynceIfPktsRx ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsSynceIfRowStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

INT1
nmhTestv2FsSynceIfPktsRxDropped ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsSynceIfPktsRxErrored ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSynceGlobalSysCtrl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSynceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSynceIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
