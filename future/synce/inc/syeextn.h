/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syeextn.h,v 1.1 2013/03/23 12:41:36 siva Exp $
 *
 * Description: This file contains extern declaration of global
 *              variables of the synce module.
 *******************************************************************/

#ifndef __SYNCEEXTN_H__
#define __SYNCEEXTN_H__

PUBLIC tSynceGlobals gSynceGlobals;
PUBLIC const tSynceSsmCodeInfo gaSynceSsmCodeTable[SYNCE_SSM_MAX_CODE + 1]
                                                   [SYNCE_SSM_OPTION_MAX];
PUBLIC UINT1 gau1SynceEsmcPduDestMacAddr[SYNCE_ENET_ADDR_LEN];
PUBLIC tSynceFsSynceEntry *gpSynceContextInfo;
PUBLIC UINT4 gau4SynceTraceOptions[MAX_SYNCE_CONTEXT];

#endif/*__SYNCEEXTN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file synceextn.h                      */
/*-----------------------------------------------------------------------*/

