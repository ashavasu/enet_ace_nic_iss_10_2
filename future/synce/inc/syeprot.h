/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syeprot.h,v 1.1 2013/03/23 12:41:36 siva Exp $ 
 *
 * Description: This file contains declaration of function
 *              prototypes of synce module.
 *******************************************************************/

#ifndef __SYNCEPROT_H__
#define __SYNCEPROT_H__

/* Add Prototypes here */

/**************************syncemain.c*******************************/
/*PUBLIC VOID SynceMainTask             PROTO ((VOID));*/
PUBLIC UINT4 SynceMainInit            PROTO ((VOID));
PUBLIC VOID SynceMainDeInit           PROTO ((VOID));
PUBLIC INT4 SynceApiLock              PROTO ((VOID));
PUBLIC INT4 SynceApiUnLock            PROTO ((VOID));
PUBLIC UINT4 SynceMainRegWithExtModules (VOID);
PUBLIC UINT4 SynceMainDeRegWithExtModules (VOID);

/**************************synceque.c********************************/
PUBLIC VOID SynceQueProcessMsgs        PROTO ((VOID));

/**************************syncepkt.c*******************************/
INT4 SyncePktValidateEsmcPdu(UINT1 *pu1Pdu, UINT2 u2PduLen);
INT4 SyncePktDetectEsmcPdu(UINT1 *pu1Pdu, UINT2 u2PduLen);

VOID SyncePktHandleRxPacket(tSynceQMsg *pSynceQMsg);
INT4 SyncePktProcessEsmcPdu(tSyncePduParams *esmcPduParams,
                         UINT1 *pu1SsmCode,
                         UINT1 *pu1Error);
INT4 SyncePktIsSsmAcceptable(UINT4 u4IfIndex, UINT1 u1SsmCode);
INT4 SyncePktConstructEsmcPdu(UINT1 *pu1Buffer,
                           UINT2 *pu4BufferLen,
                           tSynceSsmParams *pSsmParams);
VOID SyncePktDump(UINT1 *pu1Buf, UINT2 u2Len);

UINT4 SynceBufferToU32(const UINT1 *buffer);
UINT2 SynceRead2Bytes(const UINT1 **datap);
UINT4 SynceRead4Bytes(const UINT1 **datap);
VOID SynceWriteNBytes(UINT1 **ppu1Buf, UINT1 *pu1Value, UINT2 u2Len);
VOID SynceWrite4Bytes(UINT1 **ppu1Buf, UINT4 u4Value);
VOID SynceWrite2Bytes(UINT1 **ppu1Buf, UINT2 u2Value);

/**************************syncetmr.c*******************************/
PUBLIC VOID  SynceTmrInitTmrDesc   PROTO(( VOID));
PUBLIC VOID  SynceTmrStart         PROTO((UINT4 u4IfIndex, UINT1 u1TimerType,
                                          UINT4 u4Secs, UINT4 u4MiliSecs));
PUBLIC VOID  SynceTmrRestart       PROTO((UINT4 u4IfIndex, UINT1 u1TimerType,
                                          UINT4 u4Secs, UINT4 u4MiliSecs));
PUBLIC VOID  SynceTmrStop          PROTO((UINT4 u4IfIndex, UINT1 u1TimerType));
PUBLIC VOID  SynceTmrHandleExpiry  PROTO((VOID));
PUBLIC INT4  SynceTmrIsTimerRunning PROTO((UINT4 u4IfIndex, UINT1 u1TimerType));
PUBLIC VOID  SynceTmrStopAllTimers PROTO((UINT4 u4IfIndex));


/**************************syncetrc.c*******************************/

CHR1  *SynceTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   SynceTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   SynceTrcWrite      PROTO(( CHR1 *s));

/**************************synceutils.c******************************/
INT4 SynceUtilCreateContext (UINT4 u4ContextId);
INT4 SynceUtilDeleteContext (UINT4 u4ContextId);
INT4 SynceUtilGetSynceEntryFromIfIdx(UINT4 u4IfIndex,
                                        tSynceFsSynceEntry **ppSynceEntry);

INT4 SynceUtilGetContextIdFromIfIndex(UINT4 u4IfIndex, UINT4 *pu4ContextId);
UINT4 SynceUtilGetQlCodeFromSsmCode(UINT4 u4ContextId, UINT1 u1SsmCode,
                                UINT1 *pu1QlCode);
UINT4 SynceUtilGetSsmCodeFromQlCode(UINT4 u4ContextId, UINT1 u1QlCode,
                                UINT1 *pu1SsmCode);
INT4 SynceUtilIsSynceEnabled(VOID);
INT4 SynceUtilSelectContext(UINT4 u4ContextId);
INT4 SynceUtilReleseContext(VOID);
INT4 SynceUtilModuleStart(VOID);
VOID SynceUtilModuleShutdown(VOID);

/********************** syncedsg.c**********************************/
tSynceFsSynceEntry *SynceGetFsSynceEntry(UINT4 u4ContextId);
tSynceFsSynceIfEntry *SynceGetFsSynceIfEntry(UINT4 u4IfIndex);

/**********************syncetrap.c ********************************/
VOID SynceTrapRaiseTrap(tSynceFsSynceIfEntry *pSynceIfEntry, UINT1 u1TrapType);

/********************* synceapi.c *********************************/
INT4        SynceCfaRegLL (VOID);
PUBLIC INT4 SynceCfaDeRegisterLL (UINT2 u2LenOrType);
INT4        SynceRegisterClock (VOID);
PUBLIC VOID SynceDeRegisterClock (UINT1 u1Protocol);
PUBLIC INT4 SynceApiIfOperStatusChangeCb (tCfaRegInfo * pCfaRegInfo);
PUBLIC INT4 SynceApiPktRxCb (tCfaRegInfo * pCfaRegInfo);
PUBLIC INT4 SynceApiIfCreateCb (tCfaRegInfo * pCfaRegInfo);

/****************** synceclk.c ***********************************/
UINT4       SynceClkDoClockSelection(UINT4 u4ContextId, INT4 i4QlMode);

/*********************syncetx.c **********************************/
VOID SynceTxEsmcPacket(UINT4 u4IfIndex);

#endif   /* __SYNCEPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  synceprot.h                     */
/*-----------------------------------------------------------------------*/
