/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved 
*
* $Id: syesrc.h,v 1.1 2013/03/23 12:41:36 siva Exp $ 
*
* Description: This file holds the functions for the ShowRunningConfig
*			  for Synce module 
*********************************************************************/
#ifndef __SYNCESRC_H__
#define __SYNCESRC_H__

INT4 SynceShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 SynceShowRunningConfigFsSynceTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 SynceShowRunningConfigFsSynceIfTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 SynceShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...);


#endif
