/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syeglob.h,v 1.1 2013/03/23 12:41:36 siva Exp $
 *
 * Description: This file contains declaration of global variables
 *              of synce module.
 *******************************************************************/

#ifndef __SYNCEGLOBAL_H__
#define __SYNCEGLOBAL_H__

tSynceGlobals gSynceGlobals;
tSynceFsSynceEntry *gpSynceContextInfo;
UINT4 gau4SynceTraceOptions[MAX_SYNCE_CONTEXT];

#endif  /* __SYNCEGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file synceglob.h                      */
/*-----------------------------------------------------------------------*/
