/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syedefn.h,v 1.2 2015/10/15 11:12:53 siva Exp $
 *
 * Description: This file contains definitions for synce module.
 *******************************************************************/

#ifndef __SYNCEDEFN_H__
#define __SYNCEDEFN_H__

#define  SYNCE_IF_MAX_PRIORITY          255
#define  SYNCE_DEFAULT_CXT_ID           0

/* Error Types */
#define  SYNCE_SEMAPHORE_TAKE_ERROR     1
#define  SYNCE_INTERFACE_NODE_ERROR     2
#define  CONTEXT_NODE_ERROR             3
#define  SYNCE_IF_PRIORITY_DIS_ERROR    4
#define  SYNCE_IF_LOCKOUT_ON_ERROR      5

/* esmc pdu related*/
#define  SYNCE_ESMC_HEADER_SIZE         24
#define  SYNCE_ESMC_MIN_PDU_SIZE        64
#define  SYNCE_ESMC_MAX_PDU_SIZE        128
#define  SYNCE_QL_TLV_SIZE              32
#define  SYNCE_ENET_ADDR_LEN            6
#define  SYNCE_ETH_HEADER_SIZE          14
#define  SYNCE_ETH_HEADER_MAC_BYTES     12
#define  SYNCE_ESMC_PDU_VER_OFFSET      20
#define  SYNCE_MIN_PKT_SIZE             28

#define  SYNCE_ESMC_ETHER_TYPE          0x8809
#define  SYNCE_SLOW_PROTOCOL_TYPE       0x0A
#define  SYNCE_ESMC_ITU_OUI             0x19A7
#define  SYNCE_ESMC_ITU_SUBTYPE         0x0001

/* */
#define  SYNCE_DEST_OFFSET              0
#define  SYNCE_SRC_OFFSET               6
#define  SYNCE_LEN_TYPE_OFFSET          12
#define  SYNCE_SUBTYPE_OFFSET           14

/* */
#define  SYNCE_SSM_MAX_CODE             0xf

/* Traps */
#define SYNCE_TRAP_INVALID                0x0
#define SYNCE_TRAP_SIGNAL_STATUS_CHANGE   0x1
#define SYNCE_TRAP_INF_QL_CHANGED         0x2

/* Trap Octet[0] Mask values */
#define SYNCE_TRAP_SIGNAL_STATUS_ERR      0x01
#define SYNCE_TRAP_INTF_QL_CHNG           0x02

#define SYNCE_SNMPV2_TRAP_OID_LEN         11
#define SYNCE_OBJECT_NAME_LEN             256

/* Packet related */
#define SYNCE_PKT_VERSION_BITS            4
#define SYNCE_PKT_RESERVED_BYTES          3
#define SYNCE_PKT_TYPE                    1
#define SYNCE_PKT_FEILD_LENGTH            4
#define SYNCE_PKT_TLV_LENGTH              4

/* array index values */
#define SYNCE_INDEX_ZERO                  0
#define SYNCE_INDEX_ONE                   1
#define SYNCE_INDEX_TWO                   2
#define SYNCE_INDEX_THREE                 3
#define SYNCE_INDEX_FOUR                  4
#define SYNCE_INDEX_FIVE                  5
#define SYNCE_INDEX_SIX                   6
#define SYNCE_INDEX_SEVEN                 7
#define SYNCE_INDEX_EIGHT                 8
#define SYNCE_INDEX_NINE                  9
#define SYNCE_INDEX_TEN                   10
#define SYNCE_INDEX_ELEVEN                11
#define SYNCE_INDEX_TWELVE                12
#define SYNCE_INDEX_THIRTEEN              13
#define SYNCE_INDEX_FOURTEEN              14
#define SYNCE_INDEX_FIFTEEN               15

#endif  /* __SYNCEDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file syncedefn.h                     */
/*-----------------------------------------------------------------------*/
