/****************************************************************************
 * Copyright (C) 2013  Aricent Inc . All Rights Reserved
 *
 * $Id: syepe.h,v 1.2 2016/07/06 11:05:10 siva Exp $
 *
 *  Descrition : This file contains the prototypes of functions
 *               used by Protocol Engine Sub Module of Synce Module
 *
 ***************************************************************************/

#ifndef __SYNCE_PROT_ENG_H__
#define __SYNCE_PROT_ENG_H__

INT4
SynceProtSsmOptionSet(UINT4 u4ContextId,
                  INT4 i4SsmOption);


INT4
SynceSetTraceLevel(UINT4 u4ContextId,
                   UINT4 u4tracelevel);


INT4
SynceProtQlModeSet(UINT4 u4ContextId,
               INT4 i4QlMode);


INT4
SynceProtIfSynceModeSet(UINT4 u4IfIndex,
                    INT4 i4Mode);


INT4
SynceProtIfQLSet(UINT4 u4IfIndex,
             UINT4 u4QlValue,
             UINT1 u1QlSource);


INT4
SynceProtIfEsmcModeSet(UINT4 u4IfIndex,
                   INT4 i4EsmcMode,
                   INT4);


INT4
SynceProtIfPrioritySet(UINT4 u4IfIndex,
                   INT4 i4Priority);


INT4
SynceProtIfLockoutSet(UINT4 u4IfIndex);


INT4
SynceProtIfLockoutClear(UINT4 u4IfIndex);


INT4
SynceProtIfConfigSwitchManualSet(UINT4 u4IfIndex);

INT4
SynceProtIfConfigSwitchForceSet(UINT4 u4IfIndex);

INT4
SynceProtIfConfigSwitchClear(UINT4 u4IfIndex);

INT4
SynceProtIfSignalFail(UINT4 u4IfIndex,
                  UINT1 u1SigFailState);

INT4
SynceProtIfDeletionHandler(UINT4 u4IfIndex);


INT4
SynceProtIfStatusChangeHandler(UINT4 u4IfIndex,
                               UINT1 u1ChangeType);

#endif

