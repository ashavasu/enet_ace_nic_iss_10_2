/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syeinc.h,v 1.2 2013/09/28 07:36:24 siva Exp $
 *
 * Description: This file contains include files of sye module.
 *******************************************************************/

#ifndef __SYNCEINC_H__
#define __SYNCEINC_H__

#include "lr.h"
#include "cli.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "params.h"
#include "fsutlsz.h"
#include "cfa.h"
#include "fsbuddy.h"
#include "utlsll.h"
#include "fm.h"
#include "iss.h"
#include "vcm.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "sizereg.h"
#include "fssyslog.h"

#include "synce.h"
#include "syedefn.h"
#include "syetdfsg.h"
#include "syesrdef.h"
#include "syetdfs.h"
#include "syetmr.h"

#ifdef __SYNCEMAIN_C__

#include "syeglob.h"
#include "syelwg.h"
#include "syedefg.h"
#include "syesz.h"
#include "syewrg.h"

#else

#include "syeextn.h"
#include "syembclg.h"
#include "syelwg.h"
#include "syedefg.h"
#include "syesz.h"
#include "syewrg.h"

#endif /* __SYNCEMAIN_C__*/

#include "syemacs.h"
#include "syetrc.h"
#include "syncecli.h"
#include "syetmr.h"
#include "syeprot.h"
#include "syeprotg.h"
#include "syepe.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "syncenp.h"
#include "nputil.h"
#endif

#endif   /* __SYNCEINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file syeinc.h                       */
/*-----------------------------------------------------------------------*/

