/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syembclg.h,v 1.2 2016/07/06 11:05:10 siva Exp $ 
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsSynceGlobalSysCtrl[12];

extern UINT4 FsSynceContextId[14];

extern UINT4 FsSynceTraceOption[14];

extern UINT4 FsSynceQLMode[14];

extern UINT4 FsSynceSSMOptionMode[14];

extern UINT4 FsSynceContextRowStatus[14];

extern UINT4 FsSynceIfIndex[14];

extern UINT4 FsSynceIfSynceMode[14];

extern UINT4 FsSynceIfEsmcMode[14];

extern UINT4 FsSynceIfPriority[14];

extern UINT4 FsSynceIfQLValue[14];

extern UINT4 FsSynceIfLockoutStatus[14];

extern UINT4 FsSynceIfConfigSwitch[14];

extern UINT4 FsSynceIfRowStatus[14];
