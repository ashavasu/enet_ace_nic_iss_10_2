/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syetmr.c,v 1.2 2013/04/25 11:37:09 siva Exp $
 *
 * Description : This file contains procedures containing Synce Timer
 *               related operations
 *****************************************************************************/
#include "syeinc.h"

/* global definitions*/
UINT4               gaSynceTmrDefInterval[SYNCE_MAX_TMRS] = {
    SYNCE_DEF_SIGNAL_FAIL_TIME,
    SYNCE_DEF_PACKET_TX_TIME
};

tSynceTimerRange    gaSynceTmrRange[SYNCE_MAX_TMRS] = {
    {.u4MinTime = 0,.u4MaxTime = SYNCE_DEF_SIGNAL_FAIL_TIME,
     .tmrUnit = SYNCE_SECOND},
    {.u4MinTime = 0,.u4MaxTime = 1,.tmrUnit = SYNCE_SECOND}
};

/****************************************************************************
* Function     : SynceTmrInitTmrDesc                                        *
* Description  : Initialize Synce Timer Decriptors                          *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : None                                                       *
*****************************************************************************/

PUBLIC VOID
SynceTmrInitTmrDesc ()
{
    SYNCE_FN_ENTRY ();

    gSynceGlobals.aTmrDesc[SYNCE_RX_TMR].pTmrExpFunc =
        SynceTmrSignalFailHandler;
    gSynceGlobals.aTmrDesc[SYNCE_RX_TMR].i2Offset =
        (INT2) (FSAP_OFFSETOF (tSynceFsSynceIfEntry, aTimers[SYNCE_RX_TMR]));

    gSynceGlobals.aTmrDesc[SYNCE_TX_TMR].pTmrExpFunc = SynceTmrTxTimerHandler;
    gSynceGlobals.aTmrDesc[SYNCE_TX_TMR].i2Offset =
        (INT2) (FSAP_OFFSETOF (tSynceFsSynceIfEntry, aTimers[SYNCE_TX_TMR]));

    SYNCE_FN_EXIT ();

}

/****************************************************************************
* Function     : SynceTmrHandleExpiry                                       *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
* Input        : None                                                       *
* Output       : None                                                       *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
SynceTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    eSynceTmrType       tmrType = SYNCE_MAX_TMRS;
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    UINT4               u4ContextId = 0;
    VOID               *pAppData = NULL;
    INT2                i2Offset = 0;

    SYNCE_FN_ENTRY ();

    while ((pExpiredTimers = TmrGetNextExpiredTimer (gSynceGlobals.tmrList))
           != NULL)
    {
        tmrType = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        i2Offset = gSynceGlobals.aTmrDesc[tmrType].i2Offset;

        if (i2Offset < 0)
        {
            /* The timer function does not take any parameter */
            (*(gSynceGlobals.aTmrDesc[tmrType].pTmrExpFunc)) (NULL);

        }
        else
        {
            /* set the current context */
            pAppData = ((UINT1 *) pExpiredTimers - i2Offset);
            pSynceIfEntry = (tSynceFsSynceIfEntry *) pAppData;
            if (SynceUtilGetContextIdFromIfIndex
                ((UINT4) pSynceIfEntry->MibObject.i4FsSynceIfIndex,
                 &u4ContextId) != OSIX_SUCCESS)
            {
                return;
            }

            SYNCE_SELECT_CONTEXT (u4ContextId);

            /* The timer function requires a parameter */
            (*(gSynceGlobals.aTmrDesc[tmrType].pTmrExpFunc)) (pAppData);

            SYNCE_RELEASE_CONTEXT ();
        }
    }

    SYNCE_FN_EXIT ();
    return;

}

/****************************************************************************
* Function     : SynceTmrStart
* Description  : Starts SyncE Timer
* Input        : u4IfIndex - interface index
*                u1TimerType - timer type
*                u4Secs     - ticks in seconds
*                u4MiliSecs - ticks in milli seconds
* Output       : None
* Returns      : None
*****************************************************************************/

PUBLIC VOID
SynceTmrStart (UINT4 u4IfIndex, UINT1 u1TimerType, UINT4 u4Secs,
               UINT4 u4MiliSecs)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;

    SYNCE_FN_ENTRY ();

    if (u1TimerType >= SYNCE_MAX_TMRS)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Request for invalid type of timer\n"));

        SYNCE_FN_EXIT ();
        return;
    }

    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry != NULL)
    {
        pSynceIfEntry->aTimers[u1TimerType].u1TimerId = u1TimerType;
        if (TmrRestart (gSynceGlobals.tmrList,
                        &(pSynceIfEntry->aTimers[u1TimerType]),
                        pSynceIfEntry->aTimers[u1TimerType].u1TimerId,
                        u4Secs, u4MiliSecs) == TMR_FAILURE)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Tmr ReStart Failure\n"));
        }
    }

    SYNCE_FN_EXIT ();

    return;
}

/****************************************************************************
* Function     : SynceTmrRestart
* Description  : ReStarts SyncE Timer
* Input        : u4IfIndex - interface index
*                u1TimerType - timer type
*                u4Secs     - ticks in seconds
*                u4MiliSecs - ticks in milli seconds
* Output       : None
* Returns      : None
*****************************************************************************/

PUBLIC VOID
SynceTmrRestart (UINT4 u4IfIndex, UINT1 u1TimerType, UINT4 u4Secs,
                 UINT4 u4MiliSecs)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;

    SYNCE_FN_ENTRY ();

    if (u1TimerType >= SYNCE_MAX_TMRS)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Request for invalid type of timer\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry != NULL)
    {
        pSynceIfEntry->aTimers[u1TimerType].u1TimerId = u1TimerType;
        if (TmrRestart (gSynceGlobals.tmrList,
                        &(pSynceIfEntry->aTimers[u1TimerType]),
                        pSynceIfEntry->aTimers[u1TimerType].u1TimerId,
                        u4Secs, u4MiliSecs) == TMR_FAILURE)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Tmr restart failed\n"));
        }
    }

    SYNCE_FN_EXIT ();
    return;
}

/****************************************************************************
* Function     : SynceTmrStop
* Description  : Stop SyncE Timer
* Input        : u4IfIndex - interface index
*                u1TimerType - timer type
* Output       : None
* Returns      : None
****************************************************************************/

PUBLIC VOID
SynceTmrStop (UINT4 u4IfIndex, UINT1 u1TimerType)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;

    SYNCE_FN_ENTRY ();

    if (u1TimerType >= SYNCE_MAX_TMRS)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Request for invalid type of timer\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry != NULL)
    {
        if (TmrStop (gSynceGlobals.tmrList,
                     &(pSynceIfEntry->aTimers[u1TimerType])) == TMR_FAILURE)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Tmr stop failed\n"));
        }
    }

    SYNCE_FN_EXIT ();
    return;
}

/****************************************************************************
 *  Function     : SynceTmrSignalFailHandler
 *  Description  : This procedure is invoked when a Rx timer expires
 *  Input        : Application timer block
 *  Output       : none
 *  Returns      : none
 ****************************************************************************/
VOID
SynceTmrSignalFailHandler (VOID *pAppData)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    UINT4               u4IfIndex;

    SYNCE_FN_ENTRY ();

    pSynceIfEntry = (tSynceFsSynceIfEntry *) pAppData;
    u4IfIndex = (UINT4) pSynceIfEntry->MibObject.i4FsSynceIfIndex;

    SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_FAIL);

    SYNCE_FN_EXIT ();
    return;
}

/****************************************************************************
 *  Function     : SynceTmrTxTimerHandler
 *  Description  : This routine is invoked when a Tx timer expires
 *  Input        : Application timer block
 *  Output       : none
 *  Returns      : none
 ****************************************************************************/
VOID
SynceTmrTxTimerHandler (VOID *pAppData)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    UINT4               u4IfIndex = 0;

    SYNCE_FN_ENTRY ();

    pSynceIfEntry = (tSynceFsSynceIfEntry *) pAppData;
    u4IfIndex = (UINT4) pSynceIfEntry->MibObject.i4FsSynceIfIndex;
    /*send ESMC packet */

    SynceTxEsmcPacket (u4IfIndex);

    SynceTmrStart (u4IfIndex, SYNCE_TX_TMR,
                   gaSynceTmrDefInterval[SYNCE_TX_TMR], 0);

    SYNCE_FN_EXIT ();
    return;
}

/****************************************************************************
 *  Function     : SynceTmrIsTimerRunningOrExpired
 *  Description  : This routine check if a particular timer is running or not.
 *  Input        : u4IfIndex - interface index
 *                 u1TimerType - timer type
 *  Output       : none
 *  Returns      : OSIX_TRUE/OSIX_FALSE
 ****************************************************************************/
INT4
SynceTmrIsTimerRunning (UINT4 u4IfIndex, UINT1 u1TimerType)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    UINT4               u4RemainingTime = 0;

    SYNCE_FN_ENTRY ();

    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry != NULL)
    {
        TmrGetRemainingTime (gSynceGlobals.tmrList,
                             ((tTmrAppTimer *) &
                              (pSynceIfEntry->aTimers[u1TimerType])),
                             &u4RemainingTime);

        if (u4RemainingTime > 0)
        {
            return OSIX_TRUE;
        }
    }

    SYNCE_FN_EXIT ();
    return OSIX_FALSE;
}

/****************************************************************************
 *  Function     : SynceTmrStopAllTimers
 *  Description  : This routine stops all the timer running for a particular
 *                 interface index.
 *  Input        : u4IfIndex - interface index
 *  Output       :
 *  Returns      :
 ****************************************************************************/
VOID
SynceTmrStopAllTimers (UINT4 u4IfIndex)
{
    UINT1               u1TmrType = 0;

    SYNCE_FN_ENTRY ();

    for (u1TmrType = SYNCE_RX_TMR; u1TmrType < SYNCE_MAX_TMRS; u1TmrType++)
    {
        SynceTmrStop (u4IfIndex, u1TmrType);
    }

    SYNCE_FN_EXIT ();
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  syncetmr.c                      */
/*-----------------------------------------------------------------------*/
