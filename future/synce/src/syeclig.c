#ifndef __SYNCECLIG_C__
#define __SYNCECLIG_C__
/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
* $Id: syeclig.c,v 1.12 2016/07/06 11:05:10 siva Exp $
*
* Description: This file contains the Synce CLI related routines
*********************************************************************/

#include "syeinc.h"
#include "syncecli.h"

/* private declarations */
PRIVATE VOID
 
 
 
 SynceCliShowInterfaceShortInfo (tCliHandle CliHandle,
                                 INT4 i4IfIndex,
                                 UINT4 u4IfQlValue,
                                 INT4 i4IfPriority, INT4 i4SsmOption);

PRIVATE INT4
 
 
 
 SynceCliGetSsmCodeFromQlCode (UINT4 u4SsmOptionMode,
                               UINT1 u1QlCode, UINT1 *pu1SsmCode);
PRIVATE VOID
       SynceCliShowEsmcRxInfo (tCliHandle CliHandle, UINT4 u4ContextId);

PRIVATE VOID
       SynceCliShowEsmcTxInfo (tCliHandle CliHandle, UINT4 u4ContextId);

PRIVATE VOID
 
 
 
 SynceCliShowInterfaceEsmcInfo (tCliHandle CliHandle,
                                INT4 i4IfIndex,
                                UINT4 u4IfQlValue,
                                INT4 i4IfPriority, INT4 i4SsmOption);

PRIVATE INT4
 
 
 
 SynceCliClearIfCounters (tCliHandle CliHandle,
                          UINT4 u4IfIndex, UINT4 u4CounterType);

/****************************************************************************
 * Function    :  cli_process_Synce_cmd
 * Description :  This function is exported to CLI module to handle the
                  SYNCE cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Synce_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    va_list             ap;
    UINT4              *args[SYNCE_CLI_MAX_ARGS];
    INT1                i1Argno = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst = 0;
    BOOL1               b1IsDebugSet = OSIX_FALSE;
    UINT4               u4FsSynceContextId = 0;
    UINT4               u4OldTraceValue = 0;
    UINT4               u4CurTraceValue = 0;
    UINT4               u4FsSynceTraceOption = 0;
    UINT4              *pu4FsSynceTraceOption = NULL;

    tSynceFsSynceEntry  SynceSetFsSynceEntry;
    tSynceIsSetFsSynceEntry SynceIsSetFsSynceEntry;

    tSynceFsSynceIfEntry SynceSetFsSynceIfEntry;
    tSynceIsSetFsSynceIfEntry SynceIsSetFsSynceIfEntry;

    UNUSED_PARAM (u4CmdType);
    UNUSED_PARAM (u4IfIndex);

    CliRegisterLock (CliHandle, SynceApiLock, SynceApiUnLock);
    SYNCE_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        args[i1Argno++] = va_arg (ap, UINT4 *);
        if (i1Argno == SYNCE_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_SYNCE_FSSYNCETABLE:
            MEMSET (&SynceSetFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
            MEMSET (&SynceIsSetFsSynceEntry, 0,
                    sizeof (tSynceIsSetFsSynceEntry));

            SYNCE_FILL_FSSYNCETABLE_ARGS ((&SynceSetFsSynceEntry),
                                          (&SynceIsSetFsSynceEntry),
                                          args[SYNCE_INDEX_ZERO],
                                          args[SYNCE_INDEX_ONE],
                                          args[SYNCE_INDEX_TWO],
                                          args[SYNCE_INDEX_THREE],
                                          args[SYNCE_INDEX_FOUR]);

            i4RetStatus =
                SynceCliSetFsSynceTable (CliHandle, (&SynceSetFsSynceEntry),
                                         (&SynceIsSetFsSynceEntry));
            break;

        case CLI_SYNCE_FSSYNCEIFTABLE:
            MEMSET (&SynceSetFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
            MEMSET (&SynceIsSetFsSynceIfEntry, 0,
                    sizeof (tSynceIsSetFsSynceIfEntry));

            SYNCE_FILL_FSSYNCEIFTABLE_ARGS ((&SynceSetFsSynceIfEntry),
                                            (&SynceIsSetFsSynceIfEntry),
                                            args[SYNCE_INDEX_ZERO],
                                            args[SYNCE_INDEX_ONE],
                                            args[SYNCE_INDEX_TWO],
                                            args[SYNCE_INDEX_THREE],
                                            args[SYNCE_INDEX_FOUR],
                                            args[SYNCE_INDEX_FIVE],
                                            args[SYNCE_INDEX_SIX],
					    args[SYNCE_INDEX_SEVEN]);

            i4RetStatus =
                SynceCliSetFsSynceIfTable (CliHandle, (&SynceSetFsSynceIfEntry),
                                           (&SynceIsSetFsSynceIfEntry));
            break;

        case CLI_SYNCE_FSSYNCEGLOBALSYSCTRL:

            i4RetStatus = SynceCliSetFsSynceGlobalSysCtrl (CliHandle,
                                                           args
                                                           [SYNCE_INDEX_ZERO]);
            break;

        case CLI_SYNCE_FSSYNCETABLE_DEBUG:
            MEMSET (&SynceSetFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
            MEMSET (&SynceIsSetFsSynceEntry, 0,
                    sizeof (tSynceIsSetFsSynceEntry));

            /* context id */
            if (args[SYNCE_INDEX_ZERO] != NULL)
            {
                u4FsSynceContextId = *(UINT4 *) (args[SYNCE_INDEX_ZERO]);
            }

            if (nmhValidateIndexInstanceFsSynceTable ((INT4) u4FsSynceContextId)
                != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_SYNCE_ERR_MODULE_DISABLED);
                break;
            }

            nmhGetFsSynceTraceOption ((INT4) u4FsSynceContextId,
                                      &u4OldTraceValue);

            if (args[SYNCE_INDEX_ONE] != NULL)
            {
                u4CurTraceValue = *(UINT4 *) (args[SYNCE_INDEX_ONE]);
            }
            /* For trace option we use the arg[2] to detect whether trace value
             * is being set or reset.
             */
            if (args[SYNCE_INDEX_TWO] != NULL)
            {
                b1IsDebugSet = (BOOL1) (*(UINT4 *) (args[SYNCE_INDEX_TWO]));
                args[SYNCE_INDEX_TWO] = NULL;
            }

            switch (b1IsDebugSet)
            {
                case OSIX_TRUE:
                    u4FsSynceTraceOption = (u4OldTraceValue | u4CurTraceValue);
                    break;
                default:
                    u4FsSynceTraceOption =
                        (u4OldTraceValue & (~u4CurTraceValue));
                    break;
            }

            pu4FsSynceTraceOption = &u4FsSynceTraceOption;

            SYNCE_FILL_FSSYNCETABLE_ARGS ((&SynceSetFsSynceEntry),
                                          (&SynceIsSetFsSynceEntry),
                                          args[SYNCE_INDEX_ZERO],
                                          pu4FsSynceTraceOption,
                                          args[SYNCE_INDEX_TWO],
                                          args[SYNCE_INDEX_THREE],
                                          args[SYNCE_INDEX_FOUR]);

            i4RetStatus =
                SynceCliSetFsSynceTable (CliHandle, (&SynceSetFsSynceEntry),
                                         (&SynceIsSetFsSynceEntry));

            break;

        default:
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SYNCE_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gaSynceCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);

    SYNCE_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  SynceCliSetFsSynceTable
* Description :  This routine sets a value in a context.
*                It calls routine to validate the value to be set
*                and then sets the corrosponding value.
* Input       :  CliHandle
*                pSynceSetFsSynceEntry - structure containing context
*                related information.
*                pSynceIsSetFsSynceEntry - Context Field to be set
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
SynceCliSetFsSynceTable (tCliHandle CliHandle,
                         tSynceFsSynceEntry * pSynceSetFsSynceEntry,
                         tSynceIsSetFsSynceEntry * pSynceIsSetFsSynceEntry)
{

    UINT4               u4ErrorCode;

    if (SynceTestAllFsSynceTable (&u4ErrorCode, pSynceSetFsSynceEntry,
                                  pSynceIsSetFsSynceEntry, OSIX_TRUE,
                                  OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (SynceSetAllFsSynceTable (pSynceSetFsSynceEntry, pSynceIsSetFsSynceEntry,
                                 OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  SynceCliSetFsSynceIfTable
* Description :  This routine sets a value in an interface.
*                It calls a routine to validate the value to be set
*                and then sets the value.
* Input       :  CliHandle
*                 pSynceSetFsSynceIfEntry - Structure containing interface
*                 related information.
*                 pSynceIsSetFsSynceIfEntry - Interface Field to be set
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
SynceCliSetFsSynceIfTable (tCliHandle CliHandle,
                           tSynceFsSynceIfEntry * pSynceSetFsSynceIfEntry,
                           tSynceIsSetFsSynceIfEntry *
                           pSynceIsSetFsSynceIfEntry)
{
    UINT4               u4ErrorCode;

    if (SynceTestAllFsSynceIfTable (&u4ErrorCode, pSynceSetFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_TRUE,
                                    OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (SynceSetAllFsSynceIfTable
        (pSynceSetFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  SynceCliSetFsSynceGlobalSysCtrl
* Description :  This routine is used set the corrosponding value
*                when SyncE module starts or is shutting down.
* Input       :  CliHandle
*                 pFsSynceGlobalSysCtrl
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
SynceCliSetFsSynceGlobalSysCtrl (tCliHandle CliHandle,
                                 UINT4 *pFsSynceGlobalSysCtrl)
{
    UINT4               u4ErrorCode;
    INT4                i4FsSynceGlobalSysCtrl = 0;

    SYNCE_FILL_FSSYNCEGLOBALSYSCTRL (i4FsSynceGlobalSysCtrl,
                                     pFsSynceGlobalSysCtrl);

    if (nmhTestv2FsSynceGlobalSysCtrl (&u4ErrorCode, i4FsSynceGlobalSysCtrl) ==
       SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsSynceGlobalSysCtrl(i4FsSynceGlobalSysCtrl) == SNMP_FAILURE)
    {
	    CLI_FATAL_ERROR (CliHandle);
	    return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  cli_process_Synce_show_cmd
 * Description :  This function is exported to CLI module to handle the
                  SYNCE cli show commands.The display is taken care in this
                  function.
 * Input       :  Variable arguments
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
cli_process_Synce_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    va_list             ap;
    UINT4              *args[SYNCE_CLI_MAX_ARGS];
    INT1                i1Argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4ErrCode = 0;

    SYNCE_FN_ENTRY ();
    CliRegisterLock (CliHandle, SynceApiLock, SynceApiUnLock);
    SYNCE_LOCK;

    va_start (ap, u4Command);

    while (1)
    {
        args[i1Argno++] = va_arg (ap, UINT4 *);
        if (i1Argno == SYNCE_CLI_SHOW_CMDS_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    /* Cases for Show commands */

    switch (u4Command)
    {
        case CLI_SYNCE_SHOW:

            SynceCliShowContextInfo (CliHandle, *(args[SYNCE_INDEX_ZERO]));
            break;

        case CLI_SYNCE_SHOW_INTERFACE:

            SynceCliShowInterfaceInfo (CliHandle, *(args[SYNCE_INDEX_ZERO]));
            break;
        case CLI_SYNCE_SHOW_ESMC_RX:
            SynceCliShowEsmcRxInfo (CliHandle, *(args[SYNCE_INDEX_ZERO]));
            break;
        case CLI_SYNCE_SHOW_ESMC_TX:
            SynceCliShowEsmcTxInfo (CliHandle, *(args[SYNCE_INDEX_ZERO]));
            break;

        case CLI_SYNCE_CLEAR_CNTR:
            i4RetStatus = SynceCliClearIfCounters
                (CliHandle, *(args[SYNCE_INDEX_ZERO]),
                 *(args[SYNCE_INDEX_ONE]));
            break;

        default:
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
    	if ((u4ErrCode > 0) && (u4ErrCode < CLI_SYNCE_MAX_ERR))
	{
		CliPrintf (CliHandle, "\r%s", gaSynceCliErrString[u4ErrCode]);
	}
	CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);

    SYNCE_UNLOCK;

    SYNCE_FN_EXIT ();

    return i4RetStatus;
}

/****************************************************************************
 * Function    :  SynceCliShowContextInfo
 * Description :  This function shows the Synce module information based on
 *                   its context id.
 * Input       :  i4ContextId - Context Identifier
 *                CliHandle
 * Output      :  None
 * Returns     :  VOID
****************************************************************************/

VOID
SynceCliShowContextInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    PRIVATE UINT1      *asSsmOptionMode[] = {
        NULL,
        (UINT1 *) "Option 1",
        (UINT1 *) "Option 2 Gen 1",
        (UINT1 *) "Option 2 Gen 2"
    };

    PRIVATE UINT1      *asQlMode[] = {
        (UINT1 *) "QL-Disabled",
        (UINT1 *) "QL-Enabled"
    };
    PRIVATE UINT1      *asSwitchMode[] = {
        (UINT1 *) "None",
	(UINT1 *) "Force",
	(UINT1 *) "Manual"
    };

    UINT4               u4TraceOption = 0;
    UINT4               u4SystemQlValue = 0;
    UINT4               u4IfContextId = 0;
    UINT4               u4IfQlValue = 0;
    UINT1               u1SsmCode = 0;
    INT4                i4SsmOption = 0;
    INT4                i4QlMode = 0;
    INT4                i4SelectedIf = 0;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4IfPriority = 0;
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4IfConfigSwitch = 0;
    UINT1               u1IfMainSubType = 0;

    if (nmhValidateIndexInstanceFsSynceTable (i4ContextId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% %s\r\n",
                   gaSynceCliErrString[CLI_SYNCE_ERR_SYNCE_DIS_ON_CTX]);
        return;
    }

    CliPrintf (CliHandle, "\r\n-------------------"
               "-------------------------\r\n");
    CliPrintf (CliHandle, "Synchronous Ethernet Module Information\r\n");
    CliPrintf (CliHandle, "--------------------------------------------\r\n");

    nmhGetFsSynceQLMode (i4ContextId, &i4QlMode);
    nmhGetFsSynceSSMOptionMode (i4ContextId, &i4SsmOption);
    nmhGetFsSynceQLValue (i4ContextId, &u4SystemQlValue);
    nmhGetFsSynceSelectedInterface (i4ContextId, &i4SelectedIf);
    nmhGetFsSynceTraceOption (i4ContextId, &u4TraceOption);
    nmhGetFsSynceIfConfigSwitch (i4SelectedIf,&i4IfConfigSwitch);

    CliPrintf (CliHandle, "Context Identifier       : %d\r\n", u4ContextId);
    CliPrintf (CliHandle, "Operating Mode           : %s\r\n",
               asQlMode[i4QlMode]);
    CliPrintf (CliHandle, "SSM Option Mode          : %s\r\n",
               asSsmOptionMode[i4SsmOption]);
    SynceCliGetSsmCodeFromQlCode ((UINT4) i4SsmOption, (UINT1) u4SystemQlValue,
                                  &u1SsmCode);
    CliPrintf (CliHandle, "Selected Quality Level   : %s(%u)\r\n",
               (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1]).pu1Symbol,
               u4SystemQlValue);
    if (i4SelectedIf == 0)
    {
        CliPrintf (CliHandle, "Selected Interface       : None\r\n");
    }
    else
    {
        CfaGetEthernetType ((UINT4)i4SelectedIf, &u1IfMainSubType);

        if (u1IfMainSubType == CFA_FA_ENET)
        {
            CliPrintf (CliHandle, "Selected Interface       : FastEthernet %d/%d\r\n",u4ContextId,
                       i4SelectedIf);
        }
        else if (u1IfMainSubType == CFA_GI_ENET)
        {
            CliPrintf (CliHandle, "Selected Interface       : GigabitEthernet %d/%d\r\n",u4ContextId,
                       i4SelectedIf);
        }
        else if (u1IfMainSubType == CFA_XE_ENET)
        {
            CliPrintf (CliHandle, "Selected Interface       : ExtremeEthernet %d/%d\r\n",u4ContextId,
                       i4SelectedIf);
        }
        else if (u1IfMainSubType == CFA_XL_ENET)
        {
            CliPrintf (CliHandle, "Selected Interface       : XlEthernet %d/%d\r\n",u4ContextId,
                       i4SelectedIf);
        }
        else if (u1IfMainSubType == CFA_LVI_ENET)
        {
            CliPrintf (CliHandle, "Selected Interface       : LviEthernet %d/%d\r\n",u4ContextId,
                       i4SelectedIf);
        }
    }
    CliPrintf (CliHandle, "Switch Mode              : %s\r\n",asSwitchMode[i4IfConfigSwitch]);
    CliPrintf (CliHandle, "Trace Option             : 0x%x\r\n", u4TraceOption);
    CliPrintf (CliHandle, "\r\n");

    i4RetValue = nmhGetFirstIndexFsSynceIfTable (&i4IfIndex);
    /* show all the synce interfaces */
    if (i4RetValue == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n--------------------------"
                   "------------------------------------\r\n");
        CliPrintf (CliHandle, "Synchronous Ethernet Interface "
                   "Information\r\n");
        CliPrintf (CliHandle, "--------------------------"
                   "------------------------------------\r\n");
    }

    while (i4RetValue != SNMP_FAILURE)
    {
        if (SynceUtilGetContextIdFromIfIndex ((UINT4) i4IfIndex,
                                              &u4IfContextId) == OSIX_SUCCESS)
        {
            if (u4IfContextId == u4ContextId)
            {
                nmhGetFsSynceIfQLValue (i4IfIndex, (INT4 *) &u4IfQlValue);
                nmhGetFsSynceIfPriority (i4IfIndex, &i4IfPriority);

                SynceCliShowInterfaceShortInfo (CliHandle,
                                                i4IfIndex,
                                                u4IfQlValue,
                                                i4IfPriority, i4SsmOption);
            }
        }

        i4PrevIfIndex = i4IfIndex;
        i4RetValue = nmhGetNextIndexFsSynceIfTable (i4PrevIfIndex, &i4IfIndex);
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/****************************************************************************
 * Function    :  SynceCliShowInterfaceInfo
 * Description :  This function shows the Synce Interface information.
 * Input       :  u4IfIndex - Interface Identifier
 * Output      :  None
 * Returns     :  VOID
****************************************************************************/

VOID
SynceCliShowInterfaceInfo (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT4               u4CurrentValue = 0;
    UINT1               u1SsmCode = 0;
    UINT4               u4PktsTx = 0;
    UINT4               u4PktsRx = 0;
    UINT4               u4PktsDropped = 0;
    UINT4               u4PktsRxErrored = 0;
    UINT4               u4IfQlValue = 0;
    UINT4               u4IfContextId = 0;
    INT4                i4IfIndex = (INT4) u4IfIndex;
    INT4                i4IfEsmcMode = 0;
    INT4                i4IfPriority = 0;
    INT4                i4IfIsRxQlForced = 0;
    INT4                i4IfLockoutStatus = 0;
    INT4                i4IfSignalFail = 0;
    INT4                i4IfConfigSwitch = 0;
    INT4                i4SsmOption = 0;
    UINT1               u1IfMainSubType = 0;
    


    PRIVATE UINT1      *asEsmcMode[] = {
        (UINT1 *) "None",
        (UINT1 *) "Rx",
        (UINT1 *) "Tx"
    };

    PRIVATE UINT1      *asQlSource[] = {
        (UINT1 *) "SSM",
        (UINT1 *) "User Configured"
    };

    PRIVATE UINT1      *asSignalFail[] = {
        (UINT1 *) "No",
        (UINT1 *) "Yes"
    };

    PRIVATE UINT1      *asLockout[] = {
        (UINT1 *) "Off",
        (UINT1 *) "On",
    };

    PRIVATE UINT1      *asSwitchMode[] = {
        (UINT1 *) "None",
        (UINT1 *) "Force",
        (UINT1 *) "Manual"
    };
    if (nmhValidateIndexInstanceFsSynceIfTable (i4IfIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% %s\r\n",
                   gaSynceCliErrString[CLI_SYNCE_ERR_SYNCE_MODE_DISABLED]);
        return;
    }

    /* Only this get routine has been checked for failure to detect if
     * SyncE If Entry is present for this interface or not.
     * Generally get routine for column don't return failure if row with index
     * exist in the table.
     */
    if (nmhGetFsSynceIfEsmcMode (i4IfIndex, &i4IfEsmcMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% %s\r\n",
                   gaSynceCliErrString[CLI_SYNCE_ERR_SYNCE_MODE_DISABLED]);
        return;
    }

    nmhGetFsSynceIfPriority (i4IfIndex, &i4IfPriority);
    nmhGetFsSynceIfQLValue (i4IfIndex, (INT4 *) &u4IfQlValue);
    nmhGetFsSynceIfIsRxQLForced (i4IfIndex, &i4IfIsRxQlForced);
    nmhGetFsSynceIfLockoutStatus (i4IfIndex, &i4IfLockoutStatus);
    nmhGetFsSynceIfConfigSwitch (i4IfIndex,&i4IfConfigSwitch);
    nmhGetFsSynceIfSignalFail (i4IfIndex, &i4IfSignalFail);
    nmhGetFsSynceIfPktsTx (i4IfIndex, &u4PktsTx);
    nmhGetFsSynceIfPktsRx (i4IfIndex, &u4PktsRx);
    nmhGetFsSynceIfPktsRxDropped (i4IfIndex, &u4PktsDropped);
    nmhGetFsSynceIfPktsRxErrored (i4IfIndex, &u4PktsRxErrored);

    if (OSIX_FAILURE == SynceUtilGetContextIdFromIfIndex (u4IfIndex,
                                                          &u4IfContextId))
    {
        return;
    }

    nmhGetFsSynceSSMOptionMode ((INT4) u4IfContextId, &i4SsmOption);

    SynceCliGetSsmCodeFromQlCode ((UINT4) i4SsmOption, (UINT1) u4IfQlValue,
                                  &u1SsmCode);

    CliPrintf (CliHandle, "\r\n---------------------"
               "------------------------------\r\n");
    CliPrintf (CliHandle, "Synchronous Ethernet Interface Information\r\n");
    CliPrintf (CliHandle,
               "---------------------------------------------------\r\n");
    
    CfaGetEthernetType (u4IfIndex, &u1IfMainSubType);

    if (u1IfMainSubType == CFA_FA_ENET)
    {
        CliPrintf (CliHandle, "Interface            : fastEthernet %d/%u\r\n",u4IfContextId,
                   u4IfIndex);
    }
    else if (u1IfMainSubType == CFA_GI_ENET)
    {
        CliPrintf (CliHandle, "Interface            : gigabitEthernet %d/%u\r\n",u4IfContextId,
                    u4IfIndex);
    }
    else if (u1IfMainSubType == CFA_XE_ENET)
    {
        CliPrintf (CliHandle, "Interface            : extremeEthernet %d/%u\r\n",u4IfContextId,
                    u4IfIndex);
    }
    else if (u1IfMainSubType == CFA_XL_ENET)
    {
        CliPrintf (CliHandle, "Interface            : xlEthernet %d/%u\r\n",u4IfContextId,
                    u4IfIndex);
    }
    else if (u1IfMainSubType == CFA_LVI_ENET)
    {
        CliPrintf (CliHandle, "Interface            : lviEthernet %d/%u\r\n",u4IfContextId,
                    u4IfIndex);
    }

    CliPrintf (CliHandle, "ESMC Mode            : %s\r\n",
               asEsmcMode[i4IfEsmcMode]);
    CliPrintf (CliHandle, "Priority             : %u\r\n", i4IfPriority);
    CliPrintf (CliHandle, "QL of Interface      : %s(%u)\r\n",
               (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1].pu1Symbol),
               u4IfQlValue);
    u4CurrentValue = i4IfIsRxQlForced ? 1 : 0;
    CliPrintf (CliHandle, "QL Source            : %s\r\n",
               asQlSource[u4CurrentValue]);
    u4CurrentValue = i4IfLockoutStatus ? 1 : 0;
    CliPrintf (CliHandle, "Lockout              : %s\r\n",
               asLockout[u4CurrentValue]);
    CliPrintf (CliHandle, "Switch Mode          : %s\r\n",
               asSwitchMode[i4IfConfigSwitch]);
    u4CurrentValue = i4IfSignalFail ? 1 : 0;
    CliPrintf (CliHandle, "Signal Fail          : %s\r\n",
               asSignalFail[u4CurrentValue]);
    CliPrintf (CliHandle, "Packet Tx            : %u\r\n", u4PktsTx);
    CliPrintf (CliHandle, "Packet Rx            : %u\r\n", u4PktsRx);
    CliPrintf (CliHandle, "Packet Dropped       : %u\r\n", u4PktsDropped);
    CliPrintf (CliHandle, "Packet Errored       : %u\r\n", u4PktsRxErrored);

    CliPrintf (CliHandle, "\r\n");

    return;
}

/****************************************************************************
 * Function    :  SynceCliShowInterfaceShortInfo
 * Description :  This function shows the SyncE Interface information in
 *             :  short form.
 * Input       :  u4IfIndex - Interface Identifier
 *             :  pSynceFsSynceEntry - pointer to structure containing context
 *             :  related information.
 *             :  pSynceFsSynceIfEntry - pointer to structure containing SyncE
 *             :  interface related information.
 * Output      :  None
 * Returns     :  VOID
****************************************************************************/
VOID
SynceCliShowInterfaceShortInfo (tCliHandle CliHandle,
                                INT4 i4IfIndex,
                                UINT4 u4IfQlValue,
                                INT4 i4IfPriority, INT4 i4SsmOption)
{
    UINT1               u1SsmCode = 0;
    UINT1               u1IfMainSubType = 0;
    INT4                i4IfEsmcMode = 0;
    PRIVATE UINT1      *au1EsmcMode[] = {
        (UINT1 *) "None",
        (UINT1 *) "Rx",
        (UINT1 *) "Tx"
    };

    SynceCliGetSsmCodeFromQlCode ((UINT4) i4SsmOption, (UINT1) u4IfQlValue,
                                  &u1SsmCode);
    nmhGetFsSynceIfEsmcMode (i4IfIndex, &i4IfEsmcMode);

    CfaGetEthernetType ((UINT4)i4IfIndex, &u1IfMainSubType);

    if (u1IfMainSubType == CFA_FA_ENET)
    {
	    CliPrintf (CliHandle,
		       "Interface : FastEthernet 0/%-2u Priority : %-3u Esmc Mode : %-5s QL : %s(%u)\r\n",
		       i4IfIndex, i4IfPriority, au1EsmcMode[i4IfEsmcMode],
		       (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1]).pu1Symbol,
		       u4IfQlValue);
    }
    else if (u1IfMainSubType == CFA_GI_ENET)
    {
	    CliPrintf (CliHandle,
		      "Interface : GigabitEthernet 0/%-2u  Priority : %-3u  Esmc Mode : %-5s QL : %s(%u)\r\n",
		      i4IfIndex, i4IfPriority, au1EsmcMode[i4IfEsmcMode],
		      (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1]).pu1Symbol,
		      u4IfQlValue);
    }
    else if (u1IfMainSubType == CFA_XE_ENET)
    {
	    CliPrintf (CliHandle,
		       "Interface : ExtremeEthernet 0/%-2u Priority : %-3u Esmc Mode : %-5s QL : %s(%u)\r\n",
		       i4IfIndex, i4IfPriority, au1EsmcMode[i4IfEsmcMode],
		       (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1]).pu1Symbol,
		       u4IfQlValue);
    }
    else if (u1IfMainSubType == CFA_XL_ENET)
    {
	    CliPrintf (CliHandle,
		      "Interface : XlEthernet 0/%-2u  Priority : %-3u  Esmc Mode : %-5s QL : %s(%u)\r\n",
		       i4IfIndex, i4IfPriority, au1EsmcMode[i4IfEsmcMode],
		       (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1]).pu1Symbol,
		       u4IfQlValue);
    }
    else if (u1IfMainSubType == CFA_LVI_ENET)
    {
	    CliPrintf (CliHandle,
		      "Interface : LviEthernet 0/%-2u  Priority : %-3u  Esmc Mode : %-5s QL : %s(%u)\r\n",
		      i4IfIndex, i4IfPriority, au1EsmcMode[i4IfEsmcMode],
		      (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1]).pu1Symbol,
		      u4IfQlValue);
    }
}

/****************************************************************************
 * Function    :  SynceCliClearIfCounters
 * Description :  This function is used to clear interface counters.
 * Input       :  u4IfIndex  - Interface Index
 *                   u4CntrType - Counter Type
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
SynceCliClearIfCounters (tCliHandle CliHandle,
                         UINT4 u4IfIndex, UINT4 u4CntrType)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4PktsCount = 0;
    UNUSED_PARAM (CliHandle);

    if (SynceTestFsSynceIfCntrs (&u4ErrorCode, u4IfIndex, u4PktsCount)
        != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (SynceSetFsSynceIfCntrs (u4IfIndex, u4PktsCount, u4CntrType)
        != OSIX_SUCCESS)
    {
	    u4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	    CLI_SET_ERR (CLI_SYNCE_ERR_SYNCE_MODE_DISABLED);
	    return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  SynceCliGetSsmCodeFromQlCode
 * Description :  This function gets the SSM code corresponding to the QL
 *                code.
 * Input       :  u4SsmOptionMode  - SSM Option
 *             :  u1QlCode - QL code
 * Output      :  pu1SsmCode
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/
INT4
SynceCliGetSsmCodeFromQlCode (UINT4 u4SsmOptionMode,
                              UINT1 u1QlCode, UINT1 *pu1SsmCode)
{
    UINT1               u1SsmCode = 0;

    if (pu1SsmCode == NULL)
    {
        return OSIX_FAILURE;
    }

    if (u1QlCode >= SYNCE_QL_MAX)
    {
        return OSIX_FAILURE;
    }

    for (u1SsmCode = 0; u1SsmCode < SYNCE_SSM_MAX_CODE; u1SsmCode++)
    {
        if ((gaSynceSsmCodeTable[u1SsmCode][u4SsmOptionMode - 1]).u1QlCode
            == u1QlCode)
        {
            if ((gaSynceSsmCodeTable[u1SsmCode][u4SsmOptionMode - 1]).b1IsValid
                == OSIX_TRUE)
            {
                *pu1SsmCode = u1SsmCode;
            }
            else
            {
                *pu1SsmCode = 0xf;
            }

            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : IssSynceShowDebugging
 *
 *     DESCRIPTION      : This function is used to display debug level for  
 *                        SyncE module
 *
 *     INPUT            : CliHandle - CLI Handler
 *
 *     OUTPUT           : NONE
 *
 *     RETURNS          : NONE 
 *
 *****************************************************************************/

VOID
IssSynceShowDebugging (tCliHandle CliHandle)
{
    INT4                i4ContextId = 0;
    UINT4               u4TraceVal = 0;
    nmhGetFsSynceTraceOption (i4ContextId, &u4TraceVal);

    if (u4TraceVal == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rSyncE :");
    if ((u4TraceVal & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  SyncE initialization and shutdown debugging is on");
    }
    if ((u4TraceVal & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE management debugging is on");
    }
    if ((u4TraceVal & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE data path debugging is on");
    }
    if ((u4TraceVal & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE event debugging is on");
    }
    if ((u4TraceVal & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE packet dump debugging is on");
    }
    if ((u4TraceVal & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  SyncE all resources except buffers debugging is on");
    }
    if ((u4TraceVal & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE all failure debugging is on");
    }
    if ((u4TraceVal & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE buffer debugging is on");
    }
    if ((u4TraceVal & SYNCE_FN_ENTRY_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE function entry debugging is on");
    }
    if ((u4TraceVal & SYNCE_FN_EXIT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  SyncE function exit debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : SynceCliShowEsmcInfo
 *
 *     DESCRIPTION      : This function is used to display network-clock info  
 *                        of SyncE module for interfaces with esmc mode rx.
 *
 *     INPUT            : CliHandle - CLI Handler
 *                      : u4ContextId Context Id
 *     OUTPUT           : NONE
 *
 *     RETURNS          : NONE 
 *
 *****************************************************************************/

VOID
SynceCliShowEsmcRxInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{

    UINT4               u4TraceOption = 0;
    UINT4               u4SystemQlValue = 0;
    UINT4               u4IfContextId = 0;
    UINT4               u4IfQlValue = 0;
    INT4                i4SsmOption = 0;
    INT4                i4QlMode = 0;
    INT4                i4SelectedIf = 0;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4IfPriority = 0;
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4IfEsmcMode = 0;

    if (nmhValidateIndexInstanceFsSynceTable (i4ContextId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% %s\r\n",
                   gaSynceCliErrString[CLI_SYNCE_ERR_SYNCE_DIS_ON_CTX]);
        return;
    }

    nmhGetFsSynceQLMode (i4ContextId, &i4QlMode);
    nmhGetFsSynceSSMOptionMode (i4ContextId, &i4SsmOption);
    nmhGetFsSynceQLValue (i4ContextId, &u4SystemQlValue);
    nmhGetFsSynceSelectedInterface (i4ContextId, &i4SelectedIf);
    nmhGetFsSynceTraceOption (i4ContextId, &u4TraceOption);

    i4RetValue = nmhGetFirstIndexFsSynceIfTable (&i4IfIndex);
    /* show all the synce interfaces with esmc mode rx */
    if (i4RetValue == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n--------------------"
                   "-------------------------------------------\r\n");

        CliPrintf (CliHandle, "Synchronous Ethernet Interface "
                   "Information\r\n");
        CliPrintf (CliHandle, "-------------------"
                   "-------------------------------------------\r\n");
    }

    while (i4RetValue != SNMP_FAILURE)
    {
        if (SynceUtilGetContextIdFromIfIndex ((UINT4) i4IfIndex,
                                              &u4IfContextId) == OSIX_SUCCESS)
        {
            if (u4IfContextId == u4ContextId)
            {
                nmhGetFsSynceIfQLValue (i4IfIndex, (INT4 *) &u4IfQlValue);
                nmhGetFsSynceIfPriority (i4IfIndex, &i4IfPriority);
                nmhGetFsSynceIfEsmcMode (i4IfIndex, &i4IfEsmcMode);
                if ((i4IfEsmcMode == SYNCE_ESMC_MODE_RX))
                {

                    SynceCliShowInterfaceEsmcInfo (CliHandle,
                                                   i4IfIndex,
                                                   u4IfQlValue,
                                                   i4IfPriority, i4SsmOption);
                }
            }
        }

        i4PrevIfIndex = i4IfIndex;
        i4RetValue = nmhGetNextIndexFsSynceIfTable (i4PrevIfIndex, &i4IfIndex);
    }

    CliPrintf (CliHandle, "\r\n");

    return;

}

/*****************************************************************************
 *
 *     FUNCTION NAME    : SynceCliShowEsmcTxInfo
 *
 *     DESCRIPTION      : This function is used to display network-clock info  
 *                        of SyncE module for interfaces with esmc mode tx.
 *
 *     INPUT            : CliHandle - CLI Handler
 *                      : u4ContextId - Context Id
 *     OUTPUT           : NONE
 *
 *     RETURNS          : NONE 
 *
 *
 *****************************************************************************/
VOID
SynceCliShowEsmcTxInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{

    UINT4               u4TraceOption = 0;
    UINT4               u4SystemQlValue = 0;
    UINT4               u4IfContextId = 0;
    UINT4               u4IfQlValue = 0;
    INT4                i4SsmOption = 0;
    INT4                i4QlMode = 0;
    INT4                i4SelectedIf = 0;
    INT4                i4ContextId = (INT4) u4ContextId;
    INT4                i4IfIndex = 0;
    INT4                i4PrevIfIndex = 0;
    INT4                i4IfPriority = 0;
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4IfEsmcMode = 0;
    if (nmhValidateIndexInstanceFsSynceTable (i4ContextId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% %s\r\n",
                   gaSynceCliErrString[CLI_SYNCE_ERR_SYNCE_DIS_ON_CTX]);
        return;
    }

    nmhGetFsSynceQLMode (i4ContextId, &i4QlMode);
    nmhGetFsSynceSSMOptionMode (i4ContextId, &i4SsmOption);
    nmhGetFsSynceQLValue (i4ContextId, &u4SystemQlValue);
    nmhGetFsSynceSelectedInterface (i4ContextId, &i4SelectedIf);
    nmhGetFsSynceTraceOption (i4ContextId, &u4TraceOption);

    i4RetValue = nmhGetFirstIndexFsSynceIfTable (&i4IfIndex);
    /* show all the synce interfaces with esmc mode tx */
    if (i4RetValue == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n--------------------"
                   "------------------------\r\n");
        CliPrintf (CliHandle, "Synchronous Ethernet Interface "
                   "Information\r\n");
        CliPrintf (CliHandle, "-------------------"
                   "-------------------------\r\n");
    }

    while (i4RetValue != SNMP_FAILURE)
    {
        if (SynceUtilGetContextIdFromIfIndex ((UINT4) i4IfIndex,
                                              &u4IfContextId) == OSIX_SUCCESS)
        {
            if (u4IfContextId == u4ContextId)
            {
                nmhGetFsSynceIfQLValue (i4IfIndex, (INT4 *) &u4IfQlValue);
                nmhGetFsSynceIfPriority (i4IfIndex, &i4IfPriority);
                nmhGetFsSynceIfEsmcMode (i4IfIndex, &i4IfEsmcMode);
                if ((i4IfEsmcMode == SYNCE_ESMC_MODE_TX))
                {

                    SynceCliShowInterfaceEsmcInfo (CliHandle,
                                                   i4IfIndex,
                                                   u4IfQlValue,
                                                   i4IfPriority, i4SsmOption);
                }
            }
        }

        i4PrevIfIndex = i4IfIndex;
        i4RetValue = nmhGetNextIndexFsSynceIfTable (i4PrevIfIndex, &i4IfIndex);
    }

    CliPrintf (CliHandle, "\r\n");

    return;

}

/****************************************************************************
 * Function    :  SynceCliShowInterfaceEsmcInfo
 * Description :  This function shows the SyncE Interface information in
 *             :  short form of interfaces with  Esmc mode Rx/Tx .
 * Input       :  CliHandle
 *                u4IfIndex - Interface Identifier
 *             :  i4SsmOption  - SSM Option
 *             :  i4IfPriority - Priority
 * Output      :  None
 * Returns     :  VOID
****************************************************************************/
VOID
SynceCliShowInterfaceEsmcInfo (tCliHandle CliHandle,
                               INT4 i4IfIndex,
                               UINT4 u4IfQlValue,
                               INT4 i4IfPriority, INT4 i4SsmOption)
{
    UINT1               u1SsmCode = 0;

    SynceCliGetSsmCodeFromQlCode ((UINT4) i4SsmOption, (UINT1) u4IfQlValue,
                                  &u1SsmCode);
    CliPrintf (CliHandle,
               "Interface : Gi 0/%-3u  Priority : %-3u  QL : %s(%u)\r\n",
               i4IfIndex, i4IfPriority,
               (gaSynceSsmCodeTable[u1SsmCode][i4SsmOption - 1]).pu1Symbol,
               u4IfQlValue);

}
#endif
