/******************************************************************************
 * Copyright (C) 2013  Aricent Inc . All Rights Reserved 
 *
 * $Id: syetx.c,v 1.3 2013/06/07 13:11:10 siva Exp $ 
 *
 * Description : This file contains the definitions of functions related to
 *               transmission handler sub module.
 ******************************************************************************/
#include "syeinc.h"

/****************************************************************************
* Function    : SynceTxEsmcPacket
* Input       : u4IfIndex - interface index
* Description : This function sends the ESMC PDU on the input interface.
*               ESMC PDU is only sent on the interface only if,
*               ESMC MODE is 'TX'
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
VOID
SynceTxEsmcPacket (UINT4 u4IfIndex)
{
    UINT1              *pu1PktBuf = NULL;
    UINT2               u2PktLen = 0;
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tSynceSsmParams     ssmParams;
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];

    SYNCE_FN_ENTRY ();

    /* check if TX is 'on' for this interface */
    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Synchronous mode is not enabled on interface"
                    " #%d\r\n", u4IfIndex));
        SYNCE_FN_EXIT ();
        return;
    }
    else if (pSynceIfEntry->MibObject.i4FsSynceIfEsmcMode != SYNCE_ESMC_MODE_TX)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "ESMC mode not set as TX for interface"
                    " #%d\r\n", u4IfIndex));
        SYNCE_FN_EXIT ();
        return;
    }

    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();

    u2PktLen = SYNCE_ESMC_MIN_PDU_SIZE;
    pu1PktBuf = MemAllocMemBlk (SYNCE_ESMC_PKT_POOLID);
    if (pu1PktBuf == NULL)
    {
        /* not enough memory */
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_BUFFER_TRC, "Couldn't allocate memory to construct "
                    "ESMC packet\r\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    ssmParams.u1EventFlag = (pSynceIfEntry->b1TxEventPdu ? 1 : 0);
    /*reset the flag */
    pSynceIfEntry->b1TxEventPdu = OSIX_FALSE;

    if (SynceUtilGetSsmCodeFromQlCode
        ((UINT4) pSynceEntry->MibObject.i4FsSynceContextId,
         (UINT1) pSynceEntry->MibObject.u4FsSynceQLValue,
         &(ssmParams.u1SsmCode)) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1PktBuf);
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Couldn't convert QL code to SSM code\r\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    if (SyncePktConstructEsmcPdu (pu1PktBuf, &u2PktLen, &ssmParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1PktBuf);
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Error while constructing ESMC packet\r\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    /* update the source port MAC information */
    MEMSET (au1HwAddr, 0, CFA_ENET_ADDR_LEN);
    if (CfaGetIfHwAddr (u4IfIndex, au1HwAddr) == CFA_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Couldn't get MAC address for inteface #%u\r\n",
                    u4IfIndex));
        /* not returning from here */
    }
    else
    {
        MEMCPY ((pu1PktBuf + SYNCE_ENET_ADDR_LEN), au1HwAddr,
                SYNCE_ENET_ADDR_LEN);
    }

    /* Send the packet to CFA for transmission */
    pCruBuf = CRU_BUF_Allocate_MsgBufChain (u2PktLen, 0);
    if (pCruBuf == NULL)
    {
        MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1PktBuf);
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_BUFFER_TRC, "Allocation of CRU Buffer FAILED!!\r\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    if (CRU_BUF_Copy_OverBufChain (pCruBuf, pu1PktBuf, 0, u2PktLen)
        == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pCruBuf, OSIX_FALSE);
        MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1PktBuf);
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Copying over CRU Buffer failed!!\r\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    if (CfaPostPktFromL2 (pCruBuf, u4IfIndex, u2PktLen,
                          CFA_ENET_SLOW_PROTOCOL,
                          (UINT1) CFA_ENCAP_NONE) == CFA_FAILURE)
    {
        MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1PktBuf);
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Couldn't sent packet to CFA!!\r\n"));
        SYNCE_FN_EXIT ();
        return;
    }

    /* increase packet counter */
    ++(pSynceIfEntry->MibObject.u4FsSynceIfPktsTx);

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC,
                "Transmitted packet over interface #%u\r\n", u4IfIndex));

    MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1PktBuf);

    SYNCE_FN_EXIT ();
    return;
}
