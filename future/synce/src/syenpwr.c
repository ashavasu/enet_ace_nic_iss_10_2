/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id $
 *
 * Description: This file contains the wrapper for SyncE NPAPIs
 *              that will be used for programming the hardware chipsets.
 *****************************************************************************/

#include "syeinc.h"

/***************************************************************************
 *
 *    Function Name       : SynceNpWrHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tSynceNpModInfo
 *
 *    Input(s)            : FsHwNpParam of type tfsHwNp
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
SynceNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1         u1RetVal = FNP_SUCCESS;
    UINT4         u4Opcode = 0;
    tSynceNpModInfo      *pSynceNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pSynceNpModInfo = &(pFsHwNp->SynceNpModInfo);

    if (NULL == pSynceNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_NP_HW_CONFIG_SYNCE_INFO:
        {
            tSynceNpWrFsNpHwConfigSynceInfo *pEntry = NULL;
            pEntry = &pSynceNpModInfo->SynceNpFsNpHwConfigSynceInfo;
            if ( NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpHwConfigSynceInfo (pEntry->pSynceHwSynceInfo);
            break;
        }
    default:
        u1RetVal = FNP_FAILURE;
        break;

    } /* switch*/

    return (u1RetVal);
}
