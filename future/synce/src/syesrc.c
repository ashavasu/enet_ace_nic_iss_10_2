/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
* $Id: syesrc.c,v 1.9 2016/07/06 11:05:11 siva Exp $
*
* Description: This file holds the functions for the ShowRunningConfig
*              for Synce module
*********************************************************************/

#include "syeinc.h"
#include "syesrc.h"
#include "syesrdef.h"

/* Static Variables for Global Config */
PRIVATE UINT1      *gasSsmOptionMode[] = {
    NULL,
    (UINT1 *) "Option 1",
    (UINT1 *) "Option 2 Gen1",
    (UINT1 *) "Option 2 Gen2"
};

PRIVATE UINT1      *gasQlMode[] = {
    (UINT1 *) "QL-Disabled",
    (UINT1 *) "QL-Enabled"
};

/* Static Variables for Interface Config */
PRIVATE UINT1      *gasEsmcMode[] = {
    (UINT1 *) "None",
    (UINT1 *) "Rx",
    (UINT1 *) "Tx"
};

PRIVATE UINT1      *gasQlValue[] = {
    NULL,
    (UINT1 *) "QL-PRC",
    (UINT1 *) "QL-SSU-A",
    (UINT1 *) "QL-SSU-B",
    (UINT1 *) "QL-SEC",
    (UINT1 *) "QL-DNU",
    (UINT1 *) "QL-PRS",
    (UINT1 *) "QL-STU",
    (UINT1 *) "QL-ST2",
    (UINT1 *) "QL-TNC",
    (UINT1 *) "QL-ST3E",
    (UINT1 *) "QL-ST3",
    (UINT1 *) "QL-SIC",
    (UINT1 *) "QL-RES",
    (UINT1 *) "QL-PROV",
    (UINT1 *) "QL-DUS",
};
PRIVATE UINT1      *gasSwitchMode[] = {
    (UINT1 *) "None",
    (UINT1 *) "Force",
    (UINT1 *) "Manual"
};

/****************************************************************************
 Function    : SynceShowRunningConfig
 Description : This function displays the current configuration
                of SYNCE module
 Input       : Variable Arguments
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
SynceShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    INT4                i4FsSynceIfIndex = 0;
    INT4                i4RetValue = SNMP_FAILURE;
    INT4                i4PrevIfIndex = 0;
    UINT1               u1IfMainSubType = 0;

    CliRegisterLock (CliHandle, SynceApiLock, SynceApiUnLock);
    SYNCE_LOCK;

    CliPrintf (CliHandle, "!\r\n");
    SynceShowRunningConfigFsSynceTable (CliHandle, u4Module);

    CliUnRegisterLock (CliHandle);
    SYNCE_UNLOCK;
    if (u4Module == ISS_SYNCE_SHOW_RUNNING_CONFIG)
    {
        i4RetValue = nmhGetFirstIndexFsSynceIfTable (&i4FsSynceIfIndex);

        while (i4RetValue != SNMP_FAILURE)
        {
            CfaGetEthernetType ((UINT4)i4FsSynceIfIndex, &u1IfMainSubType);

            if (u1IfMainSubType == CFA_FA_ENET)
            {

                CliPrintf (CliHandle, "interface Fastethernet 0/%d \r\n",
                           i4FsSynceIfIndex);
            }
            else if (u1IfMainSubType == CFA_GI_ENET)
            {
                CliPrintf (CliHandle, "interface gigabitethernet 0/%d \r\n",
                           i4FsSynceIfIndex);
            }
            else if (u1IfMainSubType == CFA_XE_ENET)
            {
                CliPrintf (CliHandle, "interface ExtremeEthernet 0/%d \r\n",
                           i4FsSynceIfIndex);
            }
            else if (u1IfMainSubType == CFA_XL_ENET)
            {
                CliPrintf (CliHandle, "interface XlEthernet 0/%d \r\n",
                           i4FsSynceIfIndex);
            }
            else if (u1IfMainSubType == CFA_LVI_ENET)
            {
                CliPrintf (CliHandle, "interface LviEthernet 0/%d \r\n",
                           i4FsSynceIfIndex);
            }

            SynceShowRunningConfigFsSynceIfTable (CliHandle, u4Module,
                                                  i4FsSynceIfIndex);
            i4PrevIfIndex = i4FsSynceIfIndex;
            i4RetValue =
                nmhGetNextIndexFsSynceIfTable (i4PrevIfIndex,
                                               &i4FsSynceIfIndex);
            CliPrintf (CliHandle, "!\r\n");
        }

    }

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : SynceShowRunningConfigScalar
 Description : This function displays the current configuration
                of the SYNCE scalars
 Input       : Variable Arguments
 Output      : None
 Returns     : CLI_SUCCESS always
****************************************************************************/
INT4
SynceShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...)
{

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Module);

    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : SynceShowRunningConfigFsSynceTable
 Description : This function displays the current configuration
                of FsSynceTable table
 Input       : Variable Arguments
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
SynceShowRunningConfigFsSynceTable (tCliHandle CliHandle, UINT4 u4Module, ...)
{

    tSynceFsSynceEntry  SynceFsSynceEntry;
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    INT4                i4IfContextId = 0;
    INT4                i4FsSynceContextId = 0;
    INT4                i4NextFsSynceContextId = 0;
    INT4                i4QlMode = 0;
    INT4                i4SsmOpt = 0;
    UINT1               u1IfMainSubType = 0;
    INT4                i4SwitchMode = 0;

    UNUSED_PARAM (u4Module);

    MEMSET (&SynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    if (SynceUtilIsSynceEnabled () == OSIX_FALSE)
    {
            CliPrintf (CliHandle,
                       "shutdown network-clock \r\n");
    }
    if (SNMP_SUCCESS != nmhGetFirstIndexFsSynceTable (&i4FsSynceContextId))
    {
        return CLI_SUCCESS;
    }

    SynceFsSynceEntry.MibObject.i4FsSynceContextId = i4FsSynceContextId;

    while (1)
    {
        if (SynceGetAllFsSynceTable (&SynceFsSynceEntry) != OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        if (SynceFsSynceEntry.MibObject.i4FsSynceQLMode
            != SYNCE_DEF_FSSYNCEQLMODE)
        {
            i4QlMode = SynceFsSynceEntry.MibObject.i4FsSynceQLMode;
            CliPrintf (CliHandle,
                       "network-clock synchronization mode %s \r\n",
                       gasQlMode[i4QlMode]);
        }
        if (SynceFsSynceEntry.MibObject.i4FsSynceSSMOptionMode
            != SYNCE_DEF_FSSYNCESSMOPTIONMODE)
        {
            i4SsmOpt = SynceFsSynceEntry.MibObject.i4FsSynceSSMOptionMode;
            CliPrintf (CliHandle,
                       "network-clock synchronization ssm %s \r\n",
                       gasSsmOptionMode[i4SsmOpt]);
        }

        for (pSynceFsSynceIfEntry = SynceGetFirstFsSynceIfTable ();
             pSynceFsSynceIfEntry != NULL;
             pSynceFsSynceIfEntry =
             SynceGetNextFsSynceIfTable (pSynceFsSynceIfEntry))
        {
            if (SynceUtilGetContextIdFromIfIndex ((UINT4) pSynceFsSynceIfEntry->
                                                  MibObject.i4FsSynceIfIndex,
                                                  (UINT4 *) &i4IfContextId) ==
                OSIX_FAILURE)
            {
                continue;
            }

            if (i4IfContextId != SynceFsSynceEntry.MibObject.i4FsSynceContextId)
            {
                continue;

            }
            CfaGetEthernetType ((UINT4)pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex, 
                                &u1IfMainSubType);

            if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority
                != SYNCE_DEF_FSSYNCEIFPRIORITY)
            {
                if (u1IfMainSubType == CFA_FA_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock priority %d "
                               "interface FastEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority,
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_GI_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock priority %d "
                               "interface gigabitethernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority,
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_XE_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock priority %d "
                               "interface ExtremeEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority,
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_XL_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock priority %d "
                               "interface XlEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority,
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_LVI_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock priority %d "
                               "interface LviEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority,
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }

            }
            if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus
                != SYNCE_DEF_FSSYNCEIFLOCKOUTSTATUS)
            {
                if (u1IfMainSubType == CFA_FA_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock set lockout interface"
                               " FastEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_GI_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock set lockout interface"
                               " gigabitethernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_XE_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock set lockout interface"
                               " ExtremeEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_XL_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock set lockout interface"
                               " XlEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_LVI_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock set lockout interface"
                               " LviEthernet 0/%d \r\n",
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
            }

            if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch
                != SYNCE_DEF_FSSYNCEIFCONFIGSWITCH)
            {
		i4SwitchMode = pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch;
                if (u1IfMainSubType == CFA_FA_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock switch %s interface"
                               " FastEthernet 0/%d \r\n",
                               gasSwitchMode[i4SwitchMode],
			       pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_GI_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock switch %s interface"
                               " gigabitethernet 0/%d \r\n",
                               gasSwitchMode[i4SwitchMode],
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_XE_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock switch %s interface"
                               " ExtremeEthernet 0/%d \r\n",
                               gasSwitchMode[i4SwitchMode],
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_XL_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock switch %s interface"
                               " XlEthernet 0/%d \r\n",
                               gasSwitchMode[i4SwitchMode],
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
                else if (u1IfMainSubType == CFA_LVI_ENET)
                {
                    CliPrintf (CliHandle,
                               "network-clock switch %s interface"
                               " LviEthernet 0/%d \r\n",
                               gasSwitchMode[i4SwitchMode],
                               pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
                }
            }
        }

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsSynceTable (i4FsSynceContextId,
                                         &i4NextFsSynceContextId))
        {
            break;
        }

        SynceFsSynceEntry.MibObject.i4FsSynceContextId = i4NextFsSynceContextId;
    }

    SYNCE_FN_EXIT ();
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : SynceShowRunningConfigFsSynceIfTable
 Description : This function displays the current configuration
                of FsSynceIfTable table
 Input       : Variable Arguments
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
SynceShowRunningConfigFsSynceIfTable (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    tSynceFsSynceIfEntry SynceFsSynceIfEntry;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    INT4                i4FsSynceIfIndex = 0;
    INT4                i4NextFsSynceIfIndex = 0;
    UINT4               u4QlValue = 0;
    INT4                i4EsmcMode = 0;

    va_list             varArgList;

    va_start (varArgList, u4Module);
    i4FsSynceIfIndex = va_arg (varArgList, INT4);
    va_end (varArgList);

    UNUSED_PARAM (u4Module);

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    CliRegisterLock (CliHandle, SynceApiLock, SynceApiUnLock);
    SYNCE_LOCK;

    MEMSET (&SynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    SynceFsSynceIfEntry.MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (&SynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        CliUnRegisterLock (CliHandle);
        SYNCE_UNLOCK;
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return CLI_SUCCESS;
    }
    if (SynceFsSynceIfEntry.MibObject.i4FsSynceIfSynceMode
        != SYNCE_DEF_FSSYNCEIFSYNCEMODE)
    {
        CliPrintf (CliHandle, " synchronous mode \r\n");
    }
    if (SynceFsSynceIfEntry.MibObject.i4FsSynceIfEsmcMode
        != SYNCE_DEF_FSSYNCEIFESMCMODE)
    {
        i4EsmcMode = SynceFsSynceIfEntry.MibObject.i4FsSynceIfEsmcMode;
        CliPrintf (CliHandle, " esmc mode %s \r\n", gasEsmcMode[i4EsmcMode]);
    }
    if (SynceFsSynceIfEntry.MibObject.u4FsSynceIfQLValue
        != SYNCE_DEF_FSSYNCEIFQLVALUE)
    {
        u4QlValue = SynceFsSynceIfEntry.MibObject.u4FsSynceIfQLValue;
        if (SynceFsSynceIfEntry.MibObject.i4FsSynceIfIsRxQLForced != OSIX_FALSE)
        {
            CliPrintf (CliHandle,
                       " network-clock quality-level %s \r\n",
                       gasQlValue[u4QlValue]);
        }
    }

    CliUnRegisterLock (CliHandle);
    SYNCE_UNLOCK;
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);

    UNUSED_PARAM (i4NextFsSynceIfIndex);
    return CLI_SUCCESS;
}
