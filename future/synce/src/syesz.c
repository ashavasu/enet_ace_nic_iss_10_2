
/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syesz.c,v 1.2 2013/04/25 11:37:09 siva Exp $
*
* Description: This file contains sizing information of Synce
*********************************************************************/
#define _SYNCESZ_C
#include "syeinc.h"
INT4
SynceSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYNCE_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsSYNCESizingParams[i4SizingId].
                                     u4StructSize,
                                     FsSYNCESizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(SYNCEMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            SynceSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
SynceSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSYNCESizingParams);
    return OSIX_SUCCESS;
}

VOID
SynceSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYNCE_MAX_SIZING_ID; i4SizingId++)
    {
        if (SYNCEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SYNCEMemPoolIds[i4SizingId]);
            SYNCEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
