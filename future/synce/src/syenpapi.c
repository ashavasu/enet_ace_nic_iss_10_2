/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syenpapi.c,v 1.2 2013/03/30 12:06:31 siva Exp $
 *
 * Description: This file contains the SyncE NPAPIs that will be used for
 *              programming the hardware chipsets.
 *****************************************************************************/

#include "syeinc.h"
#include "nputil.h"

/***************************************************************************
 *
 *    Function Name       : SynceFsNpHwConfigSynceInfo
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpHwConfigSynceInfo
 *
 *    Input(s)            : Arguments of FsNpHwConfigSynceInfo
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
SynceFsNpHwConfigSynceInfo (tSynceHwInfo * pSynceHwSynceInfo)
{
    tFsHwNp             FsHwNp;
    tSynceNpModInfo    *pSynceNpModInfo = NULL;
    tSynceNpWrFsNpHwConfigSynceInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_SYNCE_MOD,    /* Module ID */
                         FS_NP_HW_CONFIG_SYNCE_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pSynceNpModInfo = &(FsHwNp.SynceNpModInfo);
    pEntry = &pSynceNpModInfo->SynceNpFsNpHwConfigSynceInfo;

    pEntry->pSynceHwSynceInfo = pSynceHwSynceInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
