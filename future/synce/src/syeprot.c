/******************************************************************************
 * Copyright (C) 2013  Aricent Inc . All Rights Reserved
 *
 * $Id: syeprot.c,v 1.8 2016/07/06 11:05:10 siva Exp $
 *
 * Description : This file contains the definitions of functions exposed by
 *               Protocol Engine Sub Module in SyncE Module.
 ******************************************************************************/

#ifndef SYNCE_PROT_C
#define SYNCE_PROT_C

#include "syeinc.h"

/*****************************************************************************/
/* Function                  : SynceProtSsmOptionSet                         */
/*                                                                           */
/* Description               : Sets the SSM Option mentioned by the user     */
/*                             and appropriate selected QL in the            */
/*                             context data structure.                       */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier              */
/*                             i4SsmOption - SSM Option to be set            */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtSsmOptionSet (UINT4 u4ContextId, INT4 i4SsmOption)
{
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT4               u4CurContextId = 0;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "Context Id :%d, SSM Option:%d\n",
                u4ContextId, i4SsmOption));

    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4CurContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    if (u4ContextId != u4CurContextId)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    switch (i4SsmOption)
    {
        case SYNCE_SSM_OPTION1:
            pSynceEntry->MibObject.u4FsSynceQLValue = SYNCE_QL_DNU;
            break;
        default:
            pSynceEntry->MibObject.u4FsSynceQLValue = SYNCE_QL_DUS;
            break;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceProtQlModeSet                            */
/*                                                                           */
/* Description               : Sets the QL Mode mentioned by the user        */
/*                             in the context data structure.                */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier              */
/*                             i4QlMode    - QL Mode to be set               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtQlModeSet (UINT4 u4ContextId, INT4 i4QlMode)
{
    tSynceStateData     smData;

    SYNCE_FN_ENTRY ();
    UNUSED_PARAM (i4QlMode);

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "Context Id :%d \n",
                u4ContextId));
    MEMSET (&smData, 0, sizeof (smData));
    smData.u4IfContextId = u4ContextId;

    if (SynceSmTriggerEvent (u4ContextId, QL_MODE_CHANGED_EVENT,
                             &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceProtIfSynceModeSet                       */
/*                                                                           */
/* Description               : Sets the SyncE enabled or disabled            */
/*                             in the interface data structure.              */
/*                                                                           */
/* Input                     : u4IfIndex - Interface Index                   */
/*                             i4Mode    - Synchronous mode to be set        */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfSynceModeSet (UINT4 u4IfIndex, INT4 i4Mode)
{
    tSynceFsSynceEntry *pSynceEntry = NULL;
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4IfContextId = 0;
    tSynceStateData     smData;
    UINT1               u1IfOperStatus = 0;
    INT4                i4RetStatus = OSIX_SUCCESS;
#ifdef NPAPI_WANTED
    tSynceHwInfo        synceHwInfo;
#endif

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d, SyncE Mode:%d\n",
                u4IfIndex, i4Mode));
    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();
    if (SynceUtilGetContextIdFromIfIndex (u4IfIndex,
                                          &u4IfContextId) != OSIX_SUCCESS)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    if (u4ContextId != u4IfContextId)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    if (pSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced != OSIX_TRUE)
    {
        switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
        {
            case SYNCE_SSM_OPTION1:
                pSynceIfEntry->MibObject.u4FsSynceIfQLValue = SYNCE_QL_DNU;
                break;

            default:
                pSynceIfEntry->MibObject.u4FsSynceIfQLValue = SYNCE_QL_DUS;
                break;
        }
    }

    MEMSET (&smData, 0, sizeof (smData));

    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();

    if (SynceSmTriggerEvent (u4ContextId,
                             SYNCE_MODE_CHANGED_EVENT, &smData) == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "%% SM returned failure\n"));
        return OSIX_FAILURE;
    }

    /* synce mode is enabled */
    if (i4Mode == OSIX_TRUE)
    {
        /* if interface is operationally up then signal fail will be set to
         * false
         */
        CfaGetIfOperStatus (u4IfIndex, &u1IfOperStatus);
        if (u1IfOperStatus == CFA_IF_UP)
        {
            i4RetStatus =
                SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_RESTORE);
        }
        else
        {
            i4RetStatus = SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_FAIL);
        }
    }
    else                        /* synce mode is disabled */
    {
        /* stop all running timer on this node */
        SynceTmrStopAllTimers (u4IfIndex);
    }

#ifdef NPAPI_WANTED
    /* HW call */
    MEMSET (&synceHwInfo, 0, sizeof (tSynceHwInfo));
    synceHwInfo.unHwParam.SynceHwSynceStatusInfo.u4ContextId = u4ContextId;
    synceHwInfo.unHwParam.SynceHwSynceStatusInfo.u4IfIndex = u4IfIndex;
    synceHwInfo.unHwParam.SynceHwSynceStatusInfo.u4SynceStatus = i4Mode;
    synceHwInfo.u4InfoType = SYNCE_HW_SET_SYNCE_STATUS;
    if (SynceFsNpHwConfigSynceInfo (&synceHwInfo) != FNP_SUCCESS)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "%% HW Call Failed\n"));
    }
#endif
    SYNCE_FN_EXIT ();
    return i4RetStatus;
}

/*****************************************************************************/
/* Function                  : SynceProtIfQLSet                              */
/*                                                                           */
/* Description               : Sets the QL value on an interface             */
/*                             in the interface data structure.              */
/*                                                                           */
/* Input                     : u4IfIndex - Interface Index                   */
/*                             u4QlValue - QL value to be set                */
/*                             u1QlSource - Source of QL                     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
INT4
SynceProtIfQLSet (UINT4 u4IfIndex, UINT4 u4QlCode, UINT1 u1QlSource)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT4               u4ContextId = 0;
    tSynceStateData     smData;

    SYNCE_FN_ENTRY ();

    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d, QL:%d, Source: %d\n",
                u4IfIndex, u4QlCode, u1QlSource));

    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    if (u1QlSource == SYNCE_QL_SOURCE_USER)
    {
        if (u4QlCode == 0)
        {
            /* reset the previously assigned QL */
            pSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced = OSIX_FALSE;
            switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
            {
                case SYNCE_SSM_OPTION1:
                    pSynceIfEntry->MibObject.u4FsSynceIfQLValue = SYNCE_QL_DNU;
                    break;
                default:
                    pSynceIfEntry->MibObject.u4FsSynceIfQLValue = SYNCE_QL_DUS;
                    break;
            }
        }
        else
        {
            pSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced = OSIX_TRUE;
        }
    }
    else
    {
        if (pSynceEntry->MibObject.i4FsSynceQLMode == SYNCE_QL_MODE_DISABLE)
        {
            pSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped++;
            SYNCE_FN_EXIT ();
            return OSIX_SUCCESS;
        }

        /* Packet is received on this interface, now there are 2 cases;
         * case 1 : interface was up and running and packets were being received
         * on this interface. In this condition RX(signal fail) timer would
         * be running on this interface, reset that timer.
         * case 2: this interface was down and this is the first packet received
         * on this interface, make the interface up.
         */

        if (OSIX_FALSE == SynceTmrIsTimerRunning (u4IfIndex, SYNCE_RX_TMR))
        {
            SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_RESTORE);
        }

        /* packet has come till here, so we can assume esmc mode is rx */
        SynceTmrRestart (u4IfIndex, SYNCE_RX_TMR,
                         SYNCE_DEF_SIGNAL_FAIL_TIME, 0);

        if (pSynceIfEntry->MibObject.i4FsSynceIfSignalFail == OSIX_TRUE)
        {
            pSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped++;
            SYNCE_FN_EXIT ();
            return OSIX_SUCCESS;
        }

        if (pSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced != OSIX_TRUE)
        {
            pSynceIfEntry->MibObject.u4FsSynceIfQLValue = u4QlCode;
        }
    }

    MEMSET (&smData, 0, sizeof (smData));

    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    if (SynceSmTriggerEvent (u4ContextId, QL_CHANGED_EVENT,
                             &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SynceTrapRaiseTrap (pSynceIfEntry, SYNCE_TRAP_INF_QL_CHANGED);

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceProtIfEsmcModeSet                        */
/*                                                                           */
/* Description               : Sets the ESMC mode mentioned by the user      */
/*                             in the interface data structure.              */
/*                                                                           */
/* Input                     : u4IfIndex  - Interface Index                  */
/*                             i4EsmcMode - Esmc Mode to be set              */
/*                             i4PrevEsmcMode - Previous Esmc Mode           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfEsmcModeSet (UINT4 u4IfIndex, INT4 i4EsmcMode, INT4 i4PrevEsmcMode)
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1IfOperStatus = 0;
    tSynceStateData     smData;

#ifdef NPAPI_WANTED
    tSynceHwInfo        synceHwInfo;
#endif

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d, EsmcMode:%d,"
                "PrevEsmcMode: %d\n", u4IfIndex, i4EsmcMode, i4PrevEsmcMode));

    if (i4EsmcMode == i4PrevEsmcMode)
    {
        SYNCE_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    CfaGetIfOperStatus (u4IfIndex, &u1IfOperStatus);
    /* no timer manipulation is needed if interface is oper down */
    if (u1IfOperStatus != CFA_IF_UP)
    {
        SYNCE_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    pSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceIfEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    switch (i4EsmcMode)
    {
        case SYNCE_ESMC_MODE_NONE:
            switch (i4PrevEsmcMode)
            {
                case SYNCE_ESMC_MODE_RX:
                    /* rx timer may be running */
                    if (SynceTmrIsTimerRunning (u4IfIndex, SYNCE_RX_TMR) ==
                        OSIX_TRUE)
                    {
                        SynceTmrStop (u4IfIndex, SYNCE_RX_TMR);
                    }
                    else
                    {
                        SynceTmrStop (u4IfIndex, SYNCE_RX_TMR);
                        /* it would be in signal fail state, move it to
                         * correct state.
                         */
                        SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_RESTORE);
                    }
                    break;
                case SYNCE_ESMC_MODE_TX:
                    SynceTmrStop (u4IfIndex, SYNCE_TX_TMR);
                    break;
                default:
                    break;
            }

            if (pSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced != OSIX_TRUE)
            {
                switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
                {
                    case SYNCE_SSM_OPTION1:
                        pSynceIfEntry->MibObject.
                            u4FsSynceIfQLValue = SYNCE_QL_DNU;
                        break;
                    default:
                        pSynceIfEntry->MibObject.
                            u4FsSynceIfQLValue = SYNCE_QL_DUS;
                        break;
                }

                MEMSET (&smData, 0, sizeof (smData));
                smData.u4IfIndex = u4IfIndex;
                smData.u4IfContextId = u4ContextId;
                smData.pSynceEntry = pSynceEntry;

                if (SynceSmTriggerEvent (u4ContextId, QL_CHANGED_EVENT,
                                         &smData) == OSIX_FAILURE)
                {
                    SYNCE_FN_EXIT ();
                    return OSIX_FAILURE;
                }
            }

            break;
        case SYNCE_ESMC_MODE_RX:
            switch (i4PrevEsmcMode)
            {
                case SYNCE_ESMC_MODE_TX:
                    SynceTmrStop (u4IfIndex, SYNCE_TX_TMR);
                    break;

                case SYNCE_ESMC_MODE_NONE:
                    if (pSynceEntry->MibObject.i4FsSynceQLMode
                        == SYNCE_QL_MODE_DISABLE)
                    {
                        SYNCE_TRC ((SYNCE_ALL_FAILURE_TRC |
                                    SYNCE_ALL_FAILURE_TRC,
                                    " Can't set Esmc mode = Rx :"
                                    "SYNCE_QL_MODE_DISABLED"));
                        SYNCE_FN_EXIT ();
                        return OSIX_FAILURE;
                    }
                    break;
                default:
                    break;
            }
            SynceTmrStart (u4IfIndex, SYNCE_RX_TMR,
                           SYNCE_DEF_SIGNAL_FAIL_TIME, 0);

            break;
        case SYNCE_ESMC_MODE_TX:
            switch (i4PrevEsmcMode)
            {
                case SYNCE_ESMC_MODE_RX:
                    if (SynceTmrIsTimerRunning (u4IfIndex, SYNCE_RX_TMR) ==
                        OSIX_TRUE)
                    {
                        SynceTmrStop (u4IfIndex, SYNCE_RX_TMR);
                    }
                    else
                    {
                        SynceTmrStop (u4IfIndex, SYNCE_RX_TMR);
                        /* it would be in signal fail state, move it to
                         * correct state.
                         */
                        SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_RESTORE);
                    }
                    /*intentional fall through */
                case SYNCE_ESMC_MODE_NONE:
                    SynceTmrStart (u4IfIndex, SYNCE_TX_TMR,
                                   SYNCE_DEF_PACKET_TX_TIME, 0);

                    break;
                default:
                    break;
            }
            /* When an interface is changed to TX mode we shall change its QL
             * to DNU, if it is not forced by user
             */
            if (pSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced != OSIX_TRUE)
            {
                switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
                {
                    case SYNCE_SSM_OPTION1:
                        pSynceIfEntry->MibObject.
                            u4FsSynceIfQLValue = SYNCE_QL_DNU;
                        break;
                    default:
                        pSynceIfEntry->MibObject.
                            u4FsSynceIfQLValue = SYNCE_QL_DUS;
                        break;
                }
            }

            /* if it was selected interface then clock selection has to be
             * triggered so that other interface could be chosen
             */
            if (u4IfIndex == (UINT4) pSynceEntry->MibObject.
                i4FsSynceSelectedInterface)
            {
                MEMSET (&smData, 0, sizeof (smData));
                smData.u4IfIndex = u4IfIndex;
                smData.u4IfContextId = u4ContextId;
                smData.pSynceEntry = pSynceEntry;

                if (SynceSmTriggerEvent (u4ContextId, QL_CHANGED_EVENT,
                                         &smData) == OSIX_FAILURE)
                {
                    SYNCE_FN_EXIT ();
                    return OSIX_FAILURE;
                }
            }

            break;
        default:
            SYNCE_FN_EXIT ();
            return OSIX_FAILURE;
            break;
    }

#ifdef NPAPI_WANTED
    /* HW call */
    MEMSET (&synceHwInfo, 0, sizeof (tSynceHwInfo));
    synceHwInfo.unHwParam.SynceHwSynceStatusInfo.u4ContextId = u4ContextId;
    synceHwInfo.unHwParam.SynceHwSynceStatusInfo.u4IfIndex = u4IfIndex;
    synceHwInfo.unHwParam.SynceHwSynceStatusInfo.u4SynceStatus = i4EsmcMode;
    synceHwInfo.u4InfoType = SYNCE_HW_SET_SYNCE_STATUS;
    if (SynceFsNpHwConfigSynceInfo (&synceHwInfo) != FNP_SUCCESS)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "%% HW Call Failed\n"));
    }
#endif

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceProtIfPrioritySet                        */
/*                                                                           */
/* Description               : Sets the Priority mentioned by the user       */
/*                             in the interface data structure.              */
/*                                                                           */
/* Input                     : u4IfIndex  - Interface Index                  */
/*                             i4Priority - Priority to be set               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfPrioritySet (UINT4 u4IfIndex, INT4 i4Priority)
{
    UINT4               u4ContextId = 0;
    tSynceStateData     smData;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d, Priority:%d\n",
                u4IfIndex, i4Priority));

    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    MEMSET (&smData, 0, sizeof (smData));

    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    if (OSIX_FAILURE == SynceSmTriggerEvent (u4ContextId,
                                             PRIORITY_CHANGED_EVENT, &smData))
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "%% SM returned failure\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (i4Priority);

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceProtIfLockoutSet                         */
/*                                                                           */
/* Description               : Sets the Lockout on an interface.             */
/*                                                                           */
/* Input                     : u4IfIndex  - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfLockoutSet (UINT4 u4IfIndex)
{
    UINT4               u4ContextId = 0;
    tSynceStateData     smData;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d\n", u4IfIndex));
    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    MEMSET (&smData, 0, sizeof (smData));

    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    if (SynceSmTriggerEvent (u4ContextId, ENABLE_LOCKOUT_EVENT,
                             &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceIfLockoutClear                           */
/*                                                                           */
/* Description               : Clears Lockout on an interface.               */
/*                                                                           */
/* Input                     : u4IfIndex  - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfLockoutClear (UINT4 u4IfIndex)
{
    UINT4               u4ContextId;
    tSynceStateData     smData;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d\n", u4IfIndex));
    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    MEMSET (&smData, 0, sizeof (smData));
    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    if (SynceSmTriggerEvent (u4ContextId, CLEAR_LOCKOUT_EVENT,
                             &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceProtIfConfigSwitchManualSet              */
/*                                                                           */
/* Description               : Sets manual switch on an interface.           */
/*                                                                           */
/* Input                     : u4IfIndex  - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfConfigSwitchManualSet (UINT4 u4IfIndex)
{
    UINT4               u4ContextId = 0;
    tSynceStateData     smData;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d\n", u4IfIndex));
    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    MEMSET (&smData, 0, sizeof (smData));

    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    if (SynceSmTriggerEvent (u4ContextId, SWITCH_MODE_MANUAL_EVENT,
                             &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceProtIfConfigSwitchForceSet               */
/*                                                                           */
/* Description               : Sets force switch on an interface.            */
/*                                                                           */
/* Input                     : u4IfIndex  - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfConfigSwitchForceSet (UINT4 u4IfIndex)
{
    UINT4               u4ContextId = 0;
    tSynceStateData     smData;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d\n", u4IfIndex));
    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    MEMSET (&smData, 0, sizeof (smData));

    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    if (SynceSmTriggerEvent (u4ContextId, SWITCH_MODE_FORCED_EVENT,
                             &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}
/*****************************************************************************/
/* Function                  : SynceIfConfigSwitchClear                      */
/*                                                                           */
/* Description               : Clears force/manual switch on an interface.   */
/*                                                                           */
/* Input                     : u4IfIndex  - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfConfigSwitchClear (UINT4 u4IfIndex)
{
    UINT4               u4ContextId;
    tSynceStateData     smData;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d\n", u4IfIndex));
    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    MEMSET (&smData, 0, sizeof (smData));
    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    if (SynceSmTriggerEvent (u4ContextId, CLEAR_SWITCH_EVENT,
                             &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}
/*****************************************************************************/
/* Function                  : SynceProtIfSignalFail                         */
/*                                                                           */
/* Description               : Generates a trap when the signal fail         */
/*                             occurs or when restored.                      */
/*                                                                           */
/* Input                     : u4IfIndex      - Interface Index              */
/*                             u1SigFailState - Signal Fail or restore       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

INT4
SynceProtIfSignalFail (UINT4 u4IfIndex, UINT1 u1SigFailState)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    tSynceStateData     smData;
    UINT4               u4ContextId = 0;
    UINT1               u1SmEvent = 0;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d, SignalFail:%d\n",
                u4IfIndex, u1SigFailState));
    pSynceFsSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (NULL == pSynceFsSynceIfEntry)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pSynceEntry = SYNCE_CURR_CONTEXT_INFO ();

    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    u4ContextId = (UINT4) SYNCE_CURR_CONTEXT_ID ();

    switch (u1SigFailState)
    {
        case SYNCE_SIGNAL_RESTORE:
        {
            pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSignalFail = OSIX_FALSE;
            switch (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode)
            {
                case SYNCE_ESMC_MODE_RX:
                    /* start the timer if not already running */
                    if (OSIX_FALSE == SynceTmrIsTimerRunning (u4IfIndex,
                                                              SYNCE_RX_TMR))
                    {
                        SynceTmrStart (u4IfIndex, SYNCE_RX_TMR,
                                       SYNCE_DEF_SIGNAL_FAIL_TIME, 0);
                    }

                    break;
                case SYNCE_ESMC_MODE_TX:
                    SynceTmrStart (u4IfIndex, SYNCE_TX_TMR,
                                   SYNCE_DEF_PACKET_TX_TIME, 0);
                    break;
                default:
                    break;
            }
        }
            break;
        case SYNCE_SIGNAL_FAIL:
        {
            /* reset the QL only in case when it was not set by user */
            if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced ==
                OSIX_FALSE)
            {
                switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
                {
                    case SYNCE_SSM_OPTION1:
                        pSynceFsSynceIfEntry->MibObject.
                            u4FsSynceIfQLValue = SYNCE_QL_DNU;
                        break;
                    default:
                        pSynceFsSynceIfEntry->MibObject.
                            u4FsSynceIfQLValue = SYNCE_QL_DUS;
                        break;
                }
            }

            pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSignalFail = OSIX_TRUE;
            SynceTmrStopAllTimers (u4IfIndex);
        }
            break;
        default:
            break;
    }

    MEMSET (&smData, 0, sizeof (smData));
    smData.u4IfIndex = u4IfIndex;
    smData.u4IfContextId = u4ContextId;
    smData.pSynceEntry = pSynceEntry;

    /* In QL-Enabled mode signal fail condition make the QL value on that
     * interface to be treated as QL-Failed.
     * SM does not do anything on signal fail in QL-Enabled mode, so sending
     * a QL-change event instead.
     */
    switch (pSynceEntry->MibObject.i4FsSynceQLMode)
    {
        case SYNCE_QL_MODE_ENABLE:
            u1SmEvent = QL_CHANGED_EVENT;
            break;
        case SYNCE_QL_MODE_DISABLE:
            u1SmEvent = SIGNAL_STATUS_CHANGED_EVENT;
            break;
        default:
            break;
    }

    if (SynceSmTriggerEvent (u4ContextId, u1SmEvent, &smData) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SynceTrapRaiseTrap (pSynceFsSynceIfEntry, SYNCE_TRAP_SIGNAL_STATUS_CHANGE);

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function                  : SynceProtIfDeletionHandler
 * Description               : Handles deletion of an interface
 * Input                     : u4IfIndex      - Interface Index
 * Output                    : None
 * Global Variables Referred : gSynceGlobals
 * Global Variables Modified : gSynceGlobals
 * Use of Recursion          : None
 * Returns                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

INT4
SynceProtIfDeletionHandler (UINT4 u4IfIndex)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    SYNCE_FN_ENTRY ();

    pSynceFsSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (NULL != pSynceFsSynceIfEntry)
    {
        /* free this entry */
        nmhSetFsSynceIfRowStatus ((INT4) u4IfIndex, DESTROY);
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : SynceProtIfStatusChangeHandler
 *
 * Description               : Handles interface up/down status
 *                             change of an interface.
 *
 * Input                     : u4IfIndex      - Interface Index
 *                             u1ChangeType   - Interface Up/Down
 *
 * Output                    : None
 *
 * Global Variables Referred : gSynceGlobals
 * Global Variables Modified : gSynceGlobals
 * Returns                   : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

INT4
SynceProtIfStatusChangeHandler (UINT4 u4IfIndex, UINT1 u1ChangeType)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    SYNCE_FN_ENTRY ();

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "If Index :%d, OperStatus:%d\n",
                u4IfIndex, u1ChangeType));

    pSynceFsSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (NULL == pSynceFsSynceIfEntry)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (CFA_IF_UP == u1ChangeType)
    {
        SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_RESTORE);
    }
    else                        /* if CFA_IF_DOWN  */
    {
        SynceProtIfSignalFail (u4IfIndex, SYNCE_SIGNAL_FAIL);
    }

    SYNCE_FN_EXIT ();

    return OSIX_SUCCESS;
}

#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file  synceprot.c                    */
/*-----------------------------------------------------------------------*/
