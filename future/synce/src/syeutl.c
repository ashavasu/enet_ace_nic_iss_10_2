/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syeutl.c,v 1.3 2016/07/06 11:05:11 siva Exp $
*
* Description: This file contains utility functions used by protocol Synce
*********************************************************************/

#include "syeinc.h"
#include "syesrc.h"

/****************************************************************************
 Function    :  SynceUtlDestroyRBTree
 Input       :  None
 Description :  This function delete all the created RB trees.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
SynceUtlDestroyRBTree ()
{
    if (gSynceGlobals.glbMib.FsSynceTable != NULL)
    {
        RBTreeDestroy (gSynceGlobals.glbMib.FsSynceTable, NULL, 0);
        gSynceGlobals.glbMib.FsSynceTable = NULL;
    }

    if (gSynceGlobals.glbMib.FsSynceIfTable != NULL)
    {
        RBTreeDestroy (gSynceGlobals.glbMib.FsSynceIfTable, NULL, 0);
        gSynceGlobals.glbMib.FsSynceIfTable = NULL;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceGetAllUtlFsSynceTable
 Input       :  pSynceGetFsSynceEntry
                pSyncedsFsSynceEntry
 Description :  This routine Gets the
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceGetAllUtlFsSynceTable (tSynceFsSynceEntry * pSynceGetFsSynceEntry,
                            tSynceFsSynceEntry * pSyncedsFsSynceEntry)
{
    UNUSED_PARAM (pSynceGetFsSynceEntry);
    UNUSED_PARAM (pSyncedsFsSynceEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceGetAllUtlFsSynceIfTable
 Input       :  pSynceGetFsSynceIfEntry
                pSyncedsFsSynceIfEntry
 Description :  This routine Gets the
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceGetAllUtlFsSynceIfTable (tSynceFsSynceIfEntry * pSynceGetFsSynceIfEntry,
                              tSynceFsSynceIfEntry * pSyncedsFsSynceIfEntry)
{
    UNUSED_PARAM (pSynceGetFsSynceIfEntry);
    UNUSED_PARAM (pSyncedsFsSynceIfEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  SynceUtilUpdateFsSynceTable
 * Input       :   pSynceOldFsSynceEntry
                   pSynceFsSynceEntry
                   pSynceIsSetFsSynceEntry
 * Descritpion :  This Routine checks set value
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceUtilUpdateFsSynceTable (tSynceFsSynceEntry * pSynceOldFsSynceEntry,
                             tSynceFsSynceEntry * pSynceFsSynceEntry,
                             tSynceIsSetFsSynceEntry * pSynceIsSetFsSynceEntry)
{
    INT4                i4RetValue = OSIX_SUCCESS;
    tSynceIsSetFsSynceEntry *pSynceCheckIsSetFsSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    if ((pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus == OSIX_FALSE)
        && (pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus != ACTIVE))
    {
        SYNCE_FN_EXIT ();
        return i4RetValue;
    }

    /* check if values are different than the stored values */
    pSynceCheckIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);
    if (pSynceCheckIsSetFsSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMCPY (pSynceCheckIsSetFsSynceEntry, pSynceIsSetFsSynceEntry,
            sizeof (tSynceIsSetFsSynceEntry));

    if (pSynceOldFsSynceEntry != NULL)
    {
        FsSynceTableFilterInputs (pSynceOldFsSynceEntry, pSynceFsSynceEntry,
                                  pSynceCheckIsSetFsSynceEntry);
    }

    if (SYNCE_SELECT_CONTEXT
        ((UINT4) pSynceFsSynceEntry->MibObject.i4FsSynceContextId) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceCheckIsSetFsSynceEntry);
        return OSIX_FAILURE;
    }

    if (pSynceCheckIsSetFsSynceEntry->bFsSynceTraceOption != OSIX_FALSE)
    {
        SYNCE_CTX_TRC_FLAG = pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption;
    }

    if (pSynceCheckIsSetFsSynceEntry->bFsSynceQLMode != OSIX_FALSE)
    {
        i4RetValue =
            SynceProtQlModeSet ((UINT4) pSynceFsSynceEntry->MibObject.
                                i4FsSynceContextId,
                                pSynceFsSynceEntry->MibObject.i4FsSynceQLMode);
    }

    if (pSynceCheckIsSetFsSynceEntry->bFsSynceSSMOptionMode != OSIX_FALSE)
    {
        i4RetValue =
            SynceProtSsmOptionSet
            ((UINT4) pSynceFsSynceEntry->MibObject.i4FsSynceContextId,
             pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode);
    }

    SYNCE_RELEASE_CONTEXT ();

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceCheckIsSetFsSynceEntry);

    SYNCE_FN_EXIT ();
    return i4RetValue;
}

/****************************************************************************
 * Function    :  SynceUtilUpdateFsSynceIfTable
 * Input       :   pSynceOldFsSynceIfEntry
                   pSynceFsSynceIfEntry
                   pSynceIsSetFsSynceIfEntry
 * Descritpion :  This Routine checks set value
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceUtilUpdateFsSynceIfTable (tSynceFsSynceIfEntry * pSynceOldFsSynceIfEntry,
                               tSynceFsSynceIfEntry * pSynceFsSynceIfEntry,
                               tSynceIsSetFsSynceIfEntry *
                               pSynceIsSetFsSynceIfEntry)
{
    INT4                i4RetValue = OSIX_SUCCESS;
    UINT4               u4ContextId = 0;
    tSynceIsSetFsSynceIfEntry *pSynceCheckIsSetFsSynceIfEntry = NULL;

    SYNCE_FN_ENTRY ();

    /* check if values are different than the stored values */
    pSynceCheckIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceCheckIsSetFsSynceIfEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMCPY (pSynceCheckIsSetFsSynceIfEntry, pSynceIsSetFsSynceIfEntry,
            sizeof (tSynceIsSetFsSynceIfEntry));

    if (pSynceOldFsSynceIfEntry != NULL)
    {
        FsSynceIfTableFilterInputs (pSynceOldFsSynceIfEntry,
                                    pSynceFsSynceIfEntry,
                                    pSynceCheckIsSetFsSynceIfEntry);
    }

    if (SynceUtilGetContextIdFromIfIndex ((UINT4) pSynceFsSynceIfEntry->
                                          MibObject.i4FsSynceIfIndex,
                                          &u4ContextId) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceCheckIsSetFsSynceIfEntry);
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (SYNCE_SELECT_CONTEXT (u4ContextId) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceCheckIsSetFsSynceIfEntry);
        return OSIX_FAILURE;
    }

    if (pSynceCheckIsSetFsSynceIfEntry->bFsSynceIfRowStatus != OSIX_FALSE)
    {
        /* if row status is being set, act accordingly */
        switch (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus)
        {
            case DESTROY:
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority = 0;
                /* intentional fall through */
            case NOT_IN_SERVICE:
            case NOT_READY:
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode =
                    OSIX_FALSE;
                i4RetValue =
                    SynceProtIfSynceModeSet ((UINT4) pSynceFsSynceIfEntry->
                                             MibObject.i4FsSynceIfIndex,
                                             pSynceFsSynceIfEntry->MibObject.
                                             i4FsSynceIfSynceMode);

                break;
            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                SYNCE_RELEASE_CONTEXT ();
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceCheckIsSetFsSynceIfEntry);
                SYNCE_FN_EXIT ();
                return OSIX_SUCCESS;

                break;
            case ACTIVE:
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode =
                    OSIX_TRUE;
                i4RetValue =
                    SynceProtIfSynceModeSet ((UINT4) pSynceFsSynceIfEntry->
                                             MibObject.i4FsSynceIfIndex,
                                             pSynceFsSynceIfEntry->MibObject.
                                             i4FsSynceIfSynceMode);

                break;
            default:
                SYNCE_RELEASE_CONTEXT ();

                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceCheckIsSetFsSynceIfEntry);
                SYNCE_FN_EXIT ();
                return OSIX_SUCCESS;
        }
    }

    switch (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus)
    {
        case ACTIVE:
            if (pSynceCheckIsSetFsSynceIfEntry->bFsSynceIfEsmcMode !=
                OSIX_FALSE)
            {
                if (pSynceOldFsSynceIfEntry != NULL)
                {
                    i4RetValue = SynceProtIfEsmcModeSet ((UINT4)
                                                         pSynceFsSynceIfEntry->
                                                         MibObject.
                                                         i4FsSynceIfIndex,
                                                         pSynceFsSynceIfEntry->
                                                         MibObject.
                                                         i4FsSynceIfEsmcMode,
                                                         pSynceOldFsSynceIfEntry->
                                                         MibObject.
                                                         i4FsSynceIfEsmcMode);
                }
            }

            /* intentional fall through */
        case NOT_READY:
        case NOT_IN_SERVICE:
            if (pSynceCheckIsSetFsSynceIfEntry->bFsSynceIfQLValue != OSIX_FALSE)
            {
                i4RetValue = SynceProtIfQLSet ((UINT4) pSynceFsSynceIfEntry->
                                               MibObject.i4FsSynceIfIndex,
                                               pSynceFsSynceIfEntry->MibObject.
                                               u4FsSynceIfQLValue,
                                               SYNCE_QL_SOURCE_USER);
            }
            if (pSynceCheckIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus !=
                OSIX_FALSE)
            {
                if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus
                    == OSIX_TRUE)
                {
                    i4RetValue = SynceProtIfLockoutSet ((UINT4)
                                                        pSynceFsSynceIfEntry->
                                                        MibObject.
                                                        i4FsSynceIfIndex);
                }
                else
                {
                    i4RetValue = SynceProtIfLockoutClear ((UINT4)
                                                          pSynceFsSynceIfEntry->
                                                          MibObject.
                                                          i4FsSynceIfIndex);
                }
            }
            if (pSynceCheckIsSetFsSynceIfEntry->bFsSynceIfPriority !=
                OSIX_FALSE)
            {
                i4RetValue = SynceProtIfPrioritySet ((UINT4)
                                                     pSynceFsSynceIfEntry->
                                                     MibObject.i4FsSynceIfIndex,
                                                     pSynceFsSynceIfEntry->
                                                     MibObject.
                                                     i4FsSynceIfPriority);
            }

            if (pSynceCheckIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch !=
                OSIX_FALSE)
            {
                if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch
                    == SYNCE_NO_SWITCH_MODE)
                {
                    i4RetValue = SynceProtIfConfigSwitchClear ((UINT4)
                                                        pSynceFsSynceIfEntry->
                                                        MibObject.
                                                        i4FsSynceIfIndex);
                }
		else if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch
                         == SYNCE_FORCED_SWITCH_MODE)
                {
                    i4RetValue = SynceProtIfConfigSwitchForceSet ((UINT4)
                                                          pSynceFsSynceIfEntry->
                                                          MibObject.
                                                          i4FsSynceIfIndex);
                }
		else
		{
                    i4RetValue = SynceProtIfConfigSwitchManualSet ((UINT4)
                                                          pSynceFsSynceIfEntry->
                                                          MibObject.
                                                          i4FsSynceIfIndex);
		}
            }
            break;
        default:
            break;
    }

    SYNCE_RELEASE_CONTEXT ();

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceCheckIsSetFsSynceIfEntry);

    SYNCE_FN_EXIT ();
    return i4RetValue;
}

/******************************************************************************
 * Function   : SynceUtilCreateContext
 * Description: This function will create a context node in synce table.
 * Input      : Context Id
 * Output     : none
 * Returns    : OSIX_SUCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SynceUtilCreateContext (UINT4 u4ContextId)
{
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    pSynceEntry = (tSynceFsSynceEntry *)
        MemAllocMemBlk (gSynceGlobals.contextPoolId);
    if (pSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC
                    | SYNCE_CRITICAL_TRC, "Memory Allocation failed\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pSynceEntry->MibObject.i4FsSynceContextId = (INT4) u4ContextId;
    SynceInitializeFsSynceTable (pSynceEntry);
    if (RBTreeAdd (gSynceGlobals.glbMib.FsSynceTable, (tRBElem *) pSynceEntry)
        == RB_FAILURE)
    {
        MemReleaseMemBlock (gSynceGlobals.contextPoolId, (UINT1 *) pSynceEntry);
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC
                    | SYNCE_CRITICAL_TRC, "Node addition in RBTree failed\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Initialize the trace variable array */
    gau4SynceTraceOptions[u4ContextId] =
        pSynceEntry->MibObject.u4FsSynceTraceOption;

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : SynceUtilDeleteContext
 * Description: This function will delete a context node from synce table.
 * Input      : Context Id
 * Output     : none
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
INT4
SynceUtilDeleteContext (UINT4 u4ContextId)
{
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    pSynceEntry = SynceGetFsSynceEntry (u4ContextId);
    if (pSynceEntry == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    RBTreeRemove (gSynceGlobals.glbMib.FsSynceTable, (tRBElem *) pSynceEntry);
    MemReleaseMemBlock (gSynceGlobals.contextPoolId, (UINT1 *) pSynceEntry);

    /* reset the trace variable array */
    gau4SynceTraceOptions[u4ContextId] = 0;

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceUtilGetSynceEntryFromIfIdx
* Input       : u4IfIndex - interface index
*               ppSynceEntry - pointer to the context entry
* Description : This function returns pointer to the SyncE context entry for
*               given interface index.
*
* Output      : ppSynceEntry
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
SynceUtilGetSynceEntryFromIfIdx (UINT4 u4IfIndex,
                                 tSynceFsSynceEntry ** ppSynceEntry)
{
    UINT4               u4ContextId;

    if (ppSynceEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    *ppSynceEntry = NULL;

    if (SynceUtilGetContextIdFromIfIndex (u4IfIndex, &u4ContextId) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* get the ql value of the system for the context */
    *ppSynceEntry = SynceGetFsSynceEntry (u4ContextId);
    if (*ppSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Context couldn't be retrieved for the interface #%d\n",
                    u4IfIndex));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceUtilSelectContext
* Input       : u4ContextId - Context Id
* Description : This function switches to the given context
*
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
SynceUtilSelectContext (UINT4 u4ContextId)
{
    SYNCE_CURR_CONTEXT_INFO () = SynceGetFsSynceEntry (u4ContextId);
    if (SYNCE_CURR_CONTEXT_INFO () == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Context couldn't be retrieved for the ID #%d\n",
                    u4ContextId));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceUtilReleseContext
* Input       : None
* Description : This function releases the current context
*
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
SynceUtilReleseContext ()
{
    SYNCE_CURR_CONTEXT_INFO () = NULL;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceUtilGetContextIdFromIfIndex
* Input       : u4IfIndex - interface index
* Description : This function returns context id in which the given interface
*               belongs.
* Output      : pu4ContextId
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
SynceUtilGetContextIdFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    UINT2               u2LocalPortId;

    if (pu4ContextId == NULL)
    {
        return OSIX_FAILURE;
    }

    *pu4ContextId = 0;

    /* get the context to which this interface belongs */
    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                      pu4ContextId,
                                      &u2LocalPortId) == VCM_FAILURE)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Context couldn't be retrieved for the interface #%d\n",
                    u4IfIndex));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceUtilGetQlCodeFromSsmCode
* Input       : u4IfIndex - interface index
*               u1SsmCode - ssm code value
* Description : This function returns the QL code corresponding to the SSM
*               code.
* Output      : pu1QlCode - QL code corresponding to the ssm code
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
UINT4
SynceUtilGetQlCodeFromSsmCode (UINT4 u4ContextId, UINT1 u1SsmCode,
                               UINT1 *pu1QlCode)
{
    tSynceFsSynceEntry *pSynceEntry = NULL;

    if (pu1QlCode == NULL)
    {
        return OSIX_FAILURE;
    }

    if (u1SsmCode > SYNCE_SSM_MAX_CODE)
    {
        return OSIX_FAILURE;
    }

    pSynceEntry = SynceGetFsSynceEntry (u4ContextId);
    if (pSynceEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    *pu1QlCode = ((gaSynceSsmCodeTable[u1SsmCode][(pSynceEntry->MibObject.
                                                   i4FsSynceSSMOptionMode) -
                                                  1]).u1QlCode);

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceUtilGetSsmCodeFromQlCode
* Input       : u4IfIndex - interface index
*               u1QlCode - ql code
*               pu1SsmCode - ssm code corresponding to the QL code.
* Description : This function returns the SSM code corresponding to the QL code.
* Output      : pu1SsmCode - SSM code
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
UINT4
SynceUtilGetSsmCodeFromQlCode (UINT4 u4ContextId, UINT1 u1QlCode,
                               UINT1 *pu1SsmCode)
{
    UINT1               u1SsmCode = 0;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    if (pu1SsmCode == NULL)
    {
        return OSIX_FAILURE;
    }

    if (u1QlCode >= SYNCE_QL_MAX)
    {
        return OSIX_FAILURE;
    }

    pSynceEntry = SynceGetFsSynceEntry (u4ContextId);
    if (pSynceEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    for (u1SsmCode = 0; u1SsmCode < SYNCE_SSM_MAX_CODE; u1SsmCode++)
    {
        if ((gaSynceSsmCodeTable[u1SsmCode][(pSynceEntry->MibObject.
                                             i4FsSynceSSMOptionMode) -
                                            1]).u1QlCode == u1QlCode)
        {
            if ((gaSynceSsmCodeTable[u1SsmCode][(pSynceEntry->MibObject.
                                                 i4FsSynceSSMOptionMode) -
                                                1]).b1IsValid == OSIX_TRUE)
            {
                *pu1SsmCode = u1SsmCode;
            }
            else
            {
                *pu1SsmCode = 0xf;
            }

            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : IsSynceEnabled
* Input       : none
* Description : This function is used to check synce module status.
* Output      : None
* Returns     : OSIX_TRUE or OSIX_FALSE
*****************************************************************************/
INT4
SynceUtilIsSynceEnabled ()
{
    INT4                i4SynceModStatus = OSIX_FALSE;
    nmhGetFsSynceGlobalSysCtrl (&i4SynceModStatus);
    switch (i4SynceModStatus)
    {
        case SYNCE_MODULE_START:
            return OSIX_TRUE;
        case SYNCE_MODULE_SHUTDOWN:
        default:
            return OSIX_FALSE;
    }

    /* not reached */
    return OSIX_TRUE;
}

/****************************************************************************
* Function    : SynceUtilModuleStart
* Input       : none
* Description : This function is used to start SyncE module.
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
SynceUtilModuleStart ()
{
    if (SynceMainInit () != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceUtilModuleShutdown
* Input       : none
* Description : This function is used to shutdown SyncE module.
* Output      : None
* Returns     : None
*****************************************************************************/
VOID
SynceUtilModuleShutdown ()
{
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;

    /* destroy all the synce interfaces as module is being shutdown */
    for (pSynceIfEntry = SynceGetFirstFsSynceIfTable ();
         pSynceIfEntry != NULL;
         pSynceIfEntry = SynceGetNextFsSynceIfTable (pSynceIfEntry))
    {
        SYNCE_SELECT_CONTEXT (SYNCE_DEFAULT_CXT_ID);

        SynceProtIfSynceModeSet ((UINT4) pSynceIfEntry->MibObject.
                                 i4FsSynceIfIndex, OSIX_FALSE);

        SYNCE_RELEASE_CONTEXT ();
    }

    /* destory all the contexts */
    SynceUtilDeleteContext (SYNCE_DEFAULT_CXT_ID);

    SynceMainDeInit ();
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  synceutil.c                     */
/*-----------------------------------------------------------------------*/
