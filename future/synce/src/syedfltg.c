/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syedfltg.c,v 1.3 2016/07/06 11:05:10 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Synce
*********************************************************************/

#include "syeinc.h"

/****************************************************************************
* Function    : SynceInitializeMibFsSynceTable
* Input       : pSynceFsSynceEntry
* Description : It Intializes the given structure
*               of synce table with default
*               values.
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4 SynceInitializeMibFsSynceTable (tSynceFsSynceEntry * pSynceFsSynceEntry)
{
    pSynceFsSynceEntry->MibObject.i4FsSynceQLMode = SYNCE_QL_MODE_ENABLE;
    pSynceFsSynceEntry->smState = QL_ENABLED_NORMAL_STATE;
    pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption = SYNCE_CRITICAL_TRC;
    pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode = SYNCE_SSM_OPTION1;
    pSynceFsSynceEntry->MibObject.i4FsSynceSelectedInterface = 0;
    pSynceFsSynceEntry->MibObject.u4FsSynceQLValue = SYNCE_QL_DNU;
    pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus = ACTIVE;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceInitializeMibFsSynceIfTable
* Input       : pSynceFsSynceIfEntry
* Description : It Intializes the given structure
*               of synce interface table with
*               default values.
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
SynceInitializeMibFsSynceIfTable (tSynceFsSynceIfEntry * pSynceFsSynceIfEntry)
{
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode = OSIX_FALSE;
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode = SYNCE_ESMC_MODE_RX;
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority = 0;
    pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue = SYNCE_QL_DNU;
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced = OSIX_FALSE;
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSignalFail = SYNCE_SIGNAL_FAIL;
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE;

    return OSIX_SUCCESS;
}
