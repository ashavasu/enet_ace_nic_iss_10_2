#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "syeinc.h"

/****************************************************************************
*                                                                           *
* Function     : SynceTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
SynceTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        u4sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    OsixGetSysTime (&u4sysTime);
    UtlTrcLog(0x1, 0x1, "SYNCE", "%d:%s:%d: %s", u4sysTime, pc1Fname, 
            u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : SynceTrcWrite                                              *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
SynceTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : SynceTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
SynceTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define SYNCE_TRC_BUF_SIZE    2000
    static CHR1        aBuf[SYNCE_TRC_BUF_SIZE];

    if (!(u4Flags & SYNCE_CTX_TRC_FLAG))
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&aBuf[0], fmt, ap);
    va_end (ap);

    return (&aBuf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  syncetrc.c                      */
/*-----------------------------------------------------------------------*/
