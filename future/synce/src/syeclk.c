/******************************************************************************
 * Copyright (C) 2013  Aricent Inc . All Rights Reserved
 *
 *  $Id: syeclk.c,v 1.2 2013/04/23 11:54:50 siva Exp $
 *
 *  Description : Executes the selection algorithm for selecting the best
 *                synchronization input.
*******************************************************************************/

#include "syeinc.h"

/******************************************************************************
 *  Function                 : SynceClkDoClockSelection
 *
 *  Description              : Function that implements the clock selection
 *                             algorithm as mentioned in ITU-T G.781.
 *                             Apart from selecting the interface, this function
 *                             also updates the system data structure with
 *                             the selected interface and QL.
 *
 *  Input                    : u4ContextId - context in which selection should
 *                                           run.
 *                             i4QlMode    - QL-Mode of the given context.
 *
 *  Output                   : None
 *  Global Variables Referred: None
 *  Global variables Modified: None
 *  Use of Recursion         : None
 *  Returns                  : Selected Interface Index
 *                           : 0 - if failed to select any interface
*******************************************************************************/

UINT4
SynceClkDoClockSelection (UINT4 u4ContextId, INT4 i4QlMode)
{
    tSynceFsSynceIfEntry *pCurIfEntry = NULL;
    tSynceFsSynceIfEntry *pCurBestIfEntry = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT4               u4SelectedInterface = 0;
    UINT1               u1QlValue = 0;
    UINT1               u1CurBestQlValue = 0;
    UINT4               u4IfContextId = 0;
#ifdef NPAPI_WANTED
    tSynceHwInfo        synceHwInfo;
#endif

    SYNCE_FN_ENTRY ();

    pSynceEntry = SynceGetFsSynceEntry (u4ContextId);
    if (pSynceEntry == NULL)
    {
        /* oops!  fatal error */
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_CRITICAL_TRC, "Could not find system context.\n"));
        return u4SelectedInterface;
    }

    for (pCurIfEntry = SynceGetFirstFsSynceIfTable ();
         pCurIfEntry != NULL;
         pCurIfEntry = SynceGetNextFsSynceIfTable (pCurIfEntry))
    {
        if (pCurIfEntry->MibObject.i4FsSynceIfEsmcMode == SYNCE_ESMC_MODE_TX)
        {
            continue;
        }
        if (SynceUtilGetContextIdFromIfIndex ((UINT4) pCurIfEntry->MibObject.
                                              i4FsSynceIfIndex,
                                              &u4IfContextId) == OSIX_FAILURE)
        {
            continue;
        }
        if (u4IfContextId != u4ContextId)
        {
            continue;
        }
        if (pCurIfEntry->MibObject.i4FsSynceIfSynceMode == OSIX_FALSE)
        {
            continue;
        }
        if (pCurIfEntry->MibObject.i4FsSynceIfPriority == 0)
        {
            continue;
        }
        if (pCurIfEntry->MibObject.i4FsSynceIfLockoutStatus == OSIX_TRUE)
        {
            continue;
        }

        switch (i4QlMode)
        {
            case SYNCE_QL_MODE_ENABLE:
                u1QlValue = (UINT1) pCurIfEntry->MibObject.u4FsSynceIfQLValue;
                /* just like priority higher the value of ql code, lower
                 * its magnitude.
                 * if QL[a] = 3
                 * and QL[b] = 4
                 * it mean QL[a] > QL[b]
                 */
                if (pCurIfEntry->MibObject.i4FsSynceIfIsRxQLForced == OSIX_TRUE)
                {
                    /* if user has manually assigned the QL value on interface
                     * then we need to check the signal fail condition;
                     * otherwise, this interface might be considered for
                     * selection process which might not be desirable.
                     */
                    if (pCurIfEntry->MibObject.i4FsSynceIfSignalFail ==
                        OSIX_TRUE)
                    {
                        continue;
                    }
                }
                else
                {
                    switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
                    {
                        case SYNCE_SSM_OPTION1:
                            if (u1QlValue >= SYNCE_QL_DNU)
                            {
                                continue;
                            }
                            break;
                        default:
                            if (u1QlValue >= SYNCE_QL_DUS)
                            {
                                continue;
                            }
                            break;
                    }
                }

                if (pCurBestIfEntry != NULL)
                {
                    u1CurBestQlValue =
                        (UINT1) pCurBestIfEntry->MibObject.u4FsSynceIfQLValue;
                    if (u1CurBestQlValue < u1QlValue)
                    {
                        /* already a better ql value is selected */
                        continue;
                    }
                    else
                    {
                        if (u1CurBestQlValue == u1QlValue)
                        {
                            /* check priority */
                            if (pCurBestIfEntry->MibObject.i4FsSynceIfPriority
                                <= pCurIfEntry->MibObject.i4FsSynceIfPriority)
                            {
                                /* selected source has higher priority */
                                continue;
                            }
                        }
                    }
                }
                break;
            case SYNCE_QL_MODE_DISABLE:
                if (pCurIfEntry->MibObject.i4FsSynceIfSignalFail == OSIX_TRUE)
                {
                    continue;
                }

                if (pCurBestIfEntry != NULL)
                {
                    if (pCurBestIfEntry->MibObject.i4FsSynceIfPriority
                        <= pCurIfEntry->MibObject.i4FsSynceIfPriority)
                    {
                        continue;
                    }
                }
                break;
            default:
                break;
        }

        pCurBestIfEntry = pCurIfEntry;
    }

    u1CurBestQlValue = 0;
    if (pCurBestIfEntry != NULL)
    {
        u4SelectedInterface =
            (UINT4) pCurBestIfEntry->MibObject.i4FsSynceIfIndex;
        u1CurBestQlValue =
            (UINT1) pCurBestIfEntry->MibObject.u4FsSynceIfQLValue;
    }

    /* update system values */
    if ((UINT4) (pSynceEntry->MibObject.i4FsSynceSelectedInterface) !=
        u4SelectedInterface)
    {
        SYNCE_LOG ((SYSLOG_ALERT_LEVEL, gSynceGlobals.u4SysLogId,
                    "SyncE selected Interface #%d", u4SelectedInterface));
        pSynceEntry->MibObject.i4FsSynceSelectedInterface =
            (INT4) u4SelectedInterface;

        /* call NPAPI to configure the HW */
#ifdef NPAPI_WANTED
        MEMSET (&synceHwInfo, 0, sizeof (tSynceHwInfo));
        synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4ContextId = u4ContextId;
        synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4IfIndex =
            u4SelectedInterface;
        synceHwInfo.u4InfoType = SYNCE_HW_SET_HW_CLK;
        if (SynceFsNpHwConfigSynceInfo (&synceHwInfo) != FNP_SUCCESS)
        {
            SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "%% HW Call Failed\n"));
        }
#endif
    }

    if ((u4SelectedInterface == 0) || (i4QlMode == SYNCE_QL_MODE_DISABLE))
    {
        /* QL_Undef */
        switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
        {
            case SYNCE_SSM_OPTION1:
                u1CurBestQlValue = SYNCE_QL_DNU;
                break;
            default:
                u1CurBestQlValue = SYNCE_QL_DUS;
                break;
        }
    }

    if (u1CurBestQlValue != pSynceEntry->MibObject.u4FsSynceQLValue)
    {
        /* search all the entries on which esmc mode is tx */
        for (pCurIfEntry = SynceGetFirstFsSynceIfTable ();
             pCurIfEntry != NULL;
             pCurIfEntry = SynceGetNextFsSynceIfTable (pCurIfEntry))
        {
            if (SynceUtilGetContextIdFromIfIndex ((UINT4) pCurIfEntry->
                                                  MibObject.i4FsSynceIfIndex,
                                                  &u4IfContextId) ==
                OSIX_FAILURE)
            {
                continue;
            }
            if (u4IfContextId != u4ContextId)
            {
                continue;
            }
            if (pCurIfEntry->MibObject.i4FsSynceIfEsmcMode ==
                SYNCE_ESMC_MODE_TX)
            {
                /*set the event flag */
                pCurIfEntry->b1TxEventPdu = OSIX_TRUE;
            }
        }

        SYNCE_LOG ((SYSLOG_ALERT_LEVEL, gSynceGlobals.u4SysLogId,
                    "System selected QL changed. "
                    " Old QL:%d, New QL:%d",
                    pSynceEntry->MibObject.u4FsSynceQLValue, u1CurBestQlValue));

        pSynceEntry->MibObject.u4FsSynceQLValue = u1CurBestQlValue;
    }

    SYNCE_FN_EXIT ();

    return u4SelectedInterface;
}
