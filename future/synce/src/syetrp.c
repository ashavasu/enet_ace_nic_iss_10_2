/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syetrp.c,v 1.1 2013/03/23 12:41:37 siva Exp $
 *
 * Description : This file contains the function for trap handler sub module.
 *****************************************************************************/

#include "syeinc.h"
#include "snmputil.h"

/**************************************************************************
 *  Function                  : SynceTrapRaiseTrap
 *
 *  Description               : This Function raises a trap when there
 *                              is a change in interface QL value or
 *                              signal fail status.
 *
 *  Input                     : pSynceIfEntry - structure containing
 *                              interface related information.
 *                              u1TrapType  - Type of trap
 *
 *  Output                    : None
 *  Global Variables Referred : None
 *  Global variables Modified : None
 *  Use of Recursion          : None
 *  Returns                   : None
 * ***********************************************************************/

VOID
SynceTrapRaiseTrap(tSynceFsSynceIfEntry *pSynceIfEntry, UINT1 u1TrapType)
{
    SYNCE_FN_ENTRY ();

    UNUSED_PARAM(pSynceIfEntry);
    UNUSED_PARAM(u1TrapType);

    SYNCE_FN_EXIT ();
    return;
}
