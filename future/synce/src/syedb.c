/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syedb.c,v 1.6 2016/07/06 11:05:10 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access
* for the module Synce
*********************************************************************/

#include "syeinc.h"
#include "vcm.h"
#include "cfa.h"
#include "syncecli.h"

/****************************************************************************
 Function    :  SynceTestAllFsSynceTable
 Input       :  pu4ErrorCode
                pSynceSetFsSynceEntry
                pSynceIsSetFsSynceEntry
 Description :  This Routine will test the values
                to be set in synce table.Prior to
                that it checks if context id on
                which this value is to be set exists
                or not.
 Input       :  pSynceSetFsSynceEntry - structure containing context
                related information.
                pSynceIsSetFsSynceEntry - context field to be set
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
SynceTestAllFsSynceTable (UINT4 *pu4ErrorCode,
                          tSynceFsSynceEntry * pSynceSetFsSynceEntry,
                          tSynceIsSetFsSynceEntry * pSynceIsSetFsSynceEntry,
                          INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tSynceFsSynceEntry *pSynceGetEntry = NULL;
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4IfContextId = 0;

    if (OSIX_FALSE == SynceUtilIsSynceEnabled ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYNCE_ERR_MODULE_DISABLED);
        return OSIX_FAILURE;
    }

    u4ContextId = (UINT4) pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId;
    if (u4ContextId != 0)
    {
        if (VcmIsVcExist (u4ContextId) == VCM_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_SYNCE_ERR_INVALID_CONTEXT);
            return OSIX_FAILURE;
        }
    }

    pSynceGetEntry = SynceGetFsSynceTable (pSynceSetFsSynceEntry);
    if (pSynceGetEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYNCE_ERR_INVALID_CONTEXT);
        return OSIX_FAILURE;
    }

    if (pSynceIsSetFsSynceEntry->bFsSynceTraceOption != OSIX_FALSE)
    {
        if (pSynceSetFsSynceEntry->MibObject.
            u4FsSynceTraceOption > (SYNCE_ALL_TRC))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceQLMode != OSIX_FALSE)
    {
        if ((pSynceSetFsSynceEntry->MibObject.
             i4FsSynceQLMode != SYNCE_QL_MODE_ENABLE) &&
            (pSynceSetFsSynceEntry->MibObject.
             i4FsSynceQLMode != SYNCE_QL_MODE_DISABLE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode != OSIX_FALSE)
    {
        if ((pSynceSetFsSynceEntry->MibObject.
             i4FsSynceSSMOptionMode < SYNCE_SSM_OPTION1) ||
            (pSynceSetFsSynceEntry->MibObject.
             i4FsSynceSSMOptionMode > SYNCE_SSM_OPTION2GEN2))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* SSM Option could only be changed if SyncE mode is not enabled
         * on any of the port.
         */
        if (pSynceGetEntry->MibObject.i4FsSynceSSMOptionMode
            != pSynceSetFsSynceEntry->MibObject.i4FsSynceSSMOptionMode)
        {
            pSynceIfEntry = SynceGetFirstFsSynceIfTable ();
            while (pSynceIfEntry != NULL)
            {
                if (SynceUtilGetContextIdFromIfIndex ((UINT4) pSynceIfEntry->
                                                      MibObject.
                                                      i4FsSynceIfIndex,
                                                      &u4IfContextId) ==
                    OSIX_FAILURE)
                {
                    return OSIX_FAILURE;
                }

                if ((u4IfContextId == u4ContextId)
                    && (pSynceIfEntry->MibObject.i4FsSynceIfSynceMode))
                {
                    /* synce is enabled on this interface */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_SYNCE_ERR_SSM_MODE_NOT_CHANGED);
                    return OSIX_FAILURE;
                }

                pSynceIfEntry = SynceGetNextFsSynceIfTable (pSynceIfEntry);
            }
        }
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus != OSIX_FALSE)
    {
        /* changing of row status is not allowed for SyncE table */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceTestAllFsSynceIfTable
 Input       :  pu4ErrorCode
                pSynceSetFsSynceIfEntry
                pSynceIsSetFsSynceIfEntry
 Description :  This Routine will test the values
                to be set in synce interface table.
                Prior to that it checks if interface
                id on which this value is to be set
                is valid or not.
 Input       :  pSynceSetFsSynceIfEntry - structure containing interface
                related information.
                pSynceIsSetFsSynceIfEntry - interface field to be set
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
SynceTestAllFsSynceIfTable (UINT4 *pu4ErrorCode,
                            tSynceFsSynceIfEntry * pSynceSetFsSynceIfEntry,
                            tSynceIsSetFsSynceIfEntry *
                            pSynceIsSetFsSynceIfEntry, INT4 i4RowStatusLogic,
                            INT4 i4RowCreateOption)
{
    tSynceFsSynceIfEntry *pSynceGetIfEntry = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4QlCode = 0;
    INT4                i4RetValue = OSIX_SUCCESS;
    BOOL1               b1IsValidCase = OSIX_FALSE;

    if (OSIX_FALSE == SynceUtilIsSynceEnabled ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYNCE_ERR_MODULE_DISABLED);
        return OSIX_FAILURE;
    }

    u4IfIndex = (UINT4) pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex;
    if ((CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE) ||
        (CfaIsPhysicalInterface (u4IfIndex) == CFA_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYNCE_ERR_INVALID_INTERFACE);
        return OSIX_FAILURE;
    }

    pSynceGetIfEntry = SynceGetFsSynceIfTable (pSynceSetFsSynceIfEntry);
    if (pSynceGetIfEntry == NULL)
    {
        if (pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus != OSIX_FALSE)
        {
            if ((pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                 CREATE_AND_GO) ||
                (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                 CREATE_AND_WAIT))
            {
                b1IsValidCase = OSIX_TRUE;
            }
        }

        if (pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode != OSIX_FALSE)
        {
            if (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode ==
                OSIX_TRUE)
            {
                b1IsValidCase = OSIX_TRUE;
            }
        }

        if (b1IsValidCase == OSIX_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_SYNCE_ERR_SYNCE_MODE_DISABLED);
            return OSIX_FAILURE;
        }
    }

    if (SynceUtilGetSynceEntryFromIfIdx (u4IfIndex, &pSynceEntry)
        != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYNCE_ERR_INVALID_CONTEXT);
        return OSIX_FAILURE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode != OSIX_FALSE)
    {
	    if (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode ==
			    OSIX_FALSE)
	    {
		    if ((pSynceGetIfEntry->MibObject.i4FsSynceIfConfigSwitch 
			 == SYNCE_MANUAL_SWITCH_MODE) || 
			 (pSynceGetIfEntry->MibObject.
			 i4FsSynceIfConfigSwitch == SYNCE_FORCED_SWITCH_MODE))
		    {
			    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    CLI_SET_ERR (CLI_SYNCE_ERR_IF_DIS_MODE);
			    return OSIX_FAILURE;
		    }
	    }
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode != OSIX_FALSE)
    {
        switch (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode)
        {
            case SYNCE_ESMC_MODE_RX:
                /* RX mode is not allowed in QL-Disabled Mode */
                if (pSynceEntry->MibObject.i4FsSynceQLMode ==
                    SYNCE_QL_MODE_DISABLE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_SYNCE_ERR_ESMC_RX_NOT_SUPPORTED);
                    return OSIX_FAILURE;
                }
                break;
            case SYNCE_ESMC_MODE_NONE:
            case SYNCE_ESMC_MODE_TX:
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
        }
    }

    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority != OSIX_FALSE)
    {
        if ((pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfPriority < 0) ||
            (pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfPriority > SYNCE_IF_MAX_PRIORITY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            SYNCE_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue != OSIX_FALSE)
    {
        u4QlCode = pSynceSetFsSynceIfEntry->MibObject.u4FsSynceIfQLValue;
        /* ql code '0' is used to reset the user set ql value */
        if (u4QlCode != 0)
        {
            switch (pSynceEntry->MibObject.i4FsSynceSSMOptionMode)
            {
                case SYNCE_SSM_OPTION1:
                    if ((u4QlCode < SYNCE_QL_PRC) || (u4QlCode > SYNCE_QL_DNU))
                    {
                        i4RetValue = OSIX_FAILURE;
                    }
                    break;
                case SYNCE_SSM_OPTION2GEN1:
                    if ((u4QlCode == SYNCE_QL_ST3E)
                        || (u4QlCode == SYNCE_QL_TNC)
                        || (u4QlCode < SYNCE_QL_PRS)
                        || (u4QlCode > SYNCE_QL_DUS)
                        || (u4QlCode == SYNCE_QL_PROV))
                    {
                        i4RetValue = OSIX_FAILURE;
                    }
                    break;

                case SYNCE_SSM_OPTION2GEN2:
                    if ((u4QlCode < SYNCE_QL_PRS) || (u4QlCode > SYNCE_QL_DUS)
                        || (u4QlCode == SYNCE_QL_RES))
                    {
                        i4RetValue = OSIX_FAILURE;
                    }
                    break;
                default:
                    i4RetValue = OSIX_FAILURE;
                    break;
            }
            if (i4RetValue == OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_SYNCE_ERR_INVALID_QL_CODE);
                return OSIX_FAILURE;
            }
        }
    }

    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus != OSIX_FALSE)
    {
        if ((pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfLockoutStatus != OSIX_TRUE) &&
            (pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfLockoutStatus != OSIX_FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch != OSIX_FALSE)
    {
        if ((pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfConfigSwitch != SYNCE_MANUAL_SWITCH_MODE) &&
            (pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfConfigSwitch != SYNCE_FORCED_SWITCH_MODE) &&
	    (pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfConfigSwitch != SYNCE_NO_SWITCH_MODE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	    CLI_SET_ERR (CLI_SYNCE_INVALID_SW_INPUT);
            return OSIX_FAILURE;
        }
	if ((pSynceGetIfEntry->MibObject.i4FsSynceIfConfigSwitch 
				!= SYNCE_MANUAL_SWITCH_MODE)
           && (pSynceGetIfEntry->MibObject.
             i4FsSynceIfConfigSwitch != SYNCE_FORCED_SWITCH_MODE)
	   && (pSynceSetFsSynceIfEntry->MibObject.
             i4FsSynceIfConfigSwitch == SYNCE_NO_SWITCH_MODE))
	{

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	    CLI_SET_ERR (CLI_SYNCE_ERR_INVALID_SWITCH_CLEAR);
            return OSIX_FAILURE;
	}
	if((pSynceSetFsSynceIfEntry->MibObject.
	   i4FsSynceIfConfigSwitch == SYNCE_FORCED_SWITCH_MODE) 
          || (pSynceSetFsSynceIfEntry->MibObject.
	   i4FsSynceIfConfigSwitch == SYNCE_MANUAL_SWITCH_MODE)) 
	{
		if (pSynceGetIfEntry->MibObject.i4FsSynceIfEsmcMode 
				== SYNCE_ESMC_MODE_TX)
		{
			CLI_SET_ERR (CLI_SYNCE_ERR_INT_TX_MODE);
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return OSIX_FAILURE;
		}
		if (pSynceGetIfEntry->MibObject.i4FsSynceIfLockoutStatus 
				== CLI_SYNCE_LOCKOUT_ENABLE)
		{
			CLI_SET_ERR (CLI_SYNCE_ERR_SW_IF_LOCKOUT);
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return OSIX_FAILURE;
		}
		if (pSynceGetIfEntry->MibObject.i4FsSynceIfSynceMode 
				== CLI_SYNCE_MODE_DISABLE)
		{
			CLI_SET_ERR (CLI_SYNCE_ERR_SYNCE_MODE_DISABLED);
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return OSIX_FAILURE;
		}
		if (pSynceGetIfEntry->MibObject.i4FsSynceIfPriority 
				== OSIX_FALSE)
		{
			CLI_SET_ERR (CLI_SYNCE_ERR_MS_PRIORITY_ZERO);
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return OSIX_FAILURE;
		}
	}
	if(pSynceSetFsSynceIfEntry->MibObject.
			i4FsSynceIfConfigSwitch == SYNCE_MANUAL_SWITCH_MODE)
	{
		if(SynceIsSetForceSwitchMode((UINT4)pSynceEntry->MibObject.i4FsSynceContextId)
			  	        == OSIX_SUCCESS)
		{
			CLI_SET_ERR (CLI_SYNCE_ERR_FORCE_SWITCH_SET);
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return OSIX_FAILURE;
		}
		if (pSynceGetIfEntry->MibObject.i4FsSynceIfSignalFail 
				== OSIX_TRUE)
		{
			CLI_SET_ERR (CLI_SYNCE_ERR_SW_IF_SIGNAL_FAIL);
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return OSIX_FAILURE;
		}
	}
    }
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceTestFsSynceGlobalSysCtrl
 Input       :  pu4ErrorCode
                i4FsSynceGlobalSysCtrl
 Description :  This Routine will Test
                the Value accordingly.
 Input       :  i4FsSynceGlobalSysCtrl
 Output      :  pu4ErrorCode - Error Code
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceTestFsSynceGlobalSysCtrl (UINT4 *pu4ErrorCode, INT4 i4FsSynceGlobalSysCtrl)
{
    switch (i4FsSynceGlobalSysCtrl)
    {
        case SYNCE_MODULE_START:
        case SYNCE_MODULE_SHUTDOWN:
            return OSIX_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
    }

    /* not reached */
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceTestFsSynceIfCntrs
 Description :  This Routine will Test
                the Value accordingly.
 Input       :  u4IfIndex   - Interface Index 
                u4PktsCount - Packets Count
 Output      :  pu4ErrorCode - Error Code
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceTestFsSynceIfCntrs (UINT4 *pu4ErrorCode,
                         UINT4 u4IfIndex, UINT4 u4PktsCount)
{
    if (OSIX_FALSE == SynceUtilIsSynceEnabled ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYNCE_ERR_MODULE_DISABLED);
        return OSIX_FAILURE;
    }

    if ((CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE) ||
        (CfaIsPhysicalInterface (u4IfIndex) == CFA_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_SYNCE_ERR_INVALID_INTERFACE);
        return OSIX_FAILURE;
    }

    if (u4PktsCount != 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : SynceIsSetForceSwitchMode                     *
 *                                                                           *
 * Description               : This routine is invoked to verify whether     *
 *                             forced  switch mode is set in any of the      *
 *                             interfaces                                    *
 *                                                                           *
 * Input                     : u4ContextId, i4SwitchMode                     *
 * Output                    : None                                          *
 * Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     *
 *                                                                           *
 *****************************************************************************/

INT4
SynceIsSetForceSwitchMode (UINT4 u4ContextId)
{

    tSynceFsSynceIfEntry *pCurIfEntry = NULL;
    UINT4  u4IfContextId = 0;

    SYNCE_FN_ENTRY ();
    for (pCurIfEntry = SynceGetFirstFsSynceIfTable ();
		    pCurIfEntry != NULL;
		    pCurIfEntry = SynceGetNextFsSynceIfTable (pCurIfEntry))
    {
	    if (SynceUtilGetContextIdFromIfIndex ((UINT4) pCurIfEntry->
				    MibObject.i4FsSynceIfIndex,
				    &u4IfContextId) ==
			    OSIX_FAILURE)
	    {
		    continue;
	    }
	    if (u4IfContextId != u4ContextId)
	    {
		    continue;
	    }
	    if (pCurIfEntry->MibObject.i4FsSynceIfConfigSwitch ==
			    SYNCE_FORCED_SWITCH_MODE)
	    {
		    SYNCE_FN_EXIT ();
		    return OSIX_SUCCESS;
	    }
    }
    SYNCE_FN_EXIT ();
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function                  : SynceResetSwitchMode                          *
 *                                                                           *
 * Description               : This routine is invoked to reset              *
 *                             switch mode to none                           *
 *                                                                           *
 * Input                     : u4ContextId                                   *
 * Output                    : None                                          *
 * Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     *
 *                                                                           *
 *****************************************************************************/

void
SynceResetSwitchMode (UINT4 u4ContextId,UINT4 u4SelectedInterface)
{

    tSynceFsSynceIfEntry *pCurIfEntry = NULL;
    UINT4  u4IfContextId = 0;

    SYNCE_FN_ENTRY ();
    for (pCurIfEntry = SynceGetFirstFsSynceIfTable ();
		    pCurIfEntry != NULL;
		    pCurIfEntry = SynceGetNextFsSynceIfTable (pCurIfEntry))
    {
	    if (SynceUtilGetContextIdFromIfIndex ((UINT4) pCurIfEntry->
				    MibObject.i4FsSynceIfIndex,
				    &u4IfContextId) ==
			    OSIX_FAILURE)
	    {
		    continue;
	    }
	    if (u4IfContextId != u4ContextId)
	    {
		    continue;
	    }
            if(u4SelectedInterface != (UINT4)pCurIfEntry->
			    MibObject.i4FsSynceIfIndex)		    
	    {
		    pCurIfEntry->MibObject.i4FsSynceIfConfigSwitch =
			                  SYNCE_NO_SWITCH_MODE;
	    }
    }
    SYNCE_FN_EXIT ();
}
