/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syecfa.c,v 1.1 2013/03/23 12:41:37 siva Exp $
*
* Description: This file contains the SyncE routines which interact
* with CFA.
*
*******************************************************************/
#include "syeinc.h"

/******************************************************************************/
/* Function     : SynceCfaRegLL                                               */
/*                                                                            */
/* Description  : Synce module during initialization will invoke this API aftr*/
/*                loading the structure CfaRegParams with necessary callback  */
/*                functions. On receiving a Synce packet, interface status    */
/*                changes, Synce module should invoke the mentioned call back */
/*                functions appropriately.                                    */
/*                                                                            */
/* Input        : None.                                                       */
/*                                                                            */
/* Output       : None                                                        */
/*                                                                            */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                   */
/*                                                                            */
/******************************************************************************/

INT4
SynceCfaRegLL (VOID)
{
        tCfaRegParams       CfaRegParams;
        INT4                i4RetVal = OSIX_SUCCESS;

        SYNCE_FN_ENTRY ();

        MEMSET (&CfaRegParams, 0, sizeof (tCfaRegParams));

        CfaRegParams.u2LenOrType = SYNCE_ESMC_ETHER_TYPE;
        CfaRegParams.u2RegMask = CFA_IF_OPER_ST_CHG;
        CfaRegParams.pIfOperStChg = SynceApiIfOperStatusChangeCb;

        if (CfaRegisterHL (&CfaRegParams) != CFA_SUCCESS)
        {

            SYNCE_TRC((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                        SYNCE_CRITICAL_TRC,
                         "CfaRegisterHL failed\r\n"));
            SYNCE_FN_EXIT ();
            i4RetVal = OSIX_FAILURE;
        }

        SYNCE_FN_EXIT ();
        return i4RetVal;

}

/*****************************************************************************/
/* Function     : SynceCfaDeRegisterLL                                       */
/*                                                                           */
/* Description  : This API will be invoked by Synce whenever the module is   */
/*                de-initialized. This API should relinquish all the         */
/*                registrations earlier done by Synce module during          */
/*                initialization.                                            */
/*                                                                           */
/* Input        : u2LenorType - Ether Type                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
SynceCfaDeRegisterLL (UINT2 u2LenOrType)
{
    INT4            i4RetVal = OSIX_SUCCESS;

    SYNCE_FN_ENTRY ();

    if (CfaDeregisterHL (u2LenOrType) != CFA_SUCCESS)
    {

        SYNCE_TRC((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_CRITICAL_TRC,
                                   "CfaDeregisterHL failed\r\n"));
        SYNCE_FN_EXIT ();
        i4RetVal = OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return i4RetVal;
}
