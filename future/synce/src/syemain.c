/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syemain.c,v 1.1 2013/03/23 12:41:37 siva Exp $
*
* Description: This file contains the synce task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __SYNCEMAIN_C__
#include "syeinc.h"

/* Prototypes of the functions private to this file only */
PRIVATE UINT4 SynceMainMemInit PROTO ((VOID));
PRIVATE VOID SynceMainMemClear PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : SynceMainTask                                              *
*                                                                           *
* Description  : Main function of SYNCE.                                    *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
SynceMainTask (INT1 *pArgs)
{
    UINT4               u4Event = 0;

    UNUSED_PARAM(pArgs);

    SYNCE_FN_ENTRY ();

    if (SynceMainInit() == OSIX_FAILURE)
    {
        SynceMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
    }
    /* Register the MIBs with SNMP Agent */
    RegisterFSSYNC ();

    /* Indicate the status of initialization to the main routine */
    lrInitComplete (OSIX_SUCCESS);

    /*forever event loop*/
    while (1)
    {
        if(OsixReceiveEvent (SYNCE_TIMER_EVENT |
                               SYNCE_QUEUE_EVENT,
                               OSIX_WAIT,
                               (UINT4) 0,
                               (UINT4 *) &(u4Event)) == OSIX_SUCCESS)
        {
            SynceApiLock ();

            SYNCE_TRC((SYNCE_CONTROL_PLANE_TRC, 
                        "Received Event %d\n", u4Event));

            if ((u4Event & SYNCE_TIMER_EVENT) == SYNCE_TIMER_EVENT)
            {
                SYNCE_TRC((SYNCE_CONTROL_PLANE_TRC, "SYNCE_TIMER_EVENT\n"));
                SynceTmrHandleExpiry ();
            }

            if ((u4Event & SYNCE_QUEUE_EVENT) == SYNCE_QUEUE_EVENT)
            {
                SYNCE_TRC((SYNCE_CONTROL_PLANE_TRC, "SYNCE_QUEUE_EVENT\n"));
                SynceQueProcessMsgs ();
            }

            SynceApiUnLock ();
        }
    }

    SYNCE_FN_EXIT ();
}

/****************************************************************************
*                                                                           *
* Function     : SynceMainInit                                          *
*                                                                           *
* Description  : SYNCE initialization routine.                              *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
SynceMainInit (VOID)
{
    INT4 i4SysLogId;

    SYNCE_FN_ENTRY ();

    MEMSET (&gSynceGlobals, 0, sizeof (gSynceGlobals));
    MEMCPY (gSynceGlobals.au1TaskSemName,
            SYNCE_MUT_EXCL_SEM_NAME, OSIX_NAME_LEN);

    /* global trace */
    gSynceGlobals.u4GblTrc = SYNCE_ALL_TRC;

#ifdef SYSLOG_WANTED
    /* register with syslog */
    i4SysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "SYNCE", SYSLOG_ALERT_LEVEL);
    if (i4SysLogId <= 0)
    {
        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_CRITICAL_TRC |
                SYNCE_ALL_FAILURE_TRC, "Registration with Syslog FAILED\n"));
        return OSIX_FAILURE;
    }

    gSynceGlobals.u4SysLogId = (UINT4)i4SysLogId;
#else
    UNUSED_PARAM(i4SysLogId);
#endif

    if (OsixCreateSem (SYNCE_MUT_EXCL_SEM_NAME, SYNCE_SEM_CREATE_INIT_CNT, 0,
                         &gSynceGlobals.taskSemId) == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC
                        | SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC,
                   "Semaphore creation failed for %s \n",
                   SYNCE_MUT_EXCL_SEM_NAME));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (TmrCreateTimerList ((CONST UINT1 *) SYNCE_TASK_NAME,
                            SYNCE_TIMER_EVENT,
                            NULL,
                            (tTimerListId *)(&(gSynceGlobals.tmrList)))
                                == TMR_FAILURE)
    {
        OsixSemDel (gSynceGlobals.taskSemId);
        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC |
                        SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC,
                        "Timer list creation failed for SyncE\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (SynceUtlCreateRBTree () == OSIX_FAILURE)
    {
        OsixSemDel (gSynceGlobals.taskSemId);
        TmrDeleteTimerList (gSynceGlobals.tmrList);
        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC |
                        SYNCE_CRITICAL_TRC | SYNCE_OS_RESOURCE_TRC,
                        "RB Tree Creation Failed\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Create buffer pools for data structures */
    if (SynceMainMemInit () == OSIX_FAILURE)
    {
        OsixSemDel (gSynceGlobals.taskSemId);
        TmrDeleteTimerList (gSynceGlobals.tmrList);
        SynceUtlDestroyRBTree();
        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC |
                        SYNCE_CRITICAL_TRC | SYNCE_OS_RESOURCE_TRC ,
                        "Memory Pool Creation Failed\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (OsixCreateQ (SYNCE_QUEUE_NAME, SYNCE_QUEUE_DEPTH, 0,
                     (tOsixQId *) & (gSynceGlobals.taskQueId)) == OSIX_FAILURE)
    {
        OsixSemDel (gSynceGlobals.taskSemId);
        TmrDeleteTimerList (gSynceGlobals.tmrList);
        SynceUtlDestroyRBTree();
        SynceMainMemClear();

        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_OS_RESOURCE_TRC |
                     SYNCE_ALL_FAILURE_TRC | SYNCE_CRITICAL_TRC,
                     "MsgQ Creation Failed\r\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* create synce table for default context */
    if (SynceUtilCreateContext (SYNCE_DEFAULT_CXT_ID) == OSIX_FAILURE)
    {
        OsixSemDel (gSynceGlobals.taskSemId);
        TmrDeleteTimerList (gSynceGlobals.tmrList);
        SynceUtlDestroyRBTree();
        SynceMainMemClear();
        OsixQueDel (gSynceGlobals.taskQueId);
        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC
                     | SYNCE_CRITICAL_TRC,
                    "%% Default Context Creation Failed.\r\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* The following routine initializes the timer descriptor structure */
    SynceTmrInitTmrDesc ();

    /* Register with external modules */
    if (SynceMainRegWithExtModules () == OSIX_FAILURE)
    {
        SynceMainDeInit();
        SYNCE_TRC ((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC |
                     SYNCE_CRITICAL_TRC,
                    "Registration with External Module Failed\r\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    gSynceGlobals.glbMib.i4FsSynceGlobalSysCtrl = SYNCE_MODULE_START;

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SynceMainDeInit                                            */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
SynceMainDeInit (VOID)
{
    SYNCE_FN_ENTRY ();

#ifdef SYSLOG_WANTED
    SYS_LOG_DEREGISTER (gSynceGlobals.u4SysLogId);
#endif

    SynceUtlDestroyRBTree();
    SynceMainMemClear ();

    if (gSynceGlobals.taskSemId)
    {
        OsixSemDel (gSynceGlobals.taskSemId);
        gSynceGlobals.taskSemId = 0;
    }
    if (gSynceGlobals.taskQueId)
    {
        OsixQueDel (gSynceGlobals.taskQueId);
        gSynceGlobals.taskQueId = 0;
    }
    if(gSynceGlobals.tmrList)
    {
        TmrDeleteTimerList (gSynceGlobals.tmrList);
        gSynceGlobals.tmrList = 0;
    }

    SynceMainDeRegWithExtModules();

    gSynceGlobals.glbMib.i4FsSynceGlobalSysCtrl = SYNCE_MODULE_SHUTDOWN;

    SYNCE_FN_EXIT ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SynceMainMemInit                                           */
/*                                                                           */
/* Description  : Allocates all the memory that is required for SYNCE        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
SynceMainMemInit (VOID)
{
    SYNCE_FN_ENTRY ();

    if (SynceSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* storing memory pools ids in global variable for easier access */
    gSynceGlobals.contextPoolId = 
        SYNCEMemPoolIds[MAX_SYNCE_FSSYNCETABLE_SIZING_ID];
    gSynceGlobals.ifPoolId =  
        SYNCEMemPoolIds[MAX_SYNCE_FSSYNCEIFTABLE_SIZING_ID];
    gSynceGlobals.msgQPoolId = 
        SYNCEMemPoolIds[MAX_SYNCE_MESSAGEQUEUE_SIZING_ID];
    gSynceGlobals.pktPoolId =
        SYNCEMemPoolIds[MAX_SYNCE_ESMC_PKT_SIZING_ID];

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SynceMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
SynceMainMemClear (VOID)
{
    SYNCE_FN_ENTRY ();
    SynceSizingMemDeleteMemPools ();

    gSynceGlobals.contextPoolId = 0;
    gSynceGlobals.ifPoolId =  0;
    gSynceGlobals.msgQPoolId = 0;
    gSynceGlobals.pktPoolId = 0;

    SYNCE_FN_EXIT ();
}

/****************************************************************************
 Function    :  SynceMainRegWithExtModules
 Input       :  None
 Description :  This routine registers the Synce module's APIs with the
                external modules. It also provides the corresponding
                call back functions so that the same is invoked on the
                appropriate events.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
UINT4
SynceMainRegWithExtModules (VOID)
{
    SYNCE_FN_ENTRY ();

    if (SynceCfaRegLL () == OSIX_FAILURE)
    {
        SYNCE_TRC((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_CRITICAL_TRC,
                    "Registration with cfa failed\r\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceMainDeRegWithExtModules
 Input       :  None
 Description :  This routine registers the Synce module's APIs with the
                external modules. It also provides the corresponding
                call back functions so that the same is invoked on the
                appropriate events.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
UINT4
SynceMainDeRegWithExtModules (VOID)
{
    SYNCE_FN_ENTRY ();

    if (SynceCfaDeRegisterLL (SYNCE_ESMC_ETHER_TYPE) == OSIX_FAILURE)
    {
        SYNCE_TRC((SYNCE_INIT_SHUT_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_CRITICAL_TRC,
                    "DeRegistration with cfa failed\r\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  syncemain.c                     */
/*-----------------------------------------------------------------------*/
