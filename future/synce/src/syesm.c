/******************************************************************************
 *  Copyright (C) 2013  Aricent Inc . All Rights Reserved
 *
 *  $Id: syesm.c,v 1.5 2016/07/29 09:53:56 siva Exp $
 *
 *  Description : This file contains the state machine implmented by the State
 *                Machine Sub Module in SyncE Module.
 *
 *****************************************************************************/

#ifndef SYNCE_SM_C
#define SYNCE_SM_C

#include "syeinc.h"
#include "syesm.h"

/* Transition Table */
SynceSmTransitionFn gaSynceSmTranstionTable[SYNCE_MAX_STATE][SYNCE_MAX_EVENT] = {
    {
	    /* QL_ENABLED_NORMAL_STATE */
     SynceSmFnQlEnNoSwPriorityCh, SynceSmFnQlEnNoSwQlModeCh,
     SynceSmFnQlEnNoSwQlCh, SynceSmFnInvalidStateOrEvent,
     SynceSmFnQlEnNoSwLockoutEnable, SynceSmFnQlEnNoSwClearLockout,
     SynceSmFnQlEnForceSwEn, SynceSmFnQlEnManualSwEnable,
     SynceSmFnForceSwNoCh, SynceSmFnQlEnNoSwSynceModeCh}
    ,
    {
	    /* QL_ENABLED_MANUAL_STATE */
     SynceSmFnQlEnManSwPriorityCh, SynceSmFnQlEnManSwQlModeCh,
     SynceSmFnQlEnManSwQlCh, SynceSmFnForceSwNoCh,
     SynceSmFnQlEnSwLockoutEn, SynceSmFnQlEnManSwClearLock,
     SynceSmFnQlEnForceSwEn, SynceSmFnQlEnManualSwEnable,
     SynceSmFnQlEnSwClearSw, SynceSmFnForceSwNoCh}
    ,
    {
	    /*QL_ENABLED_FORCED_STATE */
     SynceSmFnQlEnForceSwPriorityCh, SynceSmFnQlEnForceSwQlModeCh,
     SynceSmFnQlEnForceSwQlCh, SynceSmFnForceSwNoCh,
     SynceSmFnQlEnSwLockoutEn, SynceSmFnForceSwNoCh,
     SynceSmFnQlEnForceSwEn, SynceSmFnInvalidStateOrEvent,
     SynceSmFnQlEnSwClearSw, SynceSmFnForceSwNoCh}
    ,
    { /*QL_DISABLED_NORMAL_STATE*/
     SynceSmFnQlDisNoSwPriorityCh, SynceSmFnQlDisNoSwQlModeCh,
     SynceSmFnQlDisNoSwQlCh, SynceSmFnQlDisNoSwSignalCh,
     SynceSmFnQlDisNoSwLockoutEnable, SynceSmFnQlDisNoSwClearLockout,
     SynceSmFnQlDisForceSwEn, SynceSmFnQlDisManualSwEnable,
     SynceSmFnForceSwNoCh, SynceSmFnForceSwNoCh}
    ,
    {/*QL_DISABLED_MANUAL_STATE*/
     SynceSmFnQlDisManSwPriorityCh,SynceSmFnQlDisManSwQlModeCh,
     SynceSmFnForceSwNoCh, SynceSmFnQlDisManSwSignalCh,
     SynceSmFnQlDisManSwLockoutEn, SynceSmFnQlDisManSwClearLock,
     SynceSmFnQlDisForceSwEn, SynceSmFnQlDisManualSwEnable,
     SynceSmFnQlDisSwClearSw, SynceSmFnForceSwNoCh}
    ,
    {
	    /*QL_DISABLED_FORCED_STATE*/
     SynceSmFnQlDisForceSwPriorityCh, SynceSmFnQlDisForceSwQlModeCh,
     SynceSmFnForceSwNoCh, SynceSmFnForceSwNoCh,
     SynceSmFnQlDisForceSwLockoutEn, SynceSmFnForceSwNoCh,
     SynceSmFnQlDisForceSwEn,SynceSmFnInvalidStateOrEvent,
     SynceSmFnQlDisSwClearSw,SynceSmFnForceSwNoCh}
};

/*****************************************************************************
 * Function                  : SynceSmTriggerEvent                           *
 *                                                                           *
 * Description               : This routine will be invoked by the SM        *
 *                             handler to trigger the state machine of       *
 *                             the context. State transition takes place     *
 *                               according to the event occurred.               *
 *                                                                           *
 * Input                     : ContextId - Context Identifier                *
 *                               u1event   - Event Occurred                    *
 *                               eventData - Data related to event             *
 * Output                    : pu1Error  - Pointer to error number           *
 * Global Variables Referred : gSynceGblInfo                                 *
 * Global Variables Modified : gSynceGblInfo                                 *
 * Use of Recursion          : None.                                         *
 * Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     *
 *                                                                           *
 *****************************************************************************/

PUBLIC INT4
SynceSmTriggerEvent (UINT4 u4ContextId,
                     eSynceSmEvent smEvent, tSynceStateData * smData)
{
    eSynceSmState       curState, nextState;
    tSynceFsSynceEntry *pContextEntry = NULL;

    SYNCE_FN_ENTRY ();

    if (smData == NULL)
    {
        return OSIX_FAILURE;
    }

    if (smData->pSynceEntry == NULL)
    {
        /* Get current state of the context */
        if ((pContextEntry = SynceGetFsSynceEntry (u4ContextId)) == NULL)
        {
            smData->u1ErrNo = CLI_SYNCE_ERR_INVALID_CONTEXT;
            return OSIX_FAILURE;
        }

        smData->pSynceEntry = pContextEntry;
    }
    /* If wrong event is passed */
    if (smEvent >= SYNCE_MAX_EVENT)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "SM received invalid event\n"));
        return OSIX_FAILURE;
    }

    curState = smData->pSynceEntry->smState;
    /* if wrong state is passed */
    if (curState >= SYNCE_MAX_STATE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "SM is in invalid state\n"));
        return OSIX_FAILURE;
    }

    /* Refer Transition Table for next state */
    nextState = gaSynceSmTranstionTable
        [curState][smEvent] ((tSynceSmStateData) smData);

    if (nextState == (eSynceSmState) SYNCE_NO_CHANGE_STATE)
    {
        /* Check if error occurred during state function execution */
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC,
                    "Current State : %d, Event : %d, Next State : %d\n",
                    curState, smEvent, curState));
        if (smData->u1ErrNo != 0)
        {
            SYNCE_FN_EXIT ();
            SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Error occurred while SM transition\n"));
            CLI_SET_ERR (smData->u1ErrNo);
            return OSIX_FAILURE;
        }

    }
    else
    {
        smData->pSynceEntry->smState = nextState;
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC,
                    "Current State : %d, Event : %d, Next State : %d\n",
                    curState, smEvent, nextState));
    }

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlEnNoSwPriorityCh                   *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_NORMAL and        *
 *                             event occurred is PRIORITY_CHANGED.           *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 * Output                    : None                                             *
 * Global Variables Referred : gSynceGblInfo                                 *
 * Global Variables Modified : gSynceGblInfo                                 *
 * Use of Recursion          : None.                                         *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnNoSwPriorityCh (tSynceSmStateData smData)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
        /* check priority of interface */
        if (pIfEntry->MibObject.i4FsSynceIfPriority == 0)
        {
            pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
        }

        /* Call best clock selection sub module */
        SynceClkDoClockSelection ((UINT4) pStateData->pSynceEntry->MibObject.
                                  i4FsSynceContextId, SYNCE_QL_MODE_ENABLE);
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
        SYNCE_FN_EXIT ();
        return SYNCE_NO_CHANGE_STATE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlEnNoSwQlModeCh                     *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_NORMAL and        *
 *                             event occurred is QL_MODE_CHANGED.            *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : None                                             *
 *                                                                           *
 * Global Variables Referred : None                                          *
 *                                                                           *
 * Global Variables Modified : None                                             *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnNoSwQlModeCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch context Entry */
    pContextEntry = pStateData->pSynceEntry;

    /* call best clock selection sub module */
    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                              i4FsSynceContextId,
                              pContextEntry->MibObject.i4FsSynceQLMode);

    SYNCE_FN_EXIT ();
    return QL_DISABLED_NORMAL_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlEnNoSwQlCh                         *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_NORMAL and        *
 *                             event occurred is QL_CHANGED.                 *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : Error Number                                  *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : None                                             *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnNoSwQlCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch context Entry */
    pContextEntry = pStateData->pSynceEntry;

    /* call best clock selection sub module */
    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                              i4FsSynceContextId,
                              pContextEntry->MibObject.i4FsSynceQLMode);

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlEnNoSwLockoutEnable                *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_NORMAL and        *
 *                             event occurred is ENABLE_LOCKOUT_EVENT.       *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnNoSwLockoutEnable (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* fetch interface node */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
        /* Check priority of interface */
        if (pIfEntry->MibObject.i4FsSynceIfPriority == 0)
        {
            pStateData->u1ErrNo = CLI_SYNCE_ERR_MS_PRIORITY_ZERO;
            SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Lockout Rejected : Priority = 0\n"));
            SYNCE_FN_EXIT ();
            return SYNCE_NO_CHANGE_STATE;
        }

        pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_TRUE;

        /* fetch context entry */
        pContextEntry = pStateData->pSynceEntry;

        /* call best clock selection module */
        SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                                  i4FsSynceContextId,
                                  pContextEntry->MibObject.i4FsSynceQLMode);
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
        SYNCE_FN_EXIT ();
        return SYNCE_NO_CHANGE_STATE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnQlEnNoSwClearLockout                 */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_ENABLED_NORMAL and        */
/*                             event occurred is CLEAR_LOCKOUT_EVENT.        */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGloabals                                */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlEnNoSwClearLockout (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
        pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
        pContextEntry = pStateData->pSynceEntry;
        /* Call best clock selection sub module */
        SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                                  i4FsSynceContextId,
                                  pContextEntry->MibObject.i4FsSynceQLMode);
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
        SYNCE_FN_EXIT ();
        return SYNCE_NO_CHANGE_STATE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/****************************************************************************
 * Function    :  SynceSmFnQlEnNoSwSynceModeCh
 *
 * Description :  This routine shall be called in QL-Enabled mode whenever
 *                SyncE mode is changed on interface.
 * Input       :  smData - Data related to state and event.
 * Output      :  None
 * Returns     :  None
****************************************************************************/
eSynceSmState
SynceSmFnQlEnNoSwSynceModeCh (tSynceSmStateData smData)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;
    pContextEntry = pStateData->pSynceEntry;

    /* Call best clock selection sub module */
    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                              i4FsSynceContextId,
                              pContextEntry->MibObject.i4FsSynceQLMode);

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnQlDisNoSwPriorityCh                     */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_DISABLED_NORMAL and       */
/*                             event occurred is PRIORITY_CHANGED_EVENT.     */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlDisNoSwPriorityCh (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
        /* check priority of interface */
        if (pIfEntry->MibObject.i4FsSynceIfPriority == 0)
        {
            pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
        }
        pContextEntry = pStateData->pSynceEntry;

        /* Call best clock selection sub module */
        SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                                  i4FsSynceContextId,
                                  pContextEntry->MibObject.i4FsSynceQLMode);
    }
    /* If Interface Entry Doesn't Exist */
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
        SYNCE_FN_EXIT ();
        return SYNCE_NO_CHANGE_STATE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnQlDisNoSwQlModeCh                         */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_DISABLED_NORMAL and       */
/*                             event occurred is QL_MODE_CHANGED_EVENT.      */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlDisNoSwQlModeCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch context entry */
    pContextEntry = pStateData->pSynceEntry;

    /* call best clock selection sub module */
    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                              i4FsSynceContextId,
                              pContextEntry->MibObject.i4FsSynceQLMode);

    SYNCE_FN_EXIT ();
    return QL_ENABLED_NORMAL_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnQlDisNoSwQlCh                            */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_DISABLED_NORMAL and       */
/*                             event occurred is QL_CHANGED_EVENT.           */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlDisNoSwQlCh (tSynceSmStateData data)
{
    SYNCE_FN_ENTRY ();
    UNUSED_PARAM (data);
    SYNCE_FN_EXIT ();

    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnQlDisNoSwSignalCh                         */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_DISABLED_NORMAL and       */
/*                             event occurred is SIGNAL_STATUS_CHANGED.      */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlDisNoSwSignalCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch context Entry */
    pContextEntry = pStateData->pSynceEntry;

    /* call best clock selection sub module */
    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                              i4FsSynceContextId,
                              pContextEntry->MibObject.i4FsSynceQLMode);

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnQlDisNoSwLockoutEnable                 */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_DISABLED_NORMAL and       */
/*                             event occurred is ENABLE_LOCKOUT_EVENT.      */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlDisNoSwLockoutEnable (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
        /* check priority of interface */
        if (pIfEntry->MibObject.i4FsSynceIfPriority == 0)
        {
            pStateData->u1ErrNo = CLI_SYNCE_ERR_MS_PRIORITY_ZERO;
            SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Lockout Rejected : Priority = 0\n"));
            SYNCE_FN_EXIT ();
            return SYNCE_NO_CHANGE_STATE;
        }

        pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_TRUE;
        pContextEntry = pStateData->pSynceEntry;

        /* call best clock selection sub module */
        SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                                  i4FsSynceContextId,
                                  pContextEntry->MibObject.i4FsSynceQLMode);
    }
    /* If Interface Entry Doesn't Exist */
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
        SYNCE_FN_EXIT ();
        return SYNCE_NO_CHANGE_STATE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnQlDisNoSwClearLockout                 */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_DISABLED_NORMAL and       */
/*                             event occurred is CLEAR_LOCKOUT_EVENT.        */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGlobals                                 */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlDisNoSwClearLockout (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
        pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
        pContextEntry = pStateData->pSynceEntry;

        /* Call best clock selection sub module */
        SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                                  i4FsSynceContextId,
                                  pContextEntry->MibObject.i4FsSynceQLMode);
    }
    /* If Interface Entry Doesn't Exist */
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
        SYNCE_FN_EXIT ();
        return SYNCE_NO_CHANGE_STATE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/****************************************************************************
 * Function    :  SynceSmFnQlDisNoSwSynceModeCh
 *
 * Description :  This routine shall be called in QL-Disabled mode whenever
 *                SyncE mode is changed on interface.
 * Input       :  smData - data related to the state & event
 * Output      :  None
 * Returns     :  None
****************************************************************************/
eSynceSmState
SynceSmFnQlDisNoSwSynceModeCh (tSynceSmStateData smData)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;
    pContextEntry = pStateData->pSynceEntry;

    /* Call best clock selection sub module */
    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
                              i4FsSynceContextId,
                              pContextEntry->MibObject.i4FsSynceQLMode);

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  : SynceSmFnInvalidStateOrEvent                  */
/* Description               : It is invoked in case of invalid event        */
/*                                                                           */
/* Input                     : data - data required                          */
/* Output                    : *u1ErrNo - Error Number                       */
/* Global Variables Referred : None                                             */
/* Global Variables Modified : None                                            */
/* Use of Recursion          : None                                          */
/* Returns                   : new state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnInvalidStateOrEvent (tSynceSmStateData data)
{
    tSynceStateData    *pStateData = NULL;
    pStateData = (tSynceStateData *) data;

    SYNCE_FN_ENTRY ();

    pStateData->u1ErrNo = SYNCE_SM_ERR_WRONG_EVENT_OR_STATE;
    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_ALL_FAILURE_TRC,
                "Invalid state or event\n"));

    SYNCE_FN_EXIT ();

    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlEnForceSwEn                        *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_NORMAL            *
 *                             /QL_ENABLED_FORCED /QL_ENABLED_MANUAL and     *
 *                             event occurred is SWITCH_MODE_FORCED_EVENT    *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnForceSwEn (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    UINT4  u4SelectedInterface = 0; 
    UINT4               u4CurBestQlValue = 0;
    UINT4 u4CurrentContextId = 0;
#ifdef NPAPI_WANTED
    tSynceHwInfo        synceHwInfo;
#endif
    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	/* Update selected interface index */
	pContextEntry = pStateData->pSynceEntry;
	u4SelectedInterface = (UINT4) pIfEntry->MibObject.i4FsSynceIfIndex;
	SYNCE_LOG ((SYSLOG_ALERT_LEVEL, gSynceGlobals.u4SysLogId,
				"SyncE selected Interface #%d",  
				u4SelectedInterface));
	u4CurBestQlValue = pIfEntry->MibObject.u4FsSynceIfQLValue;
	pContextEntry->MibObject.i4FsSynceSelectedInterface =
                                 (INT4) u4SelectedInterface;
#ifdef NPAPI_WANTED
	MEMSET (&synceHwInfo, 0, sizeof (tSynceHwInfo));
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4ContextId = 
		(UINT4) pContextEntry->MibObject.i4FsSynceContextId;
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4IfIndex =
		u4SelectedInterface;
	synceHwInfo.u4InfoType = SYNCE_HW_SET_HW_CLK;
	if (SynceFsNpHwConfigSynceInfo (&synceHwInfo) != FNP_SUCCESS)
	{
		SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "%% HW Call Failed\n"));
	}
#endif
	if(SynceSmUpdateQLValue (u4CurrentContextId,
				u4CurBestQlValue) == OSIX_FAILURE)
	{
		SYNCE_FN_EXIT ();
		return SYNCE_NO_CHANGE_STATE;
	}
	SynceResetSwitchMode(u4CurrentContextId,u4SelectedInterface);
	SYNCE_FN_EXIT ();
	return QL_ENABLED_FORCED_STATE;
    }
    else
    {
	  pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
	  SYNCE_FN_EXIT ();
	  return SYNCE_NO_CHANGE_STATE;
    }
}
/*****************************************************************************
 * Function                  : SynceSmFnQlEnForceSwPriorityCh                *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_FORCED_STATE &    *
 *                             event occurred is PRIORITY_CHANGED.           *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 * Output                    : None                                          *
 * Global Variables Referred : gSynceGblInfo                                 *
 * Global Variables Modified : gSynceGblInfo                                 *
 * Use of Recursion          : None.                                         *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnForceSwPriorityCh (tSynceSmStateData smData)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    pContextEntry = pStateData->pSynceEntry;
	    if (pIfEntry->MibObject.i4FsSynceIfPriority == OSIX_FALSE)
	    {
		    pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
		    /* If priority is set to zero for selected interface
		     * trigger clock selection algorithm */

		    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
		        pContextEntry->MibObject.i4FsSynceSelectedInterface)
		    {
			    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
			    	i4FsSynceContextId, pContextEntry->MibObject.i4FsSynceQLMode);
			    pIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE; 
			    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
					"Force switch is reset as priority is disabled\n"));
			    SYNCE_FN_EXIT ();
			    return QL_ENABLED_NORMAL_STATE;
		    }
	    }
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlEnForceSwQlModeCh                  *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_FORCED_STATE      *
 *                             event occurred is QL_MODE_CHANGED.            *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Global Variables Referred : None                                          *
 *                                                                           *
 * Global Variables Modified : None                                          *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/
eSynceSmState
SynceSmFnQlEnForceSwQlModeCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    UINT4 u4CurrentContextId = 0;
    UINT4 u4CurBestQlValue = 0;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;
    /* Fetch context Entry */
    pContextEntry = pStateData->pSynceEntry;
    if (pContextEntry->MibObject.i4FsSynceSSMOptionMode == SYNCE_SSM_OPTION1)
    {
	    u4CurBestQlValue = SYNCE_QL_DNU;
    }
    else
    {
	    u4CurBestQlValue = SYNCE_QL_DUS;
    }
    u4CurrentContextId = (UINT4) pContextEntry->MibObject.i4FsSynceContextId;
    if(SynceSmUpdateQLValue (u4CurrentContextId,
			    u4CurBestQlValue) == OSIX_FAILURE)
    {
	    SYNCE_FN_EXIT ();
	    return SYNCE_NO_CHANGE_STATE;
    }
    SYNCE_FN_EXIT ();
    return QL_DISABLED_FORCED_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlEnSwLockoutEn                      *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_FORCED_STATE/     *
 *                             QL_ENABLED_MANUAL_STATE event occurred is     *
 *                             ENABLE_LOCKOUT_EVENT                          *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnSwLockoutEn (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* fetch interface node */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    /* Check priority of interface */
	    if (pIfEntry->MibObject.i4FsSynceIfPriority == OSIX_FALSE)
	    {
		    pStateData->u1ErrNo = CLI_SYNCE_ERR_MS_PRIORITY_ZERO;
		    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
					    "Lockout Rejected : Priority = 0\n"));
		    SYNCE_FN_EXIT ();
		    return SYNCE_NO_CHANGE_STATE;
	    }
	    /* fetch context entry */
	    pContextEntry = pStateData->pSynceEntry;
	    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
			    pContextEntry->MibObject.i4FsSynceSelectedInterface)
	    {
		    /* call best clock selection module if lock out is enabled
		     * for selected interface */
		    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
				    i4FsSynceContextId,
				    pContextEntry->MibObject.i4FsSynceQLMode);

		    pIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE; 
		    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
				"Switch mode is reset as Lockout is Set \n"));
		    SYNCE_FN_EXIT ();
		    return QL_ENABLED_NORMAL_STATE;
	    }
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlEnSwClearSw                        *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_FORCED_STATE/     *
 *                             QL_ENABLED_MANUAL_STATE  event occurred is    *
 *                             CLEAR_SWITCH_EVENT                            *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnSwClearSw (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* fetch interface node */

    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    /* fetch context entry */
	    pContextEntry = pStateData->pSynceEntry;
	    /* call best clock selection module as switch request is
	     * cleared */
	    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
			    i4FsSynceContextId,
			    pContextEntry->MibObject.i4FsSynceQLMode);
	    SYNCE_FN_EXIT ();
	    return QL_ENABLED_NORMAL_STATE;
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnForceSwNoCh                          *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             event occured does not have impact on the     *
 *                             current state                                 *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : Error Number                                  *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : None                                          *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnForceSwNoCh (tSynceSmStateData data)
{
	UNUSED_PARAM (data);
	SYNCE_FN_ENTRY ();

	SYNCE_FN_EXIT ();
	return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlDisForceSwPriorityCh               *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_FORCED_STATE &    *
 *                             event occurred is PRIORITY_CHANGED.           *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 * Output                    : None                                          *
 * Global Variables Referred : gSynceGblInfo                                 *
 * Global Variables Modified : gSynceGblInfo                                 *
 * Use of Recursion          : None.                                         *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisForceSwPriorityCh (tSynceSmStateData smData)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    pContextEntry = pStateData->pSynceEntry;
	    if (pIfEntry->MibObject.i4FsSynceIfPriority == OSIX_FALSE)
	    {
		    pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
		    /* If priority is set as zero for selected interface 
		     * trigger clock selection algorithm */
		    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
		        	pContextEntry->MibObject.i4FsSynceSelectedInterface)
		    {
		    	SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
				i4FsSynceContextId,pContextEntry->MibObject.i4FsSynceQLMode);
			SynceResetSwitchMode((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
					     SYNCE_INDEX_ZERO);
			SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
				   "Force switch is reset as priority is disabled\n"));
			SYNCE_FN_EXIT ();
			return QL_DISABLED_NORMAL_STATE;
		    }
	    }
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlDisForceSwQlModeCh                 *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_FORCED_STATE     *
 *                             event occurred is QL_MODE_CHANGED.            *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Global Variables Referred : None                                          *
 *                                                                           *
 * Global Variables Modified : None                                          *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/
eSynceSmState
SynceSmFnQlDisForceSwQlModeCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceIfEntry *pIfEntry = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch context Entry */
    pContextEntry = pStateData->pSynceEntry;
    if ((pIfEntry = SynceGetFsSynceIfEntry ((UINT4)pContextEntry->MibObject.
				            i4FsSynceSelectedInterface)) != NULL)
    {
	    /* QL value is changed to QL-DNU/DUS so Update the QL value */
	    if(SynceSmUpdateQLValue ((UINT4)pContextEntry->MibObject.i4FsSynceContextId,
				     (UINT4)pIfEntry->MibObject.u4FsSynceIfQLValue) 
			              == OSIX_FAILURE)
	    {
		    SYNCE_FN_EXIT ();
		    return SYNCE_NO_CHANGE_STATE;
	    }
	    SYNCE_FN_EXIT ();
	    return QL_ENABLED_FORCED_STATE;
    }
    else
    {
            pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return  SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlDisForceSwLockoutEn                *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISBLED_FORCED_STATE      *
 *                             event occurred is ENABLE_LOCKOUT_EVENT.       *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisForceSwLockoutEn (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    if (pIfEntry->MibObject.i4FsSynceIfPriority == OSIX_FALSE)
	    {
		    pStateData->u1ErrNo = CLI_SYNCE_ERR_MS_PRIORITY_ZERO;
		    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
					    "Lockout Rejected : Priority = 0\n"));
		    SYNCE_FN_EXIT ();
		    return SYNCE_NO_CHANGE_STATE;
	    }
	    pContextEntry = pStateData->pSynceEntry;
	    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
			    pContextEntry->MibObject.i4FsSynceSelectedInterface)
	    {
		    /* call best clock selection module is lockout is enabled
		     * for selected interface */
		    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
				    i4FsSynceContextId,
				    pContextEntry->MibObject.i4FsSynceQLMode);
		    pIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE;
		    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
			        "Force switch is reset as Lockout is set\n"));
		    SYNCE_FN_EXIT ();
		    return QL_DISABLED_NORMAL_STATE;
	    }
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlDisForceSwEn                       *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_FORCED_STATE/    *
 *                             QL_DISABLED_MANUAL/QL_DISABLED_NORMAL_STATE   *
 *                             event occurred is SWITCH_MODE_FORCED_EVENT     *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisForceSwEn (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    UINT4               u4CurBestQlValue = 0;
    UINT4               u4SelectedInterface = 0;
    UINT4               u4CurrentContextId = 0;
#ifdef NPAPI_WANTED
    tSynceHwInfo        synceHwInfo;
#endif
    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	/* Update the selected interface index */
	pContextEntry = pStateData->pSynceEntry;
	u4SelectedInterface = (UINT4) pIfEntry->MibObject.i4FsSynceIfIndex;
        pContextEntry->MibObject.i4FsSynceSelectedInterface =
                                 (INT4) u4SelectedInterface;
	SYNCE_LOG ((SYSLOG_ALERT_LEVEL, gSynceGlobals.u4SysLogId,
				"SyncE selected Interface #%d",  
				u4SelectedInterface));
	u4CurrentContextId = (UINT4) pContextEntry->MibObject.i4FsSynceContextId;
	if (pContextEntry->MibObject.i4FsSynceSSMOptionMode == SYNCE_SSM_OPTION1)
	{
		u4CurBestQlValue = SYNCE_QL_DNU;
	}
	else
	{
		u4CurBestQlValue = SYNCE_QL_DUS;
	}
#ifdef NPAPI_WANTED
	MEMSET (&synceHwInfo, 0, sizeof (tSynceHwInfo));
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4ContextId = 
		u4CurrentContextId;
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4IfIndex =
		u4SelectedInterface;
	synceHwInfo.u4InfoType = SYNCE_HW_SET_HW_CLK;
	if (SynceFsNpHwConfigSynceInfo (&synceHwInfo) != FNP_SUCCESS)
	{
		SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "%% HW Call Failed\n"));
	}
#endif
	if(SynceSmUpdateQLValue (u4CurrentContextId,
				u4CurBestQlValue) == OSIX_FAILURE)
	{
		SYNCE_FN_EXIT ();
		return SYNCE_NO_CHANGE_STATE;
	}
        SynceResetSwitchMode(u4CurrentContextId,u4SelectedInterface);
	SYNCE_FN_EXIT ();
	return QL_DISABLED_FORCED_STATE;
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
	    SYNCE_FN_EXIT ();
	    return SYNCE_NO_CHANGE_STATE;
    }
}
/*****************************************************************************
 * Function                  : SynceSmFnQlDisSwClearSw                       *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_FORCED_STATE/    * 
 *                             QL_DISABLED_MANUAL_STATE and                  *
 *                             event occurred is CLEAR_SWITCH_EVENT          *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisSwClearSw (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;
    /* fetch interface node */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    /* fetch context entry */
	    pContextEntry = pStateData->pSynceEntry;
	    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
			    i4FsSynceContextId,
			    pContextEntry->MibObject.i4FsSynceQLMode);
	    SYNCE_FN_EXIT ();
	    return QL_DISABLED_NORMAL_STATE;
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlManSwPriorityCh                    *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_MANUAL_STATE &    *
 *                             event occurred is PRIORITY_CHANGED.           *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 * Output                    : None                                          *
 * Global Variables Referred : gSynceGblInfo                                 *
 * Global Variables Modified : gSynceGblInfo                                 *
 * Use of Recursion          : None.                                         *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnManSwPriorityCh (tSynceSmStateData smData)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    pContextEntry = pStateData->pSynceEntry;
	    /* check priority of interface */
	    if (pIfEntry->MibObject.i4FsSynceIfPriority == OSIX_FALSE)
	    {
		    pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
		    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
				    pContextEntry->MibObject.i4FsSynceSelectedInterface)
		    {
			    /* If priority is disabled for selected interface
			     * trigger clock selection */
			    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
					    i4FsSynceContextId, SYNCE_QL_MODE_ENABLE);
                            pIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE;                     
			    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
				        "Manual switch is reset as priority is disabled\n"));
			    SYNCE_FN_EXIT ();
			    return QL_ENABLED_NORMAL_STATE;
		    }
	    }
	    else
	    {
		    if((pIfEntry->MibObject.i4FsSynceIfLockoutStatus == OSIX_FALSE)
		       && (pIfEntry->MibObject.u4FsSynceIfQLValue < 
			   pContextEntry->MibObject.u4FsSynceQLValue))
			    {
				    /* Trigger clock selection if there exists an
				     * interface with better QL value than the 
				     * selected interface */
				    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
						    i4FsSynceContextId, SYNCE_QL_MODE_ENABLE);
			            SynceResetSwitchMode((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
						         SYNCE_INDEX_ZERO);
			            SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
				        "Manual switch is reset as a better QL value exist\n"));
				    SYNCE_FN_EXIT ();
			            return QL_ENABLED_NORMAL_STATE;
			    }
	    }
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlEnManSwQlModeCh                    *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_MANUAL_STATE      *
 *                             event occurred is QL_MODE_CHANGED.            *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Global Variables Referred : None                                          *
 *                                                                           *
 * Global Variables Modified : None                                          *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnManSwQlModeCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    UINT4                u4CurBestQlValue = 0;
    UINT4                u4CurrentContextId = 0;
    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    pContextEntry = pStateData->pSynceEntry;
    if (pContextEntry->MibObject.i4FsSynceSSMOptionMode == SYNCE_SSM_OPTION1)
    {
	    u4CurBestQlValue = SYNCE_QL_DNU;
    }
    else
    {
	    u4CurBestQlValue = SYNCE_QL_DUS;
    }
    u4CurrentContextId = (UINT4) pContextEntry->MibObject.i4FsSynceContextId;
    if(SynceSmUpdateQLValue (u4CurrentContextId,
			    u4CurBestQlValue) == OSIX_FAILURE)
    {
	    SYNCE_FN_EXIT ();
	    return SYNCE_NO_CHANGE_STATE;
    }
    SYNCE_FN_EXIT ();
    return QL_DISABLED_MANUAL_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlEnManSwQlCh                        *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_MANUAL_STATE &    *
 *                             event occurred is QL_CHANGED.                 *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : Error Number                                  *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : None                                          *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnManSwQlCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceIfEntry *pCurIfEntry = NULL;
    UINT4 u4CurrentContextId = 0;
    UINT4 u4CurQlValue = 0;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch context Entry */
    pContextEntry = pStateData->pSynceEntry;
    u4CurrentContextId = (UINT4)pContextEntry->MibObject.i4FsSynceContextId; 
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    if (pContextEntry->MibObject.i4FsSynceSelectedInterface 
	       		!= pIfEntry->MibObject.i4FsSynceIfIndex)
	    {
		    if ((pIfEntry->MibObject.i4FsSynceIfPriority != OSIX_FALSE) &&
		        (pIfEntry->MibObject.i4FsSynceIfLockoutStatus == OSIX_FALSE) &&
		        (pIfEntry->MibObject.u4FsSynceIfQLValue <
		         pContextEntry->MibObject.u4FsSynceQLValue))
		    {
			    SynceClkDoClockSelection (u4CurrentContextId,
					    pContextEntry->MibObject.i4FsSynceQLMode);
			    SynceResetSwitchMode(u4CurrentContextId,SYNCE_INDEX_ZERO);
			    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
				        "Manual switch is reset\n"));
			    SYNCE_FN_EXIT ();
			    return QL_ENABLED_NORMAL_STATE;
		    }
	    }
	    else
	    {
		    /* In case of user configured QL value for an interface, 
		     * when admin status is down, signal fail will be set
		     * and QL change event will be triggered internally
		     * so accordingly selected QL value should be updated
		     * to DNU */
		    if((pIfEntry->MibObject.i4FsSynceIfIsRxQLForced == OSIX_TRUE)
		      && (pIfEntry->MibObject.i4FsSynceIfSignalFail == OSIX_TRUE))
		    {
			    if (pContextEntry->MibObject.i4FsSynceSSMOptionMode 
					     == SYNCE_SSM_OPTION1)
			    {
				    u4CurQlValue = SYNCE_QL_DNU;
			    }
			    else
			    {
				    u4CurQlValue = SYNCE_QL_DUS;
			    }
		    }
		    else
		    {
                            u4CurQlValue = pIfEntry->MibObject.u4FsSynceIfQLValue;
		    }
		    /* If there exists any interface with better QL value
		     * which is not locked out and has a valid priority, then 
		     * retrigger clock selection */

		    for (pCurIfEntry = SynceGetFirstFsSynceIfTable ();
				    pCurIfEntry != NULL;
				    pCurIfEntry = SynceGetNextFsSynceIfTable (pCurIfEntry))
		    {
			    if ((pCurIfEntry->MibObject.i4FsSynceIfPriority != OSIX_FALSE) &&
			        (pCurIfEntry->MibObject.i4FsSynceIfLockoutStatus == OSIX_FALSE) &&
			        (pCurIfEntry->MibObject.u4FsSynceIfQLValue < u4CurQlValue))
			    {
				    SynceClkDoClockSelection (u4CurrentContextId,
						    pContextEntry->MibObject.i4FsSynceQLMode);
				    SynceResetSwitchMode(u4CurrentContextId,SYNCE_INDEX_ZERO);
				    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
					        "Manual switch is reset \n"));
				    SYNCE_FN_EXIT ();
				    return QL_ENABLED_NORMAL_STATE;
			    }
		    }
		    if(SynceSmUpdateQLValue (u4CurrentContextId,
					     pIfEntry->MibObject.u4FsSynceIfQLValue)
				             == OSIX_FAILURE)
		    {
			    SYNCE_FN_EXIT ();
			    return SYNCE_NO_CHANGE_STATE;
		    }
	    }
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************/
/* Function                  : SynceSmFnQlEnManSwClearLock                   */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_ENABLED_MANUAL_STATE/     */
/*                             event occurred is CLEAR_LOCKOUT_EVENT.        */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGloabals                                */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                    */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlEnManSwClearLock (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;
    pContextEntry = pStateData->pSynceEntry;
    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
          pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
	  if((pIfEntry->MibObject.i4FsSynceIfPriority != OSIX_FALSE)
			  && 
	     (pIfEntry->MibObject.u4FsSynceIfQLValue < 
		pContextEntry->MibObject.u4FsSynceQLValue))
	  {
		  SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
				            i4FsSynceContextId, 
					    SYNCE_QL_MODE_ENABLE);
		  SynceResetSwitchMode((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
				       SYNCE_INDEX_ZERO);
	          SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
			     "Manual switch is reset\n"));
		  SYNCE_FN_EXIT ();
		  return QL_ENABLED_NORMAL_STATE;
	  }
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlEnManualSwEnable                   *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_NORMAL_STATE/     *
 *                             QL_ENABLED_MANUAL_STATE and                   *
 *                             event occurred is SWITCH_MODE_MANUAL_EVENT    *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlEnManualSwEnable (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT4  u4SelectedInterface = 0; 
    UINT4               u4CurBestQlValue = 0;
    UINT4 u4CurrentContextId = 0;

#ifdef NPAPI_WANTED
    tSynceHwInfo        synceHwInfo;
#endif

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* fetch interface node */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	pContextEntry = pStateData->pSynceEntry;
	pSynceEntry = SynceGetFsSynceEntry ((UINT4)pContextEntry->MibObject.i4FsSynceContextId);
	if (pSynceEntry == NULL)
	{
		SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
			    SYNCE_CRITICAL_TRC, "Could not find system context.\n"));
		SYNCE_FN_EXIT ();
		return SYNCE_NO_CHANGE_STATE;
	}
	if (((pContextEntry->MibObject.i4FsSynceSSMOptionMode == SYNCE_SSM_OPTION1) 
             && (pIfEntry->MibObject.u4FsSynceIfQLValue >= SYNCE_QL_DNU))
	                    || 
	   ((pContextEntry->MibObject.i4FsSynceSSMOptionMode  == SYNCE_SSM_OPTION2GEN1)
	     && (pIfEntry->MibObject.u4FsSynceIfQLValue >= SYNCE_QL_DUS))
	                    ||
	   ((pContextEntry->MibObject.i4FsSynceSSMOptionMode  == SYNCE_SSM_OPTION2GEN2)
	     && (pIfEntry->MibObject.u4FsSynceIfQLValue >= SYNCE_QL_DUS))
	                    ||
	   (pIfEntry->MibObject.u4FsSynceIfQLValue > 
			   pContextEntry->MibObject.u4FsSynceQLValue))
	{ 
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_MS_QL_VALUE_LOW;
            SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Manual switch Rejected as QL value is Less \n"));
            SYNCE_FN_EXIT ();
            return SYNCE_NO_CHANGE_STATE;
	}

	u4SelectedInterface = (UINT4) pIfEntry->MibObject.i4FsSynceIfIndex;
        pContextEntry->MibObject.i4FsSynceSelectedInterface =
                                 (INT4) u4SelectedInterface;

	SYNCE_LOG ((SYSLOG_ALERT_LEVEL, gSynceGlobals.u4SysLogId,
				"SyncE selected Interface #%d",  
				u4SelectedInterface));
#ifdef NPAPI_WANTED
	MEMSET (&synceHwInfo, 0, sizeof (tSynceHwInfo));
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4ContextId = 
		(UINT4) pContextEntry->MibObject.i4FsSynceContextId;
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4IfIndex =
		u4SelectedInterface;
	synceHwInfo.u4InfoType = SYNCE_HW_SET_HW_CLK;
	if (SynceFsNpHwConfigSynceInfo (&synceHwInfo) != FNP_SUCCESS)
	{
		SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "%% HW Call Failed\n"));
	}
#endif
	u4CurrentContextId = (UINT4) pContextEntry->MibObject.i4FsSynceContextId;
	u4CurBestQlValue = pIfEntry->MibObject.u4FsSynceIfQLValue;
	if(SynceSmUpdateQLValue (u4CurrentContextId,
				 u4CurBestQlValue) == OSIX_FAILURE)
	{
		SYNCE_FN_EXIT ();
		return SYNCE_NO_CHANGE_STATE;
	}
	SynceResetSwitchMode(u4CurrentContextId,u4SelectedInterface);
	SYNCE_FN_EXIT ();
	return QL_ENABLED_MANUAL_STATE;
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
	    SYNCE_FN_EXIT ();
	    return SYNCE_NO_CHANGE_STATE;
    }  
}
/*****************************************************************************
 * Function                  :SynceSmFnQlDisManSwPriorityCh                  *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_MANUAL_STATE &   *
 *                             event occurred is PRIORITY_CHANGED.           *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 * Output                    : None                                          *
 * Global Variables Referred : gSynceGblInfo                                 *
 * Global Variables Modified : gSynceGblInfo                                 *
 * Use of Recursion          : None.                                         *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisManSwPriorityCh (tSynceSmStateData smData)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    pContextEntry = pStateData->pSynceEntry;
	    if (pIfEntry->MibObject.i4FsSynceIfPriority == OSIX_FALSE)
	    {
		    pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;
		    /* If priority changed for selected interface then
		     * retrigger clock selection */
		    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
			pContextEntry->MibObject.i4FsSynceSelectedInterface)
		    {
			    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
					             pContextEntry->MibObject.i4FsSynceQLMode);
			    pIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE;
			    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
				        "Manual switch is reset as priority is disabled\n"));
			    SYNCE_FN_EXIT ();
			    return QL_DISABLED_NORMAL_STATE;
		    }

	    }
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  :SynceSmFnQlDisManSwQlModeCh                    *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_MANUAL_STATE     *
 *                             event occurred is QL_MODE_CHANGED.            *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : None                                          *
 *                                                                           *
 * Global Variables Referred : None                                          *
 *                                                                           *
 * Global Variables Modified : None                                          *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisManSwQlModeCh (tSynceSmStateData data)
{
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceIfEntry *pCurIfEntry = NULL;
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    UINT4  u4IfIndex = 0;
    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch context Entry */
    pContextEntry = pStateData->pSynceEntry;
    u4IfIndex = (UINT4) pContextEntry->MibObject.i4FsSynceSelectedInterface;
    if ((pIfEntry = SynceGetFsSynceIfEntry (u4IfIndex)) != NULL)
    {
	    if (((pContextEntry->MibObject.i4FsSynceSSMOptionMode == SYNCE_SSM_OPTION1) 
		 && (pIfEntry->MibObject.u4FsSynceIfQLValue >= SYNCE_QL_DNU))
			    || 
		 ((pContextEntry->MibObject.i4FsSynceSSMOptionMode  == SYNCE_SSM_OPTION2GEN1)
		 && (pIfEntry->MibObject.u4FsSynceIfQLValue >= SYNCE_QL_DUS))
			    ||
		 ((pContextEntry->MibObject.i4FsSynceSSMOptionMode  == SYNCE_SSM_OPTION2GEN2)
	         && (pIfEntry->MibObject.u4FsSynceIfQLValue >= SYNCE_QL_DUS)))
	    {
		    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
				    pContextEntry->MibObject.i4FsSynceQLMode);
		    SynceResetSwitchMode((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
				    SYNCE_INDEX_ZERO);
		    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
			       "Manual switch is reset as a better QL value exist\n"));
		    SYNCE_FN_EXIT ();
		    return QL_ENABLED_NORMAL_STATE;
	    }
	    else
	    {
		    /* verify and trigger clock selection if there exists an 
		     * interface with better QL which is not locked out 
		     * and has a non zero priority*/
		    for (pCurIfEntry = SynceGetFirstFsSynceIfTable ();
			 pCurIfEntry != NULL;
			 pCurIfEntry = SynceGetNextFsSynceIfTable (pCurIfEntry))
		    {
			    if((pCurIfEntry->MibObject.i4FsSynceIfPriority != OSIX_FALSE) &&
			       (pCurIfEntry->MibObject.i4FsSynceIfLockoutStatus == OSIX_FALSE) &&
			       (pCurIfEntry->MibObject.u4FsSynceIfQLValue < 
			       pIfEntry->MibObject.u4FsSynceIfQLValue))
			    {
				    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
						    pContextEntry->MibObject.i4FsSynceQLMode);
				    SynceResetSwitchMode((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
						    SYNCE_INDEX_ZERO);
		                    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
					    "Manual switch is reset as a better QL value exist\n"));
				    SYNCE_FN_EXIT ();
				    return QL_ENABLED_NORMAL_STATE;
			    }
		    }
	    }
    }
    else
    {
	  pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
	  SYNCE_FN_EXIT ();
	  return SYNCE_NO_CHANGE_STATE;
    }
    if(SynceSmUpdateQLValue ((UINT4) pContextEntry->MibObject.i4FsSynceContextId,
			     pIfEntry->MibObject.u4FsSynceIfQLValue) == OSIX_FAILURE)
    {
	    SYNCE_FN_EXIT ();
	    return SYNCE_NO_CHANGE_STATE;
    }
    SYNCE_FN_EXIT ();
    return QL_ENABLED_MANUAL_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmFnQlDisManSwSignalCh                   *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_MANUAL_STATE &   *
 *                             event occurred is SIGNAL_STATUS_CHANGED_EVENT *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 * Output                    : None                                          *
 * Global Variables Referred : gSynceGblInfo                                 *
 * Global Variables Modified : gSynceGblInfo                                 *
 * Use of Recursion          : None.                                         *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisManSwSignalCh (tSynceSmStateData smData)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) smData;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    pContextEntry = pStateData->pSynceEntry;
	    /* check priority of interface */
	    if ((pIfEntry->MibObject.i4FsSynceIfPriority != OSIX_FALSE)
                && (pIfEntry->MibObject.i4FsSynceIfLockoutStatus == OSIX_FALSE)
                && (pIfEntry->MibObject.i4FsSynceIfSignalFail == OSIX_TRUE))
	    {
		    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
				    pContextEntry->MibObject.i4FsSynceSelectedInterface)
		    {
			    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
					    i4FsSynceContextId,pContextEntry->MibObject.i4FsSynceQLMode);
                            pIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE; 
			    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
			    "Manual switch is reset as interface is in signal-fail state\n"));
			    SYNCE_FN_EXIT ();
			    return QL_DISABLED_NORMAL_STATE;
		    }

	    }
    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }

    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlDisManSwLockoutEn                  *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_MANUAL_STATE     *
 *                             event occurred is ENABLE_LOCKOUT_EVENT        *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                    *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisManSwLockoutEn (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    if (pIfEntry->MibObject.i4FsSynceIfPriority == OSIX_FALSE)
	    {
		    pStateData->u1ErrNo = CLI_SYNCE_ERR_MS_PRIORITY_ZERO;
		    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
					    "Lockout Rejected : Priority = 0\n"));
		    SYNCE_FN_EXIT ();
		    return SYNCE_NO_CHANGE_STATE;
	    }
	    pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_TRUE;
	    pContextEntry = pStateData->pSynceEntry;
	    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
			    pContextEntry->MibObject.i4FsSynceSelectedInterface)
	    {
		    SynceClkDoClockSelection ((UINT4) pContextEntry->MibObject.
				    i4FsSynceContextId,
				    pContextEntry->MibObject.i4FsSynceQLMode);
                    pIfEntry->MibObject.i4FsSynceIfConfigSwitch = SYNCE_NO_SWITCH_MODE;
		    SYNCE_TRC ((SYNCE_CRITICAL_TRC, 
				   "Manual switch is reset as Lockout is set\n"));
		    SYNCE_FN_EXIT ();
		    return QL_DISABLED_NORMAL_STATE;
	    }
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
    SYNCE_FN_EXIT ();
    return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************/
/* Function                  :SynceSmFnQlDisManSwClearLock                   */
/*                                                                           */
/* Description               : This routine is invoked when the              */
/*                             current state is QL_DISABLED_MANUAL_STATE     */
/*                             event occurred is CLEAR_LOCKOUT_EVENT.        */
/*                                                                           */
/* Input                     : data - data required by the routine           */
/*                                                                           */
/* Output                    : *perrorno - Error Number                      */
/*                                                                           */
/* Global Variables Referred : gSynceGlobals                                 */
/*                                                                           */
/* Global Variables Modified : gSynceGloabals                                */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : next state                                     */
/*                                                                           */
/*****************************************************************************/

eSynceSmState
SynceSmFnQlDisManSwClearLock (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Fetch Interface Entry */
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
          pIfEntry->MibObject.i4FsSynceIfLockoutStatus = OSIX_FALSE;

    }
    else
    {
        pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
        SYNCE_FN_EXIT ();
        return SYNCE_NO_CHANGE_STATE;
}

/*****************************************************************************
 * Function                  : SynceSmFnQlDisManualSwEnable                  *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_DISABLED_MANUAL_STATE/    *
 *                             QL_DISABLED_NORMAL_STATE and                  *
 *                             event occurred is SWITCH_MODE_MANUAL_EVENT    *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : *perrno - Error Number                        *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : gSynceGlobals                                 *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/

eSynceSmState
SynceSmFnQlDisManualSwEnable (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pSynceEntry = NULL;
    UINT4  u4SelectedInterface = 0; 
    UINT4  u4CurBestQlValue = 0;
    UINT4  u4CurrentContextId = 0;
#ifdef NPAPI_WANTED
    tSynceHwInfo        synceHwInfo;
#endif
    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	pContextEntry = pStateData->pSynceEntry;
	pSynceEntry = SynceGetFsSynceEntry ((UINT4)pContextEntry->MibObject.i4FsSynceContextId);
	if (pSynceEntry == NULL)
	{
		SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
			    SYNCE_CRITICAL_TRC, "Could not find system context.\n"));
		SYNCE_FN_EXIT ();
		return SYNCE_NO_CHANGE_STATE;
	}
	u4SelectedInterface = (UINT4) pIfEntry->MibObject.i4FsSynceIfIndex;
	pSynceEntry->MibObject.i4FsSynceSelectedInterface =
		(INT4) u4SelectedInterface;

	SYNCE_LOG ((SYSLOG_ALERT_LEVEL, gSynceGlobals.u4SysLogId,
				"SyncE selected Interface #%d",  
				u4SelectedInterface));
	u4CurrentContextId = (UINT4) pContextEntry->MibObject.i4FsSynceContextId;
	if (pContextEntry->MibObject.i4FsSynceSSMOptionMode == SYNCE_SSM_OPTION1)
	{
		u4CurBestQlValue = SYNCE_QL_DNU;
	}
	else
	{
		u4CurBestQlValue = SYNCE_QL_DUS;
	}
#ifdef NPAPI_WANTED
	MEMSET (&synceHwInfo, 0, sizeof (tSynceHwInfo));
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4ContextId = 
		(UINT4) pContextEntry->MibObject.i4FsSynceContextId;
	synceHwInfo.unHwParam.SynceHwSynceConfigHwClk.u4IfIndex =
		u4SelectedInterface;
	synceHwInfo.u4InfoType = SYNCE_HW_SET_HW_CLK;
	if (SynceFsNpHwConfigSynceInfo (&synceHwInfo) != FNP_SUCCESS)
	{
		SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "%% HW Call Failed\n"));
	}
#endif
	if(SynceSmUpdateQLValue (u4CurrentContextId,
				u4CurBestQlValue) == OSIX_FAILURE)
	{
		SYNCE_FN_EXIT ();
		return SYNCE_NO_CHANGE_STATE;
	}
        SynceResetSwitchMode(u4CurrentContextId,u4SelectedInterface);
	SYNCE_FN_EXIT ();
	return QL_DISABLED_MANUAL_STATE;
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
	    SYNCE_FN_EXIT ();
	    return SYNCE_NO_CHANGE_STATE;
    }
}


/*****************************************************************************
 * Function                  : SynceSmFnQlEnForceSwQlCh                      *
 *                                                                           *
 * Description               : This routine is invoked when the              *
 *                             current state is QL_ENABLED_FORCED_STATE &    *
 *                             event occurred is QL_CHANGED.                 *
 *                                                                           *
 * Input                     : data - data required by the routine           *
 *                                                                           *
 * Output                    : Error Number                                  *
 *                                                                           *
 * Global Variables Referred : gSynceGlobals                                 *
 *                                                                           *
 * Global Variables Modified : None                                          *
 *                                                                           *
 * Use of Recursion          : None                                          *
 *                                                                           *
 * Returns                   : next state                                     *
 *                                                                           *
 *****************************************************************************/
eSynceSmState
SynceSmFnQlEnForceSwQlCh (tSynceSmStateData data)
{
    tSynceFsSynceIfEntry *pIfEntry = NULL;
    tSynceStateData    *pStateData = NULL;
    tSynceFsSynceEntry *pContextEntry = NULL;
    UINT4 u4CurBestQlValue = 0;

    SYNCE_FN_ENTRY ();
    pStateData = (tSynceStateData *) data;

    /* Ql change will not impact as forced switch is set 
     * if the QL value of selected source is changed it needs to 
     * be updated in system*/
    if ((pIfEntry = SynceGetFsSynceIfEntry (pStateData->u4IfIndex)) != NULL)
    {
	    pContextEntry = pStateData->pSynceEntry;
	    if (pIfEntry->MibObject.i4FsSynceIfIndex ==
			    pContextEntry->MibObject.i4FsSynceSelectedInterface)
	    {
		    /* In case of user configured QL value for an interface, 
		     * when admin status is down, signal fail will be set
		     * and QL change event will be triggered internally
		     * so accordingly selected QL value should be updated
		     * to DNU */
		    if((pIfEntry->MibObject.i4FsSynceIfIsRxQLForced == OSIX_TRUE)
		      && (pIfEntry->MibObject.i4FsSynceIfSignalFail == OSIX_TRUE))
		    {
			    if (pContextEntry->MibObject.i4FsSynceSSMOptionMode 
					     == SYNCE_SSM_OPTION1)
			    {
				    u4CurBestQlValue = SYNCE_QL_DNU;
			    }
			    else
			    {
				    u4CurBestQlValue = SYNCE_QL_DUS;
			    }
		    }
		    else
		    {
		    u4CurBestQlValue = 
			    pIfEntry->MibObject.u4FsSynceIfQLValue;
                    }
		    if(SynceSmUpdateQLValue ((UINT4)pContextEntry->MibObject.
					     i4FsSynceContextId,
			 		     u4CurBestQlValue) == OSIX_FAILURE)
		    {
			    SYNCE_FN_EXIT ();
			    return SYNCE_NO_CHANGE_STATE;
		    }
	    }
    }
    else
    {
	    pStateData->u1ErrNo = CLI_SYNCE_ERR_INVALID_INTERFACE;
    }
	SYNCE_FN_EXIT ();
	return SYNCE_NO_CHANGE_STATE;
}
/*****************************************************************************
 * Function                  : SynceSmUpdateQLValue                          *
 *                                                                           *
 * Description               : This routine is invoked to update the QL      *
 *                             value in HW and to trigger Tx interfaces to   *
 *                             send the new QL value                         *
 *                                                                           *
 * Input                     :  u4ContextId,                                 *
 *                             u4CurBestQlValue                              *
 * Output                    : None                                          *
 * Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     *
 *                                                                           *
 *****************************************************************************/

INT4
SynceSmUpdateQLValue (UINT4 u4ContextId,
		     UINT4 u4CurBestQlValue)
{
    tSynceFsSynceEntry *pSynceEntry = NULL;
    tSynceFsSynceIfEntry *pCurIfEntry = NULL;
    UINT4  u4IfContextId = 0;

    SYNCE_FN_ENTRY ();
    pSynceEntry = SynceGetFsSynceEntry (u4ContextId);
    if (pSynceEntry == NULL)
    {
	    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
				    SYNCE_CRITICAL_TRC, "Could not find system context.\n"));
	    return OSIX_FAILURE;
    }
    if (u4CurBestQlValue != pSynceEntry->MibObject.u4FsSynceQLValue)
    {
	    /* search all the entries on which esmc mode is tx */
	    for (pCurIfEntry = SynceGetFirstFsSynceIfTable ();
			    pCurIfEntry != NULL;
			    pCurIfEntry = SynceGetNextFsSynceIfTable (pCurIfEntry))
	    {
		    if (SynceUtilGetContextIdFromIfIndex ((UINT4) pCurIfEntry->
					    MibObject.i4FsSynceIfIndex,
					    &u4IfContextId) ==
				            OSIX_FAILURE)
		    {
			    continue;
		    }
		    if (u4IfContextId != (UINT4) pSynceEntry->MibObject.i4FsSynceContextId)
		    {
			    continue;
		    }
		    if (pCurIfEntry->MibObject.i4FsSynceIfEsmcMode ==
				    SYNCE_ESMC_MODE_TX)
		    {
			    /*set the event flag to indicate change in the QL value
			      event PDU needs to be sent */
			    pCurIfEntry->b1TxEventPdu = OSIX_TRUE;
		    }
	    }
	    SYNCE_LOG ((SYSLOG_ALERT_LEVEL, gSynceGlobals.u4SysLogId,
				    "System selected QL changed. "
				    " Old QL:%d, New QL:%d",
				    pSynceEntry->MibObject.u4FsSynceQLValue, u4CurBestQlValue));
	    pSynceEntry->MibObject.u4FsSynceQLValue = u4CurBestQlValue;
    }
    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif
