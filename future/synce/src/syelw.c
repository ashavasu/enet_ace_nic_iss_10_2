/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syelw.c,v 1.2 2013/04/25 11:37:09 siva Exp $ 
*
* Description: This file contains some of low level functions used by protocol in Synce module
*********************************************************************/

#include "syeinc.h"
#include "cfa.h"
#include "vcm.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSynceTable
 Input       :  The Indices
                FsSynceContextId
 Output      :  The routine validates the given 
                Context id.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSynceTable (INT4 i4FsSynceContextId)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4FsSynceContextId = (UINT4) i4FsSynceContextId;

    if (OSIX_FALSE == SynceUtilIsSynceEnabled ())
    {
        return SNMP_FAILURE;
    }

    if (VcmIsVcExist (u4FsSynceContextId) == VCM_FALSE)
    {
        SYNCE_TRC ((SYNCE_MGMT_TRC, "Invalid Context Index\r\n"));
        i1RetVal = SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsSynceIfTable
 Input       :  The Indices
                FsSynceIfIndex
 Output      :  The routine validates the given 
                interface index.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsSynceIfTable (INT4 i4FsSynceIfIndex)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4FsSynceIfIndex = (UINT4) i4FsSynceIfIndex;

    if (OSIX_FALSE == SynceUtilIsSynceEnabled ())
    {
        return SNMP_FAILURE;
    }

    if ((CfaIsPhysicalInterface (u4FsSynceIfIndex) == CFA_FALSE) ||
        (CfaValidateIfIndex (u4FsSynceIfIndex) == CFA_FAILURE))
    {
        SYNCE_TRC ((SYNCE_MGMT_TRC, "Invalid Interface Index\r\n"));
        i1RetVal = SNMP_FAILURE;
    }

    return i1RetVal;
}
