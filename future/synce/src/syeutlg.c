/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syeutlg.c,v 1.2 2016/07/06 11:05:11 siva Exp $ 
*
* Description: This file contains utility functions used by protocol Synce
*********************************************************************/

#include "syeinc.h"

/****************************************************************************
 Function    :  SynceUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
SynceUtlCreateRBTree ()
{

    if (SynceFsSynceTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (SynceFsSynceIfTableCreate () == OSIX_FAILURE)

    {
        RBTreeDestroy (gSynceGlobals.glbMib.FsSynceTable, NULL, 0);
        gSynceGlobals.glbMib.FsSynceTable = NULL;
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceFsSynceTableCreate
 Input       :  None
 Description :  This function creates the FsSynceTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
SynceFsSynceTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tSynceFsSynceEntry, MibObject.FsSynceTableNode);

    if ((gSynceGlobals.glbMib.FsSynceTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsSynceTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceFsSynceIfTableCreate
 Input       :  None
 Description :  This function creates the FsSynceIfTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
SynceFsSynceIfTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tSynceFsSynceIfEntry, MibObject.FsSynceIfTableNode);

    if ((gSynceGlobals.glbMib.FsSynceIfTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsSynceIfTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsSynceTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsSynceTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsSynceTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tSynceFsSynceEntry *pFsSynceEntry1 = (tSynceFsSynceEntry *) pRBElem1;
    tSynceFsSynceEntry *pFsSynceEntry2 = (tSynceFsSynceEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsSynceEntry1->MibObject.i4FsSynceContextId >
        pFsSynceEntry2->MibObject.i4FsSynceContextId)
    {
        return 1;
    }
    else if (pFsSynceEntry1->MibObject.i4FsSynceContextId <
             pFsSynceEntry2->MibObject.i4FsSynceContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsSynceIfTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsSynceIfTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsSynceIfTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tSynceFsSynceIfEntry *pFsSynceIfEntry1 = (tSynceFsSynceIfEntry *) pRBElem1;
    tSynceFsSynceIfEntry *pFsSynceIfEntry2 = (tSynceFsSynceIfEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsSynceIfEntry1->MibObject.i4FsSynceIfIndex >
        pFsSynceIfEntry2->MibObject.i4FsSynceIfIndex)
    {
        return 1;
    }
    else if (pFsSynceIfEntry1->MibObject.i4FsSynceIfIndex <
             pFsSynceIfEntry2->MibObject.i4FsSynceIfIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsSynceTableFilterInputs
 Input       :  The Indices
                pSynceFsSynceEntry
                pSynceSetFsSynceEntry
                pSynceIsSetFsSynceEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsSynceTableFilterInputs (tSynceFsSynceEntry * pSynceFsSynceEntry,
                          tSynceFsSynceEntry * pSynceSetFsSynceEntry,
                          tSynceIsSetFsSynceEntry * pSynceIsSetFsSynceEntry)
{
    if (pSynceIsSetFsSynceEntry->bFsSynceContextId == OSIX_TRUE)
    {
        if (pSynceFsSynceEntry->MibObject.i4FsSynceContextId ==
            pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId)
            pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceTraceOption == OSIX_TRUE)
    {
        if (pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption ==
            pSynceSetFsSynceEntry->MibObject.u4FsSynceTraceOption)
            pSynceIsSetFsSynceEntry->bFsSynceTraceOption = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceQLMode == OSIX_TRUE)
    {
        if (pSynceFsSynceEntry->MibObject.i4FsSynceQLMode ==
            pSynceSetFsSynceEntry->MibObject.i4FsSynceQLMode)
            pSynceIsSetFsSynceEntry->bFsSynceQLMode = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode == OSIX_TRUE)
    {
        if (pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode ==
            pSynceSetFsSynceEntry->MibObject.i4FsSynceSSMOptionMode)
            pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus == OSIX_TRUE)
    {
        if (pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
            pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus)
            pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus = OSIX_FALSE;
    }
    if ((pSynceIsSetFsSynceEntry->bFsSynceContextId == OSIX_FALSE)
        && (pSynceIsSetFsSynceEntry->bFsSynceTraceOption == OSIX_FALSE)
        && (pSynceIsSetFsSynceEntry->bFsSynceQLMode == OSIX_FALSE)
        && (pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode == OSIX_FALSE)
        && (pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsSynceIfTableFilterInputs
 Input       :  The Indices
                pSynceFsSynceIfEntry
                pSynceSetFsSynceIfEntry
                pSynceIsSetFsSynceIfEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsSynceIfTableFilterInputs (tSynceFsSynceIfEntry * pSynceFsSynceIfEntry,
                            tSynceFsSynceIfEntry * pSynceSetFsSynceIfEntry,
                            tSynceIsSetFsSynceIfEntry *
                            pSynceIsSetFsSynceIfEntry)
{
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex ==
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex)
            pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode ==
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode)
            pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode ==
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode)
            pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority ==
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfPriority)
            pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue ==
            pSynceSetFsSynceIfEntry->MibObject.u4FsSynceIfQLValue)
            pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus ==
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus)
            pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch ==
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch)
	    pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch = OSIX_FALSE;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus == OSIX_TRUE)
    {
        if (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus)
            pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_FALSE;
    }
    if ((pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex == OSIX_FALSE)
        && (pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode == OSIX_FALSE)
        && (pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode == OSIX_FALSE)
        && (pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority == OSIX_FALSE)
        && (pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue == OSIX_FALSE)
        && (pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus == OSIX_FALSE)
        && (pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch == OSIX_FALSE)
        && (pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  SynceSetAllFsSynceTableTrigger
 Input       :  The Indices
                pSynceSetFsSynceEntry
                pSynceIsSetFsSynceEntry
 Description :  This Routine is used to send
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
SynceSetAllFsSynceTableTrigger (tSynceFsSynceEntry * pSynceSetFsSynceEntry,
                                tSynceIsSetFsSynceEntry *
                                pSynceIsSetFsSynceEntry, INT4 i4SetOption)
{

    if (pSynceIsSetFsSynceEntry->bFsSynceContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceContextId, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId,
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId);
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceTraceOption == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceTraceOption, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %u",
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId,
                      pSynceSetFsSynceEntry->MibObject.u4FsSynceTraceOption);
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceQLMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceQLMode, 14, SynceApiLock, SynceApiUnLock,
                      0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId,
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceQLMode);
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceSSMOptionMode, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId,
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceSSMOptionMode);
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceContextRowStatus, 14, SynceApiLock,
                      SynceApiUnLock, 0, 1, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId,
                      pSynceSetFsSynceEntry->MibObject.
                      i4FsSynceContextRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceSetAllFsSynceIfTableTrigger
 Input       :  The Indices
                pSynceSetFsSynceIfEntry
                pSynceIsSetFsSynceIfEntry
 Description :  This Routine is used to send
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
SynceSetAllFsSynceIfTableTrigger (tSynceFsSynceIfEntry *
                                  pSynceSetFsSynceIfEntry,
                                  tSynceIsSetFsSynceIfEntry *
                                  pSynceIsSetFsSynceIfEntry, INT4 i4SetOption)
{

    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfIndex, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex);
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfSynceMode, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode);
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfEsmcMode, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode);
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfPriority, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfPriority);
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfQLValue, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.u4FsSynceIfQLValue);
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfLockoutStatus, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.
                      i4FsSynceIfLockoutStatus);
    }

    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfConfigSwitch, 14, SynceApiLock,
                      SynceApiUnLock, 0, 0, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.
                      i4FsSynceIfConfigSwitch);
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsSynceIfRowStatus, 14, SynceApiLock,
                      SynceApiUnLock, 0, 1, 1, i4SetOption, "%i %i",
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex,
                      pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceFsSynceTableCreateApi
 Input       :  pSynceFsSynceEntry
 Description :  This Routine Creates the memory
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tSynceFsSynceEntry *
SynceFsSynceTableCreateApi (tSynceFsSynceEntry * pSetSynceFsSynceEntry)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    if (pSetSynceFsSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceFsSynceTableCreatApi: pSetSynceFsSynceEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
    if (pSynceFsSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceFsSynceTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pSynceFsSynceEntry, pSetSynceFsSynceEntry,
                sizeof (tSynceFsSynceEntry));
        if (RBTreeAdd
            (gSynceGlobals.glbMib.FsSynceTable,
             (tRBElem *) pSynceFsSynceEntry) != RB_SUCCESS)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceFsSynceTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceFsSynceEntry);
            return NULL;
        }
        return pSynceFsSynceEntry;
    }
}

/****************************************************************************
 Function    :  SynceFsSynceIfTableCreateApi
 Input       :  pSynceFsSynceIfEntry
 Description :  This Routine Creates the memory
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tSynceFsSynceIfEntry *
SynceFsSynceIfTableCreateApi (tSynceFsSynceIfEntry * pSetSynceFsSynceIfEntry)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    if (pSetSynceFsSynceIfEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceFsSynceIfTableCreatApi: pSetSynceFsSynceIfEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceFsSynceIfTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pSynceFsSynceIfEntry, pSetSynceFsSynceIfEntry,
                sizeof (tSynceFsSynceIfEntry));
        if (RBTreeAdd
            (gSynceGlobals.glbMib.FsSynceIfTable,
             (tRBElem *) pSynceFsSynceIfEntry) != RB_SUCCESS)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceFsSynceIfTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceFsSynceIfEntry);
            return NULL;
        }
        return pSynceFsSynceIfEntry;
    }
}
