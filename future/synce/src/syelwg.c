/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syelwg.c,v 1.4 2016/07/06 11:05:10 siva Exp $
*
* Description: This file contains the low level routines
*               for the module Synce
*********************************************************************/

#include "syeinc.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSynceTable
 Input       :  The Indices
                FsSynceContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSynceTable (INT4 *pi4FsSynceContextId)
{

    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry = SynceGetFirstFsSynceTable ();

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsSynceContextId = pSynceFsSynceEntry->MibObject.i4FsSynceContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsSynceIfTable
 Input       :  The Indices
                FsSynceIfIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsSynceIfTable (INT4 *pi4FsSynceIfIndex)
{

    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry = SynceGetFirstFsSynceIfTable ();

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pi4FsSynceIfIndex = pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSynceTable
 Input       :  The Indices
                FsSynceContextId
                nextFsSynceContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsSynceTable (INT4 i4FsSynceContextId,
                             INT4 *pi4NextFsSynceContextId)
{

    tSynceFsSynceEntry  SynceFsSynceEntry;
    tSynceFsSynceEntry *pNextSynceFsSynceEntry = NULL;

    MEMSET (&SynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    /* Assign the index */
    SynceFsSynceEntry.MibObject.i4FsSynceContextId = i4FsSynceContextId;

    pNextSynceFsSynceEntry = SynceGetNextFsSynceTable (&SynceFsSynceEntry);

    if (pNextSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsSynceContextId =
        pNextSynceFsSynceEntry->MibObject.i4FsSynceContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsSynceIfTable
 Input       :  The Indices
                FsSynceIfIndex
                nextFsSynceIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsSynceIfTable (INT4 i4FsSynceIfIndex,
                               INT4 *pi4NextFsSynceIfIndex)
{

    tSynceFsSynceIfEntry SynceFsSynceIfEntry;
    tSynceFsSynceIfEntry *pNextSynceFsSynceIfEntry = NULL;

    MEMSET (&SynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    SynceFsSynceIfEntry.MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    pNextSynceFsSynceIfEntry =
        SynceGetNextFsSynceIfTable (&SynceFsSynceIfEntry);

    if (pNextSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pi4NextFsSynceIfIndex =
        pNextSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceGlobalSysCtrl
 Input       :  The Indices

                The Object
                INT4 *pi4RetValFsSynceGlobalSysCtrl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceGlobalSysCtrl (INT4 *pi4RetValFsSynceGlobalSysCtrl)
{
    if (SynceGetFsSynceGlobalSysCtrl (pi4RetValFsSynceGlobalSysCtrl) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceTraceOption
 Input       :  The Indices
                FsSynceContextId

                The Object
                UINT4 *pu4RetValFsSynceTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceTraceOption (INT4 i4FsSynceContextId,
                          UINT4 *pu4RetValFsSynceTraceOption)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;

    if (SynceGetAllFsSynceTable (pSynceFsSynceEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsSynceTraceOption =
        pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption;

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceQLMode
 Input       :  The Indices
                FsSynceContextId

                The Object
                INT4 *pi4RetValFsSynceQLMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceQLMode (INT4 i4FsSynceContextId, INT4 *pi4RetValFsSynceQLMode)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;

    if (SynceGetAllFsSynceTable (pSynceFsSynceEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceQLMode = pSynceFsSynceEntry->MibObject.i4FsSynceQLMode;

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceQLValue
 Input       :  The Indices
                FsSynceContextId

                The Object
                UINT4 *pu4RetValFsSynceQLValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceQLValue (INT4 i4FsSynceContextId, UINT4 *pu4RetValFsSynceQLValue)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;

    if (SynceGetAllFsSynceTable (pSynceFsSynceEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsSynceQLValue = pSynceFsSynceEntry->MibObject.u4FsSynceQLValue;

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceSSMOptionMode
 Input       :  The Indices
                FsSynceContextId

                The Object 
                INT4 *pi4RetValFsSynceSSMOptionMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceSSMOptionMode (INT4 i4FsSynceContextId,
                            INT4 *pi4RetValFsSynceSSMOptionMode)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;

    if (SynceGetAllFsSynceTable (pSynceFsSynceEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceSSMOptionMode =
        pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode;

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceSelectedInterface
 Input       :  The Indices
                FsSynceContextId

                The Object
                INT4 *pi4RetValFsSynceSelectedInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceSelectedInterface (INT4 i4FsSynceContextId,
                                INT4 *pi4RetValFsSynceSelectedInterface)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;

    if (SynceGetAllFsSynceTable (pSynceFsSynceEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceSelectedInterface =
        pSynceFsSynceEntry->MibObject.i4FsSynceSelectedInterface;

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceContextRowStatus
 Input       :  The Indices
                FsSynceContextId

                The Object
                INT4 *pi4RetValFsSynceContextRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceContextRowStatus (INT4 i4FsSynceContextId,
                               INT4 *pi4RetValFsSynceContextRowStatus)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;

    if (SynceGetAllFsSynceTable (pSynceFsSynceEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceContextRowStatus =
        pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus;

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfSynceMode
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfSynceMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfSynceMode (INT4 i4FsSynceIfIndex,
                          INT4 *pi4RetValFsSynceIfSynceMode)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfSynceMode =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfEsmcMode
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfEsmcMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfEsmcMode (INT4 i4FsSynceIfIndex,
                         INT4 *pi4RetValFsSynceIfEsmcMode)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfEsmcMode =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfPriority
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfPriority (INT4 i4FsSynceIfIndex,
                         INT4 *pi4RetValFsSynceIfPriority)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfPriority =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfQLValue
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfQLValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfQLValue (INT4 i4FsSynceIfIndex, INT4 *pi4RetValFsSynceIfQLValue)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfQLValue =
        (INT4) pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfIsRxQLForced
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfIsRxQLForced
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfIsRxQLForced (INT4 i4FsSynceIfIndex,
                             INT4 *pi4RetValFsSynceIfIsRxQLForced)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfIsRxQLForced =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfLockoutStatus
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfLockoutStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfLockoutStatus (INT4 i4FsSynceIfIndex,
                              INT4 *pi4RetValFsSynceIfLockoutStatus)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfLockoutStatus =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfSignalFail
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfSignalFail
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfSignalFail (INT4 i4FsSynceIfIndex,
                           INT4 *pi4RetValFsSynceIfSignalFail)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfSignalFail =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSignalFail;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfPktsTx
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                UINT4 *pu4RetValFsSynceIfPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfPktsTx (INT4 i4FsSynceIfIndex, UINT4 *pu4RetValFsSynceIfPktsTx)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsSynceIfPktsTx =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsTx;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfPktsRx
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                UINT4 *pu4RetValFsSynceIfPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfPktsRx (INT4 i4FsSynceIfIndex, UINT4 *pu4RetValFsSynceIfPktsRx)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsSynceIfPktsRx =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRx;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfPktsRxDropped
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                UINT4 *pu4RetValFsSynceIfPktsRxDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfPktsRxDropped (INT4 i4FsSynceIfIndex,
                              UINT4 *pu4RetValFsSynceIfPktsRxDropped)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsSynceIfPktsRxDropped =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfPktsRxErrored
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                UINT4 *pu4RetValFsSynceIfPktsRxErrored
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfPktsRxErrored (INT4 i4FsSynceIfIndex,
                              UINT4 *pu4RetValFsSynceIfPktsRxErrored)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsSynceIfPktsRxErrored =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsSynceIfRowStatus
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                INT4 *pi4RetValFsSynceIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsSynceIfRowStatus (INT4 i4FsSynceIfIndex,
                          INT4 *pi4RetValFsSynceIfRowStatus)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfRowStatus =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsSynceIfConfigSwitch
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                retValFsSynceIfConfigSwitch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsSynceIfConfigSwitch(INT4 i4FsSynceIfIndex , 
		                 INT4 *pi4RetValFsSynceIfConfigSwitch)
{

    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;

    if (SynceGetAllFsSynceIfTable (pSynceFsSynceIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsSynceIfConfigSwitch =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch;

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceGlobalSysCtrl
 Input       :  The Indices

                The Object
             :  INT4 i4SetValFsSynceGlobalSysCtrl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceGlobalSysCtrl (INT4 i4SetValFsSynceGlobalSysCtrl)
{
    if (SynceSetFsSynceGlobalSysCtrl (i4SetValFsSynceGlobalSysCtrl) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceTraceOption
 Input       :  The Indices
                FsSynceContextId

                The Object
             :  UINT4 u4SetValFsSynceTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceTraceOption (INT4 i4FsSynceContextId,
                          UINT4 u4SetValFsSynceTraceOption)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption =
        u4SetValFsSynceTraceOption;
    pSynceIsSetFsSynceEntry->bFsSynceTraceOption = OSIX_TRUE;

    if (SynceSetAllFsSynceTable (pSynceFsSynceEntry, pSynceIsSetFsSynceEntry,
                                 OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceQLMode
 Input       :  The Indices
                FsSynceContextId

                The Object
             :  INT4 i4SetValFsSynceQLMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceQLMode (INT4 i4FsSynceContextId, INT4 i4SetValFsSynceQLMode)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.i4FsSynceQLMode = i4SetValFsSynceQLMode;
    pSynceIsSetFsSynceEntry->bFsSynceQLMode = OSIX_TRUE;

    if (SynceSetAllFsSynceTable (pSynceFsSynceEntry, pSynceIsSetFsSynceEntry,
                                 OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceSSMOptionMode
 Input       :  The Indices
                FsSynceContextId

                The Object
             :  INT4 i4SetValFsSynceSSMOptionMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceSSMOptionMode (INT4 i4FsSynceContextId,
                            INT4 i4SetValFsSynceSSMOptionMode)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode =
        i4SetValFsSynceSSMOptionMode;
    pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode = OSIX_TRUE;

    if (SynceSetAllFsSynceTable (pSynceFsSynceEntry, pSynceIsSetFsSynceEntry,
                                 OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceContextRowStatus
 Input       :  The Indices
                FsSynceContextId

                The Object
             :  INT4 i4SetValFsSynceContextRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceContextRowStatus (INT4 i4FsSynceContextId,
                               INT4 i4SetValFsSynceContextRowStatus)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
        i4SetValFsSynceContextRowStatus;
    pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus = OSIX_TRUE;

    if (SynceSetAllFsSynceTable (pSynceFsSynceEntry, pSynceIsSetFsSynceEntry,
                                 OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceIfSynceMode
 Input       :  The Indices
                FsSynceIfIndex

                The Object
             :  INT4 i4SetValFsSynceIfSynceMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfSynceMode (INT4 i4FsSynceIfIndex,
                          INT4 i4SetValFsSynceIfSynceMode)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode =
        i4SetValFsSynceIfSynceMode;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode = OSIX_TRUE;

    if (SynceSetAllFsSynceIfTable
        (pSynceFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceIfEsmcMode
 Input       :  The Indices
                FsSynceIfIndex

                The Object
             :  INT4 i4SetValFsSynceIfEsmcMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfEsmcMode (INT4 i4FsSynceIfIndex, INT4 i4SetValFsSynceIfEsmcMode)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode =
        i4SetValFsSynceIfEsmcMode;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode = OSIX_TRUE;

    if (SynceSetAllFsSynceIfTable
        (pSynceFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceIfPriority
 Input       :  The Indices
                FsSynceIfIndex

                The Object
             :  INT4 i4SetValFsSynceIfPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfPriority (INT4 i4FsSynceIfIndex, INT4 i4SetValFsSynceIfPriority)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority =
        i4SetValFsSynceIfPriority;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority = OSIX_TRUE;

    if (SynceSetAllFsSynceIfTable
        (pSynceFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceIfQLValue
 Input       :  The Indices
                FsSynceIfIndex

                The Object
             :  INT4 i4SetValFsSynceIfQLValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfQLValue (INT4 i4FsSynceIfIndex, INT4 i4SetValFsSynceIfQLValue)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue =
        (UINT4) i4SetValFsSynceIfQLValue;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue = OSIX_TRUE;

    if (SynceSetAllFsSynceIfTable
        (pSynceFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceIfLockoutStatus
 Input       :  The Indices
                FsSynceIfIndex

                The Object
             :  INT4 i4SetValFsSynceIfLockoutStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfLockoutStatus (INT4 i4FsSynceIfIndex,
                              INT4 i4SetValFsSynceIfLockoutStatus)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus =
        i4SetValFsSynceIfLockoutStatus;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus = OSIX_TRUE;

    if (SynceSetAllFsSynceIfTable
        (pSynceFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsSynceIfRowStatus
 Input       :  The Indices
                FsSynceIfIndex

                The Object
             :  INT4 i4SetValFsSynceIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfRowStatus (INT4 i4FsSynceIfIndex,
                          INT4 i4SetValFsSynceIfRowStatus)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
        i4SetValFsSynceIfRowStatus;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_TRUE;

    if (SynceSetAllFsSynceIfTable
        (pSynceFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsSynceIfConfigSwitch
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                setValFsSynceIfConfigSwitch
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsSynceIfConfigSwitch(INT4 i4FsSynceIfIndex , 
		                 INT4 i4SetValFsSynceIfConfigSwitch)
{

    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch =
        i4SetValFsSynceIfConfigSwitch;

    pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch = OSIX_TRUE;

    if (SynceSetAllFsSynceIfTable
        (pSynceFsSynceIfEntry, pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSynceIfPktsTx
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                setValFsSynceIfPktsTx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfPktsTx (INT4 i4FsSynceIfIndex, UINT4 u4SetValFsSynceIfPktsTx)
{
    if (SynceSetFsSynceIfCntrs (i4FsSynceIfIndex, u4SetValFsSynceIfPktsTx,
                                SYNCE_CNTR_TX) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSynceIfPktsRx
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                setValFsSynceIfPktsRx
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfPktsRx (INT4 i4FsSynceIfIndex, UINT4 u4SetValFsSynceIfPktsRx)
{
    if (SynceSetFsSynceIfCntrs (i4FsSynceIfIndex, u4SetValFsSynceIfPktsRx,
                                SYNCE_CNTR_RX) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSynceIfPktsRxDropped
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                setValFsSynceIfPktsRxDropped
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfPktsRxDropped (INT4 i4FsSynceIfIndex,
                              UINT4 u4SetValFsSynceIfPktsRxDropped)
{
    if (SynceSetFsSynceIfCntrs (i4FsSynceIfIndex,
                                u4SetValFsSynceIfPktsRxDropped,
                                SYNCE_CNTR_DROPPED) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsSynceIfPktsRxErrored
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                setValFsSynceIfPktsRxErrored
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsSynceIfPktsRxErrored (INT4 i4FsSynceIfIndex,
                              UINT4 u4SetValFsSynceIfPktsRxErrored)
{
    if (SynceSetFsSynceIfCntrs (i4FsSynceIfIndex,
                                u4SetValFsSynceIfPktsRxErrored,
                                SYNCE_CNTR_ERRORED) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceGlobalSysCtrl
 Input       :  The Indices

                The Object
                testValFsSynceGlobalSysCtrl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceGlobalSysCtrl (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsSynceGlobalSysCtrl)
{
    if (SynceTestFsSynceGlobalSysCtrl
        (pu4ErrorCode, i4TestValFsSynceGlobalSysCtrl) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceTraceOption
 Input       :  The Indices
                FsSynceContextId

                The Object
                testValFsSynceTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceTraceOption (UINT4 *pu4ErrorCode, INT4 i4FsSynceContextId,
                             UINT4 u4TestValFsSynceTraceOption)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption =
        u4TestValFsSynceTraceOption;
    pSynceIsSetFsSynceEntry->bFsSynceTraceOption = OSIX_TRUE;

    if (SynceTestAllFsSynceTable (pu4ErrorCode, pSynceFsSynceEntry,
                                  pSynceIsSetFsSynceEntry, OSIX_FALSE,
                                  OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceQLMode
 Input       :  The Indices
                FsSynceContextId

                The Object
                testValFsSynceQLMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceQLMode (UINT4 *pu4ErrorCode, INT4 i4FsSynceContextId,
                        INT4 i4TestValFsSynceQLMode)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.i4FsSynceQLMode = i4TestValFsSynceQLMode;
    pSynceIsSetFsSynceEntry->bFsSynceQLMode = OSIX_TRUE;

    if (SynceTestAllFsSynceTable (pu4ErrorCode, pSynceFsSynceEntry,
                                  pSynceIsSetFsSynceEntry, OSIX_FALSE,
                                  OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceSSMOptionMode
 Input       :  The Indices
                FsSynceContextId

                The Object
                testValFsSynceSSMOptionMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceSSMOptionMode (UINT4 *pu4ErrorCode, INT4 i4FsSynceContextId,
                               INT4 i4TestValFsSynceSSMOptionMode)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode =
        i4TestValFsSynceSSMOptionMode;
    pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode = OSIX_TRUE;

    if (SynceTestAllFsSynceTable (pu4ErrorCode, pSynceFsSynceEntry,
                                  pSynceIsSetFsSynceEntry, OSIX_FALSE,
                                  OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceContextRowStatus
 Input       :  The Indices
                FsSynceContextId

                The Object
                testValFsSynceContextRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceContextRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsSynceContextId,
                                  INT4 i4TestValFsSynceContextRowStatus)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceIsSetFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);

    if (pSynceFsSynceEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Assign the index */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextId = i4FsSynceContextId;
    pSynceIsSetFsSynceEntry->bFsSynceContextId = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
        i4TestValFsSynceContextRowStatus;
    pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus = OSIX_TRUE;

    if (SynceTestAllFsSynceTable (pu4ErrorCode, pSynceFsSynceEntry,
                                  pSynceIsSetFsSynceEntry, OSIX_FALSE,
                                  OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfSynceMode
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                testValFsSynceIfSynceMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfSynceMode (UINT4 *pu4ErrorCode, INT4 i4FsSynceIfIndex,
                             INT4 i4TestValFsSynceIfSynceMode)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode =
        i4TestValFsSynceIfSynceMode;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode = OSIX_TRUE;

    if (SynceTestAllFsSynceIfTable (pu4ErrorCode, pSynceFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfEsmcMode
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                testValFsSynceIfEsmcMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfEsmcMode (UINT4 *pu4ErrorCode, INT4 i4FsSynceIfIndex,
                            INT4 i4TestValFsSynceIfEsmcMode)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode =
        i4TestValFsSynceIfEsmcMode;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode = OSIX_TRUE;

    if (SynceTestAllFsSynceIfTable (pu4ErrorCode, pSynceFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfPriority
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                testValFsSynceIfPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfPriority (UINT4 *pu4ErrorCode, INT4 i4FsSynceIfIndex,
                            INT4 i4TestValFsSynceIfPriority)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority =
        i4TestValFsSynceIfPriority;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority = OSIX_TRUE;

    if (SynceTestAllFsSynceIfTable (pu4ErrorCode, pSynceFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfQLValue
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                testValFsSynceIfQLValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfQLValue (UINT4 *pu4ErrorCode, INT4 i4FsSynceIfIndex,
                           INT4 i4TestValFsSynceIfQLValue)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue =
        (UINT4) i4TestValFsSynceIfQLValue;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue = OSIX_TRUE;

    if (SynceTestAllFsSynceIfTable (pu4ErrorCode, pSynceFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfLockoutStatus
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                testValFsSynceIfLockoutStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfLockoutStatus (UINT4 *pu4ErrorCode, INT4 i4FsSynceIfIndex,
                                 INT4 i4TestValFsSynceIfLockoutStatus)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus =
        i4TestValFsSynceIfLockoutStatus;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus = OSIX_TRUE;

    if (SynceTestAllFsSynceIfTable (pu4ErrorCode, pSynceFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfRowStatus
 Input       :  The Indices
                FsSynceIfIndex

                The Object
                testValFsSynceIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsSynceIfIndex,
                             INT4 i4TestValFsSynceIfRowStatus)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
        i4TestValFsSynceIfRowStatus;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_TRUE;

    if (SynceTestAllFsSynceIfTable (pu4ErrorCode, pSynceFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfConfigSwitch
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                testValFsSynceIfConfigSwitch
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsSynceIfConfigSwitch(UINT4 *pu4ErrorCode , 
		                    INT4 i4FsSynceIfIndex , 
				    INT4 i4TestValFsSynceIfConfigSwitch)
{

    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceIsSetFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);

    if (pSynceFsSynceIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pSynceIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);

    if (pSynceIsSetFsSynceIfEntry == NULL)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceIsSetFsSynceIfEntry, 0, sizeof (tSynceIsSetFsSynceIfEntry));

    /* Assign the index */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex = i4FsSynceIfIndex;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex = OSIX_TRUE;

    /* Assign the value */
    pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch =
                                i4TestValFsSynceIfConfigSwitch;
    pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch = OSIX_TRUE;

    if (SynceTestAllFsSynceIfTable (pu4ErrorCode, pSynceFsSynceIfEntry,
                                    pSynceIsSetFsSynceIfEntry, OSIX_FALSE,
                                    OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceIsSetFsSynceIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceIsSetFsSynceIfEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfPktsTx
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                testValFsSynceIfPktsTx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfPktsTx (UINT4 *pu4ErrorCode,
                          INT4 i4FsSynceIfIndex, UINT4 u4TestValFsSynceIfPktsTx)
{
    if (SynceTestFsSynceIfCntrs (pu4ErrorCode, i4FsSynceIfIndex,
                                 u4TestValFsSynceIfPktsTx) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfPktsRx
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                testValFsSynceIfPktsRx
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfPktsRx (UINT4 *pu4ErrorCode,
                          INT4 i4FsSynceIfIndex, UINT4 u4TestValFsSynceIfPktsRx)
{
    if (SynceTestFsSynceIfCntrs (pu4ErrorCode, i4FsSynceIfIndex,
                                 u4TestValFsSynceIfPktsRx) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfPktsRxDropped
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                testValFsSynceIfPktsRxDropped
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfPktsRxDropped (UINT4 *pu4ErrorCode,
                                 INT4 i4FsSynceIfIndex,
                                 UINT4 u4TestValFsSynceIfPktsRxDropped)
{
    if (SynceTestFsSynceIfCntrs (pu4ErrorCode, i4FsSynceIfIndex,
                                 u4TestValFsSynceIfPktsRxDropped) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsSynceIfPktsRxErrored
 Input       :  The Indices
                FsSynceIfIndex

                The Object 
                testValFsSynceIfPktsRxErrored
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsSynceIfPktsRxErrored (UINT4 *pu4ErrorCode,
                                 INT4 i4FsSynceIfIndex,
                                 UINT4 u4TestValFsSynceIfPktsRxErrored)
{

    if (SynceTestFsSynceIfCntrs (pu4ErrorCode, i4FsSynceIfIndex,
                                 u4TestValFsSynceIfPktsRxErrored) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSynceGlobalSysCtrl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsSynceGlobalSysCtrl (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSynceTable
 Input       :  The Indices
                FsSynceContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSynceTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsSynceIfTable
 Input       :  The Indices
                FsSynceIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsSynceIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
