/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: syeque.c,v 1.1 2013/03/23 12:41:37 siva Exp $
 *
 * Description : This file contains the processing of
 *               messages queued
 *****************************************************************************/
#include "syeinc.h"

/****************************************************************************
*                                                                           *
* Function     : SynceQueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
SynceQueProcessMsgs ()
{
    tSynceQMsg        *pSynceQueMsg = NULL;

    SYNCE_FN_ENTRY ();

    while (OsixQueRecv(gSynceGlobals.taskQueId,
                       (UINT1 *) &pSynceQueMsg, OSIX_DEF_MSG_LEN, 0)
            == OSIX_SUCCESS)
    {
        SYNCE_TRC((SYNCE_CONTROL_PLANE_TRC, "Processing Synce Que Msg\n"));

        if(SYNCE_SELECT_CONTEXT(pSynceQueMsg->u4ContextId) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (gSynceGlobals.msgQPoolId, 
                                (UINT1 *) pSynceQueMsg);
            return;
        }

        switch (pSynceQueMsg->u4MsgType)
        {
            case SYNCE_IF_PKT_RCV:
                 SyncePktHandleRxPacket(pSynceQueMsg);
                 break;
            case SYNCE_IF_OPER_ST_CHG:
                 SynceProtIfStatusChangeHandler (pSynceQueMsg->u4IfIndex,
                                            pSynceQueMsg->u1IfOperStatus);
                 break;
            case SYNCE_IF_DELETE:
                 SynceProtIfDeletionHandler (pSynceQueMsg->u4IfIndex);
                 break;
            default:
                 /* unknown msg type */ 
                 break;
        }

        SYNCE_RELEASE_CONTEXT();

        MemReleaseMemBlock (gSynceGlobals.msgQPoolId, (UINT1 *) pSynceQueMsg);
    }

    SYNCE_FN_EXIT ();
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  synceque.c                      */
/*-----------------------------------------------------------------------*/
