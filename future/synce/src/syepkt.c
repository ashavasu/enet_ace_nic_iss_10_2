/******************************************************************************
 * Copyright (C) 2013  Aricent Inc . All Rights Reserved 
 *
 * $Id: syepkt.c,v 1.8 2015/06/15 06:50:48 siva Exp $ 
 *
 * Description : This file contains the definitions of functions related to
 *               packet handler sub module.
 ******************************************************************************/

#include "syeinc.h"

/* global declarations */
const               tSynceSsmCodeInfo
    gaSynceSsmCodeTable[SYNCE_SSM_MAX_CODE + 1][SYNCE_SSM_OPTION_MAX] = {
    /*ssm code = 0 */
    {
     {(UINT1 *) "QL-INV0", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-STU", SYNCE_QL_STU, 1, {0}},
     {(UINT1 *) "QL-STU", SYNCE_QL_STU, 1, {0}},
     },
    /*ssm code = 1 */
    {
     {(UINT1 *) "QL-INV1", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-PRS", SYNCE_QL_PRS, 1, {0}},
     {(UINT1 *) "QL-PRS", SYNCE_QL_PRS, 1, {0}},
     },
    /*ssm code = 2 */
    {
     {(UINT1 *) "QL-PRC", SYNCE_QL_PRC, 1, {0}},
     {(UINT1 *) "QL-INV2", SYNCE_QL_DUS, 0, {0}},
     {(UINT1 *) "QL-INV2", SYNCE_QL_DUS, 0, {0}},
     },
    /*ssm code = 3 */
    {
     {(UINT1 *) "QL-INV3", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-INV3", SYNCE_QL_DUS, 0, {0}},
     {(UINT1 *) "QL-INV3", SYNCE_QL_DUS, 0, {0}},
     },
    /*ssm code = 4 */
    {
     {(UINT1 *) "QL-SSU-A", SYNCE_QL_SSU_A, 1, {0}},
     {(UINT1 *) "QL-ST3", SYNCE_QL_ST3, 1, {0}},
     {(UINT1 *) "QL-TNC", SYNCE_QL_TNC, 1, {0}},
     },
    /*ssm code = 5 */
    {
     {(UINT1 *) "QL-INV5", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-INV5", SYNCE_QL_DUS, 0, {0}},
     {(UINT1 *) "QL-INV5", SYNCE_QL_DUS, 0, {0}},
     },
    /*ssm code = 6 */
    {
     {(UINT1 *) "QL-INV6", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-INV6", SYNCE_QL_DUS, 0, {0}},
     {(UINT1 *) "QL-INV6", SYNCE_QL_DUS, 0, {0}},
     },
    /*ssm code = 7 */
    {
     {(UINT1 *) "QL-INV7", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-ST2", SYNCE_QL_ST2, 1, {0}},
     {(UINT1 *) "QL-ST2", SYNCE_QL_ST2, 1, {0}},
     },
    /*ssm code = 8 */
    {
     {(UINT1 *) "QL-SSU-B", SYNCE_QL_SSU_B, 1, {0}},
     {(UINT1 *) "QL-INV8", SYNCE_QL_DUS, 0, {0}},
     {(UINT1 *) "QL-INV8", SYNCE_QL_DUS, 0, {0}},
     },
    /*ssm code = 9 */
    {
     {(UINT1 *) "QL-INV9", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-INV9", SYNCE_QL_DUS, 0, {0}},
     {(UINT1 *) "QL-INV9", SYNCE_QL_DUS, 0, {0}},
     },
    /*ssm code = 10 */
    {
     {(UINT1 *) "QL-INV10", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-ST3", SYNCE_QL_ST3, 1, {0}},
     {(UINT1 *) "QL-ST3", SYNCE_QL_ST3, 1, {0}},
     },
    /*ssm code = 11 */
    {
     {(UINT1 *) "QL-SEC", SYNCE_QL_SEC, 1, {0}},
     {(UINT1 *) "QL-INV11", SYNCE_QL_DUS, 0, {0}},
     {(UINT1 *) "QL-INV11", SYNCE_QL_DUS, 0, {0}},
     },
    /*ssm code = 12 */
    {
     {(UINT1 *) "QL-INV12", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-SMC", SYNCE_QL_SMC, 1, {0}},
     {(UINT1 *) "QL-SMC", SYNCE_QL_SMC, 1, {0}},
     },
    /*ssm code = 13 */
    {
     {(UINT1 *) "QL-INV13", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-ST3", SYNCE_QL_ST3, 1, {0}},
     {(UINT1 *) "QL-ST3E", SYNCE_QL_ST3E, 1, {0}},
     },
    /*ssm code = 14 */
    {
     {(UINT1 *) "QL-INV14", SYNCE_QL_DNU, 0, {0}},
     {(UINT1 *) "QL-RES", SYNCE_QL_RES, 1, {0}},
     {(UINT1 *) "QL-PROV", SYNCE_QL_PROV, 1, {0}},
     },
    /*ssm code = 15 */
    {
     {(UINT1 *) "QL-DNU", SYNCE_QL_DNU, 1, {0}},
     {(UINT1 *) "QL-DUS", SYNCE_QL_DUS, 1, {0}},
     {(UINT1 *) "QL-DUS", SYNCE_QL_DUS, 1, {0}},
     }
};

UINT1               gau1SynceEsmcPduDestMacAddr[SYNCE_ENET_ADDR_LEN] =
    { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };

/****************************************************************************
 * Function    :  SyncePktHandleRxPacket

 * Description :  This routine is used to handle the received
 *                SSM packet.It reads the Rx packet from the
 *                queue and calls subsequent functions to validate
 *                the packet.

 * Input       :  *pSynceQMsg - Message Queue containing the packet.

 * Output      :  None

 * Returns     :  None
****************************************************************************/

VOID
SyncePktHandleRxPacket (tSynceQMsg * pSynceQMsg)
{
    tSyncePduParams     syncePduParams;
    UINT1              *pu1RxPdu = NULL;
    UINT1               u1SsmCode = 0;
    UINT1               u1Error = 0;
    UINT1               u1QlCode = 0;
    tSynceFsSynceIfEntry *pSynceIfEntry = NULL;
    tCfaIfInfo           pCfaIfInfo;
   
    MEMSET(&pCfaIfInfo, 0, sizeof(tCfaIfInfo));

    SYNCE_FN_ENTRY ();

    if (pSynceQMsg == NULL)
    {
        SYNCE_FN_EXIT ();
        return;
    }

    MEMSET (&syncePduParams, 0, sizeof (tSyncePduParams));

    pSynceIfEntry = SynceGetFsSynceIfEntry (pSynceQMsg->u4IfIndex);
    if (pSynceIfEntry == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pSynceQMsg->uSynceMsgParam.pSyncePdu,
                                     OSIX_FALSE);
        SYNCE_FN_EXIT ();
        return;
    }
    else if (pSynceIfEntry->MibObject.i4FsSynceIfEsmcMode != SYNCE_ESMC_MODE_RX)
    {
        CRU_BUF_Release_MsgBufChain (pSynceQMsg->uSynceMsgParam.pSyncePdu,
                                     OSIX_FALSE);
        pSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped++;
        SYNCE_FN_EXIT ();
        return;
    }
   
    /* If the interface is shutdown drop the synce packet received on 
     * that interface */
    if (CfaGetIfInfo (pSynceQMsg->u4IfIndex, &pCfaIfInfo) == CFA_SUCCESS)
    {
	    if (pCfaIfInfo.u1IfOperStatus != CFA_IF_UP)
	    {
		    CRU_BUF_Release_MsgBufChain (pSynceQMsg->uSynceMsgParam.pSyncePdu,
				    OSIX_FALSE);
		    pSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped++;
		    SYNCE_FN_EXIT ();
		    return;
	    }
    }
    if ((pSynceQMsg->u4PktLen < SYNCE_ESMC_MIN_PDU_SIZE)
        || (pSynceQMsg->u4PktLen > SYNCE_ESMC_MAX_PDU_SIZE))
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC, "Packet"
                    "length greater than max ESMC PDU size\r\n"));
        pSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped++;
        CRU_BUF_Release_MsgBufChain (pSynceQMsg->uSynceMsgParam.pSyncePdu,
                                     OSIX_FALSE);
        SYNCE_FN_EXIT ();
        return;
    }

    syncePduParams.pSynceIfEntry = pSynceIfEntry;
    syncePduParams.pu1PduBuf = MemAllocMemBlk (SYNCE_ESMC_PKT_POOLID);

    pu1RxPdu = syncePduParams.pu1PduBuf;
    if (pu1RxPdu == NULL)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_BUFFER_TRC, "Memory "
                    "allocation for Packet buffer failed\r\n"));
        CRU_BUF_Release_MsgBufChain (pSynceQMsg->uSynceMsgParam.pSyncePdu,
                                     OSIX_FALSE);
        SYNCE_FN_EXIT ();
        return;
    }

    CRU_BUF_Copy_FromBufChain (pSynceQMsg->uSynceMsgParam.pSyncePdu,
                               syncePduParams.pu1PduBuf, 0,
                               pSynceQMsg->u4PktLen);

    CRU_BUF_Release_MsgBufChain (pSynceQMsg->uSynceMsgParam.pSyncePdu,
                                 OSIX_FALSE);

    syncePduParams.u4PduLen = pSynceQMsg->u4PktLen;

    SyncePktDump (syncePduParams.pu1PduBuf, (UINT2) syncePduParams.u4PduLen);

    if (SyncePktProcessEsmcPdu (&syncePduParams, &u1SsmCode, &u1Error)
        == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Couldn't parse Rx Pdu\r\n"));
        MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1RxPdu);
        SYNCE_FN_EXIT ();
        return;
    }
    else
    {
        if (SyncePktIsSsmAcceptable (pSynceQMsg->u4IfIndex, u1SsmCode) ==
            OSIX_TRUE)
        {
            if (SynceUtilGetQlCodeFromSsmCode (pSynceQMsg->u4ContextId,
                                               u1SsmCode,
                                               &u1QlCode) == OSIX_SUCCESS)
            {
                /* packet was accepted increase the counter */
                ++(pSynceIfEntry->MibObject.u4FsSynceIfPktsRx);

                SynceProtIfQLSet (pSynceQMsg->u4IfIndex, u1QlCode,
                                  SYNCE_QL_SOURCE_PDU);
            }
            else
            {
                ++(pSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped);
            }
        }
        else
        {
            SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                        "Unknown SSM Code received in ESMC PDU\r\n"));
            pSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped++;
        }
    }

    MemReleaseMemBlock (SYNCE_ESMC_PKT_POOLID, pu1RxPdu);

    SYNCE_FN_EXIT ();
    return;
}

/* ESMC PKT Format */
/*
   Octet  Size Field
    1-6     6   octets Destination address =01-80-C2-00-00-02
    7-12    6   octets Source address
    13-14   2   octets Slow protocol Ethertype = 88-09
    15      1   octet Slow protocol subtype = 0x0A
    16-18   3   octets ITU-OUI = 00-19-A7
    19-20   2   octets ITU-T subtype
    21      4   bits Version
            1   bit Event flag
            3   bits Reserved
    22-24   3   octets Reserved
    25-1514 36-1490 octets Data and padding (see point j)
    Last 4  4   octets Frame check sequence
*/

/* QL TLV Format */
/*
    8 bits  Type: 0x01
    16 bits Length: 00-04
    4 bits  0x0 (unused)
    4 bits  SSM code
 */

/****************************************************************************
 * Function    :  SyncePktProcessEsmcPdu
 * Description :  This routine is used to validate the received
 *                SSM packet for its correctness.
 * Input       :  *esmcPduParams - Received Packet.
 * Output      :  *pu1SsmCode - SSM Code
 *                *pu1Error   - Error number
 * Returns     :  None
****************************************************************************/

INT4
SyncePktProcessEsmcPdu (tSyncePduParams * esmcPduParams,
                        UINT1 *pu1SsmCode, UINT1 *pu1Error)
{
    UINT1              *pu1RxPdu = NULL;
    UINT2               u2PacketData = 0;
    UINT1               u1PacketData = 0;

    SYNCE_FN_ENTRY ();

    if (pu1SsmCode == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (pu1Error == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    *pu1SsmCode = 0;
    *pu1Error = 0;

    if (esmcPduParams == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pu1RxPdu = esmcPduParams->pu1PduBuf;
    if (pu1RxPdu == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (SyncePktValidateEsmcPdu (pu1RxPdu, (UINT2) esmcPduParams->u4PduLen) !=
        OSIX_SUCCESS)
    {
	esmcPduParams->pSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored++;
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "ESMC Packet Validation Failed\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /*skipping mac headers */
    pu1RxPdu += SYNCE_ESMC_PDU_VER_OFFSET;

    /* check version and event flag */
    u1PacketData = *pu1RxPdu++;
    if (((u1PacketData >> SYNCE_PKT_VERSION_BITS) & 0xf) != 1)
    {
        SYNCE_FN_EXIT ();
        esmcPduParams->pSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored++;
        return OSIX_FAILURE;
    }

    SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "Version : %d\n",
                     ((u1PacketData >> SYNCE_PKT_VERSION_BITS) & 0xf)));

    if ((u1PacketData & 0x08) == 0x8)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "ESMC Event PDU Received\n"));
    }
    else
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC,
                    "ESMC Information PDU Received\n"));
    }

    /* skipping reserved bytes */
    pu1RxPdu += SYNCE_PKT_RESERVED_BYTES;

    /** QL TLV Parsing */
    u1PacketData = *pu1RxPdu++;
    /* type */
    if (u1PacketData != SYNCE_PKT_TYPE)
    {
        SYNCE_FN_EXIT ();
        esmcPduParams->pSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored++;
        return OSIX_FAILURE;
    }

    SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "TLV Type : %d\n", u1PacketData));

    /* length */
    SYNCE_READ_2_BYTES (pu1RxPdu, u2PacketData);
    if (u2PacketData != SYNCE_PKT_TLV_LENGTH)
    {
        SYNCE_FN_EXIT ();
        esmcPduParams->pSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored++;
        return OSIX_FAILURE;
    }
    /* value */
    u1PacketData = *pu1RxPdu++;
    *pu1SsmCode = u1PacketData;

    SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "SSM Code : %d\n", u1PacketData));

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  SyncePktIsSsmAcceptable
 * Description :  This routine checks if the received ssm code is acceptable
 *                in the current selected option mode.
 * Input       :  u4IfIndex - interface index
 *                u1SsmCode - ssm code
 * Output      :  None
 * Returns     :  OSIX_TRUE/OSIX_FALSE
****************************************************************************/

INT4
SyncePktIsSsmAcceptable (UINT4 u4IfIndex, UINT1 u1SsmCode)
{
    UINT1               u1SsmOptionMode = 0;
    tSynceFsSynceEntry *pSynceEntry = NULL;

    SYNCE_FN_ENTRY ();

    if (SynceUtilGetSynceEntryFromIfIdx (u4IfIndex, &pSynceEntry)
        == OSIX_FAILURE)
    {
        return OSIX_FALSE;
    }

    u1SsmOptionMode = (UINT1) pSynceEntry->MibObject.i4FsSynceSSMOptionMode;

    SYNCE_FN_EXIT ();

    return gaSynceSsmCodeTable[u1SsmCode][u1SsmOptionMode - 1].b1IsValid;
}

/****************************************************************************
 * Function    :  SyncePktConstructEsmcPdu

 * Description :  This routine construct and return the ESMC PDU based on the
 *                 input parameter.

 * Input       :  *pSsmParams - structure containing the ssm code and type
 *                              of ESMC PDU to be constructed.
 *                *pu4BufferLen - Length of Packet

 * Output      :  pu1Buffer

 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/

INT4
SyncePktConstructEsmcPdu (UINT1 *pu1Buffer,
                          UINT2 *pu4BufferLen, tSynceSsmParams * pSsmParams)
{
    UINT1              *pu1Pkt = NULL;
    UINT1               u1ActualDate = 0;
    UINT1               u1Padding = 0;

    SYNCE_FN_ENTRY ();

    if (pu1Buffer == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (pu4BufferLen == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (pSsmParams == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (*pu4BufferLen < SYNCE_ESMC_MIN_PDU_SIZE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pu1Buffer, 0, SYNCE_ESMC_MIN_PDU_SIZE);

    pu1Pkt = pu1Buffer;
    /*destination address */

    SYNCE_WRITE_N_BYTES (pu1Pkt, gau1SynceEsmcPduDestMacAddr,
                         sizeof (gau1SynceEsmcPduDestMacAddr));
    /* source address */
    pu1Pkt += sizeof (gau1SynceEsmcPduDestMacAddr);
    /* ether type */
    SYNCE_WRITE_2_BYTES (pu1Pkt, SYNCE_ESMC_ETHER_TYPE);
    /* sub type */
    *pu1Pkt = SYNCE_SLOW_PROTOCOL_TYPE;
    pu1Pkt += 1;
    /* ITU-OUI */
    pu1Pkt += 1;
    SYNCE_WRITE_2_BYTES (pu1Pkt, SYNCE_ESMC_ITU_OUI);
    /* ITU-T sub type */
    SYNCE_WRITE_2_BYTES (pu1Pkt, SYNCE_ESMC_ITU_SUBTYPE);
    /* version and event flag */
    *pu1Pkt = (*pu1Pkt | (0x1 << SYNCE_PKT_VERSION_BITS));
    if (pSsmParams->u1EventFlag)
    {
        *pu1Pkt = (*pu1Pkt | 0x8);
    }
    pu1Pkt += 1;
    /* reserved bytes */
    pu1Pkt += SYNCE_PKT_RESERVED_BYTES;

    /*QL TLV */
    *pu1Pkt++ = 0x1;
    SYNCE_WRITE_2_BYTES (pu1Pkt, SYNCE_PKT_TLV_LENGTH);
    *pu1Pkt = (UINT1) (*pu1Pkt | (pSsmParams->u1SsmCode & 0xf));
    pu1Pkt++;

    /* updating how may bytes have been copied to the buffer */
    u1ActualDate = (UINT1) (pu1Pkt - pu1Buffer);
    u1Padding = (UINT1) (SYNCE_ESMC_MIN_PDU_SIZE - u1ActualDate);

    *pu4BufferLen =
        (UINT2) ((u1Padding > 0) ? SYNCE_ESMC_MIN_PDU_SIZE : u1ActualDate);

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  SyncePktDetectEsmcPdu
 * Description :  This function checks if the given packet buffer contains the
 *                destination MAC as ESMC mutlicast address and Ether type is
 *                ESMC
 * Input       :  *pu1Pdu - buffer containing the packet.
 *                 u2PduLen - length of the buffer
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/

INT4
SyncePktDetectEsmcPdu (UINT1 *pu1Pdu, UINT2 u2PduLen)
{
    UINT1              *pu1RxPdu = NULL;
    UINT2               u2Data = 0;

    SYNCE_FN_ENTRY ();

    if (pu1Pdu == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (u2PduLen < SYNCE_ESMC_MIN_PDU_SIZE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pu1RxPdu = pu1Pdu;
    /* Compare header */
    if (MEMCMP (pu1RxPdu, gau1SynceEsmcPduDestMacAddr, SYNCE_ENET_ADDR_LEN) !=
        0)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }
    pu1RxPdu += SYNCE_ETH_HEADER_MAC_BYTES;

    SYNCE_READ_2_BYTES (pu1RxPdu, u2Data);
    if (u2Data != SYNCE_ESMC_ETHER_TYPE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "Ether Type : 0x%x\n", u2Data));

    /* check slow protocol subtype */
    if (*pu1RxPdu++ != SYNCE_SLOW_PROTOCOL_TYPE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  SyncePktValidateEsmcPdu
 * Description :  This routine validates if the given buffer contains ESMC
 *                PDU or not.
 * Input       :  *pu1Pdu - buffer containing the PDU
                  u2PduLen - length of PDU
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
****************************************************************************/

INT4
SyncePktValidateEsmcPdu (UINT1 *pu1Pdu, UINT2 u2PduLen)
{
    UINT1              *pu1RxPdu = NULL;
    UINT2               u2Data = 0;

    SYNCE_FN_ENTRY ();
    pu1RxPdu = pu1Pdu;
    if (SyncePktDetectEsmcPdu (pu1RxPdu, u2PduLen) == OSIX_FAILURE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /*advance the pointer to the correct position */
    pu1RxPdu += 15;

    /* check ITU OUI */
    if (*pu1RxPdu++ != 0x0)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* check ether type */
    SYNCE_READ_2_BYTES (pu1RxPdu, u2Data);
    if (u2Data != SYNCE_ESMC_ITU_OUI)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "ITU-OUI : 0x%x\n", u2Data));

    /* check ITU-T subtype */
    SYNCE_READ_2_BYTES (pu1RxPdu, u2Data);
    if (u2Data != SYNCE_ESMC_ITU_SUBTYPE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "ITU SubType : 0x%x\n", u2Data));

    SYNCE_FN_EXIT ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  SyncePktDump
 *
 * Description :  This routine is used to dump the SyncE packet
 *
 * Input       :  *pu1Buf - packet buffer
 *                u2Len - length of buffer
 *
 * Output      :  None
 *
 * Returns     :  None
****************************************************************************/

VOID
SyncePktDump (UINT1 *pu1Buf, UINT2 u2Len)
{
    FS_ULONG            addr = 0;
    UINT1              *pu1TmpPtr = NULL;
    UINT1               u1Iterator = 0;

    if (pu1Buf == NULL)
    {
        return;
    }

    pu1TmpPtr = pu1Buf;

    while (u2Len > 0)
    {
        SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "%4ld 0x%08lx  ",
                         (int) addr, pu1TmpPtr));
        addr = addr + 16;

        for (u1Iterator = 0; ((u1Iterator < 16) && (u2Len > 0)); u1Iterator++)
        {
            SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "%02x ", *pu1TmpPtr++));
            --u2Len;
        }

        SYNCE_TRC_DUMP ((SYNCE_DUMP_TRC, "\n"));
    }
}
