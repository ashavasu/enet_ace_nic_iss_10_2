/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syedbg.c,v 1.4 2016/07/06 11:05:10 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Synce
*********************************************************************/

#include "syeinc.h"

/****************************************************************************
 Function    :  SynceGetAllFsSynceTable
 Input       :  pSynceGetFsSynceEntry
 Description :  This Routine will fetch value
                of all fields in SyncE table
                corresponding to the given indices.
 Output      :  pSynceGetFsSynceEntry - structure containing context
                related information
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceGetAllFsSynceTable (tSynceFsSynceEntry * pSynceGetFsSynceEntry)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    /* Check whether the node is already present */
    pSynceFsSynceEntry =
        RBTreeGet (gSynceGlobals.glbMib.FsSynceTable,
                   (tRBElem *) pSynceGetFsSynceEntry);

    if (pSynceFsSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pSynceGetFsSynceEntry->MibObject.u4FsSynceTraceOption =
        pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption;

    pSynceGetFsSynceEntry->MibObject.i4FsSynceQLMode =
        pSynceFsSynceEntry->MibObject.i4FsSynceQLMode;

    pSynceGetFsSynceEntry->MibObject.u4FsSynceQLValue =
        pSynceFsSynceEntry->MibObject.u4FsSynceQLValue;

    pSynceGetFsSynceEntry->MibObject.i4FsSynceSSMOptionMode =
        pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode;

    pSynceGetFsSynceEntry->MibObject.i4FsSynceSelectedInterface =
        pSynceFsSynceEntry->MibObject.i4FsSynceSelectedInterface;

    pSynceGetFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
        pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus;

    if (SynceGetAllUtlFsSynceTable (pSynceGetFsSynceEntry, pSynceFsSynceEntry)
        == OSIX_FAILURE)

    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceGetAllFsSynceIfTable
 Input       :  pSynceGetFsSynceIfEntry
 Description :  This routine will fetch value
                of all fields in SyncE interface
                table corresponding to the given indices.
 Output      :  pSynceGetFsSynceIfEntry - structure containing interface
                related information.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceGetAllFsSynceIfTable (tSynceFsSynceIfEntry * pSynceGetFsSynceIfEntry)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    /* Check whether the node is already present */
    pSynceFsSynceIfEntry =
        RBTreeGet (gSynceGlobals.glbMib.FsSynceIfTable,
                   (tRBElem *) pSynceGetFsSynceIfEntry);

    if (pSynceFsSynceIfEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode;

    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode;

    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfPriority =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority;

    pSynceGetFsSynceIfEntry->MibObject.u4FsSynceIfQLValue =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue;

    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIsRxQLForced;

    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus;

    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch;

    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfSignalFail =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSignalFail;

    pSynceGetFsSynceIfEntry->MibObject.u4FsSynceIfPktsTx =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsTx;

    pSynceGetFsSynceIfEntry->MibObject.u4FsSynceIfPktsRx =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRx;

    pSynceGetFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped;

    pSynceGetFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored =
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored;

    pSynceGetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus;

    if (SynceGetAllUtlFsSynceIfTable
        (pSynceGetFsSynceIfEntry, pSynceFsSynceIfEntry) == OSIX_FAILURE)

    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceSetAllFsSynceTable
 Input       :  pSynceSetFsSynceEntry
                pSynceIsSetFsSynceEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine will set values
                of all fields in SyncE table
                corresponding to the given indices.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceSetAllFsSynceTable (tSynceFsSynceEntry * pSynceSetFsSynceEntry,
                         tSynceIsSetFsSynceEntry * pSynceIsSetFsSynceEntry,
                         INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    tSynceFsSynceEntry *pSynceOldFsSynceEntry = NULL;
    tSynceFsSynceEntry *pSynceTrgFsSynceEntry = NULL;
    tSynceIsSetFsSynceEntry *pSynceTrgIsSetFsSynceEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pSynceOldFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
    if (pSynceOldFsSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC |
                    SYNCE_ALL_FAILURE_TRC, "Fail to Allocate Memory.\r\n"));
        return OSIX_FAILURE;
    }
    pSynceTrgFsSynceEntry =
        (tSynceFsSynceEntry *) MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
    if (pSynceTrgFsSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC |
                    SYNCE_ALL_FAILURE_TRC, "Fail to Allocate Memory.\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceEntry);
        return OSIX_FAILURE;
    }
    pSynceTrgIsSetFsSynceEntry =
        (tSynceIsSetFsSynceEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCETABLE_ISSET_POOLID);
    if (pSynceTrgIsSetFsSynceEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC |
                    SYNCE_ALL_FAILURE_TRC, "Fail to Allocate Memory.\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pSynceOldFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceTrgFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
    MEMSET (pSynceTrgIsSetFsSynceEntry, 0, sizeof (tSynceIsSetFsSynceEntry));

    /* Check whether the node is already present */
    pSynceFsSynceEntry =
        RBTreeGet (gSynceGlobals.glbMib.FsSynceTable,
                   (tRBElem *) pSynceSetFsSynceEntry);

    if (pSynceFsSynceEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
             CREATE_AND_WAIT)
            || (pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
                CREATE_AND_GO)
            ||
            ((pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pSynceFsSynceEntry =
                (tSynceFsSynceEntry *)
                MemAllocMemBlk (SYNCE_FSSYNCETABLE_POOLID);
            if (pSynceFsSynceEntry == NULL)
            {
                if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                    pSynceIsSetFsSynceEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, " function fails\r\n"));

                }
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC,
                            "Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pSynceFsSynceEntry, 0, sizeof (tSynceFsSynceEntry));
            if ((SynceInitializeFsSynceTable (pSynceFsSynceEntry)) ==
                OSIX_FAILURE)
            {
                if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                    pSynceIsSetFsSynceEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "function fails\r\n"));

                }
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC,
                            "Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pSynceIsSetFsSynceEntry->bFsSynceContextId != OSIX_FALSE)
            {
                pSynceFsSynceEntry->MibObject.i4FsSynceContextId =
                    pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId;
            }

            if (pSynceIsSetFsSynceEntry->bFsSynceTraceOption != OSIX_FALSE)
            {
                pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption =
                    pSynceSetFsSynceEntry->MibObject.u4FsSynceTraceOption;
            }

            if (pSynceIsSetFsSynceEntry->bFsSynceQLMode != OSIX_FALSE)
            {
                pSynceFsSynceEntry->MibObject.i4FsSynceQLMode =
                    pSynceSetFsSynceEntry->MibObject.i4FsSynceQLMode;
            }

            if (pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode != OSIX_FALSE)
            {
                pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode =
                    pSynceSetFsSynceEntry->MibObject.i4FsSynceSSMOptionMode;
            }

            if (pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus != OSIX_FALSE)
            {
                pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
                    pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus;
            }

            if ((pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pSynceSetFsSynceEntry->MibObject.
                                        i4FsSynceContextRowStatus == ACTIVE)))
            {
                pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
                    ACTIVE;
            }
            else if (pSynceSetFsSynceEntry->MibObject.
                     i4FsSynceContextRowStatus == CREATE_AND_WAIT)
            {
                pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gSynceGlobals.glbMib.FsSynceTable,
                 (tRBElem *) pSynceFsSynceEntry) != RB_SUCCESS)
            {
                if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                    pSynceIsSetFsSynceEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "function returns failure.\r\n"));
                }
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC,
                            "RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceEntry);
                return OSIX_FAILURE;
            }
            if (SynceUtilUpdateFsSynceTable
                (NULL, pSynceFsSynceEntry,
                 pSynceIsSetFsSynceEntry) != OSIX_SUCCESS)
            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceUtilUpdateFsSynceTable function returns failure.\r\n"));

                if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                    pSynceIsSetFsSynceEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gSynceGlobals.glbMib.FsSynceTable,
                           pSynceFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceEntry);
                return OSIX_FAILURE;
            }

            if ((pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pSynceSetFsSynceEntry->MibObject.
                                        i4FsSynceContextRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pSynceTrgFsSynceEntry->MibObject.i4FsSynceContextId =
                    pSynceSetFsSynceEntry->MibObject.i4FsSynceContextId;
                pSynceTrgFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
                    CREATE_AND_WAIT;
                pSynceTrgIsSetFsSynceEntry->bFsSynceContextRowStatus =
                    OSIX_TRUE;

                if (SynceSetAllFsSynceTableTrigger (pSynceTrgFsSynceEntry,
                                                    pSynceTrgIsSetFsSynceEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                        (UINT1 *) pSynceFsSynceEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                        (UINT1 *) pSynceOldFsSynceEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                        (UINT1 *) pSynceTrgFsSynceEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                        (UINT1 *) pSynceTrgIsSetFsSynceEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pSynceSetFsSynceEntry->MibObject.
                     i4FsSynceContextRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pSynceTrgFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
                    CREATE_AND_WAIT;
                pSynceTrgIsSetFsSynceEntry->bFsSynceContextRowStatus =
                    OSIX_TRUE;

                if (SynceSetAllFsSynceTableTrigger (pSynceTrgFsSynceEntry,
                                                    pSynceTrgIsSetFsSynceEntry,
                                                    SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                        (UINT1 *) pSynceFsSynceEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                        (UINT1 *) pSynceOldFsSynceEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                        (UINT1 *) pSynceTrgFsSynceEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                        (UINT1 *) pSynceTrgIsSetFsSynceEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
                CREATE_AND_GO)
            {
                pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
                    ACTIVE;
            }

            if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                pSynceIsSetFsSynceEntry,
                                                SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                pSynceIsSetFsSynceEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
            }
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceTable: Failure.\r\n"));
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
              CREATE_AND_WAIT)
             || (pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus ==
                 CREATE_AND_GO))
    {
        if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                            pSynceIsSetFsSynceEntry,
                                            SNMP_FAILURE) != OSIX_SUCCESS)

        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
        }
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "The row is already present.\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pSynceOldFsSynceEntry, pSynceFsSynceEntry,
            sizeof (tSynceFsSynceEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus == DESTROY)
    {
        pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus = DESTROY;

        if (SynceUtilUpdateFsSynceTable (pSynceOldFsSynceEntry,
                                         pSynceFsSynceEntry,
                                         pSynceIsSetFsSynceEntry) !=
            OSIX_SUCCESS)
        {

            if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                pSynceIsSetFsSynceEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
            }
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceUtilUpdateFsSynceTable function returns failure.\r\n"));
        }
        RBTreeRem (gSynceGlobals.glbMib.FsSynceTable, pSynceFsSynceEntry);
        if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                            pSynceIsSetFsSynceEntry,
                                            SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsSynceTableFilterInputs (pSynceFsSynceEntry, pSynceSetFsSynceEntry,
                                  pSynceIsSetFsSynceEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus == ACTIVE) &&
        (pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus !=
         NOT_IN_SERVICE))
    {
        pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pSynceTrgFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
            NOT_IN_SERVICE;
        pSynceTrgIsSetFsSynceEntry->bFsSynceContextRowStatus = OSIX_TRUE;

        if (SynceUtilUpdateFsSynceTable (pSynceOldFsSynceEntry,
                                         pSynceFsSynceEntry,
                                         pSynceIsSetFsSynceEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pSynceFsSynceEntry, pSynceOldFsSynceEntry,
                    sizeof (tSynceFsSynceEntry));
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceUtilUpdateFsSynceTable Function returns failure.\r\n"));

            if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                                pSynceIsSetFsSynceEntry,
                                                SNMP_FAILURE) != OSIX_SUCCESS)

            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceEntry);
            return OSIX_FAILURE;
        }

        if (SynceSetAllFsSynceTableTrigger (pSynceTrgFsSynceEntry,
                                            pSynceTrgIsSetFsSynceEntry,
                                            SNMP_FAILURE) != OSIX_SUCCESS)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
        }
    }

    if (pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pSynceIsSetFsSynceEntry->bFsSynceTraceOption != OSIX_FALSE)
    {
        pSynceFsSynceEntry->MibObject.u4FsSynceTraceOption =
            pSynceSetFsSynceEntry->MibObject.u4FsSynceTraceOption;
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceQLMode != OSIX_FALSE)
    {
        pSynceFsSynceEntry->MibObject.i4FsSynceQLMode =
            pSynceSetFsSynceEntry->MibObject.i4FsSynceQLMode;
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceSSMOptionMode != OSIX_FALSE)
    {
        pSynceFsSynceEntry->MibObject.i4FsSynceSSMOptionMode =
            pSynceSetFsSynceEntry->MibObject.i4FsSynceSSMOptionMode;
    }
    if (pSynceIsSetFsSynceEntry->bFsSynceContextRowStatus != OSIX_FALSE)
    {
        pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus =
            pSynceSetFsSynceEntry->MibObject.i4FsSynceContextRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pSynceFsSynceEntry->MibObject.i4FsSynceContextRowStatus = ACTIVE;
    }

    if (SynceUtilUpdateFsSynceTable (pSynceOldFsSynceEntry,
                                     pSynceFsSynceEntry,
                                     pSynceIsSetFsSynceEntry) != OSIX_SUCCESS)
    {

        if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                            pSynceIsSetFsSynceEntry,
                                            SNMP_FAILURE) != OSIX_SUCCESS)

        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));

        }
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceUtilUpdateFsSynceTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pSynceFsSynceEntry, pSynceOldFsSynceEntry,
                sizeof (tSynceFsSynceEntry));
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceEntry);
        return OSIX_FAILURE;

    }
    if (SynceSetAllFsSynceTableTrigger (pSynceSetFsSynceEntry,
                                        pSynceIsSetFsSynceEntry,
                                        SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceSetAllFsSynceTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceOldFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_POOLID,
                        (UINT1 *) pSynceTrgFsSynceEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCETABLE_ISSET_POOLID,
                        (UINT1 *) pSynceTrgIsSetFsSynceEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  SynceSetAllFsSynceIfTable
 Input       :  pSynceSetFsSynceIfEntry
                pSynceIsSetFsSynceIfEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This routine will set values
                of all fields in SyncE interface
                table corresponding to the given indices.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceSetAllFsSynceIfTable (tSynceFsSynceIfEntry * pSynceSetFsSynceIfEntry,
                           tSynceIsSetFsSynceIfEntry *
                           pSynceIsSetFsSynceIfEntry, INT4 i4RowStatusLogic,
                           INT4 i4RowCreateOption)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    tSynceFsSynceIfEntry *pSynceOldFsSynceIfEntry = NULL;
    tSynceFsSynceIfEntry *pSynceTrgFsSynceIfEntry = NULL;
    tSynceIsSetFsSynceIfEntry *pSynceTrgIsSetFsSynceIfEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pSynceOldFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceOldFsSynceIfEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC |
                    SYNCE_ALL_FAILURE_TRC, "Fail to Allocate Memory.\r\n"));
        return OSIX_FAILURE;
    }
    pSynceTrgFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
    if (pSynceTrgFsSynceIfEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC |
                    SYNCE_ALL_FAILURE_TRC, "Fail to Allocate Memory.\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceIfEntry);
        return OSIX_FAILURE;
    }
    pSynceTrgIsSetFsSynceIfEntry =
        (tSynceIsSetFsSynceIfEntry *)
        MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID);
    if (pSynceTrgIsSetFsSynceIfEntry == NULL)
    {
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC |
                    SYNCE_ALL_FAILURE_TRC, "Fail to Allocate Memory.\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceIfEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pSynceOldFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceTrgFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
    MEMSET (pSynceTrgIsSetFsSynceIfEntry, 0,
            sizeof (tSynceIsSetFsSynceIfEntry));

    /* Check whether the node is already present */
    pSynceFsSynceIfEntry =
        RBTreeGet (gSynceGlobals.glbMib.FsSynceIfTable,
                   (tRBElem *) pSynceSetFsSynceIfEntry);

    if (pSynceFsSynceIfEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
             CREATE_AND_WAIT)
            || (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                CREATE_AND_GO)
            ||
            ((pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pSynceFsSynceIfEntry =
                (tSynceFsSynceIfEntry *)
                MemAllocMemBlk (SYNCE_FSSYNCEIFTABLE_POOLID);
            if (pSynceFsSynceIfEntry == NULL)
            {
                if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                      pSynceIsSetFsSynceIfEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceIfTableTrigger function fails\r\n"));

                }
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC,
                            "Fail to Allocate Memory\r\n"));
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pSynceFsSynceIfEntry, 0, sizeof (tSynceFsSynceIfEntry));
            if ((SynceInitializeFsSynceIfTable (pSynceFsSynceIfEntry)) ==
                OSIX_FAILURE)
            {
                if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                      pSynceIsSetFsSynceIfEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceIfTableTrigger function fails\r\n"));

                }
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfIndex != OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfIndex =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex;
            }

            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode != OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode;
            }

            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode != OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode;
            }

            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority != OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfPriority;
            }

            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue != OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue =
                    pSynceSetFsSynceIfEntry->MibObject.u4FsSynceIfQLValue;
            }

            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus !=
                OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus;
            }

            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch !=
                OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch;
            }

            if (pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus != OSIX_FALSE)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus;
            }

            if ((pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pSynceSetFsSynceIfEntry->MibObject.
                                        i4FsSynceIfRowStatus == ACTIVE)))
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus = ACTIVE;
            }
            else if (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                     CREATE_AND_WAIT)
            {
                pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gSynceGlobals.glbMib.FsSynceIfTable,
                 (tRBElem *) pSynceFsSynceIfEntry) != RB_SUCCESS)
            {
                if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                      pSynceIsSetFsSynceIfEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
                }
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC | SYNCE_CRITICAL_TRC,
                            "RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
                return OSIX_FAILURE;
            }
            if (SynceUtilUpdateFsSynceIfTable
                (NULL, pSynceFsSynceIfEntry,
                 pSynceIsSetFsSynceIfEntry) != OSIX_SUCCESS)
            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceUtilUpdateFsSynceIfTable function returns failure.\r\n"));

                if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                      pSynceIsSetFsSynceIfEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gSynceGlobals.glbMib.FsSynceIfTable,
                           pSynceFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceOldFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                    (UINT1 *) pSynceTrgFsSynceIfEntry);
                MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                    (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
                return OSIX_FAILURE;
            }

            if ((pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pSynceSetFsSynceIfEntry->MibObject.
                                        i4FsSynceIfRowStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pSynceTrgFsSynceIfEntry->MibObject.i4FsSynceIfIndex =
                    pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfIndex;
                pSynceTrgFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
                    CREATE_AND_WAIT;
                pSynceTrgIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_TRUE;

                if (SynceSetAllFsSynceIfTableTrigger (pSynceTrgFsSynceIfEntry,
                                                      pSynceTrgIsSetFsSynceIfEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                        (UINT1 *) pSynceFsSynceIfEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                        (UINT1 *) pSynceOldFsSynceIfEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                        (UINT1 *) pSynceTrgFsSynceIfEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                        (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                     CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pSynceTrgFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
                    CREATE_AND_WAIT;
                pSynceTrgIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_TRUE;

                if (SynceSetAllFsSynceIfTableTrigger (pSynceTrgFsSynceIfEntry,
                                                      pSynceTrgIsSetFsSynceIfEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                                "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                        (UINT1 *) pSynceFsSynceIfEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                        (UINT1 *) pSynceOldFsSynceIfEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                        (UINT1 *) pSynceTrgFsSynceIfEntry);
                    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                        (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                CREATE_AND_GO)
            {
                pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
                    ACTIVE;
            }

            if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                  pSynceIsSetFsSynceIfEntry,
                                                  SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                  pSynceIsSetFsSynceIfEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
            }
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "Failure\r\n"));
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
              CREATE_AND_WAIT)
             || (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus ==
                 CREATE_AND_GO))
    {
        if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                              pSynceIsSetFsSynceIfEntry,
                                              SNMP_FAILURE) != OSIX_SUCCESS)

        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
        }
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC, "The row is already present.\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pSynceOldFsSynceIfEntry, pSynceFsSynceIfEntry,
            sizeof (tSynceFsSynceIfEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus == DESTROY)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus = DESTROY;

        if (SynceUtilUpdateFsSynceIfTable (pSynceOldFsSynceIfEntry,
                                           pSynceFsSynceIfEntry,
                                           pSynceIsSetFsSynceIfEntry) !=
            OSIX_SUCCESS)
        {

            if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                  pSynceIsSetFsSynceIfEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
            }
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceUtilUpdateFsSynceIfTable function returns failure.\r\n"));
        }
        RBTreeRem (gSynceGlobals.glbMib.FsSynceIfTable, pSynceFsSynceIfEntry);
        if (SynceSetAllFsSynceIfTableTrigger
            (pSynceSetFsSynceIfEntry, pSynceIsSetFsSynceIfEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsSynceIfTableFilterInputs
        (pSynceFsSynceIfEntry, pSynceSetFsSynceIfEntry,
         pSynceIsSetFsSynceIfEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus == ACTIVE) &&
        (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus !=
         NOT_IN_SERVICE))
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pSynceTrgFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
            NOT_IN_SERVICE;
        pSynceTrgIsSetFsSynceIfEntry->bFsSynceIfRowStatus = OSIX_TRUE;

        if (SynceUtilUpdateFsSynceIfTable (pSynceOldFsSynceIfEntry,
                                           pSynceFsSynceIfEntry,
                                           pSynceIsSetFsSynceIfEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pSynceFsSynceIfEntry, pSynceOldFsSynceIfEntry,
                    sizeof (tSynceFsSynceIfEntry));
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceUtilUpdateFsSynceIfTable Function returns failure.\r\n"));

            if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                                  pSynceIsSetFsSynceIfEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                            "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceOldFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                                (UINT1 *) pSynceTrgFsSynceIfEntry);
            MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                                (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
            return OSIX_FAILURE;
        }

        if (SynceSetAllFsSynceIfTableTrigger (pSynceTrgFsSynceIfEntry,
                                              pSynceTrgIsSetFsSynceIfEntry,
                                              SNMP_FAILURE) != OSIX_SUCCESS)
        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
        }
    }

    if (pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfSynceMode != OSIX_FALSE)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode =
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfSynceMode;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfEsmcMode != OSIX_FALSE)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode =
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfEsmcMode;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfPriority != OSIX_FALSE)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfPriority =
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfPriority;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfQLValue != OSIX_FALSE)
    {
        pSynceFsSynceIfEntry->MibObject.u4FsSynceIfQLValue =
            pSynceSetFsSynceIfEntry->MibObject.u4FsSynceIfQLValue;
    }
    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfLockoutStatus != OSIX_FALSE)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus =
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfLockoutStatus;
    }

    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfConfigSwitch !=
        OSIX_FALSE)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch =
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfConfigSwitch;
    }

    if (pSynceIsSetFsSynceIfEntry->bFsSynceIfRowStatus != OSIX_FALSE)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus =
            pSynceSetFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pSynceFsSynceIfEntry->MibObject.i4FsSynceIfRowStatus = ACTIVE;
    }

    if (SynceUtilUpdateFsSynceIfTable (pSynceOldFsSynceIfEntry,
                                       pSynceFsSynceIfEntry,
                                       pSynceIsSetFsSynceIfEntry) !=
        OSIX_SUCCESS)
    {

        if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                              pSynceIsSetFsSynceIfEntry,
                                              SNMP_FAILURE) != OSIX_SUCCESS)

        {
            SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                        "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));

        }
        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceUtilUpdateFsSynceIfTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pSynceFsSynceIfEntry, pSynceOldFsSynceIfEntry,
                sizeof (tSynceFsSynceIfEntry));
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceOldFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                            (UINT1 *) pSynceTrgFsSynceIfEntry);
        MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                            (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
        return OSIX_FAILURE;

    }
    if (SynceSetAllFsSynceIfTableTrigger (pSynceSetFsSynceIfEntry,
                                          pSynceIsSetFsSynceIfEntry,
                                          SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        SYNCE_TRC ((SYNCE_OS_RESOURCE_TRC,
                    "SynceSetAllFsSynceIfTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceOldFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_POOLID,
                        (UINT1 *) pSynceTrgFsSynceIfEntry);
    MemReleaseMemBlock (SYNCE_FSSYNCEIFTABLE_ISSET_POOLID,
                        (UINT1 *) pSynceTrgIsSetFsSynceIfEntry);
    return OSIX_SUCCESS;

}
