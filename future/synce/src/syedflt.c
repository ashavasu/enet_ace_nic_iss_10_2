/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syedflt.c,v 1.1 2013/03/23 12:41:37 siva Exp $ 
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Synce
*********************************************************************/

#include "syeinc.h"

/****************************************************************************
* Function    : SynceInitializeFsSynceTable
* Input       : pSynceFsSynceEntry
* Description : It Intializes the given
*               structure of synce table
*               by calling the suitable low
*               level function.
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
SynceInitializeFsSynceTable (tSynceFsSynceEntry * pSynceFsSynceEntry)
{
    if (pSynceFsSynceEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((SynceInitializeMibFsSynceTable (pSynceFsSynceEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : SynceInitializeFsSynceIfTable
* Input       : pSynceFsSynceIfEntry
* Description : It Intializes the given structure
*               of synce interface table by calling
*               the suitable low level function.
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
SynceInitializeFsSynceIfTable (tSynceFsSynceIfEntry * pSynceFsSynceIfEntry)
{
    if (pSynceFsSynceIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((SynceInitializeMibFsSynceIfTable (pSynceFsSynceIfEntry)) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
