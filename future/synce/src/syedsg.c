/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syedsg.c,v 1.3 2013/10/24 10:00:59 siva Exp $ 
*
* Description: This file contains the routines for DataStructure access
* for the module Synce.
*********************************************************************/

#include "syeinc.h"
/****************************************************************************
 Function    :  SynceGetFsSynceGlobalSysCtrl
 Input       :  The Indices
 Output       :  pi4FsSynceGlobalSysCtrl
 Descritpion :  This Routine will Get
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceGetFsSynceGlobalSysCtrl (INT4 *pi4FsSynceGlobalSysCtrl)
{
    *pi4FsSynceGlobalSysCtrl = gSynceGlobals.glbMib.i4FsSynceGlobalSysCtrl;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceSetFsSynceGlobalSysCtrl
 Input       :  i4FsSynceGlobalSysCtrl
 Description :  This Routine will Set
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
SynceSetFsSynceGlobalSysCtrl (INT4 i4FsSynceGlobalSysCtrl)
{
    INT4                i4SetOption = SNMP_SUCCESS;

    if (gSynceGlobals.glbMib.i4FsSynceGlobalSysCtrl != i4FsSynceGlobalSysCtrl)
    {
        switch (i4FsSynceGlobalSysCtrl)
        {
            case SYNCE_MODULE_START:
                if (SynceUtilModuleStart () != OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
                break;
            case SYNCE_MODULE_SHUTDOWN:
                SynceUtilModuleShutdown ();
                break;
            default:
                return OSIX_FAILURE;
        }
    }

    gSynceGlobals.glbMib.i4FsSynceGlobalSysCtrl = i4FsSynceGlobalSysCtrl;

    nmhSetCmnNew (FsSynceGlobalSysCtrl, 12, SynceApiLock,
                  SynceApiUnLock, 0, 0, 0, i4SetOption, "%i",
                  gSynceGlobals.glbMib.i4FsSynceGlobalSysCtrl);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceSetFsSynceIfCtrs
 Input       :  u4IfIndex   - Interface Index
                u4PktsCount - Packets Count
                u4Cntrtype  - Counter Type
 Description :  This Routine will Set
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
SynceSetFsSynceIfCntrs (UINT4 u4IfIndex, UINT4 u4PktsCount, UINT4 u4CntrType)
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry = SynceGetFsSynceIfEntry (u4IfIndex);
    if (pSynceFsSynceIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    switch (u4CntrType)
    {
        case SYNCE_CNTR_ALL:
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRx = u4PktsCount;
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsTx = u4PktsCount;
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored =
                u4PktsCount;
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped =
                u4PktsCount;
            break;

        case SYNCE_CNTR_TX:
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsTx = u4PktsCount;
            break;

        case SYNCE_CNTR_RX:
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRx = u4PktsCount;
            break;

        case SYNCE_CNTR_ERRORED:
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxErrored =
                u4PktsCount;
            break;

        case SYNCE_CNTR_DROPPED:
            pSynceFsSynceIfEntry->MibObject.u4FsSynceIfPktsRxDropped =
                u4PktsCount;
            break;

        default:
            break;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  SynceGetFirstFsSynceTable
 Input       :  NONE
 Description :  This routine is used to fetch
                the first entry from synce table
 Returns     :  pSynceFsSynceEntry or NULL
****************************************************************************/
tSynceFsSynceEntry *
SynceGetFirstFsSynceTable ()
{
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;

    pSynceFsSynceEntry =
        (tSynceFsSynceEntry *) RBTreeGetFirst (gSynceGlobals.glbMib.
                                               FsSynceTable);

    return pSynceFsSynceEntry;
}

/****************************************************************************
 Function    :  SynceGetNextFsSynceTable
 Input       :  pCurrentSynceFsSynceEntry
 Description :  This routine is used to fetch
                the next entry from synce table.
                (Next to the entry whose structure
                is passed.)
 Returns     :  pNextSynceFsSynceEntry or NULL
****************************************************************************/
tSynceFsSynceEntry *
SynceGetNextFsSynceTable (tSynceFsSynceEntry * pCurrentSynceFsSynceEntry)
{
    tSynceFsSynceEntry *pNextSynceFsSynceEntry = NULL;

    pNextSynceFsSynceEntry =
        (tSynceFsSynceEntry *) RBTreeGetNext (gSynceGlobals.glbMib.
                                              FsSynceTable,
                                              (tRBElem *)
                                              pCurrentSynceFsSynceEntry, NULL);

    return pNextSynceFsSynceEntry;
}

/****************************************************************************
 Function    :  SynceGetFsSynceTable
 Input       :  pSynceFsSynceEntry
 Description :  This routine is used to fetch
                an entry from synce table.
 Returns     :  pGetSynceFsSynceEntry or NULL
****************************************************************************/
tSynceFsSynceEntry *
SynceGetFsSynceTable (tSynceFsSynceEntry * pSynceFsSynceEntry)
{
    tSynceFsSynceEntry *pGetSynceFsSynceEntry = NULL;

    pGetSynceFsSynceEntry =
        (tSynceFsSynceEntry *) RBTreeGet (gSynceGlobals.glbMib.
                                          FsSynceTable,
                                          (tRBElem *) pSynceFsSynceEntry);

    return pGetSynceFsSynceEntry;
}

/****************************************************************************
 Function    :  SynceGetFirstFsSynceIfTable
 Input       :  NONE
 Description :  This routine is used to fetch
                the first entry from synce
                interface table
 Returns     :  pSynceFsSynceIfEntry or NULL
****************************************************************************/
tSynceFsSynceIfEntry *
SynceGetFirstFsSynceIfTable ()
{
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;

    pSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) RBTreeGetFirst (gSynceGlobals.glbMib.
                                                 FsSynceIfTable);

    return pSynceFsSynceIfEntry;
}

/****************************************************************************
 Function    :  SynceGetNextFsSynceIfTable
 Input       :  pCurrentSynceFsSynceIfEntry
 Description :  This routine is used to fetch
                the next entry from synce
                interface table.
                (Next to the entry whose structure
                is passed.)
 Returns     :  pNextSynceFsSynceIfEntry or NULL
****************************************************************************/
tSynceFsSynceIfEntry *
SynceGetNextFsSynceIfTable (tSynceFsSynceIfEntry * pCurrentSynceFsSynceIfEntry)
{
    tSynceFsSynceIfEntry *pNextSynceFsSynceIfEntry = NULL;

    pNextSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) RBTreeGetNext (gSynceGlobals.glbMib.
                                                FsSynceIfTable,
                                                (tRBElem *)
                                                pCurrentSynceFsSynceIfEntry,
                                                NULL);

    return pNextSynceFsSynceIfEntry;
}

/****************************************************************************
 Function    :  SynceGetFsSynceIfTable
 Input       :  pSynceFsSynceIfEntry
 Description :  This routine is used to fetch
                an entry from synce interface
                table.
 Returns     :  pGetSynceFsSynceIfEntry or NULL
****************************************************************************/
tSynceFsSynceIfEntry *
SynceGetFsSynceIfTable (tSynceFsSynceIfEntry * pSynceFsSynceIfEntry)
{
    tSynceFsSynceIfEntry *pGetSynceFsSynceIfEntry = NULL;

    pGetSynceFsSynceIfEntry =
        (tSynceFsSynceIfEntry *) RBTreeGet (gSynceGlobals.glbMib.
                                            FsSynceIfTable,
                                            (tRBElem *) pSynceFsSynceIfEntry);

    return pGetSynceFsSynceIfEntry;
}

/****************************************************************************
 Function    :  SynceGetFsSynceEntry
 Input       :  u4ContextId
 Description :  This routine returns the SyncE context entry.
 Returns     :  pGetSynceFsSynceEntry or NULL
****************************************************************************/
tSynceFsSynceEntry *
SynceGetFsSynceEntry (UINT4 u4ContextId)
{
    tSynceFsSynceEntry  key;
    tSynceFsSynceEntry *pSynceFsSynceEntry = NULL;
    key.MibObject.i4FsSynceContextId = (INT4) u4ContextId;
    pSynceFsSynceEntry = SynceGetFsSynceTable (&key);
    return pSynceFsSynceEntry;
}

/****************************************************************************
 Function    :  SynceGetFsSynceIfEntry
 Input       :  u4IfIndex
 Description :  This routine returns the SyncE If entry.
 Returns     :  pGetSynceFsSynceIfEntry or NULL
****************************************************************************/
tSynceFsSynceIfEntry *
SynceGetFsSynceIfEntry (UINT4 u4IfIndex)
{
    tSynceFsSynceIfEntry key;
    tSynceFsSynceIfEntry *pSynceFsSynceIfEntry = NULL;
    key.MibObject.i4FsSynceIfIndex = (INT4) u4IfIndex;
    pSynceFsSynceIfEntry = SynceGetFsSynceIfTable (&key);
    return pSynceFsSynceIfEntry;
}
