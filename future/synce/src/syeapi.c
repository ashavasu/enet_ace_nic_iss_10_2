/******************************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: syeapi.c,v 1.4 2015/10/15 11:12:53 siva Exp $
*
* Description: This file contains the APIs exposed by Synce module.
******************************************************************************/

#ifndef __SYNCEAPI_C__
#define __SYNCEAPI_C__

#include "syeinc.h"
#include "cfa.h"
#include "synce.h"

/*****************************************************************************/
/* Function                  : SynceApiLock                                  */
/* Description               : This API shall be used to take the mutual     */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/* Output                    : None                                          */
/* Global Variables Referred : gSynceGlobalInfo.semId                        */
/* Global Variables Modified : None.                                         */
/* Use of Recursion          : None.                                         */
/* Returns                   : SNMP_SUCCESS/SNMP_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
SynceApiLock (VOID)
{
    SYNCE_FN_ENTRY ();

    if ((gSynceGlobals.taskSemId) &&
        (OsixSemTake (gSynceGlobals.taskSemId) == OSIX_FAILURE))
    {
        SYNCE_FN_EXIT ();
        return SNMP_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function                  : SynceApiUnLock                                */
/* Description               : This API shall be used to release the mutual  */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/* Output                    : None                                          */
/* Global Variables Referred : gSynceGlobalInfo.semId                        */
/* Global Variables Modified : None.                                         */
/* Use of Recursion          : None.                                         */
/* Returns                   : SNMP_SUCCESS/SNMP_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
SynceApiUnLock (VOID)
{
    SYNCE_FN_ENTRY ();

    if ((gSynceGlobals.taskSemId) &&
        (OsixSemGive (gSynceGlobals.taskSemId) == OSIX_FAILURE))
    {
        SYNCE_FN_EXIT ();
        return SNMP_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function                  : SynceApiIfOperStatusChangeCb                  *
 *                                                                           *
 * Description               : This is the callback API registered with the  *
 *                             interface manager(CFA) for receiving          *
 *                             notification about the change in the interface*
 *                             status.                                       *
 *                                                                           *
 * Input                     : pCfaRegInfo - Pointer to the CFA interface    *
 *                                                                           *
 * Output                    : None                                          *
 * Global Variables Referred : None.                                         *
 * Global Variables Modified : None.                                         *
 * Use of Recursion          : None.                                         *
 *                                                                           *
 * Returns                   : CFA_SUCCESS/CFA_FAILURE                       *
 *****************************************************************************/
PUBLIC INT4
SynceApiIfOperStatusChangeCb (tCfaRegInfo * pCfaRegInfo)
{
    tSynceQMsg         *pSynceQMsg = NULL;

    SYNCE_FN_ENTRY ();

    if (SynceUtilIsSynceEnabled () == OSIX_FALSE)
    {
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    if (pCfaRegInfo == NULL)
    {
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    /* handle messages those are targeted for physical interface */
    if (CfaIsPhysicalInterface (pCfaRegInfo->u4IfIndex) == CFA_FALSE)
    {
        SYNCE_FN_EXIT ();
        return CFA_SUCCESS;
    }

    if ((pSynceQMsg = (tSynceQMsg *) MemAllocMemBlk (gSynceGlobals.msgQPoolId))
        == NULL)
    {

        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_BUFFER_TRC, "Memory allocation"
                    "for Message Queue failed\r\n"));
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }
    pSynceQMsg->u4MsgType = SYNCE_IF_OPER_ST_CHG;
    pSynceQMsg->u4IfIndex = pCfaRegInfo->u4IfIndex;

    /* Obtaining Context id from Interface Index */
    if (SynceUtilGetContextIdFromIfIndex (pSynceQMsg->u4IfIndex,
                                          &(pSynceQMsg->u4ContextId))
        == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Context Id" "not found\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCEMESSAGEQUEUE_POOLID,
                            (UINT1 *) pSynceQMsg);
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    pSynceQMsg->u1IfOperStatus =
        pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1IfOperStatus;

    /* Posting message to message queue */
    if (OsixSendToQ (0, SYNCE_QUEUE_NAME, (tOsixMsg *) pSynceQMsg, 0)
        == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Message posting to Message Queue failed\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCEMESSAGEQUEUE_POOLID,
                            (UINT1 *) pSynceQMsg);
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    /* Posting message receival event to Synce */
    if (OsixSendEvent (0, (const UINT1 *) SYNCE_TASK_NAME,
                       SYNCE_QUEUE_EVENT) == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Message posting failed\r\n"));
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return CFA_SUCCESS;
}

/*****************************************************************************
 * Function                  : SynceApiIfDeleteCb                            *
 *                                                                           *
 * Description               : This is the callback API provided by the Synce*
 *                             module to the Interface manager in the system *
 *                             to notify about interface deletion.           *
 *                                                                           *
 * Input                     : pCfaRegInfo - Pointer to the CFA interface    *

 * Output                    : None                                          *
 * Global Variables Referred : None.                                         *
 * Global Variables Modified : None.                                         *
 * Use of Recursion          : None.                                         *
 *                                                                           *
 * Returns                   : CFA_SUCCESS/CFA_FAILURE                       *
  *****************************************************************************/

PUBLIC INT4
SynceApiIfDeleteCb (tCfaRegInfo * pCfaRegInfo)
{
    tSynceQMsg         *pSynceQMsg = NULL;

    SYNCE_FN_ENTRY ();

    if (SynceUtilIsSynceEnabled () == OSIX_FALSE)
    {
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    if (pCfaRegInfo == NULL)
    {
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    /* handle messages those are targeted for physical interface */
    if (CfaIsPhysicalInterface (pCfaRegInfo->u4IfIndex) == CFA_FALSE)
    {
        SYNCE_FN_EXIT ();
        return CFA_SUCCESS;
    }

    if ((pSynceQMsg = (tSynceQMsg *) MemAllocMemBlk (gSynceGlobals.msgQPoolId))
        == NULL)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_BUFFER_TRC |
                    SYNCE_ALL_FAILURE_TRC,
                    "SynceApiIfDeleteCb : Memory allocation"
                    "for Message Queue failed\r\n"));
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    pSynceQMsg->u4MsgType = SYNCE_IF_DELETE;
    pSynceQMsg->u4IfIndex = pCfaRegInfo->u4IfIndex;

    /* Obtaining Context id from Interface Index */
    if (SynceUtilGetContextIdFromIfIndex (pSynceQMsg->u4IfIndex,
                                          &(pSynceQMsg->u4ContextId))
        == OSIX_FAILURE)
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEMESSAGEQUEUE_POOLID,
                            (UINT1 *) pSynceQMsg);
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Context Id not found\r\n"));
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    pSynceQMsg->u1IfOperStatus =
        pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1IfOperStatus;
    /* Posting message to message queue */
    if (OSIX_FAILURE ==
        OsixSendToQ (0, SYNCE_QUEUE_NAME, (tOsixMsg *) pSynceQMsg, 0))
    {
        MemReleaseMemBlock (SYNCE_FSSYNCEMESSAGEQUEUE_POOLID,
                            (UINT1 *) pSynceQMsg);
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Message posting to Message Queue failed\r\n"));
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    /* Posting message receival event to Synce */
    if (OsixSendEvent (0, (const UINT1 *) SYNCE_TASK_NAME, SYNCE_QUEUE_EVENT)
        == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Message Receival posting failed\r\n"));
        SYNCE_FN_EXIT ();
        return CFA_FAILURE;
    }

    SYNCE_FN_EXIT ();
    return CFA_SUCCESS;
}

/*****************************************************************************
 * Function     : SynceApiIsSyncePDU
 *
 * Description  : This function checks if the PDU is ESMC PDU or not
 *
 * Input        : pBuf - CRU buffer containing the PDU
 * Output       : None
 * Returns      : OSIX_TRUE/OSIX_FALSE
 *
 *****************************************************************************/
INT4
SynceApiIsSyncePDU (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT2               u2PduLen = 0;
    UINT2               u2ProtType = 0;
    UINT1               u1SubType = 0;
    tMacAddr            DestMacAddr;

    SYNCE_FN_ENTRY ();

    if (SynceUtilIsSynceEnabled () == OSIX_FALSE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FALSE;
    }

    if (pBuf == NULL)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "SyncE Buffer Pointer NULL!!\n"));
        return OSIX_FALSE;
    }

    u2PduLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);
    if ((u2PduLen < (UINT2) SYNCE_MIN_PKT_SIZE))
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC,
                    "SyncE buffer less than the %d bytes!!\n",
                    SYNCE_MIN_PKT_SIZE));
        return OSIX_FALSE;
    }

    SYNCE_CRU_GET_STRING (pBuf, DestMacAddr, SYNCE_DEST_OFFSET, MAC_ADDR_LEN);
    SYNCE_CRU_GET_2_BYTE (pBuf, SYNCE_LEN_TYPE_OFFSET, u2ProtType);
    SYNCE_CRU_GET_1_BYTE (pBuf, SYNCE_SUBTYPE_OFFSET, u1SubType);

    if ((MEMCMP (DestMacAddr, gau1SynceEsmcPduDestMacAddr, MAC_ADDR_LEN)) == 0)
    {
        /* Dest addr matches.
         * Check the Type and sub type */
        if ((u2ProtType == CFA_ENET_SLOW_PROTOCOL) &&
            (u1SubType == SYNCE_SLOW_PROTOCOL_TYPE))
        {
            return OSIX_TRUE;
        }
    }

    SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC, "SYNCE PDU validate failed!!\r\n"));

    SYNCE_FN_EXIT ();
    return OSIX_FALSE;
}

/*****************************************************************************
 * Function     : SynceApiEnqIncomingPdu
 *
 * Description  : This function enque the incoming ESMC PDU for processing
 *                by SyncE module.
 * Input        : pBuf - CRU buffer containing the PDU
 *                u2PduLen - length of ESMC PDU
 *                u4IfIndex - interface on which PDU was received
 * Output       : None
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/

INT4
SynceApiEnqIncomingPdu (tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT2 u2PduLen, UINT4 u4IfIndex)
{
    tSynceQMsg         *pSynceQMsg = NULL;

    SYNCE_FN_ENTRY ();

    if (SynceUtilIsSynceEnabled () == OSIX_FALSE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (pBuf == NULL)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* handle messages those are targeted for physical interface */
    if (CfaIsPhysicalInterface (u4IfIndex) == CFA_FALSE)
    {
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (gSynceGlobals.msgQPoolId != 0)
    {
        if ((pSynceQMsg =
             (tSynceQMsg *) MemAllocMemBlk (gSynceGlobals.msgQPoolId)) == NULL)
        {
            SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                        SYNCE_BUFFER_TRC, "Memory allocation"
                        "for Message Queue failed\r\n"));
            SYNCE_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }
    else
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC |
                    SYNCE_BUFFER_TRC, "Message pool Id is Zero\r\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pSynceQMsg->u4MsgType = SYNCE_IF_PKT_RCV;
    pSynceQMsg->u4IfIndex = u4IfIndex;
    /* Obtaining Context id from Interface Index */
    if (SynceUtilGetContextIdFromIfIndex (u4IfIndex,
                                          &(pSynceQMsg->u4ContextId))
        == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Context Id not found\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCEMESSAGEQUEUE_POOLID,
                            (UINT1 *) pSynceQMsg);
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pSynceQMsg->uSynceMsgParam.pSyncePdu = pBuf;
    pSynceQMsg->u4PktLen = u2PduLen;
    /* Posting message into message queue */
    if ((OsixSendToQ (0, SYNCE_QUEUE_NAME,
                      (tOsixMsg *) pSynceQMsg, 0)) == OSIX_FAILURE)
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Message posting to Message Queue failed\r\n"));
        MemReleaseMemBlock (SYNCE_FSSYNCEMESSAGEQUEUE_POOLID,
                            (UINT1 *) pSynceQMsg);
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Posting message receival event to Synce */
    if ((OsixSendEvent (0, (const UINT1 *) SYNCE_TASK_NAME,
                        SYNCE_QUEUE_EVENT) == OSIX_FAILURE))
    {
        SYNCE_TRC ((SYNCE_CONTROL_PLANE_TRC | SYNCE_ALL_FAILURE_TRC,
                    "Message posting failed\r\n"));
        SYNCE_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SYNCE_FN_EXIT ();

    return OSIX_SUCCESS;
}
#endif
