/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utltrc.c,v 1.14 2015/04/28 12:17:42 siva Exp $
 *
 * Description: This file contains the APIs for tracing and dumping,
 *                which forms a part of the FutureUTL library
 *
 *******************************************************************/

#include "osxinc.h"
#include "utltrc.h"
#include "utltrci.h"

static UINT4        gu4ShowTime = 0;
static INT4         gi4LogFd = OSIX_FILE_LOG;
static INT4         gu4InCoreLog = OSIX_LOG_METHOD;

/* This structure is used to store the trace messages
 * in an incore buffer. It is a cyclic buffer and so
 * wraps around. The API to get the logs is UtlGetLogs
 * and it supports getting the 'head' or 'tail' logs
 * in the unix like fashion.
 * i4Front - Points to the first (earliest) log
 * i4Rear  - Points to the one beyond the latest log.
 * u4Wrapped - Indicates if i4Rear has completed one round
 *             of the circular list. If so it is set and
 *             from then on i4Front also move along, indicative
 *             of the fact that the earliest log messages are
 *             being replaced by newer ones.
 */
struct tLogBuf
{

    CHR1                Log[UTL_MAX_LOGS][UTL_MAX_LOG_LEN];
    INT4                i4Front;
    INT4                i4Rear;
    UINT4               u4Wrapped;
};
static struct tLogBuf Logs;

/***************************************************************/
/*  Function Name   : UtlTrcPrint                              */
/*  Description     : Prints the input message                 */
/*  Input(s)        : pi1Msg - pointer to the message          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlTrcPrint (const char *pi1Msg)
{
    if (gu4InCoreLog == FLASH_LOG_LOCATION)
    {
        printf (" Not supported to store traces in flash location \n");
        return;
    }

    printf ("%s", pi1Msg);
}

/***************************************************************/
/*  Function Name   : UtlGetLogs                               */
/*  Description     : API to get retrieve the logs.            */
/*                    It returns the number of bytes copied.   */
/*                    The buffer ac1Buf should be sufficiently */
/*                    large to prevent overruns.               */
/*                    The size should be based on MAX_LOG_LEN  */
/*                    i4Count and UTL_MAX_LOGS                */
/*  Input(s)        : ac1Buf - Buffer into which to copy logs. */
/*                    i4Count - Number of logs to copy         */
/*                     i4Count < 0 => retrieve the last        */
/*                                    i4Count logs.            */
/*                     i4Count > 0 => retrieve the earliest    */
/*                                    i4Count logs.            */
/*                     i4Count = 0 => retrieve all the logs    */
/*                                    that are available.      */
/*  Output(s)       : Filled ac1Buf.                           */
/*  Returns         : Number of bytes copied to ac1Buf.        */
/***************************************************************/
INT4
UtlGetLogs (CHR1 ac1Buf[], INT4 i4Count)
{
    INT4                i4Len;
    INT4                i4Offset = 0;
    INT4                i4Idx = Logs.i4Front;

    if (i4Count < 0)
    {
        i4Count *= -1;
        i4Count = (i4Count > UTL_MAX_LOGS ? UTL_MAX_LOGS : i4Count);
        i4Idx = Logs.i4Rear - i4Count;
        if (i4Idx < 0)
        {
            i4Idx = UTL_MAX_LOGS + i4Idx;
        }

    }

    if (i4Count == 0)
    {
        i4Idx = Logs.i4Front;
        if (Logs.i4Rear == Logs.i4Front)
        {
            i4Count = UTL_MAX_LOGS;
        }
        else
        {
            i4Count = Logs.i4Rear - Logs.i4Front;
        }
    }

    i4Count = (i4Count > UTL_MAX_LOGS ? UTL_MAX_LOGS : i4Count);

    for (; i4Count; i4Count--)
    {
        i4Len = STRLEN (Logs.Log[i4Idx]);
        MEMCPY (ac1Buf + i4Offset, Logs.Log[i4Idx], i4Len);
        i4Offset += i4Len;

        i4Idx++;
        if (i4Idx == UTL_MAX_LOGS)
        {
            i4Idx = 0;
        }
    }

    return (i4Offset);
}

/***************************************************************/
/*  Function Name   : UtlSetLogMode                            */
/*  Description     : API to dynamically change logging        */
/*                    mechanism.                               */
/*                    The default logging mechanism is defined */
/*                    in osix.h (OSIX_LOG_METHOD)              */
/*                    By default this is set to CONSOLE_LOG    */
/*                    so that the traces appear on screen      */
/*                    Other option is INCORE_LOG which switches*/
/*                    to logging the traces in a circular buf  */
/*  Input(s)        : u4val - Logging mode to be set.          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlSetLogMode (UINT4 u4val)
{
    gu4InCoreLog = u4val;
}

/***************************************************************/
/*  Function Name   : UtlGetLogMode                            */
/*  Description     : API to get the current logging mechanism */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None                                     */
/*  Returns         : One of eLoggingModes.                    */
/***************************************************************/
UINT4
UtlGetLogMode (VOID)
{
    return (gu4InCoreLog);
}

/***************************************************************/
/*  Function Name   : UtlTrcLog                                */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST char *pi1Name,
           CONST char *pi1Fmt, ...)
{
    va_list             VarArgList;
    INT1                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST char         *pi1FmtString;
    INT4                i4ArgCount;
    INT4                pos = 0;

    if (!(u4Flag & u4Value))
    {
        /* Tracing for this type not enabled */
        return;
    }

    if (pi1Name == NULL)
    {
        UtlTrcPrint ("\nInvalid Module Name\n");
        return;
    }

    /* Tagging the Module Name to the front of the message */
    pos += SPRINTF (ai1LogMsgBuf, "%s: ", pi1Name);

    /* Checking for Arguments count limit */
    pi1FmtString = pi1Fmt;
    i4ArgCount = 0;

    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                i4ArgCount++;
            }
        }
        pi1FmtString++;
    }
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }

    va_start (VarArgList, pi1Fmt);

    vsprintf (ai1LogMsgBuf + pos, pi1Fmt, VarArgList);

    UtlTrcPrint (ai1LogMsgBuf);

    va_end (VarArgList);
}

#else /* VAR_ARGS_SUPPORT */
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST INT1 *pi1Name,
           CONST INT1 *pi1Fmt, INT4 p1, INT4 p2, INT4 p3,
           INT4 p4, INT4 p5, INT4 p6)
{
    INT4                i4ArgCount;
    INT1                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST INT1         *pi1FmtString;

    if (!(u4Flag & u4Value))
    {
        /* Tracing for this type not enabled */
        return;
    }

    if (pi1Name == NULL)
    {
        UtlTrcPrint ("\nInvalid Module Name\n");
        return;
    }

    pi1FmtString = pi1Fmt;

    /* Tagging the Module Name to the front of the message */
    SPRINTF (ai1LogMsgBuf, "%s: ", pi1Name);

    UtlTrcPrint (ai1LogMsgBuf);

    /* Count the number of arguments */
    i4ArgCount = 0;

    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                i4ArgCount++;
            }
        }
        pi1FmtString++;
    }

    switch (i4ArgCount)
    {
        case 0:
            SPRINTF (ai1LogMsgBuf, pi1Fmt);
            break;
        case 1:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1);
            break;
        case 2:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2);
            break;
        case 3:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3);
            break;
        case 4:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4);
            break;
        case 5:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5);
            break;
        case 6:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5, p6);
            break;
        default:
            SPRINTF (ai1LogMsgBuf, "Number of arguments > 6.\n");
            break;
    }

    UtlTrcPrint (ai1LogMsgBuf);
}

/****************************************************************************/
 /*                                                                          */
 /*    Function Name      : LoggerTaskMain                                   */
 /*                                                                          */
 /*    Description        : This is the main function of the logger task     */
 /*                                                                          */
 /*    Input(s)           : pi1Param - dummy parameter                       */
 /*                                                                          */
 /*    Output(s)          : None.                                            */
 /*                                                                          */
 /*    Returns            : None.                                            */
 /****************************************************************************/
VOID
LoggerTaskMain (INT1 *pi1Param)
{
    return;
}

#endif /* VAR_ARGS_SUPPORT */

/***************************************************************/
/*  Function Name   : UtlSysTrcLog                             */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4SysLevel - Syslog message levels       */
/*                    u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST char *pi1ModId, CONST char *pi1Name,
           CONST char *pi1Fmt, ...)
{
      UNUSED_PARAM (u4SysLevel);
      UNUSED_PARAM (u4Flag);
      UNUSED_PARAM (u4Value);
      UNUSED_PARAM (pi1Name);
      UNUSED_PARAM (pi1ModId);
      UNUSED_PARAM (pi1Fmt);

}
#else /* VAR_ARGS_SUPPORT */
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST INT1 *pi1ModId, CONST INT1 *pi1Name,
           CONST INT1 *pi1Fmt, INT4 p1, INT4 p2, INT4 p3,
           INT4 p4, INT4 p5, INT4 p6)
{
      UNUSED_PARAM (u4SysLevel);
      UNUSED_PARAM (u4Flag);
      UNUSED_PARAM (u4Value);
      UNUSED_PARAM (pi1Name);
      UNUSED_PARAM (pi1ModId);
      UNUSED_PARAM (pi1Fmt);
      UNUSED_PARAM (p1);
      UNUSED_PARAM (p2);
      UNUSED_PARAM (p3);
      UNUSED_PARAM (p4);
      UNUSED_PARAM (p5);
      UNUSED_PARAM (p6);
}
#endif /* VAR_ARGS_SUPPORT */
