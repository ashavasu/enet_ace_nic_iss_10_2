/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osixqnx.c,v 1.20 2015/04/28 12:17:42 siva Exp $
 *
 * Description: Contains OSIX reference code for QNX.
 *              All basic OS facilities used by protocol software
 *              from FS, use only these APIs.
 */

#include "osxinc.h"
#include "osix.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

/* The basic structure maintaining the name-to-id mapping */
/* of OSIX resources. 3 arrays - one for tasks, one for   */
/* semaphores and one for queues are maintained by OSIX.  */
/* Each array has this structure as the basic element. We */
/* use this array to store events for tasks also.         */

/* The description of fields of the structures below is as follows */
/*   ThrId    - the thread id returned by the OS                   */
/*   u4RscId  - the id returned by the OS                          */
/*   u2Free   - whether this structure is free or used             */
/*   u2TskBlk - for event simulation; is task is blocked on event  */
/*   u4Events - for event simulation; used only for tasks          */
/*   EvtCond  - Conditional variable to synch. Send/Receive Event  */
/*   EvtMutex - Mutex used to synch. task creatione.               */
/*   TskMutex - Mutex used to synchronize send/recv. of events.    */
/*   pTskStrtAddr - Entry Point function.                          */
/*   u4Arg    - argument for Entry Point Function                  */
/*   au1Name  - name is always multiple of 4 characters in length  */
typedef struct OsixRscTskStruct
{
    pthread_t           ThrId;
    UINT2               u2Free;
    UINT2               u2TskBlk;
    UINT4               u4Events;
    pthread_mutex_t     TskMutex;
    pthread_cond_t      EvtCond;
    pthread_mutex_t     EvtMutex;
    void                (*pTskStrtAddr) (INT4);
    UINT4               u4Arg;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixTsk;
typedef struct OsixRscQueStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixQue;
typedef struct OsixRscSemStruct
{
    sem_t               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixSem;

UINT4               gu4Tps = OSIX_TPS;
UINT4               gu4Stups = OSIX_STUPS;

tOsixTsk            gaOsixTsk[OSIX_MAX_TSKS + 1];
/* 0th element is invalid & not used */

tOsixSem            gaOsixSem[OSIX_MAX_SEMS + 1];
/* 0th element is invalid & not used */

tOsixQue            gaOsixQue[OSIX_MAX_QUES + 1];
/* 0th element is invalid & not used */

/* Globals */
sem_t               gOsixMutex;
struct timeval      gSysUpTimeInfo;

static UINT4 OsixRscAdd ARG_LIST ((UINT1[], UINT4, UINT4));
static VOID OsixRscDel ARG_LIST ((UINT4, UINT4));
static VOID        *OsixTskWrapper (void *pArg);
static VOID OsixInitSignals ARG_LIST ((VOID));

extern UINT4 OsixSTUPS2Ticks ARG_LIST ((UINT4));
extern UINT4 OsixTicks2STUPS ARG_LIST ((UINT4));
extern UINT4        gu4Seconds;
/************************************************************************/
/* Routines for task creation, deletion and maintenance                 */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixTskCrt                                        */
/*  Description     : Creates task.                                     */
/*  Input(s)        : au1Name[ ] -        Name of the task              */
/*                  : u4TskPrio -         Task Priority                 */
/*                  : u4StackSize -       Stack size                    */
/*                  : (*TskStartAddr)() - Entry point function          */
/*                  : u4Arg -             Arguments to above fn.        */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskCrt (UINT1 au1Name[], UINT4 u4TskPrio, UINT4 u4StackSize,
            VOID (*TskStartAddr) (INT1 *), INT1 *u4Arg, tOsixTaskId * pTskId)
{
    struct sched_param  SchedParam;
    pthread_attr_t      Attr;
    tOsixTsk           *pTsk = 0;
    UINT4               u4Idx;
    INT4                i4OsPrio;
    UINT1               au1TskName[OSIX_NAME_LEN + 4];

    MEMSET ((UINT1 *) au1TskName, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1TskName, au1Name, OSIX_NAME_LEN);

    /* For tasks, the QNX version of OsixRscAdd does not use   */
    /* the last argument. So anything can be passed; we pass 0. */
    if (OsixRscAdd (au1TskName, OSIX_TSK, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    u4TskPrio = u4TskPrio & 0x0000ffff;

    /* Remap the task priority to Vxworks's range of values. */
    i4OsPrio = OS_LOW_PRIO + (((((INT4) (u4TskPrio) - FSAP_LOW_PRIO) *
                                (OS_HIGH_PRIO -
                                 OS_LOW_PRIO)) / (FSAP_HIGH_PRIO -
                                                  FSAP_LOW_PRIO)));

    if ((i4OsPrio > 10) && (u4TskPrio > 0))
        i4OsPrio = u4TskPrio;

    /* Set the attributes for the thread. */

    if (u4StackSize < OSIX_DEFAULT_STACK_SIZE)
    {
        u4StackSize = OSIX_DEFAULT_STACK_SIZE;
    }

    SchedParam.sched_priority = (UINT4) i4OsPrio;
    pthread_attr_init (&Attr);
    pthread_attr_setstacksize (&Attr, u4StackSize);
    pthread_attr_setinheritsched (&Attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy (&Attr, SCHED_RR);
    pthread_attr_setschedparam (&Attr, &SchedParam);
    pthread_attr_setdetachstate (&Attr, PTHREAD_CREATE_DETACHED);

    /* Get the global task array index to return as the task id. */
    /* Return value check not needed because we just added it.   */
    OsixRscFind (au1TskName, OSIX_TSK, &u4Idx);
    pTsk = &gaOsixTsk[u4Idx];
    *pTskId = (tOsixTaskId) u4Idx;

    /* CondVars and associated mutexes,
     * (1) for task creation synch.
     * (2) for Events mechanism
     */
    pthread_mutex_init (&(pTsk->TskMutex), NULL);
    pthread_cond_init (&(pTsk->EvtCond), NULL);
    pthread_mutex_init (&(pTsk->EvtMutex), NULL);

    /* Store the TaskStartAddr and Args in TCB for further reference */
    pTsk->pTskStrtAddr = TskStartAddr;
    pTsk->u4Arg = u4Arg;

    /* We need the threadId to be stored in pTsk before pthreads gives
     * control to the entry point. So we use a "stub" function OsixTskWrapper
     * and use condvars to ensure an orderly creation.
     */
    pthread_mutex_lock (&(pTsk->TskMutex));

    /* Create the qnx thread. */
    if (pthread_create (&(pTsk->ThrId), &Attr, OsixTskWrapper, (void *) pTsk))
    {
        pthread_mutex_unlock (&(pTsk->TskMutex));

        pthread_mutex_destroy (&(pTsk->TskMutex));
        pthread_mutex_destroy (&(pTsk->EvtMutex));
        pthread_cond_destroy (&(pTsk->EvtCond));

        OsixRscDel (OSIX_TSK, u4Idx);
        return (OSIX_FAILURE);
    }

    /* The OSIX task has been fully created. Now let the thread run. */
    pthread_mutex_unlock (&(pTsk->TskMutex));
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixTskDel                                        */
/*  Description     : Deletes a task.                                   */
/*  Input(s)        : TskId          - Task Id                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixTskDel (tOsixTaskId TskId)
{
    pthread_mutex_destroy (&(gaOsixTsk[(UINT4) TskId].TskMutex));
    pthread_mutex_destroy (&(gaOsixTsk[(UINT4) TskId].EvtMutex));
    pthread_cond_destroy (&(gaOsixTsk[(UINT4) TskId].EvtCond));
    OsixRscDel (OSIX_TSK, (UINT4) TskId);
    pthread_cancel (gaOsixTsk[(UINT4) TskId].ThrId);
}

/************************************************************************/
/*  Function Name   : OsixTskDelay                                      */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskDelay (UINT4 u4Duration)
{
    UINT4               u4Sec;
    struct timespec     timeout;
    UINT4               u4TimeTicks;

    u4TimeTicks = OsixGetTps ();

    u4Duration = OsixSTUPS2Ticks (u4Duration);

    /* Assumption: OsixSTUPS2Ticks converts u4Duration to micro-seconds
     * We convert that into seconds and nano-seconds for the OS call.
     * If the values configured for OSIX_STUPS and OSIX_TPS change, this
     * code should be changed.
     */

    u4Sec = u4Duration / u4TimeTicks;
    timeout.tv_sec = u4Sec;
    timeout.tv_nsec = ((u4Duration - u4Sec * u4TimeTicks) * 1000);
    nanosleep (&timeout, NULL);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixDelay                                         */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*                  : i4Unit     - Units in which the duration is       */
/*                                 specified                            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixDelay (UINT4 u4Duration, INT4 i4Unit)
{
    struct timespec     timeout;
    UINT4               u4Sec = 0;
    UINT4               u4NSec = 0;

    switch (i4Unit)
    {
        case OSIX_SECONDS:
            u4Sec = u4Duration;
            break;

        case OSIX_CENTI_SECONDS:
            u4NSec = u4Duration * 10000000;
            break;

        case OSIX_MILLI_SECONDS:
            u4NSec = u4Duration * 1000000;
            break;

        case OSIX_MICRO_SECONDS:
            u4NSec = u4Duration * 1000;
            break;

        case OSIX_NANO_SECONDS:
            u4NSec = u4Duration;
            break;

        default:
            break;
    }

    timeout.tv_sec = u4Sec;
    timeout.tv_nsec = u4NSec;
    nanosleep (&timeout, NULL);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixTskIdSelf                                     */
/*  Description     : Get Osix Id of current Task                       */
/*  Input(s)        : None                                              */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskIdSelf (tOsixTaskId * pTskId)
{
    UINT4               u4Count;
    pthread_t           ThrId;

    ThrId = pthread_self ();

    for (u4Count = 1; u4Count <= OSIX_MAX_TSKS; u4Count++)
    {
        if ((gaOsixTsk[u4Count].ThrId) == ThrId)
        {
            *pTskId = (tOsixTaskId) u4Count;
            return (OSIX_SUCCESS);
        }
    }
    return (OSIX_FAILURE);
}

/************************************************************************/
/* Routines for event management - send / receive event                 */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixEvtSend                                       */
/*  Description     : Sends an event to a specified task.               */
/*  Input(s)        : TskId          - Task Id                          */
/*                  : u4Events       - The Events, OR-d                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events)
{
    UINT4               u4Idx = (UINT4) TskId;

    pthread_mutex_lock (&gaOsixTsk[u4Idx].EvtMutex);
    gaOsixTsk[u4Idx].u4Events |= u4Events;
    if ((gaOsixTsk[u4Idx].u2TskBlk) == OSIX_TRUE)
    {
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
        pthread_cond_signal (&gaOsixTsk[u4Idx].EvtCond);
    }
    pthread_mutex_unlock (&gaOsixTsk[u4Idx].EvtMutex);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixEvtRecv                                       */
/*  Description     : To receive a event.                               */
/*  Input(s)        : TskId             - Task Id                       */
/*                  : u4Events          - List of interested events.    */
/*  Output(s)       : pu4EventsReceived - Events received.              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flgs,
             UINT4 *pu4RcvEvents)
{
    UINT4               u4Idx = (UINT4) TskId;

    *pu4RcvEvents = 0;

    pthread_mutex_lock (&gaOsixTsk[u4Idx].EvtMutex);

    if ((u4Flgs == OSIX_NO_WAIT)
        && (((gaOsixTsk[u4Idx].u4Events) & u4Events) == 0))
    {
        pthread_mutex_unlock (&gaOsixTsk[u4Idx].EvtMutex);
        return (OSIX_FAILURE);
    }

    while (1)
    {
        if (((gaOsixTsk[u4Idx].u4Events) & u4Events) != 0)
        {
            /** A required event has happened **/
            *pu4RcvEvents = (gaOsixTsk[u4Idx].u4Events) & u4Events;
            gaOsixTsk[u4Idx].u4Events &= ~(*pu4RcvEvents);
            pthread_mutex_unlock (&gaOsixTsk[u4Idx].EvtMutex);
            return (OSIX_SUCCESS);
        }
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_TRUE;

        pthread_cond_wait (&gaOsixTsk[u4Idx].EvtCond,
                           &gaOsixTsk[u4Idx].EvtMutex);
    }
}

/************************************************************************/
/*  Function Name   : OsixInit                                          */
/*  Description     : The OSIX Initialization routine. To be called     */
/*                    before any other OSIX functions are used.         */
/*  Input(s)        : pOsixCfg - Pointer to OSIX config info.           */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixInitialize ()
{
    UINT4               u4Idx;

    /* Mutual exclusion semaphore to add or delete elements from */
    /* name-id-mapping list. This semaphore itself will not be   */
    /* added to the name-to-id mapping list. This is a OS        */
    /* specific call and must be mapped to relevant call for OS  */
    if ((sem_init (&gOsixMutex, 0, 1)) == -1)
    {
        return (OSIX_FAILURE);
    }

    /* Initialize all arrays. */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
        gaOsixTsk[u4Idx].u4Events = 0;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    gettimeofday (&gSysUpTimeInfo, NULL);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSysRestart                                    */
/*  Description     : This function reboots the system.                 */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSysRestart ()
{
    /* Killing the current process for shutdown */
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixShutDown                                      */
/*  Description     : Stops OSIX.                                       */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixShutDown ()
{
    UINT4               u4Idx;

    /* Re-initialize all arrays and delete global semaphore */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        if (gaOsixTsk[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixTskDel (u4Idx);
        }
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
        gaOsixTsk[u4Idx].u4Events = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        if (gaOsixSem[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixSemDel ((tOsixSemId) & (gaOsixSem[u4Idx].u4RscId));
        }
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if (gaOsixQue[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixQueDel ((tOsixQId) & (gaOsixQue[u4Idx].u4RscId));
        }
        gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
    }
    sem_destroy (&gOsixMutex);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing semaphores                                     */
/* Keep it simple - support 1 type of semaphore - binary, blocking.     */
/* After creating, the semaphore must be given before it can be taken.  */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixSemCrt                                        */
/*  Description     : Creates a sema4.                                  */
/*  Input(s)        : au1Name [ ]    - Name of the sema4.               */
/*  Output(s)       : pSemId         - The sema4 Id.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemCrt (UINT1 au1Name[], tOsixSemId * pSemId)
{
    /* For sema4, the QNX version of OsixRscAdd does not use   */
    /* the last argument. So anything can be passed; we pass 0. */
    if (OsixRscAdd (au1Name, OSIX_SEM, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Get the global task array index to return as the task id. */
    /* Return value check not needed because we just added it.   */
    OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) pSemId);

    if (sem_init ((sem_t *) * pSemId, 0, 0) == -1)
    {
        OsixRscDel (OSIX_SEM, (UINT4) *pSemId);
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemDel                                        */
/*  Description     : Deletes a sema4.                                  */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixSemDel (tOsixSemId SemId)
{
    OsixRscDel (OSIX_SEM, (UINT4) SemId);
    sem_destroy ((sem_t *) SemId);
}

/************************************************************************/
/*  Function Name   : OsixSemGive                                       */
/*  Description     : Used to release a sem4.                           */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemGive (tOsixSemId SemId)
{
    if (sem_post ((sem_t *) SemId) < 0)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemTake                                       */
/*  Description     : Used to acquire a sema4.                          */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemTake (tOsixSemId SemId)
{
    if (sem_wait ((sem_t *) SemId) == -1)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing message queues                                 */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixQueCrt                                        */
/*  Description     : Creates a OSIX Q.                                 */
/*  Input(s)        : au1name[ ] - The Name of the Queue.               */
/*                  : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MaxMsgLen- Max length of a messages             */
/*  Output(s)       : pQueId     - The QId returned.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueCrt (UINT1 au1Name[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId)
{
    struct mq_attr      QAttr;
    mqd_t               md;

    /* Assign message queue attributes */
    QAttr.mq_maxmsg = u4MaxMsgs;
    QAttr.mq_msgsize = u4MaxMsgLen;
    QAttr.mq_flags = 0;

    mq_unlink (au1Name);

    /* Open the non blocking queue */
    md = mq_open (au1Name, O_CREAT | O_RDWR /*| O_EXCL */ , 0664, &QAttr);
    if (md == -1)
    {
        perror ("mq_open");
        return (OSIX_FAILURE);
    }
    *pQueId = md;
    if (OsixRscAdd (au1Name, OSIX_QUE, *pQueId) == OSIX_FAILURE)
    {
        mq_unlink (au1Name);
        mq_close (*pQueId);
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueDel                                        */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
void
OsixQueDel (tOsixQId QueId)
{
    UINT1               au1Qname[OSIX_NAME_LEN + 4];
    UINT4               u4Idx;

    sem_wait (&gOsixMutex);
    for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if ((gaOsixQue[u4Idx].u4RscId) == (UINT4) QueId)
        {
            MEMCPY (au1Qname, gaOsixQue[u4Idx].au1Name, (OSIX_NAME_LEN + 4));
            sem_post (&gOsixMutex);
            mq_unlink (au1Qname);
            OsixRscDel (OSIX_QUE, (UINT4) QueId);
            mq_close (QueId);
            return;
        }
    }
    return;
}

/************************************************************************/
/*  Function Name   : OsixQueSend                                       */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*                  : u4MsgLen - length of the messages                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen)
{
    struct mq_attr      mq_att;
    UINT4               i4rc;

    i4rc = mq_getattr (QueId, &mq_att);
    if ((mq_att.mq_curmsgs == mq_att.mq_maxmsg) || (i4rc == -1))
    {
        return OSIX_FAILURE;
    }

    if ((i4rc = mq_send (QueId, (UINT1 *) pu1Msg, u4MsgLen, 0)) == -1)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueRecv                                       */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : u4MsgLen -  length of the messages                */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4Timeout)
{
    struct timespec     currtime;
    struct timespec     Timeout;
    FS_QNX_UINT8        u8TempNanoSec;
    FS_QNX_UINT8        u8Time;
    INT4                i4rc;
    struct mq_attr      Attr;

    if (i4Timeout == 0)
    {
        i4rc = mq_getattr (QueId, &Attr);
        if (i4rc != 0)
        {
            return (OSIX_FAILURE);
        }
        if (Attr.mq_curmsgs != 0)
        {
            i4rc = mq_receive (QueId, (UINT1 *) pu1Msg, u4MsgLen, 0);
        }
        else
        {
            return (OSIX_FAILURE);
        }
    }
    else if (i4Timeout == -1)
    {
        i4rc = mq_receive (QueId, (UINT1 *) pu1Msg, u4MsgLen, 0);
    }
    else
    {
        i4Timeout = (INT4) OsixSTUPS2Ticks ((UINT4) i4Timeout);

        gettimeofday ((struct timeval *) &currtime, NULL);
        u8TempNanoSec = timespec2nsec (&currtime);
        u8Time = i4Timeout * (1000000000 / gu4Tps);
        u8TempNanoSec += u8Time;
        nsec2timespec (&Timeout, u8TempNanoSec);
        i4rc = mq_timedreceive (QueId, pu1Msg, u4MsgLen, 0, &Timeout);
    }
    if (i4rc == -1)
    {
        perror ("Failure reason\n");
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueNumMsgs                                    */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : pu4NumberOfMsgs - Contains count upon return.     */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixQueNumMsg (tOsixQId QueId, UINT4 *pu4NumMsg)
{
    struct mq_attr      mq_a;

    mq_getattr (QueId, &mq_a);
    *pu4NumMsg = mq_a.mq_curmsgs;
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing resources based on names                       */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixRscAdd                                        */
/*  Description     : Gets a free resouce, stores Resource-Id & Name    */
/*                  : This helps in locating Resource-Id by Name        */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (sem_wait (&gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixTsk[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
                    gaOsixTsk[u4Idx].u4Events = 0;
                    MEMCPY (gaOsixTsk[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    sem_post (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixSem[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixSem[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    sem_post (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixQue[u4Idx].u4RscId = u4RscId;
                    gaOsixQue[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixQue[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixQue[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    sem_post (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    sem_post (&gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscDel                                        */
/*  Description     : Free an allocated resouce                         */
/*  Input(s)        : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixRscDel (UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (sem_wait (&gOsixMutex) == OSIX_FAILURE)
    {
        return;
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            gaOsixTsk[u4RscId].u2Free = OSIX_TRUE;
            MEMSET (gaOsixTsk[u4RscId].au1Name, '\0', (OSIX_NAME_LEN + 4));
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (&(gaOsixSem[u4Idx].u4RscId) == (sem_t *) u4RscId)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixSem[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixQue[u4Idx].u4RscId = (tOsixSemId) OSIX_RSC_INV;
                    gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixQue[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        default:
            break;
    }
    sem_post (&gOsixMutex);
}

/************************************************************************/
/*  Function Name   : OsixRscFind                                       */
/*  Description     : This locates Resource-Id by Name                  */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*  Output(s)       : pu4RscId - Osix Resource-Id                       */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }
    if (sem_wait (&gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                /* going on the assumption that length is 4 characters only */
                if (MEMCMP
                    (au1Name, gaOsixTsk[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* For tasks applications know only our array index. */
                    /* This helps us to simulate events.                 */
                    *pu4RscId = u4Idx;
                    sem_post (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixSem[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = (UINT4) &(gaOsixSem[u4Idx].u4RscId);
                    sem_post (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixQue[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = gaOsixQue[u4Idx].u4RscId;
                    sem_post (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    sem_post (&gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : Fsap2Start                                        */
/*  Description     : Function to be called to get any fsap2 application*/
/*                  : to work                                           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Fsap2Start ()
{
    while (1)
    {
        sleep (1);
    }
}

/************************************************************************/
/*  Function Name   : OsixGetSysTime                                    */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysTime (tOsixSysTime * pSysTime)
{
    struct timeval      current;

    gettimeofday (&current, NULL);

    *pSysTime = (current.tv_sec - gSysUpTimeInfo.tv_sec) * 1000000;

    *pSysTime = (*pSysTime * gu4Stups) / 1000000;

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetSysTimeIn64                                */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysTimeIn64Bit (FS_UINT8 * pSysTime)
{
    /* Currently we have support for U4 Only */
    pSysTime->u4Hi = 0;
    OsixGetSysTime (&pSysTime->u4Lo);

    return (OSIX_SUCCESS);

}

/************************************************************************/
/*  Function Name   : OsixGetSysUpTime                                  */
/*  Description     : Returns the system time in Seconds.               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysUpTime (VOID)
{
    struct timespec     stime;

    clock_gettime (CLOCK_MONOTONIC, &stime);

    return stime.tv_sec;
}

/************************************************************************/
/*  Function Name   : OsixGetTps                                        */
/*  Description     : Returns the no of system ticks in a second        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : Number of system ticks in a second                */
/************************************************************************/
UINT4
OsixGetTps ()
{
    struct timespec     resn;    /* to get the resolution of timer in nanoseconds  */

    if (clock_getres (CLOCK_REALTIME, &resn) == -1)
    {
        return (OSIX_FAILURE);
    }

    /* We receive the number of NanoSeconds for each tick in
     * resn.tv_nsec. To convert that into ticks per second, we divide
     * 10^9 (number of nanoseconds in a seconds) with the nanoseconds
     * taken for each tick */

    return (1000000000 / resn.tv_nsec);
}

/************************************************************************/
/*  Function Name   : OsixExGetTaskName                                 */
/*  Description     : Get the OSIX task Name given the Osix Task-Id.    */
/*  Input(s)        : TaskId - The Osix taskId.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to OSIX task name                         */
/************************************************************************/
const UINT1        *
OsixExGetTaskName (tOsixTaskId TskId)
{
    /* To facilitate direct use of this call in a STRCPY,
     * we return a null string instead of a NULL pointer.
     * A null pointer is returned for all non-OSIX tasks
     * e.g., TMO's idle task.
     */
    if (TskId)
    {
        return ((UINT1 *) (gaOsixTsk[(UINT4) TskId].au1Name));
    }
    return ((const UINT1 *) "");
}

/************************************************************************/
/*  Function Name   : OsixTaskWrapper                                   */
/*  Description     : Intermediate function between OsixTskCrt and      */
/*                  : application entry point function, which serves    */
/*                  : to prevent the application from kicking off even  */
/*                  : before CreateTask has completed. The synch is     */
/*                  : accomplished through the use of a per task mutex. */
/*                  : The mutex is 'taken' just before pthread_create   */
/*                  : and is given up just before close of CreateTask.  */
/*  Input(s)        : pArg - task arguments passed here.                */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
static VOID        *
OsixTskWrapper (void *pArg)
{
    void                (*TaskPtr) (INT4);
    INT4                i4Arg;
    tOsixTsk           *pTsk = (tOsixTsk *) pArg;
    tOsixTaskId         TskId;

    /* Waits till OsixCreateTask releases the lock */
    pthread_mutex_lock (&(pTsk->TskMutex));

    /* OsixCreateTask is complete, now releases the lock */
    pthread_mutex_unlock (&(pTsk->TskMutex));

    TaskPtr = pTsk->pTskStrtAddr;
    i4Arg = pTsk->u4Arg;

    /*
     * DEBUG TIP:
     * Use this to see taskname to PID mapping as reported by
     * UNIX 'ps' listing. The threadID is not if much use.
     *
     * printf ("Starting %s with thrid %ld and PID %ld\n", pTsk->au1Name,
     *        (UINT4)pTsk->ThrId, (UINT4)getpid());
     */

    pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    /* Call the actual application Entry Point function. */
    (*TaskPtr) (i4Arg);

    /* Delete the task created */
    OsixTskIdSelf (&TskId);
    OsixTskDel (TskId);

    return ((void *) NULL);
}

#if (FILESYS_SUPPORT == FSAP_ON)
/************************************************************************
 *  Function Name   : FileOpen
 *  Description     : Function to Open a file.
 *  Input           : pu1FileName - Name of file to open.
 *                    i4Mode      - whether r/w/rw.
 *  Returns         : FileDescriptor if successful
 *                    -1             otherwise.
 ************************************************************************/
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    INT4                i4Mode = 0;

    if (i4InMode & OSIX_FILE_CR)
    {
        i4Mode |= O_CREAT;
    }
    if (i4InMode & OSIX_FILE_RO)
    {
        i4Mode |= O_RDONLY;
    }
    else if (i4InMode & OSIX_FILE_WO)
    {
        i4Mode |= O_WRONLY;
        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }
    else if (i4InMode & OSIX_FILE_RW)
    {
        i4Mode |= O_RDWR;
        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }

    return open ((const CHR1 *) pu1FileName, i4Mode, 0644);
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description     : Function to close a file.
 *  Input           : i4Fd - File Descriptor to be closed.
 *  Returns         : 0 on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    return close (i4Fd);
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description     : Function to read a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer into which to read
 *                    i4Count - Number of bytes to read
 *  Returns         : Number of bytes read on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    return read (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description     : Function to write to a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : Number of bytes written on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    return write (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description     : Function to delete a file.
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : 0 on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    return unlink ((const CHR1 *) pu1FileName);
}

/************************************************************************
 *  Function Name   : FileSize  
 *  Description     : Function to return file size
 *  Input           : i4Fd - File Descriptor                    
 *  Returns         : Length on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileSize (INT4 i4Fd)
{
    struct stat         buffer;
    if (fstat (i4Fd, &buffer) < 0)
    {
        return (-1);
    }
    return buffer.st_size;

}

/************************************************************************
 *  Function Name   : FileTruncate  
 *  Description     : Function to truncate a file
 *  Input           : i4Fd - File Descriptor                    
 *                    i4Size - File size. Bytes after this size will be
 *                    truncated.                       
 *  Returns         : Length on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileTruncate (INT4 i4Fd, INT4 i4Size)
{
    return ftruncate (i4Fd, i4Size);
}

/****************************************************************************
 *  Function Name   : FileSeek
 *  Description     : Function to move the file descriptor for the
 *                    given offset in accordance with the whence.
 *  Input           : i4Fd     - File Descriptor.
 *                    i4Offset - bytes for how many bytes the file descriptor
 *                               needs to be moved.
 *                    i4Whence - it takes one of the following 3 values.
 *                    SEEK_SET - offset is set to offset bytes.
 *                    SEEK_CUR - offset is set to its current location
 *                               plus offset bytes.
 *                    SEEK_END - offset is set to the size of the file
 *                               plus offset bytes.
 *  Returns         : final offset location in SUCCESS case, -1 otherwise.
 ****************************************************************************/
INT4
FileSeek (INT4 i4Fd, INT4 i4Offset, INT4 i4Whence)
{
    i4Fd = i4Fd;
    i4Offset = i4Offset;
    i4Whence = i4Whence;
    return (-1);
}

/************************************************************************
 *  Function Name   : FileStat  
 *  Description     : Function to check if a file is present.
 *  Input           : pc1FileName - Name of the File to be checked. 
 *  Returns         : OSIX_SUCCESS - If the file exists.
 *                    OSIX_FAILURE - If the file does not exist.
 ************************************************************************/
INT4
FileStat (const CHR1 * pc1FileName)
{
    struct stat         FileInfo;
    INT4                i4FileExists = -1;

    if (pc1FileName != NULL)
    {
        /* Attempt to get the file attributes */
        i4FileExists = stat (pc1FileName, &FileInfo);
        if (i4FileExists == 0)
        {
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixCreateDir 
 *  Description     : This function used to create a directory.
 *  Inputs          : pc1DirName - Name of the Directory.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
OsixCreateDir (const CHR1 * pc1DirName)
{
    if (mkdir (pc1DirName, S_IRWXU) == -1)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#else
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    pu1FileName = pu1FileName;
    i4InMode = i4InMode;
    return (-1);
}

INT4
FileClose (INT4 i4Fd)
{
    i4Fd = i4Fd;
    return (-1);
}

UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

INT4
FileDelete (const UINT1 *pu1FileName)
{
    pu1FileName = pu1FileName;
    return (-1);
}

INT4
FileSeek (INT4 i4Fd, INT4 i4Offset, INT4 i4Whence)
{
    i4Fd = i4Fd;
    i4Offset = i4Offset;
    i4Whence = i4Whence;
    return (-1);
}

INT4
FileStat (const CHR1 * pc1FileName)
{
    pc1FileName = pc1FileName;
    return (-1);
}

INT4
OsixCreateDir (const CHR1 * pc1DirName)
{
    pc1DirName = pc1DirName;
    return (-1);
}
#endif

/************************************************************************/
/*  Function Name   : OsixSetLocalTime                                  */
/*  Description     : Obtains the system time and sets it in FSAP.      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSetLocalTime (void)
{
    time_t              t;
    struct tm          *tm, tm_r;

    time (&t);
    tm = localtime_r (&t, &tm_r);
    if (tm == NULL)
    {
        return (OSIX_FAILURE);
    }
    tm_r.tm_year += (1900);
    UtlSetTime ((tUtlTm *) & tm_r);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixGetMemInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4TotalMem - Total memory
 *                    pu4FreeMem - Free memory
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetMemInfo (UINT4 *pu4TotalMem, UINT4 *pu4FreeMem)
{
    *pu4TotalMem = 0;
    *pu4FreeMem = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetCPUInfo
 *  Description     : Function used to get the CPU utilization.
 *  Inputs          : None
 *  OutPuts         : pu4TotalUsage - Total Usage
 *                  : pu4CPUUsage - CPU Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetCPUInfo (UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage)
{
    *pu4TotalUsage = 0;
    *pu4CPUUsage = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetFlashInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4FlashUsage - Flash Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetFlashInfo (UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash)
{
    *pu4TotalUsage = 0;
    *pu4FreeFlash = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetBackTrace
 *  Description     : This function used to get the backtrace of the
 *                    current task. This function to be called in
 *                    specific scenario like MemFailure etc, to find
 *                    exact backtrace at that moment.
 *                    This function will create stacktrace.txt file
 *                    in the flash which contains backtrace along with
 *                    the current task name.
 *                    Note - If the backtrace file is fully filled, then
 *                    it will again wrap around on this file.
 *  Inputs          : None
 *  OutPuts         : None
 *  Returns         : None
 ************************************************************************/

VOID
OsixGetBackTrace (VOID)
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixInitSignals                                   */
/*  Description     : This function gets the signal from                */
/*                    signal handler.                                   */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
VOID
OsixInitSignals (VOID)
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixCoreDumpPathSetting                           */
/*  Description     : This function gets the corefile                   */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT1
OsixCoreDumpPathSetting (UINT1 *filepath)
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                    to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}
