/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmtmri.h,v 1.14 2015/04/28 12:17:42 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/
#define TMR_FREE    0
#define TMR_USED    1

/************************************************************************
*                                                                       *
*                      Defines and Typedefs                             *
*                                                                       *
*************************************************************************/

/*** OS specific code begin ***/
typedef struct _pulse tPulse;
#define OSIX_TIMER_PULSE          1       /* pulse from timer */
/*** OS specific code end ***/

/********************************************
*** Application Timer List Data Structure ***
*********************************************/
typedef struct TMO_APP_TIMER_LIST{
    tTMO_DLL    Link;
    INT4        i4RemainingTime;           /*** Time till next expiry ***/
    tOsixTaskId TskId;
    UINT4       u4Event;
    UINT4       u4Status;
    void        (*CallBackFunction)(tTimerListId);
    tOsixSemId  SemId;
   /* 
    * The List of timers which have expired.
    * To be read by the application.
    */
   tTMO_DLL       ExpdList;
}tTmrAppTimerList;

typedef tTmrAppTimerList tTimerList;
/************************************************************************
*                                                                       *
*                          Macro                                        *
*                                                                       *
*************************************************************************/
#define TMR_MUTEX_NAME (const UINT1 *)"TMMU"
#define TMR_ENTER_CS() (OsixSemTake(TmrMutex))
#define TMR_LEAVE_CS() (OsixSemGive(TmrMutex))

#define TMR_DBG_FLAG gu4TmrDbg
#define TMR_MODNAME  "TMR"

#if DEBUG_TMR == ON
#define TMR_DBG(x) UtlTrcLog x
#else
#define TMR_DBG(x)

#endif
#define TMR_PRNT(x) UtlTrcPrint(x)

VOID TmrDumpList();
UINT4 TmrLock (VOID);
UINT4 TmrUnLock (UINT4 u4s);
