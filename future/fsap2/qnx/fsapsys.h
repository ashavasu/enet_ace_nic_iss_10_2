/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapsys.h,v 1.11 2015/04/28 12:17:41 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */
#define MSG_NOSIGNAL 0
#define ifr_netmask ifr_addr
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <assert.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
