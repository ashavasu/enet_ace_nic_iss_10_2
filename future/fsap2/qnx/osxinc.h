/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id
 *
 * Description:
 * Header of all headers.
 *
 */
#ifndef OSIXINC_H
#define OSIXINC_H

/*** OS specific code begin ***/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <pthread.h>
#include <mqueue.h>
#include <sched.h>
#include <semaphore.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <sys/time.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/siginfo.h>
#include <sys/neutrino.h>

#ifdef MEM_FREE
#undef MEM_FREE
#endif

#include "osxstd.h"
#include "srmmem.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utlmacro.h"
#include "utltrc.h"
#include "osxprot.h"
#include "utlsll.h"
#include "utlhash.h"

#endif
