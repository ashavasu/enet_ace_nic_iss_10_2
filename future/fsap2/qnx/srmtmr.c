/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmtmr.c,v 1.18 2015/04/28 12:17:42 siva Exp $
 *
 * Description:
 * The SRM Timer Module.
 *
 */

/************************************************************************
*                                                                       *
*                          Header  Files                                *
*                                                                       *
*************************************************************************/
#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "osix.h"

#include "utldll.h"
#include "srmtmr.h"
#include "srmtmri.h"
#include "utltrc.h"
#include "utlmacro.h"

/************************************************************************
*                                                                       *
*               Internal Function Prototypes                            *
*                                                                       *
*************************************************************************/

/*** OS specific code begin ***/
INT4                gi4QnxTmrChid;    /* channel ID (global) */
static UINT4        OsixStartQnxTmrTask ();
static UINT4        OsixStopQnxTmrTask ();
static VOID         OsixQnxTmrTask (INT4);
static INT4         OsixQnxSetupPulseAndTimer ();

#define OSIX_PRNT UtlTrcPrint
/*** OS specific code end ***/

UINT4               gu4Seconds;
UINT4               gu4StupsCounter;
static UINT4        gu4TmrInitialized = 0;
static tOsixTaskId  TmrTaskId;
/* Indicates if tmr module is initialized (1) or not (0). */
UINT4
 
            TmrDeleteNode (tTimerListId TimerListId, tTmrAppTimer * pAppTimer);

/************************************************************************
*                                                                       *
*               Internal Static Global Variables                        *
*                                                                       *
*************************************************************************/
static tTmrAppTimerList *gaTimerLists;    /* The timer lists            */
static UINT4        gu4MaxLists;    /* Maximum no. of timer lists */
tOsixSemId          TmrMutex;    /* Mutex used within Tmr      */
#if (DEBUG_TMR == FSAP_ON)
static UINT4        gu4TmrDbg = TMR_DBG_MINOR | TMR_DBG_MAJOR |
    TMR_DBG_CRITICAL | TMR_DBG_FATAL;
#endif

extern UINT4        gu4Tps;
extern UINT4        gu4Stups;

/* Enums for u2Flags field of tTmrAppTimer - which will be used by Application*/
enum
{
    TMR_RUNNING = 0x1,
    TMR_EXPD = 0x2
};

/************************************************************************/
/*  Function Name   : TmrTimerInit                                      */
/*  Description     : Creates timer lists, initializes the active and   */
/*                  : Expired Timer Lists.                              */
/*  Input(s)        :                                                   */
/*                  : pTimerCfg - Pointer to configuration structure.   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrTimerInit (tTimerCfg * pTimerCfg)
{
    UINT4               u4rc = TMR_SUCCESS;
    UINT4               i;
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    if (!gu4TmrInitialized)
    {
        gaTimerLists =
            MEM_MALLOC ((sizeof (tTimerList) * pTimerCfg->u4MaxTimerLists),
                        tTimerList);

        if (gaTimerLists == NULL)
            return TMR_FAILURE;

        gu4MaxLists = pTimerCfg->u4MaxTimerLists;

        MEMSET (gaTimerLists, 0, gu4MaxLists * sizeof (tTimerList));

        for (i = 0; i < gu4MaxLists; ++i)
        {
            TMO_DLL_Init (&gaTimerLists[i].Link);

            TMO_DLL_Init (&gaTimerLists[i].ExpdList);
        }

        MEMSET (au1Name, '\0', (OSIX_NAME_LEN + 4));
        STRCPY (au1Name, "TMMU");
        u4rc = OsixSemCrt (au1Name, &TmrMutex);

        if (u4rc)
        {
            MEM_FREE (gaTimerLists);
            return OSIX_FAILURE;
        }

        OsixSemGive (TmrMutex);

        /* Start timer task. */
        if (OsixStartQnxTmrTask () != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        gu4TmrInitialized = 1;
        gu4Seconds = 0;
        gu4StupsCounter = gu4Stups;

        return (TMR_SUCCESS);
    }

    return (TMR_FAILURE);
}

/************************************************************************/
/*  Function Name   : TmrTimerShutdown                                  */
/*  Description     : Frees resources allocated in Init.                */
/*                  : To be called when closing down timer service.     */
/*                  : Expired Timer Lists.                              */
/*                  : Timer blocks present will have to be reclaimed    */
/*                  : by the applications, before calling this routine. */
/*  Input(s)        :                                                   */
/*                  : pTimerCfg - Pointer to configuration structure.   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrTimerShutdown (void)
{
    if (gu4TmrInitialized == 0)
    {
        return (TMR_FAILURE);
    }

    /* NOTE:
     * To prevent shutdown of timer task even before it is started,
     * we add a safety delay. Otherwise if Timertask is stopped,
     * even before it gets control, QNX hangs. '5' is an estimate
     * which works. Increase it if needed.
     */
    OsixTskDelay (5);

    OsixStopQnxTmrTask ();
    OsixSemDel (TmrMutex);
    MEM_FREE (gaTimerLists);
    gu4TmrInitialized = 0;
    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrCreateTimerList                                */
/*  Description     : Creates a timer list.                             */
/*  Input(s)        :                                                   */
/*                  : au1TaskName  - Name of the task, in order to send */
/*                  :                Event.                             */
/*                  : u4Event - Event corresponding to timeout.         */
/*                  : *CallBackFunction - Callback function to be       */
/*                  : in case it is given. Presence of a callback fn.   */
/*                  : takes higher precedence in the matter of reporting*/
/*                  : timeouts.                                         */
/*  Output(s)       :                                                   */
/*                  : pTimerListId - Handle to created timer list.      */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrCreateTimerList (const UINT1 au1TaskName[4],
                    UINT4 u4Event,
                    void (*CallBackFunction) (tTimerListId),
                    tTimerListId * pTimerListId)
{
    UINT4               u4Count;
    tTmrAppTimerList   *pTimerList = 0;
    tOsixTaskId         TaskId = 0;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    if (CallBackFunction == NULL && au1TaskName == NULL)
        return TMR_FAILURE;

    /* If the expiry notification is via an event to the task, then,
     * ensure the task has been created.
     * The TaskId is required to post the expiry event event.
     * See: TmrProcessTick.
     */
    if (!CallBackFunction)
    {
        MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
        for (u1Index = 0;
             ((u1Index < OSIX_NAME_LEN) && (au1TaskName[u1Index] != '\0'));
             u1Index++)
        {
            au1Name[u1Index] = au1TaskName[u1Index];
        }

        if (OsixRscFind (au1Name, OSIX_TSK, &TaskId) == OSIX_FAILURE)
        {
            return (TMR_FAILURE);
        }
    }

    TMR_ENTER_CS ();
    for (u4Count = 0; u4Count < gu4MaxLists; u4Count++)
        if (gaTimerLists[u4Count].u4Status == TMR_FREE)
        {
            gaTimerLists[u4Count].u4Status = TMR_USED;
            pTimerList = &gaTimerLists[u4Count];
            break;
        }
    if (u4Count == gu4MaxLists)
    {
        TMR_LEAVE_CS ();
        return TMR_FAILURE;
    }

    TMR_LEAVE_CS ();

    *pTimerListId = (tTimerListId) pTimerList;
    pTimerList->TskId = TaskId;
    pTimerList->u4Event = u4Event;
    pTimerList->i4RemainingTime = 0;
    pTimerList->CallBackFunction = CallBackFunction;

    TMO_DLL_Init (&pTimerList->Link);
    TMO_DLL_Init (&pTimerList->ExpdList);

    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrDeleteTimerList                                */
/*  Description     : Deletes a timer list.                             */
/*  Input(s)        :                                                   */
/*                  : TimerListId - Handle to the timer list.           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrDeleteTimerList (tTimerListId TimerListId)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;

    TMR_ENTER_CS ();

    if (pTimerList->u4Status != TMR_USED)
    {
        TMR_LEAVE_CS ();
        return TMR_FAILURE;
    }
    pTimerList->u4Status = TMR_FREE;
    TMR_LEAVE_CS ();
    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrStartTimer                                     */
/*  Description     : Starts a timer of a specified duration.           */
/*                  : The duration has to be specified in terms of STUPS*/
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list in which the timer   */
/*                  :               is to be started                    */
/*                  : pReference -  The timer block which will get      */
/*                  :               linked into the timer list          */
/*                  : u4Duration -  Timeout value.                      */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrStartTimer (tTimerListId TimerListId,
               tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmrNode;
    tTMO_DLL_NODE      *p, *n;
    tTMO_DLL_NODE      *pDLLNode;
    tTMO_DLL           *pList = &pTimerList->Link;
    tTMO_DLL           *pDLL;
    UINT4               elem;
    UINT4               u4val;
    UINT4               u4NumBlocks;
    UINT4               u4Incoming = u4Duration;

    u4val = TmrLock ();

    /* The magic number field has this specific value if the */
    /* timer is running. So, stop the timer and then start.  */
    /* This prevents problems due of buggy application code. */
    if ((pAppTimer->u4MagicNumber) == 0x01020304)
    {
        TmrDeleteNode (TimerListId, pAppTimer);
    }

    if (u4Incoming == 0)
    {
        /* We don't allow a timer of 0 seconds to be started.
         * But if used, it results in the stopping of a running
         * timer in the spirit of the unix alarm call
         */
        TmrUnLock (u4val);
        return TMR_FAILURE;
    }

    pAppTimer->i4RemainingTime = u4Duration;

    u4NumBlocks = TMO_DLL_Count (pList);
    if (u4Duration <= (UINT4) pTimerList->i4RemainingTime || (u4NumBlocks == 0))
    {
        tTMO_DLL_NODE      *pNextDLLNode = TMO_DLL_First (pList);
        if (pNextDLLNode == NULL)
            pNextDLLNode = (tTMO_DLL_NODE *) pList;

        pAppTimer->u4MagicNumber = 0x01020304;
        pAppTimer->u2Flags = TMR_RUNNING;

        TMO_DLL_Insert_In_Middle (pList, (tTMO_DLL_NODE *) pList,    /* prev */
                                  (tTMO_DLL_NODE *) pAppTimer,    /* Mid  */
                                  pNextDLLNode);    /* Next */

        /* if there's a second elem */
        if (u4NumBlocks >= 1)
        {
            ((tTmrAppTimer *) pNextDLLNode)->i4RemainingTime =
                pTimerList->i4RemainingTime - u4Duration;
        }
        pTimerList->i4RemainingTime = u4Duration;
    }
    else
    {
        INT4                i = 0;
        u4Duration -= pTimerList->i4RemainingTime;

        pDLL = &pTimerList->Link;
        p = pDLLNode = TMO_DLL_First (pDLL);

        TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
        {
            /* Skip first */
            if (!i)
            {
                i++;
                continue;
            }
            elem = pTmrNode->i4RemainingTime;
            if (elem > u4Duration)
            {
                break;
            }
            i++;
            u4Duration -= elem;
            p = (tTMO_DLL_NODE *) pTmrNode;
        }

        n = TMO_DLL_Next (pDLL, p);

        pAppTimer->i4RemainingTime = u4Duration;
        pAppTimer->u4MagicNumber = 0x01020304;
        pAppTimer->u2Flags = TMR_RUNNING;

        if (n == NULL)
        {
            n = &pDLL->Head;
            TMO_DLL_Insert_In_Middle (pDLL, p, (tTMO_DLL_NODE *) pAppTimer, n);
        }
        else
        {
            TMO_DLL_Insert_In_Middle (pDLL, p, (tTMO_DLL_NODE *) pAppTimer, n);
            ((tTmrAppTimer *) n)->i4RemainingTime -= u4Duration;
        }
    }
    TmrUnLock (u4val);

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "Added %d .. \n",
              u4Incoming));
#if (DEBUG_TMR == FSAP_ON)
    TmrDumpList ((UINT4) pTimerList);
#endif
    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrStopTimer                                      */
/*  Description     : Stops a timer, if it is active                    */
/*                  : If it has expired the return value indicates so   */
/*                  : In either case the timer block is removed from    */
/*                  : the timer list.                                   */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The Handle to the timer list.       */
/*                  : pReference  - Timer block.                        */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrStopTimer (tTimerListId TimerListId, tTmrAppTimer * pAppTimer)
{
    UINT4               u4val;
    UINT4               u4rc;

    u4val = TmrLock ();
    u4rc = TmrDeleteNode (TimerListId, pAppTimer);
    TmrUnLock (u4val);

    return (u4rc);
}

UINT4
TmrDeleteNode (tTimerListId TimerListId, tTmrAppTimer * pAppTimer)
{
    tTMO_DLL           *pDLL;
    tTmrAppTimer       *pNextTimer;
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    UINT4               u4rc = TMR_SUCCESS;

    if (!pAppTimer || !pTimerList)
        return (TMR_FAILURE);

    pDLL = &pTimerList->Link;

   /*************************************************
    *** Check if the timer is present in any list ****
    **************************************************/
    if (!TMO_DLL_Find (pDLL, (tTMO_DLL_NODE *) pAppTimer))
    {
        pDLL = &pTimerList->ExpdList;
        if (!TMO_DLL_Find (pDLL, (tTMO_DLL_NODE *) pAppTimer))
        {
            return TMR_FAILURE;
        }
        u4rc = TMR_EXPIRED;
    }

    if (u4rc != TMR_EXPIRED)
    {
        pNextTimer =
            (tTmrAppTimer *) TMO_DLL_Next (pDLL, (tTMO_DLL_NODE *) pAppTimer);

        if (pAppTimer == (tTmrAppTimer *) TMO_DLL_First (pDLL))
        {
            if (pNextTimer)
            {
                pTimerList->i4RemainingTime += pNextTimer->i4RemainingTime;
            }
            else
            {
                pTimerList->i4RemainingTime = 0;
            }
        }
        else
        {
            if (pNextTimer)
            {
                pNextTimer->i4RemainingTime += pAppTimer->i4RemainingTime;
            }
            else
            {
                /* rear end. nothing to do. */
            }
        }

        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "Stopped: %d\n",
                  pAppTimer->u4Data));
    }
    TMO_DLL_Delete (pDLL, (tTMO_DLL_NODE *) pAppTimer);
    pAppTimer->u4MagicNumber = 0;
    pAppTimer->u2Flags = TMR_EXPD;

#if (DEBUG_TMR == FSAP_ON)
    TmrDumpList ((UINT4) pTimerList);
#endif
    return u4rc;
}

/************************************************************************/
/*  Function Name   : TmrResizeTimer                                    */
/*  Description     : Resizes a running timer.                          */
/*                  : Trying to resize a timer to a value into the past */
/*                  : will result in failure.                           */
/*  Input(s)        :                                                   */
/*                  : TimerListId - Handle to the timer list.           */
/*                  : pReference  - Timer block.                        */
/*                  : u4Duration  - Resized value of timeout.           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrResizeTimer (tTimerListId TimerListId,
                tTmrAppTimer * pReference, UINT4 u4Duration)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmrNode;
    tTMO_DLL           *pList = &pTimerList->Link;
    INT4                i = 0;
    UINT4               u4CumTicksRem;
    UINT4               u4Incoming;
    UINT4               u4rc;
    UINT4               u4val;

    u4val = TmrLock ();
    u4CumTicksRem = pTimerList->i4RemainingTime;
    TMO_DLL_Scan (pList, pTmrNode, tTmrAppTimer *)
    {
        if (!i)
        {
            if (pTmrNode == pReference)
                break;
            i++;
            continue;
        }
        u4CumTicksRem += pTmrNode->i4RemainingTime;
        if (pTmrNode == pReference)
            break;
    }
    if (pTmrNode == NULL)
    {
        TmrUnLock (u4val);
        return TMR_FAILURE;
    }

    u4Incoming = u4Duration;

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "You want  %c [%d] to be resized to %d\n",
              pTmrNode->u4Data, u4CumTicksRem, u4Duration));
    if (u4Duration < u4CumTicksRem)
    {
        TmrUnLock (u4val);
        return TMR_FAILURE;
    }
    else if (u4CumTicksRem == u4Duration)
    {
        TmrUnLock (u4val);
        return TMR_SUCCESS;
    }
    else
    {
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "stopping..\n"));
        TmrUnLock (u4val);
        TmrStopTimer (TimerListId, pTmrNode);
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "restarting..\n"));
        u4rc = TmrStartTimer (TimerListId, pTmrNode, u4Incoming);
    }
    return u4rc;
}

/************************************************************************/
/*  Function Name   : TmrGetExpiredTimers                               */
/*  Description     : API to be called to retrieve Expired timers.      */
/*  Input(s)        :                                                   */
/*                  : TimerListId -  The timer list from which to get.  */
/*  Output(s)       :                                                   */
/*                  : ppExpiredTimers - Contains ptr. to expired timer. */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrGetExpiredTimers (tTimerListId TimerListId, tTmrAppTimer ** ppExpiredTimers)
{
    UINT4               u4val;

    tTimerList         *pTimerList = (tTimerList *) TimerListId;

    u4val = TmrLock ();
    *ppExpiredTimers = (tTmrAppTimer *) TMO_DLL_Get (&pTimerList->ExpdList);
    if (*ppExpiredTimers)
    {
        (*ppExpiredTimers)->u4MagicNumber = 0;
        (*ppExpiredTimers)->u2Flags = TMR_EXPD;
    }
    TmrUnLock (u4val);

    return (*ppExpiredTimers ? TMR_SUCCESS : TMR_FAILURE);
}

/************************************************************************/
/*  Function Name   : TmrGetRemainingTime                               */
/*  Description     : API to get the time to expire for a timer.        */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list in which the timer is*/
/*                  : pReference -  The Timer.                          */
/*  Output(s)       :                                                   */
/*                  : pu4RemainingTime - Time to expire                 */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrGetRemainingTime (tTimerListId TimerListId,
                     tTmrAppTimer * pReference, UINT4 *pu4RemainingTime)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmrNode;
    tTMO_DLL           *pList = &pTimerList->Link;
    INT4                i = 0;

    *pu4RemainingTime = pTimerList->i4RemainingTime;

    TMO_DLL_Scan (pList, pTmrNode, tTmrAppTimer *)
    {
        if (!i)
        {
            if (pTmrNode == pReference)
                break;
            i++;
            continue;
        }
        *pu4RemainingTime += pTmrNode->i4RemainingTime;
        if (pTmrNode == pReference)
            break;
    }

    if (pTmrNode == NULL)
        return TMR_FAILURE;

    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrPrintTimerStatistics                           */
/*  Description     : API to get info on active timers.                 */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list for which stats. is  */
/*                  : sought.                                           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrPrintTimerStatistics (tTimerListId TimerListId)
{
    tTMO_DLL           *pDLL;
    tTmrAppTimerList   *pTimerList;
    tTmrAppTimer       *pTmrNode;
    char                ai1buf[100];

    pTimerList = (tTimerList *) TimerListId;
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", "[ "));
    pDLL = &pTimerList->Link;
    TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
    {
        SPRINTF (ai1buf, "%ld ", pTmrNode->i4RemainingTime);
        if (pTmrNode->i4RemainingTime < 0)
        {
            TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_FATAL, TMR_MODNAME,
                      "panic negative val.\n"));
        }
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", " ]\n\n"));

    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrProcessTick                                    */
/*  Description     : Function called on every time-tick. Updates the   */
/*                  : timer list and intimates the tasks in case of     */
/*                  : timeout.                                          */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
void
TmrProcessTick (void)
{
    tTmrAppTimerList   *pTimerList;
    tTMO_DLL           *pDLL;
    tTMO_DLL_NODE      *pDLLNode;
    tTmrAppTimer       *pTmrNode;
    UINT4               u4Count;
    UINT4               u4Found = 0;
    tTMO_DLL_NODE      *pStart, *pEnd;

    /* To provide time in seconds, to the applications, a global
     * variable gu4Seconds is incremented each second.
     * The variable gu4StupsCounter is loaded with the value of STUPS
     * and gets decremented each tick. Thus at the end of gu4Stups
     * ticks, one second would have elapsed.
     */
    gu4StupsCounter--;
    if (gu4StupsCounter == 0)
    {
        gu4Seconds++;
        gu4StupsCounter = gu4Stups;
    }

    for (pTimerList = &gaTimerLists[0], u4Count = 0;
         u4Count < gu4MaxLists; u4Count++, pTimerList++, u4Found = 0)
    {
        if (gaTimerLists[u4Count].u4Status == TMR_USED)
            if (--pTimerList->i4RemainingTime == 0)
            {
                pDLL = &pTimerList->Link;
                pDLLNode = TMO_DLL_Get (pDLL);
                TMO_DLL_Add (&pTimerList->ExpdList, pDLLNode);

                TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME,
                          "Timeout: %d\n",
                          ((tTmrAppTimer *) pDLLNode)->u4Data));

                pStart = pEnd = NULL;
                TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
                {
                    if (pTmrNode->i4RemainingTime != 0)
                    {
                        break;
                    }
                    pEnd = (tTMO_DLL_NODE *) pTmrNode;
                    u4Found = 1;
                }
                if (u4Found)
                {
                    do
                    {
                        pDLL = &pTimerList->Link;
                        pDLLNode = TMO_DLL_Get (pDLL);
                        if (NULL != pDLLNode)    /* DW added check */
                            TMO_DLL_Add (&pTimerList->ExpdList, pDLLNode);
                        pStart = pDLLNode;
                    }
                    while (pStart != pEnd);
                }

                if ((pDLLNode = TMO_DLL_First (pDLL)))
                    pTimerList->i4RemainingTime =
                        ((tTmrAppTimer *) pDLLNode)->i4RemainingTime;

#if (DEBUG_TMR == FSAP_ON)
                TmrDumpList (u4Count);
#endif
                if (pTimerList->CallBackFunction)
                    (pTimerList->CallBackFunction) ((tTimerListId) pTimerList);
                else
                    OsixEvtSend (pTimerList->TskId, pTimerList->u4Event);
            }
    }
}

/************************************************************************/
/*  Function Name   : TmrDumpList                                       */
/*  Description     : Used to dump the timer list.                      */
/*                  : Used for debugging purposes.                      */
/*  Input(s)        : u4Count - Specifies the timer list.               */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
VOID
TmrDumpList (UINT4 u4Count)
{
    UINT4               pos;
    tTMO_DLL           *pDLL;
    tTmrAppTimerList   *pTimerList;
    tTmrAppTimer       *pTmrNode;

    char                ai1buf[400];
    /* Note: This size is to be increased if you have many many timers
       in the list. */

    if (u4Count < 1000)
    {
        pTimerList = &gaTimerLists[u4Count];
        pos = SPRINTF (ai1buf, "list-%ld: ", u4Count);
    }
    else
    {
        pTimerList = (tTimerList *) u4Count;
        pos = SPRINTF (ai1buf, "list-%ld: ",
                       (u4Count -
                        (UINT4) gaTimerLists) / sizeof (gaTimerLists[0]));
    }

    pos += SPRINTF (ai1buf + pos, "<%ld> [ ", pTimerList->i4RemainingTime);

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));

    pDLL = &pTimerList->Link;
    TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
    {
        SPRINTF (ai1buf, "%ld ", pTmrNode->i4RemainingTime);
        if (pTmrNode->i4RemainingTime < 0)
        {
            TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_FATAL, TMR_MODNAME,
                      "PANIC NEGATIVE VAL.\n"));
        }
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", "]["));
    /* List as mnemonics  - provided user had filled the u4Data field. */
    TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
    {
        SPRINTF (ai1buf, "%ld ", pTmrNode->u4Data);
        if (pTmrNode->i4RemainingTime < 0)
        {
            TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_FATAL, TMR_MODNAME,
                      "panic negative val.\n"));
        }
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", " ]\n\n"));

}

/************************************************************************/
/*  Function Name   : TmrGetNextExpiredTimer                            */
/*  Description     : API to get the expired timers from a timer list.  */
/*  Input(s)        : TimerListId - The timer list.                     */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
tTmrAppTimer       *
TmrGetNextExpiredTimer (tTimerListId TimerListId)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmr;
    UINT4               u4val;

    u4val = TmrLock ();
    pTmr = (tTmrAppTimer *) TMO_DLL_Get (&pTimerList->ExpdList);
    if (pTmr)
    {
        pTmr->u4MagicNumber = 0;
        pTmr->u2Flags = TMR_EXPD;
    }
    TmrUnLock (u4val);
    return pTmr;
}

/************************************************************************/
/*  Function Name   : TmrSetDbg                                         */
/*  Description     : API for changing debug level at run time.         */
/*                  :                                                   */
/*  Input(s)        : u4Value - New value of debug mask.                */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
#if DEBUG_TMR == FSAP_ON
VOID
TmrSetDbg (UINT4 u4Value)
{
    TMR_DBG_FLAG = u4Value;
}
#endif

tTMO_DLL           *
TmrGetActiveTimerList (tTimerListId TimerListId)
{
    return (&((tTimerList *) TimerListId)->Link);
}

/*** OS specific code begin ***/
/************************************************************************/
/*  Function Name   : OsixStartQnxTmrTask                               */
/*  Description     : Starts the tmr task on qnx. Receives periodic     */
/*                  : events and calls ontick processing.               */
/*  Input(s)        :                                                   */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/error number                         */
/************************************************************************/
UINT4
OsixStartQnxTmrTask ()
{
    UINT1               au1Name[OSIX_NAME_LEN + 4];
#define OSIX_TMR_TASK_PRIO 25

    MEMSET (au1Name, '\0', (OSIX_NAME_LEN + 4));
    STRCPY (au1Name, "TMR#");
    if (OsixTskCrt (au1Name, OSIX_TMR_TASK_PRIO, OSIX_DEFAULT_STACK_SIZE,
                    OsixQnxTmrTask, 0, &TmrTaskId) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixStopQnxTmrTask.                               */
/*  Description     : Stops the QNX timer task.                         */
/*  Input(s)        :                                                   */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/

UINT4
OsixStopQnxTmrTask ()
{
    /* close the message channel and delete the task */
    if (ChannelDestroy (gi4QnxTmrChid) == -1)
    {
        return (OSIX_FAILURE);
    }
    OsixTskDel (TmrTaskId);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQnxTmrTask                                    */
/*  Description     : The timer task for QNX                            */
/*  Input(s)        :                                                   */
/*  Output(s)       : None.                                             */
/*  Returns         :                                                   */
/************************************************************************/
VOID
OsixQnxTmrTask (INT4 i4Dummy)
{
    INT4                i4RcvId;
    tPulse              Pulse;

    i4Dummy = i4Dummy;            /* unused */

    if ((gi4QnxTmrChid = ChannelCreate (0)) == -1)
    {
        OSIX_PRNT ("-E couldn't create channel\n");
        /* TMR_DBG (TMR_DBG_FLAG, TMR_DBG_FATAL, TMR_MODNAME, "Not able to Create Channel to receive timer pulse\n"); */
        return;
    }

    /* Set up the pulse and timer */
    if (OsixQnxSetupPulseAndTimer () != OSIX_SUCCESS)
    {
        return;
    }

    /* Receive the timer pulses */
    while (1)
    {
        i4RcvId = MsgReceive (gi4QnxTmrChid, &Pulse, sizeof (Pulse), NULL);
        if (i4RcvId == 0 && Pulse.code == OSIX_TIMER_PULSE)
        {
            TmrProcessTick ();
        }
    }
}

/*******************************************************************************/
/*  Function Name   : OsixQnxSetupPulseAndTimer                                */
/*  Description     : This routine is responsible for setting up               */
/*                    a pulse so it sends a message with code OSIX_TIMER_PULSE */
/*                    It then sets up a periodic timer that fires              */
/*                    once per second.                                         */
/*  Input(s)        :                                                          */
/*  Output(s)       : None.                                                    */
/*  Returns         :                                                          */
/*******************************************************************************/

INT4
OsixQnxSetupPulseAndTimer ()
{
    timer_t             timerid;    /* timer ID for timer */
    struct sigevent     event;    /* event to deliver */
    struct itimerspec   timer;    /* the timer data structure */
    struct timespec     resn;    /* to get the resolution of timer in nanoseconds  */
    INT4                i4Coid;    /* connection back to ourselves */
    UINT4               u4Timeout;    /* timeout value */

    /* create a connection back to ourselves */
    i4Coid = ConnectAttach (0, 0, gi4QnxTmrChid, 0, 0);
    if (i4Coid == -1)
    {
        OSIX_PRNT ("-E QNX Timer channel couldn't ConnectAttach to self\n");
        ChannelDestroy (gi4QnxTmrChid);
        return (OSIX_FAILURE);
    }

    /* set up the kind of event that we want to deliver -- a pulse */
    SIGEV_PULSE_INIT (&event, i4Coid, SIGEV_PULSE_PRIO_INHERIT,
                      OSIX_TIMER_PULSE, 0);

    /* create the timer, binding it to the event */
    if (timer_create (CLOCK_REALTIME, &event, &timerid) == -1)
    {
        OSIX_PRNT ("-E Qnx timer - couldn't create a timer%d\n");
        ChannelDestroy (gi4QnxTmrChid);
        return (OSIX_FAILURE);
    }

    /* get the resolution of the timer in nanoseconds. This value corresponds to
       the number of ticks per second */

    if (clock_getres (CLOCK_REALTIME, &resn) == -1)
    {
        OSIX_PRNT ("-E Qnx timer - couldn't get timer resolution%d\n");
        ChannelDestroy (gi4QnxTmrChid);
        timer_delete (timerid);
        return (OSIX_FAILURE);
    }

    u4Timeout = gu4Tps / gu4Stups;

    nsec2timespec (&(timer.it_value), (FS_QNX_UINT8) u4Timeout);
    nsec2timespec (&(timer.it_interval), (FS_QNX_UINT8) u4Timeout);

    /* and start it! */
    if (timer_settime (timerid, 0, &timer, NULL) == -1)
    {
        OSIX_PRNT ("-E Qnx timer - couldn't set timer%d\n");
        ChannelDestroy (gi4QnxTmrChid);
        timer_delete (timerid);
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/*** OS specific code end ***/
/************************************************************************
 *  Function Name   : TmrLock
 *  Description     : Disables timer so that the shared data structures
 *                    can be accessed atomically.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
TmrLock (VOID)
{
    INT4                i4rc;

    i4rc = ThreadCtl (_NTO_TCTL_IO, 0);
    if (i4rc == -1)
        return (TMR_FAILURE);
    InterruptDisable ();
    return (TMR_SUCCESS);
}

/************************************************************************
 *  Function Name   : TmrUnLock
 *  Description     : Enables the timer again.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
TmrUnLock (UINT4 u4s)
{
    INT4                i4rc;

    i4rc = ThreadCtl (_NTO_TCTL_IO, 0);
    if (i4rc == -1)
        return (TMR_FAILURE);
    InterruptEnable ();
    return (TMR_SUCCESS);
}

/*****************************************************************************/
/* Function     : TmrStart                                                   */
/*                                                                           */
/* Description  : Sets the timer id in the pTimer struct. Calls the          */
/*                TmrStartTimer to start the timer.This function is provided */
/*                to make all the application use the timer library in the   */
/*                same way.                                                  */
/*                                                                           */
/* Input        : TimerListId  : The timer list id                           */
/*                pTimer       : Pointer to timer block                      */
/*                u1TimerId    : The id of the timer to be started           */
/*                u4Sec        : The seconds value of the timer.             */
/*                u4MilliSec   : The milliseconds value of the timer.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrStart (tTimerListId TimerListId, tTmrBlk * pTimer, UINT1 u1TimerId,
          UINT4 u4Sec, UINT4 u4MilliSec)
{
    UINT4               u4Duration;
    pTimer->u1TimerId = u1TimerId;

    u4Duration = u4Sec * 1000 + u4MilliSec;

    if (TmrStartTimer (TimerListId, &(pTimer->TimerNode),
                       u4Duration) != TMR_SUCCESS)
    {
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
                  "TmrStart Failed for timer 0x%x\n", pTimer));
        return (TMR_FAILURE);
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Started timer 0x%x, %ld units \n", pTimer, u4Duration));
    return TMR_SUCCESS;
}

/*****************************************************************************/
/* Function     : TmrRestart                                                 */
/*                                                                           */
/* Description  : Restarts the timer with the specified duration             */
/*                                                                           */
/* Input        : TimerListId :  The timer list id                           */
/*                pTimer       : pointer to timer block                      */
/*                u1TimerId    : The id of the timer to be started           */
/*                u4Sec        : The seconds value of the timer.             */
/*                u4MilliSec   : The milliseconds value of the timer.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrRestart (tTimerListId TimerListId, tTmrBlk * pTimer,
            UINT1 u1TimerId, UINT4 u4Sec, UINT4 u4MilliSec)
{
    UINT4               u4Duration;

    /* u4Duration = (u4Sec * gu4Stups) + ((gu4Stups * u4MilliSec) / 1000); */

    u4Duration = u4Sec * 1000 + u4MilliSec;

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Restarting timer 0x%x, TmrId %s NumTicks %ld\n",
              pTimer, u1TimerId, u4Duration));

    if (TmrStopTimer (TimerListId, &(pTimer->TimerNode)) != TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    if (TmrStart (TimerListId, pTimer, u1TimerId, u4Sec, u4MilliSec) !=
        TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "TMR Restarted\n"));
    return (TMR_SUCCESS);
}

/*****************************************************************************/
/* Function     : TmrStop                                                    */
/*                                                                           */
/* Description  : Deletes the timer from the timer list.                     */
/*                                                                           */
/* Input        : pTimer       : pointer to timer block                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrStop (tTimerListId TimerListId, tTmrBlk * pTimer)
{
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Stopping timer 0x%x, TmrId %s\n", pTimer, pTimer->u1TimerId));

    if (TmrStopTimer (TimerListId, &(pTimer->TimerNode)) != TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrSetSysTime                                     */
/*  Description     : API for changing System Time at run time.         */
/*                  :                                                   */
/*  Input(s)        : tm - Time Structure to be set to.                 */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
VOID
TmrSetSysTime (tUtlTm * tm)
{
    struct tm           BrokenTime;
    struct timespec     tp;

    BrokenTime.tm_hour = tm->tm_hour;

    BrokenTime.tm_min = tm->tm_min;

    BrokenTime.tm_sec = tm->tm_sec;

    BrokenTime.tm_mday = tm->tm_mday;

    BrokenTime.tm_mon = tm->tm_mon;

    BrokenTime.tm_year = tm->tm_year;
    BrokenTime.tm_year -= 1900;

    tp.tv_sec = mktime (&BrokenTime);

    tp.tv_nsec = 0;

    /*Set the time in the system */
    clock_settime (CLOCK_REALTIME, &tp);
}

/************************************************************************/
/*  Function Name   : TmrGetPreciseSysTime                              */
/*  Description     : Returns the seconds and Nanoseconds of the time   */
/*                  : from the Epoch (00:00:00 UTC, January 1, 1970)    */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysPreciseTime - The System time.                */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
TmrGetPreciseSysTime (tUtlSysPreciseTime * pSysPreciseTime)
{
    struct timeval      tv;

    /* Get the time in the system */
    gettimeofday (&tv, NULL);

    pSysPreciseTime->u4Sec = tv.tv_sec;
    /* microseconds to nanoseconds convertion */
    pSysPreciseTime->u4NanoSec = tv.tv_usec * 1000;

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrSetPreciseSysTime                              */
/*  Description     : Set the seconds and Nanoseconds of the time       */
/*                  : from the Epoch (00:00:00 UTC, January 1, 1970)    */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysPreciseTime - The System time.                */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
TmrSetPreciseSysTime (tUtlSysPreciseTime * pSysPreciseTime)
{
#ifndef LINUXSIM_WANTED
    struct timeval      tv;

    tv.tv_sec = pSysPreciseTime->u4Sec;
    /* nanoseconds to microseconds convertion */
    tv.tv_usec = ((pSysPreciseTime->u4NanoSec) / 1000);
    /* Set the time in the system */
    settimeofday (&tv, NULL);
#else
    SET_SYSTEM_TIME (pSysPreciseTime);
#endif

    return (OSIX_SUCCESS);
}
