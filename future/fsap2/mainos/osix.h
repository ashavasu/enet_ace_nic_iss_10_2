/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osix.h,v 1.2 2015/09/02 11:58:10 siva Exp $
 *
 * Description: This is the exported file for fsap 3000.
 *              It contains exported defines and FSAP APIs.
 */
#ifndef _OSIX_H_
#define _OSIX_H_

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif
#include "fsap.h"

/*** Configurable parameters.              ***/
#define  OSIX_NAME_LEN           4
#define  OSIX_DEF_MSG_LEN        4
#define  OSIX_MAX_Q_MSG_LEN      4
#ifdef OS_VX2PTH_WRAP 
#define  OSIX_TPS               20
#define  OSIX_STUPS              2
#else
#define  OSIX_TPS               20
#define  OSIX_STUPS              1
#endif
#define  OSIX_MAX_QUES         200
#define  OSIX_FILE_LOG           1        /* 0: disable file logging
                                           * 1: enable file logging
                                           */
#define OSIX_LOG_METHOD    CONSOLE_LOG 

/*** Shared between tmo.c and wrap.c       ***/
#define  OSIX_TSK                0
#define  OSIX_SEM                1
#define  OSIX_QUE                2
#define  OSIX_TRUE               1
#define  OSIX_FALSE              0
#define  OSIX_RSC_INV         NULL

/*** OS related constants used by fsap     ***/

/* LOW_PRIO  - Priority of lowest priority task.
 * HIGH_PRIO - Priority of highest priority task.
 */
#define  FSAP_LOW_PRIO         255  /* 31 but set as 255 for bward compat. */
#define  FSAP_HIGH_PRIO          0
#define  OS_LOW_PRIO             1
#ifdef OS_VX2PTH_WRAP 
#define  OS_HIGH_PRIO          150 
#else
#define  OS_HIGH_PRIO           99
#endif

    /* Scheduling Algorithm */

#define OSIX_SCHED_RR             (1 << 16)
#define OSIX_SCHED_FIFO           (1 << 17)
#define OSIX_SCHED_OTHER          (1 << 18)
 
    
/*** Wait flags used in OsixEvtRecv()      ***/
#undef   OSIX_WAIT
#undef   OSIX_NO_WAIT
#define  OSIX_WAIT              (~0UL)
#define  OSIX_NO_WAIT            0

/***          FILE modes                   ***/
#define OSIX_FILE_RO          0x01
#define OSIX_FILE_WO          0x02
#define OSIX_FILE_RW          0x04
#define OSIX_FILE_CR          0x08
#define OSIX_FILE_AP          0x10
#define OSIX_FILE_SY          0x20
#define OSIX_FILE_TR          0x40

/***          Time Units                   ***/
#define OSIX_SECONDS          1
#define OSIX_CENTI_SECONDS    2
#define OSIX_MILLI_SECONDS    3
#define OSIX_MICRO_SECONDS    4
#define OSIX_NANO_SECONDS     5

/*** Prototype of an Entry Point Function. ***/
typedef VOID (*OsixTskEntry)(INT1 *);

#define OsixBHEnable()
#define OsixBHDisable()

/***           FSAP APIs                   ***/
UINT4      OsixInitialize  ARG_LIST((VOID));
UINT4      OsixShutDown    ARG_LIST((VOID));
UINT4       OsixSysRestart  ARG_LIST ((VOID));
UINT4      OsixTskCrt      ARG_LIST((UINT1[], UINT4, UINT4, VOID(*)(INT1 *),
                                     INT1 *, tOsixTaskId*));
VOID       OsixTskDel      ARG_LIST((tOsixTaskId));
UINT4      OsixTskDelay    ARG_LIST((UINT4));
UINT4      OsixDelay       ARG_LIST ((UINT4, INT4));
UINT4      OsixTskIdSelf   ARG_LIST((tOsixTaskId*));
UINT4      OsixEvtSend     ARG_LIST((tOsixTaskId, UINT4));
UINT4      OsixEvtRecv     ARG_LIST((tOsixTaskId, UINT4, UINT4, UINT4*));
UINT4      OsixSemCrt      ARG_LIST((UINT1[], tOsixSemId*));
VOID       OsixSemDel      ARG_LIST((tOsixSemId));
UINT4      OsixSemGive     ARG_LIST((tOsixSemId));
UINT4      OsixSemTake     ARG_LIST((tOsixSemId));
UINT4      OsixQueCrt      ARG_LIST((UINT1[], UINT4, UINT4, tOsixQId*));
VOID       OsixQueDel      ARG_LIST((tOsixQId));
UINT4      OsixQueSend     ARG_LIST((tOsixQId, UINT1*, UINT4));
UINT4      OsixQueRecv     ARG_LIST((tOsixQId, UINT1*, UINT4, INT4));
UINT4      OsixQueNumMsg   ARG_LIST((tOsixQId, UINT4*));

UINT4      OsixGetSysUpTime ARG_LIST((VOID));
UINT4      OsixSemTimedTake ARG_LIST ((tOsixSemId, UINT2));
/*** API shared b/w tmo.c and wrap.c  
     This is not an exported API           ***/
UINT4      OsixRscFind     ARG_LIST((UINT1[], UINT4, UINT4*));

INT4       FileOpen        (const UINT1 *pu1FileName, INT4 i4Mode);
INT4       FileClose       (INT4 i4Fd);
UINT4      FileRead        (INT4 i4Fd, CHR1 *pBuf, UINT4 i4Count);
UINT4      FileWrite       (INT4 i4Fd, const CHR1 *pBuf, UINT4 i4Count);
INT4       FileDelete      (const UINT1 *pu1FileName);
INT4       FileSize        (INT4 i4Fd);
INT4       FileTruncate    (INT4 i4Fd, INT4 i4Size);
INT4       FileSeek        (INT4 i4Fd, INT4 i4Offset, INT4 i4Whence);
INT4       FileStat        (const CHR1 * pc1FileName);
INT4       OsixCreateDir   (const CHR1 * pc1DirName);
UINT4      OsixSetLocalTime (void);
UINT4      OsixGetTps      (VOID);
UINT4      FsapShowTCB (UINT1 *pu1Result);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif
#endif /* _OSIX_H_ */
