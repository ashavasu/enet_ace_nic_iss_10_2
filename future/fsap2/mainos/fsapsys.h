/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapsys.h,v 1.1.1.1 2015/04/28 12:11:59 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */
#ifndef _FSAPSYS_H
#define _FSAPSYS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <poll.h>
#include <errno.h>
#include <math.h>
#include <linux/version.h>
#endif /*FSAPSYS_H*/
