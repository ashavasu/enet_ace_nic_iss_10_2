/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osixmainOs.c,v 1.3 2017/01/24 13:21:08 siva Exp $
 *
 * Description: Contains OSIX reference code for CPSS mainOs.
 *              All basic OS facilities used by protocol software
 *              from FS, use only these APIs.
 */
#include "osxinc.h"
#include "osix.h"
#include "fsapcli.h"
#include <fcntl.h>
/* temporary - time() and localtime() are required
 * for current OsixSetLocalTime() implementation
 */
#include <time.h>

#define ERROR -1
#define OK    (0)
#define   UNUSED_PARAM(x)   ((VOID)x)
/* The basic structure maintaining the name-to-id mapping */
/* of OSIX resources. 3 arrays - one for tasks, one for   */
/* semaphores and one for queues are maintained by OSIX.  */
/* Each array has this structure as the basic element. We */
/* use this array to store events for tasks also.         */

/* The description of fields of the structures below is as follows */
/*   u4RscId  - the id returned by the OS                          */
/*   u2Free   - whether this structure is free or used             */
/*   u4Events - for event simulation; used only for tasks          */
/*   au1Name  - name is always multiple of 4 characters in length  */
typedef struct OsixRscTskStruct
{
    GT_TASK             u4RscId;
    UINT4               u4Events;
    GT_SEM              TskMutex;
    UINT2               u2Free;
    UINT2               u2Pad;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixTsk;
typedef struct OsixRscQueStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixQue;
typedef struct OsixRscSemStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixSem;

tOsixTsk            gaOsixTsk[OSIX_MAX_TSKS + 1];
/* 0th element is invalid and not used */

tOsixSem            gaOsixSem[OSIX_MAX_SEMS + 1];
/* 0th element is invalid and not used */

tOsixQue            gaOsixQue[OSIX_MAX_QUES + 1];
/* 0th element is invalid and not used */

UINT4               gu4Tps = OSIX_TPS;
UINT4               gu4Stups = OSIX_STUPS;
GT_MUTEX            gOsixMutex = (GT_MUTEX) 0;

#define     NUM_TEN_MS_PER_SEC 100
#define     OSIX_TRC_FLAG gu4OsixTrc
UINT4               gu4OsixTrc;
GT_U32              gStartTicks;

static VOID OsixInitSignals ARG_LIST ((VOID));
static UINT4 OsixRscAdd ARG_LIST ((UINT1[], UINT4, UINT4));
static VOID OsixRscDel ARG_LIST ((UINT4, UINT4));
extern UINT4 OsixSTUPS2Ticks ARG_LIST ((UINT4));
extern UINT4 OsixTicks2STUPS ARG_LIST ((UINT4));
extern UINT4        gu4Seconds;

/********************************************************/
/* Routines for task creation, deletion and maintenance */
/********************************************************/
/************************************************************************/
/*  Function Name   : OsixTskCrt                                        */
/*  Description     : Creates task.                                     */
/*  Input(s)        : au1Name[ ] -        Name of the task              */
/*                  : u4TskPrio -         Task Priority                 */
/*                  : u4StackSize -       Stack size                    */
/*                  : (*TskStartAddr)() - Entry point function          */
/*                  : u4Arg -             Arguments to above fn.        */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/

UINT4
OsixTskCrt (UINT1 au1TskName[], UINT4 u4TskPrio, UINT4 u4StackSize,
            VOID (*TskStartAddr) (INT1 *), INT1 *u4Arg, tOsixTaskId * pTskId)
{
    INT4                i4OsPrio;
    UINT4               u4Idx;
    tOsixTsk           *pTsk = 0;
    GT_SEM              TskMutex;
    UINT4               u4SchedPolicy = 0;
    UINT1               au1Name[OSIX_NAME_LEN + 4];

/*  PRINTF("SSF2F: OsixTskCrt (via osTaskCreate) TskName = %s, StackSize = %lu (+16K by kernelExt/ltaskLib.c)\n",
           (au1TskName==NULL)?"NULL":au1TskName, u4StackSize);*/

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixTskCrt (%s, %ld, %ld)\r\n",
               au1Name, u4TskPrio, u4StackSize);

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1TskName, OSIX_NAME_LEN);

    if (OsixRscFind (au1Name, OSIX_TSK, pTskId) == OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);    /* Task by this name already exists */
    }

    if (osSemCCreate (au1Name, 0, &TskMutex) != GT_OK)
    {
        fprintf (stderr, "SSI: failed to create semaphore \"%s\"\n", au1Name);
        return (OSIX_FAILURE);
    }

    u4SchedPolicy = u4TskPrio & 0xffff0000;
    u4TskPrio = u4TskPrio & 0x0000ffff;

    /* For tasks, the VxWorks version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0.   */
    if (OsixRscAdd (au1Name, OSIX_TSK, 0) == OSIX_FAILURE)
    {
        osSemDelete (TskMutex);
        return (OSIX_FAILURE);
    }

    /* Remap the task priority to Vxworks's range of values. */
    i4OsPrio = OS_LOW_PRIO + (((((INT4) (u4TskPrio) - FSAP_LOW_PRIO) *
                                (OS_HIGH_PRIO -
                                 OS_LOW_PRIO)) / (FSAP_HIGH_PRIO -
                                                  FSAP_LOW_PRIO)));

    i4OsPrio = i4OsPrio | (INT4) u4SchedPolicy;

    /* Align 4 byte */
    u4StackSize = (u4StackSize + 0x3) & ~0x3;

    /* Get the global task array index to return as the task id. */
    /* Return value check not needed because we just added it.   */
    OsixRscFind (au1Name, OSIX_TSK, &u4Idx);
    pTsk = &gaOsixTsk[u4Idx];
    *pTskId = (tOsixTaskId) u4Idx;
    pTsk->TskMutex = TskMutex;

    /* Create task and  add it to the name to id mapping linked list. */
    if (osTaskCreate ((char *) au1Name, (GT_U32) i4OsPrio, u4StackSize,
                      (VOID *) TskStartAddr, u4Arg, &(pTsk->u4RscId)) != GT_OK)
    {
        OsixRscDel (OSIX_TSK, u4Idx);
        osSemDelete (TskMutex);

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixTskDel                                        */
/*  Description     : Deletes a task.                                   */
/*  Input(s)        : TskId          - Task Id                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixTskDel (tOsixTaskId TskId)
{
    GT_TASK             i4OsTskId;
    GT_SEM              TskMutex;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixTskDel (0x%x)\r\n", (UINT4) TskId);
    TskMutex = gaOsixTsk[(UINT4) TskId].TskMutex;

    i4OsTskId = (gaOsixTsk[(UINT4) TskId].u4RscId);
    OsixRscDel (OSIX_TSK, TskId);
    osTaskDelete (i4OsTskId);
    osSemDelete (TskMutex);
}

/************************************************************************/
/*  Function Name   : OsixTskDelay                                      */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskDelay (UINT4 u4Duration)
{
    UINT4               u4Sec;
    struct timespec     timeout;

    /* Convert the duration into micro seconds */
    u4Duration = (u4Duration * (1000000 / gu4Stups));

    /* Assumption: Above calculation converts u4Duration to micro-seconds
     * We convert that into seconds and nano-seconds for the OS call.
     * If the values configured for OSIX_STUPS and OSIX_TPS change, this
     * code should be changed.
     */
    u4Sec = u4Duration / 1000000;
    timeout.tv_sec = u4Sec;
    timeout.tv_nsec = ((u4Duration - u4Sec * 1000000) * 1000);
    nanosleep (&timeout, NULL);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixDelay                                         */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*                  : i4Unit     - Units in which the duration is       */
/*                                 specified                            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixDelay (UINT4 u4Duration, INT4 i4Unit)
{
    /* The function OsixTskdelay () does not allow delays less than 100 ms.
     * For backward compatibility reasons, that function cannot be changed.
     * So, this new function OsixDelay is introduced. On any this must be
     * ported before being used */

    UNUSED_PARAM (u4Duration);
    UNUSED_PARAM (i4Unit);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixTskIdSelf                                     */
/*  Description     : Get Osix Id of current Task                       */
/*  Input(s)        : None                                              */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskIdSelf (tOsixTaskId * pTskId)
{
    UINT4               u4Count;
    GT_TASK             i4OsTskId;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixTskIdSelf (0x%x)\r\n", (UINT4) pTskId);

    if (osTaskGetSelf (&i4OsTskId) != GT_OK)
        return OSIX_FAILURE;

    for (u4Count = 1; u4Count <= OSIX_MAX_TSKS; u4Count++)
    {
        if ((gaOsixTsk[u4Count].u4RscId) == i4OsTskId)
        {
            *pTskId = (tOsixTaskId) u4Count;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/********************************************************/
/* Routines for event management - send / receive event */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixEvtSend                                       */
/*  Description     : Sends an event to a specified task.               */
/*  Input(s)        : TskId          - Task Id                          */
/*                  : u4Events       - The Events, OR-d                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events)
{
    UINT4               u4Idx = (UINT4) TskId;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixEvtSend (%ld, 0x%lx)\r\n", (UINT4) TskId, u4Events);
    gaOsixTsk[u4Idx].u4Events |= u4Events;
    if (gaOsixTsk[u4Idx].TskMutex != NULL)
    {
        osSemSignal (gaOsixTsk[u4Idx].TskMutex);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixEvtRecv                                       */
/*  Description     : To receive a event.                               */
/*  Input(s)        : TskId             - Task Id                       */
/*                  : u4Events          - List of interested events.    */
/*  Output(s)       : pu4EventsReceived - Events received.              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flgs,
             UINT4 *pu4RcvEvents)
{
    UINT4               u4Idx = (UINT4) TskId;
    GT_STATUS           u4rc;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixEvtRecv (%ld, 0x%lx, %ld, 0x%lx)\r\n", (UINT4) TskId,
               u4Events, u4Flgs, pu4RcvEvents);

    *pu4RcvEvents = 0;

    if ((u4Flgs == OSIX_NO_WAIT)
        && (((gaOsixTsk[u4Idx].u4Events) & u4Events) == 0))
    {
        return (OSIX_FAILURE);
    }

    while (1)
    {
        if (((gaOsixTsk[u4Idx].u4Events) & u4Events) != 0)
        {                        /* A required event has happened */
            *pu4RcvEvents = (gaOsixTsk[u4Idx].u4Events) & u4Events;
            gaOsixTsk[u4Idx].u4Events &= ~(*pu4RcvEvents);
            return (OSIX_SUCCESS);
        }

        u4rc = osSemWait (gaOsixTsk[u4Idx].TskMutex, OS_WAIT_FOREVER);
        if (u4rc != GT_OK)
        {
            return (OSIX_FAILURE);
        }
    }
}

/************************************************************************/
/*  Function Name   : OsixInitialize                                    */
/*  Description     : The OSIX Initialization routine. To be called     */
/*                    before any other OSIX functions are used.         */
/*  Input(s)        : pOsixCfg - Pointer to OSIX config info.           */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixInitialize (void)
{
    UINT4               u4Idx;

    /* Mutual exclusion semaphore to add or delete elements from */
    /* name-id-mapping list. This semaphore itself will not be   */
    /* added to the name-to-id mapping list. This is a VxWorks   */
    /* specific call and must be mapped to relevant call for OS  */

    if (osMutexCreate ("gOsixMutex", &gOsixMutex) != GT_OK)
    {
        return (OSIX_FAILURE);
    }

    /* Initialize all arrays. */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        gaOsixTsk[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u4Events = 0;
        gaOsixTsk[u4Idx].TskMutex = 0;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    gStartTicks = osTickGet ();
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSysRestart                                    */
/*  Description     : This function reboots the system.                 */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSysRestart ()
{
    /* Killing the current process for shutdown */
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixShutDown                                      */
/*  Description     : Stops OSIX.                                       */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixShutDown (VOID)
{
    UINT4               u4Idx;

    /* Re-initialize all arrays and delete global semaphore */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        if (gaOsixTsk[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixTskDel (u4Idx);
        }
        gaOsixTsk[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u4Events = 0;
        gaOsixTsk[u4Idx].TskMutex = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        if (gaOsixSem[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixSemDel ((tOsixSemId) & (gaOsixSem[u4Idx].u4RscId));
        }
        gaOsixSem[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if (gaOsixQue[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixQueDel ((tOsixQId) & (gaOsixQue[u4Idx].u4RscId));
        }
        gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
    }

    osMutexDelete (gOsixMutex);
    gOsixMutex = (GT_MUTEX) 0;

    return (OSIX_SUCCESS);
}

/************************************/
/* Routines for managing semaphores */
/************************************/

/* Keep it simple - support 1 type of semaphore - binary, blocking.     */
/* After creating, the semaphore must be given before it can be taken.  */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixSemCrt                                        */
/*  Description     : Creates a sema4.                                  */
/*  Input(s)        : au1Name [ ] - Name of the sema4.                  */
/*  Output(s)       : pSemId         - The sema4 Id.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemCrt (UINT1 au1SemName[], tOsixSemId * pSemId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    GT_SEM              semId;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1SemName, OSIX_NAME_LEN);

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemCrt (%s)\r\n", au1Name);

    if (OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) pSemId) == OSIX_SUCCESS)
    {
        /* Semaphore by this name already exists. */
        return (OSIX_FAILURE);
    }

    if (osSemCCreate ((char *) au1Name, 0, &semId) != GT_OK)
    {
        return (OSIX_FAILURE);
    }
    *pSemId = (tOsixSemId) semId;
    if (OsixRscAdd (au1Name, OSIX_SEM, (UINT4) *pSemId) == OSIX_FAILURE)
    {
        osSemDelete (semId);
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemDel                                        */
/*  Description     : Deletes a sema4.                                  */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixSemDel (tOsixSemId SemId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemDel (0x%lx)\r\n",
               (UINT4) SemId);
    OsixRscDel (OSIX_SEM, (UINT4) SemId);
    osSemDelete ((GT_SEM) (SemId));
}

/************************************************************************/
/*  Function Name   : OsixSemGive                                       */
/*  Description     : Used to release a sem4.                           */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemGive (tOsixSemId SemId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemGive (0x%lx)\r\n",
               (UINT4) SemId);
    if (osSemSignal ((GT_SEM) SemId) != GT_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemTake                                       */
/*  Description     : Used to acquire a sema4.                          */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemTake (tOsixSemId SemId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemTake (0x%lx)\r\n",
               (UINT4) SemId);

    if (osSemWait ((GT_SEM) SemId, OS_WAIT_FOREVER) != GT_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/****************************************/
/* Routines for managing message queues */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixQueCrt                                        */
/*  Description     : Creates a OSIX Q.                                 */
/*  Input(s)        : au1name[ ] - The Name of the Queue.               */
/*                  : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MaxMsgLen- Max length of a messages             */
/*  Output(s)       : pQueId     - The QId returned.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueCrt (UINT1 au1QName[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    GT_MSGQ_ID          msgqId;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1QName, OSIX_NAME_LEN);

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueCrt (%s, %ld, %ld)\r\n",
               au1Name, u4MaxMsgLen, u4MaxMsgs);

    if (OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) pQueId) == OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);
    }

    if (osMsgQCreate ((char *) au1Name, u4MaxMsgs, u4MaxMsgLen, &msgqId) !=
        GT_OK)
    {
        fprintf (stderr, "SSI: failed to create message queue \"%s\"\n",
                 au1Name);
        return (OSIX_FAILURE);
    }
    *pQueId = (tOsixQId) msgqId;
    if (OsixRscAdd (au1Name, OSIX_QUE, *pQueId) == OSIX_FAILURE)
    {
        osMsgQDelete (msgqId);
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueDel                                        */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
void
OsixQueDel (tOsixQId QueId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueDel (%ld)\r\n", QueId);

    OsixRscDel (OSIX_QUE, (UINT4) QueId);
    if (osMsgQDelete ((GT_MSGQ_ID) QueId) == (UINT4) ERROR)
    {
        return;
    }
    return;
}

/************************************************************************/
/*  Function Name   : OsixQueSend                                       */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*                  : u4MsgLen - length of the messages                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "",
               "OsixQueSend (%ld, 0x%x, %ld)\r\n", QueId, pu1Msg, u4MsgLen);

    if (osMsgQSend ((GT_MSGQ_ID) QueId, (char *) pu1Msg, u4MsgLen,
                    OS_MSGQ_NO_WAIT) != GT_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueRecv                                       */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : u4MsgLen -  length of the messages                */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4Timeout)
{
    GT_U32              msgSize;
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueRecv (%ld)\r\n", QueId);

    if (i4Timeout == -1)
    {
        i4Timeout = OS_MSGQ_WAIT_FOREVER;
    }
    else if (i4Timeout == 0)
    {
        i4Timeout = OS_MSGQ_NO_WAIT;
    }
    else
    {
        i4Timeout = (INT4) OsixSTUPS2Ticks ((UINT4) i4Timeout);
    }
    msgSize = u4MsgLen;
    if (osMsgQRecv ((GT_MSGQ_ID) QueId, (char *) pu1Msg, &msgSize,
                    i4Timeout) != GT_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueNumMsgs                                    */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : pu4NumberOfMsgs - Contains count upon return.     */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixQueNumMsg (tOsixQId QueId, UINT4 *pu4NumMsg)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueNumMsg (%ld)\r\n",
               QueId);
    if (osMsgQNumMsgs ((GT_MSGQ_ID) QueId, (FS_ULONG *) pu4NumMsg) != GT_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/********************************************************/
/* Routines for managing resources based on names       */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixRscAdd                                        */
/*  Description     : Gets a free resouce, stores Resource-Id & Name    */
/*                  : This helps in locating Resource-Id by Name        */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
static UINT4
OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (osMutexLock (gOsixMutex) != GT_OK)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixTsk[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixTsk[u4Idx].u4Events = 0;
                    gaOsixTsk[u4Idx].TskMutex = 0;
                    MEMCPY (gaOsixTsk[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    osMutexUnlock (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixSem[u4Idx].u4RscId = u4RscId;
                    gaOsixSem[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixSem[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixSem[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    osMutexUnlock (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixQue[u4Idx].u4RscId = u4RscId;
                    gaOsixQue[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixQue[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixQue[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    osMutexUnlock (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    osMutexUnlock (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscDel                                        */
/*  Description     : Free an allocated resouce                         */
/*  Input(s)        : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
static VOID
OsixRscDel (UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (osMutexLock (gOsixMutex) != GT_OK)
    {
        return;
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            gaOsixTsk[u4RscId].u2Free = OSIX_TRUE;
            gaOsixTsk[u4RscId].TskMutex = 0;
            MEMSET (gaOsixTsk[u4RscId].au1Name, '\0', (OSIX_NAME_LEN + 4));
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixSem[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
                    gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixSem[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
                    gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixQue[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        default:
            break;
    }
    osMutexUnlock (gOsixMutex);
}

/************************************************************************/
/*  Function Name   : OsixRscFind                                       */
/*  Description     : This locates Resource-Id by Name                  */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*  Output(s)       : pu4RscId - Osix Resource-Id                       */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }
    if (osMutexLock (gOsixMutex) != GT_OK)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixTsk[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* For tasks applications know only our array index. */
                    /* This helps us to simulate events.                 */
                    *pu4RscId = u4Idx;
                    osMutexUnlock (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixSem[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = gaOsixSem[u4Idx].u4RscId;
                    osMutexUnlock (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixQue[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = gaOsixQue[u4Idx].u4RscId;
                    osMutexUnlock (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    osMutexUnlock (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : Fsap2Start                                        */
/*  Description     : Function to be called to get any fsap2 application*/
/*                  : to work                                           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Fsap2Start (VOID)
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixGetSysTime                                    */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysTime (tOsixSysTime * pSysTime)
{
    GT_U32              CurTicks;
    GT_U32              u4TicksPerStup;

    osGetSysClockRate (&u4TicksPerStup);
    u4TicksPerStup /= gu4Stups;

    CurTicks = osTickGet ();
    *pSysTime = (CurTicks - gStartTicks) / u4TicksPerStup;

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetSysTimeIn64                                */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysTimeIn64Bit (FS_UINT8 * pSysTime)
{
    /* Currently we have support for U4 Only */
    pSysTime->u4Hi = 0;
    OsixGetSysTime (&pSysTime->u4Lo);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetSysUpTime                                  */
/*  Description     : Returns the system time in Seconds.               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysUpTime (VOID)
{
    return (gu4Seconds);
}

/************************************************************************/
/*  Function Name   : OsixExGetTaskName                                 */
/*  Description     : Get the OSIX task Name given the Osix Task-Id.    */
/*  Input(s)        : TaskId - The Osix taskId.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to OSIX task name                         */
/************************************************************************/
const UINT1        *
OsixExGetTaskName (tOsixTaskId TskId)
{
    /* To facilitate direct use of this call in a STRCPY,
     * we return a null string instead of a NULL pointer.
     * A null pointer is returned for all non-OSIX tasks
     * e.g., TMO's idle task.
     */
    if (TskId)
    {
        return ((UINT1 *) (gaOsixTsk[(UINT4) TskId].au1Name));
    }
    return ((const UINT1 *) "");
}

#if (FILESYS_SUPPORT == FSAP_ON)
/************************************************************************
 *  Function Name   : FileOpen
 *  Description     : Function to Open a file.
 *  Input           : pu1FileName - Name of file to open.
 *                    i4Mode      - whether r/w/rw.
 *  Returns         : FileDescriptor if successful
 *                    -1             otherwise.
 ************************************************************************/
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    INT4                i4Mode = 0;

    if (i4InMode & OSIX_FILE_CR)
    {
        i4Mode |= O_CREAT;
    }
    if (i4InMode & OSIX_FILE_TR)
    {
        i4Mode |= O_TRUNC;
    }
    if (i4InMode & OSIX_FILE_RO)
    {
        i4Mode |= O_RDONLY;
    }
    else if (i4InMode & OSIX_FILE_WO)
    {
        i4Mode |= O_WRONLY;

        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }
    else if (i4InMode & OSIX_FILE_RW)
    {
        i4Mode |= O_RDWR;

        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }

    return open ((const CHR1 *) pu1FileName, i4Mode, 0644);
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description     : Function to close a file.
 *  Input           : i4Fd - File Descriptor to be closed.
 *  Returns         : 0 on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    return close (i4Fd);
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description     : Function to read a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer into which to read
 *                    i4Count - Number of bytes to read
 *  Returns         : Number of bytes read on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    return read (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description     : Function to write to a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : Number of bytes written on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, CONST CHR1 * pBuf, UINT4 i4Count)
{
    return write (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description     : Function to delete a file.
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : 0 on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    return unlink ((CONST CHR1 *) pu1FileName);
}

/****************************************************************************
 *  Function Name   : FileSeek
 *  Description     : Function to move the file descriptor for the
 *                    given offset in accordance with the whence.
 *  Input           : i4Fd     - File Descriptor.
 *                    i4Offset - bytes for how many bytes the file descriptor
 *                               needs to be moved.
 *                    i4Whence - it takes one of the following 3 values.
 *                    SEEK_SET - offset is set to offset bytes.
 *                    SEEK_CUR - offset is set to its current location
 *                               plus offset bytes.
 *                    SEEK_END - offset is set to the size of the file
 *                               plus offset bytes.
 *  Returns         : final offset location in SUCCESS case, -1 otherwise.
 ****************************************************************************/
INT4
FileSeek (INT4 i4Fd, INT4 i4Offset, INT4 i4Whence)
{
    i4Fd = i4Fd;
    i4Offset = i4Offset;
    i4Whence = i4Whence;
    return (-1);
}

/************************************************************************
*  Function Name   : FileSize
*  Description     : Function to return file size
*  Input           : i4Fd - File Descriptor
*  Returns         : Length on successful deletion.
*                    -1   otherwise.
************************************************************************/
INT4
FileSize (INT4 i4Fd)
{
    struct stat         buffer;
    if (fstat (i4Fd, &buffer) < 0)
    {
        return (-1);
    }
    return buffer.st_size;

}

/************************************************************************
*  Function Name   : FileTruncate
*  Description     : Function to truncate a file
*  Input           : i4Fd - File Descriptor
*                    i4Size - File size. Bytes after this size will be
*                    truncated.
*  Returns         : Length on successful deletion.
*                    -1   otherwise.
************************************************************************/
INT4
FileTruncate (INT4 i4Fd, INT4 i4Size)
{
    return ftruncate (i4Fd, i4Size);
}

/************************************************************************
 *  Function Name   : FileStat  
 *  Description     : Function to check if a file is present.
 *  Input           : pc1FileName - Name of the File to be checked.  
 *  Returns         : OSIX_SUCCESS - If the file exists.
 *                    OSIX_FAILURE - If the file does not exist.
 ************************************************************************/
INT4
FileStat (const CHR1 * pc1FileName)
{
    struct stat         FileInfo;
    INT4                i4FileExists = -1;

    if (pc1FileName != NULL)
    {
        /* Attempt to get the file attributes */
        i4FileExists = stat (pc1FileName, &FileInfo);
        if (i4FileExists == 0)
        {
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixCreateDir 
 *  Description     : This function used to create a directory.
 *  Inputs          : pc1DirName - Name of the Directory.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
OsixCreateDir (const CHR1 * pc1DirName)
{
    if (mkdir (pc1DirName, S_IRWXU) == -1)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#else
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    pu1FileName = pu1FileName;
    i4InMode = i4InMode;
    return (-1);
}

INT4
FileClose (INT4 i4Fd)
{
    i4Fd = i4Fd;
    return (-1);
}

UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

UINT4
FileWrite (INT4 i4Fd, CONST CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

INT4
FileDelete (const UINT1 *pu1FileName)
{
    pu1FileName = pu1FileName;
    return (-1);
}

INT4
FileSeek (INT4 i4Fd, INT4 i4Offset, INT4 i4Whence)
{
    i4Fd = i4Fd;
    i4Offset = i4Offset;
    i4Whence = i4Whence;
    return (-1);
}

INT4
FileStat (const CHR1 * pc1FileName)
{
    pc1FileName = pc1FileName;
    return (-1);
}

INT4
OsixCreateDir (const CHR1 * pc1DirName)
{
    pc1DirName = pc1DirName;
    return (-1);
}
#endif

/************************************************************************
 *  Function Name   : FsapTrace
 *  Description     : Implements the trace part of CLI interface.
 *  Input           : u4Flag =1 for 'trace' command, =0 for the 'no trace' cmd
 *                    u4Value - the value of the particular trace that is
 *                              being set/unset.
 *  Output          : pu4TrcLvl - Returns the current/new trace level.
 *  Returns         : None.
 ************************************************************************/
void
FsapTrace (UINT4 u4Flag, UINT4 u4Value, UINT4 *pu4TrcLvl)
{
    if (u4Value == 0)
    {
        *pu4TrcLvl = OSIX_TRC_FLAG;
        return;
    }

    if (u4Flag)
    {
        OSIX_TRC_FLAG |= u4Value;
    }
    else
    {
        OSIX_TRC_FLAG &= ~u4Value;
    }
    *pu4TrcLvl = OSIX_TRC_FLAG;

    return;
}

/************************************************************************/
/*  Function Name   : OsixGetTps                                        */
/*  Description     : Returns the system ticks in a sec                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetTps (VOID)
{
    GT_U32              tps;
    osGetSysClockRate (&tps);
    return (UINT4) tps;
}

/************************************************************************/
/*  Function Name   : OsixSetLocalTime                                  */
/*  Description     : Obtains the system time and sets it in FSAP.      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
/*?*/
OsixSetLocalTime (VOID)
{
    time_t              t;
    struct tm          *tm;

    time (&t);
    tm = localtime (&t);
    if (tm == NULL)
    {
        return (OSIX_FAILURE);
    }
    tm->tm_year += (1900);
    UtlSetTime ((tUtlTm *) tm);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : FsapShowTCB                                       */
/*  Description     : Used to Give the Task Info to the Calling Function*/
/*  Input(s)        : None                                              */
/*  Output(s)       : pu1Result - Output Buffer                         */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
FsapShowTCB (UINT1 *pu1Result)
{
    UINT4               u4Idx;
    UINT4               u4Pos = 0;

    if (osMutexLock (gOsixMutex) != 0)
    {
        SPRINTF ((CHR1 *) pu1Result, "Cannot Access the TCB \n");
        return (OSIX_FAILURE);
    }
    for (u4Idx = 1; (u4Idx <= OSIX_MAX_TSKS) &&
         (gaOsixTsk[u4Idx].u2Free == OSIX_FALSE); u4Idx++)
    {
        u4Pos += SPRINTF ((CHR1 *) pu1Result + u4Pos,
                          "Task Name : %s Process Id : %lu \n",
                          gaOsixTsk[u4Idx].au1Name, gaOsixTsk[u4Idx].u4RscId);
    }
    osMutexUnlock (gOsixMutex);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixGetMemInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4TotalMem - Total memory
 *                    pu4FreeMem - Free memory
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetMemInfo (UINT4 *pu4TotalMem, UINT4 *pu4FreeMem)
{
    *pu4TotalMem = 0;
    *pu4FreeMem = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetCPUInfo
 *  Description     : Function used to get the CPU utilization.
 *  Inputs          : None
 *  OutPuts         : pu4TotalUsage - Total Usage
 *                  : pu4CPUUsage - CPU Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetCPUInfo (UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage)
{
    *pu4TotalUsage = 0;
    *pu4CPUUsage = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetFlashInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4FlashUsage - Flash Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetFlashInfo (UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash)
{
    *pu4TotalUsage = 0;
    *pu4FreeFlash = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetBackTrace
 *  Description     : This function used to get the backtrace of the
 *                    current task. This function to be called in
 *                    specific scenario like MemFailure etc, to find
 *                    exact backtrace at that moment.
 *                    This function will create stacktrace.txt file
 *                    in the flash which contains backtrace along with
 *                    the current task name.
 *                    Note - If the backtrace file is fully filled, then
 *                    it will again wrap around on this file.
 *  Inputs          : None
 *  OutPuts         : None
 *  Returns         : None
 ************************************************************************/

VOID
OsixGetBackTrace (VOID)
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixInitSignals                                   */
/*  Description     : This function gets the signal from                */
/*                    signal handler.                                   */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
VOID
OsixInitSignals (VOID)
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixCoreDumpPathSetting                           */
/*  Description     : This function gets the corefile                   */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT1
OsixCoreDumpPathSetting (UINT1 *filepath)
{
    return;
}

/************************************************************************
 *  Function Name   : FsapShowTask
 *  Description     : Implements the show task part of CLI interface.
 *  Input           : au1Name - TaskName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowTask (UINT1 au1TskName[], UINT1 *pu1Result, INT4 buffer_size)
{
    return;
}

/************************************************************************
 *  Function Name   : FsapShowQue
 *  Description     : Implements the show que part of CLI interface.
 *  Input           : au1Name - QueueName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowQue (UINT1 au1QName[], UINT1 *pu1Result, INT4 buffer_size)
{
    return;
}

/************************************************************************
 *  Function Name   : FsapShowSem
 *  Description     : Implements the show sem part of CLI interface.
 *  Input           : au1Name - SemName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowSem (UINT1 au1SemName[], UINT1 *pu1Result, UINT4 *pu4NextIdx,
             INT4 buffer_size)
{
    return;
}

INT4
UtlGetLoggerTaskId (VOID)
{
    return;
}

/***************************************************************/
/*  Function Name   : UtlSetLogPathForFlash                    */
/*  Description     : API to set the current logging path for  */
/*                    flash                                    */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None                                     */
/*  Returns         : One of eLoggingModes.                    */
/***************************************************************/
VOID
UtlSetLogPathForFlash (UINT1 *pu1FlashLoggingFileName)
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                    to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}

/************************************************************************/
/*  Function Name   : OsixSemTimedTake                                  */
/*  Description     : Used to acquire a sema4 on a timed basis.         */
/*  Input(s)        : pSemId - The sema4 Id.                            */
/*                    u2Seconds - Time to wait.                         */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemTimedTake (tOsixSemId SemId , UINT2 u2Seconds)
{

    UNUSED_PARAM (SemId);
    UNUSED_PARAM (u2Seconds);
    return (OsixSemTake (SemId));
}
