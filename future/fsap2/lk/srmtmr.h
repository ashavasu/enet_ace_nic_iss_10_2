/*  $Id: srmtmr.h,v 1.7 2015/04/28 12:10:15 siva Exp $ */
/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                   */
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved          */
/*                                                          */
/*  FILE NAME             :   srmtmr.h                      */
/*  PRINCIPAL AUTHOR      :                                 */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :                                 */
/*  LANGUAGE              :                                 */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  DESCRIPTION           :                                 */
/************************************************************/

#ifndef SRM_TMR_H
#define SRM_TMR_H

#include "utldll.h"

typedef VOID * tTimerListId;

typedef struct TmrAppTimer
{
   tTMO_DLL_NODE       Link;
   tTimerListId        TimerListId;
   tTMO_DLL            *pDLL;
   FS_ULONG            u4Data;
   UINT2               u2RemainingTime;
   UINT2               u2Flags;
}tTmrAppTimer;

typedef VOID (*tTmrExpFn)(VOID *);

typedef struct TmrDesc
{
    tTmrExpFn  TmrExpFn;
    INT2       i2Offset;      /* if the field is -1, the function does
                               * not take any parameter
                               */
    UINT1      au1Rsvd[2];   /* included for 4 byte allignment */
} tTmrDesc;
typedef struct TmrBlk {
    tTmrAppTimer    TimerNode;
    UINT1           u1TimerId;
    UINT1           au1Rsvd[3];
} tTmrBlk;
#endif
