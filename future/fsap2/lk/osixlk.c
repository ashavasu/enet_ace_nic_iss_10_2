/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osixlk.c,v 1.5 2015/04/28 12:10:14 siva Exp $
 *
 * Description: Contains dummmy functions for Osix calls for fsap2 to be used in
 *              kernel. kernel modules use memory pools and other datastructure
 *              related functions of FSAP2.
 */

#include "osxinc.h"
#include "osix.h"

#define   UNUSED_PARAM(x)   ((VOID)x)

typedef struct _LkMemHdr
{
    UINT4               u4Size;
    UINT4               u4Sig;
}
tLkMemHdr;

typedef struct _LkMemTail
{
    UINT4               u4Sig1;
    UINT4               u4Sig2;
}
tLkMemTail;

UINT4               gu4Stups = OSIX_STUPS;
/************************************************************************
 *  Function Name   : LkDummyReturn
 *  Description     : This function stubs out the unused function calls
 *  Input           : None.
 *  Returns         : OSIX_SUCCESS.
 ************************************************************************/
UINT4
LkDummyReturn ()
{
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : LkDummyNoReturn
 *  Description     : This function stubs out the unused function calls
 *  Input           : None.
 *  Returns         : None
 ************************************************************************/
VOID
LkDummyNoReturn ()
{
    return;
}

/************************************************************************
 *  Function Name   : FileOpen
 *  Description     : Function to Open a file.
 *  Input           : pu1FileName - Name of file to open.
 *                    i4Mode      - whether r/w/rw.
 *  Returns         : FileDescriptor if successful
 *                    -1             otherwise.
 ************************************************************************/
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4Mode)
{
    UNUSED_PARAM (pu1FileName);
    UNUSED_PARAM (i4Mode);
    return (-1);
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description     : Function to close a file.
 *  Input           : i4Fd - File Descriptor to be closed.
 *  Returns         : 0 on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    UNUSED_PARAM (i4Fd);
    return (-1);
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description     : Function to read a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer into which to read
 *                    i4Count - Number of bytes to read
 *  Returns         : Number of bytes read on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    UNUSED_PARAM (i4Fd);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (i4Count);
    return (0);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description     : Function to write to a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : Number of bytes written on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    UNUSED_PARAM (i4Fd);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (i4Count);
    return (0);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description     : Function to delete a file.
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : 0 on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    UNUSED_PARAM (pu1FileName);
    return (-1);
}

/****************************************************************************
 *  Function Name   : FileSeek
 *  Description     : Function to move the file descriptor for the
 *                    given offset in accordance with the whence.
 *  Input           : i4Fd     - File Descriptor.
 *                    i4Offset - bytes for how many bytes the file descriptor
 *                               needs to be moved.
 *                    i4Whence - it takes one of the following 3 values.
 *                    SEEK_SET - offset is set to offset bytes.
 *                    SEEK_CUR - offset is set to its current location
 *                               plus offset bytes.
 *                    SEEK_END - offset is set to the size of the file
 *                               plus offset bytes.
 *  Returns         : final offset location in SUCCESS case, -1 otherwise.
 ****************************************************************************/
INT4
FileSeek (INT4 i4Fd, INT4 i4Offset, INT4 i4Whence)
{
    UNUSED_PARAM (i4Fd);
    UNUSED_PARAM (i4Offset);
    UNUSED_PARAM (i4Whence);
    return (-1);
}

/************************************************************************
 *  Function Name   : FileStat  
 *  Description     : Function to check if a file is present.
 *  Input           : pc1FileName - Name of the File to be checked.  
 *  Returns         : OSIX_SUCCESS - If the file exists.
 *                    OSIX_FAILURE - If the file does not exist.
 ************************************************************************/
INT4
FileStat (const CHR1 * pc1FileName)
{
    UNUSED_PARAM (pc1FileName);
    return (-1);
}

/************************************************************************
 *  Function Name   : OsixCreateDir 
 *  Description     : This function used to create a directory.
 *  Inputs          : pc1DirName - Name of the Directory.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
INT4
OsixCreateDir (const CHR1 * pc1DirName)
{
    UNUSED_PARAM (pc1DirName);
    return (-1);
}

/*****************************************************************************/
/*  Function Name   : LkMalloc                                          */
/*  Description     : To allocate a specified ammount of memory requested    */
/*                    by the caller.                                         */
/*  Input(s)        : Size of the memory to be allocated.                    */
/*  Output(s)       : None.                                                  */
/*  Returns         : Pointer to the memory allocated.                       */
/*****************************************************************************/
VOID               *
LkMalloc (UINT4 u4Size)
{
    VOID               *pPtr = NULL;
    UINT4               u4Size1;
    tLkMemHdr          *pMemHdr = NULL;
    tLkMemTail         *pMemTail = NULL;

    u4Size1 = u4Size + sizeof (tLkMemHdr) + sizeof (tLkMemTail);

    if (u4Size1 > KMALLOC_ALLOC_LIMIT)
    {
        pPtr = (VOID *) vmalloc (u4Size1);

    }
    else
    {
        pPtr = (VOID *) kmalloc (u4Size1, GFP_ATOMIC);
    }

    if (pPtr == NULL)
    {
        return NULL;
    }
    pMemHdr = pPtr;
    pPtr = pPtr + sizeof (tLkMemHdr);
    pMemTail = pPtr + u4Size;

    pMemHdr->u4Size = u4Size1;
    pMemHdr->u4Sig = 0x0505;
    pMemTail->u4Sig1 = pMemTail->u4Sig2 = 0x0A0A;

    return pPtr;
}

/*****************************************************************************/
/*  Function name   : LkCalloc                                          */
/*  Description     : To allocate a specified ammount of memory requested    */
/*                    and set it to 0.                                       */
/*  Input(s)        : u4Size   - size of the memory to be allocated.           */
/*                    u4Count - number of blocks of size s.                   */
/*  Output(s)       : None.                                                  */
/*  Returns         : Pointer to the memory allocated.                       */
/*****************************************************************************/
VOID               *
LkCalloc (UINT4 u4Count, UINT4 u4Size)
{
    VOID               *pPtr = NULL;

    pPtr = LkMalloc ((u4Size * u4Count));

    if (pPtr == NULL)
    {
        return NULL;
    }

    MEMSET (pPtr, 0, (u4Size * u4Count));
    return pPtr;
}

/*****************************************************************************/
/*  Function name   : LkFree                                           */
/*  Description     : To free the memory which is allocated before by        */
/*                    LkMalloc/LkCalloc.                           */
/*  Input(s)        : Pointer pPtr- pointer to the memory to be freed          */
/*  Output(s)       : Error message if fails to free the memory.             */
/*  Returns         : None.                                                  */
/*****************************************************************************/
VOID
LkFree (VOID *pPtr)
{
    tLkMemHdr          *pMemHdr = NULL;
    tLkMemTail         *pMemTail = NULL;

    if (pPtr == NULL)
    {
        return;
    }

    pMemHdr = (tLkMemHdr *) (pPtr - sizeof (tLkMemHdr));

    if (pMemHdr == NULL)
    {
        return;
    }

    if (pMemHdr->u4Sig != 0x0505)
    {
        printk ("pMemHdr: Incorrect pointer passed to LkFree\n");
        return;
    }

    pPtr = pPtr - sizeof (tLkMemHdr);
    pMemTail = (tLkMemTail *) (pPtr + (pMemHdr->u4Size - sizeof (tLkMemTail)));

    if ((pMemTail->u4Sig1 != 0x0A0A) || (pMemTail->u4Sig2 != 0x0A0A))
    {
        printk ("pMemTail: Incorrect pointer passed to LkFree\n");
        return;
    }

    if (pMemHdr->u4Size > KMALLOC_ALLOC_LIMIT)
    {
        vfree (pPtr);
    }
    else
    {
        kfree (pPtr);
    }
    return;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                    to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}
