/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osxprot.h,v 1.4 2015/04/28 12:10:14 siva Exp $
 *
 * Description:
 * OSIX's exported functions.
 *
 */

#ifndef OSIX_OSXPROT_H
#define OSIX_OSXPROT_H
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

#define OsixGetSemId(a, b, c)      (UINT4)OSIX_SUCCESS
#define OsixGetSysTime(a)           (*a = *a)


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

VOID        *
OsixGetCurEndDynMem (VOID);

#endif
