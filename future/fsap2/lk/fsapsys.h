/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapsys.h,v 1.6 2015/04/28 12:10:14 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */

#include <linux/autoconf.h>
#include <linux/kernel.h>
#include <asm/types.h>
#include <linux/slab.h>
#include <linux/ctype.h>
#include <linux/vmalloc.h>
#include <linux/version.h>
#include <asm/uaccess.h>
#include <asm/ioctl.h>
