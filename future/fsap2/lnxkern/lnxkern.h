/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxkern.h,v 1.2 2015/11/20 10:44:07 siva Exp $
 *
 * Description:This file contains the definitions of compilation switches
 *             for the respective kernel versions
 *******************************************************************/
#ifndef __LNXKERN_H__
#define __LNXKERN_H__

#include <linux/version.h>

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,4,20)
#define LINUX_KERNEL_2_4_20VER
#endif

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,18)
#define LINUX_KERNEL_2_6_18VER
#endif

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,21)
#define LINUX_KERNEL_2_6_21VER
#endif

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,25)
#define LINUX_KERNEL_2_6_25VER
#endif

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,27)
#undef LINUX_KERNEL_2_6_27VER
#define LINUX_KERNEL_2_6_27VER
#endif

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,28)
#undef LINUX_KERNEL_2_6_28VER
#define LINUX_KERNEL_2_6_28VER
#endif

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,32)
#define LINUX_KERNEL_2_6_32VER
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,3,4)
#define LINUX_KERNEL_3_3_4VER
#endif

#endif

