/*
 * Copyright Aricent Inc, 2011.
 *
 * $Id: srmbuf.h,v 1.1.1.1 2015/04/28 12:11:04 siva Exp $
 *
 * Description: SRM Buf module's exported file.
 *
 */

#ifndef  _SRMBUF_H
#define  _SRMBUF_H

#include "kerninc.h"
#include "srmmem.h"
#include "utlsll.h"
#include "lnxkern.h"
#include <linux/types.h>
/****************************************************
************* Defined constants  ********************
*****************************************************/

#define   CRU_SUCCESS    0
#define   CRU_FAILURE  (-1)

#define   CRU_BUF_FLOWDATA_SIZE                 200
#define   CRU_BUF_GetFirstDataDesc(pBuf)        (pBuf->pSkb)
#define   CRU_BUF_GetNextDataDesc(pDD)          (NULL)
#define   CRU_BUF_GetDataPtr(pDD)               (pDD->data)
#define CRU_BUF_Get_ModuleData(pBufChain) (&(pBufChain)->ModuleData)
#define   CRU_BUF_GetBlockValidByteCount(pDD)   0 
#define   CB_READ_OFFSET(pMsg)    (pMsg->ModuleData.u4Reserved1)

/*********************************************************
*** Type definition for Buffer related Data Structures ***
**********************************************************/

/*** Type Definition for Inter-Module Data Exchange ***/
typedef struct CRU_INTERFACE {
      UINT4  u4IfIndex;
      UINT2  u2_SubReferenceNum;
      UINT1  u1_InterfaceType;
      UINT1  u1_InterfaceNum;
}tCRU_INTERFACE;

typedef struct MODULE_DATA{
    UINT1           FlowData[CRU_BUF_FLOWDATA_SIZE];
    UINT4           u4TimeStamp;
    UINT1           SecModuleData[CRU_BUF_FLOWDATA_SIZE];
    UINT4           u4Reserved1;
}tMODULE_DATA;

/*** Type Definition of Buffer Manager Configuration Parameters ***/

typedef struct DataBlockCfg {

 UINT4 u4BlockSize;   /* size of data block.
        * Min size 128 bytes Max 4096 bytes
        *                          * Should be in
        *                          multiples of 4 */
 UINT4 u4NoofBlocks;  /* Max number of blocks of u4BlockSize */

}tDataBlockCfg;

typedef struct BufConfig {
 UINT4 u4MemoryType;           /* TYPE_0 or TYPE_1 or etc as
           * defined by Memory Pool Manager
           *                                   *
           *                                   default
           *                                   is
           *                                   MEM_DEFAULT_MEM_TYPE
           *                                   */
 UINT4 u4MaxChainDesc;         /* Max # of chain descriptors  */
 UINT4 u4MaxDataBlockCfg;      /* Array size of DataBlkCfg */
 tDataBlockCfg DataBlkCfg[1];  /*details of data block */
}tBufConfig;


/***** Type Definition for Buffer Chain Descriptor *****/
typedef struct CRU_BUF_CHAIN_DESC {
   tTMO_SLL_NODE      NextBufChainHeader;
   struct sk_buff    *pSkb;
   tMODULE_DATA       ModuleData;
   UINT1              u1ValidMemoryBlock;
} tCRU_BUF_CHAIN_DESC;

typedef tCRU_BUF_CHAIN_DESC  tCRU_BUF_CHAIN_HEADER;
typedef struct sk_buff tCRU_BUF_DATA_DESC;

/* This enum is to be used for u1ValidMemoryBlock flag in CRU_BUF_CHAIN_DESC */
enum 
{
    CRU_BUF_INVALID_MEMORY_BLOCK = 0,
    CRU_BUF_VALID_MEMORY_BLOCK = 1
};


/*********************************************************
*************** MACRO definition *************************
**********************************************************/

/*************************************************************
*** Macro To Move The Valid Offset Back By `u4NumOfUnits` ***
*** Note : Write Offset Is Set To `u4NumOfUnits` Read     ***
*** Offset is set to 0                                     ***
**************************************************************/
#define CRU_BMC_Move_Back_Valid_Offset(pBufChain,u4NumOfUnits) \
        CRU_BUF_Prepend_BufChain((pBufChain),NULL,(u4NumOfUnits));

/*******************************************************
**** Easy Macros For Interface Structure Components ****
********************************************************/
#define CRU_BUF_Get_Interface_Type(tInterface)  ((tInterface).u1_InterfaceType)
#define CRU_BUF_Get_Interface_Num(tInterface)   ((tInterface).u1_InterfaceNum)
#define CRU_BUF_Get_Interface_SubRef(tInterface) ((tInterface).u2_SubReferenceNum)
#define CRU_BUF_Get_IfIndex(tInterface)         ((tInterface).u4IfIndex)

#define CRU_BUF_Set_Interface_Type(tInterface, u1IfType)\
           ((tInterface).u1_InterfaceType = u1IfType)

#define CRU_BUF_Set_Interface_Num(tInterface, u1IfNum) \
           ((tInterface).u1_InterfaceNum = u1IfNum)

#define CRU_BUF_Set_Interface_SubRef(tInterface, u2IfSubRef) \
           ((tInterface).u2_SubReferenceNum = u2IfSubRef)

        /* Set both the old and new fields which hold the i/f index
         * Upon migration to the u4IfIndex field, remove IfNum.
         */
#define CRU_BUF_Set_IfIndex(tInterface, u4IfNum)            \
           ((tInterface).u1_InterfaceNum = (tInterface).u4IfIndex =\
            (UINT1)u4IfNum)

/*******************************************************
*** MACROs For Host To Network Byte Order Conversion ***
********************************************************/
#define CRU_BMC_WTOPDU(pu1PduAddr,u2Value) \
        *((UINT2 *)(pu1PduAddr)) = OSIX_HTONS(u2Value);

#define CRU_BMC_DWTOPDU(pu1PduAddr,u4Value) \
        *((UINT4 *)(pu1PduAddr)) = OSIX_HTONL(u4Value);

/*******************************************************
*** MACROs For Network To Host Byte Order Conversion ***
********************************************************/
#define CRU_BMC_WFROMPDU(pu1PduAddr) \
        OSIX_NTOHS(*((UINT2 *)(pu1PduAddr)))

#define CRU_BMC_DWFROMPDU(pu1PduAddr) \
        OSIX_NTOHL(*((UINT4 *)(pu1PduAddr)))

/*******************************************************
*** Macro To Get The Pointer To The First Valid Data ***
*** Byte In the Message Buffer-Chain.                ***
********************************************************/
#define CRU_BMC_Get_DataPointer(pBufChain) \
        ((pBufChain)->data)


/*
 * ASSIGN
 * updates VBC, FBC
 * does OSIX_HTON
 */
#define ASSIGN_1_BYTE(pMsg, u4Offset, u1Val)            \
do {                                                    \
    UINT4 u4OldVbc;                                     \
    UINT4 u4Added;                                      \
    *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = u1Val; \
    u4OldVbc = CB_VBC(pMsg);                            \
    if(u4Offset >= u4OldVbc) {                          \
        u4Added       = (u4Offset - u4OldVbc) + 1;      \
        CB_VBC(pMsg) += u4Added;                        \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;         \
   }                                                    \
}while(0)


#define ASSIGN_2_BYTE(pMsg, u4Offset, u2Val)                            \
do {                                                                    \
        UINT4 u4OldVbc;                                                 \
        UINT4 u4Added;                                                  \
        *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = OSIX_HTONS(u2Val); \
        u4OldVbc = CB_VBC(pMsg);                                        \
        if(u4Offset >= u4OldVbc) {                                      \
            u4Added       = (u4Offset - u4OldVbc) + 2;                  \
            CB_VBC(pMsg) += u4Added;                                    \
            CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                     \
   }                                                                    \
}while(0)

#define ASSIGN_4_BYTE(pMsg, u4Offset, u4Val)                        \
do {                                                                \
    UINT4 u4OldVbc;                                                 \
    UINT4 u4Added;                                                  \
    *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = OSIX_HTONL(u4Val); \
    u4OldVbc = CB_VBC(pMsg);                                        \
    if(u4Offset >= u4OldVbc) {                                      \
        u4Added       = (u4Offset - u4OldVbc) + 4;                  \
        CB_VBC(pMsg) += u4Added;                                    \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                     \
   }                                                                \
}while(0)

#define ASSIGN_N_BYTE(pMsg, pu1Src, u4Offset, u4Len)              \
do{                                                               \
    UINT4 u4OldVbc;                                               \
    UINT4 u4Added;                                                \
    MEMCPY((VOID *)CB_FVB(pMsg)+u4Offset, (VOID *)pu1Src, u4Len); \
    u4OldVbc = CB_VBC(pMsg);                                      \
    if(u4Offset >= u4OldVbc) {                                    \
        u4Added       = (u4Offset - u4OldVbc) + u4Len;            \
        CB_VBC(pMsg) += u4Added;                                  \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                   \
    }                                                             \
}while(0)

/*
 * APPEND
 * updates VBC, FBC
 * does OSIX_HTON
 */

#define APPEND_1_BYTE(pMsg, u1Val)                                   \
do {                                                                 \
    *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = u1Val; \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {                      \
        CB_VBC(pMsg) ++;                                             \
        CB_FBC(pMsg) --;                                             \
   }                                                                 \
}while(0)

#define APPEND_2_BYTE(pMsg, u2Val)                                \
do {                                                              \
        *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = \
            OSIX_HTONS(u2Val);                                    \
        if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {               \
            CB_VBC(pMsg) += 2;                                    \
            CB_FBC(pMsg) -= 2;                                    \
   }                                                              \
}while(0)

#define APPEND_4_BYTE(pMsg, u4Val)                            \
do{                                                           \
    *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = \
        OSIX_HTONL(u4Val);                                    \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {               \
        CB_VBC(pMsg) += 4;                                    \
        CB_FBC(pMsg) -= 4;                                    \
    }                                                         \
}while(0)

#define APPEND_N_BYTE(pMsg, pu1Src, u4Len)                        \
do{                                                               \
    MEMCPY((VOID *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)), \
           (VOID *)pu1Src, u4Len);                                \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {                   \
        CB_VBC(pMsg) += u4Len;                                    \
        CB_FBC(pMsg) -= u4Len;                                    \
    }                                                             \
}while(0)


/*
 * GET
 * does OSIX_NTOH.
 * no changes to state fields.
 */
#define GET_1_BYTE(pMsg, u4Offset, u1Val)\
        u1Val = *(UINT1 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset)

#define GET_2_BYTE(pMsg, u4Offset, u2Val)                     \
do{                                                           \
        u2Val = *(UINT2 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset); \
        u2Val = OSIX_NTOHS((u2Val));                            \
}while(0)

#define GET_4_BYTE(pMsg, u4Offset, u4Val)                     \
do{                                                           \
        u4Val = *(UINT4 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset); \
        u4Val = OSIX_NTOHL(u4Val);                            \
}while(0)


#define GET_N_BYTE(pMsg, pu1Dest, u4Offset, u4Len)              \
        MEMCPY((VOID *)pu1Dest,                                 \
               (const VOID *)((UINT1 *)CB_FVB(pMsg)+u4Offset),  \
               u4Len)


/*
 * EXTRACT
 *
 * CAVEAT:
 * Users *SHOULD* update readoffset, for true fsap1 behaviour
 */

#define EXTRACT_1_BYTE(pMsg, u1Val) \
        u1Val = *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg));

#define EXTRACT_2_BYTE(pMsg, u2Val)                                  \
do{                                                                  \
    u2Val = *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)) ; \
    u2Val = OSIX_NTOHS(u2Val);                                       \
}while(0)


#define EXTRACT_4_BYTE(pMsg, u4Val)                                  \
do{                                                                  \
    u4Val = *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)) ; \
    u4Val = OSIX_NTOHL(u4Val);                                       \
}while(0)

#define EXTRACT_N_BYTE(pMsg, pu1Dest, u4Len)                            \
do{                                                                     \
    MEMCPY((VOID *)pu1Dest,                                             \
    (const VOID *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)), u4Len); \
}while(0)

#define CB_IS_LINEAR_READ(pBuf, u4Offset, u4Size)                \
    ((CRU_BUF_MoveToReadOffset(pBuf, u4Offset, &pTmpSrcDataDesc, \
                               &pu1SrcData) == CRU_SUCCESS) ? 1 : 0)

/* Returns true if pBuf has space for u4Size bytes of data starting from offset u4Offset.
*/
#define CB_IS_LINEAR_WRITE(pBuf, u4Offset, u4Size)          \
((CRU_BUF_MoveToWriteOffset(pBuf,u4Offset,&pTmpDstDataDesc, \
                            &pu1DstData,u4Size) == CRU_SUCCESS) ? 1 : 0)



/*********************************************************
***********     Prototype  Declaration  ******************
**********************************************************/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

VOID UtlDmpMsg   ARG_LIST ((UINT4 u4CurType, UINT4 u4CurDirn,
                            tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Type,
                            UINT4 u4Dirn, UINT4 u4Len, UINT4 u4HdrReqd));

INT4 BufInitManager(tBufConfig *pBufCfg);
INT4 CRU_BUF_Shutdown_Manager(void);
tCRU_BUF_CHAIN_HEADER *CRU_BUF_Allocate_MsgBufChain(UINT4 u4Size, UINT4 u4Offset);
INT4 CRU_BUF_Release_MsgBufChain(tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 u1Force);
INT4 CRU_BUF_Copy_OverBufChain_AtEnd (tCRU_BUF_CHAIN_HEADER *pBufChain,
                                      UINT1 *pu1Source,UINT4 u4Offset, 
                                      UINT4 u4Size);
INT4 CRU_BUF_Copy_OverBufChain(tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 *pu1Src, UINT4 u4Offset, UINT4 u4Size);
INT4 CRU_BUF_Copy_FromBufChain(tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 *pu1Dst, UINT4 u4Offset, UINT4 u4Size);
INT4  CRU_BUF_Link_BufChains (tCRU_BUF_CHAIN_HEADER *pBufChain1, tCRU_BUF_CHAIN_HEADER *pBufChain2);
tCRU_BUF_CHAIN_HEADER *CRU_BUF_UnLink_BufChain(tCRU_BUF_CHAIN_HEADER *pBufChain);

INT4 CRU_BUF_Concat_MsgBufChains(tCRU_BUF_CHAIN_HEADER *pBuf1, tCRU_BUF_CHAIN_HEADER *pBuf2);
INT4 CRU_BUF_Fragment_BufChain(tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Size, tCRU_BUF_CHAIN_HEADER **ppFragBuf);
INT4 CRU_BUF_Prepend_BufChain(tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 *pu1Src, UINT4 u4Size);
tCRU_BUF_CHAIN_HEADER *CRU_BUF_Duplicate_BufChain(tCRU_BUF_CHAIN_HEADER *pBuf);
UINT4 CRU_BUF_Get_ChainValidByteCount(tCRU_BUF_CHAIN_HEADER *pBuf);
tCRU_BUF_CHAIN_HEADER *CRU_BUF_Delete_BufChainAtEnd(tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Size);
INT4 CRU_BUF_Move_ValidOffset(tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Units);
UINT1 *CRU_BUF_Get_DataPtr_IfLinear(tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Offset, UINT4 u4Size);
VOID CRU_BUF_Print_AllStats(VOID);

tCRU_BUF_CHAIN_DESC * CRU_BUF_Allocate_ChainDesc (VOID);
INT4 CRU_BUF_Release_ChainDesc (tCRU_BUF_CHAIN_DESC *);
UINT1 * CRU_BUF_GetSkbMacHeader(struct sk_buff * pSkb);
VOID CRU_BUF_SetSkbMacHeader(struct sk_buff *pSkb, INT4 i4Offset);
UINT1 * CRU_BUF_GetSkbNetworkHeaderRaw(struct sk_buff * pSkb);
struct iphdr * CRU_BUF_GetSkbNetworkHeaderIp(struct sk_buff * pSkb);
VOID CRU_BUF_Set_Network_Header (tCRU_BUF_CHAIN_HEADER *pBuf,  INT4 i4Offset);
UINT1 * CRU_BUF_GetSkbTransportHeader(struct sk_buff * pSkb);
VOID CRU_BUF_KernMemFreeSkb (struct sk_buff *pSkb);
PUBLIC INT4 CRU_BUF_MoveToReadOffset (tCRU_BUF_CHAIN_DESC * pChainDesc, UINT4 u4Offset,
                          tCRU_BUF_DATA_DESC ** ppDataDescPtr,
                          UINT1 **ppu1DataPtr);

INT4 CRU_BUF_MoveToWriteOffset (tCRU_BUF_CHAIN_DESC * pChainDesc,
                           UINT4 u4Offset,
                           tCRU_BUF_DATA_DESC ** ppDataDescPtr,
                           UINT1 **ppu1DataPtr, UINT4 u4Size);
#ifdef LINUX_KERNEL_2_4_20VER
struct sk_buff * CRU_BUF_KernSkbPad (struct sk_buff *pSkb, UINT4 u4Pad);
#else
INT4 CRU_BUF_KernSkbPad (struct sk_buff *pSkb, UINT4 u4Pad);
#endif
UINT1 * CRU_BUF_DataDesc_Push (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Len);
#ifdef LINUX_KERNEL_2_4_20VER
struct sk_buff * CRU_BUF_KernSkbCopyExpand (const struct sk_buff *pSkb,
                                          UINT4 u4NewHeadRoom,
                                          UINT4 u4NewTailRoom,
                                          INT4 gfp_mask);
#else
struct sk_buff * CRU_BUF_KernSkbCopyExpand (const struct sk_buff *pSkb,
                                          UINT4 u4NewHeadRoom,
                                          UINT4 u4NewTailRoom,
                                          gfp_t gfp_mask);
#endif
struct sk_buff * ArBufKernDequeueSkb (struct sk_buff_head *pDeQueueList);
VOID ArBufKernSkbQueueTail (struct sk_buff_head *pQueueList,
                            struct sk_buff *pNewSkb);
struct sk_buff * ArBufKernSkbReallocHeadroom (struct sk_buff *pSkb, 
                                              UINT4 u4HeadRoom);
tCRU_BUF_DATA_DESC *
CRU_BUF_Allocate_DataDesc (UINT4 u4Size);
INT4
CRU_BUF_Initialize_DataBlocks (tBufConfig * pBufCfg);
VOID
CRU_BUF_Sort_DataBlocks (void);
VOID
CRU_BUF_Sort_DataBlocks (void);
INT4
CRU_BUF_Create_ChainDescPool (UINT4 u4MaxChainDesc);
INT4
CRU_BUF_Delete_ChainDescPool (void);
INT4                                                                                       CRU_BUF_Create_DataBlockPool (tBufConfig * pBufCfg, UINT1 u1Flag);

INT4
CRU_BUF_Delete_DataBlockPool (void);
VOID
CRU_BUF_Copy_LinearBufToChain (tCRU_BUF_CHAIN_DESC * pChainDesc,
                               tCRU_BUF_DATA_DESC * pDataDesc, UINT1 *pu1Data,
                                         UINT1 *pu1Src, UINT4 u4Offset, UINT4 u4Size);

INT4                                                                                       CRU_BUF_Copy_LinearBufFromChain (tCRU_BUF_CHAIN_DESC * pChainDesc,
                                 tCRU_BUF_DATA_DESC * pDataDesc, UINT1 *pu1Data,
                                          UINT1 *pu1Dst, UINT4 u4Offset, UINT4 u4Size);      

tCRU_BUF_DATA_DESC *
CRU_BUF_Get_LastDataDesc (tCRU_BUF_DATA_DESC * pDataDesc);

INT4                                                                                       CRU_BUF_Split_MsgBufChainAtOffset (tCRU_BUF_CHAIN_DESC * pChainDesc,
                                   tCRU_BUF_DATA_DESC * pDataDesc,
                                              UINT1 *pu1Data,
                                                      tCRU_BUF_CHAIN_DESC ** ppFragChainDescPtr);
INT4                                                                                       CRU_BUF_Config_Validate (tBufConfig * pBufLibInitData);
INT4
CRU_BUF_Release_DataDesc (tCRU_BUF_DATA_DESC * pFirstDataDesc);
UINT4                                                                                      CRU_BUF_Get_ChainStartFreeByteCount (tCRU_BUF_CHAIN_DESC * pChainDesc);        

#ifdef SECURITY_KERNEL_WANTED
/*******************************************************************************
 *                    ProtoTypes for Security CRU Buffer                       *
********************************************************************************/
tCRU_BUF_CHAIN_DESC *SEC_CRU_BUF_Allocate_ChainDesc (VOID);
tCRU_BUF_CHAIN_HEADER * SEC_CRU_BUF_Allocate_MsgBufChain (UINT4 u4Size, UINT4 u4Offset);
tCRU_BUF_CHAIN_HEADER * SEC_CRU_BUF_Duplicate_BufChain (tCRU_BUF_CHAIN_HEADER * pBuf);
INT4 SEC_CRU_BUF_Release_ChainDesc (tCRU_BUF_CHAIN_DESC * pCruBuf);
INT4 SEC_CRU_BUF_Release_MsgBufChain (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Force);
#endif

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif  /* _SRMBUF_H */
