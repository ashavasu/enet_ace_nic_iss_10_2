/*
 * Copyright Aricent Inc, 2011.
 *
 * $Id: fsapsys.h,v 1.1.1.1 2015/04/28 12:11:04 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */
#ifndef _FSAPSYS_H_
#define _FSAPSYS_H_

#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/ctype.h>
#include <linux/vmalloc.h>
#include <asm/uaccess.h>
#include <asm/ioctl.h>
#include <linux/nfs_fs.h>

#endif
