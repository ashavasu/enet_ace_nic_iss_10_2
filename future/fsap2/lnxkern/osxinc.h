/*
 * Copyright Aricent Inc, 1999-2001.
 *
 * $Id: osxinc.h,v 1.1.1.1 2015/04/28 12:11:04 siva Exp $
 *
 * Description:
 * Header of all headers.
 *
 */

#ifndef _OSXINC_H_
#define _OSXINC_H_

#include "osxstd.h"
#include "srmmem.h"
#include "srmbuf.h"
#include "srmtmr.h"
#include "osxsys.h"
#include "utlmacro.h"
#include "utltrc.h"
#include "utldll.h"
#include "osxprot.h"

VOID         UtlTrcClose (VOID);

#endif
