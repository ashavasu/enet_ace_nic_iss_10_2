/*
 * Copyright Aricent Inc, 2011.
 *
 * $Id: osxprot.h,v 1.2 2015/09/02 11:58:10 siva Exp $
 *
 * Description:
 * OSIX's exported functions.
 *
 */

#ifndef OSIX_OSXPROT_H
#define OSIX_OSXPROT_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

UINT4 OsixGetSysTime(tOsixSysTime *pSysTime);

UINT4
OsixInit (tOsixCfg * pOsixCfg);

UINT4 OsixGetTps (VOID);
UINT4
OsixShutDown (void);

UINT4 OsixSendEvent (UINT4 u4Node, const UINT1 au1TskName[], UINT4 u4Events);

UINT4 OsixReceiveEvent (UINT4 u4QueId, UINT4 u4Flags, UINT4 u4Timeout, UINT4 *pu4RcvdEvts);

UINT4 OsixCreateQ (const UINT1 au1QName[4], UINT4 u4QDepth, UINT4 u4QMode,
             tOsixQId * pQId);

UINT4 OsixDeleteQ (UINT4 u4Node, const UINT1 au1QName[4]);

UINT4
OsixGetQId (UINT4 u4NodeId, const UINT1 au1QName[4], tOsixQId * pQId);

UINT4
OsixGetNumMsgsInQ (UINT4 u4Node, const UINT1 au1QName[4], UINT4 *pu4Msgs);


UINT4 OsixSendToQ (UINT4 u4Node, const UINT1 au1QName[4], 
                  tOsixMsg * pMsg,UINT4 u4Prio);

UINT4 OsixReceiveFromQ (UINT4 u4Node, const UINT1 au1QName[4], UINT4 u4Flags,
                  UINT4 u4Timeout, tOsixMsg ** ppu1Msg);

UINT4 OsixCreateSem (const UINT1 au1SemName[4], UINT4 u4InitialCount, UINT4 u4Flags,
               tOsixSemId * pSemId);

UINT4 OsixDeleteSem (UINT4 u4Node, const UINT1 au1SemName[4]);

UINT4 OsixGetSemId (UINT4 u4NodeId,
   const UINT1 au1SemName[4],
   tOsixSemId *pSemId);


UINT4 OsixTakeSem (UINT4 u4Node, const UINT1 au1SemName[4], UINT4 u4Flags,
             UINT4 u4Timeout);

UINT4 OsixGiveSem (UINT4 u4Node, const UINT1 au1SemName[4]);

UINT4 OsixCreateTask (const UINT1 au1TskName[4], UINT4 u4TskPrio, UINT4 u4StackSize,
                VOID (*TskStartAddr) (INT1 *), INT1 ai1TskArgs[1],
                UINT4 u4Mode, tOsixTaskId * pTskId);

UINT4 OsixDeleteTask (UINT4 u4NodeId, const UINT1 au1TskName[4]);

UINT4
OsixDelayTask (UINT4 u4Duration);

UINT4
OsixIntLock(void);

UINT4
OsixIntUnlock(UINT4 u4s);


VOID
OsixSetDbg(UINT4 u4Value);

VOID
Fsap2Start(void);

UINT4
OsixGetCurTaskId (void);

const UINT1 *
OsixExGetTaskName(tOsixTaskId TaskId);

UINT4
OsixReadNodeId (UINT4 *pu4NodeId);

UINT4
OsixGetMemInfo(UINT4 *pu4TotalMem, UINT4 *pu4FreeMem);

UINT4
OsixGetCPUInfo(UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage);

UINT4
OsixGetFlashInfo(UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash);

UINT4
OsixGetTaskId (UINT4 u4NodeId,
               const UINT1 au1TaskName [4],
                     tOsixTaskId *pTaskId);


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

VOID        *
OsixGetCurEndDynMem (VOID);

UINT4
OsixTakeTimedSem (UINT4 u4NodeId,
             const UINT1 au1SemName[4],
             UINT4 u4Flags,
             UINT4 u4Timeout);

#endif
