/*
 * Copyright (C) Aricent Inc, 2011.
 *
 * $Id: osix.h,v 1.4 2015/09/15 11:53:08 siva Exp $
 *
 * Description: This is the exported file for fsap.
 *              It contains exported defines and FSAP APIs.
 */
#ifndef _OSIX_H_
#define _OSIX_H_

#include "kerninc.h"
#include "osxinc.h"
#include "lr.h"
#include "fsbuddy.h"
#include "srmbuf.h"
#include "lnxkern.h"
#include "fsap.h"

#include <linux/autoconf.h>
#include <linux/workqueue.h>
#include  <linux/wait.h>
#include  <linux/delay.h>
#include <linux/version.h>
#include <linux/kthread.h>

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/*** Configurable parameters.              ***/
#define  OSIX_NAME_LEN           4
#define  OSIX_DEF_MSG_LEN        4
#define  OSIX_MAX_Q_MSG_LEN      4
#define  OSIX_TPS              100
#define  OSIX_STUPS            100
#define  OSIX_MAX_QUES         20
#define  OSIX_READ_QUE            1
#define  OSIX_WRITE_QUE           2
#define  OSIX_CHD_QUE             3
#define  OSIX_DEP_QUE             4
#define  OSIX_CPY_QUE             5
#define  OSIX_CPS_QUE             6
#define  OSIX_KTK_QUE             7
#define  OSIX_FILE_LOG           0        /* 0: disable file logging
                                           * 1: enable file logging
                                           */
#define OSIX_LOG_METHOD    CONSOLE_LOG 

enum
{
 KERN_USER_DATA_Q = 1,
 KERN_USER_CTRL_Q,
 KERN_USER_RQ,
 KERN_USER_WQ,
 KERN_TO_KERN_Q
};


/*** Shared between tmo.c and wrap.c       ***/
#define  OSIX_TSK                0
#define  OSIX_SEM                1
#define  OSIX_QUE                2
#define  OSIX_TRUE               1
#define  OSIX_FALSE              0
#define  OSIX_RSC_INV         NULL

/*** OS related constants used by fsap     ***/

/* LOW_PRIO  - Priority of lowest priority task.
 * HIGH_PRIO - Priority of highest priority task.
 */
#define  FSAP_LOW_PRIO         254
#define  FSAP_HIGH_PRIO          0
#define  OS_LOW_PRIO           254
#define  OS_HIGH_PRIO            0

    /* Scheduling Algorithm */

#define OSIX_SCHED_RR             (1 << 16)
#define OSIX_SCHED_FIFO           (1 << 17)
#define OSIX_SCHED_OTHER          (1 << 18)
    
/*** Wait flags used in OsixEvtRecv()      ***/
#undef   OSIX_WAIT
#undef   OSIX_NO_WAIT
#define  OSIX_WAIT               0
#define  OSIX_NO_WAIT            2

        /* FILE modes */
#define OSIX_FILE_RO          0x01
#define OSIX_FILE_WO          0x02
#define OSIX_FILE_RW          0x04
#define OSIX_FILE_CR          0x08
#define OSIX_FILE_AP          0x10

#define SYS_MAX_MEMPOOLS 1000
#ifdef LINUX_KERNEL_2_4_20VER

#include <linux/tqueue.h>
typedef struct tq_struct   tKernTaskStruct;
#endif

typedef struct sk_buff tSkb;
typedef struct sk_buff_head tSkBufHead;
typedef wait_queue_head_t tWaitQueueHead;
typedef spinlock_t tSpinLock;
typedef struct tasklet_struct tTasklet;
/*typedef struct sk_buff tSkb; */
typedef struct file tFile;
typedef struct inode tInode;
typedef struct net_device tNetDevice;
typedef struct packet_type tPktType;

extern INT4 gi4SecDevFd;
extern tOsixQId         gCfaPktQId;

/***          FILE modes                   ***/
#define OSIX_FILE_RO          0x01
#define OSIX_FILE_WO          0x02
#define OSIX_FILE_RW          0x04
#define OSIX_FILE_CR          0x08
#define OSIX_FILE_AP          0x10
#define OSIX_FILE_SY          0x20
#define OSIX_FILE_TR          0x40


#define  GDD_CHAR_DEV_Q_SIZE 50
#define OSIX_SEC_DEVICE_FILE_NAME "ksec"
#define OSIX_SEC_MAJOR_NUMBER 200
#define UNALLOCATED -1
#define OSIX_KERN_ONE 1
#define OSIX_KERN_ZERO 0

typedef VOID (*OsixTskEntry)(INT1 *);
VOID OsixGetTimeOfDay (struct timeval *pTimeval);
VOID OsixInitWaitqHead (wait_queue_head_t *pWaitQ);
VOID OsixInitSem (struct semaphore *pSem, UINT4 u4Value);
VOID OsixLkModuleInit (VOID *pCallbackFn);
VOID OsixLkModuleDeInit (VOID *pCallbackFn);
UINT4 OsixSemTake (tOsixSemId SemId);
UINT4 OsixSemGive (tOsixSemId SemId);
VOID  OsixSemDel (tOsixSemId SemId);

UINT4 OsixBHEnable (VOID);
UINT4 OsixBHDisable (VOID);

VOID OsixEnableLocalIrq (VOID);
VOID OsixDisableLocalIrq (VOID);
VOID OsixInitTimer (struct timer_list *pTimer);
VOID OsixDeleteTimer (struct timer_list *pTimer);
VOID OsixLkSpinLockInit (spinlock_t *pLock);
UINT4 OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen);
UINT4 OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events);

UINT4 OsixTskCrt (UINT1 au1TskName[], UINT4 u4TskPrio, UINT4 u4Data,
            VOID (*TskStartAddr) (INT1 *), INT1 *pArg, tOsixTaskId * pTskId);
UINT4 OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4Timeout);
UINT4 OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flg,
             UINT4 *pu4RcvEvents);
UINT4 OsixQueCrt (UINT1 au1QName[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId);
VOID OsixQueDel (tOsixQId QueId);
UINT4  FileCreate ( const CHR1* au1FileName, UINT4 u4Type, UINT4 u4Mode, 
         UINT4 u4MajorNumber, UINT4 u4MinorNumber);
UINT4 OsixTskIdSelf (tOsixTaskId * pTskId);

UINT4 OsixSemCrt (UINT1 au1SemName[], tOsixSemId * pSemId);

UINT4 OsixInitialize (VOID);
VOID OsixTskDel (tOsixTaskId TskId);

UINT4 OsixRscAddQType (UINT1 au1Name[], UINT4 u4RscType);
UINT4 OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId);
VOID OsixRscDel (UINT4 u4RscType, VOID *pRscId);
UINT4 OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen);
UINT4 OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, VOID *pRscId);
UINT4 OsixRscFindQType (UINT4 u4QueId, UINT4 *u4RscType);

UINT4 OsixSetLocalTime (void);
VOID Fsap2Start (VOID);
VOID Fsap2Shutdown (VOID);
UINT4 OsixSysRestart (VOID);

/*** API shared b/w tmo.c and wrap.c
     This is not an exported API           ***/

INT4       FileOpen        (const UINT1 *pu1FileName, INT4 i4Mode);
INT4       FileClose       (INT4 i4Fd);
UINT4      FileRead        (INT4 i4Fd, CHR1 *pBuf, UINT4 i4Count);
UINT4      FileWrite       (INT4 i4Fd, const CHR1 *pBuf, UINT4 i4Count);
INT4       FileDelete      (const UINT1 *pu1FileName);
UINT4      OsixTskDelay     ARG_LIST ((UINT4));
UINT4      OsixQueNumMsg    ARG_LIST ((tOsixQId, UINT4*));
UINT4      OsixGetSysUpTime ARG_LIST ((VOID));
UINT4      OsixSemTimedTake ARG_LIST ((tOsixSemId, UINT2));

UINT4
FsapShowTCB (UINT1 *pu1Result);
void
FsapTrace (UINT4 u4Flag, UINT4 u4Value, UINT4 *pu4TrcLvl);
UINT4
FsapShowSem (UINT1 au1SemName[], UINT1 *pu1Result, UINT4 *pu4NextIdx);
UINT4
FsapShowTask (UINT1 au1TskName[], UINT1 *pu1Result);


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif
#endif /* _OSIX_H_ */
