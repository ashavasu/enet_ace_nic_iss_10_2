/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: kerninc.h,v 1.1.1.1 2015/04/28 12:11:04 siva Exp $
 *
 * Description : This file contains the Common Linux related Includes 
 *               required for compiling the any module in Kernel Mode
 *
 ***************************************************************************/
#ifndef _KERN_INC_H_
#define _KERN_INC_H_

/* -------------------------- Kernel Header Files ----------------------- */

#include <linux/autoconf.h>
#include <linux/fs.h>  
#include <linux/module.h>  
#include <linux/init.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/if_arp.h>
#include <linux/inetdevice.h>
#include <linux/in.h>
#include <linux/version.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter_bridge.h>
#include <linux/ip.h>
#include <net/route.h>
#include <linux/icmp.h>
#include <linux/ctype.h>
#include <linux/random.h>

#endif /* _KERN_INC_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file kerninc.h                       */
/*-----------------------------------------------------------------------*/
