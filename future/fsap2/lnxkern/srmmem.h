/*
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: srmmem.h,v 1.1.1.1 2015/04/28 12:11:05 siva Exp $
 *
 * Description: SRM Mem module's exported file.
 *
 */

#ifndef SRM_MEM_H
#define SRM_MEM_H

#define  MEM_FREE_POOL_UNIT_COUNT(_Id)      MemGetFreeUnits(_Id)

/************************************************************************
*                                                                       *
*                      Defines and Typedefs                             *
*                                                                       *
*************************************************************************/
/* Error codes */
#define  MEM_SUCCESS                    0
#define  MEM_OK_BUT_NOT_ALIGNED         1
#define  MEM_FAILURE               (UINT4) (~0UL)

/* Debug Levels to be used in MemSetDbgLevel */
#define  MEM_DBG_MINOR                0x1
#define  MEM_DBG_MAJOR                0x2
#define  MEM_DBG_CRITICAL             0x4
#define  MEM_DBG_FATAL                0x8
#define  MEM_DBG_ALWAYS              0x10

/* Bit map representation of MEM TYPE */
#define MEM_DEFAULT_MEMORY_TYPE      0x00
#define MEM_HEAP_MEMORY_TYPE         0x01

#define  INVALID_POOL_ID                0

#if DEBUG_MEM == FSAP_ON
/* this macro returns the variable name ,that is passed , as a string */
#define VAR_STR(x) #x
#define MemCreateMemPool(size,u4NumNodes,MemType,PoolId) \
        MemCreateMemPoolDbg(size,u4NumNodes,MemType,PoolId,__FILE__,\
                            __FUNCTION__,__LINE__,VAR_STR(PoolId))
#endif
    
typedef struct MemChunkCfg
{
   UINT4 u4StartAddr;      /* StartAddr of  chunk */
   UINT4 u4Length;         /* Length of each chunk */
} tMemChunkCfg;

typedef struct MemTypeCfg
{
        UINT4 u4MemoryType;
        /* Type of memory, say, DRAM, SRAM, etc */

        UINT4 u4NumberOfChunks;
        /* Number of non-contiguous chunks present in this memory type  */

        tMemChunkCfg MemChunkCfg[1];
        /* Memory Chunk details */

} tMemTypeCfg;

typedef struct MemPoolCfg
{
        UINT4 u4MaxMemPools;
        /* Max number of memory pools in the system */

        UINT4 u4NumberOfMemTypes;
        /* Number of memory types present in the system */

        tMemTypeCfg MemTypes [1];
        /* Memory type details */

} tMemPoolCfg;


/* Health check related values */
enum 
{
    UP_AND_RUNNING = 1,
    DOWN_NONRECOVERABLE_ERR,
    UP_RECOVERABLE_RUNTIME_ERR
};
enum
{
    TASK_INIT_FAILURE = 1,
    INSUFFICIENT_STARTUP_MEMORY,
    CRU_BUFF_EXHAUSTED,
    CONFIG_RESTORE_FAILED,
    PROTOCOL_MEMPOOL_EXHAUSTED
};

typedef UINT4 tMemPoolId;

typedef struct {
    tMemPoolId  MemAllocErrPoolId;
    UINT1       u1MemAllocErrReason;
    UINT1       u1ErrorStatus;
    UINT1       au1Reserved[2];
} tOsixMemStatus;

/************************************************************************
*                                                                       *
*                      Function Prototypes                              *
*                                                                       *
*************************************************************************/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

UINT4
MemInitMemPool (tMemPoolCfg *pMemPoolCfg);

#if DEBUG_MEM == FSAP_ON
UINT4
MemCreateMemPoolDbg (UINT4 u4BlockSize, UINT4 u4NumberOfBlocks,
                     UINT4 u4TypeOfMemory, tMemPoolId *pPoolId,
                     const CHR1 *pu1FilePath, const CHR1 *pu1FuncName,
                     UINT4 u4LiineNo,const CHR1 *pu1Variable);

#define MemAllocateMemBlock(Id,pp) MemAllocateMemBlockLeak(Id, pp, __FILE__,__LINE__,__PRETTY_FUNCTION__)

#define MemAllocMemBlk(Id) MemAllocMemBlkLeak(Id,__FILE__,__LINE__,__PRETTY_FUNCTION__)

UINT4
MemAllocateMemBlockLeak (tMemPoolId PoolId, UINT1 **ppu1Block, const CHR1 *, UINT4, const CHR1 *);

VOID *
MemAllocMemBlkLeak (tMemPoolId PoolId, const CHR1 *, UINT4, const CHR1 *);

VOID
MemLeak (tMemPoolId PoolId);

UINT4
MemPrintMemPoolStatistics (tMemPoolId PoolId);

#else
UINT4
MemAllocateMemBlock (tMemPoolId PoolId, UINT1 **ppu1Block);

VOID * MemAllocMemBlk (tMemPoolId PoolId);

UINT4
MemCreateMemPool (UINT4 u4BlockSize, UINT4 u4NumberOfBlocks,
                  UINT4 u4TypeOfMemory, tMemPoolId *pPoolId);
#endif

UINT4
MemDeleteMemPool (tMemPoolId PoolId);

UINT4
MemReleaseMemBlock (tMemPoolId PoolId, UINT1 *pu1Block);

UINT4
MemGetFreeUnits (UINT4 u4QueID);

UINT4
MemShutDownMemPool (void);

VOID
MemSetDbg(UINT4 u4Val);

VOID 
OsixGetMemoryStatus (tOsixMemStatus *pMemoryStatus);
VOID
OsixSetHealthStatus (tOsixMemStatus *pHealthStatus);


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif
#endif
