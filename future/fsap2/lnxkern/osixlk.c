 /*
  * Copyright (C) 2011 Aricent Inc . All Rights Reserved  
  *
  * $Id: osixlk.c,v 1.2 2015/09/02 11:58:10 siva Exp $
  *
  * Description: Contains OSIX reference code for Linux Kernel.
  *              All basic OS facilities used by protocol software
  *              from Aricent, use only these APIs.
  */

#ifndef _OSIXLK_C_
#define _OSIXLK_C_

#include "osix.h"

struct timespec     gSysUpTimeInfo;
static clock_t      gStartTicks;

UINT4               gu4MyNodeId = 1;

struct semaphore    gOsixSemId;
typedef struct _LkMemHdr
{
    UINT4               u4Size;
    UINT4               u4Sig;
}

tLkMemHdr;

typedef struct _LkMemTail
{
    UINT4               u4Sig1;
    UINT4               u4Sig2;
}
tLkMemTail;

typedef struct OsixRscTskStruct
{
    tKthread           *pKthreadId;
    struct semaphore    EvtSemId;
    wait_queue_head_t   TskEvent;
    void                (*pTskStrtAddr) (INT1 *);
    INT1               *pArg;
    UINT4               u4Events;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    UINT2               u2Free;
    UINT2               u2CondWaitFlag;

}
tOsixTsk;

/* A circular queue is simulated using an allocated linear memory */
/* region. Read and write pointers are used to take out and put   */
/* messages in the queue. All messages are the same size only.    */
/* So, a task or thread reads messages from this queue to service */
/* the requests one by ine i.e. one command or activity at a time */

/* The description of fields used in struct below are as follows: */
/*   pQBase - linear memory location holding messages             */
/*   pQEnd - pointer after last byte of the queue                 */
/*   pQRead - pointer where next message can be read from         */
/*   pQWrite - pointer where next message can be written to       */
/*   u4MsgLen - the length of messages on this queue              */
/*   QueCond  - Conditional variable to synch. Send/Receive       */
/*   QueMutex - semaphore for mutual exclusion during writes      */
typedef struct
{
    UINT1              *pQBase;
    UINT1              *pQEnd;
    UINT1              *pQRead;
    UINT1              *pQWrite;
    UINT4               u4MsgLen;
    UINT4               u4OverFlows;
    UINT2               u2MsgQCondFlag;
    struct semaphore    MsgQSemId;
    wait_queue_head_t   MsgQEvent;
}
tKernMsgQ;

typedef struct
{
    tSkBufHead          QueueHead;
    tWaitQueueHead      QueueWait;
    tKernMsgQ          *pKernQId;
    UINT1               aQueName[OSIX_NAME_LEN];
    UINT4               i4QueueLen;
} tQueueInfo;

typedef struct
{
    tQueueInfo          QueueInfo;
    UINT4               u4MsgLen;
    INT1                i1Module;
    UINT1               u1Padding;
} tLkModQ;

tLkModQ             gaLkModQ[OSIX_MAX_QUES];

typedef struct OsixRscQueStruct
{
    UINT4               RscId;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    UINT4               u4QueType;
    UINT2               u2Free;
    UINT1               u1Pad[2];
}
tOsixQue;

typedef struct OsixRscSemStruct
{
    tOsixSemId          SemId;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    UINT2               u2Free;
    UINT1               u1Pad[2];

}
tOsixSem;

static INT4         Kernel_Receive_MsgQ (tOsixQId QId, UINT1 *pMsg,
                                         INT4 i4Timeout);
static INT4         Kernel_Send_MsgQ (tOsixQId QId, UINT1 *pMsg);
static VOID         Kernel_Delete_MsgQ (tKernMsgQ * QId);
static tKernMsgQ   *Kernel_Create_MsgQ (UINT4 u4MaxMsgs, UINT4 u4MsgLen);
static UINT4        Kernel_MsgQ_NumMsgs (tKernMsgQ * QId);
static INT4        *OsixTskWrapper ARG_LIST ((VOID *));

tOsixTsk            gaOsixTsk[OSIX_MAX_TSKS + 1];
/* 0th element is invalid and not used */

tOsixSem            gaOsixSem[OSIX_MAX_SEMS + 1];
/* 0th element is invalid and not used */

tOsixQue            gaOsixQue[OSIX_MAX_QUES + 1];
/* 0th element is invalid and not used */

extern UINT4        gu4Seconds;
/************************************************************************
 *  Function Name   : FileOpen
 *  Description        : This function stubs out the unused function calls
 *  Input                : pu1FileName - Name of file to open.
 *                             i4Mode      - whether r/w/rw.
 *  Returns             :   -1             otherwise.
 ************************************************************************/
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    UNUSED_PARAM (pu1FileName);
    UNUSED_PARAM (i4InMode);
    return 0;
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description        : This function stubs out the unused function calls
 *  Input                : i4Fd - File Descriptor to be closed.
 *  Returns             : -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    UNUSED_PARAM (i4Fd);
    return 0;
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description        : This function stubs out the unused function calls
 *  Input                 : i4Fd - File Descriptor.
 *                            pBuf - Buffer into which to read
 *                            i4Count - Number of bytes to read
 *  Returns             : 0.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    UNUSED_PARAM (i4Fd);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (i4Count);
    return (0);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description        : This function stubs out the unused function calls
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : 0
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    UNUSED_PARAM (i4Fd);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (i4Count);
    return (0);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description        : This function stubs out the unused function calls
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : -1
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    UNUSED_PARAM (pu1FileName);
    return (-1);
}

/*****************************************************************************/
/*  Function Name   : LkMalloc                                               */
/*  Description     : To allocate a specified amount of memory requested     */
/*                    by the caller.                                         */
/*  Input(s)        : Size of the memory to be allocated.                    */
/*  Output(s)       : None.                                                  */
/*  Returns         : Pointer to the memory allocated.                       */
/*****************************************************************************/
VOID               *
LkMalloc (UINT4 u4Size)
{
    VOID               *pPtr = NULL;
    UINT4               u4Size1;
    tLkMemHdr          *pMemHdr = NULL;
    tLkMemTail         *pMemTail = NULL;

    u4Size1 = u4Size + sizeof (tLkMemHdr) + sizeof (tLkMemTail);

    if (u4Size1 > KMALLOC_ALLOC_LIMIT)
    {
        pPtr = (VOID *) vmalloc (u4Size1);

    }
    else
    {
        pPtr = (VOID *) kmalloc (u4Size1, GFP_ATOMIC);
    }

    if (pPtr == NULL)
    {
        return NULL;
    }
    pMemHdr = pPtr;
    pPtr = pPtr + sizeof (tLkMemHdr);
    pMemTail = pPtr + u4Size;

    pMemHdr->u4Size = u4Size1;
    pMemHdr->u4Sig = 0x0505;
    pMemTail->u4Sig1 = pMemTail->u4Sig2 = 0x0A0A;

    return pPtr;
}

/*****************************************************************************/
/*  Function name   : LkCalloc                                               */
/*  Description     : To allocate a specified amount of memory requested     */
/*                    and set it to 0.                                       */
/*  Input(s)        : u4Size   - size of the memory to be allocated.         */
/*                    u4Count - number of blocks of size s.                  */
/*  Output(s)       : None.                                                  */
/*  Returns         : Pointer to the memory allocated.                       */
/*****************************************************************************/
VOID               *
LkCalloc (UINT4 u4Count, UINT4 u4Size)
{
    VOID               *pPtr = NULL;

    pPtr = LkMalloc ((u4Size * u4Count));

    if (pPtr == NULL)
    {
        return NULL;
    }

    MEMSET (pPtr, 0, (u4Size * u4Count));
    return pPtr;
}

/*****************************************************************************/
/*  Function name   : LkFree                                                 */
/*  Description     : To free the memory which is allocated before by        */
/*                    LkMalloc/LkCalloc.                                     */
/*  Input(s)        : Pointer pPtr- pointer to the memory to be freed        */
/*  Output(s)       : Error message if fails to free the memory.             */
/*  Returns         : None.                                                  */
/*****************************************************************************/
VOID
LkFree (VOID *pPtr)
{
    tLkMemHdr          *pMemHdr = NULL;
    tLkMemTail         *pMemTail = NULL;

    if (pPtr == NULL)
    {
        return;
    }

    pMemHdr = (tLkMemHdr *) (pPtr - sizeof (tLkMemHdr));

    if (pMemHdr == NULL)
    {
        return;
    }

    if (pMemHdr->u4Sig != 0x0505)
    {
        printk ("pMemHdr: Incorrect pointer passed to LkFree\n");
        return;
    }

    pPtr = pPtr - sizeof (tLkMemHdr);
    pMemTail = (tLkMemTail *) (pPtr + (pMemHdr->u4Size - sizeof (tLkMemTail)));

    if ((pMemTail->u4Sig1 != 0x0A0A) || (pMemTail->u4Sig2 != 0x0A0A))
    {
        printk ("pMemTail: Incorrect pointer passed to LkFree\n");
        return;
    }

    if (pMemHdr->u4Size > KMALLOC_ALLOC_LIMIT)
    {
        vfree (pPtr);
    }
    else
    {
        kfree (pPtr);
    }
    return;
}

/*****************************************************************************/
/*  Function name   : OsixTskCrt                                             */
/*  Description     : Wrapper function for the initialisation of a tasklet   */
/*                    If the Task Name is ROOT then gLrMainTasklet           */
/*                    is assigned to                                         */
/*                           tasklet structure                               */
/*                          If the Task Name is SEC then gSecTasklet is      */
/*                          assigned to                                      */
/*                           tasklet structure                               */
/*  Input(s)        : au1TskName - Name of the Task                          */
/*                        TskStartAddr - Entry Function of the Task          */
/*                         u4Data - Data to be passed to the tasklet         */
/*                    pCallbkfn - ptr to the callback function               */
/*                    u4Data - data to the tasklet                           */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
UINT4
OsixTskCrt (UINT1 au1TskName[], UINT4 u4TskPrio, UINT4 u4Data,
            VOID (*TskStartAddr) (INT1 *), INT1 *pArg, tOsixTaskId * pTskId)
{
    tOsixTsk           *pTsk = 0;
    UINT4               u4Idx = 0;
    UINT4               u4Flags = 0;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    UNUSED_PARAM (u4TskPrio);
    UNUSED_PARAM (pTskId);

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1TskName, OSIX_NAME_LEN);

    if (OsixRscFind (au1Name, OSIX_TSK, (VOID *) pTskId) == OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);    /* Task by this name already exists */
    }

    /* For tasks, the lnxkern version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0.   */

    if (OsixRscAdd (au1Name, OSIX_TSK, NULL) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    OsixRscFind (au1Name, OSIX_TSK, (VOID *) pTskId);
    u4Idx = *pTskId;
    pTsk = &gaOsixTsk[u4Idx];

    /* Create a semaphore for this particular task
     * and populate the same in TCB for further reference */

    sema_init (&(pTsk->EvtSemId), 1);

    /* Store the TaskStartAddr and Args in TCB for further reference */
    pTsk->pTskStrtAddr = TskStartAddr;
    pTsk->pArg = pArg;

    /* Thread Creation in Kernel */
    pTsk->pKthreadId =
        kthread_create (OsixTskWrapper, (void *) pTsk, (const CHR1 *) au1Name);
    if (pTsk->pKthreadId == NULL)
    {
        return (OSIX_FAILURE);
    }

    /* spawn the thread */
    wake_up_process (pTsk->pKthreadId);

    /* Event Handling Framework */
    init_waitqueue_head (&(pTsk->TskEvent));

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixTaskWrapper                                   */
/*  Description     : Intermediate function between OsixTskCrt and      */
/*                  : application entry point function, which serves    */
/*                  : to prevent the application from kicking off even  */
/*                  : before CreateTask has completed.                  */
/*  Input(s)        : pArg - task arguments passed here.                */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
static INT4        *
OsixTskWrapper (VOID *pArg)
{
    void                (*TaskPtr) (INT1 *);
    tOsixTsk           *pTsk = (tOsixTsk *) pArg;
    tOsixTaskId         TskId;
    UINT4               u4Flags = 0;
    INT1               *pFuncArg = NULL;

    if ((pTsk != NULL) && (pTsk->pTskStrtAddr != NULL))
    {
        TaskPtr = pTsk->pTskStrtAddr;
        pFuncArg = pTsk->pArg;
    }
    else
    {
        return 0;
    }

    /* Call the actual application Entry Point function. */
    (*TaskPtr) (pFuncArg);

    if (OsixTskIdSelf (&TskId) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    OsixTskDel (TskId);

    return 0;
}

/************************************************************************/
/*  Function Name   : OsixTskDel                                        */
/*  Description     : Deletes a task.                                   */
/*  Input(s)        : TskId          - Task Id                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixTskDel (tOsixTaskId TskId)
{
    OsixRscDel (OSIX_TSK, (VOID *) &TskId);
    return;
}

/************************************************************************/
/*  Function Name   : OsixTskDelay                                      */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskDelay (UINT4 u4Duration)
{

    UINT4               u4Interval = u4Duration / OSIX_STUPS * 1000;    /*In msecs */
    msleep (u4Interval);

    return OSIX_SUCCESS;
}

UINT4
OsixTskIdSelf (tOsixTaskId * pTskId)
{
    struct task_struct *pTsk = current;
    UINT4               u4Idx = 0;
    UINT4               u4Flags = 0;
    OsixSemTake (&gOsixSemId);

    /* scan global task array to find the task */
    for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        /* validate the pointers */
        if ((pTsk == NULL) || (gaOsixTsk[u4Idx].pKthreadId == NULL))
        {
            continue;
        }
        if (MEMCMP
            (pTsk, gaOsixTsk[u4Idx].pKthreadId,
             sizeof (struct task_struct *)) == 0)
        {
            /***
             * For the case of tasks, applications know only our array
             * index.  This helps us to simulate events.             
             ***/
            *pTskId = u4Idx;
            OsixSemGive (&gOsixSemId);
            return (OSIX_SUCCESS);
        }
    }
    OsixSemGive (&gOsixSemId);
    return (OSIX_FAILURE);

}

/************************************************************************/
/*  Function Name   : OsixQueCrt                                        */
/*  Description     : Initializes the queue in the kernel.              */
/*  Input(s)        : au1name[ ] - The Name of the Queue.               */
/*                     : u4MaxMsgs  - Max messages that can be held.    */
/*                     : u4MaxMsgLen- Max length of a messages          */
/*                       pQueId   - Queue Identifier.                   */
/*  Output(s)      : pQueId     - The QId returned.                     */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueCrt (UINT1 au1QName[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId)
{

    INT4                i4Index = 0;
    UINT4               u4QueType = 0;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    VOID               *pId = NULL;

    UNUSED_PARAM (u4MaxMsgLen);
    UNUSED_PARAM (u4MaxMsgs);

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1QName, OSIX_NAME_LEN);

    /* the LnxKern version of OsixRscFind returns pointer to void pointer,
     * hence assigned the QueId pointer to void pointer to remove dereferncing 
     * type-punned warning */

    pId = pQueId;
    if (OsixRscFind (au1Name, OSIX_QUE, pId) == OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);
    }

    for (i4Index = 1; i4Index < OSIX_MAX_QUES; i4Index++)
    {

        if (gaLkModQ[i4Index].i1Module == OSIX_FALSE)
        {
            switch (*pQueId)
            {
                case KERN_USER_RQ:
                {
                    u4QueType = OSIX_READ_QUE;
                    gaLkModQ[i4Index].QueueInfo.i4QueueLen =
                        GDD_CHAR_DEV_Q_SIZE;
                    init_waitqueue_head (&
                                         (gaLkModQ[i4Index].QueueInfo.
                                          QueueWait));
                    skb_queue_head_init (&
                                         (gaLkModQ[i4Index].QueueInfo.
                                          QueueHead));
                }
                    break;
                case KERN_USER_WQ:
                {
                    u4QueType = OSIX_WRITE_QUE;
                    gaLkModQ[i4Index].QueueInfo.i4QueueLen =
                        GDD_CHAR_DEV_Q_SIZE;
                    init_waitqueue_head (&
                                         (gaLkModQ[i4Index].QueueInfo.
                                          QueueWait));
                    skb_queue_head_init (&
                                         (gaLkModQ[i4Index].QueueInfo.
                                          QueueHead));
                }
                    break;

                case KERN_USER_CTRL_Q:
                {
                    u4QueType = OSIX_CPY_QUE;
                }
                    break;

                case KERN_USER_DATA_Q:
                {
                    u4QueType = OSIX_CPS_QUE;
                }
                    break;

                default:
                {
                    u4QueType = OSIX_KTK_QUE;
                    gaLkModQ[i4Index].QueueInfo.pKernQId =
                        Kernel_Create_MsgQ (u4MaxMsgs, u4MaxMsgLen);
                    if (gaLkModQ[i4Index].QueueInfo.pKernQId == NULL)
                    {
                        return (OSIX_FAILURE);
                    }
                }
                    break;
            }                    /* End of switch */
            break;
        }                        /* End of if */
    }                            /* End of for */

    *pQueId = i4Index;
    if (OsixRscAdd (au1Name, OSIX_QUE, pQueId) == OSIX_FAILURE)
    {
        gaLkModQ[i4Index].i1Module = OSIX_FALSE;
        return (OSIX_FAILURE);
    }

    gaLkModQ[i4Index].i1Module = OSIX_TRUE;
    if (OsixRscAddQType (au1Name, u4QueType) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return (OSIX_SUCCESS);

}

/************************************************************************/
/*  Function Name   : OsixQueDel                                        */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
VOID
OsixQueDel (tOsixQId QueId)
{
    UINT4               u4QueType = 0;

    if (OSIX_FAILURE == OsixRscFindQType (QueId, &u4QueType))
    {
        return;
    }

    if ((u4QueType == OSIX_READ_QUE) || (u4QueType == OSIX_WRITE_QUE))
    {
        skb_queue_purge (&(gaLkModQ[QueId].QueueInfo.QueueHead));
        gaLkModQ[QueId].QueueInfo.i4QueueLen = UNALLOCATED;
    }
    OsixRscDel (OSIX_QUE, &QueId);

    return;
}

/************************************************************************/
/*  Function Name   : OsixRscFind                                       */
/*  Description     : This locates Resource-Id by Name                  */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*  Output(s)       : pu4RscId - Osix Resource-Id                       */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, VOID *pRscId)
{
    tOsixSemId         *pOsixSemId = NULL;
    UINT4               u4Idx = 0;
    UINT4               u4Flags = 0;
    UINT4              *pu4Id = NULL;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }

    OsixSemTake (&gOsixSemId);

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixTsk[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /***
                     * For the case of tasks, applications know only our array
                     * index.  This helps us to simulate events.             
                     ***/
                    pu4Id = pRscId;
                    *pu4Id = u4Idx;
                    OsixSemGive (&gOsixSemId);
                    return (OSIX_SUCCESS);
                }
            }
            break;
        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixSem[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* pThread version of OsixRscFind returns pointer to semId */
                    pOsixSemId = (tOsixSemId *) pRscId;
                    *pOsixSemId = gaOsixSem[u4Idx].SemId;
                    OsixSemGive (&gOsixSemId);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixQue[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *((UINT4 *) pRscId) = gaOsixQue[u4Idx].RscId;
                    OsixSemGive (&gOsixSemId);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (&gOsixSemId);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscFindQType                                  */
/*  Description     : THie function finds the queue type for the given  */
/*  Name                                                                */
/*  Input(s)        : au1Name -   Name of the queue                     */
/*                  :                                                   */
/*  Output(s)       : u4RscType - Queue Type                            */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscFindQType (UINT4 u4QueId, UINT4 *u4RscType)
{
    UINT4               u4Idx = 0;
    UINT4               u4Flags = 0;

    for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if (u4QueId == gaOsixQue[u4Idx].RscId)
        {
            *u4RscType = gaOsixQue[u4Idx].u4QueType;
            return (OSIX_SUCCESS);
        }
    }
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscAddQType                                   */
/*  Description     : THie function Adds the queue type for the given   */
/*  Name                                                                */
/*  Input(s)        : au1Name -   Name of the queue                     */
/*                  :                                                   */
/*  Output(s)       : u4RscType - Queue Type                            */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscAddQType (UINT1 au1Name[], UINT4 u4RscType)
{
    UINT4               u4Idx = 0;
    UINT4               u4Flags = 0;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }

    OsixSemTake (&gOsixSemId);

    for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if (MEMCMP
            (au1Name, gaOsixQue[u4Idx].au1Name, (OSIX_NAME_LEN + 4)) == 0)
        {
            gaOsixQue[u4Idx].u4QueType = u4RscType;
            OsixSemGive (&gOsixSemId);

            return (OSIX_SUCCESS);
        }
    }
    OsixSemGive (&gOsixSemId);

    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscAdd                                        */
/*  Description     : Gets a free resouce, stores Resource-Id & Name    */
/*                  : This helps in locating Resource-Id by Name        */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx = 0;
    UINT4               u4Flags = 0;

    OsixSemTake (&gOsixSemId);

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixTsk[u4Idx].u2Free = OSIX_FALSE;

                    MEMCPY (gaOsixTsk[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));

                    OsixSemGive (&gOsixSemId);
                    return (OSIX_SUCCESS);
                }
            }
            break;
        case OSIX_SEM:
            /* scan global semaphore array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixSem[u4Idx].SemId = (tOsixSemId) pu4RscId;
                    MEMCPY (gaOsixSem[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (&gOsixSemId);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixQue[u4Idx].RscId = *pu4RscId;
                    gaOsixQue[u4Idx].u2Free = OSIX_FALSE;
                    MEMCPY (gaOsixQue[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (&gOsixSemId);

                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }

    OsixSemGive (&gOsixSemId);

    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscDel                                        */
/*  Description     : Free an allocated resouce                         */
/*  Input(s)        : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixRscDel (UINT4 u4RscType, VOID *pRscId)
{
    UINT4               u4Idx = 0;
    UINT4               u4Flags = 0;

    OsixSemTake (&gOsixSemId);

    switch (u4RscType)
    {
        case OSIX_TSK:
            u4Idx = *((UINT4 *) pRscId);
            gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
            MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (gaOsixSem[u4Idx].SemId == (tOsixSemId) pRscId)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixSem[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].RscId) == *((UINT4 *) pRscId))
                {
                    gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixQue[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (&gOsixSemId);

}

/************************************************************************/
/*  Function Name   : OsixQueSend                                       */
/*  Description     : Sends a message to a Queue.                       */
/*                         If the Queue Id is IDS_QUE_ID then it sends  */
/*                         the                                          */
/*                         message to the SNORT queue                   */
/*                         If the Queue Id is CFA_PACKET_QID then it    */
/*                         sends the                                    */
/*                         message to the SEC USER  queue               */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*                  : u4MsgLen - length of the message                  */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen)
{
    UINT4               u4QueType = 0;

    UNUSED_PARAM (u4MsgLen);

    if (OSIX_FAILURE == OsixRscFindQType (QueId, &u4QueType))
    {
        return OSIX_FAILURE;
    }

    if (u4QueType == OSIX_CPY_QUE)
    {
        if (copy_to_user
            (((tOsixKernUserInfo *) pu1Msg)->pDest,
             ((tOsixKernUserInfo *) pu1Msg)->pSrc, u4MsgLen))
        {
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    else if (u4QueType == OSIX_KTK_QUE)
    {
        if ((pu1Msg != NULL) && (u4MsgLen != 0))
        {
            if (Kernel_Send_MsgQ (QueId, pu1Msg) == -1)
            {
                return OSIX_FAILURE;
            }
            gaLkModQ[QueId].u4MsgLen = u4MsgLen;
            return OSIX_SUCCESS;
        }
        else
        {
            return OSIX_FAILURE;
        }
    }

    if (gaLkModQ[QueId].QueueInfo.QueueHead.qlen >
        gaLkModQ[QueId].QueueInfo.i4QueueLen)
    {
        return OSIX_FAILURE;
    }

    if ((u4QueType == OSIX_READ_QUE) || (u4QueType == OSIX_WRITE_QUE))
    {
        UINT4               Ptr;
        tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

        MEMCPY (&Ptr, pu1Msg, u4MsgLen);
        pBuf = (tCRU_BUF_CHAIN_HEADER *) Ptr;
        __skb_queue_tail (&(gaLkModQ[QueId].QueueInfo.QueueHead), pBuf->pSkb);
        wake_up_interruptible (&(gaLkModQ[QueId].QueueInfo.QueueWait));
#ifdef SECURITY_KERNEL_WANTED
        SEC_CRU_BUF_Release_ChainDesc (pBuf);
#endif
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixEvtSend                                       */
/*  Description     : Sends an event to a specified task.               */
/*  Input(s)        : TskId          - Task Id                          */
/*                  : u4Events       - The Events, OR-d                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events)
{
    UINT4               u4Flags = 0;

    OsixSemTake (&(gaOsixTsk[TskId].EvtSemId));
    gaOsixTsk[TskId].u4Events |= u4Events;
    gaOsixTsk[TskId].u2CondWaitFlag = OSIX_KERN_ONE;
    OsixSemGive (&(gaOsixTsk[TskId].EvtSemId));
    wake_up (&gaOsixTsk[TskId].TskEvent);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueRecv                                       */
/*  Description     : Receives a message from a Queue.                  */
/*                         If the Queue Id is SECUSR_QUE_ID then it     */
/*                         receives the message                         */
/*                          from SECUSER Queue                          */
/*                         If the Queue Id is IDS_QUE_ID then it        */
/*                         receives the message                         */
/*                          from SNORT Queue                            */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : u4QueType -  Indicates the type of the queue      */
/*                  whether read                                        */
/*                                         queue or write queue         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4TimeOut)
{
    UINT4               u4QueType = 0;
    tSkb               *pSkb = NULL;

    UNUSED_PARAM (u4MsgLen);

    if (OsixRscFindQType (QueId, &u4QueType) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }
    if (u4QueType == OSIX_READ_QUE)
    {
        if (gaLkModQ[QueId].QueueInfo.QueueHead.qlen == 0)    /* no available data */
        {
            /* Timed wait is not implemented. We wait forever */

            if (i4TimeOut == OSIX_NO_WAIT)
            {
                return OSIX_FAILURE;
            }

#ifdef LINUX_KERNEL_2_4_20VER

            interruptible_sleep_on (&(gaLkModQ[QueId].QueueInfo.QueueWait));
#else
            wait_event_interruptible ((gaLkModQ[QueId].QueueInfo.QueueWait),
                                      (gaLkModQ[QueId].QueueInfo.QueueHead.
                                       qlen != 0));
#endif

            if (signal_pending (current))
            {
                return OSIX_FAILURE;
            }

        }

        pSkb = __skb_dequeue (&(gaLkModQ[QueId].QueueInfo.QueueHead));
        MEMCPY (pu1Msg, &pSkb, OSIX_DEF_MSG_LEN);
    }
    else if (u4QueType == OSIX_WRITE_QUE)
    {
        pSkb = __skb_dequeue (&(gaLkModQ[QueId].QueueInfo.QueueHead));
        MEMCPY (pu1Msg, &pSkb, OSIX_DEF_MSG_LEN);
    }
    else if (u4QueType == OSIX_CPS_QUE)
    {
        pSkb = (tSkb *) ((tOsixKernUserInfo *) (pu1Msg))->pDest;
        if ((pSkb->tail + u4MsgLen) > pSkb->end)
        {
            CRU_BUF_KernMemFreeSkb (((tOsixKernUserInfo *) pu1Msg)->pDest);
            return OSIX_FAILURE;
        }
        if (copy_from_user
            ((skb_put (((tOsixKernUserInfo *) (pu1Msg))->pDest, u4MsgLen)),
             ((tOsixKernUserInfo *) pu1Msg)->pSrc, u4MsgLen))
        {
            CRU_BUF_KernMemFreeSkb (((tOsixKernUserInfo *) pu1Msg)->pDest);
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    else if (u4QueType == OSIX_CPY_QUE)
    {
        if (copy_from_user (((tOsixKernUserInfo *) pu1Msg)->pDest,
                            ((tOsixKernUserInfo *) pu1Msg)->pSrc, u4MsgLen))
        {
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;

    }
    else if (u4QueType == OSIX_KTK_QUE)
    {
        if (Kernel_Receive_MsgQ (QueId, pu1Msg, i4TimeOut) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;

    }
    else
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : OsixEvtRecv                                       */
/*  Description     : This function is used to wait on event to         */
/*                    receive an event                                  */
/*                    If the task Id is CFA_TASK_ID then it will        */
/*                    wait on the sec user queue                        */
/*                    to receive an event                               */
/*                    If the task Id is IDS_TASK_ID then it will        */
/*                    wait on the Snort queue                           */
/*                    to receive an event                               */
/*  Input(s)        : TskId             - Task Id                       */
/*                     : u4Events         - List of interested events.  */
/*                     : u4Flg              - Flags to be used          */
/*  Output(s)       : pu4EventsReceived - Events received.              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flg,
             UINT4 *pu4RcvEvents)
{
    DEFINE_WAIT (wait);
    UINT4               u4Idx = (UINT4) TskId;
    UINT4               u4Flags = 0;
    *pu4RcvEvents = 0;

    OsixSemTake (&(gaOsixTsk[TskId].EvtSemId));
    if ((u4Flg == OSIX_NO_WAIT) &&
        (((gaOsixTsk[u4Idx].u4Events) & u4Events) == 0))
    {
        OsixSemGive (&(gaOsixTsk[TskId].EvtSemId));
        return (OSIX_FAILURE);
    }
    OsixSemGive (&(gaOsixTsk[TskId].EvtSemId));
    while (OSIX_TRUE)
    {
        add_wait_queue (&gaOsixTsk[TskId].TskEvent, &wait);
        set_current_state (TASK_INTERRUPTIBLE);
        if (((gaOsixTsk[u4Idx].u4Events) & u4Events) != 0)
        {
            OsixSemTake (&(gaOsixTsk[TskId].EvtSemId));
            /* A required event has happened. No need to wait */
            finish_wait (&gaOsixTsk[TskId].TskEvent, &wait);
            *pu4RcvEvents = (gaOsixTsk[u4Idx].u4Events) & u4Events;
            gaOsixTsk[u4Idx].u4Events &= ~(*pu4RcvEvents);
            OsixSemGive (&(gaOsixTsk[TskId].EvtSemId));
            return (OSIX_SUCCESS);
        }
        /* Event not happened, and hence goes to sleep
         * until some events happen i.e. OsixEvtSend is made */
        schedule ();
        finish_wait (&gaOsixTsk[TskId].TskEvent, &wait);
    }
}

/*****************************************************************************/
/*  Function name   : OsixGetTimeOfDay                                       */
/*  Description     : Wrapper function to get the time of the day            */
/*  Input(s)        : None                                                   */
/*  Output(s)       : pTimeval - pointer to the timeval to be set            */
/*  Returns         : None                                                   */
/*****************************************************************************/
VOID
OsixGetTimeOfDay (struct timeval *pTimeval)
{
    do_gettimeofday (pTimeval);
    return;
}

/************************************************************************/
/*  Function Name   : OsixSemCrt                                        */
/*  Description     : Creates a sema4.                                  */
/*  Input(s)        : au1Name [ ] - Name of the sema4.                  */
/*  Output(s)       : pSemId         - The sema4 Id.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/

UINT4
OsixSemCrt (UINT1 au1SemName[], tOsixSemId * pSemId)
{

    UINT1               au1Name[OSIX_NAME_LEN + 4];

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1SemName, OSIX_NAME_LEN);

    if (OsixRscFind (au1Name, OSIX_SEM, (VOID *) pSemId) == OSIX_SUCCESS)
    {
        /* Semaphore by this name already exists. */
        return (OSIX_FAILURE);
    }

    *pSemId = (tOsixSemId) LkMalloc (sizeof (struct semaphore));

    if (*pSemId == NULL)
    {
        return OSIX_FAILURE;
    }

    sema_init (*pSemId, 1);

    if (OsixRscAdd (au1Name, OSIX_SEM, (UINT4 *) *pSemId) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*  Function name   : OsixSemTake                                            */
/*  Description     : Function to acquire the spin lock by disabling the     */
/*                    interrupts locally                                     */
/*  Input(s)        : SemId - Semaphore Id                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : OSIX_SUCCESS                                           */
/*****************************************************************************/

UINT4
OsixSemTake (tOsixSemId SemId)
{

    if (down_interruptible (SemId) != OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*  Function name   : OsixBHEnable                                           */
/*  Description     : Function to disable local bottom halves by             */
/*                    incrementing the preempt_count                         */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : OSIX_SUCCESS                                           */
/*****************************************************************************/

UINT4
OsixBHEnable ()
{
    local_bh_enable ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*  Function name   : OsixBHDisable                                          */
/*  Description     : Function to enable  local bottom halves by             */
/*                    decrementing the preempt_count                         */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : OSIX_SUCCESS                                           */
/*****************************************************************************/

UINT4
OsixBHDisable ()
{
    local_bh_disable ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*  Function name   : OsixSemGive                                            */
/*  Description     : Function to acquire the spin lock by enabling the      */
/*                    interrupts locally                                     */
/*  Input(s)        : SemId - Semaphore Id                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/

UINT4
OsixSemGive (tOsixSemId SemId)
{

    up (SemId);
    return (OSIX_SUCCESS);

}

/************************************************************************/
/*  Function Name   : OsixSemDel                                        */
/*  Description     : Deletes a sema4.                                  */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixSemDel (tOsixSemId SemId)
{
    OsixRscDel (OSIX_SEM, (VOID *) SemId);
    LkFree (SemId);
    return;
}

/************************************************************************/
/*  Function Name   : Fsap2Start                                        */
/*  Description     : This function stubs out the unused function calls */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Fsap2Start (VOID)
{
    return;
}

/************************************************************************/
/*  Function Name   : Fsap2Shutdown                                     */
/*  Description     : This function stubs out the unused function calls */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Fsap2Shutdown (VOID)
{
    tOsixTaskId         TskId;

    if (OsixRscFind ("ROOT", OSIX_TSK, (VOID *) &TskId) == OSIX_FAILURE)
    {
        return;
    }
    gaOsixTsk[TskId].u2CondWaitFlag = OSIX_KERN_ONE;
    wake_up_interruptible (&gaOsixTsk[TskId].TskEvent);
    return;
}

/************************************************************************/
/*  Function Name   : OsixInitialize                                    */
/*  Description     : The OSIX Initialization routine. To be called     */
/*                    before any other OSIX functions are used.         */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixInitialize (VOID)
{
    UINT4               u4Idx = 0;

    sema_init (&gOsixSemId, 1);

    /* Initialize all arrays. */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].pTskStrtAddr = NULL;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u4QueType = OSIX_FALSE;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
        gaLkModQ[u4Idx].i1Module = OSIX_FALSE;
    }
    gSysUpTimeInfo = CURRENT_TIME_SEC;
    gStartTicks = jiffies;

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixShutDown                                      */
/*  Description     : Stops OSIX.                                       */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixShutDown ()
{
    UINT4               u4Idx = 0;

    /* Re - Initialize all arrays. */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].pTskStrtAddr = NULL;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u4QueType = OSIX_FALSE;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
        gaLkModQ[u4Idx].i1Module = OSIX_FALSE;
    }

    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : OsixSetLocalTime                                  */
/*  Description     : Obtains the system time and sets it in FSAP.      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSetLocalTime (void)
{
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : Kernel_Create_MsgQ                                */
/*  Description     : Creates a queue using a linear block of memory.   */
/*  Input(s)        : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MaxMsgLen- Max length of a messages             */
/*  Output(s)       : None                                              */
/*  Returns         : Queue-Id, NULL if creation fails                  */
/************************************************************************/
tKernMsgQ          *
Kernel_Create_MsgQ (UINT4 u4MaxMsgs, UINT4 u4MsgLen)
{
    tKernMsgQ          *pKernMsgQ;

    /* Allocate memory for holding messages. Create a semaphore for      */
    /* protection between multiple simultaneous calls to write or read   */
    /* Initialize the read and write pointers to the Q start location    */
    /* Initia the pointer marking the end of the queue's memory location */
    u4MsgLen = sizeof (void *);
    pKernMsgQ =
        (tKernMsgQ *) LkMalloc (((u4MaxMsgs + 1) * u4MsgLen) +
                                sizeof (tKernMsgQ));
    if (pKernMsgQ == NULL)
    {
        return (NULL);
    }
    pKernMsgQ->pQBase = (UINT1 *) ((UINT1 *) pKernMsgQ + sizeof (tKernMsgQ));

    /*Semaphore for Locking while Queue updations */
    sema_init (&(pKernMsgQ->MsgQSemId), 1);
    /*Conditional Waiting */
    init_waitqueue_head (&(pKernMsgQ->MsgQEvent));

    pKernMsgQ->pQEnd = (pKernMsgQ->pQBase) + ((u4MaxMsgs + 1) * u4MsgLen);
    pKernMsgQ->pQRead = pKernMsgQ->pQBase;
    pKernMsgQ->pQWrite = pKernMsgQ->pQBase;
    pKernMsgQ->u4MsgLen = u4MsgLen;

    pKernMsgQ->u4OverFlows = 0;

    return (pKernMsgQ);
}

/************************************************************************/
/*  Function Name   : Kernel_Delete_MsgQ                                */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Kernel_Delete_MsgQ (tKernMsgQ * QId)
{
    tKernMsgQ          *pKernMsgQ = (tKernMsgQ *) QId;
    /* Wait for semaphore to ensure that when the queue is deleted */
    /* no one is reading from or writing into it. Then delete the  */
    /* semaphore, free the queue memory and initialize queue start */
    LkFree ((VOID *) pKernMsgQ);
}

/************************************************************************/
/*  Function Name   : Kernel_Send_MsgQ                                  */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*  Output(s)       : None                                              */
/*  Returns         : 0 on SUCCESS and (-1) on FAILURE                  */
/************************************************************************/
INT4
Kernel_Send_MsgQ (tOsixQId QId, UINT1 *pMsg)
{
    tKernMsgQ          *pKernMsgQ = NULL;
    UINT1              *pWrite, *pRead, *pBase, *pEnd;
    UINT4               u4MsgLen;
    UINT4               u4Flags;

    pKernMsgQ = gaLkModQ[QId].QueueInfo.pKernQId;
    /* Ensure mutual exclusion. Wait and take the mutual exclusion        */
    /* semaphore. A write is possible if the queue is not full. Queue is  */
    /* recognized as full if by writing one more message, write and read  */
    /* pointers become equal. Actually, this means that the queue holds   */
    /* only u4MaxMsgs-1 messages to be safe. When checking the pointers   */
    /* or when advancing the write pointer after the write operation,     */
    /* take care of the wrap-around since this is a circular queue. When  */
    /* the message is written, advance the write pointer by u4MsgLen.     */
    OsixSemTake (&(pKernMsgQ->MsgQSemId));

    pWrite = pKernMsgQ->pQWrite;
    pRead = pKernMsgQ->pQRead;
    pBase = pKernMsgQ->pQBase;
    pEnd = pKernMsgQ->pQEnd;
    u4MsgLen = OSIX_MAX_Q_MSG_LEN;

    if (((pWrite + u4MsgLen) == pEnd) && (pRead == pBase))
    {
        OsixSemGive (&(pKernMsgQ->MsgQSemId));
        pKernMsgQ->u4OverFlows++;
        return (-1);
    }
    if ((pWrite + u4MsgLen) == pRead)
    {
        OsixSemGive (&(pKernMsgQ->MsgQSemId));
        pKernMsgQ->u4OverFlows++;
        return (-1);
    }
    memcpy (pWrite, pMsg, u4MsgLen);
    (pKernMsgQ->pQWrite) += u4MsgLen;

    if ((pKernMsgQ->pQWrite) == pEnd)
    {
        (pKernMsgQ->pQWrite) = pBase;
    }

    /* unblock anyone waiting to read */
    pKernMsgQ->u2MsgQCondFlag = 1;

    wake_up_interruptible (&(pKernMsgQ->MsgQEvent));

    /* allow others to read/write/delete */
    OsixSemGive (&(pKernMsgQ->MsgQSemId));

    return (0);
}

/************************************************************************/
/*  Function Name   : Kernel_Receive_MsgQ                               */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : 0 on SUCCESS and (-1) on FAILURE                  */
/************************************************************************/
INT4
Kernel_Receive_MsgQ (tOsixQId QId, UINT1 *pMsg, INT4 i4Timeout)
{
    tKernMsgQ          *pKernMsgQ = NULL;
    struct timespec     ts;
    struct timeval      now;
    UINT4               u4Sec;
    UINT4               u4Flags;
    INT4                i4rc;
    UINT4               u4MicroSec;

    pKernMsgQ = gaLkModQ[QId].QueueInfo.pKernQId;

    if (i4Timeout == OSIX_WAIT)
    {
        if ((pKernMsgQ->pQWrite) == (pKernMsgQ->pQRead))
        {
            wait_event_interruptible ((pKernMsgQ->MsgQEvent),
                                      ((pKernMsgQ->pQWrite) !=
                                       (pKernMsgQ->pQRead)));
            if (signal_pending (current))
            {
                return OSIX_FAILURE;
            }
        }
    }
    else if (i4Timeout == OSIX_NO_WAIT)
    {
        if ((pKernMsgQ->pQWrite) == (pKernMsgQ->pQRead))
        {
            return OSIX_FAILURE;
        }
    }
    else if (i4Timeout > 0)
    {
        if ((pKernMsgQ->pQWrite) == (pKernMsgQ->pQRead))
        {
            i4Timeout = msecs_to_jiffies (i4Timeout);
            wait_event_interruptible_timeout ((pKernMsgQ->MsgQEvent),
                                              ((pKernMsgQ->pQWrite) !=
                                               (pKernMsgQ->pQRead)), i4Timeout);
            if (signal_pending (current))
            {
                return OSIX_FAILURE;
            }
        }
    }

    OsixSemTake (&(pKernMsgQ->MsgQSemId));
    /* There is at least 1 message in the queue and we have locked the */
    /* mutual exclusion semaphore so nobody else changes the state.    */
    MEMCPY (pMsg, pKernMsgQ->pQRead, pKernMsgQ->u4MsgLen);
    (pKernMsgQ->pQRead) += (pKernMsgQ->u4MsgLen);
    if ((pKernMsgQ->pQRead) == (pKernMsgQ->pQEnd))
    {
        (pKernMsgQ->pQRead) = (pKernMsgQ->pQBase);
    }
    OsixSemGive (&(pKernMsgQ->MsgQSemId));
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : Kernel_MsgQ_NumMsgs                               */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : None                                              */
/*  Returns         : pu4NumberOfMsgs - Contains count upon return.     */
/************************************************************************/
UINT4
Kernel_MsgQ_NumMsgs (tKernMsgQ * QId)
{
    tKernMsgQ           pKernMsgQ = *((tKernMsgQ *) QId);
    UINT4               u4Msgs;

    if ((pKernMsgQ.pQWrite) < (pKernMsgQ.pQRead))
    {
        u4Msgs =
            (pKernMsgQ.pQWrite) - (pKernMsgQ.pQBase) + (pKernMsgQ.pQEnd) -
            (pKernMsgQ.pQRead);
        return (u4Msgs / (pKernMsgQ.u4MsgLen));
    }
    else
    {
        return (((pKernMsgQ.pQWrite) -
                 (pKernMsgQ.pQRead)) / (pKernMsgQ.u4MsgLen));
    }
}

/************************************************************************/
/*  Function Name   : FsapShowTCB                                       */
/*  Description     : Used to Give the Task Info to the Calling Function*/
/*  Input(s)        : None                                              */
/*  Output(s)       : pu1Result - Output Buffer                         */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
FsapShowTCB (UINT1 *pu1Result)
{
    UNUSED_PARAM (pu1Result);
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : FsapTrace
 *  Description     : Implements the trace part of CLI interface.
 *  Input           : u4Flag =1 for 'trace' command, =0 for the 'no trace' cmd
 *                    u4Value - the value of the particular trace that is
 *                              being set/unset.
 *  Output          : pu4TrcLvl - Returns the current/new trace level.
 *  Returns         : None.
 ************************************************************************/
void
FsapTrace (UINT4 u4Flag, UINT4 u4Value, UINT4 *pu4TrcLvl)
{
    UNUSED_PARAM (u4Flag);
    UNUSED_PARAM (u4Value);
    UNUSED_PARAM (pu4TrcLvl);
    return;
}

/************************************************************************
 *  Function Name   : FsapShowSem
 *  Description     : Implements the show sem part of CLI interface.
 *  Input           : au1Name - SemName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowSem (UINT1 au1SemName[], UINT1 *pu1Result, UINT4 *pu4NextIdx)
{
    UNUSED_PARAM (au1SemName);
    UNUSED_PARAM (pu1Result);
    UNUSED_PARAM (pu4NextIdx);
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : FsapShowTask
 *  Description     : Implements the show task part of CLI interface.
 *  Input           : au1Name - TaskName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowTask (UINT1 au1TskName[], UINT1 *pu1Result)
{
    UNUSED_PARAM (au1TskName);
    UNUSED_PARAM (pu1Result);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixGetTps                                        */
/*  Description     : Returns the no of system ticks in a second        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : Number of system ticks in a second                */
/************************************************************************/
UINT4
OsixGetTps ()
{
    return HZ;
}

/************************************************************************/
/*  Function Name   : OsixQueNumMsgs                                    */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : pu4NumberOfMsgs - Contains count upon return.     */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixQueNumMsg (tOsixQId QueId, UINT4 *pu4NumMsg)
{
    UNUSED_PARAM (QueId);
    UNUSED_PARAM (pu4NumMsg);
    return 0;
}

/************************************************************************/
/*  Function Name   : OsixGetSysTime                                    */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysTime (tOsixSysTime * pSysTime)
{
    *pSysTime = (jiffies - gStartTicks);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetSysUpTime                                  */
/*  Description     : Returns the system time in Seconds.               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysUpTime (VOID)
{
    return (gSysUpTimeInfo.tv_sec);
}

/************************************************************************/
/*  Function Name   : OsixExGetTaskName                                 */
/*  Description     : Get the OSIX task Name given the Osix Task-Id.    */
/*  Input(s)        : TaskId - The Osix taskId.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to OSIX task name                         */
/************************************************************************/
const UINT1        *
OsixExGetTaskName (tOsixTaskId TskId)
{
    /* To facilitate direct use of this call in a STRCPY,
     * we return a null string instead of a NULL pointer.
     * A null pointer is returned for all non-OSIX tasks
     * e.g., TMO's idle task.
     */
    if (TskId)
    {
        return ((UINT1 *) (gaOsixTsk[(UINT4) TskId].au1Name));
    }
    return ((const UINT1 *) "");
}

/************************************************************************
 *  Function Name   : OsixGetMemInfo
 *  Description     : This function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4TotalMem - Total memory available
 *                    pu4FreeMem - Free memory available
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetMemInfo (UINT4 *pu4TotalMem, UINT4 *pu4FreeMem)
{
    UNUSED_PARAM (pu4TotalMem);
    UNUSED_PARAM (pu4FreeMem);
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetCPUInfo
 *  Description     : This function used to get the current CPU utilization.
 *                    This information will be collected from the file '/proc/stat'
 *  Inputs          : None
 *  OutPuts         : pu4TotalUsage - Total Usage
 *                  : pu4CPUUsage - Current CPU Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetCPUInfo (UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage)
{
    UNUSED_PARAM (pu4TotalUsage);
    UNUSED_PARAM (pu4CPUUsage);
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetFlashInfo
 *  Description     : This function used to get the total memory and free
 *                    memory in flash from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4FlashUsage - Free flash memory available
 *                  : pu4TotalUsage - Total flash memory available
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetFlashInfo (UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash)
{
    UNUSED_PARAM (pu4TotalUsage);
    UNUSED_PARAM (pu4FreeFlash);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixSysRestart                                    */
/*  Description     : This function reboots the system.                 */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSysRestart ()
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}

/************************************************************************/
/*  Function Name   : OsixSemTimedTake                                  */
/*  Description     : Used to acquire a sema4 on a timed basis.         */
/*  Input(s)        : pSemId - The sema4 Id.                            */
/*                    u2Seconds - Time to wait.                         */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemTimedTake (tOsixSemId SemId , UINT2 u2Seconds)
{

    UNUSED_PARAM (SemId);
    UNUSED_PARAM (u2Seconds);
    return (OsixSemTake (SemId));
}

#endif
