/*
 * Copyright Aricent Inc, 2011.
 *
 * $Id: utltrci.h,v 1.1.1.1 2015/04/28 12:11:05 siva Exp $
 *
 * Description:
 * UTL trace module's internal file.
 *
 */

#ifndef _UTLTRCI_H
#define _UTLTRCI_H

#ifdef VAR_ARGS_SUPPORT
#include <stdarg.h>
#endif /* VAR_ARGS_SUPPORT */

#define MAX_LOG_STR_LEN                256

#define MAX_CHARS_PER_LINE              60

#define MAX_CHARS_FOR_BYTE_DUMP          6

#endif
