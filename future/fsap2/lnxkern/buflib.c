 /*
  * Copyright (C) 2011 Aricent Inc . All Rights Reserved  
  *
  * $Id: buflib.c,v 1.3 2018/02/20 10:52:54 siva Exp $
  *
  * Description: This file contains buffer processing APIs  for kernel. 
  *              This operates on skb buffer of linux.
  * */

#ifndef _BUFLIB_C_
#define _BUFLIB_C_

#include <linux/autoconf.h>
#include "kerninc.h"
#include "osxinc.h"
#include "bufcom.h"
#include <linux/module.h>
#include <linux/version.h>
#include <linux/ip.h>
#include <linux/netdevice.h>
#include <asm/atomic.h>

tCRU_BUF_FREE_QUE_DESC *pCRU_BUF_Chain_FreeQueDesc;
static tOsixSemId   gBufSemId;
UINT4               gu4MemoryType;
UINT4               gu4Allocs = 0;
UINT4               gu4Frees = 0;

#define CRU_MIN_HDR   128        /* Space to do Prepend        */
#define CRU_MTU_SZ   4000        /* All packets are 1500 bytes */
#define   UNUSED_PARAM(x)   ((VOID)x)
UINT4
 
 
 
        CRU_BUF_Get_ChainStartFreeByteCount (tCRU_BUF_CHAIN_DESC * pChainDesc);

VOID                CRU_BUF_Print_AllStats
ARG_LIST ((VOID))
{
    /* Not Implemented. */
    return;
}

INT4                CRU_BUF_Link_BufChains
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBufChain1,
           tCRU_BUF_CHAIN_HEADER * pBufChain2))
{
    pBufChain1 = pBufChain1;
    pBufChain2 = pBufChain2;
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : BufInitManager                                         */
/*  Description     : This is the initialization procedure that must be      */
/*                    called during system startup .                         */
/*  Input(s)        : pBufLibInitData  - Pointer to the buffer initialise    */
/*                            configuration data                             */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/

INT4
BufInitManager (tBufConfig * pBufCfg)
{
    UINT1               au1SemName[5];

    MEMSET (au1SemName, 0, sizeof (au1SemName));

    STRCPY ((INT1 *) au1SemName, "BUFS");

    /* Check whether buffer library is already initialized
     * using global semaphore  */
    if (OsixGetSemId (0, au1SemName, &gBufSemId) == OSIX_SUCCESS)
    {
        return CRU_FAILURE;
    }

    if (OsixCreateSem (au1SemName, 1, OSIX_LOCAL, &gBufSemId) != OSIX_SUCCESS)
    {
        return CRU_FAILURE;
    }

    gu4MemoryType = pBufCfg->u4MemoryType;

    /* Create Chain Descriptor Pool */
    if (CRU_BUF_Create_ChainDescPool ((UINT4) pBufCfg->u4MaxChainDesc)
        != CRU_SUCCESS)
    {
        CRU_BUF_Shutdown_Manager ();
        return CRU_FAILURE;
    }
    gu4Allocs = gu4Frees = 0;
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Shutdown_Manager                               */
/*  Description     : Procedure to  release memory pools allocated for the   */
/*                      chain descriptors, data descriptors and data blocks. */
/*  Input(s)        : none                                                   */
/*  Output(s)       : none                                                   */
/*  Return(s)       : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/

INT4
CRU_BUF_Shutdown_Manager ()
{
    if (gBufSemId == 0)
    {
        return (CRU_FAILURE);
    }

    /* Delete semaphore created for Mutual Exclusion  */
    OsixSemDel (gBufSemId);

    gBufSemId = 0;

    if (pCRU_BUF_Chain_FreeQueDesc == NULL)
    {
        return CRU_SUCCESS;
    }

    /*   Delete Chain Descriptor Pool  */
    if (CRU_BUF_Delete_ChainDescPool () != CRU_SUCCESS)
    {
        return CRU_FAILURE;
    }

    gu4Allocs = gu4Frees = 0;
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Allocate_MsgBufChain                           */
/*  Description     : This procedure allocates a Message buffer of 'u4Size'  */
/*                    from free pool.                                        */
/*  Input(s)        : u4Size      - Size of the buffer to be allocated       */
/*                        u4Offset   - In the allocated buffer where         */
/*                                     should the valid data be starting.    */
/*                                     This is used basically to reserve     */
/*                                     for headers of lower                  */
/*                                          layer protocols.                 */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the newly allocated buffer on success,      */
/*                        NULL on failure                                    */
/*****************************************************************************/

tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Allocate_MsgBufChain (UINT4 u4Size, UINT4 u4Offset)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    if ((pBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
        return NULL;

    /*
     * To make concatenate operation to be efficient, we
     * allocate MTU_SIZE bytes extra, so that at least one
     * concatenate operation for every packet will not need
     * a reallocation of the data size. See the CRU_BUF_Concat
     * logic for more clarity.
     */

    u4Size += (CRU_MTU_SZ + CRU_MIN_HDR);

    pBuf->pSkb = alloc_skb (u4Size, GFP_ATOMIC);

    if (pBuf->pSkb)
    {
        pBuf->pSkb->data = (pBuf->pSkb->head + CRU_MIN_HDR + u4Offset);
        pBuf->pSkb->tail = pBuf->pSkb->data;
        gu4Allocs += 1;
    }
    else
    {
        printk ("\n\n-E CRU: SKB Alloc failed\n\n");
        CRU_BUF_Release_ChainDesc (pBuf);
        return NULL;
    }

    return (pBuf);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_OverBufChain_AtEnd                        */
/*  Description     : Copies 'u4Size' bytes from  pu1Src to the u4Offset of  */
/*                    message Chain.                                         */
/*                    Unlike CRU_BUF_Copy_OverBufChain, this function        */
/*                    only considers the valid bytes. Thus it is useful      */
/*                    to "append" bytes to a chained buffer.                 */
/*  Input(s)        : pChainDesc   - Pointer to the buffer in which the copy */
/*                         needs to be done.                                 */
/*                    pu1Src  -  Pointer to the linear array from which copy */
/*                               to the buffer needs to be done.             */
/*                    u4Offset  - Offset(Relative to Valid offset)  in the   */
/*                               buffer starting from which copy needs to be */
/*                               done                                        */
/*                    u4Size   - Number of bytes to copy                     */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
EXPORT INT4
CRU_BUF_Copy_OverBufChain_AtEnd (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT1 *pu1Src, UINT4 u4Offset, UINT4 u4Size)
{
    unsigned char      *pNewTail;

    pNewTail = pBuf->pSkb->data + u4Offset + u4Size;

    MEMCPY ((pBuf->pSkb->data) + u4Offset, pu1Src, u4Size);

    if (pNewTail > pBuf->pSkb->tail)
    {
        pBuf->pSkb->len += (pNewTail - pBuf->pSkb->tail);
        pBuf->pSkb->tail = pNewTail;
    }

    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_OverBufChain                              */
/*  Description     : Copies 'u4Size' bytes from  pu1Src to the u4Offset of  */
/*                    message Chain                                          */
/*  Input(s)        : pChainDesc   - Pointer to the buffer in which the copy */
/*                         needs to be done.                                 */
/*                    pu1Src  -  Pointer to the linear array from which copy */
/*                               to the buffer needs to be done.             */
/*                    u4Offset  - Offset(Relative to Valid offset)  in the   */
/*                               buffer starting from which copy needs to be */
/*                               done                                        */
/*                    u4Size   - Number of bytes to copy                     */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/

INT4
CRU_BUF_Copy_OverBufChain (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Src,
                           UINT4 u4Offset, UINT4 u4Size)
{
    unsigned char      *pNewTail;

    pNewTail = pBuf->pSkb->data + u4Offset + u4Size;

    MEMCPY ((pBuf->pSkb->data) + u4Offset, pu1Src, u4Size);

    if (pNewTail > pBuf->pSkb->tail)
    {
        pBuf->pSkb->len += (pNewTail - pBuf->pSkb->tail);
        pBuf->pSkb->tail = pNewTail;
    }

    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_FromBufChain                              */
/*  Description     : Copies 'u4Size' bytes from Chain at the u4Offset to Dst*/
/*  Input(s)        : pChainDesc  - Buffer from which copy needs to be done  */
/*                    pu1Dst  - Array to which the specified buffer content  */
/*                              needs to be copied                           */
/*                    u4Offset  - Relative offset in the buffer from which   */
/*                         data needs to be read and copied to the dest      */
/*                    u4Size  - Requested number of bytes to copy            */
/*  Output(s)       : None                                                   */
/*  Returns         : Returns the number of  bytes copied from Chain         */
/*****************************************************************************/
INT4
CRU_BUF_Copy_FromBufChain (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Dst,
                           UINT4 u4Offset, UINT4 u4Size)
{
    UINT4               u4Data = pBuf->pSkb->len - u4Offset;

    u4Size = (u4Size <= u4Data) ? u4Size : u4Data;
    MEMCPY (pu1Dst, pBuf->pSkb->data + u4Offset, u4Size);
    return u4Size;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Concat_MsgBufChains                            */
/*  Description     : Procedure to Concatenate  two CRU buffer chains        */
/*  Input(s)        : pBuf1 - First Buffer  Chain to which the second  */
/*                                   specified buffer chain is concatenated              */
/*                        pBuf2 - Second buffer chain                      */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
/*
 *
 * Picture below depicts the case where pBuf2 can't fit into
 * the tailroom of pBuf1, thus forcing a fresh alloc.
 * We kmalloc a data portion of u4Size bytes.
 * and 'plug it' into pBuf1, taking care to free
 * the original pBuf1's data portion.
 * pBuf2 is free-ed as a normal 'cru' buffer.
 *
 *  <-------------- u4Size --------------->
 *
 *  +-----------------------+
 *  |     |         |       |
 *  +-----------------------+
 *  h     d         t       e
 *
 *          +-----------------------------+
 *          |       |           |         |
 *          +-----------------------------+
 *          h       d           t         e
 *
 */
INT4
CRU_BUF_Concat_MsgBufChains (tCRU_BUF_CHAIN_HEADER * pBuf1,
                             tCRU_BUF_CHAIN_HEADER * pBuf2)
{
    UINT4               u4Size;

#if defined (LINUX_KERNEL_2_4_20VER) || defined (LINUX_KERNEL_2_6_18VER) || defined (LINUX_KERNEL_2_6_21VER)
    UINT4               u4Buf1Len = ((pBuf1->pSkb->end) - pBuf1->pSkb->head);

    UINT4               u4Buf1DataLen =
        ((pBuf1->pSkb->tail) - pBuf1->pSkb->data);

    UINT4               u4Buf2DataLen =
        ((pBuf2->pSkb->tail) - pBuf2->pSkb->data);

#else

    UINT4               u4Buf1Len =
        (skb_end_pointer (pBuf1->pSkb) - pBuf1->pSkb->head);
    UINT4               u4Buf1DataLen =
        (skb_tail_pointer (pBuf1->pSkb) - pBuf1->pSkb->data);
    UINT4               u4Buf2DataLen =
        ((pBuf2->pSkb->tail) - pBuf2->pSkb->data);

#endif

    UINT4               Buf1HeadRoom;

    /* Total size is:
     * headroom of pBuf1   +   len of each buffer + tail room of pBuf2.
     */
    Buf1HeadRoom = (pBuf1->pSkb->data - pBuf1->pSkb->head);
    u4Size = Buf1HeadRoom + u4Buf1DataLen + u4Buf2DataLen;

    if (u4Buf1Len < u4Size)
    {
        if (pskb_expand_head (pBuf1->pSkb, 0, u4Buf2DataLen, GFP_ATOMIC) != 0)
        {
            return (CRU_FAILURE);
        }
    }

    MEMCPY (pBuf1->pSkb->tail, pBuf2->pSkb->data, u4Buf2DataLen);
    pBuf1->pSkb->len += pBuf2->pSkb->len;
    pBuf1->pSkb->tail += u4Buf2DataLen;
    CRU_BUF_Release_MsgBufChain (pBuf2, 0);
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Fragment_BufChain                              */
/*  Description     : Procedure to Fragment the Chain  at size 'u4Size'      */
/*  Input(s)        : pBuf  - Pointer to the buffer to be fragmented   */
/*                    u4Size  -  Offset at which fragmentation needs to be   */
/*                               done                                        */
/*  Output(s)       : ppFragBuf - Pointer to the fragment which is */
/*                          the result of the requested fragmentation        */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
/*
 *      MINHDR      <--------------- LEN -------------->
 *      +---------------------------------------------------------+
 *      |/////|     |               |                  |          |
 *      +---------------------------------------------------------+
 *     head        data             ^                  tail       end
 *      <-u4offset-><--- u4Size ----><------- u4FragSize --------->
 *
 *
 *      <------ FRAGMENT 1 --------><-------- FRAGMENT 2 -------->
 */

INT4
CRU_BUF_Fragment_BufChain (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size,
                           tCRU_BUF_CHAIN_HEADER ** ppFragBuf)
{
    tCRU_BUF_CHAIN_HEADER *pNewBuf;
    UINT4               u4Offset = (pBuf->pSkb->data - pBuf->pSkb->head);

#if defined (LINUX_KERNEL_2_4_20VER) || defined (LINUX_KERNEL_2_6_18VER) || defined (LINUX_KERNEL_2_6_21VER)
    UINT4               u4FragSize =
        ((pBuf->pSkb->end) - pBuf->pSkb->data - u4Size + u4Offset);
    UINT4               u4DataLen = ((pBuf->pSkb->tail) - pBuf->pSkb->data);
#else
    UINT4               u4FragSize =
        (skb_end_pointer (pBuf->pSkb) - pBuf->pSkb->data - u4Size + u4Offset);
    UINT4               u4DataLen =
        (skb_tail_pointer (pBuf->pSkb) - pBuf->pSkb->data);
#endif

    /* Alloc FRAGMENT 2 */
    pNewBuf = CRU_BUF_Allocate_MsgBufChain (u4FragSize, u4Offset);

    if (pNewBuf)
    {
        /* Construct it. */
        MEMCPY (pNewBuf->pSkb->data, ((pBuf->pSkb->data) + u4Size),
                (u4DataLen - u4Size));

        pNewBuf->pSkb->len = pBuf->pSkb->len - u4Size;
        pNewBuf->pSkb->tail = pNewBuf->pSkb->data + pNewBuf->pSkb->len;
        pBuf->pSkb->len = u4Size;
        pBuf->pSkb->tail = (pBuf->pSkb->data) + u4Size;
        *ppFragBuf = pNewBuf;

        //print_ioctl_param (pNewBuf->data, u4DataLen - u4Size);
        return (CRU_SUCCESS);
    }
    else
        printk ("Allocation failed for fragmentation\n");

    return (CRU_FAILURE);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Prepend_BufChain                               */
/*  Description     : Procedure to prepend  u4Size bytes from pu1Src .       */
/*  Input(s)        : pBuf  - Pointer to the buffer in which prepend         */
/*                                    needs to be done                       */
/*                    pu1Src   - Pointer to the array whose content needs to */
/*                                    be copied to the buffer                */
/*                    u4Size  - Number of bytes to prepend                   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Prepend_BufChain (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Src,
                          UINT4 u4Size)
{
    /* Check if we have enough headroom to prepend. */
    if (skb_headroom (pBuf->pSkb) < u4Size)
    {
        if (skb_cow (pBuf->pSkb, u4Size) != 0)
        {
            return CRU_FAILURE;
        }
    }

    skb_push (pBuf->pSkb, u4Size);

    if (pu1Src)
    {
        MEMCPY (pBuf->pSkb->data, pu1Src, u4Size);
    }
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Duplicate_BufChain                             */
/*  Description     : Procedure to duplicate the buffer (Chain )             */
/*  Input(s)        : pBuf  - Pointer to the buffer to be duplicated         */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the buffer which has been duplicated        */
/*****************************************************************************/

tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Duplicate_BufChain (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tCRU_BUF_CHAIN_HEADER *pNewBuf = NULL;

#if defined (LINUX_KERNEL_2_4_20VER) || defined (LINUX_KERNEL_2_6_18VER) || defined (LINUX_KERNEL_2_6_21VER)

    UINT4               u4BufSize = ((pBuf->pSkb->end) - pBuf->pSkb->head);

#else

    UINT4               u4BufSize =
        (skb_end_pointer (pBuf->pSkb) - pBuf->pSkb->head);

#endif

    if ((pNewBuf = CRU_BUF_Allocate_ChainDesc ()) == NULL)
        return NULL;

    pNewBuf->pSkb = alloc_skb (u4BufSize, GFP_ATOMIC);

    if (pNewBuf->pSkb)
    {
        pNewBuf->pSkb->data =
            (pNewBuf->pSkb->head) + ((pBuf->pSkb->data) - (pBuf->pSkb->head));
        pNewBuf->pSkb->tail = (pNewBuf->pSkb->data + pBuf->pSkb->len);
        pNewBuf->pSkb->len = pBuf->pSkb->len;
        MEMCPY (pNewBuf->pSkb->head, pBuf->pSkb->head, u4BufSize);
        gu4Allocs += 1;
    }
    else
    {
        printk ("\n\n-E CRU: Alloc failed\n\n");
        CRU_BUF_Release_ChainDesc (pNewBuf);
        return NULL;
    }
    return (pNewBuf);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Allocate_ChainDesc                             */
/*  Description     : This Procedure allocates & returns a chain Descriptor  */
/*                    after Properly Initializing it.                        */
/*  Input(s)        : none                                                   */
/*  Output(s)       : none                                                   */
/*  Returns         : Pointer to the allocated Chain Descriptor              */
/*****************************************************************************/
tCRU_BUF_CHAIN_DESC *
CRU_BUF_Allocate_ChainDesc (VOID)
{
    UINT1               u1SemReq = 0;
    tCRU_BUF_CHAIN_DESC *pChnDesc = NULL;

    if (IS_OSIX_SEM_REQ (u1SemReq))
    {
        if (OsixSemTake (gBufSemId) != OSIX_SUCCESS)
        {
            return NULL;
        }
    }
    if ((pChnDesc = (tCRU_BUF_CHAIN_DESC *) (VOID *) MemAllocMemBlk
         (pCRU_BUF_Chain_FreeQueDesc->u2_QueId)) == NULL)
    {
        if (IS_OSIX_SEM_REQ (u1SemReq))
        {
            OsixSemGive (gBufSemId);
        }
        return NULL;
    }

    pChnDesc->u1ValidMemoryBlock = CRU_BUF_VALID_MEMORY_BLOCK;
    pChnDesc->pSkb = NULL;
    pChnDesc->NextBufChainHeader.pNext = NULL;
    CRU_BUF_MEMSET (&(pChnDesc->ModuleData), 0, sizeof (tMODULE_DATA));

    if (IS_OSIX_SEM_REQ (u1SemReq))
    {
        OsixSemGive (gBufSemId);
    }

    return pChnDesc;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Release_ChainDesc                              */
/*  Description     : This Procedure releases a chain Descriptor             */
/*  Input(s)        : pCruBuf - Buffer to be released                        */
/*  Output(s)       : none                                                   */
/*  Returns         : Returns CRU_SUCCESS                                    */
/*****************************************************************************/

INT4
CRU_BUF_Release_ChainDesc (tCRU_BUF_CHAIN_DESC * pCruBuf)
{
    UINT1               u1SemReq = 0;
    if (pCruBuf == NULL)
    {
        return CRU_FAILURE;
    }
    if (IS_OSIX_SEM_REQ (u1SemReq))
    {
        if (OsixSemTake (gBufSemId) != OSIX_SUCCESS)
        {
            return CRU_FAILURE;
        }
    }
    if (pCruBuf->u1ValidMemoryBlock != CRU_BUF_VALID_MEMORY_BLOCK)
    {
        if (IS_OSIX_SEM_REQ (u1SemReq))
        {
            OsixSemGive (gBufSemId);
        }
        return CRU_FAILURE;
    }
    /*  Release chain after data descriptors */
    if (MemReleaseMemBlock (pCRU_BUF_Chain_FreeQueDesc->u2_QueId,
                            (UINT1 *) pCruBuf) == MEM_FAILURE)
    {
        if (IS_OSIX_SEM_REQ (u1SemReq))
        {
            OsixSemGive (gBufSemId);
        }
        return CRU_FAILURE;
    }
    pCruBuf->u1ValidMemoryBlock = CRU_BUF_INVALID_MEMORY_BLOCK;
    if (IS_OSIX_SEM_REQ (u1SemReq))
    {
        OsixSemGive (gBufSemId);
    }

    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_ChainValidByteCount                        */
/*  Description     : Procedure to get number of valid bytes of Chain        */
/*  Input(s)        : pBuf   - Pointer to the buffer whose valid byte        */
/*                          count is requested                               */
/*  Output(s)       : None                                                   */
/*  Returns         : Number of valid data bytes of chain                    */
/*****************************************************************************/
UINT4
CRU_BUF_Get_ChainValidByteCount (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    return (pBuf->pSkb->len);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Delete_BufChainAtEnd                           */
/*  Description     : Procedure to Delete 'u4Size' bytes at the end of chain */
/*  Input(s)        : pBuf  - Pointer to the buffer which needs to be        */
/*                           trimmed                                         */
/*                    u4Size    - Number of bytes to trim                    */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the trimmed buffer                          */
/*****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Delete_BufChainAtEnd (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Size)
{
    if (CRU_BUF_Get_ChainValidByteCount (pBuf) <= u4Size)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return NULL;
    }
    else
    {
        pBuf->pSkb->tail -= u4Size;
        pBuf->pSkb->len -= u4Size;

        return (pBuf);
    }
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Move_ValidOffset                               */
/*  Description     : Procedure to move the valid_offset to u4UnitCount.     */
/*                    The u4_ValidByteCount of Chain is updated .            */
/*  Input(s)        : pChainDesc   - Pointer to the buffer                   */
/*                    u4UnitCount  - Number of units to move the valid offset*/
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Move_ValidOffset (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Units)
{

#if defined (LINUX_KERNEL_2_4_20VER) || defined (LINUX_KERNEL_2_6_18VER) || defined (LINUX_KERNEL_2_6_21VER)
    UINT4               u4BufSize = (pBuf->pSkb->end) - pBuf->pSkb->head;
#else
    UINT4               u4BufSize =
        (skb_end_pointer (pBuf->pSkb) - pBuf->pSkb->head);
#endif

    /* Check if we have enough room to move in tailward direction */
    if (u4Units <= (u4BufSize - (pBuf->pSkb->data - pBuf->pSkb->head)))
    {
        pBuf->pSkb->data += u4Units;

        /* Subtract carefully - as we are dealing with unsigned ints */
        if ((pBuf->pSkb->len) <= u4Units)
        {
            pBuf->pSkb->len = 0;
        }
        else
        {
            pBuf->pSkb->len -= u4Units;
        }
        return (CRU_SUCCESS);
    }
    return (CRU_FAILURE);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_DataPtr_IfLinear                           */
/*  Description     : Procedure to check if u4Units from u4Offset are        */
/*                    linear in the chain buffer. A pointer to the           */
/*                    linear buffer will be returned if successful, NULL     */
/*                    otherwise.                                             */
/*  Input(s)        : pBuf  - Pointer to the buffer                          */
/*                    u4Offset    - Offset (Relative) from which the buffer  */
/*                                   is checked  to be linear                */
/*                    u4Units     - Number of units expected to be linear    */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to linear buffer start returned or NULL        */
/*****************************************************************************/
UINT1              *
CRU_BUF_Get_DataPtr_IfLinear (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Offset,
                              UINT4 u4Size)
{
    if ((pBuf == NULL) || (pBuf->pSkb == NULL) || (pBuf->pSkb->len == 0))
    {
        return NULL;
    }
    if ((u4Offset + u4Size) <= (pBuf->pSkb->len))
    {
        return ((pBuf->pSkb->data) + u4Offset);
    }
    return (NULL);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_UnLink_BufChains                               */
/*  Description     : Procedure to unlink  two mesg_chains in linked list .  */
/*  Input(s)        : pBufChain  - Pointer to the message chaine in which   */
/*                         the head message will be unlinked                 */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the message which has been unlinked         */
/*****************************************************************************/

tCRU_BUF_CHAIN_HEADER *
CRU_BUF_UnLink_BufChain (tCRU_BUF_CHAIN_HEADER * pBufChain)
{
    pBufChain = pBufChain;
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Release_MsgBufChain                            */
/*  Description     : The procedure releases the mesg desc to mesg desc pool,*/
/*                    decrements the reference count of Data desc and        */
/*                    Data Blk. If the reference count of DataDesc/DataBlk   */
/*                    becomes '0' it releases them to respective buffer pools*/
/*                    When 'u1ForcedRelease' flag is set the referece count  */
/*                    will be ignored and the same will released immediately */
/*  Input(s)        : pBuf  - Pointer to the buffer which needs to be        */
/*                                  released                                 */
/*                    u1Force - Flag which states whether to release         */
/*                        the buffer without checking the reference count or */
/*                        not                                                */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Release_MsgBufChain (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Force)
{
    UNUSED_PARAM (u1Force);
    if (pBuf == NULL)
    {
        return CRU_FAILURE;
    }
    if (pBuf->pSkb != NULL)
    {
        kfree_skb ((struct sk_buff *) pBuf->pSkb);
        pBuf->pSkb = NULL;
    }
    if (CRU_BUF_Release_ChainDesc (pBuf) != CRU_SUCCESS)
    {
        return CRU_FAILURE;
    }
    gu4Frees += 1;
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_MoveToReadOffset                               */
/*  Description     : Procedure to move to offfset for a read operation on   */
/*                    the chain. This moves to the specifed offset and if    */
/*                    the  movement is valid  returns the  Data desc ptr and */
/*                    offset pointer as return values or returns CRU_FAILURE */
/*                    as return value .                                      */
/*  Input(s)        : pChainDesc - Pointer to the buffer from which read     */
/*                                 operation is to be done in the specified  */
/*                                 offset                                    */
/*                    u4Offset   - Relative offset from the valid offset from*/
/*                                 which read needs to be done               */
/*  Output(s)       : ppDataDescPtr - Pointer to the data descriptor which   */
/*                                    has be data block from which data needs*/
/*                                    to be read.                            */
/*                    ppu1DataPtr   - Pointer to the start of data in the    */
/*                                    data block from which to read          */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
PUBLIC INT4
CRU_BUF_MoveToReadOffset (tCRU_BUF_CHAIN_DESC * pChainDesc, UINT4 u4Offset,
                          tCRU_BUF_DATA_DESC ** ppDataDescPtr,
                          UINT1 **ppu1DataPtr)
{
    UNUSED_PARAM (ppDataDescPtr);
    UNUSED_PARAM (ppu1DataPtr);
    if (pChainDesc == NULL)
    {
        return CRU_FAILURE;
    }

    if (pChainDesc->pSkb == NULL)
    {
        return CRU_FAILURE;
    }

    if (skb_pull (pChainDesc->pSkb, u4Offset) == NULL)
    {
        return CRU_FAILURE;
    }
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_MoveToWriteOffset                              */
/*  Description     : Procedure to move to offfset for a write operation on  */
/*                    the chain. A check is made to ensure the chain has     */
/*                    enough space to move to the specified offset.If no     */
/*                    extra space is allocated dynamically  then the pointer */
/*                    to same will be returned. otherwise , CRU_FAILURE will */
/*                    be returned .                                          */
/*  Input(s)        : pChainDesc  - Pointer to the Message buffer in which   */
/*                                  write operation is going to be performed */
/*                    u4Offset    - Relative offset from the Valid Offset    */
/*                                  from which write operation is to be done */
/*                    u4Size      - Number of bytes to be copied             */
/*  Output(s)       : ppDataDescPtr  - Pointer to the Data Descriptor which  */
/*                                      has the data block in which copy has */
/*                                      to be done.                          */
/*                    ppu1DataPtr    - Pointer to the data in the data block */
/*                                     starting from which write operation   */
/*                                     needs to be done.                     */
/*  Returns         : CRU_SUCCESS/ CRU_FAILURE                               */
/*****************************************************************************/
INT4
CRU_BUF_MoveToWriteOffset (tCRU_BUF_CHAIN_DESC * pChainDesc,
                           UINT4 u4Offset,
                           tCRU_BUF_DATA_DESC ** ppDataDescPtr,
                           UINT1 **ppu1DataPtr, UINT4 u4Size)
{

    UNUSED_PARAM (ppDataDescPtr);
    UNUSED_PARAM (ppu1DataPtr);
    UNUSED_PARAM (u4Offset);

    if (pChainDesc == NULL)
    {
        return CRU_FAILURE;
    }

    if (pChainDesc->pSkb == NULL)
    {
        return CRU_FAILURE;
    }

    if (skb_push (pChainDesc->pSkb, u4Size) == NULL)
    {
        return CRU_FAILURE;
    }
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_KernMemFreeSkb                                 */
/*  Description     : This function is used to free the SKB                  */
/*  Input(s)        : pSkb  - Pointer to the iskb to be freed                */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
VOID
CRU_BUF_KernMemFreeSkb (struct sk_buff *pSkb)
{
    kfree_skb (pSkb);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Release_DataDesc                               */
/*  Description     : The procedure to release the Datadesc if u2_UsageCount */
/*                    reaches 0 .                                            */
/*  Input(s)        : pFirstDataDesc  - Pointer to the start of the Data     */
/*                            Descriptor (or first data descriptor)          */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Release_DataDesc (tCRU_BUF_DATA_DESC * pFirstDataDesc)
{
    UNUSED_PARAM (pFirstDataDesc);
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_ChainStartFreeByteCount                    */
/*  Description     : Procedure to get Chain start free byte count before    */
/*                    pFirstValidDataDesc of chain                           */
/*  Input(s)        : pChainDesc - Pointer the Message chain whose number of */
/*                                 prepending free bytes is requested        */
/*  Output(s)       : None                                                   */
/*  Returns         : Number of Starting free bytes in the buffer            */
/*****************************************************************************/
UINT4
CRU_BUF_Get_ChainStartFreeByteCount (tCRU_BUF_CHAIN_DESC * pChainDesc)
{
    UNUSED_PARAM (pChainDesc);
    return 0;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Allocate_DataDesc                              */
/*  Description     : This Procedure returns a Data_Buf_Desc linked with a   */
/*                    Data Buffer Of 'u4Size'. If 'u4Size' is 0, No Data     */
/*                    Buffer will be allocated, Only Data Descriptor is      */
/*                    returned.                                              */
/*  Input(s)        : u4Size   - Size of the Data Buffer to be allocated     */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the Data Descriptor with the requested size */
/*                    of buffer allocated on SUCCESS,  NULL on failure       */
/*****************************************************************************/
tCRU_BUF_DATA_DESC *
CRU_BUF_Allocate_DataDesc (UINT4 u4Size)
{
    UNUSED_PARAM (u4Size);
    return NULL;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Initialize_DataBlocks                          */
/*  Description     : Procedure to initialize the buffer pool records.       */
/*  Input(s)        : Buffer Configuration Parameter receivied from          */
/*                    application                                            */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Initialize_DataBlocks (tBufConfig * pBufCfg)
{
    UNUSED_PARAM (pBufCfg);
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Sort_DataBlocks                                */
/*  Description     : To sort the Data buffer Blocks to be allocated         */
/*                    according to  No Of Bytes(Size * No_Of_Units) in       */
/*                    descending order using Bubble Sort algorithm           */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
VOID
CRU_BUF_Sort_DataBlocks (void)
{
    return;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Create_ChainDescPool                           */
/*  Description     : Procedure to create Message (Chain) Desc pool. This is */
/*                    called by  the Buffer library initialization  module.  */
/*  Input(s)        : u4MaxChainDesc - Max no. configured Chain descriptors  */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS    / CRU_FAILURE                           */
/*****************************************************************************/
INT4
CRU_BUF_Create_ChainDescPool (UINT4 u4MaxChainDesc)
{
    UINT4               u4PoolId = 0;

    pCRU_BUF_Chain_FreeQueDesc =
        CRU_BUF_MALLOC (sizeof (tCRU_BUF_FREE_QUE_DESC),
                        tCRU_BUF_FREE_QUE_DESC);
    if (pCRU_BUF_Chain_FreeQueDesc == NULL)
    {
        return CRU_FAILURE;
    }

    CRU_BUF_MEMSET (pCRU_BUF_Chain_FreeQueDesc, 0,
                    sizeof (tCRU_BUF_FREE_QUE_DESC));

    pCRU_BUF_Chain_FreeQueDesc->u4_Size = sizeof (tCRU_BUF_CHAIN_DESC);
    pCRU_BUF_Chain_FreeQueDesc->u4_UnitsCount = u4MaxChainDesc;

    if (MemCreateMemPool
        (pCRU_BUF_Chain_FreeQueDesc->u4_Size,
         pCRU_BUF_Chain_FreeQueDesc->u4_UnitsCount, gu4MemoryType,
         &u4PoolId) == MEM_FAILURE)
    {
        return CRU_FAILURE;
    }

    pCRU_BUF_Chain_FreeQueDesc->u2_QueId = (UINT2) u4PoolId;
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Delete_ChainDescPool                           */
/*  Description     : Procedure to delete Message (Chain) Descriptor pool    */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Delete_ChainDescPool (void)
{
    if (pCRU_BUF_Chain_FreeQueDesc->u2_QueId != INVALID_POOL_ID)
    {
        if (MemDeleteMemPool (pCRU_BUF_Chain_FreeQueDesc->u2_QueId) ==
            MEM_FAILURE)
        {
            CRU_BUF_FREE (pCRU_BUF_Chain_FreeQueDesc);
            return CRU_FAILURE;
        }
    }

    CRU_BUF_FREE (pCRU_BUF_Chain_FreeQueDesc);
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Create_DataBlockPool                           */
/*  Description     : Procedure to create Data Block pool. This is called by */
/*                    the Buffer Library Initialization  module .            */
/*  Input(s)        : pBufCfg - Pointer to the application provided buffer   */
/*                              configuration information                    */
/*                    u1Flag = TRUE (Data+Data Descriptors) to be allocated  */
/*                    u1Flag = FALSE Only Data Descriptors to be allocated   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Create_DataBlockPool (tBufConfig * pBufCfg, UINT1 u1Flag)
{
    UNUSED_PARAM (pBufCfg);
    UNUSED_PARAM (u1Flag);
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Delete_DataBlockPool                           */
/*  Description     : Procedure to delete Data Blocks  pool                  */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Delete_DataBlockPool (void)
{
    return CRU_SUCCESS;
}

//*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_LinearBufToChain                          */
/*  Description     : Procedure to Copy linear data to Chain. Updated the    */
/*                    ValidOffset of input chain.                            */
/*  Input(s)        : pChainDesc - Pointer to the input Message Chain        */
/*                    pDataDesc  - Pointer to the Data Descriptor which      */
/*                                 points to the data buffer which needs     */
/*                                 updation                                  */
/*                    pu1Data    - Starting point of the data in the data    */
/*                                 buffer starting from which copy has to be */
/*                                 done                                      */
/*                    pu1Src     - Pointer to the linear buffer from which   */
/*                                 the data will be copied to the chain      */
/*                    u4Offset   - Relative offset to the valid offset in the*/
/*                                 chained buffer at which point copy needs  */
/*                                 to be done                                */
/*                    u4Size     - Number of bytes to be copied from the     */
/*                                 linear buffer to the chain buffer         */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
VOID
CRU_BUF_Copy_LinearBufToChain (tCRU_BUF_CHAIN_DESC * pChainDesc,
                               tCRU_BUF_DATA_DESC * pDataDesc, UINT1 *pu1Data,
                               UINT1 *pu1Src, UINT4 u4Offset, UINT4 u4Size)
{
    UNUSED_PARAM (pChainDesc);
    UNUSED_PARAM (pDataDesc);
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (pu1Src);
    UNUSED_PARAM (u4Offset);
    UNUSED_PARAM (u4Size);
    return;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_LinearBufFromChain                        */
/*  Description     : Procedure to extract 'u4Size' bytes from input buffer  */
/*                    into linear buffer                                     */
/*  Input(s)        : pChainDesc - Pointer to the message descriptor from    */
/*                                 which bytes needs to be copied            */
/*                    pDataDesc  - Pointer to the Data Descriptor which      */
/*                                 points to the data block which contains   */
/*                                 the data to be copied.                    */
/*                    pu1Data    - Pointer to the start of data in the data  */
/*                                 block from which copy needs to be done.   */
/*                    u4Offset   - Offset in the Chain buffer from which     */
/*                                 bytes are requested to be copied          */
/*                    u4Size     - Number of bytes to copy                   */
/*  Output(s)       : pu1Dst     - Pointer to the destination linear buffer  */
/*                                 to which the bytes needs to be copied.    */
/*  Returns         : Returns no of bytes copied                             */
/*****************************************************************************/
INT4
CRU_BUF_Copy_LinearBufFromChain (tCRU_BUF_CHAIN_DESC * pChainDesc,
                                 tCRU_BUF_DATA_DESC * pDataDesc, UINT1 *pu1Data,
                                 UINT1 *pu1Dst, UINT4 u4Offset, UINT4 u4Size)
{
    UNUSED_PARAM (pChainDesc);
    UNUSED_PARAM (pDataDesc);
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (pu1Dst);
    UNUSED_PARAM (u4Offset);
    UNUSED_PARAM (u4Size);
    return 0;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_LastDataDesc                               */
/*  Description     : Procedure to get Last data desc of Chain form current  */
/*                    data desc given.                                       */
/*  Input(s)        : pDataDesc - Start of Data Descriptor whose tail end    */
/*                                is expected                                */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the Last data descriptor in the chain       */
/*****************************************************************************/
tCRU_BUF_DATA_DESC *
CRU_BUF_Get_LastDataDesc (tCRU_BUF_DATA_DESC * pDataDesc)
{
    UNUSED_PARAM (pDataDesc);
    return NULL;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Split_MsgBufChainAtOffset                      */
/*  Description     : Procedure to split the Chain at given Data desc and its*/
/*                    internal data block offset. This is called by the      */
/*                    buffer fragmentation module .                          */
/*  Input(s)        : pChainDesc - Pointer to the Entire buffer which needs  */
/*                                 to be split.                              */
/*                    pDataDesc  - Pointer to the Data Descriptor which      */
/*                                 contains the data block, at which split   */
/*                                 needs to be done.                         */
/*                    pu1Data    - Pointer to the data in the data block, at */
/*                                 which the fragmentation needs to be done  */
/*  Output(s)       : p_pFragChainDescPtr - Pointer to the Split up or       */
/*                             fragmented chain which has resulted           */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
INT4
CRU_BUF_Split_MsgBufChainAtOffset (tCRU_BUF_CHAIN_DESC * pChainDesc,
                                   tCRU_BUF_DATA_DESC * pDataDesc,
                                   UINT1 *pu1Data,
                                   tCRU_BUF_CHAIN_DESC ** ppFragChainDescPtr)
{
    UNUSED_PARAM (pChainDesc);
    UNUSED_PARAM (pDataDesc);
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (ppFragChainDescPtr);
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Config_Validate                                */
/*  Description     : This procedure checks validity of buffer library       */
/*                    initialization parameters
 *                    */
/*  Input(s)        : pBufLibInitData - Pointer to the input buffer library  */
/*                                      initialization parameter to validate */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_FAILURE returned if                                */
/*                      Memory Type is invalid                               */
/*                      Max # of Chain Descriptors allocated is Zero         */
/*                      No. of Blocks allocated for any size is Zero         */
/*                      Min Block Size < 128 and Max Block Size > 4096       */
/*                      Block Size not a multiple of 4                       */
/*                    Otherwise, CRU_SUCCESS                                 */
/*****************************************************************************/
INT4
CRU_BUF_Config_Validate (tBufConfig * pBufLibInitData)
{
    UNUSED_PARAM (pBufLibInitData);
    return CRU_SUCCESS;
}
#endif
