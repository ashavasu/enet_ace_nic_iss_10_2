/********************************************************************
 * Copyright (C) 2011 Aricent Inc.
 *
 * $Id: fsapcfg.h,v 1.1.1.1 2015/04/28 12:11:04 siva Exp $
 *
 * Description: Compilation switches for FSAP.
 *
 *******************************************************************/
#ifndef _FSAPCFG_H_
#define _FSAPCFG_H_

/* Flags to be used to identify endinan-ness                 */
#define OSIX_LITTLE_ENDIAN      1
#define OSIX_BIG_ENDIAN         2

/* Flags used in setting user configuration.                 */
#define FSAP_ON                 1
#define FSAP_OFF                2

#undef SYS_NUM_OF_TIME_UNITS_IN_A_SEC
/* HZ is the frequenct at which the jiffies are updated in kernel 
   (for 2.4 it is 100 2.6 it is 1000) */
#define   SYS_NUM_OF_TIME_UNITS_IN_A_SEC     HZ

/**********************************************/
/*  C O N F I G U R A B L E   S E C T I O N   */
/**********************************************/

/* Enables Multinode communication mechanism.                     */
#undef MCM_WANTED

/* Set RHS depending on processor endiannes.                      */

#ifndef NPSIM_WANTED
#if defined(OS_PTHREADS) || defined(OS_TMO)
#define OSIX_HOST               OSIX_LITTLE_ENDIAN
#else
#define OSIX_HOST               OSIX_BIG_ENDIAN
#endif
#else
/* For NPSIM use always Little Endian. */
#define OSIX_HOST               OSIX_LITTLE_ENDIAN
#endif

/* Set RHS to FSAP_ON to enable MULTINODE support                 */
#define MULTI_NODE_SUPPORT      FSAP_OFF

/* undef line below if target does not have varargs support.      */
#define VAR_ARGS_SUPPORT

/* Set RHS to FSAP_ON, if target env. has file system support     */
#define FILESYS_SUPPORT         FSAP_ON

/*
 * For Multiboard support, the IPC subsystem uses ACK mechanism
 * to guarantee reliable delivery. By default ACKs are disabled.
 * If ACK mechanism is required, change the line below to
 * #undef OSIX_IPC_NO_ACK
 * disabled. 
 */
#define OSIX_IPC_NO_ACK 

/* Set RHS to FSAP_ON, to enable tracing for OSIX.                */
#define DEBUG_OSIX              FSAP_OFF

/* Set RHS to FSAP_ON, to enable tracing for memory mgmt. module. */
#define DEBUG_MEM               FSAP_OFF

/* Set RHS to FSAP_ON, to enable tracing for Timer module.        */
#define DEBUG_TMR               FSAP_OFF

/* Set RHS to FSAP_ON, to enable tracing for Buffer mgmt. module. */
#define DEBUG_BUF               FSAP_OFF

/* Set RHS to FSAP_ON, to enable debug version of UTL library.    */
#define DEBUG_UTL               FSAP_OFF

/* Set RHS to FSAP_ON, to include the math support in fsap.       */
#define MATH_SUPPORT            FSAP_OFF

/* Set RHS to FSAP_ON, to make unaligned memory accesses safe.    */
#define ALIGN_SAFE              FSAP_OFF

#endif
