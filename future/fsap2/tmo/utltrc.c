/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utltrc.c,v 1.17 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * The UTL-TRC module.
 *
 */

#include "osxinc.h"
#include "utltrc.h"
#include "utltrci.h"
#include "osix.h"
#include <time.h>
#include <unistd.h>

static UINT4        gu4ShowTime = 0;
static INT4         gi4LogFd = OSIX_FILE_LOG;
static INT4         gu4InCoreLog = OSIX_LOG_METHOD;

/* This structure is used to store the trace messages 
 * in an incore buffer. It is a cyclic buffer and so
 * wraps around. The API to get the logs is UtlGetLogs
 * and it supports getting the 'head' or 'tail' logs
 * in the unix like fashion.
 * i4Front - Points to the first (earliest) log
 * i4Rear  - Points to the one beyond the latest log.
 * u4Wrapped - Indicates if i4Rear has completed one round
 *             of the circular list. If so it is set and
 *             from then on i4Front also move along, indicative
 *             of the fact that the earliest log messages are
 *             being replaced by newer ones.
 */
struct tLogBuf
{

    CHR1                Log[UTL_MAX_LOGS][UTL_MAX_LOG_LEN];
    INT4                i4Front;
    INT4                i4Rear;
    UINT4               u4Wrapped;
};
static struct tLogBuf Logs;

/***************************************************************/
/*  Function Name   : UtlTrcPrint                              */
/*  Description     : Prints the input message                 */
/*                    Based on settings in osix.h it logs to   */
/*                    file. Also based on settings, the traces */
/*                    can be displayed on screen or made to    */
/*                    accumulate in an incore buffer           */
/*                    See OSIX_FILE_LOG and OSIX_LOG_METHOD    */
/*                    in osix.h                                */
/*  Input(s)        : pc1Msg - pointer to the message          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlTrcPrint (const CHR1 * pc1Msg)
{
    INT4                i4Len;
    UINT1               au1LogFile[20];
    CHR1                ac1TmpBuf[80];

    if (gi4LogFd == 1)
    {
        SPRINTF ((CHR1 *) au1LogFile, "fsir.log.%d", getpid ());

        gi4LogFd = FileOpen (au1LogFile, OSIX_FILE_RW | OSIX_FILE_CR);

        if (gi4LogFd)
        {
            SPRINTF (ac1TmpBuf, "Creating log file %s\n", au1LogFile);

            /* Display on screen and put one copy in the log file */
            printf (ac1TmpBuf);
            UtlTrcPrint (ac1TmpBuf);
        }
    }

    if (gu4InCoreLog == FLASH_LOG_LOCATION)
    {
        printf (" Not supported to store traces in flash location \n");
        return;
    }

    if (gu4InCoreLog == INCORE_LOG)
    {
        /* Do not print on screen. Only store in memory */
        i4Len = STRLEN (pc1Msg);
        MEMCPY (Logs.Log[Logs.i4Rear], pc1Msg,
                (i4Len > UTL_MAX_LOG_LEN ? UTL_MAX_LOG_LEN : i4Len));

        /* Null terminate it. If the string is of length UTL_MAX_LOG_LEN,
         * then the last character will be lost as this will be used to
         * store the '\0' byte. Since we deal with array indexes rather
         * than no. of bytes, the relational expression is slightly
         * different (i.e., we use '>=' ).
         */
        Logs.Log[Logs.i4Rear]
            [i4Len >= UTL_MAX_LOG_LEN ? (UTL_MAX_LOG_LEN - 1) : i4Len] = 0;

        Logs.i4Rear++;
        if (Logs.u4Wrapped)
        {
            Logs.i4Front++;
        }

        if (Logs.i4Rear == UTL_MAX_LOGS)
        {
            Logs.i4Rear = 0;
            Logs.u4Wrapped = 1;
        }
        if (Logs.i4Front == UTL_MAX_LOGS)
        {
            Logs.i4Front = 0;
        }
    }
    else
    {
        /* print to screen */
        i4Len = printf ("%s", pc1Msg);
    }

    if (gi4LogFd > 1)
    {
        FileWrite (gi4LogFd, pc1Msg, i4Len);
    }
}

/***************************************************************/
/*  Function Name   : UtlSetLogMode                            */
/*  Description     : API to dynamically change logging        */
/*                    mechanism.                               */
/*                    The default logging mechanism is defined */
/*                    in osix.h (OSIX_LOG_METHOD)              */
/*                    By default this is set to CONSOLE_LOG    */
/*                    so that the traces appear on screen      */
/*                    Other option is INCORE_LOG which switches*/
/*                    to logging the traces in a circular buf  */
/*  Input(s)        : u4val - Logging mode to be set.          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlSetLogMode (UINT4 u4val)
{
    gu4InCoreLog = u4val;
}

/***************************************************************/
/*  Function Name   : UtlGetLogMode                            */
/*  Description     : API to get the current logging mechanism */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None                                     */
/*  Returns         : One of eLoggingModes.                    */
/***************************************************************/
UINT4
UtlGetLogMode (VOID)
{
    return (gu4InCoreLog);
}

/***************************************************************/
/*  Function Name   : UtlGetLogs                               */
/*  Description     : API to get retrieve the logs.            */
/*                    It returns the number of bytes copied.   */
/*                    The buffer ac1Buf should be sufficiently */
/*                    large to prevent overruns.               */
/*                    The size should be based on MAX_LOG_LEN  */
/*                    i4Count and UTL_MAX_LOGS                */
/*  Input(s)        : ac1Buf - Buffer into which to copy logs. */
/*                    i4Count - Number of logs to copy         */
/*                     i4Count < 0 => retrieve the last        */
/*                                    i4Count logs.            */
/*                     i4Count > 0 => retrieve the earliest    */
/*                                    i4Count logs.            */
/*                     i4Count = 0 => retrieve all the logs    */
/*                                    that are available.      */
/*  Output(s)       : Filled ac1Buf.                           */
/*  Returns         : Number of bytes copied to ac1Buf.        */
/***************************************************************/
INT4
UtlGetLogs (CHR1 ac1Buf[], INT4 i4Count)
{
    INT4                i4Len;
    INT4                i4Offset = 0;
    INT4                i4Idx = Logs.i4Front;

    if (i4Count < 0)
    {
        i4Count *= -1;
        i4Count = (i4Count > UTL_MAX_LOGS ? UTL_MAX_LOGS : i4Count);
        i4Idx = Logs.i4Rear - i4Count;
        if (i4Idx < 0)
        {
            i4Idx = UTL_MAX_LOGS + i4Idx;
        }

    }

    if (i4Count == 0)
    {
        i4Idx = Logs.i4Front;
        if (Logs.i4Rear == Logs.i4Front)
        {
            i4Count = UTL_MAX_LOGS;
        }
        else
        {
            i4Count = Logs.i4Rear - Logs.i4Front;
        }
    }

    i4Count = (i4Count > UTL_MAX_LOGS ? UTL_MAX_LOGS : i4Count);

    for (; i4Count; i4Count--)
    {
        i4Len = STRLEN (Logs.Log[i4Idx]);
        MEMCPY (ac1Buf + i4Offset, Logs.Log[i4Idx], i4Len);
        i4Offset += i4Len;

        i4Idx++;
        if (i4Idx == UTL_MAX_LOGS)
        {
            i4Idx = 0;
        }
    }

    return (i4Offset);
}

/***************************************************************/
/*  Function Name   : UtlTrcLog                                */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST char *pi1Name,
           CONST char *pi1Fmt, ...)
{
    va_list             VarArgList;
    va_list             VarArgListA;
    char                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST char         *pi1FmtString;
    INT4                i4ArgCount;
    INT4                pos = 0;
    INT4                i4val;
    time_t              t;
    struct tm          *tm = 0;
    char               *pNullPtr = 0;

    /* unused variable */
    pNullPtr = pNullPtr;

    /* Is Tracing for this type enabled */
    if (!(u4Flag & u4Value))
    {
        return;
    }

    /* Crash the program if module name is (null). */
#if (DEBUG_UTL == FSAP_ON)
    if (pi1Name == NULL)
    {
        *pNullPtr = 0;
    }
#endif

    pi1FmtString = pi1Fmt;
    i4ArgCount = 0;

    va_start (VarArgList, pi1Fmt);
    va_start (VarArgListA, pi1Fmt);

    /* Count the number of arguments and perform sanity
     * checks such as ensuring the parameter corresponding
     * to %s is a non-null pointer
     */
    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                /* We assume fmt-spec is one of csdu,
                 * or any other 4 byte quantity.. 
                 * As per ANSI-C, char is to be
                 * treated as int when used in varargs.
                 */
                i4val = va_arg (VarArgList, int);

                i4ArgCount++;

                /* Is arg to %s is a (null). */
#if (DEBUG_UTL == FSAP_ON)
                if (*pi1FmtString == 's' && i4val == 0)
                {
                    /* For the case of TMO, we crash the program 
                     * to help to locate-and-fix the problem,
                     * using gdb's backtrace.
                     * For RTOS like environments, it'd be
                     * better to merely return without crashing
                     * the program.
                     */
                    *pNullPtr = 0;
                }
#endif

            }
        }
        pi1FmtString++;
    }

#if (DEBUG_UTL == FSAP_ON)
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }
#endif

    /* Set the current wall clock time. and the Module Name.
     * To save on calls to sprintf, try to push all parameters
     * into one single sprintf call whenever possible.
     */
    if (gu4ShowTime)
    {
        t = time (NULL);

        tm = localtime (&t);

        if (tm != NULL)
        {
            pos += SPRINTF (ai1LogMsgBuf, "%d:%d:%d: %d:%d:%d:%s ",
                            tm->tm_hour, tm->tm_min, tm->tm_sec,
                            tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,
                            pi1Name);
        }
    }
    else
    {
        /* Time not needed, Tag the Module Name alone, if present */
        /* Idea is if module name is null, we don't want the ':'  */
        if (strcmp (pi1Name, ""))
        {
            pos += SPRINTF (ai1LogMsgBuf + pos, "%s: ", pi1Name);
        }
    }

    vsprintf (ai1LogMsgBuf + pos, pi1Fmt, VarArgListA);

    UtlTrcPrint ((const char *) ai1LogMsgBuf);

    va_end (VarArgList);
}
#else /* VAR_ARGS_SUPPORT */
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST INT1 *pi1Name,
           CONST INT1 *pi1Fmt, INT4 p1, INT4 p2, INT4 p3,
           INT4 p4, INT4 p5, INT4 p6)
{
    INT4                i4ArgCount;
    INT4                i4p[6];
    INT4                i4val;
    INT1                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    INT1                ai1Spec[6];
    CONST INT1         *pi1FmtString;
    time_t              t;
    struct tm          *tm;
    char               *pNullPtr = 0;

    /* unused variable */
    pNullPtr = pNullPtr;

    i4p[0] = p1;
    i4p[1] = p2;
    i4p[2] = p3;
    i4p[3] = p4;
    i4p[4] = p5;
    i4p[5] = p6;

    /* Is Tracing for this type enabled. */
    if (!(u4Flag & u4Value))
    {
        return;
    }

    /* Crash the program if module name is (null). */
#if (DEBUG_UTL == FSAP_ON)
    if (pi1Name == NULL)
    {
        *pNullPtr = 0;
    }
#endif

    pi1FmtString = pi1Fmt;

    i4ArgCount = 0;

    /* Count the number of arguments and perform sanity
     * checks such as ensuring the parameter corresponding
     * to %s is a non-null pointer
     */
    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                /* We assume fmt-spec is one of csdu,
                 * or any other 4 byte quantity. 
                 * As per ANSI-C, char is to be
                 * treated as int when used in varargs.
                 */
                i4val = i4p[i4ArgCount];

                i4ArgCount++;

                /* Is arg to %s is a (null). */
#if (DEBUG_UTL == FSAP_ON)
                if (*pi1FmtString == 's' && i4val == 0)
                {
                    /* For the case of TMO, we crash the program 
                     * to help to locate-and-fix the problem,
                     * using gdb's backtrace.
                     * For RTOS like environments, it'd be
                     * better to merely return without crashing
                     * the program.
                     */
                    *pNullPtr = 0;
                }
#endif
            }
        }
        pi1FmtString++;
    }

#if (DEBUG_UTL == FSAP_ON)
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }
#endif

    /* Set the current wall clock time. and the Module Name.
     * To save on calls to sprintf, try to push all parameters
     * into one single sprintf call whenever possible.
     */
    if (gu4ShowTime)
    {
        t = time (NULL);

        tm = localtime (&t);

        pos += SPRINTF (ai1LogMsgBuf, "%d:%d:%d: %d:%d:%d:%s ",
                        tm->tm_hour, tm->tm_min, tm->tm_sec,
                        tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900,
                        pi1Name);
    }
    else
    {
        /* Time not needed, Tag the Module Name alone, if present */
        /* Idea is if module name is null, we don't want the ':'  */
        if (strcmp (pi1Name, ""))
        {
            pos += SPRINTF (ai1LogMsgBuf + pos, "%s: ", pi1Name);
        }
    }

    switch (i4ArgCount)
    {
        case 0:
            SPRINTF (ai1LogMsgBuf, pi1Fmt);
            break;
        case 1:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1);
            break;
        case 2:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2);
            break;
        case 3:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3);
            break;
        case 4:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4);
            break;
        case 5:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5);
            break;
        case 6:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5, p6);
            break;
        default:
            SPRINTF (ai1LogMsgBuf, "Number of arguments > 6.\n");
            break;
    }

    UtlTrcPrint (ai1LogMsgBuf);
}
#endif /* VAR_ARGS_SUPPORT */

/***************************************************************/
/*  Function Name   : UtlSysTrcLog                             */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4SysLevel - Syslog message levels       */
/*                    u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST char *pi1ModId, CONST char *pi1Name,
           CONST char *pi1Fmt, ...)
{
      UNUSED_PARAM (u4SysLevel);
      UNUSED_PARAM (u4Flag);
      UNUSED_PARAM (u4Value);
      UNUSED_PARAM (pi1ModId);
      UNUSED_PARAM (pi1Name);
      UNUSED_PARAM (pi1Fmt);

}
#else /* VAR_ARGS_SUPPORT */
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST INT1 *pi1ModId, CONST INT1 *pi1Name,
           CONST INT1 *pi1Fmt, INT4 p1, INT4 p2, INT4 p3,
           INT4 p4, INT4 p5, INT4 p6)
{
      UNUSED_PARAM (u4SysLevel);
      UNUSED_PARAM (u4Flag);
      UNUSED_PARAM (u4Value);
      UNUSED_PARAM (pi1Name);
      UNUSED_PARAM (pi1ModId);
      UNUSED_PARAM (pi1Fmt);
      UNUSED_PARAM (p1);
      UNUSED_PARAM (p2);
      UNUSED_PARAM (p3);
      UNUSED_PARAM (p4);
      UNUSED_PARAM (p5);
      UNUSED_PARAM (p6);
}
#endif /* VAR_ARGS_SUPPORT */

/***************************************************************/
/*  Function Name   : UtlShowTime                              */
/*  Description     : Used to enable/disable display of time.  */
/*  Input(s)        : u4Flag - A boolean flag.                 */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlShowTime (UINT4 u4Flag)
{
    gu4ShowTime = u4Flag;
}

VOID
UtlTrcClose (VOID)
{
    if (gi4LogFd > 1)
    {
        FileClose (gi4LogFd);
    }
}

/****************************************************************************/
 /*                                                                          */
 /*    Function Name      : LoggerTaskMain                                   */
 /*                                                                          */
 /*    Description        : This is the main function of the logger task     */
 /*                                                                          */
 /*    Input(s)           : pi1Param - dummy parameter                       */
 /*                                                                          */
 /*    Output(s)          : None.                                            */
 /*                                                                          */
 /*    Returns            : None.                                            */
 /****************************************************************************/
VOID
LoggerTaskMain (INT1 *pi1Param)
{
    return;
}
