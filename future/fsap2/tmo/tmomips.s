	.file 1 "tmomips2.s"
	.section	".text"
	.align 2
	.globl TmoSched
	.type TmoSched, @function
	.ent TmoSched 2

TmoSched:
TmoCtxtSave:
       subu $sp, 16                    # Create the Stack Frame
       sw $31, 0($sp)
       .frame $sp, 16, $31             # Indicate virtual stack frame

       
         # Preserve GPR[8, 1] 
       sw $8, 4($sp)
       sw $4, 12($sp)
       .set noat
       sw $1, 8($sp)
       .set at

       # pTMOv2_Etcb = p_curtcb->x
       la $4, pTMOv2_Etcb 


       la $8, p_curtcb
       lw $25, 0($8)   # position p_curtcb

       lw $8, 12($25)  # position p_curtcb->x

       sw $8, 0($4)    # pTMOv2_Etcb = p_curtcb->x

       # Preserve the Current TCB into p_curtcb->context
       # Preserve GPRS[31, 29, 30, 0:7, 9:24, 26,27] 
       # and  SPRS into p_curtcb->context[1..31]. context[1]
       # starts at 20th Position; GPR[25] keeps track of p_curtcb
       sw $31, 20($25)  # Store the Current Program Counter 
                        #  @p_curtcb->context[IPC]


       move $4, $sp
       addu $4, 16
       sw $4, 24($25)   # Store the Current Stack Pointer 
                        #  @p_curtcb->context[SP]


       sw $30, 28($25)  # Store the Current Stack frame pointer 
                        #  @p_curtcb->context[3]

       lw $8, 8($sp)
       sw $8, 32($25)   # Store GPR[1]

       # Store GPRs[2..7] into p_curtcb->context
       sw $2, 36($25)
       sw $3, 40($25)

       lw $8, 12($sp)  # Restore GPR[4] from Stack and store
       sw $8, 16($25)

       sw $5, 44($25)
       sw $6, 48($25)
       sw $7, 52($25)

       lw $8, 4($sp)  # Restore GPR[8] from Stack and store
       sw $8, 56($25)


       # Store GPRs[9..24, 26, 27] into p_curtcb->context
       sw $9, 60($25)
       sw $10, 64($25) 
       sw $11, 68($25)
       sw $12, 72($25)
       sw $13, 76($25)
       sw $14, 80($25)
       sw $15, 84($25)
       sw $16, 88($25)
       sw $17, 92($25)
       sw $18, 96($25)
       sw $19, 100($25)
       sw $20, 104($25)
       sw $21, 108($25)
       sw $22, 112($25)
       sw $23, 116($25)
       sw $24, 120($25)

       sw $26, 124($25)
       sw $27, 128($25)

       # Preserve SPRs [HI, LO] into p_curtcb
       mfhi $8
       sw $8, 132($25)
       mflo $8
       sw $8, 136($25)

       .cprestore 12
       jal TmoReSched
       nop

      addu $sp, 16     # Cleanup Stack Frame


       # TmoReSched reschedules the Tasks, by means of 
       # re-adjusting the Run Q w.r.t. to their Task States
       # Upon returning from TmoReSched, p_curtcb will get
       # updated for the current task that has to be scheduled
       
TmoCtxtSwitch:
        # pTMOv2_Etcb = p_curtcb->x
       la $4, pTMOv2_Etcb 
       la $8, p_curtcb
       lw $25, 0($8)          # position p_curtcb

       lw $8, 12($25)         # position p_curtcb->x
       sw $8, 0($4)           # pTMOv2_Etcb = p_curtcb->x

       lw $sp, 24($25)        # Restore the Current Stack Pointer 
                              #  @p_curtcb->context[SP]

       subu $sp, 16           # Create Stack Frame
       .frame $sp, 16, $31    # Indicate virtual stack frame


       lw $30, 28($25)        # Restore the Current Stack frame pointer 
                              #  @p_curtcb->context[3]


       .set noat
       lw $1, 32($25)         # Restore GPR[1]
       .set at

       # Restore GPR[2..7, 9, 11..24, 26,27]
       lw $2, 36($25)
       lw $3, 40($25)
       lw $4, 16($25)
       lw $5, 44($25)
       lw $6, 48($25)
       lw $7, 52($25)

       lw $9, 60($25)
       lw $10, 64($25)
       lw $11, 68($25)
       lw $12, 72($25)
       lw $13, 76($25)
       lw $14, 80($25)
       lw $15, 84($25)
       lw $16, 88($25)
       lw $17, 92($25)
       lw $18, 96($25)
       lw $19, 100($25)
       lw $20, 104($25)
       lw $21, 108($25)
       lw $22, 112($25)
       lw $23, 116($25)
       lw $24, 120($25)

       lw $26, 124($25)
       lw $27, 128($25)

       # Restore SPRs [HI, LO] into p_curtcb
       lw $8, 132($25)
       mthi $8
       lw $8, 136($25)
       mtlo $8

       # Restore the GPRs from p_curtcb->context
       lw $31, 20($25)        # Restore the Current Program Counter 
                              #  @p_curtcb->context[IPC]

       lw $8, 56($25)         # Restore GPR[8]

       sw $31, 0($sp)         # Next Task to be Scheduled

       lw $25, 0($sp)         # Set GPR[25] to IPC
       lw $31, 0($sp)         # GPR[31] = Current IPC of the scheduled Task
       addu $sp, 16
       nop

       j $31                  # Schedule the Current Task
       nop
	.end TmoSched
