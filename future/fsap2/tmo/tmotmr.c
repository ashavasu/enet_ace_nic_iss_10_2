/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmotmr.c,v 1.15 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * Timer Mgmt. routines.
 *
 */

#include "tmotypes.h"
#include "tmo.h"

extern unsigned int u4KernTicks;
LIST                wd_list;
unsigned int        u4KernTicks = 0;

extern void         TmrProcessTick (void);

/****************************************************************
**  FUNCTION : Timer Task
****************************************************************  
**  RETURN VALUE :
**  DESCRIPTION  :
**    Timer task process the list of etcbs with active timers.  It
**    is activated via by the watchdog timer at the Operating System
**    Clock rate.  When the watchdog timer expires it sends an event 
**   to the timer task. The timer task then processes the list of etcbs 
**    with active timers.  Each active timer is decremented.  If a
**    timer reaches zero an event is set in the etcb, a check is made
**    against the etcb event mask, and a unit is sent to the etcb 
**    semaphore if necessary.  Finally if the timer is periodic it
**    is restarted with its original u4_Count.
*******************************************************************/
PRIVATE VOID
TMO_Timer_Service (INT4 i4_NumOfTicks)
{
    /* Call TMR API to process timer lists. */
    TmrProcessTick ();
    TmoWdStart (tmoWdID, i4_NumOfTicks, TMO_Timer_Service, i4_NumOfTicks);
}

/******************************************************************
**  FUNCTION : Start timer service
******************************************************************  
**  RETURN VALUE:
**  DESCRIPTION:
**         This function starts watchdog timer for timer service.
****************************************************************/
EXPORT UINT4
TMOv2_Start_Timer_Service (INT4 i4_NumOfTicks)
{
    TmoWdStart (tmoWdID, i4_NumOfTicks, TMO_Timer_Service, i4_NumOfTicks);
    return 0;
}

/***************************************************************
**  FUNCTION : Stop timer service
***************************************************************  
**  RETURN VALUE:
**  DESCRIPTION :
**    This function stops watchdog timer for timer service.
****************************************************************/
EXPORT INT4
TMOv2_Stop_Timer_Service ()
{
    return TmoWdCancel (tmoWdID);
}

/*************************************************************
**  FUNCTION : Task Delay
**************************************************************  
**  RETURN VALUE:
**  DESCRIPTION :
**    This function suspends task for the specified time interval
**************************************************************/
EXPORT INT4
TMOv2_Delay_Task (i4_Duration)
     INT4                i4_Duration;
{
    if (i4_Duration < 0)
        return -1;
    if (i4_Duration == 0)
        i4_Duration = 1;
    p_curtcb->count = i4_Duration;
    p_curtcb->status &= ~TASK_RUN;
    p_curtcb->status |= TASK_SLEEP;
    TmoSched ();

    return 0;
}

/********************************************************************
**  FUNCTION : Set Value Of Kernel's Tick Counter
********************************************************************  
**  RETURN VALUE:
**  DESCRIPTION:
**     This function will change the value returned by TMOv2_Get_Tick,
**     but will not change any delay fields or timeouts selected for
**     any task.
*********************************************************************/
EXPORT VOID
TMOv2_Set_Tick (u4_TickCounter)
     UINT4               u4_TickCounter;
{
    u4KernTicks = u4_TickCounter;
}

/******************************************************************
**  FUNCTION : Get Value Of Kernel's Tick Counter 
*******************************************************************  
**  RETURN VALUE :
**  DESCRIPTION  :
**    This Function Returns The Current Value Of The Global Ticks
********************************************************************/
EXPORT UINT4
TMOv2_Get_Tick ()
{
    return u4KernTicks;
}

/* This is called from ALARM SIGNAL handler */

void
TmoClkInt (void)
{
    WDOG_TIMER         *p_wd;
    TCB                *p_tcb, *p_tmp;

    /* Update the system clock. */
    ++u4KernTicks;

    for (p_tcb = sleepq.p_head; p_tcb;)
    {
        p_tmp = p_tcb->p_next;
        /* =0 takes care of WAIT_FOREVER. */
        if (p_tcb->count > 0)
        {
            if (--p_tcb->count == 0)
            {
                if ((p_tcb->status) & TASK_SEMAPHORE)
                {
                    p_tcb->semStatus = SEM_TIMEOUT;
                }
                TmoNodeDel ((LIST *) & sleepq, (NODE *) p_tcb);
                TmoNodeAdd ((LIST *) & readyq, (NODE *) p_tcb);
            }
        }
        p_tcb = p_tmp;
    }

    for (p_wd = (WDOG_TIMER *) wd_list.p_head; p_wd; p_wd = p_wd->p_next)
    {
        if (p_wd->delay == 0)    /* Timer is canceled */
            continue;
        if (--p_wd->count == 0)
        {
            if (p_wd->p_func == NULL)
                TmoExit ("clkisr: NULL FUNCPTR\n", 0);
            (*p_wd->p_func) (p_wd->param);
            p_wd->count = p_wd->delay;
        }
    }
}

WDOG_ID
TmoWdCreate ()
{
    WDOG_ID             wd_id;

    if ((wd_id = (WDOG_ID) malloc (sizeof (WDOG_TIMER))) == NULL)
        TmoExit ("TmoWdCreate: can't allocate memory for watchdof timer\n", 0);
    memset ((char *) wd_id, 0, sizeof (WDOG_TIMER));

    TmoNodeAdd ((LIST *) & wd_list, (NODE *) wd_id);
    return (wd_id);
}

void
TmoWdDelete (wd_id)
     WDOG_ID             wd_id;
{
    if (!TmoNodeFind ((LIST *) & wd_list, (NODE *) wd_id))
        TmoExit ("wdDelete: can't find wd_id=0x%08lx\n", (char *) wd_id);
    TmoNodeDel ((LIST *) & wd_list, (NODE *) wd_id);
    free ((char *) wd_id);
}

void
TmoWdStart (wd_id, delay, p_func, param)
     WDOG_ID             wd_id;
     int                 delay;
     tTmoTaskMainFunc    p_func;
     int                 param;
{
    WDOG_TIMER         *p_wd;

    if (!TmoNodeFind ((LIST *) & wd_list, (NODE *) wd_id))
        TmoExit ("TmoWdStart:invalid wd_id=0x%08lx\n", (char *) wd_id);

    p_wd = (WDOG_TIMER *) wd_id;
    p_wd->p_func = p_func;
    p_wd->delay = delay;
    p_wd->param = param;
    p_wd->count = delay;
    return;
}

UINT4
TmoWdCancel (wd_id)
     WDOG_ID             wd_id;
{
    WDOG_TIMER         *p_wd;

    if (!TmoNodeFind ((LIST *) & wd_list, (NODE *) wd_id))
        TmoExit ("TmoWdCancel:invalid wd_id=0x%08lx\n", (char *) wd_id);

    p_wd = (WDOG_TIMER *) wd_id;

    p_wd->delay = 0;

    return 0;
}
