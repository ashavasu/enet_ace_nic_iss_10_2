/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmokern.c,v 1.11 2015/04/28 12:18:19 siva Exp $
 *
 * TMO's RTOS Kernel Simulator.
 */

#include "tmo.h"

unsigned long       gStups;
#ifdef TASK_STACK_MONITOR
static void         TmoCheckStack (TCB * pTcb);
#endif

TASKQHEAD           runq;
TASKQHEAD           readyq;
TASKQHEAD           sleepq;

TASKQHEAD           nullq = { NULL, NULL, 0 };

TCB                *p_curtcb = NULL;    /* Current TCB */

int                 scheduler_disabled = 0;
int                 signal_number = 0;

int                 received_signal[32] = { 0 };
int                 kernel_booting = 1;
static void         TmoLoop (void);

void
TmoStart (n)
     int                 n;
{
    if (!p_curtcb)
    {
        exit (n);
    }
    if (runq.p_head == NULL && readyq.p_head == NULL && sleepq.p_head == NULL)
    {
        exit (n);
    }

    kernel_booting = 0;
    TmoLoop ();
}

#ifdef TASK_STACK_MONITOR
void
TmoCheckStack (TCB * pTcb)
{
    int                 ss = pTcb->stack_size;
    int                *p = pTcb->original_sp;

    if (p == pTcb->roof)
    {
        printf ("%s: Hit Stack Bottom %d!!!\n\n", pTcb->name, pTcb->stack_size);
    }
    while (p < pTcb->roof)
    {
        if (*p)
        {

            /* Lower the roof */
            pTcb->roof = p;
            ss = (int) ((int) pTcb->initial_sp - (int) p);

            /* Inform user if we've crossed a checkpoint. */
            if (ss > pTcb->checkpoint)
            {
                printf ("%s: task stack has touched %d bytes.\n", pTcb->name,
                        ss);
                pTcb->checkpoint = ss + 1000;
            }

            break;
        }
        p++;
    }
}
#endif

void
TmoReSched (void)
{
    TCB                *pTcb, *pOldTcb;
    NODE               *p_node;

#ifdef TASK_STACK_MONITOR
    TmoCheckStack (p_curtcb);
#endif

    if (readyq.count > 0)
    {
        if (&runq == NULL || &readyq == NULL)
            TmoExit ("TmoReSched: runq/readyq is NULL\n", 0);
        /* Very inefficient but straightforward indeed! */
        for (p_node = TmoNodeDelHead ((LIST *) & readyq); p_node;)
        {
            TmoNodeAdd ((LIST *) & runq, p_node);
            p_node = TmoNodeDelHead ((LIST *) & readyq);
        }
    }

    if (scheduler_disabled)
        return;

    pOldTcb = p_curtcb;

    if (p_curtcb->status & TASK_DELETED)
    {
        goto skip;
    }

    if (p_curtcb->status & TASK_SLEEP)
    {
        TmoNodeAdd ((LIST *) & sleepq, (NODE *) p_curtcb);
        goto skip;
    }

    if (kernel_booting)
        return;

    /* Get next highest priority task from runq */
    /* If none found, no rescheduling is done.  */
    /* Put current task in the runq.            */
    for (pTcb = runq.p_head; pTcb; pTcb = pTcb->p_next)
    {
        if (p_curtcb->priority > pTcb->priority)
            break;
    }
    if (pTcb == NULL)
    {
        return;
    }
    TmoNodeAdd ((LIST *) & runq, (NODE *) p_curtcb);

  skip:
    /* get new task control block */
    p_curtcb = (TCB *) TmoNodeDelHead ((LIST *) & runq);

    if (p_curtcb->status & TASK_DELETED)
    {
        free (pOldTcb->original_sp);
        free ((char *) pOldTcb->name);
        free ((char *) pOldTcb);
        pOldTcb = NULL;            /* prevent from tasktrace */
    }

    p_curtcb->nswitch++;
    p_curtcb->status = TASK_RUN;
}

void
TmoKernInit (int tps, int stups)
{
    TCB                *p_tcb;
    int                 TMO_id;

    tps = tps;                    /* unused param */

    scheduler_disabled = 0;
    sleepq = readyq = runq = nullq;

    TMO_id = TMOv2_Spawn_Task ((const INT1 *) "idle", 0, 10000, TmoOs, 0, 0);
    p_tcb = (TCB *) TMOv2_Get_VxId ((UINT4) TMO_id);

    TmoNodeDel ((LIST *) & readyq, (NODE *) p_tcb);
    p_tcb->priority = 255;
    p_tcb->status = TASK_RUN;
    p_tcb->name = TmoStrSave ("idle");
    p_tcb->x = (int) TMOv2_Get_Etcb (TMO_id);
    pTMOv2_Etcb = (tTMOv2_ETCB_REC *) p_tcb->x;
#ifdef i386
    p_tcb->context[IEIP] = (int) p_tcb->entry;
    p_tcb->context[IEBP] = p_tcb->context[IESP] = (int) p_tcb->initial_sp;
#endif
#ifdef MC68000
    p_tcb->context[IPC] = (int) p_tcb->entry;
    p_tcb->context[IA6] = p_tcb->context[IA7] = (int) p_tcb->initial_sp;
#endif
#ifdef MPCxx
    p_tcb->context[SPR0] = (int) p_tcb->entry;
    p_tcb->context[SPR31] = p_tcb->context[SPR1] = (int) p_tcb->initial_sp;
#endif
#ifdef IDT_MIPS2_RC32334
    /* MIPS II Archietecture RC32334 Mips Processor from IDT */
    p_tcb->context[IPC] = (int) p_tcb->entry;
    p_tcb->context[IPARM] = (int) *((p_tcb->initial_sp) + 1);    /* Store the Param */
    /* Assuming that the Task passes only one
     * parameter to the function and Stack GROWS DOWN */
    p_tcb->context[SPR3] = (int) *((p_tcb->initial_sp) + 1);    /* Store Param */
#endif
    p_curtcb = p_tcb;

    gStups = stups;

    TmoSched ();
}

void
TmoOs (int arg)
{
    arg = arg;
    for (;;)
    {
        TmoSched ();
    }
}

static void
TmoLoop (void)
{
    unsigned long       u4TickInterval;
    unsigned long       u4Ticks;
    unsigned long       u4TicksPerStup;
    struct timespec     req;
    clock_t             st_time, en_time;

    /* the interval between 2 ticks in micro-seconds */
    /* this is also the timer granularity in micro-seconds */
    /* all calculations in this routine will be based on micro-seconds */
    u4TickInterval = 1000000 / gStups;

    req.tv_sec = u4TickInterval / 1000000;
    req.tv_nsec = (u4TickInterval % 1000000) * 1000;

    u4TicksPerStup = sysconf (_SC_CLK_TCK) / gStups;

    for (;;)
    {
        /* To allow other linux processes to run, we do nanosleep for    */
        /* timer granularity between processing 2 ticks. We rely on the  */
        /* times() call to provide us with an accurate clock.            */

        st_time = times (NULL);
        nanosleep (&req, NULL);
        en_time = times (NULL);

        /* If the retval of times() rolls over, assume one tick elapsed */
        if (en_time < st_time)
        {
            u4Ticks = 1;
        }
        else
        {
            u4Ticks = ((en_time - st_time) / u4TicksPerStup);
        }

        while (u4Ticks != 0)
        {
            TmoClkInt ();
            TmoSched ();
            u4Ticks--;
        }
    }
}
