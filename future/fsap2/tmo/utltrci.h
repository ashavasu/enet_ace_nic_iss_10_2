/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utltrci.h,v 1.14 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * UTL trace module's internal file.
 *
 */

#ifdef VAR_ARGS_SUPPORT
#include <stdarg.h>
#endif /* VAR_ARGS_SUPPORT */

#define MAX_LOG_STR_LEN                256

#define MAX_CHARS_PER_LINE              60

#define MAX_CHARS_FOR_BYTE_DUMP          6
