/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmo.h,v 1.18 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * Define data structure for TMO.
 *
 */


#ifndef _TMO_H
#define _TMO_H

#include <stdio.h>
#include <signal.h>
#include <setjmp.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <sys/times.h>
#include <stdlib.h>
#include <string.h>

#include "tmotypes.h"
#include "tmotypdfs.h"
#include "srmbuf.h"

#define TMO_STACKSIZE        20000        /***  Default task stack size     ***/

/** Macros **/
#define TMO_SYS_MALLOC(x)          (x *)malloc(sizeof(x))
#define TMO_SYS_FREE(x)            free(x)
#define TMO_MALLOC(Size, Type)     (Type *)malloc(Size)
#define TMO_FREE(Ptr)              free(Ptr)

/*******************************************
**** Task Management Type Declarations *****
********************************************/

/**************************************************
**** Extended Task Control Block Data Structure ***
***************************************************/
typedef struct TMO_ETCB_REC{
   UINT4   u4_DsName;                   /**** Name Assigned To Data structure */
   UINT4   u4_Vw_Task_Id;               /**** OS simulator task Identifier    */
   UINT4   u4_Tmo_Task_Id;              /**** Os Services Task Id             */
                                        /*** i.e., TMO Task Id.               */
   UINT4   OsixTaskId;                  /*** OsixTaskId                       */
   UINT4   TntTaskId;

   SEM_ID  Sem_Id;                      /**** Sema4 for event waiting         */
   struct TMO_ETCB_REC *pNextEtcb;      /**** Next Etcb                       */
   struct TMO_ETCB_REC *pPrevEtcb;      /**** Previous Etcb                   */
}tTMOv2_ETCB_REC;

/**************************************************
***** Task Table Data Structure Declarations ****** 
***************************************************/
typedef struct TMO_TASK_TABLE{
   tTMOv2_ETCB_REC **a_p_Etcb; /**** Array Of Pointers To Etcbs      ****/
}tTMOv2_TASK_TABLE;

extern tTMOv2_TASK_TABLE    *pTMOv2_TaskTable; /*** Address of Task Table  **/
extern tTMOv2_ETCB_REC      *pTMOv2_Etcb;
extern WDOG_ID               tmoWdID;

/*****************************************************
**** Definitions for semaphore implementation    *****
******************************************************/
#define  TMO_WAIT       0
#define  TMO_NO_WAIT    1
#define  WAIT_FOREVER   2

struct sem_wait_node {
        struct sem_wait_node *p_next;
        struct sem_wait_node *p_prev;
        int priority;
        TCB *p_tcb;
};

typedef struct sem_wait_node SEM_WAIT_NODE;

struct sem_wait_list {
        SEM_WAIT_NODE *p_head;
        SEM_WAIT_NODE *p_tail;
        int count;
};

typedef struct sem_wait_list SEM_WAIT_LIST;


struct semaphore {
        SEM_WAIT_LIST list;
        int           count;
};

typedef struct semaphore  SEMAPHORE;

/*****************************************************
**** Definitions for list manipulation utilities *****
******************************************************/

/*
 * Node structure is for general doubly linked list
 * with priority
 */

struct node {
        struct node *p_next;
        struct node *p_prev;
        int priority;
};

typedef struct node NODE;

/*
 * List structure is for general purpose queue use
 */

struct list {
        struct node *p_head;
        struct node *p_tail;
        int count;
};

typedef struct list LIST;

void  TmoNodeAdd(LIST *, NODE *);
int   TmoNodeDel(LIST *, NODE *);
NODE* TmoNodeDelHead (LIST *p_list);

void                TmoKernInit (int tps, int stups);
void                TmoWdStart (WDOG_ID wd_id, int delay, tTmoTaskMainFunc p_func,
                                int param);
UINT4               TmoWdCancel (WDOG_ID wd_id);
void                TmoStackBasement(INT4);
int *               TmoStackAlloc (int);
void                TmoExit (const char *fmt, char *p_char);
int                 TmoTaskDel (TCB *);
void                TmoReSched (void);
tTMOv2_ETCB_REC *   TMOv2_Get_Etcb (INT4);
void                TmoOs (int);
UINT4               TMOv2_Get_VxId (UINT4);
char *              TmoStrSave (const char *);
void                TmoSched (void);
void                TmoClkInt(void);

NODE *              TmoNodeFind (LIST *p_list, NODE *p_node);
void                TmoNodeAddTail (LIST *p_list, NODE *p_node);
void                TmoNodeAdd (LIST *p_list, NODE *p_node);
NODE *              TmoNodeDelHead (LIST *p_list);

INT4 TmoTaskSuspend (UINT4 u4TaskId);
void TmoListPrint (const char *name, LIST *p_list);
void TmoLogMessage (const char *fmt, int arg1, int arg2, int arg3,
                    int arg4, int arg5, int arg6);

#endif /*** _TMO_H ***/
