/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapsys.h,v 1.12 2015/04/28 12:18:19 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <time.h>
#include <assert.h>

/* ATM-AAL5 Driver support */
/* Used by ISIS/MPLS       */
#ifdef LANAI_WANTED
#include <stdint.h>
#include <linux/atm.h>
#endif
