/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmotsk.c,v 1.20 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * Task management provides the application with the ability to
 * create tasks, delete tasks, and to monitor task scheduling.
 *
 */

#include "tmotypes.h"
#include "tmo.h"

#define PRIVATE static
#define EXPORT

static TCB         *TmoTcbFindById (int tid, TASKQHEAD ** pTaskQ);
static int          TmoTaskNameToId (char *name);
extern UINT4        gu4TMOMaxETCBs;

tTMOv2_ETCB_REC    *pTMOv2_Etcb;

/**********************************************************************
** FUNCTION : Allocate And Initialize The Extended Task Control Block.
***********************************************************************
**  RETURN VALUE :
**  DESCRIPTION  :
**    This function allocates memory for the task control block
**    and initializes the task control block.
**********************************************************************/
PRIVATE INT4
TMO_Init_Etcb (TCB * p_tcb, const INT1 *pi1_TaskName)
{
    UINT4               i;

 /***** Allocate Task Control Block *****/
    pTMOv2_Etcb = TMO_SYS_MALLOC (tTMOv2_ETCB_REC);
    if (pTMOv2_Etcb == NULL)
    {
        return (-1);
    }

    bzero ((char *) pTMOv2_Etcb, sizeof (tTMOv2_ETCB_REC));

 /**** add task control block pointer to task variables ****/
    p_tcb->x = (int) pTMOv2_Etcb;

 /***** Initialize Task Control Block *****/
    memcpy ((void *) (&(pTMOv2_Etcb->u4_DsName)),
            (const void *) pi1_TaskName, sizeof (INT4));

    pTMOv2_Etcb->u4_Vw_Task_Id = (int) p_tcb;
    pTMOv2_Etcb->Sem_Id = (SEM_ID) TMOv2_Create_Semaphore (0);

 /***** Enter TCB Address Into Task Table *****/
    for (i = 1; i < gu4TMOMaxETCBs; i++)
    {
        if (pTMOv2_TaskTable->a_p_Etcb[i] == NULL)
        {
            pTMOv2_TaskTable->a_p_Etcb[i] = pTMOv2_Etcb;
            pTMOv2_Etcb->u4_Tmo_Task_Id = i;
            return (i);
        }
    }
    return (-1);
}

/****************************************************************
**  FUNCTION : Get ETCB
******************************************************************
**  RETURN VALUE : Address of ETCB
**  DESCRIPTION  : This function returns the address of ETCB
**                  for Specified TaskID.
*******************************************************************/
EXPORT tTMOv2_ETCB_REC *
TMOv2_Get_Etcb (i4_TaskId)
     INT4                i4_TaskId;
{
    return ((tTMOv2_ETCB_REC *) pTMOv2_TaskTable->a_p_Etcb[i4_TaskId]);
}

/******************************************************************
**  FUNCTION : Create and activate a task
*******************************************************************
**  RETURN VALUE : i4_Status
**  DESCRIPTION:
**    This function allocates memory for the task control block and
**    spawns the specified task.
*******************************************************************/
EXPORT INT4
TMOv2_Spawn_Task (pi1_TaskName, i4_Priority, u4StackSize,
                  pTaskMainFunc, i4_TaskParam, u4TaskMode)
     const INT1         *pi1_TaskName;
     INT4                i4_Priority;
     UINT4               u4StackSize;
     VOID                (*pTaskMainFunc) (INT4);
     INT4                i4_TaskParam;
     UINT4               u4TaskMode;
{
    INT4                i4_TaskId;
    tTMOv2_ETCB_REC    *pCurrEtcb;
    TCB                *p_tcb;
    int                *stack_top;

    u4TaskMode = 0;
    u4StackSize = ((u4StackSize == 0) ? TMO_STACKSIZE : u4StackSize);

    if ((p_tcb = (TCB *) malloc (sizeof (TCB))) == NULL)
        TmoExit ("TMOv2_SpawnTask:Can't allocate tcb\n", 0);

    memset ((char *) p_tcb, 0, sizeof (TCB));
    p_tcb->priority = i4_Priority;
    p_tcb->taskId = (int) p_tcb;
    p_tcb->name = TmoStrSave ((const char *) pi1_TaskName);
    p_tcb->entry = (tTmoTaskMainFunc) pTaskMainFunc;
    p_tcb->status = TASK_RUN;

    /* Align stack-top to 4 byte boundary */
    p_tcb->original_sp = malloc (u4StackSize + 8);
    if (p_tcb->original_sp == NULL)
    {
        TmoExit ("TMOv2_SpawnTask:Can't allocate stack\n", 0);
    }
    stack_top = (int *) ((char *) p_tcb->original_sp + u4StackSize + 7);

    p_tcb->initial_sp = (int *) ((unsigned long) (stack_top - 1) & 0xfffffffc);
    p_tcb->stack_size = u4StackSize + 8;

    /*
     * TMO's builtin Stack Corruption Detector.
     * Blank the stack.
     * TmoCheckStack detects stack corruption by looking for a non-null word.
     * The roof denotes at any ponit of time, the maximum 'height' attained by
     * a task's stack. checkpoints are placed every so many bytes. TMO emits a
     * informatory note with each passing checkpoint. The choice of checkpoint
     * size is based on experience.
     */
#ifdef TASK_STACK_MONITOR
    memset ((char *) p_tcb->original_sp, '\0', p_tcb->stack_size);
    p_tcb->roof = p_tcb->initial_sp;
    p_tcb->checkpoint = 2000;
#endif

    /* Place the entry point parameter on the stack. */
    *(p_tcb->initial_sp) = i4_TaskParam;
    p_tcb->initial_sp = (p_tcb->initial_sp) - 1;

#ifdef i386
    /*  The actual entry point will be invoked from fn. TmoStackBasement. */
    p_tcb->context[IEIP] = (int) TmoStackBasement;
    p_tcb->context[IEBP] = p_tcb->context[IESP] = (int) p_tcb->initial_sp;
#endif

#ifdef MC68000
    p_tcb->context[IPC] = (int) p_tcb->entry;
    p_tcb->context[IA6] = p_tcb->context[IA7] = (int) p_tcb->initial_sp;
#endif

#ifdef MPCxx
    p_tcb->context[SPR0] = (int) TmoStackBasement;
    p_tcb->context[SPR1] = (int) p_tcb->initial_sp;    /* Stack pointer */
    p_tcb->context[SPR31] = (int) p_tcb->initial_sp;
    /* Assuming that the Task passes only one
     * parameter to the function and Stack GROWS DOWN */
    p_tcb->context[SPR3] = (int) *((p_tcb->initial_sp) + 1);    /* Store Param */
#endif

#ifdef IDT_MIPS2_RC32334
    /* MIPS II Archietecture RC32334 Mips Processor from IDT */
    p_tcb->context[IPC] = (int) p_tcb->entry;
    p_tcb->context[IPARM] = (int) *((p_tcb->initial_sp) + 1);    /* Store the Param */
    p_tcb->context[ISP] = (int) p_tcb->initial_sp;
#endif

    TmoNodeAdd ((LIST *) & readyq, (NODE *) p_tcb);

 /***** Initialize The Task Control Block *****/
    pCurrEtcb = pTMOv2_Etcb;
    i4_TaskId = TMO_Init_Etcb (p_tcb, pi1_TaskName);
    pTMOv2_Etcb = pCurrEtcb;
    return i4_TaskId;
}

/********************************************************************
**  FUNCTION : Delete A Specified Task.
*********************************************************************
**  RETURN VALUE :
**  DESCRIPTION  : This Function, Frees All The Task's
**                 Resources, And Deletes The Task.
********************************************************************/
EXPORT INT4
TMOv2_Delete_Task (i4_TaskId)
     INT4                i4_TaskId;
{
    INT4                i;
    tTMOv2_ETCB_REC    *pTcb;
    UINT4               u4_Tmo_Task_Id;

    pTcb = TMOv2_Get_Etcb (i4_TaskId);
    if (pTcb)
    {
        TMOv2_Delete_Semaphore (pTcb->Sem_Id);
    /*************************************************************
    *** Save the Vx960 id before freeing the task entry so we ****
    *** can delete the task afterwards.                       ****
    **************************************************************/
        i = pTcb->u4_Vw_Task_Id;
        u4_Tmo_Task_Id = pTcb->u4_Tmo_Task_Id;
        TMO_SYS_FREE ((VOID *) pTMOv2_TaskTable->a_p_Etcb[u4_Tmo_Task_Id]);
        pTMOv2_TaskTable->a_p_Etcb[u4_Tmo_Task_Id] = NULL;

        if (pTcb == pTMOv2_Etcb)
            TmoTaskDel (p_curtcb);
        else
            TmoTaskDel ((TCB *) i);

        return TMO_OK;
    }
    else
        return (-1);
}

/***************************************************************
**  FUNCTION: get task id of the current task
*****************************************************************
**  RETURN VALUE:
**
**  DESCRIPTION:
**    This function determines the task id of current task.
**
******************************************************************/
EXPORT INT4
TMOv2_Get_TmoId (void)
{
    return (pTMOv2_Etcb->u4_Tmo_Task_Id);
}

UINT4
TMOv2_Get_VxId (u4_TmoId)
     UINT4               u4_TmoId;
{
    tTMOv2_ETCB_REC    *pEtcb;

    if (u4_TmoId == 0)
        u4_TmoId = TMOv2_Get_TmoId ();
    pEtcb = TMOv2_Get_Etcb (u4_TmoId);
    return (pEtcb->u4_Vw_Task_Id);
}

/****************************************************************
**  FUNCTION : Examine Priority Of A Task
****************************************************************
**  RETURN VALUE :
**  DESCRIPTION  :
**    This function is used to determine the current priority
**    of a spedified task.
*******************************************************************/
EXPORT INT4
TMOv2_Set_Task_Priority (i4TaskId, i4NewPriority)
     INT4                i4TaskId;
     INT4                i4NewPriority;
{
    TCB                *p_tcb;
    TASKQHEAD          *p_tq;

    if (p_curtcb == (TCB *) i4TaskId)
    {
        p_curtcb->priority = i4NewPriority;
        TmoSched ();
        return OK;
    }
    if ((p_tcb = TmoTcbFindById (i4TaskId, &p_tq)) == NULL)
        return (ERROR);
    TmoNodeDel ((LIST *) p_tq, (NODE *) p_tcb);
    p_tcb->priority = i4NewPriority;
    TmoNodeAdd ((LIST *) p_tq, (NODE *) p_tcb);
    TmoSched ();
    return OK;
}

/******************************************************************
**  FUNCTION : Change priority of a task
*******************************************************************
**  RETURN VALUE:
**  DESCRIPTION:
**    This function changes the specified task's priority to
**    the specified priority.
********************************************************************/
EXPORT INT4
TMOv2_Get_Task_Priority (i4TaskId, pi4Priority)
     INT4                i4TaskId;
     INT4               *pi4Priority;
{
    TCB                *p_tcb;
    TASKQHEAD          *p_tq;

    if (p_curtcb == (TCB *) i4TaskId)
    {
        *pi4Priority = p_curtcb->priority;
        return (OK);
    }
    if ((p_tcb = TmoTcbFindById (i4TaskId, &p_tq)) == NULL)
        return (ERROR);
    *pi4Priority = p_tcb->priority;
    return (OK);
}

/********************************************************************
**  FUNCTION : TMOv2_Get_Task_Name
*********************************************************************
**  RETURN VALUE:
**  Pointer to the name of the requested task.
**  DESCRIPTION:
**    TMOv2_Get_Task_Name() returns the name of the task identified
**    by the given OSS task id.
*********************************************************************/
EXPORT INT1        *
TMOv2_Get_Task_Name (i4_TaskId)    /* task to get name of */
     INT4                i4_TaskId;
{
    tTMOv2_ETCB_REC    *pEtcb;    /* extended task control block */
    TCB                *p_tcb;
    TASKQHEAD          *p_tq;

    if ((pEtcb = TMOv2_Get_Etcb (i4_TaskId)) == NULL)
    {
        return NULL;
    }
    if ((p_tcb = TmoTcbFindById (pEtcb->u4_Vw_Task_Id, &p_tq)) == NULL)
        return NULL;
    return (INT1 *) (p_tcb->name);
}

/***************************************************************
**  FUNCTION : TMOv2_Get_Task_ID
****************************************************************
**  RETURN VALUE :
**         The OSS Task Id Is Returned Or 0 If No Task Could Be
**         Found With The Given Name.
**  DESCRIPTION :
**         TMOv2_Get_Task_ID() Returns The OSS Task Id Given The
**         Name Of The Task.
***************************************************************/
EXPORT INT4
TMOv2_Get_Task_ID (pi1_TaskName)    /* name of task */
     INT1               *pi1_TaskName;
{
    int                 i4_VxId;
    /*** Vx960 Task ID              ***/

    UINT4               i;
   /*** Current Entry In Task Table ***/

    tTMOv2_ETCB_REC   **p_pEtcbPtr;
    /*** Extended Task Control Block ***/

 /**************************************************
 **** Get The Vx960 Task Id For The Given Task. ****
 ***************************************************/
    if ((i4_VxId = TmoTaskNameToId ((char *) pi1_TaskName)) == TMO_ERROR)
    {
        return 0;
    }
 /*********************************************
 **** Search The Task Table For The Task. *****
 **********************************************/
    for (i = 1, p_pEtcbPtr = &pTMOv2_TaskTable->a_p_Etcb[1];
         i < gu4TMOMaxETCBs; i++, p_pEtcbPtr++)
    {
        if (*p_pEtcbPtr != NULL &&
            (*p_pEtcbPtr)->u4_Vw_Task_Id == (UINT4) i4_VxId)
        {
            return (*p_pEtcbPtr)->u4_Tmo_Task_Id;
        }
    }
    return 0;
            /***** Couldn't Find Task In Table *****/
}

INT4
TMOv2_Suspend_Task (UINT4 u4TaskId)
{
    return (TmoTaskSuspend (TMOv2_Get_VxId (u4TaskId)));
}

INT4
TmoTaskSuspend (UINT4 u4TaskId)
{
    TCB                *p_tcb;

    p_tcb = (TCB *) u4TaskId;

    if (p_tcb == NULL || p_tcb == p_curtcb)
    {
        p_curtcb->status &= ~TASK_RUN;
        p_curtcb->status |= TASK_SLEEP;
        TmoSched ();
    }
    else
    {
        if (TmoNodeFind ((LIST *) & runq, (NODE *) p_tcb))
            TmoNodeDel ((LIST *) & runq, (NODE *) p_tcb);
        else if (TmoNodeFind ((LIST *) & readyq, (NODE *) p_tcb))
            TmoNodeDel ((LIST *) & readyq, (NODE *) p_tcb);
        else
            return ERROR;
        p_tcb->status &= ~TASK_RUN;
        p_tcb->status |= TASK_SLEEP;
        TmoNodeAdd ((LIST *) & sleepq, (NODE *) p_tcb);
    }
    return (OK);
}

INT4
TMOv2_Resume_Task (UINT4 u4TaskId)
{
    TCB                *p_tcb;
    p_tcb = (TCB *) (TMOv2_Get_VxId (u4TaskId));
    if (p_tcb == NULL)
        return ERROR;
    if (TmoNodeFind ((LIST *) & sleepq, (NODE *) p_tcb))
        TmoNodeDel ((LIST *) & sleepq, (NODE *) p_tcb);
    else
        return ERROR;

    p_tcb->status &= ~TASK_SLEEP;
    p_tcb->status |= TASK_RUN;

    TmoNodeAdd ((LIST *) & readyq, (NODE *) p_tcb);
    return OK;
}

UINT4
TMOv2_Get_Cur_TaskId (void)
{
    return (pTMOv2_Etcb->u4_Tmo_Task_Id);
}

int
TmoTaskNameToId (char *name)
{
    TCB                *p_tcb;

    if (strcmp (p_curtcb->name, name) == 0)
        return (p_curtcb->taskId);

    for (p_tcb = sleepq.p_head; p_tcb;)
    {
        if (strcmp (p_tcb->name, name) == 0)
            return (p_tcb->taskId);
        p_tcb = p_tcb->p_next;
    }
    for (p_tcb = runq.p_head; p_tcb;)
    {
        if (strcmp (p_tcb->name, name) == 0)
            return (p_tcb->taskId);
        p_tcb = p_tcb->p_next;
    }
    for (p_tcb = readyq.p_head; p_tcb;)
    {
        if (strcmp (p_tcb->name, name) == 0)
            return (p_tcb->taskId);
        p_tcb = p_tcb->p_next;
    }
    return (0);
}

int
TmoTaskDel (p_tcb)
     TCB                *p_tcb;
{

    if (p_tcb == p_curtcb)
    {
        p_tcb->status |= TASK_DELETED;
    }
    else
    {
        if (TmoNodeFind ((LIST *) & runq, (NODE *) p_tcb))
            TmoNodeDel ((LIST *) & runq, (NODE *) p_tcb);
        else if (TmoNodeFind ((LIST *) & readyq, (NODE *) p_tcb))
            TmoNodeDel ((LIST *) & readyq, (NODE *) p_tcb);
        else if (TmoNodeFind ((LIST *) & sleepq, (NODE *) p_tcb))
            TmoNodeDel ((LIST *) & sleepq, (NODE *) p_tcb);
        else
        {
            TmoLogMessage ("TmoTaskDel:Task ID 0x%08lx doesn't exsit\n",
                           (int) p_tcb, 0, 0, 0, 0, 0);
            return (ERROR);
        }
        free (p_tcb->original_sp);
        free ((char *) p_tcb->name);
        free ((char *) p_tcb);
    }

    TmoSched ();
    return (OK);
}

static TCB         *
TmoTcbFindById (tid, p_tq)
     int                 tid;
     TASKQHEAD         **p_tq;
{
    TCB                *p_tcb;

    for (p_tcb = sleepq.p_head; p_tcb; p_tcb = p_tcb->p_next)
    {
        if (p_tcb->taskId == tid)
        {
            *p_tq = &sleepq;
            return (p_tcb);
        }
    }
    for (p_tcb = readyq.p_head; p_tcb; p_tcb = p_tcb->p_next)
    {
        if (p_tcb->taskId == tid)
        {
            *p_tq = &readyq;
            return (p_tcb);
        }
    }
    for (p_tcb = runq.p_head; p_tcb; p_tcb = p_tcb->p_next)
    {
        if (p_tcb->taskId == tid)
        {
            *p_tq = &runq;
            return (p_tcb);
        }
    }
    *p_tq = NULL;
    return (NULL);
}

/* This function prevents a sigsegv, when a entrypoint fn.
 * "simply" returns. */
void
TmoStackBasement (INT4 taskParam)
{
    (p_curtcb->entry) (taskParam);
    /* cst */
    TmoTaskSuspend ((UINT4) p_curtcb);
}

EXPORT TCB         *
TMOv2_Get_tcb (INT4 i4_TaskId)
{
    return ((TCB
             *) (((tTMOv2_ETCB_REC *) pTMOv2_TaskTable->a_p_Etcb[i4_TaskId])->
                 u4_Vw_Task_Id));
}
