/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osxport.h,v 1.14 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * Contains portable definitions for OSIX.
 *
 */

#ifndef OSIXNAMES_H
#define OSIXNAMES_H

/************************************************************************
*                                                                       *
*                         Tasks                                         *
*                                                                       *
*************************************************************************/
#if MULTI_NODE_SUPPORT==FSAP_ON
static const UINT1* OSIX_IPC_SEND_TASK=(UINT1 *)"IPCS";
static const UINT1* OSIX_IPC_RECV_TASK=(UINT1 *)"IPCR";

/* Add application task names here onwards.
*/
static const UINT1* OSIX_APP_TASK1=(UINT1 *)"0006";
static const UINT1* OSIX_APP_TASK2=(UINT1 *)"0007";
static const UINT1* OSIX_APP_TASK3=(UINT1 *)"0008";
static const UINT1* OSIX_APP_TASK4=(UINT1 *)"0009";
static const UINT1* OSIX_APP_TASK5=(UINT1 *)"0010";
#endif

/************************************************************************
*                                                                       *
*                         Queues                                        *
*                                                                       *
*************************************************************************/
#if MULTI_NODE_SUPPORT==FSAP_ON
static const UINT1* IPSQ_NAME=(UINT1 *)"IPSQ";
#endif

/* Add application task names here onwards. 
*/

/************************************************************************
*                                                                       *
*                         Semaphores                                    *
*                                                                       *
*************************************************************************/
#define OSIX_MUTEX_NAME       (const UINT1 *)"OSMU"
#define TMR_MUTEX_NAME        (const UINT1 *)"TMMU"

#define OSIX_GET_UNIQ_SEM() OsixGetNextSem()


#endif
