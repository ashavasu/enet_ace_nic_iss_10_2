
#include "tmotypes.h"
#include "tmotypdfs.h"
#include "tmo.h"

/* Creates a queue using a linear block of memory. */
/* Returns NULL if creation fails.                 */
tTmoQId
TMOv2_Create_MsgQ (UINT4 u4MaxMsgs, UINT4 u4MsgLen)
{
    SEM_ID              qSem;
    tTmoQ              *pTmoQ;

    /* Allocate memory for holding messages. Create a semaphore for      */
    /* protection between multiple simultaneous calls to write or read   */
    /* Initialize the read and write pointers to the Q start location    */
    /* Initia the pointer marking the end of the queue's memory location */
    pTmoQ = (tTmoQ *) malloc (((u4MaxMsgs + 1) * u4MsgLen) + sizeof (tTmoQ));
    if (pTmoQ == NULL)
    {
        return (NULL);
    }
    pTmoQ->pQBase = (UINT1 *) ((UINT1 *) pTmoQ + sizeof (tTmoQ));
    if ((qSem = TMOv2_Create_Semaphore (0)) == 0)
    {
        free (pTmoQ);
        return (NULL);
    }
    pTmoQ->MSem = qSem;

    if ((qSem = TMOv2_Create_Semaphore (0)) == 0)
    {
        free (pTmoQ);
        TMOv2_Delete_Semaphore (pTmoQ->MSem);
        return (NULL);
    }
    pTmoQ->BSem = qSem;

    pTmoQ->pQEnd = (pTmoQ->pQBase) + ((u4MaxMsgs + 1) * u4MsgLen);
    pTmoQ->pQRead = pTmoQ->pQBase;
    pTmoQ->pQWrite = pTmoQ->pQBase;
    pTmoQ->u4MsgLen = u4MsgLen;
    pTmoQ->u4OverFlows = 0;

    TMOv2_Give_Semaphore (pTmoQ->MSem);    /* 1st write can take semaphore now */
    return (pTmoQ);
}

VOID
TMOv2_Delete_MsgQ (tTmoQId QId)
{
    tTmoQ              *pTmoQ = (tTmoQ *) QId;
    /* Wait for semaphore to ensure that when the queue is deleted */
    /* no one is reading from or writing into it. Then delete the  */
    /* semaphore, free the queue memory and initialize queue start */
    if (TMOv2_Take_Semaphore (pTmoQ->MSem, WAIT, 0) != TMO_OK)
    {
        return;
    }
    TMOv2_Delete_Semaphore (pTmoQ->MSem);
    TMOv2_Delete_Semaphore (pTmoQ->BSem);
    free ((VOID *) pTmoQ);
}

INT4
TMOv2_Send_MsgQ (tTmoQId QId, UINT1 *pMsg)
{
    tTmoQ              *pTmoQ = (tTmoQ *) QId;
    UINT1              *pWrite, *pRead, *pBase, *pEnd;
    UINT4               u4MsgLen;

    /* Ensure mutual exclusion. Wait and take the mutual exclusion        */
    /* semaphore. A write is possible if the queue is not full. Queue is  */
    /* recognized as full if by writing one more message, write and read  */
    /* pointers become equal. Actually, this means that the queue holds   */
    /* only u4MaxMsgs-1 messages to be safe. When checking the pointers   */
    /* or when advancing the write pointer after the write operation,     */
    /* take care of the wrap-around since this is a circular queue. When  */
    /* the message is written, advance the write pointer by u4MsgLen.     */
    if (TMOv2_Take_Semaphore (pTmoQ->MSem, WAIT, 0) != TMO_OK)
    {
        return (TMO_NOT_OK);
    }

    pWrite = pTmoQ->pQWrite;
    pRead = pTmoQ->pQRead;
    pBase = pTmoQ->pQBase;
    pEnd = pTmoQ->pQEnd;
    u4MsgLen = pTmoQ->u4MsgLen;

    if (((pWrite + u4MsgLen) == pEnd) && (pRead == pBase))
    {
        TMOv2_Give_Semaphore (pTmoQ->MSem);
        pTmoQ->u4OverFlows++;
        return (TMO_NOT_OK);
    }
    if ((pWrite + u4MsgLen) == pRead)
    {
        TMOv2_Give_Semaphore (pTmoQ->MSem);
        pTmoQ->u4OverFlows++;
        return (TMO_NOT_OK);
    }
    memcpy (pWrite, pMsg, u4MsgLen);
    (pTmoQ->pQWrite) += u4MsgLen;

    if ((pTmoQ->pQWrite) == pEnd)
    {
        (pTmoQ->pQWrite) = pBase;
    }
    TMOv2_Give_Semaphore (pTmoQ->BSem);    /* unblock anyone waiting to read    */
    TMOv2_Give_Semaphore (pTmoQ->MSem);    /* allow others to read/write/delete */
    return (TMO_OK);
}

INT4
TMOv2_Receive_MsgQ (tTmoQId QId, UINT1 *pMsg, INT4 i4Timeout)
{
    tTmoQ              *pTmoQ = (tTmoQ *) QId;

    /* Only FM task/thread reads from the queue. Multiple other tasks may */
    /* write. Deletion of the queue is also done only by the FM task. So  */
    /* a semaphore for mutual exclusion is needed for writing but not for */
    /* reading. Only a blocking semaphore to wait for a message is needed */
    /* if the queue is empty.                                             */

    /* Check if queue exists. If yes, wait and take the mutual exclusion  */
    /* semaphore. A read is possible if the queue is not empty. Queue is  */
    /* recognized as empty if write and read pointers are equal i.e. all  */
    /* the written messages have already been read.                       */
    if (TMOv2_Take_Semaphore (pTmoQ->MSem, WAIT, 0) != TMO_OK)
    {
        return (TMO_NOT_OK);
    }

    /* If i4Timeout is 0, it is NO_WAIT. */
    if (!i4Timeout && (pTmoQ->pQWrite) == (pTmoQ->pQRead))
    {
        TMOv2_Give_Semaphore (pTmoQ->MSem);
        return TMO_NOT_OK;
    }
    while ((pTmoQ->pQWrite) == (pTmoQ->pQRead))
    {
        /* Queue is empty. Block on the semaphore. When a write is done */
        /* the writer will release the semaphore and unblock this wait. */
        /* Before blocking, allow someone else to write into queue.     */
        TMOv2_Give_Semaphore (pTmoQ->MSem);
        if (TMOv2_Take_Semaphore (pTmoQ->BSem, WAIT, 0) != TMO_OK)
        {
            return (TMO_NOT_OK);
        }
        if (TMOv2_Take_Semaphore (pTmoQ->MSem, WAIT, i4Timeout) != TMO_OK)
        {
            return (TMO_NOT_OK);
        }
    }

    /* There is at least 1 message in the queue and we have locked the */
    /* mutual exclusion semaphore so nobody else changes the state.    */
    memcpy (pMsg, pTmoQ->pQRead, pTmoQ->u4MsgLen);
    (pTmoQ->pQRead) += (pTmoQ->u4MsgLen);
    if ((pTmoQ->pQRead) == (pTmoQ->pQEnd))
    {
        (pTmoQ->pQRead) = (pTmoQ->pQBase);
    }
    TMOv2_Give_Semaphore (pTmoQ->MSem);
    return (TMO_OK);
}

UINT4
TMOv2_MsgQ_NumMsgs (tTmoQId QId)
{
    tTmoQ               TmoQ = *((tTmoQ *) QId);
    UINT4               u4Msgs;

    if ((TmoQ.pQWrite) < (TmoQ.pQRead))
    {
        u4Msgs = (TmoQ.pQWrite) - (TmoQ.pQBase) + (TmoQ.pQEnd) - (TmoQ.pQRead);
        return (u4Msgs / (TmoQ.u4MsgLen));
    }
    else
    {
        return (((TmoQ.pQWrite) - (TmoQ.pQRead)) / (TmoQ.u4MsgLen));
    }
}
