/****************************************************************
**  FUNCTION : TMO Utilities
***************************************************************** 
**
**  FILE NAME: tmoutil.c
**  DESCRIPTION:
**     Utility routines used by other TMO code
**
**   DATE         NAME       REFERENCE      REASON
**   -----------  ----       ---------      ------
**   31-Jan-2002  FSPL       FSRouter       Create
** 
**  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*******************************************************************/
/************************************************************************/
/*                                                                      */
/*  CHANGE RECORD:                                                      */
/*  Version      Author      Date          Description of change        */
/*   1.0         rajeshs     31-JAN-2002   original creation           */
/************************************************************************/

#include "tmo.h"

void
TmoLogMessage (const char *fmt, int arg1, int arg2, int arg3,
               int arg4, int arg5, int arg6)
{
    if (p_curtcb == NULL)
        printf ("**** micro kernel ****: Vx960 Emulator haven't started\n");
    else
        printf ("%s: ", p_curtcb->name);
    printf (fmt, arg1, arg2, arg3, arg4, arg5, arg6);
    fflush (stdout);
}

void
TmoListPrint (const char *name, LIST * p_list)
{
    NODE               *p_node;

    printf ("List Name:%s p_head=%p p_tail=%p count=%d\n",
            name, (void *) p_list->p_head, (void *) p_list->p_tail,
            p_list->count);
    for (p_node = p_list->p_head; p_node; p_node = p_node->p_next)
        printf ("%p %p %p %d\n",
                (void *) p_node, (void *) p_node->p_next,
                (void *) p_node->p_prev, p_node->priority);
}

int
TmoNodeDel (p_list, p_node)
     LIST               *p_list;
     NODE               *p_node;
{
    NODE               *p_tmp;

    if (p_list == NULL || p_node == NULL)
        TmoExit ("TmoNodeDel: p_list/p_node is NULL\n", 0);

    for (p_tmp = p_list->p_head; p_tmp; p_tmp = p_tmp->p_next)
    {
        if (p_tmp == p_node)
            break;
    }
    if (p_tmp == NULL)
    {
        fprintf (stderr,
                 "TmoNodeDel: can't find p_node=%p in p_list=%p\n",
                 (void *) p_node, (void *) p_list);
        return -1;
    }
    if (p_tmp->p_next == NULL)
    {                            /* @tail */
        if (p_tmp->p_prev == NULL)
        {                        /* @head */
            p_list->p_head = p_list->p_tail = NULL;
            if (p_list->count != 1)
            {
                TmoListPrint ("xxx", p_list);
                TmoExit ("TmoNodeDel: p_list=0x%08lx has wrong structure\n",
                         (char *) p_list);
            }
        }
        else
        {
            p_list->p_tail = p_tmp->p_prev;
            p_tmp->p_prev->p_next = NULL;
        }
    }
    else
    {
        if (p_tmp->p_prev == NULL)
        {                        /* @head */
            p_list->p_head = p_tmp->p_next;
            p_tmp->p_next->p_prev = NULL;
        }
        else
        {                        /* neither @head nor @tail */
            p_tmp->p_prev->p_next = p_tmp->p_next;
            p_tmp->p_next->p_prev = p_tmp->p_prev;
        }
    }
    p_tmp->p_next = p_tmp->p_prev = NULL;
    p_list->count--;

    return (0);
}

NODE               *
TmoNodeFind (LIST * p_list, NODE * p_node)
{
    NODE               *p_tmp;

    if (p_list == NULL || p_node == NULL)
        TmoExit ("TmoNodeFind: p_list/p_node is NULL\n", 0);

    for (p_tmp = p_list->p_head; p_tmp; p_tmp = p_tmp->p_next)
    {
        if (p_tmp == p_node)
            break;
    }
    return (p_tmp);
}

void
TmoNodeAddTail (LIST * p_list, NODE * p_node)
{
    if (p_list == NULL || p_node == NULL)
        TmoExit ("TmoNodeAddTail: p_list/p_node is NULL\n", 0);

    if (p_list->p_tail != NULL)
    {
        p_list->p_tail->p_next = p_node;
        p_node->p_next = NULL;
        p_node->p_prev = p_list->p_tail;
        p_list->p_tail = p_node;
        p_list->count++;
    }
    else
    {
        if (p_list->p_head != NULL)
        {
            TmoListPrint ("TmoNodeAddTail: ", p_list);
        }
        p_list->p_head = p_list->p_tail = p_node;
        p_node->p_next = p_node->p_prev = NULL;
        p_list->count = 1;
    }
}

void
TmoNodeAdd (p_list, p_node)
     LIST               *p_list;
     NODE               *p_node;
{
    NODE               *p_tmp;
    int                 priority;

    if (p_list == NULL || p_node == NULL)
        TmoExit ("TmoNodeAdd: p_list/p_node is NULL\n", 0);

    priority = p_node->priority;

    for (p_tmp = p_list->p_head; p_tmp; p_tmp = p_tmp->p_next)
    {
        if (p_tmp == p_node)
        {
            TmoListPrint ("TmoNodeAdd:", p_list);
            TmoExit ("TmoNodeAdd: 0x%08lx exists\n", (char *) p_tmp);
        }
        if (p_tmp->priority > priority)
            break;
    }
    if (p_tmp == NULL)
        TmoNodeAddTail (p_list, p_node);
    else
    {
        if (p_tmp->p_prev != NULL)
        {
            p_tmp->p_prev->p_next = p_node;
            p_node->p_prev = p_tmp->p_prev;
            p_node->p_next = p_tmp;
            p_tmp->p_prev = p_node;
        }
        else
        {
            p_node->p_next = p_tmp;
            p_tmp->p_prev = p_node;
            p_node->p_prev = NULL;
            p_list->p_head = p_node;
        }
        p_list->count++;
    }
}

NODE               *
TmoNodeDelHead (p_list)
     LIST               *p_list;
{
    NODE               *p_tmp;

    if (p_list == NULL)
        TmoExit ("TmoNodeDelHead: p_list is NULL\n", 0);

    if (p_list->p_head == NULL)
        return NULL;

    p_tmp = p_list->p_head;
    p_list->p_head = p_tmp->p_next;
    if (p_list->p_head != NULL)
        p_list->p_head->p_prev = NULL;
    p_tmp->p_next = p_tmp->p_prev = NULL;
    if (--p_list->count == 0)
        p_list->p_tail = NULL;
    return (p_tmp);
}

void
TmoExit (fmt, p_char)
     const char         *fmt;
     char               *p_char;
{
    TCB                *p_tcb;

    fprintf (stderr, fmt, p_char);
    scheduler_disabled = 1;
    printf ("\n\nKernel internal data structure dump:\n");

    printf ("runq:\n");
    for (p_tcb = runq.p_head; p_tcb; p_tcb = p_tcb->p_next)
        printf ("%s: nswitch=%d priority=%d count=%d\n",
                p_tcb->name, p_tcb->nswitch, p_tcb->priority, p_tcb->count);

    printf ("%s: nswitch=%d priority=%d count=%d\n",
            p_curtcb->name, p_curtcb->nswitch, p_curtcb->priority,
            p_curtcb->count);

    printf ("readyq:\n");
    for (p_tcb = readyq.p_head; p_tcb; p_tcb = p_tcb->p_next)
        printf ("%s: nswitch=%d priority=%d count=%d\n",
                p_tcb->name, p_tcb->nswitch, p_tcb->priority, p_tcb->count);

    printf ("sleepq:\n");
    for (p_tcb = sleepq.p_head; p_tcb; p_tcb = p_tcb->p_next)
        printf ("%s: nswitch=%d priority=%d count=%d\n",
                p_tcb->name, p_tcb->nswitch, p_tcb->priority, p_tcb->count);

    exit (0);
}

char               *
TmoStrSave (const char *s)
{
    char               *p;

    if ((p = malloc (strlen (s) + 1)) == NULL)
        TmoExit ("TmoStrSave:Can't allocate memory\n", 0);
    strcpy (p, s);
    return (p);
}

#ifdef STACK_CORRUPTION_DETECTION
extern void         PrintStack ();
void
ShowStack (TCB * ptcb)
{
    printf ("\n%s:", ptcb->name);
    PrintStack (ptcb->context[4], (unsigned int) (ptcb->initial_sp));
}

void
TmoTaskStacksShow ()
{
    TCB                *pTcb;

    ShowStack (p_curtcb);

    for (pTcb = sleepq.p_head; pTcb;)
    {
        printf ("\n%s:", pTcb->name);
        PrintStack (pTcb->context[4], (unsigned int) (pTcb->initial_sp));
        pTcb = pTcb->p_next;
    }
    for (pTcb = runq.p_head; pTcb;)
    {
        printf ("\n%s:", pTcb->name);
        PrintStack (pTcb->context[4], (unsigned int) (pTcb->initial_sp));
        pTcb = pTcb->p_next;
    }
    for (pTcb = readyq.p_head; pTcb;)
    {
        printf ("\n%s:", pTcb->name);
        PrintStack (pTcb->context[4], (unsigned int) (pTcb->initial_sp));
        pTcb = pTcb->p_next;
    }
    signal (SIGUSR1, TmoTaskStacksShow);
    return;
}
#endif
