	.file	"tmo_ppc.s"
	.section	".text"
	.align 2
	.globl TmoSched
	.type TmoSched,@function

TmoSched:                         # |-------- PROLOGUE SEGMENT --------------|
TmoCtxtSave:                      # |  A prologue to save the stack and      |
        stwu 1,-32(1)             # |   previous Link register contents      |
        mflr 0                    # |=> Get the previous link register       |
        stw 31,28(1)              # |=> Save the frame ptr                   |
        stw 0,36(1)               # |=> Save the Link Register (ret address) | 
        mr 31,1                   # |=> (frame ptr)= stack pointer           |
                                  # |----------------------------------------|
        lis 9,p_curtcb@ha         #   Retrieve the address of p_curtcb     
        lwz 11,p_curtcb@l(9)      #     and store in register 11 

        lis 9, pTMOv2_Etcb@ha     #   Assign the content of 12 byte of 
        lwz 12, 12(11)            #    p_curtcb to pTMOv2_Etcb
        stw 12, pTMOv2_Etcb@l(9)  #

        stmw 0, 16(11)            # =>  Store all the GPR register contents  
                                  #        in p_curtcb->context[]

        mfcr 5                    # |  store the condition register
        stw 5, (16+(32*4))(11)    # |    in p_curtcb->context[32]
	
        bl TmoReSched



TmoCtxtSwitch:
        
        lis 3,p_curtcb@ha         # Get the address of p_curtcb pointer
        lwz 4,p_curtcb@l(3)       # and store in the 11 register
        
        lis 5, pTMOv2_Etcb@ha     #   Assign the content of 12 byte of
        lwz 12, 12(4)             #    p_curtcb to pTMOv2_Etcb
        stw 12, pTMOv2_Etcb@l(5)  #

        lwz 0, 16(4)              # | 
        lwz 1, 16+4(4)            # | 
        lwz 2, 16+8(4)            # |     Restore back the 
        lwz 3, 16+12(4)           # |    contents of all GPR registers
        lwz 5, 16+20(4)           # | 
        lwz 6, 16+24(4)           # | 
        lmw 7, 16+28(4)           # | 

        lwz 5,(16+(32*4))(4)      # Get the condition Register 
        mtcrf 0xff, 5             # Restore back the condition register

                              #------------ EPILOGUE SEGMENT------------| 
        lwz 31, 28(31)        #=> restore back the stack frame pointer  |
        lwz 0, 16(4)          #=> get the content of the link register  |
        mtlr 0                #=> Restore the Link Register             |
        addi 1,1,32           #=> Remove the stack frame                |
        blr                   #--------------- GO HOME -----------------|
        

