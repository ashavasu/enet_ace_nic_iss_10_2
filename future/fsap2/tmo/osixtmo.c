/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osixtmo.c,v 1.22 2015/04/28 12:18:19 siva Exp $
 *
 * Description: Contains OSIX reference code for TMO.
 *              All basic OS facilities used by protocol software
 *              from FS, use only these APIs.
 */

#include "osxinc.h"
#include "tmotypdfs.h"
#include "osix.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "fsapcli.h"

/* The basic structure maintaining the name-to-id mapping         */
/* of OSIX resources. 3 arrays - one for tasks, one for           */
/* semaphores and one for queues are maintained by OSIX.          */
/* Each array has this structure as the basic element. We         */
/* use this array to store events for tasks also.                 */

/* The structure has the following elements:                      */
/*   u4RscId  - the id returned by the OS                         */
/*   u2Free   - whether this structure is free or used            */
/*   u2TskBlk - for event simulation; is task is blocked on event */
/*   u4Events - for event simulation; used only for tasks         */
/*   au1Name  - name is always multiple of 4 characters in length */
typedef struct OsixRscTskStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2TskBlk;
    UINT4               u4Events;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixTsk;
typedef struct OsixRscQueStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;    /* field introduced for alignment */
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixQue;
typedef struct OsixRscSemStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;    /* field introduced for alignment */
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixSem;

UINT4               gu4Tps = OSIX_TPS;
UINT4               gu4Stups = OSIX_STUPS;

/* 1st element = tasks; 2nd = semaphores; 3rd = queues */
tOsixTsk            gaOsixTsk[OSIX_MAX_TSKS + 2];
/* 0th element is invalid and not used */

tOsixSem            gaOsixSem[OSIX_MAX_SEMS + 1];
/* 0th element is invalid and not used */

tOsixQue            gaOsixQue[OSIX_MAX_QUES + 1];
/* 0th element is invalid and not used */

tOsixSemId          gOsixMutex = (tOsixSemId) OSIX_RSC_INV;

UINT4               gu4OsixTrc;
#define OSIX_TRC_FLAG gu4OsixTrc

static UINT4 OsixRscAdd ARG_LIST ((UINT1[4], UINT4, UINT4));
static VOID OsixRscDel ARG_LIST ((UINT4, UINT4));
static UINT4 FsapShowQueData ARG_LIST ((tTmoQ * pTmoQ, UINT1 *pu1Result));

extern UINT4        OsixSTUPS2Ticks (UINT4);
extern UINT4        OsixTicks2STUPS (UINT4);
extern UINT4        gu4Seconds;

/********************************************************/
/* Routines for task creation, deletion and maintenance */
/********************************************************/
/************************************************************************
 *  Function Name   : OsixTskCrt
 *  Description     : OSIX API used by applications to create a task or 
 *                    process.
 *  Input           : pu1TskName   - task name - pointer to char string.
 *                    u4TskPrio    - task priority. lower no. is higher.
 *                    u4StackSize  - Stack size in bytes.
 *                    TskStartAddr - Entry point function
 *                    ai1TskStartArgs - a single argument to entry fn
 *  Output          : pTskId - Pointer to memory location in which the
 *                               id of the created task in returned
 *  Returns         : OSIX_FAILURE / OSIX_SUCCESS
 ************************************************************************/

UINT4
OsixTskCrt (UINT1 au1TskName[], UINT4 u4TskPrio, UINT4 u4StackSize,
            VOID (*TskStartAddr) (INT1 *), INT1 *u4Arg, tOsixTaskId * pTskId)
{
    INT4                i4OsTskId;
    UINT4               u4OsPrio;
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1TskName, OSIX_NAME_LEN);

    u4TskPrio = u4TskPrio & 0x0000ffff;

    /* Remap the task priority to TMO's range of values. */
    u4OsPrio = OS_LOW_PRIO + (((((INT4) (u4TskPrio) - FSAP_LOW_PRIO) *
                                (OS_HIGH_PRIO -
                                 OS_LOW_PRIO)) / (FSAP_HIGH_PRIO -
                                                  FSAP_LOW_PRIO)));
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixTskCrt (%s, %ld, %ld)\r\n",
               au1Name, u4TskPrio, u4StackSize);

    /* Create task and  and it to the name to id mapping linked list. */
    i4OsTskId =
        TMOv2_Spawn_Task ((INT1 *) au1Name, (INT4) u4OsPrio, u4StackSize,
                          TskStartAddr, (INT4) u4Arg, 0);
    if (i4OsTskId <= 0)
    {
        return OSIX_FAILURE;
    }
    if (OsixRscAdd (au1Name, OSIX_TSK, (UINT4) i4OsTskId) == OSIX_FAILURE)
    {
        TMOv2_Delete_Task (i4OsTskId);
        return (OSIX_FAILURE);
    }

    /* Get the global task array index to return as the task id. */
    /* Return value check not needed because we just created it. */
    OsixRscFind (au1Name, OSIX_TSK, pTskId);
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixTskDel
 *  Description     : This deletes the specified task.
 *  Input           : TskId - ID of task to be deleted.
 *  Returns         : None.
 ************************************************************************/
VOID
OsixTskDel (tOsixTaskId TskId)
{
    INT4                i4OsTskId;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixTskDel (0x%x)\r\n", (UINT4) TskId);
    i4OsTskId = (INT4) (gaOsixTsk[(UINT4) TskId].u4RscId);
    OsixRscDel (OSIX_TSK, (UINT4) i4OsTskId);
    TMOv2_Delete_Task (i4OsTskId);
}

/************************************************************************
 *  Function Name   : OsixTskSuspend
 *  Description     : This suspends the specified task.
 *                    This is an internal function.
 *  Input           : TskId - ID of task to be deleted.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
static UINT4
OsixTskSuspend (tOsixTaskId TskId)
{
    INT4                i4OsTskId;

    i4OsTskId = (INT4) (gaOsixTsk[(UINT4) TskId].u4RscId);
    if (TMOv2_Suspend_Task (i4OsTskId) == TMO_OK)
    {
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixTskResume
 *  Description     : This resumes the specified task which suspended.
 *  Input           : TskId - ID of task to be deleted.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
static UINT4
OsixTskResume (tOsixTaskId TskId)
{
    INT4                i4OsTskId;

    i4OsTskId = (INT4) (gaOsixTsk[(UINT4) TskId].u4RscId);
    if (TMOv2_Resume_Task (i4OsTskId) == TMO_OK)
    {
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixTskDelay
 *  Description     : This delays the execution of a specified task.
 *  Input           : u4Duration - Delay duration in STUPS.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixTskDelay (UINT4 u4Duration)
{
    if (TMOv2_Delay_Task (OsixSTUPS2Ticks (u4Duration)) != TMO_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixDelay                                         */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*                  : i4Unit     - Units in which the duration is       */
/*                                 specified                            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixDelay (UINT4 u4Duration, INT4 i4Unit)
{
    /* The function OsixTskdelay () does not allow delays less than 100 ms.
     * For backward compatibility reasons, that function cannot be changed.
     * So, this new function OsixDelay is introduced. On any this must be
     * ported before being used */

    UNUSED_PARAM (u4Duration);
    UNUSED_PARAM (i4Unit);

    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixTskIdSelf
 *  Description     : This returns the current task ID.
 *                    The ID returned is the index of the mapping table.
 *  Input/Output    : pTskId - Pointer to memory location in which the
 *                             id of the created task in returned.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixTskIdSelf (tOsixTaskId * pTskId)    /* Get ID of current task */
{
    UINT4               u4Count;
    INT4                i4OsTskId;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixTskIdSelf (0x%x)\r\n", (UINT4) pTskId);
    i4OsTskId = TMOv2_Get_TmoId ();
    if (i4OsTskId <= 0)
    {
        return OSIX_FAILURE;
    }
    for (u4Count = 1; u4Count <= OSIX_MAX_TSKS + 1; u4Count++)
    {
        if ((gaOsixTsk[u4Count].u4RscId) == (UINT4) i4OsTskId)
        {
            *pTskId = (tOsixTaskId) u4Count;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/********************************************************/
/* Routines for event management - send / receive event */
/********************************************************/
/************************************************************************
 *  Function Name   : OsixEvtSend
 *  Description     : This Sends a specific event to a specific task.
 *                    The ID returned is the index of the mapping table.
 *  Input           : TskId - ID of task to which to send the event.
 *                    u4Events - The event to be sent.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events)
{
    UINT4               u4Idx = (UINT4) TskId;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixEvtSend (%ld, 0x%lx)\r\n", (UINT4) TskId, u4Events);
    gaOsixTsk[u4Idx].u4Events |= u4Events;
    if ((gaOsixTsk[u4Idx].u2TskBlk) == OSIX_TRUE)
    {
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
        if (OsixTskResume (TskId) == OSIX_SUCCESS)
        {
            return (OSIX_SUCCESS);
        }
    }
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixEvtRecv
 *  Description     : This is called by a task to receive an event.
 *                    It supports both blocking and non-blocking mode.
 *  Input           : TskId - ID of task which wants to receive the Event.
 *                    u4Events - The event to be sent.
 *                    u4Flg    - OSIX_WAIT (0) / OSIX_NO_WAIT (2)
 *  Output          : pu4RcvEvents - Pointer to location containing
 *                                   events received if successful.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flg,
             UINT4 *pu4RcvEvents)
{
    UINT4               u4Idx = (UINT4) TskId;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_TSK_TRC, "",
               "OsixEvtRecv (%ld, 0x%lx, %ld, 0x%lx)\r\n", (UINT4) TskId,
               u4Events, u4Flg, pu4RcvEvents);
    *pu4RcvEvents = 0;

    /* If non-blocking receive, and if no events present, return. */
    if (u4Flg == OSIX_NO_WAIT && ((gaOsixTsk[u4Idx].u4Events) & u4Events) == 0)
    {
        return (OSIX_FAILURE);
    }

    while (1)
    {
        if (((gaOsixTsk[u4Idx].u4Events) & u4Events) != 0)
        {                        /* A required event has happened */
            *pu4RcvEvents = (gaOsixTsk[u4Idx].u4Events) & u4Events;
            gaOsixTsk[u4Idx].u4Events &= ~(*pu4RcvEvents);
            return (OSIX_SUCCESS);
        }

        /* Indicate that we are blocked on EvtRecv. */
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_TRUE;
        if (OsixTskSuspend (TskId) == OSIX_FAILURE)
        {
            gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
            return (OSIX_FAILURE);
        }
    }
}

/************************************************************************
 *  Function Name   : OsixInitialize
 *  Description     : This is called by the applications at startup.
 *                    It initializes the OSIXC library. It must be called
 *                    only once at startup. Repeated calls will reinitialize
 *                    the data structures.
 *  Input           : None.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixInitialize (void)
{
    UINT4               u4Idx;

    /* Mutual exclusion semaphore to add or delete elements from */
    /* name-id-mapping list. This semaphore itself will not be   */
    /* added to the name-to-id mapping list. This is a VxWorks   */
    /* specific call and must be mapped to relevant call for OS  */
    gOsixMutex = (tOsixSemId) TMOv2_Create_Semaphore (1);
    if (gOsixMutex <= 0)
    {
        return (OSIX_FAILURE);
    }

    /* Initialize all arrays. */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS + 1; u4Idx++)
    {
        gaOsixTsk[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
        gaOsixTsk[u4Idx].u4Events = 0;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    TMOv2_Start (OSIX_MAX_TSKS + 1, OSIX_MAX_QUES, OSIX_MAX_SEMS, gu4Tps,
                 gu4Stups);

    /* Switch off trace by default. */
    gu4OsixTrc = 0;

    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixShutDown
 *  Description     : This frees up resources allocated by OSIX during
 *                    initialization and resets all OSIX's internal
 *                    data structures. This must be called exactly once
 *                    when the system is to be shutdown.
 *  Input           : None.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixShutDown ()
{
    UINT4               u4Idx;

    /* Re-initialize all arrays and delete global semaphore */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        if (gaOsixTsk[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixTskDel (u4Idx);
        }
        gaOsixTsk[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
        gaOsixTsk[u4Idx].u4Events = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        if (gaOsixSem[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixSemDel ((tOsixSemId) (gaOsixSem[u4Idx].u4RscId));
        }
        gaOsixSem[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if (gaOsixQue[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixQueDel ((tOsixQId) (gaOsixQue[u4Idx].u4RscId));
        }
        gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
    }
    TMOv2_Delete_Semaphore ((SEM_ID) gOsixMutex);
    gOsixMutex = (tOsixSemId) OSIX_RSC_INV;
    UtlTrcClose ();
    return (OSIX_SUCCESS);
}

/************************************/
/* Routines for managing semaphores */
/************************************/

/* Keep it simple - support 1 type of semaphore - binary, blocking.     */
/* After creating, the semaphore must be given before it can be taken.  */
/************************************************************************
 *  Function Name   : OsixSemCrt
 *  Description     : This creates a semaphore.
 *                    OSIXC supports only binary, blocking sema4s.
 *                    The sema4 is created w/ a resource count of 0.
 *                    Thus a TAKE operation on a freshly created sema4,
 *                    would block the caller. If the sema4 is to be used
 *                    for mutex operations, applications need to do a 
 *                    SemGive prior to starting operation.
 *  Input           : au1Name - Pointer to a character string containing
 *                              name of the sema4 being created.
 *  Output          : pSemId - Pointer to memory location containing
 *                             ID of created sema4.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixSemCrt (UINT1 au1SemName[], tOsixSemId * pSemId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1SemName, OSIX_NAME_LEN);

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemCrt (%s)\r\n", au1Name);
    *pSemId = (tOsixSemId) TMOv2_Create_Semaphore (0);
    if (*pSemId == 0)
    {
        return (OSIX_FAILURE);
    }
    if (OsixRscAdd (au1Name, OSIX_SEM, *pSemId) == OSIX_FAILURE)
    {
        TMOv2_Delete_Semaphore ((SEM_ID) (*pSemId));
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);

}

/************************************************************************
 *  Function Name   : OsixSemDel
 *  Description     : This deletes a specified semaphore.
 *  Input           : SemId - ID of sema4 to be deleted.
 *  Returns         : None.
 ************************************************************************/
VOID
OsixSemDel (tOsixSemId SemId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemDel (0x%lx)\r\n",
               (UINT4) SemId);
    OsixRscDel (OSIX_SEM, (UINT4) SemId);
    TMOv2_Delete_Semaphore ((SEM_ID) SemId);
}

/************************************************************************
 *  Function Name   : OsixSemGive
 *  Description     : This performs a GIVE operation on a sema4.
 *  Input           : SemId - ID of sema4 to be given.
 *  Returns         : OSIX_SUCCESS.
 ************************************************************************/
UINT4
OsixSemGive (tOsixSemId SemId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemGive (0x%lx)\r\n",
               (UINT4) SemId);
    TMOv2_Give_Semaphore ((SEM_ID) SemId);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixSemTake
 *  Description     : This performs a TAKE operation on a sema4.
 *  Input           : SemId - ID of sema4 to be taken.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixSemTake (tOsixSemId SemId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_SEM_TRC, "", "OsixSemTake (0x%lx)\r\n",
               (UINT4) SemId);
    if (TMOv2_Take_Semaphore ((SEM_ID) SemId, WAIT, 0) != TMO_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/****************************************/
/* Routines for managing message queues */
/****************************************/

/************************************************************************
 *  Function Name   : OsixQueCrt
 *  Description     : This creates a OSIX queue of a specified name.
 *  Input           : au1Name - Character string containing name of Queue.
 *                    u4MaxMsgLen - Length in bytes of the message.
 *                    u4MaxMsgs   - Queue Depth -- max. no. of messages.
 *  Output          : pQueId - Pointer to memory location containing
 *                             ID of the created Queue.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixQueCrt (UINT1 au1QName[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    MEMCPY (au1Name, au1QName, OSIX_NAME_LEN);

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueCrt (%s, %ld, %ld)\r\n",
               au1Name, u4MaxMsgLen, u4MaxMsgs);
    *pQueId = (tOsixQId) TMOv2_Create_MsgQ (u4MaxMsgs, u4MaxMsgLen);
    if (*pQueId == (tOsixQId) NULL)
    {
        return (OSIX_FAILURE);
    }
    if (OsixRscAdd (au1Name, OSIX_QUE, *pQueId) == OSIX_FAILURE)
    {
        TMOv2_Delete_MsgQ ((tTmoQId) (*pQueId));
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixQueDel
 *  Description     : This deletes a specified queue.
 *  Input           : QueId - ID of queue to be deleted.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
void
OsixQueDel (tOsixQId QueId)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueDel (%ld)\r\n", QueId);
    OsixRscDel (OSIX_QUE, (UINT4) QueId);
    TMOv2_Delete_MsgQ ((tTmoQId) QueId);
    return;
}

/************************************************************************
 *  Function Name   : OsixQueSend
 *  Description     : This sends a message to a specified queue.
 *  Input           : QueId - ID of queue to which to enqueue.
 *                    pu1Msg - Pointer to message to be enqueued.
 *                    u4MsgLen - Length of message being enqueued.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen)
{
    u4MsgLen = 0;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "",
               "OsixQueSend (%ld, 0x%x, %ld)\r\n", QueId, pu1Msg, u4MsgLen);
    /* Typically native OS calls take message Length as an argument.
     * In the case of TMO Queues, the value supplied in the call
     * OsixQueCrt is used implicitly as the message length.
     * Hence the u4MsgLen parameter is not used in this function.
     */
    if (TMOv2_Send_MsgQ ((tTmoQId) QueId, pu1Msg) != TMO_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixQueRecv
 *  Description     : This is called by a task to receive a message from
 *                    a specified Queue.
 *  Input           : QueId - ID of queue from which to receive.
 *                    u4MsgLen - Length of message being enqueued.
 *                    i4Timeout - Timeout value in STUPS, for which
 *                    to wait.
 *
 *                    The i4Timeout field is interpreted as follows:
 *                    i4Timeout < 0  => blocking receive
 *                    i4Timeout = 0  => non-blocking receive
 *                    i4Timeout > 0  => block for a specified duration.
 *
 *  Output            pu1Msg - Contains the pointer to the received message
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4Timeout)
{
    u4MsgLen = 0;

    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueRecv (%ld)\r\n", QueId);
    /* Typically native OS calls take message Length as an argument.
     * In the case of TMO Queues, the value supplied in the call
     * OsixQueCrt is used implicitly as the message length.
     * Hence the u4MsgLen parameter is not used in this function.
     */
    if (i4Timeout > 0)
    {
        i4Timeout = (INT4) OsixSTUPS2Ticks ((UINT4) i4Timeout);
    }

    if (TMOv2_Receive_MsgQ ((tTmoQId) QueId, pu1Msg, i4Timeout) != TMO_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixQueNumMsg
 *  Description     : This returns the count of messages in a queue.
 *  Input           : QueId - ID of queue.
 *  Output            pu4NumMsg - Pointer to location which contains
 *                    the count of messages upon success.
 *  Returns         : 
 ************************************************************************/
UINT4
OsixQueNumMsg (tOsixQId QueId, UINT4 *pu4NumMsg)
{
    UtlTrcLog (OSIX_TRC_FLAG, OSIX_QUE_TRC, "", "OsixQueNumMsg (%ld)\r\n",
               QueId);
    *pu4NumMsg = (UINT4) (TMOv2_MsgQ_NumMsgs ((tTmoQId) QueId));
    return (OSIX_SUCCESS);
}

/********************************************************/
/* Routines for managing resources based on names       */
/********************************************************/
/************************************************************************
 *  Function Name   : OsixRscAdd
 *  Description     : This adds a name to OSIXC's name-to-id mapping table.
 *  Input           : au1Name - Name of the resource being added.
 *                    u4RscType - Whether task or queue or sema4.
 *                    one of {OSIX_RSC_TSK, OSIX_RSC_QUE, OSIX_RSC_SEM }
 *                    u4RscId - ID of the resource being added.
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE.
 ************************************************************************/
static UINT4
OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS + 1; u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixTsk[u4Idx].u4RscId = u4RscId;
                    gaOsixTsk[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixTsk[u4Idx].u2TskBlk = OSIX_FALSE;
                    gaOsixTsk[u4Idx].u4Events = 0;
                    MEMCPY (gaOsixTsk[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixSem[u4Idx].u4RscId = u4RscId;
                    gaOsixSem[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixSem[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixSem[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixQue[u4Idx].u4RscId = u4RscId;
                    gaOsixQue[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixQue[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixQue[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }

    OsixSemGive (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixRscDel
 *  Description     : This deletes a mapping from the mapping table.
 *  Input           : u4RscType - Whether task or queue or sema4.
 *                    one of {OSIX_RSC_TSK, OSIX_RSC_QUE, OSIX_RSC_SEM }
 *                    u4RscId - ID of the resource to be deleted.
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE.
 ************************************************************************/
VOID
OsixRscDel (UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return;
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS + 1; u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixTsk[u4Idx].u4RscId = (tOsixSemId) OSIX_RSC_INV;
                    gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixTsk[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixSem[u4Idx].u4RscId = (tOsixSemId) OSIX_RSC_INV;
                    gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixSem[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixQue[u4Idx].u4RscId = (tOsixSemId) OSIX_RSC_INV;
                    gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixQue[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (gOsixMutex);
}

/************************************************************************
 *  Function Name   : OsixRscFind
 *  Description     : This searchies the name-to-id mapping tables using
 *                    the name and returns the resource identifier to the
 *                    caller.
 *  Input           : au1Name - Name of the resource to locate.
 *                    u4RscType - Whether task or queue or sema4.
 *                    one of {OSIX_RSC_TSK, OSIX_RSC_QUE, OSIX_RSC_SEM }
 *  Output          : pu4RscId - Pointer to memory location containing
 *                    the ID if the resource exists.
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }
    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS + 1; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixTsk[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* For tasks applications know only our array index. */
                    /* This helps us to simulate events.                 */
                    *pu4RscId = u4Idx;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixSem[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = gaOsixSem[u4Idx].u4RscId;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixQue[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = gaOsixQue[u4Idx].u4RscId;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : Fsap2Start
 *  Description     : This is to be called by an application after all
 *                    resources have been created and the system is ready
 *                    to start working.
 *                    On some OSs it is required to make a system call
 *                    to transfer control from the main thread to the OS.
 *  Returns         : This call typically does not return.
 ************************************************************************/
VOID
Fsap2Start (void)
{
    TmoStart (0);
}

/************************************************************************
 *  Function Name   : OsixGetSysTime
 *  Description     : This returns the system time in STUPS since startup.
 *  Output          : pSysTime - Pointer to memory location containing
 *                    systime upon return.
 *  Returns         : 
 ************************************************************************/
UINT4
OsixGetSysTime (tOsixSysTime * pSysTime)
{
    *pSysTime = OsixTicks2STUPS (TMOv2_Get_Tick ());
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetSysUpTime                                  */
/*  Description     : Returns the system time in Seconds.               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysUpTime (VOID)
{
    return (gu4Seconds);
}

/************************************************************************
 *  Function Name   : OsixExGetTaskName
 *  Description     : This returns the name of a task given the taskId.
 *  Input           : TskId - ID of task.
 *  Returns         : Pointer to char string containing task name.
 ************************************************************************/
const UINT1        *
OsixExGetTaskName (tOsixTaskId TskId)
{
    /* To facilitate direct use of this call in a STRCPY,
     * we return a null string instead of a NULL pointer.
     * A null pointer is returned for all non-OSIX tasks
     * e.g., TMO's idle task.
     */
    if (TskId)
    {
        return ((UINT1 *) (gaOsixTsk[(UINT4) TskId].au1Name));
    }
    return ((const UINT1 *) "");
}

/************************************************************************
 *  Function Name   : FileOpen
 *  Description     : Function to Open a file.
 *  Input           : pu1FileName - Name of file to open.
 *                    i4Mode      - whether r/w/rw.
 *  Returns         : FileDescriptor if successful
 *                    -1             otherwise.
 ************************************************************************/
#if (FILESYS_SUPPORT == FSAP_ON)
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    INT4                i4Mode = 0;

    if (i4InMode & OSIX_FILE_CR)
    {
        i4Mode |= O_CREAT;
    }
    if (i4InMode & OSIX_FILE_RO)
    {
        i4Mode |= O_RDONLY;
    }
    else if (i4InMode & OSIX_FILE_WO)
    {
        i4Mode |= O_WRONLY;

        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }
    else if (i4InMode & OSIX_FILE_RW)
    {
        i4Mode |= O_RDWR;

        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }

    return open ((const CHR1 *) pu1FileName, i4Mode, 0644);
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description     : Function to close a file.
 *  Input           : i4Fd - File Descriptor to be closed.
 *  Returns         : 0 on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    return close (i4Fd);
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description     : Function to read a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer into which to read
 *                    i4Count - Number of bytes to read
 *  Returns         : Number of bytes read on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    return read (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description     : Function to write to a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : Number of bytes written on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    return write (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description     : Function to delete a file.
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : 0 on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    return unlink ((const CHR1 *) pu1FileName);
}
#else
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    pu1FileName = pu1FileName;
    i4InMode = i4InMode;
    return (-1);
}

INT4
FileClose (INT4 i4Fd)
{
    i4Fd = i4Fd;
    return (-1);
}

UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

INT4
FileDelete (const UINT1 *pu1FileName)
{
    pu1FileName = pu1FileName;
    return (-1);
}
#endif

/************************************************************************
 *  Function Name   : FsapShowTask
 *  Description     : Implements the show task part of CLI interface.
 *  Input           : au1Name - TaskName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowTask (UINT1 au1TskName[], UINT1 *pu1Result, INT4 buffer_size)
{
    TCB                *pTcb;
    UINT4               u4Idx;
    UINT4               u4Pos = 0;
    UINT4               u4NumMatches = 0;
    INT4                i4Value;
    INT4                buffer_length = buffer_size;
    UINT1              *pu1ResultStart = pu1Result;

    const CHR1         *pc1Heading =
        "  Name Pending Prio    BP     SP       ISP      Entry    Stack      Ctxt\r\n"
        "       Events                                   Point   Size [KB]  Switches\r\n"
        "  --------------------------------------------------------------------------\r\n";

    u4Pos = SPRINTF ((CHR1 *) pu1Result, "%s", pc1Heading);
    pu1Result += u4Pos;
    u4Pos = 0;

    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        if ((gaOsixTsk[u4Idx].u2Free) == OSIX_FALSE)
        {
            if (au1TskName != NULL)
            {
                if (au1TskName &&
                    (UtlStrCaseCmp ((CHR1 *) au1TskName,
                                    (CHR1 *) gaOsixTsk[u4Idx].au1Name)))
                    continue;

                u4NumMatches++;
            }
            pTcb = TMOv2_Get_tcb (gaOsixTsk[u4Idx].u4RscId);
            if (pTcb)
            {
                i4Value = SNPRINTF ((CHR1 *) pu1Result + u4Pos, buffer_size,
#ifdef TASK_STACK_MONITOR
                                    "%6s%6ld%6d%9lx%9lx%9lx%9lx%5d (%4d)%8d\r\n",
#else
                                    "%6s%6ld%6d%9lx%9lx%9lx%9lx%7d%8d\r\n",
#endif
                                    gaOsixTsk[u4Idx].au1Name,
                                    gaOsixTsk[u4Idx].u4Events,
                                    pTcb->priority,
                                    (UINT4) pTcb->context[4],
                                    (UINT4) pTcb->context[5],
                                    (UINT4) pTcb->initial_sp,
                                    (UINT4) pTcb->entry,
#ifdef TASK_STACK_MONITOR
                                    (((int) pTcb->initial_sp -
                                      (int) pTcb->roof)) / 1024,
#endif
                                    (pTcb->stack_size) / 1024, pTcb->nswitch);
                if (i4Value < 0)
                {
                    return OSIX_FAILURE;
                }
                u4Pos += (UINT4) i4Value;
                buffer_size = buffer_length - (INT4) u4Pos;
            }
        }
    }

    if (au1TskName != NULL)
    {
        if (!u4NumMatches)
        {
            /* SPRINTF adds a trailing \0 ensuring that the header (pc1Heading)
             * does not show up in the output. */
            pu1Result = pu1ResultStart;
            SPRINTF ((CHR1 *) pu1Result, "No such task.\r\n");
        }
        else
        {
            pu1Result = pu1ResultStart;
        }
    }
    else
    {
        pu1Result = pu1ResultStart;
    }

    OsixSemGive (gOsixMutex);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : FsapShowQueData
 *  Description     : Called from FsapShowQue to display the data present
 *                    in a given (pTmoQ) queue.
 *  Input           : pTmoQ - Pointer to TMO's que data structure.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : No. of bytes written to output buffer.
 ************************************************************************/
UINT4
FsapShowQueData (tTmoQ * pTmoQ, UINT1 *pu1Result)
{
    UINT4               u4Count;
    UINT4               u4Queued;
    UINT4               u4MsgLen;
    UINT4               u4Byte;
    UINT1              *pMsg;
    UINT1              *pDatum;
    UINT4               u4Pos = 0;

    u4MsgLen = pTmoQ->u4MsgLen;
    u4Queued = (pTmoQ->pQWrite - pTmoQ->pQRead) / u4MsgLen;
    pMsg = pTmoQ->pQRead;
    pDatum = MEM_MALLOC (u4MsgLen, UINT1);

    if (pDatum == NULL)
    {
        return u4Pos;
    }

    u4Pos += SPRINTF ((CHR1 *) pu1Result + u4Pos, "\r\nMessages in Q:\r\n");

    for (u4Count = 0; u4Count < u4Queued; u4Count++)
    {
        MEMCPY (pDatum, pMsg, u4MsgLen);

        /* If msglength is 4, assume it is a pointer and display it
         * else do a byte by byte dump of the message.
         */
        if (u4MsgLen == 4)
        {
            u4Pos +=
                SPRINTF ((CHR1 *) pu1Result + u4Pos, " 0x%lx\r\n",
                         (UINT4) pMsg);
        }
        else
        {
            for (u4Byte = 0; u4Byte < u4MsgLen; u4Byte++)
            {
                u4Pos +=
                    SPRINTF ((CHR1 *) pu1Result + u4Pos, " %x",
                             *(CHR1 *) (pMsg + u4Byte));
            }
            u4Pos += SPRINTF ((CHR1 *) pu1Result + u4Pos, "\r\n");
        }
        pMsg += u4MsgLen;
    }

    MEM_FREE (pDatum);

    return (u4Pos);
}

/************************************************************************
 *  Function Name   : FsapShowQue
 *  Description     : Implements the show que part of CLI interface.
 *  Input           : au1Name - QueueName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowQue (UINT1 au1QName[], UINT1 *pu1Result, INT4 buffer_size)
{
    tOsixQId            QueId;
    tTmoQ              *pTmoQ;
    UINT4               u4Queued;
    UINT4               u4MsgLen;
    UINT4               u4Idx;
    UINT4               u4Pos = 0;
    UINT4               u4TmpPos = 0;
    UINT4               u4NumMatches = 0;
    UINT4               u4SpecificQ = (au1QName ? 1 : 0);
    UINT4               u4Iter = 0;
    INT4                i4Value;
    INT4                buffer_length = buffer_size;
    UINT1              *pu1ResultStart = pu1Result;
    const CHR1         *pc1Heading =
        "   Name      ID        Q Depth  MaxMsgLen    Queued   OverFlows\r\n"
        "  --------------------------------------------------------------\r\n";

    u4Pos = SPRINTF ((CHR1 *) pu1Result, "%s", pc1Heading);
    pu1Result += u4Pos;
    u4Pos = 0;

    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /*
     * If the CLI cmd is "show que" we iterate over all queues once.
     * If the CLI cmd is "show que qname" first iteration of do-while
     * composes the tabular form, the second iteration, then writes
     * the messages in each que. Since the code is similar we use
     * a while loop around the foo loop and distinguish the iterations
     * by means of the two variables u4Iter and u4SpecificQ.
     */
    do
    {
        u4Iter++;
        for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
        {
            if ((gaOsixQue[u4Idx].u2Free) == OSIX_FALSE)
            {
                if (au1QName != NULL)
                {
                    if (au1QName &&
                        (UtlStrCaseCmp ((CHR1 *) au1QName,
                                        (CHR1 *) gaOsixQue[u4Idx].au1Name)))
                        continue;

                    u4NumMatches++;
                }

                QueId = (UINT4) &gaOsixQue[u4Idx];
                pTmoQ = (tTmoQ *) gaOsixQue[u4Idx].u4RscId;

                u4MsgLen = pTmoQ->u4MsgLen;
                u4Queued = (pTmoQ->pQWrite - pTmoQ->pQRead) / u4MsgLen;
                if (u4Iter == 1)
                {
                    i4Value = SNPRINTF ((CHR1 *) pu1Result + u4Pos, buffer_size,
                                        "%6s %10lx %10ld %10ld %10ld %10ld\r\n",
                                        gaOsixQue[u4Idx].au1Name, QueId,
                                        ((pTmoQ->pQEnd -
                                          pTmoQ->pQBase -
                                          1) / (pTmoQ->u4MsgLen)), u4MsgLen,
                                        u4Queued, pTmoQ->u4OverFlows);
                    if (i4Value < 0)
                    {
                        return OSIX_FAILURE;
                    }
                    u4Pos += (UINT4) i4Value;
                    buffer_size = buffer_length - (INT4) u4Pos;

                }
                else
                {
                    if (u4SpecificQ)
                    {
                        u4TmpPos = FsapShowQueData (pTmoQ, pu1Result + u4Pos);
                        if (u4TmpPos == 0)
                        {
                            return (OSIX_FAILURE);
                        }
                        u4Pos += u4TmpPos;
                    }
                }
            }
        }
    }
    while (u4Iter < 2);

    if (au1QName != NULL)
    {
        if (!u4NumMatches)
        {
            /* SPRINTF adds a trailing \0 ensuring that the header (pc1Heading)
             * does not show up in the output. */
            pu1Result = pu1ResultStart;
            SPRINTF ((CHR1 *) pu1Result, "No such queue.\r\n");
        }
        else
        {
            pu1Result = pu1ResultStart;
        }
    }
    else
    {
        pu1Result = pu1ResultStart;
    }
    OsixSemGive (gOsixMutex);

    return (0);
}

/************************************************************************
 *  Function Name   : FsapShowSem
 *  Description     : Implements the show sem part of CLI interface.
 *  Input           : au1Name - SemName if specified on command line,
 *                              NULL otherwise.
 *  Output          : pu1Result - Buffer containing formatted output.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILRE
 ************************************************************************/
UINT4
FsapShowSem (UINT1 au1SemName[], UINT1 *pu1Result, UINT4 *pu4NextIdx,
             INT4 buffer_size)
{
    tOsixSemId          SemId;
    UINT4               u4Pos = 0;
    UINT4               u4Idx;
    UINT4               u4NumMatches = 0;
    UINT4               u4Count = 0;
    INT4                i4Value;
    INT4                buffer_length = buffer_size;
    UINT1              *pu1ResultStart = pu1Result;
    const CHR1         *pc1Heading =
        "    Name     ID     Count \r\n" "   -----------------------\r\n";

    if (*pu4NextIdx == 1)
    {
        u4Pos = SPRINTF ((CHR1 *) pu1Result, "%s", pc1Heading);
        pu1Result += u4Pos;
        u4Pos = 0;
    }

    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Since the output buffer (pu1Result) is a fixed size, to avoid overflow
     * we transfer the info of 20 semaphores at a time.
     * The return value of OSIX_SUCCESS indicates to caller that there is
     * more to fetch. A return value of OSIX_FAILURE indicates to caller that
     * there is no more. The number 20 is purely a (safe) heuristic. */

    for (u4Idx = *pu4NextIdx; (u4Count < 20) && (u4Idx <= OSIX_MAX_SEMS);
         u4Idx++, u4Count++)
    {
        if ((gaOsixSem[u4Idx].u2Free) == OSIX_FALSE)
        {
            if (au1SemName != NULL)
            {
                if (au1SemName &&
                    (UtlStrCaseCmp ((CHR1 *) au1SemName,
                                    (CHR1 *) gaOsixSem[u4Idx].au1Name)))
                    continue;

                u4NumMatches++;
            }
            SemId = gaOsixSem[u4Idx].u4RscId;

            i4Value =
                SNPRINTF ((CHR1 *) pu1Result + u4Pos, "%8s%10lx", buffer_size,
                          gaOsixSem[u4Idx].au1Name, SemId);
            if (i4Value < 0)
            {
                return OSIX_FAILURE;
            }
            u4Pos += (UINT4) i4Value;
            buffer_size = buffer_length - (INT4) u4Pos;
            u4Pos += TmoGetSemInfo (SemId, pu1Result + u4Pos);
        }
    }

    if (au1SemName != NULL)
    {
        if (!u4NumMatches)
        {
            /* SPRINTF adds a trailing \0 ensuring that the header (pc1Heading)
             * does not show up in the output. */
            pu1Result = pu1ResultStart;
            SPRINTF ((CHR1 *) pu1Result, "No such sem.\r\n");
            OsixSemGive (gOsixMutex);
            return (OSIX_FAILURE);
        }
        else
        {
            pu1Result = pu1ResultStart;
        }
    }
    else
    {
        pu1Result = pu1ResultStart;
    }

    *pu4NextIdx = ++u4Idx;
    OsixSemGive (gOsixMutex);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : FsapTrace
 *  Description     : Implements the trace part of CLI interface.
 *  Input           : u4Flag =1 for 'trace' command, =0 for the 'no trace' cmd
 *                    u4Value - the value of the particular trace that is
 *                              being set/unset.
 *  Output          : pu4TrcLvl - Returns the current/new trace level.
 *  Returns         : None.
 ************************************************************************/
void
FsapTrace (UINT4 u4Flag, UINT4 u4Value, UINT4 *pu4TrcLvl)
{
    if (u4Value == 0)
    {
        *pu4TrcLvl = OSIX_TRC_FLAG;
        return;
    }

    if (u4Flag)
    {
        OSIX_TRC_FLAG |= u4Value;
    }
    else
    {
        OSIX_TRC_FLAG &= ~u4Value;
    }
    *pu4TrcLvl = OSIX_TRC_FLAG;

    return;
}

/************************************************************************/
/*  Function Name   : OsixGetTps                                        */
/*  Description     : Returns the no of system ticks in a second        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : Number of system ticks in a second                */
/************************************************************************/
UINT4
OsixGetTps ()
{
    return OSIX_TPS;
}

/************************************************************************/
/*  Function Name   : OsixSetLocalTime                                  */
/*  Description     : Obtains the system time and sets it in FSAP.      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSetLocalTime (void)
{
    time_t              t;
    struct tm          *tm;

    time (&t);
    tm = localtime (&t);
    if (tm == NULL)
    {
        return (OSIX_FAILURE);
    }
    tm->tm_year += (1900);
    UtlSetTime ((tUtlTm *) tm);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixGetMemInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4TotalMem - Total memory
 *                    pu4FreeMem - Free memory
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetMemInfo (UINT4 *pu4TotalMem, UINT4 *pu4FreeMem)
{
    *pu4TotalMem = 0;
    *pu4FreeMem = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetCPUInfo
 *  Description     : Function used to get the CPU utilization.
 *  Inputs          : None
 *  OutPuts         : pu4TotalUsage - Total Usage
 *                  : pu4CPUUsage - CPU Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetCPUInfo (UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage)
{
    *pu4TotalUsage = 0;
    *pu4CPUUsage = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetFlashInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4FlashUsage - Flash Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetFlashInfo (UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash)
{
    *pu4TotalUsage = 0;
    *pu4FreeFlash = 0;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                    to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}
