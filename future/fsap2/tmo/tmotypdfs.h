/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmotypdfs.h,v 1.16 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * Constants and Prototypes.
 *
 */

#ifndef _TMO_TYP_DFS_H
#define _TMO_TYP_DFS_H

#include <signal.h>

typedef UINT4 SEM_ID;

/* A circular queue is simulated using an allocated linear memory */
/* region. Read and write pointers are used to take out and put   */
/* messages in the queue. All messages are the same size only.    */
/* So, a task or thread reads messages from this queue to service */
/* the requests one by ine i.e. one command or activity at a time */
typedef struct
{
    UINT1      *pQBase;   /* linear memory location holding messages      */
    UINT1      *pQEnd;    /* pointer after last byte of the queue         */
    UINT1      *pQRead;   /* pointer where next message can be read from  */
    UINT1      *pQWrite;  /* pointer where next message can be written to */
    UINT4      u4MsgLen;  /* the length of messages on this queue         */
    SEM_ID     MSem;      /* semaphore for mutual exclusion during writes */
    SEM_ID     BSem;      /* semaphore for blocking those waiting on queue*/
    UINT4      u4OverFlows;/* No. of times, Q Send failed due toa  full Q  */
} tTmoQ;
typedef tTmoQ* tTmoQId;

/*** Return Values and Error codes ***/
#define TMO_OK                (0)
#define TMO_NOT_OK            (-1)
#define TMO_ERROR             (-1)
#define TMO_SEM_TIMEOUT        1
#define TMO_NO_SEM             3

/*** WAIT FLags for Queues and Events ***/
#define WAIT                   0
#define NO_WAIT                1
#define WAIT_FOREVER           2

/*********************************************
***** the core RTOS simulator definitions ****
**********************************************/

typedef VOID (*tTmoTaskMainFunc)(INT4);

#define i386

#ifdef i386
/* EAX/ECX/EDX/EBX/ESP/EBP/ESI/EDI/EFLAG/EIP */

#define NREGS 10

#define IEAX  0  /*  0 */
#define IECX  1  /*  4 */
#define IEDX  2  /*  8 */
#define IEBX  3  /* 12 */
#define IESP  4  /* 16 */
#define IEBP  5  /* 20 */
#define IESI  6  /* 24 */
#define IEDI  7  /* 28 */
#define IEFLAG 8 /* 32 */
#define IEIP 9   /* 36 */

#endif

#ifdef MC68000

#define NREGS 17

#define ID0 0
#define ID1 1
#define ID2 2
#define ID3 3
#define ID4 4
#define ID5 5
#define ID6 6
#define ID7 7

#define IA0 8
#define IA1 9
#define IA2 10
#define IA3 11
#define IA4 12
#define IA5 13
#define IA6 14
#define IA7 15
#define IPC 16

#endif

#ifdef MPCxx
/* 32 SPR and 1 condition register */
#define NREGS 33

#define SPR0  0    /*  0 */
#define SPR1  1    /*  4 */
#define SPR2  2    /*  8 */
#define SPR3  3    /* 12 */
#define SPR4  4    /* 16 */
#define SPR5  5    /* 20 */
#define SPR6  6    /* 24 */
#define SPR7  7    /* 28 */
#define SPR8  8    /* 32 */
#define SPR9  9    /* 36 */
#define SPR10  10   /* 40 */
#define SPR11  11   /* 44 */
#define SPR12  12   /* 48 */
#define SPR13  13   /* 52 */
#define SPR14  14   /* 56 */
#define SPR15  15   /* 60 */
#define SPR16  16   /* 64 */
#define SPR17  17   /* 68 */
#define SPR18  18   /* 72 */
#define SPR19  19   /* 76 */
#define SPR20  20   /* 80 */
#define SPR21  21   /* 84 */
#define SPR22  22   /* 88 */
#define SPR23  23   /* 92 */
#define SPR24  24   /* 96 */
#define SPR25  25   /* 100 */
#define SPR26  26   /* 104 */
#define SPR27  27   /* 108 */
#define SPR28  28   /* 112 */
#define SPR29  29   /* 116 */
#define SPR30  30   /* 120 */
#define SPR31  31   /* 124 */
#define CNDREG 32   /* 128*/
#endif

#ifdef IDT_MIPS2_RC32334 
 /* IDT Mips Processor has 32 GPRs. Out of 32 GPRs,
 * the GPRs[0, 25, 28] were not preserved/restored
 * while context switching happens for the following
 * reasons:
 *   (a) GPR[0] will always have the value 0, so no
 *       need to preserve it across tasks(threads)
 *
 *   (b) GPR[25] is used as the PIC Controller in
 *       MIPS Processor, meaning that before jumping
 *       to the entry point of function calls, the
 *       entry point of the function has to be set
 *       to this GPR so that the Global pointer can
 *       access the static/global variables by keeping
 *       this entry point at GPR[25] as the Offset
 *  (c) GPR[28] is exclusively used by MIPS Processor,
 *      which should not be accessed by us.
 *
 * So GPRs[1..5..24, 26, 27, 29..31] totally 28 GPRs,
 * Special Registers HI, LO were preserved/restored
 * while context switching happens. So the Number of
 * regs is derieved as 29 + 2 = 31
 */
#define NREGS 31

 /* IPARM - GPR[4] that's being used for passing first
 *         argument to the function calls
 *
 * IPC - Current Program Counter. In MIPS Processor
 *       the GPR[31] is used for holding the return
 *       addresses, so GPR[31] is mapped to IPC
 *
 * ISP - Current Stack Pointer. In MIPS Processor
 *      the GPR[29] is used as the Stack pointer
 *      and hence GPR[29] is mapped to ISP
 *
 */
#define IPARM 0   /* First Parameter to the function calls GPR[4] */
#define IPC   1   /* Program Counter GPR[31] */
#define ISP   2   /* Stack Pointer GPR[29] */

#endif

#define OK    (0)
#define ERROR (-1)


struct watch_dog_timer {
    struct watch_dog_timer *p_next;
    struct watch_dog_timer *p_prev;
    int priority;
    int delay;
    int count;
    tTmoTaskMainFunc p_func;
    int param;
};

typedef struct watch_dog_timer WDOG_TIMER;
typedef struct watch_dog_timer * WDOG_ID;

/* status of task control block */

#define TASK_RUN       0x01
#define TASK_SLEEP     0x02
#define TASK_SEMAPHORE 0x04
#define TASK_DELETED   0x08
#define SEM_TIMEOUT                0x10
#define SEM_TIMEOUT_AND_REMOVED    0x40

struct tcb {
    struct tcb *p_next;
    struct tcb *p_prev;
    int     priority;
    int     x;
    int     context[NREGS];
    char    *name;
    int     nswitch;
    int     status;
    int     semStatus;
    tTmoTaskMainFunc entry;
    int     *original_sp;
    int     *initial_sp;
    int     stack_size;
    int     taskId;
    int     count;
#ifdef TASK_STACK_MONITOR
    int     *roof;
    int     checkpoint;
#endif
};

typedef struct tcb TCB;

struct task_queue_head {
    TCB *p_head;
    TCB *p_tail;
    int count;
};

typedef struct task_queue_head TASKQHEAD;

extern void          TmoSched(void);

extern TASKQHEAD    runq;
extern TASKQHEAD    readyq;
extern TASKQHEAD    sleepq;
extern TCB          *p_curtcb;

extern int scheduler_disabled;

/*********************************************
******  FUNCTION PROTOTYPES Definitions ******
**********************************************/

VOID  TmoStart (int);
UINT4 TMOv2_Start(UINT4 u4Tasks, UINT4 u4Qs, UINT4 u4Sems, UINT4, UINT4);

/***********************************
***** Task Management Functions ****
************************************/
INT4  TMOv2_Spawn_Task        ARG_LIST((const INT1 *pi1_TaskName,
                                        INT4 i4_Priority, UINT4 u4StackSize,
                                      VOID  (*pTaskMainFunc)(INT4),
                                      INT4 i4_TaskParam, UINT4 u4TaskMode));
INT4  TMOv2_Delete_Task       ARG_LIST((INT4 i4_TaskId));
INT4  TMOv2_Delay_Task        ARG_LIST((INT4 i4_Duration));
INT4  TMOv2_Get_Task_Priority ARG_LIST((INT4 i4_TaskId, INT4 *pi4_Priority));
INT4  TMOv2_Set_Task_Priority ARG_LIST((INT4 i4_TaskId, INT4 i4_NewPriority));
INT4  TMOv2_Get_Task_ID       ARG_LIST((INT1 *pi1_TaskName));
INT1  *TMOv2_Get_Task_Name    ARG_LIST((INT4 i4_TaskId));
INT4  TMOv2_Get_TmoId         ARG_LIST((VOID));
INT4  TMOv2_Suspend_Task      ARG_LIST((UINT4 u4TaskId));
INT4  TMOv2_Resume_Task       ARG_LIST((UINT4 u4TaskId));
UINT4 TMOv2_Get_Cur_TaskId    ARG_LIST((VOID));

/****************************************
******  Queue Management Functions ******
*****************************************/

UINT4 TMOv2_Create_Queue(UINT4 u4Depth);

UINT4 TMOv2_Delete_Queue(UINT4 u4QId);

INT4  TMOv2_EnQueue_Message  ARG_LIST((UINT4 u4_QueueNum, UINT1 *pu1_ChnDscAddr,
                                     UINT4 u4MsgPriority));
UINT4 TMOv2_DeQueue_Message  ARG_LIST((UINT4 u4_QueueNum, UINT4 Flags,
                                     UINT4 u4Timeout, UINT1 **));
INT4 TMOv2_Query_Queue(UINT4 u4_QueueNum);

tTmoQId TMOv2_Create_MsgQ ARG_LIST((UINT4, UINT4));
VOID TMOv2_Delete_MsgQ ARG_LIST((tTmoQId));
INT4 TMOv2_Send_MsgQ ARG_LIST((tTmoQId, UINT1*));
INT4 TMOv2_Receive_MsgQ ARG_LIST((tTmoQId, UINT1*g, INT4));
UINT4 TMOv2_MsgQ_NumMsgs ARG_LIST((tTmoQId));

/************************************
***** Timer Management Functions ****
*************************************/
UINT4  TMOv2_Get_Tick             ARG_LIST((VOID));
VOID   TMOv2_Set_Tick             ARG_LIST((UINT4 u4_TickCount));
UINT4  TMOv2_Start_Timer_Service ARG_LIST((INT4 i4_NumOfTicks));
INT4   TMOv2_Stop_Timer_Service   ARG_LIST((VOID));
WDOG_ID TmoWdCreate              ARG_LIST((VOID));
void TmoWdDelete                 ARG_LIST((WDOG_ID wd_id));

/****************************************
***** Semaphore Management Functions ****
*****************************************/
SEM_ID TMOv2_Create_Semaphore ARG_LIST((UINT4));
VOID   TMOv2_Delete_Semaphore ARG_LIST((SEM_ID SemId));
VOID   TMOv2_Give_Semaphore   ARG_LIST((SEM_ID SemId));
UINT4  TMOv2_Take_Semaphore   ARG_LIST((SEM_ID SemId, UINT4, UINT4));

/* CLI support functions for OSIX. */
EXPORT TCB * TMOv2_Get_tcb (INT4 i4_TaskId);
EXPORT UINT4 TmoGetSemInfo (SEM_ID SemId, UINT1 *pu1Result);

#endif /******** _TMO_TYP_DFS_H *********/
