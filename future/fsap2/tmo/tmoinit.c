/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmoinit.c,v 1.15 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * TMO initializes the global data structure and the headers
 * of link lists.
 *
 */

#include "tmotypes.h"
#include "tmo.h"

#define PRIVATE static
#define EXPORT

INT4                TMOv2_Initialized = 0;
UINT4               gu4TMOMaxETCBs;
UINT4               gu4TMOMaxQs;
tTMOv2_TASK_TABLE  *pTMOv2_TaskTable;
WDOG_ID             tmoWdID;

/********************************************************************
**  FUNCTION : Init TMO.
*********************************************************************
**  RETURN VALUE:
**  DESCRIPTION:
**    This function initializes TMO.
**********************************************************************/
static VOID
TMOv2Init (UINT4 u4MaxTasks, UINT4 u4MaxQueues, UINT4 tps, UINT4 stups)
{
    UINT4               i;

    if (TMOv2_Initialized)
        return;
    TMOv2_Initialized = 1;

    /*
     * Increment MaxTasks by two.
     * One is for the idle task.
     * The other is because somewhere 0 is used as ret. val for FAILURE. and
     * hence it can't be a TCB index.
     */
    gu4TMOMaxETCBs = u4MaxTasks + 2;
    gu4TMOMaxQs = u4MaxQueues;

    pTMOv2_TaskTable = TMO_SYS_MALLOC (tTMOv2_TASK_TABLE);
    memset (pTMOv2_TaskTable, 0, sizeof (*pTMOv2_TaskTable));
    pTMOv2_TaskTable->a_p_Etcb =
        (tTMOv2_ETCB_REC **) malloc (gu4TMOMaxETCBs *
                                     sizeof (tTMOv2_ETCB_REC *));
    for (i = 0; i < u4MaxTasks; ++i)
        pTMOv2_TaskTable->a_p_Etcb[i] = NULL;

    TmoKernInit (tps, stups);

/***** Create Watchdog Timer For Timer Service *****/
    tmoWdID = TmoWdCreate ();
}

/********************************************************************
**  FUNCTION : Start TMO.
*********************************************************************
**  RETURN VALUE:
**  DESCRIPTION:
**    This is the API to be invoked by applications using TMO.
**********************************************************************/
EXPORT UINT4
TMOv2_Start (UINT4 u4Tasks, UINT4 u4Qs, UINT4 u4Sems,
             UINT4 u4tps, UINT4 u4stups)
{
    int                 rc = 0;

    u4Sems = u4Sems;            /* Unused */
    TMOv2Init (u4Tasks, u4Qs, u4tps, u4stups);
    return rc;
}
