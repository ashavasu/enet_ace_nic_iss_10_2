/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmotypes.h,v 1.16 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * Type definitions.
 *
 */

#ifndef _TMO_TYPES_H
#define _TMO_TYPES_H

typedef char               BOOLEAN;
typedef char               BOOL1;
#ifdef i386
typedef char               INT1;
#else
typedef signed char        INT1;
#endif

typedef unsigned char      UINT1;
typedef UINT1              BYTE;

typedef short              INT2;
typedef unsigned short     UINT2;

typedef int                INT4;
typedef unsigned int       UINT4;

#define VOID               void


#ifndef NULL
#define NULL    (0)
#endif

#if !defined(SUCCESS) || (SUCCESS != 0)
#define SUCCESS (0)
#endif

#if !defined(FAILURE) || (FAILURE != (-1))
#define FAILURE (-1)
#endif

#if !defined(PRIVATE) || (PRIVATE != static)
#define PRIVATE static
#endif

#if !defined(PUBLIC) || (PUBLIC != extern)
#define PUBLIC  extern
#endif

#ifndef EXPORT
#define EXPORT
#endif

#ifdef  __STDC__
#define ARG_LIST(x)  x
#else
#define ARG_LIST(x)  ()
#endif

#endif /**** _TMO_TYPES_H ****/
