/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmosem.c,v 1.14 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * Semaphore Management Routines.
 *
 */

#include "tmotypes.h"
#include "tmo.h"

/*******************************************************************
**  FUNCTION:
**     Create semaphore. 
********************************************************************  
**
**  RETURN VALUE:
**         semaphore identifier
**  DESCRIPTION:
**         Creates a sema4 with initial count set to u4Count.
**
********************************************************************/
SEM_ID
TMOv2_Create_Semaphore (UINT4 u4Count)
{
    SEMAPHORE          *p_sem;

    if ((p_sem = (SEMAPHORE *) malloc (sizeof (SEMAPHORE))) == NULL)
        TmoExit ("semCreatev2: Can't allocate semaphore\n", 0);
    memset ((char *) p_sem, 0, sizeof (*p_sem));
    p_sem->count = u4Count;
    return ((SEM_ID) p_sem);
}

/*******************************************************************
**  FUNCTION:
**     Give semaphore.  
********************************************************************  
**
**  RETURN VALUE:
**        None. 
**  DESCRIPTION:
**        Does a give operation on a sema4 - semId.
**
********************************************************************/
void
TMOv2_Give_Semaphore (semId)
     SEM_ID              semId;
{
    SEMAPHORE          *p_sem;
    SEM_WAIT_NODE      *p_sem_wait;
    int                 rc;

    p_sem = (SEMAPHORE *) semId;
    p_sem->count++;

    /* Wake up everybody who took this semaphore */
    /* PMT */
    p_sem_wait = (SEM_WAIT_NODE *) TmoNodeDelHead ((LIST *) & p_sem->list);
    for (; p_sem_wait;)
    {
        if (!(p_sem_wait->p_tcb->semStatus & SEM_TIMEOUT))
        {
            /* If sem_wait_list has a node but the corr. task had
             * been deleted in the meantime, nothing is to be done.
             */
            rc = TmoNodeDel ((LIST *) & sleepq, (NODE *) p_sem_wait->p_tcb);
            if (rc == 0)
            {
                TmoNodeAdd ((LIST *) & readyq, (NODE *) p_sem_wait->p_tcb);
                p_sem_wait->p_tcb->status = TASK_RUN;
                free ((char *) p_sem_wait);
            }
        }
        else
        {
            p_sem_wait->p_tcb->status = TASK_RUN;
            /* Status indicates that semTakev2 shouldn't try to
             * delete this node.
             */
            p_sem_wait->p_tcb->semStatus |= SEM_TIMEOUT_AND_REMOVED;

#if  I_THOUGHT_OF_DOING_A_PEEKABOO
            p_sem_wait->p_tcb->semStatus &= ~SEM_TIMEOUT;
            free ((char *) p_sem_wait);
#endif
        }
        p_sem_wait = (SEM_WAIT_NODE *) TmoNodeDelHead ((LIST *) & p_sem->list);
    }
    TmoSched ();
}

/*****************************************************************
**  FUNCTION:
**     Take semaphore  
*******************************************************************  
**
**  RETURN VALUE:
**       TMO_SEM_TIMEOUT - If blocking and timed out.
**       TMO_NO_SEM      - Invalid sem4 Id.
**       success         - o/w
**         
**  DESCRIPTION:
**       Does a sema4 take operation.
******************************************************************/
UINT4
TMOv2_Take_Semaphore (sem_id, flag, timeout)
     SEM_ID              sem_id;
     UINT4               flag;
     UINT4               timeout;
/********************************************************
*** This will be ignored since TMOv2_Take_Semaphore() *****
*** doesn't support timeout                         *****
*********************************************************/
{
    SEMAPHORE          *p_sem;
    SEM_WAIT_NODE      *p_sem_wait;

    p_sem = (SEMAPHORE *) sem_id;

    for (;;)
    {
        if (p_sem->count > 0)
        {
            --p_sem->count;
            return 0;
        }

        if (flag == TMO_NO_WAIT)
            return TMO_NO_SEM;

        /* Put this guy into sleep */
        p_curtcb->status &= ~TASK_RUN;
        p_curtcb->status |= (TASK_SLEEP | TASK_SEMAPHORE);
        p_curtcb->status &= ~SEM_TIMEOUT;
        p_curtcb->count = timeout;
        if ((p_sem_wait =
             (SEM_WAIT_NODE *) malloc (sizeof (*p_sem_wait))) == NULL)
            TmoExit ("TMOv2_Take_Semaphore:Can't allocate memory\n", 0);
        memset ((char *) p_sem_wait, 0, sizeof (*p_sem_wait));

        p_sem_wait->p_tcb = p_curtcb;
        p_sem_wait->priority = p_curtcb->priority;
        TmoNodeAdd ((LIST *) & p_sem->list, (NODE *) p_sem_wait);

        TmoSched ();
        if ((p_curtcb->semStatus) & SEM_TIMEOUT)
        {
            p_curtcb->semStatus &= ~SEM_TIMEOUT;
            if (!(p_curtcb->semStatus & SEM_TIMEOUT_AND_REMOVED))
            {
                TmoNodeDel ((LIST *) & p_sem->list, (NODE *) p_sem_wait);
            }
            else
            {
                free ((char *) p_sem_wait);
                --p_sem->count;
                return 0;
            }
            free ((char *) p_sem_wait);
            return TMO_SEM_TIMEOUT;
        }
    }
}

/****************************************************************
**  FUNCTION:
**     Delete semaphore  
*****************************************************************  
**
**  RETURN VALUE:
**      None. 
**  DESCRIPTION:
**      Deletes a semaphore
**
******************************************************************/
void
TMOv2_Delete_Semaphore (semId)
     SEM_ID              semId;
{
    free ((void *) semId);
}

UINT4
TmoGetSemInfo (SEM_ID SemId, UINT1 *pu1Result)
{
    UINT4               u4Pos = 0;
    UINT4               ebp;
    SEMAPHORE          *pSem = (SEMAPHORE *) SemId;
    SEM_WAIT_LIST      *pSemWaitList = &(pSem->list);
    SEM_WAIT_NODE      *pSemWaitNode;
    TCB                *pTcb;

    if (pSem->count)
    {
        u4Pos += sprintf ((char *) pu1Result + u4Pos, "Available (%d)\r\n",
                          pSem->count);
        return (u4Pos);
    }
    else
    {
        u4Pos += sprintf ((char *) pu1Result + u4Pos, "  Locked");

        pSemWaitNode = pSemWaitList->p_head;
        if (pSemWaitNode)
        {
            u4Pos +=
                sprintf ((char *) pu1Result + u4Pos,
                         "\r\n       \\ WAIT LIST:\r\n");
        }
        while (pSemWaitNode)
        {
            /* From the TCB, get the EBP register and to be of any use,
             * go up two stack frames to cross fsap calls
             * and land in protocol code
             */
            pTcb = pSemWaitNode->p_tcb;
            ebp = pTcb->context[4];
            ebp = *(unsigned long *) ebp;
            ebp = *(unsigned long *) ebp;
            u4Pos +=
                sprintf ((char *) pu1Result + u4Pos,
                         "            |- "
                         "Task %s is @ 0x%lx \r\n",
                         pTcb->name, *(unsigned long *) (ebp + 4));
            pSemWaitNode = pSemWaitNode->p_next;
        }
        u4Pos += sprintf ((char *) pu1Result + u4Pos, "\r\n");
    }

    return (u4Pos);
}
