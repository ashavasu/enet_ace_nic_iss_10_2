/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osixwrap.c,v 1.12 2015/04/28 12:18:19 siva Exp $
 *
 * Description: The wrapper file which maps fsap2 calls
 *              to fsap 3000 calls.
 */
#include "osxinc.h"
#include "osix.h"

UINT4               gu4MyNodeId = 1;
extern UINT4        gu4Tps;
extern UINT4        gu4Stups;

UINT4               OsixSTUPS2Ticks (UINT4);
UINT4               OsixTicks2STUPS (UINT4);
UINT4               OsixGetCurTaskId (void);
UINT4               OsixGetNodeIdOfSelf (void);
INT4                OsixGetCallerTaskId (void);

/************************************************************************
 *  Function Name   : OsixInit
 *  Description     : This is called by the applications at startup.
 *  Input           : None.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixInit (tOsixCfg * pOsixCfg)
{
    pOsixCfg = pOsixCfg;        /* Unused Param */

    gu4Tps = pOsixCfg->u4TicksPerSecond;
    gu4Stups = pOsixCfg->u4SystemTimingUnitsPerSecond;

    return (OsixInitialize ());
}

/************************************************************************
 *  Function Name   : OsixSendEvent
 *  Description     : This sends an event to a task.
 *  Input           : u4Node - Unused
 *                    au1TskName - Name of task to which to send event.
 *                    u4Events - A bit mask of the event to be sent.
 *  Returns         : None.
 ************************************************************************/
UINT4
OsixSendEvent (UINT4 u4Node, const UINT1 au1TskName[], UINT4 u4Events)
{
    tOsixTaskId         TskId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1TskName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1TskName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    if (OsixRscFind (au1Name, OSIX_TSK, &TskId) == OSIX_SUCCESS)
    {
        return OsixEvtSend (TskId, u4Events);
    }
    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixReceiveEvent
 *  Description     : This is called by an application to receive
 *                    an event.
 *  Input           : u4Node - Unused
 *                    u4Flags - whether blocking (OSIX_WAIT) or
 *                                  non-blocking (OSIX_NO_WAIT)
 *                    u4Timeout - Unused (Not supported.)
 *  Output          : pu4RcvdEvts - Pointer to memory location which
 *                                  contains received events upon return.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixReceiveEvent (UINT4 u4Events, UINT4 u4Flags, UINT4 u4Timeout,
                  UINT4 *pu4RcvdEvts)
{
    tOsixTaskId         TskId;

    u4Timeout = 0;                /* unused; */

    if (OsixTskIdSelf (&TskId) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OsixEvtRecv (TskId, u4Events, u4Flags, pu4RcvdEvts));
}

/************************************************************************
 *  Function Name   : OsixCreateQ
 *  Description     : This creates a queue of a specified name.
 *  Input           : au1QName - Name of queue to be created.
 *                    u4QDepth - Maximum no. of messages that can be stored.
 *                              
 *                    u4QMode -  Unused.
 *  Output          : pQId -     Pointer to memory location which
 *                               contains Q-ID upon return.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixCreateQ (const UINT1 au1QName[4], UINT4 u4QDepth,
             UINT4 u4QMode, tOsixQId * pQId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1QName[u1Index] != '\0')); u1Index++)
    {
        au1Name[u1Index] = au1QName[u1Index];
    }

    u4QMode = 0;                /* unused; */
    if (OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) pQId) == OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OsixQueCrt (au1Name, OSIX_MAX_Q_MSG_LEN, u4QDepth, pQId));
}

/************************************************************************
 *  Function Name   : OsixDeleteQ
 *  Description     : This deletes a queue of a specified name.
 *  Input           : u4Node - Unused.
 *                    au1QName - Name of queue to be deleted.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixDeleteQ (UINT4 u4Node, const UINT1 au1QName[4])
{
    tOsixQId            QueId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1QName[u1Index] != '\0')); u1Index++)
    {
        au1Name[u1Index] = au1QName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    if (OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) &QueId) == OSIX_SUCCESS)
    {
        OsixQueDel (QueId);
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixSendToQ
 *  Description     : This sends a message to a specified queue.
 *  Input           : u4Node   - Unused.
 *                    au1QName - Name of queue to which to send.
 *                    pMsg     - Pointer to message to be sent.
 *                    u4Prio   - Unused.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixSendToQ (UINT4 u4Node, const UINT1 au1QName[4],
             tOsixMsg * pMsg, UINT4 u4Prio)
{
    tOsixQId            QueId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1QName[u1Index] != '\0')); u1Index++)
    {
        au1Name[u1Index] = au1QName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    u4Prio = 0;                    /* unused; */
    if (OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) &QueId) == OSIX_SUCCESS)
    {
        /* We enqueue the message pointer as the message . We assume that */
        /* all tasks are in a common memory space and can share pointers. */
        return (OsixQueSend (QueId, (UINT1 *) (&pMsg), OSIX_DEF_MSG_LEN));
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixReceiveFromQ
 *  Description     : This is called by applns. to receive a message.
 *  Input           : u4Node   - Unused.
 *                    au1QName - Name of queue to which to send.
 *                    u4Flags  - Whether blocking (OSIX_WAIT) or
 *                                   non-blocking (OSIX_NO_WAIT)
 *                    u4Timeout - Timeout value if OSIX_WAIT.
 *  Output          : ppu1Msg - Pointer to memory location which contains
 *                              address of received msg upon return.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixReceiveFromQ (UINT4 u4Node, const UINT1 au1QName[4], UINT4 u4Flags,
                  UINT4 u4Timeout, tOsixMsg ** ppu1Msg)
{
    tOsixQId            QueId;
    INT4                i4Timeout = 0;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1QName[u1Index] != '\0')); u1Index++)
    {
        au1Name[u1Index] = au1QName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    if (u4Flags != OSIX_NO_WAIT)
    {
        /* Flag not equal to OSIX_NO_WAIT with 0 timeout means wait forever. */
        /* Too large a value - greater than 2e+31 is also a wait forever.    */
        i4Timeout = (u4Timeout == 0) ? -1 : (INT4) u4Timeout;
    }
    if (u4Flags == OSIX_NO_WAIT)
    {
        i4Timeout = 0;
    }

    if (OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) &QueId) == OSIX_SUCCESS)
    {
        return OsixQueRecv (QueId, (UINT1 *) ppu1Msg, OSIX_DEF_MSG_LEN,
                            i4Timeout);
    }
    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixGetQId
 *  Description     : This returns the Q-ID given the Queue name.
 *  Input           : u4NodeId   - Unused.
 *                    au1QName   - Name of queue
 *  Output          : pQId       - Pointer to memory location which contains
 *                                 the Q-ID upon return.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixGetQId (UINT4 u4NodeId, const UINT1 au1QName[4], tOsixQId * pQId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1QName[u1Index] != '\0')); u1Index++)
    {
        au1Name[u1Index] = au1QName[u1Index];
    }

    u4NodeId = 0;
    if (OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) pQId) == OSIX_SUCCESS)
    {
        return (OSIX_SUCCESS);
    }
    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixGetNumMsgsInQ
 *  Description     : This returns the number of msgs in the queue.
 *  Input           : u4Node   - Unused.
 *                    au1QName - Name of queue
 *  Output          : pu4Msgs  - Pointer to memory location which contains
 *                               the count of messages upon return.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixGetNumMsgsInQ (UINT4 u4Node, const UINT1 au1QName[4], UINT4 *pu4Msgs)
{
    tOsixQId            QueId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1QName[u1Index] != '\0')); u1Index++)
    {
        au1Name[u1Index] = au1QName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    if (OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) &QueId) == OSIX_SUCCESS)
    {
        return (OsixQueNumMsg (QueId, pu4Msgs));
    }
    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixCreateSem
 *  Description     : This creates a sema4 of a given name.
 *  Input           : au1SemName  - Name of sema4.
 *                    u4InitialCount - Initial value of sema4.
 *                    u4Flags        - Unused.
 *  Output          : pSemId - Pointer to memory which contains SEM-ID
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixCreateSem (const UINT1 au1SemName[4], UINT4 u4InitialCount,
               UINT4 u4Flags, tOsixSemId * pSemId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1SemName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1SemName[u1Index];
    }

    u4Flags = 0;                /* unused; */

    if (OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) pSemId) == OSIX_SUCCESS)
    {
        /* Semaphore by this name already exists. */
        return (OSIX_FAILURE);
    }
    if (OsixSemCrt (au1Name, pSemId) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    /* Implementation assumes that mutex is created when u4InitialCount */
    /* is 1, otherwise sem is for task sync. OsixSemCrt creates binary  */
    /* sem in blocked state. So, if u4InitialCount is 1, give the sem.  */
    if (u4InitialCount == 1)
    {
        OsixSemGive (*pSemId);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixDeleteSem
 *  Description     : This deletes sema4 of a given name.
 *  Input           : u4Node   - Unused.
 *                    au1SemName - Name of sema4 to be deleted.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixDeleteSem (UINT4 u4Node, const UINT1 au1SemName[4])
{
    tOsixSemId          SemId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1SemName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1SemName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    if (OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) &SemId) == OSIX_SUCCESS)
    {
        OsixSemDel (SemId);
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixTakeSem
 *  Description     : This is called by appln.s to take a sema4.
 *  Input           : u4Node     - Unused.
 *                    au1SemName - Name of sema4.
 *                    u4Flags    - Unused
 *                    u4Timeout  - Unused
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixTakeSem (UINT4 u4Node, const UINT1 au1SemName[4],
             UINT4 u4Flags, UINT4 u4Timeout)
{
    tOsixSemId          SemId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1SemName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1SemName[u1Index];
    }

    /* Unused */
    u4Node = 0;
    u4Timeout = 0;
    u4Flags = 0;

    if (OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) &SemId) == OSIX_SUCCESS)
    {
        return (OsixSemTake (SemId));
    }
    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixGiveSem
 *  Description     : This is called by appln.s to give a sema4.
 *  Input           : u4Node     - Unused.
 *                    au1SemName - Name of sema4.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixGiveSem (UINT4 u4Node, const UINT1 au1SemName[4])
{
    tOsixSemId          SemId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1SemName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1SemName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    if (OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) &SemId) == OSIX_SUCCESS)
    {
        OsixSemGive (SemId);
        return (OSIX_SUCCESS);
    }
    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OsixGetSemId
 *  Description     : This returns the SemId given the SemName.
 *  Input           : u4Node     - Unused.
 *                    au1SemName - Name of sema4.
 *  Output          : pId - Pointer to memory location containing SEM-ID.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixGetSemId (UINT4 u4Node, const UINT1 au1SemName[4], tOsixSemId * pId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1SemName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1SemName[u1Index];
    }

    u4Node = 0;                    /* unused; */
    if (OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) pId) == OSIX_SUCCESS)
    {
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixCreateTask
 *  Description     : OSIX API used by applications to create a task or 
 *                    process.
 *  Input           : au1TskName   - task name - pointer to char string.
 *                    u4TskPrio    - task priority. lower no. is higher.
 *                    u4StackSize  - Stack size in bytes.
 *                    TskStartAddr - Entry point function
 *                    ai1TskArgs   - a single argument to entry fn
 *                    u4Mode       - Unused.
 *  Output          : pTskId - Pointer to memory location in which the
 *                               id of the created task in returned
 *  Returns         : OSIX_FAILURE / OSIX_SUCCESS
 ************************************************************************/
UINT4
OsixCreateTask (const UINT1 au1TskName[4], UINT4 u4TskPrio,
                UINT4 u4StackSize, void (*TskStartAddr) (INT1 *),
                INT1 ai1TskArgs[1], UINT4 u4Mode, tOsixTaskId * pTskId)
{
    UINT4               u4Arg;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1TskName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1TskName[u1Index];
    }

    u4Mode = 0;                    /* unused; */
    u4Arg = (ai1TskArgs == NULL) ? 0 : (UINT4) (*(UINT4 *) ai1TskArgs);
    if (OsixRscFind (au1Name, OSIX_TSK, pTskId) == OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);    /* Task by this name already exists */
    }
    return (OsixTskCrt (au1Name, u4TskPrio, u4StackSize,
                        (OsixTskEntry) TskStartAddr, u4Arg, pTskId));
}

/************************************************************************
 *  Function Name   : OsixDeleteTask
 *  Description     : This deletes a task of a specified name.
 *  Input           : u4Node     - Unused.
 *                    au1TskName - Name of task to be deleted.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixDeleteTask (UINT4 u4NodeId, const UINT1 au1TskName[4])
{
    tOsixTaskId         TskId;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1TskName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1TskName[u1Index];
    }

    u4NodeId = 0;                /* unused; */
    if (OsixRscFind (au1Name, OSIX_TSK, &TskId) == OSIX_SUCCESS)
    {
        OsixTskDel (TskId);
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixDelayTask
 *  Description     : This delays a task for a specified duration.
 *  Input           : u4Duration     - Delay in units of STUPS.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixDelayTask (UINT4 u4Duration)
{
    OsixTskDelay (u4Duration);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixGetTaskId
 *  Description     : This returns the TaskId given the task name.
 *  Input           : u4Node - Unused
 *                    au1TskName - Name of task.
 *  Output          : pTskId - Pointer to memory location containing TSK-ID.
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE.
 ************************************************************************/
UINT4
OsixGetTaskId (UINT4 u4Node, const UINT1 au1TskName[4], tOsixTaskId * pTskId)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    for (u1Index = 0;
         ((u1Index < OSIX_NAME_LEN) && (au1TskName[u1Index] != '\0'));
         u1Index++)
    {
        au1Name[u1Index] = au1TskName[u1Index];
    }

    u4Node = 0;
    if (OsixRscFind (au1Name, OSIX_TSK, pTskId) == OSIX_SUCCESS)
    {
        return (OSIX_SUCCESS);
    }
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixGetCallerTaskId
 *  Description     : Not supported.
 *  Returns         : OSIX_SUCCESS
 ************************************************************************/
INT4
OsixGetCallerTaskId (void)
{
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixGetCurTaskId
 *  Description     : This returns the TaskId of caller task.
 *  Input           : None.
 *  Returns         : TSK-ID of caller.
 ************************************************************************/
UINT4
OsixGetCurTaskId (void)
{
    tOsixTaskId         TskId;

    if (OsixTskIdSelf (&TskId) == OSIX_FAILURE)
    {
        return (0);
    }
    return ((UINT4) TskId);
}

/************************************************************************
 *  Function Name   : OsixGetNodeIdOfSelf
 *  Description     : Not supported.
 *  Returns         : OSIX_SUCCESS
 ************************************************************************/
UINT4
OsixGetNodeIdOfSelf (void)
{
    return 1;
}

/************************************************************************
 *  Function Name   : OsixIntLock
 *  Description     : Not supported.
 *  Returns         : OSIX_SUCCESS
 ************************************************************************/
UINT4
OsixIntLock ()
{
    return 1;
}

/************************************************************************
 *  Function Name   : OsixIntUnlock
 *  Description     : Not supported.
 *  Returns         : OSIX_SUCCESS
 ************************************************************************/
UINT4
OsixIntUnlock (UINT4 u4L)
{
    u4L = 0;
    return 1;
}

/************************************************************************
 *  Function Name   : OsixSTUPS2Ticks
 *  Description     : Converts STUPS to Ticks.
 *  Inputs          : u4Time - Duration in STUPS.
 *  Returns         : Duration in Ticks.
 ************************************************************************/
UINT4
OsixSTUPS2Ticks (UINT4 u4Time)
{
    return (u4Time * gu4Tps / gu4Stups);
}

/************************************************************************
 *  Function Name   : OsixTicks2STUPS
 *  Description     : Converts Ticks to STUPS.
 *  Inputs          : u4Time - Duration in Ticks.
 *  Returns         : Duration in STUPS.
 ************************************************************************/
UINT4
OsixTicks2STUPS (UINT4 u4Time)
{
    return (u4Time * gu4Stups / gu4Tps);
}

/************************************************************************
 *  Function Name   : OsixSetDbg
 *  Description     : Not supported
 *  Returns         : None.
 ************************************************************************/
#if DEBUG_OSIX == FSAP_ON
VOID
OsixSetDbg (UINT4 u4Value)
{
    u4Value = 0;
    return;
}
#endif
