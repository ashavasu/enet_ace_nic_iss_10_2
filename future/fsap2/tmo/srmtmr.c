/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmtmr.c,v 1.20 2015/04/28 12:18:19 siva Exp $
 *
 * Description:
 * The SRM Timer Module.
 *
 */

/************************************************************************
*                                                                       *
*                          Header  Files                                *
*                                                                       *
*************************************************************************/
#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "osix.h"

#include "utldll.h"
#include "srmtmr.h"
#include "srmtmri.h"
#include "utltrc.h"
#include "utlmacro.h"

/************************************************************************
*                                                                       *
*               Internal Function Prototypes                            *
*                                                                       *
*************************************************************************/

UINT4               gu4TmrInitialized = 0;
/* Indicates if tmr module is initialized (1) or not (0). */
UINT4
 
            TmrDeleteNode (tTimerListId TimerListId, tTmrAppTimer * pAppTimer);

/************************************************************************
*                                                                       *
*               Internal Static Global Variables                        *
*                                                                       *
*************************************************************************/
UINT4               gu4Seconds;
static UINT4        gu4StupsCounter;
static tTmrAppTimerList *gaTimerLists;    /* The timer lists            */
static UINT4        gu4MaxLists;    /* Maximum no. of timer lists */
tOsixSemId          TmrMutex;    /* Mutex used within Tmr      */
#if (DEBUG_TMR == FSAP_ON)
static UINT4        gu4TmrDbg = TMR_DBG_MINOR | TMR_DBG_MAJOR |
    TMR_DBG_CRITICAL | TMR_DBG_FATAL;
#endif

extern UINT4        gu4Tps;
extern UINT4        gu4Stups;

/************************************************************************/
/*  Function Name   : TmrTimerInit                                      */
/*  Description     : Creates timer lists, initializes the active and   */
/*                  : Expired Timer Lists.                              */
/*  Input(s)        :                                                   */
/*                  : pTimerCfg - Pointer to configuration structure.   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrTimerInit (tTimerCfg * pTimerCfg)
{
    UINT4               u4rc = TMR_SUCCESS;
    UINT4               i;
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    if (!gu4TmrInitialized)
    {
        gaTimerLists =
            MEM_MALLOC ((sizeof (tTimerList) * pTimerCfg->u4MaxTimerLists),
                        tTimerList);

        if (gaTimerLists == NULL)
            return TMR_FAILURE;
        gu4MaxLists = pTimerCfg->u4MaxTimerLists;
        memset (gaTimerLists, 0, gu4MaxLists * sizeof (tTimerList));

        for (i = 0; i < gu4MaxLists; ++i)
        {
            TMO_DLL_Init (&gaTimerLists[i].Link);

            TMO_DLL_Init (&gaTimerLists[i].ExpdList);
        }

        MEMSET (au1Name, '\0', (OSIX_NAME_LEN + 4));
        STRCPY (au1Name, "TMMU");
        u4rc = OsixSemCrt (au1Name, &TmrMutex);
        if (u4rc)
        {
            MEM_FREE (gaTimerLists);
            return OSIX_FAILURE;
        }

        OsixSemGive (TmrMutex);
        TMOv2_Start_Timer_Service (gu4Tps / gu4Stups);

        gu4Seconds = 0;
        gu4StupsCounter = gu4Stups;
        gu4TmrInitialized = 1;

        return u4rc;
    }
    else
        return TMR_FAILURE;
}

/************************************************************************/
/*  Function Name   : TmrTimerShutdown                                  */
/*  Description     : Frees resources allocated in Init.                */
/*                  : To be called when closing down timer service.     */
/*                  : Expired Timer Lists.                              */
/*                  : Timer blocks present will have to be reclaimed    */
/*                  : by the applications, before calling this routine. */
/*  Input(s)        :                                                   */
/*                  : pTimerCfg - Pointer to configuration structure.   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrTimerShutdown (void)
{
    if (gu4TmrInitialized == 0)
    {
        return (TMR_FAILURE);
    }

    TMOv2_Stop_Timer_Service ();
    MEM_FREE (gaTimerLists);
    OsixSemDel (TmrMutex);
    gu4TmrInitialized = 0;
    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrCreateTimerList                                */
/*  Description     : Creates a timer list.                             */
/*  Input(s)        :                                                   */
/*                  : au1TaskName  - Name of the task, in order to send */
/*                  :                Event.                             */
/*                  : u4Event - Event corresponding to timeout.         */
/*                  : *CallBackFunction - Callback function to be       */
/*                  : in case it is given. Presence of a callback fn.   */
/*                  : takes higher precedence in the matter of reporting*/
/*                  : timeouts.                                         */
/*  Output(s)       :                                                   */
/*                  : pTimerListId - Handle to created timer list.      */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrCreateTimerList (const UINT1 au1TaskName[4],
                    UINT4 u4Event,
                    void (*CallBackFunction) (tTimerListId),
                    tTimerListId * pTimerListId)
{
    UINT4               u4Count;
    tTmrAppTimerList   *pTimerList = 0;
    tOsixTaskId         TaskId = 0;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    if (CallBackFunction == NULL && au1TaskName == NULL)
        return (TMR_FAILURE);

    /* If the expiry notification is via an event to the task, then,
     * ensure the task has been created.
     * The TaskId is required to post the expiry event event.
     * See: TmrProcessTick.
     */
    if (!CallBackFunction)
    {
        MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
        for (u1Index = 0;
             ((u1Index < OSIX_NAME_LEN) && (au1TaskName[u1Index] != '\0'));
             u1Index++)
        {
            au1Name[u1Index] = au1TaskName[u1Index];
        }

        if (OsixRscFind (au1Name, OSIX_TSK, &TaskId) == OSIX_FAILURE)
        {
            TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME,
                      "Invalid Task %s .. \n", au1Name));
            return (TMR_FAILURE);
        }
    }

    TMR_ENTER_CS ();
    for (u4Count = 0; u4Count < gu4MaxLists; u4Count++)
        if (gaTimerLists[u4Count].u4Status == TMR_FREE)
        {
            gaTimerLists[u4Count].u4Status = TMR_USED;
            pTimerList = &gaTimerLists[u4Count];
            break;
        }

    if (u4Count == gu4MaxLists)
    {
        TMR_LEAVE_CS ();
        return TMR_FAILURE;
    }

    TMR_LEAVE_CS ();

    *pTimerListId = (tTimerListId) pTimerList;
    pTimerList->TskId = TaskId;
    pTimerList->u4Event = u4Event;
    pTimerList->i4RemainingTime = 0;
    pTimerList->CallBackFunction = CallBackFunction;

    TMO_DLL_Init (&pTimerList->Link);
    TMO_DLL_Init (&pTimerList->ExpdList);

    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrDeleteTimerList                                */
/*  Description     : Deletes a timer list.                             */
/*  Input(s)        :                                                   */
/*                  : TimerListId - Handle to the timer list.           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrDeleteTimerList (tTimerListId TimerListId)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;

    TMR_ENTER_CS ();

    if (pTimerList->u4Status != TMR_USED)
    {
        TMR_LEAVE_CS ();
        return TMR_FAILURE;
    }
    pTimerList->u4Status = TMR_FREE;
    TMR_LEAVE_CS ();
    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrStartTimer                                     */
/*  Description     : Starts a timer of a specified duration.           */
/*                  : The duration has to be specified in terms of STUPS*/
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list in which the timer   */
/*                  :               is to be started                    */
/*                  : pReference -  The timer block which will get      */
/*                  :               linked into the timer list          */
/*                  : u4Duration -  Timeout value.                      */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrStartTimer (tTimerListId TimerListId,
               tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmrNode;
    tTMO_DLL_NODE      *p, *n;
    tTMO_DLL_NODE      *pDLLNode;
    tTMO_DLL           *pList = &pTimerList->Link;
    tTMO_DLL           *pDLL;
    UINT4               elem;
    UINT4               u4val;
    UINT4               u4NumBlocks;
    UINT4               u4Incoming = u4Duration;

    u4val = TmrLock ();

    /* The magic number field has this specific value if the */
    /* timer is running. So, stop the timer and then start.  */
    /* This prevents problems due of buggy application code. */
    if ((pAppTimer->u4MagicNumber) == 0x01020304)
    {
        TmrDeleteNode (TimerListId, pAppTimer);
    }

    if (u4Incoming == 0)
    {
        /* We don't allow a timer of 0 seconds to be started.
         * But if used, it results in the stopping of a running
         * timer in the spirit of the unix alarm call
         */
        TmrUnLock (u4val);
        return TMR_FAILURE;
    }

    pAppTimer->i4RemainingTime = u4Duration;

    u4NumBlocks = TMO_DLL_Count (pList);
    if (u4Duration <= (UINT4) pTimerList->i4RemainingTime || (u4NumBlocks == 0))
    {
        tTMO_DLL_NODE      *pNextDLLNode = TMO_DLL_First (pList);
        if (pNextDLLNode == NULL)
            pNextDLLNode = (tTMO_DLL_NODE *) pList;

        pAppTimer->u4MagicNumber = 0x01020304;

        TMO_DLL_Insert_In_Middle (pList, (tTMO_DLL_NODE *) pList,    /* prev */
                                  (tTMO_DLL_NODE *) pAppTimer,    /* Mid  */
                                  pNextDLLNode);    /* Next */

        /* if there's a second elem */
        if (u4NumBlocks >= 1)
        {
            ((tTmrAppTimer *) pNextDLLNode)->i4RemainingTime =
                pTimerList->i4RemainingTime - u4Duration;
        }
        pTimerList->i4RemainingTime = u4Duration;
    }
    else
    {
        INT4                i = 0;
        u4Duration -= pTimerList->i4RemainingTime;

        pDLL = &pTimerList->Link;
        p = pDLLNode = TMO_DLL_First (pDLL);

        TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
        {
            /* Skip first */
            if (!i)
            {
                i++;
                continue;
            }
            elem = pTmrNode->i4RemainingTime;
            if (elem > u4Duration)
            {
                break;
            }
            i++;
            u4Duration -= elem;
            p = (tTMO_DLL_NODE *) pTmrNode;
        }

        n = TMO_DLL_Next (pDLL, p);

        pAppTimer->i4RemainingTime = u4Duration;
        pAppTimer->u4MagicNumber = 0x01020304;

        if (n == NULL)
        {
            n = &pDLL->Head;
            TMO_DLL_Insert_In_Middle (pDLL, p, (tTMO_DLL_NODE *) pAppTimer, n);
        }
        else
        {
            TMO_DLL_Insert_In_Middle (pDLL, p, (tTMO_DLL_NODE *) pAppTimer, n);
            ((tTmrAppTimer *) n)->i4RemainingTime -= u4Duration;
        }
    }
    TmrUnLock (u4val);

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "Added %d .. \n",
              u4Incoming));
#if (DEBUG_TMR == FSAP_ON)
    TmrDumpList ((UINT4) pTimerList);
#endif
    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrStopTimer                                      */
/*  Description     : Stops a timer, if it is active                    */
/*                  : If it has expired the return value indicates so   */
/*                  : In either case the timer block is removed from    */
/*                  : the timer list.                                   */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The Handle to the timer list.       */
/*                  : pReference  - Timer block.                        */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrStopTimer (tTimerListId TimerListId, tTmrAppTimer * pAppTimer)
{
    UINT4               u4val;
    UINT4               u4rc;

    u4val = TmrLock ();
    u4rc = TmrDeleteNode (TimerListId, pAppTimer);
    TmrUnLock (u4val);

    return (u4rc);
}

UINT4
TmrDeleteNode (tTimerListId TimerListId, tTmrAppTimer * pAppTimer)
{
    tTMO_DLL           *pDLL;
    tTmrAppTimer       *pNextTimer;
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    UINT4               u4rc = TMR_SUCCESS;

    if (!pAppTimer || !pTimerList)
        return (TMR_FAILURE);

    pDLL = &pTimerList->Link;

   /*************************************************
    *** Check if the timer is present in any list ****
    **************************************************/
    if (!TMO_DLL_Find (pDLL, (tTMO_DLL_NODE *) pAppTimer))
    {
        pDLL = &pTimerList->ExpdList;
        if (!TMO_DLL_Find (pDLL, (tTMO_DLL_NODE *) pAppTimer))
        {
            return TMR_FAILURE;
        }
        u4rc = TMR_EXPIRED;
    }

    if (u4rc != TMR_EXPIRED)
    {
        pNextTimer =
            (tTmrAppTimer *) TMO_DLL_Next (pDLL, (tTMO_DLL_NODE *) pAppTimer);

        if (pAppTimer == (tTmrAppTimer *) TMO_DLL_First (pDLL))
        {
            if (pNextTimer)
            {
                pTimerList->i4RemainingTime += pNextTimer->i4RemainingTime;
            }
            else
            {
                pTimerList->i4RemainingTime = 0;
            }
        }
        else
        {
            if (pNextTimer)
            {
                pNextTimer->i4RemainingTime += pAppTimer->i4RemainingTime;
            }
            else
            {
                /* rear end. nothing to do. */
            }
        }

        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "Stopped: %d\n",
                  pAppTimer->u4Data));
    }
    TMO_DLL_Delete (pDLL, (tTMO_DLL_NODE *) pAppTimer);
    pAppTimer->u4MagicNumber = 0;

#if (DEBUG_TMR == FSAP_ON)
    TmrDumpList ((UINT4) pTimerList);
#endif
    return u4rc;
}

/************************************************************************/
/*  Function Name   : TmrResizeTimer                                    */
/*  Description     : Resizes a running timer.                          */
/*                  : Trying to resize a timer to a value into the past */
/*                  : will result in failure.                           */
/*  Input(s)        :                                                   */
/*                  : TimerListId - Handle to the timer list.           */
/*                  : pReference  - Timer block.                        */
/*                  : u4Duration  - Resized value of timeout.           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrResizeTimer (tTimerListId TimerListId,
                tTmrAppTimer * pReference, UINT4 u4Duration)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmrNode;
    tTMO_DLL           *pList = &pTimerList->Link;
    INT4                i = 0;
    UINT4               u4CumTicksRem;
    UINT4               u4Incoming;
    UINT4               u4rc;
    UINT4               u4val;

    u4val = TmrLock ();
    u4CumTicksRem = pTimerList->i4RemainingTime;
    TMO_DLL_Scan (pList, pTmrNode, tTmrAppTimer *)
    {
        if (!i)
        {
            if (pTmrNode == pReference)
                break;
            i++;
            continue;
        }
        u4CumTicksRem += pTmrNode->i4RemainingTime;
        if (pTmrNode == pReference)
            break;
    }
    if (pTmrNode == NULL)
    {
        TmrUnLock (u4val);
        return TMR_FAILURE;
    }

    u4Incoming = u4Duration;

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "You want  %c [%d] to be resized to %d\n",
              pTmrNode->u4Data, u4CumTicksRem, u4Duration));
    if (u4Duration < u4CumTicksRem)
    {
        TmrUnLock (u4val);
        return TMR_FAILURE;
    }
    else if (u4CumTicksRem == u4Duration)
    {
        TmrUnLock (u4val);
        return TMR_SUCCESS;
    }
    else
    {
        TmrUnLock (u4val);
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "stopping..\n"));
        TmrStopTimer (TimerListId, pTmrNode);
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "restarting..\n"));
        u4rc = TmrStartTimer (TimerListId, pTmrNode, u4Incoming);
    }

    return u4rc;
}

/************************************************************************/
/*  Function Name   : TmrGetExpiredTimers                               */
/*  Description     : API to be called to retrieve Expired timers.      */
/*  Input(s)        :                                                   */
/*                  : TimerListId -  The timer list from which to get.  */
/*  Output(s)       :                                                   */
/*                  : ppExpiredTimers - Contains ptr. to expired timer. */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrGetExpiredTimers (tTimerListId TimerListId, tTmrAppTimer ** ppExpiredTimers)
{
    UINT4               u4val;

    tTimerList         *pTimerList = (tTimerList *) TimerListId;

    u4val = TmrLock ();
    *ppExpiredTimers = (tTmrAppTimer *) TMO_DLL_Get (&pTimerList->ExpdList);
    if (*ppExpiredTimers)
    {
        (*ppExpiredTimers)->u4MagicNumber = 0;
    }
    TmrUnLock (u4val);

    return (*ppExpiredTimers ? TMR_SUCCESS : TMR_FAILURE);
}

/************************************************************************/
/*  Function Name   : TmrGetRemainingTime                               */
/*  Description     : API to get the time to expire for a timer.        */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list in which the timer is*/
/*                  : pReference -  The Timer.                          */
/*  Output(s)       :                                                   */
/*                  : pu4RemainingTime - Time to expire                 */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrGetRemainingTime (tTimerListId TimerListId,
                     tTmrAppTimer * pReference, UINT4 *pu4RemainingTime)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmrNode;
    tTMO_DLL           *pList = &pTimerList->Link;
    INT4                i = 0;

    *pu4RemainingTime = pTimerList->i4RemainingTime;

    TMO_DLL_Scan (pList, pTmrNode, tTmrAppTimer *)
    {
        if (!i)
        {
            if (pTmrNode == pReference)
                break;
            i++;
            continue;
        }
        *pu4RemainingTime += pTmrNode->i4RemainingTime;
        if (pTmrNode == pReference)
            break;
    }

    if (pTmrNode == NULL)
        return TMR_FAILURE;

    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrPrintTimerStatistics                           */
/*  Description     : API to get info on active timers.                 */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list for which stats. is  */
/*                  : sought.                                           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrPrintTimerStatistics (tTimerListId TimerListId)
{
    tTMO_DLL           *pDLL;
    tTmrAppTimerList   *pTimerList;
    tTmrAppTimer       *pTmrNode;
    char                ai1buf[100];

    pTimerList = (tTimerList *) TimerListId;
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", "[ "));
    pDLL = &pTimerList->Link;
    TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
    {
        SPRINTF (ai1buf, "%ld ", pTmrNode->i4RemainingTime);
        if (pTmrNode->i4RemainingTime < 0)
        {
            TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_FATAL, TMR_MODNAME,
                      "panic negative val.\n"));
        }
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", " ]\n\n"));

    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrProcessTick                                    */
/*  Description     : Function called on every time-tick. Updates the   */
/*                  : timer list and intimates the tasks in case of     */
/*                  : timeout.                                          */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
void
TmrProcessTick (void)
{
    tTmrAppTimerList   *pTimerList;
    tTMO_DLL           *pDLL;
    tTMO_DLL_NODE      *pDLLNode;
    tTmrAppTimer       *pTmrNode;
    UINT4               u4Count;
    UINT4               u4Found = 0;
    tTMO_DLL_NODE      *pStart, *pEnd;

    gu4StupsCounter--;
    if (gu4StupsCounter == 0)
    {
        gu4Seconds++;
        gu4StupsCounter = gu4Stups;
    }
    for (pTimerList = &gaTimerLists[0], u4Count = 0;
         u4Count < gu4MaxLists; u4Count++, pTimerList++, u4Found = 0)
    {
        if (gaTimerLists[u4Count].u4Status == TMR_USED)
            if (--pTimerList->i4RemainingTime == 0)
            {
                pDLL = &pTimerList->Link;
                pDLLNode = TMO_DLL_Get (pDLL);
                TMO_DLL_Add (&pTimerList->ExpdList, pDLLNode);

                TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME,
                          "Timeout: %d\n",
                          ((tTmrAppTimer *) pDLLNode)->u4Data));

                pStart = pEnd = NULL;
                TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
                {
                    if (pTmrNode->i4RemainingTime != 0)
                    {
                        break;
                    }
                    pEnd = (tTMO_DLL_NODE *) pTmrNode;
                    u4Found = 1;
                }
                if (u4Found)
                {
                    do
                    {
                        pDLL = &pTimerList->Link;
                        pDLLNode = TMO_DLL_Get (pDLL);
                        TMO_DLL_Add (&pTimerList->ExpdList, pDLLNode);
                        pStart = pDLLNode;
                    }
                    while (pStart != pEnd);
                }

                if ((pDLLNode = TMO_DLL_First (pDLL)))
                    pTimerList->i4RemainingTime =
                        ((tTmrAppTimer *) pDLLNode)->i4RemainingTime;

#if (DEBUG_TMR == FSAP_ON)
                TmrDumpList (u4Count);
#endif
                if (pTimerList->CallBackFunction)
                    (pTimerList->CallBackFunction) ((tTimerListId) pTimerList);
                else
                    OsixEvtSend (pTimerList->TskId, pTimerList->u4Event);
            }
    }
}

/************************************************************************/
/*  Function Name   : TmrDumpList                                       */
/*  Description     : Used to dump the timer list.                      */
/*                  : Used for debugging purposes.                      */
/*  Input(s)        : u4Count - Specifies the timer list.               */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
VOID
TmrDumpList (UINT4 u4Count)
{
    UINT4               pos;
    tTMO_DLL           *pDLL;
    tTmrAppTimerList   *pTimerList;
    tTmrAppTimer       *pTmrNode;

    char                ai1buf[400];
    /* Note: This size is to be increased if you have many many timers
       in the list. */

    if (u4Count < 1000)
    {
        pTimerList = &gaTimerLists[u4Count];
        pos = SPRINTF (ai1buf, "list-%ld: ", u4Count);
    }
    else
    {
        pTimerList = (tTimerList *) u4Count;
        pos = SPRINTF (ai1buf, "list-%ld: ",
                       (u4Count -
                        (UINT4) gaTimerLists) / sizeof (gaTimerLists[0]));
    }

    pos += SPRINTF (ai1buf + pos, "<%ld> [ ", pTimerList->i4RemainingTime);

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));

    pDLL = &pTimerList->Link;
    TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
    {
        SPRINTF (ai1buf, "%ld ", pTmrNode->i4RemainingTime);
        if (pTmrNode->i4RemainingTime < 0)
        {
            TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_FATAL, TMR_MODNAME,
                      "PANIC NEGATIVE VAL.\n"));
        }
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", "]["));
    /* List as mnemonics  - provided user had filled the u4Data field. */
    TMO_DLL_Scan (pDLL, pTmrNode, tTmrAppTimer *)
    {
        SPRINTF (ai1buf, "%ld ", pTmrNode->u4Data);
        if (pTmrNode->i4RemainingTime < 0)
        {
            TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_FATAL, TMR_MODNAME,
                      "panic negative val.\n"));
        }
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", ai1buf));
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MAJOR, TMR_MODNAME, "%s", " ]\n\n"));

}

/************************************************************************/
/*  Function Name   : TmrGetNextExpiredTimer                            */
/*  Description     : API to get the expired timers from a timer list.  */
/*  Input(s)        : TimerListId - The timer list.                     */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
tTmrAppTimer       *
TmrGetNextExpiredTimer (tTimerListId TimerListId)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmr;
    UINT4               u4val;

    u4val = TmrLock ();
    pTmr = (tTmrAppTimer *) TMO_DLL_Get (&pTimerList->ExpdList);
    if (pTmr)
    {
        pTmr->u4MagicNumber = 0;
    }
    TmrUnLock (u4val);
    return pTmr;
}

/************************************************************************/
/*  Function Name   : TmrSetDbg                                         */
/*  Description     : API for changing debug level at run time.         */
/*                  :                                                   */
/*  Input(s)        : u4Value - New value of debug mask.                */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
#if DEBUG_TMR == FSAP_ON
VOID
TmrSetDbg (UINT4 u4Value)
{
    TMR_DBG_FLAG = u4Value;
}
#endif

/*****************************************************************************/
/* Function     : TmrStart                                                   */
/*                                                                           */
/* Description  : Sets the timer id in the pTimer struct. Calls the          */
/*                TmrStartTimer to start the timer.This function is provided */
/*                to make all the application use the timer library in the   */
/*                same way.                                                  */
/*                                                                           */
/* Input        : TimerListId  : The timer list id                           */
/*                pTimer       : Pointer to timer block                      */
/*                u1TimerId    : The id of the timer to be started           */
/*                u4Sec        : The seconds value of the timer.             */
/*                u4MilliSec   : The milliseconds value of the timer.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrStart (tTimerListId TimerListId, tTmrBlk * pTimer, UINT1 u1TimerId,
          UINT4 u4Sec, UINT4 u4MilliSec)
{
    UINT4               u4Duration;
    pTimer->u1TimerId = u1TimerId;

    u4Duration = (u4Sec * gu4Stups) + ((gu4Stups * u4MilliSec) / 1000);
    if (TmrStartTimer (TimerListId, &(pTimer->TimerNode),
                       u4Duration) != TMR_SUCCESS)
    {
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
                  "TmrStart Failed for timer 0x%x\n", pTimer));
        return (TMR_FAILURE);
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Started timer 0x%x, %ld units \n", pTimer, u4Duration));
    return TMR_SUCCESS;
}

/*****************************************************************************/
/* Function     : TmrRestart                                                 */
/*                                                                           */
/* Description  : Restarts the timer with the specified duration             */
/*                                                                           */
/* Input        : TimerListId :  The timer list id                           */
/*                pTimer       : pointer to timer block                      */
/*                u1TimerId    : The id of the timer to be started           */
/*                u4Sec        : The seconds value of the timer.             */
/*                u4MilliSec   : The milliseconds value of the timer.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrRestart (tTimerListId TimerListId, tTmrBlk * pTimer,
            UINT1 u1TimerId, UINT4 u4Sec, UINT4 u4MilliSec)
{
    UINT4               u4Duration;

    u4Duration = (u4Sec * gu4Stups) + ((gu4Stups * u4MilliSec) / 1000);

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Restarting timer 0x%x, TmrId %s NumTicks %ld\n",
              pTimer, u1TimerId, u4Duration));

    if (TmrStopTimer (TimerListId, &(pTimer->TimerNode)) != TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    if (TmrStart (TimerListId, pTimer, u1TimerId, u4Sec, u4MilliSec) !=
        TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "TMR Restarted\n"));
    return (TMR_SUCCESS);
}

/*****************************************************************************/
/* Function     : TmrStop                                                    */
/*                                                                           */
/* Description  : Deletes the timer from the timer list.                     */
/*                                                                           */
/* Input        : pTimer       : pointer to timer block                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrStop (tTimerListId TimerListId, tTmrBlk * pTimer)
{
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Stopping timer 0x%x, TmrId %s\n", pTimer, pTimer->u1TimerId));

    if (TmrStopTimer (TimerListId, &(pTimer->TimerNode)) != TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    return (TMR_SUCCESS);
}
