	.file	"tmo_386.s"

	.text

	.align 4

	.globl TmoSched

TmoSched:
TmoCtxtSave:
    movl  p_curtcb,%eax          # load eax with p_curtcb
    addl  $12,%eax               # load eax with p_curtcb->x
    pushl  %ecx                  # save ecx, which will be overwritten.
    movl  (%eax) , %ecx          # move *p_curtcb->x to ecx.
    movl  %ecx , pTMOv2_Etcb     # move ecx to pTMOv2_Etcb
    popl  %ecx                   # restore ecx.
    addl  $4,%eax                # set eax to p_curtcb->context[]

    movl  %ecx,4(%eax)           # Save the regs ...
    movl  %edx,8(%eax)           #
    movl  %ebx,12(%eax)          # 
    movl  %ebp,16(%eax)          #
    movl  %esp,%ecx              #
    addl  $4,%ecx                #
    movl  %ecx,20(%eax)          #
    movl  %esi,24(%eax)          #
    movl  %edi,28(%eax)          # ...
    pushf
    popl   %ecx
    movl  %ecx,32(%eax)          # save the flags regsiter.  
    pop   %ecx
    movl  %ecx,36(%eax)          # save the return value.
    call  TmoReSched     

TmoCtxtSwitch:
    movl  p_curtcb,%eax          # load eax with p_curtcb
    addl  $12,%eax               # add 12 to get to p_curtcb->x
    pushl %ecx                   # save ecx
    movl  (%eax) , %ecx          # load ecx with *p_curtcb->x (=pTMOv2_Etcb)
    movl  %ecx, pTMOv2_Etcb      # set pTMOv2_Etcb for new task.
    popl  %ecx                   # restore ecx
    addl  $4,%eax                # set eax to p_curtcb->context[]

    movl  4(%eax),%ecx           # Load the regs ...
    movl  8(%eax),%edx           #
    movl  12(%eax),%ebx          #
    movl  16(%eax),%ebp          #
    movl  20(%eax),%esp          #
    movl  24(%eax),%esi          #
    movl  28(%eax),%edi          # ...
    pushl 32(%eax)         
    popf                         # load the flags register.
    jmp   *36(%eax)              # jump to the return value (new IP).
