/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utltrc.h,v 1.14 2015/04/28 12:20:25 siva Exp $
 *
 * Description: Contains the Macros and definitions for tracing and
 *              dumping, which forms a part of FutureUTL library
 *
 *******************************************************************/
#ifndef _UTLTRC_H
#define _UTLTRC_H

#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utlmacro.h"

#define CONST  const

/* System-wide constants */
#define  UTL_DBG_OFF           0x00000000
/* initial value for all module trace flags */

#define  UTL_DBG_ALL           0xffffffff
/* all debugs enabled */

#define  UTL_DBG_INIT_SHUTDN   0x00000001
/* for debugging module init and shutdown */

#define  UTL_DBG_CTRL_IF       0x00000002
/* for debugging of control messages got from other modules */

#define  UTL_DBG_STATUS_IF     0x00000004
/* for debugging status indications from other modules */

#define  UTL_DBG_SNMP_IF       0x00000008
/* for debugging of all SNMP messages */

#define  UTL_DBG_TMR_IF        0x00000010
/* for debugging timer start, stop and timeout */

#define  UTL_DBG_BUF_IF        0x00000020
/* for debugging buffer alloc, release and
 * major buffer operations done by this module */

#define  UTL_DBG_MEM_IF        0x00000040
/* for debugging memory pool creation & deletion
 * and memory alloc, release */

#define  UTL_DBG_RX            0x00000100
/* for debugging of received packets */

#define  UTL_DBG_TX            0x00000200
/* for debugging of transmitted packets */


#define  DUMP_DIRN_NONE        0x00000000
#define  DUMP_DIRN_NA          0x00000000
#define  DUMP_DIRN_INC         0x00000001
#define  DUMP_DIRN_OUT         0x00000002



/* Prototype Declarations */
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


#ifdef VAR_ARGS_SUPPORT
VOID UtlTrcLog   ARG_LIST ((UINT4 u4Flag, UINT4 u4Value, CONST char *pi1Name,
                            CONST char *pi1Fmt, ...));
VOID UtlSysTrcLog ARG_LIST ((UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST char *pi1ModId, CONST char *pi1Name,
                            CONST char *pi1Fmt, ...));
#else
VOID UtlTrcLog ();
VOID UtlSysTrcLog ();
#endif /* VAR_ARGS_SUPPORT */

VOID UtlTrcPrint ARG_LIST ((const char *pi1Msg));


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif


#endif /* _UTLTRC_H */
