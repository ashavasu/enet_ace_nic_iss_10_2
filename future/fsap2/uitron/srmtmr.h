/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmtmr.h,v 1.15 2015/04/28 12:20:25 siva Exp $
 *
 * Description: Timer prototypes
 *
 *******************************************************************/


#ifndef SRM_TMR_H
#define SRM_TMR_H

#include "utldll.h"
/************************************************************************
*                                                                       *
*                      Defines and Typedefs                             *
*                                                                       *
*************************************************************************/
#define  TMR_SUCCESS             0
#define  TMR_EXPIRED             2
#define  TMR_FAILURE        (~0UL)
/********* uITRON-specific declarations begin *********/
#define  ALM_NO        		 1 /* used to execute continuously*/
/********* uITRON-specific declarations end *********/

/* Debug levels */
#define  TMR_DBG_MINOR         0x1
#define  TMR_DBG_MAJOR         0x2
#define  TMR_DBG_CRITICAL      0x4
#define  TMR_DBG_FATAL         0x8

typedef  struct TimerCfg
{
    UINT4 u4MaxTimerLists; /* Max Number of Timer Lists in the system */
} tTimerCfg;

typedef UINT4 tTimerListId;

typedef struct TmrAppTimer
{
   /*
    * Link, should be the first field. Even if you include this in your
    * applications make sure it is the first field.
    */
   tTMO_DLL_NODE       Link;
   UINT4               u4Data;
   INT4                i4RemainingTime;     /* Ticks Until Expiration */
   UINT4               u4MagicNumber;       /* Detect starting running timer */
}tTmrAppTimer;


typedef VOID (*tTmrExpFn)(VOID *);

typedef struct TmrDesc
{
    tTmrExpFn  TmrExpFn;
    INT2       i2Offset;      /* if the field is -1, the function does
                               * not take any parameter
                               */
    UINT1      au1Rsvd[2];   /* included for 4 byte allignment */
} tTmrDesc;

typedef struct TmrBlk {
    tTmrAppTimer    TimerNode;
    UINT1           u1TimerId;
    UINT1           au1Rsvd[3];
} tTmrBlk;

/************************************************************************
*                                                                       *
*                      Function Prototypes                              *
*                                                                       *
*************************************************************************/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

UINT4
TmrTimerInit (tTimerCfg *pTimerCfg);

UINT4
TmrCreateTimerList (const UINT1 au1TaskName [4],
                    UINT4 u4Event,
                    void (*CallBackFunction)(tTimerListId),
                    tTimerListId *pTimerListId);

UINT4
TmrDeleteTimerList (tTimerListId TimerListId);

/* Following 3 APIs are deprecated */
/* See the new APIs further down   */
/* Refer documentation for details */
UINT4
TmrStartTimer (tTimerListId TimerListId,
               tTmrAppTimer *pReference,
               UINT4 u4Duration);

UINT4
TmrStopTimer (tTimerListId TimerListId,
              tTmrAppTimer *pReference);

UINT4
TmrResizeTimer (tTimerListId TimerListId,
                tTmrAppTimer *pReference,
                UINT4 u4Duration);

/* The following new Timer APIs are created to simplify */
/* the application interface and to bring in a certain  */
/* amount of uniformity to the application code. These  */
/* shall be used in preference to the older APIs        */
/* They new APIs are:
   - TmrStart
   - TmrStop
   - TmrRestart
 */
UINT4
TmrStart (tTimerListId TimerListId, tTmrBlk *pTimer, UINT1 u1TimerId,
          UINT4 u4Sec, UINT4 u4MilliSec);

UINT4
TmrStop (tTimerListId TimerListId, tTmrBlk *pTimer);

UINT4
TmrRestart (tTimerListId TimerListId, tTmrBlk *pTimer, UINT1 u1TimerId,
            UINT4 u4Sec, UINT4 u4MilliSec);

UINT4
TmrGetExpiredTimers (tTimerListId TimerListId,
                     tTmrAppTimer **ppExpiredTimers);

UINT4
TmrGetRemainingTime (tTimerListId TimerListId,
                     tTmrAppTimer *pReference,
                     UINT4 *pu4RemainingTime);

UINT4
TmrPrintTimerStatistics (tTimerListId TimerListId);

UINT4
TmrShutDownTimer ();

void
TmrProcessTick ();

tTmrAppTimer *
TmrGetNextExpiredTimer(tTimerListId TimerListId);

VOID
TmrSetDbg(UINT4 u4Value);
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif
