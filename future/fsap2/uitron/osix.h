/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osix.h,v 1.21 2015/04/28 12:20:25 siva Exp $
 *
 * Description: Osix Prototypes
 *
 *******************************************************************/

#ifndef _OSIX_H_
#define _OSIX_H_

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif
#include "fsap.h"

/*** Configurable parameters.              ***/
#define  OSIX_NAME_LEN           4
#define  OSIX_DEF_MSG_LEN        4
#define  OSIX_MAX_Q_MSG_LEN      4
#define  OSIX_TPS             1000
#define  OSIX_STUPS              1
#define  OSIX_MAX_QUES         200

/*** Shared between tmo.c and wrap.c       ***/
#define  OSIX_TSK                0
#define  OSIX_SEM                1
#define  OSIX_QUE                2
#define  OSIX_TRUE               1
#define  OSIX_FALSE              0
#define  OSIX_RSC_INV         NULL

#define OSIX_TSKID_BASE          3
#define OSIX_SEMID_BASE          3
#define OSIX_QUEID_BASE          1

/* 
 * TOT_TSKS:
 * +1 to make allowance for Scavenger Task,
 * +1 since taskid can not take the value 1
 * +1 since TMT_TSKID takes the value 2
 */
#define OSIX_TOT_TSKS    (OSIX_MAX_TSKS + OSIX_TSKID_BASE)

/*
 * TOT_SEMS:
 * +1 to make allowance for Mutual exclusion semaphore
 *     to add or delete elements from name-id-mapping list,
 * +1 since semid can not take the value  1
 * +OSIX_MAX_QUES to accomodate sem used for Que simulation
 */
#define OSIX_TOT_SEMS    (OSIX_MAX_SEMS + OSIX_SEMID_BASE)

#define OSIX_TOT_QUES    (OSIX_MAX_QUES + OSIX_QUEID_BASE)

/*** OS related constants used by fsap     ***/

/* LOW_PRIO  - Priority of lowest priority task.
 * HIGH_PRIO - Priority of highest priority task.
 */
#define  FSAP_LOW_PRIO          31
#define  FSAP_HIGH_PRIO          0
#define  OS_LOW_PRIO             1
#define  OS_HIGH_PRIO           99

    /* Scheduling Algorithm */

#define OSIX_SCHED_RR             (1 << 16)
#define OSIX_SCHED_FIFO           (1 << 17)
#define OSIX_SCHED_OTHER          (1 << 18)
 

/*** Wait flags used in OsixEvtRecv()      ***/
#undef   OSIX_WAIT
#undef   OSIX_NO_WAIT
#define  OSIX_WAIT              (~0UL)
#define  OSIX_NO_WAIT            0

/***          FILE modes                   ***/
#define OSIX_FILE_RO             0
#define OSIX_FILE_WO             1
#define OSIX_FILE_RW             2
#define OSIX_FILE_CR             4
#define OSIX_FILE_AP             8

/***          Time Units                   ***/
#define OSIX_SECONDS          1
#define OSIX_CENTI_SECONDS    2
#define OSIX_MILLI_SECONDS    3
#define OSIX_MICRO_SECONDS    4
#define OSIX_NANO_SECONDS     5

/*** Prototype of an Entry Point Function. ***/
typedef VOID (*OsixTskEntry)(INT1 *);

#define OsixBHEnable()
#define OsixBHDisable()

/***           FSAP APIs                   ***/
UINT4      OsixRscAdd           ARG_LIST ((UINT1[], UINT4, UINT4));
VOID       OsixRscDel           ARG_LIST ((UINT4, UINT4));
UINT4      OsixRscFind          ARG_LIST((UINT1[], UINT4, UINT4*));
UINT4      OsixTskCrt           ARG_LIST((UINT1[], UINT4, UINT4,
                                          VOID(*)(INT1 *), INT1 *, tOsixTaskId*));
VOID       OsixTskDel           ARG_LIST((tOsixTaskId));
UINT4      OsixTskDelay         ARG_LIST((UINT4));
UINT4      OsixDelay            ARG_LIST ((UINT4, INT4));
UINT4      OsixTskIdSelf        ARG_LIST((tOsixTaskId*));
UINT4      OsixEvtSend          ARG_LIST((tOsixTaskId, UINT4));
UINT4      OsixEvtRecv          ARG_LIST((tOsixTaskId, UINT4, UINT4, UINT4*));
UINT4      OsixInitialize       ARG_LIST((VOID));
UINT4      OsixShutDown         ARG_LIST((VOID));
UINT4      OsixSemCrt           ARG_LIST((UINT1[], tOsixSemId*));
VOID       OsixSemDel           ARG_LIST((tOsixSemId));
UINT4      OsixSemGive          ARG_LIST((tOsixSemId));
UINT4      OsixSemTake          ARG_LIST((tOsixSemId));
UINT4      OsixQueCrt           ARG_LIST((UINT1[], UINT4, UINT4, tOsixQId*));
VOID       OsixQueDel           ARG_LIST((tOsixQId));
UINT4      OsixQueSend          ARG_LIST((tOsixQId, UINT1*, UINT4));
UINT4      OsixQueRecv          ARG_LIST((tOsixQId, UINT1*, UINT4, INT4));
UINT4      OsixQueNumMsg        ARG_LIST((tOsixQId, UINT4*));
UINT4      OsixGetSysUpTime     ARG_LIST ((VOID));

INT4       FileOpen        (const UINT1 *pu1FileName, INT4 i4Mode);
INT4       FileClose       (INT4 i4Fd);
UINT4      FileRead        (INT4 i4Fd, CHR1 *pBuf, UINT4 i4Count);
UINT4      FileWrite       (INT4 i4Fd, const CHR1 *pBuf, UINT4 i4Count);
INT4       FileDelete      (const UINT1 *pu1FileName);
UINT4      OsixSetLocalTime (void);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /* _OSIX_H_ */
