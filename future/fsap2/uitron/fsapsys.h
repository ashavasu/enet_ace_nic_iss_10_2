/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapsys.h,v 1.10 2015/04/28 12:20:25 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
