/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osixuit.c,v 1.16 2015/04/28 12:20:25 siva Exp $
 *
 * Description: Contains OSIX reference code for uITRON.
 *              All basic OS facilities used by protocol software
 *              from FS, use only these APIs.
 */

#include "osxinc.h"
#include "osix.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
/*******************************************************************
*
*               Internal Function Prototypes
*
********************************************************************/
#define OSIX_SCAVENGER_TASK    (UINT1 *)"SCAV"
#define OSIX_SCAVENGER_PRIO                 1
#define UITRON_SEM_ID_MUTEX                 2
#define OSIX_SCAVENGER_EVENT           0x8000
#define UITRON_MAX_TASK_STACK_SIZE      10000

/* The basic structure maintaining the name-to-id mapping */
/* of OSIX resources. 3 arrays - one for tasks, one for   */
/* semaphores and one for queues are maintained by OSIX.  */
/* Each array has this structure as the basic element. We */
/* use this array to store events for tasks also.         */

/* The description of fields of the structures below is as follows */
/*   u4OsId   - the thread id returned by the OS                   */
/*   u4RscId  - the id returned by the OS                          */
/*   u2Free   - whether this structure is free or used             */
/*   u4DelFlag- Delete flag used by Scavenger task                 */
/*   u4SusFlag- Suspend flag used by Scavenger task                */
/*   u4Events - for event simulation; used only for tasks          */
/*   u4Arg    - argument for Entry Point Function                  */
/*   pTskStrtAddr - Entry Point function.                          */
/*   TskMutex - Mutex used to synchronize send/recv. of events.    */
/*   au1Name  - name is always multiple of 4 characters in length  */
typedef struct OsixRscTskStruct
{
    ID                  u4OsId;
    UINT2               u2Free;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    UINT4               u4DelFlag;
    UINT4               u4SusFlag;
    ID                  u4EventFlagId;    /* uitron event flag id */
}
tOsixTsk;
typedef struct OsixRscSemStruct
{
    UINT4               u4OsId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixSem;
typedef struct OsixRscQueStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixQue;

/* A circular queue is simulated using an allocated linear memory */
/* region. Read and write pointers are used to take out and put   */
/* messages in the queue. All messages are the same size only.    */
/* So, a task or thread reads messages from this queue to service */
/* the requests one by ine i.e. one command or activity at a time */

/* The description of fields used in struct below are as follows: */
/*   pQBase - linear memory location holding messages             */
/*   pQEnd - pointer after last byte of the queue                 */
/*   pQRead - pointer where next message can be read from         */
/*   pQWrite - pointer where next message can be written to       */
/*   u4MsgLen - the length of messages on this queue              */
/*   MSem - semaphore for mutual exclusion during writes          */
/*   BSem - semaphore for blocking those waiting on queue         */
typedef struct
{
    UINT1              *pQBase;
    UINT1              *pQEnd;
    UINT1              *pQRead;
    UINT1              *pQWrite;
    UINT4               u4MsgLen;
    tOsixSemId          MSem;
    tOsixSemId          BSem;
}
tUitQ;
typedef tUitQ      *tUitQId;

/* Global variables */
UINT4               gu4Tps = 0;
UINT4               gu4Stups = 0;
UINT4               gu4NextSema4 = 10;

/* 1st element = tasks; 2nd = semaphores; 3rd = queues */
tOsixTsk            gaOsixTsk[OSIX_TOT_TSKS + 1];
/* 0th element is invalid and not used */

tOsixSem            gaOsixSem[OSIX_TOT_SEMS + 1];
/* 0th element is invalid and not used */

tOsixQue            gaOsixQue[OSIX_MAX_QUES + 1];
/* 0th element is invalid and not used */

tOsixSemId          gOsixMutex = (tOsixSemId) UITRON_SEM_ID_MUTEX;
extern UINT4        OsixSTUPS2Ticks (UINT4);
extern UINT4        OsixTicks2STUPS (UINT4);
extern UINT4        gu4Seconds;

static INT4 OsixScanForDelTask ARG_LIST ((VOID));
static VOID OsixScavengerTask ARG_LIST ((INT4));
static tUitQId UIT_Create_MsgQ ARG_LIST ((UINT4, UINT4));
static VOID UIT_Delete_MsgQ ARG_LIST ((tUitQId));
static INT4 UIT_Send_MsgQ ARG_LIST ((tUitQId, UINT1 *));
static INT4 UIT_Receive_MsgQ ARG_LIST ((tUitQId, UINT1 *g, INT4));
static UINT4 UIT_MsgQ_NumMsgs ARG_LIST ((tUitQId));

/*********************************/
/* Routines for managing task    */
/*********************************/
/************************************************************************/
/*  Function Name   : OsixTskCrt                                        */
/*  Description     : Creates task.                                     */
/*  Input(s)        : au1Name[ ] -        Name of the task              */
/*                  : u4TskPrio -         Task Priority                 */
/*                  : u4StackSize -       Stack size                    */
/*                  : (*TskStartAddr)() - Entry point function          */
/*                  : u4Arg -             Arguments to above fn.        */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskCrt (UINT1 au1Name[], UINT4 u4TskPrio, UINT4 u4StackSize,
            VOID (*TskStartAddr) (INT1 *), INT1 *u4Arg, tOsixTaskId * pTskId)
{
    UINT4               u4rc = OSIX_SUCCESS;
    tOsixTsk            Tsk;
    UINT4               u4Idx;
    INT4                i4arg = 0;
    T_CTSK              tCreTask;
    T_CFLG              tCreFlag;

    /* For tasks, the uITRON version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0.   */
    if (OsixRscAdd (au1Name, OSIX_TSK, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    u4TskPrio = u4TskPrio & 0x0000ffff;

    /* Get the global task array index to return as the task id. */
    /* Return value check not needed because we just added it.   */
    OsixRscFind (au1Name, OSIX_TSK, &u4Idx);
    Tsk = gaOsixTsk[u4Idx];
    /* place the task id in return arg pTaskId */
    *pTskId = u4Idx;

    if (u4StackSize == 0)
        u4StackSize = OSIX_DEFAULT_STACK_SIZE;

    /* Creating an event flag to associate with this task */

    /* Extended info about create event flag */
    tCreFlag.exinf = (VP) NULL;

    /* Multiple processes can wait on an event */
    tCreFlag.flgatr = TA_WMUL;

    /* Initially its null */
    tCreFlag.iflgptn = (UINT4) NULL;

    /* Create an event flag by using cre_flg */
    u4rc = (UINT4) cre_flg (Tsk.u4EventFlagId, &tCreFlag);

    if (u4rc != E_OK)
    {
        OsixRscDel (OSIX_TSK, u4Idx);
        return OSIX_FAILURE;
    }
    /* Creating a TASK */

    /* Extended information if any */
    tCreTask.exinf = (VP) NULL;

    /* Setting to high-level language */
    tCreTask.tskatr = TA_ASM;

    /* Task Start Address */
    tCreTask.task = (FP) TskStartAddr;

    /* Task priority */
    tCreTask.itskpri = OsixMapTaskPriority (u4TskPrio);

    /* Stack Size */
    tCreTask.stksz = OsixMapTaskStackSize (u4StackSize);

    /* uitron-creating a task but not started */
    u4rc = (UINT4) cre_tsk (Tsk.u4OsId, &tCreTask);

    if (u4rc != E_OK)
    {
        OsixRscDel (OSIX_TSK, u4Idx);
        return OSIX_FAILURE;
    }

    /* Now start the task - uitron */
    if (u4Arg)
        i4arg = (INT4) ((INT4 *) u4Arg);

    u4rc = (UINT4) sta_tsk ((ID) Tsk.u4OsId, i4arg);

    if (u4rc != E_OK)
    {
        OsixTskDel (*pTskId);
        OsixRscDel (OSIX_TSK, u4Idx);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixTskDel                                        */
/*  Description     : Deletes a task.                                   */
/*  Input(s)        : TskId          - Task Id                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixTskDel (tOsixTaskId TskId)
{
    tOsixTaskId         ScavTskId;
    INT4                i4OsTskId;

    i4OsTskId = (INT4) (gaOsixTsk[(UINT4) TskId].u4OsId);
    OsixRscDel (OSIX_TSK, (UINT4) i4OsTskId);

    /* Set the delete flag */
    gaOsixTsk[(UINT4) TskId].u4DelFlag = OSIX_SET;

    if (OsixRscFind (OSIX_SCAVENGER_TASK, OSIX_TSK, &ScavTskId) == OSIX_SUCCESS)
        set_flg (gaOsixTsk[(UINT4) ScavTskId].u4EventFlagId,
                 OSIX_SCAVENGER_EVENT);
}

/************************************************************************/
/*  Function Name   : OsixTskDelay                                      */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskDelay (UINT4 u4Duration)
{
    dly_tsk ((DLYTIME) OsixSTUPS2Ticks ((UINT4) u4Duration));
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixDelay                                         */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*                  : i4Unit     - Units in which the duration is       */
/*                                 specified                            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixDelay (UINT4 u4Duration, INT4 i4Unit)
{
    /* The function OsixTskdelay () does not allow delays less than 100 ms.
     * For backward compatibility reasons, that function cannot be changed.
     * So, this new function OsixDelay is introduced. On any this must be
     * ported before being used */

    UNUSED_PARAM (u4Duration);
    UNUSED_PARAM (i4Unit);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixTskIdSelf                                     */
/*  Description     : Get Osix Id of current Task                       */
/*  Input(s)        : None                                              */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskIdSelf (tOsixTaskId * pTskId)    /* Get ID of current task */
{
    UINT4               u4Count;
    INT4                i4OsTskId;

    get_tid ((ID *) & i4OsTskId);    /* uitron */

    if (i4OsTskId <= 0)
        return OSIX_FAILURE;

    for (u4Count = 1; u4Count <= OSIX_TOT_TSKS; u4Count++)
    {
        if ((gaOsixTsk[u4Count].u4OsId) == (UINT4) i4OsTskId)
        {
            *pTskId = (tOsixTaskId) u4Count;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/************************************************************************/
/*  Function Name   : OsixEvtSend                                       */
/*  Description     : Sends an event to a specified task.               */
/*  Input(s)        : TskId          - Task Id                          */
/*                  : u4Events       - The Events, OR-d                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events)
{
    if (!u4Events)
        return OSIX_FAILURE;

    /* uitron-setting the bit pattern 
     * A logical sum (OR operation) is taken for the values of the event flag
     * specified by flgid with the values of u4Events
     */
    if ((UINT4) set_flg (gaOsixTsk[(UINT4) TskId].u4EventFlagId, u4Events) !=
        E_OK)
        return OSIX_FAILURE;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixEvtRecv                                       */
/*  Description     : To receive a event.                               */
/*  Input(s)        : TskId             - Task Id                       */
/*                  : u4Events          - List of interested events.    */
/*                  : u4Flgs                                            */
/*  Output(s)       : pu4EventsReceived - Events received.              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flgs,
             UINT4 *pu4RcvEvents)
{
    UINT4               u4rc;
    INT4                i4Timeout;
    UINT4               u4WaitEvt;

    /* uitron-Getting the values of receive event flag function */
    /* Wait bit pattern value */
    u4WaitEvt = u4Events;

    if (u4Flgs == OSIX_NO_WAIT)
        i4Timeout = TMO_POL;
    else
        i4Timeout = TMO_FEVR;

    u4rc = (UINT4) twai_flg ((UINT *) pu4RcvEvents,
                             gaOsixTsk[(UINT4) TskId].u4EventFlagId,
                             u4WaitEvt, TWF_ORW, i4Timeout);
    if (u4rc == E_OK)
    {
        /* After the wait state of the task has been released,
         * clear only those bits of that flagid which it has been waited.
         * Use clear event flag (clr_flg) system call with  
         * the complement of wait pattern as an argument.
         */
        if (*pu4RcvEvents)
            u4rc =
                (UINT4) clr_flg (gaOsixTsk[(UINT4) TskId].u4EventFlagId,
                                 ~u4WaitEvt);

        *pu4RcvEvents = *pu4RcvEvents & u4WaitEvt;
    }

    if (u4rc != E_OK)
        return OSIX_FAILURE;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixInit                                          */
/*  Description     : The OSIX Initialization routine. To be called     */
/*                    before any other OSIX functions are used.         */
/*  Input(s)        : pOsixCfg - Pointer to OSIX config info.           */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixInitialize ()
{
    UINT4               u4Idx;
    T_CSEM              tCreSem;    /* uitron-Strcuture to create sema4 */
    UINT4               u4rc;

    tOsixTaskId         ScavTaskId;
    /* Mutual exclusion semaphore to add or delete elements from */
    /* name-id-mapping list. This semaphore itself will not be   */
    /* added to the name-to-id mapping list. This is a OS   */
    /* specific call and must be mapped to relevant call for OS  */

    /* uitron- Assign values to the sema4 variables */

    /* uitron-waiting tasks on sema4 is FIFO */
    tCreSem.sematr = TA_TFIFO;

    /* Initial sema4 count */
    tCreSem.isemcnt = 1;

    /* Value of extended info */
    tCreSem.exinf = (VP) NULL;

    /* Creating a sema4 */
    u4rc = (UINT4) cre_sem ((ID) gOsixMutex, &tCreSem);

    if (u4rc != E_OK)
        return OSIX_FAILURE;

    /* Initialize all arrays. */
    for (u4Idx = OSIX_TSKID_BASE; u4Idx <= OSIX_TOT_TSKS; u4Idx++)
    {
        gaOsixTsk[u4Idx].u4OsId = (ID) u4Idx;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
        gaOsixTsk[u4Idx].u4DelFlag = OSIX_RESET;
        gaOsixTsk[u4Idx].u4SusFlag = OSIX_RESET;
        gaOsixTsk[u4Idx].u4EventFlagId = (ID) (u4Idx);
    }
    for (u4Idx = OSIX_SEMID_BASE; u4Idx <= OSIX_TOT_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u4OsId = (UINT4) u4Idx;
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = OSIX_QUEID_BASE; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }

    /* Create a Scavenger Task for Task Deletion. */

    if (OsixTskCrt (OSIX_SCAVENGER_TASK, OSIX_SCAVENGER_PRIO,
                    OSIX_DEFAULT_STACK_SIZE, OsixScavengerTask, 0, &ScavTaskId))
    {
        del_sem ((ID) gOsixMutex);
        return OSIX_FAILURE;
    }
    return (OSIX_SUCCESS);

}

/************************************************************************/
/*  Function Name   : OsixShutDown                                      */
/*  Description     : Stops OSIX.                                       */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixShutDown ()
{
    UINT4               u4Idx;
    UINT4               u4rc = OSIX_SUCCESS;

    /* Delete Semaphore */
    del_sem ((ID) gOsixMutex);
    for (u4Idx = OSIX_SEMID_BASE; u4Idx <= OSIX_TOT_SEMS; u4Idx++)
    {
        del_sem ((ID) u4Idx);
    }

    /* Delete the scavenger task, it's ID is OSIX_TSKID_BASE */
    u4rc = ter_tsk (OSIX_TSKID_BASE);
    if (u4rc == E_OK || u4rc == E_OBJ)
    {
        u4rc = del_tsk (OSIX_TSKID_BASE);
    }

    /* Delete the event associate with the scavenger task */
    del_flg (OSIX_TSKID_BASE);

    for (u4Idx = OSIX_TSKID_BASE; u4Idx <= OSIX_TOT_TSKS; u4Idx++)
    {
        /* Delete the scavenger task, it's ID is OSIX_TSKID_BASE */
        ter_tsk (u4Idx);
        del_tsk (u4Idx);
        /* Delete the event associate with the scavenger task */
        del_flg (u4Idx);
    }

    /* Re-initialize all arrays and delete global semaphore */
    for (u4Idx = OSIX_TSKID_BASE; u4Idx <= OSIX_TOT_TSKS; u4Idx++)
    {
        gaOsixTsk[u4Idx].u4OsId = (ID) OSIX_RSC_INV;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u4DelFlag = OSIX_RESET;
        gaOsixTsk[u4Idx].u4SusFlag = OSIX_RESET;
        gaOsixTsk[u4Idx].u4EventFlagId = (ID) NULL;
    }
    for (u4Idx = OSIX_SEMID_BASE; u4Idx <= OSIX_TOT_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u4OsId = (UINT4) OSIX_RSC_INV;
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
    }
    for (u4Idx = OSIX_QUEID_BASE; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
    }

    gOsixMutex = (tOsixSemId) UITRON_SEM_ID_MUTEX;
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing semaphores                                     */
/* Keep it simple - support 1 type of semaphore - binary, blocking.     */
/* After creating, the semaphore must be given before it can be taken.  */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixSemCrt                                        */
/*  Description     : Creates a sema4.                                  */
/*  Input(s)        : au1Name [ ] - Name of the sema4.                  */
/*  Output(s)       : pSemId         - The sema4 Id.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemCrt (UINT1 au1Name[], tOsixSemId * pSemId)
{
    T_CSEM              tCreSem;    /* uitron-Strcuture to create sema4 */
    UINT4               u4rc;
    UINT4               u4Idx;

    /* For sem, the uITRON version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0. */
    if (OsixRscAdd (au1Name, OSIX_SEM, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* the uITRON version of OsixRscFind returns pointer to semId */
    OsixRscFind (au1Name, OSIX_SEM, &u4Idx);

    /* place the sem id in return arg pSemId */
    *pSemId = (tOsixSemId) u4Idx;

    /* uitron- Assign values to the sema4 variables */

    /* uitron-waiting tasks on sema4 is FIFO */
    tCreSem.sematr = TA_TFIFO;

    /* Initial sema4 count */
    tCreSem.isemcnt = 0;

    /* Value of extended info */
    tCreSem.exinf = (VP) NULL;

    /* Creating a sema4 */
    u4rc = (UINT4) cre_sem ((ID) * pSemId, &tCreSem);

    if (u4rc != E_OK)
    {
        OsixRscDel (OSIX_SEM, (UINT4) *pSemId);
        return OSIX_FAILURE;
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemDel                                        */
/*  Description     : Deletes a sema4.                                  */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixSemDel (tOsixSemId SemId)
{
    OsixRscDel (OSIX_SEM, (UINT4) SemId);
    del_sem ((ID) SemId);
}

/************************************************************************/
/*  Function Name   : OsixSemGive                                       */
/*  Description     : Used to release a sem4.                           */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemGive (tOsixSemId SemId)
{
    if (sig_sem ((ID) SemId) != E_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemTake                                       */
/*  Description     : Used to acquire a sema4.                          */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemTake (tOsixSemId SemId)
{
    if (twai_sem ((ID) SemId, (INT4) TMO_FEVR) != E_OK)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetNextSem                                    */
/*  Description     : Used to get the unique  name of the sema4.        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : Name of the Sema4                                 */
/************************************************************************/
UINT4
OsixGetNextSem ()
{
    OsixSemTake (gOsixMutex);
    gu4NextSema4++;
    OsixSemGive (gOsixMutex);
    return gu4NextSema4;
}

/****************************************/
/* Routines for managing message queues */
/****************************************/
/************************************************************************/
/*  Function Name   : OsixQueCrt                                        */
/*  Description     : Creates a OSIX Q.                                 */
/*  Input(s)        : au1name[ ] - The Name of the Queue.               */
/*                  : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MaxMsgLen- Max length of a messages             */
/*  Output(s)       : pQueId     - The QId returned.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueCrt (UINT1 au1Name[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId)
{
    *pQueId = (tOsixQId) UIT_Create_MsgQ (u4MaxMsgs, u4MaxMsgLen);
    if (*pQueId == (tOsixQId) NULL)
    {
        return (OSIX_FAILURE);
    }
    if (OsixRscAdd (au1Name, OSIX_QUE, *pQueId) == OSIX_FAILURE)
    {
        UIT_Delete_MsgQ ((tUitQId) (*pQueId));
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueDel                                        */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
VOID
OsixQueDel (tOsixQId QueId)
{
    OsixRscDel (OSIX_QUE, (UINT4) QueId);
    UIT_Delete_MsgQ ((tUitQId) QueId);
    return;
}

/************************************************************************/
/*  Function Name   : OsixQueSend                                       */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*                  : u4MsgLen - length of the messages                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen)
{
    u4MsgLen = 0;

    /* Typically native OS calls take message Length as an argument.
     * In the case of uITRON Queues, the value supplied in the call
     * OsixQueCrt is used implicitly as the message length.
     * Hence the u4MsgLen parameter is not used in this function.
     */
    if (UIT_Send_MsgQ ((tUitQId) QueId, pu1Msg) != 0)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueRecv                                       */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : u4MsgLen -  length of the messages                */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4Timeout)
{
    u4MsgLen = 0;

    /* Typically native OS calls take message Length as an argument.
     * In the case of uITRON Queues, the value supplied in the call
     * OsixQueCrt is used implicitly as the message length.
     * Hence the u4MsgLen parameter is not used in this function.
     */
    if (i4Timeout > 0)
    {
        i4Timeout = (INT4) OsixSTUPS2Ticks ((UINT4) i4Timeout);
    }

    if (UIT_Receive_MsgQ ((tUitQId) QueId, pu1Msg, i4Timeout) != 0)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueNumMsgs                                    */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : pu4NumberOfMsgs - Contains count upon return.     */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixQueNumMsg (tOsixQId QueId, UINT4 *pu4NumMsg)
{
    *pu4NumMsg = (UINT4) (UIT_MsgQ_NumMsgs ((tUitQId) QueId));
    return (OSIX_SUCCESS);
}

/********************************************************/
/* Routines for managing resources based on names       */
/********************************************************/
/************************************************************************/
/*  Function Name   : OsixRscAdd                                        */
/*  Description     : Gets a free resouce, stores Resource-Id & Name    */
/*                  : This helps in locating Resource-Id by Name        */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find a free slot */
            for (u4Idx = OSIX_TSKID_BASE; u4Idx <= OSIX_TOT_TSKS; u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixTsk[u4Idx].u2Free = OSIX_FALSE;
                    MEMCPY (gaOsixTsk[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find a free slot */
            for (u4Idx = OSIX_SEMID_BASE; u4Idx < OSIX_TOT_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixSem[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixSem[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find a free slot */
            for (u4Idx = OSIX_QUEID_BASE; u4Idx < OSIX_TOT_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixQue[u4Idx].u4RscId = u4RscId;
                    gaOsixQue[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixQue[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixQue[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }

    OsixSemGive (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscDel                                        */
/*  Description     : Free an allocated resouce                         */
/*  Input(s)        : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixRscDel (UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return;
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            gaOsixTsk[u4RscId].u2Free = OSIX_TRUE;
            MEMSET (gaOsixTsk[u4RscId].au1Name, '\0', (OSIX_NAME_LEN + 4));
            gaOsixTsk[u4RscId].u4DelFlag = OSIX_RESET;
            gaOsixTsk[u4RscId].u4SusFlag = OSIX_RESET;
            break;

        case OSIX_SEM:
            gaOsixSem[u4RscId].u2Free = OSIX_TRUE;
            gaOsixSem[u4RscId].u2Filler = 0;
            MEMSET (gaOsixSem[u4RscId].au1Name, '\0', (OSIX_NAME_LEN + 4));
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = OSIX_QUEID_BASE; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixQue[u4Idx].u4RscId = (UINT4) OSIX_RSC_INV;
                    gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
                    gaOsixQue[u4Idx].u2Filler = 0;
                    MEMSET (gaOsixQue[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (gOsixMutex);
}

/************************************************************************/
/*  Function Name   : OsixRscFind                                       */
/*  Description     : This locates Resource-Id by Name                  */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*  Output(s)       : pu4RscId - Osix Resource-Id                       */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }
    if (OsixSemTake (gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            for (u4Idx = OSIX_TSKID_BASE; u4Idx <= OSIX_TOT_TSKS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixTsk[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* For tasks applications know only our array index. */
                    /* This helps us to simulate events.                 */
                    *pu4RscId = u4Idx;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = OSIX_SEMID_BASE; u4Idx <= OSIX_TOT_SEMS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixSem[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* For semaphore applications know only our array index. */
                    *pu4RscId = u4Idx;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = OSIX_QUEID_BASE; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixQue[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = gaOsixQue[u4Idx].u4RscId;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : Fsap2Start                                        */
/*  Description     : Function to be called to get any fsap2 application*/
/*                  : to work                                           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
void
Fsap2Start ()
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixGetSysTime                                    */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*                  : pSysTime - The System time.                       */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
OsixGetSysTime (tOsixSysTime * pSysTime)
{
    SYSTIME             s;

    /* SYSTIME is a 64-bit quantity. Passing *pSysTime
     * directly to get_tim would result in stack corruption */
    get_tim (&s);
    *pSysTime = OsixTicks2STUPS ((UINT4) s);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixGetSysUpTime                                  */
/*  Description     : Returns the system time in Seconds.               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysUpTime (VOID)
{
    return (gu4Seconds);
}

/************************************************************************/
/*  Function Name   : OsixGetTps                                        */
/*  Description     : Returns the no of system ticks in a second        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : Number of system ticks in a second                */
/************************************************************************/
UINT4
OsixGetTps ()
{
    return (OSIX_TPS);
}

/************************************************************************/
/*  Function Name   : OsixExGetTaskName                                 */
/*  Description     : Get the OSIX task Name given the Osix Task-Id.    */
/*  Input(s)        : TaskId - The Osix taskId.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to OSIX task name                         */
/************************************************************************/
const UINT1        *
OsixExGetTaskName (tOsixTaskId TskId)
{
    /* To facilitate direct use of this call in a STRCPY,
     * we return a null string instead of a NULL pointer.
     * A null pointer is returned for all non-OSIX tasks
     * e.g., TMO's idle task.
     */
    if (TskId)
    {
        return ((UINT1 *) (gaOsixTsk[(UINT4) TskId].au1Name));
    }
    return ((const UINT1 *) "");
}

/************************************************************************/
/*  Function Name   : OsixScanForDelTask                                */
/*  Description     : Scans all the TCB ready for Task Deletion         */
/*  Input(s)        : Pointer to the TaskId                             */
/*  Output(s)       : None.                                             */
/*  Returns         :                                                   */
/************************************************************************/
static INT4
OsixScanForDelTask ()
{

    UINT4               u4Idx;

    OsixSemTake (gOsixMutex);

    for (u4Idx = 1; u4Idx <= OSIX_TOT_TSKS; u4Idx++)
    {
        if (gaOsixTsk[u4Idx].u2Free == OSIX_TRUE)
        {
            if (gaOsixTsk[u4Idx].u4DelFlag == OSIX_SET)
            {
                gaOsixTsk[u4Idx].u4DelFlag = OSIX_RESET;
                OsixSemGive (gOsixMutex);
                return u4Idx;
            }
        }
    }

    OsixSemGive (gOsixMutex);
    return u4Idx;
}

/************************************************************************/
/*  Function Name   : OsixProcessDelScav                                */
/*  Description     : deletes the Scavenger TASK                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS /OSIX_FAILURE                        */
/************************************************************************/
UINT4
OsixProcessDelScav ()
{

    INT4                i4Idx;
    INT4                u4rc;

    /* Get the Index of the Task to be deleted */
    i4Idx = OsixScanForDelTask ();

    /* uitron-Delete the event created for the particular task */
    u4rc = (UINT4) del_flg (gaOsixTsk[i4Idx].u4EventFlagId);
    if (u4rc != E_OK)
        return OSIX_FAILURE;

    /* uitron-Terminate the Task */
    u4rc = ter_tsk (gaOsixTsk[i4Idx].u4OsId);
    /* If the task is already in DORMANT state or now it went to DORMANT state */
    if (u4rc == E_OBJ || u4rc == E_OK)
    {
        /* uitron-Delete the Task */
        u4rc = del_tsk (gaOsixTsk[i4Idx].u4OsId);

        if (u4rc != E_OK)
        {
            return OSIX_FAILURE;
        }
    }
    else
        return OSIX_FAILURE;

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixScavengerTask                                 */
/*  Description     : "Event-ually" deletes the task                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         :                                                   */
/************************************************************************/
static VOID
OsixScavengerTask (INT4 u4dummy)
{
    UINT4               u4RecdEvent;
    UINT4               u4rc;
    tOsixTaskId         TskId;

    while (1)
    {
        if (OsixTskIdSelf (&TskId) == OSIX_FAILURE)
            return;                /* (OSIX_FAILURE); */
        u4rc =
            OsixEvtRecv (TskId, OSIX_SCAVENGER_EVENT, OSIX_WAIT, &u4RecdEvent);
        if ((!u4rc) && u4RecdEvent)
            OsixProcessDelScav ();
    }

}

/************************************************************************/
/*  Function Name   : OsixMapTaskPriority                               */
/*  Description     : Maps OSIX task priority to that of target uitron  */
/*  Input(s)        : u4TaskPriority - OSIX task priority.              */
/*  Output(s)       : None.                                             */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
OsixMapTaskPriority (UINT4 u4TaskPriority)
{
    UINT4               u4TaskPri;

    if (u4TaskPriority == 0)    /* Highest priority */
        u4TaskPri = 1;

    if (u4TaskPriority > 0 && u4TaskPriority <= 8)
        u4TaskPri = u4TaskPriority;

    if (u4TaskPriority > 8)
        u4TaskPri = 8;

    return u4TaskPri;
}

/************************************************************************/
/*  Function Name  : OsixMapTaskStackSize                               */
/*  Description     : If the user specified stack size is more than the */
/*                 configured size in uITRON template, then this        */
/*                 function converts to the uITRON supported size       */
/*  Input(s)        : u4StackSize - OSIX task stack size                */
/*  Output(s)       : None.                                             */
/*  Returns        :                                                    */
/************************************************************************/
UINT4
OsixMapTaskStackSize (UINT4 u4StackSize)
{

    if (u4StackSize > UITRON_MAX_TASK_STACK_SIZE)
        return UITRON_MAX_TASK_STACK_SIZE;
    else
        return u4StackSize;
}

/************************************************************************/
/*  Function Name   : UIT_Create_MsgQ                               */
/*  Description     : Creates a queue using a linear block of memory.   */
/*  Input(s)        : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MaxMsgLen- Max length of a messages             */
/*  Output(s)       : None                                              */
/*  Returns         : Queue-Id, NULL if creation fails                  */
/************************************************************************/
tUitQId
UIT_Create_MsgQ (UINT4 u4MaxMsgs, UINT4 u4MsgLen)
{
    T_CSEM              tCreSem;
    tUitQ              *pUitQ;
    UINT4               u4rc;

    /* Allocate memory for holding messages. Create a semaphore for      */
    /* protection between multiple simultaneous calls to write or read   */
    /* Initialize the read and write pointers to the Q start location    */
    /* Initia the pointer marking the end of the queue's memory location */
    pUitQ = (tUitQ *) malloc (((u4MaxMsgs + 1) * u4MsgLen) + sizeof (tUitQ));
    if (pUitQ == NULL)
    {
        return (NULL);
    }
    pUitQ->pQBase = (UINT1 *) ((UINT1 *) pUitQ + sizeof (tUitQ));

    tCreSem.sematr = TA_TFIFO;
    tCreSem.isemcnt = 0;
    tCreSem.exinf = (VP) NULL;
    pUitQ->MSem = OsixGetNextSem ();
    u4rc = (UINT4) cre_sem ((ID) (pUitQ->MSem), &tCreSem);
    if (u4rc != E_OK)
    {
        free ((VOID *) pUitQ);
        return (NULL);
    }
    sig_sem ((ID) (pUitQ->MSem));

    tCreSem.sematr = TA_TFIFO;
    tCreSem.isemcnt = 0;
    tCreSem.exinf = (VP) NULL;
    pUitQ->BSem = OsixGetNextSem ();
    u4rc = (UINT4) cre_sem ((ID) (pUitQ->BSem), &tCreSem);
    if (u4rc != E_OK)
    {
        del_sem ((ID) pUitQ->MSem);
        free ((VOID *) pUitQ);
        return (NULL);
    }

    pUitQ->pQEnd = (pUitQ->pQBase) + ((u4MaxMsgs + 1) * u4MsgLen);
    pUitQ->pQRead = pUitQ->pQBase;
    pUitQ->pQWrite = pUitQ->pQBase;
    pUitQ->u4MsgLen = u4MsgLen;

    return (pUitQ);
}

/************************************************************************/
/*  Function Name   : UIT_Delete_MsgQ                               */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
UIT_Delete_MsgQ (tUitQId QId)
{
    tUitQ              *pUitQ = (tUitQ *) QId;
    /* Wait for semaphore to ensure that when the queue is deleted */
    /* no one is reading from or writing into it. Then delete the  */
    /* semaphore, free the queue memory and initialize queue start */
    if (OsixSemTake (pUitQ->MSem))
    {
        return;
    }
    del_sem ((ID) pUitQ->MSem);
    del_sem ((ID) pUitQ->BSem);
    free ((VOID *) pUitQ);
}

/************************************************************************/
/*  Function Name   : UIT_Send_MsgQ                                 */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*  Output(s)       : None                                              */
/*  Returns         : 0 on SUCCESS and (-1) on FAILURE                  */
/************************************************************************/
INT4
UIT_Send_MsgQ (tUitQId QId, UINT1 *pMsg)
{
    tUitQ              *pUitQ = (tUitQ *) QId;
    UINT1              *pWrite, *pRead, *pBase, *pEnd;
    UINT4               u4MsgLen;

    /* Ensure mutual exclusion. Wait and take the mutual exclusion        */
    /* semaphore. A write is possible if the queue is not full. Queue is  */
    /* recognized as full if by writing one more message, write and read  */
    /* pointers become equal. Actually, this means that the queue holds   */
    /* only u4MaxMsgs-1 messages to be safe. When checking the pointers   */
    /* or when advancing the write pointer after the write operation,     */
    /* take care of the wrap-around since this is a circular queue. When  */
    /* the message is written, advance the write pointer by u4MsgLen.     */
    if (OsixSemTake (pUitQ->MSem))
    {
        return (-1);
    }

    pWrite = pUitQ->pQWrite;
    pRead = pUitQ->pQRead;
    pBase = pUitQ->pQBase;
    pEnd = pUitQ->pQEnd;
    u4MsgLen = pUitQ->u4MsgLen;

    if (((pWrite + u4MsgLen) == pEnd) && (pRead == pBase))
    {
        OsixSemGive (pUitQ->MSem);
        return (-1);
    }
    if ((pWrite + u4MsgLen) == pRead)
    {
        OsixSemGive (pUitQ->MSem);
        return (-1);
    }
    memcpy (pWrite, pMsg, u4MsgLen);
    (pUitQ->pQWrite) += u4MsgLen;

    if ((pUitQ->pQWrite) == pEnd)
    {
        (pUitQ->pQWrite) = pBase;
    }
    OsixSemGive (pUitQ->BSem);    /* unblock anyone waiting to read    */
    OsixSemGive (pUitQ->MSem);    /* allow others to read/write/delete */
    return (0);
}

/************************************************************************/
/*  Function Name   : UIT_Receive_MsgQ                              */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : 0 on SUCCESS and (-1) on FAILURE                  */
/************************************************************************/
INT4
UIT_Receive_MsgQ (tUitQId QId, UINT1 *pMsg, INT4 i4Timeout)
{
    tUitQ              *pUitQ = (tUitQ *) QId;

    /* Only FM task/thread reads from the queue. Multiple other tasks may */
    /* write. Deletion of the queue is also done only by the FM task. So  */
    /* a semaphore for mutual exclusion is needed for writing but not for */
    /* reading. Only a blocking semaphore to wait for a message is needed */
    /* if the queue is empty.                                             */

    /* Check if queue exists. If yes, wait and take the mutual exclusion  */
    /* semaphore. A read is possible if the queue is not empty. Queue is  */
    /* recognized as empty if write and read pointers are equal i.e. all  */
    /* the written messages have already been read.                       */
    if (OsixSemTake (pUitQ->MSem))
    {
        return (-1);
    }

    /* nrm - fix for the NO_WAIT case! if i4Timeout is 0, it is NO_WAIT. */
    if (!i4Timeout && (pUitQ->pQWrite) == (pUitQ->pQRead))
    {
        OsixSemGive (pUitQ->MSem);
        return -1;
    }
    while ((pUitQ->pQWrite) == (pUitQ->pQRead))
    {
        /* Queue is empty. Block on the semaphore. When a write is done */
        /* the writer will release the semaphore and unblock this wait. */
        /* Before blocking, allow someone else to write into queue.     */
        OsixSemGive (pUitQ->MSem);

        if (OsixSemTake (pUitQ->BSem))
        {
            return (-1);
        }
        if (OsixSemTake (pUitQ->MSem))
        {
            return (-1);
        }
    }

    /* There is at least 1 message in the queue and we have locked the */
    /* mutual exclusion semaphore so nobody else changes the state.    */
    memcpy (pMsg, pUitQ->pQRead, pUitQ->u4MsgLen);
    (pUitQ->pQRead) += (pUitQ->u4MsgLen);
    if ((pUitQ->pQRead) == (pUitQ->pQEnd))
    {
        (pUitQ->pQRead) = (pUitQ->pQBase);
    }
    OsixSemGive (pUitQ->MSem);
    return (0);
}

/************************************************************************/
/*  Function Name   : UIT_MsgQ_NumMsgs                              */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : None                                              */
/*  Returns         : pu4NumberOfMsgs - Contains count upon return.     */
/************************************************************************/
UINT4
UIT_MsgQ_NumMsgs (tUitQId QId)
{
    tUitQ               UitQ = *((tUitQ *) QId);
    UINT4               u4Msgs;

    if ((UitQ.pQWrite) < (UitQ.pQRead))
    {
        u4Msgs = (UitQ.pQWrite) - (UitQ.pQBase) + (UitQ.pQEnd) - (UitQ.pQRead);
        return (u4Msgs / (UitQ.u4MsgLen));
    }
    else
    {
        return (((UitQ.pQWrite) - (UitQ.pQRead)) / (UitQ.u4MsgLen));
    }
}

#if (FILESYS_SUPPORT == FSAP_ON)
/************************************************************************
 *  Function Name   : FileOpen
 *  Description     : Function to Open a file.
 *  Input           : pu1FileName - Name of file to open.
 *                    i4Mode      - whether r/w/rw.
 *  Returns         : FileDescriptor if successful
 *                    -1             otherwise.
 ************************************************************************/
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    INT4                i4Mode = 0;

    if (i4InMode & OSIX_FILE_CR)
    {
        i4Mode |= O_CREAT;
    }
    if (i4InMode & OSIX_FILE_RO)
    {
        i4Mode |= O_RDONLY;
    }
    else if (i4InMode & OSIX_FILE_WO)
    {
        i4Mode |= O_WRONLY;

        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }
    else if (i4InMode & OSIX_FILE_RW)
    {
        i4Mode |= O_RDWR;

        if (i4InMode & OSIX_FILE_AP)
        {
            i4Mode |= O_APPEND;
        }
    }

    return open ((const CHR1 *) pu1FileName, i4Mode, 0644);
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description     : Function to close a file.
 *  Input           : i4Fd - File Descriptor to be closed.
 *  Returns         : 0 on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    return close (i4Fd);
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description     : Function to read a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer into which to read
 *                    i4Count - Number of bytes to read
 *  Returns         : Number of bytes read on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    return read (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description     : Function to write to a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : Number of bytes written on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    return write (i4Fd, pBuf, i4Count);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description     : Function to delete a file.
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : 0 on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    return unlink ((const CHR1 *) pu1FileName);
}
#else
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4InMode)
{
    pu1FileName = pu1FileName;
    i4InMode = i4InMode;
    return (-1);
}

INT4
FileClose (INT4 i4Fd)
{
    i4Fd = i4Fd;
    return (-1);
}

UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    i4Fd = i4Fd;
    pBuf = pBuf;
    i4Count = i4Count;
    return (-1);
}

INT4
FileDelete (const UINT1 *pu1FileName)
{
    pu1FileName = pu1FileName;
    return (-1);
}
#endif
/************************************************************************/
/*  Function Name   : OsixSetLocalTime                                  */
/*  Description     : Obtains the system time and sets it in FSAP.      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSetLocalTime (void)
{
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixGetMemInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4TotalMem - Total memory
 *                    pu4FreeMem - Free memory
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetMemInfo (UINT4 *pu4TotalMem, UINT4 *pu4FreeMem)
{
    *pu4TotalMem = 0;
    *pu4FreeMem = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetCPUInfo
 *  Description     : Function used to get the CPU utilization.
 *  Inputs          : None
 *  OutPuts         : pu4TotalUsage - Total Usage
 *                  : pu4CPUUsage - CPU Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetCPUInfo (UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage)
{
    *pu4TotalUsage = 0;
    *pu4CPUUsage = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetFlashInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4FlashUsage - Flash Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetFlashInfo (UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash)
{
    *pu4TotalUsage = 0;
    *pu4FreeFlash = 0;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                    to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}
