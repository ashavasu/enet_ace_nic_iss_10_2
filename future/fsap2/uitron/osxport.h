/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osxport.h,v 1.14 2015/04/28 12:20:25 siva Exp $
 *
 * Description: Common OS defines 
 *
 *******************************************************************/

#ifndef OSIXNAMES_H
#define OSIXNAMES_H

/************************************************************************
*                                                                       *
*                         Tasks                                         *
*                                                                       *
*************************************************************************/
#define OSIX_SCAVENGER_TASK    (UINT1 *)"SCAV"      
#define OSIX_SCAVENGER_PROI     1  
#define OSIX_SCAVENGER_EVENT    0x8000
#define OSIX_SUSPEND_EVENT      0x4000
#define TMR_TASK_ID             2      
#define TMR_TASK_PROI           15  
/************************************************************************
*                                                                       *
*                         Queues                                        *
*                                                                       *
*************************************************************************/

/* Add application task names here onwards.*/

/************************************************************************
*                                                                       *
*                         Semaphores                                    *
*                                                                       *
*************************************************************************/
#define OSIX_MUTEX_NAME       (UINT1 *)"OSMU"
#define TMR_MUTEX_NAME        (UINT1 *)"TMMU"

#define OSIX_GET_UNIQ_SEM() OsixGetNextSem()

#endif
