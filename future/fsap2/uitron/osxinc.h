/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osxinc.h
 *
 * Description: Header of all headers
 *
 *******************************************************************/

#ifndef OSX_INC_H 
#define OSX_INC_H

/*******************************************************************/
/* uITRON Header files                                             */
/*******************************************************************/
#include <cyg/compat/uitron/uit_func.h>
#include "fsapcfg.h"
#include "osxstd.h"

#ifdef BOOLEAN 
#undef BOOLEAN
#endif

#ifdef MEM_FREE
#undef MEM_FREE
#endif

#include "srmmem.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utlmacro.h"
#include "utltrc.h"
#include "osxprot.h" 
#include "utlsll.h"
#include "utlhash.h"

UINT4      OsixProcessDelScav   ARG_LIST ((VOID));
UINT4      OsixMapTaskPriority  ARG_LIST ((UINT4));
UINT4      OsixMapTaskStackSize ARG_LIST ((UINT4));
#endif
