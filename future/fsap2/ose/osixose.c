/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osixose.c,v 1.17 2015/04/28 12:16:32 siva Exp $
 *
 * Description: Contains OSIX reference code for OSE.
 *              All basic OS facilities used by protocol software
 *              from FS, use only these APIs.
 */

#include "osxinc.h"
#include "osix.h"

/* The basic structure maintaining the name-to-id mapping         */
/* of OSIX resources. 3 arrays - one for tasks, one for           */
/* semaphores and one for queues are maintained by OSIX.          */
/* Each array has this structure as the basic element. We         */
/* use this array to store events for tasks also.                 */

/* The structure has the following elements:                      */
/*   u4RscId  - the id returned by the OS                         */
/*   u2Free   - whether this structure is free or used            */
/*   u4Events - for event simulation; used only for tasks         */
/*   au1Name  - name is always multiple of 4 characters in length */
typedef struct OsixRscTskStruct
{

    UINT4               u4RscId;
    UINT4               u4Events;
    UINT4               u4Arg;
    SEMAPHORE          *TskMutex;
    UINT2               u2Free;
    UINT2               u2Pad;
    void                (*pTskStrtAddr) (INT4);
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixTsk;

typedef struct OsixRscQueStruct
{
    UINT4               u4RscId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixQue;

typedef struct OsixRscSemStruct
{
    UINT4               SemId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixSem;

/* A circular queue is simulated using an allocated linear memory */
/* region. Read and write pointers are used to take out and put   */
/* messages in the queue. All messages are the same size only.    */
/* So, a task or thread reads messages from this queue to service */
/* the requests one by ine i.e. one command or activity at a time */
typedef struct
{
    UINT1              *pQBase;
    UINT1              *pQEnd;
    UINT1              *pQRead;
    UINT1              *pQWrite;
    UINT4               u4MsgLen;
    tOsixSemId          MSem;
    tOsixSemId          BSem;
}
tOseQ;

typedef struct
{
    SIGSELECT           sig_no;
    void                (*pvTaskStartAddr) ();
    UINT4               u4TaskStartArgs;
}
tTaskStartAddrArgs;

typedef struct
{
    SIGSELECT           sig_no;
    UINT4               u4EventData;
}
tEventData;

union SIGNAL
{
    SIGSELECT           sig_no;
    tTaskStartAddrArgs  TaskStartAddrArgs;
    tEventData          EventData;
};

typedef tOseQ      *tOseQId;
typedef tOsixSemId  SEM_ID;

tOsixTsk            gaOsixTsk[OSIX_MAX_TSKS + 1];
/* 0th element is invalid and not used */

tOsixSem            gaOsixSem[OSIX_MAX_SEMS + 1];
/* 0th element is invalid and not used */

tOsixQue            gaOsixQue[OSIX_MAX_QUES + 1];
/* 0th element is invalid and not used */

UINT4               gu4Tps = OSIX_TPS;
UINT4               gu4Stups = OSIX_STUPS;
UINT4               gSysTicks = 0;

tOsixSemId          gOsixMutex = (tOsixSemId) OSIX_RSC_INV;

static OSENTRYPOINT RcvTaskArgs;

static UINT4 OsixRscAdd ARG_LIST ((UINT1[], UINT4, UINT4));
static VOID OsixRscDel ARG_LIST ((UINT4, UINT4));

static tOseQId OSE_Create_MsgQ ARG_LIST ((UINT4, UINT4));
static VOID OSE_Delete_MsgQ ARG_LIST ((tOseQId));
static INT4 OSE_Send_MsgQ ARG_LIST ((tOseQId, UINT1 *));
static INT4 OSE_Receive_MsgQ ARG_LIST ((tOseQId, UINT1 *, INT4));
static UINT4 OSE_MsgQ_NumMsgs ARG_LIST ((tOseQId));

extern UINT4 OsixSTUPS2Ticks ARG_LIST ((UINT4));
extern UINT4 OsixTicks2STUPS ARG_LIST ((UINT4));
extern UINT4        gu4Seconds;

#define TASK_SIG_NUM         10
#define EVENT_SIG_NUM        11

/************************************************************************/
/* Routines for task creation, deletion and maintenance                 */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixTskCrt                                        */
/*  Description     : Creates task.                                     */
/*  Input(s)        : au1Name[ ] -        Name of the task              */
/*                  : u4TskPrio -         Task Priority                 */
/*                  : u4StackSize -       Stack size                    */
/*                  : (*TskStartAddr)() - Entry point function          */
/*                  : u4Arg -             Arguments to above fn.        */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskCrt (UINT1 au1Name[], UINT4 u4TskPrio, UINT4 u4StackSize,
            VOID (*TskStartAddr) (INT1 *), INT1 *u4Arg, tOsixTaskId * pTskId)
{
    tOsixTsk           *pTsk = 0;
    union SIGNAL       *SndTaskAddrArgs;
    UINT4               u4Idx;
    INT4                i4OsPrio;

    /* For tasks, the OSE version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0.   */
    if (OsixRscAdd (au1Name, OSIX_TSK, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    u4TskPrio = u4TskPrio & 0x0000ffff;

    /* Get the global task array index to return as the task id. */
    /* Return value check not needed because we just added it.   */
    OsixRscFind (au1Name, OSIX_TSK, &u4Idx);

    pTsk = &gaOsixTsk[u4Idx];
    *pTskId = (tOsixTaskId) u4Idx;

    /* Create mutex for event synchronisation */
    pTsk->TskMutex = create_sem (0);
    if (pTsk->TskMutex == 0)
    {
        OsixRscDel (OSIX_TSK, u4Idx);
        return OSIX_FAILURE;
    }

    /* Remap the task priority to OSE's range of values. */
    i4OsPrio = OS_LOW_PRIO + (((((INT4) (u4TskPrio) - FSAP_LOW_PRIO) *
                                (OS_HIGH_PRIO -
                                 OS_LOW_PRIO)) / (FSAP_HIGH_PRIO -
                                                  FSAP_LOW_PRIO)));

    /* In OSE, arguments cannot be passed during the creation of process.
     * So  a dummy function is created and TaskStartAddr, TaskStartArgs
     * are sent through the signals
     */
    pTsk->u4RscId =
        create_process (OS_PRI_PROC, (char *) au1Name, RcvTaskArgs,
                        (OSADDRESS) u4StackSize, (OSPRIORITY) i4OsPrio,
                        0, 0, NULL, 0, 0);

    /* Pass task start address and arguments to dummy process thru' signals */
    SndTaskAddrArgs = alloc (sizeof (tTaskStartAddrArgs), TASK_SIG_NUM);
    if (SndTaskAddrArgs == NULL)
    {
        kill_sem (pTsk->TskMutex);
        OsixRscDel (OSIX_TSK, u4Idx);
        return OSIX_FAILURE;
    }

    SndTaskAddrArgs->TaskStartAddrArgs.pvTaskStartAddr = TskStartAddr;
    SndTaskAddrArgs->TaskStartAddrArgs.u4TaskStartArgs = u4Arg;

    send (&SndTaskAddrArgs, pTsk->u4RscId);

    /* Start the execution of the created process */
    start (pTsk->u4RscId);

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixTskDel                                        */
/*  Description     : Deletes a task.                                   */
/*  Input(s)        : TskId          - Task Id                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixTskDel (tOsixTaskId TskId)
{
    UINT4               u4RscId = 0;

    /* A task can try to kill itself.
     * For this to work properly kill_proc must be called finally.
     */
    u4RscId = gaOsixTsk[(UINT4) TskId].u4RscId;

    /* It is an error to kill a semaphore that is being waited on;
     * Semaphores are anyway automatically returned to the system by
     * killing the process that owns them.
     .*/
/*     kill_sem (gaOsixTsk[(UINT4) TskId].TskMutex); */
    OsixRscDel (OSIX_TSK, (UINT4) TskId);
    kill_proc (u4RscId);
}

/************************************************************************/
/*  Function Name   : OsixTskDelay                                      */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskDelay (UINT4 u4Duration)
{

    if (u4Duration == 0)
        return OSIX_FAILURE;

    /* change time frame to msec */
    u4Duration = OsixSTUPS2Ticks (u4Duration) * (gSysTicks / MSEC_PER_SEC);
    delay ((OSTIME) u4Duration);

    return OSIX_SUCCESS;

}

/************************************************************************/
/*  Function Name   : OsixDelay                                         */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*                  : i4Unit     - Units in which the duration is       */
/*                                 specified                            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixDelay (UINT4 u4Duration, INT4 i4Unit)
{
    /* The function OsixTskdelay () does not allow delays less than 100 ms.
     * For backward compatibility reasons, that function cannot be changed.
     * So, this new function OsixDelay is introduced. On any this must be
     * ported before being used */

    UNUSED_PARAM (u4Duration);
    UNUSED_PARAM (i4Unit);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixTskIdSelf                                     */
/*  Description     : Get Osix Id of current Task                       */
/*  Input(s)        : None                                              */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskIdSelf (tOsixTaskId * pTskId)    /* Get ID of current task */
{
    UINT4               u4Count;
    INT4                i4OsTskId;

    i4OsTskId = (INT4) current_process ();

    for (u4Count = 1; u4Count <= OSIX_MAX_TSKS; u4Count++)
    {
        if ((gaOsixTsk[u4Count].u4RscId) == (UINT4) i4OsTskId)
        {
            *pTskId = (tOsixTaskId) u4Count;
            return (OSIX_SUCCESS);
        }
    }
    return (OSIX_FAILURE);
}

/************************************************************************/
/* Routines for event management - send / receive event                 */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixEvtSend                                       */
/*  Description     : Sends an event to a specified task.               */
/*  Input(s)        : TskId          - Task Id                          */
/*                  : u4Events       - The Events, OR-d                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events)
{
    UINT4               u4Idx = (UINT4) TskId;

    gaOsixTsk[u4Idx].u4Events |= u4Events;

    if ((gaOsixTsk[u4Idx].TskMutex) != 0)
    {
        signal_sem (gaOsixTsk[u4Idx].TskMutex);
    }

    return (OSIX_SUCCESS);

}

/************************************************************************/
/*  Function Name   : OsixEvtRecv                                       */
/*  Description     : To receive a event.                               */
/*  Input(s)        : TskId             - Task Id                       */
/*                  : u4Events          - List of interested events.    */
/*  Output(s)       : pu4EventsReceived - Events received.              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flg,
             UINT4 *pu4RcvEvents)
{
    UINT4               u4Idx = (UINT4) TskId;

    *pu4RcvEvents = 0;

    if ((u4Flg == OSIX_NO_WAIT) &&
        (((gaOsixTsk[u4Idx].u4Events) & u4Events) == 0))
    {
        return (OSIX_FAILURE);
    }

    while (1)
    {
        if (((gaOsixTsk[u4Idx].u4Events) & u4Events) != 0)
        {                        /* A required event has happened */
            *pu4RcvEvents = (gaOsixTsk[u4Idx].u4Events) & u4Events;
            gaOsixTsk[u4Idx].u4Events &= ~(*pu4RcvEvents);
            return (OSIX_SUCCESS);
        }

        wait_sem (gaOsixTsk[u4Idx].TskMutex);
    }
}

/************************************************************************/
/*  Function Name   : OsixInitialize                                    */
/*  Description     : The OSIX Initialization routine. To be called     */
/*                    before any other OSIX functions are used.         */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixInitialize (void)
{
    UINT4               u4Idx;

    /* Mutual exclusion semaphore to add or delete elements from */
    /* name-id-mapping list. This semaphore itself will not be   */
    /* added to the name-to-id mapping list. This is a OSE */
    /* specific call and must be mapped to relevant call for OS  */

    gOsixMutex = (tOsixSemId) create_sem (1);

    gSysTicks = system_tick ();

    if (gOsixMutex <= 0)
    {
        return (OSIX_FAILURE);
    }

    /* Initialize all arrays. */
    for (u4Idx = 0; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u4Events = 0;
        gaOsixTsk[u4Idx].TskMutex = 0;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixShutDown                                      */
/*  Description     : Stops OSIX.                                       */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixShutDown ()
{
    UINT4               u4Idx;

    /* Re-initialize all arrays and delete global semaphore */
    for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
    {
        if (gaOsixTsk[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixTskDel (u4Idx);
        }
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u4Events = 0;
        gaOsixTsk[u4Idx].TskMutex = 0;
    }

    for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        if (gaOsixSem[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixSemDel ((gaOsixSem[u4Idx].SemId));
        }

        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
    }
    for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if (gaOsixQue[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixQueDel ((tOsixQId) (gaOsixQue[u4Idx].u4RscId));
        }
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
    }

    kill_sem ((SEMAPHORE *) gOsixMutex);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing semaphores                                     */
/* Keep it simple - support 1 type of semaphore - binary, blocking.     */
/* After creating, the semaphore must be given before it can be taken.  */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixSemCrt                                        */
/*  Description     : Creates a sema4.                                  */
/*  Input(s)        : au1Name [ ] - Name of the sema4.                  */
/*  Output(s)       : pSemId         - The sema4 Id.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemCrt (UINT1 au1Name[], tOsixSemId * pSemId)
{
    *pSemId = (tOsixSemId) create_sem (0);
    if (*pSemId == 0)
    {
        return OSIX_FAILURE;
    }
    if (OsixRscAdd (au1Name, OSIX_SEM, *pSemId) == OSIX_FAILURE)
    {
        kill_sem ((SEMAPHORE *) (*pSemId));
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);

}

/************************************************************************/
/*  Function Name   : OsixSemDel                                        */
/*  Description     : Deletes a sema4.                                  */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixSemDel (tOsixSemId SemId)
{
    OsixRscDel (OSIX_SEM, (UINT4) SemId);
    kill_sem ((SEMAPHORE *) SemId);
}

/************************************************************************/
/*  Function Name   : OsixSemGive                                       */
/*  Description     : Used to release a sem4.                           */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixSemGive (tOsixSemId SemId)
{
    signal_sem ((SEMAPHORE *) SemId);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixSemTake                                       */
/*  Description     : Used to acquire a sema4.                          */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemTake (tOsixSemId SemId)
{
    wait_sem ((SEMAPHORE *) SemId);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing message queues                                 */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixQueCrt                                        */
/*  Description     : Creates a OSIX Q.                                 */
/*  Input(s)        : au1name[ ] - The Name of the Queue.               */
/*                  : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MaxMsgLen- Max length of a messages             */
/*  Output(s)       : pQueId     - The QId returned.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueCrt (UINT1 au1Name[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId)
{
    *pQueId = (tOsixQId) OSE_Create_MsgQ (u4MaxMsgs, u4MaxMsgLen);
    if (*pQueId == (tOsixQId) NULL)
    {
        return (OSIX_FAILURE);
    }
    if (OsixRscAdd (au1Name, OSIX_QUE, *pQueId) == OSIX_FAILURE)
    {
        OSE_Delete_MsgQ ((tOseQId) (*pQueId));
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueDel                                        */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
void
OsixQueDel (tOsixQId QueId)
{
    OsixRscDel (OSIX_QUE, (UINT4) QueId);
    OSE_Delete_MsgQ ((tOseQId) QueId);
    return;
}

/************************************************************************/
/*  Function Name   : OsixQueSend                                       */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*                  : u4MsgLen - length of the messages                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen)
{
    u4MsgLen = 0;

    /* Typically native OS calls take message Length as an argument.
     * In the case of OSE Queues, the value supplied in the call
     * OsixQueCrt is used implicitly as the message length.
     * Hence the u4MsgLen parameter is not used in this function.
     */
    if (OSE_Send_MsgQ ((tOseQId) QueId, pu1Msg) != 0)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueRecv                                       */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : u4MsgLen -  length of the messages                */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4Timeout)
{
    u4MsgLen = 0;

    /* Typically native OS calls take message Length as an argument.
     * In the case of Pthreads Queues, the value supplied in the call
     * OsixQueCrt is used implicitly as the message length.
     * Hence the u4MsgLen parameter is not used in this function.
     */
    if (i4Timeout > 0)
    {
        i4Timeout = (INT4) OsixSTUPS2Ticks ((UINT4) i4Timeout);
    }

    if (OSE_Receive_MsgQ ((tOseQId) QueId, pu1Msg, i4Timeout) != 0)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueNumMsgs                                    */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : pu4NumberOfMsgs - Contains count upon return.     */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixQueNumMsg (tOsixQId QueId, UINT4 *pu4NumMsg)
{
    *pu4NumMsg = (UINT4) (OSE_MsgQ_NumMsgs ((tOseQId) QueId));
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing resources based on names                       */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixRscAdd                                        */
/*  Description     : Gets a free resouce, stores Resource-Id & Name    */
/*                  : This helps in locating Resource-Id by Name        */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (OsixSemTake (gOsixMutex) != 0)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixTsk[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixTsk[u4Idx].u4Events = 0;
                    gaOsixTsk[u4Idx].TskMutex = 0;
                    MEMCPY (gaOsixTsk[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixSem[u4Idx].u2Filler = 0;
                    gaOsixSem[u4Idx].SemId = u4RscId;
                    MEMCPY (gaOsixSem[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixQue[u4Idx].u4RscId = u4RscId;
                    gaOsixQue[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixQue[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixQue[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }

    OsixSemGive (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscDel                                        */
/*  Description     : Free an allocated resouce                         */
/*  Input(s)        : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixRscDel (UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (OsixSemTake (gOsixMutex) != 0)
    {
        return;
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            gaOsixTsk[u4RscId].u2Free = OSIX_TRUE;
            gaOsixTsk[u4RscId].TskMutex = 0;
            MEMSET (gaOsixTsk[u4RscId].au1Name, '\0', (OSIX_NAME_LEN + 4));
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (((gaOsixSem[u4Idx].SemId)) == u4RscId)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixSem[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u4RscId) == u4RscId)
                {
                    gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixQue[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (gOsixMutex);
}

/************************************************************************/
/*  Function Name   : OsixRscFind                                       */
/*  Description     : This locates Resource-Id by Name                  */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*  Output(s)       : pu4RscId - Osix Resource-Id                       */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }
    if (OsixSemTake (gOsixMutex) != 0)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_TSKS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixTsk[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* For tasks applications know only our array index. */
                    /* This helps us to simulate events.                 */
                    *pu4RscId = u4Idx;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixSem[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* pThread version of OsixRscFind returns pointer to semId */
                    *pu4RscId = (UINT4) (gaOsixSem[u4Idx].SemId);
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixQue[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = gaOsixQue[u4Idx].u4RscId;
                    OsixSemGive (gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : Fsap2Start                                        */
/*  Description     : Function to be called to get any fsap2 application*/
/*                  : to work                                           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Fsap2Start ()
{
    /* To view o/p generated by appln, current running process is suspended */
    stop (current_process ());
    /*** OS specific code end ***/
}

/************************************************************************/
/*  Function Name   : OsixGetSysTime                                    */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysTime (tOsixSysTime * pSysTime)
{
    *pSysTime = (tOsixSysTime) get_ticks ();
    *pSysTime = OsixTicks2STUPS (*pSysTime);

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetSysUpTime                                  */
/*  Description     : Returns the system time in Seconds.               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysUpTime (VOID)
{
    return (gu4Seconds);
}

/************************************************************************/
/*  Function Name   : OsixGetTps                                        */
/*  Description     : Returns the no of system ticks in a second        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : Number of system ticks in a second                */
/************************************************************************/
UINT4
OsixGetTps ()
{
    return (OSIX_TPS);
}

/************************************************************************/
/*  Function Name   : OsixExGetTaskName                                 */
/*  Description     : Get the OSIX task Name given the Osix Task-Id.    */
/*  Input(s)        : TaskId - The Osix taskId.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to OSIX task name                         */
/************************************************************************/
const UINT1        *
OsixExGetTaskName (tOsixTaskId TskId)
{
    /* To facilitate direct use of this call in a STRCPY,
     * we return a null string instead of a NULL pointer.
     * A null pointer is returned for all non-OSIX tasks
     * e.g., TMO's idle task.
     */
    if (TskId)
    {
        return ((UINT1 *) (gaOsixTsk[(UINT4) TskId].au1Name));
    }
    return ((const UINT1 *) "");
}

/************************************************************************/
/*  Function Name   : OSE_Create_MsgQ                                   */
/*  Description     : Creates a queue using a linear block of memory.   */
/*  Input(s)        : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MsgLen- Max length of a messages             */
/*  Output(s)       : None                                              */
/*  Returns         : Queue-Id, NULL if creation fails                  */
/************************************************************************/
tOseQId
OSE_Create_MsgQ (UINT4 u4MaxMsgs, UINT4 u4MsgLen)
{
    tOseQ              *pOseQ;

    /* Allocate memory for holding messages. Create a semaphore for      */
    /* protection between multiple simultaneous calls to write or read   */
    /* Initialize the read and write pointers to the Q start location    */
    /* Initia the pointer marking the end of the queue's memory location */
    pOseQ = (tOseQ *) malloc (((u4MaxMsgs + 1) * u4MsgLen) + sizeof (tOseQ));
    if (pOseQ == NULL)
    {
        return (NULL);
    }
    pOseQ->pQBase = (UINT1 *) ((UINT1 *) pOseQ + sizeof (tOseQ));

    pOseQ->MSem = (tOsixSemId) create_sem (1);
    pOseQ->BSem = (tOsixSemId) create_sem (1);

    pOseQ->pQEnd = (pOseQ->pQBase) + ((u4MaxMsgs + 1) * u4MsgLen);
    pOseQ->pQRead = pOseQ->pQBase;
    pOseQ->pQWrite = pOseQ->pQBase;
    pOseQ->u4MsgLen = u4MsgLen;

    return (pOseQ);
}

/************************************************************************/
/*  Function Name   : OSE_Delete_MsgQ                               */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OSE_Delete_MsgQ (tOseQId QId)
{
    tOseQ              *pOseQ = (tOseQ *) QId;
    /* Wait for semaphore to ensure that when the queue is deleted */
    /* no one is reading from or writing into it. Then delete the  */
    /* semaphore, free the queue memory and initialize queue start */
    if (OsixSemTake (pOseQ->MSem))
    {
        return;
    }
    OsixSemDel (pOseQ->MSem);
    OsixSemDel (pOseQ->BSem);
    free ((VOID *) pOseQ);
}

/************************************************************************/
/*  Function Name   : OSE_Send_MsgQ                                     */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QId -    The Q Id.                              */
/*                  : pMsg -   Pointer to message to be sent.         */
/*  Output(s)       : None                                              */
/*  Returns         : 0 on SUCCESS and (-1) on FAILURE                  */
/************************************************************************/
INT4
OSE_Send_MsgQ (tOseQId QId, UINT1 *pMsg)
{
    tOseQ              *pOseQ = (tOseQ *) QId;
    UINT1              *pWrite, *pRead, *pBase, *pEnd;
    UINT4               u4MsgLen;

    /* Ensure mutual exclusion. Wait and take the mutual exclusion        */
    /* semaphore. A write is possible if the queue is not full. Queue is  */
    /* recognized as full if by writing one more message, write and read  */
    /* pointers become equal. Actually, this means that the queue holds   */
    /* only u4MaxMsgs-1 messages to be safe. When checking the pointers   */
    /* or when advancing the write pointer after the write operation,     */
    /* take care of the wrap-around since this is a circular queue. When  */
    /* the message is written, advance the write pointer by u4MsgLen.     */
    if (OsixSemTake (pOseQ->MSem))
    {
        return (-1);
    }

    pWrite = pOseQ->pQWrite;
    pRead = pOseQ->pQRead;
    pBase = pOseQ->pQBase;
    pEnd = pOseQ->pQEnd;
    u4MsgLen = pOseQ->u4MsgLen;

    if (((pWrite + u4MsgLen) == pEnd) && (pRead == pBase))
    {
        OsixSemGive (pOseQ->MSem);
        return (-1);
    }
    if ((pWrite + u4MsgLen) == pRead)
    {
        OsixSemGive (pOseQ->MSem);
        return (-1);
    }
    memcpy (pWrite, pMsg, u4MsgLen);
    (pOseQ->pQWrite) += u4MsgLen;

    if ((pOseQ->pQWrite) == pEnd)
    {
        (pOseQ->pQWrite) = pBase;
    }
    OsixSemGive (pOseQ->BSem);    /* unblock anyone waiting to read    */
    OsixSemGive (pOseQ->MSem);    /* allow others to read/write/delete */
    return (0);
}

/************************************************************************/
/*  Function Name   : OSE_Receive_MsgQ                              */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QId -     The Q Id.                             */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pMsg -    Pointer to message to be sent.        */
/*  Returns         : 0 on SUCCESS and (-1) on FAILURE                  */
/************************************************************************/
INT4
OSE_Receive_MsgQ (tOseQId QId, UINT1 *pMsg, INT4 i4Timeout)
{
    tOseQ              *pOseQ = (tOseQ *) QId;

    /* Only FM task/thread reads from the queue. Multiple other tasks may */
    /* write. Deletion of the queue is also done only by the FM task. So  */
    /* a semaphore for mutual exclusion is needed for writing but not for */
    /* reading. Only a blocking semaphore to wait for a message is needed */
    /* if the queue is empty.                                             */

    /* Check if queue exists. If yes, wait and take the mutual exclusion  */
    /* semaphore. A read is possible if the queue is not empty. Queue is  */
    /* recognized as empty if write and read pointers are equal i.e. all  */
    /* the written messages have already been read.                       */
    if (OsixSemTake (pOseQ->MSem))
    {
        return (-1);
    }

    /* NO_WAIT case: if i4Timeout is 0, it is NO_WAIT. */
    if (!i4Timeout && (pOseQ->pQWrite) == (pOseQ->pQRead))
    {
        OsixSemGive (pOseQ->MSem);
        return -1;
    }
    while ((pOseQ->pQWrite) == (pOseQ->pQRead))
    {
        /* Queue is empty. Block on the semaphore. When a write is done */
        /* the writer will release the semaphore and unblock this wait. */
        /* Before blocking, allow someone else to write into queue.     */
        OsixSemGive (pOseQ->MSem);

        if (OsixSemTake (pOseQ->BSem))
        {
            return (-1);
        }
        if (OsixSemTake (pOseQ->MSem))
        {
            return (-1);
        }
    }

    /* There is at least 1 message in the queue and we have locked the */
    /* mutual exclusion semaphore so nobody else changes the state.    */
    memcpy (pMsg, pOseQ->pQRead, pOseQ->u4MsgLen);
    (pOseQ->pQRead) += (pOseQ->u4MsgLen);
    if ((pOseQ->pQRead) == (pOseQ->pQEnd))
    {
        (pOseQ->pQRead) = (pOseQ->pQBase);
    }
    OsixSemGive (pOseQ->MSem);
    return (0);
}

/************************************************************************/
/*  Function Name   : OSE_MsgQ_NumMsgs                              */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QId -     The Q Id.                             */
/*  Output(s)       : None                                              */
/*  Returns         : pu4NumberOfMsgs - Contains count upon return.     */
/************************************************************************/
UINT4
OSE_MsgQ_NumMsgs (tOseQId QId)
{
    tOseQ               OseQ = *((tOseQ *) QId);
    UINT4               u4Msgs;

    if ((OseQ.pQWrite) < (OseQ.pQRead))
    {
        u4Msgs = (OseQ.pQWrite) - (OseQ.pQBase) + (OseQ.pQEnd) - (OseQ.pQRead);
        return (u4Msgs / (OseQ.u4MsgLen));
    }
    else
    {
        return (((OseQ.pQWrite) - (OseQ.pQRead)) / (OseQ.u4MsgLen));
    }
}

/************************************************************************/
/*  Function Name   : Dummy Process                                     */
/*  Description     : Waits to receive the signal which has task        */
/*                    starting address & arguments and calls the task   */
/*  Input(s)        :                                                   */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       : None                                              */
/*  Returns         :                                                   */
/************************************************************************/
OS_PROCESS (RcvTaskArgs)
{
    union SIGNAL       *RcvTaskAddrArgs;
    static SIGSELECT    RcvOnlyTaskAddrArgs[2];

    void                (*TaskStartAddr) ();
    UINT4               u4TaskStartArgs;

    /* The first element of SIGSELECT arr is number of wanted signals.
       The second element is to receive the signal of type mentioned */
    RcvOnlyTaskAddrArgs[0] = 1;
    RcvOnlyTaskAddrArgs[1] = TASK_SIG_NUM;

    /* Wait to receive the task start address and arguments */
    RcvTaskAddrArgs = receive ((SIGSELECT *) RcvOnlyTaskAddrArgs);

    /* check if expected signal is received */
    if (RcvTaskAddrArgs->sig_no == TASK_SIG_NUM)
    {
        TaskStartAddr = RcvTaskAddrArgs->TaskStartAddrArgs.pvTaskStartAddr;
        u4TaskStartArgs = RcvTaskAddrArgs->TaskStartAddrArgs.u4TaskStartArgs;

        /* Release the buffer allocated for signal */
        free_buf (&RcvTaskAddrArgs);

        /* Call the actual task with arguments */
        (*TaskStartAddr) (u4TaskStartArgs);

        /* Suspend the current process incase it drops out       */
        /* This is needed for proper operation on targetboards   */
        stop (current_process ());
    }

}

/************************************************************************
 *  Function Name   : FileOpen
 *  Description     : Function to Open a file.
 *  Input           : pu1FileName - Name of file to open.
 *                    i4Mode      - whether r/w/rw.
 *  Returns         : FileDescriptor if successful
 *                    -1             otherwise.
 ************************************************************************/
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4Mode)
{
    /* Has to be ported if needed */
    (void) pu1FileName;
    (void) i4Mode;
    return (-1);
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description     : Function to close a file.
 *  Input           : i4Fd - File Descriptor to be closed.
 *  Returns         : 0 on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    /* Has to be ported if needed */
    (void) i4Fd;
    return (-1);
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description     : Function to read a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer into which to read
 *                    i4Count - Number of bytes to read
 *  Returns         : Number of bytes read on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    /* Has to be ported if needed */
    (void) i4Fd;
    (void) pBuf;
    (void) i4Count;
    return (0);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description     : Function to write to a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : Number of bytes written on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    /* Has to be ported if needed */
    (void) i4Fd;
    (void) pBuf;
    (void) i4Count;
    return (0);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description     : Function to delete a file.
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : 0 on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    /* Has to be ported if needed */
    (void) pu1FileName;
    return (-1);
}

/************************************************************************/
/*  Function Name   : OsixSetLocalTime                                  */
/*  Description     : Obtains the system time and sets it in FSAP.      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSetLocalTime (void)
{
    time_t              t;
    struct tm          *tm;

    time (&t);
    tm = localtime (&t);
    if (tm == NULL)
    {
        return (OSIX_FAILURE);
    }
    tm->tm_year += (1900);
    UtlSetTime ((tUtlTm *) tm);
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : OsixGetMemInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4TotalMem - Total memory
 *                    pu4FreeMem - Free memory
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetMemInfo (UINT4 *pu4TotalMem, UINT4 *pu4FreeMem)
{
    *pu4TotalMem = 0;
    *pu4FreeMem = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetCPUInfo
 *  Description     : Function used to get the CPU utilization.
 *  Inputs          : None
 *  OutPuts         : pu4TotalUsage - Total Usage
 *                  : pu4CPUUsage - CPU Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetCPUInfo (UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage)
{
    *pu4TotalUsage = 0;
    *pu4CPUUsage = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetFlashInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4FlashUsage - Flash Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetFlashInfo (UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash)
{
    *pu4TotalUsage = 0;
    *pu4FreeFlash = 0;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixSysRestart                                    */
/*  Description     : This function reboots the system.                 */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSysRestart ()
{
    /* Killing the current process for shutdown */
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                    to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}
