/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmtmri.h,v 1.16 2015/04/28 12:16:32 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/
#ifndef _SRMTMRI_H
#define _SRMTMRI_H

#define TMR_FREE    0
#define TMR_USED    1

/************************************************************************
*                                                                       *
*                      Defines and Typedefs                             *
*                                                                       *
*************************************************************************/

/********************************************
*** Application Timer List Data Structure ***
*********************************************/
typedef struct TMO_APP_TIMER_LIST{
    tTMO_DLL    Link;
    INT4        i4RemainingTime;           /*** Time till next expiry ***/
    tOsixTaskId TskId;
    UINT4       u4Event;
    UINT4       u4Status;
    void        (*CallBackFunction)(tTimerListId);
    tOsixSemId  SemId;
   /* 
    * The List of timers which have expired.
    * To be read by the application.
    */
   tTMO_DLL       ExpdList;
}tTmrAppTimerList;

typedef tTmrAppTimerList tTimerList;
/************************************************************************
*                                                                       *
*                          Macro                                        *
*                                                                       *
*************************************************************************/
#define TMR_MUTEX_NAME   (UINT1 *)"TMMU"
#define TMR_ENTER_CS()   (OsixSemTake(TmrMutex))
#define TMR_LEAVE_CS()   (OsixSemGive(TmrMutex))

#define TmrLock()        TMR_ENTER_CS()
#define TmrUnLock(u4val) TMR_LEAVE_CS()

#define TMR_DBG_FLAG gu4TmrDbg
#define TMR_MODNAME  "TMR"

#if DEBUG_TMR == FSAP_ON
#define TMR_DBG(x) UtlTrcLog x
#else
#define TMR_DBG(x)

#endif
#define TMR_PRNT(x) UtlTrcPrint(x)

VOID TmrDumpList(UINT4);

#endif /*_SRMTMRI_H*/
