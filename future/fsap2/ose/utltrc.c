/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utltrc.c,v 1.14 2015/04/28 12:16:32 siva Exp $
 *
 * Description: This file contains the APIs for tracing and dumping,
 *                which forms a part of the FutureUTL library
 *
 *******************************************************************/

#include "osxinc.h"
#include "utltrc.h"
#include "utltrci.h"

/***************************************************************/
/*  Function Name   : UtlTrcPrint                              */
/*  Description     : Prints the input message                 */
/*  Input(s)        : pi1Msg - pointer to the message          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlTrcPrint (const char *pi1Msg)
{
    printf ("%s", pi1Msg);
}

/***************************************************************/
/*  Function Name   : UtlTrcLog                                */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST char *pi1Name,
           CONST char *pi1Fmt, ...)
{
    va_list             VarArgList;
    INT1                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST char         *pi1FmtString;
    INT4                i4ArgCount;
    INT4                pos = 0;

    if (!(u4Flag & u4Value))
    {
        /* Tracing for this type not enabled */
        return;
    }

    if (pi1Name == NULL)
    {
        UtlTrcPrint ("\nInvalid Module Name\n");
        return;
    }

    /* Tagging the Module Name to the front of the message */
    pos += SPRINTF (ai1LogMsgBuf, "%s: ", pi1Name);

    /* Checking for Arguments count limit */
    pi1FmtString = pi1Fmt;
    i4ArgCount = 0;

    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                i4ArgCount++;
            }
        }
        pi1FmtString++;
    }
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }

    va_start (VarArgList, pi1Fmt);

    vsprintf (ai1LogMsgBuf + pos, pi1Fmt, VarArgList);

    UtlTrcPrint (ai1LogMsgBuf);

    va_end (VarArgList);
}
#else /* VAR_ARGS_SUPPORT */
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST INT1 *pi1Name,
           CONST INT1 *pi1Fmt, INT4 p1, INT4 p2, INT4 p3,
           INT4 p4, INT4 p5, INT4 p6)
{
    INT4                i4ArgCount;
    INT1                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST INT1         *pi1FmtString;

    if (!(u4Flag & u4Value))
    {
        /* Tracing for this type not enabled */
        return;
    }

    if (pi1Name == NULL)
    {
        UtlTrcPrint ("\nInvalid Module Name\n");
        return;
    }

    pi1FmtString = pi1Fmt;

    /* Tagging the Module Name to the front of the message */
    SPRINTF (ai1LogMsgBuf, "%s: ", pi1Name);

    UtlTrcPrint (ai1LogMsgBuf);

    /* Count the number of arguments */
    i4ArgCount = 0;

    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                i4ArgCount++;
            }
        }
        pi1FmtString++;
    }

    switch (i4ArgCount)
    {
        case 0:
            SPRINTF (ai1LogMsgBuf, pi1Fmt);
            break;
        case 1:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1);
            break;
        case 2:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2);
            break;
        case 3:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3);
            break;
        case 4:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4);
            break;
        case 5:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5);
            break;
        case 6:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5, p6);
            break;
        default:
            SPRINTF (ai1LogMsgBuf, "Number of arguments > 6.\n");
            break;
    }

    UtlTrcPrint (ai1LogMsgBuf);
}
#endif /* VAR_ARGS_SUPPORT */

/***************************************************************/
/*  Function Name   : UtlSysTrcLog                             */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4SysLevel - Syslog message levels       */
/*                    u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST char *pi1ModId, CONST char *pi1Name,
           CONST char *pi1Fmt, ...)
{
      UNUSED_PARAM (u4SysLevel);
      UNUSED_PARAM (u4Flag);
      UNUSED_PARAM (u4Value);
      UNUSED_PARAM (pi1Name);
      UNUSED_PARAM (pi1ModId);
      UNUSED_PARAM (pi1Fmt);

}
#else /* VAR_ARGS_SUPPORT */
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST INT1 *pi1ModId, CONST INT1 *pi1Name,
           CONST INT1 *pi1Fmt, INT4 p1, INT4 p2, INT4 p3,
           INT4 p4, INT4 p5, INT4 p6)
{
      UNUSED_PARAM (u4SysLevel);
      UNUSED_PARAM (u4Flag);
      UNUSED_PARAM (u4Value);
      UNUSED_PARAM (pi1Name);
      UNUSED_PARAM (pi1ModId);
      UNUSED_PARAM (pi1Fmt);
      UNUSED_PARAM (p1);
      UNUSED_PARAM (p2);
      UNUSED_PARAM (p3);
      UNUSED_PARAM (p4);
      UNUSED_PARAM (p5);
      UNUSED_PARAM (p6);
}
#endif /* VAR_ARGS_SUPPORT */
