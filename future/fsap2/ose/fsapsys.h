/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapsys.h,v 1.13 2015/04/28 12:16:32 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */
#ifndef _FSAPSYS_H
#define _FSAPSYS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>
#include <stdarg.h>

#endif /*FSAPSYS_H*/
