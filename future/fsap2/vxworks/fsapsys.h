/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapsys.h,v 1.12 2015/04/28 12:21:07 siva Exp $
 *
 * Description: System Headers exported from FSAP.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "math.h"
#include "assert.h"
#include <time.h>
#if (defined(OS_VX2PTH_WRAP) || defined(OS_CPSS_MAIN_OS))
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#else
#include "types/vxCpu.h"
#include "taskLib.h"
#endif
