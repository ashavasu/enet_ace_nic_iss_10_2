/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osxinc.h,v 1.13 2015/04/28 12:21:07 siva Exp $
 *
 * Description:
 * Header of all headers.
 *
 *******************************************************************/

#ifndef OSX_INC_H
#define OSX_INC_H

#include <stdio.h>
#ifdef OS_VX2PTH_WRAP 
#include <errno.h>
#include <unistd.h>
#include <sched.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/time.h>
#include <string.h>
#include "v2pthread.h"
#include "vxw_defs.h"
#include "vxw_hdrs.h"
#else
#include <vxWorks.h>
#include <errnoLib.h>
#include <objLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <semLib.h>
#include <sysLib.h>
#include <tickLib.h>
#include <intLib.h>
#include <iv.h>
#include <wdLib.h>
#include <logLib.h>
#endif
#ifdef NETBUF_WANTED
#include <netBufLib.h>
#endif /* NETBUF_WANTED */

#ifdef MEM_FREE
#undef MEM_FREE
#endif

#include "osxstd.h"
#include "srmmem.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utlmacro.h"
#include "utltrc.h"
#include "osxprot.h"
#include "utlsll.h"
#include "utlhash.h"

#endif          /* !OSX_INC_H */
