/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmtmr.c,v 1.24 2015/04/28 12:21:07 siva Exp $
 *
 * Description:
 * The SRM Timer Module.
 *
 */

/************************************************************************
*                                                                       *
*                          Header  Files                                *
*                                                                       *
*************************************************************************/
#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "osix.h"

#include "utldll.h"
#include "srmtmr.h"
#include "srmtmri.h"
#include "utltrc.h"
#include "utlmacro.h"
#include <time.h>
#include <sys/times.h>

/************************************************************************
*                                                                       *
*                   Externs                                             *
*                                                                       *
*************************************************************************/
extern UINT4        gu4Tps;
extern UINT4        gu4Stups;
extern UINT4        gu4TimerSpeed;

/************************************************************************
*                                                                       *
*               Static variables and Functions.                         *
*                                                                       *
*************************************************************************/
UINT4               gu4Seconds;
static UINT4        gu4StupsCounter;
static UINT4        gu4TmrInitialized = 0;

/* The timer-lists returned through TmrCreateTimerList */
static tTmrAppTimerList *gaTimerLists;

/* Maximum no. of timer lists */
static UINT4        gu4MaxLists;

/* Mutex used within Tmr */
static tOsixSemId   TmrMutex;

static tOsixTaskId  TmrTskId;

/**
 * Data-structures and constants related to timer-wheels implementation.
 **/
struct Wheel
{
    UINT4               u4StepSize;
    UINT4               u4MaxSteps;
    UINT4               u4CurStep;
    tTMO_DLL           *pActiveList;
};

/* Default Wheel Sizes - 60 seconds, 60 minutes, 24 hours, 7 days. */
static UINT4        au4WheelSizes[] = { 0, 60, 60, 24, 7 };

#define TMR_MAX_WHEELS (sizeof(au4WheelSizes)/sizeof(au4WheelSizes[0]))
static struct Wheel Wheel[TMR_MAX_WHEELS + 1];
static UINT4        gu4MaxWheels;
static tTMO_DLL    *gpActiveList;

/* Bit-masks for u2Flags field of tTmrAppTimer */
enum
{
    TMR_RUNNING = 0x1,
    TMR_EXPD = 0x2
};

/* Macros to determine timer-status */
#define TMR_IS_RUNNING(pTmrNode) ((pTmrNode)->u2Flags & TMR_RUNNING)
#define TMR_IS_EXPIRED(pTmrNode) ((pTmrNode)->u2Flags & TMR_EXPD)

static VOID         TmrTaskMain (INT1 *);
static UINT4        TmrDeleteNode (tTimerListId, tTmrAppTimer *);

#if (DEBUG_TMR == FSAP_ON)
static UINT4        gu4TmrDbg = TMR_DBG_CRITICAL | TMR_DBG_FATAL;
#endif

/* Variables and functions related to timer checkpointing */
#if (DEBUG_TMR == FSAP_ON)
static UINT4        gu4ALSize;
static tTMO_DLL    *gpActiveListCopy;
static void         TmrDbgWalk (void);
#endif

/************************************************************************/
/*  Function Name   : TmrTimerInit                                      */
/*  Description     : Creates timer lists, initializes the active and   */
/*                  : Expired Timer Lists.                              */
/*  Input(s)        :                                                   */
/*                  : pTimerCfg - Pointer to configuration structure.   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrTimerInit (tTimerCfg * pTimerCfg)
{
    UINT4               u4rc = TMR_SUCCESS;
    UINT4               u4Idx;
    UINT4               u4Wheel;
    UINT4               u4TotSteps = 0;
    UINT4               u4StaticWheel;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    tTMO_DLL           *pActiveList;

    if (!gu4TmrInitialized)
    {
        /*** Allocate timerlists ***/
        gaTimerLists =
            MEM_MALLOC ((sizeof (tTimerList) * pTimerCfg->u4MaxTimerLists),
                        tTimerList);
        if (gaTimerLists == NULL)
            return (TMR_FAILURE);

        gu4MaxLists = pTimerCfg->u4MaxTimerLists;
        MEMSET (gaTimerLists, 0, gu4MaxLists * sizeof (tTimerList));
        for (u4Idx = 0; u4Idx < gu4MaxLists; ++u4Idx)
        {
            TMO_DLL_Init (&gaTimerLists[u4Idx].ExpdList);
        }

        /*** Create the mutex sema4 for internal use. ***/
        MEMSET (au1Name, '\0', (OSIX_NAME_LEN + 4));
        STRCPY (au1Name, "TMMU");
        u4rc = OsixSemCrt (au1Name, &TmrMutex);
        if (u4rc)
        {
            MEM_FREE (gaTimerLists);
            return TMR_FAILURE;
        }
        OsixSemGive (TmrMutex);

        /*** Create timer wheels ***/
        /* The first wheel is unused */
        u4Wheel = 0;
        Wheel[u4Wheel].u4MaxSteps = 1;
        Wheel[u4Wheel].u4StepSize = 1;
        Wheel[u4Wheel].u4CurStep = 0;
        ++u4Wheel;

        /**
         * Create the Wheels:
         * We use one call to MALLOC to allocate all the space that we'd need
         * for the DLLs and then aportion it among the wheels by suitable ptr
         * manipulation.
         *
         * First we check if STUPS > 1. If so we create an extra wheel for
         * the sub-second dimention.
         **/
        gu4MaxWheels = TMR_MAX_WHEELS - 1;

        if (gu4Stups > 1)
        {
            ++gu4MaxWheels;
            Wheel[u4Wheel].u4MaxSteps = gu4Stups;
            Wheel[u4Wheel].u4StepSize = 1;
            Wheel[u4Wheel].u4CurStep = 0;
            u4TotSteps += Wheel[u4Wheel].u4MaxSteps;
            ++u4Wheel;
        }
        for (u4StaticWheel = 1; u4Wheel <= gu4MaxWheels;
             ++u4Wheel, ++u4StaticWheel)
        {
            Wheel[u4Wheel].u4CurStep = 0;
            Wheel[u4Wheel].u4MaxSteps = au4WheelSizes[u4StaticWheel];
            Wheel[u4Wheel].u4StepSize =
                Wheel[u4Wheel - 1].u4StepSize * Wheel[u4Wheel - 1].u4MaxSteps;

            u4TotSteps += Wheel[u4Wheel].u4MaxSteps;
        }
        gpActiveList = MEM_MALLOC (sizeof (tTMO_DLL) * u4TotSteps, tTMO_DLL);
        if (gpActiveList == NULL)
        {
            MEM_FREE (gaTimerLists);
            OsixSemDel (TmrMutex);
            return (TMR_FAILURE);
        }
        pActiveList = gpActiveList;

#if (DEBUG_TMR == FSAP_ON)
        gpActiveListCopy =
            MEM_MALLOC (sizeof (tTMO_DLL) * u4TotSteps, tTMO_DLL);
        if (gpActiveListCopy == NULL)
        {
            MEM_FREE (gaTimerLists);
            MEM_FREE (gpActiveList);
            OsixSemDel (TmrMutex);
            return (TMR_FAILURE);
        }
        gu4ALSize = sizeof (tTMO_DLL) * u4TotSteps;
#endif

        /* Set the pointers to the DLLs from the memory allocated above */
        /* Initialize the Wheels and DLLs.                              */
        for (u4Wheel = 1; u4Wheel <= gu4MaxWheels; ++u4Wheel)
        {
            Wheel[u4Wheel].pActiveList = pActiveList;
            pActiveList += Wheel[u4Wheel].u4MaxSteps;

            for (u4Idx = 0; u4Idx < Wheel[u4Wheel].u4MaxSteps; ++u4Idx)
                TMO_DLL_Init (&Wheel[u4Wheel].pActiveList[u4Idx]);
        }

        /* The first time the copy needs to be done explicity.
         * Other times it gets done alongwith TmrLock().
         */
        TMR_DBG_CPY ();

        /*** Set Globals ***/
        gu4TmrInitialized = 1;
        gu4Seconds = 0;
        gu4StupsCounter = gu4Stups;
        au4WheelSizes[0] = gu4Stups;

        /*** Create timer task. ***/
        MEMSET (au1Name, '\0', (OSIX_NAME_LEN + 4));
        STRCPY (au1Name, "TMR#");
        OsixTskCrt (au1Name, 5 | OSIX_SCHED_RR,
                    17000, TmrTaskMain, 0, &TmrTskId);

        return (TMR_SUCCESS);
    }

    /*** Timer Library Already Initialized ***/
    return (TMR_FAILURE);
}

/************************************************************************/
/*  Function Name   : TmrTimerShutdown                                  */
/*  Description     : Frees resources allocated in Init.                */
/*                  : To be called when closing down timer service.     */
/*                  : Expired Timer Lists.                              */
/*                  : Timer blocks present will have to be reclaimed    */
/*                  : by the applications, before calling this routine. */
/*  Input(s)        :                                                   */
/*                  : pTimerCfg - Pointer to configuration structure.   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrTimerShutdown (void)
{
    if (gu4TmrInitialized == 0)
    {
        return (TMR_FAILURE);
    }

    OsixTskDel (TmrTskId);
    OsixSemDel (TmrMutex);

    MEM_FREE (gpActiveList);
    MEM_FREE (gaTimerLists);
#if (DEBUG_TMR == FSAP_ON)
    MEM_FREE (gpActiveListCopy);
#endif
    gu4TmrInitialized = 0;
    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrCreateTimerList                                */
/*  Description     : Creates a timer list.                             */
/*  Input(s)        :                                                   */
/*                  : au1TaskName  - Name of the task, in order to send */
/*                  :                Event.                             */
/*                  : u4Event - Event corresponding to timeout.         */
/*                  : *CallBackFunction - Callback function to be       */
/*                  : in case it is given. Presence of a callback fn.   */
/*                  : takes higher precedence in the matter of reporting*/
/*                  : timeouts.                                         */
/*  Output(s)       :                                                   */
/*                  : pTimerListId - Handle to created timer list.      */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrCreateTimerList (const UINT1 au1TaskName[4],
                    UINT4 u4Event,
                    void (*CallBackFunction) (tTimerListId),
                    tTimerListId * pTimerListId)
{
    UINT4               u4Count;
    tTmrAppTimerList   *pTimerList = 0;
    tOsixTaskId         TaskId = 0;
    UINT1               au1Name[OSIX_NAME_LEN + 4], u1Index;

    if (CallBackFunction == NULL && au1TaskName == NULL)
        return TMR_FAILURE;

    /* If the expiry notification is via an event to the task, then,
     * ensure the task has been created.
     * The TaskId is required to post the expiry event event.
     * See: TmrProcessTick.
     */
    if (!CallBackFunction)
    {
        MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
        for (u1Index = 0;
             ((u1Index < OSIX_NAME_LEN) && (au1TaskName[u1Index] != '\0'));
             u1Index++)
        {
            au1Name[u1Index] = au1TaskName[u1Index];
        }

        if (OsixRscFind (au1Name, OSIX_TSK, &TaskId) == OSIX_FAILURE)
        {
            return (TMR_FAILURE);
        }
    }

    TmrLock ();
    for (u4Count = 0; u4Count < gu4MaxLists; u4Count++)
        if (gaTimerLists[u4Count].u4Status == TMR_FREE)
        {
            gaTimerLists[u4Count].u4Status = TMR_USED;
            pTimerList = &gaTimerLists[u4Count];
            break;
        }
    if (u4Count == gu4MaxLists)
    {
        TmrUnLock ();
        return (TMR_FAILURE);
    }

    *pTimerListId = (tTimerListId) pTimerList;
    pTimerList->TskId = TaskId;
    pTimerList->u4Event = u4Event;
    pTimerList->i4RemainingTime = 0;
    pTimerList->CallBackFunction = CallBackFunction;

    TMO_DLL_Init (&pTimerList->ExpdList);

    TmrUnLock ();

    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrDeleteTimerList                                */
/*  Description     : Deletes a timer list.                             */
/*  Input(s)        :                                                   */
/*                  : TimerListId - Handle to the timer list.           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrDeleteTimerList (tTimerListId TimerListId)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;

    TmrLock ();

    if (pTimerList->u4Status != TMR_USED)
    {
        TmrUnLock ();
        return TMR_FAILURE;
    }
    pTimerList->u4Status = TMR_FREE;
    TMO_DLL_Init (&pTimerList->ExpdList);

    TmrUnLock ();
    return TMR_SUCCESS;
}

/************************************************************************/
/*  Function Name   : TmrStartTimer                                     */
/*  Description     : Starts a timer of a specified duration.           */
/*                  : The duration has to be specified in terms of STUPS*/
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list in which the timer   */
/*                  :               is to be started                    */
/*                  : pReference -  The timer block which will get      */
/*                  :               linked into the timer list          */
/*                  : u4Duration -  Timeout value.                      */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrStartTimer (tTimerListId TimerListId,
               tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    UINT4               u4Wheel;
    UINT4               u4MaxSteps;
    UINT4               u4StepSize;
    UINT4               u4CurStep;
    UINT4               u4CummTime;
    UINT4               u4RemTime;
    UINT4               u4Step;
    UINT4               u4Steps;
    UINT4               u4Offset;

    if (u4Duration > Wheel[gu4MaxWheels].u4StepSize *
        Wheel[gu4MaxWheels].u4MaxSteps)
    {
        /* Timer too large. */
        return (TMR_FAILURE);
    }

    TmrLock ();

    if (TMR_IS_RUNNING (pAppTimer) || TMR_IS_EXPIRED (pAppTimer))
    {
        TmrDeleteNode (TimerListId, pAppTimer);
    }

    /* Locate the right wheel to insert into */
    u4Wheel = 1;
    u4CummTime = u4RemTime = 0;
    u4MaxSteps = Wheel[u4Wheel].u4MaxSteps;
    u4StepSize = Wheel[u4Wheel].u4StepSize;
    u4CurStep = Wheel[u4Wheel].u4CurStep;
    u4CurStep = (u4CurStep == u4MaxSteps) ? 0 : u4CurStep;

    u4Offset = 0;
    while (u4Duration > (u4CummTime + (u4MaxSteps * u4StepSize)))
    {
        u4CummTime += (u4MaxSteps - u4CurStep) * u4StepSize;
        ++u4Wheel;
        u4MaxSteps = Wheel[u4Wheel].u4MaxSteps;
        u4StepSize = Wheel[u4Wheel].u4StepSize;
        u4CurStep = Wheel[u4Wheel].u4CurStep;
        u4Offset = u4StepSize;
    }

    /* Wheel Found. Find the slot and remnant time and insert */
    u4RemTime = u4Duration - u4CummTime;
    u4Steps = ((u4RemTime + u4Offset) / u4StepSize);
    u4RemTime = u4RemTime % u4StepSize;

    u4Step = (u4CurStep + u4Steps - 1) % u4MaxSteps;

    pAppTimer->u4RemainingTime = u4RemTime;

    pAppTimer->u2Flags = 0;
    pAppTimer->u2Flags |= TMR_RUNNING;
    pAppTimer->TimerListId = TimerListId;
    pAppTimer->pDLL = &Wheel[u4Wheel].pActiveList[u4Step];
    TMO_DLL_Add (&Wheel[u4Wheel].pActiveList[u4Step],
                 (tTMO_DLL_NODE *) pAppTimer);

    TmrUnLock ();
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "Added %x, %d units \n",
              pAppTimer, u4Duration));
    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrStopTimer                                      */
/*  Description     : Stops a timer, if it is active                    */
/*                  : If it has expired the return value indicates so   */
/*                  : In either case the timer block is removed from    */
/*                  : the timer list.                                   */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The Handle to the timer list.       */
/*                  : pReference  - Timer block.                        */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrStopTimer (tTimerListId TimerListId, tTmrAppTimer * pAppTimer)
{
    UINT4               u4rc;

    TmrLock ();
    u4rc = TmrDeleteNode (TimerListId, pAppTimer);
    TmrUnLock ();
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "Stopped: %x\n",
              pAppTimer));

    return (u4rc);
}

UINT4
TmrDeleteNode (tTimerListId TimerListId, tTmrAppTimer * pAppTimer)
{
    if (!pAppTimer || !TimerListId)
        return (TMR_FAILURE);

    if (TMR_IS_EXPIRED (pAppTimer))
    {
        TMO_DLL_Delete (pAppTimer->pDLL, (tTMO_DLL_NODE *) pAppTimer);
        pAppTimer->u2Flags &= ~TMR_EXPD;
        return (TMR_SUCCESS);
    }

    pAppTimer->u2Flags &= ~TMR_RUNNING;
    TMO_DLL_Delete (pAppTimer->pDLL, (tTMO_DLL_NODE *) pAppTimer);
    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrResizeTimer                                    */
/*  Description     : Resizes a running timer.                          */
/*                  : Trying to resize a timer to a value into the past */
/*                  : will result in failure.                           */
/*  Input(s)        :                                                   */
/*                  : TimerListId - Handle to the timer list.           */
/*                  : pReference  - Timer block.                        */
/*                  : u4Duration  - Resized value of timeout.           */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrResizeTimer (tTimerListId TimerListId,
                tTmrAppTimer * pReference, UINT4 u4Duration)
{
    if (TMR_IS_RUNNING (pReference) || TMR_IS_EXPIRED (pReference))
    {
        TmrStopTimer (TimerListId, pReference);
        return (TmrStartTimer (TimerListId, pReference, u4Duration));
    }
    return (TMR_FAILURE);
}

/************************************************************************/
/*  Function Name   : TmrGetExpiredTimers                               */
/*  Description     : API to be called to retrieve Expired timers.      */
/*  Input(s)        :                                                   */
/*                  : TimerListId -  The timer list from which to get.  */
/*  Output(s)       :                                                   */
/*                  : ppExpiredTimers - Contains ptr. to expired timer. */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrGetExpiredTimers (tTimerListId TimerListId, tTmrAppTimer ** ppExpiredTimers)
{

    tTimerList         *pTimerList = (tTimerList *) TimerListId;

    TmrLock ();
    *ppExpiredTimers = (tTmrAppTimer *) TMO_DLL_Get (&pTimerList->ExpdList);
    if (*ppExpiredTimers)
    {
        (*ppExpiredTimers)->u2Flags &= ~TMR_RUNNING;
    }
    TmrUnLock ();

    return (*ppExpiredTimers ? TMR_SUCCESS : TMR_FAILURE);
}

/************************************************************************/
/*  Function Name   : TmrGetRemainingTime                               */
/*  Description     : API to get the time to expire for a timer.        */
/*  Input(s)        :                                                   */
/*                  : TimerListId - The timer list in which the timer is*/
/*                  : pReference -  The Timer.                          */
/*  Output(s)       :                                                   */
/*                  : pu4RemainingTime - Time to expire                 */
/*  Returns         :                                                   */
/************************************************************************/
UINT4
TmrGetRemainingTime (tTimerListId TimerListId,
                     tTmrAppTimer * pReference, UINT4 *pu4RemainingTime)
{
    tTMO_DLL           *pDLL;
    UINT4               u4Wheel;
    UINT4               u4RemTime;
    UINT4               u4Found;
    tTMO_DLL           *pLo, *pHi, *pCur;

    TimerListId = TimerListId;    /* Unused Param */
    u4RemTime = 0;
    u4Found = 0;

    TmrLock ();
    if (TMR_IS_EXPIRED (pReference))
    {
        *pu4RemainingTime = 0;
        TmrUnLock ();
        return (TMR_SUCCESS);
    }

    pDLL = pReference->pDLL;
    for (u4Wheel = 1; (u4Wheel <= gu4MaxWheels) && !u4Found; ++u4Wheel)
    {
        pLo = Wheel[u4Wheel].pActiveList;
        pHi = pLo + (Wheel[u4Wheel].u4MaxSteps - 1);
        pCur = pLo + Wheel[u4Wheel].u4CurStep;
        if (pDLL >= pLo && pDLL <= pHi)
        {
            if (pDLL >= pCur)
            {
                u4RemTime += (pDLL - pCur) * Wheel[u4Wheel].u4StepSize;
            }
            else
            {
                u4RemTime += (((pDLL - pLo) +
                               (pHi - pCur))) * Wheel[u4Wheel].u4StepSize;
            }
            u4Found = 1;
        }
        else
        {
            u4RemTime += (pHi - pCur) * Wheel[u4Wheel].u4StepSize;;
        }
    }

    TmrUnLock ();
    if (!u4Found)
    {
        return (TMR_FAILURE);
    }
    *pu4RemainingTime = u4RemTime + (pReference->u4RemainingTime);
    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrProcessTick                                    */
/*  Description     : Function called on every time-tick. Updates the   */
/*                  : timer list and intimates the tasks in case of     */
/*                  : timeout.                                          */
/*  Input(s)        :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
void
TmrProcessTick (void)
{
    tTmrAppTimerList   *pTimerList;
    tTMO_DLL           *pDLL;
    tTMO_DLL_NODE      *pDLLNode;
    tTmrAppTimer       *pAppTimer;
    UINT4               u4More;
    UINT4               u4Wheel;
    UINT4               u4CurStep;
    UINT4               u4MaxSteps;
    UINT4               u4StepSize;
    UINT4               u4Step;
    UINT4               u4Steps;
    UINT4               u4RemTime;
    UINT4               u4Count;
    UINT4               u4WheelOffset;

    /* To provide time in seconds, to the applications, a global
     * variable gu4Seconds is incremented each second.
     * The variable gu4StupsCounter is loaded with the value of STUPS
     * and gets decremented each tick. Thus at the end of gu4Stups
     * ticks, one second would have elapsed.
     */

    TmrLock ();
    gu4StupsCounter--;
    if (gu4StupsCounter == 0)
    {
        gu4Seconds++;
        gu4StupsCounter = gu4Stups;
    }

    u4More = 1;
    for (u4Wheel = 1; (u4Wheel <= gu4MaxWheels) && u4More; ++u4Wheel)
    {
        u4More = 0;
        if ((Wheel[u4Wheel].u4CurStep) == Wheel[u4Wheel].u4MaxSteps)
        {
            Wheel[u4Wheel].u4CurStep = 0;
            u4More = 1;
        }
        u4CurStep = Wheel[u4Wheel].u4CurStep;
        pDLL = &(Wheel[u4Wheel].pActiveList[u4CurStep]);

        u4MaxSteps = Wheel[u4Wheel - 1].u4MaxSteps;
        u4StepSize = Wheel[u4Wheel - 1].u4StepSize;
        u4CurStep = Wheel[u4Wheel - 1].u4CurStep;

        u4Count = TMO_DLL_Count (pDLL);
        while (u4Count--)
        {
            pDLLNode = TMO_DLL_Get (pDLL);
            if (pDLLNode == NULL)
            {
                TmrUnLock ();
                return;
            }
            pAppTimer = (tTmrAppTimer *) pDLLNode;

            u4RemTime = pAppTimer->u4RemainingTime;
            if (u4RemTime && (u4Wheel == 1))
            {
                u4RemTime--;
            }

            if (u4RemTime)
            {
                u4Steps = u4RemTime / u4StepSize;
                u4RemTime = u4RemTime % u4StepSize;

                u4WheelOffset = 1;
                /* When the u4Steps becomes zero, the timer node has to be added 
                 * to the previous appropriate wheel whose stepsize is less than
                 * the current u4RemTime */
                while (u4Steps == 0)
                {
                    u4WheelOffset++;
                    u4Steps =
                        u4RemTime / Wheel[u4Wheel - u4WheelOffset].u4StepSize;
                    u4RemTime =
                        u4RemTime % Wheel[u4Wheel - u4WheelOffset].u4StepSize;
                }
                u4Step =
                    (Wheel[u4Wheel - u4WheelOffset].u4CurStep + u4Steps -
                     1) % u4MaxSteps;
                pAppTimer->u4RemainingTime = u4RemTime;
                pAppTimer->pDLL =
                    &Wheel[u4Wheel - u4WheelOffset].pActiveList[u4Step];
                TMO_DLL_Add (&Wheel[u4Wheel - u4WheelOffset].
                             pActiveList[u4Step], pDLLNode);
            }
            else
            {
                pAppTimer->u2Flags &= ~TMR_RUNNING;
                pAppTimer->u2Flags |= TMR_EXPD;
                pTimerList = (tTmrAppTimerList *) (pAppTimer->TimerListId);

                if (pTimerList->u4Status == TMR_USED)
                {
                    pAppTimer->pDLL = &pTimerList->ExpdList;
                    TMO_DLL_Add (&pTimerList->ExpdList, pDLLNode);

                    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
                              "Expired %x\n", pAppTimer));
                    TmrUnLock ();

                    if (pTimerList->CallBackFunction)
                        (pTimerList->
                         CallBackFunction) ((tTimerListId) pTimerList);
                    else
                        OsixEvtSend (pTimerList->TskId, pTimerList->u4Event);

                    TmrLock ();
                }
            }
        }

        ++Wheel[u4Wheel].u4CurStep;
    }

    TmrUnLock ();
}

/************************************************************************/
/*  Function Name   : TmrGetNextExpiredTimer                            */
/*  Description     : API to get the expired timers from a timer list.  */
/*  Input(s)        : TimerListId - The timer list.                     */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
tTmrAppTimer       *
TmrGetNextExpiredTimer (tTimerListId TimerListId)
{
    tTimerList         *pTimerList = (tTimerList *) TimerListId;
    tTmrAppTimer       *pTmr;

    TmrLock ();
    pTmr = (tTmrAppTimer *) TMO_DLL_Get (&pTimerList->ExpdList);
    if (pTmr)
    {
        pTmr->u2Flags &= ~TMR_EXPD;
    }
    TmrUnLock ();
    return pTmr;
}

/************************************************************************/
/*  Function Name   : TmrSetDbg                                         */
/*  Description     : API for changing debug level at run time.         */
/*                  :                                                   */
/*  Input(s)        : u4Value - New value of debug mask.                */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
#if DEBUG_TMR == FSAP_ON
VOID
TmrSetDbg (UINT4 u4Value)
{
    TMR_DBG_FLAG = u4Value;
}
#endif

void
TmrTaskMain (INT1 *pi1Dummy)
{
    UINT4               u4IncreaseTimerSpeed;

    /* Unused Param. */
    pi1Dummy = pi1Dummy;

    /* Initialize the timer speed 
     * 1 indicates timer is running in real time speed
     */
    gu4TimerSpeed = 1;

    for (;;)
    {
        OsixTskDelay (1);
        /* 1 Tick is completed so Process Tick */
        u4IncreaseTimerSpeed = gu4TimerSpeed;
        while (u4IncreaseTimerSpeed != 0)
        {
            TmrProcessTick ();
            u4IncreaseTimerSpeed--;
        }
    }
}

#if (DEBUG_TMR == FSAP_ON)
static void
TmrDbgWalk (void)
{
    UINT4               u4Wheel, u4Step;
    tTMO_DLL           *pDLL;
    tTmrAppTimer       *pNode;

    /* The outer for-loop is done only on the first wheel, where,
     * for some reason, all the known crashes have occurred.
     * Iterating over all wheels (gu4MaxWheels), may alter application behaviour,
     * as it is likely to consume a lot of time. Hence this optimisation.
     * Should a wheel other than the first wheel be corrupted, then
     * change the loop below to iterate over all wheels.
     */
    for (u4Wheel = 1; (u4Wheel <= 1 /* gu4MaxWheels */ ); ++u4Wheel)
    {
        for (u4Step = 0; u4Step < Wheel[u4Wheel].u4MaxSteps; ++u4Step)
        {
            pDLL = &(Wheel[u4Wheel].pActiveList[u4Step]);
            TMO_DLL_Scan (pDLL, pNode, tTmrAppTimer *);
        }
    }
}
#endif

/*****************************************************************************/
/* Function     : TmrStart                                                   */
/*                                                                           */
/* Description  : Sets the timer id in the pTimer struct. Calls the          */
/*                TmrStartTimer to start the timer.This function is provided */
/*                to make all the application use the timer library in the   */
/*                same way.                                                  */
/*                                                                           */
/* Input        : TimerListId  : The timer list id                           */
/*                pTimer       : Pointer to timer block                      */
/*                u1TimerId    : The id of the timer to be started           */
/*                u4Sec        : The seconds value of the timer.             */
/*                u4MilliSec   : The milliseconds value of the timer.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrStart (tTimerListId TimerListId, tTmrBlk * pTimer, UINT1 u1TimerId,
          UINT4 u4Sec, UINT4 u4MilliSec)
{
    UINT4               u4Duration;
    pTimer->u1TimerId = u1TimerId;

    u4Duration = (u4Sec * gu4Stups) + ((gu4Stups * u4MilliSec) / 1000);
    if (TmrStartTimer (TimerListId, &(pTimer->TimerNode),
                       u4Duration) != TMR_SUCCESS)
    {
        TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
                  "TmrStart Failed for timer 0x%x\n", pTimer));
        return (TMR_FAILURE);
    }
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Started timer 0x%x, %ld units \n", pTimer, u4Duration));
    return TMR_SUCCESS;
}

/*****************************************************************************/
/* Function     : TmrRestart                                                 */
/*                                                                           */
/* Description  : Restarts the timer with the specified duration             */
/*                                                                           */
/* Input        : TimerListId :  The timer list id                           */
/*                pTimer       : pointer to timer block                      */
/*                u1TimerId    : The id of the timer to be started           */
/*                u4Sec        : The seconds value of the timer.             */
/*                u4MilliSec   : The milliseconds value of the timer.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrRestart (tTimerListId TimerListId, tTmrBlk * pTimer,
            UINT1 u1TimerId, UINT4 u4Sec, UINT4 u4MilliSec)
{
    UINT4               u4Duration;

    u4Duration = (u4Sec * gu4Stups) + ((gu4Stups * u4MilliSec) / 1000);

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Restarting timer 0x%x, TmrId %s NumTicks %ld\n",
              pTimer, u1TimerId, u4Duration));

    if (TmrStopTimer (TimerListId, &(pTimer->TimerNode)) != TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    if (TmrStart (TimerListId, pTimer, u1TimerId, u4Sec, u4MilliSec) !=
        TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME, "TMR Restarted\n"));
    return (TMR_SUCCESS);
}

/*****************************************************************************/
/* Function     : TmrStop                                                    */
/*                                                                           */
/* Description  : Deletes the timer from the timer list.                     */
/*                                                                           */
/* Input        : pTimer       : pointer to timer block                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
UINT4
TmrStop (tTimerListId TimerListId, tTmrBlk * pTimer)
{
    TMR_DBG ((TMR_DBG_FLAG, TMR_DBG_MINOR, TMR_MODNAME,
              "Stopping timer 0x%x, TmrId %s\n", pTimer, pTimer->u1TimerId));

    if (TmrStopTimer (TimerListId, &(pTimer->TimerNode)) != TMR_SUCCESS)
    {
        return (TMR_FAILURE);
    }

    return (TMR_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrSetSysTime                                     */
/*  Description     : API for changing System Time at run time.         */
/*                  :                                                   */
/*  Input(s)        : tm - Time Structure to be set to.                 */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
VOID
TmrSetSysTime (tUtlTm * tm)
{
    struct tm           tBrokenTime;
    struct timespec     tp;

    tBrokenTime.tm_hour = tm->tm_hour;

    tBrokenTime.tm_min = tm->tm_min;

    tBrokenTime.tm_sec = tm->tm_sec;

    tBrokenTime.tm_mday = tm->tm_mday;

    tBrokenTime.tm_mon = tm->tm_mon;

    tBrokenTime.tm_year = tm->tm_year;
    tBrokenTime.tm_year -= 1900;

    tp.tv_sec = mktime (&tBrokenTime);

    tp.tv_nsec = 0;

    /*Set the time in the system */
    clock_settime (CLOCK_REALTIME, &tp);

    /* Print the Date set by the user */
    clock_gettime (CLOCK_REALTIME, &tp);
    PRINTF ("%s\r\n", ctime (&(tp.tv_sec)));
}

/************************************************************************/
/*  Function Name   : TmrGetPreciseSysTime                              */
/*  Description     : Returns the seconds and Nanoseconds of the time   */
/*                  : from the Epoch (00:00:00 UTC, January 1, 1970)    */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysPreciseTime - The System time.                */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
TmrGetPreciseSysTime (tUtlSysPreciseTime * pSysPreciseTime)
{
    struct timespec     tp;

    /* Get the time in the system */
    clock_gettime (CLOCK_REALTIME, &tp);

    pSysPreciseTime->u4Sec = tp.tv_sec;
    pSysPreciseTime->u4NanoSec = tp.tv_nsec;

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : TmrSetPreciseSysTime                              */
/*  Description     : Set the seconds and Nanoseconds of the time       */
/*                  : from the Epoch (00:00:00 UTC, January 1, 1970)    */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysPreciseTime - The System time.                */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
TmrSetPreciseSysTime (tUtlSysPreciseTime * pSysPreciseTime)
{
    struct timespec     tp;

    tp.tv_sec = pSysPreciseTime->u4Sec;
    tp.tv_nsec = pSysPreciseTime->u4NanoSec;

    /*Set the time in the system */
    clock_settime (CLOCK_REALTIME, &tp);

    return (OSIX_SUCCESS);
}
