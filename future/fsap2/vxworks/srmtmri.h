/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved     */
/*                                                          */
/*  FILE NAME             :   srmtmri.h                     */
/*  PRINCIPAL AUTHOR      :                                 */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :                                 */
/*  LANGUAGE              :                                 */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  DESCRIPTION           :                                 */
/************************************************************/
/*                                                          */
/*  CHANGE RECORD:                                          */
/*  Version      Author  Date       Description of change   */
/*    3.0       Ananth  03-AOR-2000 Added Rev. History      */
/************************************************************/

#define TMR_FREE    0
#define TMR_USED    1

/************************************************************************
*                                                                       *
*                      Defines and Typedefs                             *
*                                                                       *
*************************************************************************/

/********************************************
*** Application Timer List Data Structure ***
*********************************************/
typedef struct TMO_APP_TIMER_LIST{
    tTMO_DLL    Link;
    INT4        i4RemainingTime;           /*** Time till next expiry ***/
    tOsixTaskId TskId;
    UINT4       u4Event;
    UINT4       u4Status;
    void        (*CallBackFunction)(tTimerListId);
    tOsixSemId  SemId;
   /* 
    * The List of timers which have expired.
    * To be read by the application.
    */
   tTMO_DLL       ExpdList;
}tTmrAppTimerList;

typedef tTmrAppTimerList tTimerList;
/************************************************************************
*                                                                       *
*                          Macro                                        *
*                                                                       *
*************************************************************************/
#define TMR_MUTEX_NAME (const UINT1 *)"TMMU"

#if (DEBUG_TMR == FSAP_ON)
/* To ensure the timer library is sane (i.e., the DLLS (gpActiveList)
 * are not corrupted by bad memory operations we perform some checkpointing.
 * At the end of (almost) every timer API a copy of the ActiveList is made.
 * When the next call is made to the timer library, the copy is compared
 * against the 'master'. Should there be a diff (memcmp below fails), we
 * crash the program. This ensures that gpActiveList is sane. It is possible
 * that the DLL data structure is sane, but the chain is 'bad' somewhere.
 * To detect this we call TmrDbgWalk(). Again, it is possible that the
 * chain is perfect but the TimerListId field in a timerblock has been
 * corrupted. This additional enhancement can be done at the discretion of
 * the user.
 *
 * note: These functions are called on every timer tick and are likely to
 * consume much time. So use it wisely.
 */
#define TMR_DBG_CPY()   MEMCPY(gpActiveListCopy, gpActiveList, gu4ALSize)
#define TMR_DBG_CHK()\
 if (MEMCMP(gpActiveListCopy, gpActiveList, gu4ALSize) != 0) {\
     char *p = 0;\
     UtlTrcPrint ("Timer Library Corrupted since last visitation.\n");\
     *p = 0;\
 }\
 TmrDbgWalk();
#else
#define TMR_DBG_CPY()
#define TMR_DBG_CHK()
#endif

/* We couple the above mentioned sanity checks with the timer mutex operations.
 * as they always go hand in hand. */
#if (DEBUG_TMR == FSAP_ON)
  #define TmrLock()  \
                     OsixSemTake(TmrMutex);\
                     TMR_DBG_CHK()
  #define TmrUnLock()\
                     TMR_DBG_CPY();\
                     TmrDbgWalk();\
                     OsixSemGive(TmrMutex);
#else
  #define TmrLock()   (OsixSemTake(TmrMutex))
  #define TmrUnLock() (OsixSemGive(TmrMutex))
#endif

#define TMR_DBG_FLAG gu4TmrDbg
#define TMR_MODNAME  "TMR"

#if DEBUG_TMR == FSAP_ON
#define TMR_DBG(x) UtlTrcLog x
#else
#define TMR_DBG(x)

#endif
#define TMR_PRNT(x) UtlTrcPrint(x)

/*
 * ------------------------------------------
 * Additions to workaround SIGVTALRM problem.
 * ------------------------------------------
 */


#include <unistd.h>

typedef struct timeval tTimeVal;

#define FSAP_TIME_NOW         (tTimeVal *)(0)
#define FSAP_TIME_EQUAL (t1,t2) \
        (((t1).tv_sec == (t2).tv_sec) && ((t1).tv_usec == (t2).tv_usec))

#define __gettimeofday(t) gettimeofday(t, NULL)

#define FSAP_TIME_SET(t1,t2)               \
    do {                                   \
        if ((t2) == FSAP_TIME_NOW)         \
            __gettimeofday((t1));          \
        else {                             \
            (t1)->tv_sec  = (t2)->tv_sec;  \
            (t1)->tv_usec = (t2)->tv_usec; \
        }                                  \
    } while (0)

#define FSAP_TIME_ADD(t1,t2)        \
    (t1)->tv_sec  += (t2)->tv_sec;  \
    (t1)->tv_usec += (t2)->tv_usec; \
    if ((t1)->tv_usec > 1000000) {  \
        (t1)->tv_sec  += 1;         \
        (t1)->tv_usec -= 1000000;   \
    }
#define  FSAP_TIME_SUB(t1,t2)       \
    (t1)->tv_sec  -= (t2)->tv_sec;  \
    (t1)->tv_usec -= (t2)->tv_usec; \
    if ((t1)->tv_usec < 0) {        \
        (t1)->tv_sec  -= 1;         \
        (t1)->tv_usec += 1000000;   \
    }

#define FSAP_SCHEDULER_LOAD(now)                             \
    if (FSAP_TIME_CMP((now), &gLoadTickNext) >= 0) {         \
        tTimeVal ttmp;                                       \
        FSAP_TIME_SET (&ttmp, (now));                        \
        do {                                                 \
            FSAP_TIME_SUB (&ttmp, &gLoadTickGap);            \
            TmrProcessTick();                                \
        } while (FSAP_TIME_CMP(&ttmp, &gLoadTickNext) >= 0); \
        FSAP_TIME_SET (&gLoadTickNext, (now));               \
        FSAP_TIME_ADD (&gLoadTickNext, &gLoadTickGap);       \
    }

#define FSAP_TIME_CMP(t1, t2)        \
        ((((tTimeVal *)t1)->tv_sec - ((tTimeVal *)t2)->tv_sec) ? \
         (((tTimeVal *)t1)->tv_sec - ((tTimeVal *)t2)->tv_sec) : \
         (((tTimeVal *)t1)->tv_usec - ((tTimeVal *)t2)->tv_usec))
