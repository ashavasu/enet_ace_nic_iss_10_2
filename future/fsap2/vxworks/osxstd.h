/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osxstd.h,v 1.20 2015/04/28 12:21:07 siva Exp $
 *
 * Description: target OS header files, compile-time switch
 *               header file
 *
 *******************************************************************/

#ifndef _OSIX_STD_H
#define _OSIX_STD_H

#include "fsapcfg.h"
#include <semLib.h>
/************************************************************************
*                                                                       *
*                         Basic Types                                   *
*                                                                       *
*************************************************************************/

typedef char            BOOLEAN;
typedef char            BOOL1;
typedef char            CHR1;

/* Note:
 * ----
 * This is FSAP's definition of VOID.
 * VxWorks also has one - a typedef
 * For proper compilation, include <vxworks.h> needs
 * to precede this file.
 */
#undef VOID
#define VOID            void

/******** VxWorks Specific Code ************/
typedef signed char            INT1;
/******** VxWorks Specific Code ************/

typedef unsigned char   UINT1;


/******** VxWorks Specific Code ************/
typedef signed short           INT2;
/******** VxWorks Specific Code ************/

typedef unsigned short  UINT2;


/******** VxWorks Specific Code ************/
typedef signed long            INT4;
/******** VxWorks Specific Code ************/


typedef unsigned long   UINT4;

typedef float           FLT4;
typedef double          DBL8;
typedef unsigned long   FS_ULONG;
typedef long long unsigned int AR_UINT8;


/* For driver writers, and in case you are using longjmp etc. */
typedef volatile char            VINT1;
typedef volatile unsigned char   VUINT1;
typedef volatile short           VINT2;
typedef volatile unsigned short  VUINT2;
typedef volatile int             VINT4;
typedef volatile unsigned long   VUINT4;

#define FSAP_OFFSETOF(StructType,Member)  (UINT4)(&(((StructType*)0)->Member))

#ifdef __STDC__
#define ARG_LIST(x)     x
#else
#define ARG_LIST(x)  ()
#endif /* __STDC__ */

#ifndef NULL
#define NULL    (0)
#endif

#if !defined(PRIVATE) /* || (PRIVATE != static) */
#define PRIVATE static
#endif

#if !defined(VOLATILE) /* || (VOLATILE != volatile) */
#define VOLATILE volatile
#endif

#if !defined(PUBLIC) /* || (PUBLIC != extern) */
#define PUBLIC  extern
#endif

#if !defined(FALSE)  || (FALSE != 0)
#define FALSE  (0)
#endif

#if !defined(TRUE) || (TRUE != 1)
#define TRUE   (1)
#endif

#ifndef EXPORT
#define EXPORT
#endif

#if OSIX_HOST == OSIX_LITTLE_ENDIAN
#define OSIX_NTOHL(x) (UINT4)(((x & 0xFF000000)>>24) | \
                              ((x & 0x00FF0000)>>8)  | \
                              ((x & 0x0000FF00)<<8 ) | \
                              ((x & 0x000000FF)<<24)   \
                             )
#define OSIX_NTOHS(x) (UINT2)(((x & 0xFF00)>>8) | ((x & 0x00FF)<<8))
#define OSIX_HTONL(x) (UINT4)(OSIX_NTOHL(x))
#define OSIX_HTONS(x) (UINT2)(OSIX_NTOHS(x))
#define OSIX_NTOHF(x) (FLT4)(FsNtohf (x))
#define OSIX_HTONF(x) (FLT4)(FsNtohf(x))
#else
#define OSIX_NTOHL(x) (UINT4)(x)
#define OSIX_NTOHS(x) (UINT2)(x)
#define OSIX_HTONL(x) (UINT4)(x)
#define OSIX_HTONS(x) (UINT2)(x)
#define OSIX_NTOHF(x) (FLT4)(x)
#define OSIX_HTONF(x) (FLT4)(x)
#endif

#endif  /* !OSIX_STD_H */
