/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlsll.c,v 1.16 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL SLL module.
 *
 */

#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "srmmem.h"
#include "utlsll.h"

/***************************************************************/
/*  Function Name   : TMO_SLL_Insert_In_Middle                 */
/*  Description     : Inserts the Node 'pMid' in between       */
/*                    'pPrev' and 'pNext' and updates the node */
/*                    count of the 'pList'                     */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*                    pPrev - Pointer to previous Node         */
/*                    pMid  - Pointer to Node which needs to   */
/*                              be added                       */
/*                    pNext -  Pointer to the Next Node        */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Insert_In_Middle (tTMO_SLL * pList, tTMO_SLL_NODE * pPrev,
                          tTMO_SLL_NODE * pMid, tTMO_SLL_NODE * pNext)
#else
VOID
TMO_SLL_Insert_In_Middle (pList, pPrev, pMid, pNext)
     tTMO_SLL           *pList;
     tTMO_SLL_NODE      *pPrev;
     tTMO_SLL_NODE      *pMid;
     tTMO_SLL_NODE      *pNext;
#endif /* __STDC__ */
{
    pList->u4_Count++;
    pMid->pNext = pNext;
    pPrev->pNext = pMid;
    if (pNext == &pList->Head)
        pList->Tail = pMid;
}

/***************************************************************/
/*  Function Name   :  TMO_SLL_Replace                         */
/*  Description     :  Replaces the old Node 'pOld' with the   */
/*                     new node 'pNew' and also initialise     */
/*                     the old node                            */
/*  Input(s)        :  pList - Pointer to Singly Linked List   */
/*                     pOld  - Pointer to Old Node             */
/*                     pNew -  Pointer to New Node             */
/*  Output(s)       :  None                                    */
/*  Returns         :  None                                    */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Replace (tTMO_SLL * pList, tTMO_SLL_NODE * pOld, tTMO_SLL_NODE * pNew)
#else
VOID
TMO_SLL_Replace (pList, pOld, pNew)
     tTMO_SLL           *pList;
     tTMO_SLL_NODE      *pOld;
     tTMO_SLL_NODE      *pNew;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pPrev;
    tTMO_SLL_NODE      *pNext;

    if ((pPrev = TMO_SLL_Previous (pList, pOld)) == NULL)
        pPrev = &pList->Head;
    pNext = pOld->pNext;
    pNew->pNext = pNext;
    pPrev->pNext = pNew;
    if (pNext == &pList->Head)
        pList->Tail = pNew;
    TMO_SLL_Init_Node (pOld);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Delete_In_Middle                 */
/*  Description     : Removes the 'pNode' from 'pList' and     */
/*                    updates the pointers appropriately using */
/*                    'pPrev' and 'pNext'. After removing from */
/*                    the list, 'pNode'  is initialised and    */
/*                    count of nodes is updated in 'pList'.    */
/*  Input(s)        : pList - Pointer to Singly Linked List    */
/*                    pPrev - Pointer to Previous Node         */
/*                    pNode - Pointer to Node which is to be   */
/*                            removed                          */
/*                    pNext - Pointer to Next Node             */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Delete_In_Middle (tTMO_SLL * pList, tTMO_SLL_NODE * pPrev,
                          tTMO_SLL_NODE * pNode, tTMO_SLL_NODE * pNext)
#else
VOID
TMO_SLL_Delete_In_Middle (pList, pPrev, pNode, pNext)
     tTMO_SLL           *pList;
     tTMO_SLL_NODE      *pPrev;
     tTMO_SLL_NODE      *pNode;
     tTMO_SLL_NODE      *pNext;
#endif /* __STDC__ */
{
 /***** Deleting The Last Node *****/
    if (pNext == &pList->Head)
        pList->Tail = pPrev;
    pPrev->pNext = pNext;
    TMO_SLL_Init_Node (pNode);
    pList->u4_Count--;
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Concat                           */
/*  Description     : Concatenates 'pAddList' at the end of    */
/*                    'pDstList'  and updates the node count   */
/*                    parameter in 'pDstList' appropriately    */
/*  Input(s)        : pDstlist - Pointer to Singly Linked List */
/*                    pAddList - Pointer to Singly Linked List */
/*                    which needs to be appended to 'pDstList' */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Concat (tTMO_SLL * pDstList, tTMO_SLL * pAddList)
#else
VOID
TMO_SLL_Concat (pDstList, pAddList)
     tTMO_SLL           *pDstList;
     tTMO_SLL           *pAddList;
#endif /* __STDC__ */
{
    if (TMO_SLL_Count (pAddList) == 0)
        return;

 /**** make the last node of pDstList to point to first node os addlist */
    pDstList->Tail->pNext = pAddList->Head.pNext;

 /**** make the dstlist Head prev to point to the last node in addlist */
    pDstList->Tail = pAddList->Tail;

 /***** make the addlist last node to point to dstlist */
    pAddList->Tail->pNext = &pDstList->Head;
    pDstList->u4_Count += pAddList->u4_Count;
    TMO_SLL_Init (pAddList);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Delete                           */
/*  Description     : Deletes the 'pNode' from the singly      */
/*                    linked list 'pList'                      */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*                    pNode - Pointer to Node which needs to be*/
/*                    deleted from the list                    */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Delete (tTMO_SLL * pList, tTMO_SLL_NODE * pNode)
#else
VOID
TMO_SLL_Delete (pList, pNode)
     tTMO_SLL           *pList;
     tTMO_SLL_NODE      *pNode;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pPrev;

 /***** Deleting The First Node *****/

    if (pNode == NULL)
        return;
    if (!(TMO_SLL_Is_Node_In_List (pNode)))
        return;
    if ((pPrev = TMO_SLL_Previous (pList, pNode)) == NULL)
        pPrev = &pList->Head;
    TMO_SLL_Delete_In_Middle (pList, pPrev, pNode, pNode->pNext);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Extract                          */
/*  Description     : From 'pSrcList' extracts the nodes       */
/*                    starting from 'pStartNode' to 'pEndNode' */
/*                    and the destination list 'pDstList'  is  */
/*                    formed.  Node count in lists 'pDstList'  */
/*                    and 'pSrcList' are updated properly      */
/*  Input(s)        : pSrcList - Pointer to Source Singly      */
/*                               Linked List                   */
/*                    pStartNode - Pointer to Starting Node    */
/*                                 from which the list needs   */
/*                                 to be extracted             */
/*                    pEndNode   - Pointer to the End Node upto*/
/*                        which the list needs to be extracted */
/*  Output(s)       : pDstList - Extracted list is placed in   */
/*                               this param                    */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Extract (tTMO_SLL * pSrcList, tTMO_SLL_NODE * pStartNode,
                 tTMO_SLL_NODE * pEndNode, tTMO_SLL * pDstList)
#else
VOID
TMO_SLL_Extract (pSrcList, pStartNode, pEndNode, pDstList)
     tTMO_SLL           *pSrcList;
     tTMO_SLL_NODE      *pStartNode;
     tTMO_SLL_NODE      *pEndNode;
     tTMO_SLL           *pDstList;
#endif /* __STDC__ */
{
    UINT4               i;
    tTMO_SLL_NODE      *pTempNode;
    tTMO_SLL_NODE      *pPrev;

 /***** Deleting The First Node *****/
    if (!(pPrev = TMO_SLL_Previous (pSrcList, pStartNode)))
        pPrev = &pSrcList->Head;
    if (pEndNode->pNext == &pSrcList->Head)
        pSrcList->Tail = pPrev;
    pPrev->pNext = pEndNode->pNext;
    pDstList->Head.pNext = pStartNode;
    pEndNode->pNext = &pDstList->Head;
    pDstList->Tail = pEndNode;
    for (pTempNode = pDstList->Head.pNext, i = 0; pTempNode != &pDstList->Head;
         pTempNode = pTempNode->pNext, i++);
    pSrcList->u4_Count -= i;
    pDstList->u4_Count = i;
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Get                              */
/*  Description     : Removes the first node from the list     */
/*                    'pList' and returns the same.            */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the first Node in the list,   */
/*                    if the list is not empty.                */
/*                    NULL, otherwise                          */
/***************************************************************/
#ifdef __STDC__
tTMO_SLL_NODE      *
TMO_SLL_Get (tTMO_SLL * pList)
#else
tTMO_SLL_NODE      *
TMO_SLL_Get (pList)
     tTMO_SLL           *pList;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pRetNode;

    if (TMO_SLL_Count (pList) == 0)
        return (NULL);
    pRetNode = pList->Head.pNext;
    TMO_SLL_Delete_In_Middle (pList, &pList->Head, pList->Head.pNext,
                              pList->Head.pNext->pNext);
    return (pRetNode);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Insert                           */
/*  Description     : Inserts the node 'pNode' as a next node  */
/*                    to the 'pPrev' node and updates the node */
/*                    count of the 'pList'.                    */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*                    pPrev - Pointer to the node next to which*/
/*                            the new node needs to be inserted*/
/*                    pNode - Pointer of the node which needs  */
/*                            to be inserted                   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Insert (tTMO_SLL * pList, tTMO_SLL_NODE * pPrev, tTMO_SLL_NODE * pNode)
#else
VOID
TMO_SLL_Insert (pList, pPrev, pNode)
     tTMO_SLL           *pList;
     tTMO_SLL_NODE      *pPrev;
     tTMO_SLL_NODE      *pNode;
#endif /* __STDC__ */
{
    if (pPrev == NULL)
        pPrev = &pList->Head;
    TMO_SLL_Insert_In_Middle (pList, pPrev, pNode, pPrev->pNext);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Nth                              */
/*  Description     : Returns the 'u4NodeNum'th node from      */
/*                    the 'pList'.                             */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*                    u4NodeNum - Position of node of interest */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the Node request from the list*/
/*                    NULL, otherwise                          */
/***************************************************************/
#ifdef __STDC__
tTMO_SLL_NODE      *
TMO_SLL_Nth (tTMO_SLL * pList, UINT4 u4_NodeNum)
#else
tTMO_SLL_NODE      *
TMO_SLL_Nth (pList, u4_NodeNum)
     tTMO_SLL           *pList;
     UINT4               u4_NodeNum;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pNthNode;

    if ((u4_NodeNum == 0) || (pList->u4_Count < u4_NodeNum))
        return (NULL);
    for (pNthNode = pList->Head.pNext; --u4_NodeNum; pNthNode = pNthNode->pNext)
    {
        ;
    }
    return (pNthNode);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Previous                         */
/*  Description     : Returns the next node of  'pNode'  from  */
/*                    the 'pList'                              */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*                    pNode - Pointer of the node whose        */
/*                            previous node is requested       */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the previous node if valid,   */
/*                    NULL, otherwise                          */
/***************************************************************/
#ifdef __STDC__
tTMO_SLL_NODE      *
TMO_SLL_Previous (tTMO_SLL * pList, tTMO_SLL_NODE * pNode)
#else
tTMO_SLL_NODE      *
TMO_SLL_Previous (pList, pNode)
     tTMO_SLL           *pList;
     tTMO_SLL_NODE      *pNode;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pPrev;
    UINT4               u4NumNodes;
    UINT4               u4Count = 0;

    if (pNode == NULL)
        return (TMO_SLL_Last (pList));

    u4NumNodes = TMO_SLL_Count (pList);
    for (pPrev = &pList->Head; u4Count++ < u4NumNodes && pNode != pPrev->pNext;
         pPrev = pPrev->pNext);
    return (((pPrev == &pList->Head) || (u4Count > u4NumNodes)) ? NULL : pPrev);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Find                             */
/*  Description     : Finds the specified node 'pNode' from    */
/*                    the list 'pList'                         */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*                    pNode - Pointer of the node which needs  */
/*                            to be found in list 'pList'.     */
/*  Output(s)       : None                                     */
/*  Returns         : Position of the node in the list if a    */
/*                    matching one is found,                   */
/*                    0 otherwise                              */
/***************************************************************/
#ifdef __STDC__
UINT4
TMO_SLL_Find (tTMO_SLL * pList, tTMO_SLL_NODE * pNode)
#else
UINT4
TMO_SLL_Find (pList, pNode)
     tTMO_SLL           *pList;
     tTMO_SLL_NODE      *pNode;
#endif /* __STDC__ */
{
    UINT4               i;
    tTMO_SLL_NODE      *pSearchNode, *pHead;

    for (pHead = &pList->Head, i = 1, pSearchNode = pList->Head.pNext;
         pSearchNode != pHead; pSearchNode = pSearchNode->pNext, i++)
    {
        if (pSearchNode == pNode)
            break;
    }
    return ((pSearchNode == pNode) ? i : 0);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_FreeNodes                        */
/*  Description     : Frees the nodes present in the 'pList'   */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_FreeNodes (tTMO_SLL * pList, tMemPoolId MemPoolId)
#else
VOID
TMO_SLL_FreeNodes (pList, MemPoolId)
     tTMO_SLL           *pList;
     tMemPoolId          MemPoolId;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pTempNode, *pNode, *pHead;

    for (pHead = &pList->Head, pNode = pList->Head.pNext; pNode != pHead;
         pNode = pTempNode)
    {
        pTempNode = pNode->pNext;
        if (MemPoolId == 0)
        {
            TMO_SLL_FREE (pNode);
        }
        else
        {
            MemReleaseMemBlock (MemPoolId, (UINT1 *) pNode);
        }
    }
    TMO_SLL_Init (pList);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Clear                            */
/*  Description     : Removes and initialises all nodes from   */
/*                    the list and the memory is not freed     */
/*  Input(s)        : pList - Pointer to the Singly Linked List*/
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_SLL_Clear (tTMO_SLL * pList)
#else
VOID
TMO_SLL_Clear (pList)
     tTMO_SLL           *pList;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pTempNode, *pNode, *pHead;

    for (pHead = &pList->Head, pNode = pList->Head.pNext; pNode != pHead;
         pNode = pTempNode)
    {
        pTempNode = pNode->pNext;
        TMO_SLL_Init_Node (pNode);
    }
    TMO_SLL_Init (pList);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Get_N_Node                       */
/*  Description     : Deletes first 'u4_NumOfNodes' from the   */
/*                    list and formed into a list              */
/*  Input(s)        : pList - Pointer to Singly linked list    */
/*                    u4_NumOfNodes - Count of nodes to be     */
/*                          extracted                          */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the first element to the List */
/*                    of first `u4_NumOfNodes` on success,     */
/*                    NULL on failure                          */
/***************************************************************/
#ifdef __STDC__
tTMO_SLL_NODE      *
TMO_SLL_Get_N_Node (tTMO_SLL * pList, UINT4 u4_NumOfNodes)
#else
tTMO_SLL_NODE      *
TMO_SLL_Get_N_Node (pList, u4_NumOfNodes)
     tTMO_SLL           *pList;
     UINT4               u4_NumOfNodes;
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pHead, *pPrev;

    if (TMO_SLL_Count (pList) < u4_NumOfNodes)
        return (NULL);
    pHead = pPrev = TMO_SLL_Get (pList);
    if (pPrev != NULL)
    {
        while (--u4_NumOfNodes)
        {
            pPrev->pNext = TMO_SLL_Get (pList);
            pPrev = pPrev->pNext;
        }
    }
    return (pHead);
}

/***************************************************************/
/*  Function Name   : TMO_SLL_Delete_Till_Node                 */
/*  Description     : Deletes the nodes starting from          */
/*                    'pStartNode' till the 'pEndNode'. If     */
/*                    'pStartNode' is NULL or not a valid list */
/*                    node then none of the nodes will be      */
/*                    deleted. If 'pEndNode' is NULL then only */
/*                    'pStartNode' will be deleted             */
/*  Input(s)        : pSrcList - Pointer to Singly Linked List */
/*                    pStartNode - Pointer to the Start of the */
/*                           Node starting from which the nodes*/
/*                           needs to be deleted               */
/*                    pEndNode - Pointer to the end node till  */
/*                          which all nodes needs to be deleted*/
/*                          from the specified 'pStartNode'    */
/*                    pDelFunc - Callback delete function      */
/*  Output(s)       : None                                     */
/*  Returns         : Number of nodes deleted from the List,   */
/*                    0 otherwise                              */
/***************************************************************/
#ifdef __STDC__
UINT4
TMO_SLL_Delete_Till_Node (tTMO_SLL * pSrcList, tTMO_SLL_NODE * pStartNode,
                          tTMO_SLL_NODE * pEndNode,
                          VOID (*pDelFunc) (tTMO_SLL_NODE *))
#else
UINT4
TMO_SLL_Delete_Till_Node (pSrcList, pStartNode, pEndNode, pDelFunc)
     tTMO_SLL           *pSrcList;
     tTMO_SLL_NODE      *pStartNode;
     tTMO_SLL_NODE      *pEndNode;
     VOID                (*pDelFunc) (tTMO_SLL_NODE *);
#endif /* __STDC__ */
{
    tTMO_SLL_NODE      *pNext;
    tTMO_SLL_NODE      *pCurr;
    UINT4               u4DelCount = 0;

    if (pStartNode == NULL)
    {
        return 0;
    }
    if (!(TMO_SLL_Is_Node_In_List (pStartNode)))
        return 0;

    if (pEndNode == NULL)
    {
        TMO_SLL_Delete (pSrcList, pStartNode);
        pDelFunc (pStartNode);
        u4DelCount = 1;
    }
    else
    {
        /*pNext = pStartNode ; */
        for (pCurr = pStartNode, pNext = TMO_SLL_Next (pSrcList, pStartNode);
             pCurr != pEndNode;
             pCurr = pNext, pNext = TMO_SLL_Next (pSrcList, pNext))
        {

            if (pCurr == NULL)
            {
                return u4DelCount;
            }

            TMO_SLL_Delete (pSrcList, pCurr);
            pDelFunc (pCurr);
            u4DelCount++;
        }
        if (pCurr != NULL)
        {
            u4DelCount++;
            TMO_SLL_Delete (pSrcList, pCurr);
            pDelFunc (pCurr);
        }
    }

    return u4DelCount;
}
