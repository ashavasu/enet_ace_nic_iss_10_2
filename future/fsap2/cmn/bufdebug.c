
/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bufdebug.c,v 1.16 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Contains debug routines support for buffer library.
 *
 */

#include "utltrc.h"
#include "utltrci.h"
#include "bufcom.h"
#include "bufdebug.h"

/* Private functions */
PRIVATE VOID UtlDumpChainHdr ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf));
PRIVATE VOID UtlDumpDataDesc ARG_LIST ((tCRU_BUF_DATA_DESC * pDataDesc));
PRIVATE VOID UtlDumpDataBlk ARG_LIST ((tCRU_BUF_DATA_DESC *, UINT4));

/***************************************************************/
/*  Function Name   : UtlDmpMsg                                */
/*  Description     : Dumps the specified packet after checking*/
/*                    the type and direction with the passed   */
/*                    values. Invokes UtlTrcPrint for the      */
/*                    dumping.                                 */
/*  Input(s)        : u4CurType - The type of the packets for  */
/*                    which dumping is enabled.                */
/*                    u4CurDirn - The direction of packets for */
/*                    which dumping is enabled.                */
/*                    pBuf - The buffer containing the packet  */
/*                    or control message to be dumped.         */
/*                    u4Type - The type of this current packet */
/*                    or message.                              */
/*                    u4Dirn - The direction of reception of   */
/*                    this current packet.                     */
/*                    u4Len - The number of bytes which are    */
/*                    requested to be dumped; the actual bytes */
/*                    dumped will be the minimum of this value */
/*                    and the number of bytes present in the   */
/*                    packet.                                  */
/*                    u4HdrReqd - A Boolean value which        */
/*                    specifies whether the buffer header is   */
/*                    also to be dumped or not.                */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
UtlDmpMsg (UINT4 u4CurType, UINT4 u4CurDirn, tCRU_BUF_CHAIN_HEADER * pBuf,
           UINT4 u4Type, UINT4 u4Dirn, UINT4 u4Len, UINT4 u4HdrReqd)
#else
VOID
UtlDmpMsg (u4CurType, u4CurDirn, pBuf, u4Type, u4Dirn, u4Len, u4HdrReqd)
     UINT4               u4CurType;
     UINT4               u4CurDirn;
     tCRU_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4Type;
     UINT4               u4Dirn;
     UINT4               u4Len;
     UINT4               u4HdrReqd;
#endif /* __STDC__ */
{
    UINT4               u4MsgLen;
    UINT4               u4NumDataBlks, u4DataBlkNum;
    char                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    tCRU_BUF_DATA_DESC *pDataDesc;
    UINT4               u4NumBytes;

    if (!(u4CurType & u4Type))
    {
        /* Dumping messages of this "type" not enabled */
        return;
    }
    if (!(u4CurDirn & u4Dirn))
    {
        /* Dumping messages in this direction not enabled */
        return;
    }

    if (pBuf == NULL)
    {
        UtlTrcPrint ("Invalid Buffer pointer to Dump\n");
        return;
    }

    if (u4HdrReqd == TRUE)
    {
        UtlTrcPrint ("\nCHAIN DESCRIPTOR\n");
        UtlDumpChainHdr (pBuf);
    }

    /* Get the Valid byte count of the message */
    u4MsgLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    if (u4MsgLen == 0)
    {
        UtlTrcPrint ("No Valid Bytes in Buffer\n");
        return;
    }

    /* Take the minimum of u4Len and u4MsgLen as 
     * the no. of bytes to be dumped */
    if (u4MsgLen > u4Len)
    {
        u4MsgLen = u4Len;
    }

    /* Find the number of valid data blocks in the message */
    u4NumDataBlks = 0;
    pDataDesc = pBuf->pFirstValidDataDesc;

    while (pDataDesc && (pDataDesc->u4_ValidByteCount != 0))
    {
        u4NumDataBlks++;
        pDataDesc = pDataDesc->pNext;
    }

    pDataDesc = pBuf->pFirstValidDataDesc;

    for (u4DataBlkNum = 1; (u4MsgLen && (u4DataBlkNum <= u4NumDataBlks));
         u4DataBlkNum++)
    {
        if (u4HdrReqd == TRUE)
        {
            SPRINTF (ai1LogMsgBuf, "\nDATA DESCRIPTOR# %u\n", u4DataBlkNum);
            UtlTrcPrint ((const char *) ai1LogMsgBuf);
            UtlDumpDataDesc (pDataDesc);
        }
        SPRINTF (ai1LogMsgBuf, "\nData Block# %u\n", u4DataBlkNum);
        UtlTrcPrint ((const char *) ai1LogMsgBuf);

        /* Number of valid bytes in the data block */
        u4NumBytes = pDataDesc->u4_ValidByteCount;
        if (u4MsgLen >= u4NumBytes)
        {
            /* dump all the data in the block */
            UtlDumpDataBlk (pDataDesc, u4NumBytes);
            u4MsgLen -= u4NumBytes;
        }
        else
        {
            /* dump upto the requested number of bytes and BREAK */
            UtlDumpDataBlk (pDataDesc, u4MsgLen);
            break;
        }
        pDataDesc = pDataDesc->pNext;
    }
}

/***************************************************************/
/*  Function Name   : UtlDumpChainHdr                          */
/*  Description     : Dumps the chain descriptor data structure*/
/*  Input(s)        : pBuf - pointer to the chain descriptor   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
PRIVATE VOID
UtlDumpChainHdr (tCRU_BUF_CHAIN_HEADER * pBuf)
#else
PRIVATE VOID
UtlDumpChainHdr (pBuf)
     tCRU_BUF_CHAIN_HEADER *pBuf;
#endif
{
    char                au1LogMsgBuf[MAX_LOG_STR_LEN];

    SPRINTF (au1LogMsgBuf, "\tpNextChain %p\n", (void *) pBuf->pNextChain);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tpFirstDataDesc %p \n",
             (void *) pBuf->pFirstDataDesc);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tpLastDataDesc %p \n",
             (void *) pBuf->pLastDataDesc);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tpFirstValidDataDesc %p\n",
             (void *) pBuf->pFirstValidDataDesc);
    UtlTrcPrint (au1LogMsgBuf);

    SPRINTF (au1LogMsgBuf, "\nMODULE DATA\n");
    UtlTrcPrint (au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu1_SourceModuleId %u\n",
             pBuf->ModuleData.u1_SourceModuleId);
    UtlTrcPrint (au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu1_DestinModuleId %u\n",
             pBuf->ModuleData.u1_DestinModuleId);
    UtlTrcPrint (au1LogMsgBuf);

    SPRINTF (au1LogMsgBuf, "Interface ID\n");
    UtlTrcPrint (au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu1_InterfaceType %u \n",
             pBuf->ModuleData.InterfaceId.u1_InterfaceType);
    UtlTrcPrint (au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu4IfIndex %u \n",
             pBuf->ModuleData.InterfaceId.u4IfIndex);
    UtlTrcPrint (au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu2_SubReferenceNum %u\n",
             pBuf->ModuleData.InterfaceId.u2_SubReferenceNum);
    UtlTrcPrint (au1LogMsgBuf);

    return;
}

/***************************************************************/
/*  Function Name   : UtlDumpDataDesc                          */
/*  Description     : Dumps the data descriptor data structure */
/*  Input(s)        : pDataDesc - pointer to the data          */
/*                    descriptor                               */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
PRIVATE VOID
UtlDumpDataDesc (tCRU_BUF_DATA_DESC * pDataDesc)
#else
PRIVATE VOID
UtlDumpDataDesc (pDataDesc)
     tCRU_BUF_DATA_DESC *pDataDesc;
#endif
{
    char                au1LogMsgBuf[MAX_LOG_STR_LEN];

    SPRINTF (au1LogMsgBuf, "\tpNext %p \n", (void *) pDataDesc->pNext);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tpPrev %p \n", (void *) pDataDesc->pPrev);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tpu1_FirstValidByte %p\n",
             pDataDesc->pu1_FirstValidByte);
    UtlTrcPrint ((const char *) au1LogMsgBuf);

    SPRINTF (au1LogMsgBuf, "\tu4_ValidByteCount %u \n",
             pDataDesc->u4_ValidByteCount);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu4_FreeByteCount %u\n",
             pDataDesc->u4_FreeByteCount);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu2_UsageCount %u\n", pDataDesc->u2_UsageCount);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    SPRINTF (au1LogMsgBuf, "\tu2_QueId %u\n", pDataDesc->u2_QueId);
    UtlTrcPrint ((const char *) au1LogMsgBuf);
    return;
}

/***************************************************************/
/*  Function Name   : UtlDumpDataBlk                           */
/*  Description     : Dumps the valid data present in the data */
/*                    block                                    */
/*  Input(s)        : pDataDesc - pointer to the DataDesc      */
/*                    u4NumBytes - The no. of bytes to dump    */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
PRIVATE VOID
UtlDumpDataBlk (tCRU_BUF_DATA_DESC * ptDataDesc, UINT4 u4NumBytes)
#else
PRIVATE VOID
UtlDumpDataBlk (ptDataDesc, u4NumBytes)
     tCRU_BUF_DATA_DESC *ptDataDesc;
     UINT4               u4NumBytes;
#endif
{
    char                ai1LogMsgBuf[MAX_CHARS_PER_LINE];
    INT1                ai1TmpMsgBuf[MAX_CHARS_FOR_BYTE_DUMP];
    INT1               *pi1MsgData;
    UINT4               u4NumChars;

    if (ptDataDesc->pu1_FirstValidByte)
    {
        pi1MsgData = (INT1 *) ptDataDesc->pu1_FirstValidByte;
    }
    else
    {
        pi1MsgData = (INT1 *) ptDataDesc + sizeof (tCRU_BUF_DATA_DESC);
    }

    while (u4NumBytes)
    {
        ai1LogMsgBuf[0] = '\0';
        for (u4NumChars = STRLEN (ai1LogMsgBuf);
             (u4NumBytes != 0)
             && (u4NumChars < (MAX_CHARS_PER_LINE - MAX_CHARS_FOR_BYTE_DUMP));
             u4NumChars = STRLEN (ai1LogMsgBuf))
        {
            SPRINTF ((char *) ai1TmpMsgBuf, "0x%.2x ", (UINT1) *pi1MsgData);
            STRCAT ((char *) ai1LogMsgBuf, (char *) ai1TmpMsgBuf);
            u4NumBytes--;
            pi1MsgData++;
        }
        STRCAT (ai1LogMsgBuf, "\n");
        UtlTrcPrint ((const char *) ai1LogMsgBuf);
    }

    return;

}
