/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlsll.h,v 1.18 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL SLL module's exported file.
 *
 */

#ifndef _UTLSLL_H
#define _UTLSLL_H


/**** Type Definitions                       ****/

/************************************ 
*** Type Definition for SLL_NODE ****
*************************************/
typedef struct TMO_SLL_NODE{
   struct TMO_SLL_NODE *pNext; /**** Points to The Next Node In The List ****/
}tTMO_SLL_NODE;

/*******************************************************
*** Type Definition For SLL Structure               ****
*** List is Organised as Singly Linked Cicular List ****
********************************************************/
typedef struct TMO_SLL{
   tTMO_SLL_NODE Head;     /**** Header List Node *****/
   tTMO_SLL_NODE *Tail;    /**** Tail Node        *****/
   UINT4         u4_Count; /**** Number Of Nodes In List ****/
   UINT4         u4_Offset;/**** Offset of the sll node in the user structure****/
}tTMO_SLL;



/**** MACRO Definitions                       ****/
#define TMO_SLL_FREE(pNodeAddr)   MEM_FREE(pNodeAddr)

/*************************************************
**** This Macro Initialises the SLL Structure ****
**************************************************/

#define TMO_SLL_Init(pList)   UTL_SLL_Init(pList,0)

#define UTL_SLL_Init(pList,u4Offset) \
    {\
        (pList)->Head.pNext = &(pList)->Head; \
        (pList)->Tail       = &(pList)->Head; \
        (pList)->u4_Count   = 0;\
        (pList)->u4_Offset  = u4Offset;\
    }

/********************************************
**** This Macro Initialises the SLL Node ****
*********************************************/
#define TMO_SLL_Init_Node(pNode) \
        (pNode)->pNext = NULL;


/**********************************************************************
*****this macro gets the the structure ptr from node ptr and pList*****
***********************************************************************/

#define UTL_SLL_GET_STRUCT_PTR(pList,pNode,TypeCast) \
        ((pNode == NULL) ? NULL: ((TypeCast)((VOID *)((UINT1*)pNode-(pList)->u4_Offset))))


/**********************************************************************
***this macro gets the sll node ptr from the structure ptr and pList***
***********************************************************************/

#define UTL_SLL_GET_NODE_PTR(pList,pStruct) \
        ((pStruct == NULL) ? NULL: ((tTMO_SLL_NODE*)\
                                    ((VOID *)((UINT1*)pStruct+(pList)->u4_Offset))))

/*****************************************************
**** This Macro Appends a Node to end of the List ****
******************************************************/
#define TMO_SLL_Add(pList,pNode) \
        TMO_SLL_Insert_In_Middle((pList),(pList)->Tail,(pNode),&(pList)->Head)

/********************************************************************
*** This Macro Gives the Count of Number of Elements in the List ****
*********************************************************************/
#define TMO_SLL_Count(pList) ((pList)->u4_Count)

/********************************************************
*** This Macro returns the First Element in the List ****
*********************************************************/
#define TMO_SLL_First(pList) \
        ((TMO_SLL_Count((pList)) == 0) ? NULL: ((VOID *)(pList)->Head.pNext))

/*******************************************************
*** This Macro returns the Last Element in the List ****
********************************************************/
#define TMO_SLL_Last(pList) \
        ((TMO_SLL_Count((pList)) == 0) ? NULL : ((VOID *)(pList)->Tail))

/******************************************************************
*** This Macro returns the Element Next To `pNode` in the List ****
*******************************************************************/
#define TMO_SLL_Next(pList,pNode) \
        (((pNode) == NULL) ? TMO_SLL_First(pList) : \
        (((pNode)->pNext == &(pList)->Head) ? NULL : ((VOID *)(pNode)->pNext)))

#define TMO_SLL_Is_Node_In_List(pNode) \
        (pNode->pNext != NULL)

/******************************************************************
**** This Macro Is Useful For Scanning Through The Entire List ****
**** pList -  List TO BE Scanned                               ****
**** pNode -  Pointer To A Variable of Type `TypeCast` In      ****
****          Which The Nodes Are Returned One By One          ****
**** Cast  -  Appropriate Cast(e.g. t_NODE *,When Ptr Is A     ****
****          Pointer To t_NODE Type).                         ****
*******************************************************************/
#define TMO_SLL_Scan(pList,pNode,TypeCast) \
        for(pNode = (TypeCast)(TMO_SLL_First((pList))); \
            pNode != NULL; \
            pNode = (TypeCast)TMO_SLL_Next((pList),((tTMO_SLL_NODE *)(VOID *)(pNode))))

/******************************************************************
**** This Macro Is Useful For Scanning Through The Entire List ****
**** This macro allows deletion within the loop and does not   ****
**** use offset                                                ****
**** pList -  List TO BE Scanned                               ****
**** pNode -  Pointer To A Variable of Type `TypeCast` In      ****
****          Which The Nodes Are Returned One By One          ****
**** pTempNode - Pointer To a structure of Type `TypeCast`     ****
****             in which sll node is a member.Holds the next  ****
****             node to pStruct                               ****
****             types is  Returned One By One                 ****
**** TypeCast -  Appropriate Cast(e.g. t_NODE *,When Ptr Is A  ****
****          Pointer To t_NODE Type).                         ****
*******************************************************************/

#define TMO_DYN_SLL_Scan(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))))

/******************************************************************
**** This Macro Is Useful For Scanning Through The Entire List ****
**** This macro allows deletion within the loop                ****
**** pList -  List TO BE Scanned                               ****
**** pStruct -Pointer To a structure of Type `TypeCast`        ****
****          in which sll node is a member and nodes of this  ****
****          types is  Returned One By One                    ****
**** pTemp   -Pointer To a structure of Type `TypeCast`        ****
****          in which sll node is a member.Holds the next     ****
****          node to pStruct                                  ****
****          types is  Returned One By One                    ****
**** Cast  -  Appropriate Cast(e.g. t_NODE *,When Ptr Is A     ****
****          Pointer To t_NODE Type).                         ****
*******************************************************************/

#define UTL_SLL_OFFSET_SCAN(pList,pStruct,pTemp,TypeCast) \
            for(pStruct = UTL_SLL_GET_STRUCT_PTR(pList,TMO_SLL_First((pList)),\
                          TypeCast),\
                          pTemp = ((pStruct == NULL)? NULL : \
                          UTL_SLL_GET_STRUCT_PTR(pList,TMO_SLL_Next((pList),\
                          UTL_SLL_GET_NODE_PTR(pList,pStruct)),TypeCast)); \
                pStruct != NULL; \
                pStruct = pTemp ,\
                pTemp   = ((pStruct == NULL)? NULL :\
                          UTL_SLL_GET_STRUCT_PTR(pList,TMO_SLL_Next((pList),\
                          UTL_SLL_GET_NODE_PTR(pList,pStruct)),TypeCast)))


/****        Prototype Definitions                    ****/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


VOID TMO_SLL_Insert_In_Middle(tTMO_SLL *pList, tTMO_SLL_NODE *pPrev, tTMO_SLL_NODE *pMid, tTMO_SLL_NODE *pNext);


VOID TMO_SLL_Replace ARG_LIST((tTMO_SLL      *pList, \
                               tTMO_SLL_NODE *pOld, \
                               tTMO_SLL_NODE *pNew));

VOID TMO_SLL_Delete_In_Middle ARG_LIST((tTMO_SLL      *pList, \
                                        tTMO_SLL_NODE *pPrev, \
                                        tTMO_SLL_NODE *pNode, \
                                        tTMO_SLL_NODE *pNext));

VOID TMO_SLL_Concat ARG_LIST((tTMO_SLL *pDstList,tTMO_SLL *pAddList));

VOID TMO_SLL_Delete ARG_LIST((tTMO_SLL *pList,tTMO_SLL_NODE *pNode));

VOID TMO_SLL_Extract ARG_LIST((tTMO_SLL *pSrcList,tTMO_SLL_NODE *pStartNode,tTMO_SLL_NODE *pEndNode,tTMO_SLL *pDstList));

tTMO_SLL_NODE *TMO_SLL_Get ARG_LIST((tTMO_SLL *pList));

VOID TMO_SLL_Insert ARG_LIST((tTMO_SLL *pList,tTMO_SLL_NODE *pPrev,tTMO_SLL_NODE *pNode));

tTMO_SLL_NODE *TMO_SLL_Nth ARG_LIST((tTMO_SLL *pList,UINT4 u4NodeNum));

tTMO_SLL_NODE *TMO_SLL_Previous ARG_LIST((tTMO_SLL *pList,tTMO_SLL_NODE *pNode));

UINT4 TMO_SLL_Find ARG_LIST((tTMO_SLL *pList,tTMO_SLL_NODE *pNode));

VOID TMO_SLL_FreeNodes ARG_LIST((tTMO_SLL *pList, tMemPoolId MemPoolId));

VOID TMO_SLL_Clear ARG_LIST((tTMO_SLL *pList));

tTMO_SLL_NODE *TMO_SLL_Get_N_Node ARG_LIST((tTMO_SLL *pList,UINT4 u4NumOfNodes));

UINT4 TMO_SLL_Delete_Till_Node ARG_LIST((tTMO_SLL       *pSrcList, \
                                         tTMO_SLL_NODE  *pStartNode, \
                                         tTMO_SLL_NODE  *pEndNode, \
                                         VOID   (*pDelFunc)(tTMO_SLL_NODE *)));

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /**** _UTLSLL_H ****/
