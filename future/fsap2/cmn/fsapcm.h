/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: fsapcm.h,v 1.11 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Exported functions from fsap's CM module, which
 *              provides multinode support.
 */

/* Prototype of callback function that applications would
 * have to register with CM.
 * The two params that CM passes back are the received CRU buffer
 * and the length of the packet.
 */
#ifndef _FSAP_CM_H
#define _FSAP_CM_H
typedef void (*tCmCallBackFn)(void *, UINT4 u4Len);

/************************************************************************
*                                                                       *
*               Exported Function Prototypes                            *
*                                                                       *
*************************************************************************/
INT4                CmReadNodes (UINT4 *pu4SelfNodeId, UINT4 *pu4PeerNodeId);
INT4                CmInit (UINT4 u4SelfNodeId);
INT4                CmShut (void);
INT4                CmSend (UINT4 u4DestNode, tCRU_BUF_CHAIN_DESC *, UINT4 len);
void                CmRegisterCallBack(tCmCallBackFn cb);
#endif
