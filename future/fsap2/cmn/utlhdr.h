/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlhdr.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Common types and defines used by UTL module.
 *
 */

/**************************************************************
*** The following types are specified in the interface_type ***
*** field of the physical interface structure. The field    ***
*** values are the same as the MIB-II values for ifType.    ***
***************************************************************/
#ifndef _UTLHDR_H
#define _UTLHDR_H
#define  CRU_RFC877X25_PHYS_INTERFACE_TYPE   0x05
#define  CRU_ENET_PHYS_INTERFACE_TYPE        0x06
#define  CRU_TOKB_PHYS_INTERFACE_TYPE        0x08
#define  CRU_TOKR_PHYS_INTERFACE_TYPE        0x09
#define  CRU_MAN_PHYS_INTERFACE_TYPE         0x0A
#define  CRU_FDDI_PHYS_INTERFACE_TYPE        0x0F
#define  CRU_SDLC_PHYS_INTERFACE_TYPE        0x11
#define  CRU_ISDN_BASIC_INTERFACE_TYPE       0x14
#define  CRU_ISDN_PRI_INTERFACE_TYPE         0x15
#define  CRU_PPP_PHYS_INTERFACE_TYPE         0x17
#define  CRU_FRMRL_PHYS_INTERFACE_TYPE       0x20
#define  CRU_X25_PHYS_INTERFACE_TYPE         0x26
#define  CRU_SONET_PHYS_INTERFACE_TYPE       0x27
#define  CRU_X25_SVC_INTERFACE_TYPE          0x86
#define  CRU_DDCMP_PHYS_INTERFACE_TYPE       0x87
#define  CRU_UNDEFINED_PHYS_INTERFACE_TYPE   0x99

/********************************************
***** Ethernet Protocol Type Constants  *****
*********************************************/
#define  CRU_ENET_IP_TYPE        0x0800
#define  CRU_ENET_ARP_TYPE       0x0806
#define  CRU_ENET_XNS_TYPE       0x0600
#define  CRU_ENET_DECNET_TYPE    0x6003
#define  CRU_ENET_RARP_TYPE      0x8035
#define  CRU_ENET_AARP_TYPE      0x80F3
#define  CRU_ENET_IPX_TYPE       0x8137
#define  CRU_ENET_ATALK_TYPE     0x809B
#define  CRU_ENET_LOOPBACK_TYPE  0x9000

/**************************************************************
***** Ethernet Protocol Type Constants For IEEE 802.1 OUI *****
***************************************************************/
#define  CRU_ENET_PFCS_8023_TYPE        0x0001
#define  CRU_ENET_NFCS_8023_TYPE        0x0007
#define  CRU_ENET_PFCS_8024_TYPE        0x0002
#define  CRU_ENET_NFCS_8024_TYPE        0x0008
#define  CRU_ENET_PFCS_8025_TYPE        0x0003
#define  CRU_ENET_NFCS_8025_TYPE        0x0009
#define  CRU_ENET_PFCS_FDDI_TYPE        0x0004
#define  CRU_ENET_NFCS_FDDI_TYPE        0x000A
#define  CRU_ENET_PFCS_8026_TYPE        0x0005
#define  CRU_ENET_NFCS_8026_TYPE        0x000B
#define  CRU_ENET_PFCS_BPDU_TYPE        0x000E

/*********************************************
*** LLC SAP Type constants *******************
**********************************************/
#define  CRU_LLC_STAP_SAP        0x42
#define  CRU_LLC_SNAP_SAP        0xAA
#define  CRU_LLC_ISO_L4_SAP      0xFE
#define  CRU_LLC_CONTROL_UI_TYPE 0x03

/****************************************************
*** Different types of Encapsulation header sizes ***
*****************************************************/
#define  CRU_MAX_ENET_ADDR_LEN         6
#define  CRU_ENET_TYPE_LEN_SIZE        2
#define  CRU_ENET_HEADER_SIZE          14
#define  CRU_LLC_HEADER_SIZE           3
#define  CRU_DLS_HEADER_SIZE           8
#define  CRU_SNAP_HEADER_SIZE          5
#define  CRU_IP_HEADER_SIZE            20
#define  CRU_UDP_HEADER_SIZE           8
#define  CRU_IPX_HEADER_SIZE           30
#define  CRU_XNS_HEADER_SIZE           30
#define  CRU_MAX_ENET_DATA_SIZE        1500
#define  CRU_TCP_HEADER_SIZE           20


typedef struct CRU_ENET_HEADER{
        UINT1 u1_Dst_Addr[CRU_MAX_ENET_ADDR_LEN];
        UINT1 u1_Src_Addr[CRU_MAX_ENET_ADDR_LEN];
        UINT2 u2_Len_Or_Type;
        UINT2 u2Pad;
}tCRU_ENET_HEADER;

/*****************************************************
*** The following structure is defined for         ***
*** comparisons on incoming frame MAC addresses.   ***
*** It may also be used for MAC address assignment ***
******************************************************/
typedef struct CRU_MAC_ADDR{
        UINT4 u4_Word;
        UINT2 u2_Word;
        UINT2 u2Pad;
}tCRU_MAC_ADDR;

/*******************************************
*** Type-1 LLC Header Format definitions ***
********************************************/
typedef struct CRU_LLC_HEADER{
        UINT1 u1_Dst_LSap;
        UINT1 u1_Src_LSap;
        UINT1 u1_Control;
        UINT1 u1Pad;
}tCRU_LLC_HEADER;
 
#endif
