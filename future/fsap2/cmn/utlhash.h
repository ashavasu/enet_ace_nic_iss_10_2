/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlhash.h,v 1.18 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL HASH module's exported file.
 *
 */

#ifndef _UTLHASH_H
#define _UTLHASH_H

/***** utlhash.h *****/

/*****      Constant definition *****/
#define  MATCH_NOT_FOUND    0
#define  INSERT_PRIORTO     1
#define  INSERT_NEXTTO      2

/*****      Type Definition     *****/

#define tTMO_HASH_NODE   tTMO_SLL_NODE 
#define tTMO_HASH_BUCKET tTMO_SLL


typedef  struct tTMO_HASH_TABLE{
         UINT4             u4_HashSize;       /* Size of hash Table */
         UINT4             (*pInsertFunc)(tTMO_HASH_NODE *, UINT1 *);  /* Pointer to Hash Insert Func */
         tOsixSemId         SemId;             /* Sema4 used for mutex */
         UINT1             u1_MutexLockFlag;  /* Flag which specifies whether
                                               * Mutual Exclusion is enabled
                                               * or NOT. */
         UINT1             au1Pad[3];
         tTMO_HASH_BUCKET  HashList[1];       /* Array of Hash buckets */
}tTMO_HASH_TABLE;


/*****      Macros Definition     *****/
#define TMO_HASH_BUCKET_INIT        TMO_SLL_Init
#define UTL_HASH_BUCKET_INIT        UTL_SLL_Init
#define TMO_HASH_ADD_NODE           TMO_SLL_Add
#define TMO_HASH_REPLACE_NODE       TMO_SLL_Replace
#define TMO_HASH_DELETE_NODE        TMO_SLL_Delete
#define TMO_HASH_INSERT_NODE        TMO_SLL_Insert
#define TMO_HASH_FIRST_NODE         TMO_SLL_First
#define TMO_HASH_LAST_NODE          TMO_SLL_Last
#define TMO_HASH_NEXT_NODE          TMO_SLL_Next
#define TMO_HASH_INIT_NODE          TMO_SLL_Init_Node
#define TMO_HASH_SCAN_BUCKET        TMO_SLL_Scan
#define UTL_HASH_SCAN_BUCKET        UTL_SLL_OFFSET_SCAN
#define TMO_HASH_DYN_SCAN_BUCKET    TMO_DYN_SLL_Scan

#define TMO_HASH_Scan_Bucket(pHashTab,u4HashIndex,pNodePtr,TypeCast) \
        TMO_HASH_SCAN_BUCKET(&(pHashTab)->HashList[(u4HashIndex)],(pNodePtr),TypeCast)

#define  TMO_HASH_DYN_Scan_Bucket(pHashTab, u4HashIndex, pNode, pTempNode, \
                                  TypeCast) \
         TMO_HASH_DYN_SCAN_BUCKET(&((pHashTab)->HashList[(u4HashIndex)]), pNode, \
                                  pTempNode, TypeCast)

#define UTL_HASH_Scan_Bucket(pHashTab,u4HashIndex,pNodePtr,pTemp,TypeCast) \
        UTL_HASH_SCAN_BUCKET(&(pHashTab)->HashList[(u4HashIndex)],(pNodePtr),pTemp,TypeCast)

#define TMO_HASH_Scan_Table(pHashTab,u4HashIndex) \
        for((u4HashIndex) = 0; (u4HashIndex) < (pHashTab)->u4_HashSize; (u4HashIndex) = (u4HashIndex) +1)

#define TMO_HASH_Get_First_Bucket_Node(pHashTab,u4HashIndex) \
        TMO_HASH_FIRST_NODE(&pHashTab->HashList[u4HashIndex])
        
#define TMO_HASH_Get_Next_Bucket_Node(pHashTab,u4HashIndex,pNode) \
        TMO_HASH_NEXT_NODE(&pHashTab->HashList[u4HashIndex],pNode)

#define TMO_HASH_Bucket_Count(pHashTab,u4HashIndex) \
        TMO_SLL_Count(&((pHashTab)->HashList[(u4HashIndex)]))
    
#define TMO_HASH_Init_Node(pNode) TMO_HASH_INIT_NODE(pNode)
#define TMO_HASH_Insert_Bucket(pHashTab, u4HashIndex, pPrev, pNode)\
        TMO_HASH_INSERT_NODE (&(pHashTab)->HashList[u4HashIndex],pPrev,pNode)
/********   Prototype Declaration  *********/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

tTMO_HASH_TABLE *TMO_HASH_Create_Table ARG_LIST((UINT4 u4HashSize, \
                                                 UINT4 (*pInsertFunc)(tTMO_HASH_NODE *, UINT1 *),\
                                                 UINT1 u1MutexLockFlag));

tTMO_HASH_TABLE *UTL_HASH_Create_Table ARG_LIST((UINT4 u4HashSize, \
                                                 UINT4 (*pInsertFunc)(tTMO_HASH_NODE *, UINT1 *),\
                                                 UINT1 u1MutexLockFlag, UINT4 u4Offset));


VOID TMO_HASH_Add_Node ARG_LIST((tTMO_HASH_TABLE *pHashTab,tTMO_HASH_NODE *pNode,UINT4 u4HashIndex,UINT1 *pu1InsertFuncParam));

VOID TMO_HASH_Delete_Node ARG_LIST((tTMO_HASH_TABLE *pHashTab,tTMO_HASH_NODE *pNode,UINT4 u4HashIndex));

VOID TMO_HASH_Replace_Node ARG_LIST((tTMO_HASH_TABLE *pHashTab,tTMO_HASH_NODE *pOldNode,UINT4 u4HashIndex,tTMO_HASH_NODE *pNewNode));

VOID TMO_HASH_Delete_Table ARG_LIST((tTMO_HASH_TABLE *pHashTab,VOID (*pFreeNodeMemFunc)(tTMO_HASH_NODE *)));
VOID TMO_HASH_Clean_Table ARG_LIST((tTMO_HASH_TABLE * pHashTab)); /* MSAD ADD */
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /***** _UTLHASH_H *****/
