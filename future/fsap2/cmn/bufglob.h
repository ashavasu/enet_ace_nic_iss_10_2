/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bufglob.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Global variables in the SRM Buf module.
 *
 */
#ifndef  _BUFGLOB_H
#define _BUFGLOB_H

#ifdef BUFLIB_C
#define EXTERN
#else
#define EXTERN extern
#endif

EXTERN tCRU_BUF_FREE_QUE_DESC *pCRU_BUF_Chain_FreeQueDesc; 
EXTERN tCRU_BUF_FREE_QUE_DESC *pCRU_BUF_DataBlk_FreeQueDesc; 

EXTERN UINT4    gu4MaxDataBlockCfgs;
EXTERN UINT4 gu4MinBufferSize;
EXTERN UINT4 gu4MemoryType;

EXTERN tCRU_BUF_FREE_QUE_DESC *pCRU_BUF_DataDesc_FreeQueDesc; 
EXTERN UINT4 gu4MaxDataBlocks;

/* Named string - for the Semaphore used in Buffer library */
EXTERN UINT1 gau1SemName[5];         

#endif  /* _BUFGLOB_H */
