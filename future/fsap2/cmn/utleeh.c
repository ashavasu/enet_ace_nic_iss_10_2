/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utleeh.c,v 1.16 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL EEH module.
 *
 */

#include "utleehi.h"
#include "srmmem.h"

/* Global data structures */
PRIVATE tEEH_EVENT_INFO gEehMasterInfo = { UTL_EEH_LOG_NONE,
    0, 0, 0, NULL, NULL
};

PRIVATE UINT4       gu4EehRecordPoolId = 0;
PRIVATE tOsixSemId  gEehSemId = 0;

/* Prototype declarations for the public functions */
EXPORT UINT4 UtlEehInit ARG_LIST ((UINT4 u4HistSize, UINT4 u4MsgSize));
EXPORT UINT4 UtlEehShutDown ARG_LIST ((VOID));
EXPORT UINT4 UtlEehGetLevel ARG_LIST ((VOID));
EXPORT VOID UtlEehSetLevel ARG_LIST ((UINT4 u4Level));
EXPORT VOID UtlEehNotify ARG_LIST ((UINT4 u4Type, UINT4 u4Code,
                                    UINT4 u4Src, UINT4 u4Dst, UINT4 u4Param1,
                                    UINT4 u4Param2, UINT4 u4Param3,
                                    UINT4 u4Param4, INT1 *pi1Str));
EXPORT VOID UtlEehPrintHist ARG_LIST ((UINT4 u4ModID));
EXPORT VOID UtlEehResetHist ARG_LIST ((VOID));

/***************************************************************/
/*  Function Name   : UtlEehInit                               */
/*  Description     : Allocates memory pools for the error or  */
/*                    Event record table and initializes the   */
/*                    master info structure                    */
/*  Input(s)        : u4HistSize - The maximum number of       */
/*                    records in the record table              */
/*                    u4MsgSize - The size of the message      */
/*                    string in each record.                   */
/*  Output(s)       : None                                     */
/*  Returns         : UTL_EEH_SUCCESS                          */
/*                    UTL_ERR_PARAM_ERR - error in input parms */
/*                    UTL_ERR_NO_MEM - memory not available    */
/*                    UTL_ERR_NO_RSRC - resource not available */
/***************************************************************/
#ifdef __STDC__
EXPORT UINT4
UtlEehInit (UINT4 u4HistSize, UINT4 u4MsgSize)
#else
EXPORT UINT4
UtlEehInit (u4HistSize, u4MsgSize)
     UINT4               u4HistSize;    /* The number of history records to be maintained */
     UINT4               u4MsgSize;    /* The size of the message associated with each error/event */
#endif /* __STDC__ */
{
    UINT4               u4TmpMsgSize = u4MsgSize;
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    /* Rukmani  - added the following check on 07/05/99 */
    if (u4HistSize == 0)
    {
        UtlTrcPrint ("\n\r Invalid Parameter to UtlEehInit.\n");
        return (UTL_ERR_PARAM_ERR);
    }

    /* Adjusting the specified message size to a multiple of 4 */
    if ((u4MsgSize % 4) != 0)
    {
        u4TmpMsgSize = u4MsgSize + (4 - (u4MsgSize % 4));
    }

    /* 
     * Create a memory pool of u4HistSize objects, each object being 
     * (u4MsgSize + sizeof (tEEH_EVENT_LOG_RECORD)) in size.
     */
    if (MemCreateMemPool
        ((sizeof (tEEH_EVENT_LOG_RECORD) + u4TmpMsgSize), u4HistSize,
         MEM_DEFAULT_MEMORY_TYPE, &gu4EehRecordPoolId) == MEM_FAILURE)
        return UTL_ERR_NO_MEM;

    /* 
     * Create a semaphore to synchonise access to global data structures
     * between tasks.
     */

    MEMSET (au1Name, '\0', OSIX_NAME_LEN + 4);
    STRCPY (au1Name, UTL_EEH_SEM_NAME);
    if (OsixSemCrt (au1Name, &gEehSemId) != OSIX_SUCCESS)
    {
        return UTL_ERR_NO_RSRC;
    }

    OsixSemGive (gEehSemId);
    gEehMasterInfo.u4ReqNoOfEehRecords = u4HistSize;
    gEehMasterInfo.u4MaxMsgSize = u4MsgSize;

    return UTL_EEH_SUCCESS;
}

/***************************************************************/
/*  Function Name   : UtlEehShutDown                           */
/*  Description     : Releases the memory pools and other      */
/*                    resources and updates the master info    */
/*                    structure.                               */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : UTL_EEH_SUCCESS                          */
/*                    UTL_ERR_MEM_ERR - Mem pool release failed*/
/*                    UTL_ERR_RSRC_ERR - delete semaphore      */
/*                    failed.                                  */
/***************************************************************/
EXPORT UINT4
UtlEehShutDown (VOID)
{
    /* Delete the memory pool created during EehInit. */
    if (MemDeleteMemPool (gu4EehRecordPoolId) == MEM_SUCCESS)
    {
        gEehMasterInfo.u4EehCtrlLevel = UTL_EEH_LOG_NONE;
        gEehMasterInfo.u4ReqNoOfEehRecords = 0;
        gEehMasterInfo.u4NoOfEehRecords = 0;
        gEehMasterInfo.ptHead = NULL;
        gEehMasterInfo.ptTail = NULL;
    }
    else
    {
        return UTL_ERR_MEM_ERR;
    }

    /* delete the eeh semaphore */
    OsixSemDel (gEehSemId);
    return UTL_EEH_SUCCESS;
}

/***************************************************************/
/*  Function Name   : UtlEehGetLevel                           */
/*  Description     : Returns the current level of error and   */
/*                    event notification that is enabled       */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : the current level                        */
/***************************************************************/
EXPORT UINT4
UtlEehGetLevel (VOID)
{
    return (gEehMasterInfo.u4EehCtrlLevel);
}

/***************************************************************/
/*  Function Name   : UtlEehSetLevel                           */
/*  Description     : Sets the level of error and event        */
/*                    handling notification                    */
/*  Input(s)        : u4Level - The level to be set            */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
EXPORT VOID
UtlEehSetLevel (UINT4 u4Level)
#else
EXPORT VOID
UtlEehSetLevel (u4Level)
     UINT4               u4Level;
#endif /* __STDC__ */
{
    /* 
     * Set only the bits in u4Level corresponding to the Event and
     * error categories defined in utleeh.h
     */
    gEehMasterInfo.u4EehCtrlLevel =
        (u4Level &
         (UTL_EEH_LOG_NONE | UTL_EEH_LOG_CONF_ERR | UTL_EEH_LOG_PROTO_ERR |
          UTL_EEH_LOG_MAJOR_EVT | UTL_EEH_LOG_MINOR_EVT));
    return;
}

/***************************************************************/
/*  Function Name   : UtlEehNotify                             */
/*  Description     : Records the error or event in the table  */
/*                    if the type of error or event is enabled */
/*  Input(s)        : u4Type - type of this error              */
/*                    u4Code - the error or event code         */
/*                    u4Src - the module id of the source      */
/*                    module                                   */
/*                    u4Dst - the module id of the notifying   */
/*                    module                                   */
/*                    u4Param1..4 - parameters which the module*/
/*                    wishes to record.                        */
/*                    pStr - a string which the module wishes  */
/*                    to record                                */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
EXPORT VOID
UtlEehNotify (UINT4 u4Type, UINT4 u4Code,
              UINT4 u4Src, UINT4 u4Dst, UINT4 u4Param1, UINT4 u4Param2,
              UINT4 u4Param3, UINT4 u4Param4, INT1 *pi1Str)
#else
VOID
UtlEehNotify (u4Type, u4Code, u4Src, u4Dst, u4Param1, u4Param2,
              u4Param3, u4Param4, pi1Str)
     UINT4               u4Type;
     UINT4               u4Code;
     UINT4               u4Src;
     UINT4               u4Dst;
     UINT4               u4Param1;
     UINT4               u4Param2;
     UINT4               u4Param3;
     UINT4               u4Param4;
     UINT1              *pi1Str;
#endif /* __STDC__ */
{
    tEEH_EVENT_LOG_RECORD *ptLogRecord;
    tEEH_EVENT_LOG_RECORD *ptTmpLogRecord;

    if (u4Type != UTL_ERR_FATAL)
    {
        /* Check whether this type of logging is currently enabled */
#if  0                            /* rukmani  - changed on 07/05/99 */
        if (!(u4Type & gEehMasterInfo.u4EehCtrlLevel))
        {
            return;
        }
#else
        switch (u4Type)
        {
            case UTL_ERR_CONF:
                if (!(gEehMasterInfo.u4EehCtrlLevel & UTL_EEH_LOG_CONF_ERR))
                    return;
                else
                    break;

            case UTL_ERR_PROTO:
                if (!(gEehMasterInfo.u4EehCtrlLevel & UTL_EEH_LOG_PROTO_ERR))
                    return;
                else
                    break;

            case UTL_EVT_MAJOR:
                if (!(gEehMasterInfo.u4EehCtrlLevel & UTL_EEH_LOG_MAJOR_EVT))
                    return;
                else
                    break;

            case UTL_EVT_MINOR:
                if (!(gEehMasterInfo.u4EehCtrlLevel & UTL_EEH_LOG_MINOR_EVT))
                    return;
                else
                    break;

            default:
                return;
        }
#endif /*  0 */
    }

    /* Wait for the error/event semaphore */
    OsixSemTake (gEehSemId);

    if (gEehMasterInfo.u4NoOfEehRecords >= gEehMasterInfo.u4ReqNoOfEehRecords)
    {
        /*
         * If the number of active history records has exceeded the required
         * number, release the oldest record from the tail back to the object
         * pool, and decrement the active records counter.
         */
        ptTmpLogRecord = gEehMasterInfo.ptTail->ptPrevLogRecord;
        if (MemReleaseMemBlock (gu4EehRecordPoolId,
                                (UINT1 *) gEehMasterInfo.ptTail) == MEM_FAILURE)
        {
            OsixSemGive (gEehSemId);
            return;
        }
        ptTmpLogRecord->ptNextLogRecord = NULL;
        gEehMasterInfo.ptTail = ptTmpLogRecord;
        gEehMasterInfo.u4NoOfEehRecords--;
    }

    /* Allocate an object from the pool to log this error/event */
    if ((ptLogRecord = (tEEH_EVENT_LOG_RECORD *)
         (VOID *) MemAllocMemBlk (gu4EehRecordPoolId)) != NULL)
    {
        /* 
         * if this is the first error/event to be logged, update the pointer to
         * the Tail record.
         */
        if (gEehMasterInfo.ptTail == NULL)
        {
            gEehMasterInfo.ptTail = ptLogRecord;
        }
        ptLogRecord->ptPrevLogRecord = NULL;

        /* Rukmani  - added the following two lines on 10/05/99 */
        if (gEehMasterInfo.ptHead)
            gEehMasterInfo.ptHead->ptPrevLogRecord = ptLogRecord;

        ptLogRecord->ptNextLogRecord = gEehMasterInfo.ptHead;
        gEehMasterInfo.ptHead = ptLogRecord;
        gEehMasterInfo.u4NoOfEehRecords++;
    }
    else
    {                            /* allocation of object failed */
        OsixSemGive (gEehSemId);
        return;
    }

    /* Release the error/event semaphore */
    OsixSemGive (gEehSemId);

    /* Log the error/event in the record */
    ptLogRecord->u4EventType = u4Type;
    ptLogRecord->u4EventCode = u4Code;
    ptLogRecord->u4EventSrcModule = u4Src;
    ptLogRecord->u4EventDstModule = u4Dst;
    ptLogRecord->u4EventParam1 = u4Param1;
    ptLogRecord->u4EventParam2 = u4Param2;
    ptLogRecord->u4EventParam3 = u4Param3;
    ptLogRecord->u4EventParam4 = u4Param4;

    OsixGetSysTime ((tOsixSysTime *) & ptLogRecord->u4EventTime);

    ptLogRecord->pi1EventStr =
        (INT1 *) ptLogRecord + sizeof (tEEH_EVENT_LOG_RECORD);

    /*
     * Limit the input to string to the length specified during init and copy
     * it to the error/event record
     */
    if (pi1Str)
    {
        STRNCPY (ptLogRecord->pi1EventStr, pi1Str, gEehMasterInfo.u4MaxMsgSize);
        (ptLogRecord->pi1EventStr)[gEehMasterInfo.u4MaxMsgSize - 1] = '\0';
    }
    else
        (ptLogRecord->pi1EventStr)[0] = '\0';

    /* 
     * If the type of error is UTL_FATAL_ERR, call the OS specific fault
     * handler.
     */
    if (u4Type == UTL_ERR_FATAL)
    {
    }

    return;
}

/***************************************************************/
/*  Function Name   : UtlEehPrintHist                          */
/*  Description     : Prints the errors and events notified by */
/*                    the specified module.                    */
/*  Input(s)        : u4Mod - the module whose notifications   */
/*                    are to be printed.                       */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
EXPORT VOID
UtlEehPrintHist (UINT4 u4Mod)
#else
EXPORT VOID
UtlEehPrintHist (u4Mod)
     UINT4               u4Mod;
#endif /* __STDC__ */
{
    UINT4               u4RecordNum = 0;

    tEEH_EVENT_LOG_RECORD *ptLogRecord;
    char                ai1PrintStr[150];    /* Temporary string to print event header info */

    /* Parse from the oldest log record (Tail) */
    ptLogRecord = gEehMasterInfo.ptTail;
    while (ptLogRecord)
    {
        if ((u4Mod == 0) || (u4Mod == ptLogRecord->u4EventDstModule))
        {
            u4RecordNum++;
            SPRINTF (ai1PrintStr, "\n\n LogRecord Information# %u\n",
                     u4RecordNum);
            UtlTrcPrint ((const char *) ai1PrintStr);

            SPRINTF (ai1PrintStr, "\n\tType 0x%08x  \tCode 0x%08x\n",
                     ptLogRecord->u4EventType, ptLogRecord->u4EventCode);
            UtlTrcPrint ((const char *) ai1PrintStr);

            SPRINTF (ai1PrintStr, "\tSrc  0x%08x \tDst  0x%08x\n",
                     ptLogRecord->u4EventSrcModule,
                     ptLogRecord->u4EventDstModule);
            UtlTrcPrint ((const char *) ai1PrintStr);

            SPRINTF (ai1PrintStr, "\tParam1 0x%08x \tParam2 0x%08x \n",
                     ptLogRecord->u4EventParam1, ptLogRecord->u4EventParam2);
            UtlTrcPrint ((const char *) ai1PrintStr);

            SPRINTF (ai1PrintStr, "\tParam3 0x%08x \tParam4 0x%08x \n",
                     ptLogRecord->u4EventParam3, ptLogRecord->u4EventParam4);
            UtlTrcPrint ((const char *) ai1PrintStr);

            SPRINTF (ai1PrintStr, "\tTime 0x%08x\n", ptLogRecord->u4EventTime);
            UtlTrcPrint ((const char *) ai1PrintStr);

            UtlTrcPrint ("\tEventStr  ");
            if (ptLogRecord->pi1EventStr)
                UtlTrcPrint ((const char *) ptLogRecord->pi1EventStr);
            else
                UtlTrcPrint (" (null)");
            UtlTrcPrint ("\n");
        }
        ptLogRecord = ptLogRecord->ptPrevLogRecord;
    }
    return;
}

/***************************************************************/
/*  Function Name   : UtlEehResetHist                          */
/*  Description     : Clears the History record                */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
EXPORT VOID
UtlEehResetHist (VOID)
{
    tEEH_EVENT_LOG_RECORD *ptTmpLogRecord;

    /* Release the objects to the mem pool starting from the head */
    ptTmpLogRecord = gEehMasterInfo.ptHead;
    while (ptTmpLogRecord)
    {
        gEehMasterInfo.ptHead = ptTmpLogRecord->ptNextLogRecord;
        if (MemReleaseMemBlock (gu4EehRecordPoolId,
                                (UINT1 *) ptTmpLogRecord) == MEM_SUCCESS)
        {
            gEehMasterInfo.u4NoOfEehRecords--;
        }
        else
        {
            gEehMasterInfo.ptHead = ptTmpLogRecord;
            return;
        }

        ptTmpLogRecord = ptTmpLogRecord->ptNextLogRecord;
    }
    gEehMasterInfo.ptTail = NULL;
    return;
}
