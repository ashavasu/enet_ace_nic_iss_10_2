/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlbit.c,v 1.14 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL bit module.
 *
 */

#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utlbit.h"

/***************************************************************/
/*  Function Name   : CRU_BML_Set_Bit_In32BInt                 */
/*  Description     : Sets the bit specified in 'u1BitPosition'*/
/*                    in 32 bit integer pointed to by          */
/*                    'pu4Number' to 1.  Bit numbering is from */
/*                    right to left (0 - 31)                   */
/*  Input(s)        : pu4Number  - Pointer to the 32bit integer*/
/*                    u1BitPosition - Position of the bit which*/
/*                                     needs to be set         */
/*  Output(s)       : None                                     */
/*  Returns         : UTL_SUCCESS, if operation successful,    */
/*                    UTL_FAILURE, Otherwise                   */
/***************************************************************/
#ifdef __STDC__
UINT1
CRU_BML_Set_Bit_In32BInt (UINT4 *pu4Number, UINT1 u1BitPosition)
#else
UINT1
CRU_BML_Set_Bit_In32BInt (pu4Number, u1BitPosition)
     UINT4              *pu4Number;
     UINT1               u1BitPosition;
#endif /* __STDC__ */
{

    /* Standard error checking */

    if (u1BitPosition >= 32)
        return (!UTL_SUCCESS);

    *pu4Number |= (1 << u1BitPosition);

    return (UTL_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CRU_BML_Clear_Bit_In32BInt               */
/*  Description     : Clears the bit specified in              */
/*                    'u1BitPosition' in 32 bit integer pointed*/
/*                    to by 'pu4Number' to 1. Bit numbering is */
/*                    from right to left (0 - 31)              */
/*  Input(s)        : pu4Number  -   Pointer to 32bit integer  */
/*                    u1BitPosition - Position of the bit which*/
/*                                    needs to be set          */
/*  Output(s)       : None                                     */
/*  Returns         : UTL_SUCCESS, if operation successful,    */
/*                    UTL_FAILURE, Otherwise                   */
/***************************************************************/
#ifdef __STDC__
UINT1
CRU_BML_Clear_Bit_In32BInt (UINT4 *pu4Number, UINT1 u1BitPosition)
#else
UINT1
CRU_BML_Clear_Bit_In32BInt (pu4Number, u1BitPosition)
     UINT4              *pu4Number;
     UINT1               u1BitPosition;
#endif /* __STDC__ */
{

    /* Standard error checking */

    if (u1BitPosition >= 32)
        return (!UTL_SUCCESS);

    *pu4Number &= ~(1 << u1BitPosition);

    return (UTL_SUCCESS);

}

/***************************************************************/
/*  Function Name   : CRU_BML_Check_Bit_In32BInt               */
/*  Description     : Checks the status of bit specified in    */
/*                    'u1BitPosition' in   32 bit integer      */
/*                    'u4Number'. Bit numbering is from right  */
/*                    to left (0 - 31)                         */
/*  Input(s)        : u4Number   - 32bit integer               */
/*                    u1BitPosition - Position of the bit which*/
/*                              needs to be checked            */
/*  Output(s)       : None                                     */
/*  Returns         : 1, if the specified bit is SET,          */
/*                    0, Otherwise                             */
/***************************************************************/
#ifdef __STDC__
UINT1
CRU_BML_Check_Bit_In32BInt (UINT4 u4Number, UINT1 u1BitPosition)
#else
UINT1
CRU_BML_Check_Bit_In32BInt (u4Number, u1BitPosition)
     UINT4               u4Number;
     UINT1               u1BitPosition;
#endif /* __STDC__ */
{
    /* Returns 0 if that bit is not set else returns 1 */

    return ((u4Number & (1 << u1BitPosition)) ? (1) : (0));
}

/***************************************************************/
/*  Function Name   : CRU_BML_Find_SetBit_In32BInt             */
/*  Description     : Scans the number and returns the bit     */
/*                    position of the first most significant   */
/*                    bit (MSB) set.  Bit numbering is from    */
/*                    right to left (0 - 31)                   */
/*  Input(s)        : u4Number  -  32bit integer               */
/*  Output(s)       : None                                     */
/*  Returns         : 0 if not bits are set,                   */
/*                    32 if MSB is set,                        */
/*                    1 if LSB is set                          */
/***************************************************************/
#ifdef __STDC__
UINT1
CRU_BML_Find_SetBit_In32BInt (UINT4 u4Number)
#else
UINT1
CRU_BML_Find_SetBit_In32BInt (u4Number)
     UINT4               u4Number;
#endif /* __STDC__ */
{

    /* Scans the number and returns the bit position of the least
       significant bit set. Note that it returns 0 if no bits are
       set, 32 if MSB is set, 1 if LSB is set. Order of bits is 
       again from right to left (0 - 31).
     */

    UINT1               i;
    UINT4               u4Temp = 0x80000000L;

    for (i = 0; i < 32; i++, u4Temp >>= 1)
        if (u4Temp & u4Number)
        {
            return (32 - i);
        }

    /* None of the bits is set - return 0 */
    return (0);

}

/***************************************************************/
/*  Function Name   : CRU_BML_Print_AllBits_In32BInt           */
/*  Description     : Prints the various bits of the 32 bit    */
/*                    number 'u4Number' - ordering is from     */
/*                    right to left (0 - 31)                   */
/*  Input(s)        : u4Number  -  32bit integer               */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
CRU_BML_Print_AllBits_In32BInt (UINT4 u4Number)
#else
VOID
CRU_BML_Print_AllBits_In32BInt (u4Number)
     UINT4               u4Number;
#endif /* __STDC__ */
{
    INT4                i;

    for (i = 31; i >= 0; i--)
    {
        if (CRU_BML_Check_Bit_In32BInt (u4Number, (UINT1) i))
            UtlTrcLog (1, 1, "BITL", "1");
        else
            UtlTrcLog (1, 1, "BITL", "0");
    }
    UtlTrcLog (1, 1, "BITL", " ");

}

/***************************************************************/
/*  Function Name   : CRU_BML_Set_Bit_InArray                  */
/*  Description     : The array 'pu1Data' is specified as a    */
/*                    sequence of bits (size of the bit array  */
/*                    is always divisible by 32 - i.e. minimum */
/*                    size bit array should be 32 or 4 bytes). */
/*                    The bit array is specified from left to  */
/*                    right as a sequence of bits in memory.   */
/*                    For ex. the following organisation of a  */
/*                    4 byte value in                          */
/*                            0x0A                             */
/*                            0x0B                             */
/*                            0x0C                             */
/*                            0x0D                             */
/*                    memory is 0xDCBA in 4 byte integer terms,*/
/*                    and the following bit organisation in bit*/
/*                    array terms:                             */
/*                    Number     Bit #s in Bit Array           */
/*                    0x00001010        0 - 7  (left to right) */
/*                    0x00001011        8 - 15 (left to right) */
/*                    0x00001100        16 - 23(left to right) */
/*                    0x00001101        24 - 31(left to right) */
/*                    This function set the bit specified by   */
/*                    'u2BitNumber' in the bit array pointed to*/
/*                    by 'pu1Data'. Size of the array          */
/*                    (specified by 'u2BitArraySize') must     */
/*                    always be a multiple of 4 bytes          */
/*  Input(s)        : pu1Data       - Pointer to the bit array */
/*                    u2BitArraySize - Size of Bit Array(bytes)*/
/*                    u2BitNumber     -  Bit to be SET         */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
CRU_BML_Set_Bit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize,
                         UINT2 u2BitNumber)
#else
VOID
CRU_BML_Set_Bit_InArray (pu1Data, u2BitArraySize, u2BitNumber)
     UINT1              *pu1Data;
     UINT2               u2BitArraySize;    /* in bytes */
     UINT2               u2BitNumber;    /* Bit to set */
#endif /* __STDC__ */
{
    UINT2               u2Temp1;
    UINT1               u1Temp;

    /* Unused Param. */
    u2BitArraySize = u2BitArraySize;

    u2Temp1 = u2BitNumber / 32;
    u1Temp = u2BitNumber % 32;

    UtlTrcLog (1, 1, "BITL", "u1Temp = %d\n", u1Temp);
    UtlTrcLog (1, 1, "BITL", "Bit to set = %d\n",
               (8 * (u1Temp / 8)) + (7 - (u1Temp % 8)));
    CRU_BML_Set_Bit_In32BInt ((UINT4 *) ((VOID *) pu1Data + u2Temp1),
                              (UINT1) ((8 * (u1Temp / 8)) +
                                       (7 - (u1Temp % 8))));

}

/***************************************************************/
/*  Function Name   : CRU_BML_Clear_Bit_InArray                */
/*  Description     : This function clears the bit specified by*/
/*                    'u2BitNumber' in the bit array pointed to*/
/*                    by 'pu1Data'. The size of the array      */
/*                    (specified by 'u2BitArraySize') must     */
/*                    always be a multiple of 4 bytes          */
/*  Input(s)        : pu1Data          - Pointer to bit array  */
/*                    u2BitArraySize  -  Size of the Bit Array */
/*                                        in bytes             */
/*                    u2BitNumber -  Bit position to be cleared*/
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
CRU_BML_Clear_Bit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize,
                           UINT2 u2BitNumber)
#else
VOID
CRU_BML_Clear_Bit_InArray (pu1Data, u2BitArraySize, u2BitNumber)
     UINT1              *pu1Data;
     UINT2               u2BitArraySize;    /* in bytes */
     UINT2               u2BitNumber;    /* Bit to clear */
#endif /* __STDC__ */
{
    UINT2               u2Temp1;
    UINT1               u1Temp;

    /* Unused Param. */
    u2BitArraySize = u2BitArraySize;

    u2Temp1 = u2BitNumber / 32;
    u1Temp = u2BitNumber % 32;

    CRU_BML_Clear_Bit_In32BInt ((UINT4 *) ((VOID *) pu1Data + u2Temp1),
                                (UINT1) ((8 * (u1Temp / 8)) +
                                         (7 - (u1Temp % 8))));
}

/***************************************************************/
/*  Function Name   : CRU_BML_Check_Bit_InArray                */
/*  Description     : This function check the bit specified by */
/*                    'u2BitNumber' in the bit array pointed to*/
/*                    by 'pu1Data'. The size of the array      */
/*                    (specified by 'u2BitArraySize') must     */
/*                    always be a multiple of 4 bytes          */
/*  Input(s)        : pu1Data           - Pointer to bit array */
/*                    u2BitArraySize - Size of the Bit Array   */
/*                                      in bytes               */
/*                    u2BitNumber  - Bit position to be checked*/
/*  Output(s)       : None                                     */
/*  Returns         : 1 -  if the bit is set,                  */
/*                    0 - if the bit is not set                */
/***************************************************************/
#ifdef __STDC__
UINT1
CRU_BML_Check_Bit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize,
                           UINT2 u2BitNumber)
#else
UINT1
CRU_BML_Check_Bit_InArray (pu1Data, u2BitArraySize, u2BitNumber)
     UINT1              *pu1Data;
     UINT2               u2BitArraySize;    /* in bytes */
     UINT2               u2BitNumber;    /* Bit to check */
#endif /* __STDC__ */
{
    UINT2               u2Temp1;
    UINT1               u1Temp;

    /* Unused Param. */
    u2BitArraySize = u2BitArraySize;

    u2Temp1 = u2BitNumber / 32;
    u1Temp = u2BitNumber % 32;

    return (CRU_BML_Check_Bit_In32BInt
            (*((UINT4 *) ((VOID *) pu1Data + u2Temp1)),
             (UINT1) ((8 * (u1Temp / 8)) + (7 - (u1Temp % 8)))));
}

/***************************************************************/
/*  Function Name   : CRU_BML_Find_FirstSetBit_InArray         */
/*  Descripthon     : This function finds the bit position     */
/*                    (left to right) of the first set bit in  */
/*                    the bit array pointed to by 'pu1Data'.   */
/*                    The size of the array (specified by      */
/*                    'u2BitArraySize') must always be a       */
/*                    multiple of 4 bytes                      */
/*  Input(s)        : pu1Data        - Pointer to the bit array*/
/*                    u2BitArraySize - Size of the Bit Array   */
/*                                      in bytes               */
/*  Output(s)       : None                                     */
/*  Returns         : 0  if no bits are set,                   */
/*                    Bit position which has been set starting */
/*                    from 1                                   */
/***************************************************************/
#ifdef __STDC__
UINT2
CRU_BML_Find_FirstSetBit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize)
#else
UINT2
CRU_BML_Find_FirstSetBit_InArray (pu1Data, u2BitArraySize)
     UINT1              *pu1Data;
     UINT2               u2BitArraySize;    /* in bytes */
#endif /* __STDC__ */
{
    UINT2               u2Temp1;
    UINT1               u1Temp;

    for (u2Temp1 = 0; u2Temp1 < u2BitArraySize; u2Temp1 += 4)
    {

        if ((u1Temp =
             CRU_BML_Find_SetBit_In32BInt (*
                                           ((UINT4
                                             *) ((VOID *) (pu1Data +
                                                           u2Temp1))))) != 0)
        {
            return ((32 - u1Temp) + (32 * u2Temp1) + 1);
        }
    }

    return (0);
}

/***************************************************************/
/*  Function Name   : CRU_BML_Find_NextSetBit_InArray          */
/*  Description     : This function determines the position of */
/*                    the next set bit in the bit array pointed*/
/*                    to by 'pu1Data'. Position of currently   */
/*                    set bit (i.e. after which the search     */
/*                    should start is specified by             */
/*                    'u2CurrentlySetBit').  Bit positioning is*/
/*                    again from left to right. The size of the*/
/*                    array (specified by 'u2BitArraySize')must*/
/*                    always be a multiple of 4 bytes          */
/*  Input(s)        : pu1Data         - Pointer to bit array   */
/*                    u2BitArraySize  - Size of the Bit Array  */
/*                                       in bytes              */
/*                    u2CurrentlySetBit - Bit position of the  */
/*                         currently set bit in the Bit Array  */
/*  Output(s)       : None                                     */
/*  Returns         : 0  if no bits are set,                   */
/*                    Bit position which has been set starting */
/*                    from 'u2CurrenlytSetBit'                 */
/***************************************************************/
#ifdef __STDC__
UINT2
CRU_BML_Find_NextSetBit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize,
                                 UINT2 u2CurrentlySetBit)
#else
UINT2
CRU_BML_Find_NextSetBit_InArray (pu1Data, u2BitArraySize, u2CurrentlySetBit)
     UINT1              *pu1Data;
     UINT2               u2BitArraySize;    /* in bytes */
     UINT2               u2CurrentlySetBit;
#endif /* __STDC__ */
{

    UINT2               u2Temp1 = 0, u2Temp2 = 0;
    UINT4               u4Temp;
    UINT2               u2count = u2BitArraySize / 4;
    UINT1               u1Temp1;

    if (u2CurrentlySetBit != 0)
    {
        u2Temp1 = u2CurrentlySetBit / 32;
        u1Temp1 = u2CurrentlySetBit % 32;
        u4Temp = OSIX_NTOHL (*((UINT4 *) ((VOID *) pu1Data + u2Temp1)));
        u4Temp &= ~(0xFFFFFFFFL << (32 - u1Temp1));
        if ((u2Temp2 =
             CRU_BML_Find_FirstSetBit_InArray ((UINT1 *) &u4Temp, 4)) != 0)
            return (u2Temp2 + (32 * u2Temp1));
        if (u1Temp1 != 0)
            u2Temp1++;
    }
    else
    {
        u2Temp1 = u2Temp2 = 0;
    }
    for (; u2Temp1 < u2count; u2Temp1++)
    {
        u4Temp = *((UINT4 *) ((VOID *) pu1Data + u2Temp1));
        u4Temp = OSIX_NTOHL (u4Temp);
        if ((u2Temp2 =
             CRU_BML_Find_FirstSetBit_InArray ((UINT1 *) &u4Temp, 4)) != 0)
            return (u2Temp2 + (32 * u2Temp1));
    }

    return (0);

}

/***************************************************************/
/*  Function Name   : CRU_BML_Print_AllBits_InArray            */
/*  Description     : This function prints the bits of the     */
/*                    specified bit array from left to right   */
/*  Input(s)        : pu1Data        - Pointer to the bit array*/
/*                    u2BitArraySize - Size of the Bit Array in*/
/*                                      bytes                  */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
CRU_BML_Print_AllBits_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize)
#else
VOID
CRU_BML_Print_AllBits_InArray (pu1Data, u2BitArraySize)
     UINT1              *pu1Data;
     UINT2               u2BitArraySize;
#endif /* __STDC__ */
{

    /* Prints the bits of the specified bit array from left to 
       right.
     */
    UINT2               u2Temp1, u2Temp2, u2CurrentlySetBit;

    u2Temp2 = u2CurrentlySetBit = 0;
    while ((u2Temp1 = CRU_BML_Find_NextSetBit_InArray (pu1Data,
                                                       u2BitArraySize,
                                                       u2CurrentlySetBit)))
    {
        for (; u2Temp2 < u2Temp1; u2Temp2++)
            UtlTrcLog (1, 1, "BITL", "0");
        UtlTrcLog (1, 1, "BITL", "1");
        u2CurrentlySetBit = u2Temp1;
        u2Temp2 = u2Temp1 + 1;
    }

    for (; u2Temp2 < (4 * u2BitArraySize); u2Temp2++)
        UtlTrcLog (1, 1, "BITL", "0");

    UtlTrcLog (1, 1, "BITL", " ");
}
