/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmbuf.h,v 1.17 2015/04/28 12:00:00 siva Exp $
 *
 * Description: SRM Buf module's exported file.
 *
 */

#ifndef  _SRMBUF_H
#define  _SRMBUF_H
#include "fsap.h"
/****************************************************
************* Defined constants  ********************
*****************************************************/
#define   CRU_SUCCESS    0
#define   CRU_FAILURE  (-1)
#define   CRU_MAX_MODINFO_LEN 12

#define   CRU_BUF_GetFirstDataDesc(pBuf)        (pBuf->pFirstValidDataDesc)
#define   CRU_BUF_GetNextDataDesc(pDD)          (pDD->pNext)
#define   CRU_BUF_GetDataPtr(pDD)               (pDD->pu1_FirstValidByte)
#define   CRU_BUF_GetBlockValidByteCount(pDD)   (pDD->u4_ValidByteCount)

/*********************************************************
*** Type definition for Buffer related Data Structures ***
**********************************************************/

/**** Type Definition for Data Buffer Descriptor *****/
typedef struct CRU_BUF_DATA_DESC{

        struct CRU_BUF_DATA_DESC *pNext;  /* Pointer to the
                                           * next Data Desc */
        struct CRU_BUF_DATA_DESC *pPrev;  /* Pointer to the
                                           * previous Data Desc */

        UINT1  *pu1_FirstByte;          /* Starting of data -
                                         * Specifically used
                                         * in case of fragmentation */
        UINT1  *pu1_FirstValidByte;     /* Start of first valid Byte */
        UINT4  u4_ValidByteCount;       /* Number of Valid bytes in
                                         * this data descriptor and
                                         * data block.  */
        UINT4  u4_FreeByteCount;        /* Number of free bytes present
                                         * in this data block.  */
        UINT2  u2_UsageCount;           /* Reference count for this
                                         * data block */
        UINT2  u2_QueId;                /* Memory pool Id to which
                                         * this object belongs to.  */
}tCRU_BUF_DATA_DESC;

/*** Type Definition for Inter-Module Data Exchange ***/
typedef struct CRU_INTERFACE {
      UINT4  u4IfIndex;
      UINT2  u2_SubReferenceNum;
      UINT1  u1_InterfaceType;
      UINT1  u1_InterfaceNum;
}tCRU_INTERFACE;


typedef struct MODULE_DATA{
    UINT1           u1_SourceModuleId;
    UINT1           u1_DestinModuleId;
    UINT2           u2Reserved;            /* added for packing */
    tCRU_INTERFACE  InterfaceId;
    UINT4           u4Reserved1;
    UINT4           u4Reserved2;
    FS_ULONG        u4Reserved3; 
    DECL_FLOWDATA()
 /* au1Reserved member is added for security module data */
 UINT1           au1ModuleInfo[CRU_MAX_MODINFO_LEN];
}tMODULE_DATA;


/***** Type Definition for Buffer Chain Descriptor *****/
typedef struct CRU_BUF_CHAIN_DESC{
        struct CRU_BUF_CHAIN_DESC *pNextChain;   /* Next Message in the chain */
        tCRU_BUF_DATA_DESC *pFirstDataDesc;      /* First Data Descriptor of
                                                  * this message */
        tCRU_BUF_DATA_DESC *pLastDataDesc;       /* Last Data Descriptor of
                                                  * this message */
        tCRU_BUF_DATA_DESC *pFirstValidDataDesc; /* Data Descriptor where the
                                                  * valid data starts. */
        tMODULE_DATA       ModuleData;           /* Module Related data */

        UINT4              u4SystemData;         /* SRM Buf private data */
        UINT4              u4TimeStamp;          /* Time of allocn.      */

}tCRU_BUF_CHAIN_DESC;

typedef tCRU_BUF_CHAIN_DESC  tCRU_BUF_CHAIN_HEADER;


enum {buf_block_free = 0, buf_block_allocated};
#define BUF_SYSTEM_DATA(pMsg) (pMsg->u4SystemData)
#define BUF_TIMESTAMP(pMg)    (pMsg->u4TimeStamp)



/*** Type Definition of Buffer Manager Configuration Parameters ***/
typedef struct DataBlockCfg {

   UINT4 u4BlockSize;   /* size of data block.
                         * Min size 128 bytes Max 4096 bytes
                         * Should be in multiples of 4 */
   UINT4 u4NoofBlocks;  /* Max number of blocks of u4BlockSize */

}tDataBlockCfg;

typedef struct BufConfig {
   UINT4 u4MemoryType;           /* TYPE_0 or TYPE_1 or etc as
                                  * defined by Memory Pool Manager
                                  * default is MEM_DEFAULT_MEM_TYPE */
   UINT4 u4MaxChainDesc;         /* Max # of chain descriptors  */
   UINT4 u4MaxDataBlockCfg;      /* Array size of DataBlkCfg */
   tDataBlockCfg DataBlkCfg[1];  /*details of data block */
}tBufConfig;




/*********************************************************
*************** MACRO definition *************************
**********************************************************/

/******************************************************
*** Macro To Get The Pointer To Message Information ***
*** Given The Message Buffer Chain Descriptor       ***
*******************************************************/
#define CRU_BUF_Get_ModuleData(pBufChain) (&(pBufChain)->ModuleData)

/*************************************************************
*** MACROS TO EXTRACT THE FIELDS FROM THE MODULE-DATA AREA ***
**************************************************************/
#define CRU_BUF_Get_SourceModuleId(pBufChain) \
        ((pBufChain)->ModuleData.u1_SourceModuleId)

#define CRU_BUF_Get_DestinModuleId(pBufChain) \
       ((pBufChain)->ModuleData.u1_DestinModuleId)

#define CRU_BUF_Get_InterfaceId(pBufChain) \
       ((pBufChain)->ModuleData.InterfaceId)

#define CRU_BUF_Get_U2Reserved(pBufChain) \
        ((pBufChain)->ModuleData.u2Reserved)

#define CRU_BUF_Get_U4Reserved1(pBufChain) \
        ((pBufChain)->ModuleData.u4Reserved1)

#define CRU_BUF_Get_U4Reserved2(pBufChain) \
        ((pBufChain)->ModuleData.u4Reserved2)

#define CRU_BUF_Get_U4Reserved3(pBufChain) \
        ((pBufChain)->ModuleData.u4Reserved3)

/********************************************************
*** MACROS TO FILL THE FIELDS IN THE MODULE-DATA AREA ***
*********************************************************/
#define CRU_BUF_Set_SourceModuleId(pBufChain,u1ModuleId) \
        ((pBufChain)->ModuleData.u1_SourceModuleId = (u1ModuleId))

#define CRU_BUF_Set_DestinModuleId(pBufChain,u1ModuleId) \
        ((pBufChain)->ModuleData.u1_DestinModuleId = (u1ModuleId))

#define CRU_BUF_Set_InterfaceId(pBufChain,IfIdStruct) \
        ((pBufChain)->ModuleData.InterfaceId = (IfIdStruct))

#define CRU_BUF_Set_U2Reserved(pBufChain,u2Resv) \
        ((pBufChain)->ModuleData.u2Reserved = (u2Resv))

#define CRU_BUF_Set_U4Reserved1(pBufChain,u4Resv1) \
        ((pBufChain)->ModuleData.u4Reserved1 = (u4Resv1))

#define CRU_BUF_Set_U4Reserved2(pBufChain,u4Resv2) \
        ((pBufChain)->ModuleData.u4Reserved2 = (u4Resv2))

#define CRU_BUF_Set_U4Reserved3(pBufChain,u4Resv3) \
        ((pBufChain)->ModuleData.u4Reserved3 = (u4Resv3))

/*******************************************************
**** Easy Macros For Interface Structure Components ****
********************************************************/
#define CRU_BUF_Get_Interface_Type(tInterface)  ((tInterface).u1_InterfaceType)
#define CRU_BUF_Get_Interface_Num(tInterface)   ((tInterface).u1_InterfaceNum)
#define CRU_BUF_Get_Interface_SubRef(tInterface) ((tInterface).u2_SubReferenceNum)
#define CRU_BUF_Get_IfIndex(tInterface)         ((tInterface).u4IfIndex)

#define CRU_BUF_Set_Interface_Type(tInterface, u1IfType)\
           ((tInterface).u1_InterfaceType = u1IfType)

#define CRU_BUF_Set_Interface_Num(tInterface, u1IfNum) \
           ((tInterface).u1_InterfaceNum = u1IfNum)

#define CRU_BUF_Set_Interface_SubRef(tInterface, u2IfSubRef) \
           ((tInterface).u2_SubReferenceNum = u2IfSubRef)

        /* Set both the old and new fields which hold the i/f index
         * Upon migration to the u4IfIndex field, remove IfNum.
         */
#define CRU_BUF_Set_IfIndex(tInterface, u4IfNum)            \
           ((tInterface).u1_InterfaceNum = (tInterface).u4IfIndex =\
            (UINT1)u4IfNum)

/*******************************************************
*** MACROs For Host To Network Byte Order Conversion ***
********************************************************/
#define CRU_BMC_WTOPDU(pu1PduAddr,u2Value) \
        *((UINT2 *)(pu1PduAddr)) = OSIX_HTONS(u2Value);

#define CRU_BMC_DWTOPDU(pu1PduAddr,u4Value) \
        *((UINT4 *)(pu1PduAddr)) = OSIX_HTONL(u4Value);

/*******************************************************
*** MACROs For Network To Host Byte Order Conversion ***
********************************************************/
#define CRU_BMC_WFROMPDU(pu1PduAddr) \
        OSIX_NTOHS(*((UINT2 *)((VOID *)pu1PduAddr)))

#define CRU_BMC_DWFROMPDU(pu1PduAddr) \
        OSIX_NTOHL(*((UINT4 *)((VOID *)pu1PduAddr)))

/*******************************************************
*** Macro To Get The Pointer To The First Valid Data ***
*** Byte In the Message Buffer-Chain.                ***
********************************************************/
#define CRU_BMC_Get_DataPointer(pBufChain) \
        ((pBufChain)->pFirstValidDataDesc->pu1_FirstValidByte)


/* SRM BUF Extension calls used by other protocols */
/*
 * Macros to access CRU BUF fields
 */

#define   CB_FDD(pMsg)            (pMsg->pFirstValidDataDesc)          
#define   CB_FB(pMsg)             ((CB_FDD(pMsg))->pu1_FirstByte)      
#define   CB_FVB(pMsg)            ((CB_FDD(pMsg))->pu1_FirstValidByte) 
#define   CB_VBC(pMsg)            (CB_FDD(pMsg)->u4_ValidByteCount)    
#define   CB_FBC(pMsg)            (CB_FDD(pMsg)->u4_FreeByteCount)     
#define   CB_READ_OFFSET(pMsg)    (pMsg->ModuleData.u4Reserved1)       
#define   CB_WRITE_OFFSET(pMsg)   (pMsg->ModuleData.u4Reserved2)       

/*
 * ASSIGN
 * updates VBC, FBC
 * does OSIX_HTON
 */
#define ASSIGN_1_BYTE(pMsg, u4Offset, u1Val)            \
do {                                                    \
    UINT4 u4OldVbc;                                     \
    UINT4 u4Added;                                      \
    *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = u1Val; \
    u4OldVbc = CB_VBC(pMsg);                            \
    if(u4Offset >= u4OldVbc) {                          \
        u4Added       = (u4Offset - u4OldVbc) + 1;      \
        CB_VBC(pMsg) += u4Added;                        \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;         \
   }                                                    \
}while(0)


#define ASSIGN_2_BYTE(pMsg, u4Offset, u2Val)                            \
do {                                                                    \
        UINT4 u4OldVbc;                                                 \
        UINT4 u4Added;                                                  \
        *(UINT2 *)(VOID *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = OSIX_HTONS(u2Val); \
        u4OldVbc = CB_VBC(pMsg);                                        \
        if(u4Offset >= u4OldVbc) {                                      \
            u4Added       = (u4Offset - u4OldVbc) + 2;                  \
            CB_VBC(pMsg) += u4Added;                                    \
            CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                     \
   }                                                                    \
}while(0)

#define ASSIGN_4_BYTE(pMsg, u4Offset, u4Val)                        \
do {                                                                \
    UINT4 u4OldVbc;                                                 \
    UINT4 u4Added;                                                  \
    *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+u4Offset) = OSIX_HTONL(u4Val); \
    u4OldVbc = CB_VBC(pMsg);                                        \
    if(u4Offset >= u4OldVbc) {                                      \
        u4Added       = (u4Offset - u4OldVbc) + 4;                  \
        CB_VBC(pMsg) += u4Added;                                    \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                     \
   }                                                                \
}while(0)

#define ASSIGN_N_BYTE(pMsg, pu1Src, u4Offset, u4Len)              \
do{                                                               \
    UINT4 u4OldVbc;                                               \
    UINT4 u4Added;                                                \
    MEMCPY((VOID *)CB_FVB(pMsg)+u4Offset, (VOID *)pu1Src, u4Len); \
    u4OldVbc = CB_VBC(pMsg);                                      \
    if(u4Offset >= u4OldVbc) {                                    \
        u4Added       = (u4Offset - u4OldVbc) + u4Len;            \
        CB_VBC(pMsg) += u4Added;                                  \
        CB_FBC(pMsg)  = CB_FBC(pMsg) - u4Added;                   \
    }                                                             \
}while(0)

/*
 * APPEND
 * updates VBC, FBC
 * does OSIX_HTON
 */

#define APPEND_1_BYTE(pMsg, u1Val)                                   \
do {                                                                 \
    *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = u1Val; \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {                      \
        CB_VBC(pMsg) ++;                                             \
        CB_FBC(pMsg) --;                                             \
   }                                                                 \
}while(0)

#define APPEND_2_BYTE(pMsg, u2Val)                                \
do {                                                              \
        *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = \
            OSIX_HTONS(u2Val);                                    \
        if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {               \
            CB_VBC(pMsg) += 2;                                    \
            CB_FBC(pMsg) -= 2;                                    \
   }                                                              \
}while(0)

#define APPEND_4_BYTE(pMsg, u4Val)                            \
do{                                                           \
    *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)) = \
        OSIX_HTONL(u4Val);                                    \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {               \
        CB_VBC(pMsg) += 4;                                    \
        CB_FBC(pMsg) -= 4;                                    \
    }                                                         \
}while(0)

#define APPEND_N_BYTE(pMsg, pu1Src, u4Len)                        \
do{                                                               \
    MEMCPY((VOID *)((UINT1 *)CB_FVB(pMsg)+CB_WRITE_OFFSET(pMsg)), \
           (VOID *)pu1Src, u4Len);                                \
    if(CB_WRITE_OFFSET(pMsg) >= CB_VBC(pMsg)) {                   \
        CB_VBC(pMsg) += u4Len;                                    \
        CB_FBC(pMsg) -= u4Len;                                    \
    }                                                             \
}while(0)


/*
 * GET
 * does OSIX_NTOH.
 * no changes to state fields.
 */
#define GET_1_BYTE(pMsg, u4Offset, u1Val)\
        u1Val = *(UINT1 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset)

#define GET_2_BYTE(pMsg, u4Offset, u2Val)                     \
do{                                                           \
        u2Val = *(UINT2 *)(VOID *)((UINT1 *)(CB_FVB(pMsg))+u4Offset); \
        u2Val = OSIX_NTOHS((u2Val));                            \
}while(0)

#define GET_4_BYTE(pMsg, u4Offset, u4Val)                     \
do{                                                           \
        u4Val = *(UINT4 *)((UINT1 *)(CB_FVB(pMsg))+u4Offset); \
        u4Val = OSIX_NTOHL(u4Val);                            \
}while(0)


#define GET_N_BYTE(pMsg, pu1Dest, u4Offset, u4Len)              \
        MEMCPY((VOID *)pu1Dest,                                 \
               (const VOID *)((UINT1 *)CB_FVB(pMsg)+u4Offset),  \
               u4Len)


/*
 * EXTRACT
 *
 * CAVEAT:
 * Users *SHOULD* update readoffset, for true fsap1 behaviour
 */

#define EXTRACT_1_BYTE(pMsg, u1Val) \
        u1Val = *(UINT1 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg));

#define EXTRACT_2_BYTE(pMsg, u2Val)                                  \
do{                                                                  \
    u2Val = *(UINT2 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)) ; \
    u2Val = OSIX_NTOHS(u2Val);                                       \
}while(0)


#define EXTRACT_4_BYTE(pMsg, u4Val)                                  \
do{                                                                  \
    u4Val = *(UINT4 *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)) ; \
    u4Val = OSIX_NTOHL(u4Val);                                       \
}while(0)

#define EXTRACT_N_BYTE(pMsg, pu1Dest, u4Len)                            \
do{                                                                     \
    MEMCPY((VOID *)pu1Dest,                                             \
    (const VOID *)((UINT1 *)CB_FVB(pMsg)+CB_READ_OFFSET(pMsg)), u4Len); \
}while(0)

#define CB_IS_LINEAR_READ(pBuf, u4Offset, u4Size)                \
    ((CRU_BUF_MoveToReadOffset(pBuf, u4Offset, &pTmpSrcDataDesc, \
                               &pu1SrcData) == CRU_SUCCESS) ? 1 : 0)

/* Returns true if pBuf has space for u4Size bytes of data starting from offset u4Offset.
*/
#define CB_IS_LINEAR_WRITE(pBuf, u4Offset, u4Size)          \
((CRU_BUF_MoveToWriteOffset(pBuf,u4Offset,&pTmpDstDataDesc, \
                            &pu1DstData,u4Size) == CRU_SUCCESS) ? 1 : 0)




/*********************************************************
***********     Prototype  Declaration  ******************
**********************************************************/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

INT4 BufInitManager ARG_LIST((tBufConfig *pBufLibInitData));

INT4 CRU_BUF_Shutdown_Manager ARG_LIST((VOID));
#ifdef MEMTRACE_WANTED
#define CRU_BUF_Allocate_MsgBufChain(u4Size,u4Offset) \
     CRU_BUF_Allocate_MsgBufChain_memtrace\
        (u4Size,u4Offset,__FILE__,__LINE__,__PRETTY_FUNCTION__)
#define CRU_BUF_Duplicate_BufChain(pBuf) \
     CRU_BUF_Duplicate_BufChain_memtrace\
        (pBuf,__FILE__,__LINE__,__PRETTY_FUNCTION__)

tCRU_BUF_CHAIN_HEADER *CRU_BUF_Duplicate_BufChain_memtrace  
   ARG_LIST((tCRU_BUF_CHAIN_HEADER *,const CHR1 *,INT4,const CHR1 *));

tCRU_BUF_CHAIN_HEADER *CRU_BUF_Allocate_MsgBufChain_memtrace
   ARG_LIST((UINT4,UINT4,const CHR1 *,INT4,const CHR1 *));
#else
tCRU_BUF_CHAIN_HEADER *CRU_BUF_Allocate_MsgBufChain ARG_LIST((UINT4 u4Size,UINT4 u4ValidOffsetValue));

tCRU_BUF_CHAIN_HEADER *CRU_BUF_Duplicate_BufChain ARG_LIST((tCRU_BUF_CHAIN_HEADER *pChainDesc));
#endif
INT4 CRU_BUF_Release_MsgBufChain ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain,UINT1 u1ForcedRelease));

INT4 CRU_BUF_Copy_OverBufChain ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain,UINT1 *pu1Source,UINT4 u4Offset,UINT4 u4Size));

INT4 CRU_BUF_Copy_OverBufChain_AtEnd ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain,
                                               UINT1 *pu1Source,UINT4 u4Offset,
                                               UINT4 u4Size));

INT4 CRU_BUF_Copy_FromBufChain ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain,UINT1 *pu1Dst,UINT4 u4Offset,UINT4 u4Size));

VOID CRU_BUF_Concat_MsgBufChains ARG_LIST((tCRU_BUF_CHAIN_HEADER *pChainDesc1,tCRU_BUF_CHAIN_HEADER *pChainDesc2));

INT4 CRU_BUF_Fragment_BufChain ARG_LIST((tCRU_BUF_CHAIN_HEADER *pChainDesc,UINT4 u4Size,tCRU_BUF_CHAIN_HEADER **ppFragChainDescPtr));

INT4 CRU_BUF_Prepend_BufChain ARG_LIST((tCRU_BUF_CHAIN_HEADER *pChainDesc,UINT1 *pu1Src,UINT4 u4Size));

INT4  CRU_BUF_Link_BufChains ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain1, tCRU_BUF_CHAIN_HEADER *pBufChain2));

tCRU_BUF_CHAIN_HEADER *CRU_BUF_UnLink_BufChain(tCRU_BUF_CHAIN_HEADER *pBufChain);

UINT4 CRU_BUF_Get_ChainValidByteCount ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain));

tCRU_BUF_CHAIN_HEADER *CRU_BUF_Delete_BufChainAtEnd ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain,UINT4 u4Size));

INT4 CRU_BUF_Move_ValidOffset ARG_LIST((tCRU_BUF_CHAIN_HEADER *pBufChain,UINT4 u4Units));

UINT1 *CRU_BUF_Get_DataPtr_IfLinear(tCRU_BUF_CHAIN_HEADER *pChainDesc, UINT4 u4Offset, UINT4 u4Units);

VOID CRU_BUF_Print_AllStats ARG_LIST((VOID));

VOID UtlDmpMsg   ARG_LIST ((UINT4 u4CurType, UINT4 u4CurDirn,
                            tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Type,
                            UINT4 u4Dirn, UINT4 u4Len, UINT4 u4HdrReqd));

INT4
    CRU_BUF_Copy_BufChains (tCRU_BUF_CHAIN_HEADER *pDstChainDesc,
                                                 tCRU_BUF_CHAIN_HEADER *pSrcChainDesc, UINT4 u4DstOffset,
                                                                      UINT4 u4SrcOffset, UINT4 u4Size);


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif  /* _SRMBUF_H */
