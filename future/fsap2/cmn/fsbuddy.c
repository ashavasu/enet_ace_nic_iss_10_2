/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsbuddy.c,v 1.8 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Smart Buddy Implementation for variable sized memory
 *              allocation.
 ******************************************************************************/

#include "osxstd.h"
#include "fsbuddy.h"
#include "osxinc.h"

/* Added for 64-bit Buddy Alignment */
#define MEM_ALIGN  ((~0UL) & ~(FS_ULONG)(sizeof(FS_ULONG) - 1))
#define MEM_ALIGN_BYTE (sizeof(FS_ULONG) - 1)
enum
{ BUDDY_INACTIVE = 0, BUDDY_ACTIVE = 1 };
enum
{ BUDDY_TRUE = 0, BUDDY_FALSE = 1 };

#define BUDDY_INCR_BUF(i,buf,pos)\
    (((gBuddyTable[i].u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF) ? (buf + pos) : (buf + 4 + pos))

#define BUDDY_BIT_OFFSET(i,x,y)\
    (2 *  ((y - x - gBuddyTable[i].u2HdrSize) / \
        gBuddyTable[i].u4MinBlkSize))

#define BUDDY_START_BYTE(x)  ((x)/8)

#define BUDDY_START_BIT(x)   ((x) % 8)
#define BUDDY_COMPARE_BUFFS(i,x,y) \
    (((FS_ULONG)(y) - (FS_ULONG)(x)) <= gBuddyTable[i].u4MaxBlkSize)  ? 0 : 1

#define BUDDY_GET_FREE_LEFT_BLKS(u1Pat) \
        (((u1Pat & 0x01) == 1) ? 0 : \
        (((u1Pat & 0x04) == 0x04) ? 1 :\
        ((((u1Pat & 0x10) == 0x10) ? 2 : \
        (((u1Pat & 0x40) == 0x40) ? 3 : \
        ((u1Pat == 0) ? 4 : 0))))))

#define BUDDY_GET_FREE_RIGHT_BLKS(u1Pat) \
        (((u1Pat == 1) || (u1Pat == 0)) ? 4 : \
        (((u1Pat & 0x40) == 0x40) ? 1 :\
        ((((u1Pat & 0x10) == 0x10) ? 2 : \
        (((u1Pat & 0x04) == 0x04) ? 3 : 0)))))

typedef struct _BuddyBuf
{
    struct _BuddyBuf   *pNext;
    UINT1               au1Data[4];
}
tBuddyBuf;

/*
 * u4MaxBlkSize - The max block size that can be allocated to the appln.
 * u4MinBlkSize - The min ...
 * pBuddyBuf    - Pointer to list of buddy buffers.
 * pu1FreeQ     - Pointer to free buddy blocks of different sizes.
 * u4NumBlks    - Number of buddy bufs of size u4MaxBlkSize.
 * u2HdrSize    - Size of header in buddy buf.
 * u1BuddyStatus- Status of this buddy instance.
 * u1CFlag      - Whether buffers are contiguous or not.
 */
typedef struct _BuddyTable
{
    UINT4               u4MaxBlkSize;
    UINT4               u4MinBlkSize;
    UINT4               u4MemAlloc;
    tBuddyBuf          *pBuddyBuf;
    UINT1             **pu1FreeQ;
    UINT4               u4NumBlks;
    UINT2               u2HdrSize;
    UINT1               u1BuddyStatus;
    UINT1               u1CFlag;
}
tBuddyTable;

PRIVATE INT4        BuddyAddIntoBlk (UINT1, UINT1 *, UINT4);
PRIVATE UINT1      *BuddyDeleteFirstBlock (UINT1, UINT4);
PRIVATE INT4        BuddyDeleteInMiddle (UINT1, UINT1 *, UINT4);
PRIVATE VOID        BuddySetACBits (UINT1, UINT1 *, UINT1 *, UINT4);
PRIVATE VOID        BuddyResetACBits (UINT1, UINT1 *, UINT1 *, UINT4 *);
PRIVATE INT4        BuddySetBACBits (UINT1, UINT1 *, UINT1 *, UINT4);
PRIVATE INT4        BuddyTRAndMerge (UINT1, UINT1 *, UINT1 *, UINT4 *);
PRIVATE VOID        BuddyTLAndMerge (UINT1, UINT1 *, UINT1 **, UINT4 *);

PRIVATE tBuddyTable *gBuddyTable;
PRIVATE UINT4       gu4BuddyMaxInstances;
tOsixSemId          gu1BuddySemId;
/*******************************************************************************
 * Function    : MemBuddyInit ()
 * Description : This function initializes the global Buddy table.
 * Input (s)   : None
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred and Modified.
 * Returns     : VOID
 ******************************************************************************/

PUBLIC INT4
MemBuddyInit (UINT4 u4Instances)
{
    UINT1               u1Id = 0;

    if (OsixCreateSem ((const UINT1 *) "BUD1", 1, 0, &gu1BuddySemId) ==
        OSIX_FAILURE)
    {
        return (BUDDY_FAILURE);
    }

    gBuddyTable = MEM_CALLOC (sizeof (tBuddyTable), u4Instances, tBuddyTable);
    if (gBuddyTable == NULL)
        return (BUDDY_FAILURE);

    gu4BuddyMaxInstances = u4Instances;
    for (u1Id = 0; u1Id < u4Instances; u1Id++)
    {
        gBuddyTable[u1Id].u1BuddyStatus = BUDDY_INACTIVE;
        gBuddyTable[u1Id].u4MemAlloc = 0;
        gBuddyTable[u1Id].u4NumBlks = 0;
        gBuddyTable[u1Id].u4MaxBlkSize = 0;
        gBuddyTable[u1Id].u4MinBlkSize = 0;
        gBuddyTable[u1Id].u2HdrSize = 0;
    }
    return (BUDDY_SUCCESS);
}

/*******************************************************************************
 * Function    :  MemBuddyCreate ()
 * Description :  This function allocates the buddy table, initialises all the
 *                buffers and the associated header information.
 * Input (s)   :  u4MaxBlkSize - The maximum block size which can
 *                               be allocated to application
 *                u4MinBlkSize - The minimum block size which can
 *                               be allocated to application
 *                u4NumBlks    - The number of Buddy buffers to be
 *                               allocated.
 * Outputs(s)   : None
 * Globals      : gBuddyTable Referred and Modified
 * Returns      : u1Id                - Identifier of the Buddy, if the
 *                                      initialization is successful
 *                BUDDY_FAILURE       - otherwise.
 ******************************************************************************/

PUBLIC INT4
MemBuddyCreate (UINT4 u4MaxBlkSize, UINT4 u4MinBlkSize, UINT4 u4NumBlks,
                UINT1 u1CFlag)
{
    UINT1               u1Id = 0;
    UINT1               u1Status = BUDDY_FALSE;
    UINT2               u2HdrSize = 0;
    UINT4               u4Cnt = 0;
    UINT4               u4BuddyBufSize = 0;
    tBuddyBuf          *pBuf = NULL;

    if (((u4MaxBlkSize % u4MinBlkSize) != 0) || ((u4MinBlkSize % 4) != 0))
    {
        return (BUDDY_FAILURE);
    }

    OsixSemTake (gu1BuddySemId);
    /* Get the free Buddy Identifier
     */

    for (u1Id = 0; u1Id < gu4BuddyMaxInstances; u1Id++)
    {
        if (gBuddyTable[u1Id].u1BuddyStatus == BUDDY_INACTIVE)
        {
            u1Status = BUDDY_TRUE;
            break;
        }
    }

    if (u1Status == BUDDY_FALSE)
    {
        OsixSemGive (gu1BuddySemId);
        return (BUDDY_FAILURE);
    }

    /* Added for 64-bit Buddy Alignment */
    u4MinBlkSize = (u4MinBlkSize + MEM_ALIGN_BYTE) & MEM_ALIGN;
    u4MaxBlkSize = (u4MaxBlkSize + MEM_ALIGN_BYTE) & MEM_ALIGN;
    /* Initialize the Buddy Table with the sizes specified
     */

    gBuddyTable[u1Id].u1BuddyStatus = BUDDY_ACTIVE;
    gBuddyTable[u1Id].u4MaxBlkSize = u4MaxBlkSize;
    gBuddyTable[u1Id].u4MinBlkSize = u4MinBlkSize;
    gBuddyTable[u1Id].u4NumBlks = u4NumBlks;
    gBuddyTable[u1Id].u4MemAlloc = 0;
    gBuddyTable[u1Id].u1CFlag = u1CFlag;

    /*  Allocate the memory for the Queues
     */

    gBuddyTable[u1Id].pu1FreeQ =
        MEM_CALLOC ((u4MaxBlkSize / u4MinBlkSize), sizeof (VOID *), UINT1 *);

    if (gBuddyTable[u1Id].pu1FreeQ == NULL)
    {
        OsixSemGive (gu1BuddySemId);
        return (BUDDY_FAILURE);
    }

    /* The size of header can be calculated as sum of
     * pointer to next buffer and AC bits
     *
     * pointer to next buffer -  4 Bytes
     * AC bits  = 2 * number of minimum blocks bits
     *          = 2 * (u4MaxBlkSize / u4MinBlkSize) bits
     *          = CEIL (2 * (u4MaxBlkSize / u4MinBlkSize) / 32) * 4 Bytes
     *
     * NOTE: Pointer to Next Buffer is not necessary if entire Buffer is
     * allocated as a single Block
     */

    /* As we dont support non-contiguous memory allocation this check 
     * u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF is done to be true always */
    if ((u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF)
    {
        u2HdrSize = (UINT2)
            (4 * (((((u4MaxBlkSize / u4MinBlkSize) * 2) - 1) / 32) + 1));
    }
    else
    {
/*
// Commented as we dont support non-contiguous memory allocation
        u2HdrSize = (UINT2)
            (4 * (((((u4MaxBlkSize / u4MinBlkSize) * 2) - 1) / 32) + 2));
*/
    }
    gBuddyTable[u1Id].u2HdrSize = u2HdrSize;

    /* The size of buddy buffers to be allocated is sum of MaxBlkSize
     * and header size
     */

    u4BuddyBufSize = u4MaxBlkSize + u2HdrSize;

    /* Allocate the Buddy Buffers and insert them into Max Buddy Block
     * List
     */

    /* As we dont support non-contiguous memory allocation this check 
     * u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF is done to be true always */
    if ((u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF)
    {

        pBuf = MEM_CALLOC ((u4BuddyBufSize * u4NumBlks), sizeof (UINT1),
                           tBuddyBuf);
        if (pBuf == NULL)
        {
            /* if the memory allocation fails, release all the
             * previously allcoated buffers
             */

            OsixSemGive (gu1BuddySemId);
            MemBuddyDestroy (u1Id);
            return (BUDDY_FAILURE);
        }

        gBuddyTable[u1Id].pBuddyBuf = pBuf;

        for (u4Cnt = 0; u4Cnt < u4NumBlks; u4Cnt++)
        {
            /* Ensuring Header Bits are initially set to 0's
             */

            MEMSET (pBuf, 0x00, u2HdrSize);

            BuddyAddIntoBlk (u1Id, ((UINT1 *) pBuf + u2HdrSize), u4MaxBlkSize);
            BuddySetBACBits (u1Id, (UINT1 *) pBuf, ((UINT1 *) pBuf + u2HdrSize),
                             u4MaxBlkSize);
            pBuf = (tBuddyBuf *) ((VOID *) ((UINT1 *) pBuf + u4BuddyBufSize));
        }
    }
    else
    {

/*
// Commented as we dont support non-contiguous memory allocation
        for (u4Cnt = 0; u4Cnt < u4NumBlks; u4Cnt++)
        {
            pBuf = MEM_CALLOC (u4BuddyBufSize, sizeof (UINT1), tBuddyBuf);

            if (pBuf == NULL)
            {
                 // if the memory allocation fails, release all the
                 // previously allcoated buffers
                 //

                MemBuddyDestroy (u1Id);
                return (BUDDY_FAILURE);
            }

            // Ensure Header Bits are initially set to 0's
            

            MEMSET (pBuf, 0x00, (gBuddyTable[u1Id].u2HdrSize));

            // insert the buffer into buffer list and also in the max block list
            

            if (gBuddyTable[u1Id].pBuddyBuf != NULL)
            {
                pBuf->pNext = gBuddyTable[u1Id].pBuddyBuf;
            }
            gBuddyTable[u1Id].pBuddyBuf = pBuf;

            // since the complete buffer is free, the block must be held in the
            // maximum block size queue also
            //

            BuddyAddIntoBlk (u1Id, ((UINT1 *) pBuf + u2HdrSize), u4MaxBlkSize);
            BuddySetBACBits (u1Id, (UINT1 *) pBuf, ((UINT1 *) pBuf + u2HdrSize),
                             u4MaxBlkSize);
        }
*/
    }
    OsixSemGive (gu1BuddySemId);
    return (u1Id);
}

/*******************************************************************************
 * Function    : MemBuddyDestroy ()
 * Description : This function de-initlizes the Buddy table, frees all the
 *               associated resources.
 * Input(s)    : u1Id - The Identifier of Buddy
 * Output(s)   : None
 * Globals     : gBuddyTable Referred, and Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
MemBuddyDestroy (UINT1 u1Id)
{
    tBuddyBuf          *pBuddyBuf = NULL;
/*
// Commented as we currently do not support non-contiguous memory allocation 
//    tBuddyBuf          *pNextBuf = NULL;
*/

    OsixSemTake (gu1BuddySemId);
    if (u1Id >= gu4BuddyMaxInstances)
    {
        /*Out of bound input. reject this */
        OsixSemGive (gu1BuddySemId);
        return;
    }
    gBuddyTable[u1Id].u1BuddyStatus = BUDDY_INACTIVE;
    gBuddyTable[u1Id].u4NumBlks = 0;
    gBuddyTable[u1Id].u4MaxBlkSize = 0;
    gBuddyTable[u1Id].u4MinBlkSize = 0;
    gBuddyTable[u1Id].u2HdrSize = 0;

    if (gBuddyTable[u1Id].pu1FreeQ != NULL)
    {
        MEM_FREE (gBuddyTable[u1Id].pu1FreeQ);
        gBuddyTable[u1Id].pu1FreeQ = NULL;
    }

    pBuddyBuf = gBuddyTable[u1Id].pBuddyBuf;
    gBuddyTable[u1Id].pBuddyBuf = NULL;

    if (((gBuddyTable[u1Id].u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF)
        && (pBuddyBuf != NULL))
    {
        MEM_FREE (pBuddyBuf);
        pBuddyBuf = NULL;
    }
    else
    {
        /* 
           // Commented as we currently do not support non-contiguous memory allocation
           // And else part will only be hit when pBuddyBuf == NULL
           while (pBuddyBuf != NULL)
           {
           pNextBuf = pBuddyBuf->pNext;
           MEM_FREE (pBuddyBuf);
           pBuddyBuf = pNextBuf;
           }
         */
    }
    OsixSemGive (gu1BuddySemId);
}

/*******************************************************************************
 * Function    : MemBuddyAlloc ()
 * Description : This function allocates buddy block to the application.
 * Input(s)    : u1Id   - The Buddy Identifier
 *               u4Size - Size of the block to be allocated
 * Output(s)   : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : Pointer to a buddy block, if available
 *               NULL, Otherwise
 ******************************************************************************/

PUBLIC UINT1       *
MemBuddyAlloc (UINT1 u1Id, UINT4 u4Size)
{
    UINT1              *pu1BuddyBlk = NULL;
    UINT1              *pu1BuddyBuf = NULL;
    UINT1              *pu1FragBlk = NULL;
    UINT4               u4BuddySize = 0;
    UINT4               u4NrstSize = 0;
    UINT4               u4FragSize = 0;

    OsixSemTake (gu1BuddySemId);
    if ((u4Size > gBuddyTable[u1Id].u4MaxBlkSize) || (u4Size == 0))
    {
        OsixSemGive (gu1BuddySemId);
        return (NULL);
    }

    /* Get the nearest buddy size */

    u4BuddySize = ((u4Size - 1) / gBuddyTable[u1Id].u4MinBlkSize) + 1;
    u4BuddySize *= gBuddyTable[u1Id].u4MinBlkSize;

    u4NrstSize = u4BuddySize;

    /* Try to get the buffer in the same size queue */

    while ((pu1BuddyBlk = BuddyDeleteFirstBlock (u1Id, u4BuddySize)) == NULL)
    {
        u4BuddySize += gBuddyTable[u1Id].u4MinBlkSize;
        if (u4BuddySize > gBuddyTable[u1Id].u4MaxBlkSize)
        {
            if ((gBuddyTable[u1Id].u1CFlag & BUDDY_HEAP_EXTN) ==
                BUDDY_HEAP_EXTN)
            {
                pu1BuddyBlk = MEM_CALLOC (sizeof (UINT1), u4Size, UINT1);
                OsixSemGive (gu1BuddySemId);
                return (pu1BuddyBlk);
            }
            OsixSemGive (gu1BuddySemId);
            return (NULL);
        }
    }

    /* Get the appropriate buffer */

    pu1BuddyBuf = (UINT1 *) gBuddyTable[u1Id].pBuddyBuf;

    if ((gBuddyTable[u1Id].u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF)
    {
        pu1BuddyBuf = pu1BuddyBlk -
            ((pu1BuddyBlk - pu1BuddyBuf) %
             (gBuddyTable[u1Id].u2HdrSize + gBuddyTable[u1Id].u4MaxBlkSize));
    }
    else
    {
        while ((pu1BuddyBuf != NULL)
               && (BUDDY_COMPARE_BUFFS (u1Id, pu1BuddyBuf +
                                        gBuddyTable[u1Id].u2HdrSize,
                                        pu1BuddyBlk)))
        {
            pu1BuddyBuf = (UINT1 *) ((tBuddyBuf *) (VOID *) pu1BuddyBuf)->pNext;
        }
    }

    /* Fragment the bigger block, retain the requested block size and insert
     * the remaining block in the appropriate list */

    /* Nearest size refers to the size of the buddy buffer that is the best fit.
     * If such a buffer does not exist, we try to grab a buffer whose size is a
     * multiple of the minimum block size and which is greater than the
     * requested size (also greater than the Nearest Size). Hence we have to
     * fragment the bigger size buffer, into the requested block and keep the
     * remaining in the appropriate queue. For example, if 64 is the min block
     * size, and the user application has requested for, say 126 bytes, the best
     * fit would be 128 byte buffer. If the 128 byte buffer queue is empty, then
     * we may have to allocate a buffer from the higher queues, viz 192, 256
     * etc. Say we allocated a buffer of 256 bytes. Then we return the 128 bytes
     * to the application and keep the remaining 128 bytes in the 128 size
     * queue.
     */

    if (u4NrstSize != u4BuddySize)
    {
        u4FragSize = (u4BuddySize - u4NrstSize);
        pu1FragBlk = pu1BuddyBlk + u4NrstSize;
        BuddyAddIntoBlk (u1Id, pu1FragBlk, u4FragSize);
    }

    BuddySetACBits (u1Id, pu1BuddyBuf, pu1BuddyBlk, u4NrstSize);
    gBuddyTable[u1Id].u4MemAlloc += u4NrstSize;

    OsixSemGive (gu1BuddySemId);
    return (pu1BuddyBlk);
}

/*******************************************************************************
 * Function    : MemBuddyFree ()
 * Description : This function moves the given block back to the buddy free
 *               pools
 * Input(s)    : u1Id        - The Buddy pool identifier
 *               pu1BuddyBlk - Pointer to the buddy block which is to be
 *                             released
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred and Modified
 * Returns     : BUDDY_SUCCESS     - if the buffer is successfully released
 *               BUDDY_FAILURE     - otherwise
 ******************************************************************************/

PUBLIC INT4
MemBuddyFree (UINT1 u1Id, UINT1 *pu1BuddyBlk)
{
    INT4                i4RetCode = BUDDY_FAILURE;
    UINT4               u4BlkSize = 0;
    UINT1              *pu1BuddyBuf = NULL;
    UINT4               u4MaxBlkSize = gBuddyTable[u1Id].u4MaxBlkSize;
    UINT4               u4NumBlks = gBuddyTable[u1Id].u4NumBlks;
    UINT2               u2HdrSize = gBuddyTable[u1Id].u2HdrSize;

    OsixSemTake (gu1BuddySemId);
    if (!((pu1BuddyBlk >= (UINT1 *) gBuddyTable[u1Id].pBuddyBuf) &&
          pu1BuddyBlk <
          (UINT1 *) gBuddyTable[u1Id].pBuddyBuf + (u4MaxBlkSize +
                                                   u2HdrSize) * u4NumBlks))
    {
        MEM_FREE (pu1BuddyBlk);
        OsixSemGive (gu1BuddySemId);
        return (BUDDY_SUCCESS);
    }

    /* get the matching buffer */

    pu1BuddyBuf = (UINT1 *) gBuddyTable[u1Id].pBuddyBuf;

    if ((gBuddyTable[u1Id].u1CFlag & BUDDY_CONT_BUF) == BUDDY_CONT_BUF)
    {
        pu1BuddyBuf = pu1BuddyBlk -
            ((pu1BuddyBlk - pu1BuddyBuf) %
             (gBuddyTable[u1Id].u2HdrSize + gBuddyTable[u1Id].u4MaxBlkSize));
    }
    else
    {
        while ((pu1BuddyBuf != NULL)
               && (BUDDY_COMPARE_BUFFS (u1Id, pu1BuddyBuf +
                                        gBuddyTable[u1Id].u2HdrSize,
                                        pu1BuddyBlk)))
        {
            pu1BuddyBuf = (UINT1 *) ((tBuddyBuf *) (VOID *) pu1BuddyBuf)->pNext;
        }
    }

    if (pu1BuddyBuf != NULL)
    {
        BuddyResetACBits (u1Id, pu1BuddyBuf, pu1BuddyBlk, &u4BlkSize);
        gBuddyTable[u1Id].u4MemAlloc -= u4BlkSize;

        /* If the block to the right of the freed block is free, Then
         * Merge it with the Free Block
         */

        BuddyTRAndMerge (u1Id, pu1BuddyBuf, pu1BuddyBlk, &u4BlkSize);

        /* If the block to the left of the freed/merged block is free, then
         * Merge it with the free block and get the pointer of the
         * left free block
         */

        BuddyTLAndMerge (u1Id, pu1BuddyBuf, &pu1BuddyBlk, &u4BlkSize);

        /* Add the Merged Block to the Free Q
         */

        BuddyAddIntoBlk (u1Id, pu1BuddyBlk, u4BlkSize);
        i4RetCode = BUDDY_SUCCESS;
    }

    OsixSemGive (gu1BuddySemId);
    return (i4RetCode);
}

/*******************************************************************************
 * Function    : BuddyAddIntoBlk ()
 * Description : This function inserts the given block into a queue which
 *               matches the size of the buffer. It does not actually free the
 *               buffer. This buffer may get allocated if a request for a buffer
 *               of this size is made subsequently. This buffer gets merged with
 *               its buddies periodically.
 * Input(s)    : u1Id        - The Buddy Identifier
 *               pu1BuddyBlk - Pointer to the buddy block to be released
 *               u4BuddySize - Size of the buddy block which identifies the
 *                             queue where the buddy block is to be placed
 * Outputs(s   : None
 * Globals     : gBuddyTable Referred and Modified
 * Returns     : BUDDY_SUCCESS
 ******************************************************************************/

PRIVATE INT4
BuddyAddIntoBlk (UINT1 u1Id, UINT1 *pu1Buf, UINT4 u4BuddySize)
{
    UINT4               u4BuddyIdx;

    u4BuddyIdx = ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) - 1);

    if ((tBuddyBuf *) ((VOID *) (gBuddyTable[u1Id].pu1FreeQ[u4BuddyIdx])) !=
        NULL)
    {
        ((tBuddyBuf *) (VOID *) pu1Buf)->pNext =
            (tBuddyBuf *) ((VOID *) (gBuddyTable[u1Id].pu1FreeQ[u4BuddyIdx]));
    }
    else
    {
        ((tBuddyBuf *) (VOID *) pu1Buf)->pNext = NULL;
    }
    gBuddyTable[u1Id].pu1FreeQ[u4BuddyIdx] = pu1Buf;
    return (BUDDY_SUCCESS);
}

/******************************************************************************
 * Function    : BuddyDeleteFirstBlock ()
 * Description : This function deletes the first buddy block from corresponding
 *               buddy queue which matches the given size
 * Input(s)    : u1Id        - The Buddy Identifier
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : BuddyBlock which is deleted
 ******************************************************************************/

PRIVATE UINT1      *
BuddyDeleteFirstBlock (UINT1 u1Id, UINT4 u4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT4               u4BuddyIdx = 0;

    u4BuddyIdx = ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) - 1);

    pu1Buf = (UINT1 *) gBuddyTable[u1Id].pu1FreeQ[u4BuddyIdx];

    if (pu1Buf != NULL)
    {
        gBuddyTable[u1Id].pu1FreeQ[u4BuddyIdx] =
            (UINT1 *) ((tBuddyBuf *) (VOID *) pu1Buf)->pNext;
    }

    return (pu1Buf);
}

/*******************************************************************************
 * Function    : BuddyDeleteInMiddle ()
 * Description : This function delinks the given buddy block from the queue
 *               which matches the given size
 * Input(s)    : u1Id        - The Buddy Identifier
 *               pu1BuddyBlk - Pointer to the Block to be deleted
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : BUDDY_SUCCESS, if the given block is successfully De-linked
 *               BUDDY_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
BuddyDeleteInMiddle (UINT1 u1Id, UINT1 *pu1BuddyBlk, UINT4 u4BuddySize)
{
    INT4                i4RetVal = BUDDY_FAILURE;
    UINT4               u4BuddyIdx = 0;
    tBuddyBuf          *pPrevBlk = NULL;
    tBuddyBuf          *pBuddyBlk = NULL;

    u4BuddyIdx = ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) - 1);

    pBuddyBlk = (tBuddyBuf *) ((VOID *) gBuddyTable[u1Id].pu1FreeQ[u4BuddyIdx]);

    while (pBuddyBlk != NULL)
    {
        if (pu1BuddyBlk == (UINT1 *) pBuddyBlk)
        {
            if (pPrevBlk == NULL)
            {
                gBuddyTable[u1Id].pu1FreeQ[u4BuddyIdx] =
                    (UINT1 *) pBuddyBlk->pNext;
            }
            else
            {
                pPrevBlk->pNext = pBuddyBlk->pNext;
            }
            break;
        }
        pPrevBlk = pBuddyBlk;
        pBuddyBlk = pBuddyBlk->pNext;
    }

    return (i4RetVal);
}

/*******************************************************************************
 * Function    : BuddySetACBits ()
 * Description : This function set the AC (Allocation and Continuation) bits
 *               in the Header of the buffer whenever a Block is allocated from
 *               the Buddy Buffer.
 * Input(s)    : u1Id        - The Buddy Identifier
 *               pu1BuddyBuf - Pointer to the Buddy Buffer which includes the
 *                             given block
 *               pu1BuddyBlk - Pointer to the Block which is being allocated
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
BuddySetACBits (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                UINT4 u4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1StartBit = 0;
    UINT1               u1ChkMask = 0;
    UINT1               u1BitPos = 0;
    UINT2               u2BitOffset = 0;
    UINT2               u2NumBitsToSet = 0;

    pu1Buf = pu1BuddyBuf;

    /* BUDDY_BIT_OFFSET gives the bit offset in the header which corresponds to
     * the buddy block.
     * Note: Each minimum size block will have 2 bits in the header, one A =
     * alloc bit and C = continue bit. BUDDY_BIT_OFFSET is used to calculate the
     * appropriate bit corresponding to the given buddy block
     */

    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf, pu1BuddyBlk);

    /* BUDDY_START_BIT gives the offset of bit in a given byte */

    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);

    /* Increment the pu1Buf to the position in the header to reset
     * the AC bits
     */

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));

    u2NumBitsToSet = (UINT2)
        ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) * 2);

    u1ChkMask = (UINT1) (0xC0 >> u1StartBit);
    u1BitPos = u1StartBit;

    while (u2NumBitsToSet > 0)
    {
        if ((u2NumBitsToSet <= 8) || (u1StartBit != 0))
        {
            *pu1Buf &= (UINT1) (~u1ChkMask);
            if (u2NumBitsToSet == 2)
            {
                *pu1Buf |= (0xC0 >> u1BitPos);
            }
            else
            {
                *pu1Buf |= (0x80 >> u1BitPos);
            }
            u2NumBitsToSet -= 2;
            u1StartBit += 2;
            if (u1StartBit == 8)
            {
                pu1Buf = pu1Buf + 1;
                u1BitPos = 0;
                u1StartBit = 0;
                u1ChkMask = 0xC0;
            }
            else
            {
                u1BitPos += 2;
                u1ChkMask >>= 2;
            }
        }
        else
        {
            *pu1Buf = 0xAA;
            pu1Buf = pu1Buf + 1;
            u2NumBitsToSet -= 8;
            u1BitPos = 0;
        }
    }
}

/*******************************************************************************
 * Function    : BuddyResetACBits ()
 * Description : This function set the AC (Allocation and Continuation) bits
 *               in the Header of the buffer whenever a Block is allocated from
 *               the Buddy Buffer.
 * Input(s)    : pu1BuddyBuf - Pointer to the Buddy Buffer which includes the
 *                             given block
 *               pu1BuddyBlk - Pointer to the Block which is being allocated
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
BuddyResetACBits (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                  UINT4 *pu4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1StartBit = 0;
    UINT1               u1SetMask = 0;
    UINT1               u1ChkMask = 0;
    UINT1               u1BitPos = 0;
    UINT2               u2BitOffset = 0;

    pu1Buf = pu1BuddyBuf;

    /* BUDDY_BIT_OFFSET gives the offset of bit from the starting of the
     * NextPointer field in the pu1BuddyBuf
     */

    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf, pu1BuddyBlk);

    /* BUDDY_START_BIT gives the offset of bit ina  given byte
     */

    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);

    /* Increment the pu1Buf to the position in the header to reset
     * the AC bits
     */

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));
    u1ChkMask = (UINT1) (0xC0 >> u1StartBit);
    u1SetMask = (UINT1) (0x80 >> u1StartBit);
    u1BitPos = u1StartBit;
    *pu4BuddySize = 0;

    while (1)
    {
        if ((*pu1Buf & u1ChkMask) == u1ChkMask)
        {
            *pu1Buf &= ~u1ChkMask;
            *pu1Buf |= (0x40 >> u1BitPos);
            (*pu4BuddySize)++;
            break;
        }
        else if ((*pu1Buf & u1ChkMask) == u1SetMask)
        {
            *pu1Buf &= ~u1ChkMask;
            (*pu4BuddySize)++;
        }
        u1BitPos += 2;

        if (u1BitPos == 8)
        {
            u1BitPos = 0;
            u1ChkMask = 0xC0;
            u1SetMask = 0x80;
            pu1Buf = pu1Buf + 1;
        }
        else
        {
            u1ChkMask >>= 2;
            u1SetMask >>= 2;
        }
    }

    *pu4BuddySize *= gBuddyTable[u1Id].u4MinBlkSize;

}

/*******************************************************************************
 * Function    : BuddySetBACBits ()
 * Description : This function sets the BAC (Boundary Allocation and
 *               Continuation) bits in the Header of the buffer when the
 *               associated blocks are fragmented.
 * Input(s)    : pu1BuddyBuf - Pointer to the buddy buffer
 *               pu1BuddyBlk - Pointer to the Block which is being fragmented
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : BUDDY_SUCCESS
 ******************************************************************************/

PRIVATE INT4
BuddySetBACBits (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                 UINT4 u4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1StartBit = 0;
    UINT2               u2BitOffset = 0;

    pu1Buf = pu1BuddyBuf;

    /* move to the last offset, i.e the boundary of the block */

    u2BitOffset = (UINT2)
        BUDDY_BIT_OFFSET (u1Id, pu1Buf, (pu1BuddyBlk +
                                         (u4BuddySize -
                                          gBuddyTable[u1Id].u4MinBlkSize)));

    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));

    *pu1Buf &= ~(0xC0 >> u1StartBit);
    *pu1Buf |= (0x40 >> u1StartBit);

    return (BUDDY_SUCCESS);
}

/******************************************************************************
 *  Function Name   : BuddyTRAndMerge
 *  Description     : This function merges the released blocks with the other
 *                    released blocks at the right if any.
 *  Input (s)        : pu1BuddyBuf - The pointer the buddy buffer
 *                    u2BlkSize - The released block size
 *                    pu2RBitOffset - The right offset
 *  Outputs (s)      : pu2RBitOffset - The new right offset after merger
 *                    pu2NewBlkSize - The Merged block size
 *                    pbRMFlag      - The Right merge flag, if true there are
 *                                    blocks at the right which can be merged.
 *  Globals          : refers to global Buddy table
 *  Returns         : BUDDY_SUCCESS
 ******************************************************************************/

PRIVATE INT4
BuddyTRAndMerge (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                 UINT4 *pu4BlkSize)
{
    UINT1               u1StartBit = 0;
    UINT1               u1SetPat = BUDDY_FALSE;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1FreeBlk = NULL;
    UINT1               u1BitMask = 0;
    UINT1               u1EndChkMask = 0;
    UINT4               u4FreeBlkSize = 0;
    UINT2               u2BitOffset = 0;

    pu1Buf = pu1BuddyBuf;
    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf,
                                            (pu1BuddyBlk + *pu4BlkSize));

    /* If the given Block is the rightmost Block, then return as it cannot be
     * merged
     */

    if (u2BitOffset == (gBuddyTable[u1Id].u4MaxBlkSize /
                        gBuddyTable[u1Id].u4MinBlkSize) * 2)
    {
        return (BUDDY_FAILURE);
    }

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));
    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);
    u1BitMask = (UINT1) (0xC0 >> u1StartBit);
    u1EndChkMask = (UINT1) (0x40 >> u1StartBit);

    /* If the Block right to the given block is Allocated block, Then
     * return
     */

    if (((*pu1Buf & u1BitMask) != 0) && ((*pu1Buf & u1BitMask) != u1EndChkMask))
    {
        return (BUDDY_FAILURE);
    }

    /* Since the Buffer right to the given block is free, reset the boundary
     * bit of the free block
     */

    if (u1StartBit != 0)
    {
        *pu1Buf &= ~(u1BitMask << 2);
        u4FreeBlkSize = BUDDY_GET_FREE_RIGHT_BLKS ((*pu1Buf &
                                                    (0xFF >> u1StartBit)));
        u4FreeBlkSize -= (u1StartBit / 2);
    }
    else
    {
        *(pu1Buf - 1) &= 0xFC;
        u4FreeBlkSize = BUDDY_GET_FREE_RIGHT_BLKS (*pu1Buf);
    }

    if ((*pu1Buf & (0xFF >> u1StartBit)) == 0)
    {
        u1SetPat = BUDDY_TRUE;
        pu1Buf = pu1Buf + 1;
    }

    while (u1SetPat == BUDDY_TRUE)
    {
        if (*pu1Buf != 0)
        {
            u4FreeBlkSize += BUDDY_GET_FREE_RIGHT_BLKS (*pu1Buf);
            u1SetPat = BUDDY_FALSE;
        }
        else
        {
            u4FreeBlkSize += 4;
            pu1Buf = pu1Buf + 1;
        }
    }

    u4FreeBlkSize *= gBuddyTable[u1Id].u4MinBlkSize;
    pu1FreeBlk = pu1BuddyBlk + *pu4BlkSize;

    BuddyDeleteInMiddle (u1Id, pu1FreeBlk, u4FreeBlkSize);
    *pu4BlkSize = (*pu4BlkSize) + u4FreeBlkSize;
    return (BUDDY_SUCCESS);
}

/******************************************************************************
 *  Function Name   : BuddyTLAndMerge
 *  Description     : This function merges the released blocks with the other
 *                    released blocks at the right if any.
 *  Input (s)       : pu1BuddyBuf - The pointer the buddy buffer
 *                    u2BlkSize - The released block size
 *                    pu2RBitOffset - The right offset
 *  Outputs (s)     : pu2RBitOffset - The new right offset after merger
 *                    pu2NewBlkSize - The Merged block size
 *                    pbRMFlag      - The Right merge flag, if true there are
 *                                    blocks at the right which can be merged.
 *  Globals         : refers to global Buddy table
 *  Returns         : BUDDY_SUCCESS
 ******************************************************************************/

PRIVATE VOID
BuddyTLAndMerge (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 **pu1BuddyBlk,
                 UINT4 *pu4BlkSize)
{
    UINT1               u1StartBit = 0;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1FreeBlk = NULL;
    UINT1               u1BitMask = 0;
    UINT1               u1SetPat = BUDDY_FALSE;
    UINT2               u2NumBlks = 0;
    UINT4               u4FreeBlkSize = 0;
    UINT2               u2BitOffset = 0;

    pu1Buf = pu1BuddyBuf;
    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf, (*pu1BuddyBlk));

    if (u2BitOffset == 0)
    {
        return;
    }
    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));
    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);
    u2BitOffset -= u1StartBit;

    if (u1StartBit == 0)
    {
        pu1Buf = pu1Buf - 1;
        u1StartBit = 8;
        u2BitOffset -= 8;
    }

    u1BitMask = (UINT1) (0xC0 >> (u1StartBit - 2));

    if ((*pu1Buf & u1BitMask) != (0x40 >> (u1StartBit - 2)))
    {
        return;
    }

    *pu1Buf &= ~u1BitMask;
    u2NumBlks = BUDDY_GET_FREE_LEFT_BLKS ((*pu1Buf &
                                           (0xFF << (8 - u1StartBit))));
    u2NumBlks -= ((8 - u1StartBit) / 2);

    if ((*pu1Buf & (0xFF << (8 - u1StartBit))) == 0)
    {
        u1SetPat = BUDDY_TRUE;
        pu1Buf = pu1Buf - 1;
    }

    while ((u1SetPat == BUDDY_TRUE) && (u2BitOffset > 0))
    {
        if (*pu1Buf != 0)
        {
            u2NumBlks += BUDDY_GET_FREE_LEFT_BLKS (*pu1Buf);
            u1SetPat = BUDDY_FALSE;
        }
        else
        {
            u2NumBlks += 4;
            pu1Buf = pu1Buf - 1;
            u2BitOffset -= 8;
        }
    }

    u4FreeBlkSize = u2NumBlks * gBuddyTable[u1Id].u4MinBlkSize;
    pu1FreeBlk = (*pu1BuddyBlk) - u4FreeBlkSize;

    BuddyDeleteInMiddle (u1Id, pu1FreeBlk, u4FreeBlkSize);
    *pu1BuddyBlk = pu1FreeBlk;
    *pu4BlkSize = (*pu4BlkSize) + u4FreeBlkSize;
}

PUBLIC VOID
MemBuddyPrintStatistics (UINT1 u1Id)
{
    UINT2              *pu1FreeMem = NULL;
    UINT2               u2Cnt = 0;
    UINT2               u2NumBlks = 0;
    UINT4               u4FreeBuf = 0;
    tBuddyBuf          *pBuf = NULL;

    u2NumBlks = (UINT2) ((gBuddyTable[u1Id].u4MaxBlkSize) /
                         (gBuddyTable[u1Id].u4MinBlkSize));
    pu1FreeMem = (UINT2 *) MEM_CALLOC (u2NumBlks, sizeof (UINT2), UINT2 *);

    if (!pu1FreeMem)
    {
        return;
    }
    for (u2Cnt = 0; u2Cnt < u2NumBlks; u2Cnt++)
    {
        pu1FreeMem[u2Cnt] = 0;
        pBuf = (tBuddyBuf *) ((VOID *) (gBuddyTable[u1Id].pu1FreeQ[u2Cnt]));
        while (pBuf != NULL)
        {
            pu1FreeMem[u2Cnt]++;
            pBuf = pBuf->pNext;
        }
    }

    for (u2Cnt = 0; u2Cnt < u2NumBlks; u2Cnt++)
    {
        u4FreeBuf +=
            (gBuddyTable[u1Id].u4MinBlkSize * (u2Cnt + 1)) * pu1FreeMem[u2Cnt];
    }

    MEM_FREE (pu1FreeMem);
}
