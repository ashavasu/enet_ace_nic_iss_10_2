/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: fsapcm.c,v 1.13 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Code for Multinode support.
 */

#include "osxinc.h"
#include "fsapcm.h"

#ifdef MCM_WANTED
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>

/************************************************************************
*                                                                       *
*                             Typedefs                                  *
*                                                                       *
*************************************************************************/
typedef UINT4       tIPAddr;
typedef struct
{
    UINT4               u4NId;
    tIPAddr             ipAddr;
    UINT4               u4Port;
}
tNode2IpAddr;

tCmCallBackFn       Callback;

/************************************************************************
*                                                                       *
*                            Macros                                     *
*                                                                       *
*************************************************************************/

#define BUFSIZE 4000
/* Maximum length of CM's Rx buffer. */

#define CM_PRINT(x) CmDbgPrint (x, __FUNCTION__, __LINE__)

/************************************************************************
*                                                                       *
*                    Global Variables                                   *
*                                                                       *
*************************************************************************/
static tNode2IpAddr gaNode2IpAddrTable[2];
/* Stores Node Id to IP address, Port No. mapping. for all nodes in the 
 * system.
 */

static UINT4        gu4NodeCount = 0;
/* Population count of above array. */

static int          gFD;
/* The FD used to send/receive o'er the UDP socket. */

/************************************************************************
*                                                                       *
*               Internal Function Prototypes                            *
*                                                                       *
*************************************************************************/
static VOID         CmTaskMain (INT1 *);
/* CM Task's Entry point function. */

static INT4         CmSearchDBase (UINT4 u4NodeId, tIPAddr *, UINT4 *pu4Port);
/* Function to search the Node2IpAddr Table */

static INT4         CmAddNode (UINT4 u4NodeId, tIPAddr ipAddr, UINT4 u4Port);
/* Adds an entry in the database. */

static UINT4        CmReceive (INT1 ai1IpcBuf[], UINT4 u4BufSize);

/************************************************************************/
/*
 * Debug routine.
 */
static void
CmDbgPrint (const char *err, const char *func, int lineno)
{
    UtlTrcLog (1, 1, "", "%s %s, %d\n", err, func, lineno);
    return;
}

/*
 * Called from main() at startup.
 * Given the NodeId as param, searches the <NId, IPaddress> table above
 * and opens a UDP socket, which is used to both send and receive
 * packets.
 */
INT4
CmInit (UINT4 u4SelfNodeId)
{
    INT4                i4rc;
    struct sockaddr_in  sender;
    tIPAddr             MyIpAddr;
    UINT4               u4Port;
    tOsixTaskId         TaskId;

    i4rc = CmSearchDBase (u4SelfNodeId, &MyIpAddr, &u4Port);

    if (i4rc < 0)
    {
        CM_PRINT ("-E- OsixSearchDbase failed, database possibly"
                  " not initialized.\n\n");
        return -1;
    }

    Callback = NULL;

    gFD = socket (AF_INET, SOCK_DGRAM, 0);
    if (gFD < 0)
    {
        perror ("");
        return -1;
    }
    fcntl (gFD, F_SETFL, O_NONBLOCK);
    sender.sin_family = AF_INET;
    sender.sin_port = htons ((UINT2) u4Port);

    /*
       bcopy ((char *) &MyIpAddr, (char *) &sender.sin_addr.s_addr, 4);
     */
    memcpy ((char *) &sender.sin_addr.s_addr, (char *) &MyIpAddr, 4);

    i4rc = bind (gFD, (struct sockaddr *) &sender, sizeof (sender));
    if (i4rc < 0)
    {
        perror ("Sender bind");
        return i4rc;
    }

    OsixCreateTask ((const UINT1 *) "CM", 15, 0, CmTaskMain, 0, 0, &TaskId);

    return i4rc;
}

/*
 * Appls need to call this functions to register their callback.
 * This functions will be invoked from CM task context.
 */
void
CmRegisterCallBack (tCmCallBackFn cb)
{
    Callback = cb;
}

/*
 * CM Task Entry Point function.
 * Spins in a delayed while loop doing a socket recvfrom().
 */

static VOID
CmTaskMain (INT1 *pParam)
{
    INT1                ai1IpcBuf[BUFSIZE];
    INT4                u4rc;
    tCRU_BUF_CHAIN_DESC *pMsg;

    pParam = pParam;            /* Unused var. */

    if (!Callback)
    {
        CM_PRINT ("-E- Callback not registered.");
    }

    while (1)
    {
        while ((u4rc = CmReceive (ai1IpcBuf, BUFSIZE)) > 0)
        {
            if ((pMsg = CRU_BUF_Allocate_MsgBufChain (u4rc, 0)) == NULL)
            {
                CM_PRINT ("-E- CRU Alloc failed.");
                return;
            }

            CRU_BUF_Copy_OverBufChain (pMsg, (void *) ai1IpcBuf, 0, u4rc);

            (*Callback) (pMsg, u4rc);
        }

        OsixDelayTask (1);
        /* Pause for 1 STUP and poll for packet. */
    }
}

/*
 * CM shutdown routine.
 * Use shutdown() instead of close() to avoid
 * lingering of UDP sockets.
 */

INT4
CmShut (void)
{
    int                 rc;

    rc = shutdown (gFD, 2);
    return rc;
}

static INT4
CmAddNode (UINT4 u4NodeId, tIPAddr ipAddr, UINT4 u4Port)
{
    gaNode2IpAddrTable[gu4NodeCount].u4NId = u4NodeId;
    gaNode2IpAddrTable[gu4NodeCount].ipAddr = ipAddr;
    gaNode2IpAddrTable[gu4NodeCount].u4Port = u4Port;
    gu4NodeCount++;

    return 0;
}

static INT4
CmSearchDBase (UINT4 u4NodeId, tIPAddr * pIPAddr, UINT4 *pu4Port)
{
    UINT4               u4Idx;
    tNode2IpAddr       *probe = gaNode2IpAddrTable;

    for (u4Idx = 0; u4Idx < gu4NodeCount; ++u4Idx, ++probe)
    {
        if (probe->u4NId == u4NodeId)
        {
            *pIPAddr = probe->ipAddr;
            *pu4Port = probe->u4Port;
            return 0;
        }
    }
    return -1;
}

/*
 * Given a Destination Node Id, and a linear buffer, sends
 * the packet to the appropriate destination over the UDP connection..
 * Returns - a positive value on success; a negative value if failed.
 */
INT4
CmSend (UINT4 u4DestNode, tCRU_BUF_CHAIN_DESC * pMsg, UINT4 len)
{
    INT4                i4rc = 0;
    struct sockaddr_in  to;
    UINT4               u4Port;
    tIPAddr             HisIpAddr;
    UINT1              *ipcmsg;

    if (u4DestNode == 0)
    {
        return (-1);
    }

    i4rc = CmSearchDBase (u4DestNode, &HisIpAddr, &u4Port);
    to.sin_family = AF_INET;
    to.sin_port = htons ((UINT2) u4Port);

    memcpy ((char *) &to.sin_addr.s_addr, (char *) &HisIpAddr, 4);

    /* Convert CRU To a linear buffer. */
    if ((ipcmsg = MEM_MALLOC (len, UINT1)) == NULL)
    {
        CM_PRINT ("-E- MEM_MALLOC failed.");
        return (-1);
    }
    if (CRU_BUF_Copy_FromBufChain (pMsg, ipcmsg, 0, len) == CRU_FAILURE)
    {
        return (-1);
    };
    while (1)
    {
        i4rc = sendto (gFD, (void *) ipcmsg, len, 0,
                       (struct sockaddr *) &to, sizeof (to));
        if (i4rc < 0 && errno == 4)
        {
            continue;
        }
        else
        {
            break;
        }
    }
    if (i4rc < 0)
        UtlTrcLog (1, 1, "", "sendto error %d...\n", errno);
    else
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
    MEM_FREE (ipcmsg);
    return i4rc;
}

/*
 * Waits for a packet ...
 * Caller should pass a suitably sized buffer.
 */
UINT4
CmReceive (INT1 ai1IpcBuf[], UINT4 u4BufSize)
{
    struct sockaddr_in  from;
    UINT4               fromlen = sizeof (from);
    INT4                i4rc;

  loop:i4rc = recvfrom (gFD, (void *) ai1IpcBuf, u4BufSize, 0,
                     (struct sockaddr *) &from,
                     (socklen_t *) & fromlen);
    if (i4rc < 0)
    {
        OsixDelayTask (1);
        goto loop;
    }

    return i4rc;
}

/*
 * Read the NodeId to IP Address+Port-No mapping from
 * the database file. The database file is the one
 * pointed to by the environment variable "CMDBASE".
 *
 * Typical input file contents:
 * 1                      # Node-Id of this EXE.
 * 1 10.20.6.5 6000       # <Node-id> <Host IPaddr> <Local Port>
 * 2 10.20.6.6 6001       # Peer Node details.
 */
INT4
CmReadNodes (UINT4 *pu4NodeId, UINT4 *pu4PeerNodeId)
{
    UINT4               u4NId, u4Port;
    char               *i1Name, *token;
    char                ai1Line[80];
    FILE               *fptr;
    UINT4               ipAddr;
    UINT4               u4rc;

    if ((i1Name = getenv ("CMDBASE")) == NULL)
    {
        UtlTrcPrint ("sh var. CMDBASE not set ...\n");
        return -1;
    }
    fptr = fopen ((const char *) i1Name, "r");
    if (fptr == NULL)
    {
        perror ("InitDatabase");
        return -1;
    }
    UtlTrcPrint ("Data base read is as follows ...\n");

    /* Obtain the NodeId */
    if (fgets (ai1Line, 80, fptr) != NULL)
    {
        token = strtok (ai1Line, " \t\n");
        *pu4NodeId = atoi (token);

    }
    while (fgets (ai1Line, 80, fptr) != NULL)
    {

        token = strtok (ai1Line, " \t\n");
        u4NId = atoi (token);

        token = strtok (NULL, " \t\n");
        ipAddr = inet_addr (token);

        token = strtok (NULL, " \t\n");
        u4Port = atoi (token);

        UtlTrcLog (1, 1, "", "%d %x %d\n", u4NId, ipAddr, u4Port);

        /* Add the node to Osix's database. */
        u4rc = CmAddNode (u4NId, ipAddr, u4Port);

        if (u4rc)
        {
            UtlTrcPrint ("CmAddNode failed\n");
            return -1;
        }

        if (u4NId == *pu4NodeId)
        {
            continue;
        }

        *pu4PeerNodeId = u4NId;
    }

    return 0;
}
#endif /* MCM_WANTED */
