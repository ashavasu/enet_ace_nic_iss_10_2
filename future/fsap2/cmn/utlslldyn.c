/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlslldyn.c,v 1.5 2015/04/28 12:00:00 siva Exp $
 *
 * Description:
 *   Singly linked lists where the data is stored as a ptr in the node.
 *   For implementation of SLLs with embedded nodes see utlsll.[ch]
 *
 */

#include "osxinc.h"
#include "utlslldyn.h"

enum eUtlDynSllOper
{ DELETE = 0, INSERT };

struct UtlDynSllNode
{
    struct UtlDynSllNode *pNext;
    tDynSLLElem        *pElem;
};

struct UtlDynSllList
{
    struct UtlDynSllNode Head;
    struct UtlDynSllNode *Tail;
    tDynSLLCmpFn        Cmp;
    tMemPoolId          PoolId;
    UINT4               u4Count;
};

static struct UtlDynSllNode *UtlDynSllSearch (enum eUtlDynSllOper,
                                              tDynSLLElem *,
                                              struct UtlDynSllList *,
                                              tDynSLLCmpFn);

/****************************************************************************
 *    Function Name      : UtlDynSllInit
 *
 *    Description        : Initializes an instance of a SLL List
 *
 *    Input(s)           : u4NumNodes is the maximum no. of nodes for this
 *                         instance.
 *                         Cmp is a pointer to a comparison function called
 *                         whenever two elements in the list need to be
 *                         compared.
 *
 *    Output(s)          : None.
 *
 *    Returns            : Handle to Created sll list/ NULL if failure.
 ****************************************************************************/
tUtlDynSll
UtlDynSllInit (UINT4 u4NumNodes, tDynSLLCmpFn Cmp)
{
    UINT4               u4rc;
    tUtlDynSll          pDynSllList;

    if ((pDynSllList = MEM_MALLOC (sizeof (struct UtlDynSllList),
                                   struct UtlDynSllList)) == NULL)
    {
        return (NULL);
    }

    pDynSllList->Head.pNext = &pDynSllList->Head;
    pDynSllList->Tail = &pDynSllList->Head;
    pDynSllList->u4Count = 0;
    pDynSllList->Cmp = Cmp;

    /* Allocate memory pool for SLL List nodes */

    u4rc =
        MemCreateMemPool (sizeof (struct UtlDynSllNode), u4NumNodes,
                          MEM_HEAP_MEMORY_TYPE, &(pDynSllList->PoolId));
    if (u4rc)
    {
        MEM_FREE (pDynSllList);
        return (NULL);
    }

    return (pDynSllList);
}

/****************************************************************************
 *    Function Name      : UtlDynSllShut
 *
 *    Description        : Deletes an instance of a sll list
 *                         This function ensures that the memory allocated
 *                         by the application gets freed.
 *
 *    Input(s)           : pDynSllList is the pointer to the list returned by
 *                         an earlier call to UtlDynSllCreateList.
 *                         FreeFn is the callback provided by the user.
 *
 *    Output(s)          : None.
 *
 *    Returns            : None.
 ****************************************************************************/
void
UtlDynSllShut (tUtlDynSll pDynSllList, tDynSLLFreeFn FreeFn)
{
    if (pDynSllList == NULL)
    {
        return;
    }

    UtlDynSllDrain (pDynSllList, FreeFn);
    MemDeleteMemPool (pDynSllList->PoolId);
    MEM_FREE (pDynSllList);
}

/****************************************************************************
 *    Function Name      : UtlDynSllDrain
 *
 *    Description        : Deletes all the nodes  from an SLL
 *                         list starting from the first node. Does not delete
 *                         the List.
 *
 *    Input(s)           : pDynSllList is the pointer to the list returned by
 *                         an earlier call to UtlDynSllInit.
 *                         FreeFn is the callback provided by the user.
 *
 *    Output(s)          : None.
 *
 *    Returns            : None.
 ****************************************************************************/
void
UtlDynSllDrain (tUtlDynSll pDynSllList, tDynSLLFreeFn FreeFn)
{
    struct UtlDynSllNode *pNext, *pCur;

    /* if empty list */
    if (pDynSllList == NULL || pDynSllList->u4Count == 0)
    {
        return;
    }

    pCur = pDynSllList->Head.pNext;

    while (pCur != &pDynSllList->Head)
    {
        pNext = pCur->pNext;
        FreeFn (pCur->pElem);
        MemReleaseMemBlock (pDynSllList->PoolId, (UINT1 *) pCur);
        pCur = pNext;
    }

    /* reinitialize the list */
    pDynSllList->u4Count = 0;
    pDynSllList->Head.pNext = &pDynSllList->Head;
    pDynSllList->Tail = &pDynSllList->Head;
}

/****************************************************************************
 *    Function Name      : UtlDynSllAdd
 *
 *    Description        : Adds an element (key) to an SLL List pDynSllList
 *
 *    Input(s)           : key is the pointer to the new element to be inserted
 *                         into the list referred to by pDynSllList.
 *
 *    Output(s)          : None.
 *
 *    Returns            : DYN_SLL_SUCCESS/DYN_SLL_FAILURE
 ****************************************************************************/
UINT4
UtlDynSllAdd (tUtlDynSll pDynSllList, tDynSLLElem * key)
{
    struct UtlDynSllNode *pPrev, *pNew;

    if (pDynSllList == NULL)
    {
        return (DYN_SLL_FAILURE);
    }

    pNew =
        (struct UtlDynSllNode *) (VOID *) MemAllocMemBlk (pDynSllList->PoolId);
    if (pNew == NULL)
    {
        return (DYN_SLL_FAILURE);
    }

    pNew->pElem = key;

    /* UtlDynSllSearch returns the node after which the new node is to be inserted */
    pPrev = UtlDynSllSearch (INSERT, key, pDynSllList, NULL);

    /* if key already present return failure */
    if (pPrev == NULL)
    {
        MemReleaseMemBlock (pDynSllList->PoolId, (UINT1 *) pNew);
        return (DYN_SLL_FAILURE);
    }

    pNew->pNext = pPrev->pNext;
    pPrev->pNext = pNew;
    pDynSllList->u4Count++;
    if (pNew->pNext == &pDynSllList->Head)
    {
        pDynSllList->Tail = pNew;
    }

    return (DYN_SLL_SUCCESS);
}

/****************************************************************************
 *    Function Name      : UtlDynSllDelete
 *
 *    Description        : Removes an element (key) from the list(pDynSllList)
 *
 *    Input(s)           : key is the pointer to the element to be removed
 *                         from the list referred to by pDynSllList.
 *
 *    Output(s)          : None.
 *
 *    Returns            : Returns the element that is reoved from the list
 *                         or NULL if not found.
 ****************************************************************************/
tDynSLLElem        *
UtlDynSllDelete (tUtlDynSll pDynSllList, tDynSLLElem * key)
{
    struct UtlDynSllNode *pPrev, *pNode, *pNext;

    /* if list is empty */
    if (pDynSllList == NULL || pDynSllList->u4Count == 0)
    {
        return (NULL);
    }

    /* UtlDynSllSearch returns the previous node of the node to be deleted if it is
     * found , else it returns NULL
     */
    pPrev = UtlDynSllSearch (DELETE, key, pDynSllList, NULL);

    if (pPrev == NULL)
    {
        /* node is not present */
        return (NULL);
    }
    else
    {
        pNode = pPrev->pNext;
        pNext = pNode->pNext;

        if (pNext == &pDynSllList->Head)
        {
            /* deleting the last node */
            pDynSllList->Tail = pPrev;
        }
        pPrev->pNext = pNext;
        pDynSllList->u4Count--;
        key = pNode->pElem;
        MemReleaseMemBlock (pDynSllList->PoolId, (UINT1 *) pNode);
        return (key);
    }
}

/****************************************************************************
 *    Function Name      : UtlDynSllGet
 *
 *    Description        : Returns a pointer to the application structure pointer
 *                         for a given key.
 *
 *    Input(s)           : pDynSllList - Pointer to List
 *                         key - The item to be found.
 *
 *    Output(s)          : None.
 *
 *    Returns            : The node for the given key. NULL if none found.
 ****************************************************************************/
tDynSLLElem        *
UtlDynSllGet (tUtlDynSll pDynSllList, tDynSLLElem * key)
{
    struct UtlDynSllNode *pNode;

    /* if list is empty */
    if (pDynSllList == NULL || pDynSllList->u4Count == 0)
    {
        return (NULL);
    }

    /* UtlDynSllSearch returns the prvious node if found else it returns NULL */
    pNode = UtlDynSllSearch (DELETE, key, pDynSllList, NULL);

    return ((pNode == NULL) ? NULL : (pNode->pNext)->pElem);
}

/****************************************************************************
 *    Function Name      : UtlDynSllGetNext
 *
 *    Description        : Returns a pointer to the next element in the list.
 *
 *    Input(s)           : pDynSllList - Pointer to the List.
 *                         key    - The key whose 'next' is sought.
 *                         Cmp    - The compare function.
 *
 *    Output(s)          : None.
 *
 *    Returns            : Pointer to the next node or NULL if none found.
 ****************************************************************************/
tDynSLLElem        *
UtlDynSllGetNext (tUtlDynSll pDynSllList, tDynSLLElem * key, tDynSLLCmpFn Cmp)
{
    struct UtlDynSllNode *pCur, *pPrev, *pHead;
    INT4                i4Cmp;

    Cmp = (Cmp == NULL) ? (pDynSllList->Cmp) : Cmp;

    pHead = pPrev = &pDynSllList->Head;

    pCur = pDynSllList->Head.pNext;

    while (pCur != pHead)
    {
        i4Cmp = (*Cmp) (key, pCur->pElem);
        if (i4Cmp > 0)
        {
            pCur = pPrev;
            break;
        }

        if (i4Cmp == 0)
        {
            break;
        }

        pPrev = pCur;
        pCur = pCur->pNext;
    }

    /* return Next of pCur if it is not the Head,else there is no Next. */
    return ((pCur->pNext == pHead) ? NULL : (pCur->pNext)->pElem);
}

/****************************************************************************
 *    Function Name      : UtlDynSllCount
 *
 *    Description        : Returns the count of nodes in a given list.
 *
 *    Input(s)           : pDynSllList - pointer to the list.
 *
 *    Output(s)          : pu4Count - Contains the count on return.
 *
 *    Returns            : RB_SUCCESS/RB_FAILURE
 ****************************************************************************/
UINT4
UtlDynSllCount (tUtlDynSll pDynSllList, UINT4 *pu4Count)
{
    if (pDynSllList == NULL)
        return (DYN_SLL_FAILURE);

    *pu4Count = pDynSllList->u4Count;
    return (DYN_SLL_SUCCESS);
}

/****************************************************************************
 *    Function Name      : UtlDynSllFirst
 *
 *    Description        : Returns the first key in a given list.
 *
 *    Input(s)           : pDynSllList - pointer to the list.
 *
 *    Output(s)          : None.
 *
 *    Returns            : The first key in the list.NULL if list is empty
 *****************************************************************************/
tDynSLLElem        *
UtlDynSllFirst (tUtlDynSll pDynSllList)
{
    return ((pDynSllList->u4Count ==
             0) ? NULL : (pDynSllList->Head.pNext)->pElem);

}

/****************************************************************************
 *    Function Name      : UtlDynSllLast
 *
 *    Description        : Returns the last key in a given list.
 *
 *    Input(s)           : pDynSllList - pointer to the list.
 *
 *    Output(s)          : None.
 *
 *    Returns            : The last key in the list.NULL if list is empty
 *****************************************************************************/
tDynSLLElem        *
UtlDynSllLast (tUtlDynSll pDynSllList)
{
    return ((pDynSllList->u4Count == 0) ? NULL : (pDynSllList->Tail)->pElem);

}

/****************************************************************************
 *    Function Name      : UtlDynSllWalk
 *
 *    Description        : Scans through the list.
 *
 *    Input(s)           : pDynSllList - pointer to the list.
 *                         action : call back provided by the application.
 *
 *    Output(s)          : None.
 *
 *    Returns            : None
 *****************************************************************************/
VOID
UtlDynSllWalk (tUtlDynSll pDynSllList, tDynSLLScanFn action)
{
    struct UtlDynSllNode *pNode;
    UINT4               u4rc = DYN_SLL_WALK_CONT;

    if (pDynSllList == NULL || pDynSllList->u4Count == 0)
    {
        return;
    }

    pNode = pDynSllList->Head.pNext;

    while (pNode != &pDynSllList->Head)
    {
        u4rc = (*action) (((struct UtlDynSllNode *) pNode)->pElem);
        if (u4rc == DYN_SLL_WALK_BREAK)
        {
            break;
        }

        pNode = pNode->pNext;
    }
}

/* --------------------------------------------------------------------- */
/* PRIVATE FUNCTIONS BEGIN */

/****************************************************************************
 *    Function Name      : UtlDynSllSearch
 *
 *    Description        : Returns the required key in a given list based on
 *                         the Oper variable.
 *                         Oper = INSERT , it returns the node after which the
 *                                         new node is to be inserted.
 *                         Oper = DELETE , it returns the previous node of the
 *                                         node to be deleted.
 *
 *    Input(s)           : pDynSllList - pointer to the list.
 *
 *    Output(s)          : None.
 *
 *    Returns            : Returns the required structure pointer.
 *****************************************************************************/
static struct UtlDynSllNode *
UtlDynSllSearch (enum eUtlDynSllOper eOper, tDynSLLElem * key,
                 struct UtlDynSllList *pDynSllList, tDynSLLCmpFn Cmp)
{
    struct UtlDynSllNode *pNode, *pPrev;
    INT4                i4Cmp;
    INT4                i4Found = 0;

    Cmp = (Cmp == NULL) ? (pDynSllList->Cmp) : Cmp;

    pPrev = &pDynSllList->Head;

    pNode = (pDynSllList->u4Count == 0) ? NULL : pDynSllList->Head.pNext;

    while (pNode != NULL)
    {
        i4Cmp = (*Cmp) (key, pNode->pElem);
        if (i4Cmp < 0)
        {
            pPrev = pNode;
            pNode = (pNode == pDynSllList->Tail) ? NULL : pNode->pNext;
        }
        else if (i4Cmp > 0)
            break;
        else
        {
            i4Found = 1;
            break;
        }
    }

    if (i4Found && eOper == INSERT)
        return (NULL);

    return ((!i4Found && eOper == DELETE) ? NULL : pPrev);
}
