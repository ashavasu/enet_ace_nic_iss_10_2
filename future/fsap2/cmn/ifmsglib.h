/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmsglib.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: SRM IfMsg module type-definitions.
 *
 */

#ifndef _IFMSGLIB_H
#define _IFMSGLIB_H

/* The Memory Pool ID structure definition */
typedef tMemPoolId tIfMsgMemPoolId;

#define IFMSG_INVALID_POOL_ID       (-1)
#define IFMSG_STATUS_INITIALIZED    (1)
#define IFMSG_STATUS_UNINITIALIZED  (0)

/* Global Record for IfMsg related data structures */
typedef struct _IfMsgGlobalRecord {
   UINT4           u4Initialized; /* Flag for IfMsg lib init status   */
   tIfMsgMemPoolId IfMsgPoolId;   /* Mem Pool ID for the IfMsg Blocks */
#if (DEBUG_IFMSG == FSAP_ON)
   UINT4           u4DbgLevel;    /* Debug level for IfMsg Library    */
   UINT4           u4TotalCount;  /* Total Number of IfMsgBlocks      */
   UINT4           u4TotalAllocs; /* Total No. of allocations         */
   UINT4           u4TotalRels;   /* Total No. of Releases            */
#endif
} tIfMsgGblRec;

static tIfMsgGblRec gIfMsgGlbRec;

/* Memory related Macros */
#define IFMSG_CREATE_MEM_POOL(u4BlkSize, u4NumBlks, u4MemType, pPoolId) \
   MemCreateMemPool((u4BlkSize), (u4NumBlks), (u4MemType), (pPoolId))

#define IFMSG_ALLOCATE_MEM_BLOCK(PoolId, ppBlk) \
   MemAllocateMemBlock((PoolId), (UINT1 **)(ppBlk))

#define IFMSG_RELEASE_MEM_BLOCK(PoolId, pBlk) \
   MemReleaseMemBlock((PoolId), (UINT1 *)(pBlk))

#define IFMSG_DELETE_MEM_POOL(PoolId) \
   MemDeleteMemPool((PoolId))

#define IFMSG_MEM_SUCCESS      MEM_SUCCESS
#define IFMSG_MEM_FAILURE      MEM_FAILURE

#define IFMSG_MEMSET           memset

/* Macros for debugging */
#if (DEBUG_IFMSG == FSAP_ON)

#define IFMSG_DBG(u4DbgType, pi1DbgStr)   \
   UtlTrcLog(gIfMsgGlbRec.u4DbgLevel,     \
             (u4DbgType),                 \
             "IFMSG",                     \
             (pi1DbgStr))

#define IFMSG_PRINT(pi1Fmt)         UtlTrcLog(1, 1, "IFMSG", (pi1Fmt))

#define IFMSG_PRINT1(pi1Fmt, Arg1)  UtlTrcLog(1, 1, "IFMSG", (pi1Fmt), (Arg1))

#else /* DEBUG_IFMSG */

#define IFMSG_DBG(u4DbgType, pi1DbgStr)

#define IFMSG_PRINT(pi1Fmt)

#define IFMSG_PRINT1(pi1Fmt, Arg1)

#endif /* DEBUG_IFMSG */

#define IFMSG_DBG_MAJOR    UTL_DBG_INIT_SHUTDN
#endif /* _IFMSGLIB_H */
