/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlslldyn.h,v 1.4 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Header file for SLL-DYN.
 */

#ifndef _UTLDYNSLL_H
#define _UTLDYNSLL_H

enum {DYN_SLL_SUCCESS = 0, DYN_SLL_FAILURE};

typedef void  tDynSLLElem;

typedef struct UtlDynSllList  *tUtlDynSll;


#define UTL_DYN_SLL_SCAN(pDynSllList,pCurrent,pTemp,TypeCast) \
        for(pCurrent = (TypeCast)(UtlDynSllFirst(pDynSllList)),\
            pTemp = (TypeCast)(UtlDynSllGetNext(pDynSllList,\
                                                (tDynSLLElem *)pCurrent, NULL));\
            pCurrent != NULL;\
            pCurrent = pTemp,\
            pTemp = (pCurrent == NULL) ? NULL :\
            (TypeCast)(UtlDynSllGetNext(pDynSllList,(tDynSLLElem*)pCurrent, NULL)))

/* tDynSLLCmpFn is the prototype of the comparison function
 * to be passed to SLL_Create_List.
 *
 * It compares the two elements e1 and e2 and returns an integer
 * less than, equal to, or greater than zero if  e1  is  found, respectively,
 * to be less than, to match, or be greater than e2.
 */

typedef enum {DYN_SLL_WALK_BREAK, DYN_SLL_WALK_CONT} eDynSllWalkReturns;
typedef INT4 (*tDynSLLFreeFn) (tDynSLLElem *elem);
typedef INT4 (*tDynSLLCmpFn) (tDynSLLElem *e1, tDynSLLElem *e2);
typedef INT4 (*tDynSLLScanFn) (tDynSLLElem *e);


tUtlDynSll     UtlDynSllInit(UINT4 u4NumNodes, tDynSLLCmpFn Cmp);
void           UtlDynSllShut(tUtlDynSll, tDynSLLFreeFn FreeFn);
void           UtlDynSllDrain(tUtlDynSll, tDynSLLFreeFn FreeFn);
UINT4          UtlDynSllAdd(tUtlDynSll, tDynSLLElem *);
tDynSLLElem *  UtlDynSllDelete(tUtlDynSll, tDynSLLElem *);
tDynSLLElem *  UtlDynSllGet(tUtlDynSll, tDynSLLElem *);
tDynSLLElem *  UtlDynSllGetNext(tUtlDynSll, tDynSLLElem *, tDynSLLCmpFn Cmp);
UINT4          UtlDynSllCount(tUtlDynSll, UINT4 *);
tDynSLLElem *  UtlDynSllFirst(tUtlDynSll );
tDynSLLElem *  UtlDynSllLast(tUtlDynSll );
void           UtlDynSllWalk(tUtlDynSll, tDynSLLScanFn);
#endif
