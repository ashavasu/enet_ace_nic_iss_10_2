/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmifmsg.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: SRM IfMsg module's exported file.
 *
 */

#ifndef _SRMIFMSG_H
#define _SRMIFMSG_H

#define IFMSG_SUCCESS      (0)
#define IFMSG_FAILURE      (-1)

/* IfMsg Error Codes */
#define IFMSG_ERR_INV_CFG  (0x1000)   /* Invalid Input Configuration Values */
#define IFMSG_ERR_MEM_RSC  (0x1001)   /* Failed to allocate memory resource */
#define IFMSG_ERR_NO_INIT  (0x1002)   /* Alloc/Release called before Init   */

/* Values for u2Direction of tIfMsg */
#define IFMSG_DIR_DOWN     (0)        /* Source protocol is higher layer to 
                                         Destination protocol module        */
#define IFMSG_DIR_UP       (1)        /* Source protocol is lower layer to
                                         Destination protocol module        */

/* Values for u2MsgType of tIfMsg */
#define IFMSG_TYPE_DATA    (0)        /* u2MsgType = DATA                   */

#define IFMSG_TYPE_CTRL    (1)        /* u2MsgType = CONTROL                */

/* Definitions of Exported Structures */
typedef struct _IfMsgConfigParms {
   UINT4 u4NumMsgs; /* Number of IfMsg blocks required in the system */
   UINT4 u4MemType; /* Type of Memory from where the blocks will be
                       allocated. This facility is supported by the
                       Mem library of SRM (FSAP2) */
} tIfMsgCfg;

typedef tCRU_INTERFACE        tIfMsgIfId;
typedef tCRU_BUF_CHAIN_HEADER tIfMsgBuf;

/* Definition of the IfMsg Structure */
typedef struct _InterfaceMessage {
   UINT2      u2MsgType;                      /* Data or Control Message    */
   UINT2      u2Primitive;                    /* Control Message OPCODE     */
   UINT4      u4ProtocolId;                   /* Source protocol ID         */
   UINT2      u2Direction;                    /* Direction of message viz.
                                                 HL to LL (IFMSG_DIR_DOWN)  
                                                 LL to HL (IFMSG_DIR_UP)    */
   UINT2      u2Align;                        /* For data alignment         */
   tIfMsgIfId IfId;                           /* Interface ID               */
   tIfMsgBuf  *pData;                         /* Pointer to the data buffer */
   UINT4      u4DataLen;                      /* Length of Data             */
   VOID       *pProtocolInfo;                 /* Protocol Specific Params   */
   INT4       (*ReleaseFunc) ARG_LIST((VOID *pProtocolInfo)); 
                                              /* Release function for the
                                                 pProtocolInfo */
} tIfMsg;

/* Prototype declarations for Exported functions */
INT4 IfMsgInit          ARG_LIST((tIfMsgCfg *pIfMsgCfg));
INT4 IfMsgAllocateIfMsg ARG_LIST((tIfMsg **ppIfMsg));
INT4 IfMsgReleaseIfMsg  ARG_LIST((tIfMsg *pIfMsg));
INT4 IfMsgShutDown      ARG_LIST((VOID));

#if (DEBUG_IFMSG == FSAP_ON)

VOID IfMsgSetDbg        ARG_LIST((UINT4 u4NewDbgLevel));

UINT4 IfMsgGetDbg       ARG_LIST((VOID));

VOID IfMsgPrintDbgInfo  ARG_LIST((VOID));

#define IFMSG_PRINT_STATS()         IfMsgPrintDbgInfo()

#else /* DEBUG_IFMSG */

#define IFMSG_PRINT_STATS()

#endif /* DEBUG_IFMSG */

#endif /* _SRMIFMSG_H */
