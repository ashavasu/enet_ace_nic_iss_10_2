/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmbuf.h,v 1.7 2015/04/28 12:00:02 siva Exp $
 *
 * Description: SRM Buf module's exported file (for linear buffers).
 *
 */

#ifndef  _SRMBUF_H
#define  _SRMBUF_H

/****************************************************
************* Defined constants  ********************
*****************************************************/

#define   CRU_SUCCESS    0
#define   CRU_FAILURE  (-1)

#define   CRU_BUF_GetFirstDataDesc(pBuf)        (pBuf)
#define   CRU_BUF_GetNextDataDesc(pDD)          (NULL)
#define   CRU_BUF_GetDataPtr(pDD)               (pDD->pu1_FirstValidByte)
#define   CRU_BUF_GetBlockValidByteCount(pDD)   ((pDD->u4_ValidByteCount))
#define   CRU_BUF_Copy_OverBufChain_AtEnd       CRU_BUF_Copy_OverBufChain

/*********************************************************
*** Type definition for Buffer related Data Structures ***
**********************************************************/

/*** Type Definition for Inter-Module Data Exchange ***/
typedef struct CRU_INTERFACE {
      UINT4  u4IfIndex;
      UINT2  u2_SubReferenceNum;
      UINT1  u1_InterfaceType;
      UINT1  u1_InterfaceNum;
}tCRU_INTERFACE;

typedef struct MODULE_DATA{
    UINT1           u1_SourceModuleId;
    UINT1           u1_DestinModuleId;
    UINT2           u2Reserved;            /* added for packing */
    tCRU_INTERFACE  InterfaceId;
    UINT4           u4Reserved1;
    UINT4           u4Reserved2;
    UINT4           u4Reserved3;

}tMODULE_DATA;

/*** Type Definition of Buffer Manager Configuration Parameters ***/

typedef struct _tBufConfig
{
        UINT4 u4BlkMin;
        UINT4 u4Allocs;
        UINT4 u4AllocFails;
        UINT4 u4FreeFails;
} tBufConfig;

typedef struct _tCruFreeBlk
{
    struct _tCruFreeBlk *pNxt;
    UINT4                u4Size;
} tCruFreeBlk;

typedef struct _tCruFreeHdr
{
    struct _tCruFreeHdr *pNxt;
} tCruFreeHdr;

typedef struct _tCruBufPool
{
    tCruFreeHdr *pFreeHdr;
    tCruFreeBlk *pFreeBlk;
    UINT4       u4BlkMin;
    UINT4       u4Allocs;
    UINT4       u4Frees;
    UINT4       u4AllocFails;
    UINT4       u4FreeFails;
    UINT1       *pu1Heap;
} tCruBufPool;

typedef struct _tCruBufHdr
{
    UINT4               u4BufSize;
    UINT4               u4_ValidByteCount;
    UINT4               u4_FreeByteCount;
    UINT1              *pu1_FirstByte;
    UINT1              *pu1_FirstValidByte;
    struct _tCruBufHdr *pNext;
    tMODULE_DATA        ModuleData;
    struct _tCruBufHdr *pFirstValidDataDesc;
} tCruBufHdr;

typedef tCruBufHdr tCRU_BUF_CHAIN_HEADER;
typedef tCruBufHdr tCRU_BUF_CHAIN_DESC;
typedef tCruBufHdr tCRU_BUF_DATA_DESC;

/*********************************************************
*************** MACRO definition *************************
**********************************************************/

/*************************************************************
*** Macro To Move The Valid Offset Back By `u4NumOfUnits` ***
*** Note : Write Offset Is Set To `u4NumOfUnits` Read     ***
*** Offset is set to 0                                     ***
**************************************************************/
#define CRU_BMC_Move_Back_Valid_Offset(pBufChain,u4NumOfUnits) \
        CRU_BUF_Prepend_BufChain((pBufChain),NULL,(u4NumOfUnits));

/******************************************************
*** Macro To Get The Pointer To Message Information ***
*** Given The Message Buffer Chain Descriptor       ***
*******************************************************/
#define CRU_BUF_Get_ModuleData(pBufChain) (&(pBufChain)->ModuleData)

/*************************************************************
*** MACROS TO EXTRACT THE FIELDS FROM THE MODULE-DATA AREA ***
**************************************************************/
#define CRU_BUF_Get_SourceModuleId(pBufChain) \
        ((pBufChain)->ModuleData.u1_SourceModuleId)

#define CRU_BUF_Get_DestinModuleId(pBufChain) \
       ((pBufChain)->ModuleData.u1_DestinModuleId)

#define CRU_BUF_Get_InterfaceId(pBufChain) \
       ((pBufChain)->ModuleData.InterfaceId)

#define CRU_BUF_Get_U2Reserved(pBufChain) \
        ((pBufChain)->ModuleData.u2Reserved)

#define CRU_BUF_Get_U4Reserved1(pBufChain) \
        ((pBufChain)->ModuleData.u4Reserved1)

#define CRU_BUF_Get_U4Reserved2(pBufChain) \
        ((pBufChain)->ModuleData.u4Reserved2)

#define CRU_BUF_Get_U4Reserved3(pBufChain) \
        ((pBufChain)->ModuleData.u4Reserved3)

/********************************************************
*** MACROS TO FILL THE FIELDS IN THE MODULE-DATA AREA ***
*********************************************************/
#define CRU_BUF_Set_SourceModuleId(pBufChain,u1ModuleId) \
        ((pBufChain)->ModuleData.u1_SourceModuleId = (u1ModuleId))

#define CRU_BUF_Set_DestinModuleId(pBufChain,u1ModuleId) \
        ((pBufChain)->ModuleData.u1_DestinModuleId = (u1ModuleId))

#define CRU_BUF_Set_InterfaceId(pBufChain,IfIdStruct) \
        ((pBufChain)->ModuleData.InterfaceId = (IfIdStruct))

#define CRU_BUF_Set_U2Reserved(pBufChain,u2Resv) \
        ((pBufChain)->ModuleData.u2Reserved = (u2Resv))

#define CRU_BUF_Set_U4Reserved1(pBufChain,u4Resv1) \
        ((pBufChain)->ModuleData.u4Reserved1 = (u4Resv1))

#define CRU_BUF_Set_U4Reserved2(pBufChain,u4Resv2) \
        ((pBufChain)->ModuleData.u4Reserved2 = (u4Resv2))

#define CRU_BUF_Set_U4Reserved3(pBufChain,u4Resv3) \
        ((pBufChain)->ModuleData.u4Reserved3 = (u4Resv3))

/*******************************************************
**** Easy Macros For Interface Structure Components ****
********************************************************/
#define CRU_BUF_Get_Interface_Type(tInterface)  ((tInterface).u1_InterfaceType)
#define CRU_BUF_Get_Interface_Num(tInterface)   ((tInterface).u1_InterfaceNum)
#define CRU_BUF_Get_Interface_SubRef(tInterface) ((tInterface).u2_SubReferenceNum)
#define CRU_BUF_Get_IfIndex(tInterface)         ((tInterface).u4IfIndex)

#define CRU_BUF_Set_Interface_Type(tInterface, u1IfType)\
           ((tInterface).u1_InterfaceType = u1IfType)

#define CRU_BUF_Set_Interface_Num(tInterface, u1IfNum) \
           ((tInterface).u1_InterfaceNum = u1IfNum)

#define CRU_BUF_Set_Interface_SubRef(tInterface, u2IfSubRef) \
           ((tInterface).u2_SubReferenceNum = u2IfSubRef)

        /* Set both the old and new fields which hold the i/f index
         * Upon migration to the u4IfIndex field, remove IfNum.
         */
#define CRU_BUF_Set_IfIndex(tInterface, u4IfNum)            \
           ((tInterface).u1_InterfaceNum = (tInterface).u4IfIndex =\
            (UINT1)u4IfNum)

/*******************************************************
*** MACROs For Host To Network Byte Order Conversion ***
********************************************************/
#define CRU_BMC_WTOPDU(pu1PduAddr,u2Value) \
        *((UINT2 *)(pu1PduAddr)) = OSIX_HTONS(u2Value);

#define CRU_BMC_DWTOPDU(pu1PduAddr,u4Value) \
        *((UINT4 *)(pu1PduAddr)) = OSIX_HTONL(u4Value);

/*******************************************************
*** MACROs For Network To Host Byte Order Conversion ***
********************************************************/
#define CRU_BMC_WFROMPDU(pu1PduAddr) \
        OSIX_NTOHS(*((UINT2 *)(pu1PduAddr)))

#define CRU_BMC_DWFROMPDU(pu1PduAddr) \
        OSIX_NTOHL(*((UINT4 *)(pu1PduAddr)))

/*******************************************************
*** Macro To Get The Pointer To The First Valid Data ***
*** Byte In the Message Buffer-Chain.                ***
********************************************************/
#define CRU_BMC_Get_DataPointer(pBufChain) \
        ((pBufChain)->pu1_FirstValidByte)

/*********************************************************
***********     Prototype  Declaration  ******************
**********************************************************/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


INT4          BufInitManager                     (tBufConfig *pBufCfg);

INT4          CRU_BUF_Shutdown_Manager           (void);

tCruBufHdr *  CRU_BUF_Allocate_MsgBufChain       (UINT4 u4Size, UINT4 u4Offset);

INT4          CRU_BUF_Release_MsgBufChain        (tCruBufHdr *pBuf,
                                                  UINT1 u1Force);

INT4          CRU_BUF_Copy_OverBufChain          (tCruBufHdr *pBuf,
                                                  UINT1 *pu1Src,
                                                  UINT4 u4Offset, UINT4 u4Size);

INT4          CRU_BUF_Copy_FromBufChain          (tCruBufHdr *pBuf,
                                                  UINT1 *pu1Dst,
                                                  UINT4 u4Offset, UINT4 u4Size);

INT4          CRU_BUF_Link_BufChains             (tCruBufHdr *pBufChain1,
                                                  tCruBufHdr *pBufChain2);

tCruBufHdr *  CRU_BUF_UnLink_BufChain            (tCruBufHdr *pBufChain);

INT4          CRU_BUF_Concat_MsgBufChains        (tCruBufHdr *pBuf1,
                                                  tCruBufHdr *pBuf2);
INT4          CRU_BUF_Fragment_BufChain          (tCruBufHdr *pBuf,
                                                  UINT4 u4Size,
                                                  tCruBufHdr **ppFragBuf);

INT4          CRU_BUF_Prepend_BufChain           (tCruBufHdr *pBuf,
                                                  UINT1 *pu1Src,
                                                  UINT4 u4Size);

UINT4         CRU_BUF_Get_ChainValidByteCount    (tCruBufHdr *pBuf);

INT4          CRU_BUF_Move_ValidOffset           (tCruBufHdr *pBuf,
                                                  UINT4 u4Units);

UINT1      *  CRU_BUF_Get_DataPtr_IfLinear       (tCruBufHdr *pBuf,
                                                  UINT4 u4Offset,
                                                  UINT4 u4Size);

VOID          CRU_BUF_Print_AllStats             (VOID);

tCruBufHdr *  CRU_BUF_Duplicate_BufChain         (tCruBufHdr *pBuf);

tCruBufHdr *  CRU_BUF_Delete_BufChainAtEnd       (tCruBufHdr *pBuf,
                                                  UINT4 u4Size);

VOID          UtlDmpMsg                          (UINT4 u4CurType,
                                                  UINT4 u4CurDirn,
                                                  tCruBufHdr *pBuf,
                                                  UINT4 u4Type, UINT4 u4Dirn,
                                                  UINT4 u4Len, UINT4 u4HdrReqd);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif  /* _SRMBUF_H */
