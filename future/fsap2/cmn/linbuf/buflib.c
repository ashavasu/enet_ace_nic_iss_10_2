/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: buflib.c,v 1.8 2015/04/28 12:00:02 siva Exp $
 *
 * Description:
 *    This file implements fsap buffer managment APIs using
 *    a linear buffer.
 */

#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osix.h"
#include "utlmacro.h"

tCruBufPool         gCruBufPool;
tOsixSemId          gCruSem = 0;

/* These definitions allow the system heap to hold 5 packets of 65536 bytes */
#define CRU_BLK_MIN   512
#define CRU_NUM_BLKS  32000

/* Some applications assume that the library will take care of allocating */
/* memory if a prepend operation requires extra bytes in the beginning of */
/* the buffer. To avoid breaking existing code, we always this amount of  */
/* extra space at the beginning of the buffer.                            */
#define CRU_MIN_HDR   128

INT4
BufInitManager (tBufConfig * pBufCfg)
{
    tCruBufPool        *pPool;
    UINT4               u4Size, u4Blks, u4BlkMin;
    tCruFreeHdr        *pHdr;
    tCruFreeBlk        *pFreeBlk;

    /* In future, if multiple buffer pools are needed, allocate the structure */
    /* for a new pool here. Code after this can remain unchanged except sem.  */
    pPool = &gCruBufPool;

    if (gCruSem)                /* library has already been initialized */
    {
        return (CRU_FAILURE);
    }

    if (OsixSemCrt ("CRU", &gCruSem) == OSIX_FAILURE)
    {
        return (CRU_FAILURE);
    }
    OsixSemGive (gCruSem);

    u4BlkMin = CRU_BLK_MIN;
    u4Blks = CRU_NUM_BLKS;

    pPool->u4BlkMin = u4BlkMin;
    pPool->u4Allocs = 0;
    pPool->u4Frees = 0;
    pPool->u4AllocFails = 0;
    pPool->u4FreeFails = 0;

    u4Size =
        (u4BlkMin * u4Blks) + (sizeof (tCruBufHdr) * u4Blks) +
        sizeof (tCruFreeBlk);
    if ((pPool->pu1Heap = (UINT1 *) MEM_MALLOC (u4Size, UINT1)) == NULL)
    {
        OsixSemDel (gCruSem);
        gCruSem = 0;
        return (CRU_FAILURE);
    }

    /* Create a linked list of free buffer headers */
    pPool->pFreeHdr = (tCruFreeHdr *) (pPool->pu1Heap);
    pPool->pFreeBlk =
        (tCruFreeBlk *) ((pPool->pu1Heap) + (sizeof (tCruBufHdr) * u4Blks));
    for (pHdr = pPool->pFreeHdr; pHdr != NULL; pHdr = pHdr->pNxt)
    {
        pHdr->pNxt = (tCruFreeHdr *) ((UINT1 *) pHdr + sizeof (tCruBufHdr));
        pHdr->pNxt = ((UINT1 *) (pHdr->pNxt) == (UINT1 *) (pPool->pFreeBlk)) ?
            NULL : pHdr->pNxt;
    }

    /* At first there are two free blocks - one of size tCruFreeBlk and   */
    /* the other of size u4BlkMin*u4Blks. We assume that CRU_BLK_MIN is   */
    /* greater than sizeof(tCruBlk) so that 1st block is never allocated. */
    /* This just makes the code to allocate and release blocks simpler    */
    /* and more efficient for runtime performance. As allocation and free */
    /* happens, the 2nd block may get split up into many blocks.          */
    pFreeBlk = pPool->pFreeBlk;
    pFreeBlk->u4Size = 0;        /*sizeof(tCruFreeBlk); */
    pFreeBlk->pNxt =
        (tCruFreeBlk *) ((UINT1 *) pFreeBlk + sizeof (tCruFreeBlk));

    pFreeBlk = pFreeBlk->pNxt;
    pFreeBlk->u4Size = (u4BlkMin * u4Blks);
    pFreeBlk->pNxt = NULL;

    return (CRU_SUCCESS);
}

INT4
CRU_BUF_Shutdown_Manager ()
{
    tCruBufPool        *pPool;

    /* In future, if multiple buffer pools are needed, allocate the structure */
    /* for a new pool here. Code after this can remain unchanged except sem.  */
    pPool = &gCruBufPool;

    if (gCruSem == 0)            /* library has already been shutdown */
    {
        OsixSemDel (gCruSem);
        gCruSem = 0;
        MEM_FREE (pPool->pu1Heap);
        pPool->u4BlkMin = 0;
        pPool->u4Allocs = 0;
        pPool->u4Frees = 0;
        pPool->u4AllocFails = 0;
        pPool->u4FreeFails = 0;
        pPool->pFreeHdr = NULL;
        pPool->pFreeBlk = NULL;
        return (CRU_SUCCESS);
    }
    return (CRU_FAILURE);
}

tCruBufHdr         *
CRU_BUF_Allocate_MsgBufChain (UINT4 u4Size, UINT4 u4Offset)
{
    tCruFreeBlk        *pBlk = NULL;
    tCruFreeBlk        *pNxt = NULL;
    tCruFreeBlk        *pPrv = NULL;
    tCruBufHdr         *pBuf = NULL;
    tCruBufPool        *pPool;
    UINT4               u4Blks;

    /* In future, if multiple buffer pools are needed, allocate the structure */
    /* for a new pool here. Code after this can remain unchanged except sem.  */
    pPool = &gCruBufPool;

    /* Some applications assume that the library will take care of allocating */
    /* memory if a prepend operation requires extra bytes in the beginning of */
    /* the buffer. To avoid breaking existing code, we leave this amount of   */
    /* extra space always at the beginning of the buffer.                     */
    u4Offset += CRU_MIN_HDR;
    u4Size += CRU_MIN_HDR;

    /* Allocate only in multiples of the minimum block size */
    u4Blks = u4Size / CRU_BLK_MIN;
    u4Blks = (u4Size % CRU_BLK_MIN) ? (u4Blks + 1) : u4Blks;
    u4Size = (u4Blks * CRU_BLK_MIN);

    if (OsixSemTake (gCruSem) == OSIX_FAILURE)
    {
        (pPool->u4AllocFails) += 1;
        return (NULL);
    }

    pBuf = (tCruBufHdr *) (pPool->pFreeHdr);
    if (pBuf == NULL)
    {
        (pPool->u4AllocFails) += 1;
        OsixSemGive (gCruSem);
        return (NULL);
    }
    pPool->pFreeHdr = (pPool->pFreeHdr)->pNxt;

    pPrv = pPool->pFreeBlk;
    for (pBlk = pPrv->pNxt; pBlk != NULL; pPrv = pBlk, pBlk = pBlk->pNxt)
    {
        if ((pBlk->u4Size) == u4Size)
        {
            pPrv->pNxt = pBlk->pNxt;
            break;
        }

        if ((pBlk->u4Size) > u4Size)
        {
            pNxt = (tCruFreeBlk *) ((UINT1 *) pBlk + u4Size);
            pNxt->pNxt = pBlk->pNxt;
            pPrv->pNxt = pNxt;
            pNxt->u4Size = ((pBlk->u4Size) - u4Size);
            break;
        }
    }

    if (pBlk != NULL)
    {
        pBuf->u4BufSize = u4Size;
        pBuf->u4_ValidByteCount = 0;
        pBuf->u4_FreeByteCount = u4Size - u4Offset;
        pBuf->pu1_FirstByte = (UINT1 *) pBlk;
        pBuf->pu1_FirstValidByte = pBuf->pu1_FirstByte + u4Offset;
        pBuf->pNext = NULL;
        pBuf->pFirstValidDataDesc = pBuf;
        MEMSET (&pBuf->ModuleData, 0, sizeof (tMODULE_DATA));
        (pPool->u4Allocs) += 1;
    }
    else
    {
        ((tCruFreeHdr *) pBuf)->pNxt = pPool->pFreeHdr;
        pPool->pFreeHdr = (tCruFreeHdr *) pBuf;
        (pPool->u4AllocFails) += 1;
        pBuf = NULL;
        printf ("alloc failed\n");
    }
    OsixSemGive (gCruSem);

    return (pBuf);
}

INT4
CRU_BUF_Release_MsgBufChain (tCruBufHdr * pBuf, UINT1 u1Force)
{
    tCruBufPool        *pPool = &gCruBufPool;
    tCruFreeBlk        *pPrv = NULL;
    tCruFreeBlk        *pBlk = NULL;
    tCruFreeBlk        *pNxt = NULL;

    if (pBuf == NULL)
    {
        return CRU_FAILURE;
    }

    if (OsixSemTake (gCruSem) == OSIX_FAILURE)
    {
        (pPool->u4FreeFails) += 1;
        return (CRU_FAILURE);
    }

    u1Force = u1Force = 0;        /* not used; avoid compiler warnings. */

    /* In future, if multiple buffer pools are needed, allocate the structure */
    /* for a new pool here. Code after this can remain unchanged except sem.  */
    pPool = &gCruBufPool;

    /* First we release the block. Then we release the header. */
    pPrv = pPool->pFreeBlk;
    pBlk = (tCruFreeBlk *) (pBuf->pu1_FirstByte);
    pBlk->u4Size = pBuf->u4BufSize;    /* + sizeof(tCruBufHdr); */

    for (pNxt = pPrv->pNxt; (pNxt != NULL) && (pNxt < pBlk);
         pPrv = pNxt, pNxt = pNxt->pNxt);

    pBlk->pNxt = pNxt;
    pPrv->pNxt = pBlk;

    /* If previous block and this released block are adjacent, join them. */
    if (((UINT1 *) pPrv + (pPrv->u4Size)) == (UINT1 *) pBlk)
    {
        (pPrv->u4Size) += (pBlk->u4Size);
        pPrv->pNxt = pBlk->pNxt;
        pBlk = pPrv;
    }

    /* If next block and this released block are adjacent, join them. If    */
    /* previous block, released block and next block are all adjacent, this */
    /* logic will result in all three being joined into one free block.     */
    if (((UINT1 *) pBlk + (pBlk->u4Size)) == (UINT1 *) pNxt)
    {
        (pBlk->u4Size) += (pNxt->u4Size);
        pBlk->pNxt = pNxt->pNxt;
    }

    /* Release the header - place it at start of free headers list. */
    ((tCruFreeHdr *) pBuf)->pNxt = pPool->pFreeHdr;
    pPool->pFreeHdr = (tCruFreeHdr *) pBuf;

    (pPool->u4Frees) += 1;
    OsixSemGive (gCruSem);
    return (CRU_SUCCESS);
}

INT4
CRU_BUF_Copy_OverBufChain (tCruBufHdr * pBuf, UINT1 *pu1Src,
                           UINT4 u4Offset, UINT4 u4Size)
{
    MEMCPY ((pBuf->pu1_FirstValidByte) + u4Offset, pu1Src, u4Size);
    if ((u4Size + u4Offset) > pBuf->u4_ValidByteCount)
    {
        pBuf->u4_ValidByteCount = u4Size + u4Offset;

        pBuf->u4_FreeByteCount = (pBuf->u4BufSize) -
            (pBuf->pu1_FirstValidByte - pBuf->pu1_FirstByte) -
            (pBuf->u4_ValidByteCount);
    }

    return (CRU_SUCCESS);
}

INT4
CRU_BUF_Copy_FromBufChain (tCruBufHdr * pBuf, UINT1 *pu1Dst,
                           UINT4 u4Offset, UINT4 u4Size)
{
    UINT4               u4Data = (pBuf->u4_ValidByteCount) - u4Offset;

    u4Size = (u4Size <= u4Data) ? u4Size : u4Data;
    MEMCPY (pu1Dst, (pBuf->pu1_FirstValidByte) + u4Offset, u4Size);
    return (u4Size);
}

INT4
CRU_BUF_Concat_MsgBufChains (tCruBufHdr * pBuf1, tCruBufHdr * pBuf2)
{
    UINT4               u4Offset, u4Size;
    tCruBufHdr         *pBuf;

    u4Offset = (pBuf1->pu1_FirstValidByte) - (pBuf1->pu1_FirstByte);
    u4Size = u4Offset + (pBuf1->u4_ValidByteCount) + (pBuf2->u4_ValidByteCount);

    if ((pBuf1->u4BufSize) >= u4Size)
    {
        MEMCPY ((pBuf1->pu1_FirstValidByte) + pBuf1->u4_ValidByteCount,
                pBuf2->pu1_FirstValidByte, pBuf2->u4_ValidByteCount);
        pBuf1->u4_ValidByteCount += pBuf2->u4_ValidByteCount;
        pBuf1->u4_FreeByteCount -= pBuf2->u4_ValidByteCount;
        /*
           pBuf1->u4_FreeByteCount  = pBuf1->u4BufSize - pBuf2->u4_ValidByteCount;
         */
        CRU_BUF_Release_MsgBufChain (pBuf2, 0);
        return (CRU_SUCCESS);
    }
    else
    {
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, u4Offset)) == NULL)
        {
            return (CRU_FAILURE);
        }

        MEMCPY (pBuf->pu1_FirstByte, pBuf1->pu1_FirstByte, u4Offset);
        MEMCPY (pBuf->pu1_FirstValidByte, pBuf1->pu1_FirstValidByte,
                pBuf1->u4_ValidByteCount);
        MEMCPY ((pBuf->pu1_FirstValidByte) + pBuf1->u4_ValidByteCount,
                pBuf2->pu1_FirstValidByte, pBuf2->u4_ValidByteCount);
        pBuf->u4_ValidByteCount =
            (pBuf1->u4_ValidByteCount) + (pBuf2->u4_ValidByteCount);
        pBuf->u4_FreeByteCount -= pBuf->u4_ValidByteCount;

        memcpy (pBuf1, pBuf, sizeof (tCruBufHdr));

        CRU_BUF_Release_MsgBufChain (pBuf2, 0);
        return (CRU_SUCCESS);
    }
}

INT4
CRU_BUF_Fragment_BufChain (tCruBufHdr * pBuf, UINT4 u4Size,
                           tCruBufHdr ** ppFragBuf)
{
    tCruBufHdr         *pNewBuf;

    pNewBuf = CRU_BUF_Allocate_MsgBufChain ((pBuf->u4BufSize) - u4Size -
                                            CRU_MIN_HDR,
                                            (pBuf->pu1_FirstValidByte) -
                                            (pBuf->pu1_FirstByte) -
                                            CRU_MIN_HDR);
    if (pNewBuf == NULL)
    {
        return (CRU_FAILURE);
    }
    pNewBuf->pu1_FirstValidByte =
        (pNewBuf->pu1_FirstByte) + ((pBuf->pu1_FirstValidByte) -
                                    (pBuf->pu1_FirstByte));

    pNewBuf->u4_ValidByteCount = (pBuf->u4_ValidByteCount) - u4Size;
    pNewBuf->u4_FreeByteCount -= pNewBuf->u4_ValidByteCount;
    MEMCPY (pNewBuf->pu1_FirstValidByte,
            (UINT1 *) (pBuf->pu1_FirstValidByte) + u4Size,
            (pBuf->u4_ValidByteCount) - u4Size);
    pBuf->u4_ValidByteCount = u4Size;
    *ppFragBuf = pNewBuf;
    return (CRU_SUCCESS);
}

INT4
CRU_BUF_Prepend_BufChain (tCruBufHdr * pBuf, UINT1 *pu1Src, UINT4 u4Size)
{
    if (u4Size > (UINT4) ((pBuf->pu1_FirstValidByte) - (pBuf->pu1_FirstByte)))
    {
        return (CRU_FAILURE);
    }
    (pBuf->pu1_FirstValidByte) = (pBuf->pu1_FirstValidByte) - u4Size;
    pBuf->u4_ValidByteCount += u4Size;
    if (pu1Src)
    {
        MEMCPY (pBuf->pu1_FirstValidByte, pu1Src, u4Size);
    }
    return (CRU_SUCCESS);
}

tCruBufHdr         *
CRU_BUF_Duplicate_BufChain (tCruBufHdr * pBuf)
{
    tCruBufHdr         *pNewBuf;

    pNewBuf = CRU_BUF_Allocate_MsgBufChain (pBuf->u4BufSize,
                                            (pBuf->pu1_FirstValidByte) -
                                            (pBuf->pu1_FirstByte));
    if (pNewBuf == NULL)
    {
        return (NULL);
    }
    pNewBuf->pu1_FirstValidByte =
        (pNewBuf->pu1_FirstByte) + ((pBuf->pu1_FirstValidByte) -
                                    (pBuf->pu1_FirstByte));
    pNewBuf->u4_ValidByteCount = pBuf->u4_ValidByteCount;
    pNewBuf->u4_FreeByteCount = pBuf->u4_FreeByteCount;
    MEMCPY (pNewBuf->pu1_FirstByte, pBuf->pu1_FirstByte, pBuf->u4BufSize);
    MEMCPY (&pNewBuf->ModuleData, &pBuf->ModuleData, sizeof (tMODULE_DATA));

    return (pNewBuf);
}

UINT4
CRU_BUF_Get_ChainValidByteCount (tCruBufHdr * pBuf)
{
    return (pBuf->u4_ValidByteCount);
}

tCruBufHdr         *
CRU_BUF_Delete_BufChainAtEnd (tCruBufHdr * pBuf, UINT4 u4Size)
{
    if (u4Size < (pBuf->u4_ValidByteCount))
    {
        (pBuf->u4_ValidByteCount) -= u4Size;
        (pBuf->u4_FreeByteCount) += u4Size;
        return (pBuf);
    }
    CRU_BUF_Release_MsgBufChain (pBuf, 0);
    return (NULL);
}

INT4
CRU_BUF_Move_ValidOffset (tCruBufHdr * pBuf, UINT4 u4Units)
{
    if (u4Units <=
        ((pBuf->u4BufSize) - (pBuf->pu1_FirstValidByte - pBuf->pu1_FirstByte)))
    {
        pBuf->pu1_FirstValidByte += u4Units;
        if ((pBuf->u4_ValidByteCount) <= u4Units)
        {
            /* Here the offset after the 'move' falls beyond the
             * range of valid bytes.
             */
            pBuf->u4_FreeByteCount -= (u4Units - pBuf->u4_ValidByteCount);
            pBuf->u4_ValidByteCount = 0;
        }
        else
        {
            /* Here the offset after the 'move' falls within the 
             * range of valid bytes. So only update the ValidByteCount.
             * FreeByteCount remains as-is
             */
            pBuf->u4_ValidByteCount -= u4Units;
        }
        return (CRU_SUCCESS);
    }
    return (CRU_FAILURE);
}

UINT1              *
CRU_BUF_Get_DataPtr_IfLinear (tCruBufHdr * pBuf, UINT4 u4Offset, UINT4 u4Size)
{
    if ((u4Offset + u4Size) <= (pBuf->u4_ValidByteCount))
    {
        return ((pBuf->pu1_FirstValidByte) + u4Offset);
    }
    return (NULL);
}

VOID                CRU_BUF_Print_AllStats
ARG_LIST ((VOID))
{
    UINT1               au1Buf[100];

    sprintf ((CHR1 *) au1Buf, "|**********BUFFER MANAGER STATS***********|\n");
    sprintf ((CHR1 *) au1Buf,
             "ALLOCATED          : %d    FREED        : %d \n",
             gCruBufPool.u4Allocs, gCruBufPool.u4Frees);
    sprintf ((CHR1 *) au1Buf,
             "FAILED ALLOCATIONS : %d    FAILED FREES : %d \n",
             gCruBufPool.u4AllocFails, gCruBufPool.u4FreeFails);
    sprintf ((CHR1 *) au1Buf, "|**********BUFFER MANAGER STATS***********|\n");
}

INT4                CRU_BUF_Link_BufChains
ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBufChain1,
           tCRU_BUF_CHAIN_HEADER * pBufChain2))
{
    pBufChain1 = pBufChain1;
    pBufChain2 = pBufChain2;
    return (CRU_SUCCESS);
}

tCRU_BUF_CHAIN_HEADER *
CRU_BUF_UnLink_BufChain (tCRU_BUF_CHAIN_HEADER * pBufChain)
{
    pBufChain = pBufChain;
    return (CRU_SUCCESS);
}
void                ShowFreeBlks (void);
void
ShowFreeBlks (void)
{
    tCruFreeBlk        *pBlk;

    for (pBlk = gCruBufPool.pFreeBlk; pBlk; pBlk = pBlk->pNxt)
    {
        printf ("[%x] - %d\n", (UINT4) pBlk, pBlk->u4Size);
    }
}

VOID
UtlDmpMsg (UINT4 u4CurType, UINT4 u4CurDirn, tCRU_BUF_CHAIN_HEADER * pBuf,
           UINT4 u4Type, UINT4 u4Dirn, UINT4 u4Len, UINT4 u4HdrReqd)
{
    u4CurType = u4CurType;
    u4CurDirn = u4CurDirn;
    pBuf = pBuf;
    u4Type = u4Type;
    u4Dirn = u4Dirn;
    u4Len = u4Len;
    u4HdrReqd = u4HdrReqd;
    return;
}
