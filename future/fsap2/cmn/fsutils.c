/* 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsutils.c,v 1.39 2017/06/22 13:44:53 siva Exp $
 *
 * Description: Implementation of some C library calls.
 *              These functions are mostly never directly used.
 *              Actual usage is via macros defined in cmn/utlmacro.h.
 *
 */
#include "osxinc.h"
#include <stdarg.h>

static INT4         UtlStrNumCmp (const CHR1 * s1, const CHR1 * s2);
static UINT4        gu4TicksCorr;
UINT4               gu4TimerSpeed;

#define   UNUSED_PARAM(x)   ((VOID)x)

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlSnprintf                                      */
/*                                                                          */
/*    Description        : An Implementation of snprintf function.          */
/*                         Note: This is not a full implementation.         */
/*                         i/e, not all fmt. specifiers are supported       */
/*                         Currently supports 'c' 'd' 'x' 's'.              */
/*                         Using any other fmt. spec. than the above        */
/*                         would result in undefined behaviour.             */
/*                                                                          */
/*    Input(s)           : pc1Buf - buffer which contains the formatted str */
/*                                upon return from function call.           */
/*                         u4Sz - Limit of bytes to write.                  */
/*                         c1fmt  - fmt spec. + args                        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : No. of bytes written.                            */
/*                         or would have been written in case of overflow   */
/*                         See man snprintf for details.                    */
/*                                                                          */
/****************************************************************************/
UINT4
UtlSnprintf (CHR1 * pc1Buf, UINT4 u4Sz, const CHR1 * c1fmt, ...)
{
    va_list             args;
    UINT4               u4Size;
    va_start (args, c1fmt);
    u4Size = UtlVsnprintf (pc1Buf, u4Sz, c1fmt, args);
    va_end (args);
    return (u4Size);
}

/**
 * UtlVsnprintf -- an implementation of snprintf.
 */

/* Constants used in the implementation of UtlVsnprintf */
#define ZEROPAD 1                /* pad with zero */
#define SIGN    2                /* unsigned/signed long */
#define PLUS    4                /* show plus */
#define SPACE   8                /* space if plus */
#define LEFT    16                /* left justified */
#define SPECIAL 32                /* 0x */
#define LARGE   64                /* use 'ABCDEF' instead of 'abcdef' */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlConverNumber                                  */
/*                                                                          */
/*    Description        : Non-exported function called from UtlVsnprintf   */
/*                         Converts the number i4Num to a string based on   */
/*                         input the radix, Fieldwidth, Flags.              */
/*    Input(s)           : pc1Buf - buffer which contains the formatted str */
/*                                  upon return from function call.         */
/*                         pc1End - End of the buffer                       */
/*                         i4Num  - The no. to be converted.                */
/*                         i4Base - The radix of i4Num.                     */
/*                         i4Size - The fieldwidth.                         */
/*                         i4Precision - Precision.                         */
/*                         i4Type - Flags set in UtlSnprintf                */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : The buffer pc1Buf containing the string          */
/*                         representation of the number i4Num.              */
/*                                                                          */
/****************************************************************************/
static CHR1        *
UtlConverNumber (CHR1 * pc1Buf, CHR1 * pc1End, INT4 i4Num, INT4 i4Base,
                 INT4 i4Size, INT4 i4Precision, INT4 i4Type, INT4 isUnsigned)
{
    CHR1                c1Pad, c1Sign, ac1TmpBuf[66];
    const CHR1         *pc1Digits;
    const CHR1          ac1SmallDigits[] =
        "0123456789abcdefghijklmnopqrstuvwxyz";
    const CHR1          ac1LargeDigits[] =
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    INT4                i4Idx;
    UINT4               u4Remainder;
    UINT4               u4Num;

    pc1Digits = (i4Type & LARGE) ? ac1LargeDigits : ac1SmallDigits;
    if (i4Type & LEFT)
        i4Type &= ~ZEROPAD;
    if (i4Base < 2 || i4Base > 36)
        return (NULL);
    c1Pad = (i4Type & ZEROPAD) ? '0' : ' ';
    c1Sign = 0;
    if (i4Type & SIGN)
    {
        if (i4Num < 0)
        {
            c1Sign = '-';
            i4Num = -i4Num;
            i4Size--;
        }
        else if (i4Type & PLUS)
        {
            c1Sign = '+';
            i4Size--;
        }
        else if (i4Type & SPACE)
        {
            c1Sign = ' ';
            i4Size--;
        }
    }
    if (i4Type & SPECIAL)
    {
        if (i4Base == 16)
            i4Size -= 2;
        else if (i4Base == 8)
            i4Size--;
    }

    i4Idx = 0;
    if (i4Num == 0)
        ac1TmpBuf[i4Idx++] = '0';
    else
    {
        if (isUnsigned)
        {
            u4Num = (UINT4) i4Num;
            while (u4Num != 0)
            {
                u4Remainder = u4Num % i4Base;
                u4Num = u4Num / i4Base;
                ac1TmpBuf[i4Idx++] = pc1Digits[u4Remainder];
            }
        }
        else
        {
            while (i4Num != 0)
            {
                u4Remainder = i4Num % i4Base;
                i4Num = i4Num / i4Base;
                ac1TmpBuf[i4Idx++] = pc1Digits[u4Remainder];
            }
        }
    }

    if (i4Idx > i4Precision)
        i4Precision = i4Idx;
    i4Size -= i4Precision;
    if (!(i4Type & (ZEROPAD + LEFT)))
    {
        while (i4Size-- > 0)
        {
            if (pc1Buf <= pc1End)
                *pc1Buf = ' ';
            ++pc1Buf;
        }
    }
    if (c1Sign)
    {
        if (pc1Buf <= pc1End)
            *pc1Buf = c1Sign;
        ++pc1Buf;
    }
    if (i4Type & SPECIAL)
    {
        if (i4Base == 8)
        {
            if (pc1Buf <= pc1End)
                *pc1Buf = '0';
            ++pc1Buf;
        }
        else if (i4Base == 16)
        {
            if (pc1Buf <= pc1End)
                *pc1Buf = '0';
            ++pc1Buf;
            if (pc1Buf <= pc1End)
                *pc1Buf = pc1Digits[33];
            ++pc1Buf;
        }
    }
    if (!(i4Type & LEFT))
    {
        while (i4Size-- > 0)
        {
            if (pc1Buf <= pc1End)
                *pc1Buf = c1Pad;
            ++pc1Buf;
        }
    }
    while (i4Idx < i4Precision--)
    {
        if (pc1Buf <= pc1End)
            *pc1Buf = '0';
        ++pc1Buf;
    }
    while (i4Idx-- > 0)
    {
        if (pc1Buf <= pc1End)
            *pc1Buf = ac1TmpBuf[i4Idx];
        ++pc1Buf;
    }
    while (i4Size-- > 0)
    {
        if (pc1Buf <= pc1End)
            *pc1Buf = ' ';
        ++pc1Buf;
    }
    return (pc1Buf);
}

/****************************************************************************/
/* Internal function called from UtlVsnprintf.                              */
/* Does an intelligent atoi (by skipping spaces).                           */
/****************************************************************************/

static int
UtlGetNumFromStr (const char **s)
{
    INT4                i4Val = 0;

    while (isdigit (**s))
        i4Val = i4Val * 10 + *((*s)++) - '0';
    return i4Val;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlVsnprintf                                     */
/*                                                                          */
/*    Description        : An Implementation of vsnprintf function.         */
/*                         Note: This is not a full implementation.         */
/*                         i/e, not all fmt. specifiers are supported       */
/*                         Currently supports 'c' 'd' 'x' 's'.              */
/*                         Using any other fmt. spec. than the above        */
/*                         would result in undefined behaviour.             */
/*                         Supports precision, justification & fieldwidth.  */
/*                                                                          */
/*    Input(s)           : pc1Buf - buffer which contains the formatted str */
/*                                upon return from function call.           */
/*                         u4Sz - Limit of bytes to write.                  */
/*                         c1fmt  - fmt spec.                               */
/*                         args - argument list                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : No. of bytes written.                            */
/*                         or would have been written in case of overflow   */
/*                                                                          */
/****************************************************************************/
UINT4
UtlVsnprintf (CHR1 * pc1Buf, UINT4 u4Sz, const CHR1 * c1fmt, va_list args)
{
    UINT4               u4Idx;
    UINT4               u4Len;
    UINT4               u4Flags;
    UINT4               u4Base;
    UINT4               u4Num;
    INT4                i4FieldWidth;
    INT4                i4Precision;
    INT4                i4Qualifier;
    INT4                i4IsUnsigned;
    CHR1                c1val;
    CHR1                c1FmtFlag;
    CHR1                c1Type = '\0';
    CHR1               *pc1Str, *pc1End;
    const CHR1         *pc1StrVal;
    CHR1                c1FltFormat[64] = { '\0' };
    DBL8                d8FlNum;
    CHR1                ac1FltStr[64] = { '\0' };

    pc1Str = pc1Buf;
    pc1End = pc1Buf + u4Sz - 1;

    for (; *c1fmt; ++c1fmt)
    {
        if (*c1fmt != '%')
        {
            if (pc1Str <= pc1End)
                *pc1Str = *c1fmt;
            ++pc1Str;
            continue;
        }

        /* process flags */
        u4Flags = 0;
      repeat:
        ++c1fmt;                /* this also skips first '%' */
        switch (*c1fmt)
        {
            case '-':
                u4Flags |= LEFT;
                goto repeat;
            case '+':
                u4Flags |= PLUS;
                goto repeat;
            case ' ':
                u4Flags |= SPACE;
                goto repeat;
            case '#':
                u4Flags |= SPECIAL;
                goto repeat;
            case '0':
                u4Flags |= ZEROPAD;
                goto repeat;
        }

        /* get field width */
        i4FieldWidth = -1;
        if (isdigit (*c1fmt))
            i4FieldWidth = UtlGetNumFromStr (&c1fmt);
        else if (*c1fmt == '*')
        {
            ++c1fmt;
            /* it's the next argument */
            i4FieldWidth = (INT4) va_arg (args, int);
            if (i4FieldWidth < 0)
            {
                i4FieldWidth = -i4FieldWidth;
                u4Flags |= LEFT;
            }
        }

        /* get the precision */
        i4Precision = -1;
        if (*c1fmt == '.')
        {
            ++c1fmt;
            if (isdigit (*c1fmt))
                i4Precision = UtlGetNumFromStr (&c1fmt);
            else if (*c1fmt == '*')
            {
                ++c1fmt;
                /* it's the next argument */
                i4Precision = (INT4) va_arg (args, int);
            }
            if (i4Precision < 0)
                i4Precision = 0;
        }

        /* get the conversion qualifier */
        i4Qualifier = -1;
        i4IsUnsigned = 0;
        if (*c1fmt == 'h' || *c1fmt == 'l' || *c1fmt == 'L')
        {
            i4Qualifier = *c1fmt;
            i4IsUnsigned = 1;
            ++c1fmt;
        }

        /* Merely transfer any sequence of %s */
        if (*c1fmt == '%')
        {
            c1FmtFlag = 0;
            while (*c1fmt == '%')
            {
                if (pc1Str <= pc1End)
                {
                    if (!c1FmtFlag)
                    {
                        *pc1Str = '%';
                        ++pc1Str;
                        c1FmtFlag = 1;
                    }
                    else
                    {
                        c1FmtFlag = 0;
                    }
                }
                ++c1fmt;
            }
            /* To balance the normal increment in the for loop above */
            --c1fmt;
            if (!c1FmtFlag)
                --c1fmt;
            continue;
        }

        /* default base */
        u4Base = 10;

        switch (*c1fmt)
        {
            case 'c':
                if (!(u4Flags & LEFT))
                {
                    while (--i4FieldWidth > 0)
                    {
                        if (pc1Str <= pc1End)
                            *pc1Str = ' ';
                        ++pc1Str;
                    }
                }

                c1val = (CHR1) va_arg (args, int);
                if (pc1Str <= pc1End)
                    *pc1Str = c1val;
                ++pc1Str;
                while (--i4FieldWidth > 0)
                {
                    if (pc1Str <= pc1End)
                        *pc1Str = ' ';
                    ++pc1Str;
                }
                continue;

            case 'd':
                u4Flags |= SIGN;
                break;

            case 'u':
                i4IsUnsigned = 1;
                break;

            case 'X':
                u4Flags |= LARGE;
            case 'x':
                u4Base = 16;
                i4IsUnsigned = 1;
                break;

            case 's':
                pc1StrVal = va_arg (args, char *);
                if (!pc1StrVal)
                    pc1StrVal = "<null>";

                u4Len = STRNLEN (pc1StrVal, i4Precision);

                if (!(u4Flags & LEFT))
                {
                    while ((INT4) u4Len < i4FieldWidth--)
                    {
                        if (pc1Str <= pc1End)
                            *pc1Str = ' ';
                        ++pc1Str;
                    }
                }

                for (u4Idx = 0; u4Idx < u4Len; u4Idx++)
                {
                    if (pc1Str <= pc1End)
                        *pc1Str = *pc1StrVal;
                    ++pc1Str;
                    ++pc1StrVal;
                }
                while ((INT4) u4Len < i4FieldWidth--)
                {
                    if (pc1Str <= pc1End)
                        *pc1Str = ' ';
                    ++pc1Str;
                }
                continue;

            case 'f':
            case 'e':
                u4Flags |= SIGN;
                if (*c1fmt == 'f')
                {
                    c1Type = 'f';
                }
                if (*c1fmt == 'e')
                {
                    c1Type = 'e';
                }
                d8FlNum = (DBL8) va_arg (args, double);
                if (i4FieldWidth != -1 || i4Precision != -1)
                {
                    if (u4Flags & LEFT)
                    {
                        if (i4Precision == -1)
                        {
                            SPRINTF (c1FltFormat, "%s%c%d%c", "%", '-',
                                     i4FieldWidth, c1Type);
                            SPRINTF (ac1FltStr, c1FltFormat, d8FlNum);
                        }
                        else
                        {
                            SPRINTF (c1FltFormat, "%s%c%d%c%d%c", "%", '-',
                                     i4FieldWidth, '.', i4Precision, c1Type);
                            SPRINTF (ac1FltStr, c1FltFormat, d8FlNum);
                        }

                    }
                    else
                    {
                        if (i4Precision == -1)
                        {
                            SPRINTF (c1FltFormat, "%s%d%c", "%", i4FieldWidth,
                                     c1Type);
                            SPRINTF (ac1FltStr, c1FltFormat, d8FlNum);
                        }
                        else
                        {
                            SPRINTF (c1FltFormat, "%s%d%c%d%c", "%",
                                     i4FieldWidth, '.', i4Precision, c1Type);
                            SPRINTF (ac1FltStr, c1FltFormat, d8FlNum);
                        }

                    }
                }
                else
                {
                    SPRINTF (c1FltFormat, "%s%c", "%", c1Type);
                    SPRINTF (ac1FltStr, c1FltFormat, d8FlNum);
                }
                u4Len = STRLEN (ac1FltStr);

                if (!(u4Flags & LEFT))
                {
                    while ((INT4) u4Len < i4FieldWidth--)
                    {
                        if (pc1Str <= pc1End)
                        {
                            *pc1Str = ' ';
                        }
                        ++pc1Str;
                    }
                }

                for (u4Idx = 0; u4Idx < u4Len; u4Idx++)
                {
                    if (pc1Str <= pc1End)
                    {
                        *pc1Str = ac1FltStr[u4Idx];
                    }
                    ++pc1Str;
                }
                while ((INT4) u4Len < i4FieldWidth--)
                {
                    if (pc1Str <= pc1End)
                    {
                        *pc1Str = ' ';
                    }
                    ++pc1Str;
                }

                continue;

            default:
                /* Assume it is 4 bytes and swallow it. */
                va_arg (args, int);
                continue;
        }

        if (i4Qualifier == 'l')
        {
            u4Num = (UINT4) va_arg (args, int);
        }
        else if (i4Qualifier == 'h')
        {
            u4Num = (UINT4) va_arg (args, int);
        }
        else
        {
            u4Num = (UINT4) va_arg (args, int);
        }

        pc1Str = UtlConverNumber (pc1Str, pc1End, u4Num, u4Base,
                                  i4FieldWidth, i4Precision, u4Flags,
                                  i4IsUnsigned);
        if (pc1Str == NULL)
            return 0;
    }

    /* Null terminate */
    if (pc1Str <= pc1End)
        *pc1Str = '\0';
    else
    {
        *pc1End = '\0';
    }

    /* Return the number of characters that 'would have been written'
     * This is as per the C99 standard and the snprintf man page on Linux
     */
    return (pc1Str - pc1Buf);
}

/*
 *
 * "Calendar implementation".
 * Functions to retrieve the current calendar time
 * in 'struct tm' format and as a date string.
 *
 * @Exported Funcs : UtlGetTime and UtlSetTime
 * @Exported types : tUtlTm
 *
 * @Usage:
 *     To get it to work, this library must first be 'seeded'.
 *     This involves, calling UtlSetTime to set the current
 *     calendar time. After this the library can sustain itself.
 *     tm_year stores the 4 digit year with year 2000 being
 *     used as the BASE year i/e, dates less than 2000 are not
 *     supported.
 *
 *     + To obtain the current time in 'tm' format use UtlGetTime.
 *     + To obtain a string representation call UtlGetTimeStr passing
 *      a buffer of suitable length.
 *      The display string is 20 bytes long
 *      Dec 31 23:59:58 2000
 *      So a buffer of atleast 21 bytes is required. 1 byte for
 *      null termination of string.
 *
 * @Seeding:
 *
 *     The values of 'tUtlTm' fields are the same as those
 *     of the standard 'struct tm', except for tm_year, which
 *     stores the 4 digit year. Years should be >= 2000.
 *     Ref /usr/include/time.h for 'struct tm' values.
 *
 *     On *nix, seeding can be done as follows:
 *
 *        time_t t;
 *        struct tm *tm;
 *        time(&t);
 *        tm = localtime(&t);
 *        tm.tm_year += (1900);
 *        UtlSetTime (tm);
 *
 *        strptime() is another alternative.
 *
 * @Note:
 *     Daylight savings and timezones are not supported.
 *
 */

/* The following structure stores the details of the day of the year on which
 * a given month starts for a normal year or a leap year. */
static const UINT2  au2DaysInMonth[2][13] = {
    /* Normal Year */
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},

    /* Leap Year   */
    {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}
};

const CHR1         *ac1Month[12] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

/* Globals that maintain state. */
static tUtlTm       gTmBuf;
extern UINT4        gu4Stups;
UINT4               gu4Secs;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlGetTimeStr                                    */
/*                                                                          */
/*    Description        : This function gets the time as a string.         */
/*                         The display string is 21 bytes long, including   */
/*                         null termination.                                */
/*                                                                          */
/*    Input(s)           : ac1TimeStr - Buffer of length atleast 21 bytes   */
/*                                                                          */
/*    Output(s)          : Filled Buffer.                                   */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/

VOID
UtlGetTimeStr (CHR1 ac1TimeStr[])
{
    tUtlTm              tm;

    UtlGetTime (&tm);
#define MAX_TIME_STR 21
    SNPRINTF ((CHR1 *) ac1TimeStr, MAX_TIME_STR, "%s %2u %.2u:%.2u:%.2u %4u",
              ac1Month[tm.tm_mon], tm.tm_mday, tm.tm_hour, tm.tm_min,
              tm.tm_sec, tm.tm_year);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlGetTimeStrForTicks                            */
/*                                                                          */
/*    Description        : This function gets the time as a string for a    */
/*                         given number of ticks.                           */
/*                         The display string is 21 bytes long, including   */
/*                         null termination.                                */
/*                                                                          */
/*    Input(s)           : ac1TimeStr - Buffer of length atleast 21 bytes   */
/*                                                                          */
/*    Output(s)          : Filled Buffer.                                   */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/
VOID
UtlGetTimeStrForTicks (UINT4 u4Ticks, CHR1 ac1TimeStr[])
{
    tUtlTm              tm;

    UtlGetTimeForTicks (u4Ticks, &tm);

    SNPRINTF ((CHR1 *) ac1TimeStr, TIME_STR_BUF_SIZE,
              "%s %2d %.2d:%.2d:%.2d %4d", ac1Month[tm.tm_mon],
              tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, tm.tm_year);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlGetTimeForTicks                               */
/*                                                                          */
/*    Description        : This function gets the tUtlTm fmt. time, given   */
/*                         the number of elapsed ticks.                     */
/*                                                                          */
/*    Input(s)           : tm - ptr to tUtlTm variable.                     */
/*                         utTicks - Elapsed Ticks (stups actually)         */
/*                                                                          */
/*    Output(s)          : Filled tm.                                       */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/
VOID
UtlGetTimeForTicks (UINT4 u4Ticks, tUtlTm * tm)
{
    UINT4               u4ElapsedSecs;
    UINT4               u4DaysInYear;
    UINT4               u4Days;
    UINT4               u4Leap;
    UINT4               u4Month;
    UINT4               u4Secs;
    tUtlTm              TmBuf;

    u4ElapsedSecs = u4Ticks / gu4Stups;
    u4Secs = gu4Secs + u4ElapsedSecs;
    u4Days = u4Secs / 86400;

    TmBuf = gTmBuf;

    /* 60 seconds in a day, 60 mins in a hour,
     * 24 hours in a day and 7 days in a week.
     * Being invariant, these are calculated straightaway.
     */
    TmBuf.tm_sec = u4Secs % 60;
    TmBuf.tm_min = (u4Secs / 60) % 60;
    TmBuf.tm_hour = (u4Secs / 3600) % 24;

    /* 01-01-2000 is a Saturday. So we add a bias of 6 */
    TmBuf.tm_wday = (u4Days + 6) % 7;

    TmBuf.tm_year = TM_BASE_YEAR;
    TmBuf.tm_mon = 0;
    TmBuf.tm_yday = 0;
    TmBuf.tm_mday = 0;
    TmBuf.tm_mon = 0;
    /* Number of days in a month/year varies.
     * So we evaluated it.
     * Using the elapsed days since the last call to this
     * function, we determine the current year.
     * The dayoftheyear field is also updated.
     */
    u4DaysInYear = DAYS_IN_YEAR ((TmBuf.tm_year));
    while (u4Days >= u4DaysInYear)
    {
        u4DaysInYear = DAYS_IN_YEAR ((TmBuf.tm_year));
        TmBuf.tm_year++;
        u4Days -= u4DaysInYear;
        u4DaysInYear = DAYS_IN_YEAR ((TmBuf.tm_year));
    }

    TmBuf.tm_yday = u4Days + 1;

    /* Determine  month and day of the month */
    u4Leap = (IS_LEAP (TmBuf.tm_year) ? 1 : 0);

    for (u4Month = 1; u4Month <= 12; u4Month++)
    {
        if (TmBuf.tm_yday <= au2DaysInMonth[u4Leap][u4Month])
        {
            TmBuf.tm_mday = TmBuf.tm_yday - au2DaysInMonth[u4Leap][u4Month - 1];
            TmBuf.tm_mon = u4Month - 1;    /* tm_mon is 0 - 11 */
            break;
        }
    }

    *tm = TmBuf;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlGetTime                                       */
/*                                                                          */
/*    Description        : This function gets current time in tUtlTm fmt.   */
/*                                                                          */
/*    Input(s)           : tm - ptr to tUtlTm variable.                     */
/*                                                                          */
/*    Output(s)          : Filled tm.                                       */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/

VOID
UtlGetTime (tUtlTm * tm)
{
    UINT4               u4Ticks;

    OsixGetSysTime (&u4Ticks);
    u4Ticks -= gu4TicksCorr;
    UtlGetTimeForTicks (u4Ticks, tm);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlSetTime                                       */
/*                                                                          */
/*    Description        : This function seeds the Tm facility.             */
/*                         It requires as input a 'struct tUtlTm' with      */
/*                         properly initialized values.                     */
/*                         The range of values of 'tUtlTm' fields are the   */
/*                         same as those of the standard 'struct tm'        */
/*                         except for the tm_year field which stores the    */
/*                         complete 4 digit year representation instead of  */
/*                         the two digit 1900-biased value of 'struct tm'   */
/*                         This should be taken care of while seeding       */
/*                                                                          */
/*    Input(s)           : tm - ptr to tUtlTm variable w/ current time.     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/
VOID
UtlSetTime (tUtlTm * tm)
{
    UINT4               u4year = tm->tm_year;

    gTmBuf = *tm;
    gu4Secs = 0;

    --u4year;
    while (u4year >= TM_BASE_YEAR)
    {
        gu4Secs += SECS_IN_YEAR (u4year);
        --u4year;
    }
    gu4Secs += (tm->tm_yday * 86400);
    gu4Secs += (tm->tm_hour * 3600);
    gu4Secs += (tm->tm_min * 60);
    gu4Secs += (tm->tm_sec);

    /* Function to Set the Linux System Time */
    TmrSetSysTime (tm);

    OsixGetSysTime (&gu4TicksCorr);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlInetNtoa                                      */
/*                                                                          */
/*    Description        : Minimal implementaion of inet_ntoa.              */
/*                         tUtlInAddr is the equivalent of 'struct inaddr'  */
/*                                                                          */
/*    Input(s)           : InAddr - The address in struct in_addr format.   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to CHR1 containing ntoa-ed address.      */
/*                                                                          */
/****************************************************************************/
CHR1               *
UtlInetNtoa (tUtlInAddr InAddr)
{
    static CHR1         ac1Addr[20];
    /* We need 15 bytes to store 255.255.255.255 */

    CHR1               *pcByte;

    pcByte = (CHR1 *) & InAddr;

    (void) SPRINTF (ac1Addr, "%d.%d.%d.%d", (UINT1) pcByte[0],
                    (UINT1) pcByte[1], (UINT1) pcByte[2], (UINT1) pcByte[3]);

    return (ac1Addr);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlInetNtoa6                                     */
/*                                                                          */
/*    Description        : Minimal implementaion of inet_ntoa for v6 addrs. */
/*                         tUtlIn6Addr is the equivalent of 'struct in6addr'*/
/*                                                                          */
/*    Input(s)           : In6Addr - The address in struct in6_addr format. */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to CHR1 containing ntop-ed address.      */
/*                                                                          */
/****************************************************************************/
CHR1               *
UtlInetNtoa6 (tUtlIn6Addr In6Addr)
{
    static CHR1         ac1Addr[50];
    CHR1               *pcByte;

    pcByte = (CHR1 *) & (In6Addr.u1addr);

    (void) SNPRINTF (ac1Addr, 50, "%.4x:%.4x:%.4x:%.4x:%.4x:%.4x:%.4x:%.4x",
                     ((UINT2)
                      ((((UINT2) pcByte[0]) << 8) | (pcByte[1] & 0xff))),
                     ((UINT2)
                      ((((UINT2) pcByte[2]) << 8) | (pcByte[3] & 0xff))),
                     ((UINT2)
                      ((((UINT2) pcByte[4]) << 8) | (pcByte[5] & 0xff))),
                     ((UINT2)
                      ((((UINT2) pcByte[6]) << 8) | (pcByte[7] & 0xff))),
                     ((UINT2)
                      ((((UINT2) pcByte[8]) << 8) | (pcByte[9] & 0xff))),
                     ((UINT2)
                      ((((UINT2) pcByte[10]) << 8) | (pcByte[11] & 0xff))),
                     ((UINT2)
                      ((((UINT2) pcByte[12]) << 8) | (pcByte[13] & 0xff))),
                     ((UINT2)
                      ((((UINT2) pcByte[14]) << 8) | (pcByte[15] & 0xff))));

    return (ac1Addr);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlInetAddr                                      */
/*                                                                          */
/*    Description        : Implementation of inet_addr.                     */
/*                                                                          */
/*    Input(s)           : pc1Addr - Inet Addr as a string.                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Converted Address on success, -1 on failure.     */
/*                         UtlInetAton is to be preferred to this function  */
/*                         See man inet_addr                                */
/*                                                                          */
/****************************************************************************/
UINT4
UtlInetAddr (const CHR1 * pc1Addr)
{
    tUtlInAddr          InAddr;

    if (UtlInetAton (pc1Addr, &InAddr))
        return (InAddr.u4Addr);
    return ((UINT4) 0xffffffff);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlInetAton                                      */
/*                                                                          */
/*    Description        : Implementation of inet_aton.                     */
/*                         tUtlInAddr is the equivalent of 'struct inaddr'  */
/*                                                                          */
/*    Input(s)           : pc1Addr - Address to be aton-ed.                 */
/*                       : InAddr - The aton-ed address.                    */
/*                                                                          */
/*    Output(s)          : InAddr filled.                                   */
/*                                                                          */
/*    Returns            : 1 if Successful / 0 otherwise.                   */
/*                                                                          */
/****************************************************************************/
INT4
UtlInetAton (const CHR1 * pc1Addr, tUtlInAddr * pInAddr)
{
    UINT4               u4Val;
    UINT4               u4Base, u4NumParts;
    UINT4               au4Parts[4];
    UINT4              *pu4Parts = au4Parts;
    CHR1                c;
    UINT4               u4DigitCount = 0;

#define MAX_DIGIT_IN_OCTET 3

    MEMSET (au4Parts, 0, sizeof (au4Parts));
    if ((*pc1Addr != '\0') && !(isxdigit (*pc1Addr)))
        return (0);
    for (;;)
    {
        /*
         * Collect number up to ``.''.
         * Values are specified as for C:
         * 0x=hex, 0=octal, other=decimal.
         */
        u4Val = 0;
        u4Base = 10;
        u4DigitCount = 0;
        if (*pc1Addr == '0')
        {
            if (*++pc1Addr == 'x' || *pc1Addr == 'X')
            {
                u4Base = 16, pc1Addr++;
                if ((*pc1Addr == '.') || !(*pc1Addr))
                    return (0);
            }
            else
                u4Base = 8;
        }
        while ((c = *pc1Addr) != '\0')
        {
            if (u4DigitCount > MAX_DIGIT_IN_OCTET)
            {
                return (0);
            }
            u4DigitCount++;
            if (IS_ASCII (c) && isdigit (c))
            {
                u4Val = (u4Val * u4Base) + (c - '0');
                pc1Addr++;
                continue;
            }
            if (u4Base == 16 && IS_ASCII (c) && isxdigit (c))
            {
                u4Val = (u4Val << 4) + (c + 10 - (islower (c) ? 'a' : 'A'));
                pc1Addr++;
                continue;
            }
            break;
        }
        if (*pc1Addr == '.')
        {
            /*
             * Internet format:
             *        a.b.c.d
             *        a.b.c        (with c treated as 16-bits)
             *        a.b        (with b treated as 24 bits)
             */
            if (pu4Parts >= au4Parts + 3 || u4Val > 0xff)
                return (0);
            *pu4Parts++ = u4Val, pc1Addr++;
            if ((*pc1Addr == '\0') || !(isxdigit (*pc1Addr)))
                return (0);
        }
        else
            break;
    }
    /*
     * Check for trailing characters.
     */
    if (*pc1Addr && (!IS_ASCII (*pc1Addr) || !isspace (*pc1Addr)))
        return (0);
    /*
     * Concoct the address according to
     * the number of parts specified.
     */
    u4NumParts = pu4Parts - au4Parts + 1;
    switch (u4NumParts)
    {

        case 1:                /* a -- 32 bits */
            return (0);

        case 2:                /* a.b -- 8.24 bits */
            if (u4Val > 0xff)
                return (0);
            u4Val |= au4Parts[0] << 24;
            break;

        case 3:                /* a.b.c -- 8.8.16 bits */
            if (u4Val > 0xff)
                return (0);
            u4Val |= (au4Parts[0] << 24) | (au4Parts[1] << 16);
            break;

        case 4:                /* a.b.c.d -- 8.8.8.8 bits */
            if (u4Val > 0xff)
                return (0);
            u4Val |=
                (au4Parts[0] << 24) | (au4Parts[1] << 16) | (au4Parts[2] << 8);
            break;
    }

    if (pInAddr)
        pInAddr->u4Addr = OSIX_HTONL (u4Val);
    return (1);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlInetAton6                                     */
/*                                                                          */
/*    Description        : Implementation of inet_aton for v6 addrs.        */
/*                         tUtlIn6Addr is the equivalent of 'struct in6addr'*/
/*                                                                          */
/*    Input(s)           : pc1Addr - Address to be aton-ed.                 */
/*                       : In6Addr - The aton-ed address.                   */
/*                                                                          */
/*    Output(s)          : In6Addr filled.                                  */
/*                                                                          */
/*    Returns            : 1 if Successful / 0 otherwise.                   */
/*                                                                          */
/****************************************************************************/

INT4
UtlInetAton6 (const CHR1 * pc1Addr, tUtlIn6Addr * pIn6Addr)
{
    tUtlInAddr          InAddr;
    UINT1               au1Parts[16], *pu1Parts, *pu1EndOfParts, *pu1Colonp;
    CONST CHR1         *pu1CurTok;
    INT1                i1Ch, i1DigitFlag, n, i;
    UINT4               u4Val;
    UINT4               u4ColonCount = 0;

    MEMSET ((pu1Parts = au1Parts), '\0', 16);
    pu1EndOfParts = pu1Parts + 16;
    pu1Colonp = NULL;

    /* Leading :: requires some special handling. */
    if (*pc1Addr == ':')
        if (*++pc1Addr != ':')
            return (0);

    for (i = 0; i < (INT1) STRLEN (pc1Addr); i++)
    {
        if (pc1Addr[i] == ':')
        {
            u4ColonCount++;
        }
    }
    if (u4ColonCount > 7)
    {
        return (0);
    }

    pu1CurTok = pc1Addr;
    i1DigitFlag = 0;
    u4Val = 0;

    while ((i1Ch = *pc1Addr++) != '\0')
    {
        CHR1                pch;
        if (isxdigit (i1Ch))
        {
            if ((i1Ch >= 'a') && (i1Ch <= 'f'))
                pch = (CHR1) (i1Ch - 'a' + 10);
            else if ((i1Ch >= 'A') && (i1Ch <= 'F'))
                pch = (CHR1) (i1Ch - 'A' + 10);
            else
                pch = (CHR1) (i1Ch - '0');

            u4Val <<= 4;
            u4Val |= pch;

            if (u4Val > 0xffff)
                return (0);

            i1DigitFlag = 1;
            continue;
        }
        if (i1Ch == ':')
        {
            pu1CurTok = pc1Addr;

            if (!i1DigitFlag)
            {
                if (pu1Colonp)
                    return (0);
                pu1Colonp = pu1Parts;
                continue;
            }
            if (pu1Parts + sizeof (INT2) > pu1EndOfParts)
                return (0);

            *pu1Parts++ = (UINT1) (u4Val >> 8) & 0xff;
            *pu1Parts++ = (UINT1) u4Val & 0xff;
            i1DigitFlag = 0;
            u4Val = 0;
            if (*pc1Addr == '\0')
                return (0);
            continue;
        }
        if ((i1Ch == '.') && ((pu1Parts + 4) <= pu1EndOfParts) &&
            (UtlInetAton ((const CHR1 *) pu1CurTok, &InAddr) > 0))
        {
            InAddr.u4Addr = OSIX_HTONL (InAddr.u4Addr);
            *pu1Parts++ = (UINT1) (((InAddr.u4Addr) & 0xff000000) >> 24);
            *pu1Parts++ = (UINT1) (((InAddr.u4Addr) & 0xff0000) >> 16);
            *pu1Parts++ = (UINT1) (((InAddr.u4Addr) & 0xff00) >> 8);
            *pu1Parts++ = (UINT1) ((InAddr.u4Addr) & 0xff);
            i1DigitFlag = 0;
            break;                /* '\0' was seen by UtlInetAton (). */
        }
        return (0);
    }

    if (i1DigitFlag)
    {
        if (pu1Parts + sizeof (INT2) > pu1EndOfParts)
            return (0);
        *pu1Parts++ = (UINT1) (u4Val >> 8) & 0xff;
        *pu1Parts++ = (UINT1) u4Val & 0xff;
    }
    if (pu1Colonp != NULL)
    {
        n = (INT1) (pu1Parts - pu1Colonp);
        for (i = 1; i <= n; i++)
        {
            if ((pu1EndOfParts - i) == (pu1Colonp + n - i))
                return (0);

            pu1EndOfParts[-i] = pu1Colonp[n - i];
            pu1Colonp[n - i] = 0;
        }
        pu1Parts = pu1EndOfParts;
    }
    if (pu1Parts != pu1EndOfParts)
        return (0);

    if (pIn6Addr)
        MEMCPY (pIn6Addr, au1Parts, 16);
    return (1);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlStrCaseCmp                                    */
/*                                                                          */
/*    Description        : Implementation of strcasecmp                     */
/*                                                                          */
/*    Input(s)           : pc1String1 and pc1String2 are the strings to     */
/*                         compare.                                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Return an integer less than, equal to,           */
/*                         or greater than zero  if s1 (or  the  first  n   */
/*                         bytes thereof)  is  found,  respectively, to  be */
/*                         less than, to match, or be greater than s2.      */
/*                                                                          */
/****************************************************************************/
INT4
UtlStrCaseCmp (const CHR1 * pc1String1, const CHR1 * pc1String2)
{
    while (*pc1String1 != '\0'
           && tolower (*pc1String1) == tolower (*pc1String2))
    {
        pc1String1++;
        pc1String2++;
    }

    return (tolower (*pc1String1) - tolower (*pc1String2));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlStrnCaseCmp                                   */
/*                                                                          */
/*    Description        : Implementation of strncasecmp                    */
/*                                                                          */
/*    Input(s)           : pc1String1 and pc1String2 are the strings to     */
/*                         compare.                                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Same as UtlStrnCaseCmp, except that it compares  */
/*                         only the first n characters.                     */
/*                                                                          */
/****************************************************************************/
INT4
UtlStrnCaseCmp (const CHR1 * pc1String1, const CHR1 * pc1String2, UINT4 u4Len)
{
    if (pc1String1 == pc1String2 || u4Len == 0)
        return (0);

    while (*pc1String1 != '\0'
           && tolower (*pc1String1) == tolower (*pc1String2) && --u4Len)
    {
        pc1String1++;
        pc1String2++;
    }

    return (tolower (*pc1String1) - tolower (*pc1String2));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlStrnlen ()                                    */
/*                                                                          */
/*    Description        : Determines the length of a fixed-size string     */
/*                                                                          */
/*    Input(s)           : pcString and Max Length of the string.           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : The length of the string if the length is        */
/*                         less than or equal to 'u4Len'. Otherwise returns */
/*                         strlen (pcString)                                */
/*                                                                          */
/****************************************************************************/
UINT4
UtlStrnlen (const CHR1 * pc1String, UINT4 u4Max)
{
    const CHR1         *pc1Start = pc1String;

    while (*pc1String && (u4Max-- > 0))
        pc1String++;

    return (pc1String - pc1Start);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlAtoi ()                                       */
/*                                                                          */
/*    Description        : Determines the Integer value for the given string*/
/*                                                                          */
/*    Input(s)           : pcString - input string.                         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : The signed integer value of the input string.    */
/*                                                                          */
/****************************************************************************/
INT4
UtlAtoi (const CHR1 * pc1String)
{
    const char         *s;
    int                 n, sign;

    s = pc1String;

    for (; isspace (*s); s++);

    sign = (*s == '-') ? -1 : 1;

    if (*s == '+' || *s == '-')
        s++;

    for (n = 0; isdigit (*s); s++)
        n = 10 * n + *s - '0';

    return sign * n;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlCalcIpCheckSum                                */
/*                                                                          */
/*    Description        : Implementation of IP Checksumming.               */
/*                                                                          */
/*    Input(s)           : pBuf points to the databuffer to be checksummed. */
/*                         u4Size is the size of this buffer.               */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Returns the computed checksum.                   */
/*                                                                          */
/****************************************************************************/
UINT2
UtlCalcIpCheckSum (UINT1 *pBuf, UINT4 u4Size)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;

    while (u4Size > 1)
    {
        u2Tmp = *((UINT2 *) (VOID *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }

    if (u4Size == 1)
    {
        u2Tmp = 0;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) (~((UINT2) u4Sum));

    return (u2Tmp);
}

/****************************************************************************/
/*    Function Name      : FsNtohf                                          */
/*                                                                          */
/*    Description        : Host to network conversion for floats.           */
/*                                                                          */
/*    Input(s)           : fValue is the value to be converted.             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : The converted value.                             */
/****************************************************************************/
FLT4
FsNtohf (FLT4 fValue)
{
    UINT1              *pTemp;
    UINT1              *pTempNew;
    FLT4                fNewValue;

    pTemp = (UINT1 *) &fValue;
    pTempNew = (UINT1 *) &fNewValue;
    pTempNew[0] = pTemp[3];
    pTempNew[3] = pTemp[0];
    pTempNew[1] = pTemp[2];
    pTempNew[2] = pTemp[1];

    return (fNewValue);
}

/****************************************************************************/
/*    Function Name      : UtlU8Mul                                         */
/*                                                                          */
/*    Description        : Multiply two UINT8s.                             */
/*                                                                          */
/*    Input(s)           : pu8Val1, pu8Val2 - values 2 b multiplied.        */
/*                                                                          */
/*    Output(s)          : pu8Result - Product.                             */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
void
UtlU8Mul (FS_UINT8 * pu8Result, FS_UINT8 * pu8Val1, FS_UINT8 * pu8Val2)
{
    INT4                i4Bit;
    FS_UINT8            u8Val1;
    FS_UINT8            u8Val2;

    u8Val1.u4Hi = pu8Val1->u4Hi;
    u8Val1.u4Lo = pu8Val1->u4Lo;
    u8Val2.u4Hi = pu8Val2->u4Hi;
    u8Val2.u4Lo = pu8Val2->u4Lo;

    pu8Result->u4Hi = pu8Result->u4Lo = 0;
    for (i4Bit = 0; i4Bit < 32; ++i4Bit)
    {
        /* If i-th bit is set */
        if ((u8Val2.u4Lo & (0x1 << i4Bit)) != 0)
        {
            /* overflow to Hi if such is the case */
            if ((pu8Result->u4Lo + (u8Val1.u4Lo << i4Bit)) < pu8Result->u4Lo)
            {
                pu8Result->u4Hi += 1;
            }
            /* accumulate lo */
            pu8Result->u4Lo += (u8Val1.u4Lo) << i4Bit;

            /* add to Hi, any carries arising out of the above */
            pu8Result->u4Hi += (i4Bit > 0) ? (u8Val1.u4Lo >> (32 - i4Bit)) : 0;

            /* accumulate hi */
            pu8Result->u4Hi += (u8Val1.u4Hi << i4Bit);
        }

        /* accumulate hi if the i-th bit is set */
        if ((u8Val2.u4Hi & (0x1 << i4Bit)) != 0)
        {
            pu8Result->u4Hi += (u8Val1.u4Lo) << i4Bit;
        }
    }
}

/****************************************************************************/
/*    Function Name      : UtlStrAdd                                        */
/*                                                                          */
/*    Description        : Add two strings of numbers.                      */
/*                                                                          */
/*    Input(s)           : s1, s2 - values 2 b added.                       */
/*                                                                          */
/*    Output(s)          : sum - Resulting sum.                             */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
static void
UtlStrAdd (char *sum, char *s1, char *s2)
{
    CHR1               *pc1S1, *pc1S2, *pc1Str;
    CHR1                ac1Acc[21];
    INT4                i4Len1 = STRLEN (s1);
    INT4                i4Len2 = STRLEN (s2);
    INT4                i4Len, i4RemLen;
    INT4                i4Idx, i4Idx2;
    INT4                i4Carry;
    INT4                i4Char, i4Char1, i4Char2;

    pc1S1 = s1 + i4Len1 - 1;
    pc1S2 = s2 + i4Len2 - 1;

    if (i4Len1 < i4Len2)
    {
        i4Len = i4Len1;
        i4RemLen = i4Len2 - i4Len1;
    }
    else
    {
        i4Len = i4Len2;
        i4RemLen = i4Len1 - i4Len2;
    }

    i4Idx = 0;
    i4Carry = 0;
    while (i4Len--)
    {
        i4Char1 = *pc1S1-- - '0';
        i4Char2 = *pc1S2-- - '0';
        ac1Acc[i4Idx++] = (i4Char1 + i4Char2 + i4Carry) % 10 + '0';
        if (i4Char1 + i4Char2 + i4Carry > 9)
            i4Carry = 1;
        else
            i4Carry = 0;
    }

    if (i4Len1 < i4Len2)
    {
        pc1Str = pc1S2;
    }
    else
    {
        pc1Str = pc1S1;
    }

    while (i4RemLen--)
    {
        i4Char = *pc1Str-- - '0';

        ac1Acc[i4Idx++] = (i4Char + i4Carry) % 10 + '0';
        if (i4Char + i4Carry > 9)
            i4Carry = 1;
        else
            i4Carry = 0;
    }

    if (i4Carry)
        ac1Acc[i4Idx++] = i4Carry + '0';
    ac1Acc[i4Idx] = '\0';

    /* reverse */
    for (i4Idx = 0, i4Idx2 = strlen (ac1Acc) - 1;
         i4Idx < i4Idx2; i4Idx++, i4Idx2--)
    {
        i4Char = ac1Acc[i4Idx];
        ac1Acc[i4Idx] = ac1Acc[i4Idx2];
        ac1Acc[i4Idx2] = (CHR1) i4Char;
    }
    MEMSET (sum, '\0', 21);
    MEMCPY (sum, ac1Acc, 21);
}

/****************************************************************************/
/*    Function Name      : UtlU8Div                                         */
/*                                                                          */
/*    Description        : Divides two UINT8 strings.                       */
/*                         Currently only implements div by 1 million       */
/*                                                                          */
/*    Input(s)           : pu8Val1 - Numerator.                             */
/*                         pu8Val2 - Denomenator                            */
/*                                                                          */
/*    Output(s)          : pu8Result - Quotient                             */
/*                         pu8Reminder - Reminder                           */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
void
UtlU8Div (FS_UINT8 * pu8Result, FS_UINT8 * pu8Reminder, FS_UINT8 * pu8Val1,
          FS_UINT8 * pu8Val2)
{
#ifndef LINUX_KERN
    AR_UINT8            aru8Val1 = 0;
    AR_UINT8            aru8Val2 = 0;
    AR_UINT8            aru8Result = 0;
    AR_UINT8            aru8Reminder = 0;

    aru8Val1 = 0x00000000000000000000000000000000 | pu8Val1->u4Hi;
    aru8Val1 = aru8Val1 << 32;
    aru8Val1 = aru8Val1 | pu8Val1->u4Lo;

    aru8Val2 = 0x00000000000000000000000000000000 | pu8Val2->u4Hi;
    aru8Val2 = aru8Val2 << 32;
    aru8Val2 = aru8Val2 | pu8Val2->u4Lo;

    aru8Result = aru8Val1 / aru8Val2;
    aru8Reminder = aru8Val1 % aru8Val2;

    /* Converting the AR_UINT8 back to FS_UINT8 */
    pu8Result->u4Lo = (int) aru8Result;
    pu8Result->u4Hi = aru8Result >> 32;

    pu8Reminder->u4Lo = (int) aru8Reminder;
    pu8Reminder->u4Hi = aru8Reminder >> 32;
#else
    CHR1                ac1Str[21];

    MEMSET (ac1Str, 0, 21);
    FSAP_U8_2STR (pu8Val1, ac1Str);
    if (STRLEN (ac1Str) >= 6)
    {
        (VOID) pu8Val2;
        ac1Str[STRLEN (ac1Str) - 6] = '\0';
        /* Greater than a million fill the quotient and
         *          * set reminder to zero. Approximated value */
        FSAP_STR2_U8 (ac1Str, pu8Result);
        FSAP_U8_CLR (pu8Reminder);
    }
    else
    {
        /* Less than a million return reminder. Set quotient to zero. */
        FSAP_U8_ASSIGN (pu8Reminder, pu8Val1);
        FSAP_U8_CLR (pu8Result);
    }
#endif
}

/****************************************************************************/
/*    Function Name      : UtlU82Str                                        */
/*                                                                          */
/*    Description        : Convert a UINT8 to its string representaiton.    */
/*                                                                          */
/*    Input(s)           : pu8Val - value to be stringified.                */
/*                                                                          */
/*    Output(s)          : pc1Str - Converted string representation.        */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
void
UtlU82Str (FS_UINT8 * pu8Val, CHR1 * pc1Str)
{
    FS_UINT8            u8Val;
    UINT4               u4Val;
    INT4                i4Idx;
    INT4                i4Idx2;
    INT4                i4c;    /* temp used during swap operation */
    INT4                i4e;    /* loop variable used to scale exponent */
    INT4                i4Posn;
    CHR1                ac1HiSum[21];
    CHR1                ac1PPbuf[40];
    CHR1                ac1LoSum[21];

    /* String representation of multiples of (MAX_UINT4+1) */
    const CHR1         *ac1HiVals[] = {
        "",
        "4294967296",
        "8589934592",
        "12884901888",
        "17179869184",
        "21474836480",
        "25769803776",
        "30064771072",
        "34359738368",
        "38654705664"
    };

    u8Val.u4Hi = pu8Val->u4Hi;
    u8Val.u4Lo = pu8Val->u4Lo;

    i4Idx = 0;
    MEMSET (ac1HiSum, '\0', 21);
    MEMSET (ac1LoSum, '\0', 21);
    MEMSET (ac1PPbuf, '\0', 40);

    /* Convert Lo to string */
    u4Val = u8Val.u4Lo;
    do
    {
        ac1LoSum[i4Idx++] = u4Val % 10 + '0';
    }
    while ((u4Val /= 10) > 0);

    ac1LoSum[i4Idx] = '\0';

    /* reverse LoSum to get the string representation */
    for (i4Idx = 0, i4Idx2 = STRLEN (ac1LoSum) - 1;
         i4Idx < i4Idx2; i4Idx++, i4Idx2--)
    {
        i4c = ac1LoSum[i4Idx];
        ac1LoSum[i4Idx] = ac1LoSum[i4Idx2];
        ac1LoSum[i4Idx2] = (CHR1) i4c;
    }

    /* Convert Hi to string */
    i4Posn = 0;
    u4Val = u8Val.u4Hi;

    do
    {
        /* Get the rightmost digit */
        i4c = u4Val % 10;

        /* move the *value* corr to this digit */
        /* into the accumulator PP (partial product */
        STRNCPY (ac1PPbuf, ac1HiVals[i4c], 23);

        /* Multiply by 10 for each digit scanned */
        for (i4e = 0; i4e < i4Posn; i4e++)
            STRCAT (ac1PPbuf, "0");

        i4Posn++;

        /* accumulate this digit's value to the ac1HiSum */
        UtlStrAdd (ac1HiSum, ac1PPbuf, ac1HiSum);

    }
    while ((u4Val /= 10) > 0);

    /* Add the values of low and Hi and place it */
    /* in the output buffer */
    UtlStrAdd (pc1Str, ac1HiSum, ac1LoSum);
}

/****************************************************************************/
/*    Function Name      : UtlStr2U8                                        */
/*                                                                          */
/*    Description        : Convert a string to its UINT8 representaiton.    */
/*                                                                          */
/*    Input(s)           : pc1Str - String to be converted.                 */
/*                                                                          */
/*    Output(s)          : pu8Val - Converted value in UINT8.               */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
UINT4
UtlStr2U8 (CHR1 * pc1Str, FS_UINT8 * pu8Result)
{
    const CHR1         *s = pc1Str;
    const CHR1         *str = pc1Str;
    FS_UINT8            u8Exp;
    FS_UINT8            u8Val;
    UINT4               u4Base = 0;
    INT4                i4c;

    /* skip any spaces */
    do
    {
        i4c = *s++;
    }
    while (i4c == ' ');

    /* check if it is a hexadecimal no */
    if (i4c == '0' && (*s == 'x' || *s == 'X'))
    {
        i4c = s[1];
        s += 2;
        str += 2;
        u4Base = 16;
    }

    if (i4c == 0)
    {
        return 0;
    }

    /* it is either octal or decimal */
    if (u4Base == 0)
        u4Base = i4c == '0' ? 8 : 10;

    /* validate input for range */
    if (u4Base == 10)
    {
        while (*pc1Str != '\0')
        {
            if (*pc1Str >= '0' && *pc1Str <= '9')
            {
                pc1Str++;
            }
            else
            {
                return 0;
            }
        }
        if (UtlStrNumCmp (s - 1, "18446744073709551615") > 0)
        {
            return 0;
        }
    }
    else if (u4Base == 16)
    {
        while (*str != '\0')
        {
            if (((*str >= '0') && (*str <= '9')) ||
                ((*str >= 'a') && (*str <= 'f')) ||
                ((*str >= 'A') && (*str <= 'F')))
            {
                str++;
            }
            else
            {
                return 0;
            }
        }
        if (UtlStrNumCmp (s - 1, "ffffffffffffffff") > 0)
        {
            return 0;
        }
    }
    else if (u4Base == 8)
    {
        if (UtlStrNumCmp (s - 1, "1777777777777777777777") > 0)
        {
            return 0;
        }
    }
    pu8Result->u4Hi = 0;
    pu8Result->u4Lo = 0;

    u8Exp.u4Hi = 0;
    u8Exp.u4Lo = u4Base;

    /* Get the leftmost digit */
    for (;; i4c = *s++)
    {
        if (isdigit (i4c))
            i4c -= '0';
        else if (isalpha (i4c))
            i4c -= isupper (i4c) ? 'A' - 10 : 'a' - 10;
        else
            break;

        if (i4c >= (INT4) u4Base)
            break;

        u8Val.u4Hi = 0;
        u8Val.u4Lo = i4c;

        /* multiply by Base */
        FSAP_U8_MUL (pu8Result, &u8Exp, pu8Result);

        /* add the new digit using u8 addition */
        FSAP_U8_ADD (pu8Result, &u8Val, pu8Result);
    }

    return (1);
}

/****************************************************************************/
/*    Function Name      : UtlStr2U8                                        */
/*                                                                          */
/*    Description        : Compare two strings of numbers lexicographically.*/
/*                                                                          */
/*    Input(s)           : s1, s2 - Strings to be compared.                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Similar to strcmp.                               */
/****************************************************************************/
static INT4
UtlStrNumCmp (const CHR1 * s1, const CHR1 * s2)
{
    INT4                i4len1 = STRLEN (s1);
    INT4                i4len2 = STRLEN (s2);
    INT4                c1, c2;

    if (i4len1 < i4len2)
        return -1;
    if (i4len1 > i4len2)
        return 1;

    while (*s1 != '\0')
    {
        c1 = *s1;
        c2 = *s2;
        if (isdigit (*s1))
            c1 -= '0';
        else if (isalpha (*s1))
            c1 -= isupper (*s1) ? 'A' - 10 : 'a' - 10;

        if (isdigit (*s2))
            c2 -= '0';
        else if (isalpha (*s2))
            c2 -= isupper (*s2) ? 'A' - 10 : 'a' - 10;

        if (c1 != c2)
        {
            break;
        }
        s1++;
        s2++;
    }
    return (*s1 - *s2);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlGetCurDate                                    */
/*                                                                          */
/*    Description        : This function gets current date in dd-Mon-yyyy   */
/*                                                                          */
/*    Input(s)           : ac1CurDateStr - String to store the date         */
/*                                                                          */
/*    Output(s)          : Filled ac1CurDateStr                             */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/

VOID
UtlGetCurDate (CHR1 ac1CurDateStr[])
{
    tUtlTm              curtime;

    UtlGetTime (&curtime);

    SPRINTF ((CHR1 *) ac1CurDateStr, "%02u-%s-%4u",
             curtime.tm_mday, ac1Month[curtime.tm_mon], curtime.tm_year);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlGetTimeSinceEpoch                             */
/*                                                                          */
/*    Description        : This function returns time in seconds since      */
/*                         Base year - 2000                                 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Time in Seconds since base year -2000            */
/*                                                                          */
/****************************************************************************/

UINT4
UtlGetTimeSinceEpoch (VOID)
{
    UINT4               u4Ticks;
    UINT4               u4ElapsedSecs;
    UINT4               u4Secs;
    OsixGetSysTime (&u4Ticks);
    u4Ticks -= gu4TicksCorr;
    u4ElapsedSecs = u4Ticks / gu4Stups;
    u4Secs = gu4Secs + u4ElapsedSecs;
    return u4Secs;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlGetTimeInSecs                                 */
/*                                                                          */
/*    Description        : This function gets current time in Ticks.        */
/*                                                                          */
/*    Input(s)           : NONE                                             */
/*                                                                          */
/*    Output(s)          : System Time Since Epoch in Seconds.              */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/

UINT4
UtlGetTimeInSecs ()
{
    UINT4               u4Ticks;
    UINT4               u4Secs;

    OsixGetSysTime (&u4Ticks);
    u4Ticks -= gu4TicksCorr;
    u4Secs = u4Ticks / gu4Stups;
    return u4Secs;
}

/*****************************************************************************/
/* Function Name    :  UtlGetTimeForSeconds                                   */
/* Description      :  This procedure computes the time for given seconds    */
/* Input Parameters :  tUtlTm tm -- Time structure ,                         */
/*                     u4Seconds -- Total num of seconds                     */
/* Output Parameters : NONE                                                  */
/* Return Value      : NONE                                                   */
/*****************************************************************************/

VOID
UtlGetTimeForSeconds (UINT4 u4Secs, tUtlTm * tm)
{
    UINT4               u4DaysInYear;
    UINT4               u4Days;
    UINT4               u4Leap;
    UINT4               u4Month;
    tUtlTm              TmBuf;

    MEMSET (&TmBuf, 0, sizeof (TmBuf));
    u4Days = u4Secs / 86400;

    /* 60 seconds in a day, 60 mins in a hour,
     * 24 hours in a day and 7 days in a week.
     * Being invariant, these are calculated straightaway.
     */
    TmBuf.tm_sec = u4Secs % 60;
    TmBuf.tm_min = (u4Secs / 60) % 60;
    TmBuf.tm_hour = (u4Secs / 3600) % 24;

    /* 01-01-2000 is a Saturday. So we add a bias of 6 */
    TmBuf.tm_wday = (u4Days + 6) % 7;

    TmBuf.tm_year = TM_BASE_YEAR;
    TmBuf.tm_mon = 0;
    TmBuf.tm_yday = 0;
    TmBuf.tm_mday = 0;
    TmBuf.tm_mon = 0;
    /* Number of days in a month/year varies.
     * So we evaluated it.
     * Using the elapsed days since the last call to this
     * function, we determine the current year.
     * The dayoftheyear field is also updated.
     */
    u4DaysInYear = DAYS_IN_YEAR ((TmBuf.tm_year));
    while (u4Days >= u4DaysInYear)
    {
        u4DaysInYear = DAYS_IN_YEAR ((TmBuf.tm_year));
        TmBuf.tm_year++;
        u4Days -= u4DaysInYear;
        u4DaysInYear = DAYS_IN_YEAR ((TmBuf.tm_year));
    }
    /*Removing this logic since yday can be zero and we are 
     * incrementing mday by one while calculating mday from yday*/
    TmBuf.tm_yday = u4Days;

    /* Determine  month and day of the month */
    u4Leap = (IS_LEAP (TmBuf.tm_year) ? 1 : 0);

    for (u4Month = 1; u4Month <= 12; u4Month++)
    {
        if (TmBuf.tm_yday < au2DaysInMonth[u4Leap][u4Month])
        {
            TmBuf.tm_mday =
                (TmBuf.tm_yday - au2DaysInMonth[u4Leap][u4Month - 1]) + 1;
            TmBuf.tm_mon = u4Month - 1;    /* tm_mon is 0 - 11 */
            break;
        }
    }

    *tm = TmBuf;
}

/*************************************************************************/
/* Function Name     : UtlGetSecondsForTime                              */
/* Description       : This procedure converts the time in tUtlTm struct */
/*                     to seconds since Base Year(2000)                  */
/* Input(s)          : tm - Time in tUtlTm structure                     */
/* Output(s)         : None                                              */
/* Returns           : u4Secs -Time in seconds since Base Year(2000)     */
/*************************************************************************/
UINT4
UtlGetSecondsForTime (tUtlTm * tm)
{
    UINT4               u4Year = TM_BASE_YEAR;
    UINT4               u4Secs = 0;

    while (u4Year < tm->tm_year)
    {
        u4Secs += SECS_IN_YEAR (u4Year);
        ++u4Year;
    }

    /*For SECS_IN_DAY, seconds should be calculated from the previous
     *      *      *      * day for current day*/
    u4Secs += ((tm->tm_yday - 1) * SECS_IN_DAY);
    u4Secs += (tm->tm_hour * SECS_IN_HOUR);
    u4Secs += (tm->tm_min * SECS_IN_MINUTE);
    u4Secs += (tm->tm_sec);
    return u4Secs;
}

/******************************************************************************/
/*  Function Name   : UtlGetTraceOptionValue                                  */
/*                                                                            */
/*  Description     : This function used to get the trace types configured in */
/*                    the given trace input string.                           */
/*                                                                            */
/*                    Given input trace string will be compared with the      */
/*                    valid trace strings supported by the module. If the     */
/*                    valid trace string is given, then the bit number        */
/*                    corresponding to the TraceID will be set in the output  */
/*                    Trace option variable.                                  */
/*                                                                            */
/*                    If any one of the given string is not supported, then   */
/*                    the output trace option will be set to zero and         */
/*                    returned.                                               */
/*                                                                            */
/*  Input(s)        : UtlValidTraces - Structure containing valid traces      */
/*                                     supported by the particular module.    */
/*                                     It contains two values, pu1TraceStrings*/
/*                                     containing array of valid trace strings*/
/*                                     supported and u2MaxTrcTypes contains   */
/*                                     maximum trace types supported by the   */
/*                                     particular module.                     */
/*                    pu1TraceInput  - Input trace string, that needs to be   */
/*                                     parsed                                 */
/*                    u2TrcLen       - Length of the input trace string       */
/*                                                                            */
/*  Output(s)       : pu4TraceOption - Variable having the information of     */
/*                                     trace types configured. Bit number     */
/*                                     corresponding to the trace ID will     */
/*                                     be set in this variable.               */
/*                    pu1TraceStatus - Flag indicates whether trace is        */
/*                                     "enable" trace or "disable" trace      */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
UtlGetTraceOptionValue (tUtlValidTraces UtlValidTraces, UINT1 *pu1TraceInput,
                        UINT2 u2TrcLen, UINT4 *pu4TraceOption,
                        UINT1 *pu1TraceStatus)
{
    UINT2               u2TokenSize = 0;
    UINT2               u2TrcId;
    UINT1               u1ValidToken = 0;
    UINT1               u1Delimiter = (UINT1) UTL_TRACE_TOKEN_DELIMITER;

    *pu4TraceOption = 0;
    *pu1TraceStatus = UTL_TRACE_DISABLE;

    if ((!pu1TraceInput) || (!UtlValidTraces.pu1TraceStrings))
    {
        return;
    }

    /* Check for enable token */
    if (!STRNCMP (pu1TraceInput, "enable ", STRLEN ("enable ")))
    {
        pu1TraceInput += STRLEN ("enable ");
        u2TrcLen -= STRLEN ("enable ");
        *pu1TraceStatus = UTL_TRACE_ENABLE;
    }
    /* Check for disable token */
    else if (!STRNCMP (pu1TraceInput, "disable ", STRLEN ("disable ")))
    {
        pu1TraceInput += STRLEN ("disable ");
        u2TrcLen -= STRLEN ("disable ");
    }
    else
    {
        /* First  token has to be either enable or disable */
        return;
    }

    while (u2TrcLen > 0)
    {
        /* scan for delimiter and find the trace string */

        while ((u2TrcLen > 0)
               && (*(pu1TraceInput + u2TokenSize) != u1Delimiter))
        {
            u2TrcLen--;
            u2TokenSize++;
        }

        /* A single trace string is parsed, now check it with the valid
         * trace type supported. If the given string is not a valid trace
         * type, then trace option will be resetted to 0 and returned */

        for (u2TrcId = 0; u2TrcId < UtlValidTraces.u2MaxTrcTypes; u2TrcId++)
        {
            if (!STRNCMP (((UtlValidTraces.pu1TraceStrings)
                           + (u2TrcId * UTL_MAX_TRC_LEN)),
                          pu1TraceInput, u2TokenSize))
            {
                *pu4TraceOption |= (1 << u2TrcId);
                u1ValidToken = 1;
                break;
            }
        }

        /* Given input trace string matched with any valid trace supported,
         * output trace option will be set to zero and return. */

        if (!u1ValidToken)
        {
            *pu4TraceOption = 0;
            return;
        }

        if (u2TrcLen > 0)
        {
            /* Trace length has to be subtracted by 1 for space delimiter.
             * u2TrcLen will be zero, when the end of the input string is
             * reached */
            u2TrcLen--;
        }
        pu1TraceInput += (u2TokenSize + 1);    /* Plus for space delimiter */
        u2TokenSize = 0;        /* Reinitialize Token size with zero */
    }
/*
// Commented to fix Coverity Warnings. u2TokenSize value will never be zero. 
    if (u2TokenSize != 0)
    {
        // Some junk inputs at end of the trace input
        *pu4TraceOption = 0;
    }
*/
    return;
}

/******************************************************************************/
/*  Function Name   : UtlGetTraceOptionString                                 */
/*                                                                            */
/*  Description     : This function used to get the trace string from the     */
/*                    given trace option value.                               */
/*                                                                            */
/*                    Given input trace option value will parsed bit by bit.  */
/*                    If the particular bit is set, then the bit numberth     */
/*                    trace string will be appended to the output trace       */
/*                    string.                                                 */
/*                                                                            */
/*                    If the trace option value given is zero, then the       */
/*                    function will be returned without further processing.   */
/*                                                                            */
/*  Input(s)        : UtlValidTraces - Structure containing valid traces      */
/*                                     supported by the particular module.    */
/*                                     It contains two values, pu1TraceStrings*/
/*                                     containing array of valid trace strings*/
/*                                     supported and u2MaxTrcTypes contains   */
/*                                     maximum trace types supported by the   */
/*                                     particular module.                     */
/*                    u4TrcOption    - Variable having the information of     */
/*                                     trace types configured. Bit number     */
/*                                     corresponding to the trace ID will     */
/*                                     be returned in the output trace string */
/*                                                                            */
/*  Output(s)       : pu1RetTrcString- Output trace string, will have the     */
/*                                     list of traces enabled separated by    */
/*                                     a space as a delimited.                */
/*                    pu2TrcLen      - Returns the length of the trace string */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
UtlGetTraceOptionString (tUtlValidTraces UtlValidTraces, UINT4 u4TrcOption,
                         UINT1 *pu1RetTrcString, UINT2 *pu2TrcLen)
{
    UINT2               u2BitNo = 0;
    UINT1               u1Flag = OSIX_FALSE;

    *pu2TrcLen = 0;

    /* Verifying input strings for NULL */
    if ((!pu1RetTrcString) || (!UtlValidTraces.pu1TraceStrings))
    {
        return;
    }

    /* No trace is enabled */
    if (u4TrcOption == 0)
    {
        return;
    }

    for (; u2BitNo < UtlValidTraces.u2MaxTrcTypes; u2BitNo++)
    {
        if ((u4TrcOption >> u2BitNo) & OSIX_TRUE)
        {
            if (u1Flag == OSIX_FALSE)
            {
                STRNCAT (pu1RetTrcString, "enable ", STRLEN ("enable "));
                *pu2TrcLen = STRLEN (pu1RetTrcString);
                u1Flag = OSIX_TRUE;
            }

            STRNCAT (pu1RetTrcString, (UtlValidTraces.pu1TraceStrings
                                       + (u2BitNo * UTL_MAX_TRC_LEN)),
                     STRLEN ((UtlValidTraces.pu1TraceStrings
                              + (u2BitNo * UTL_MAX_TRC_LEN))));

            *pu2TrcLen += STRLEN (UtlValidTraces.pu1TraceStrings +
                                  (u2BitNo * UTL_MAX_TRC_LEN));

            /* Adding one space as a delimiter between the trace strings */
            STRNCAT (pu1RetTrcString, " ", STRLEN (" "));
            (*pu2TrcLen)++;
        }
    }

    /* Remove the last space delimiter */
    (*pu2TrcLen)--;
    MEMSET ((pu1RetTrcString + (*pu2TrcLen)), 0, 1);

    return;
}

/*****************************************************************************/
/* Function Name    :  UtlGetTimerSpeed                                      */
/* Description      :  This procedure returns the timer speed                */
/* Input Parameters :  NONE                                                  */
/* Output Parameters : NONE                                                  */
/* Return Value      : Timer Speed                                           */
/*****************************************************************************/

UINT4
UtlGetTimerSpeed (VOID)
{
    return (gu4TimerSpeed);
}

/*****************************************************************************/
/* Function Name    :  UtlSetTimerSpeed                                      */
/* Description      :  This procedure Sets the timer speed                   */
/* Input Parameters :  NONE                                                  */
/* Output Parameters : NONE                                                  */
/* Return Value      : Timer Speed                                           */
/*****************************************************************************/

VOID
UtlSetTimerSpeed (UINT4 u4TimerSpeed)
{
    gu4TimerSpeed = u4TimerSpeed;
}

/************************************************************************/
/*  Function Name   : UtlGetNTPSysTime                                  */
/*  Description     : Returns the seconds and picoseconds of the time   */
/*                  : from the Epoch (00:00:00 UTC, January 1, 1900)    */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysPreciseTime - The System time.                */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
UtlGetNTPSysTime (tUtlSysPreciseTime * pSysPreciseTime)
{
    /*currently not supported */
    UNUSED_PARAM (pSysPreciseTime);
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : UtlGetPreciseSysTime                              */
/*  Description     : Returns the seconds and Nanoseconds of the time   */
/*                  : from the Epoch (00:00:00 UTC, January 1, 1970)    */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysPreciseTime - The System time.                */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
UtlGetPreciseSysTime (tUtlSysPreciseTime * pSysPreciseTime)
{
    return (TmrGetPreciseSysTime (pSysPreciseTime));
}

/************************************************************************/
/*  Function Name   : UtlSetPreciseSysTime                              */
/*  Description     : Set the seconds and Nanoseconds of the time       */
/*                  : from the Epoch (00:00:00 UTC, January 1, 1970)    */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysPreciseTime - The System time.                */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
UtlSetPreciseSysTime (tUtlSysPreciseTime * pSysPreciseTime)
{
    UINT4               u4SecDiff = 0;

    gu4Secs = 0;

    /* FSAP Base Year is TM_BASE_YEAR so subtract the Second form 1970 to
     * TM_BASE_YEAR and update the gu4Secs */
    if (pSysPreciseTime->u4Sec >= TM_SEC_SYS_BASE_2_FSAP_BASE_YEAR)
    {
        u4SecDiff = pSysPreciseTime->u4Sec - TM_SEC_SYS_BASE_2_FSAP_BASE_YEAR;
    }
    gu4Secs = u4SecDiff;

    /* check if system time should be configured */
    if (pSysPreciseTime->u4SysTimeUpdate == OSIX_TRUE)
    {
        /* Set System Time */
        TmrSetPreciseSysTime (pSysPreciseTime);
    }

    OsixGetSysTime (&gu4TicksCorr);

    return (OSIX_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlInetNtop                                      */
/*                                                                          */
/*    Description        : UtlInetNtop converts the numeric address into    */
/*                         a text string suitable for presentation          */
/*                                                                          */
/*    Input(s)           : i4AddrFamily - specifies the family of the       */
/*                                        address AF_INET or AF_INET6       */
/*                         pSrc    - Pointer to the buffer holding the      */
/*                                   IPv4 or IPv6 address                   */
/*                         pc1Dest - pointer to the buffer where the        */
/*                                   resultant string is stored             */
/*                         i4Size - Specifies the size of the pc1Dest buffer*/
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : returns the string of IP address                 */
/*                                                                          */
/****************************************************************************/
INT4
UtlInetNtop (INT4 i4AddrFamily, const VOID *pSrc, CHR1 * pc1Dest, INT4 i4Size)
{
    tUtlInAddr          InAddr;
    tUtlIn6Addr         In6Addr;
    CHR1               *pc1IPAddr = NULL;

    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));

    if (pSrc == NULL || pc1Dest == NULL)
    {
        return (OSIX_FAILURE);
    }

    if (i4AddrFamily == AF_INET)
    {
        if (i4Size < UTL_INET_ADDRSTRLEN)
        {
            return (OSIX_FAILURE);
        }
        InAddr.u4Addr = *(const UINT4 *) pSrc;
        pc1IPAddr = UtlInetNtoa (InAddr);
        STRCPY (pc1Dest, pc1IPAddr);
    }
    else if (i4AddrFamily == AF_INET6)
    {
        if (i4Size < UTL_INET6_ADDRSTRLEN)
        {
            return (OSIX_FAILURE);
        }
        if (STRLEN (pSrc) >= sizeof (In6Addr.u1addr))
        {
            STRNCPY (In6Addr.u1addr, (const UINT1 *) pSrc,
                     sizeof (In6Addr.u1addr) - 1);
            In6Addr.u1addr[sizeof (In6Addr.u1addr) - 1] = '\0';
        }
        else
        {
            STRCPY (In6Addr.u1addr, (const UINT1 *) pSrc);
        }
        pc1IPAddr = UtlInetNtoa6 (In6Addr);
        STRCPY (pc1Dest, pc1IPAddr);
    }
    else
    {
        return OSIX_FAILURE;
    }
    return (OSIX_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : UtlInetPton                                      */
/*                                                                          */
/*    Description        : UtlInetPton converts an address in string format */
/*                         to its numeric binary form                       */
/*                                                                          */
/*    Input(s)           : i4AddrFamily - specifies the family of the       */
/*                                        address AF_INET or AF_INET6       */
/*                         pc1Src - pointer to the buffer where the string  */
/*                                  is stored                               */
/*                         pDest  - Pointer to the buffer holding the       */
/*                                  IPv4 or IPv6 address                    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : returns the IP address in numeric form           */
/*                                                                          */
/****************************************************************************/

INT4
UtlInetPton (INT4 i4AddrFamily, const CHR1 * pc1Src, VOID *pDest)
{
    tUtlIn6Addr         In6Addr;
    tUtlInAddr          InAddr;

    MEMSET (&InAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&In6Addr, 0, sizeof (tUtlIn6Addr));

    if (pc1Src == NULL || pDest == NULL)
    {
        return (OSIX_FAILURE);
    }

    if (i4AddrFamily == AF_INET)
    {
        InAddr.u4Addr = UtlInetAddr (pc1Src);
        MEMCPY (pDest, &InAddr.u4Addr, sizeof (InAddr));
    }
    else if (i4AddrFamily == AF_INET6)
    {
        UtlInetAton6 (pc1Src, &In6Addr);
        MEMCPY (pDest, In6Addr.u1addr, sizeof (In6Addr));
    }
    else
    {
        return OSIX_FAILURE;
    }
    return (OSIX_SUCCESS);
}

/************************************************************************
 *  Function Name   : UtlHex2Char
 *  Description     : This function will convert hexadecimal to alphabets
 *  Input           : pu1HexStr - string which has to be modified     
 *  Output          : Converted value
 *  Returns         : NONE                 
 ************************************************************************/

INT1
UtlHex2Char (UINT1 *pu1HexStr)
{
    INT1                ch1, ch2;

    ch1 = (INT1) toupper (pu1HexStr[0]);
    ch2 = (INT1) toupper (pu1HexStr[1]);
    ch1 = (INT1) ((ch1 >= 'A') ? (ch1 - 'A' + 10) : (ch1 - '0'));
    ch2 = (INT1) ((ch2 >= 'A') ? (ch2 - 'A' + 10) : (ch2 - '0'));
    ch1 <<= 4;
    ch1 += ch2;
    return ch1;
}
