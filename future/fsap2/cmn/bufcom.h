/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bufcom.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Common include file for SRM Buf module.
 *
 */

#ifndef _BUFCOM_H
#define _BUFCOM_H

#include "osxinc.h"
#include "bufdefn.h"
#include "bufmacs.h"
#include "buftdfs.h"

#include "bufglob.h"

#endif  /* _BUFCOM_H */
