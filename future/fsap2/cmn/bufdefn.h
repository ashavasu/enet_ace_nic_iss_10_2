/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bufdefn.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: SRM Buf module definitions.
 *
 */
#include "lr.h"
#ifndef  _BUFDEFN_H
#define  _BUFDEFN_H


#define CRU_BUF_STS_ALLOC_DONE       1
#define CRU_BUF_STS_ALLOC_FAIL       2
#define CRU_BUF_STS_RELSE_DONE       3
#define CRU_BUF_STS_RELSE_FAIL       4            

/* The following two are documented and hence validation check is done
 * based on these parameters only. 
 */
#define MIN_BLOCK_SIZE     128
#define MAX_BLOCK_SIZE     10000

#endif  /* _BUFDEFN_H  */
