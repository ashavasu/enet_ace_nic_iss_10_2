/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: buflib.c,v 1.20 2015/05/13 11:00:56 siva Exp $
 *
 * Description:
 *    This module  contains Buffer Management  Procedures to
 *    be used by Protocol Software Modules. These Procedures
 *    are  meant to  Allocate  or  Deallocate Free  Buffers,
 *    various Buffer copy and read/write operations normally
 *    used  by  the  protocol   software  modules  that  are
 *    involved in the protocol Packet processing.
 *
 */

#define BUFLIB_C

#include "bufcom.h"
#include "srmmemi.h"

#ifdef MEMTRACE_WANTED
#include "memtrace.h"
#endif

static tOsixSemId   gBufSemId;
#ifdef MEMTRACE_WANTED
tOsixSemId          gMemTraceSemId;
UINT1               gau1MemSem[] = "MEML";
#endif
/*****************************************************************************
*** All PRIVATE Buffer Routine Definition Starts Here                      ***
*****************************************************************************/
PRIVATE UINT4
      CRU_BUF_Get_ChainStartFreeByteCount (tCRU_BUF_CHAIN_DESC * pChainDesc);

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Allocate_ChainDesc                             */
/*  Description     : This Procedure allocates & returns a chain Descriptor  */
/*                    after Properly Initializing it.                        */
/*  Input(s)        : none                                                   */
/*  Output(s)       : none                                                   */
/*  Returns         : Pointer to the allocated Chain Descriptor              */
/*****************************************************************************/
#ifdef  __STDC__
PRIVATE tCRU_BUF_CHAIN_DESC *
CRU_BUF_Allocate_ChainDesc (void)
#else
PRIVATE tCRU_BUF_CHAIN_DESC *
CRU_BUF_Allocate_ChainDesc (void)
#endif                            /* __STDC__ */
{
    tCRU_BUF_CHAIN_DESC *pChnDesc = NULL;

    if ((pChnDesc = (tCRU_BUF_CHAIN_DESC *) (VOID *) MemAllocMemBlk
         (pCRU_BUF_Chain_FreeQueDesc->u2_QueId)) == NULL)
    {
        return NULL;
    }

    CRU_BUF_MEMSET (&(pChnDesc->ModuleData), 0, sizeof (tMODULE_DATA));

    pChnDesc->pNextChain = NULL;
    pChnDesc->pFirstValidDataDesc = NULL;

    pChnDesc->pFirstDataDesc = NULL;
    pChnDesc->pLastDataDesc = NULL;

    return pChnDesc;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Release_DataDesc                               */
/*  Description     : The procedure to release the Datadesc if u2_UsageCount */
/*                    reaches 0 .                                            */
/*  Input(s)        : pFirstDataDesc  - Pointer to the start of the Data     */
/*                            Descriptor (or first data descriptor)          */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Release_DataDesc (tCRU_BUF_DATA_DESC * pFirstDataDesc)
#else
PRIVATE INT4
CRU_BUF_Release_DataDesc (pFirstDataDesc)
     tCRU_BUF_DATA_DESC *pFirstDataDesc;
#endif /* __STDC__ */
{

    UINT2               u2PoolID = 0;

    tCRU_BUF_DATA_DESC *pTmpDataDesc = NULL;

    if (pFirstDataDesc == NULL)
        return CRU_FAILURE;

    if (OsixSemTake (gBufSemId) != OSIX_SUCCESS)
        return CRU_FAILURE;

    for (pTmpDataDesc = pFirstDataDesc->pNext; pFirstDataDesc;
         (pTmpDataDesc = (pTmpDataDesc) ? pTmpDataDesc->pNext : NULL))
    {
        pFirstDataDesc->u2_UsageCount--;

        if (!pFirstDataDesc->u2_UsageCount)
        {

            u2PoolID = pFirstDataDesc->u2_QueId;
            if (MemReleaseMemBlock
                (pCRU_BUF_DataBlk_FreeQueDesc[u2PoolID].u2_QueId,
                 (UINT1 *) pFirstDataDesc) == MEM_FAILURE)
            {
                OsixSemGive (gBufSemId);
                return CRU_FAILURE;
            }
        }

        pFirstDataDesc = pTmpDataDesc;
    }

    OsixSemGive (gBufSemId);

    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Allocate_DataDesc                              */
/*  Description     : This Procedure returns a Data_Buf_Desc linked with a   */
/*                    Data Buffer Of 'u4Size'. If 'u4Size' is 0, No Data     */
/*                    Buffer will be allocated, Only Data Descriptor is      */
/*                    returned.                                              */
/*  Input(s)        : u4Size   - Size of the Data Buffer to be allocated     */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the Data Descriptor with the requested size */
/*                    of buffer allocated on SUCCESS,  NULL on failure       */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE tCRU_BUF_DATA_DESC *
CRU_BUF_Allocate_DataDesc (UINT4 u4Size)
#else
PRIVATE tCRU_BUF_DATA_DESC *
CRU_BUF_Allocate_DataDesc (u4Size)
     UINT4               u4Size;
#endif /* __STDC__ */
{
    INT4                i4Index = 0, i4TempSize = 0;
    UINT4               u4TotalSize = 0;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    tCRU_BUF_DATA_DESC *pHeadDataDesc = NULL;

    tCRU_BUF_DATA_DESC *pPrevDataDesc = NULL;

    /*
     * If the requested size is < gu4MinBufferSize 
     * try to alloc a block of size gu4MinBufferSize.
     */
    i4TempSize = u4Size;

 /*************************************************************
 *** Return Data Descriptor Alone If Requested Size Is Zero ***
 **************************************************************/
    if (!u4Size)
    {
        if ((pDataDesc = (tCRU_BUF_DATA_DESC *) (VOID *) MemAllocMemBlk
             (pCRU_BUF_DataDesc_FreeQueDesc->u2_QueId)) == NULL)
        {
            return NULL;
        }

        pDataDesc->u2_QueId = pCRU_BUF_DataDesc_FreeQueDesc->u2_QueId;

        pDataDesc->pNext = NULL;
        pDataDesc->pPrev = NULL;
        pDataDesc->pu1_FirstByte = NULL;
        pDataDesc->pu1_FirstValidByte = NULL;
        pDataDesc->u4_ValidByteCount = 0;
        pDataDesc->u4_FreeByteCount = 0;
        pDataDesc->u2_UsageCount = 1;

        return pDataDesc;
    }

 /*************************************************************
 *** Check if the Requested Size can be allocated using the ***
 *** available data buffers and if not return NULL.         *** 
 **************************************************************/

    if (OsixSemTake (gBufSemId) != OSIX_SUCCESS)
        return NULL;

    for (i4Index = 0, u4TotalSize = 0; ((u4Size > u4TotalSize) &&
                                        (i4Index < (INT4) gu4MaxDataBlockCfgs)
                                        &&
                                        (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].
                                         u4_Size));
         (u4TotalSize +=
          pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u4_Size *
          (MEM_FREE_POOL_UNIT_COUNT
           (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u2_QueId))), i4Index++);

    if (u4TotalSize < u4Size)
    {
        OsixSemGive (gBufSemId);
        return NULL;
    }

 /*****************************************************************
 *** Allocate Optimally using the available data buffers        ***
 *** the request for a given size as follows :                  ***
 *** Search for a Data Buffer Block which is nearest in size to ***
 *** that Requested. Allocate a Data Descriptor for that Data   ***
 *** Buffer Block and properly assign pointers,return pointer   ***
 *** to the Data Descriptor.                                    *** 
 *** If all Data Buffer Blocks available are smaller in size    ***
 *** than that Requested,Chain the available Data Buffer Blocks ***
 *** (allocating a Data Descriptor for each Data Buffer Block)  ***
 *** properly link the Data Descriptor and update pointers      ***
 *** in each Data Descriptor structure,return a pointer the     ***
 *** first Data Descriptor.                                     ***
 ******************************************************************/

    pPrevDataDesc = NULL;
    while (i4TempSize > 0)
    {

        for (i4Index = 0; ((i4Index < (INT4) gu4MaxDataBlockCfgs) &&
                           (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u4_Size));
             i4Index++)
        {
            if ((MEM_FREE_POOL_UNIT_COUNT
                 (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u2_QueId))
                && (i4TempSize >=
                    (INT4) pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u4_Size))
            {
                break;
            }
        }

        if ((i4Index >= (INT4) gu4MaxDataBlockCfgs) ||
            (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u4_Size == 0))
        {
            i4Index--;
            while (MEM_FREE_POOL_UNIT_COUNT
                   (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u2_QueId) == 0)
            {
                i4Index--;
                if (i4Index < 0)
                {
                    OsixSemGive (gBufSemId);
                    CRU_BUF_Release_DataDesc (pHeadDataDesc);
                    return NULL;
                }
            }
        }

        /*  Allocation of right size datablk   */
        if (i4TempSize != (INT4) pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u4_Size)
        {
            if (MEM_FREE_POOL_UNIT_COUNT
                (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u2_QueId) == 0)
                i4Index++;
        }

        /* Allocation of Data Descriptor */
        if ((pDataDesc = (tCRU_BUF_DATA_DESC *) (VOID *) MemAllocMemBlk
             (pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u2_QueId)) == NULL)
        {
            OsixSemGive (gBufSemId);
            CRU_BUF_Release_DataDesc (pHeadDataDesc);
            return NULL;
        }

        i4TempSize -= pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u4_Size;
        pDataDesc->u2_QueId = (UINT2) i4Index;

        if (pPrevDataDesc)
            pPrevDataDesc->pNext = pDataDesc;
        else
        {
         /**** Save The First Data Descriptor (To Be Returned) ****/
            pHeadDataDesc = pDataDesc;
        }
        pDataDesc->pPrev = pPrevDataDesc;
        pDataDesc->pNext = NULL;
        pDataDesc->u2_UsageCount = 1;
        pDataDesc->pu1_FirstValidByte =
            (UINT1 *) pDataDesc + sizeof (tCRU_BUF_DATA_DESC);
        pDataDesc->pu1_FirstByte = pDataDesc->pu1_FirstValidByte;
        pDataDesc->u4_ValidByteCount = 0;
        pDataDesc->u4_FreeByteCount =
            pCRU_BUF_DataBlk_FreeQueDesc[i4Index].u4_Size;
        pPrevDataDesc = pDataDesc;
      /**** Becomes Previous Descriptor In Next Loop ****/
    }

    OsixSemGive (gBufSemId);

    return pHeadDataDesc;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Initialize_DataBlocks                          */
/*  Description     : Procedure to initialize the buffer pool records.       */
/*  Input(s)        : Buffer Configuration Parameter receivied from          */
/*                    application                                            */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/

#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Initialize_DataBlocks (tBufConfig * pBufCfg)
#else
PRIVATE INT4
CRU_BUF_Initialize_DataBlocks (pBufCfg)
     tBufConfig         *pBufCfg;
#endif /* __STDC__ */
{
    UINT4               PoolId = 0;

    pCRU_BUF_DataBlk_FreeQueDesc =
        CRU_BUF_CALLOC (sizeof (tCRU_BUF_FREE_QUE_DESC),
                        ((pBufCfg->u4MaxDataBlockCfg) + 1),
                        tCRU_BUF_FREE_QUE_DESC);
    if (!pCRU_BUF_DataBlk_FreeQueDesc)
        return CRU_FAILURE;

    for (PoolId = 0; PoolId < pBufCfg->u4MaxDataBlockCfg; PoolId++)
    {
        pCRU_BUF_DataBlk_FreeQueDesc[PoolId].u2_QueId = 0;
        pCRU_BUF_DataBlk_FreeQueDesc[PoolId].u4_UnitsCount =
            pBufCfg->DataBlkCfg[PoolId].u4NoofBlocks;
        pCRU_BUF_DataBlk_FreeQueDesc[PoolId].u4_Size =
            pBufCfg->DataBlkCfg[PoolId].u4BlockSize;

    }

    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Sort_DataBlocks                                */
/*  Description     : To sort the Data buffer Blocks to be allocated         */
/*                    according to  No Of Bytes(Size * No_Of_Units) in       */
/*                    descending order using Bubble Sort algorithm           */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE VOID
CRU_BUF_Sort_DataBlocks (void)
#else
PRIVATE VOID
CRU_BUF_Sort_DataBlocks (void)
#endif                            /* __STDC__ */
{
    UINT4               u4i = 0, u4j = 0, u4Count = 0;
    tCRU_BUF_FREE_QUE_DESC TmpQueDesc;

    for (u4Count = 0; ((u4Count < gu4MaxDataBlockCfgs) &&
                       (pCRU_BUF_DataBlk_FreeQueDesc[u4Count].u4_Size));
         u4Count++);

   /**** Now Sort Using Bubble Sort Algorithm ****/
    for (u4i = 0; u4i < u4Count; u4i++)
    {
        for (u4j = 0; u4j < (u4Count - u4i); u4j++)
        {
            if (pCRU_BUF_DataBlk_FreeQueDesc[u4j].u4_Size <
                pCRU_BUF_DataBlk_FreeQueDesc[u4j + 1].u4_Size)
            {

                TmpQueDesc.u2_QueId =
                    pCRU_BUF_DataBlk_FreeQueDesc[u4j].u2_QueId;
                TmpQueDesc.u4_UnitsCount =
                    pCRU_BUF_DataBlk_FreeQueDesc[u4j].u4_UnitsCount;
                TmpQueDesc.u4_Size = pCRU_BUF_DataBlk_FreeQueDesc[u4j].u4_Size;

                pCRU_BUF_DataBlk_FreeQueDesc[u4j].u2_QueId =
                    pCRU_BUF_DataBlk_FreeQueDesc[u4j + 1].u2_QueId;
                pCRU_BUF_DataBlk_FreeQueDesc[u4j].u4_UnitsCount =
                    pCRU_BUF_DataBlk_FreeQueDesc[u4j + 1].u4_UnitsCount;
                pCRU_BUF_DataBlk_FreeQueDesc[u4j].u4_Size =
                    pCRU_BUF_DataBlk_FreeQueDesc[u4j + 1].u4_Size;

                pCRU_BUF_DataBlk_FreeQueDesc[u4j + 1].u2_QueId =
                    TmpQueDesc.u2_QueId;
                pCRU_BUF_DataBlk_FreeQueDesc[u4j + 1].u4_UnitsCount =
                    TmpQueDesc.u4_UnitsCount;
                pCRU_BUF_DataBlk_FreeQueDesc[u4j + 1].u4_Size =
                    TmpQueDesc.u4_Size;
            }                    /* end of if */
        }                        /* end of for */
    }                            /* end of for */

    return;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Create_ChainDescPool                           */
/*  Description     : Procedure to create Message (Chain) Desc pool. This is */
/*                    called by  the Buffer library initialization  module.  */
/*  Input(s)        : u4MaxChainDesc - Max no. configured Chain descriptors  */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS    / CRU_FAILURE                           */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Create_ChainDescPool (UINT4 u4MaxChainDesc)
#else
PRIVATE INT4
CRU_BUF_Create_ChainDescPool (u4MaxChainDesc)
     UINT4               u4MaxChainDesc;
#endif /* __STDC__ */
{
    UINT4               u4PoolId = 0;

    pCRU_BUF_Chain_FreeQueDesc =
        CRU_BUF_MALLOC (sizeof (tCRU_BUF_FREE_QUE_DESC),
                        tCRU_BUF_FREE_QUE_DESC);
    if (!pCRU_BUF_Chain_FreeQueDesc)
        return CRU_FAILURE;

    CRU_BUF_MEMSET (pCRU_BUF_Chain_FreeQueDesc, 0,
                    sizeof (tCRU_BUF_FREE_QUE_DESC));

    pCRU_BUF_Chain_FreeQueDesc->u4_Size = sizeof (tCRU_BUF_CHAIN_DESC);
    pCRU_BUF_Chain_FreeQueDesc->u4_UnitsCount = u4MaxChainDesc;

    if (MemCreateMemPool
        (pCRU_BUF_Chain_FreeQueDesc->u4_Size,
         pCRU_BUF_Chain_FreeQueDesc->u4_UnitsCount, gu4MemoryType,
         &u4PoolId) == MEM_FAILURE)
        return CRU_FAILURE;

    pCRU_BUF_Chain_FreeQueDesc->u2_QueId = (UINT2) u4PoolId;

    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Delete_ChainDescPool                           */
/*  Description     : Procedure to delete Message (Chain) Descriptor pool    */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Delete_ChainDescPool (void)
#else
PRIVATE INT4
CRU_BUF_Delete_ChainDescPool (void)
#endif                            /* __STDC__ */
{
    if (pCRU_BUF_Chain_FreeQueDesc->u2_QueId != INVALID_POOL_ID)
    {
        if (MemDeleteMemPool (pCRU_BUF_Chain_FreeQueDesc->u2_QueId) ==
            MEM_FAILURE)
        {
            CRU_BUF_FREE (pCRU_BUF_Chain_FreeQueDesc);
            return CRU_FAILURE;
        }
    }

    CRU_BUF_FREE (pCRU_BUF_Chain_FreeQueDesc);
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Create_DataBlockPool                           */
/*  Description     : Procedure to create Data Block pool. This is called by */
/*                    the Buffer Library Initialization  module .            */
/*  Input(s)        : pBufCfg - Pointer to the application provided buffer   */
/*                              configuration information                    */
/*                    u1Flag = TRUE (Data+Data Descriptors) to be allocated  */
/*                    u1Flag = FALSE Only Data Descriptors to be allocated   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Create_DataBlockPool (tBufConfig * pBufCfg, UINT1 u1Flag)
#else
PRIVATE INT4
CRU_BUF_Create_DataBlockPool (pBufCfg, u1Flag)
     tBufConfig         *pBufCfg;
     UINT1               u1Flag;
#endif /* __STDC__ */
{
    UINT4               u4PoolId = 0;
    UINT4               u4QueIndex = 0;
    UINT4               u4ObjSize = 0;
    INT4                i4RetVal = 0;

    if (u1Flag == TRUE)
    {
        /*    Allocation for Data + Data Descriptor   */
        i4RetVal = CRU_BUF_Initialize_DataBlocks (pBufCfg);
        if (i4RetVal == CRU_FAILURE)
            return CRU_FAILURE;

        CRU_BUF_Sort_DataBlocks ();

        for (u4QueIndex = 0; ((u4QueIndex < pBufCfg->u4MaxDataBlockCfg) &&
                              (pCRU_BUF_DataBlk_FreeQueDesc[u4QueIndex].
                               u4_Size)); u4QueIndex++)
        {
            u4ObjSize =
                pCRU_BUF_DataBlk_FreeQueDesc[u4QueIndex].u4_Size +
                sizeof (tCRU_BUF_DATA_DESC);

            if (MemCreateMemPool
                (u4ObjSize,
                 pCRU_BUF_DataBlk_FreeQueDesc[u4QueIndex].u4_UnitsCount,
                 gu4MemoryType, &u4PoolId) == MEM_FAILURE)
                return CRU_FAILURE;

            pCRU_BUF_DataBlk_FreeQueDesc[u4QueIndex].u2_QueId =
                (UINT2) u4PoolId;

        }
    }

    else if (u1Flag == FALSE)
    {
        /*    Allocation for  Data Descriptor */
        pCRU_BUF_DataDesc_FreeQueDesc =
            CRU_BUF_MALLOC (sizeof (tCRU_BUF_FREE_QUE_DESC),
                            tCRU_BUF_FREE_QUE_DESC);
        if (!pCRU_BUF_DataDesc_FreeQueDesc)
            return CRU_FAILURE;

        CRU_BUF_MEMSET (pCRU_BUF_DataDesc_FreeQueDesc, 0,
                        sizeof (tCRU_BUF_FREE_QUE_DESC));

        pCRU_BUF_DataDesc_FreeQueDesc->u4_Size = sizeof (tCRU_BUF_DATA_DESC);

        /* Make the number of Data descriptors (alone) used in the fragmentation
         * to be equal to 20% of maximum number of data blocks configured. 
         */
        pCRU_BUF_DataDesc_FreeQueDesc->u4_UnitsCount =
            1 + (gu4MaxDataBlocks * 20) / 100;

        if (MemCreateMemPool
            (pCRU_BUF_DataDesc_FreeQueDesc->u4_Size,
             pCRU_BUF_DataDesc_FreeQueDesc->u4_UnitsCount, gu4MemoryType,
             &u4PoolId) == MEM_FAILURE)
            return CRU_FAILURE;
        pCRU_BUF_DataDesc_FreeQueDesc->u2_QueId = (UINT2) u4PoolId;
    }

    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Delete_DataBlockPool                           */
/*  Description     : Procedure to delete Data Blocks  pool                  */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Delete_DataBlockPool (void)
#else
PRIVATE INT4
CRU_BUF_Delete_DataBlockPool (void)
#endif                            /* __STDC__ */
{
    UINT4               u4QueIndex = 0;

    /*  Delete Data Block Pool */
    for (u4QueIndex = 0; u4QueIndex < gu4MaxDataBlockCfgs; u4QueIndex++)
    {

        if (pCRU_BUF_DataBlk_FreeQueDesc[u4QueIndex].u2_QueId !=
            INVALID_POOL_ID)
        {
            if (MemDeleteMemPool
                (pCRU_BUF_DataBlk_FreeQueDesc[u4QueIndex].u2_QueId) ==
                MEM_FAILURE)
            {
                return CRU_FAILURE;
            }
        }
    }

    /* Delete Data Descriptor Pool */
    if (pCRU_BUF_DataDesc_FreeQueDesc->u2_QueId != INVALID_POOL_ID)
    {
        if (MemDeleteMemPool (pCRU_BUF_DataDesc_FreeQueDesc->u2_QueId) ==
            MEM_FAILURE)
        {
            return CRU_FAILURE;
        }
    }

    CRU_BUF_FREE (pCRU_BUF_DataDesc_FreeQueDesc);

    CRU_BUF_FREE (pCRU_BUF_DataBlk_FreeQueDesc);
    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_LinearBufToChain                          */
/*  Description     : Procedure to Copy linear data to Chain. Updated the    */
/*                    ValidOffset of input chain.                            */
/*  Input(s)        : pChainDesc - Pointer to the input Message Chain        */
/*                    pDataDesc  - Pointer to the Data Descriptor which      */
/*                                 points to the data buffer which needs     */
/*                                 updation                                  */
/*                    pu1Data    - Starting point of the data in the data    */
/*                                 buffer starting from which copy has to be */
/*                                 done                                      */
/*                    pu1Src     - Pointer to the linear buffer from which   */
/*                                 the data will be copied to the chain      */
/*                    u4Offset   - Relative offset to the valid offset in the*/
/*                                 chained buffer at which point copy needs  */
/*                                 to be done                                */
/*                    u4Size     - Number of bytes to be copied from the     */
/*                                 linear buffer to the chain buffer         */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE VOID
CRU_BUF_Copy_LinearBufToChain (tCRU_BUF_CHAIN_DESC * pChainDesc,
                               tCRU_BUF_DATA_DESC * pDataDesc, UINT1 *pu1Data,
                               UINT1 *pu1Src, UINT4 u4Offset, UINT4 u4Size)
#else
PRIVATE VOID
CRU_BUF_Copy_LinearBufToChain (pChainDesc, pDataDesc, pu1Data, pu1Src, u4Offset,
                               u4Size)
     tCRU_BUF_CHAIN_DESC *pChainDesc;
     tCRU_BUF_DATA_DESC *pDataDesc;
     UINT1              *pu1Data;
     UINT1              *pu1Src;
     UINT4               u4Offset;
     UINT4               u4Size;
#endif /* __STDC__ */
{
    UINT4               u4CopyByteCount = 0;
    UINT4               u4ByteCount = 0;
    INT4                i4AddedByteCount = 0;

    /* Unused Parameter */
    pChainDesc = pChainDesc;
    u4Offset = u4Offset;

    while (u4Size > 0)
    {

        if (pDataDesc->pNext &&
            CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc->pNext))
            u4ByteCount = CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc);
        else
            u4ByteCount = CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc) +
                CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT (pDataDesc);

        if ((u4CopyByteCount =
             (u4ByteCount - (pu1Data - pDataDesc->pu1_FirstValidByte))) >
            u4Size)
            u4CopyByteCount = u4Size;

        if ((i4AddedByteCount = (pu1Data + u4CopyByteCount -
                                 pDataDesc->pu1_FirstValidByte -
                                 pDataDesc->u4_ValidByteCount)) > 0)
        {
            pDataDesc->u4_ValidByteCount += i4AddedByteCount;
            pDataDesc->u4_FreeByteCount -= i4AddedByteCount;
        }

        CRU_BUF_MEMCPY (pu1Data, pu1Src, u4CopyByteCount);
        pu1Src += u4CopyByteCount;
        u4Size -= u4CopyByteCount;

        if (pDataDesc->pNext)
        {
            pDataDesc = pDataDesc->pNext;
            pu1Data = pDataDesc->pu1_FirstValidByte;
        }
    }

    return;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_LinearBufFromChain                        */
/*  Description     : Procedure to extract 'u4Size' bytes from input buffer  */
/*                    into linear buffer                                     */
/*  Input(s)        : pChainDesc - Pointer to the message descriptor from    */
/*                                 which bytes needs to be copied            */
/*                    pDataDesc  - Pointer to the Data Descriptor which      */
/*                                 points to the data block which contains   */
/*                                 the data to be copied.                    */
/*                    pu1Data    - Pointer to the start of data in the data  */
/*                                 block from which copy needs to be done.   */
/*                    u4Offset   - Offset in the Chain buffer from which     */
/*                                 bytes are requested to be copied          */
/*                    u4Size     - Number of bytes to copy                   */
/*  Output(s)       : pu1Dst     - Pointer to the destination linear buffer  */
/*                                 to which the bytes needs to be copied.    */
/*  Returns         : Returns no of bytes copied                             */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Copy_LinearBufFromChain (tCRU_BUF_CHAIN_DESC * pChainDesc,
                                 tCRU_BUF_DATA_DESC * pDataDesc, UINT1 *pu1Data,
                                 UINT1 *pu1Dst, UINT4 u4Offset, UINT4 u4Size)
#else
PRIVATE INT4
CRU_BUF_Copy_LinearBufFromChain (pChainDesc, pDataDesc, pu1Data, pu1Dst,
                                 u4Offset, u4Size)
     tCRU_BUF_CHAIN_DESC *pChainDesc;
     tCRU_BUF_DATA_DESC *pDataDesc;
     UINT1              *pu1Data;
     UINT1              *pu1Dst;
     UINT4               u4Offset;
     UINT4               u4Size;
#endif /* __STDC__ */
{

    UINT4               u4CopyByteCount = 0;
    UINT4               u4OrigSize = 0;

    /* Unused Param */
    pChainDesc = pChainDesc;
    u4Offset = u4Offset;

    for (u4OrigSize = u4Size; pDataDesc && (u4Size > 0);)
    {
        if ((u4CopyByteCount =
             (CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc) -
              (pu1Data - pDataDesc->pu1_FirstValidByte))) >= u4Size)
            u4CopyByteCount = u4Size;
        CRU_BUF_MEMCPY ((VOID *) pu1Dst, (VOID *) pu1Data, u4CopyByteCount);
        pu1Dst += u4CopyByteCount;
        u4Size -= u4CopyByteCount;

        pDataDesc = pDataDesc->pNext;
        if (!pDataDesc)
            break;
        pu1Data = pDataDesc->pu1_FirstValidByte;
    }

    return (u4OrigSize - u4Size);

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_LastDataDesc                               */
/*  Description     : Procedure to get Last data desc of Chain form current  */
/*                    data desc given.                                       */
/*  Input(s)        : pDataDesc - Start of Data Descriptor whose tail end    */
/*                                is expected                                */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the Last data descriptor in the chain       */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE tCRU_BUF_DATA_DESC *
CRU_BUF_Get_LastDataDesc (tCRU_BUF_DATA_DESC * pDataDesc)
#else
PRIVATE tCRU_BUF_DATA_DESC *
CRU_BUF_Get_LastDataDesc (pDataDesc)
     tCRU_BUF_DATA_DESC *pDataDesc;
#endif /* __STDC__ */
{
    for (; pDataDesc->pNext; pDataDesc = pDataDesc->pNext);
    return pDataDesc;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_ChainStartFreeByteCount                    */
/*  Description     : Procedure to get Chain start free byte count before    */
/*                    pFirstValidDataDesc of chain                           */
/*  Input(s)        : pChainDesc - Pointer the Message chain whose number of */
/*                                 prepending free bytes is requested        */
/*  Output(s)       : None                                                   */
/*  Returns         : Number of Starting free bytes in the buffer            */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE UINT4
CRU_BUF_Get_ChainStartFreeByteCount (tCRU_BUF_CHAIN_DESC * pChainDesc)
#else
PRIVATE UINT4
CRU_BUF_Get_ChainStartFreeByteCount (pChainDesc)
     tCRU_BUF_CHAIN_DESC *pChainDesc;
#endif /* __STDC__ */
{
    UINT4               u4FreeByteCount = 0;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;

    for (pDataDesc = pChainDesc->pFirstValidDataDesc; pDataDesc;
         pDataDesc = pDataDesc->pPrev)
    {
        u4FreeByteCount +=
            CRU_BUF_GET_FIRST_BLOCK_FREE_BYTE_COUNT (pDataDesc,
                                                     pChainDesc->
                                                     pFirstValidDataDesc);
    }

    return u4FreeByteCount;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Split_MsgBufChainAtOffset                      */
/*  Description     : Procedure to split the Chain at given Data desc and its*/
/*                    internal data block offset. This is called by the      */
/*                    buffer fragmentation module .                          */
/*  Input(s)        : pChainDesc - Pointer to the Entire buffer which needs  */
/*                                 to be split.                              */
/*                    pDataDesc  - Pointer to the Data Descriptor which      */
/*                                 contains the data block, at which split   */
/*                                 needs to be done.                         */
/*                    pu1Data    - Pointer to the data in the data block, at */
/*                                 which the fragmentation needs to be done  */
/*  Output(s)       : p_pFragChainDescPtr - Pointer to the Split up or       */
/*                             fragmented chain which has resulted           */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Split_MsgBufChainAtOffset (tCRU_BUF_CHAIN_DESC * pChainDesc,
                                   tCRU_BUF_DATA_DESC * pDataDesc,
                                   UINT1 *pu1Data,
                                   tCRU_BUF_CHAIN_DESC ** ppFragChainDescPtr)
#else
PRIVATE INT4
CRU_BUF_Split_MsgBufChainAtOffset (pChainDesc, pDataDesc, pu1Data,
                                   ppFragChainDescPtr)
     tCRU_BUF_CHAIN_DESC *pChainDesc;
     tCRU_BUF_DATA_DESC *pDataDesc;
     UINT1              *pu1Data;
     tCRU_BUF_CHAIN_DESC **ppFragChainDescPtr;
#endif /* __STDC__ */
{

    tCRU_BUF_DATA_DESC *pFragDataDesc = NULL;
    tCRU_BUF_DATA_DESC *pTmpDataDesc = NULL;
    tCRU_BUF_CHAIN_DESC *pFragChainDesc = NULL;

    if ((*ppFragChainDescPtr = CRU_BUF_Allocate_ChainDesc ()) != NULL)
    {
        pFragChainDesc = *ppFragChainDescPtr;

        if (pDataDesc->pu1_FirstValidByte == pu1Data)
        {

            if (pDataDesc != pChainDesc->pFirstDataDesc)
                pDataDesc->pPrev->pNext = NULL;
            else
            {
                pChainDesc->pFirstDataDesc = NULL;
                pChainDesc->pFirstValidDataDesc = NULL;
            }

            pFragChainDesc->pLastDataDesc = pChainDesc->pLastDataDesc;
            pChainDesc->pLastDataDesc = pDataDesc->pPrev;
            pDataDesc->pPrev = NULL;
            pFragChainDesc->pFirstValidDataDesc = pDataDesc;
            pFragChainDesc->pFirstDataDesc =
                pFragChainDesc->pFirstValidDataDesc;

            return CRU_SUCCESS;
        }
        else
        {

            if ((pFragDataDesc = CRU_BUF_Allocate_DataDesc (0)) == NULL)
            {
                CRU_BUF_Release_MsgBufChain (*ppFragChainDescPtr, TRUE);
                return CRU_FAILURE;
            }

            pFragDataDesc->pu1_FirstValidByte = pu1Data;
            pFragDataDesc->pu1_FirstByte = pDataDesc->pu1_FirstByte;

            pFragDataDesc->u4_ValidByteCount = pDataDesc->u4_ValidByteCount -
                (pu1Data - pDataDesc->pu1_FirstValidByte);

            pFragDataDesc->u4_FreeByteCount = pDataDesc->u4_FreeByteCount;

            pFragDataDesc->pNext = pDataDesc->pNext;

            if (pDataDesc->pNext)
                pDataDesc->pNext->pPrev = pFragDataDesc;

            pTmpDataDesc =
                (tCRU_BUF_DATA_DESC *) ((VOID *) (pDataDesc->pu1_FirstByte -
                                                  sizeof (tCRU_BUF_DATA_DESC)));
            pTmpDataDesc->u2_UsageCount++;

            pFragChainDesc->pFirstDataDesc = pFragDataDesc;
            if (pChainDesc->pFirstDataDesc == pChainDesc->pLastDataDesc)
            {
                pFragChainDesc->pLastDataDesc = pFragChainDesc->pFirstDataDesc;
            }
            else
            {
                pFragChainDesc->pLastDataDesc = pChainDesc->pLastDataDesc;
            }
            pFragChainDesc->pFirstValidDataDesc = pFragDataDesc;
            pDataDesc->u4_ValidByteCount -= pFragDataDesc->u4_ValidByteCount;
            pDataDesc->u4_FreeByteCount = 0;
            pDataDesc->pNext = NULL;
            pChainDesc->pLastDataDesc = pDataDesc;

            return CRU_SUCCESS;
        }

    }
    else
        return CRU_FAILURE;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_MoveToReadOffset                               */
/*  Description     : Procedure to move to offfset for a read operation on   */
/*                    the chain. This moves to the specifed offset and if    */
/*                    the  movement is valid  returns the  Data desc ptr and */
/*                    offset pointer as return values or returns CRU_FAILURE */
/*                    as return value .                                      */
/*  Input(s)        : pChainDesc - Pointer to the buffer from which read     */
/*                                 operation is to be done in the specified  */
/*                                 offset                                    */
/*                    u4Offset   - Relative offset from the valid offset from*/
/*                                 which read needs to be done               */
/*  Output(s)       : ppDataDescPtr - Pointer to the data descriptor which   */
/*                                    has be data block from which data needs */
/*                                    to be read.                            */
/*                    ppu1DataPtr   - Pointer to the start of data in the    */
/*                                    data block from which to read          */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_MoveToReadOffset (tCRU_BUF_CHAIN_DESC * pChainDesc, UINT4 u4Offset,
                          tCRU_BUF_DATA_DESC ** ppDataDescPtr,
                          UINT1 **ppu1DataPtr)
#else
PRIVATE INT4
CRU_BUF_MoveToReadOffset (pChainDesc, u4Offset, ppDataDescPtr, ppu1DataPtr)
     tCRU_BUF_CHAIN_DESC *pChainDesc;
     UINT4               u4Offset;
     tCRU_BUF_DATA_DESC **ppDataDescPtr;
     UINT1             **ppu1DataPtr;
#endif /* __STDC__ */
{
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;

    /* Check if offset falls within the valid byte region */
    if (u4Offset >= CRU_BUF_Get_ChainValidByteCount (pChainDesc))
    {
        *ppDataDescPtr = NULL;
        *ppu1DataPtr = NULL;
        return CRU_FAILURE;
    }

    pDataDesc = pChainDesc->pFirstValidDataDesc;

    while (pDataDesc
           && (u4Offset + 1) > CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc))
    {
        u4Offset -= CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc);
        if (!pDataDesc->pNext)
            break;
        pDataDesc = pDataDesc->pNext;
    }

    if (pDataDesc == NULL)
    {
        return CRU_FAILURE;
    }
    if (u4Offset > CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc))
    {
        *ppDataDescPtr = NULL;
        *ppu1DataPtr = NULL;
        return CRU_FAILURE;
    }

    *ppDataDescPtr = pDataDesc;
    *ppu1DataPtr = pDataDesc->pu1_FirstValidByte + u4Offset;

    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_MoveToWriteOffset                              */
/*  Description     : Procedure to move to offfset for a write operation on  */
/*                    the chain. A check is made to ensure the chain has     */
/*                    enough space to move to the specified offset.If no     */
/*                    extra space is allocated dynamically  then the pointer */
/*                    to same will be returned. otherwise , CRU_FAILURE will */
/*                    be returned .                                          */
/*  Input(s)        : pChainDesc  - Pointer to the Message buffer in which   */
/*                                  write operation is going to be performed */
/*                    u4Offset    - Relative offset from the Valid Offset    */
/*                                  from which write operation is to be done */
/*                    u4Size      - Number of bytes to be copied             */
/*  Output(s)       : ppDataDescPtr  - Pointer to the Data Descriptor which  */
/*                                      has the data block in which copy has */
/*                                      to be done.                          */
/*                    ppu1DataPtr    - Pointer to the data in the data block */
/*                                     starting from which write operation   */
/*                                     needs to be done.                     */
/*  Returns         : CRU_SUCCESS/ CRU_FAILURE                               */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_MoveToWriteOffset (tCRU_BUF_CHAIN_DESC * pChainDesc, UINT4 u4Offset,
                           tCRU_BUF_DATA_DESC ** ppDataDescPtr,
                           UINT1 **ppu1DataPtr, UINT4 u4Size)
#else
PRIVATE INT4
CRU_BUF_MoveToWriteOffset (pChainDesc, u4Offset, ppDataDescPtr, ppu1DataPtr,
                           u4Size)
     tCRU_BUF_CHAIN_DESC *pChainDesc;
     UINT4               u4Offset;
     tCRU_BUF_DATA_DESC **ppDataDescPtr;
     UINT1             **ppu1DataPtr;
     UINT4               u4Size;
#endif /* __STDC__ */
{
    UINT1               u1BestFit = FALSE;
    UINT4               u4TmpOffset = 0, u4ValidByteCount = 0, u4FreeByteCount =
        0;
    UINT4               u4DescValidBytes = 0, u4DescFreeBytes = 0;
    INT4                i4AddedByteCount = 0;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;

    UINT1               u1Found = FALSE;
    UINT1               u1InitFlag = FALSE;
    UINT4               u4ByteCount = 0;
    UINT4               u4NumAllocBytes = 0;

    u4TmpOffset = u4ValidByteCount = u4FreeByteCount = u4DescFreeBytes = 0;

    for (pDataDesc = pChainDesc->pFirstValidDataDesc, u4TmpOffset = 0;
         pDataDesc; pDataDesc = pDataDesc->pNext)
    {
        u4DescValidBytes = CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc);
        u4DescFreeBytes = CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT (pDataDesc);

        if (!u4DescValidBytes)
        {
            if (u1Found == FALSE)
            {
                u1Found = TRUE;
		if ((pDataDesc != pChainDesc->pFirstValidDataDesc) &&
			          (pDataDesc->pPrev != NULL)) 

		{
			u4FreeByteCount =
				CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT (pDataDesc->
						pPrev);
		}
            }

            u4FreeByteCount +=
                CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT (pDataDesc);
        }

        u4ValidByteCount += u4DescValidBytes;
        u4TmpOffset += (u4DescValidBytes + u4DescFreeBytes);
        if (u4TmpOffset >= u4Offset)
        {
            if (u1BestFit == FALSE)
            {
                u1BestFit = TRUE;
                *ppDataDescPtr = pDataDesc;
                if (pDataDesc->pPrev)
                    *ppu1DataPtr = pDataDesc->pu1_FirstValidByte +
                        (u4DescValidBytes + u4DescFreeBytes) -
                        (u4TmpOffset - u4Offset);
                else
                    *ppu1DataPtr = pDataDesc->pu1_FirstValidByte + u4TmpOffset -
                        (u4TmpOffset - u4Offset);
            }
        }
    }

    if (u4TmpOffset >= (u4Offset + u4Size) && u1BestFit == TRUE)
    {
        pDataDesc = *ppDataDescPtr;

/* Valid Byte Count = valid bytes in the buffer starting pu1_FirstValidByte +
 * in between free spaces Free Byte Count = Trailing Free bytes in the buffer
 */
        if ((i4AddedByteCount =
             (*ppu1DataPtr - pDataDesc->pu1_FirstValidByte -
              pDataDesc->u4_ValidByteCount)) > 0)
        {
            pDataDesc->u4_ValidByteCount += i4AddedByteCount;
            pDataDesc->u4_FreeByteCount -= i4AddedByteCount;
        }
        return CRU_SUCCESS;
    }

   /**** If Write Operation And Space Not Available                  ****/
    /*  if ((u4_ValidByteCount+u4_FreeByteCount) <(u4_Offset+u4_Size))   */
    /* COSTLY OPERATION STARTS NOW DUE TO IMPROPER HANDLING OF BUFFER    */
    /* Allocated less then need, So get extra buffer from Free Pool      */

    else
    {
        u4NumAllocBytes =
            (u4Offset + u4Size) - (u4ValidByteCount + u4FreeByteCount);
        u4NumAllocBytes =
            (u4NumAllocBytes <
             gu4MinBufferSize) ? gu4MinBufferSize : u4NumAllocBytes;

        if (!(pDataDesc = CRU_BUF_Allocate_DataDesc (u4NumAllocBytes)))
            return CRU_FAILURE;

      /***** Link pDataDesc With pChainDesc *****/
        pChainDesc->pLastDataDesc->pNext = pDataDesc;
        pDataDesc->pPrev = pChainDesc->pLastDataDesc;
        pChainDesc->pLastDataDesc = CRU_BUF_Get_LastDataDesc (pDataDesc);
    }

    for (u4TmpOffset = 0, pDataDesc = pChainDesc->pFirstValidDataDesc;
         pDataDesc; pDataDesc = pDataDesc->pNext)
    {
        if (pDataDesc->pNext
            && CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc->pNext))
        {
            u4ByteCount = CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc);
        }
        else
        {
            u1InitFlag = TRUE;
            u4ByteCount =
                (CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc) +
                 CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT (pDataDesc));
        }

        u4TmpOffset += u4ByteCount;
        if (u4TmpOffset > u4Offset)
        {
            *ppDataDescPtr = pDataDesc;
            *ppu1DataPtr =
                pDataDesc->pu1_FirstValidByte + u4ByteCount - (u4TmpOffset -
                                                               u4Offset);

            if ((i4AddedByteCount =
                 (*ppu1DataPtr - pDataDesc->pu1_FirstValidByte -
                  pDataDesc->u4_ValidByteCount)) > 0)
            {
                pDataDesc->u4_ValidByteCount += i4AddedByteCount;
                pDataDesc->u4_FreeByteCount -= i4AddedByteCount;
            }

            return CRU_SUCCESS;
        }
        else
        {
            if (u1InitFlag == TRUE)
            {
                pDataDesc->u4_ValidByteCount += pDataDesc->u4_FreeByteCount;
                pDataDesc->u4_FreeByteCount = 0;
            }
        }
    }

    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Config_Validate                                */
/*  Description     : This procedure checks validity of buffer library       */
/*                    initialization parameters                                   */
/*  Input(s)        : pBufLibInitData - Pointer to the input buffer library  */
/*                                      initialization parameter to validate */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_FAILURE returned if                                */
/*                      Memory Type is invalid                               */
/*                      Max # of Chain Descriptors allocated is Zero         */
/*                      No. of Blocks allocated for any size is Zero         */
/*                      Min Block Size < 128 and Max Block Size > 4096       */
/*                      Block Size not a multiple of 4                       */
/*                    Otherwise, CRU_SUCCESS                                 */
/*****************************************************************************/
#ifdef __STDC__
PRIVATE INT4
CRU_BUF_Config_Validate (tBufConfig * pBufLibInitData)
#else
PRIVATE INT4
CRU_BUF_Config_Validate (pBufLibInitData)
     tBufConfig         *pBufLibInitData;
#endif /* __STDC__ */
{

    UINT4               u4ConfigVal = 0;
    UINT4               u4MinBufferSize = MAX_BLOCK_SIZE;

    /* Check validity of parameters 
     * check if Memory Type is valid - Default
     */

    if (pBufLibInitData->u4MemoryType != 0)
    {
        if (MemIsValidMemoryType (pBufLibInitData->u4MemoryType) != MEM_SUCCESS)
            return CRU_FAILURE;
    }

    /* Check validity of Max ChainDesc in Buffer Configuration */
    if ((pBufLibInitData->u4MaxChainDesc) == 0)
        return CRU_FAILURE;

    gu4MaxDataBlocks = 0;

    /* Check for validity of Data Block information for all configurations */
    for (u4ConfigVal = 0; u4ConfigVal < pBufLibInitData->u4MaxDataBlockCfg;
         u4ConfigVal++)
    {

        /* Number of data blocks cannot be ZERO */
        if ((pBufLibInitData->DataBlkCfg[u4ConfigVal].u4NoofBlocks) <= 0)
            return CRU_FAILURE;

        /* Get the total number of Data Blocks configured.  */
        gu4MaxDataBlocks +=
            (pBufLibInitData->DataBlkCfg[u4ConfigVal]).u4NoofBlocks;

        /* Block size should not be less than the MIN value and should not be 
         *  greater than the MAX value.
         */
        if ((pBufLibInitData->DataBlkCfg[u4ConfigVal].u4BlockSize <
             MIN_BLOCK_SIZE)
            || (pBufLibInitData->DataBlkCfg[u4ConfigVal].u4BlockSize >
                MAX_BLOCK_SIZE))
            return CRU_FAILURE;

        /* Block Size should always be in multiples of FOUR */
        if ((pBufLibInitData->DataBlkCfg[u4ConfigVal].u4BlockSize) % 4 != 0)
            return CRU_FAILURE;

        /* Try to find out the minimal configured block size value and store it 
         * in a global variable. 
         */
        if (pBufLibInitData->DataBlkCfg[u4ConfigVal].u4BlockSize <
            u4MinBufferSize)
            u4MinBufferSize =
                pBufLibInitData->DataBlkCfg[u4ConfigVal].u4BlockSize;

    }

    gu4MinBufferSize = u4MinBufferSize;

    return CRU_SUCCESS;
}

/*****************************************************
*** PUBLIC Buffer Routine Declarations Starts Here ***
******************************************************/

/*****************************************************************************/
/*  Function Name   : BufInitManager                                         */
/*  Description     : This is the initialization procedure that must be      */
/*                    called during system startup .  This module does the   */
/*                    following tasks:                                       */
/*                    1. Creates and initializes the Free Pool records array */
/*                    2. Creates Free Pool for Buffer Chain Descriptors .    */
/*                    3. Creates Free Pool for Data Descriptors(for          */
/*                       fragmentation which is application only in case of  */
/*                       chained buffers                                     */
/*                    4. Creates Free Pool for Actual Data Blocks.           */
/*  NOTE:   No other buffer management routines can be called  unless this   */
/*          routine has been  called  as the  first buffer manager call .    */
/*          This routine should be called ONLY once in the entire lifetime   */
/*          of an application .                                              */
/*  Input(s)        : pBufLibInitData  - Pointer to the buffer initialise    */
/*                            configuration data                             */
/*  Output(s)       :                                                        */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/

#ifdef __STDC__
EXPORT INT4
BufInitManager (tBufConfig * pBufLibInitData)
#else
EXPORT INT4
BufInitManager (pBufLibInitData)
     tBufConfig         *pBufLibInitData;
#endif /* __STDC__ */

{

    INT4                i4RetVal = 0;

    STRNCPY ((INT1 *) gau1SemName, "BUFS", STRLEN ("BUFS"));
    gau1SemName[STRLEN ("BUFS")] = '\0';

    /* Check whether buffer library is already initialized
     * using global semaphore  */
    if (OsixGetSemId (0, gau1SemName, &gBufSemId) == OSIX_SUCCESS)
        return CRU_FAILURE;

    /* Create semaphore to provide mutual exclusion   */
    if (OsixSemCrt (gau1SemName, &gBufSemId) != OSIX_SUCCESS)
    {
        return CRU_FAILURE;
    }

    /* OsixSemCrt creates sema4 with InitialCount of 0
     * So do a Give to fake an InitialCount of 1
     */
    OsixSemGive (gBufSemId);
#ifdef MEMTRACE_WANTED
    if (OsixSemCrt (gau1MemSem, &gMemTraceSemId) != OSIX_SUCCESS)
    {
        return CRU_FAILURE;
    }
    OsixSemGive (gMemTraceSemId);
#endif
    /*  Validate Init Configuration parameters  */
    if (CRU_BUF_Config_Validate (pBufLibInitData) == CRU_FAILURE)
        return CRU_FAILURE;

    gu4MaxDataBlockCfgs = pBufLibInitData->u4MaxDataBlockCfg;
    gu4MemoryType = pBufLibInitData->u4MemoryType;

    /* Create Chain Descriptor Pool */
    if (CRU_BUF_Create_ChainDescPool ((UINT4) pBufLibInitData->u4MaxChainDesc)
        != CRU_SUCCESS)
    {
        CRU_BUF_Shutdown_Manager ();
        return CRU_FAILURE;
    }

    /*  Create Data Descriptor alone - which will be used for fragmentation
     *   Flag FALSE passed - indicates allocation of descriptor only  
     */
    if ((i4RetVal =
         CRU_BUF_Create_DataBlockPool (pBufLibInitData, FALSE)) == CRU_FAILURE)
    {
        CRU_BUF_Shutdown_Manager ();
        return CRU_FAILURE;
    }

    /* For all Data Blocks configured Create Data Descriptor with data
     * Flag TRUE passed - indicates allocation of data + descriptor 
     */

    if ((i4RetVal =
         CRU_BUF_Create_DataBlockPool (pBufLibInitData, TRUE)) == CRU_FAILURE)
    {
        CRU_BUF_Shutdown_Manager ();
        return CRU_FAILURE;
    }

    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Allocate_MsgBufChain                           */
/*  Description     : This procedure allocates a Message buffer of 'u4Size'  */
/*                    from free pool.                                        */
/*  Input(s)        : u4Size  - Size of the buffer to be allocated           */
/*                    u4ValidDataOffset  - In the allocated buffer where     */
/*                          should the valid data be starting.  This is used */
/*                          basically to reserve space for headers of lower  */
/*                          layer protocols.                                 */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the newly allocated buffer on success,      */
/*                    NULL on failure                                        */
/*****************************************************************************/
#ifdef MEMTRACE_WANTED
#ifdef __STDC__
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Allocate_MsgBufChain_memtrace (UINT4 u4Size,
                                       UINT4 u4ValidDataOffset,
                                       const CHR1 * pi1File,
                                       INT4 i4Line, const CHR1 * pFunc)
#else
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Allocate_MsgBufChain_memtrace (u4Size, u4ValidDataOffset,
                                       pi1File, i4Line, pFunc)
     UINT4               u4Size;
     UINT4               u4ValidDataOffset;
     const CHR1         *pi1File;
     INT4                i4Line;
     const CHR1         *pFunc;
#endif /* __STDC__ */
#else
#ifdef __STDC__
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Allocate_MsgBufChain (UINT4 u4Size, UINT4 u4ValidDataOffset)
#else
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Allocate_MsgBufChain (u4Size, u4ValidDataOffset)
     UINT4               u4Size;
     UINT4               u4ValidDataOffset;
#endif /* __STDC__ */
#endif
{
    UINT1              *pu1LastByte = NULL;
    tCRU_BUF_CHAIN_DESC *pChainDesc = NULL;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    tCRU_BUF_DATA_DESC *pTmpDataDesc = NULL;

    if (u4ValidDataOffset >= u4Size)
        return NULL;

    if ((pChainDesc = CRU_BUF_Allocate_ChainDesc ()) == NULL)
    {
        /*When CruBuffLeak occurs, update the ISSHealthChkErrReason
           as CruBufExhausted and status as UP and recoverable */
        OSIX_SET_HEALTH_STATUS (UP_RECOVERABLE_RUNTIME_ERR);
        OSIX_SET_HEALTH_ERR_REASON (CRU_BUFF_EXHAUSTED);

        return NULL;
    }

    if ((pDataDesc = CRU_BUF_Allocate_DataDesc (u4Size)) == NULL)
    {
        /*When CruBuffLeak occurs, update the ISSHealthChkErrReason
           as CruBufExhausted and status as UP and recoverable */
        OSIX_SET_HEALTH_STATUS (UP_RECOVERABLE_RUNTIME_ERR);
        OSIX_SET_HEALTH_ERR_REASON (CRU_BUFF_EXHAUSTED);

        CRU_BUF_Release_MsgBufChain (pChainDesc, TRUE);
        return NULL;
    }

    pChainDesc->pFirstDataDesc = pDataDesc;
    for (pTmpDataDesc = pDataDesc; pTmpDataDesc->pNext;
         pTmpDataDesc = pTmpDataDesc->pNext);
    pChainDesc->pLastDataDesc = pTmpDataDesc;

   /***************************************************************
   *** Offsetting the first_data_byte :                         ***
   *** Offset is added to first_data_byte                       ***
   *** If the sum exceeds the last_valid_byte                   ***
   *** first_data_byte is made NULL and first_data_byte         ***
   *** Corresponding to the next Data Descriptor is incremented ***
   *** to the amount that exceeds the present Data Buffer size  ***
   *** this is repeated until Offsetting is finished or all Data***
   *** Buffers exhausts.                                        ***
   ***************************************************************/

    /* Move valid offset to appropriate position - parse thro' all buffers */
    pDataDesc->pu1_FirstValidByte += u4ValidDataOffset;
    for (pTmpDataDesc = pDataDesc; (pTmpDataDesc->pu1_FirstValidByte >
                                    (pu1LastByte =
                                     (pTmpDataDesc->pu1_FirstByte +
                                      (pTmpDataDesc->u4_FreeByteCount - 1))))
         && (pTmpDataDesc->pNext); pTmpDataDesc = pTmpDataDesc->pNext)
    {

        u4ValidDataOffset = pTmpDataDesc->pu1_FirstValidByte - pu1LastByte - 1;
        pTmpDataDesc->pu1_FirstValidByte = NULL;
        pTmpDataDesc->pNext->pu1_FirstValidByte += u4ValidDataOffset;
    }

    if ((pTmpDataDesc->pu1_FirstByte > pu1LastByte) && (!pTmpDataDesc->pNext))
    {
        CRU_BUF_Release_MsgBufChain (pChainDesc, TRUE);
        return NULL;
    }

    /* Offset portion used to reserve for header; Valid Byte Count - does not 
     * have any valid bytes written into it 
     */
    pTmpDataDesc->u4_FreeByteCount -= u4ValidDataOffset;

    pChainDesc->pFirstValidDataDesc = pTmpDataDesc;

    BUF_SYSTEM_DATA (pChainDesc) = buf_block_allocated;
#ifdef MEMTRACE_WANTED
    AddNode (pChainDesc, CRU_BUF_MEM_TYPE, pi1File, i4Line, pFunc);
#endif
    return pChainDesc;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Release_MsgBufChain                            */
/*  Description     : The procedure releases the mesg desc to mesg desc pool,*/
/*                    decrements the reference count of Data desc and        */
/*                    Data Blk. If the reference count of DataDesc/DataBlk   */
/*                    becomes '0' it releases them to respective buffer pools*/
/*                    When 'u1ForcedRelease' flag is set the referece count  */
/*                    will be ignored and the same will released immediately */
/*  Input(s)        : pChainDesc  - Pointer to the buffer which needs to be  */
/*                                  released                                 */
/*                    u1ForcedRelease - Flag which states whether to release */
/*                        the buffer without checking the reference count or */
/*                        not                                                */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Release_MsgBufChain (tCRU_BUF_CHAIN_HEADER * pChainDesc,
                             UINT1 u1ForcedRelease)
#else
EXPORT INT4
CRU_BUF_Release_MsgBufChain (pChainDesc, u1ForcedRelease)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT1               u1ForcedRelease;
#endif /* __STDC__ */
{

    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    tCRU_BUF_DATA_DESC *pTmpDataDesc = NULL;
    tCRU_BUF_DATA_DESC *pNextDataDesc = NULL;
    UINT4               u4FreeChain = OSIX_FALSE;
    UINT2               u2PoolID = 0;

    if (!pChainDesc)
    {
        return CRU_FAILURE;
    }
    else
    {
        if ((pChainDesc->pFirstDataDesc == NULL)
            || (pChainDesc->pFirstValidDataDesc == NULL))
            u4FreeChain = OSIX_TRUE;
    }

    if (OsixSemTake (gBufSemId) != OSIX_SUCCESS)
        return CRU_FAILURE;

    if (u4FreeChain == OSIX_FALSE)
    {
        for (pDataDesc = pChainDesc->pFirstDataDesc; pDataDesc;
             pDataDesc = pNextDataDesc)
        {
            pNextDataDesc = pDataDesc->pNext;
            pDataDesc->u2_UsageCount--;

            if (!((pDataDesc->u2_UsageCount > 0) && (u1ForcedRelease == FALSE)))
            {

                if (pDataDesc->pu1_FirstByte ==
                    ((UINT1 *) pDataDesc + sizeof (tCRU_BUF_DATA_DESC)))
                {
                    u2PoolID = pDataDesc->u2_QueId;
                    if (MemReleaseMemBlock
                        (pCRU_BUF_DataBlk_FreeQueDesc[u2PoolID].u2_QueId,
                         (UINT1 *) pDataDesc) == MEM_FAILURE)
                    {
                        OsixSemGive (gBufSemId);
                        return CRU_FAILURE;
                    }

                }
                else
                {
                    /* Fragmented Descriptor  */
                    pTmpDataDesc =
                        (tCRU_BUF_DATA_DESC
                         *) ((VOID *) (pDataDesc->pu1_FirstByte -
                                       sizeof (tCRU_BUF_DATA_DESC)));
                    pTmpDataDesc->u2_UsageCount--;

                    if (pTmpDataDesc->u2_UsageCount <= 0)
                    {
                        /* Release Fragmented Descriptor, DataBlk Desc */
                        u2PoolID = pTmpDataDesc->u2_QueId;
                        if (MemReleaseMemBlock
                            (pCRU_BUF_DataBlk_FreeQueDesc[u2PoolID].u2_QueId,
                             (UINT1 *) pTmpDataDesc) == MEM_FAILURE)
                        {
                            OsixSemGive (gBufSemId);
                            return CRU_FAILURE;
                        }
                        if (MemReleaseMemBlock
                            (pCRU_BUF_DataDesc_FreeQueDesc->u2_QueId,
                             (UINT1 *) pDataDesc) == MEM_FAILURE)
                        {
                            OsixSemGive (gBufSemId);
                            return CRU_FAILURE;
                        }
                    }

                    else
                    {
                        /* Release Fragmented Descriptor alone */
                        if (MemReleaseMemBlock
                            (pCRU_BUF_DataDesc_FreeQueDesc->u2_QueId,
                             (UINT1 *) pDataDesc) == MEM_FAILURE)
                        {
                            OsixSemGive (gBufSemId);
                            return CRU_FAILURE;
                        }
                    }
                }

            }
        }                        /* For */
    }

    /*  Release chain after data descriptors */
    if (MemReleaseMemBlock (pCRU_BUF_Chain_FreeQueDesc->u2_QueId,
                            (UINT1 *) pChainDesc) == MEM_FAILURE)
    {
        OsixSemGive (gBufSemId);
        return CRU_FAILURE;
    }

    OsixSemGive (gBufSemId);

    BUF_SYSTEM_DATA (pChainDesc) = buf_block_free;
#ifdef MEMTRACE_WANTED
    DelNode (pChainDesc);
#endif
    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_OverBufChain_AtEnd                        */
/*  Description     : Copies 'u4Size' bytes from  pu1Src to the u4Offset of  */
/*                    message Chain.                                         */
/*                    Unlike CRU_BUF_Copy_OverBufChain, this function        */
/*                    only considers the valid bytes. Thus it is useful      */
/*                    to "append" bytes to a chained buffer.                 */
/*  Input(s)        : pChainDesc   - Pointer to the buffer in which the copy */
/*                         needs to be done.                                 */
/*                    pu1Src  -  Pointer to the linear array from which copy */
/*                               to the buffer needs to be done.             */
/*                    u4Offset  - Offset(Relative to Valid offset)  in the   */
/*                               buffer starting from which copy needs to be */
/*                               done                                        */
/*                    u4Size   - Number of bytes to copy                     */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Copy_OverBufChain_AtEnd (tCRU_BUF_CHAIN_HEADER * pChainDesc,
                                 UINT1 *pu1Src, UINT4 u4Offset, UINT4 u4Size)
#else
EXPORT INT4
CRU_BUF_Copy_OverBufChain_AtEnd (pChainDesc, pu1Src, u4Offset, u4Size)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT1              *pu1Src;
     UINT4               u4Offset;
     UINT4               u4Size;
#endif /* __STDC__ */
{
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4DescFreeBytes;
    UINT4               u4CopyByteCount = 0;
    UINT4               u4FreeByteCount;

    pDataDesc = pChainDesc->pFirstValidDataDesc;
    while (pDataDesc
           && (u4Offset + 1) > CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc))
    {
        if (pDataDesc->pNext)
        {
            u4Offset -= CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc);
            pDataDesc = pDataDesc->pNext;
        }
        else
            break;
    }
    if (pDataDesc == NULL)
    {
        return CRU_FAILURE;
    }
    if (u4Offset > CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc))
    {
        return CRU_FAILURE;
    }

    pu1Data = pDataDesc->pu1_FirstValidByte + u4Offset;

    u4DescFreeBytes = (((pDataDesc->pu1_FirstValidByte +
                         pDataDesc->u4_ValidByteCount) - pu1Data) +
                       CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT (pDataDesc));
    if (u4DescFreeBytes < u4Size)
    {
        if (!(pDataDesc = CRU_BUF_Allocate_DataDesc (u4Size)))
            return (CRU_FAILURE);

        pChainDesc->pLastDataDesc->pNext = pDataDesc;
        pDataDesc->pPrev = pChainDesc->pLastDataDesc;
        pChainDesc->pLastDataDesc = CRU_BUF_Get_LastDataDesc (pDataDesc);

    }

    while (u4Size > 0)
    {
        u4FreeByteCount = CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT (pDataDesc);

        u4CopyByteCount = (u4FreeByteCount > u4Size) ? u4Size : u4FreeByteCount;

        pu1Data = pDataDesc->pu1_FirstValidByte + pDataDesc->u4_ValidByteCount;
        CRU_BUF_MEMCPY (pu1Data, pu1Src, u4CopyByteCount);
        pDataDesc->u4_ValidByteCount += u4CopyByteCount;
        pDataDesc->u4_FreeByteCount -= u4CopyByteCount;
        pu1Src += u4CopyByteCount;
        u4Size -= u4CopyByteCount;

        if (pDataDesc->pNext)
        {
            pDataDesc = pDataDesc->pNext;
            pu1Data = pDataDesc->pu1_FirstValidByte;
        }
    }
    return (CRU_SUCCESS);
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_OverBufChain                              */
/*  Description     : Copies 'u4Size' bytes from  pu1Src to the u4Offset of  */
/*                    message Chain                                          */
/*  Input(s)        : pChainDesc   - Pointer to the buffer in which the copy */
/*                         needs to be done.                                 */
/*                    pu1Src  -  Pointer to the linear array from which copy */
/*                               to the buffer needs to be done.             */
/*                    u4Offset  - Offset(Relative to Valid offset)  in the   */
/*                               buffer starting from which copy needs to be */
/*                               done                                        */
/*                    u4Size   - Number of bytes to copy                     */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Copy_OverBufChain (tCRU_BUF_CHAIN_HEADER * pChainDesc, UINT1 *pu1Src,
                           UINT4 u4Offset, UINT4 u4Size)
#else
EXPORT INT4
CRU_BUF_Copy_OverBufChain (pChainDesc, pu1Src, u4Offset, u4Size)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT1              *pu1Src;
     UINT4               u4Offset;
     UINT4               u4Size;
#endif /* __STDC__ */
{
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT1              *pu1Data = NULL;

    if (CRU_BUF_MoveToWriteOffset
        (pChainDesc, u4Offset, &pDataDesc, &pu1Data, u4Size) == 0)
    {
        CRU_BUF_Copy_LinearBufToChain (pChainDesc, pDataDesc, pu1Data, pu1Src,
                                       u4Offset, u4Size);
        return CRU_SUCCESS;
    }

    return CRU_FAILURE;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Copy_FromBufChain                              */
/*  Description     : Copies 'u4Size' bytes from Chain at the u4Offset to src*/
/*  Input(s)        : pChainDesc  - Buffer from which copy needs to be done  */
/*                    pu1Dst  - Array to which the specified buffer content  */
/*                              needs to be copied                           */
/*                    u4Offset  - Relative offset in the buffer from which   */
/*                         data needs to be read and copied to the dest      */
/*                    u4Size  - Requested number of bytes to copy            */
/*  Output(s)       : None                                                   */
/*  Returns         : Returns the number of  bytes copied from Chain         */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Copy_FromBufChain (tCRU_BUF_CHAIN_HEADER * pChainDesc, UINT1 *pu1Dst,
                           UINT4 u4Offset, UINT4 u4Size)
#else
EXPORT INT4
CRU_BUF_Copy_FromBufChain (pChainDesc, pu1Dst, u4Offset, u4Size)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT1              *pu1Dst;
     UINT4               u4Offset;
     UINT4               u4Size;
#endif /* __STDC__ */
{
    UINT1              *pu1Data = NULL;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;

    if (!pChainDesc || !pChainDesc->pFirstValidDataDesc)
    {
        return CRU_FAILURE;
    }

    if (CRU_BUF_MoveToReadOffset (pChainDesc, u4Offset, &pDataDesc, &pu1Data) ==
        CRU_SUCCESS)
    {
        return (CRU_BUF_Copy_LinearBufFromChain (pChainDesc, pDataDesc, pu1Data,
                                                 pu1Dst, u4Offset, u4Size));
    }

    return 0;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Concat_MsgBufChains                            */
/*  Description     : Procedure to Concatenate  two CRU buffer chains        */
/*  Input(s)        : pChainDesc1 - First Buffer  Chain to which the second  */
/*                       specified buffer chain is concatenated              */
/*                    pChainDesc2 - Second buffer chain                      */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
#ifdef __STDC__
EXPORT VOID
CRU_BUF_Concat_MsgBufChains (tCRU_BUF_CHAIN_HEADER * pChainDesc1,
                             tCRU_BUF_CHAIN_HEADER * pChainDesc2)
#else
EXPORT VOID
CRU_BUF_Concat_MsgBufChains (pChainDesc1, pChainDesc2)
     tCRU_BUF_CHAIN_HEADER *pChainDesc1;
     tCRU_BUF_CHAIN_HEADER *pChainDesc2;
#endif /* __STDC__ */
{

    tCRU_BUF_DATA_DESC *pDataDesc = NULL;

    for (pDataDesc = pChainDesc1->pFirstValidDataDesc;
         pDataDesc->pNext
         && CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc->pNext);
         pDataDesc = pDataDesc->pNext);

    if (pDataDesc->pNext)
    {
        pChainDesc1->pLastDataDesc = pDataDesc;
        pDataDesc->u4_FreeByteCount = 0;
        CRU_BUF_Release_DataDesc (pDataDesc->pNext);
        pDataDesc->pNext = NULL;
    }

    pChainDesc1->pLastDataDesc->pNext = pChainDesc2->pFirstDataDesc;
    pChainDesc2->pFirstDataDesc->pPrev = pChainDesc1->pLastDataDesc;
    pChainDesc1->pLastDataDesc = pChainDesc2->pLastDataDesc;
    pChainDesc2->pFirstDataDesc = pChainDesc2->pLastDataDesc = NULL;

    /*  Release chain after data descriptors */
    if (MemReleaseMemBlock (pCRU_BUF_Chain_FreeQueDesc->u2_QueId,
                            (UINT1 *) pChainDesc2) == MEM_FAILURE)
    {
        return;
    }

    return;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Fragment_BufChain                              */
/*  Description     : Procedure to Fragment the Chain  at size 'u4Size'      */
/*  Input(s)        : pChainDesc  - Pointer to the buffer to be fragmented   */
/*                    u4Size  -  Offset at which fragmentation needs to be   */
/*                               done                                        */
/*  Output(s)       : ppFragChainDescPtr  - Pointer to the fragment which is */
/*                          the result of the requested fragmentation        */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Fragment_BufChain (tCRU_BUF_CHAIN_HEADER * pChainDesc, UINT4 u4Size,
                           tCRU_BUF_CHAIN_HEADER ** ppFragChainDescPtr)
#else
EXPORT INT4
CRU_BUF_Fragment_BufChain (pChainDesc, u4Size, ppFragChainDescPtr)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT4               u4Size;
     tCRU_BUF_CHAIN_HEADER **ppFragChainDescPtr;
#endif /* __STDC__ */
{
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT1              *pu1Data = NULL;

    *ppFragChainDescPtr = NULL;

    if (CRU_BUF_MoveToReadOffset (pChainDesc, u4Size, &pDataDesc, &pu1Data) ==
        CRU_SUCCESS)
    {
        if (CRU_BUF_Split_MsgBufChainAtOffset (pChainDesc, pDataDesc, pu1Data,
                                               ppFragChainDescPtr) ==
            CRU_SUCCESS)
        {
            return CRU_SUCCESS;
        }
    }

    return CRU_FAILURE;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Prepend_BufChain                               */
/*  Description     : Procedure to prepend  u4Size bytes from pu1Src .       */
/*  Input(s)        : pChainDesc  - Pointer to the buffer in which prepend   */
/*                             needs to be done                              */
/*                    pu1Src   - Pointer to the array whose content needs to */
/*                           to be copied to the buffer                      */
/*                    u4Size  - Number of bytes to prepend                   */
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Prepend_BufChain (tCRU_BUF_CHAIN_HEADER * pChainDesc, UINT1 *pu1Src,
                          UINT4 u4Size)
#else
EXPORT INT4
CRU_BUF_Prepend_BufChain (pChainDesc, pu1Src, u4Size)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT1              *pu1Src;
     UINT4               u4Size;
#endif /* __STDC__ */
{

    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT4               u4FreeByteCount = 0, u4CopyByteCount = 0;

    UINT2               u2UsageCount = 0;

    u2UsageCount = pChainDesc->pFirstValidDataDesc->u2_UsageCount;

    if (u2UsageCount > 1)        /* Buffer seems to be have been duplicated. */
        u4FreeByteCount = 0;
    else
        u4FreeByteCount = CRU_BUF_Get_ChainStartFreeByteCount (pChainDesc);

    /* In case of Duplicated Buffer, New Buffer Allocated  */
    if (u4FreeByteCount < u4Size)
    {

        if ((pDataDesc =
             CRU_BUF_Allocate_DataDesc (u4Size - u4FreeByteCount)) == NULL)
            return CRU_FAILURE;

        pChainDesc->pFirstDataDesc->pPrev =
            CRU_BUF_Get_LastDataDesc (pDataDesc);
        pChainDesc->pFirstDataDesc->pPrev->pNext = pChainDesc->pFirstDataDesc;
        pChainDesc->pFirstDataDesc = pDataDesc;
    }

    if (u2UsageCount > 1)        /* Buffer seems to be have been duplicated. */
        pDataDesc = pChainDesc->pFirstValidDataDesc->pPrev;
    else
        pDataDesc = pChainDesc->pFirstValidDataDesc;

    for (; u4Size > 0; pDataDesc = pDataDesc->pPrev)
    {

        if ((u4CopyByteCount =
             CRU_BUF_GET_FIRST_BLOCK_FREE_BYTE_COUNT (pDataDesc,
                                                      pChainDesc->
                                                      pFirstValidDataDesc)) > 0)
        {

            if (u4CopyByteCount > u4Size)
                u4CopyByteCount = u4Size;

            if (pDataDesc != pChainDesc->pFirstValidDataDesc)
            {
                pDataDesc->pu1_FirstValidByte =
                    pDataDesc->pu1_FirstByte + pDataDesc->u4_FreeByteCount;
                pDataDesc->u4_FreeByteCount = 0;
            }

            pDataDesc->pu1_FirstValidByte -= u4CopyByteCount;

            if (pu1Src != NULL)
            {
                CRU_BUF_MEMCPY (pDataDesc->pu1_FirstValidByte,
                                (pu1Src + u4Size - u4CopyByteCount),
                                u4CopyByteCount);
            }

            pDataDesc->u4_ValidByteCount += u4CopyByteCount;
        }

        if (!(u4Size -= u4CopyByteCount))
            pChainDesc->pFirstValidDataDesc = pDataDesc;
    }

    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Duplicate_BufChain                             */
/*  Description     : Procedure to duplicate the buffer (Chain )             */
/*  Input(s)        : pChainDesc  - Pointer to the buffer to be duplicated   */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the buffer which has been duplicated        */
/*****************************************************************************/
#ifdef MEMTRACE_WANTED
#ifdef __STDC__
EXPORT tCRU_BUF_CHAIN_DESC *
CRU_BUF_Duplicate_BufChain_memtrace (tCRU_BUF_CHAIN_HEADER * pChainDesc,
                                     const CHR1 * pi1File,
                                     INT4 i4Line, const CHR1 * pFunc)
#else
EXPORT tCRU_BUF_CHAIN_DESC *
CRU_BUF_Duplicate_BufChain_memtrace (pChainDesc, pi1File, i4Line, pFunc)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     const CHR1         *pi1File;
     INT4                i4Line;
     const CHR1         *pFunc;
#endif /* __STDC__ */
#else
#ifdef __STDC__
EXPORT tCRU_BUF_CHAIN_DESC *
CRU_BUF_Duplicate_BufChain (tCRU_BUF_CHAIN_HEADER * pChainDesc)
#else
EXPORT tCRU_BUF_CHAIN_DESC *
CRU_BUF_Duplicate_BufChain (pChainDesc)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
#endif /* __STDC__ */
#endif
{

    tCRU_BUF_CHAIN_DESC *pDupChainDesc = NULL;
    tCRU_BUF_DATA_DESC *pTmpDataDesc = NULL;

    if (!(pDupChainDesc = CRU_BUF_Allocate_ChainDesc ()))
        return NULL;

    CRU_BUF_MEMCPY (pDupChainDesc, pChainDesc, sizeof (tCRU_BUF_CHAIN_DESC));
    pDupChainDesc->pNextChain = NULL;

    /* Increment the Usage count in all the data descriptors */
    pTmpDataDesc = pDupChainDesc->pFirstDataDesc;
    for (; pTmpDataDesc; pTmpDataDesc = pTmpDataDesc->pNext)
        pTmpDataDesc->u2_UsageCount++;

#ifdef MEMTRACE_WANTED
    AddNode (pDupChainDesc, CRU_BUF_MEM_TYPE, pi1File, i4Line, pFunc);
#endif
    return pDupChainDesc;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Link_BufChains                                 */
/*  Description     : Procedure to link  two mesg_chains in linked list .    */
/*  Input(s)        : pChainDesc1 - Pointer to the first message             */
/*                    pChainDesc2 - Pointer to the second message            */
/*  Output(s)       : None                                                   */
/*  Returns         : None                                                   */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Link_BufChains (tCRU_BUF_CHAIN_HEADER * pChainDesc1,
                        tCRU_BUF_CHAIN_HEADER * pChainDesc2)
#else
EXPORT INT4
CRU_BUF_Link_BufChains (pChainDesc1, pChainDesc2)
     tCRU_BUF_CHAIN_HEADER *pChainDesc1;
     tCRU_BUF_CHAIN_HEADER *pChainDesc2;
#endif /* __STDC__ */
{
    pChainDesc2->pNextChain = pChainDesc1->pNextChain;
    pChainDesc1->pNextChain = pChainDesc2;
    return CRU_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_UnLink_BufChains                               */
/*  Description     : Procedure to unlink  two mesg_chains in linked list .  */
/*  Input(s)        : pChainDesc  - Pointer to the message chaine in which   */
/*                         the head message will be unlinked                 */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the message which has been unlinked         */
/*****************************************************************************/
#ifdef __STDC__
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_UnLink_BufChain (tCRU_BUF_CHAIN_HEADER * pChainDesc)
#else
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_UnLink_BufChain (pChainDesc)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
#endif /* __STDC__ */
{

    tCRU_BUF_CHAIN_DESC *pTmpChainDesc = NULL;

    pTmpChainDesc = pChainDesc->pNextChain;
    pChainDesc->pNextChain = NULL;

    return pTmpChainDesc;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_ChainValidByteCount                        */
/*  Description     : Procedure to get number of valid bytes of Chain        */
/*  Input(s)        : pChainDesc   - Pointer to the buffer whose valid byte  */
/*                          count is requested                               */
/*  Output(s)       : None                                                   */
/*  Returns         : Number of valid data bytes of chain                    */
/*****************************************************************************/
#ifdef __STDC__
EXPORT UINT4
CRU_BUF_Get_ChainValidByteCount (tCRU_BUF_CHAIN_HEADER * pChainDesc)
#else
EXPORT UINT4
CRU_BUF_Get_ChainValidByteCount (pChainDesc)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
#endif /* __STDC__ */
{
    UINT4               u4ValidByteCount = 0;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;

    for (pDataDesc = pChainDesc->pFirstValidDataDesc, u4ValidByteCount = 0;
         pDataDesc; pDataDesc = pDataDesc->pNext)
    {
        u4ValidByteCount += CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pDataDesc);
    }

    return u4ValidByteCount;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Delete_BufChainAtEnd                           */
/*  Description     : Procedure to Delete 'u4Size' bytes at the end of chain */
/*  Input(s)        : pChainDesc  - Pointer to the buffer which needs to be  */
/*                           trimmed                                         */
/*                    u4Size    - Number of bytes to trim                    */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to the trimmed buffer                          */
/*****************************************************************************/
#ifdef __STDC__
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Delete_BufChainAtEnd (tCRU_BUF_CHAIN_HEADER * pChainDesc, UINT4 u4Size)
#else
EXPORT tCRU_BUF_CHAIN_HEADER *
CRU_BUF_Delete_BufChainAtEnd (pChainDesc, u4Size)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT4               u4Size;
#endif /* __STDC__ */
{

    UINT4               u4ValidByteCount = 0;
    tCRU_BUF_DATA_DESC *pDataDesc = NULL;

    UINT1              *pu1Data = NULL;
    tCRU_BUF_DATA_DESC *pFreeDataDesc = NULL;

    if ((u4ValidByteCount = CRU_BUF_Get_ChainValidByteCount (pChainDesc)) <=
        u4Size)
    {
        CRU_BUF_Release_MsgBufChain (pChainDesc, FALSE);
        return NULL;
    }

    if (CRU_BUF_MoveToReadOffset
        (pChainDesc, (u4ValidByteCount - u4Size - 1), &pDataDesc,
         &pu1Data) != CRU_SUCCESS)
        return NULL;

    if (pChainDesc->pLastDataDesc == pDataDesc)
    {
        pDataDesc->u4_ValidByteCount -= u4Size;
        pDataDesc->u4_FreeByteCount += u4Size;
    }
    else
    {
        pDataDesc->u4_FreeByteCount += (pDataDesc->u4_ValidByteCount -
                                        (pu1Data + 1 -
                                         pDataDesc->pu1_FirstValidByte));
        pDataDesc->u4_ValidByteCount =
            (pu1Data + 1 - pDataDesc->pu1_FirstValidByte);
        pDataDesc->pNext->pPrev = NULL;
        pFreeDataDesc = pDataDesc->pNext;
        pDataDesc->pNext = NULL;
        CRU_BUF_Release_DataDesc (pFreeDataDesc);
        pChainDesc->pLastDataDesc = pDataDesc;
    }

    return pChainDesc;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Move_ValidOffset                               */
/*  Description     : Procedure to move the valid_offset to u4UnitCount.     */
/*                    The u4_ValidByteCount of Chain is updated .            */
/*  Input(s)        : pChainDesc   - Pointer to the buffer                   */
/*                    u4UnitCount  - Number of units to move the valid offset*/
/*  Output(s)       : None                                                   */
/*  Returns         : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Move_ValidOffset (tCRU_BUF_CHAIN_HEADER * pChainDesc, UINT4 u4UnitCount)
#else
EXPORT INT4
CRU_BUF_Move_ValidOffset (pChainDesc, u4UnitCount)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT4               u4UnitCount;
#endif /* __STDC__ */
{

    tCRU_BUF_DATA_DESC *pDataDesc = NULL;
    UINT1              *pu1Data = NULL;
    tCRU_BUF_DATA_DESC *pTmpDataDesc = NULL;

    /* Check if Offset greater than valid byte count of chain */
    if (u4UnitCount > CRU_BUF_Get_ChainValidByteCount (pChainDesc))
        return CRU_FAILURE;

    if (CRU_BUF_MoveToWriteOffset
        (pChainDesc, u4UnitCount, &pDataDesc, &pu1Data, 0) < 0)
        return CRU_FAILURE;

    for (pTmpDataDesc = pChainDesc->pFirstValidDataDesc;
         pTmpDataDesc != pDataDesc; pTmpDataDesc = pTmpDataDesc->pNext)
    {
        pTmpDataDesc->u4_FreeByteCount += pTmpDataDesc->u4_ValidByteCount;
        pTmpDataDesc->u4_ValidByteCount = 0;
        pTmpDataDesc->pu1_FirstValidByte = pTmpDataDesc->pu1_FirstByte;
    }

    pDataDesc->u4_ValidByteCount -= (pu1Data - pDataDesc->pu1_FirstValidByte);
    pDataDesc->pu1_FirstValidByte = pu1Data;

    pChainDesc->pFirstValidDataDesc = pDataDesc;

    return CRU_SUCCESS;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Get_DataPtr_IfLinear                           */
/*  Description     : Procedure to check if u4Units from u4Offset are        */
/*                    linear in the chain buffer. A pointer to the           */
/*                    linear buffer will be returned if successful, NULL     */
/*                    otherwise.                                                    */
/*  Input(s)        : pChainDesc  - Pointer to the buffer                    */
/*                    u4Offset    - Offset (Relative) from which the buffer  */
/*                                   is checked  to be linear                */
/*                      u4Units         - Number of units expected to be linear    */
/*  Output(s)       : None                                                   */
/*  Returns         : Pointer to linear buffer start returned or NULL        */
/*****************************************************************************/
#ifdef __STDC__
EXPORT UINT1       *
CRU_BUF_Get_DataPtr_IfLinear (tCRU_BUF_CHAIN_HEADER * pChainDesc,
                              UINT4 u4Offset, UINT4 u4Units)
#else
EXPORT UINT1       *
CRU_BUF_Get_DataPtr_IfLinear (pChainDesc, u4Offset, u4Units)
     tCRU_BUF_CHAIN_HEADER *pChainDesc;
     UINT4               u4Offset;
     UINT4               u4Units;
#endif /* __STDC__ */
{

    tCRU_BUF_DATA_DESC *pTmpDataDesc = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4TmpOffset = 0;

    /* check if pointer to Message is valid */

    if ((CRU_BUF_MoveToReadOffset
         (pChainDesc, u4Offset, &pTmpDataDesc, &pu1Data)) != CRU_SUCCESS)
        return NULL;

    u4TmpOffset = CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pTmpDataDesc);

    /* Offset in pTmpDataDesc - recalculate */
    u4Offset = pu1Data - pTmpDataDesc->pu1_FirstValidByte;

    /* Total Byte Count starting pu1_FirstByte 
     *  Check if linear starting specified offset from the beginning of chain
     */
    if (u4TmpOffset >= (u4Offset + u4Units))
        return (pu1Data);
    else
        return NULL;

}

/*****************************************************************************/
/*  Function Name   : CRU_BUF_Shutdown_Manager                               */
/*  Description     : Procedure to  release memory pools allocated for the   */
/*                      chain descriptors, data descriptors and data blocks.   */
/*  Input(s)        : none                                                           */
/*  Output(s)       : none                                                           */
/*  Return(s)       : CRU_SUCCESS / CRU_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
EXPORT INT4
CRU_BUF_Shutdown_Manager ()
#else
EXPORT INT4
CRU_BUF_Shutdown_Manager ()
#endif                            /* __STDC__ */
{

    if (gBufSemId == 0)
    {
        return (CRU_FAILURE);
    }

    STRCPY ((INT1 *) gau1SemName, "BUFS");
    /* Delete semaphore created for Mutual Exclusion  */
    OsixSemDel (gBufSemId);

    if ((pCRU_BUF_Chain_FreeQueDesc == NULL) ||
        (pCRU_BUF_DataDesc_FreeQueDesc == NULL) ||
        (pCRU_BUF_DataBlk_FreeQueDesc == NULL))
        return CRU_SUCCESS;

    /*   Delete Chain Descriptor Pool  */
    if (CRU_BUF_Delete_ChainDescPool () != CRU_SUCCESS)
        return CRU_FAILURE;

    /*   Delete Data Block and Data Descriptor Pool */
    if (CRU_BUF_Delete_DataBlockPool () != CRU_SUCCESS)
        return CRU_FAILURE;

    gu4MaxDataBlockCfgs = 0;
    gu4MinBufferSize = 0;
    gu4MemoryType = 0;
    gu4MaxDataBlocks = 0;
    gBufSemId = 0;

    return CRU_SUCCESS;

}

/************************************************************************************** 
 * FUNCTION NAME: CRU_BUF_Copy_BufChains
 *
 * DESCRIPTION : This function is used to copy the contents of source buffer
 *               to the destination buffer of the source & destination
 *               buffer chains respectively.
 *
 * INPUTS      : pDstChainDesc - Destination chain descriptor
 *               pSrcChainDesc - Source chain descriptor
 *               u4DstOffset   - Destination offset
 *               u4SrcOffset   - Source offset
 *               u4Size        - Size of data to be copied
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : Number of bytes copied.
 *
 ***************************************************************************************/

INT4
CRU_BUF_Copy_BufChains (tCRU_BUF_CHAIN_HEADER * pDstChainDesc,
                        tCRU_BUF_CHAIN_HEADER * pSrcChainDesc,
                        UINT4 u4DstOffset, UINT4 u4SrcOffset, UINT4 u4Size)
{

    tCRU_BUF_DATA_DESC *pSrcDataDesc = NULL;
    UINT1              *pu1SrcData = NULL;
    UINT4               u4SrcSize = 0;

    /* Note : This CRU_BUF_Copy_BufChains works only if the src and the destination chain
       descriptors are pointing to different memory location . If src and destination
       are same this API will not give correct expected result. */
    if (CRU_BUF_MoveToReadOffset (pSrcChainDesc, u4SrcOffset, &pSrcDataDesc,
                                  &pu1SrcData) == CRU_FAILURE)
    {
        return CRU_FAILURE;
    }

    while (pSrcDataDesc && (u4Size > 0))
    {
        if ((u4SrcSize =
             (CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT (pSrcDataDesc) -
              (pu1SrcData - pSrcDataDesc->pu1_FirstValidByte))) >= u4Size)
            u4SrcSize = u4Size;

        if (CRU_BUF_Copy_OverBufChain (pDstChainDesc, pu1SrcData,
                                       u4DstOffset, u4SrcSize) != CRU_SUCCESS)
        {
            return CRU_FAILURE;
        }

        u4Size -= u4SrcSize;
        u4DstOffset += u4SrcSize;

        if (u4Size == 0)
        {
            return CRU_SUCCESS;
        }
        pSrcDataDesc = pSrcDataDesc->pNext;

        if (!pSrcDataDesc)
        {
            return CRU_FAILURE;
        }
        pu1SrcData = pSrcDataDesc->pu1_FirstValidByte;
    }

    return CRU_FAILURE;
}
