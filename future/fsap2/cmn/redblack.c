/* $Id: redblack.c,v 1.19 2017/09/22 12:25:32 siva Exp $ */
#include "osxinc.h"
#include "redblack.h"

#undef  DEBUG

#ifdef DEBUG
#include <stdio.h>
#include <assert.h>
#define trace(s) fprintf(stderr, "%s:%d (%s) %s\n", __FILE__, __LINE__, __FUNCTION__, (s));
#else
#define trace(s)
#endif

#define    IS_LEFT(p)    ((p) == (p)->parent->child[0])
#define    IS_RIGHT(p)    ((p) == (p)->parent->child[1])
#define    IS_ROOT(p)    ((p)->parent->parent == 0)
#define SIBLING(p)    (((p)->parent->child[0] == (p)) ? ((p)->parent->child[1]) : ((p)->parent->child[0]))
#define    SLANT(p)    ((p) == (p)->parent->child[1])
#define IS_RED(e)    ((e) && (e)->red)
#define IS_BLACK(e)    (((e) == 0) || !((e)->red))
#define IS_BLACK_STRICT(e)    ((e) && !((e)->red))

#define RB_SEM_TAKE(T)\
do{\
if(T->b1MutualExclusive==OSIX_TRUE)\
{OsixSemTake(T->SemId);}\
}while(0)
#define RB_SEM_GIVE(T)\
do{\
if(T->b1MutualExclusive==OSIX_TRUE)\
{OsixSemGive(T->SemId);}\
}while(0)
#define RB_SEM_CRT(au1Name,pSemId) OsixSemCrt(au1Name,pSemId)
#define RB_SEM_DEL(semId) OsixSemDel(semId)
#define T_INORDER 0
#define T_PREORDER 1
#define T_POSTORDER 2

tOsixSemId          gu4RbSemArray[MAX_RBTREE_INST] = { 0 };
tOsixSemId          gu4SemNameSemId;

/** Local Functions *********************************************************/
static YTNODE      *
setchild (YTNODE * p, int dir, YTNODE * c)
{
    p->child[dir] = c;
    if (c)
    {
        c->parent = p;
    }
    return c;
}

/** Binary Tree Routines ****************************************************/
static YTNODE      *
ytFirst (YTREE * tree, int order)
{
    YTNODE             *p = tree->root.child[0];

    if (!p)
    {
        return 0;
    }

    switch (order)
    {
        case T_INORDER:
            while (p->child[0])
            {
                p = p->child[0];
            }
            return p;
        case T_POSTORDER:
            while (p->child[0] || p->child[1])
            {
                if (p->child[0])
                {
                    p = p->child[0];
                }
                else
                {
                    p = p->child[1];
                }
            }
            return p;
        case T_PREORDER:
            return p;
        default:
            return 0;
    }
}

static YTNODE      *
ytNext (YTNODE * p, int order)
{
    switch (order)
    {
        case T_PREORDER:
            if (p->child[0])
            {
                return p->child[0];
            }
            else if (p->child[1])
            {
                return p->child[1];
            }
            else if (IS_ROOT (p))
            {
                return 0;
            }
            else if (IS_LEFT (p))
            {
                if (p->parent->child[1])
                {
                    return p->parent->child[1];
                }
                else
                {
                    p = p->parent;
                }
            }
            else
            {
                p = p->parent;
            }

            while (!IS_ROOT (p))
            {
                if (IS_LEFT (p))
                {
                    if (p->parent->child[1])
                    {
                        return p->parent->child[1];
                    }
                    else
                    {
                        p = p->parent;
                    }
                }
                else
                {
                    p = p->parent;
                }
            }
            return 0;

        case T_POSTORDER:
            if (IS_ROOT (p))
            {
                return 0;
            }
            else if (IS_LEFT (p))
            {
                if (p->parent->child[1])
                {
                    p = p->parent->child[1];
                    while (p->child[0] || p->child[1])
                    {
                        if (p->child[0])
                        {
                            p = p->child[0];
                        }
                        else
                        {
                            p = p->child[1];
                        }
                    }
                    return (YTNODE *) p;
                }
                else
                {
                    return p->parent;
                }
            }
            else
            {
                return p->parent;
            }
        case T_INORDER:
            if (p->child[1])
            {
                p = p->child[1];
                while (p->child[0])
                {
                    p = p->child[0];
                }
                return (YTNODE *) p;
            }
            else
            {
                while (IS_RIGHT (p))
                {
                    p = p->parent;
                    if (IS_ROOT (p))
                    {
                        return 0;
                    }
                }
                return IS_ROOT (p) ? 0 : p->parent;
            }

        default:
            return 0;
    }
}

static YTNODE      *
ytFind (YTREE * tree, void *item)
{
    YTNODE             *p;

    RB_ASSERT (tree != 0);

    for (p = tree->root.child[0]; p;)
    {
        int                 diff = tree->compare (item, tree->keyof (tree, p));

        if (diff < 0)
        {
            p = p->child[0];
        }
        else if (diff > 0)
        {
            p = p->child[1];
        }
        else
        {
            return p;
        }
    }

    return 0;
}

/** Red-Black Trees *********************************************************/
static void
ytInit (YTREE * tree, INT4 (*compare) (void *, void *),
        void *(*keyof) (YTREE *, YTNODE *))
{
    RB_ASSERT (compare != 0);
    RB_ASSERT (keyof != 0);

    tree->root.child[0] = 0;
    tree->root.child[1] = 0;
    tree->root.parent = 0;
    tree->root.red = 0;
    tree->compare = compare;
    tree->keyof = keyof;
    tree->count = 0;
}

static YTNODE      *
__basicrotate (YTNODE * p, int d)
{
    YTNODE             *t = p->child[!d];

    setchild (p, !d, t->child[d]);
    setchild (t, d, p);
    return t;
}

static void
rotate (YTNODE * n, int d)
{
    YTNODE             *p = n->parent, *r;
    int                 s = SLANT (n);

    r = __basicrotate (n, d);
    setchild (p, s, r);
}

static YTNODE      *
__singlerotate (YTNODE * p, int d)
{
    YTNODE             *t = __basicrotate (p, d);

    p->red = 1;
    t->red = 0;
    return t;
}

static YTNODE      *
__doublerotate (YTNODE * p, int d)
{
    setchild (p, !d, __singlerotate (p->child[!d], !d));
    return __singlerotate (p, d);
}

static void
__swap (YTNODE * a, YTNODE * b)
{
    YTNODE             *ap, *bp, *ac[2], *bc[2];
    int                 ad, bd, red;

    ap = a->parent;
    ad = (a == ap->child[1]);
    ac[0] = a->child[0];
    ac[1] = a->child[1];

    bp = b->parent;
    bd = (b == bp->child[1]);
    bc[0] = b->child[0];
    bc[1] = b->child[1];

    if (bp == a)
    {
        setchild (ap, ad, b);
        setchild (b, bd, a);
        setchild (b, !bd, ac[!bd]);

        setchild (a, 0, bc[0]);
        setchild (a, 1, bc[1]);
    }
    else if (ap == b)
    {
        setchild (bp, bd, a);
        setchild (a, ad, b);
        setchild (a, !ad, bc[!ad]);

        setchild (b, 0, ac[0]);
        setchild (b, 1, ac[1]);
    }
    else
    {
        setchild (ap, ad, b);
        setchild (bp, bd, a);

        setchild (a, 0, bc[0]);
        setchild (a, 1, bc[1]);

        setchild (b, 0, ac[0]);
        setchild (b, 1, ac[1]);
    }

    /* Swap the colours */
    red = a->red;
    a->red = b->red;
    b->red = red;
}

static void
ytInsert (YTREE * tree, YTNODE * entry)
{
    YTNODE              dummy;
    YTNODE             *g = 0, *t = &dummy, *p = 0, *q;
    int                 d = 0, last = 0;

    if ((tree == 0) || (entry == 0))
    {
        return;
    }

    if ((q = tree->root.child[0]) == 0)
    {
        /* Empty tree */
        entry->red = 0;            /* Root is always black */
        entry->child[0] = entry->child[1] = 0;
        setchild (&(tree->root), 0, entry);
        tree->count++;

        RB_ASSERT (tree->count == 1);
        return;
    }

    dummy.parent = 0;
    dummy.child[0] = 0;
    dummy.child[1] = q;
    dummy.red = 1;

    for (;;)
    {
        int                 v;

        if (q == 0)
        {
            entry->child[0] = entry->child[1] = 0;
            entry->red = 1;
            q = setchild (p, d, entry);
        }
        else if (q->child[0] && q->child[1] && q->child[0]->red
                 && q->child[1]->red)
        {
            /* Node with 2 red children -- flip colors */
            q->red = 1;
            q->child[0]->red = q->child[1]->red = 0;
        }

        RB_ASSERT (q);

        /* Fix red violation */
        if (p && q->red && p->red)
        {
            int                 e = (g == t->child[1]);

            RB_ASSERT (g);
            if (q == p->child[last])
            {
                setchild (t, e, __singlerotate (g, !last));
            }
            else
            {
                setchild (t, e, __doublerotate (g, !last));
            }
        }

        v = tree->compare (tree->keyof (tree, q), tree->keyof (tree, entry));
        if (v == 0)
        {
            break;
        }

        last = d;
        d = (v < 0);

        if (g)
        {
            t = g;
        }

        g = p;
        p = q;
        q = q->child[d];
    }

    /* Update the root */
    setchild (&(tree->root), 0, dummy.child[1]);

    /* Ensure tree root is black */
    tree->root.child[0]->red = 0;

    tree->count++;
}

static void
__adjust (YTNODE * e)
{
    for (;;)
    {
        YTNODE             *s, *p, *n[2], *c[2];
        int                 d;

        if (e->parent == 0)
        {
            /* Can't really happen -- note that this does NOT mean root -- Keep Klocworks happy */
            return;
        }

        if (IS_ROOT (e))
        {
            /* Case 1: The node is the root */
            trace ("Case 1");
            return;
        }

        p = e->parent;
        d = SLANT (e);
        s = SIBLING (e);
        c[0] = e->child[0];
        c[1] = e->child[1];

        if (IS_RED (s))
        {
            /* Case 2: The node's sibling is red */
            /* -- this falls through to the next case */
            trace ("Case 2");
            p->red = 1;
            s->red = 0;
            rotate (p, d);

            RB_ASSERT (c[0] == e->child[0]);
            RB_ASSERT (c[1] == e->child[1]);
        }

        p = e->parent;
        d = SLANT (e);
        s = SIBLING (e);
        n[0] = s->child[0];
        n[1] = s->child[1];

        if (IS_BLACK (p) && IS_BLACK_STRICT (s) && IS_BLACK (n[0])
            && IS_BLACK (n[1]))
        {
            /* Case 3: The node's parent, sibling & nephews are black */
            trace ("Case 3");
            s->red = 1;            /* FIXME */
            e = e->parent;

            continue;
        }

        if (IS_RED (p) && IS_BLACK_STRICT (s) && IS_BLACK (n[0])
            && IS_BLACK (n[1]))
        {
            /* Case 4: The node's parent is red, sibling & nephews are black */
            trace ("Case 4");
            s->red = 1;
            p->red = 0;
            return;
        }

        if (IS_BLACK_STRICT (s) && IS_RED (n[d]) && IS_BLACK (n[!d]))
        {
            /* Case 5: The node's sibling is black, near nephew is red and far nephew is black */
            /* -- this falls through to the next case */
            trace ("Case 5");
            s->red = 1;
            n[d]->red = 0;
            rotate (s, !d);
        }

        p = e->parent;
        d = SLANT (e);
        s = SIBLING (e);
        RB_ASSERT (s);
        n[0] = s->child[0];
        n[1] = s->child[1];

        /* Case 6: The node's sibling is black, far nephew is red */
        trace ("Case 6");
        RB_ASSERT (IS_BLACK (s) && IS_RED (n[!d]));

        s->red = p->red;
        p->red = 0;
        n[!d]->red = 0;
        rotate (p, d);

        return;
    }
}

static void
ytDelete (YTREE * tree, YTNODE * entry)
{
    int                 d;
    YTNODE             *p;

    if ((tree == 0) || (entry == 0))
    {
        return;
    }

    if (entry->child[0] && entry->child[1])
    {
        YTNODE             *m = entry->child[0];

        while (m->child[1])
        {
            m = m->child[1];
        }

        /* Swap the node and its predecessor */
        __swap (entry, m);
    }

    RB_ASSERT (!(entry->child[0] && entry->child[1]));

    if (!entry->red)
    {
        if (IS_RED (entry->child[0]))
        {
            trace ("Black, Left Child Red");
            entry->red = 1;
            entry->child[0]->red = 0;
        }
        else if (IS_RED (entry->child[1]))
        {
            trace ("Black, Right Child Red");
            entry->red = 1;
            entry->child[1]->red = 0;
        }
        else
        {
            trace ("Black, No Red Children, Adjusting");
            __adjust (entry);
        }
    }
    else
    {
        trace ("Red");
    }

    RB_ASSERT (!(entry->child[0] && entry->child[1]));

    p = entry->parent;
    d = (entry == p->child[1]);

    if (entry->child[0])
    {
        setchild (p, d, entry->child[0]);
    }
    else
    {
        setchild (p, d, entry->child[1]);
    }

    entry->parent = entry->child[0] = entry->child[1] = 0;
    tree->count--;
}

/****************************************************************************/
/** Adaptation to FSAP ******************************************************/
/****************************************************************************/
#define    NODEPTR(T, N)    (((T)->NodeType == RB_EMBD_NODE) ? \
                         ((void *)((unsigned char *)(N) - (T)->u4Offset)) : \
                         (((tRBNode *)(N))->key) )

static UINT4        gu4SemCnt;

UINT4
RBTreeLibInit (void)
{
    gu4SemCnt = 1;
    MEMSET (gu4RbSemArray, 0, sizeof (gu4RbSemArray));
    return (RB_SUCCESS);

}

void
RBTreeLibShut (void)
{
    gu4SemCnt = 1;
    return;
}

static void        *
__keyof (YTREE * T, YTNODE * N)
{
    void               *z;
    tRBTree             t = (tRBTree) T;

    z = NODEPTR (t, N);

    return z;
}

static              tRBTree
__create (tRBCompareFn compare, unsigned int offset, UINT1 au1SemName[])
{
    tRBTree             T;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    UINT1               u1AppSem = OSIX_TRUE;
    UINT4               u4LoopIndex = 0;
    UINT4               u4Len = 0;

    if (STRCMP (au1SemName, "") == 0)
    {
        u1AppSem = OSIX_FALSE;
    }

    if ((T = MEM_MALLOC (sizeof (struct rbtree), struct rbtree)) == 0)
    {
        return (0);
    }

    T->u4Offset = offset;

    ytInit (&(T->__tree__), compare, __keyof);

    MEMSET (au1Name, '\0', OSIX_NAME_LEN + 4);

    /* When two threads tries to get the semaphore name, there is a
     * possibility that both the threads get the same semaphore name,
     * since the semaphore name is generated based on the loop index. 
     * LOCK is taken before using the loop index, so that the the next 
     * thread doesnt use the same loopindex. */
    OsixSemTake (gu4SemNameSemId);
    if (u1AppSem == OSIX_FALSE)
    {
        gu4SemCnt++;
        for (u4LoopIndex = 0; u4LoopIndex < MAX_RBTREE_INST; u4LoopIndex++)
        {
            if (gu4RbSemArray[u4LoopIndex] == FALSE)
            {
                au1Name[0] = (UINT1) ((u4LoopIndex % U1MAX) + 1);
                au1Name[1] = (UINT1) (((u4LoopIndex / U1MAX) % U1MAX) + 1);
                au1Name[2] = 'b';
                au1Name[3] = 'r';
                break;
            }
        }
    }
    else
    {
        u4Len =
            ((STRLEN (au1SemName) <
              (sizeof (au1Name) -
               1)) ? STRLEN (au1SemName) : (sizeof (au1Name) - 1));
        STRNCPY ((INT1 *) &(au1Name), au1SemName, u4Len);
        au1Name[u4Len] = '\0';

    }

    if (OsixSemCrt (au1Name, &T->SemId) != 0)
    {
        OsixSemGive (gu4SemNameSemId);
        MEM_FREE (T);
        return (0);
    }
    if ((u1AppSem == OSIX_FALSE) && (u4LoopIndex < MAX_RBTREE_INST))
    {
        gu4RbSemArray[u4LoopIndex] = T->SemId;
    }
    OsixSemGive (gu4SemNameSemId);

    T->b1MutualExclusive = OSIX_TRUE;
    RB_SEM_GIVE (T);
    return (T);
}

static              tRBTree
__create_withoutsem (tRBCompareFn compare, unsigned int offset)
{
    tRBTree             T;

    if ((T = MEM_MALLOC (sizeof (struct rbtree), struct rbtree)) == 0)
    {
        return (0);
    }

    T->u4Offset = offset;

    ytInit (&(T->__tree__), compare, __keyof);

    T->b1MutualExclusive = OSIX_FALSE;
    return (T);
}

tRBTree
RBTreeCreate (UINT4 u4NumNodes, tRBCompareFn Cmp)
{
    tRBTree             T;
    UINT4               u4RBIndex = 0;

    if ((T = __create (Cmp, 0, (UINT1 *) "")) == 0)
    {
        return (0);
    }

    if (MemCreateMemPool
        (sizeof (tRBNode), u4NumNodes, MEM_DEFAULT_MEMORY_TYPE,
         &(T->PoolId)) != 0)
    {
        for (u4RBIndex = 0; u4RBIndex < MAX_RBTREE_INST; u4RBIndex++)
        {
            if (gu4RbSemArray[u4RBIndex] == T->SemId)
            {
                gu4RbSemArray[u4RBIndex] = FALSE;
                break;
            }
        }
        RB_SEM_DEL (T->SemId);
        MEM_FREE (T);
        return (0);
    }

    T->NodeType = RB_NOT_EMBD_NODE;
    T->next_cache = NULL;

    return (T);
}

tRBTree
RBTreeCreateEmbedded (UINT4 u4Offset, tRBCompareFn Cmp)
{
    tRBTree             T;

    if ((T = __create (Cmp, u4Offset, (UINT1 *) "")) == 0)
    {
        return (0);
    }

    T->PoolId = 0;
    T->NodeType = RB_EMBD_NODE;
    T->next_cache = NULL;

    return (T);
}

tRBTree
RBTreeCreateEmbeddedExt (UINT4 u4Offset, tRBCompareFn Cmp, UINT1 u1SemLockFlag)
{
    tRBTree             T;

    if (u1SemLockFlag == RBCREATE_SEM)
    {
        if ((T = __create (Cmp, u4Offset, (UINT1 *) "")) == 0)
        {
            return (0);
        }
    }
    else
    {
        if ((T = __create_withoutsem (Cmp, u4Offset)) == 0)
        {
            return (0);
        }
    }
    T->PoolId = 0;
    T->NodeType = RB_EMBD_NODE;
    T->next_cache = NULL;

    return (T);
}

tRBTree
RBTreeCreateEmbeddedExtended (UINT4 u4Offset, tRBCompareFn Cmp,
                              UINT1 au1SemName[])
{
    tRBTree             T;

    if ((T = __create (Cmp, u4Offset, au1SemName)) == 0)
    {
        return (0);
    }

    T->PoolId = 0;
    T->NodeType = RB_EMBD_NODE;
    T->next_cache = NULL;

    return (T);
}

VOID
RBTreeDisableMutualExclusion (tRBTree T)
{
    UINT4               u4RBIndex = 0;
    if (T == 0)
    {
        return;
    }
    if (T->b1MutualExclusive == OSIX_TRUE)
    {
        T->b1MutualExclusive = OSIX_FALSE;
        for (u4RBIndex = 0; u4RBIndex < MAX_RBTREE_INST; u4RBIndex++)
        {
            if (gu4RbSemArray[u4RBIndex] == T->SemId)
            {
                gu4RbSemArray[u4RBIndex] = FALSE;
                break;
            }
        }
        RB_SEM_DEL (T->SemId);
        gu4SemCnt--;
    }
}
UINT4
RBTreeEnableMutualExclusion (tRBTree T)
{
    UINT1               au1Name[OSIX_NAME_LEN + 4];
    if (T == 0)
    {
        return (RB_FAILURE);
    }
    if (T->b1MutualExclusive == OSIX_FALSE)
    {
        gu4SemCnt++;
        MEMSET (au1Name, '\0', OSIX_NAME_LEN + 4);
        au1Name[0] = (UINT1) ((UINT1) (gu4SemCnt >> 16) + '0');
        au1Name[1] = (UINT1) ((UINT1) (gu4SemCnt >> 8) + '0');
        au1Name[2] = (UINT1) ((UINT1) (gu4SemCnt) + '0');
        au1Name[3] = 'r';
        if (RB_SEM_CRT (au1Name, &T->SemId) != 0)
        {
            return RB_FAILURE;
        }
        RB_SEM_GIVE (T);
        T->b1MutualExclusive = OSIX_TRUE;
    }
    return RB_SUCCESS;
}
static void
__destroynode (YTNODE * N, unsigned int offset, tRBKeyFreeFn fn, UINT4 arg,
               tMemPoolId * pool)
{
    void               *z =
        pool ? (((tRBNode *) N)->key) : (void *) (((unsigned char *) N) -
                                                  offset);

    if (N->child[0])
    {
        __destroynode (N->child[0], offset, fn, arg, pool);
    }

    if (N->child[1])
    {
        __destroynode (N->child[1], offset, fn, arg, pool);
    }

    if (fn)
    {
        fn (z, arg);
    }

    if (pool)
    {
        MemReleaseMemBlock (*pool, (UINT1 *) N);
    }
}

static void
__destroy (tRBTree T, tRBKeyFreeFn fn, UINT4 arg)
{
    YTREE              *t = &(T->__tree__);

    if (t->root.child[0])
    {
        __destroynode (t->root.child[0], T->u4Offset,
                       fn, arg,
                       (T->NodeType == RB_EMBD_NODE) ? 0 : &(T->PoolId));
        t->root.child[0] = 0;
    }

    t->count = 0;
}

void
RBTreeDrain (tRBTree T, tRBKeyFreeFn FreeFn, UINT4 u4Arg)
{
    if (T == 0)
    {
        return;
    }

    __destroy (T, FreeFn, u4Arg);
    T->next_cache = NULL;
}

void
RBTreeDestroy (tRBTree T, tRBKeyFreeFn FreeFn, UINT4 u4Arg)
{
    UINT4               u4RBIndex = 0;

    RBTreeDrain (T, FreeFn, u4Arg);

    if (T->NodeType == RB_NOT_EMBD_NODE)
    {
        MemDeleteMemPool (T->PoolId);
    }

    if (T->b1MutualExclusive == OSIX_TRUE)
    {
        for (u4RBIndex = 0; u4RBIndex < MAX_RBTREE_INST; u4RBIndex++)
        {
            if (gu4RbSemArray[u4RBIndex] == T->SemId)
            {
                gu4RbSemArray[u4RBIndex] = FALSE;
                break;
            }
        }
        RB_SEM_DEL (T->SemId);
    }

    MEM_FREE (T);
}

UINT4
RBTreeCount (tRBTree T, UINT4 *pu4Count)
{
    if (T == 0)
    {
        return (RB_FAILURE);
    }

    *pu4Count = T->__tree__.count;
    return (RB_SUCCESS);
}

UINT4
RBTreeAdd (tRBTree T, tRBElem * key)
{
    UINT4               r;

    if (T == 0)
    {
        return (RB_FAILURE);
    }

    RB_SEM_TAKE (T);

    if (ytFind (&(T->__tree__), key) != 0)
    {
        r = RB_FAILURE;
    }
    else
    {
        if (T->NodeType == RB_NOT_EMBD_NODE)
        {
            tRBNode            *N;

            if ((N = (tRBNode *) (VOID *) MemAllocMemBlk (T->PoolId)) == 0)
            {
                r = RB_FAILURE;
            }
            else
            {
                N->key = key;

                ytInsert (&(T->__tree__), &(N->__node__));
                r = RB_SUCCESS;
            }
        }
        else
        {
            YTNODE             *N =
                (YTNODE *) ((VOID *) (((unsigned char *) key) + T->u4Offset));
            ytInsert (&(T->__tree__), N);

            r = RB_SUCCESS;
        }
    }

    RB_SEM_GIVE (T);

    return r;
}

tRBElem            *
RBTreeRem (struct rbtree * T, tRBElem * key)
{
    YTNODE             *N;
    void               *node;

    if (T == 0)
    {
        return (0);
    }

    RB_SEM_TAKE (T);

    if ((N = ytFind (&(T->__tree__), key)) == 0)
    {
        node = 0;
    }
    else
    {
        if (T->next_cache == (tRBNode *) N)
        {
            T->next_cache = NULL;
        }

        ytDelete (&(T->__tree__), N);

        node = NODEPTR (T, N);

        if (T->NodeType == RB_NOT_EMBD_NODE)
        {
            MemReleaseMemBlock (T->PoolId, (UINT1 *) N);
        }
    }

    RB_SEM_GIVE (T);

    return node;
}

tRBElem            *
RBTreeGet (struct rbtree * T, tRBElem * key)
{
    YTNODE             *N;
    void               *node;

    if (T == 0)
    {
        return (0);
    }

    RB_SEM_TAKE (T);

    if ((T->next_cache != NULL) && ((*(T->__tree__.compare))
                                    (key,
                                     (T->NodeType ==
                                      RB_NOT_EMBD_NODE) ? T->next_cache->
                                     key : (tRBElem *) ((UINT1 *) T->
                                                        next_cache -
                                                        (T->u4Offset))) == 0))
    {
        N = (YTNODE *) (T->next_cache);
        node = NODEPTR (T, N);
    }
    else if ((N = ytFind (&(T->__tree__), key)) == 0)
    {
        node = 0;
    }
    else
    {
        node = NODEPTR (T, N);
    }

    RB_SEM_GIVE (T);

    return node;
}

static INT4
__walk (tRBTree T, YTNODE * N, tRBWalkFn action, void *arg, UINT4 level,
        void *out)
{
    if (N == 0)
    {
        return RB_WALK_CONT;
    }

    if ((N->child[0] == 0) && (N->child[1] == 0))
    {
        if (action (NODEPTR (T, N), leaf, level, arg, out) == RB_WALK_BREAK)
        {
            return RB_WALK_BREAK;
        }
    }
    else
    {
        if (action (NODEPTR (T, N), preorder, level, arg, out) == RB_WALK_BREAK)
        {
            return RB_WALK_BREAK;
        }

        if (__walk (T, N->child[0], action, arg, level + 1, out) ==
            RB_WALK_BREAK)
        {
            return RB_WALK_BREAK;
        }

        if (action (NODEPTR (T, N), postorder, level, arg, out) ==
            RB_WALK_BREAK)
        {
            return RB_WALK_BREAK;
        }

        if (__walk (T, N->child[1], action, arg, level + 1, out) ==
            RB_WALK_BREAK)
        {
            return RB_WALK_BREAK;
        }

        if (action (NODEPTR (T, N), endorder, level, arg, out) == RB_WALK_BREAK)
        {
            return RB_WALK_BREAK;
        }
    }

    return RB_WALK_CONT;
}

VOID
RBTreeWalk (tRBTree T, tRBWalkFn action, void *arg, void *out)
{
    if (T == 0)
    {
        return;
    }

    RBSemTake (T);
    __walk (T, T->__tree__.root.child[0], action, arg, 0, out);
    RBSemGive (T);
}

tRBElem            *
RBTreeGetFirst (tRBTree T)
{
    YTNODE             *N;
    void               *node;

    if (T == 0)
    {
        return (0);
    }

    RB_SEM_TAKE (T);

    if ((N = ytFirst (&(T->__tree__), T_INORDER)) == 0)
    {
        node = 0;
    }
    else
    {
        node = NODEPTR (T, N);
    }

    if (N != 0)
    {
        T->next_cache = (tRBNode *) N;
    }

    RB_SEM_GIVE (T);

    return node;
}

tRBElem            *
RBTreeGetNext (tRBTree T, tRBElem * key, tRBCompareFn compare)
{
    int                 c;
    YTNODE             *x, *y, *z;
    void               *r;

    if (T == 0)
    {
        return 0;
    }

    RB_SEM_TAKE (T);

    y = 0;
    x = T->__tree__.root.child[0];

    compare = compare ? compare : (T->__tree__.compare);

    if (T->next_cache != NULL &&
        (*compare) (key, (T->NodeType == RB_NOT_EMBD_NODE) ?
                    T->next_cache->key : (tRBElem *) ((UINT1 *) T->next_cache -
                                                      (T->u4Offset))) == 0)
    {
        x = (YTNODE *) (T->next_cache);
    }

    if (x)
    {
        compare = compare ? compare : (T->__tree__.compare);

        while (x)
        {
            y = x;

            if ((c = compare (key, NODEPTR (T, x))) < 0)
            {
                x = x->child[0];
            }
            else if (c > 0)
            {
                x = x->child[1];
            }
            else
            {
                break;
            }
        }

        if (x)
        {
            z = ytNext (x, T_INORDER);
            r = z ? NODEPTR (T, z) : 0;
            if (z != 0)
            {
                T->next_cache = (tRBNode *) z;
            }
        }
        else
        {
            z = (compare (key, NODEPTR (T, y)) > 0) ? ytNext (y, T_INORDER) : y;
            r = z ? NODEPTR (T, z) : 0;
            if (z != 0)
            {
                T->next_cache = (tRBNode *) z;
            }
        }
    }
    else
    {
        r = 0;
    }

    RB_SEM_GIVE (T);
    return r;
}

void
RBTreeDelete (tRBTree T)
{
    RBTreeDestroy (T, 0, 0);
}

UINT4
RBTreeRemove (struct rbtree *T, tRBElem * key)
{
    YTNODE             *N;
    UINT4               r;

    if (T == 0)
    {
        return RB_FAILURE;
    }

    RB_SEM_TAKE (T);

    if (T->NodeType == RB_EMBD_NODE)
    {
        /* In this case, 'key' is guaranteed to be a pointer to a node in the tree */
        if (T->next_cache == (key + T->u4Offset))
        {
            T->next_cache = NULL;
        }
        ytDelete (&(T->__tree__),
                  (YTNODE *) ((VOID *) ((UINT1 *) key + T->u4Offset)));
        r = RB_SUCCESS;
    }
    else
    {
        if ((N = ytFind (&(T->__tree__), key)) == 0)
        {
            r = RB_FAILURE;
        }
        else
        {
            if (T->next_cache == (tRBNode *) N)
            {
                T->next_cache = NULL;
            }
            ytDelete (&(T->__tree__), N);
            MemReleaseMemBlock (T->PoolId, (UINT1 *) N);
            r = RB_SUCCESS;
        }
    }

    RB_SEM_GIVE (T);

    return r;
}

VOID
RBSemTake (tRBTree T)
{
    do
    {
        if (T->b1MutualExclusive == OSIX_TRUE)
        {
            OsixSemTake (T->SemId);
        }
    }
    while (0);
}

VOID
RBSemGive (tRBTree T)
{
    do
    {
        if (T->b1MutualExclusive == OSIX_TRUE)
        {
            OsixSemGive (T->SemId);
        }
    }
    while (0);
}

/** Testing *****************************************************************/
#ifdef TESTING
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

struct dummy
{
    tRBNodeEmbd         __header__;
    int                 value;
};
static int
compare (void *pa, void *pb)
{
    struct dummy       *a = (struct dummy *) pa;
    struct dummy       *b = (struct dummy *) pb;

    if (a->value == b->value)
    {
        return 0;
    }
    else if (a->value > b->value)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

int
main (int C, char **V)
{
    int                 i, k = 0, K, ntest = 100, audit = 1, walk = 0, embed =
        0;
    tRBTree             T;
    struct dummy       *node = 0, *N;

    for (;;)
    {
        int                 o;

        if ((o = getopt (C, V, "a:nws:e")) == (-1))
        {
            break;
        }

        switch (o)
        {
            case 'n':
                audit = 0;
                break;
            case 'a':
                audit = atoi (optarg);
                break;
            case 'w':
                walk = 1;
                break;
            case 'e':
                embed = 1;
                break;
            case 's':
                srand (atoi (optarg));
                break;
            default:
                break;
        }
    }

    C -= (optind - 1);
    V += (optind - 1);

    if (C > 1)
    {
        ntest = atoi (V[1]);
    }

    fprintf (stderr, "Testing for %d entries\n", ntest);

    if ((node = (struct dummy *) malloc (ntest * sizeof (struct dummy))) == 0)
    {
        fprintf (stderr, "Not enough memory\n");
        return 1;
    }

    RBTreeLibInit ();

    T = embed ? RBTreeCreateEmbedded (0, compare) : RBTreeCreate (10000,
                                                                  compare);

    fprintf (stderr, "Inserting into %s tree ... ",
             embed ? "embedded" : "non-embedded");
    for (i = 0; i < ntest; i++)
    {
        int                 n;

        for (;;)
        {
            n = rand () % (2 * ntest);

            node[i].value = n;

            if (RBTreeGet (T, &node[i]) == 0)
            {
                break;
            }
        }

        RBTreeAdd (T, &node[i]);
        k++;

        RBTreeCount (T, &K) RB_ASSERT (K == k);
    }
    fprintf (stderr, "[done]\n");

    if (walk)
    {
        fprintf (stderr, "Stepping through tree ... ");
        for (N = RBTreeGetFirst (T); N; N = RBTreeGetNext (T, N, 0))
        {
            fprintf (stderr, "%d ", N->value);
        }
        fprintf (stderr, "[done]\n");
    }

    fprintf (stderr, "Deleting from tree ... ");
    for (i = 0; i < ntest; i++)
    {
        RBTreeRem (T, (&node[i]));
        k--;
        RBTreeCount (T, &K) RB_ASSERT (K == k);
    }
    fprintf (stderr, "[done]\n");

    RBTreeDelete (T);
    RBTreeLibShut ();

    free (node);
    return 0;
}
#endif
