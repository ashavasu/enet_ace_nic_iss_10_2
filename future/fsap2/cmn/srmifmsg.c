/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmifmsg.c,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: The SRM IfMsg Module.
 *
 */

#include "ifmsginc.h"

/***************************************************************/
/*  Function Name   : IfMsgInit                                */
/*  Description     : Initializes the IfMsg library.           */
/*                    Creates a memory pool with the required  */
/*                    number of blocks.                        */
/*  Input(s)        : pIfMsgCfg - Pointer to the IfMsg library */
/*                    configuration structure.                 */
/*  Output(s)       : None                                     */
/*  Returns         : IFMSG_SUCCESS                            */
/*                    IFMSG_ERR_INV_CFG                        */
/*                    IFMSG_ERR_MEM_RSC                        */
/*                    IFMSG_FAILURE                            */
/***************************************************************/
#ifdef __STDC__
INT4
IfMsgInit (tIfMsgCfg * pIfMsgCfg)
#else
INT4
IfMsgInit (pIfMsgCfg)
     tIfMsgCfg          *pIfMsgCfg;
#endif
{
    tIfMsgMemPoolId     PoolId;
    UINT4               u4Count;

    if (!pIfMsgCfg)
    {
        IFMSG_DBG (IFMSG_DBG_MAJOR, "Invalid Configuration Parameter.\n");
        return IFMSG_ERR_INV_CFG;
    }

    u4Count = pIfMsgCfg->u4NumMsgs;
    if (u4Count == 0)
    {
        IFMSG_DBG (IFMSG_DBG_MAJOR,
                   "Invalid Configuration Parameter: No. of Blocks.\n");
        return IFMSG_ERR_INV_CFG;
    }

    gIfMsgGlbRec.IfMsgPoolId = (tIfMsgMemPoolId) IFMSG_INVALID_POOL_ID;
#if (DEBUG_IFMSG == FSAP_ON)
    gIfMsgGlbRec.u4DbgLevel = IFMSG_DBG_MAJOR;
    gIfMsgGlbRec.u4TotalCount = 0;
    gIfMsgGlbRec.u4TotalAllocs = 0;
    gIfMsgGlbRec.u4TotalRels = 0;
#endif

    /* Create the Memory Pool for the IfMsg structures */
    if (IFMSG_CREATE_MEM_POOL (sizeof (tIfMsg),
                               u4Count,
                               pIfMsgCfg->u4MemType,
                               &PoolId) != IFMSG_MEM_SUCCESS)
    {
        return IFMSG_ERR_MEM_RSC;
    }

    gIfMsgGlbRec.u4Initialized = IFMSG_STATUS_INITIALIZED;
    gIfMsgGlbRec.IfMsgPoolId = PoolId;
#if (DEBUG_IFMSG == FSAP_ON)
    gIfMsgGlbRec.u4TotalCount = u4Count;
#endif

    return IFMSG_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IfMsgAllocateIfMsg                       */
/*  Description     : Allocates an IfMsg Block                 */
/*  Input(s)        : ppIfMsg - Address of pointer to IfMsg    */
/*                    structure                                */
/*  Output(s)       : ppIfMsg - pointer to the allocated IfMsg */
/*                    structure                                */
/*  Returns         : IFMSG_SUCCESS or IFMSG_FAILURE           */
/***************************************************************/
#ifdef __STDC__
INT4
IfMsgAllocateIfMsg (tIfMsg ** ppIfMsg)
#else
INT4
IfMsgAllocateIfMsg (ppIfMsg)
     tIfMsg            **ppIfMsg;
#endif
{
#if (DEBUG_IFMSG == FSAP_ON)
    if (gIfMsgGlbRec.u4Initialized != IFMSG_STATUS_INITIALIZED)
    {
        IFMSG_DBG (IFMSG_DBG_MAJOR, "Library Not Initialized.\n");
        return IFMSG_ERR_NO_INIT;
    }
#endif

    if (IFMSG_ALLOCATE_MEM_BLOCK (gIfMsgGlbRec.IfMsgPoolId,
                                  ppIfMsg) != IFMSG_MEM_SUCCESS)
    {
        return IFMSG_FAILURE;
    }

    IFMSG_MEMSET (*ppIfMsg, 0, sizeof (tIfMsg));

#if (DEBUG_IFMSG == FSAP_ON)
    gIfMsgGlbRec.u4TotalAllocs++;
#endif
    return IFMSG_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IfMsgReleaseIfMsg                        */
/*  Description     : Releases the IfMsg Block back to IfMsg   */
/*                    Pool                                     */
/*  Input(s)        : pIfMsg - Pointer to IfMsg structure.     */
/*  Output(s)       : None.                                    */
/*  Returns         : IFMSG_SUCCESS or IFMSG_FAILURE           */
/***************************************************************/
#ifdef __STDC__
INT4
IfMsgReleaseIfMsg (tIfMsg * pIfMsg)
#else
INT4
IfMsgReleaseIfMsg (pIfMsg)
     tIfMsg             *pIfMsg;
#endif
{
#if (DEBUG_IFMSG == FSAP_ON)
    if (gIfMsgGlbRec.u4Initialized != IFMSG_STATUS_INITIALIZED)
    {
        IFMSG_DBG (IFMSG_DBG_MAJOR, "Library Not Initialized.\n");
        return IFMSG_ERR_NO_INIT;
    }
#endif

    if (IFMSG_RELEASE_MEM_BLOCK (gIfMsgGlbRec.IfMsgPoolId,
                                 pIfMsg) != IFMSG_MEM_SUCCESS)
    {
        return IFMSG_FAILURE;
    }

#if (DEBUG_IFMSG == FSAP_ON)
    gIfMsgGlbRec.u4TotalRels++;
#endif
    return IFMSG_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IfMsgShutDown                            */
/*  Description     : Shuts down the IfMsg library.            */
/*                    Deletes the Memory Pool and releases the */
/*                    Local Data Structures.                   */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None.                                    */
/*  Returns         : IFMSG_SUCCESS or IFMSG_FAILURE           */
/***************************************************************/
#ifdef __STDC__
INT4
IfMsgShutDown (VOID)
#else
INT4
IfMsgShutDown ()
#endif
{
    if (gIfMsgGlbRec.u4Initialized != IFMSG_STATUS_INITIALIZED)
    {
        IFMSG_DBG (IFMSG_DBG_MAJOR, "Library Not Initialized.\n");
        return IFMSG_ERR_NO_INIT;
    }

    if (IFMSG_DELETE_MEM_POOL (gIfMsgGlbRec.IfMsgPoolId) != IFMSG_MEM_SUCCESS)
    {
        return IFMSG_FAILURE;
    }

    gIfMsgGlbRec.u4Initialized = IFMSG_STATUS_UNINITIALIZED;
    return IFMSG_SUCCESS;
}

#if (DEBUG_IFMSG == FSAP_ON)

/***************************************************************/
/*  Function Name   : IfMsgSetDbg                              */
/*  Description     : Sets the debug level for the IfMsg       */
/*                    Library.                                 */
/*  Input(s)        : u4NewLevel - The Debug level to be set.  */
/*  Output(s)       : None.                                    */
/*  Returns         : None.                                    */
/***************************************************************/
#ifdef __STDC__
VOID
IfMsgSetDbg (UINT4 u4NewLevel)
#else
VOID
IfMsgSetDbg (u4NewLevel)
     UINT4               u4NewLevel;
#endif
{
    gIfMsgGlbRec.u4DbgLevel = u4NewLevel;
    return;
}

/***************************************************************/
/*  Function Name   : IfMsgGetDbg                              */
/*  Description     : Gets the debug level for the IfMsg       */
/*                    Library.                                 */
/*  Input(s)        : None.                                    */
/*  Output(s)       : The current debug level                  */
/*  Returns         : None.                                    */
/***************************************************************/
#ifdef __STDC__
UINT4
IfMsgGetDbg (VOID)
#else
UINT4
IfMsgGetDbg ()
#endif
{
    return (gIfMsgGlbRec.u4DbgLevel);
}

/***************************************************************/
/*  Function Name   : IfMsgPrintDbgInfo                        */
/*  Description     : Prints the IfMsg library statistics      */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None.                                    */
/*  Returns         : None.                                    */
/***************************************************************/
#ifdef __STDC__
VOID
IfMsgPrintDbgInfo (VOID)
#else
VOID
IfMsgPrintDbgInfo ()
#endif
{
    IFMSG_PRINT ("------ IfMsg library: Statistics ----- BEGIN\n");

    IFMSG_PRINT1 ("Total No. of Blks                          %d.\n",
                  gIfMsgGlbRec.u4TotalCount);
    IFMSG_PRINT1 ("No. of Blks Allocated Since Init           %d.\n",
                  gIfMsgGlbRec.u4TotalAllocs);
    IFMSG_PRINT1 ("No. of Blks Released Since Init            %d.\n",
                  gIfMsgGlbRec.u4TotalRels);

    IFMSG_PRINT ("------ IfMsg library: Statistics ----- END\n");
    return;
}

#endif
