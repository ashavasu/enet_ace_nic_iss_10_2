/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmsginc.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Common include file for SRM IfMsg module.
 *
 */
/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                   */
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved          */
/*                                                          */
/*  FILE NAME             : ifmsginc.h                      */
/*  PRINCIPAL AUTHOR      :                                 */
/*  SUBSYSTEM NAME        :                                 */
/*  MODULE NAME           :                                 */
/*  LANGUAGE              :                                 */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  DESCRIPTION           :                                 */
/************************************************************/
/*                                                          */
/*  CHANGE RECORD:                                          */
/*  Version      Author  Date    Description of change      */
/*   1.0         Satish  Nov1999     Create Original        */
/************************************************************/

#ifndef _IFMSGINC_H
#define _IFMSGINC_H

#include "osxinc.h" 
#include "osxstd.h"
#include "srmmem.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utltrc.h"
#include "ifmsglib.h"
#include "srmifmsg.h"

#endif /* _IFMSGINC_H */
