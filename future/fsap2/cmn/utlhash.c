/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlhash.c,v 1.17 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL Hash module.
 *
 */

#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "srmmem.h"
#include "utlsll.h"
#include "utlhash.h"
#include "utlmacro.h"
#include "utlport.h"

/*****    Internal Macros Definition     *****/

#define TMO_HASH_MALLOC        MEM_MALLOC
#define TMO_HASH_FREE          MEM_FREE

/*****      Global Variable Definition     *****/
static UINT4        u4HashTabCount = 0;
static tTMO_HASH_TABLE *TMO_HASH_Tbl_Crt (UINT4 u4HashSize,
                                          UINT4 (*pInsertFunc)
                                          (tTMO_HASH_NODE *, UINT1 *),
                                          UINT1 u1MutexLockFlag);

/***************************************************************/
/*  Function Name   : TMO_HASH_Create_Table                    */
/*  Description     : This procedure creates a hash table of   */
/*                    size 'u4HashSize' and returns a pointer  */
/*                    to the hash table.  If more than one node*/
/*                    hashes on to the same value, the order in*/
/*                    which the elements are to be placed is   */
/*                    determined by the insertion function.    */
/*                    If it is NULL, the Node is added to the  */
/*                    end of the list. The template for        */
/*                    pInsertFunc Is.                          */
/*                                                             */
/*                    UINT1 insert_fn ARG_LIST((tTMO_HASH_NODE */
/*                         *pNode, UINT1 *pInsertFunc_param)); */
/*                    pInsertFunc_param is the parameter to be */
/*                    passed to the insert_fn when it is       */
/*                    invoked by TMO_HASH_append_Node. This    */
/*                    procedure is called for each Node in the */
/*                    list corresponding to the hash value. If */
/*                    the current Node is to be inserted before*/
/*                    this pNode,insert_fn should return TRUE. */
/*                    Otherwise FALSE.                         */
/*  Input(s)        : u4HashSize - Hash Table Size             */
/*                    pInsertFunc - Pointer to the insertion   */
/*                                    function                 */
/*                    u1MutexLockFlag - Flag indicating whether*/
/*                         to enable Mutual Exclustion or not  */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the Hash Table                */
/***************************************************************/
tTMO_HASH_TABLE    *
TMO_HASH_Create_Table (UINT4 u4HashSize,
                       UINT4 (*pInsertFunc) (tTMO_HASH_NODE *, UINT1 *),
                       UINT1 u1MutexLockFlag)
{
    tTMO_HASH_TABLE    *pHashTab;
    UINT4               u4HashIndex;

    pHashTab = TMO_HASH_Tbl_Crt (u4HashSize, pInsertFunc, u1MutexLockFlag);
    if (pHashTab)
    {
        for (u4HashIndex = 0; u4HashIndex < u4HashSize; u4HashIndex++)
        {
            TMO_HASH_BUCKET_INIT (&pHashTab->HashList[u4HashIndex]);
        }
    }
    return (pHashTab);
}

/***************************************************************/
/*  Function Name   : UTL_HASH_Create_Table                    */
/*  Description     : Offset aware version of the above fn.    */
/*                                                             */
/*  Input(s)        : u4HashSize - Hash Table Size             */
/*                    pInsertFunc - Pointer to the insertion   */
/*                                    function                 */
/*                    u1MutexLockFlag - Flag indicating whether*/
/*                         to enable Mutual Exclustion or not  */
/*                    u4Offset - Byte offset of HASH_NODE      */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the Hash Table                */
/***************************************************************/
tTMO_HASH_TABLE    *
UTL_HASH_Create_Table (UINT4 u4HashSize,
                       UINT4 (*pInsertFunc) (tTMO_HASH_NODE *, UINT1 *),
                       UINT1 u1MutexLockFlag, UINT4 u4Offset)
{
    tTMO_HASH_TABLE    *pHashTab;
    UINT4               u4HashIndex;

    pHashTab = TMO_HASH_Tbl_Crt (u4HashSize, pInsertFunc, u1MutexLockFlag);
    if (pHashTab != NULL)
    {
        for (u4HashIndex = 0; u4HashIndex < u4HashSize; u4HashIndex++)
        {
            UTL_HASH_BUCKET_INIT (&pHashTab->HashList[u4HashIndex], u4Offset);
        }
    }
    return (pHashTab);
}

/***************************************************************/
/*  Function Name   : TMO_HASH_Tbl_Crt                         */
/*  Description     : Common code used by above two functions. */
/*                                                             */
/*  Input(s)        : u4HashSize - Hash Table Size             */
/*                    pInsertFunc - Pointer to the insertion   */
/*                                    function                 */
/*                    u1MutexLockFlag - Flag indicating whether*/
/*                         to enable Mutual Exclustion or not  */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the Hash Table                */
/***************************************************************/
static tTMO_HASH_TABLE *
TMO_HASH_Tbl_Crt (UINT4 u4HashSize,
                  UINT4 (*pInsertFunc) (tTMO_HASH_NODE *, UINT1 *),
                  UINT1 u1MutexLockFlag)
{
    tTMO_HASH_TABLE    *pHashTab;
    UINT4               u4HashMemSize;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];

    u4HashMemSize =
        sizeof (tTMO_HASH_TABLE) + (u4HashSize - 1) * sizeof (tTMO_HASH_BUCKET);
    if ((pHashTab = TMO_HASH_MALLOC (u4HashMemSize, tTMO_HASH_TABLE)) != 0)
    {
        pHashTab->u4_HashSize = u4HashSize;
        pHashTab->pInsertFunc = pInsertFunc;
        pHashTab->u1_MutexLockFlag = u1MutexLockFlag;

        if (pHashTab->u1_MutexLockFlag == TRUE)
        {
            /* Create the Semaphore for Mutual Exclusion
             * Sema4 names are four characters of the format
             * Ha{char}{char} where char runs from 0-255 
             */
            au1SemName[0] = 'H';
            au1SemName[1] = 'a';
            au1SemName[2] = (UINT1) ((UINT1) (u4HashTabCount >> 8) + '0');
            au1SemName[3] = (UINT1) ((UINT1) (u4HashTabCount) + '0');
            u4HashTabCount++;

            if (OsixSemCrt (au1SemName, &(pHashTab->SemId)) != OSIX_SUCCESS)
            {
                UtlTrcLog (1, 1, "HASH", "\n HashTable Creation : failed ");
                TMO_HASH_FREE (pHashTab);
                return (NULL);
            }
            OsixSemGive (pHashTab->SemId);
        }
    }
    return (pHashTab);
}

/***************************************************************/
/*  Function Name   : TMO_HASH_Add_Node                        */
/*  Description     : This function puts the 'pNode' in hash   */
/*                    table in the hash value specified by     */
/*                    'u4HashIndex'.  It invokes the function  */
/*                    pointed by 'pInsertFunc' with the        */
/*                    specified 'pu1InsertFuncParam' to find   */
/*                    out where the element is to be inserted  */
/*                    in the list                              */
/*  Input(s)        : pHashTab - Pointer to the Hash Table in  */
/*                       which a the node needs to be inserted */
/*                    pNode - Pointer to the node which has to */
/*                              be added                       */
/*                    u4HashIndex - Value of the Hash Index    */
/*                    pu1InsertFuncParam - Parameter which will*/
/*                         be passed to the Insert Routine     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
TMO_HASH_Add_Node (tTMO_HASH_TABLE * pHashTab, tTMO_HASH_NODE * pNode,
                   UINT4 u4HashIndex, UINT1 *pu1InsertFuncParam)
{
    UINT1               u1Found = FALSE;
    tTMO_HASH_NODE     *pTmpNodePtr;
    tTMO_HASH_NODE     *pNextNodePtr;
    tTMO_HASH_NODE     *pPrevNodePtr = NULL;
    INT4                i4RetValue;
    INT4                i4InsertPos = MATCH_NOT_FOUND;

    if (pHashTab->pInsertFunc == NULL)
    {
        if (pHashTab->u1_MutexLockFlag == TRUE)
        {
            i4RetValue = OsixSemTake (pHashTab->SemId);
            if (i4RetValue != OSIX_SUCCESS)
            {
                UtlTrcLog (1, 1, "HASH", "\n SemTake returns error %d",
                           i4RetValue);
                return;
            }
        }
        TMO_HASH_ADD_NODE (&pHashTab->HashList[u4HashIndex], pNode);
        if (pHashTab->u1_MutexLockFlag == TRUE)
        {
            OsixSemGive (pHashTab->SemId);
        }

    }
    else
    {
        UTL_HASH_Scan_Bucket (pHashTab, u4HashIndex, pTmpNodePtr, pNextNodePtr,
                              tTMO_HASH_NODE *)
        {

            i4InsertPos =
                (*pHashTab->pInsertFunc) (pTmpNodePtr, pu1InsertFuncParam);
            if ((i4InsertPos == INSERT_NEXTTO)
                || (i4InsertPos == INSERT_PRIORTO))
            {
                u1Found = TRUE;
                break;
            }
            pPrevNodePtr = pTmpNodePtr;
        }

        if (u1Found == FALSE)
            pTmpNodePtr = TMO_HASH_LAST_NODE (&pHashTab->HashList[u4HashIndex]);
        if (pHashTab->u1_MutexLockFlag == TRUE)
        {
            i4RetValue = OsixSemTake (pHashTab->SemId);
            if (i4RetValue != OSIX_SUCCESS)
            {
                UtlTrcLog (1, 1, "HASH", "\n SemTake returns error %d",
                           i4RetValue);
                return;
            }
        }
        /* The new has node needs to be inserted prior to the current node */
        if (i4InsertPos == INSERT_PRIORTO)
        {
            if (pPrevNodePtr)
                TMO_HASH_INSERT_NODE (&pHashTab->HashList[u4HashIndex],
                                      pPrevNodePtr, pNode);
            else                /* Needs to be added as the root/head node */
                TMO_HASH_INSERT_NODE (&pHashTab->HashList[u4HashIndex], NULL,
                                      pNode);
        }
        else                    /* Insert next to the current node */
            TMO_HASH_INSERT_NODE (&pHashTab->HashList[u4HashIndex], pTmpNodePtr,
                                  pNode);

        if (pHashTab->u1_MutexLockFlag == TRUE)
        {
            OsixSemGive (pHashTab->SemId);
        }
    }
}

/***************************************************************/
/*  Function Name   : TMO_HASH_Delete_Node                     */
/*  Description     : This procedure deletes the specified node*/
/*                    'pNode' from the hash table 'pHashTab'   */
/*  Input(s)        : pHashTab - Pointer to the Hash Table     */
/*                    pNode    - Pointer to the node which has */
/*                               to be deleted                 */
/*                    u4HashIndex -  Value of the Hash Index   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
TMO_HASH_Delete_Node (tTMO_HASH_TABLE * pHashTab, tTMO_HASH_NODE * pNode,
                      UINT4 u4HashIndex)
{
    INT4                i4RetValue;

    if (pHashTab->u1_MutexLockFlag == TRUE)
    {
        i4RetValue = OsixSemTake (pHashTab->SemId);
        if (i4RetValue != OSIX_SUCCESS)
        {
            UtlTrcLog (1, 1, "HASH", "\n SemTake returns error %d", i4RetValue);
            return;
        }
    }
    TMO_HASH_DELETE_NODE (&pHashTab->HashList[u4HashIndex], pNode);
    if (pHashTab->u1_MutexLockFlag == TRUE)
    {
        OsixSemGive (pHashTab->SemId);
    }
}

/***************************************************************/
/*  Function Name   : TMO_HASH_Replace_Node                    */
/*  Description     : This procedure replaces the node pointed */
/*                    to by 'pOldNode' by the node pointed to  */
/*                    by 'pNewNode'                            */
/*  Input(s)        : pHashTab    - Pointer to the Hash Table  */
/*                    pOldNode    - Pointer to the Old Node    */
/*                    u4HashIndex - Value of the Hash Index    */
/*                    pNewNode    - Pointer to the New Node    */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
TMO_HASH_Replace_Node (tTMO_HASH_TABLE * pHashTab, tTMO_HASH_NODE * pOldNode,
                       UINT4 u4HashIndex, tTMO_HASH_NODE * pNewNode)
{

    INT4                i4RetValue;
    if (pHashTab->u1_MutexLockFlag == TRUE)
    {
        i4RetValue = OsixSemTake (pHashTab->SemId);
        if (i4RetValue != OSIX_SUCCESS)
        {
            UtlTrcLog (1, 1, "HASH", "\n SemTake returns error %d", i4RetValue);
            return;
        }
    }
    TMO_HASH_REPLACE_NODE (&pHashTab->HashList[u4HashIndex], pOldNode,
                           pNewNode);
    if (pHashTab->u1_MutexLockFlag == TRUE)
    {
        i4RetValue = OsixSemTake (pHashTab->SemId);
        if (i4RetValue != OSIX_SUCCESS)
        {
            UtlTrcLog (1, 1, "HASH", "\n SemGive returns error %d", i4RetValue);
            return;
        }
    }
}

/***************************************************************/
/*  Function Name   : TMO_HASH_Delete_Table                    */
/*  Description     : This procedure deletes the hash table    */
/*                    'pHashTab'. For each node in the hash    */
/*                    table, it calls 'pMemFreeFun' because the*/
/*                    hash function has not allocated the space*/
/*                    for those nodes. If this pointer is null,*/
/*                    then it calls 'free' system call to      */
/*                    release the space                        */
/*  Input(s)        : pHashTab    - Pointer to the Hash Table  */
/*                    pMemFreeFun - Pointer to the Memory Free */
/*                                  function                   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
TMO_HASH_Delete_Table (tTMO_HASH_TABLE * pHashTab,
                       VOID (*pMemFreeFunc) (tTMO_HASH_NODE *))
{
    UINT4               u4_HashIndex;
    tTMO_HASH_NODE     *pTempNodePtr2, *pTmpNodePtr;

    if (pHashTab->u1_MutexLockFlag == TRUE)
    {
        OsixSemDel (pHashTab->SemId);
    }
    TMO_HASH_Scan_Table (pHashTab, u4_HashIndex)
    {
        for (pTmpNodePtr =
             TMO_HASH_FIRST_NODE (&pHashTab->HashList[u4_HashIndex]);
             pTmpNodePtr; pTmpNodePtr = pTempNodePtr2)
        {
            pTempNodePtr2 =
                TMO_HASH_NEXT_NODE (&pHashTab->HashList[u4_HashIndex],
                                    pTmpNodePtr);
            if (!pMemFreeFunc)
                TMO_HASH_FREE (pTmpNodePtr);
            else
                (*pMemFreeFunc) (pTmpNodePtr);
        }
    }
    TMO_HASH_FREE (pHashTab);
}

/* MSAD ADD -S- */
/***************************************************************/
/*  Function Name   : TMO_HASH_Clean_Table                     */
/*  Description     : This procedure Cleans the hash table     */
/*                    'pHashTab'. For each node in the hash    */
/*                    table, it calls parse the complete SLL   */
/*                    and release all the blocks taken earlier */
/*                    to the pool                              */
/*  Input(s)        : pHashTab    - Pointer to the Hash Table  */
/*                    pMemFreeFun - Pointer to the Memory Free */
/*                                  function                   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
TMO_HASH_Clean_Table (tTMO_HASH_TABLE * pHashTab)
{
    UINT4               u4_HashIndex;
    tTMO_HASH_NODE     *pTempNodePtr2, *pTmpNodePtr;

    if (pHashTab == NULL)
        return;

    if (pHashTab->u1_MutexLockFlag == TRUE)
    {
    }
    TMO_HASH_Scan_Table (pHashTab, u4_HashIndex)
    {
        for (pTmpNodePtr =
             TMO_HASH_FIRST_NODE (&pHashTab->HashList[u4_HashIndex]);
             pTmpNodePtr; pTmpNodePtr = pTempNodePtr2)
        {
            pTempNodePtr2 =
                TMO_HASH_NEXT_NODE (&pHashTab->HashList[u4_HashIndex],
                                    pTmpNodePtr);

            TMO_HASH_Delete_Node (pHashTab, pTmpNodePtr, u4_HashIndex);
        }
    }
}

/* MSAD ADD -E- */
