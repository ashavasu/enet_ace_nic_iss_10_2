/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utldll.c,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL Dll module.
 *
 */

#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "srmmem.h"
#include "utldll.h"

#define TMO_DLL_FREE(pNodeAddr)   MEM_FREE(pNodeAddr)

/***************************************************************/
/*  Function Name   : TMO_DLL_Insert_In_Middle                 */
/*  Description     : Inserts the node 'pMid' in between       */
/*                    'pPrev' and 'pNext' and updates the node */
/*                    count of the 'pList                      */
/*  Input(s)        : pList - Pointer to the Doubly Linked List*/
/*                    pPrev - Pointer to previous Node         */
/*                    pMid  - Pointer to Node which needs to   */
/*                             be added                        */
/*                    pNext -  Pointer to the Next Node        */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_Insert_In_Middle (tTMO_DLL * pList, tTMO_DLL_NODE * pPrev,
                          tTMO_DLL_NODE * pMid, tTMO_DLL_NODE * pNext)
#else
VOID
TMO_DLL_Insert_In_Middle (pList, pPrev, pMid, pNext)
     tTMO_DLL           *pList;
     tTMO_DLL_NODE      *pPrev;
     tTMO_DLL_NODE      *pMid;
     tTMO_DLL_NODE      *pNext;
#endif /* __STDC__ */
{
    pList->u4_Count++;
    pMid->pNext = pNext;
    pMid->pPrev = pPrev;
    pPrev->pNext = pMid;
    pNext->pPrev = pMid;
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Replace                          */
/*  Description     : Replaces the old Node 'pOld' with the    */
/*                    new node 'pNew' and also initialise the  */
/*                    old node                                 */
/*  Input(s)        : pList - Pointer to the Doubly Linked List*/
/*                    pOld  - Pointer to Old Node              */
/*                    pNew  - Pointer to New Node              */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_Replace (tTMO_DLL * pList, tTMO_DLL_NODE * pOld, tTMO_DLL_NODE * pNew)
#else
VOID
TMO_DLL_Replace (pList, pOld, pNew)
     tTMO_DLL           *pList;
     tTMO_DLL_NODE      *pOld;
     tTMO_DLL_NODE      *pNew;
#endif /* __STDC__ */
{
    tTMO_DLL_NODE      *pPrev = pOld->pPrev;
    tTMO_DLL_NODE      *pNext = pOld->pNext;

    /* Unused Param. */
    pList = pList;

    pNew->pNext = pNext;
    pNew->pPrev = pPrev;
    pPrev->pNext = pNew;
    pNext->pPrev = pNew;
    TMO_DLL_Init_Node (pOld);
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Delete_In_Middle                 */
/*  Description     : Removes the 'pNode' from the 'pList' and */
/*                    updates the pointers appropriately using */
/*                    'pPrev' and 'pNext'. After removing from */
/*                    the list, 'pNode' is initialised and     */
/*                    count of nodes is updated in 'pList'     */
/*  Input(s)        : pList - Pointer to the Linked List       */
/*                    pPrev - Pointer to Previous Node         */
/*                    pNode - Pointer to Node which is to be   */
/*                            removed                          */
/*                    pNext - Pointer to Next Node             */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_Delete_In_Middle (tTMO_DLL * pList, tTMO_DLL_NODE * pPrev,
                          tTMO_DLL_NODE * pNode, tTMO_DLL_NODE * pNext)
#else
VOID
TMO_DLL_Delete_In_Middle (pList, pPrev, pNode, pNext)
     tTMO_DLL           *pList;
     tTMO_DLL_NODE      *pPrev;
     tTMO_DLL_NODE      *pNode;
     tTMO_DLL_NODE      *pNext;
#endif /* __STDC__ */
{
    pPrev->pNext = pNext;
    pNext->pPrev = pPrev;
    TMO_DLL_Init_Node (pNode);
    (pList)->u4_Count--;
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Insert                           */
/*  Description     : Inserts the node 'pNode' as a next node  */
/*                    to the 'pPrev' node and updates the node */
/*                    count of the 'pList'                     */
/*  Input(s)        : pList - Pointer to the Linked List       */
/*                    pPrev - Pointer to the node next to which*/
/*                            the new node needs to be inserted*/
/*                    pNode - Pointer of the node which needs  */
/*                            to be inserted                   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_Insert (tTMO_DLL * pList, tTMO_DLL_NODE * pPrev, tTMO_DLL_NODE * pNode)
#else
VOID
TMO_DLL_Insert (pList, pPrev, pNode)
     tTMO_DLL           *pList;
     tTMO_DLL_NODE      *pPrev;
     tTMO_DLL_NODE      *pNode;
#endif /* __STDC__ */
{
    if (pPrev == NULL)
        pPrev = &pList->Head;
    TMO_DLL_Insert_In_Middle (pList, pPrev, pNode, pPrev->pNext);
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Concat                           */
/*  Description     : Concatenates 'pAddList' at the end of    */
/*                    'pDstList' and updates the node count    */
/*                    parameter in 'pDstList' appropriately.   */
/*  Input(s)        : pDstlist - Pointer to the Linked List    */
/*                    pAddList - Pointer to Linked List which  */
/*                        needs to be appended to 'pDstList'   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_Concat (tTMO_DLL * pDstList, tTMO_DLL * pAddList)
#else
VOID
TMO_DLL_Concat (pDstList, pAddList)
     tTMO_DLL           *pDstList;
     tTMO_DLL           *pAddList;
#endif /* __STDC__ */
{
    if (TMO_DLL_Count (pAddList) == 0)
        return;

 /**** make the last node of pDstList to point to first node os addlist */
    pDstList->Head.pPrev->pNext = pAddList->Head.pNext;

 /**** make the first node of pAddList to point to last node os dstlist */
    pAddList->Head.pNext->pPrev = pDstList->Head.pPrev;

 /**** make the dstlist Head prev to point to the last node in addlist */
    pDstList->Head.pPrev = pAddList->Head.pPrev;

 /**** make the addlist last node to point to dstlist ******/
    pAddList->Head.pPrev->pNext = &pDstList->Head;
    pDstList->u4_Count += pAddList->u4_Count;
    TMO_DLL_Init (pAddList);
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Extract                          */
/*  Description     : From 'pSrcList' extracts the nodes       */
/*                    starting from 'pStartNode' to 'pEndNode' */
/*                    and the destination list 'pDstList'  is  */
/*                    formed.  Node count in 'pDstList' and    */
/*                    'pSrcList' are updated properly          */
/*  Input(s)        : pSrcList - Pointer to Source Linked List */
/*                    pstartNode - Pointer to the Starting Node*/
/*                       from which list needs to be extracted */
/*                    pEndNode - Pointer to the End Node upto  */
/*                        which the list needs to be extracted */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_Extract (tTMO_DLL * pSrcList, tTMO_DLL_NODE * pStartNode,
                 tTMO_DLL_NODE * pEndNode, tTMO_DLL * pDstList)
#else
VOID
TMO_DLL_Extract (pSrcList, pStartNode, pEndNode, pDstList)
     tTMO_DLL           *pSrcList;
     tTMO_DLL_NODE      *pStartNode;
     tTMO_DLL_NODE      *pEndNode;
     tTMO_DLL           *pDstList;
#endif /* __STDC__ */
{
    UINT4               i;
    tTMO_DLL_NODE      *pTempNode;

    pStartNode->pPrev->pNext = pEndNode->pNext;
    pEndNode->pNext->pPrev = pStartNode->pPrev;
    pDstList->Head.pNext = pStartNode;
    pStartNode->pPrev = &pDstList->Head;
    pDstList->Head.pPrev = pEndNode;
    pEndNode->pNext = &pDstList->Head;
    for (pTempNode = pDstList->Head.pNext, i = 0; pTempNode != &pDstList->Head;
         pTempNode = pTempNode->pNext, i++);
    pSrcList->u4_Count -= i;
    pDstList->u4_Count = i;
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Get                              */
/*  Description     : Removes the first node from 'pList' and  */
/*                    returns the same.                        */
/*  Input(s)        : pList - Pointer to the Linked List       */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the first Node in the list,   */
/*                       if the list is not empty              */
/*                    NULL, otherwise                          */
/***************************************************************/
#ifdef __STDC__
tTMO_DLL_NODE      *
TMO_DLL_Get (tTMO_DLL * pList)
#else
tTMO_DLL_NODE      *
TMO_DLL_Get (pList)
     tTMO_DLL           *pList;
#endif /* __STDC__ */
{
    tTMO_DLL_NODE      *pRetNode;

    if (TMO_DLL_Count (pList) == 0)
        return (NULL);
    pRetNode = pList->Head.pNext;
    TMO_DLL_Delete_In_Middle (pList, &pList->Head, pList->Head.pNext,
                              pList->Head.pNext->pNext);
    return (pRetNode);
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Nth                              */
/*  Description     : Returns 'u4NodeNum'th node from 'pList'  */
/*  Input(s)        : pList    - Pointer to the Linked List    */
/*                    u4NodeNum - Position of node of interest */
/*  Output(s)       : None                                     */
/*  Returns         : Pointer to the Node request from the list*/
/*                    NULL, otherwise                          */
/***************************************************************/
#ifdef __STDC__
tTMO_DLL_NODE      *
TMO_DLL_Nth (tTMO_DLL * pList, UINT4 u4_NodeNum)
#else
tTMO_DLL_NODE      *
TMO_DLL_Nth (pList, u4_NodeNum)
     tTMO_DLL           *pList;
     UINT4               u4_NodeNum;
#endif /* __STDC__ */
{
    tTMO_DLL_NODE      *pNthNode;

    if ((u4_NodeNum == 0) || (pList->u4_Count < u4_NodeNum))
        return (NULL);
    for (pNthNode = pList->Head.pNext; --u4_NodeNum; pNthNode = pNthNode->pNext)
    {
        ;
    }
    return (pNthNode);
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Find                             */
/*  Description     : Finds the specified node 'pNode' from the*/
/*                    list 'pList'.                            */
/*  Input(s)        : pList - Pointer to the Linked List       */
/*                    pNode - Pointer of the node which needs  */
/*                            to be found in list 'pList'      */
/*  Output(s)       : None                                     */
/*  Returns         : Position of the node in the list if a    */
/*                       matching one is found,                */
/*                    0 otherwise                              */
/***************************************************************/
#ifdef __STDC__
UINT4
TMO_DLL_Find (tTMO_DLL * pList, tTMO_DLL_NODE * pNode)
#else
UINT4
TMO_DLL_Find (pList, pNode)
     tTMO_DLL           *pList;
     tTMO_DLL_NODE      *pNode;
#endif /* __STDC__ */
{
    UINT4               i;
    tTMO_DLL_NODE      *pSearchNode, *pHead;

    for (pHead = &pList->Head, i = 1, pSearchNode = pList->Head.pNext;
         pSearchNode != pHead; pSearchNode = pSearchNode->pNext, i++)
    {
        if (pSearchNode == pNode)
            break;
    }
    return ((pSearchNode == pNode) ? i : 0);
}

/***************************************************************/
/*  Function Name   : TMO_DLL_FreeNodes                        */
/*  Description     : Frees the nodes present in the 'pList'   */
/*  Input(s)        : pList - Pointer to the Linked List       */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_FreeNodes (tTMO_DLL * pList)
#else
VOID
TMO_DLL_FreeNodes (pList)
     tTMO_DLL           *pList;
#endif /* __STDC__ */
{
    tTMO_DLL_NODE      *pTempNode, *pNode, *pHead;

    for (pHead = &pList->Head, pNode = pList->Head.pNext; pNode != pHead;
         pNode = pTempNode)
    {
        pTempNode = pNode->pNext;
        TMO_DLL_FREE (pNode);
    }
    TMO_DLL_Init (pList);
}

/***************************************************************/
/*  Function Name   : TMO_DLL_Clear                            */
/*  Description     : Removes and initialises all nodes from   */
/*                    the list and the memory is not freed     */
/*  Input(s)        : pList   -   Pointer to the Linked List   */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef __STDC__
VOID
TMO_DLL_Clear (tTMO_DLL * pList)
#else
VOID
TMO_DLL_Clear (pList)
     tTMO_DLL           *pList;
#endif /* __STDC__ */
{
    tTMO_DLL_NODE      *pTempNode, *pNode, *pHead;

    for (pHead = &pList->Head, pNode = pList->Head.pNext; pNode != pHead;
         pNode = pTempNode)
    {
        pTempNode = pNode->pNext;
        TMO_DLL_Init_Node (pNode);
    }
    TMO_DLL_Init (pList);
}
