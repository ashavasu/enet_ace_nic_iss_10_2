/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapcfg.h,v 1.24 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Compilation switches for FSAP.
 *
 *******************************************************************/
#ifndef _FSAPCFG_H
#define _FSAPCFG_H

/* Flags to be used to identify endinan-ness                 */

#if defined(NPSIM_WANTED) || defined(LINUXSIM_WANTED) || defined(OS_TMO) || defined (OS_VX2PTH_WRAP) || defined (OS_CPSS_MAIN_OS)
#ifndef CC_DIAB
#ifndef OS_QNX
#include <endian.h>
#endif
#endif
#endif

#if defined (OS_RTLINUX) && defined (MBSM_WANTED)
#include <linux/version.h>
#endif

#define OSIX_LITTLE_ENDIAN      1
#define OSIX_BIG_ENDIAN         2

/* Flags used in setting user configuration.                 */
#define FSAP_ON                 1
#define FSAP_OFF                2


/**********************************************/
/*  C O N F I G U R A B L E   S E C T I O N   */
/**********************************************/

/* Enables Multinode communication mechanism.                     */
#undef MCM_WANTED

/* Set RHS depending on processor endiannes.                      */

#ifndef NPSIM_WANTED
#if defined(LINUXSIM_WANTED) || defined(OS_TMO) || defined (OS_VX2PTH_WRAP) || defined (OS_CPSS_MAIN_OS)

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define OSIX_HOST               OSIX_LITTLE_ENDIAN
#else
#define OSIX_HOST               OSIX_BIG_ENDIAN
#endif

#else
#ifdef OS_LITTLE_ENDIAN
#define OSIX_HOST               OSIX_LITTLE_ENDIAN 
#else
#define OSIX_HOST               OSIX_BIG_ENDIAN
#endif
#endif
#else
/* For NPSIM use always Little Endian. */
#define OSIX_HOST               OSIX_LITTLE_ENDIAN
#endif

/* Set RHS to FSAP_ON to enable MULTINODE support                 */
#define MULTI_NODE_SUPPORT      FSAP_OFF

/* undef line below if target does not have varargs support.      */
#define VAR_ARGS_SUPPORT

/* Set RHS to FSAP_ON, if target env. has file system support     */
#define FILESYS_SUPPORT         FSAP_ON

/*
 * For Multiboard support, the IPC subsystem uses ACK mechanism
 * to guarantee reliable delivery. By default ACKs are disabled.
 * If ACK mechanism is required, change the line below to
 * #undef OSIX_IPC_NO_ACK
 * disabled. 
 */
#define OSIX_IPC_NO_ACK 

/* Set RHS to FSAP_ON, to enable tracing for OSIX.                */
#define DEBUG_OSIX              FSAP_OFF

/* Set RHS to FSAP_ON, to enable tracing for memory mgmt. module. */
#ifdef MEMTRACE_WANTED
#define DEBUG_MEM               FSAP_ON
#else
#define DEBUG_MEM               FSAP_OFF
#endif

/* Set RHS to FSAP_ON, to enable tracing for Timer module.        */
#define DEBUG_TMR               FSAP_OFF

/* Set RHS to FSAP_ON, to enable tracing for Buffer mgmt. module. */
#define DEBUG_BUF               FSAP_OFF

/* Set RHS to FSAP_ON, to enable debug version of UTL library.    */
#define DEBUG_UTL               FSAP_OFF

/* Set RHS to FSAP_ON, to include the math support in fsap.       */
#define MATH_SUPPORT            FSAP_ON


#endif /*_FSAPCFG_H*/
