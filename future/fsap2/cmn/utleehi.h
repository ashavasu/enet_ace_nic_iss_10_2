/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utleehi.h,v 1.16 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL EEH module's internal file.
 *
 */
#ifndef _UTLEEHI_H
#define _UTLEEHI_H

#include "osxinc.h"
#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "osxprot.h"
#include "utltrc.h"
#include "utleeh.h"
#ifndef LINUX_KERN
#include "string.h"
#endif
    
/* UTL EEH Semaphore Name, number of resources */
#define UTL_EEH_SEM_NAME      "UEEH"
#define UTL_EEH_SEM_NUM         (1)

typedef struct EventLogRecord {
   UINT4 u4EventType;
   UINT4 u4EventCode;
   UINT4 u4EventSrcModule;
   UINT4 u4EventDstModule;
   UINT4 u4EventParam1;
   UINT4 u4EventParam2;
   UINT4 u4EventParam3;
   UINT4 u4EventParam4;

   UINT4 u4EventTime;

   INT1 *pi1EventStr;  /* Memory of sizeof(MessageSize) specified in UtlEehInit()will be allocated */
   struct EventLogRecord *ptPrevLogRecord;
   struct EventLogRecord *ptNextLogRecord;
}tEEH_EVENT_LOG_RECORD;


typedef struct EventInfo {
   UINT4 u4EehCtrlLevel;
   UINT4 u4ReqNoOfEehRecords; /* HistSize (param of Init) is stored here */
   UINT4 u4NoOfEehRecords;    /* Number of active error records */
   UINT4 u4MaxMsgSize;         /* Maximum length of each error/event string */
   tEEH_EVENT_LOG_RECORD *ptHead;
   tEEH_EVENT_LOG_RECORD *ptTail;
}tEEH_EVENT_INFO;


#endif /* _UTLEEHI_H */
