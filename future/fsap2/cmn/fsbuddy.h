/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsbuddy.h,v 1.6 2015/04/28 12:00:00 siva Exp $
 *
 * Description: This file contains all the type definitions, macros, constants
 * used by the buddy module
 *
 *****************************************************************************/

#ifndef __BUDDY_H__
#define __BUDDY_H__

enum {BUDDY_SUCCESS = 0, BUDDY_FAILURE = -1 };
enum {BUDDY_CONT_BUF = 0, BUDDY_NON_CONT_BUF = 1, BUDDY_HEAP_EXTN = 2 };

INT4    MemBuddyInit            ARG_LIST((UINT4));
INT4    MemBuddyCreate          ARG_LIST((UINT4, UINT4, UINT4, UINT1));
VOID    MemBuddyDestroy         ARG_LIST((UINT1));
UINT1 * MemBuddyAlloc           ARG_LIST((UINT1, UINT4));
INT4    MemBuddyFree            ARG_LIST((UINT1, UINT1 *));
VOID    MemBuddyPrintStatistics ARG_LIST((UINT1 u1Id));

#endif /* __BUDDY_H__ */
