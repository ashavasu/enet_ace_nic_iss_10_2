/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: buftdfs.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: SRM Buf module type-definitions.
 *
 */
#ifndef  _BUFTDFS_H
#define _BUFTDFS_H

/*************************************************************
*** Type Definition for Buffer Pool Free Queue Descriptor ****
**************************************************************/

typedef struct CRU_BUF_FREE_QUE_DESC{
        UINT2 u2_QueId;         /* Index into Buffer Pool Record Array  */
        UINT2 u2Pad;            /* Alignment bytes */
        UINT4 u4_Size;          /* Size of Buffer in each pool */
        UINT4 u4_UnitsCount;    /* Total number of units in Buffer Pool */

        /* Buffer Statistics */
#ifdef BUF_DEBUG
        UINT4 u4_UnitsInUseCount; /* No. of Buffers of Size in use currently */
        UINT4 u4_AllocCount;      /* No. of units allocated   */
        UINT4 u4_ReleaseCount;    /* No. of units released    */
        UINT4 u4_AllocFailCount;  /* No. of units for which allocation failed */
        UINT4 u4_ReleaseFailCount; /* No. of units for which release failed */
        UINT4 u4_PeakUsageCount;  /* Peak usage count */
#endif

}tCRU_BUF_FREE_QUE_DESC;


#endif  /* _BUFTDFS_H */
