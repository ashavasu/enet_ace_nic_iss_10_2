/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlport.h,v 1.16 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Portable file for UTL module.
 *
 */
#ifndef _UTLPORT_H
#define _UTLPORT_H

#define UTL_CREATE_SEM(SemaName, initialcount, flags, pId) \
        OsixCreateSem((UINT1 *)SemaName, initialcount, flags, pId)

#define UTL_DELETE_SEM(nodeId, SemaName) OsixDeleteSem(nodeId, SemaName) \
        OsixDeleteSem(nodeId, (UINT1 *)SemaName)

#define UTL_TAKE_SEM(nodeId, SemaName, flags, timeout) \
        OsixTakeSem(nodeId, SemaName, flags, timeout)

#define UTL_GIVE_SEM(nodeId, SemaName) \
        OsixGiveSem(nodeId, SemaName)

#define UTL_GET_UNIQ_SEM() OsixGetNextSem()

#endif /*_UTLPORT_H*/
