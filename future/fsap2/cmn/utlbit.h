/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utlbit.h,v 1.14 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL Bit module's exported file.
 *
 */

#ifndef  _UTLBIT_H_
#define  _UTLBIT_H_

#define UTL_SUCCESS      0 
#define UTL_FAILURE      (-1)

/***            Prototype declarations             ***/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif
UINT1 CRU_BML_Set_Bit_In32BInt (UINT4 *pu4Number, UINT1 u1BitPosition);

UINT1 CRU_BML_Clear_Bit_In32BInt(UINT4 *pu4Number, UINT1 u1BitPosition);

UINT1 CRU_BML_Check_Bit_In32BInt (UINT4 u4Number, UINT1 u1BitPosition);

UINT1 CRU_BML_Find_SetBit_In32BInt (UINT4 u4Number);

void CRU_BML_Print_AllBits_In32BInt (UINT4 u4Number);

void  CRU_BML_Set_Bit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize, UINT2 u2BitNumber);

void  CRU_BML_Clear_Bit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize, UINT2 u2BitNumber);

UINT1 CRU_BML_Check_Bit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize, UINT2 u2BitNumber);

UINT2 CRU_BML_Find_FirstSetBit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize);

UINT2 CRU_BML_Find_NextSetBit_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize, UINT2 u2CurrentlySetBit);

void  CRU_BML_Print_AllBits_InArray (UINT1 *pu1Data, UINT2 u2BitArraySize);
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif


#endif  /* _UTLBIT_H_ */
