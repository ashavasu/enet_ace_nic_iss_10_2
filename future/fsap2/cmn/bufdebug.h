/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bufdebug.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Debug statements used in SRM Buf module.
 *
 */

#ifndef _BUFDEBUG_H
#define _BUFDEBUG_H

UINT1  gu1DbgBuf[200];

#define CRU_BUF_PRINT(p1) UtlTrcPrint(p1)

#define CRU_BUF_PRINT_MSG_P1(p1) UtlTrcPrint(p1)

#define CRU_BUF_PRINT_MSG_P2(p1,p2) { \
        sprintf(gu1DbgBuf,p1,p2); \
        UtlTrcPrint(gu1DbgBuf);\
        }
#define CRU_BUF_PRINT_MSG_P3(p1,p2,p3) { \
        sprintf(gu1DbgBuf,p1,p2, p3); \
        UtlTrcPrint(gu1DbgBuf);\
        }
#define CRU_BUF_PRINT_MSG_P4(p1,p2,p3,p4)  {\
        sprintf(gu1DbgBuf,p1,p2, p3, p4); \
        UtlTrcPrint(gu1DbgBuf);\
        }
#define CRU_BUF_PRINT_MSG_P5(p1,p2,p3,p4,p5)  {\
        sprintf(gu1DbgBuf,p1,p2, p3, p4, p5); \
        UtlTrcPrint(gu1DbgBuf);\
        }
#define CRU_BUF_PRINT_MSG_P6(p1,p2,p3,p4,p5,p6)  {\
        sprintf(gu1DbgBuf,p1,p2, p3, p4, p5, p6); \
        UtlTrcPrint(gu1DbgBuf);\
        }
#define CRU_BUF_PRINT_MSG_P7(p1,p2,p3,p4,p5,p6,p7)  {\
        sprintf(gu1DbgBuf,p1,p2, p3, p4, p5, p6, p7); \
        UtlTrcPrint(gu1DbgBuf);\
        }
#define CRU_BUF_PRINT_MSG_P8(p1,p2,p3,p4,p5,p6,p7,p8)  {\
        sprintf(gu1DbgBuf,p1,p2, p3, p4, p5, p6, p7, p8); \
        UtlTrcPrint(gu1DbgBuf);\
        }
#define CRU_BUF_PRINT_MSG_P9(p1,p2,p3,p4,p5,p6,p7,p8,p9)  {\
        sprintf(gu1DbgBuf,p1,p2, p3, p4, p5, p6, p7, p8, p9); \
        UtlTrcPrint(gu1DbgBuf);\
        }

/*****************************************************************************/
/*            Prototype  Declarations                                        */
/*****************************************************************************/



VOID  CRU_BUF_Update_Stats(tCRU_BUF_FREE_QUE_DESC *pBufQueDesc, UINT1 u1_BufStatsFlag);
VOID  CRU_BUF_Print_Stats(void);
VOID  CRU_BUF_Print_DataDescChainStats(tCRU_BUF_DATA_DESC *pDataDesc);

#endif          /* End of Buffer Debug Header File  */



