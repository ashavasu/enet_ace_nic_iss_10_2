/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utleeh.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL EEH module's exported file.
 *
 */
#ifndef _UTLEEH_H
#define _UTLEEH_H

/* Return Values */
#define UTL_EEH_SUCCESS     (0)
#define UTL_ERR_NO_MEM      (1)
#define UTL_ERR_MEM_ERR     (2)
#define UTL_ERR_NO_RSRC     (3)
#define UTL_ERR_RSRC_ERR    (4)
#define UTL_ERR_PARAM_ERR   (5)


/*
 * Categories of errors and events
 */
#define UTL_ERR_FATAL   1
#define UTL_ERR_CONF    2
#define UTL_ERR_PROTO   3
#define UTL_EVT_MAJOR   4
#define UTL_EVT_MINOR   5


/*
 * Flags for controlling the logging of errors 
 * and events
 */
#define UTL_EEH_LOG_NONE         0x0000
#define UTL_EEH_LOG_CONF_ERR     0x0001
#define UTL_EEH_LOG_PROTO_ERR    0x0002
#define UTL_EEH_LOG_MAJOR_EVT    0x0004
#define UTL_EEH_LOG_MINOR_EVT    0x0008
    
/*
 * Macros used for notifying errors and events
 */

#define UTL_NOTIFY_FATAL_ERR(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str) \
   UtlEehNotify(UTL_ERR_FATAL, u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)

#ifdef ERR_ON
#define UTL_NOTIFY_CONF_ERR(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str) \
        UtlEehNotify(UTL_ERR_CONF, u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#else
#define UTL_NOTIFY_CONF_ERR(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#endif

#ifdef ERR_ON
#define UTL_NOTIFY_PROTO_ERR(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str) \
        UtlEehNotify(UTL_ERR_PROTO, u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#else
#define UTL_NOTIFY_PROTO_ERR(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#endif

#ifdef ERR_ON
#define UTL_NOTIFY_MAJOR_EVT(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str) \
        UtlEehNotify(UTL_EVT_MAJOR, u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#else
#define UTL_NOTIFY_MAJOR_EVT(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#endif

#ifdef ERR_ON
#define UTL_NOTIFY_MINOR_EVT(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str) \
        UtlEehNotify(UTL_EVT_MINOR, u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#else
#define UTL_NOTIFY_MINOR_EVT(u4Code, u4Src, u4Dst, u4Param1, u4Param2, u4Param3, u4Param4, pi1Str)
#endif

#endif /* _UTLEEH_H */
