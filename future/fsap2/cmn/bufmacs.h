/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bufmacs.h,v 1.15 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Macros used in the SRM buf module.
 *
 */
#ifndef  _BUFMACS_H 
#define  _BUFMACS_H 

#define CRU_BUF_MALLOC(Size,Type)       MEM_MALLOC(Size,Type)
#define CRU_BUF_CALLOC(Size,Count,Type) MEM_CALLOC(Size,Count,Type)
#define CRU_BUF_FREE(Ptr)               MEM_FREE(Ptr)

#define CRU_BUF_MEMSET                  MEMSET
#define CRU_BUF_MEMCPY                  MEMCPY

#define CRU_HTONS(u2_ShortIntVal)       OSIX_HTONS(u2_ShortIntVal)
#define CRU_NTOHS(u2_ShortIntVal)       OSIX_NTOHS(u2_ShortIntVal)
#define CRU_HTONL(u4_LongIntVal)        OSIX_HTONL(u4_LongIntVal)
#define CRU_NTOHL(u4_LongIntVal)        OSIX_NTOHL(u4_LongIntVal)


#define CRU_BUF_GET_FIRST_BLOCK_FREE_BYTE_COUNT(pDataDesc,pFirstValidDataDesc) (((pDataDesc) != (pFirstValidDataDesc)) ? (pDataDesc)->u4_FreeByteCount : (UINT4) (((tCRU_BUF_DATA_DESC *)((VOID *)(pDataDesc->pu1_FirstByte - sizeof(tCRU_BUF_DATA_DESC))) != pDataDesc) ? 0 : ((pDataDesc)->pu1_FirstValidByte - (pDataDesc)->pu1_FirstByte))) 

#define CRU_BUF_GET_LAST_BLOCK_FREE_BYTE_COUNT(pDataDesc) \
        ((pDataDesc)->u4_FreeByteCount)

#define CRU_BUF_GET_BLOCK_VALID_BYTE_COUNT(pDataDesc) \
        ((pDataDesc)->u4_ValidByteCount)

#endif  /*  _BUFMACS_H  */
