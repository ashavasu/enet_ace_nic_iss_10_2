/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: memport.h,v 1.16 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Portable macros for the SRM Mem module.
 *
 */

#ifndef _MEMPORT_H
#define _MEMPORT_H

#define MEM_CREATE_SEM(SemaName, initialcount, flags, pId) \
        OsixCreateSem((UINT1 *)SemaName, initialcount, flags, pId)

#define MEM_DELETE_SEM(nodeId, SemaName) OsixDeleteSem(nodeId, SemaName) \
        OsixDeleteSem(nodeId, (UINT1 *)SemaName)

#define MEM_TAKE_SEM(nodeId, SemaName, flags, timeout) \
        OsixTakeSem(nodeId, SemaName, flags, timeout)

#define MEM_GIVE_SEM(nodeId, SemaName) \
        OsixGiveSem(nodeId, SemaName)

#define MEM_GET_UNIQ_SEM() OsixGetNextSem()

extern UINT4 OsixGetNextSem(void);

#endif /*_MEMPORT_H*/
