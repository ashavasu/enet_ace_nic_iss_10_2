/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utldll.h,v 1.18 2015/04/28 12:00:00 siva Exp $
 *
 * Description: UTL DLL module's exported file.
 *
 */

#ifndef _UTLDLL_H
#define _UTLDLL_H


/***** Type Definitions *****/
typedef struct TMO_DLL_NODE{
        struct TMO_DLL_NODE *pNext; /* Points at the next node in the list */
        struct TMO_DLL_NODE *pPrev; /* Points at the previous node in the list */
}tTMO_DLL_NODE;

typedef struct TMO_DLL{
    tTMO_DLL_NODE Head;  /**** Header List Node        ****/
    UINT4      u4_Count; /**** Number Of Nodes In List ****/
    UINT4      u4_Offset;/**** Offset of the node in the user structure****/
}tTMO_DLL;


/***  MACRO Definition  ***/
#define TMO_DLL_Init(pList) UTL_DLL_INIT(pList,0)

#define UTL_DLL_INIT(pList,u4Offset) \
    {\
        (pList)->Head.pNext = &(pList)-> Head; \
        (pList)->Head.pPrev = &(pList)-> Head; \
        (pList)->u4_Count   = 0; \
        (pList)->u4_Offset  =u4Offset;\
    }

/***** This Macro Initialises The Node *****/
#define TMO_DLL_Init_Node(pNode) \
        (pNode)->pPrev = (pNode)->pNext = NULL;

/**********************************************************************
 * UTL_DLL_GET_STRUCT_PTR:
 * This macro gets the the structure ptr from node ptr and pList
 * It is used during the scan operation, UTL_DLL_OFFSET_SCAN().
 **********************************************************************/

#define UTL_DLL_GET_STRUCT_PTR(pList,pNode,TypeCast) \
        ((pNode == NULL) ? NULL: ((TypeCast)(VOID *)((UINT1*)pNode-(pList)->u4_Offset)))

/**********************************************************************
***this macro gets the dll node ptr from the structure ptr and pList***
***********************************************************************/

#define UTL_DLL_GET_NODE_PTR(pList,pStruct) \
        ((pStruct == NULL) ? NULL: ((tTMO_DLL_NODE*)(VOID *)\
                                    ((UINT1*)pStruct+(pList)->u4_Offset)))


#define TMO_DLL_Add(pList,pNode) \
        TMO_DLL_Insert_In_Middle((pList),(pList)->Head.pPrev,(pNode),&(pList)->Head)

#define TMO_DLL_Count(pList) ((pList)->u4_Count)

#define TMO_DLL_Delete(pList,pNode) \
        (TMO_DLL_Is_Node_In_List(pNode) != 0) ? \
        (TMO_DLL_Delete_In_Middle((pList),(pNode)->pPrev, \
                                  (pNode),(pNode)->pNext), 1) : 0

#define TMO_DLL_First(pList) \
        ((TMO_DLL_Count((pList)) == 0) ? NULL: (pList)->Head.pNext)

#define TMO_DLL_Last(pList) \
        ((TMO_DLL_Count((pList)) == 0) ? NULL : (pList)->Head.pPrev)

#define TMO_DLL_Next(pList,pNode) \
        (((pNode) == NULL) ? TMO_DLL_First(pList) : \
        (((pNode)->pNext == &(pList)->Head) ? NULL : (pNode)->pNext))

#define TMO_DLL_Previous(pList,pNode) \
        (((pNode) == NULL) ? TMO_DLL_Last(pList) : \
        (((pNode)->pPrev == &(pList)->Head) ? NULL : (pNode)->pPrev))

#define TMO_DLL_Is_Node_In_List(pNode) \
        (((pNode)->pNext != NULL) && \
         ((pNode)->pPrev != NULL) && \
         ((pNode)->pNext->pPrev == pNode) && \
         ((pNode)->pPrev->pNext == pNode))

/*********************************************************************
**** This Macro Is Useful For Scanning Through The Entire List    ****
**** 'pNode' Is A Temp. Variable Of Type 'TypeCast'. pList is the ****
**** Pointer To linked_list                                       ****
**********************************************************************/
#define TMO_DLL_Scan(pList,pNode,TypeCast) \
        for(pNode = (TypeCast)(TMO_DLL_First((pList))); \
            pNode != NULL; \
            pNode = (TypeCast)(TMO_DLL_Next((pList),((tTMO_DLL_NODE *)(VOID *)(pNode)))))

/******************************************************************
**** This Macro Is Useful For Scanning Through The Entire List ****
**** This macro allows deletion within the loop                ****
**** pList -  List TO BE Scanned                               ****
**** pType -Pointer To a structure of Type `TypeCast`        ****
****          in which dll node is a member and nodes of this  ****
****          types is  Returned One By One                    ****
**** pTemp   -Pointer To a structure of Type `TypeCast`        ****
****          in which dll node is a member.Holds the next     ****
****          node to pType                                  ****
****          types is  Returned One By One                    ****
**** Cast  -  Appropriate Cast(e.g. t_NODE *,When Ptr Is A     ****
****          Pointer To t_NODE Type).                         ****
*******************************************************************/

#define UTL_DLL_OFFSET_SCAN(pList,pType,pTemp,TypeCast) \
            for(pType = UTL_DLL_GET_STRUCT_PTR(pList,TMO_DLL_First((pList)),\
                          TypeCast),\
                          pTemp = ((pType == NULL)? NULL : \
                          UTL_DLL_GET_STRUCT_PTR(pList,TMO_DLL_Next((pList),\
                          UTL_DLL_GET_NODE_PTR(pList,pType)),TypeCast)); \
                pType != NULL; \
                pType = pTemp ,\
                pTemp   = ((pType == NULL)? NULL :\
                          UTL_DLL_GET_STRUCT_PTR(pList,TMO_DLL_Next((pList),\
                          UTL_DLL_GET_NODE_PTR(pList,pType)),TypeCast)))


/****  Prototype Declarations   ***/
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

VOID  TMO_DLL_Concat  ARG_LIST((tTMO_DLL *pDstList, tTMO_DLL *pAddList));
VOID  TMO_DLL_Extract ARG_LIST((tTMO_DLL *pSrcList, tTMO_DLL_NODE *pStartNode,tTMO_DLL_NODE *pEndNode,tTMO_DLL *pDstList));
VOID  TMO_DLL_FreeNodes ARG_LIST((tTMO_DLL *pList));
VOID  TMO_DLL_Replace ARG_LIST((tTMO_DLL *pList, tTMO_DLL_NODE *pOld, tTMO_DLL_NODE *pNew));
VOID  TMO_DLL_Insert_In_Middle ARG_LIST((tTMO_DLL *pList, tTMO_DLL_NODE *pPrev, tTMO_DLL_NODE *pMid, tTMO_DLL_NODE *pNext));
VOID  TMO_DLL_Insert  ARG_LIST((tTMO_DLL *pList, tTMO_DLL_NODE *pPrev, tTMO_DLL_NODE *pNode));
VOID  TMO_DLL_Delete_In_Middle ARG_LIST((tTMO_DLL *pList, tTMO_DLL_NODE *pPrev, tTMO_DLL_NODE *pNode, tTMO_DLL_NODE *pNext));
UINT4 TMO_DLL_Find    ARG_LIST((tTMO_DLL *pList, tTMO_DLL_NODE *pNode));
tTMO_DLL_NODE *TMO_DLL_Get ARG_LIST((tTMO_DLL *pList));
tTMO_DLL_NODE *TMO_DLL_Nth ARG_LIST((tTMO_DLL *pList, UINT4 u4_NodeNum));
VOID TMO_DLL_Clear ARG_LIST((tTMO_DLL *pList));
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#endif /**** _UTLDLL_H ****/
