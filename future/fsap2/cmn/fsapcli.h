/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsapcli.h,v 1.4 2015/04/28 12:00:00 siva Exp $
 *
 * Description: Exported file for fsap's CLI
 */
#ifndef _FSAPCLI_H
#define _FSAPCLI_H
enum {
    FSAP_SHOW_TASK = 1,
    FSAP_SHOW_QUE,
    FSAP_SHOW_SEM,
    FSAP_SHOW_MEMPOOL,
    FSAP_TASK_SUSPEND,
    FSAP_TASK_RESUME,
    FSAP_TRACE_ON,
    FSAP_TRACE_OFF
};

enum {
    OSIX_TSK_TRC = 0x01,
    OSIX_QUE_TRC = 0x02,
    OSIX_SEM_TRC = 0x04,
    OSIX_MEM_TRC = 0x08,
    OSIX_BUF_TRC = 0x10,
    OSIX_TMR_TRC = 0x20,
    OSIX_ALL_TRC = ~0L
};

INT1  CliProcessFsapCmd ARG_LIST((UINT4 u4Command, ...));
UINT4 FsapShowTask          ARG_LIST((UINT1 au1Name[], UINT1 *pu1Result, INT4 buffer_size));
UINT4 FsapShowQue           ARG_LIST((UINT1 au1Name[], UINT1 *pu1Result, INT4 buffer_size));
UINT4 FsapShowSem           ARG_LIST((UINT1 au1SemName[], UINT1 *pu1Result, UINT4 *pu4NextIdx, INT4 buffer_size));
void  FsapTrace             ARG_LIST((UINT4 u4Flag, UINT4 u4Value, UINT4 *pu4TrcLvl));
UINT4 TmrShowTimerList      ARG_LIST((UINT4 u4Count, UINT1 *pu1Result));
#endif
