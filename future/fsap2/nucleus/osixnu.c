/************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                             */
/* Licensee Aricent Inc.,1997-2002                    */
/* $Id: osixnu.c,v 1.19 2015/04/28 12:15:47 siva Exp $ */
/*                                                                      */
/*  FILE NAME             :  osixnu.c                                   */
/*  PRINCIPAL AUTHOR      :  Aricent Inc.                            */
/*  SUBSYSTEM NAME        :  FSAP2                                      */
/*  MODULE NAME           :  OSIX                                       */
/*  LANGUAGE              :  C                                          */
/*  TARGET ENVIRONMENT    :  Nucleus                                    */
/*  DATE OF FIRST RELEASE :  Mar-2002                                   */
/*  DESCRIPTION           :  Contains OSIX reference code for Nucleus   */
/*                        :  All basic OS facilities used by protocol   */
/*                        :  software from FS use only these APIs       */
/************************************************************************/

#include "osxinc.h"
#include "osix.h"
/*******************************************************************
*
*               Typedef
*
********************************************************************/
/* Prototype of nucleus' Entry Point function. */
typedef VOID        (*NU_EPFP) (UNSIGNED, VOID *);

#define OSIX_MUTEX_NAME        "OSMU"
#define OSIX_SCAVENGER_TASK    "SCAV"
#define OSIX_SCAVENGER_PRIO     1
#define OSIX_SCAVENGER_EVENT    0x8000

/* The basic structure maintaining the name-to-id mapping */
/* of OSIX resources. 3 arrays - one for tasks, one for   */
/* semaphores and one for queues are maintained by OSIX.  */
/* Each array has this structure as the basic element. We */
/* use this array to store events for tasks also.         */

/* The description of fields of the structures below is as follows */
/*   TaskId   - the Task id returned by the OS                     */
/*   QueId    - the Que  id returned by the OS                     */
/*   SemId    - the Sem  id returned by the OS                     */
/*   u4StackAddr - Start Address of task stack.                    */
/*   u4QStartAddr - Start address of the Que                       */
/*   u4DelFlag    - Flag used by Scavenger task to delete a task   */
/*   u2Free   - whether this structure is free or used             */
/*   u4Events - for event simulation; used only for tasks          */
/*   au1Name  - name is always multiple of 4 characters in length  */

typedef struct OsixRscTskStruct
{
    NU_TASK             TaskId;
    UINT4               u4StackAddr;
    UINT4               u4DelFlag;
    UINT4               u4Events;
    UINT2               u2Free;
    UINT2               u2Pad;
    NU_SEMAPHORE        TskMutex;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixTsk;
typedef struct OsixRscQueStruct
{
    NU_QUEUE            QueueId;
    UINT4               u4QStartAddr;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixQue;
typedef struct OsixRscSemStruct
{
    NU_SEMAPHORE        SemId;
    UINT2               u2Free;
    UINT2               u2Filler;
    UINT1               au1Name[OSIX_NAME_LEN + 4];
}
tOsixSem;

UINT4               gu4Tps = OSIX_TPS;
UINT4               gu4Stups = OSIX_STUPS;
UINT4               gu4SemCnt = 0;

tOsixTsk            gaOsixTsk[OSIX_MAX_TSKS + 2];
/* 0th element is invalid and not used  */
/* One extra task is for SCAVENGER task */

tOsixSem            gaOsixSem[OSIX_MAX_SEMS + 1];
/* 0th element is invalid and not used  */

tOsixQue            gaOsixQue[OSIX_MAX_QUES + 1];
/* 0th element is invalid and not used  */

NU_SEMAPHORE        gOsixMutex;

/* Prototypes of Private  Functions. */
static VOID OsixRscDel ARG_LIST ((UINT4, UINT4));
static VOID OsixScavengerTask ARG_LIST ((INT4));
static UINT4 OsixRscAdd ARG_LIST ((UINT1[], UINT4, UINT4));
static UINT4 OsixProcessDelScav ARG_LIST ((VOID));

extern UINT4 OsixSTUPS2Ticks ARG_LIST ((UINT4));
extern UINT4 OsixTicks2STUPS ARG_LIST ((UINT4));
extern UINT4        gu4Seconds;

/************************************************************************/
/* Routines for task creation, deletion and maintenance */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixTskCrt                                        */
/*  Description     : Creates task.                                     */
/*  Input(s)        : au1Name[ ] -        Name of the task              */
/*                  : u4TskPrio -         Task Priority                 */
/*                  : u4StackSize -       Stack size                    */
/*                  : (*TskStartAddr)() - Entry point function          */
/*                  : u4Arg -             Arguments to above fn.        */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskCrt (UINT1 au1Name[], UINT4 u4TskPrio, UINT4 u4StackSize,
            VOID (*TskStartAddr) (INT1 *), INT1 *u4Arg, tOsixTaskId * pTskId)
{
    tOsixTsk           *pTsk;
    UINT1              *pu1StackAddr;
    UINT4               u4Idx;
    INT4                i4OsPrio;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];

    if (u4StackSize == 0)
    {
        u4StackSize = OSIX_DEFAULT_STACK_SIZE;
    }

    u4TskPrio = u4TskPrio & 0x0000ffff;

    /* For tasks, the nucleus version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0.  */
    if (OsixRscAdd (au1Name, OSIX_TSK, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Remap the task priority to Nucleus's range of values. */
    i4OsPrio = OS_LOW_PRIO + (((((INT4) (u4TskPrio) - FSAP_LOW_PRIO) *
                                (OS_HIGH_PRIO -
                                 OS_LOW_PRIO)) / (FSAP_HIGH_PRIO -
                                                  FSAP_LOW_PRIO)));

    /* Get the global task array index to return as the task id. */
    /* Return value check not needed because we just added it.   */
    OsixRscFind (au1Name, OSIX_TSK, &u4Idx);
    pTsk = &(gaOsixTsk[u4Idx]);
    *pTskId = (tOsixTaskId) u4Idx;

    /* Allocate space for the Stack. */
    if ((pu1StackAddr = MEM_MALLOC (u4StackSize, UINT1)) == NULL)
    {
        OsixRscDel (OSIX_TSK, u4Idx);
        return (OSIX_FAILURE);
    }

    /* Set the stack-start address to be the high address of the     */
    /* allocated memory chunk. Stack starts growing down from there. */
    pTsk->u4StackAddr = (UINT4) pu1StackAddr;

    /* Create a unique name for the sema4. */
    MEMSET (au1SemName, '\0', (OSIX_NAME_LEN + 4));
    SPRINTF ((CHR1 *) au1SemName, "%s%.2ld", "#s", gu4SemCnt);
    gu4SemCnt++;

    /* Initialize the Nucleus SCB */
    MEMSET (&(pTsk->TskMutex), 0, sizeof (NU_SEMAPHORE));

    if (NU_Create_Semaphore (&(pTsk->TskMutex), (CHR1 *) au1SemName, 0, NU_FIFO)
        != NU_SUCCESS)
    {
        MEM_FREE (pu1StackAddr);
        OsixRscDel (OSIX_TSK, u4Idx);
        return (OSIX_FAILURE);
    }

    /* Initialize the Nuclues TCB */
    MEMSET (&(pTsk->TaskId), 0, sizeof (NU_TASK));

    /* Task created  not started */

    /* Nucleus expects the 4th argument as 'argc' , 5th argument as 'argv'
     * FS Tasks will consider only one parameter. 
     * To make the required parameter to get passed to FS task, 
     * the 4th argument is passed as 'u4Arg'. 
     * The 5th argument will be unused by FS tasks.
     */

    if (NU_Create_Task
        (&(pTsk->TaskId), (char *) au1Name, (NU_EPFP) TskStartAddr, u4Arg,
         (void *) 0, (void *) pu1StackAddr, u4StackSize, (UINT1) i4OsPrio,
         0, NU_PREEMPT, NU_NO_START) != NU_SUCCESS)
    {
        NU_Delete_Semaphore (&(pTsk->TskMutex));
        MEM_FREE (pu1StackAddr);
        OsixRscDel (OSIX_TSK, u4Idx);
        return (OSIX_FAILURE);
    }

    NU_Resume_Task (&(pTsk->TaskId));
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixTskDel                                        */
/*  Description     : Deletes a task.                                   */
/*  Input(s)        : TskId          - Task Id                          */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixTskDel (tOsixTaskId TskId)
{
    tOsixTaskId         ScavTskId;
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    /* Set the TCB delete flag */
    gaOsixTsk[(UINT4) TskId].u4DelFlag = OSIX_SET;

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    STRCPY (au1Name, OSIX_SCAVENGER_TASK);

    if (OsixRscFind (au1Name, OSIX_TSK, &ScavTskId) == OSIX_SUCCESS)
    {
        OsixEvtSend (ScavTskId, OSIX_SCAVENGER_EVENT);
    }
    return;
}

/************************************************************************/
/*  Function Name   : OsixTskDelay                                      */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskDelay (UINT4 u4Duration)
{
    u4Duration = OsixSTUPS2Ticks (u4Duration);
    NU_Sleep (u4Duration);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixDelay                                         */
/*  Description     : Suspends a task for a specified duration.         */
/*  Input(s)        : u4Duration - Delay time.                          */
/*                  : i4Unit     - Units in which the duration is       */
/*                                 specified                            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixDelay (UINT4 u4Duration, INT4 i4Unit)
{
    /* The function OsixTskdelay () does not allow delays less than 100 ms.
     * For backward compatibility reasons, that function cannot be changed.
     * So, this new function OsixDelay is introduced. On any this must be
     * ported before being used */

    UNUSED_PARAM (u4Duration);
    UNUSED_PARAM (i4Unit);

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixTskIdSelf                                     */
/*  Description     : Get Osix Id of current Task                       */
/*  Input(s)        : None                                              */
/*  Output(s)       : pTskId -            Task Id.                      */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixTskIdSelf (tOsixTaskId * pTskId)    /* Get ID of current task */
{
    NU_TASK            *i4OsTskId;
    UINT4               u4Count;

    i4OsTskId = NU_Current_Task_Pointer ();

    if (i4OsTskId == NU_NULL)
    {
        return (OSIX_FAILURE);
    }

    /* One more is for SCAVENGER task */
    for (u4Count = 1; u4Count <= (OSIX_MAX_TSKS + 1); u4Count++)
    {
        if (&(gaOsixTsk[u4Count].TaskId) == i4OsTskId)
        {
            *pTskId = (tOsixTaskId) u4Count;
            return (OSIX_SUCCESS);
        }
    }
    return (OSIX_FAILURE);
}

/************************************************************************/
/* Routines for event management - send / receive event                 */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixEvtSend                                       */
/*  Description     : Sends an event to a specified task.               */
/*  Input(s)        : TskId          - Task Id                          */
/*                  : u4Events       - The Events, OR-d                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixEvtSend (tOsixTaskId TskId, UINT4 u4Events)
{
    UINT4               u4Idx = (UINT4) TskId;

    gaOsixTsk[u4Idx].u4Events |= u4Events;
    NU_Release_Semaphore (&(gaOsixTsk[u4Idx].TskMutex));
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixEvtRecv                                       */
/*  Description     : To receive a event.                               */
/*  Input(s)        : TskId             - Task Id                       */
/*                  : u4Events          - List of interested events.    */
/*  Output(s)       : pu4EventsReceived - Events received.              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/

UINT4
OsixEvtRecv (tOsixTaskId TskId, UINT4 u4Events, UINT4 u4Flgs,
             UINT4 *pu4RcvEvents)
{
    UINT4               u4Idx = (UINT4) TskId;

    *pu4RcvEvents = 0;

    if ((u4Flgs == OSIX_NO_WAIT)
        && (((gaOsixTsk[u4Idx].u4Events) & u4Events) == 0))
    {
        return (OSIX_FAILURE);
    }

    while (1)
    {
        if (((gaOsixTsk[u4Idx].u4Events) & u4Events) != 0)
        {                        /* A required event has happened */
            *pu4RcvEvents = (gaOsixTsk[u4Idx].u4Events) & u4Events;
            gaOsixTsk[u4Idx].u4Events &= ~(*pu4RcvEvents);
            return (OSIX_SUCCESS);
        }

        if (NU_Obtain_Semaphore (&(gaOsixTsk[u4Idx].TskMutex), NU_SUSPEND)
            != NU_SUCCESS)
        {
            return (OSIX_FAILURE);
        }
    }
}

/************************************************************************/
/*  Function Name   : OsixInitialize                                    */
/*  Description     : The OSIX Initialization routine. To be called     */
/*                    before any other OSIX functions are used.         */
/*  Input(s)        : pOsixCfg - Pointer to OSIX config info.           */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixInitialize ()
{
    tOsixTaskId         ScavTaskId;
    UINT4               u4Idx;
    UINT1               au1Name[OSIX_NAME_LEN + 4];

    /* Mutual exclusion semaphore to add or delete elements from */
    /* name-id-mapping list. This semaphore itself will not be   */
    /* added to the name-to-id mapping list. This is a OS        */
    /* specific call and must be mapped to relevant call for OS  */

    /* Initialize the Nucleus SCB */
    MEMSET (&gOsixMutex, 0, sizeof (NU_SEMAPHORE));

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    STRCPY (au1Name, OSIX_MUTEX_NAME);
    if (NU_Create_Semaphore (&gOsixMutex, (CHR1 *) au1Name, 1,
                             NU_FIFO) != NU_SUCCESS)
    {
        return (OSIX_FAILURE);
    }

    /* Initialize all arrays. */
    /* One more task to make allowance for SCAV task */
    for (u4Idx = 0; u4Idx <= (OSIX_MAX_TSKS + 1); u4Idx++)
    {
        gaOsixTsk[u4Idx].u4DelFlag = OSIX_RESET;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u4Events = 0;
        MEMSET (gaOsixTsk[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
        MEMSET (gaOsixSem[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
        MEMSET (gaOsixQue[u4Idx].au1Name, '\0', (OSIX_NAME_LEN + 4));
    }

    MEMSET ((UINT1 *) au1Name, '\0', (OSIX_NAME_LEN + 4));
    STRCPY (au1Name, OSIX_SCAVENGER_TASK);

    /* Create a Scavenger Task for Task Deletion. */
    if (OsixTskCrt (au1Name, OSIX_SCAVENGER_PRIO,
                    OSIX_DEFAULT_STACK_SIZE, OsixScavengerTask, 0, &ScavTaskId))
    {
        NU_Delete_Semaphore (&gOsixMutex);
        return (OSIX_FAILURE);
    }

    gu4SemCnt = 0;

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixShutDown                                      */
/*  Description     : Stops OSIX.                                       */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixShutDown ()
{
    UINT4               u4Idx;

    /* Re-initialize all arrays and delete global semaphore */
    /* One more task to make allowance for SCAV task */
    for (u4Idx = 0; u4Idx <= (OSIX_MAX_TSKS + 1); u4Idx++)
    {
        if (gaOsixTsk[u4Idx].u2Free != OSIX_TRUE)
        {
            /* We delete scavenger task here so we cant */
            /* use osix call anymore to delete task     */
            MEM_FREE ((UINT1 *) gaOsixTsk[u4Idx].u4StackAddr);
            NU_Terminate_Task (&(gaOsixTsk[u4Idx].TaskId));
            NU_Delete_Task (&(gaOsixTsk[u4Idx].TaskId));
            NU_Delete_Semaphore (&(gaOsixTsk[u4Idx].TskMutex));
        }
        gaOsixTsk[u4Idx].u4DelFlag = OSIX_RESET;
        gaOsixTsk[u4Idx].u2Free = OSIX_TRUE;
        gaOsixTsk[u4Idx].u4Events = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
    {
        if (gaOsixSem[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixSemDel (&(gaOsixSem[u4Idx].SemId));
        }
        gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
        gaOsixSem[u4Idx].u2Filler = 0;
    }
    for (u4Idx = 0; u4Idx <= OSIX_MAX_QUES; u4Idx++)
    {
        if (gaOsixQue[u4Idx].u2Free != OSIX_TRUE)
        {
            OsixQueDel (&(gaOsixQue[u4Idx].QueueId));
        }
        gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
        gaOsixQue[u4Idx].u2Filler = 0;
    }

    /* Delete Semaphore */
    NU_Delete_Semaphore (&gOsixMutex);

    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing semaphores                                     */
/* Keep it simple - support 1 type of semaphore - binary, blocking.     */
/* After creating, the semaphore must be given before it can be taken.  */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixSemCrt                                        */
/*  Description     : Creates a sema4.                                  */
/*  Input(s)        : au1Name [ ] - Name of the sema4.                  */
/*  Output(s)       : pSemId         - The sema4 Id.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemCrt (UINT1 au1Name[], tOsixSemId * pSemId)
{
    /* For sem, the pThreads version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0.  */
    if (OsixRscAdd (au1Name, OSIX_SEM, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Get the global semaphore id to return as the semaphore   */
    /* Return value check not needed because we just added it.  */
    OsixRscFind (au1Name, OSIX_SEM, (UINT4 *) pSemId);

    /* Initialize the Nucleus SCB */
    MEMSET (*pSemId, 0, sizeof (NU_SEMAPHORE));

    if (NU_Create_Semaphore (*pSemId, (char *) au1Name, 0, NU_FIFO) !=
        NU_SUCCESS)
    {
        OsixRscDel (OSIX_SEM, (UINT4) *pSemId);
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemDel                                        */
/*  Description     : Deletes a sema4.                                  */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
OsixSemDel (tOsixSemId SemId)
{
    OsixRscDel (OSIX_SEM, (UINT4) SemId);
    NU_Delete_Semaphore (SemId);
}

/************************************************************************/
/*  Function Name   : OsixSemGive                                       */
/*  Description     : Used to release a sem4.                           */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemGive (tOsixSemId SemId)
{
    if (NU_Release_Semaphore (SemId) != NU_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixSemTake                                       */
/*  Description     : Used to acquire a sema4.                          */
/*  Input(s)        : pSemId         - The sema4 Id.                    */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSemTake (tOsixSemId SemId)
{
    if (NU_Obtain_Semaphore (SemId, NU_SUSPEND) != NU_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing message queues */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixQueCrt                                        */
/*  Description     : Creates a OSIX Q.                                 */
/*  Input(s)        : au1name[ ] - The Name of the Queue.               */
/*                  : u4MaxMsgs  - Max messages that can be held.       */
/*                  : u4MaxMsgLen- Max length of a messages             */
/*  Output(s)       : pQueId     - The QId returned.                    */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueCrt (UINT1 au1Name[], UINT4 u4MaxMsgLen,
            UINT4 u4MaxMsgs, tOsixQId * pQueId)
{
    tOsixQue           *pQue;
    UINT4               u4QStartAddr;
    UINT4               u4QSize;

    /* For queue, the nucleus version of OsixRscAdd does not use */
    /* the last argument. So anything can be passed; we pass 0.  */
    if (OsixRscAdd (au1Name, OSIX_QUE, 0) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    /* Get the global queue id to return as the queue id       */
    /* Return value check not needed because we just added it. */
    OsixRscFind (au1Name, OSIX_QUE, (UINT4 *) pQueId);

    /* Allocate Memory for the Q Depth */
    /* Variable length messages require additional 4 Bytes per message */
    u4QSize = u4MaxMsgs * (u4MaxMsgLen + sizeof (UINT4));

    /* Allocate space for the queue.
     * 4 extra bytes to enforce proper word alignment.
     * i/e, word-aligment should round off to higher 4 byte boundary.
     */
    u4QStartAddr = (UINT4) MEM_MALLOC ((u4QSize + 4), UINT4);

    if (u4QStartAddr == 0)
    {
        OsixRscDel (OSIX_QUE, (UINT4) *pQueId);
        return (OSIX_FAILURE);
    }

    /* Align Q start address to word boundary as required by nucleus.
     * Round off should be done to higher 4 byte boundary hence the +3.
     */
    u4QStartAddr = (u4QStartAddr + 3) & 0xfffffffc;

    /* Initialize the Nucleus QCB */
    MEMSET (*pQueId, 0, sizeof (NU_QUEUE));

    /* Create the Nucleus Queue. */
    if (NU_Create_Queue
        ((NU_QUEUE *) * pQueId, (char *) au1Name, (VOID *) u4QStartAddr,
         (u4QSize / sizeof (UINT4)), NU_VARIABLE_SIZE,
         (u4MaxMsgLen / sizeof (UINT4)), NU_FIFO) != NU_SUCCESS)
    {
        OsixRscDel (OSIX_QUE, (UINT4) *pQueId);
        return (OSIX_FAILURE);
    }

    /* pQueId points to the first element of tOsixQue struct */
    pQue = (tOsixQue *) (*pQueId);

    /* Store the Memory pointer in the Qcb for release */
    pQue->u4QStartAddr = (UINT4) u4QStartAddr;

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueDel                                        */
/*  Description     : Deletes a Q.                                      */
/*  Input(s)        : QueId     - The QId returned.                     */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
void
OsixQueDel (tOsixQId QueId)
{
    NU_Delete_Queue ((NU_QUEUE *) QueId);
    OsixRscDel (OSIX_QUE, (UINT4) QueId);
    /*** The memory allocated for the Que, is released in OsixRscDel. ***/

    return;
}

/************************************************************************/
/*  Function Name   : OsixQueSend                                       */
/*  Description     : Sends a message to a Q.                           */
/*  Input(s)        : QueId -    The Q Id.                              */
/*                  : pu1Msg -   Pointer to message to be sent.         */
/*                  : u4MsgLen - length of the messages                 */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueSend (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen)
{
    if (NU_Send_To_Queue ((NU_QUEUE *) QueId, (void *) pu1Msg,
                          u4MsgLen / sizeof (UINT4),
                          NU_NO_SUSPEND) != NU_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueRecv                                       */
/*  Description     : Receives a message from a Q.                      */
/*  Input(s)        : QueId -     The Q Id.                             */
/*                  : u4MsgLen -  length of the messages                */
/*                  : i4Timeout - Time to wait in case of WAIT.         */
/*  Output(s)       : pu1Msg -    Pointer to message to be sent.        */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixQueRecv (tOsixQId QueId, UINT1 *pu1Msg, UINT4 u4MsgLen, INT4 i4Timeout)
{
    UINT4               u4MessageSize;

    if (i4Timeout > 0)
    {
        i4Timeout = (INT4) OsixSTUPS2Ticks ((UINT4) i4Timeout);
    }
    if (NU_Receive_From_Queue
        ((NU_QUEUE *) QueId, (void *) pu1Msg, u4MsgLen / sizeof (UINT4),
         &u4MessageSize, i4Timeout) != NU_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixQueNumMsgs                                    */
/*  Description     : Returns No. of messages currently in a Q.         */
/*  Input(s)        : QueId -     The Q Id.                             */
/*  Output(s)       : pu4NumberOfMsgs - Contains count upon return.     */
/*  Returns         : OSIX_SUCCESS                                      */
/************************************************************************/
UINT4
OsixQueNumMsg (tOsixQId QueId, UINT4 *pu4NumMsg)
{
    NU_TASK            *pTcb;
    UINT4              *pu4Dummy;
    UINT4               u4Dummy;
    CHR1                i1Dummy;
    if (NU_Queue_Information ((NU_QUEUE *) QueId, &i1Dummy,
                              (void *) &pu4Dummy, &u4Dummy, &u4Dummy,
                              pu4NumMsg, (OPTION *) & u4Dummy, &u4Dummy,
                              (OPTION *) & u4Dummy, &u4Dummy,
                              &pTcb) != NU_SUCCESS)
        return (OSIX_FAILURE);

    return (OSIX_SUCCESS);
}

/************************************************************************/
/* Routines for managing resources based on names       */
/************************************************************************/
/************************************************************************/
/*  Function Name   : OsixRscAdd                                        */
/*  Description     : Gets a free resouce, stores Resource-Id & Name    */
/*                  : This helps in locating Resource-Id by Name        */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
static UINT4
OsixRscAdd (UINT1 au1Name[], UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    u4RscId = u4RscId;            /* unused */

    if (OsixSemTake (&gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find a free slot */
            /* One more is for SCAVANGER task             */
            for (u4Idx = 1; u4Idx <= (OSIX_MAX_TSKS + 1); u4Idx++)
            {
                if ((gaOsixTsk[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixTsk[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixTsk[u4Idx].u4Events = 0;
                    MEMCPY (gaOsixTsk[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if ((gaOsixSem[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixSem[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixSem[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find a free slot */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if ((gaOsixQue[u4Idx].u2Free) == OSIX_TRUE)
                {
                    gaOsixQue[u4Idx].u2Free = OSIX_FALSE;
                    gaOsixQue[u4Idx].u2Filler = 0;
                    MEMCPY (gaOsixQue[u4Idx].au1Name, au1Name,
                            (OSIX_NAME_LEN + 4));
                    OsixSemGive (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }

    OsixSemGive (&gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : OsixRscDel                                        */
/*  Description     : Free an allocated resouce                         */
/*  Input(s)        : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*                  : u4RscId -   Resource-Id returned by OS            */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
static VOID
OsixRscDel (UINT4 u4RscType, UINT4 u4RscId)
{
    UINT4               u4Idx;

    if (OsixSemTake (&gOsixMutex) == OSIX_FAILURE)
    {
        return;
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            gaOsixTsk[u4RscId].u2Free = OSIX_TRUE;
            gaOsixTsk[u4RscId].u4Events = 0;
            MEMSET (gaOsixTsk[u4RscId].au1Name, '\0', (OSIX_NAME_LEN + 4));
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (&(gaOsixSem[u4Idx].SemId) == (NU_SEMAPHORE *) u4RscId)
                {
                    gaOsixSem[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixSem[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (&(gaOsixQue[u4Idx].QueueId) == (NU_QUEUE *) u4RscId)
                {
                    /***
                     * As tOsixQId is not an array-index, pointer to the memory
                     * to be freed can't be accessed.
                     * So MEM_FREE is done here 
                     ***/
                    MEM_FREE ((UINT1 *) gaOsixQue[u4Idx].u4QStartAddr);
                    gaOsixQue[u4Idx].u2Free = OSIX_TRUE;
                    MEMSET (gaOsixQue[u4Idx].au1Name, '\0',
                            (OSIX_NAME_LEN + 4));
                    break;
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (&gOsixMutex);
}

/************************************************************************/
/*  Function Name   : OsixRscFind                                       */
/*  Description     : This locates Resource-Id by Name                  */
/*  Input(s)        : au1Name -   Name of the resource                  */
/*                  : u4RscType - Type of resource (Task/Queue/Sema4)   */
/*  Output(s)       : pu4RscId - Osix Resource-Id                       */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixRscFind (UINT1 au1Name[], UINT4 u4RscType, UINT4 *pu4RscId)
{
    UINT4               u4Idx;

    if (STRCMP (au1Name, "") == 0)
    {
        return (OSIX_FAILURE);
    }
    if (OsixSemTake (&gOsixMutex) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    switch (u4RscType)
    {
        case OSIX_TSK:
            /* scan global task array to find the task */
            /* One more is for SCAVANGER task          */
            for (u4Idx = 1; u4Idx <= (OSIX_MAX_TSKS + 1); u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixTsk[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    /* For tasks applications know only our array index. */
                    *pu4RscId = u4Idx;
                    OsixSemGive (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_SEM:
            /* scan global semaphore array to find the semaphore */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_SEMS; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixSem[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = (UINT4) &(gaOsixSem[u4Idx].SemId);
                    OsixSemGive (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        case OSIX_QUE:
            /* scan global queue array to find the queue */
            for (u4Idx = 1; u4Idx <= OSIX_MAX_QUES; u4Idx++)
            {
                if (MEMCMP
                    (au1Name, gaOsixQue[u4Idx].au1Name,
                     (OSIX_NAME_LEN + 4)) == 0)
                {
                    *pu4RscId = (UINT4) &(gaOsixQue[u4Idx].QueueId);
                    OsixSemGive (&gOsixMutex);
                    return (OSIX_SUCCESS);
                }
            }
            break;

        default:
            break;
    }
    OsixSemGive (&gOsixMutex);
    return (OSIX_FAILURE);
}

/************************************************************************/
/*  Function Name   : Fsap2Start                                        */
/*  Description     : Function to be called to get any fsap2 application*/
/*                  : to work                                           */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Fsap2Start ()
{
    return;
}

/************************************************************************/
/*  Function Name   : OsixGetSysTime                                    */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysTime (tOsixSysTime * pSysTime)
{
    *pSysTime = (tOsixSysTime) NU_Retrieve_Clock ();
    *pSysTime = OsixTicks2STUPS (*pSysTime);
    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixGetSysUpTime                                  */
/*  Description     : Returns the system time in Seconds.               */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
UINT4
OsixGetSysUpTime (VOID)
{
    return (gu4Seconds);
}

/************************************************************************/
/*  Function Name   : OsixGetTps                                        */
/*  Description     : Returns the no of system ticks in a second        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : Number of system ticks in a second                */
/************************************************************************/
UINT4
OsixGetTps ()
{
    return (OSIX_TPS);
}

/************************************************************************/
/*  Function Name   : OsixExGetTaskName                                 */
/*  Description     : Get the OSIX task Name given the Osix Task-Id.    */
/*  Input(s)        : TaskId - The Osix taskId.                         */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer to OSIX task name                         */
/************************************************************************/
const UINT1        *
OsixExGetTaskName (tOsixTaskId TskId)
{
    /* To facilitate direct use of this call in a STRCPY,
     * we return a null string instead of a NULL pointer.
     * A null pointer is returned for all non-OSIX tasks
     * e.g., TMO's idle task.
     */
    if (TskId)
    {
        return ((UINT1 *) (gaOsixTsk[(UINT4) TskId].au1Name));
    }
    return ((const UINT1 *) "");
}

/************************************************************************/
/*  Function Name   : OsixScanForDelTask                                */
/*  Description     : Scans all the TCB ready for Task Deletion         */
/*  Input(s)        : Pointer to the TaskId                             */
/*  Output(s)       : None.                                             */
/*  Returns         :                                                   */
/************************************************************************/
static INT4
OsixScanForDelTask (VOID)
{
    INT4                i;

    OsixSemTake (&gOsixMutex);

    /* OSIX_MAX_TSKS,  +1  for SCAVENGER task */
    for (i = 1; i <= (OSIX_MAX_TSKS + 1); i++)
    {
        if (gaOsixTsk[i].u4DelFlag == OSIX_SET)
        {
            gaOsixTsk[i].u4DelFlag = OSIX_RESET;
            OsixSemGive (&gOsixMutex);
            return i;
        }
    }
    OsixSemGive (&gOsixMutex);
    return i;
}

/************************************************************************/
/*  Function Name   : OsixProcessDelScav                                */
/*  Description     : deletes the Scavenger TASK                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS /OSIX_FAILURE                        */
/************************************************************************/
static UINT4
OsixProcessDelScav ()
{
    NU_TASK            *pTsk;
    INT4                i4Idx;

    /* Get the Index of the Task to be deleted */
    i4Idx = OsixScanForDelTask ();

    /* One more is for SCAVENGER task */
    if (i4Idx > (OSIX_MAX_TSKS + 1))
    {
        return (OSIX_FAILURE);
    }

    OsixRscDel (OSIX_TSK, i4Idx);

    pTsk = &(gaOsixTsk[i4Idx].TaskId);

    /* Terminate the Task */
    if (NU_Terminate_Task (pTsk) != NU_SUCCESS)
    {
        return (OSIX_FAILURE);
    }

    /* Delete the Task */
    if (NU_Delete_Task (pTsk) != NU_SUCCESS)
    {
        return (OSIX_FAILURE);
    }

    /* Free the stack */
    MEM_FREE ((UINT1 *) gaOsixTsk[i4Idx].u4StackAddr);

    NU_Delete_Semaphore (&(gaOsixTsk[i4Idx].TskMutex));

    return (OSIX_SUCCESS);
}

/************************************************************************/
/*  Function Name   : OsixScavengerTask                                 */
/*  Description     : "Event-ually" deletes the task                    */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         :                                                   */
/************************************************************************/
static VOID
OsixScavengerTask (INT4 u4Dummy)
{
    tOsixTaskId         TskId;
    UINT4               u4RecdEvent;
    UINT4               u4rc;

    u4Dummy = u4Dummy;

    while (1)
    {
        if (OsixTskIdSelf (&TskId) == OSIX_FAILURE)
        {
            return;                /*OSIX_FAILURE */
        }

        u4rc =
            OsixEvtRecv (TskId, OSIX_SCAVENGER_EVENT, OSIX_WAIT, &u4RecdEvent);
        if ((!u4rc) && u4RecdEvent)
        {
            OsixProcessDelScav ();
        }
    }
}

/************************************************************************
 *  Function Name   : FileOpen
 *  Description     : Function to Open a file.
 *  Input           : pu1FileName - Name of file to open.
 *                    i4Mode      - whether r/w/rw.
 *  Returns         : FileDescriptor if successful
 *                    -1             otherwise.
 ************************************************************************/
INT4
FileOpen (const UINT1 *pu1FileName, INT4 i4Mode)
{
    return (-1);
}

/************************************************************************
 *  Function Name   : FileClose
 *  Description     : Function to close a file.
 *  Input           : i4Fd - File Descriptor to be closed.
 *  Returns         : 0 on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileClose (INT4 i4Fd)
{
    return (-1);
}

/************************************************************************
 *  Function Name   : FileRead
 *  Description     : Function to read a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer into which to read
 *                    i4Count - Number of bytes to read
 *  Returns         : Number of bytes read on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileRead (INT4 i4Fd, CHR1 * pBuf, UINT4 i4Count)
{
    return (0);
}

/************************************************************************
 *  Function Name   : FileWrite
 *  Description     : Function to write to a file.
 *  Input           : i4Fd - File Descriptor.
 *                    pBuf - Buffer from which to write
 *                    i4Count - Number of bytes to write
 *  Returns         : Number of bytes written on SUCCESS
 *                    -1   otherwise.
 ************************************************************************/
UINT4
FileWrite (INT4 i4Fd, const CHR1 * pBuf, UINT4 i4Count)
{
    return (0);
}

/************************************************************************
 *  Function Name   : FileDelete
 *  Description     : Function to delete a file.
 *  Input           : pu1FileName - Name of file to be deleted.
 *  Returns         : 0 on successful deletion.
 *                    -1   otherwise.
 ************************************************************************/
INT4
FileDelete (const UINT1 *pu1FileName)
{
    return (-1);
}

/************************************************************************/
/*  Function Name   : OsixSetLocalTime                                  */
/*  Description     : Obtains the system time and sets it in FSAP.      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSetLocalTime (void)
{
    return (OSIX_FAILURE);
}

/************************************************************************
 *  Function Name   : OsixGetMemInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4TotalMem - Total memory
 *                    pu4FreeMem - Free memory
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetMemInfo (UINT4 *pu4TotalMem, UINT4 *pu4FreeMem)
{
    *pu4TotalMem = 0;
    *pu4FreeMem = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetCPUInfo
 *  Description     : Function used to get the CPU utilization.
 *  Inputs          : None
 *  OutPuts         : pu4TotalUsage - Total Usage
 *                  : pu4CPUUsage - CPU Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetCPUInfo (UINT4 *pu4TotalUsage, UINT4 *pu4CPUUsage)
{
    *pu4TotalUsage = 0;
    *pu4CPUUsage = 0;
    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : OsixGetFlashInfo
 *  Description     : Function used to get the total memory and free
 *                    memory from the file '/proc/meminfo'.
 *  Inputs          : None
 *  OutPuts         : pu4FlashUsage - Flash Usage
 *  Returns         : OSIX_SUCCESS/OSIX_FAILURE
 ************************************************************************/
UINT4
OsixGetFlashInfo (UINT4 *pu4TotalUsage, UINT4 *pu4FreeFlash)
{
    *pu4TotalUsage = 0;
    *pu4FreeFlash = 0;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixSysRestart                                    */
/*  Description     : This function reboots the system.                 */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : OSIX_SUCCESS/OSIX_FAILURE                         */
/************************************************************************/
UINT4
OsixSysRestart ()
{
    /* Killing the current process for shutdown */
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : OsixGetCurEndDynMem                               */
/*  Description     : This function returns the pointer that points     */
/*                    to end of dynamic memory                          */
/*  Input(s)        : None.                                             */
/*  Output(s)       : None.                                             */
/*  Returns         : Pointer                                           */
/************************************************************************/
VOID               *
OsixGetCurEndDynMem ()
{
    return NULL;
}
