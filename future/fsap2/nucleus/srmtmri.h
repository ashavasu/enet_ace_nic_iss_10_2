/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: srmtmri.h,v 1.15 2015/04/28 12:15:47 siva Exp $
 *
 * Description: Timer Prototypes and Macros used by timer routines
 *
 *******************************************************************/

#ifndef SRM_TMRI_H
#define SRM_TMRI_H

#define TMR_FREE    0
#define TMR_USED    1

/************************************************************************
*                                                                       *
*                      Defines and Typedefs                             *
*                                                                       *
*************************************************************************/

/********************************************
*** Application Timer List Data Structure ***
*********************************************/
typedef struct TMO_APP_TIMER_LIST{
    tTMO_DLL    Link;
    INT4        i4RemainingTime;           /*** Time till next expiry ***/
    UINT4       TskId;                     /* Used to send Event */
    UINT4       u4Event;
    UINT4       u4Status;
    void        (*CallBackFunction)(tTimerListId);
    tOsixSemId  SemId;
   /* 
    * The List of timers which have expired.
    * To be read by the application.
    */
   tTMO_DLL       ExpdList;
}tTmrAppTimerList;

typedef tTmrAppTimerList tTimerList;

#define OSIX_TMR_TASK_PRIO 1
/* High priority for the timer task. */

/************************************************************************
*                                                                       *
*                          Macro                                        *
*                                                                       *
*************************************************************************/
#define TMR_MUTEX_NAME (const UINT1 *)"TMMU"
#define TMR_ENTER_CS() (OsixSemTake(TmrMutex))
#define TMR_LEAVE_CS() (OsixSemGive(TmrMutex))

#define TMR_DBG_FLAG gu4TmrDbg
#define TMR_MODNAME  "TMR"

#if DEBUG_TMR == ON
#define TMR_DBG(x) UtlTrcLog x
#else
#define TMR_DBG(x)

#endif
#define TMR_PRNT(x) UtlTrcPrint(x)

VOID  TmrDumpList(UINT4);
UINT4 TmrLock (VOID);
UINT4 TmrUnLock (UINT4 u4s);

#endif
