####################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# Description: makefile for Fsap2.
####################################################

### Replace the PRJ_BASE with your Project Base
### directory
PRJ_BASE = C:\Fsap2_TargetTest\future

### Assuming that cmn_make.mk is placed under
### the folder $(PRJ_BASE)/LR
COMMONMAKE = $(PRJ_BASE)$/fsap2$/apps2$/cmn_make.mk
include $(COMMONMAKE)

ALL = FutFsap.a

clean:
	-@$(RM) $(OBJS)
	-@$(RM) .$/FutFsap.o
	-@$(RM) .$/FutFsap.a

all .PHONY: $(ALL)

# Project/Module Object File(s) #######################
FSAP_OSIX_BASE_DIR = $(PRJ_BASE)$/fsap2$/ose
FSAP_CMN_BASE_DIR = $(PRJ_BASE)$/fsap2$/cmn


INC_CC = -I$(FSAP_OSIX_BASE_DIR) -I$(FSAP_CMN_BASE_DIR) \
         -I$(PRJ_BASE)\fsap2\apps2

CC_WARNING_FLAGS=-Wall -Wunused -Wmissing-declarations -Wimplicit\
 -Wwrite-strings -Wswitch -Wcast-qual -Wcast-align -Wshadow -Waggregate-return\
 -Wnested-externs -Wmissing-prototypes -fno-common -W

CC_FLAGS = ${CC_OPTIMIZATION_FLAGS} ${CC_WARNING_FLAGS} $(CC_DEFINES)


# FSAP object files
OBJS=$(FSAP_OSIX_BASE_DIR)\osixose.o\
 $(FSAP_OSIX_BASE_DIR)\osixwrap.o $(FSAP_OSIX_BASE_DIR)\srmtmr.o\
 $(FSAP_OSIX_BASE_DIR)\utltrc.o $(FSAP_CMN_BASE_DIR)\bufdebug.o\
 $(FSAP_CMN_BASE_DIR)\buflib.o $(FSAP_CMN_BASE_DIR)\fsapcm.o\
 $(FSAP_CMN_BASE_DIR)\srmifmsg.o\
 $(FSAP_CMN_BASE_DIR)\srmmem.o $(FSAP_CMN_BASE_DIR)\utlbit.o\
 $(FSAP_CMN_BASE_DIR)\utldll.o $(FSAP_CMN_BASE_DIR)\utleeh.o\
 $(FSAP_CMN_BASE_DIR)\utlhash.o $(FSAP_CMN_BASE_DIR)\utlsll.o

FutFsap.o: $(OBJS)
	$(ECHO) $(LD) $(LDFLAGS) $@ $(OBJS) $(ECHOEND)
	$(LD) -r -o $@ $(OBJS)
	-@$(RM) $(OBJS)

FutFsap.a: $(OBJS)
	$(AR) $(ARFLAGS) $@ $(OBJS)
	-@$(RM) $(OBJS)

%.o: %.c
	$(ECHO)Compile (CC)  $@ from $<$(ECHOEND)
	$(CC) $(CFLAGS) $(CC_DEBUG_FLAGS) $(CC_OPTIMIZATION_FLAGS) $(DEFINES) $(INCLUDE) $(CCOUT) $<
