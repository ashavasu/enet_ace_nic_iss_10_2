# Specify the nucleus OS Path
NU_PATH=C:\ati\mnt

# OSIX Source codes pertaining to nucleus
NU_DIR=.\nucleus

# COMMON Source codes
CMN_DIR=.\cmn


CPP=cl.exe

ALL : "fsap.lib"

CLEAN :
      -@erase "$(NU_DIR)\*.obj"
      -@erase "$(CMN_DIR)\*.obj"
      -@erase "fsap.lib"

CPP_PROJ=/nologo /MTd /W3 /Gm /GX /ZI /Od \
 /I "./nucleus" /I "./cmn" /I "$(NU_PATH)\Plus" /I "$(NU_PATH)\Hardware" \
 /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /Fp"$(NU_DIR)\nucleus.pch" \
 /YX /Fd"$(NU_DIR)\\" /FD /GZ /c 

    
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"fsap.lib" 
LIB32_OBJS= \
    "$(NU_DIR)\osixwrap.obj" "$(NU_DIR)\osixnu.obj"  \
    "$(NU_DIR)\srmtmr.obj" "$(NU_DIR)\utltrc.obj" \
    "$(CMN_DIR)\bufdebug.obj" "$(CMN_DIR)\buflib.obj" \
    "$(CMN_DIR)\srmifmsg.obj" "$(CMN_DIR)\utlsll.obj" "$(CMN_DIR)\srmmem.obj" \
    "$(CMN_DIR)\utlbit.obj" "$(CMN_DIR)\utldll.obj" "$(CMN_DIR)\utleeh.obj" \
    "$(CMN_DIR)\utlhash.obj" "$(CMN_DIR)\redblack.obj"


"fsap.lib" : $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<
    -@erase "$(NU_DIR)\*.obj"
      -@erase "$(CMN_DIR)\*.obj"

.c.obj:
        $(CPP) $(CPP_PROJ) $< /Fo"$@"

