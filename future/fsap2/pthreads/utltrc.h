/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utltrc.h,v 1.16 2017/12/07 10:07:34 siva Exp $
 *
 * Description:
 * This file contains the Macros
 * and definitions for tracing and
 * dumping, which forms a part of
 * the FutureUTL library.
 *
 */
#ifndef _UTLTRC_H
#define _UTLTRC_H

#include "osxstd.h"
#include "srmbuf.h"
#include "osxsys.h"
#include "utlmacro.h"

#define CONST  const

/* Constants relating to utltrc logging mechanism 
 * Calling UtlSetLogMode with CONSOLE_LOG causes traces 
 * to appear on the console.
 * Calling with INCORE_LOG causes traces to be accumulated
 * in a cyclic buffer in memory.
 */
enum eLoggingModes {
    CONSOLE_LOG = 1,
    INCORE_LOG,
    FLASH_LOG,
    SYSLOG,
    FLASH_LOG_LOCATION
};

/* Settings for INCORE logs.
 * UTL_MAX_LOGS is the size of the history buffer used in
 * the implementation. UTL_MAX_LOG_LEN is the maximum
 * length of each log message
 */
#define UTL_MAX_LOGS           100
#define UTL_MAX_LOG_LEN        200

/* System-wide constants */
#define  UTL_DBG_OFF           0x00000000
/* initial value for all module trace flags */

#define  UTL_DBG_ALL           0xffffffff
/* all debugs enabled */

#define  UTL_DBG_INIT_SHUTDN   0x00000001
/* for debugging module init and shutdown */

#define  UTL_DBG_CTRL_IF       0x00000002
/* for debugging of control messages got from other modules */

#define  UTL_DBG_STATUS_IF     0x00000004
/* for debugging status indications from other modules */

#define  UTL_DBG_SNMP_IF       0x00000008
/* for debugging of all SNMP messages */

#define  UTL_DBG_TMR_IF        0x00000010
/* for debugging timer start, stop and timeout */

#define  UTL_DBG_BUF_IF        0x00000020
/* for debugging buffer alloc, release and
 * major buffer operations done by this module */

#define  UTL_DBG_MEM_IF        0x00000040
/* for debugging memory pool creation & deletion
 * and memory alloc, release */

#define  UTL_DBG_RX            0x00000100
/* for debugging of received packets */

#define  UTL_DBG_TX            0x00000200
/* for debugging of transmitted packets */


#define  DUMP_DIRN_NONE        0x00000000
#define  DUMP_DIRN_NA          0x00000000
#define  DUMP_DIRN_INC         0x00000001
#define  DUMP_DIRN_OUT         0x00000002



/* Prototype Declarations */
#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif


#ifdef VAR_ARGS_SUPPORT
VOID UtlTrcLog   ARG_LIST ((UINT4 u4Flag, UINT4 u4Value, CONST char *pi1Name,
                            CONST char *pi1Fmt, ...));
VOID UtlSysTrcLog ARG_LIST ((UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value, CONST char *pi1ModId, CONST char *pi1Name,
                            CONST char *pi1Fmt, ...));
VOID UtlTrcPrintArg ARG_LIST ((CONST char*pi1Fmt, ...));

#else
VOID UtlTrcLog ();
VOID UtlSysTrcLog ();
#endif  /* VAR_ARGS_SUPPORT */

VOID UtlTrcPrint ARG_LIST ((const char *pi1Msg));

/* Functions to enable disable date/time display * in UtlTrc logs. */
VOID UtlShowDate (UINT4 u4Flag);
VOID UtlShowTime (UINT4 u4Flag);

/* Logging related functions. */
INT4  UtlGetLogs (CHR1 ac1Buf[], INT4 i4Count);
VOID  UtlSetLogMode (UINT4 u4val);
UINT4 UtlGetLogMode (VOID);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

#define UTL_THRESHOLD_VAL      80
#define ISS_CONFIG_FLASH_FILE_NAME_LEN     256
#define ISS_DATE_TIME_STR_LEN    24
#define ISS_DEBUG_TIMESTAMP_ENABLED        1
#define ISS_DEBUG_TIMESTAMP_DISABLED       2


VOID  UtlSetLogPathForFlash (UINT1*);
VOID  UtlGetLogPathForFlash (CHR1 ac1Buf[]);
VOID LoggerTaskMain (INT1 *);
VOID LoggerProcessEvent(VOID);
INT4 UtlGetLoggerTaskId(VOID);

#endif /* _UTLTRC_H */
