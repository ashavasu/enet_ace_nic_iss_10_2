/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: osxinc.h,v 1.14 2015/04/28 12:17:03 siva Exp $
 *
 * Description:
 * Header of all headers.
 *
 */

#include "osxstd.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/time.h>
#include <errno.h>
#include <stdint.h>


#ifdef MEM_FREE
#undef MEM_FREE
#endif

#include "srmmem.h"
#include "srmbuf.h"
#include "srmtmr.h"
#include "osxsys.h"

#include "utlmacro.h"
#include "utltrc.h"
#include "osxprot.h"

#include "utlsll.h"
#include "utlhash.h"

VOID UtlTrcClose (VOID);
