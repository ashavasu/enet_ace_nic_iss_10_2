/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: utltrc.c,v 1.21 2017/12/07 10:07:34 siva Exp $
 *
 * Description:
 * The UTL-TRC module.
 *
 */

#include "osxinc.h"
#include "utltrc.h"
#include "utltrci.h"
#include "osix.h"
#include <time.h>
#include <unistd.h>
#include "fsap.h"
#include "lr.h"
#include "fssyslog.h"

#define LOGGER_SEND_EVENT    OsixEvtSend
#define LOGGER_MSG_ENQ_EVENT          0x0002
#define LOGGER_LOG_PATH_CHG_EVENT     0x0004

#define ISS_LOG_BUFFER_SIZE           (UTL_MAX_LOGS * UTL_MAX_LOG_LEN)
#define LOGGER_TASK                   "LOGF"
#define LOGGER_GET_TASK_ID            OsixGetTaskId
#define LOGGER_TASK_ID                gLoggerTaskId
#define LOGGER_RECV_EVENT             OsixEvtRecv

static UINT1        gau1FlashFileName[ISS_CONFIG_FLASH_FILE_NAME_LEN + 1];
static UINT1        gau1FlashFilePath[ISS_CONFIG_FLASH_FILE_NAME_LEN + 1] =
    { 0 };
static INT4         gi4FlashLogFd = OSIX_FALSE;
static INT4         gu4IsFlashFileOpen = OSIX_FALSE;

static tOsixTaskId  gLoggerTaskId = 0;
UINT1              *gpu1BufferFlash;

static UINT4        gu4ShowTime = ISS_DEBUG_TIMESTAMP_DISABLED;
static INT4         gi4LogFd = OSIX_FILE_LOG;
static INT4         gu4InCoreLog = OSIX_LOG_METHOD;
static INT4         gu4IsFileOpen = OSIX_FALSE;
/* This structure is used to store the trace messages 
 * in an incore buffer. It is a cyclic buffer and so
 * wraps around. The API to get the logs is UtlGetLogs
 * and it supports getting the 'head' or 'tail' logs
 * in the unix like fashion.
 * i4Front - Points to the first (earliest) log
 * i4Rear  - Points to the one beyond the latest log.
 * u4Wrapped - Indicates if i4Rear has completed one round
 *             of the circular list. If so it is set and
 *             from then on i4Front also move along, indicative
 *             of the fact that the earliest log messages are
 *             being replaced by newer ones.
 */
struct tLogBuf
{

    CHR1                Log[UTL_MAX_LOGS][UTL_MAX_LOG_LEN];
    INT4                i4Front;
    INT4                i4Rear;
    UINT4               u4Wrapped;
};
static struct tLogBuf Logs;

/***************************************************************/
/*  Function Name   : UtlTrcPrint                              */
/*  Description     : Prints the input message                 */
/*                    Based on settings in osix.h it logs to   */
/*                    file. Also based on settings, the traces */
/*                    can be displayed on screen or made to    */
/*                    accumulate in an incore buffer           */
/*                    See OSIX_FILE_LOG and OSIX_LOG_METHOD    */
/*                    in osix.h                                */
/*  Input(s)        : pc1Msg - pointer to the message          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlTrcPrint (const CHR1 * pc1Msg)
{
    INT4                i4Len = 0;
    UINT1               au1LogFile[20];
    CHR1                ac1TmpBuf[80];
    tOsixMemStatus      HealthStatus;

    OsixGetMemoryStatus (&HealthStatus);
    if (((gi4LogFd == 1) && (gu4IsFileOpen == OSIX_FALSE))
        ||
        (((gu4InCoreLog == FLASH_LOG)
          || (HealthStatus.u1ErrorStatus == DOWN_NONRECOVERABLE_ERR)
          || (gu4InCoreLog == FLASH_LOG_LOCATION))
         && (gu4IsFileOpen == OSIX_FALSE)))

    {
        UTL_GET_LOG_FILE_NAME (au1LogFile);

        gi4LogFd = FileOpen (au1LogFile, OSIX_FILE_RW | OSIX_FILE_CR);

        if (gi4LogFd)
        {
            SNPRINTF (ac1TmpBuf, 80, "Creating log file %s\n", au1LogFile);

            /* Display on screen and put one copy in the log file */
            printf ("%s", ac1TmpBuf);
            gu4IsFileOpen = OSIX_TRUE;
            UtlTrcPrint (ac1TmpBuf);
        }
    }

    if ((gu4InCoreLog == INCORE_LOG) || (gu4InCoreLog == FLASH_LOG_LOCATION))
    {
        /* Do not print on screen. Only store in memory */
        i4Len = STRLEN (pc1Msg);
        MEMCPY (Logs.Log[Logs.i4Rear], pc1Msg,
                (i4Len > UTL_MAX_LOG_LEN ? UTL_MAX_LOG_LEN : i4Len));

        /* Null terminate it. If the string is of length UTL_MAX_LOG_LEN,
         * then the last character will be lost as this will be used to
         * store the '\0' byte. Since we deal with array indexes rather
         * than no. of bytes, the relational expression is slightly
         * different (i.e., we use '>=' ).
         */
        Logs.Log[Logs.i4Rear]
            [i4Len >= UTL_MAX_LOG_LEN ? (UTL_MAX_LOG_LEN - 1) : i4Len] = 0;

        Logs.i4Rear++;
        if (Logs.u4Wrapped)
        {
            Logs.i4Front++;
        }

        if (Logs.i4Rear == UTL_MAX_LOGS)
        {
            Logs.i4Rear = 0;
            Logs.u4Wrapped = 1;
        }
        if (Logs.i4Front == UTL_MAX_LOGS)
        {
            Logs.i4Front = 0;
        }
        if (Logs.i4Rear == (UTL_MAX_LOGS * UTL_THRESHOLD_VAL / 100))
        {
            LOGGER_SEND_EVENT (UtlGetLoggerTaskId (), LOGGER_MSG_ENQ_EVENT);
        }

    }
    else if (gu4InCoreLog == CONSOLE_LOG)
    {
        /* print to screen */
        i4Len = printf ("%s", pc1Msg);
    }

    if (((gu4InCoreLog == FLASH_LOG) || (gu4InCoreLog == FLASH_LOG_LOCATION) ||
         (HealthStatus.u1ErrorStatus == DOWN_NONRECOVERABLE_ERR))
        && (gi4LogFd > 1))
    {
        i4Len = STRLEN (pc1Msg);
        FileWrite (gi4LogFd, pc1Msg, i4Len);
    }
}

/***************************************************************/
/*  Function Name   : UtlSetLogMode                            */
/*  Description     : API to dynamically change logging        */
/*                    mechanism.                               */
/*                    The default logging mechanism is defined */
/*                    in osix.h (OSIX_LOG_METHOD)              */
/*                    By default this is set to CONSOLE_LOG    */
/*                    so that the traces appear on screen      */
/*                    Other option is INCORE_LOG which switches*/
/*                    to logging the traces in a circular buf  */
/*  Input(s)        : u4val - Logging mode to be set.          */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlSetLogMode (UINT4 u4val)
{
    gu4InCoreLog = u4val;
    if (gu4InCoreLog == CONSOLE_LOG)
    {
        gu4IsFileOpen = OSIX_FALSE;
    }
}

/***************************************************************/
/*  Function Name   : UtlGetLogMode                            */
/*  Description     : API to get the current logging mechanism */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None                                     */
/*  Returns         : One of eLoggingModes.                    */
/***************************************************************/
UINT4
UtlGetLogMode (VOID)
{
    return (gu4InCoreLog);
}

/***************************************************************/
/*  Function Name   : UtlSetLogPathForFlash                    */
/*  Description     : API to set the current logging path for  */
/*                    flash                                    */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None                                     */
/*  Returns         : One of eLoggingModes.                    */
/***************************************************************/
VOID
UtlSetLogPathForFlash (UINT1 *pu1FlashLoggingFileName)
{
    if (STRCMP (gau1FlashFilePath, pu1FlashLoggingFileName) != 0)
    {
        if (STRLEN (pu1FlashLoggingFileName) >= sizeof (gau1FlashFilePath))
        {
            STRNCPY (gau1FlashFilePath, pu1FlashLoggingFileName,
                     sizeof (gau1FlashFilePath) - 1);
            gau1FlashFilePath[sizeof (gau1FlashFilePath) - 1] = '\0';

        }
        else
        {
            STRCPY (gau1FlashFilePath, pu1FlashLoggingFileName);
        }
        LOGGER_SEND_EVENT (UtlGetLoggerTaskId (), LOGGER_LOG_PATH_CHG_EVENT);
    }
}

/***************************************************************/
/*  Function Name   : UtlGetLogPathForFlash                    */
/*  Description     : API to set the current logging path for  */
/*                    flash                                    */
/*  Input(s)        : None.                                    */
/*  Output(s)       : None                                     */
/*  Returns         : One of eLoggingModes.                    */
/***************************************************************/
VOID
UtlGetLogPathForFlash (CHR1 ac1Buf[])
{
    STRCPY (ac1Buf, gau1FlashFilePath);
}

/***************************************************************/
/*  Function Name   : UtlGetLogs                               */
/*  Description     : API to get retrieve the logs.            */
/*                    It returns the number of bytes copied.   */
/*                    The buffer ac1Buf should be sufficiently */
/*                    large to prevent overruns.               */
/*                    The size should be based on MAX_LOG_LEN  */
/*                    i4Count and UTL_MAX_LOGS                */
/*  Input(s)        : ac1Buf - Buffer into which to copy logs. */
/*                    i4Count - Number of logs to copy         */
/*                     i4Count < 0 => retrieve the last        */
/*                                    i4Count logs.            */
/*                     i4Count > 0 => retrieve the earliest    */
/*                                    i4Count logs.            */
/*                     i4Count = 0 => retrieve all the logs    */
/*                                    that are available.      */
/*  Output(s)       : Filled ac1Buf.                           */
/*  Returns         : Number of bytes copied to ac1Buf.        */
/***************************************************************/
INT4
UtlGetLogs (CHR1 ac1Buf[], INT4 i4Count)
{
    INT4                i4Len;
    INT4                i4Offset = 0;
    INT4                i4Idx = Logs.i4Front;

    if (i4Count < 0)
    {
        i4Count *= -1;
        i4Count = (i4Count > UTL_MAX_LOGS ? UTL_MAX_LOGS : i4Count);
        i4Idx = Logs.i4Rear - i4Count;
        if (i4Idx < 0)
        {
            i4Idx = UTL_MAX_LOGS + i4Idx;
        }

    }

    if (i4Count == 0)
    {
        i4Idx = Logs.i4Front;
        if (Logs.i4Rear == Logs.i4Front)
        {
            i4Count = UTL_MAX_LOGS;
        }
        else
        {
            i4Count = Logs.i4Rear - Logs.i4Front;
        }
    }

    i4Count = (i4Count > UTL_MAX_LOGS ? UTL_MAX_LOGS : i4Count);

    for (; i4Count; i4Count--)
    {
        i4Len = STRLEN (Logs.Log[i4Idx]);
        MEMCPY (ac1Buf + i4Offset, Logs.Log[i4Idx], i4Len);
        i4Offset += i4Len;

        i4Idx++;
        if (i4Idx == UTL_MAX_LOGS)
        {
            i4Idx = 0;
        }
    }

    return (i4Offset);
}

/***************************************************************/
/*  Function Name   : UtlTrcLog                                */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST char *pi1Name,
           CONST char *pi1Fmt, ...)
{
    va_list             VarArgList;
    va_list             VarArgListA;
    char                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST char         *pi1FmtString;
    INT4                i4ArgCount;
    INT4                pos = 0;
    INT4                i4val;
    char               *pNullPtr = 0;

    /* unused variable */
    pNullPtr = pNullPtr;

    /* Is Tracing for this type enabled */
    if (!(u4Flag & u4Value))
    {
        return;
    }

    /* Crash the program if module name is (null). */
#if (DEBUG_UTL == FSAP_ON)
    if (pi1Name == NULL)
    {
        *pNullPtr = 0;
    }
#endif

    pi1FmtString = pi1Fmt;
    i4ArgCount = 0;

    va_start (VarArgList, pi1Fmt);
    va_start (VarArgListA, pi1Fmt);

    /* Count the number of arguments and perform sanity
     * checks such as ensuring the parameter corresponding
     * to %s is a non-null pointer
     */
    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                /* We assume fmt-spec is one of csdu,
                 * or any other 4 byte quantity.. 
                 * As per ANSI-C, char is to be
                 * treated as int when used in varargs.
                 */
                i4val = (INT4) va_arg (VarArgList, int);

                i4ArgCount++;

                /* Is arg to %s is a (null). */
#if (DEBUG_UTL == FSAP_ON)
                if (*pi1FmtString == 's' && i4val == 0)
                {
                    /* For the case of TMO, we crash the program 
                     * to help to locate-and-fix the problem,
                     * using gdb's backtrace.
                     * For RTOS like environments, it'd be
                     * better to merely return without crashing
                     * the program.
                     */
                    *pNullPtr = 0;
                }
#else
                UNUSED_PARAM (i4val);
#endif

            }
        }
        pi1FmtString++;
    }

#if (DEBUG_UTL == FSAP_ON)
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }
#endif

    UTL_TRC_LOG_ADD_MOD_NAME (pi1Name, pos, ai1LogMsgBuf);
    vsnprintf (ai1LogMsgBuf + pos, (MAX_LOG_STR_LEN - pos), pi1Fmt,
               VarArgListA);

    UTL_TRC_LOG_MSG (pi1Name, ai1LogMsgBuf);

    va_end (VarArgList);
    va_end (VarArgListA);
}
#else /* VAR_ARGS_SUPPORT */
VOID
UtlTrcLog (UINT4 u4Flag, UINT4 u4Value, CONST INT1 *pi1Name,
           CONST INT1 *pi1Fmt, INT4 p1, INT4 p2, INT4 p3,
           INT4 p4, INT4 p5, INT4 p6)
{
    INT4                i4ArgCount;
    INT4                i4p[6];
    INT4                i4val;
    INT1                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    INT1                ai1Spec[6];
    CONST INT1         *pi1FmtString;
    char               *pNullPtr = 0;

    /* unused variable */
    *pNullPtr = *pNullPtr;

    i4p[0] = p1;
    i4p[1] = p2;
    i4p[2] = p3;
    i4p[3] = p4;
    i4p[4] = p5;
    i4p[5] = p6;

    /* Is Tracing for this type enabled. */
    if (!(u4Flag & u4Value))
    {
        return;
    }

    /* Crash the program if module name is (null). */
#if (DEBUG_UTL == FSAP_ON)
    if (pi1Name == NULL)
    {
        *pNullPtr = 0;
    }
#endif

    pi1FmtString = pi1Fmt;

    i4ArgCount = 0;

    /* Count the number of arguments and perform sanity
     * checks such as ensuring the parameter corresponding
     * to %s is a non-null pointer
     */
    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                /* We assume fmt-spec is one of csdu,
                 * or any other 4 byte quantity. 
                 * As per ANSI-C, char is to be
                 * treated as int when used in varargs.
                 */
                i4val = i4p[i4ArgCount];

                i4ArgCount++;

                /* Is arg to %s is a (null). */
#if (DEBUG_UTL == FSAP_ON)
                if (*pi1FmtString == 's' && i4val == 0)
                {
                    /* For the case of TMO, we crash the program 
                     * to help to locate-and-fix the problem,
                     * using gdb's backtrace.
                     * For RTOS like environments, it'd be
                     * better to merely return without crashing
                     * the program.
                     */
                    *pNullPtr = 0;
                }
#endif
            }
        }
        pi1FmtString++;
    }

#if (DEBUG_UTL == FSAP_ON)
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }
#endif

    UTL_TRC_LOG_ADD_MOD_NAME (pi1Name, pos, ai1LogMsgBuf);

    switch (i4ArgCount)
    {
        case 0:
            SPRINTF (ai1LogMsgBuf, pi1Fmt);
            break;
        case 1:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1);
            break;
        case 2:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2);
            break;
        case 3:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3);
            break;
        case 4:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4);
            break;
        case 5:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5);
            break;
        case 6:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5, p6);
            break;
        default:
            SPRINTF (ai1LogMsgBuf, "Number of arguments > 6.\n");
            break;
    }

    UTL_TRC_LOG_MSG (pi1Name, ai1LogMsgBuf);
}
#endif /* VAR_ARGS_SUPPORT */

/***************************************************************/
/*  Function Name   : UtlSysTrcLog                             */
/*  Description     : This fuction will print the trace message*/
/*                    after checking the flag against the      */
/*                    value.                                   */
/*  Input(s)        : u4SysLevel - Syslog message levels       */
/*                    u4Flag - Current value of the trace flag */
/*                    u4Value - Value against which the flag is*/
/*                    to be compared.                          */
/*                    pi1Name - Name of te invoking Module     */
/*                    pi1Fmt - Format String                   */
/*                    Other arguments as per the format string */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
#ifdef VAR_ARGS_SUPPORT
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value,
              CONST char *pi1ModId, CONST char *pi1Name, CONST char *pi1Fmt,
              ...)
{
    va_list             VarArgList;
    va_list             VarArgListA;
    char                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST char         *pi1FmtString;
    INT4                i4ArgCount;
    INT4                pos = 0;
    INT4                i4val;
    char               *pNullPtr = 0;

    /* unused variable */
    pNullPtr = pNullPtr;

    /* Is Tracing for this type enabled */
    if (!(u4Flag & u4Value))
    {
        return;
    }

    /* Crash the program if module name is (null). */
#if (DEBUG_UTL == FSAP_ON)
    if (pi1Name == NULL)
    {
        *pNullPtr = 0;
    }
#endif

    pi1FmtString = pi1Fmt;
    i4ArgCount = 0;

    va_start (VarArgList, pi1Fmt);
    va_start (VarArgListA, pi1Fmt);

    /* Count the number of arguments and perform sanity
     * checks such as ensuring the parameter corresponding
     * to %s is a non-null pointer
     */
    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                /* We assume fmt-spec is one of csdu,
                 * or any other 4 byte quantity.. 
                 * As per ANSI-C, char is to be
                 * treated as int when used in varargs.
                 */
                i4val = (INT4) va_arg (VarArgList, int);

                i4ArgCount++;

                /* Is arg to %s is a (null). */
#if (DEBUG_UTL == FSAP_ON)
                if (*pi1FmtString == 's' && i4val == 0)
                {
                    /* For the case of TMO, we crash the program 
                     * to help to locate-and-fix the problem,
                     * using gdb's backtrace.
                     * For RTOS like environments, it'd be
                     * better to merely return without crashing
                     * the program.
                     */
                    *pNullPtr = 0;
                }
#else
                UNUSED_PARAM (i4val);
#endif

            }
        }
        pi1FmtString++;
    }

#if (DEBUG_UTL == FSAP_ON)
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }
#endif

    UtlShowTime (u4Flag);
    UTL_TRC_LOG_ADD_MOD_NAME (pi1Name, pos, ai1LogMsgBuf);
    vsprintf (ai1LogMsgBuf + pos, pi1Fmt, VarArgListA);

    if (u4SysLevel != SYSLOG_INVAL_LEVEL)
    {
        UTL_SYS_TRC_LOG_MSG (u4SysLevel, pi1ModId, ai1LogMsgBuf);
    }
    else
    {
        UtlTrcPrint ((const char *) ai1LogMsgBuf);
    }

    UNUSED_PARAM (pi1ModId);
    va_end (VarArgList);
    va_end (VarArgListA);
}
#else /* VAR_ARGS_SUPPORT */
VOID
UtlSysTrcLog (UINT4 u4SysLevel, UINT4 u4Flag, UINT4 u4Value,
              CONST INT1 *pi1ModId, CONST INT1 *pi1Name, CONST INT1 *pi1Fmt,
              INT4 p1, INT4 p2, INT4 p3, INT4 p4, INT4 p5, INT4 p6)
{
    INT4                i4ArgCount;
    INT4                i4p[6];
    INT4                i4val;
    INT1                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    INT1                ai1Spec[6];
    CONST INT1         *pi1FmtString;
    char               *pNullPtr = 0;

    /* unused variable */
    *pNullPtr = *pNullPtr;

    i4p[0] = p1;
    i4p[1] = p2;
    i4p[2] = p3;
    i4p[3] = p4;
    i4p[4] = p5;
    i4p[5] = p6;

    /* Is Tracing for this type enabled. */
    if (!(u4Flag & u4Value))
    {
        return;
    }

    /* Crash the program if module name is (null). */
#if (DEBUG_UTL == FSAP_ON)
    if (pi1Name == NULL)
    {
        *pNullPtr = 0;
    }
#endif

    pi1FmtString = pi1Fmt;

    i4ArgCount = 0;

    /* Count the number of arguments and perform sanity
     * checks such as ensuring the parameter corresponding
     * to %s is a non-null pointer
     */
    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                /* We assume fmt-spec is one of csdu,
                 * or any other 4 byte quantity. 
                 * As per ANSI-C, char is to be
                 * treated as int when used in varargs.
                 */
                i4val = i4p[i4ArgCount];

                i4ArgCount++;

                /* Is arg to %s is a (null). */
#if (DEBUG_UTL == FSAP_ON)
                if (*pi1FmtString == 's' && i4val == 0)
                {
                    /* For the case of TMO, we crash the program 
                     * to help to locate-and-fix the problem,
                     * using gdb's backtrace.
                     * For RTOS like environments, it'd be
                     * better to merely return without crashing
                     * the program.
                     */
                    *pNullPtr = 0;
                }
#endif
            }
        }
        pi1FmtString++;
    }

#if (DEBUG_UTL == FSAP_ON)
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }
#endif

    UTL_TRC_LOG_ADD_MOD_NAME (pi1Name, pos, ai1LogMsgBuf);

    switch (i4ArgCount)
    {
        case 0:
            SPRINTF (ai1LogMsgBuf, pi1Fmt);
            break;
        case 1:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1);
            break;
        case 2:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2);
            break;
        case 3:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3);
            break;
        case 4:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4);
            break;
        case 5:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5);
            break;
        case 6:
            SPRINTF (ai1LogMsgBuf, pi1Fmt, p1, p2, p3, p4, p5, p6);
            break;
        default:
            SPRINTF (ai1LogMsgBuf, "Number of arguments > 6.\n");
            break;
    }

    UtlShowTime (u4Flag);
    if (u4SysLevel != SYSLOG_INVAL_LEVEL)
    {
        UTL_SYS_TRC_LOG_MSG (u4SysLevel, pi1ModId, ai1LogMsgBuf);
    }
    else
    {
        UtlTrcPrint ((const char *) ai1LogMsgBuf);
    }
    UNUSED_PARAM (pi1ModId);
}
#endif /* VAR_ARGS_SUPPORT */

/***************************************************************/
/*  Function Name   : UtlShowTime                              */
/*  Description     : Used to enable/disable display of time.  */
/*  Input(s)        : u4Flag - A boolean flag.                 */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UtlShowTime (UINT4 u4Flag)
{
    gu4ShowTime = u4Flag;
}

VOID
UtlTrcClose (VOID)
{
    if (gi4LogFd > 1)
    {
        FileClose (gi4LogFd);
        gi4LogFd = OSIX_FILE_LOG;
        gu4IsFileOpen = OSIX_FALSE;
    }
}

#ifdef VAR_ARGS_SUPPORT
VOID
UtlTrcPrintArg (CONST char *pi1Fmt, ...)
{
    va_list             VarArgList;
    va_list             VarArgListA;
    char                ai1LogMsgBuf[MAX_LOG_STR_LEN];
    CONST char         *pi1FmtString;
    INT4                i4ArgCount;
    char               *pNullPtr = 0;
    /* unused variable */
    pNullPtr = pNullPtr;

    pi1FmtString = pi1Fmt;
    i4ArgCount = 0;

    va_start (VarArgList, pi1Fmt);
    va_start (VarArgListA, pi1Fmt);

    /* Count the number of arguments and perform sanity
     * checks such as ensuring the parameter corresponding
     * to %s is a non-null pointer
     */
    while (*pi1FmtString)
    {
        if (*pi1FmtString == '%')
        {
            pi1FmtString++;
            if (*pi1FmtString != '%')
            {
                /* We assume fmt-spec is one of csdu,
                 * or any other 4 byte quantity..
                 * As per ANSI-C, char is to be
                 * treated as int when used in varargs.
                 */
                va_arg (VarArgList, int);

                i4ArgCount++;

                /* Is arg to %s is a (null). */
#if (DEBUG_UTL == FSAP_ON)
                if (*pi1FmtString == 's' && i4val == 0)
                {
                    /* For the case of TMO, we crash the program
                     * to help to locate-and-fix the problem,
                     * using gdb's backtrace.
                     * For RTOS like environments, it'd be
                     * better to merely return without crashing
                     * the program.
                     */
                    *pNullPtr = 0;
                }
#endif

            }
        }
        pi1FmtString++;
    }

#if (DEBUG_UTL == FSAP_ON)
    if (i4ArgCount > 6)
    {
        UtlTrcPrint ("Number of arguments > 6.\n");
        return;
    }
#endif

    vsprintf (ai1LogMsgBuf, pi1Fmt, VarArgListA);

    UTL_TRC_LOG_MSG ("Np", ai1LogMsgBuf);

    va_end (VarArgList);
    va_end (VarArgListA);
}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : LoggerProcessEvent                               */
/*                                                                          */
/*    Description        : This is the process the even from LoggerTaskMain */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
LoggerProcessEvent (VOID)
{
    CHR1                ac1TmpBuf[ISS_CONFIG_FLASH_FILE_NAME_LEN];
    CHR1                ac1DateTime[ISS_DATE_TIME_STR_LEN];
    INT4                i4LogSize = 0;
    static UINT1        au1Temp[ISS_LOG_BUFFER_SIZE];

    if ((gi4FlashLogFd == 0)
        || ((UtlGetLogMode () == FLASH_LOG_LOCATION)
            && (gu4IsFlashFileOpen == OSIX_FALSE)))
    {
        UtlGetTimeStr (ac1DateTime);
        UtlGetLogPathForFlash (ac1TmpBuf);

        if (STRCMP (gau1FlashFileName, ac1TmpBuf) != 0)
        {
            if (gi4FlashLogFd)
            {
                UtlTrcPrint ("Closing the flash debug file\n");
                FileClose (gi4FlashLogFd);
            }
        }

        if (STRCMP (ac1TmpBuf, "") == 0)
        {
            SNPRINTF ((CHR1 *) gau1FlashFileName,
                      ISS_CONFIG_FLASH_FILE_NAME_LEN, "Log_%s", ac1DateTime);
        }
        else
        {
            SNPRINTF ((CHR1 *) gau1FlashFileName,
                      ISS_CONFIG_FLASH_FILE_NAME_LEN, "%s/Log_%s", ac1TmpBuf,
                      ac1DateTime);
            MEMSET (ac1TmpBuf, '\0', 80);
        }

        gi4FlashLogFd =
            FileOpen (gau1FlashFileName, OSIX_FILE_RW | OSIX_FILE_CR);

        if (gi4FlashLogFd)
        {
            SNPRINTF (ac1TmpBuf, ISS_CONFIG_FLASH_FILE_NAME_LEN,
                      "Creating log file for flash %s\n", gau1FlashFileName);
            gu4IsFlashFileOpen = OSIX_TRUE;
        }
        else
        {
            SNPRINTF (ac1TmpBuf, ISS_CONFIG_FLASH_FILE_NAME_LEN,
                      "Error: Creating log file for flash %s failed\n",
                      gau1FlashFileName);
        }
        UtlTrcPrint (ac1TmpBuf);
        printf ("%s\n", ac1TmpBuf);
    }
    else
    {
        /* Write the incore buffer to flash log */
        MEMSET (au1Temp, 0, ISS_LOG_BUFFER_SIZE);
        gpu1BufferFlash = &au1Temp[0];
        /* The size of gpu1StrFlash should be > (UTL_MAX_LOGS*UTL_MAX_LOG_LEN) */
        i4LogSize = UtlGetLogs ((CHR1 *) gpu1BufferFlash, UTL_MAX_LOGS);

        if (gi4FlashLogFd && i4LogSize > 0)
        {
            if (FileStat ((CHR1 *) gau1FlashFileName) == OSIX_SUCCESS)
            {
                FileWrite (gi4FlashLogFd, (CHR1 *) gpu1BufferFlash, i4LogSize);
            }
            else
            {
                FileClose (gi4FlashLogFd);
                gi4FlashLogFd = 0;
                gu4IsFlashFileOpen = OSIX_FALSE;
                SNPRINTF (ac1TmpBuf,
                          ISS_CONFIG_FLASH_FILE_NAME_LEN,
                          "Error: Writing to flash file %s failed, dropping log buffer !!!\n",
                          gau1FlashFileName);
                printf ("%s\n", ac1TmpBuf);
            }
        }
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LoggerTaskMain                                   */
/*                                                                          */
/*    Description        : This is the main function of the logger task     */
/*                                                                          */
/*    Input(s)           : pi1Param - dummy parameter                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
LoggerTaskMain (INT1 *pi1Param)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pi1Param);
    if (LOGGER_GET_TASK_ID (SELF, (UINT1 *) LOGGER_TASK, (&gLoggerTaskId)) ==
        OSIX_FAILURE)
    {
        UtlTrcPrint ("Error in getting Task Id\n");
    }

    /* MSR main wait loop : checks for receipt of any events */
    while (1)
    {
        if ((LOGGER_RECV_EVENT
             (LOGGER_TASK_ID,
              (LOGGER_MSG_ENQ_EVENT | LOGGER_LOG_PATH_CHG_EVENT), OSIX_WAIT,
              &u4Events)) == OSIX_SUCCESS)
        {

            if (u4Events & LOGGER_MSG_ENQ_EVENT)
            {
                LoggerProcessEvent ();
            }
            if (u4Events & LOGGER_LOG_PATH_CHG_EVENT)
            {
                if (gi4FlashLogFd)
                {
                    UtlTrcPrint ("Closing the flash debug file\n");
                    FileClose (gi4FlashLogFd);
                    gi4FlashLogFd = 0;
                    gu4IsFlashFileOpen = OSIX_FALSE;
                }
            }
        }
    }
    return;
}

INT4
UtlGetLoggerTaskId (VOID)
{
    return gLoggerTaskId;
}
