/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcli.c,v 1.154 2017/12/13 10:47:40 siva Exp $
 *
 * Description: Action routines of STDIP module specific commands
 *
 *******************************************************************/
#ifndef __IPCLI_C__
#define __IPCLI_C__
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "rtm.h"
#include "ipcli.h"
#include "vcm.h"
#include "stdipwr.h"
#include "stdiplow.h"
#include "fsrtmwr.h"
#include "fsrtmlw.h"
#include  "fsmirtlw.h"
#include "rtmdefns.h"
#include "rmap.h"
#include "rtmtdfs.h"
#include "rtmfrt.h"
#include "iss.h"
#include "ipvx.h"
#include "ip4ipvx.h"
#include "arp.h"
#include "bgp.h"
#include "snmputil.h"
#ifdef IP_WANTED
#include "ipprotos.h"
#include "icmptyps.h"
#include "fsiplow.h"
#include "fsmpiplw.h"
#endif
#ifdef ICCH_WANTED
#include "icch.h"
#endif
#define IP_GET_IP_ADDR(au1Str,OctStr)\
{\
    SPRINTF ((CHR1 *)au1Str, "%d.%d.%d.%d", OctStr[0], OctStr[1],\
             OctStr[2], OctStr[3]);\
}
#define ROUTE_INFO  sizeof (tRtInfo) * (MAX_ROUTING_PROTOCOLS + 1)

PRIVATE tRtInfo     gaRouteInfo[ROUTE_INFO];

PRIVATE VOID        IpObtainProtoType (INT4 i4Proto, INT4 i4ProtoType,
                                       INT1 *pi1Proto);
extern UINT1        gu1EcmpAcrossProtocol;

INT4
cli_process_stdip_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[IP_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT1               u1VlanIdArgIndex = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT1              *pu1IpCxtName = NULL;
    UINT4               u4VcId = 0;
    INT4                i4ShowAllVlan = FALSE;
    UINT1               au1IfName[IF_PREFIX_LEN];
    UINT4               u4IfIndex = 0;
    UINT4               u4NextIfIndex = 0;

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Fourth argument is ContextName */
    pu1IpCxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     * Store IP_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == IP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

#ifdef IP_WANTED
    /* Lock need not be taken for LNXIP as no data stuctures are used in the 
       functions called below. Care should be taken when the data structures 
       are accessed in future. */
    CliRegisterLock (CliHandle, IpFwdProtocolLock, IpFwdProtocolUnlock);
    IPFWD_PROT_LOCK ();
#endif /* IP_WANTED */

    /*Get context Id from context Name */
    if (pu1IpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1IpCxtName, &u4VcId) != VCM_TRUE)
        {
            i4RetStatus = CLI_FAILURE;
        }
    }
    else
    {
        u4VcId = IP_DEFAULT_CONTEXT;
    }
    if (i4RetStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
#ifdef IP_WANTED
        IPFWD_PROT_UNLOCK ();
        CliUnRegisterLock (CliHandle);
#endif
        return CLI_FAILURE;
    }

    if (UtilIpSetCxt (u4VcId) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
#ifdef IP_WANTED
        IPFWD_PROT_UNLOCK ();
        CliUnRegisterLock (CliHandle);
#endif
        return CLI_FAILURE;
    }

    switch (u4Command)
    {
        case CLI_IP_INTERFACE:
            i4IfaceIndex = CLI_GET_IFINDEX ();
#ifdef IP_WANTED
            IPFWD_PROT_UNLOCK ();
#endif
            i4RetStatus =
                IpSetInterfaceAdminStatus (CliHandle, (UINT4) i4IfaceIndex,
                                           (INT4) CLI_ATOI (args[IP_ZERO]));
#ifdef IP_WANTED
            IPFWD_PROT_LOCK ();
#endif
            break;

        case CLI_IP_SHOW_TRAFFIC:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
#ifdef IP_WANTED
            IPFWD_PROT_UNLOCK ();
#endif
            if (i4IfaceIndex == IP_MINUS_ONE)
            {
                i4ShowAllVlan = CLI_PTR_TO_I4 (args[IP_ONE]);

                if (i4ShowAllVlan == TRUE)
                {
                    u1VlanIdArgIndex = IP_ONE;
                    u1VlanIdArgIndex++;
                    MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                    SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d", "vlan",
                              *(INT4 *) args[u1VlanIdArgIndex]);

                    /* Print All the IP interfaces with
                     * IfAlias as "au1IfName"
                     */
                    if (CfaCliGetFirstIfIndexFromName
                        (au1IfName, &u4NextIfIndex) != OSIX_FAILURE)
                    {
                        do
                        {
                            u4IfIndex = u4NextIfIndex;
                            i4RetStatus =
                                IpShowIfTrafficInCxt (CliHandle, u4IfIndex,
                                                      (UINT1) CLI_PTR_TO_U4
                                                      (args[IP_ZERO]), u4VcId);
                        }
                        while (CfaCliGetNextIfIndexFromName
                               (au1IfName, u4IfIndex,
                                &u4NextIfIndex) != OSIX_FAILURE);
                    }

                }
                else
                {
                    IpShowTrafficInCxt (CliHandle,
                                        (UINT1) CLI_PTR_TO_U4 (args[IP_ZERO]),
                                        u4VcId);
                }
            }
            else
            {
                i4RetStatus =
                    IpShowIfTrafficInCxt (CliHandle, (UINT4) i4IfaceIndex,
                                          (UINT1) CLI_PTR_TO_U4 (args[IP_ZERO]),
                                          u4VcId);
            }
#ifdef IP_WANTED
            IPFWD_PROT_LOCK ();
#endif
            break;

        case CLI_IP_SHOW_INFO:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }

#ifdef IP_WANTED
            IPFWD_PROT_UNLOCK ();
#endif
            i4RetStatus = IpShowInfoInCxt (CliHandle, u4VcId);
#ifdef IP_WANTED
            IPFWD_PROT_LOCK ();
#endif
            break;

        case CLI_IP_ROUTING:
            i4RetStatus = IpSetRouting (CliHandle, (INT4) IP_FORW_ENABLE);
            break;

        case CLI_IP_NO_ROUTING:
            i4RetStatus = IpSetRouting (CliHandle, (INT4) IP_FORW_DISABLE);
            break;

        case CLI_IP_DEFAULT_TTL:
            /* args[0] - default TTL value
             */
            i4RetStatus = IpSetDefaultTTL (CliHandle, *(INT4 *) args[IP_ZERO]);
            break;

        case CLI_IP_NO_DEFAULT_TTL:
            i4RetStatus = IpSetDefaultTTL (CliHandle, (INT4) IP_DEF_TTL);
            break;

        default:
            CliPrintf (CliHandle, "\r% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;
    }
    /*Reset Context */
    UtilIpReleaseCxt ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > IP_ZERO) && (u4ErrCode < CLI_IP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", StdipCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (IP_ZERO);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

#ifdef IP_WANTED
    CliUnRegisterLock (CliHandle);
    IPFWD_PROT_UNLOCK ();
#endif /* IP_WANTED */

    return i4RetStatus;
}

INT4
cli_process_rtm_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[IP_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    INT1                u1flag = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    INT4                i4IpRoutePreference = 0;
    UINT4               u4IpRouteNextHop = 0;
    INT4                i4RedisCtrlFlag = -1;
    UINT4               u4VcId = 0;
    UINT1              *pu1IpCxtName = NULL;
    INT4                i4IpDefAdminDistance = IP_ZERO;
    INT4                i4RtmRouteLeakStatus = RTM_VRF_ROUTE_LEAK_DISABLED;
    CLI_SET_CMD_STATUS (CLI_FAILURE);

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Fourth argument is always ContextName */
    pu1IpCxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     * Store IP_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == IP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, RtmProtocolLock, RtmProtocolUnlock);
    RTM_PROT_LOCK ();

    /*Get context Id from context Name */
    if (pu1IpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1IpCxtName, &u4VcId) != VCM_TRUE)
        {
            i4RetStatus = CLI_FAILURE;
        }
    }
    else
    {
        u4VcId = IP_DEFAULT_CONTEXT;
    }
    if (i4RetStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
        CliUnRegisterLock (CliHandle);
        RTM_PROT_UNLOCK ();
        return CLI_FAILURE;
    }

    /*Set Context Id, which will be used by the following
       configuration commands */
    if (UtilIpSetCxt (u4VcId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
        CliUnRegisterLock (CliHandle);
        RTM_PROT_UNLOCK ();
        return CLI_FAILURE;
    }

    switch (u4Command)
    {
        case CLI_IP_SHOW_ROUTE:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }
            /* args[0]  - CmdType for show ip route command
             * args[1]  - If CmdType is SHOW_ROUTE_IPADDR, then an ipaddress
             */
            /* IP address is Cmdtype is SHOW_ROUTE_IPADDR is passed as args[1]
             */

            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            switch (u4CmdType)
            {
                case SHOW_ROUTE:
                    i4RetStatus = IpShowRouteInCxt (CliHandle, u1flag, u4VcId);
                    break;

                case SHOW_ROUTE_DETAILS:
                    u1flag = 1;
                    i4RetStatus = IpShowRouteInCxt (CliHandle, u1flag, u4VcId);
                    break;

                case SHOW_ROUTE_SUMMARY:
                    i4RetStatus = IpShowRouteSummaryInCxt (CliHandle, u4VcId);
                    break;

                case SHOW_ROUTE_CONNECTED:
                    i4RetStatus =
                        IpShowRouteProtoInCxt (CliHandle, (INT1) CIDR_LOCAL_ID,
                                               u4VcId);
                    break;

                case SHOW_ROUTE_STATIC:
                    i4RetStatus =
                        IpShowRouteProtoInCxt (CliHandle, (INT1) CIDR_STATIC_ID,
                                               u4VcId);
                    break;

                case SHOW_ROUTE_RIP:
                    i4RetStatus =
                        IpShowRouteProtoInCxt (CliHandle, (INT1) RIP_ID,
                                               u4VcId);
                    break;

                case SHOW_ROUTE_OSPF:
                    i4RetStatus =
                        IpShowRouteProtoInCxt (CliHandle, (INT1) OSPF_ID,
                                               u4VcId);
                    break;

                case SHOW_ROUTE_BGP:
                    i4RetStatus =
                        IpShowRouteProtoInCxt (CliHandle, (INT1) BGP_ID,
                                               u4VcId);
                    break;

                case SHOW_ROUTE_ISIS:
                    i4RetStatus =
                        IpShowRouteProtoInCxt (CliHandle, (INT1) ISIS_ID,
                                               u4VcId);
                    break;

                case SHOW_ROUTE_IPADDR:
                    if (args[2] != NULL)
                    {
                        i4RetStatus = IpShowRouteIpAddrInCxt (CliHandle, TRUE,
                                                              *(INT4 *) (VOID *)
                                                              args[1],
                                                              *(INT4 *) (VOID *)
                                                              args[2], u4VcId);
                    }
                    else
                    {
                        i4RetStatus = IpShowRouteIpAddrInCxt (CliHandle, FALSE,
                                                              *(INT4 *) (VOID *)
                                                              args[1],
                                                              0, u4VcId);
                    }
                    break;
                case SHOW_ROUTE_FAILED:
                    i4RetStatus = IpShowFrtEntry (CliHandle);
                    break;

                default:
                    break;
            }
            break;

        case CLI_IP_ROUTE:
            /* args[0] - destination ip address 
             * args[1] - ip mask
             * args[2] - next hop
             * args[3] - distance metric
             * args[4] - Rt -private status
             * args[5] - Optional nexthop ip
             */

            /* When next hop is not given, it is assumed the value of 0.0.0.0
             * for directly connected networks.
             * In this case, it is mandatory to give interface as one of the
             * inputs.*/

            /* Route entry addition for stack interface vlan 4094 and
             * Link local address range is not permitted */

            u4IpRouteNextHop = (args[2] == NULL) ?
                (UINT4) CLI_INET_ADDR ("0.0.0.0") : *args[2];
            if (u4IpRouteNextHop == IP_GEN_BCAST_ADDR)
            {
                CliPrintf (CliHandle,
                           "\r%% Invalid next hop address specified\r\n");
                return CLI_FAILURE;
            }
            if ((args[5] != NULL))
            {
                nmhGetFsMIRtmRouteLeakStatus (&i4RtmRouteLeakStatus);
                if (i4RtmRouteLeakStatus == RTM_VRF_ROUTE_LEAK_ENABLED)
                {
                    u4IpRouteNextHop = CLI_PTR_TO_U4 (args[5]);
                }
                else
                {
                    CLI_SET_ERR (CLI_IP_ROUTE_LEAK_ERR);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            i4IpRoutePreference = CLI_PTR_TO_I4 (args[3]);
            if (i4IpRoutePreference == IP_ZERO)
            {
                if (IpGetDefAdminDistance (u4VcId, &i4IpRoutePreference)
                    == CLI_FAILURE)
                {
                    CLI_SET_ERR (CLI_IP_DISTANCE_GET_ERR);
                    return CLI_FAILURE;
                }
            }

            i4RedisCtrlFlag = CLI_PTR_TO_I4 (args[4]);
            if (i4RedisCtrlFlag == TRUE)
            {
                i4RedisCtrlFlag = IP_DONT_REDIS_ROUTE;
            }
            else
            {
                i4RedisCtrlFlag = IP_REDIS_ROUTE;
            }
            RTM_PROT_UNLOCK ();
            IpvxLock ();
            i4RetStatus =
                IpSetRoute (CliHandle, u4VcId, i4IfaceIndex, *args[0],
                            *args[1], u4IpRouteNextHop,
                            i4IpRoutePreference, i4RedisCtrlFlag);
            IpvxUnLock ();
            RTM_PROT_LOCK ();
            break;

        case CLI_IP_NO_ROUTE:
            /* args[0] - destination ip address 
             * args[1] - ip mask
             * args[2] - next hop
             * args[3] - RT Private
             * args[4] - Optional next hop ip address
             */
            u4IpRouteNextHop = (args[2] == NULL) ?
                (UINT4) CLI_INET_ADDR ("0.0.0.0") : *args[2];
            if ((args[4] != NULL))
            {
                nmhGetFsMIRtmRouteLeakStatus (&i4RtmRouteLeakStatus);
                if (i4RtmRouteLeakStatus == RTM_VRF_ROUTE_LEAK_ENABLED)
                {
                    u4IpRouteNextHop = *args[4];
                }
                else
                {
                    CLI_SET_ERR (CLI_IP_ROUTE_LEAK_ERR);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            i4RedisCtrlFlag = CLI_PTR_TO_I4 (args[3]);
            if (i4RedisCtrlFlag == TRUE)
            {
                i4RedisCtrlFlag = IP_REDIS_ROUTE;
            }
            else
            {
                i4RedisCtrlFlag = IP_MINUS_ONE;
            }
            RTM_PROT_UNLOCK ();
            i4RetStatus = IpSetNoRoute (CliHandle, u4VcId,
                                        *args[0],
                                        *args[1],
                                        u4IpRouteNextHop, i4RedisCtrlFlag);
            RTM_PROT_LOCK ();
            break;

        case CLI_IP_ROUTE_DEF_DISTANCE:

            /* Configure the default administrative distance for 
             * static IPv4 route for default/user VRF 
             */

            /* Default administrative distance is passed in args[0] */
            i4IpDefAdminDistance = (INT4) *args[0];
            RTM_PROT_UNLOCK ();
            IpvxLock ();
            i4RetStatus = IpSetDefAdminDistance (CliHandle, u4VcId,
                                                 i4IpDefAdminDistance);
            IpvxUnLock ();
            RTM_PROT_LOCK ();
            break;

        case CLI_IP_SHOW_DEF_DISTANCE:

            if (pu1IpCxtName == NULL)
            {
                /* If no VRF/Context specified */
                u4VcId = VCM_INVALID_VC;
            }
            i4RetStatus = IpShowDefAdminDistance (CliHandle, u4VcId);
            break;

        default:
            CliPrintf (CliHandle, "\r% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;
    }

    /*Reset Context */
    UtilIpReleaseCxt ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_IP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", StdipCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);
    RTM_PROT_UNLOCK ();

    return i4RetStatus;
}

INT4
cli_process_arp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[IP_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    UINT1               au1MacAddress[MAC_LEN];
    UINT1              *pu1ArpCxtName = NULL;
    UINT4               u4ArpCxtId = VCM_INVALID_VC;
    INT4                i4ShowAllVlan = FALSE;
    UINT4               u4NextIfIndex = 0;
    UINT1               au1IfName[IF_PREFIX_LEN];
    UINT1               u1VlanIdArgIndex = 0;
    UINT1               u1ArgIndex = 0;

    CLI_SET_CMD_STATUS (CLI_FAILURE);

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Fourth argument is always ContextName */
    pu1ArpCxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     * Store IP_CLI_MAX_ARGS arguements at the max. 
     */
    if (pu1ArpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1ArpCxtName, &u4ArpCxtId) != VCM_TRUE)
        {
            i4RetStatus = CLI_FAILURE;
        }

    }
    else
    {
        u4ArpCxtId = IP_DEFAULT_CONTEXT;
    }
    if (i4RetStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Arp Context Id\r\n");
        va_end (ap);
        return CLI_FAILURE;
    }

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == IP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, ArpProtocolLock, ArpProtocolUnLock);
    ARP_PROT_LOCK ();

    /*Set Context Id for ARP, which will be used by the 
       configuration commands */
    if (ArpSetContext (u4ArpCxtId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Arp Context Id\r\n");
        CliUnRegisterLock (CliHandle);
        ARP_PROT_UNLOCK ();
        return CLI_FAILURE;
    }

    switch (u4Command)
    {
        case CLI_IP_SHOW_ARP:
            /* args[0] - CmdType for show ip arp command
             * args[1] - If Cmdtype is SHOW_IP_ARP_IPADDR then ip address
             *           else is CmdType is SHOW_IP_ARP_MACADDR then mac address
             */
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            ARP_PROT_UNLOCK ();

            switch (u4CmdType)
            {
                case SHOW_IP_ARP:
                    i4RetStatus = IpShowArpInCxt (CliHandle, u4ArpCxtId);
                    break;

                case SHOW_IP_ARP_INTF:
                {
                    u1ArgIndex = IP_ONE;
                    u1ArgIndex++;
                    /* ShowAllVlabnn Arg Index ==> args[2] 
                     */
                    i4ShowAllVlan = CLI_PTR_TO_I4 (args[u1ArgIndex]);
                    if ((i4IfaceIndex == IP_ZERO) && (i4ShowAllVlan == TRUE))
                    {
                        u1VlanIdArgIndex = u1ArgIndex;
                        /* Vlan Id Arg Index ==> args [3] */
                        u1VlanIdArgIndex++;
                        MEMSET (au1IfName, 0, IF_PREFIX_LEN);
                        SNPRINTF ((CHR1 *) au1IfName, IF_PREFIX_LEN, "%s%d",
                                  "vlan", *(INT4 *) (VOID *)
                                  args[u1VlanIdArgIndex]);

                        /* Print All the IP interfaces with
                         * IfAlias as "au1IfName"
                         */
                        if (CfaCliGetFirstIfIndexFromName
                            (au1IfName, &u4NextIfIndex) != OSIX_FAILURE)
                        {
                            do
                            {
                                i4IfaceIndex = (INT4) u4NextIfIndex;
                                IpShowArpInterfaceInCxt (CliHandle, u4ArpCxtId,
                                                         i4IfaceIndex);
                            }
                            while (CfaCliGetNextIfIndexFromName
                                   (au1IfName, (UINT4) i4IfaceIndex,
                                    &u4NextIfIndex) != OSIX_FAILURE);
                        }

                    }
                    else
                    {
                        IpShowArpInterfaceInCxt (CliHandle, u4ArpCxtId,
                                                 i4IfaceIndex);
                    }

                    break;
                }
                case SHOW_IP_ARP_IPADDR:
                    i4RetStatus =
                        IpShowArpIpAddressInCxt
                        (CliHandle, u4ArpCxtId, *(UINT4 *) (VOID *) args[1]);
                    break;

                case SHOW_IP_ARP_MACADDR:
                    MEMSET (au1MacAddress, 0, MAC_LEN);
                    CliStrToMac ((UINT1 *) args[1], au1MacAddress);
                    i4RetStatus =
                        IpShowArpMacAddressInCxt (CliHandle, u4ArpCxtId,
                                                  au1MacAddress);
                    break;

                case SHOW_IP_ARP_SUMMARY:
                    ARP_PROT_LOCK ();
                    i4RetStatus = IpShowArpSummaryInCxt (CliHandle, u4ArpCxtId);
                    ARP_PROT_UNLOCK ();
                    break;

                case SHOW_IP_ARP_INFO:
                    i4RetStatus = IpShowArpInfoInCxt (CliHandle, u4ArpCxtId);
                    break;

                case SHOW_IP_ARP_STATS:
                    i4RetStatus = IpShowArpStatsInCxt (CliHandle, u4ArpCxtId);
                    break;

                default:
                    break;
            }
            ARP_PROT_LOCK ();
            break;

        case CLI_IP_SHOW_PROXY_ARP:
            if (pu1ArpCxtName == NULL)
            {
                u4ArpCxtId = VCM_INVALID_VC;
            }
            i4RetStatus = IpShowProxyArpInfoInCxt (CliHandle, u4ArpCxtId);
            break;

        case CLI_IP_ARP_TIMEOUT:
            /* args[0] - Timeout value to set */
            /* For Set Routines we are taking a lock here in order to 
             * avoid multiple unlocks in the function */
            i4RetStatus = IpSetArpCacheTimeout (CliHandle, *(INT4 *) (VOID *)
                                                args[0]);
            break;

        case CLI_IP_NO_ARP_TIMEOUT:
            i4RetStatus = IpSetArpCacheTimeout (CliHandle,
                                                (INT4) IP_DEF_ARP_TIMEOUT);
            break;

        case CLI_IP_ARP:
            /* args[0]  - ip address
             * args[1]  - mac address
             */
            if (pu1ArpCxtName == NULL)
            {
                u4ArpCxtId = VCM_INVALID_VC;
            }
            i4RetStatus = IpSetArp (CliHandle, i4IfaceIndex, *args[0],
                                    (UINT1 *) args[1], u4ArpCxtId);
            break;

        case CLI_IP_NO_ARP:
            /* args[0]  - ip address
             */
            i4RetStatus = IpSetNoArp (CliHandle, *args[0], u4ArpCxtId);
            break;

        case CLI_IP_ARP_MAX_RETRIES:
            /* args[0] - retries value
             */
            i4RetStatus = IpSetArpMaxRetries (CliHandle, *(INT4 *) (VOID *)
                                              args[0]);
            break;

        case CLI_IP_NO_ARP_MAX_RETRIES:
            i4RetStatus = IpSetArpMaxRetries (CliHandle,
                                              (INT4) ARP_DEF_REQ_RETRIES);
            break;
        case CLI_IP_CLEAR_ARP:
            i4RetStatus = IpSetClearArp (CliHandle, u4ArpCxtId);
            break;
        case CLI_IP_PROXY_ARP:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = IpConfigProxyArp (CliHandle, i4IfaceIndex,
                                            ARP_PROXY_ENABLE);
            break;

        case CLI_IP_NO_PROXY_ARP:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = IpConfigProxyArp (CliHandle, i4IfaceIndex,
                                            ARP_PROXY_DISABLE);
            break;
#ifdef IP_WANTED
        case CLI_IP_PROXY_ARP_SUBNET_OPTION:
            i4RetStatus = IpConfigProxyArpSubnetOption (CliHandle,
                                                        ARP_PROXY_ENABLE);
            break;
        case CLI_IP_NO_PROXY_ARP_SUBNET_OPTION:
            i4RetStatus = IpConfigProxyArpSubnetOption (CliHandle,
                                                        ARP_PROXY_DISABLE);
            break;

        case CLI_IP_LOCAL_PROXY_ARP:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = IpConfigLocalProxyArp (CliHandle, i4IfaceIndex,
                                                 ARP_PROXY_ENABLE);
            break;

        case CLI_IP_NO_LOCAL_PROXY_ARP:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = IpConfigLocalProxyArp (CliHandle, i4IfaceIndex,
                                                 ARP_PROXY_DISABLE);
            break;

#endif
        case CLI_ARP_DEBUG:
            i4RetStatus = IpArpSetDebugLevel (CliHandle, u4ArpCxtId,
                                              CLI_PTR_TO_U4 (args[0]),
                                              CLI_PTR_TO_U4 (args[1]));
            break;

        default:
            CliPrintf (CliHandle, "\r% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }
    /*Reset ARP Context */
    ArpResetContext ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_IP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", StdipCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);
    ARP_PROT_UNLOCK ();

    return i4RetStatus;
}

/*********************************************************************
 *  Function Name : IpShowIfTrafficInCxt
 *  Description   : If  Statistics for Ipv4
 *  Input(s)      : CliHandle, IfIndex, HCFlag
 *  Output(s)     :  
 *  Return Values : CLI_FAILURE, CLI_SUCCESS.
 *********************************************************************/

INT4 
     
     
     
     
     
     
     
    IpShowIfTrafficInCxt
    (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 u1HCFlag, UINT4 u4VcId)
{
    UINT1               au1Alias[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1              *pu1IfaceInfo = NULL;
    INT4                i4IpIfStatsIPVersion = INET_ADDR_TYPE_IPV4;
    UINT4               u4ContextId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4IpIfStatsInHdrErrors = IP_ZERO;
    UINT4               u4IpIfStatsInNoRoutes = IP_ZERO;
    UINT4               u4IpIfStatsInAddrErrors = IP_ZERO;
    UINT4               u4IpIfStatsInUnknownProtos = IP_ZERO;
    UINT4               u4IpIfStatsInTruncatedPkts = IP_ZERO;
    UINT4               u4IpIfStatsReasmReqds = IP_ZERO;
    UINT4               u4IpIfStatsReasmOKs = IP_ZERO;
    UINT4               u4IpIfStatsReasmFails = IP_ZERO;
    UINT4               u4IpIfStatsInDiscards = IP_ZERO;
    UINT4               u4IpIfStatsOutDiscards = IP_ZERO;
    UINT4               u4IpIfStatsOutFragReqds = IP_ZERO;
    UINT4               u4IpIfStatsOutFragOKs = IP_ZERO;
    UINT4               u4IpIfStatsOutFragFails = IP_ZERO;
    UINT4               u4IpIfStatsOutFragCreates = IP_ZERO;
    UINT4               u4IpIfStatsDiscontinuityTime = IP_ZERO;
    UINT4               u4IpIfStatsRefreshRate = IP_ZERO;
    UINT4               u4Port = 0;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInReceives;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInDelivers;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutRequests;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutTransmits;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInMcastOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutMcastOctets;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCInBcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpIfStatsHCOutBcastPkts;

    FS_UINT8            u8IpIfStatsHCInReceives;
    FS_UINT8            u8IpIfStatsHCInOctets;
    FS_UINT8            u8IpIfStatsHCInForwDatagrams;
    FS_UINT8            u8IpIfStatsHCInDelivers;
    FS_UINT8            u8IpIfStatsHCOutRequests;
    FS_UINT8            u8IpIfStatsHCOutForwDatagrams;
    FS_UINT8            u8IpIfStatsHCOutTransmits;
    FS_UINT8            u8IpIfStatsHCOutOctets;
    FS_UINT8            u8IpIfStatsHCInMcastPkts;
    FS_UINT8            u8IpIfStatsHCInMcastOctets;
    FS_UINT8            u8IpIfStatsHCOutMcastPkts;
    FS_UINT8            u8IpIfStatsHCOutMcastOctets;
    FS_UINT8            u8IpIfStatsHCInBcastPkts;
    FS_UINT8            u8IpIfStatsHCOutBcastPkts;
    UINT1               au1HCTempCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1HCTempCnt[CFA_CLI_U8_STR_LENGTH];

    MEMSET (&au1Alias, IP_ZERO, CFA_CLI_MAX_IF_NAME_LEN);
    pu1IfaceInfo = &au1Alias[IP_ZERO];

    MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

    MEMSET (&RetValIpIfStatsHCInReceives, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInOctets, IP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInDelivers, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutRequests, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutTransmits, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCOutBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpIfStatsHCInMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));

    FSAP_U8_CLR (&u8IpIfStatsHCInReceives);
    FSAP_U8_CLR (&u8IpIfStatsHCInOctets);
    FSAP_U8_CLR (&u8IpIfStatsHCInForwDatagrams);
    FSAP_U8_CLR (&u8IpIfStatsHCInDelivers);
    FSAP_U8_CLR (&u8IpIfStatsHCOutRequests);
    FSAP_U8_CLR (&u8IpIfStatsHCOutForwDatagrams);
    FSAP_U8_CLR (&u8IpIfStatsHCOutTransmits);
    FSAP_U8_CLR (&u8IpIfStatsHCOutOctets);
    FSAP_U8_CLR (&u8IpIfStatsHCInMcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCOutMcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCOutMcastOctets);
    FSAP_U8_CLR (&u8IpIfStatsHCInBcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCOutBcastPkts);
    FSAP_U8_CLR (&u8IpIfStatsHCInMcastOctets);

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;

    }
    if (u4ShowAllCxt == FALSE)
    {
        if (nmhGetFsMIStdIpIfStatsContextId
            (i4IpIfStatsIPVersion, (INT4) u4IfIndex,
             (INT4 *) &u4ContextId) != SNMP_FAILURE)
        {
            if (u4ContextId != u4VcId)
            {
                CliPrintf (CliHandle,
                           "\rThis interface is not mapped to the specified VRF\r\n");
                return CLI_SUCCESS;
            }
        }
    }
    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) != IP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r Please check the interface you have entered. ."
                   " IP traffic statistics will be displayed only for vlan interfaces and router ports\r\n");
        return CLI_SUCCESS;
    }
    /* Getting the IP Related Information */

    (VOID) nmhGetFsMIStdIpIfStatsHCInReceives (i4IpIfStatsIPVersion,
                                               (INT4) u4IfIndex,
                                               &RetValIpIfStatsHCInReceives);
    (VOID) nmhGetFsMIStdIpIfStatsHCInOctets (i4IpIfStatsIPVersion,
                                             (INT4) u4IfIndex,
                                             &RetValIpIfStatsHCInOctets);
    (VOID) nmhGetFsMIStdIpIfStatsInHdrErrors (i4IpIfStatsIPVersion,
                                              (INT4) u4IfIndex,
                                              &u4IpIfStatsInHdrErrors);
    (VOID) nmhGetFsMIStdIpIfStatsInNoRoutes (i4IpIfStatsIPVersion,
                                             (INT4) u4IfIndex,
                                             &u4IpIfStatsInNoRoutes);
    (VOID) nmhGetFsMIStdIpIfStatsInAddrErrors (i4IpIfStatsIPVersion,
                                               (INT4) u4IfIndex,
                                               &u4IpIfStatsInAddrErrors);
    (VOID) nmhGetFsMIStdIpIfStatsInUnknownProtos (i4IpIfStatsIPVersion,
                                                  (INT4) u4IfIndex,
                                                  &u4IpIfStatsInUnknownProtos);
    (VOID) nmhGetFsMIStdIpIfStatsInTruncatedPkts (i4IpIfStatsIPVersion,
                                                  (INT4) u4IfIndex,
                                                  &u4IpIfStatsInTruncatedPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCInForwDatagrams (i4IpIfStatsIPVersion,
                                                    (INT4) u4IfIndex,
                                                    &RetValIpIfStatsHCInForwDatagrams);
    (VOID) nmhGetFsMIStdIpIfStatsReasmReqds (i4IpIfStatsIPVersion,
                                             (INT4) u4IfIndex,
                                             &u4IpIfStatsReasmReqds);
    (VOID) nmhGetFsMIStdIpIfStatsReasmOKs (i4IpIfStatsIPVersion,
                                           (INT4) u4IfIndex,
                                           &u4IpIfStatsReasmOKs);
    (VOID) nmhGetFsMIStdIpIfStatsReasmFails (i4IpIfStatsIPVersion,
                                             (INT4) u4IfIndex,
                                             &u4IpIfStatsReasmFails);
    (VOID) nmhGetFsMIStdIpIfStatsInDiscards (i4IpIfStatsIPVersion,
                                             (INT4) u4IfIndex,
                                             &u4IpIfStatsInDiscards);
    (VOID) nmhGetFsMIStdIpIfStatsHCInDelivers (i4IpIfStatsIPVersion,
                                               (INT4) u4IfIndex,
                                               &RetValIpIfStatsHCInDelivers);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutRequests (i4IpIfStatsIPVersion,
                                                (INT4) u4IfIndex,
                                                &RetValIpIfStatsHCOutRequests);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutForwDatagrams (i4IpIfStatsIPVersion,
                                                     (INT4) u4IfIndex,
                                                     &RetValIpIfStatsHCOutForwDatagrams);
    (VOID) nmhGetFsMIStdIpIfStatsOutDiscards (i4IpIfStatsIPVersion,
                                              (INT4) u4IfIndex,
                                              &u4IpIfStatsOutDiscards);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragReqds (i4IpIfStatsIPVersion,
                                               (INT4) u4IfIndex,
                                               &u4IpIfStatsOutFragReqds);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragOKs (i4IpIfStatsIPVersion,
                                             (INT4) u4IfIndex,
                                             &u4IpIfStatsOutFragOKs);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragFails (i4IpIfStatsIPVersion,
                                               (INT4) u4IfIndex,
                                               &u4IpIfStatsOutFragFails);
    (VOID) nmhGetFsMIStdIpIfStatsOutFragCreates (i4IpIfStatsIPVersion,
                                                 (INT4) u4IfIndex,
                                                 &u4IpIfStatsOutFragCreates);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutTransmits (i4IpIfStatsIPVersion,
                                                 (INT4) u4IfIndex,
                                                 &RetValIpIfStatsHCOutTransmits);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutOctets (i4IpIfStatsIPVersion,
                                              (INT4) u4IfIndex,
                                              &RetValIpIfStatsHCOutOctets);
    (VOID) nmhGetFsMIStdIpIfStatsHCInMcastPkts (i4IpIfStatsIPVersion,
                                                (INT4) u4IfIndex,
                                                &RetValIpIfStatsHCInMcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCInMcastOctets (i4IpIfStatsIPVersion,
                                                  (INT4) u4IfIndex,
                                                  &RetValIpIfStatsHCInMcastOctets);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutMcastPkts (i4IpIfStatsIPVersion,
                                                 (INT4) u4IfIndex,
                                                 &RetValIpIfStatsHCOutMcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutMcastOctets (i4IpIfStatsIPVersion,
                                                   (INT4) u4IfIndex,
                                                   &RetValIpIfStatsHCOutMcastOctets);
    (VOID) nmhGetFsMIStdIpIfStatsHCInBcastPkts (i4IpIfStatsIPVersion,
                                                (INT4) u4IfIndex,
                                                &RetValIpIfStatsHCInBcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsHCOutBcastPkts (i4IpIfStatsIPVersion,
                                                 (INT4) u4IfIndex,
                                                 &RetValIpIfStatsHCOutBcastPkts);
    (VOID) nmhGetFsMIStdIpIfStatsDiscontinuityTime (i4IpIfStatsIPVersion,
                                                    (INT4) u4IfIndex,
                                                    &u4IpIfStatsDiscontinuityTime);
    (VOID) nmhGetFsMIStdIpIfStatsRefreshRate (i4IpIfStatsIPVersion,
                                              (INT4) u4IfIndex,
                                              &u4IpIfStatsRefreshRate);

    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInReceives,
                       RetValIpIfStatsHCInReceives.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInReceives,
                       RetValIpIfStatsHCInReceives.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInOctets, RetValIpIfStatsHCInOctets.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInOctets, RetValIpIfStatsHCInOctets.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInForwDatagrams,
                       RetValIpIfStatsHCInForwDatagrams.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInForwDatagrams,
                       RetValIpIfStatsHCInForwDatagrams.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInDelivers,
                       RetValIpIfStatsHCInDelivers.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInDelivers,
                       RetValIpIfStatsHCInDelivers.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutRequests,
                       RetValIpIfStatsHCOutRequests.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutRequests,
                       RetValIpIfStatsHCOutRequests.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutForwDatagrams,
                       RetValIpIfStatsHCOutForwDatagrams.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutForwDatagrams,
                       RetValIpIfStatsHCOutForwDatagrams.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutTransmits,
                       RetValIpIfStatsHCOutTransmits.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutTransmits,
                       RetValIpIfStatsHCOutTransmits.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutOctets, RetValIpIfStatsHCOutOctets.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutOctets, RetValIpIfStatsHCOutOctets.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInMcastPkts,
                       RetValIpIfStatsHCInMcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInMcastPkts,
                       RetValIpIfStatsHCInMcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutMcastPkts,
                       RetValIpIfStatsHCOutMcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutMcastPkts,
                       RetValIpIfStatsHCOutMcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutMcastOctets,
                       RetValIpIfStatsHCOutMcastOctets.msn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInBcastPkts,
                       RetValIpIfStatsHCInBcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInBcastPkts,
                       RetValIpIfStatsHCInBcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCOutBcastPkts,
                       RetValIpIfStatsHCOutBcastPkts.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCOutBcastPkts,
                       RetValIpIfStatsHCOutBcastPkts.lsn);
    FSAP_U8_ASSIGN_HI (&u8IpIfStatsHCInMcastOctets,
                       RetValIpIfStatsHCInMcastOctets.msn);
    FSAP_U8_ASSIGN_LO (&u8IpIfStatsHCInMcastOctets,
                       RetValIpIfStatsHCInMcastOctets.lsn);

    /* After getting all the values just Print it */

    /*Display Ip related Information */
    CfaCliGetIfName (u4IfIndex, (INT1 *) pu1IfaceInfo);
    CliPrintf (CliHandle, "\r\nIPv4 %sStatistics for interface %s\r\n",
               ((u1HCFlag) ? "High Count " : ""), pu1IfaceInfo);
    CliPrintf (CliHandle,
               "----------------------------------------------------\r\n");

    if (u1HCFlag == IP_CLI_HC_DISABLED)
    {
        CliPrintf (CliHandle,
                   "\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCInReceives.lsn, "Rcvd",
                   RetValIpIfStatsHCInOctets.lsn, "InOctets",
                   u4IpIfStatsInHdrErrors, "HdrErrors");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsInNoRoutes, "InNoRoutes",
                   u4IpIfStatsInAddrErrors, "AddrErrors",
                   u4IpIfStatsInUnknownProtos, "UnknownProtos");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsInTruncatedPkts, "TruncatedPkts",
                   RetValIpIfStatsHCInForwDatagrams.lsn, "FwdDatagrms",
                   u4IpIfStatsReasmReqds, "ReasmReqds");

        CliPrintf (CliHandle, "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsReasmOKs, "ReasmOKs",
                   u4IpIfStatsReasmFails, "ReasmFails",
                   u4IpIfStatsInDiscards, "Discards");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCInDelivers.lsn, "Delivers",
                   RetValIpIfStatsHCOutRequests.lsn, "OutRequests",
                   RetValIpIfStatsHCOutForwDatagrams.lsn, "OutFwdDgrms");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsOutDiscards, "OutDiscards",
                   u4IpIfStatsOutFragReqds, "FragReqds",
                   u4IpIfStatsOutFragOKs, "FragOKs");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   u4IpIfStatsOutFragFails, "FragFails",
                   u4IpIfStatsOutFragCreates, "FragCreates",
                   RetValIpIfStatsHCOutRequests.lsn, "OutTrnsmits");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCOutOctets.lsn, "OutOctets",
                   RetValIpIfStatsHCInMcastPkts.lsn, "InMcstPkts",
                   RetValIpIfStatsHCInMcastPkts.lsn, "InMcstOctets");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCOutMcastPkts.lsn, "OutMcstPkts",
                   RetValIpIfStatsHCOutMcastOctets.lsn, "OutMcstOctets",
                   RetValIpIfStatsHCInBcastPkts.lsn, "InBcstPkts");

        CliPrintf (CliHandle,
                   "%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                   RetValIpIfStatsHCOutBcastPkts.lsn, "OutBcstPkts",
                   u4IpIfStatsDiscontinuityTime, "DiscntTime",
                   u4IpIfStatsRefreshRate, "RefrshRate");

    }
    else
    {
        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInReceives, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCInOctets, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\t",
                   au1HCTempCount, "InRcvs", au1HCTempCnt, "InOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInForwDatagrams, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCInDelivers, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\r\n%5s\t%-15s\t",
                   au1HCTempCount, "InFwdDgrms", au1HCTempCnt, "InDelivers");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCOutRequests, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutForwDatagrams, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\r\n",
                   au1HCTempCount, "OutRequests", au1HCTempCnt, "OutFwdDgrms");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCOutTransmits, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutOctets, (CHR1 *) & au1HCTempCnt);

        CliPrintf (CliHandle,
                   "%5s\t%-15s\r\n%5s\t%-15s\t",
                   au1HCTempCount, "OutTrnsmits", au1HCTempCnt, "OutOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInMcastPkts, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCInMcastOctets, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\r\n%5s\t%-15s\t",
                   au1HCTempCount, "InMcstPkts", au1HCTempCnt, "InMcstOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCOutMcastPkts, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutMcastOctets, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\r\n",
                   au1HCTempCount, "OutMcstPkts", au1HCTempCnt,
                   "OutMcstOctets");

        MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
        MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

        FSAP_U8_2STR (&u8IpIfStatsHCInBcastPkts, (CHR1 *) & au1HCTempCount);
        FSAP_U8_2STR (&u8IpIfStatsHCOutBcastPkts, (CHR1 *) & au1HCTempCnt);
        CliPrintf (CliHandle,
                   "%5s\t%-15s\t%5s\t%-15s\r\n",
                   au1HCTempCount, "InBcast", au1HCTempCnt, "OutBcast");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowTrafficInCxt                                     */
/*                                                                           */
/* Description      : This function is invoked to display IP statistics      */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u1HCFlag - High count flag                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowTrafficInCxt (tCliHandle CliHandle, UINT1 u1HCFlag, UINT4 u4VcId)
{
    INT4                i4IpSystemStatsIPVersion = INET_ADDR_TYPE_IPV4;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4PrevVcId;
    tIpCliInfo          IpCliInfo;

    UINT4               u4IpSystemStatsInDiscards = IP_ZERO;
    UINT4               u4IpSystemStatsInDelivers = IP_ZERO;
    UINT4               u4IpSystemStatsOutDiscards = IP_ZERO;
    UINT4               u4IpSystemStatsReasmFails = IP_ZERO;
    UINT4               u4IpSystemStatsOutFragCreates = IP_ZERO;
    UINT4               u4IpSystemStatsInMcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsOutMcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsInTruncatedPkts = IP_ZERO;
    UINT4               u4IpSystemStatsInOctets = IP_ZERO;
    UINT4               u4IpSystemStatsInNoRoutes = IP_ZERO;
    UINT4               u4IpSystemStatsOutForwDatagrams = IP_ZERO;
    UINT4               u4IpSystemStatsOutFragReqds = IP_ZERO;
    UINT4               u4IpSystemStatsOutTransmits = IP_ZERO;
    UINT4               u4IpSystemStatsOutOctets = IP_ZERO;
    UINT4               u4IpSystemStatsInMcastOctets = IP_ZERO;
    UINT4               u4IpSystemStatsOutMcastOctets = IP_ZERO;
    UINT4               u4IpSystemStatsInBcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsOutBcastPkts = IP_ZERO;
    UINT4               u4IpSystemStatsDiscontinuityTime = IP_ZERO;
    UINT4               u4IpSystemStatsRefreshRate = IP_ZERO;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInReceives;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInOctets;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInDelivers;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutRequests;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutForwDatagrams;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutTransmits;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutOctets;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutMcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutMcastOctets;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInBcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCOutBcastPkts;
    tSNMP_COUNTER64_TYPE RetValIpSystemStatsHCInMcastOctets;
    tSNMP_COUNTER64_TYPE RetValUdpHCInDatagrams;
    tSNMP_COUNTER64_TYPE RetValUdpHCOutDatagrams;

    FS_UINT8            u8IpSystemStatsHCInReceives;
    FS_UINT8            u8IpSystemStatsHCInOctets;
    FS_UINT8            u8IpSystemStatsHCInForwDatagrams;
    FS_UINT8            u8IpSystemStatsHCInDelivers;
    FS_UINT8            u8IpSystemStatsHCOutRequests;
    FS_UINT8            u8IpSystemStatsHCOutForwDatagrams;
    FS_UINT8            u8IpSystemStatsHCOutTransmits;
    FS_UINT8            u8IpSystemStatsHCOutOctets;
    FS_UINT8            u8IpSystemStatsHCInMcastPkts;
    FS_UINT8            u8IpSystemStatsHCOutMcastPkts;
    FS_UINT8            u8IpSystemStatsHCOutMcastOctets;
    FS_UINT8            u8IpSystemStatsHCInBcastPkts;
    FS_UINT8            u8IpSystemStatsHCOutBcastPkts;
    FS_UINT8            u8IpSystemStatsHCInMcastOctets;
    FS_UINT8            u8UdpHCInDatagrams;
    FS_UINT8            u8UdpHCOutDatagrams;
    UINT1               au1HCTempCount[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1HCTempCnt[CFA_CLI_U8_STR_LENGTH];

    MEMSET (&IpCliInfo, IP_ZERO, sizeof (tIpCliInfo));

    MEMSET (&RetValUdpHCInDatagrams, IP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValUdpHCOutDatagrams, IP_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInReceives, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInDelivers, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutRequests, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutForwDatagrams, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutTransmits, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutMcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCOutBcastPkts, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&RetValIpSystemStatsHCInMcastOctets, IP_ZERO,
            sizeof (tSNMP_COUNTER64_TYPE));

    MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

    FSAP_U8_CLR (&u8IpSystemStatsHCInReceives);
    FSAP_U8_CLR (&u8IpSystemStatsHCInOctets);
    FSAP_U8_CLR (&u8IpSystemStatsHCInForwDatagrams);
    FSAP_U8_CLR (&u8IpSystemStatsHCInDelivers);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutRequests);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutForwDatagrams);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutTransmits);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutOctets);
    FSAP_U8_CLR (&u8IpSystemStatsHCInMcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutMcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutMcastOctets);
    FSAP_U8_CLR (&u8IpSystemStatsHCInBcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCOutBcastPkts);
    FSAP_U8_CLR (&u8IpSystemStatsHCInMcastOctets);
    FSAP_U8_CLR (&u8UdpHCInDatagrams);
    FSAP_U8_CLR (&u8UdpHCOutDatagrams);

    if (u4VcId == VCM_INVALID_VC)
    {
        if (VcmGetFirstActiveL3Context (&u4VcId) == VCM_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = TRUE;
    }

    /*SWARNA: Need a function to check validity of u4VcId */

    /*If valid VcId is given, the loop gets executed only once, otherwise it loops
     * through all the VRFs*/
    do
    {
        u4PrevVcId = u4VcId;

        /* Getting the IP Related Information */
        nmhGetFsMIStdIpSystemStatsInReceives
            ((INT4) u4VcId, i4IpSystemStatsIPVersion,
             &IpCliInfo.IpStats.u4In_rcvs);

        nmhGetFsMIStdIpSystemStatsInHdrErrors
            ((INT4) u4VcId, i4IpSystemStatsIPVersion,
             &IpCliInfo.IpStats.u4In_hdr_err);

        nmhGetFsMIStdIpSystemStatsInAddrErrors
            ((INT4) u4VcId, i4IpSystemStatsIPVersion,
             &IpCliInfo.IpStats.u4In_addr_err);

        nmhGetFsMIStdIpSystemStatsInUnknownProtos
            ((INT4) u4VcId, i4IpSystemStatsIPVersion,
             &IpCliInfo.IpStats.u4Bad_proto);

        nmhGetFsMIStdIpSystemStatsReasmOKs ((INT4) u4VcId,
                                            i4IpSystemStatsIPVersion,
                                            &IpCliInfo.IpStats.u4Reasm_oks);

        nmhGetFsMIStdIpReasmTimeout ((INT4) u4VcId,
                                     (INT4 *) &IpCliInfo.IpStats.
                                     u4Reasm_timeout);

        nmhGetFsMIStdIpSystemStatsReasmReqds ((INT4) u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &IpCliInfo.IpStats.u4Reasm_reqs);

        nmhGetFsMIStdIpSystemStatsOutFragOKs ((INT4) u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &IpCliInfo.IpStats.
                                              u4Out_frag_pkts);

        nmhGetFsMIStdIpSystemStatsOutFragFails
            ((INT4) u4VcId, i4IpSystemStatsIPVersion,
             &IpCliInfo.IpStats.u4Frag_fails);

        nmhGetFsMIStdIpSystemStatsInForwDatagrams ((INT4) u4VcId,
                                                   i4IpSystemStatsIPVersion,
                                                   &IpCliInfo.IpStats.
                                                   u4Forw_attempts);

        nmhGetFsMIStdIpSystemStatsOutRequests ((INT4) u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &IpCliInfo.IpStats.
                                               u4Out_requests);

        nmhGetFsMIStdIpSystemStatsOutNoRoutes ((INT4) u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &IpCliInfo.IpStats.
                                               u4Out_no_routes);

        nmhGetFsMIStdIcmpStatsInMsgs ((INT4) u4VcId, i4IpSystemStatsIPVersion,
                                      &IpCliInfo.IcmpStats.u4IcmpInMsgs);

        nmhGetFsMIStdIcmpStatsInErrors ((INT4) u4VcId, i4IpSystemStatsIPVersion,
                                        &IpCliInfo.IcmpStats.u4IcmpInErr);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_DEST_UNREACH,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInDestUnreach);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_REDIRECT,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInRedirects);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_TIME_EXCEED,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInTimeExceeds);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_PARAM_PROB,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInParmProbs);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion, ICMP_QUENCH,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInSrcQuench);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion, ICMP_ECHO,
                                         &IpCliInfo.IcmpStats.u4IcmpInEchos);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_ECHO_REPLY,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInEchosReps);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_MASK_RQST,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInAddrMasks);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_MASK_REPLY,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInAddrMaskReps);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_TIMESTAMP,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInTimeStamps);

        nmhGetFsMIStdIcmpMsgStatsInPkts ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         ICMP_TIME_REPLY,
                                         &IpCliInfo.IcmpStats.
                                         u4IcmpInTimeStampsReps);

        nmhGetFsMIStdIcmpStatsOutMsgs ((INT4) u4VcId, i4IpSystemStatsIPVersion,
                                       &IpCliInfo.IcmpStats.u4IcmpOutMsgs);

        nmhGetFsMIStdIcmpStatsOutErrors ((INT4) u4VcId,
                                         i4IpSystemStatsIPVersion,
                                         &IpCliInfo.IcmpStats.u4IcmpOutErr);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_DEST_UNREACH,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutDestUnreach);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_REDIRECT,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutRedirects);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_TIME_EXCEED,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutTimeExceeds);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_PARAM_PROB,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutParmProbs);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion, ICMP_QUENCH,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutSrcQuench);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion, ICMP_ECHO,
                                          &IpCliInfo.IcmpStats.u4IcmpOutEchos);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_ECHO_REPLY,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutEchosReps);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_MASK_RQST,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutAddrMasks);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_MASK_REPLY,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutAddrMaskReps);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_TIMESTAMP,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutTimeStamps);

        nmhGetFsMIStdIcmpMsgStatsOutPkts ((INT4) u4VcId,
                                          i4IpSystemStatsIPVersion,
                                          ICMP_TIME_REPLY,
                                          &IpCliInfo.IcmpStats.
                                          u4IcmpOutTimeStampsReps);

        /*Get the new objects */
        nmhGetFsMIStdIpSystemStatsInDiscards ((INT4) u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsInDiscards);
        nmhGetFsMIStdIpSystemStatsInDelivers ((INT4) u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsInDelivers);
        nmhGetFsMIStdIpSystemStatsOutDiscards ((INT4) u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsOutDiscards);
        nmhGetFsMIStdIpSystemStatsReasmFails ((INT4) u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsReasmFails);
        nmhGetFsMIStdIpSystemStatsOutFragCreates ((INT4) u4VcId,
                                                  i4IpSystemStatsIPVersion,
                                                  &u4IpSystemStatsOutFragCreates);
        nmhGetFsMIStdIpSystemStatsInMcastPkts ((INT4) u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsInMcastPkts);
        nmhGetFsMIStdIpSystemStatsOutMcastPkts ((INT4) u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutMcastPkts);
        nmhGetFsMIStdIpSystemStatsInTruncatedPkts ((INT4) u4VcId,
                                                   i4IpSystemStatsIPVersion,
                                                   &u4IpSystemStatsInTruncatedPkts);
        nmhGetFsMIStdIpSystemStatsInOctets ((INT4) u4VcId,
                                            i4IpSystemStatsIPVersion,
                                            &u4IpSystemStatsInOctets);
        nmhGetFsMIStdIpSystemStatsInNoRoutes ((INT4) u4VcId,
                                              i4IpSystemStatsIPVersion,
                                              &u4IpSystemStatsInNoRoutes);
        nmhGetFsMIStdIpSystemStatsOutForwDatagrams ((INT4) u4VcId,
                                                    i4IpSystemStatsIPVersion,
                                                    &u4IpSystemStatsOutForwDatagrams);
        nmhGetFsMIStdIpSystemStatsOutFragReqds ((INT4) u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutFragReqds);
        nmhGetFsMIStdIpSystemStatsOutTransmits ((INT4) u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutTransmits);
        nmhGetFsMIStdIpSystemStatsOutOctets ((INT4) u4VcId,
                                             i4IpSystemStatsIPVersion,
                                             &u4IpSystemStatsOutOctets);
        nmhGetFsMIStdIpSystemStatsInMcastOctets ((INT4) u4VcId,
                                                 i4IpSystemStatsIPVersion,
                                                 &u4IpSystemStatsInMcastOctets);
        nmhGetFsMIStdIpSystemStatsOutMcastOctets ((INT4) u4VcId,
                                                  i4IpSystemStatsIPVersion,
                                                  &u4IpSystemStatsOutMcastOctets);
        nmhGetFsMIStdIpSystemStatsInBcastPkts ((INT4) u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsInBcastPkts);
        nmhGetFsMIStdIpSystemStatsOutBcastPkts ((INT4) u4VcId,
                                                i4IpSystemStatsIPVersion,
                                                &u4IpSystemStatsOutBcastPkts);
        nmhGetFsMIStdIpSystemStatsDiscontinuityTime ((INT4) u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &u4IpSystemStatsDiscontinuityTime);
        nmhGetFsMIStdIpSystemStatsRefreshRate ((INT4) u4VcId,
                                               i4IpSystemStatsIPVersion,
                                               &u4IpSystemStatsRefreshRate);

        /*Get IP HC counters if HC flag is set */

        if (u1HCFlag != IP_CLI_HC_DISABLED)
        {
            nmhGetFsMIStdIpSystemStatsHCInReceives ((INT4) u4VcId,
                                                    i4IpSystemStatsIPVersion,
                                                    &RetValIpSystemStatsHCInReceives);
            nmhGetFsMIStdIpSystemStatsHCInOctets ((INT4) u4VcId,
                                                  i4IpSystemStatsIPVersion,
                                                  &RetValIpSystemStatsHCInOctets);
            nmhGetFsMIStdIpSystemStatsHCInForwDatagrams ((INT4) u4VcId,
                                                         i4IpSystemStatsIPVersion,
                                                         &RetValIpSystemStatsHCInForwDatagrams);
            nmhGetFsMIStdIpSystemStatsHCInDelivers ((INT4) u4VcId,
                                                    i4IpSystemStatsIPVersion,
                                                    &RetValIpSystemStatsHCInDelivers);
            nmhGetFsMIStdIpSystemStatsHCOutRequests ((INT4) u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &RetValIpSystemStatsHCOutRequests);
            nmhGetFsMIStdIpSystemStatsHCOutForwDatagrams ((INT4) u4VcId,
                                                          i4IpSystemStatsIPVersion,
                                                          &RetValIpSystemStatsHCOutForwDatagrams);
            nmhGetFsMIStdIpSystemStatsHCOutTransmits ((INT4) u4VcId,
                                                      i4IpSystemStatsIPVersion,
                                                      &RetValIpSystemStatsHCOutTransmits);
            nmhGetFsMIStdIpSystemStatsHCOutOctets ((INT4) u4VcId,
                                                   i4IpSystemStatsIPVersion,
                                                   &RetValIpSystemStatsHCOutOctets);
            nmhGetFsMIStdIpSystemStatsHCInMcastPkts ((INT4) u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &RetValIpSystemStatsHCInMcastPkts);
            nmhGetFsMIStdIpSystemStatsHCInMcastOctets ((INT4) u4VcId,
                                                       i4IpSystemStatsIPVersion,
                                                       &RetValIpSystemStatsHCInMcastOctets);
            nmhGetFsMIStdIpSystemStatsHCOutMcastPkts ((INT4) u4VcId,
                                                      i4IpSystemStatsIPVersion,
                                                      &RetValIpSystemStatsHCOutMcastPkts);
            nmhGetFsMIStdIpSystemStatsHCOutMcastOctets ((INT4) u4VcId,
                                                        i4IpSystemStatsIPVersion,
                                                        &RetValIpSystemStatsHCOutMcastOctets);
            nmhGetFsMIStdIpSystemStatsHCInBcastPkts ((INT4) u4VcId,
                                                     i4IpSystemStatsIPVersion,
                                                     &RetValIpSystemStatsHCInBcastPkts);
            nmhGetFsMIStdIpSystemStatsHCOutBcastPkts ((INT4) u4VcId,
                                                      i4IpSystemStatsIPVersion,
                                                      &RetValIpSystemStatsHCOutBcastPkts);

            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInReceives,
                               RetValIpSystemStatsHCInReceives.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInReceives,
                               RetValIpSystemStatsHCInReceives.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInOctets,
                               RetValIpSystemStatsHCInOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInOctets,
                               RetValIpSystemStatsHCInOctets.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInForwDatagrams,
                               RetValIpSystemStatsHCInForwDatagrams.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInForwDatagrams,
                               RetValIpSystemStatsHCInForwDatagrams.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInDelivers,
                               RetValIpSystemStatsHCInDelivers.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInDelivers,
                               RetValIpSystemStatsHCInDelivers.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutRequests,
                               RetValIpSystemStatsHCOutRequests.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutRequests,
                               RetValIpSystemStatsHCOutRequests.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutForwDatagrams,
                               RetValIpSystemStatsHCOutForwDatagrams.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutForwDatagrams,
                               RetValIpSystemStatsHCOutForwDatagrams.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutTransmits,
                               RetValIpSystemStatsHCOutTransmits.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutTransmits,
                               RetValIpSystemStatsHCOutTransmits.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutOctets,
                               RetValIpSystemStatsHCOutOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutOctets,
                               RetValIpSystemStatsHCOutOctets.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInMcastPkts,
                               RetValIpSystemStatsHCInMcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInMcastPkts,
                               RetValIpSystemStatsHCInMcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutMcastPkts,
                               RetValIpSystemStatsHCOutMcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutMcastPkts,
                               RetValIpSystemStatsHCOutMcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutMcastOctets,
                               RetValIpSystemStatsHCOutMcastOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutMcastOctets,
                               RetValIpSystemStatsHCOutMcastOctets.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInBcastPkts,
                               RetValIpSystemStatsHCInBcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInBcastPkts,
                               RetValIpSystemStatsHCInBcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCOutBcastPkts,
                               RetValIpSystemStatsHCOutBcastPkts.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCOutBcastPkts,
                               RetValIpSystemStatsHCOutBcastPkts.lsn);
            FSAP_U8_ASSIGN_HI (&u8IpSystemStatsHCInMcastOctets,
                               RetValIpSystemStatsHCInMcastOctets.msn);
            FSAP_U8_ASSIGN_LO (&u8IpSystemStatsHCInMcastOctets,
                               RetValIpSystemStatsHCInMcastOctets.lsn);
        }
        /* After getting all the values just Print it */
        VcmGetAliasName (u4VcId, au1ContextName);
        CliPrintf (CliHandle, "VRF Name:          %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "----------------\r\n");
        /*Display Ip related Information */
        CliPrintf (CliHandle, "\r\nIP %sStatistics\r\n",
                   ((u1HCFlag) ? "High Count " : ""));
        CliPrintf (CliHandle, "--------------------\r\n");

        if (u1HCFlag == IP_CLI_HC_DISABLED)
        {

            CliPrintf (CliHandle, " Rcvd:  ");
            CliPrintf (CliHandle, "%u total, %u header error discards\r\n",
                       IpCliInfo.IpStats.u4In_rcvs,
                       IpCliInfo.IpStats.u4In_hdr_err);

            CliPrintf (CliHandle,
                       "        %u bad ip address discards, %u unsupported protocol discards\r\n",
                       IpCliInfo.IpStats.u4In_addr_err,
                       IpCliInfo.IpStats.u4Bad_proto);
            CliPrintf (CliHandle, " Frags: ");

            CliPrintf (CliHandle,
                       "%u reassembled, %u timeouts, %u needs reassembly\r\n",
                       IpCliInfo.IpStats.u4Reasm_oks,
                       IpCliInfo.IpStats.u4Reasm_timeout,
                       IpCliInfo.IpStats.u4Reasm_reqs);

            CliPrintf (CliHandle,
                       "        %u fragmented, %u couldn't fragment\r\n",
                       IpCliInfo.IpStats.u4Out_frag_pkts,
                       IpCliInfo.IpStats.u4Frag_fails);

            CliPrintf (CliHandle, " Bcast: ");

            CliPrintf (CliHandle, " Sent:  ");

            CliPrintf (CliHandle, "%u forwarded, %u generated requests\r\n",
                       IpCliInfo.IpStats.u4Forw_attempts,
                       IpCliInfo.IpStats.u4Out_requests);

            CliPrintf (CliHandle, " Drop:  ");

            /*Display the new mib objects */
            CliPrintf (CliHandle,
                       "\r\n\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                       u4IpSystemStatsInDiscards, "InDiscards",
                       u4IpSystemStatsInDelivers, "InDelivers",
                       u4IpSystemStatsInMcastPkts, "InMcastPkts");
            CliPrintf (CliHandle,
                       "\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                       u4IpSystemStatsInTruncatedPkts, "InTruncated",
                       u4IpSystemStatsInOctets, "InOctets",
                       u4IpSystemStatsInNoRoutes, "InNoRoutes");
            CliPrintf (CliHandle,
                       "\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                       u4IpSystemStatsReasmFails, "ReasmFails",
                       u4IpSystemStatsInMcastOctets, "InMcast Octets",
                       u4IpSystemStatsInBcastPkts, "InBcastPkts");
            CliPrintf (CliHandle,
                       "\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                       u4IpSystemStatsOutDiscards, "OutDiscards",
                       u4IpSystemStatsOutMcastPkts, "OutMcastPkts",
                       u4IpSystemStatsOutFragCreates, "OutFrgCreates");
            CliPrintf (CliHandle,
                       "\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                       u4IpSystemStatsOutForwDatagrams, "OutForwDgrms",
                       u4IpSystemStatsOutTransmits, "OutTrnsmits",
                       u4IpSystemStatsOutFragReqds, "OutFrgRqds");
            CliPrintf (CliHandle,
                       "\r\n%5d\t%-15s\t%5d\t%-15s\t%5d\t%-15s\r\n",
                       u4IpSystemStatsOutOctets, "OutOctets",
                       u4IpSystemStatsOutMcastOctets, "OutMcstOctets",
                       u4IpSystemStatsOutBcastPkts, "OutBcstPkts");

            CliPrintf (CliHandle, "\r\n%5d\t%-15s\t%5d\t%-15s\r\n",
                       u4IpSystemStatsDiscontinuityTime, "DiscntTime",
                       u4IpSystemStatsRefreshRate, "RefrshRate");
        }
        else
        {
            /*Display only the High capacity counters */
            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInReceives,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCInOctets, (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle,
                       "%5s\t%-15s\t%5s\t%-15s\t",
                       au1HCTempCount, "InRcvs", au1HCTempCnt, "InOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInForwDatagrams,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCInDelivers,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\r\n%5s\t%-15s\t", au1HCTempCount,
                       "InFwdDgrms", au1HCTempCnt, "InDelivers");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCOutRequests,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutForwDatagrams,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\t%5s\t%-15s\r\n", au1HCTempCount,
                       "OutRequests", au1HCTempCnt, "OutFwdDgrms");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCOutTransmits,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutOctets, (CHR1 *) & au1HCTempCnt);

            CliPrintf (CliHandle,
                       "%5s\t%-15s\t%5s\t%-15s\t",
                       au1HCTempCount, "OutTrnsmits", au1HCTempCnt,
                       "OutOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInMcastPkts,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCInMcastOctets,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\r\n%5s\t%-15s\t", au1HCTempCount,
                       "InMcstPkts", au1HCTempCnt, "InMcstOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCOutMcastPkts,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutMcastOctets,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\t%5s\t%-15s\r\n", au1HCTempCount,
                       "OutMcstPkts", au1HCTempCnt, "OutMcstOctets");

            MEMSET (au1HCTempCount, IP_ZERO, CFA_CLI_U8_STR_LENGTH);
            MEMSET (au1HCTempCnt, IP_ZERO, CFA_CLI_U8_STR_LENGTH);

            FSAP_U8_2STR (&u8IpSystemStatsHCInBcastPkts,
                          (CHR1 *) & au1HCTempCount);
            FSAP_U8_2STR (&u8IpSystemStatsHCOutBcastPkts,
                          (CHR1 *) & au1HCTempCnt);
            CliPrintf (CliHandle, "%5s\t%-15s\t%5s\t%-15s\r\n", au1HCTempCount,
                       "InBcast", au1HCTempCnt, "OutBcast");
        }

        /* Display Icmp related Information */

        if (u1HCFlag == IP_CLI_HC_DISABLED)
        {

            CliPrintf (CliHandle, "\r\nICMP Statistics:\r\n");
            CliPrintf (CliHandle, "----------------\r\n");
            CliPrintf (CliHandle, " Rcvd:  ");

            CliPrintf (CliHandle, "%u total, %u InErrors, ",
                       IpCliInfo.IcmpStats.u4IcmpInMsgs,
                       IpCliInfo.IcmpStats.u4IcmpInErr);

            CliPrintf (CliHandle, " %u unreachable, %u redirects\r\n",
                       IpCliInfo.IcmpStats.u4IcmpInDestUnreach,
                       IpCliInfo.IcmpStats.u4IcmpInRedirects);

            CliPrintf (CliHandle,
                       "        %u time exceeded, %u param problems, ",
                       IpCliInfo.IcmpStats.u4IcmpInTimeExceeds,
                       IpCliInfo.IcmpStats.u4IcmpInParmProbs);

            CliPrintf (CliHandle, " %u quench\r\n",
                       IpCliInfo.IcmpStats.u4IcmpInSrcQuench);

            CliPrintf (CliHandle, "        %u echo, %u echo reply, ",
                       IpCliInfo.IcmpStats.u4IcmpInEchos,
                       IpCliInfo.IcmpStats.u4IcmpInEchosReps);

            CliPrintf (CliHandle, " %u mask requests, %u mask replies, \r\n",
                       IpCliInfo.IcmpStats.u4IcmpInAddrMasks,
                       IpCliInfo.IcmpStats.u4IcmpInAddrMaskReps);

            CliPrintf (CliHandle,
                       "        %u timestamp , %u time stamp reply, \r\n",
                       IpCliInfo.IcmpStats.u4IcmpInTimeStamps,
                       IpCliInfo.IcmpStats.u4IcmpInTimeStampsReps);

            CliPrintf (CliHandle, " Sent:  ");

            CliPrintf (CliHandle, "%u total, %u OutErrors, ",
                       IpCliInfo.IcmpStats.u4IcmpOutMsgs,
                       IpCliInfo.IcmpStats.u4IcmpOutErr);

            CliPrintf (CliHandle, " %u unreachable, %u redirects\r\n",
                       IpCliInfo.IcmpStats.u4IcmpOutDestUnreach,
                       IpCliInfo.IcmpStats.u4IcmpOutRedirects);

            CliPrintf (CliHandle,
                       "        %u time exceeded, %u param problems, ",
                       IpCliInfo.IcmpStats.u4IcmpOutTimeExceeds,
                       IpCliInfo.IcmpStats.u4IcmpOutParmProbs);

            CliPrintf (CliHandle, " %u quench\r\n",
                       IpCliInfo.IcmpStats.u4IcmpOutSrcQuench);

            CliPrintf (CliHandle, "        %u echo, %u echo reply, ",
                       IpCliInfo.IcmpStats.u4IcmpOutEchos,
                       IpCliInfo.IcmpStats.u4IcmpOutEchosReps);

            CliPrintf (CliHandle, " %u mask requests, %u mask replies, \r\n",
                       IpCliInfo.IcmpStats.u4IcmpOutAddrMasks,
                       IpCliInfo.IcmpStats.u4IcmpOutAddrMaskReps);

            CliPrintf (CliHandle,
                       "        %u timestamp , %u time stamp reply, \r\n",
                       IpCliInfo.IcmpStats.u4IcmpOutTimeStamps,
                       IpCliInfo.IcmpStats.u4IcmpOutTimeStampsReps);
        }
    }
    while ((u4ShowAllCxt == TRUE) &&
           (VcmGetNextActiveL3Context (u4PrevVcId, &u4VcId) != VCM_FAILURE));
    /*end of do while */
    return (CLI_SUCCESS);
}

/*********************************************************************
 *  Function Name : IpSetRouting
 *  Description   : Calls Low level Set Routine for Enabling IP routing
 *                  or to Disable it.
 * Input Parameters : 1. CliHandle - CliContext ID                            
 *                    2. i4RoutingStatus - CLI_ENABLE / CLI_DISABLE            
 *                                                                           
 * Output Parameters : None                                                 
 *                                                                           
 * Return Value     :  CLI_SUCCESS/CLI_FAILURE                               
 *********************************************************************/

INT4
IpSetRouting (tCliHandle CliHandle, INT4 i4RoutingStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IpForwarding (&u4ErrorCode, i4RoutingStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIpForwarding (i4RoutingStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetDefaultTTL                                        */
/*                                                                           */
/* Description      : This function is invoked to set the default TTL        */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4MaximumPaths - maximum paths value to set         */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetDefaultTTL (tCliHandle CliHandle, INT4 i4IpDefaultTTL)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IpDefaultTTL (&u4ErrorCode, i4IpDefaultTTL) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIpDefaultTTL (i4IpDefaultTTL) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowRouteInCxt                                       */
/*                                                                           */
/* Description      : This function is invoked to display IP routing table   */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowRouteInCxt (tCliHandle CliHandle, INT1 u1flag, UINT4 u4VcId)
{
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4OutCome = 0;
    INT4                i4IpRouteIfIndex = 0;
    UINT4               u4IpRouteMetric1 = 0;
    UINT4               u4FsIpRouteNextDest = 0;
    UINT4               u4FsIpRouteNextMask = 0;
    UINT4               u4FsIpRouteNextHopNext = 0;
    UINT4               u4FsIpRouteNextStatus = 0;
    INT4                i4FsIpRouteNextProto = 0;
    tRtInfo            *pRt = NULL;
    tRtInfo             InRtInfo;
    INT1                i1GetNextFlag = FALSE;
    INT1                i1FoundFlag = FALSE;
    INT1                i1NetDisplayed = TRUE;
    INT1                i1DisplayFlag = TRUE;
    INT1                i1ShowAllCxt = FALSE;
    INT1                ai1Proto[IP_SIX] = { 0 };
    UINT4               u4MaskBits;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CHR1               *pu1NetAddress = NULL;
    INT1               *pi1IfName = NULL;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];

    UINT4               u4CurrCxtId = 0;
    UINT4               u4NextCxtId = 0;
    UINT4               u4CurrRtDestType = INET_ADDR_TYPE_IPV4;
    UINT4               u4NextRtDestType = 0;
    UINT4               u4LocalDestPrefix = 0;
    UINT4               u4BgpNextHop = 0;

    tSNMP_OCTET_STRING_TYPE *pCurrDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    tSNMP_OID_TYPE     *pCurrRoutePolicy = NULL;
    tSNMP_OID_TYPE     *pNextRoutePolicy = NULL;
    UINT4               u4CurrPrefixLength = 0;
    UINT4               u4NextPrefixLength;
    UINT4               u4CurrNextHopType = INET_ADDR_TYPE_IPV4;
    UINT4               u4NextNextHopType = INET_ADDR_TYPE_IPV4;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    tIpConfigInfo       IpIfInfo;
    INT4                i4RtValue;
    INT4                i4RoutePreference = IP_ZERO;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT1               u1OperStatus = 0;

    MEMSET (&IpIfInfo, 0, sizeof (IpIfInfo));
    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&DestAddr, 0, sizeof (DestAddr));

    DestAddr.i4_Length = sizeof (UINT4);
    DestAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LocalDestPrefix;

    pu1NetAddress = (CHR1 *) & au1NetAddress[0];
    pi1IfName = &ai1IfaceName[0];
    CliPrintf (CliHandle,
               "\r\nCodes: C - connected, S - static, R - rip, B - bgp, O - ospf, I - isis, E - ECMP\r\n");
    CliPrintf (CliHandle,
               "IA - OSPF inter area, N1 - OSPF NSSA external type 1,\r\n");
    CliPrintf (CliHandle,
               "N2 - OSPF NSSA external type 2, E1 - OSPF external type 1,\r\n");
    CliPrintf (CliHandle,
               "E2 - OSPF external type 2 L1 - ISIS Level1, L2 - ISIS Level2, ia - ISIS Inter Area\r\n");
    if (u1flag == 1)
    {
        CliPrintf (CliHandle, "BR - Best Route \r\n");
        CliPrintf (CliHandle, "HW - Hardware Status, RE - Reachable Route\r\n");
    }
    CliPrintf (CliHandle, "\r\n");

    if (IpAllocateMemory
        (&pNextDestAddr, &pNextNextHopAddr, &pNextRoutePolicy, &pCurrDestAddr,
         &pCurrNextHopAddr, &pCurrRoutePolicy) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u4VcId == VCM_INVALID_VC)
    {
        i1ShowAllCxt = TRUE;
        if (VcmGetFirstActiveL3Context (&u4VcId) == VCM_FAILURE)
        {
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetFirstIndexFsMIStdInetCidrRouteTable
            ((INT4 *) &u4NextCxtId, (INT4 *) &u4NextRtDestType,
             pNextDestAddr, &u4NextPrefixLength,
             pNextRoutePolicy, (INT4 *) &u4NextNextHopType, pNextNextHopAddr);
        RTM_PROT_LOCK ();

    }
    else
    {

        RTM_PROT_UNLOCK ();

        i4OutCome = nmhGetNextIndexFsMIStdInetCidrRouteTable
            ((INT4) u4VcId, (INT4 *) &u4NextCxtId,
             (INT4) u4CurrRtDestType, (INT4 *) &u4NextRtDestType,
             pCurrDestAddr, pNextDestAddr,
             u4CurrPrefixLength, &u4NextPrefixLength,
             pCurrRoutePolicy, pNextRoutePolicy,
             (INT4) u4CurrNextHopType, (INT4 *) &u4NextNextHopType,
             pCurrNextHopAddr, pNextNextHopAddr);

        RTM_PROT_LOCK ();
        if (u4VcId != u4NextCxtId)
        {
            /*No entries exist for this context */
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
    }

    /*
     * 1. Get first prefix.
     * 2. Get RouteInfo for that prefix, protocol
     *      -- Display all paths for this prefix for this protocol for all
     *         protocols.
     * 3. Get Next Prefix.
     */

    if (i4OutCome == SNMP_SUCCESS)
    {

        VcmGetAliasName (u4NextCxtId, au1ContextName);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Vrf Name:          %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "---------\r\n");

        IP_OCTETSTRING_TO_INTEGER ((pNextDestAddr), u4FsIpRouteNextDest);
        u4FsIpRouteNextMask = CfaGetCidrSubnetMask (u4NextPrefixLength);

        InRtInfo.u4DestNet = u4FsIpRouteNextDest;
        InRtInfo.u4DestMask = u4FsIpRouteNextMask;
        RtmApiGetBestRouteEntryInCxt (u4NextCxtId, InRtInfo, &pRt);
        if (pRt != NULL)
        {
            i1GetNextFlag = TRUE;
            i1FoundFlag = TRUE;
        }

        do                        /* untill we have prefixes in routing table */
        {
            do                    /* untill we have an alternate routes for above prefix */
            {
                if (i1FoundFlag == TRUE)
                {
                    u4FsIpRouteNextDest = pRt->u4DestNet;
                    u4FsIpRouteNextMask = pRt->u4DestMask;
                    u4FsIpRouteNextHopNext = pRt->u4NextHop;
                    u4FsIpRouteNextStatus = pRt->u4Flag;

                    i4FsIpRouteNextProto = pRt->u2RtProto;
                    u4NextRtDestType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
                    u4NextPrefixLength =
                        CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
                    u4NextNextHopType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop,
                                               (pNextNextHopAddr));

                    RTM_PROT_UNLOCK ();
                    nmhGetFsMIStdInetCidrRouteIfIndex
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, &i4IpRouteIfIndex);

                    nmhGetFsMIStdInetCidrRouteMetric1
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, (INT4 *) &u4IpRouteMetric1);

                    nmhGetFsMIRtmCommonRoutePreference ((INT4) u4NextCxtId,
                                                        u4FsIpRouteNextDest,
                                                        u4FsIpRouteNextMask,
                                                        IP_ZERO,
                                                        u4FsIpRouteNextHopNext,
                                                        &i4RoutePreference);

                    RTM_PROT_LOCK ();

                    /* If Interface OperStatus is DOWN, don't count route */

                    CfaGetIfOperStatus ((UINT4) i4IpRouteIfIndex,
                                        &u1OperStatus);
                    CfaGetIfUnnumAssocIPIf ((UINT4) i4IpRouteIfIndex,
                                            &u4UnnumAssocIPIf);

                    i4RtValue =
                        CfaIpIfGetIfInfo ((UINT4) i4IpRouteIfIndex, &IpIfInfo);
                    UNUSED_PARAM (i4RtValue);
                    if ((IpIfInfo.u4Addr == 0) && (u4UnnumAssocIPIf != 0))
                    {
                        /*Route learned on unnumbered interface */
                        u4FsIpRouteNextHopNext = 0;
                    }
                    if ((CfaCliGetIfName ((UINT4) i4IpRouteIfIndex,
                                          pi1IfName) == OSIX_SUCCESS)
                        && (u1OperStatus == CFA_IF_UP))
                    {
                        if (i1DisplayFlag == TRUE)
                        {
                            MEMSET (ai1Proto, 0, IP_SIX);
                            IpObtainProtoType (i4FsIpRouteNextProto,
                                               pRt->i4MetricType, ai1Proto);

                            /* Display the protocol */
                            CliPrintf (CliHandle, "%s ", ai1Proto);

                            /*Call macro for IpAddress to String format, to Print */
                            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                                       u4FsIpRouteNextDest);
                            CliPrintf (CliHandle, "%s", pu1NetAddress);

                            /* Obtain the number of mask bits from net mask */
                            u4MaskBits = CliGetMaskBits (u4FsIpRouteNextMask);
                            CliPrintf (CliHandle, "/%-2d ", u4MaskBits);
                            if (u1flag == 1)
                            {
                                if ((u4FsIpRouteNextStatus & RTM_RT_HWSTATUS) ==
                                    RTM_RT_HWSTATUS)
                                {
                                    CliPrintf (CliHandle, "HW  ");
                                }
                                if ((u4FsIpRouteNextStatus & RTM_RT_REACHABLE)
                                    == RTM_RT_REACHABLE)
                                {
                                    CliPrintf (CliHandle, "RE  ");
                                }
                                if ((u4FsIpRouteNextStatus & RTM_BEST_RT) ==
                                    RTM_BEST_RT)
                                {
                                    CliPrintf (CliHandle, "BR  ");
                                }
                            }

                        }
                        else
                        {
                            CliPrintf (CliHandle, "              ");
                        }

                        /* If the next hop is 0 then the destination net is
                         * directly reachable */
                        if (u4FsIpRouteNextHopNext == 0)
                        {
                            if ((i4FsIpRouteNextProto == CIDR_STATIC_ID) &&
                                (pRt->u1BitMask & RTM_PRIV_RT_MASK))
                            {
                                if (*pi1IfName == '0')
                                {
                                    u4PagingStatus =
                                        (UINT4) CliPrintf (CliHandle,
                                                           "is directly connected (private) \r\n");
                                }
                                else
                                {
                                    u4PagingStatus =
                                        (UINT4) CliPrintf (CliHandle,
                                                           "is directly connected, %s (private) \r\n",
                                                           pi1IfName);
                                }
                            }
                            else if (i4FsIpRouteNextProto == OTHERS_ID)    /* rip sink route */
                            {
                                u4PagingStatus =
                                    (UINT4) CliPrintf (CliHandle,
                                                       "is subnetted \r\n");
                            }
                            else
                            {
                                if (*pi1IfName == '0')
                                {
                                    u4PagingStatus =
                                        (UINT4) CliPrintf (CliHandle,
                                                           "is directly connected \r\n");
                                }
                                else
                                {
                                    u4PagingStatus =
                                        (UINT4) CliPrintf (CliHandle,
                                                           "is directly connected, %s\r\n",
                                                           pi1IfName);
                                }

                            }

                            if (u4PagingStatus == CLI_FAILURE)
                            {
                                /* User pressed 'q' at more prompt so break */
                                break;
                            }

                        }
                        else    /* Destination Net is reachable via next hop */
                        {
                            if (pRt->u4Flag & RTM_ECMP_RT)
                            {
                                CliPrintf (CliHandle, "[E] ");
                            }
                            /* Display the distance metric */
                            CliPrintf (CliHandle, "[%d/%u] ", i4RoutePreference,
                                       u4IpRouteMetric1);

                            /*Display the next hop */
                            /*Call macro for IpAddress to String format, to Print */

                            MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
                            if (i4FsIpRouteNextProto == BGP_ID)
                            {
                                u4LocalDestPrefix = u4FsIpRouteNextDest;
                                u4BgpNextHop = u4FsIpRouteNextHopNext;
#ifdef BGP_WANTED
                                Bgp4TrieQueryIndirectNextHOPInCxt (u4NextCxtId,
                                                                   &DestAddr,
                                                                   (UINT2)
                                                                   u4NextPrefixLength,
                                                                   (UINT1
                                                                    *) (VOID *)
                                                                   &u4BgpNextHop);
#endif
                                if (u4BgpNextHop != u4FsIpRouteNextHopNext)
                                {

                                    CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                                               u4BgpNextHop)
                                        u4PagingStatus = CLI_FAILURE;
                                    u4PagingStatus =
                                        (UINT4) CliPrintf (CliHandle, "via %s ",
                                                           pu1NetAddress);
                                    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
                                }

                            }
                            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                                       u4FsIpRouteNextHopNext)
                                if ((i4FsIpRouteNextProto == CIDR_STATIC_ID) &&
                                    (pRt->u1BitMask & RTM_PRIV_RT_MASK))
                            {
                                u4PagingStatus =
                                    (UINT4) CliPrintf (CliHandle,
                                                       "via %s (private)\r\n",
                                                       pu1NetAddress);
                            }
                            else
                            {
                                u4PagingStatus =
                                    (UINT4) CliPrintf (CliHandle, "via %s\r\n",
                                                       pu1NetAddress);
                            }
                            if (u4PagingStatus == CLI_FAILURE)
                            {
                                /* User pressed 'q' at more prompt so break */
                                break;
                            }
                        }
                    }
                    else
                    {
                        i1NetDisplayed = FALSE;
                    }
                }
                if (i1GetNextFlag == TRUE)
                {
                    pRt = pRt->pNextAlternatepath;
                    if ((pRt != NULL)
                        && (pRt->i4Metric1 == (INT4) u4IpRouteMetric1))
                    {
                        i1FoundFlag = TRUE;
                        if (i1NetDisplayed == TRUE)
                        {
                            i1DisplayFlag = FALSE;
                        }
                    }
                    else
                    {
                        i1FoundFlag = FALSE;
                    }
                }
            }
            while (i1FoundFlag == TRUE);    /* alt path for pfx exists */

            i1FoundFlag = TRUE;
            i1NetDisplayed = TRUE;
            /* Get next Prefix and the route info for preferred protocol */
            i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId,
                                               u4FsIpRouteNextDest,
                                               u4FsIpRouteNextMask, &pRt);
            if (i4RetVal == SNMP_SUCCESS)
            {
                i1GetNextFlag = TRUE;
                i1DisplayFlag = TRUE;
            }
            while (i4RetVal == SNMP_FAILURE)
            {
                i1FoundFlag = FALSE;
                if (i1ShowAllCxt == FALSE)
                {
                    i1GetNextFlag = FALSE;
                    break;
                }
                u4CurrCxtId = u4NextCxtId;
                if (VcmGetNextActiveL3Context (u4CurrCxtId, &u4NextCxtId)
                    == VCM_FAILURE)
                {
                    i1GetNextFlag = FALSE;
                    break;
                }
                MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
                InRtInfo.u4DestNet = 0;
                InRtInfo.u4DestMask = 0;

                RtmApiGetBestRouteEntryInCxt (u4NextCxtId, InRtInfo, &pRt);
                if (pRt != NULL)
                {
                    /* display default route in this context */
                    i1GetNextFlag = TRUE;
                    i1FoundFlag = TRUE;
                    i4RetVal = SNMP_SUCCESS;
                }
                else
                {
                    /* display non default route in this context */
                    i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId, 0, 0, &pRt);
                    if (i4RetVal == SNMP_SUCCESS)
                    {
                        i1FoundFlag = TRUE;
                    }
                }
                if (i1FoundFlag == TRUE)
                {
                    VcmGetAliasName (u4NextCxtId, au1ContextName);
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "Vrf Name:          %s\r\n",
                               au1ContextName);
                    CliPrintf (CliHandle, "---------\r\n");
                    i1GetNextFlag = TRUE;
                    i1DisplayFlag = TRUE;
                }
            }

        }
        while (i1GetNextFlag == TRUE);    /* prefixes in RT */
    }
    IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                        pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

INT1
CliIpGetNextRouteInCxt (UINT4 u4VcId, UINT4 u4RouteDest, UINT4 u4RouteMask,
                        tRtInfo ** ppRt)
{
    tRtInfo             InRtInfo;
    tRtInfo            *pOutRtInfo = NULL;

    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
    InRtInfo.u4DestNet = u4RouteDest;
    InRtInfo.u4DestMask = u4RouteMask;

    if (RtmApiGetNextBestRouteEntryInCxt
        (u4VcId, InRtInfo, &pOutRtInfo) == IP_SUCCESS)
    {
        *ppRt = pOutRtInfo;
        return (SNMP_SUCCESS);
    }

    *ppRt = NULL;
    return (SNMP_FAILURE);
}

INT1
CliIpGetNextRoute (UINT4 u4RouteDest, UINT4 u4RouteMask, tRtInfo ** ppRt)
{
    tRtInfo             InRtInfo;
    tRtInfo            *pOutRtInfo = NULL;

    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
    InRtInfo.u4DestNet = u4RouteDest;
    InRtInfo.u4DestMask = u4RouteMask;

    if (IpGetNextBestRouteEntry (InRtInfo, &pOutRtInfo) == IP_SUCCESS)
    {
        *ppRt = pOutRtInfo;
        return (SNMP_SUCCESS);
    }

    *ppRt = NULL;
    return (SNMP_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowFrtEntry                                         */
/*                                                                           */
/* Description      : This function is invoked to display routes from        */
/*                    failed routing table                                   */
/*                                                                           */
/* Input Parameters : 1. CliHandle                                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowFrtEntry (tCliHandle CliHandle)
{
    INT4                i4IpRouteMetric1 = 0;
    INT4                i4IpRouteIfIndex = 0;
    UINT4               u4FsIpRouteNextDest = 0;
    UINT4               u4FsIpRouteNextMask = 0;
    UINT4               u4FsIpRouteNextHopNext = 0;
    INT4                i4FsIpRouteNextProto = 0;
    tRtmFrtInfo        *pRt = NULL;
    tRtmFrtInfo         InRtInfo;
    INT1                ai1Proto[IP_SIX] = { 0 };
    UINT4               u4MaskBits;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    CHR1               *pu1NetAddress = NULL;
    INT1               *pi1IfName = NULL;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    tCfaIfInfo          IfInfo;
    UINT4               u4NextRtDestType = 0;
    UINT4               u4LocalDestPrefix = 0;
    UINT4               u4BgpNextHop = 0;
    UINT4               u4CxtId = 0;
    INT4                i4RoutePreference = 0;
    tSNMP_OCTET_STRING_TYPE *pCurrDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    tSNMP_OID_TYPE     *pCurrRoutePolicy = NULL;
    tSNMP_OID_TYPE     *pNextRoutePolicy = NULL;
    UINT4               u4NextPrefixLength;
    UINT4               u4NextNextHopType = INET_ADDR_TYPE_IPV4;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    tIpConfigInfo       IpIfInfo;

    MEMSET (&IpIfInfo, 0, sizeof (IpIfInfo));
    MEMSET (&InRtInfo, 0, sizeof (tRtmFrtInfo));
    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&DestAddr, 0, sizeof (DestAddr));
    DestAddr.i4_Length = sizeof (UINT4);
    DestAddr.pu1_OctetList = (UINT1 *) (VOID *) &u4LocalDestPrefix;

    pu1NetAddress = (CHR1 *) & au1NetAddress[0];
    pi1IfName = &ai1IfaceName[0];

    CliPrintf (CliHandle,
               "\r\nCodes: C - connected, S - static, R - rip, B - bgp, O - ospf, I - isis\r\n");
    CliPrintf (CliHandle,
               "IA - OSPF inter area, N1 - OSPF NSSA external type 1,\r\n");
    CliPrintf (CliHandle,
               "N2 - OSPF NSSA external type 2, E1 - OSPF external type 1,\r\n");
    CliPrintf (CliHandle,
               "E2 - OSPF external type 2 L1 - ISIS Level1, L2 - ISIS Level2, ia - ISIS Inter Area\r\n");
    CliPrintf (CliHandle, "\r\n");

    if (IpAllocateMemory
        (&pNextDestAddr, &pNextNextHopAddr, &pNextRoutePolicy, &pCurrDestAddr,
         &pCurrNextHopAddr, &pCurrRoutePolicy) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    pRt = RtmGetFirstFrtEntry ();
    if (pRt != NULL)
    {
        u4CxtId = pRt->u4CxtId;
        VcmGetAliasName (u4CxtId, au1ContextName);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Vrf Name:          %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "---------\r\n");
    }
    while (pRt != NULL)
    {
        u4FsIpRouteNextDest = pRt->u4DestNet;
        u4FsIpRouteNextMask = pRt->u4DestMask;
        u4FsIpRouteNextHopNext = pRt->u4NextHop;
        i4FsIpRouteNextProto = pRt->u2RtProto;
        u4NextRtDestType = INET_ADDR_TYPE_IPV4;
        u4CxtId = pRt->u4CxtId;
        IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
        u4NextPrefixLength = CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
        u4NextNextHopType = INET_ADDR_TYPE_IPV4;
        IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop, (pNextNextHopAddr));

        RTM_PROT_UNLOCK ();
        nmhGetFsMIStdInetCidrRouteIfIndex
            ((INT4) u4CxtId, (INT4) u4NextRtDestType,
             pNextDestAddr, u4NextPrefixLength,
             pNextRoutePolicy, (INT4) u4NextNextHopType,
             pNextNextHopAddr, &i4IpRouteIfIndex);

        nmhGetFsMIStdInetCidrRouteMetric1
            ((INT4) u4CxtId, (INT4) u4NextRtDestType,
             pNextDestAddr, u4NextPrefixLength,
             pNextRoutePolicy, (INT4) u4NextNextHopType,
             pNextNextHopAddr, &i4IpRouteMetric1);

        nmhGetFsMIRtmCommonRoutePreference ((INT4) u4CxtId,
                                            u4FsIpRouteNextDest,
                                            u4FsIpRouteNextMask, IP_ZERO,
                                            u4FsIpRouteNextHopNext,
                                            &i4RoutePreference);

        RTM_PROT_LOCK ();

        CfaGetIfInfo ((UINT4) i4IpRouteIfIndex, &IfInfo);
        if ((CfaCliGetIfName ((UINT4) i4IpRouteIfIndex,
                              pi1IfName) == OSIX_SUCCESS)
            && (IfInfo.u1IfOperStatus == CFA_IF_UP))
        {

            MEMSET (ai1Proto, 0, IP_SIX);

            IpObtainProtoType (i4FsIpRouteNextProto,
                               pRt->i4MetricType, ai1Proto);

            /* Display the protocol */
            CliPrintf (CliHandle, "%s ", ai1Proto);

            /*Call macro for IpAddress to String format, to Print */
            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress, u4FsIpRouteNextDest);

            CliPrintf (CliHandle, "%s", pu1NetAddress);

            /* Obtain the number of mask bits from net mask */
            u4MaskBits = CliGetMaskBits (u4FsIpRouteNextMask);
            CliPrintf (CliHandle, "/%-2d ", u4MaskBits);
        }
        if (u4FsIpRouteNextHopNext == 0)
        {
            if ((i4FsIpRouteNextProto == CIDR_STATIC_ID) &&
                (pRt->u1BitMask & RTM_PRIV_RT_MASK))
            {
                if (*pi1IfName == '0')
                {
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle,
                                           "is directly connected (private) \r\n");
                }
                else
                {
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle,
                                           "is directly connected, %s (private) \r\n",
                                           pi1IfName);
                }
            }
            else if (i4FsIpRouteNextProto == OTHERS_ID)    /* rip sink route */
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "is subnetted \r\n");
            }
            else
            {
                if (*pi1IfName == '0')
                {
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle,
                                           "is directly connected \r\n");
                }
                else
                {
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle,
                                           "is directly connected, %s\r\n",
                                           pi1IfName);
                }
            }
            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt so break */
            }
        }
        else                    /* Destination Net is reachable via next hop */
        {
            /* Display the distance metric */
            CliPrintf (CliHandle, "[%d/%u] ", i4RoutePreference,
                       i4IpRouteMetric1);

            /*Display the next hop */
            /*Call macro for IpAddress to String format, to Print */

            MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
            if (i4FsIpRouteNextProto == BGP_ID)
            {
                u4LocalDestPrefix = u4FsIpRouteNextDest;
                u4BgpNextHop = u4FsIpRouteNextHopNext;
#ifdef BGP_WANTED
                Bgp4TrieQueryIndirectNextHOPInCxt (u4CxtId,
                                                   &DestAddr,
                                                   (UINT2)
                                                   u4NextPrefixLength,
                                                   (UINT1 *) (VOID *)
                                                   &u4BgpNextHop);
#endif
                if (u4BgpNextHop != u4FsIpRouteNextHopNext)
                {

                    CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                               u4BgpNextHop)
                        u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle, "via %s ", pu1NetAddress);
                    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
                }

            }
            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                       u4FsIpRouteNextHopNext)
                if ((i4FsIpRouteNextProto == CIDR_STATIC_ID) &&
                    (pRt->u1BitMask & RTM_PRIV_RT_MASK))
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle,
                                       "via %s (private)\r\n", pu1NetAddress);
            }
            else
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "via %s\r\n", pu1NetAddress);
            }
            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt so break */
                break;
            }
        }
        pRt = RtmGetNextFrtEntry (pRt);
        if ((pRt != NULL) && (pRt->u4CxtId != u4CxtId))
        {
            VcmGetAliasName (u4CxtId, au1ContextName);
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "Vrf Name:          %s\r\n", au1ContextName);
            CliPrintf (CliHandle, "---------\r\n");
        }
    }
    IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                        pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowRouteIpAddrInCxt                                      */
/*                                                                           */
/* Description      : This function is invoked to display routes to a        */
/*                    destination network                                    */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IpAddress - IP Address                            */
/*                    3. u4VcId  - VRF ID                                    */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowRouteIpAddrInCxt (tCliHandle CliHandle, INT1 i1MaskFlag, INT4 i4IpAddress,
                        INT4 i4IpMask, UINT4 u4VcId)
{
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4OutCome = 0;
    INT4                i4IpRouteIfIndex = 0;
    INT4                i4Cnt = 0;
    INT4                i4Routes = 0;
    UINT4               u4IpRouteMetric1 = 0;
    UINT4               u4FsIpRouteNextDest = 0;
    UINT4               u4FsIpRouteNextMask = 0;
    UINT4               u4FsIpRouteNextHopNext = 0;
    INT4                i4FsIpRouteNextProto = 0;
    UINT4               u4NextCxtId = 0;
    tRtInfo            *pRt = NULL;
    INT1                i1GetNextFlag = FALSE;
    INT1                i1FoundFlag = FALSE;
    INT1                i1DisplayFlag = TRUE;
    INT1                i1Flag = 0;
    INT1                ai1Proto[IP_SIX] = { 0 };
    INT1                i1ShowAllCxt = FALSE;
    UINT4               u4MaskBits = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4NextRtDestType = 0;
    CHR1               *pu1NetAddress = NULL;
    INT1               *pi1IfName = NULL;
    tRtInfo            *pRtTemp = NULL;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    tRtInfo            *paRtSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    tCfaIfInfo          IfInfo;

    tSNMP_OCTET_STRING_TYPE *pCurrDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextNextHopAddr = NULL;
    tSNMP_OID_TYPE     *pCurrRoutePolicy = NULL;
    tSNMP_OID_TYPE     *pNextRoutePolicy = NULL;
    UINT4               u4CurrCxtId = 0;
    UINT4               u4CurrPrefixLength = 0;
    UINT4               u4NextPrefixLength = 0;
    UINT4               u4CurrNextHopType = INET_ADDR_TYPE_IPV4;
    UINT4               u4CurrRtDestType = INET_ADDR_TYPE_IPV4;
    UINT4               u4NextNextHopType = INET_ADDR_TYPE_IPV4;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    INT4                i4RoutePreference = IP_ZERO;
    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    pu1NetAddress = (CHR1 *) & au1NetAddress[0];
    pi1IfName = &ai1IfaceName[0];

    /*
     * 1. Get first prefix.
     * 2. Get RouteInfo for that prefix for all protocols
     * 3. For the specified  destination network display 
     *    the best paths
     * 4. Get Next Prefix.
     */
    if (IpAllocateMemory
        (&pNextDestAddr, &pNextNextHopAddr, &pNextRoutePolicy, &pCurrDestAddr,
         &pCurrNextHopAddr, &pCurrRoutePolicy) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u4VcId == VCM_INVALID_VC)
    {
        i1ShowAllCxt = TRUE;
        if (VcmGetFirstActiveL3Context (&u4VcId) == VCM_FAILURE)
        {
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetFirstIndexFsMIStdInetCidrRouteTable
            ((INT4 *) &u4NextCxtId, (INT4 *) &u4NextRtDestType,
             pNextDestAddr, &u4NextPrefixLength,
             pNextRoutePolicy, (INT4 *) &u4NextNextHopType, pNextNextHopAddr);
        RTM_PROT_LOCK ();

    }
    else
    {
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetNextIndexFsMIStdInetCidrRouteTable
            ((INT4) u4VcId, (INT4 *) &u4NextCxtId,
             (INT4) u4CurrRtDestType, (INT4 *) &u4NextRtDestType,
             pCurrDestAddr, pNextDestAddr,
             u4CurrPrefixLength, &u4NextPrefixLength,
             pCurrRoutePolicy, pNextRoutePolicy,
             (INT4) u4CurrNextHopType, (INT4 *) &u4NextNextHopType,
             pCurrNextHopAddr, pNextNextHopAddr);

        RTM_PROT_LOCK ();
        if (u4VcId != u4NextCxtId)
        {
            /*No entries exist for this context */
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
    }

    if (i4OutCome == SNMP_FAILURE)
    {
        IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                            pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);

        return (CLI_SUCCESS);
    }
    if (i4Routes == 0)
    {
        CliPrintf (CliHandle,
                   "\r\nCodes: C - connected, S - static, "
                   "R - RIP, B - BGP, O - OSPF\r\n\r\n");
        CliPrintf (CliHandle,
                   "IA - OSPF inter area, N1 - OSPF NSSA external type 1,\r\n");
        CliPrintf (CliHandle,
                   "N2 - OSPF NSSA external type 2, E1 - OSPF external type 1,\r\n");
        CliPrintf (CliHandle, "E2 - OSPF external type 2\r\n");
    }
    VcmGetAliasName (u4NextCxtId, au1ContextName);
    CliPrintf (CliHandle, "\r\nVrf Name:          %s\r\n", au1ContextName);
    CliPrintf (CliHandle, "----------------\r\n");

    pRtTemp = gaRouteInfo;

    CLI_MEMSET (pRtTemp, 0, ROUTE_INFO);
    CLI_MEMSET (&paRtSpecInfo, 0, sizeof (paRtSpecInfo));
    for (i4Cnt = 0; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
    {
        paRtSpecInfo[i4Cnt] = (tRtInfo *) & pRtTemp[i4Cnt];
    }

    IP_OCTETSTRING_TO_INTEGER ((pNextDestAddr), u4FsIpRouteNextDest);
    u4FsIpRouteNextMask = CfaGetCidrSubnetMask (u4NextPrefixLength);
    /* Get all paths from all protos for given prefix. */
    if (IpForwardingTableGetRouteInfoInCxt (u4NextCxtId,
                                            u4FsIpRouteNextDest,
                                            u4FsIpRouteNextMask,
                                            -1, (tRtInfo **) & paRtSpecInfo)
        != IP_FAILURE)
    {

        for (i4Cnt = 1; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
        {
            pRt = (tRtInfo *) paRtSpecInfo[i4Cnt];
            if (i4IpAddress == (INT4) pRt->u4DestNet)
            {
                if ((pRt->u4DestNet == u4FsIpRouteNextDest)
                    && (pRt->u4RowStatus == 1)
                    && (pRt->u4DestMask == u4FsIpRouteNextMask))
                {
                    i1GetNextFlag = TRUE;
                    i1FoundFlag = TRUE;
                    break;
                }
            }
        }
    }
    if (pRt == NULL)
    {
        IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                            pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);

        return CLI_SUCCESS;
    }
    do                            /* until we have prefixes in routing table */
    {
        if (i1MaskFlag == TRUE)
        {
            if ((i4IpAddress == (INT4) pRt->u4DestNet) &&
                (i4IpMask == (INT4) pRt->u4DestMask))
            {
                i1Flag = TRUE;
            }
            else
            {
                i1Flag = FALSE;
            }
        }
        else
        {
            i1Flag = (INT1) IpAddrMatch ((UINT4) i4IpAddress,
                                         pRt->u4DestNet, pRt->u4DestMask);
        }
        if (i1Flag == TRUE)
        {
            do                    /* untill we have an alternate routes for above prefix */
            {
                if (i1FoundFlag == TRUE)
                {

                    u4FsIpRouteNextDest = pRt->u4DestNet;
                    u4FsIpRouteNextMask = pRt->u4DestMask;
                    u4FsIpRouteNextHopNext = pRt->u4NextHop;

                    i4FsIpRouteNextProto = pRt->u2RtProto;
                    u4NextRtDestType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
                    u4NextPrefixLength =
                        CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
                    u4NextNextHopType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop,
                                               (pNextNextHopAddr));

                    RTM_PROT_UNLOCK ();
                    nmhGetFsMIStdInetCidrRouteIfIndex
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, &i4IpRouteIfIndex);

                    nmhGetFsMIStdInetCidrRouteMetric1
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, (INT4 *) &u4IpRouteMetric1);

                    nmhGetFsMIRtmCommonRoutePreference ((INT4) u4NextCxtId,
                                                        u4FsIpRouteNextDest,
                                                        u4FsIpRouteNextMask,
                                                        IP_ZERO,
                                                        u4FsIpRouteNextHopNext,
                                                        &i4RoutePreference);

                    RTM_PROT_LOCK ();
                    /* If Interface OperStatus is DOWN, don't count route */

                    CfaGetIfInfo ((UINT4) i4IpRouteIfIndex, &IfInfo);
                    if ((CfaCliGetIfName ((UINT4) i4IpRouteIfIndex,
                                          pi1IfName) == OSIX_SUCCESS)
                        && (IfInfo.u1IfOperStatus == CFA_IF_UP))
                    {

                        i4Routes++;
                        if (i1DisplayFlag == TRUE)
                        {
                            MEMSET (ai1Proto, 0, IP_SIX);
                            IpObtainProtoType (i4FsIpRouteNextProto,
                                               pRt->i4MetricType, ai1Proto);
                            /* Display the protocol */
                            CliPrintf (CliHandle, "%s ", ai1Proto);

                            /*Call macro for IpAddress to String format, to Print */
                            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                                       u4FsIpRouteNextDest);
                            CliPrintf (CliHandle, "%s", pu1NetAddress);

                            /* Obtain the number of mask bits from net mask */
                            u4MaskBits = CliGetMaskBits (u4FsIpRouteNextMask);
                            CliPrintf (CliHandle, "/%-2d ", u4MaskBits);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "              ");
                        }

                        /* If the next hop is 0 then the destination net is
                         * directly reachable */
                        if (u4FsIpRouteNextHopNext == 0)
                        {
                            u4PagingStatus =
                                (UINT4) CliPrintf (CliHandle,
                                                   "is directly connected, %s\r\n",
                                                   pi1IfName);

                            if (u4PagingStatus == CLI_FAILURE)
                            {
                                /* User pressed 'q' at more prompt so break */
                                break;
                            }
                        }
                        else    /* Destination Net is reachable via next hop */
                        {

                            /* Display the distance metric */
                            CliPrintf (CliHandle, "[%d/%u] ", i4RoutePreference,
                                       u4IpRouteMetric1);

                            /*Display the next hop */
                            /*Call macro for IpAddress to String format, to Print */

                            MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);

                            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                                       u4FsIpRouteNextHopNext)
                                u4PagingStatus =
                                (UINT4) CliPrintf (CliHandle, "via %s\r\n",
                                                   pu1NetAddress);

                            if (u4PagingStatus == CLI_FAILURE)
                            {
                                /* User pressed 'q' at more prompt so break */
                                break;
                            }
                        }
                    }
                }
                if (i1GetNextFlag == TRUE)
                {
                    pRt = pRt->pNextAlternatepath;
                    if ((pRt != NULL)
                        && (pRt->i4Metric1 == (INT4) u4IpRouteMetric1))
                    {
                        i1FoundFlag = TRUE;
                        i1DisplayFlag = FALSE;
                    }
                    else
                    {
                        i1FoundFlag = FALSE;
                    }
                }
            }
            while (i1FoundFlag == TRUE);    /* alt path for pfx exists */
        }
        i1FoundFlag = TRUE;
        /* Get next Prefix and the route info for preferred protocol */
        i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId,
                                           u4FsIpRouteNextDest,
                                           u4FsIpRouteNextMask, &pRt);
        while (i4RetVal == SNMP_FAILURE)
        {
            if (i1ShowAllCxt == FALSE)
            {
                i1GetNextFlag = FALSE;
                break;
            }
            u4CurrCxtId = u4NextCxtId;
            if (VcmGetNextActiveL3Context (u4CurrCxtId, &u4NextCxtId)
                == VCM_FAILURE)
            {
                i1GetNextFlag = FALSE;
                break;
            }

            VcmGetAliasName (u4NextCxtId, au1ContextName);
            CliPrintf (CliHandle, "\r\nVrf Name:          %s\r\n",
                       au1ContextName);
            CliPrintf (CliHandle, "----------------\r\n");

            i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId, 0, 0, &pRt);
        }

        if (i4RetVal == SNMP_SUCCESS)
        {
            i1GetNextFlag = TRUE;
            i1DisplayFlag = TRUE;
            u4FsIpRouteNextDest = pRt->u4DestNet;
            u4FsIpRouteNextMask = pRt->u4DestMask;
            u4FsIpRouteNextHopNext = pRt->u4NextHop;
            i4FsIpRouteNextProto = pRt->u2RtProto;
            u4NextRtDestType = INET_ADDR_TYPE_IPV4;
            IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
            u4NextPrefixLength = CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
            u4NextNextHopType = INET_ADDR_TYPE_IPV4;
            IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop, (pNextNextHopAddr));
        }

    }
    while (i1GetNextFlag == TRUE);    /* prefixes in RT */
    IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                        pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowRouteProtoInCxt                                  */
/*                                                                           */
/* Description      : This function is invoked to display routes for a       */
/*                    particular protocol                                    */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i1Protocol   - Protocol ID                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowRouteProtoInCxt (tCliHandle CliHandle, INT1 i1Protocol, UINT4 u4VcId)
{
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4OutCome = 0;
    INT4                i4IpRouteIfIndex = 0;
    UINT4               u4IpRouteMetric1 = 0;
    UINT4               u4FsIpRouteNextDest = 0;
    UINT4               u4FsIpRouteNextMask = 0;
    UINT4               u4FsIpRouteNextHopNext = 0;
    INT4                i4FsIpRouteNextProto = 0;
    UINT4               u4Cnt = 0;
    tRtInfo            *pRt = NULL;
    tRtInfo             InRtInfo;
    INT1                i1GetNextFlag = FALSE;
    INT1                i1FoundFlag = FALSE;
    INT1                i1DisplayFlag = TRUE;
    INT1                i1ShowAllCxt = FALSE;
    INT1                ai1Proto[IP_SIX] = { 0 };
    UINT4               u4MaskBits = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4NextCxtId = 0;
    UINT4               u4NextRtDestType = 0;
    CHR1               *pu1NetAddress = NULL;
    INT1               *pi1IfName = NULL;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];

    tSNMP_OCTET_STRING_TYPE *pCurrDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextNextHopAddr = NULL;
    tSNMP_OID_TYPE     *pCurrRoutePolicy = NULL;
    tSNMP_OID_TYPE     *pNextRoutePolicy = NULL;
    UINT4               u4CurrRtDestType = INET_ADDR_TYPE_IPV4;
    UINT4               u4CurrCxtId = 0;
    UINT4               u4CurrPrefixLength = 0;
    UINT4               u4NextPrefixLength = 0;
    UINT4               u4CurrNextHopType = INET_ADDR_TYPE_IPV4;
    UINT4               u4NextNextHopType = INET_ADDR_TYPE_IPV4;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    tIpConfigInfo       IpIfInfo;
    INT4                i4RtValue = 0;
    INT4                i4RoutePreference = IP_ZERO;
    UINT1               u1OperStatus = 0;
    if (IpAllocateMemory
        (&pNextDestAddr, &pNextNextHopAddr, &pNextRoutePolicy, &pCurrDestAddr,
         &pCurrNextHopAddr, &pCurrRoutePolicy) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    MEMSET (&IpIfInfo, 0, sizeof (IpIfInfo));

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

    pu1NetAddress = (CHR1 *) & au1NetAddress[0];
    pi1IfName = &ai1IfaceName[0];

    /* 1. Get first prefix. */
    if (u4VcId == VCM_INVALID_VC)
    {
        i1ShowAllCxt = TRUE;
        if (VcmGetFirstActiveL3Context (&u4VcId) == VCM_FAILURE)
        {
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetFirstIndexFsMIStdInetCidrRouteTable
            ((INT4 *) &u4NextCxtId,
             (INT4 *) &u4NextRtDestType, pNextDestAddr,
             &u4NextPrefixLength, pNextRoutePolicy,
             (INT4 *) &u4NextNextHopType, pNextNextHopAddr);
        RTM_PROT_LOCK ();

    }
    else
    {
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetNextIndexFsMIStdInetCidrRouteTable
            ((INT4) u4VcId, (INT4 *) &u4NextCxtId,
             (INT4) u4CurrRtDestType, (INT4 *) &u4NextRtDestType,
             pCurrDestAddr, pNextDestAddr,
             u4CurrPrefixLength, &u4NextPrefixLength,
             pCurrRoutePolicy, pNextRoutePolicy,
             (INT4) u4CurrNextHopType, (INT4 *) &u4NextNextHopType,
             pCurrNextHopAddr, pNextNextHopAddr);
        RTM_PROT_LOCK ();
        if (u4VcId != u4NextCxtId)
        {
            /*No entries exist for this context */
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
    }

    if (i4OutCome == SNMP_FAILURE)
    {
        IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                            pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);

        return (CLI_SUCCESS);
    }

    VcmGetAliasName (u4NextCxtId, au1ContextName);
    CliPrintf (CliHandle, "Vrf Name:          %s\r\n", au1ContextName);
    CliPrintf (CliHandle, "----------------\r\n");

    IP_OCTETSTRING_TO_INTEGER ((pNextDestAddr), u4FsIpRouteNextDest);
    u4FsIpRouteNextMask = CfaGetCidrSubnetMask (u4NextPrefixLength);
    InRtInfo.u4DestNet = u4FsIpRouteNextDest;
    InRtInfo.u4DestMask = u4FsIpRouteNextMask;
    RtmApiGetBestRouteEntryInCxt (u4NextCxtId, InRtInfo, &pRt);
    if (pRt != NULL)
    {
        i1GetNextFlag = TRUE;
        i1FoundFlag = TRUE;
    }

    CliPrintf (CliHandle, "\r\n");

    do                            /* untill we have prefixes in routing table */
    {

        if ((pRt != NULL) && (i1Protocol == pRt->u2RtProto))
        {
            do                    /* untill we have an alternate routes for above prefix */
            {
                if (i1FoundFlag == TRUE)
                {

                    u4Cnt++;
                    u4FsIpRouteNextDest = pRt->u4DestNet;
                    u4FsIpRouteNextMask = pRt->u4DestMask;
                    u4FsIpRouteNextHopNext = pRt->u4NextHop;

                    i4FsIpRouteNextProto = pRt->u2RtProto;
                    u4NextRtDestType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
                    u4NextPrefixLength =
                        CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
                    u4NextNextHopType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop,
                                               (pNextNextHopAddr));
                    RTM_PROT_UNLOCK ();
                    nmhGetFsMIStdInetCidrRouteIfIndex
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, &i4IpRouteIfIndex);

                    nmhGetFsMIStdInetCidrRouteMetric1
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, (INT4 *) &u4IpRouteMetric1);

                    nmhGetFsMIRtmCommonRoutePreference ((INT4) u4NextCxtId,
                                                        u4FsIpRouteNextDest,
                                                        u4FsIpRouteNextMask,
                                                        IP_ZERO,
                                                        u4FsIpRouteNextHopNext,
                                                        &i4RoutePreference);
                    RTM_PROT_LOCK ();

                    /* If Interface OperStatus is DOWN, don't count route */

                    i4RtValue =
                        CfaIpIfGetIfInfo ((UINT4) i4IpRouteIfIndex, &IpIfInfo);
                    UNUSED_PARAM (i4RtValue);
                    if (IpIfInfo.u4Addr == 0)
                    {
                        /*router learned on unnumbered interface */
                        u4FsIpRouteNextHopNext = 0;
                    }

                    CfaGetIfOperStatus ((UINT4) i4IpRouteIfIndex,
                                        &u1OperStatus);

                    if ((CfaCliGetIfName ((UINT4) i4IpRouteIfIndex,
                                          pi1IfName) == OSIX_SUCCESS)
                        && (u1OperStatus == CFA_IF_UP))
                    {
                        if (i1DisplayFlag == TRUE)
                        {
                            MEMSET (ai1Proto, 0, IP_SIX);
                            IpObtainProtoType (i4FsIpRouteNextProto,
                                               pRt->i4MetricType, ai1Proto);
                            /* Display the protocol */
                            CliPrintf (CliHandle, "\r%s ", ai1Proto);

                            /*Call macro for IpAddress to String format, to Print */
                            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                                       u4FsIpRouteNextDest);
                            CliPrintf (CliHandle, "%s", pu1NetAddress);

                            /* Obtain the number of mask bits from net mask */
                            u4MaskBits = CliGetMaskBits (u4FsIpRouteNextMask);
                            CliPrintf (CliHandle, "/%-2d ", u4MaskBits);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "              ");
                        }

                        /* If the next hop is 0 then the destination net is
                         * directly reachable */
                        if (u4FsIpRouteNextHopNext == 0)
                        {
                            if ((i4FsIpRouteNextProto == CIDR_STATIC_ID) &&
                                (pRt->u1BitMask & RTM_PRIV_RT_MASK))
                            {
                                u4PagingStatus =
                                    (UINT4) CliPrintf (CliHandle,
                                                       "is directly connected, %s (private) \r\n",
                                                       pi1IfName);
                            }
                            else
                            {
                                u4PagingStatus =
                                    (UINT4) CliPrintf (CliHandle,
                                                       "is directly connected, %s\r\n",
                                                       pi1IfName);
                            }
                            if (u4PagingStatus == CLI_FAILURE)
                            {
                                /* User pressed 'q' at more prompt so break */
                                break;
                            }
                        }
                        else    /* Destination Net is reachable via next hop */
                        {

                            if (pRt->u4Flag & RTM_ECMP_RT)
                            {
                                CliPrintf (CliHandle, "[E] ");
                            }

                            /* Display the distance metric */
                            CliPrintf (CliHandle, "[%d", i4RoutePreference);
                            CliPrintf (CliHandle, "/%u] ", u4IpRouteMetric1);

                            /*Display the next hop */
                            /*Call macro for IpAddress to String format, to Print */

                            MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);

                            CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress,
                                                       u4FsIpRouteNextHopNext);
                            if ((i4FsIpRouteNextProto == CIDR_STATIC_ID) &&
                                (pRt->u1BitMask & RTM_PRIV_RT_MASK))
                            {
                                u4PagingStatus =
                                    (UINT4) CliPrintf (CliHandle,
                                                       "via %s (private)\r\n",
                                                       pu1NetAddress);
                            }
                            else
                            {
                                u4PagingStatus =
                                    (UINT4) CliPrintf (CliHandle, "via %s\r\n",
                                                       pu1NetAddress);
                            }

                            if (u4PagingStatus == CLI_FAILURE)
                            {
                                /* User pressed 'q' at more prompt so break */
                                break;
                            }
                        }
                    }
                }
                if (i1GetNextFlag == TRUE)
                {
                    pRt = pRt->pNextAlternatepath;
                    if ((pRt != NULL)
                        && (pRt->i4Metric1 == (INT4) u4IpRouteMetric1))
                    {
                        i1FoundFlag = TRUE;
                        i1DisplayFlag = FALSE;
                    }
                    else
                    {
                        i1FoundFlag = FALSE;
                    }
                }
            }
            while (i1FoundFlag == TRUE);    /* alt path for pfx exists */
        }
        i1FoundFlag = TRUE;
        /* Get next Prefix and the route info for preferred protocol */
        i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId,
                                           u4FsIpRouteNextDest,
                                           u4FsIpRouteNextMask, &pRt);
        while (i4RetVal == SNMP_FAILURE)
        {
            if (i1ShowAllCxt == FALSE)
            {
                i1GetNextFlag = FALSE;
                break;
            }
            u4CurrCxtId = u4NextCxtId;
            if (VcmGetNextActiveL3Context (u4CurrCxtId, &u4NextCxtId)
                == VCM_FAILURE)
            {
                i1GetNextFlag = FALSE;
                break;
            }

            VcmGetAliasName (u4NextCxtId, au1ContextName);
            CliPrintf (CliHandle, "\r\nVrf Name:          %s\r\n",
                       au1ContextName);
            CliPrintf (CliHandle, "----------------\r\n");
            i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId, 0, 0, &pRt);
        }

        if (i4RetVal == SNMP_SUCCESS)
        {
            i1GetNextFlag = TRUE;
            i1DisplayFlag = TRUE;
            u4FsIpRouteNextDest = pRt->u4DestNet;
            u4FsIpRouteNextMask = pRt->u4DestMask;
            u4FsIpRouteNextHopNext = pRt->u4NextHop;
            i4FsIpRouteNextProto = pRt->u2RtProto;
            u4NextRtDestType = INET_ADDR_TYPE_IPV4;
            IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
            u4NextPrefixLength = CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
            u4NextNextHopType = INET_ADDR_TYPE_IPV4;
            IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop, (pNextNextHopAddr));
        }

    }
    while (i1GetNextFlag == TRUE);    /* prefixes in RT */
    IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                        pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowRouteSummaryInCxt                                */
/*                                                                           */
/* Description      : This function is invoked to display IP routing table   */
/*                    summary                                                */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowRouteSummaryInCxt (tCliHandle CliHandle, UINT4 u4VcId)
{
    INT4                i4RetVal = 0;
    INT4                i4OutCome = 0;
    INT4                i4IpRouteIfIndex = 0;
    UINT4               u4IpRouteMetric1 = 0;
    UINT4               u4FsIpRouteNextDest = 0;
    UINT4               u4FsIpRouteNextMask = 0;
    INT4                i4FsIpRouteNextProto = 0;
    INT4                i4Cnt = 0;
    UINT4               u4NextRtDestType = 0;
    tRtInfo            *pRt = NULL;
    INT1                i1GetNextFlag = FALSE;
    INT1                i1FoundFlag = FALSE;
    INT1                i1ShowAllCxt = FALSE;
    UINT4               u4ConnectedRoutes = 0;
    UINT4               u4StaticRoutes = 0;
    UINT4               u4RipRoutes = 0;
    UINT4               u4BgpRoutes = 0;
    UINT4               u4OspfRoutes = 0;
    UINT4               u4IsisRoutes = 0;
    UINT4               u4TotalRoutes = 0;
    UINT4               u4FrtCount = 0;
    tRtInfo            *pRtTemp = NULL;
    tRtInfo            *paRtSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    tCfaIfInfo          IfInfo;

    tSNMP_OCTET_STRING_TYPE *pCurrDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextNextHopAddr = NULL;
    tSNMP_OID_TYPE     *pCurrRoutePolicy = NULL;
    tSNMP_OID_TYPE     *pNextRoutePolicy = NULL;
    UINT4               u4NextCxtId = 0;
    UINT4               u4CurrCxtId = 0;
    UINT4               u4CurrPrefixLength = 0;
    UINT4               u4NextPrefixLength = 0;
    UINT4               u4CurrNextHopType = INET_ADDR_TYPE_IPV4;
    UINT4               u4NextNextHopType = INET_ADDR_TYPE_IPV4;
    UINT4               u4CurrRtDestType = INET_ADDR_TYPE_IPV4;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT4               u4EcmpRoutes = 0;

    if (IpAllocateMemory
        (&pNextDestAddr, &pNextNextHopAddr, &pNextRoutePolicy, &pCurrDestAddr,
         &pCurrNextHopAddr, &pCurrRoutePolicy) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (u4VcId == VCM_INVALID_VC)
    {
        i1ShowAllCxt = TRUE;
        if (VcmGetFirstActiveL3Context (&u4VcId) == VCM_FAILURE)
        {
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetFirstIndexFsMIStdInetCidrRouteTable
            ((INT4 *) &u4NextCxtId, (INT4 *) &u4NextRtDestType,
             pNextDestAddr, &u4NextPrefixLength,
             pNextRoutePolicy, (INT4 *) &u4NextNextHopType, pNextNextHopAddr);
        RTM_PROT_LOCK ();

    }
    else
    {
        RTM_PROT_UNLOCK ();

        i4OutCome = nmhGetNextIndexFsMIStdInetCidrRouteTable
            ((INT4) u4VcId, (INT4 *) &u4NextCxtId,
             (INT4) u4CurrRtDestType, (INT4 *) &u4NextRtDestType,
             pCurrDestAddr, pNextDestAddr,
             u4CurrPrefixLength, &u4NextPrefixLength,
             pCurrRoutePolicy, pNextRoutePolicy,
             (INT4) u4CurrNextHopType, (INT4 *) &u4NextNextHopType,
             pCurrNextHopAddr, pNextNextHopAddr);
        RTM_PROT_LOCK ();
        if (u4VcId != u4NextCxtId)
        {
            /*No entries exist for this context */
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return CLI_SUCCESS;
        }
    }

    /*
     * 1. Get first prefix.
     * 2. Get RouteInfo for that prefix, protocol
     *      -- Display all paths for this prefix for this protocol for all
     *         protocols.
     * 3. Get Next Prefix.
     */

    if (i4OutCome == SNMP_SUCCESS)
    {
        VcmGetAliasName (u4NextCxtId, au1ContextName);
        CliPrintf (CliHandle, "VRF Name:          %s\r\n", au1ContextName);
        CliPrintf (CliHandle, "----------------\r\n");
        pRtTemp = gaRouteInfo;
        if (pRtTemp == NULL)
        {
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return (CLI_FAILURE);
        }
        CLI_MEMSET (pRtTemp, 0, ROUTE_INFO);
        CLI_MEMSET (&paRtSpecInfo, 0, sizeof (paRtSpecInfo));
        for (i4Cnt = 0; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)

        {
            paRtSpecInfo[i4Cnt] = (tRtInfo *) & pRtTemp[i4Cnt];
        }

        IP_OCTETSTRING_TO_INTEGER ((pNextDestAddr), u4FsIpRouteNextDest);
        u4FsIpRouteNextMask = CfaGetCidrSubnetMask (u4NextPrefixLength);

        /* Get all paths from all protos for given prefix. */
        if (IpForwardingTableGetRouteInfoInCxt (u4NextCxtId,
                                                u4FsIpRouteNextDest,
                                                u4FsIpRouteNextMask,
                                                -1, (tRtInfo **) & paRtSpecInfo)
            != IP_FAILURE)
        {

            /* for each protocol, display all paths for given prefix
             * start with 1st no-null route and follow alt paths and alt
             * routes from other less preferred protocols */
            for (i4Cnt = 1; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
            {
                pRt = (tRtInfo *) paRtSpecInfo[i4Cnt];
                if ((pRt->u4DestNet == u4FsIpRouteNextDest)
                    && (pRt->u4RowStatus == 1)
                    && (pRt->u4DestMask == u4FsIpRouteNextMask))
                {
                    i1GetNextFlag = TRUE;
                    i1FoundFlag = TRUE;
                    break;
                }
                else
                {
                    pRt = NULL;
                }
            }
        }
        do                        /* untill we have prefixes in routing table */
        {
            do                    /* untill we have an alternate routes for above prefix */
            {
                if (i1FoundFlag == TRUE)
                {
                    u4FsIpRouteNextDest = pRt->u4DestNet;
                    u4FsIpRouteNextMask = pRt->u4DestMask;

                    i4FsIpRouteNextProto = pRt->u2RtProto;
                    u4NextRtDestType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
                    u4NextPrefixLength =
                        CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
                    u4NextNextHopType = INET_ADDR_TYPE_IPV4;
                    IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop,
                                               (pNextNextHopAddr));

                    RTM_PROT_UNLOCK ();
                    nmhGetFsMIStdInetCidrRouteIfIndex
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, &i4IpRouteIfIndex);

                    nmhGetFsMIStdInetCidrRouteMetric1
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, (INT4 *) &u4IpRouteMetric1);
                    RTM_PROT_LOCK ();
                    /* If Interface OperStatus is DOWN, don't count route */

                    CfaGetIfInfo ((UINT4) i4IpRouteIfIndex, &IfInfo);
                    if ((IfInfo.u1IfOperStatus == CFA_IF_UP))
                    {
                        /* Increment the number of routes 
                           depending upon the protocol */
                        switch (i4FsIpRouteNextProto)
                        {
                            case CIDR_LOCAL_ID:
                                u4ConnectedRoutes++;
                                break;

                            case CIDR_STATIC_ID:
                                u4StaticRoutes++;
                                break;

                            case RIP_ID:
                                u4RipRoutes++;
                                break;

                            case BGP_ID:
                                u4BgpRoutes++;
                                break;

                            case OSPF_ID:
                                u4OspfRoutes++;
                                break;
                            case ISIS_ID:
                                u4IsisRoutes++;
                            default:
                                break;
                        }
                        u4TotalRoutes++;
                        if (pRt->u4Flag & RTM_ECMP_RT)
                        {
                            u4EcmpRoutes++;
                        }
                    }
                }
                if (i1GetNextFlag == TRUE)
                {
                    pRt = pRt->pNextAlternatepath;
                    if ((pRt != NULL)
                        && (pRt->i4Metric1 == (INT4) u4IpRouteMetric1))
                    {
                        i1FoundFlag = TRUE;
                    }
                    else
                    {
                        i1FoundFlag = FALSE;
                    }
                }
            }
            while (i1FoundFlag == TRUE);    /* alt path for pfx exists */
            i1FoundFlag = TRUE;
            /* Get next Prefix and the route info for preferred protocol */
            i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId,
                                               u4FsIpRouteNextDest,
                                               u4FsIpRouteNextMask, &pRt);

            while (i4RetVal == SNMP_FAILURE)
            {
                if (i1ShowAllCxt == FALSE)
                {
                    i1GetNextFlag = FALSE;
                    break;
                }
                u4CurrCxtId = u4NextCxtId;
                CliPrintf (CliHandle, "\r\nRoute Source    Routes\r\n");
                CliPrintf (CliHandle, "connected         %-32d\r\n",
                           u4ConnectedRoutes);
                CliPrintf (CliHandle, "static            %-32d\r\n",
                           u4StaticRoutes);
                CliPrintf (CliHandle, "rip               %-32d\r\n",
                           u4RipRoutes);
                CliPrintf (CliHandle, "bgp               %-32d\r\n",
                           u4BgpRoutes);
                CliPrintf (CliHandle, "ospf              %-32d\r\n",
                           u4OspfRoutes);
                CliPrintf (CliHandle, "isis              %-32d\r\n",
                           u4IsisRoutes);
                CliPrintf (CliHandle, "Total             %-32d\r\n\r\n",
                           u4TotalRoutes);
                CliPrintf (CliHandle, "Total ECMP routes %-32d\r\n\r\n",
                           u4EcmpRoutes);
                RtmGetFrtTableCount (&u4FrtCount);
                CliPrintf (CliHandle, "Total Failed routes %-32d\r\n\r\n",
                           u4FrtCount);

                u4ConnectedRoutes = 0;
                u4StaticRoutes = 0;
                u4RipRoutes = 0;
                u4BgpRoutes = 0;
                u4OspfRoutes = 0;
                u4IsisRoutes = 0;
                u4TotalRoutes = 0;

                if (VcmGetNextActiveL3Context (u4CurrCxtId, &u4NextCxtId)
                    == VCM_FAILURE)
                {
                    i1GetNextFlag = FALSE;
                    break;
                }
                VcmGetAliasName (u4NextCxtId, au1ContextName);
                CliPrintf (CliHandle, "VRF Name:          %s\r\n",
                           au1ContextName);
                CliPrintf (CliHandle, "----------------\r\n");
                i4RetVal = CliIpGetNextRouteInCxt (u4NextCxtId, 0, 0, &pRt);
            }

            if (i4RetVal == SNMP_SUCCESS)
            {
                i1GetNextFlag = TRUE;
            }

        }
        while (i1GetNextFlag == TRUE);    /* prefixes in RT */
    }

    /* Display the Routing Table summary */
    if (i1ShowAllCxt == FALSE)
    {
        CliPrintf (CliHandle, "\r\nRoute Source    Routes\r\n");
        CliPrintf (CliHandle, "connected         %-32d\r\n", u4ConnectedRoutes);
        CliPrintf (CliHandle, "static            %-32d\r\n", u4StaticRoutes);
        CliPrintf (CliHandle, "rip               %-32d\r\n", u4RipRoutes);
        CliPrintf (CliHandle, "bgp               %-32d\r\n", u4BgpRoutes);
        CliPrintf (CliHandle, "ospf              %-32d\r\n", u4OspfRoutes);
        CliPrintf (CliHandle, "isis              %-32d\r\n", u4IsisRoutes);
        CliPrintf (CliHandle, "Total             %-32d\r\n\r\n", u4TotalRoutes);
        CliPrintf (CliHandle, "Total ECMP routes %-32d\r\n\r\n", u4EcmpRoutes);
    }
    IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                        pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetRoute                                             */
/*                                                                           */
/* Description      : This function is invoked to add a static route         */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - interface index                      */
/*                    3. u4RouteDest - Destination IP address                */
/*                    4. u4RouteMask - Destination IP mask                   */
/*                    5. u4RouteNextHop   - Next hop IP address              */
/*                    6. i4RouteDistance - Distance metric                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetRoute (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4IfaceIndex,
            UINT4 u4IpRouteDest, UINT4 u4IpRouteMask, UINT4 u4IpRouteNextHop,
            INT4 i4IpRouteDistance, INT4 i4RedisCtrlFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4RetStatus = 0;
    INT4                i4RouteProto = 0;
    INT4                i4IpRouteTos = IP_DEF_TOS;
    INT4                i4RtDestType = INET_ADDR_TYPE_IPV4;
    INT4                i4RtNextHopType = INET_ADDR_TYPE_IPV4;
    tSNMP_OCTET_STRING_TYPE RouteDest;
    tSNMP_OCTET_STRING_TYPE RouteNexthop;
    tSNMP_OID_TYPE      RoutePolicy;
    UINT4               u4PrefixLength = 0;
    UINT1               au1RouteDest[MAX_ADDR_LEN];
    UINT4               au4RtPolicy[MAX_ADDR_LEN];
    UINT1               au1NextHop[MAX_ADDR_LEN];
    UINT4               u4RDst = u4IpRouteDest;
    UINT4               u4RNxtHop = u4IpRouteNextHop;
    INT4                i4RouteIfIndex = 0;
    INT4                i4RoutePreference = IP_ZERO;
    INT4                i4RoutePrivateStatus = 0;
    INT4                i4RouteMetric1 = IP_ZERO;
    CHR1               *pu1String = NULL;
    UINT1               au1String[255];
    UINT4               u4MaskBits = 0;

    tRtInfo            *pRt = NULL;
    tRtInfo            *pRtTemp = NULL;
    tRtInfo            *paRtSpecInfo[MAX_ROUTING_PROTOCOLS + 1];

    MEMSET (au1RouteDest, 0, MAX_ADDR_LEN);
    MEMSET (au1NextHop, 0, MAX_ADDR_LEN);
    MEMSET (&au4RtPolicy, 0, sizeof (au4RtPolicy));

    RouteDest.pu1_OctetList = au1RouteDest;
    RouteDest.i4_Length = 0;
    RoutePolicy.pu4_OidList = (UINT4 *) &au4RtPolicy;
    RoutePolicy.u4_Length = 0;
    RouteNexthop.pu1_OctetList = au1NextHop;
    RouteNexthop.i4_Length = 0;
    /* copy the route destination */
    u4RDst = OSIX_HTONL (u4RDst);
    MEMSET (RouteDest.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (RouteDest.pu1_OctetList, &u4RDst, (sizeof (u4RDst)));
    RouteDest.i4_Length = sizeof (u4RDst);
    pu1String = (CHR1 *) & au1String[0];

    /* copy the route pfx length */
    u4PrefixLength = CfaGetCidrSubnetMaskIndex (u4IpRouteMask);
    /* copy the next hop */
    u4RNxtHop = OSIX_HTONL (u4RNxtHop);
    MEMSET (RouteNexthop.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (RouteNexthop.pu1_OctetList, &u4RNxtHop, (sizeof (u4RNxtHop)));
    RouteNexthop.i4_Length = sizeof (u4RNxtHop);

    /*Used to delete  the Entry Created Here
     *in case  any of the Test/Set Operation Fails.
     */
    /*Check whether rowstaus is created or not */
    u4RetStatus =
        (UINT4) nmhGetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId,
                                                  i4RtDestType, &RouteDest,
                                                  u4PrefixLength, &RoutePolicy,
                                                  i4RtNextHopType,
                                                  &RouteNexthop, &i4RowStatus);
    if (u4RetStatus == SNMP_SUCCESS)
    {
        /*Get the protocol ID for the given route */
        if (nmhGetFsMIStdInetCidrRouteProto ((INT4) u4ContextId, i4RtDestType,
                                             &RouteDest, u4PrefixLength,
                                             &RoutePolicy, i4RtNextHopType,
                                             &RouteNexthop,
                                             &i4RouteProto) == SNMP_SUCCESS)
        {
            /*If the route is not static,set the u4RetStatus as SNMP_FAILURE
             *so that a new entry is created for the given route*/
            if (i4RouteProto != CIDR_STATIC_ID)
            {
                pRtTemp = gaRouteInfo;
                if (pRtTemp == NULL)
                {
                    return (CLI_FAILURE);
                }
                CLI_MEMSET (pRtTemp, 0, ROUTE_INFO);
                CLI_MEMSET (&paRtSpecInfo, 0, sizeof (paRtSpecInfo));
                paRtSpecInfo[CIDR_STATIC_ID] =
                    (tRtInfo *) & pRtTemp[CIDR_STATIC_ID];
                if (IpForwardingTableGetRouteInfoInCxt
                    (u4ContextId, u4IpRouteDest, u4IpRouteMask, 3,
                     (tRtInfo **) & paRtSpecInfo[CIDR_STATIC_ID]) != IP_FAILURE)
                {
                    pRt = (tRtInfo *) paRtSpecInfo[CIDR_STATIC_ID];
                    if ((pRt->u4DestNet == u4IpRouteDest)
                        && (pRt->u4RowStatus == 1)
                        && (pRt->u4DestMask == u4IpRouteMask)
                        && (pRt->u4NextHop == u4IpRouteNextHop))
                    {
                        u4RetStatus = SNMP_SUCCESS;
                    }
                    else
                    {
                        u4RetStatus = SNMP_FAILURE;
                    }
                }
                else
                {
                    u4RetStatus = SNMP_FAILURE;
                }
            }
        }
        nmhGetFsMIStdInetCidrRouteIfIndex ((INT4) u4ContextId, i4RtDestType,
                                           &RouteDest, u4PrefixLength,
                                           &RoutePolicy, i4RtNextHopType,
                                           &RouteNexthop, &i4RouteIfIndex);

        nmhGetFsMIRtmCommonRoutePreference ((INT4) u4ContextId, u4IpRouteDest,
                                            u4IpRouteMask, i4IpRouteTos,
                                            u4IpRouteNextHop,
                                            &i4RoutePreference);

        nmhGetFsMIStdInetCidrRouteMetric1 ((INT4) u4ContextId, i4RtDestType,
                                           &RouteDest, u4PrefixLength,
                                           &RoutePolicy, i4RtNextHopType,
                                           &RouteNexthop, &i4RouteMetric1);

        nmhGetFsMIRtmCommonRoutePrivateStatus ((INT4) u4ContextId,
                                               u4IpRouteDest, u4IpRouteMask,
                                               i4IpRouteTos, u4IpRouteNextHop,
                                               &i4RoutePrivateStatus);

        if (i4RouteProto == CIDR_STATIC_ID)
        {
            if (u4IpRouteNextHop == 0)
            {
                if ((i4RouteIfIndex == i4IfaceIndex)
                    && (i4RouteMetric1 == i4IpRouteDistance)
                    && (i4RoutePrivateStatus == i4RedisCtrlFlag))
                    return (SNMP_SUCCESS);
            }
            else
            {
                if ((i4RouteMetric1 == i4IpRouteDistance)
                    && (i4RoutePrivateStatus == i4RedisCtrlFlag))
                    return (SNMP_SUCCESS);
            }
        }
    }

    if (u4RetStatus == SNMP_FAILURE)
    {
        if (MAX_ECMP_PATHS == (RtmCheckEcmpRouteCountForDest (u4ContextId,
                                                              u4IpRouteDest,
                                                              u4IpRouteMask,
                                                              i4IpRouteDistance)))
        {
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpRouteDest);
            u4MaskBits = CliGetMaskBits (u4IpRouteMask);
            mmi_printf ("%%CLI Max ECMP routes already exist for the"
                        " destination [%s/%d]\r\n", pu1String, u4MaskBits);
            return CLI_FAILURE;
        }

        /* user tries to create a static route with AD 255,
         * dont add, just return.
         */
        if (i4IpRouteDistance == MAX_AD)
        {
            return SNMP_SUCCESS;
        }

        /*No Row Exists with this instance , create one */
        if (nmhTestv2FsMIStdInetCidrRouteStatus
            (&u4ErrorCode, (INT4) u4ContextId, i4RtDestType, &RouteDest,
             u4PrefixLength, &RoutePolicy, i4RtNextHopType, &RouteNexthop,
             IPFWD_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop,
                                              IPFWD_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else                        /* Row Exists with this instance */
    {
        if (IPFWD_ACTIVE == i4RowStatus)
        {
            if (i4IpRouteDistance == MAX_AD)
            {
                nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId,
                                                  i4RtDestType, &RouteDest,
                                                  u4PrefixLength, &RoutePolicy,
                                                  i4RtNextHopType,
                                                  &RouteNexthop, IPFWD_DESTROY);
                return CLI_SUCCESS;
            }
            else
            {
                nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId,
                                                  i4RtDestType, &RouteDest,
                                                  u4PrefixLength, &RoutePolicy,
                                                  i4RtNextHopType,
                                                  &RouteNexthop,
                                                  IPFWD_NOT_IN_SERVICE);
            }
        }
    }

    if (i4IfaceIndex != 0)
    {
        if (nmhTestv2FsMIStdInetCidrRouteIfIndex
            (&u4ErrorCode, (INT4) u4ContextId, i4RtDestType, &RouteDest,
             u4PrefixLength, &RoutePolicy, i4RtNextHopType, &RouteNexthop,
             i4IfaceIndex) == SNMP_FAILURE)
        {
            if (u4RetStatus == SNMP_FAILURE)
            {                    /*Destroy the Entry which is created Here */
                nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId,
                                                  i4RtDestType, &RouteDest,
                                                  u4PrefixLength, &RoutePolicy,
                                                  i4RtNextHopType,
                                                  &RouteNexthop, IPFWD_DESTROY);
            }
            return (CLI_FAILURE);
        }

        if (nmhSetFsMIStdInetCidrRouteIfIndex
            ((INT4) u4ContextId, i4RtDestType, &RouteDest, u4PrefixLength,
             &RoutePolicy, i4RtNextHopType,
             &RouteNexthop, i4IfaceIndex) == SNMP_FAILURE)
        {
            if (u4RetStatus == SNMP_FAILURE)
            {                    /*Destroy the Entry which is created Here */
                nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId,
                                                  i4RtDestType, &RouteDest,
                                                  u4PrefixLength, &RoutePolicy,
                                                  i4RtNextHopType,
                                                  &RouteNexthop, IPFWD_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Test the administrative distance value */
    if (nmhTestv2FsMIRtmCommonRoutePreference (&u4ErrorCode,
                                               (INT4) u4ContextId,
                                               u4IpRouteDest, u4IpRouteMask,
                                               i4IpRouteTos, u4IpRouteNextHop,
                                               IP_ONE) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {                        /*Destroy the Entry which is created Here */
            nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop, IPFWD_DESTROY);
        }
        return (CLI_FAILURE);
    }

    /* Set the administrative distance value */
    if (nmhSetFsMIRtmCommonRoutePreference ((INT4) u4ContextId, u4IpRouteDest,
                                            u4IpRouteMask, i4IpRouteTos,
                                            u4IpRouteNextHop,
                                            IP_ONE) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {                        /*Destroy the Entry which is created Here */
            nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop, IPFWD_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4RedisCtrlFlag == IP_DONT_REDIS_ROUTE)
    {
        RTM_PROT_LOCK ();
        if (nmhTestv2FsMIRtmCommonRoutePrivateStatus
            (&u4ErrorCode, (INT4) u4ContextId, u4IpRouteDest, u4IpRouteMask,
             i4IpRouteTos, u4IpRouteNextHop, i4RedisCtrlFlag) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            if (u4RetStatus == SNMP_FAILURE)
            {
                /*Destroy the Entry which is created Here */
                nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId,
                                                  i4RtDestType, &RouteDest,
                                                  u4PrefixLength, &RoutePolicy,
                                                  i4RtNextHopType,
                                                  &RouteNexthop, IPFWD_DESTROY);
            }
            return (CLI_FAILURE);
        }

        if (nmhSetFsMIRtmCommonRoutePrivateStatus
            ((INT4) u4ContextId, u4IpRouteDest, u4IpRouteMask, i4IpRouteTos,
             u4IpRouteNextHop, i4RedisCtrlFlag) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            if (u4RetStatus == SNMP_FAILURE)
            {                    /*Destroy the Entry which is created Here */
                nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId,
                                                  i4RtDestType, &RouteDest,
                                                  u4PrefixLength, &RoutePolicy,
                                                  i4RtNextHopType,
                                                  &RouteNexthop, IPFWD_DESTROY);
            }
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        RTM_PROT_UNLOCK ();
    }

    if (nmhTestv2FsMIStdInetCidrRouteMetric1 (&u4ErrorCode,
                                              (INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop,
                                              i4IpRouteDistance) ==
        SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {                        /*Destroy the Entry which is created Here */
            nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop, IPFWD_DESTROY);
        }
        return (CLI_FAILURE);
    }

    if (nmhSetFsMIStdInetCidrRouteMetric1 ((INT4) u4ContextId, i4RtDestType,
                                           &RouteDest, u4PrefixLength,
                                           &RoutePolicy, i4RtNextHopType,
                                           &RouteNexthop,
                                           i4IpRouteDistance) == SNMP_FAILURE)

    {
        if (u4RetStatus == SNMP_FAILURE)
        {                        /*Destroy the Entry which is created Here */
            nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop, IPFWD_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsMIStdInetCidrRouteStatus
        (&u4ErrorCode, (INT4) u4ContextId, i4RtDestType, &RouteDest,
         u4PrefixLength, &RoutePolicy, i4RtNextHopType, &RouteNexthop,
         IPFWD_ACTIVE) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {
            /*Destroy the Entry which is created Here */
            nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop, IPFWD_DESTROY);
        }
        return (CLI_FAILURE);
    }
    if (nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                          &RouteDest, u4PrefixLength,
                                          &RoutePolicy, i4RtNextHopType,
                                          &RouteNexthop,
                                          IPFWD_ACTIVE) == SNMP_FAILURE)
    {
        if (u4RetStatus == SNMP_FAILURE)
        {                        /*Destroy the Entry which is created Here */
            nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                              &RouteDest, u4PrefixLength,
                                              &RoutePolicy, i4RtNextHopType,
                                              &RouteNexthop, IPFWD_DESTROY);
        }
        CliPrintf (CliHandle, "%% Maximum number of routes reached\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetNoRoute                                           */
/*                                                                           */
/* Description      : This function is invoked to delete a static route which*/
/*                    is reachable through a next hop                        */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - interface index                      */
/*                    2. u4RouteDest - Destination IP address                */
/*                    3. u4RouteMask - Destination IP mask                   */
/*                    4. u4RouteNextHop - next hop address                   */
/*                    5. i4RedisCtrlFlag - Private status                    */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetNoRoute (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4IpRouteDest,
              UINT4 u4IpRouteMask, UINT4 u4IpRouteNextHop, INT4 i4RedisCtrlFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RtDestType = INET_ADDR_TYPE_IPV4;
    INT4                i4RtNextHopType = INET_ADDR_TYPE_IPV4;
    tSNMP_OCTET_STRING_TYPE RouteDest;
    tSNMP_OCTET_STRING_TYPE RouteNexthop;
    tSNMP_OID_TYPE      RoutePolicy;
    UINT4               u4PrefixLength = 0;
    UINT1               au1RouteDest[MAX_ADDR_LEN];
    UINT4               au4RtPolicy[MAX_ADDR_LEN];
    UINT1               au1NextHop[MAX_ADDR_LEN];
    UINT4               u4RDst = u4IpRouteDest;
    UINT4               u4RNxtHop = u4IpRouteNextHop;

    MEMSET (au1RouteDest, 0, MAX_ADDR_LEN);
    MEMSET (au1NextHop, 0, MAX_ADDR_LEN);
    MEMSET (&au4RtPolicy, 0, sizeof (au4RtPolicy));

    RouteDest.pu1_OctetList = au1RouteDest;
    RouteDest.i4_Length = 0;
    RoutePolicy.pu4_OidList = (UINT4 *) &au4RtPolicy;
    RoutePolicy.u4_Length = 0;
    RouteNexthop.pu1_OctetList = au1NextHop;
    RouteNexthop.i4_Length = 0;
    /* copy the route destination */
    u4RDst = OSIX_HTONL (u4RDst);
    MEMSET (RouteDest.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (RouteDest.pu1_OctetList, &u4RDst, (sizeof (u4RDst)));
    RouteDest.i4_Length = sizeof (u4RDst);

    /* copy the route pfx length */
    u4PrefixLength = CfaGetCidrSubnetMaskIndex (u4IpRouteMask);
    /* copy the next hop */
    u4RNxtHop = OSIX_HTONL (u4RNxtHop);
    MEMSET (RouteNexthop.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (RouteNexthop.pu1_OctetList, &u4RNxtHop, (sizeof (u4RNxtHop)));
    RouteNexthop.i4_Length = sizeof (u4RNxtHop);

    UNUSED_PARAM (CliHandle);
    if (i4RedisCtrlFlag == IP_REDIS_ROUTE)
    {
        RTM_PROT_LOCK ();
        if (nmhTestv2FsMIRtmCommonRoutePrivateStatus
            (&u4ErrorCode, (INT4) u4ContextId, u4IpRouteDest, u4IpRouteMask,
             IP_DEF_TOS, u4IpRouteNextHop, i4RedisCtrlFlag) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return CLI_FAILURE;
        }
        if (nmhSetFsMIRtmCommonRoutePrivateStatus
            ((INT4) u4ContextId, u4IpRouteDest, u4IpRouteMask, IP_DEF_TOS,
             u4IpRouteNextHop, i4RedisCtrlFlag) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return CLI_FAILURE;
        }
        RTM_PROT_UNLOCK ();
    }
    if (nmhSetFsMIStdInetCidrRouteStatus ((INT4) u4ContextId, i4RtDestType,
                                          &RouteDest, u4PrefixLength,
                                          &RoutePolicy, i4RtNextHopType,
                                          &RouteNexthop,
                                          IPFWD_DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP_NO_DEST_ROUTE_ERR);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetNoRouteInterface                                  */
/*                                                                           */
/* Description      : This function is invoked to delete a static route which*/
/*                    is directly reachable through an interface             */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - interface index                      */
/*                    2. u4RouteDest - Destination IP address                */
/*                    3. u4RouteMask - Destination IP mask                   */
/*                    4. u4RouteNextHop - next hop address                   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetNoRouteInterface (tCliHandle CliHandle, INT4 i4IfaceIndex,
                       UINT4 u4IpRouteDest, UINT4 u4IpRouteMask,
                       UINT4 u4IpRouteNextHop)
{
    /* Find if the route is present. If yes then if it is directly reachable
     * through an interface then check and the interface through which it
     * reached matched the interface specified by the user then delete the
     * route
     */
    INT4                i4OutCome = 0;
    INT4                i4IpRouteIfIndex = 0;
    INT4                i4Cnt = 0;
    UINT4               u4FsIpRouteNextDest = 0;
    UINT4               u4FsIpRouteNextMask = 0;
    INT4                i4FsIpRouteNextTos = 0;
    UINT4               u4FsIpRouteNextHopNext = 0;
    tRtInfo            *pRt = NULL;
    INT1                i1GetNextFlag = FALSE;
    INT1                i1FoundFlag = FALSE;
    tRtInfo            *pRtTemp = NULL;
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    tRtInfo            *paRtSpecInfo[MAX_ROUTING_PROTOCOLS + 1];

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);

    /* 1. Get first prefix. */

    i4OutCome = nmhGetFirstIndexIpCidrRouteTable (&u4FsIpRouteNextDest,
                                                  &u4FsIpRouteNextMask,
                                                  &i4FsIpRouteNextTos,
                                                  &u4FsIpRouteNextHopNext);

    if (i4OutCome == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP_NO_DEST_ROUTE_ERR);
        return (CLI_FAILURE);
    }

    pRtTemp = gaRouteInfo;
    if (pRtTemp == NULL)
    {
        return (CLI_FAILURE);
    }

    CLI_MEMSET (pRtTemp, 0, ROUTE_INFO);
    CLI_MEMSET (&paRtSpecInfo, 0, sizeof (paRtSpecInfo));

    for (i4Cnt = 0; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
    {
        paRtSpecInfo[i4Cnt] = (tRtInfo *) & pRtTemp[i4Cnt];
    }
    do
    {
        if ((u4FsIpRouteNextDest == u4IpRouteDest) &&
            (u4FsIpRouteNextMask == u4IpRouteMask))
        {
            /* Found a matching prefix */

            /*Get all paths from all static protocol for given prefix. */
            if (IpForwardingTableGetRouteInfo (u4FsIpRouteNextDest,
                                               u4FsIpRouteNextMask,
                                               CIDR_STATIC_ID,
                                               (tRtInfo **) & paRtSpecInfo) !=
                IP_FAILURE)
            {
                pRt = (tRtInfo *) paRtSpecInfo[0];
                do
                {
                    /* Check if the routes NextHop matches with the given 
                     * u4IpRouteNextHop(0), since these are routes which are 
                     * directly connected routes and hence for these routes the 
                     * NextHop address should be zero
                     */
                    if ((pRt->u4DestNet == u4FsIpRouteNextDest)
                        && (pRt->u4RowStatus == 1)
                        && (pRt->u4DestMask == u4FsIpRouteNextMask)
                        && (pRt->u4NextHop == u4IpRouteNextHop))
                    {

                        /* Get the interface index through which this route
                         * is reachable
                         */

                        if (nmhGetIpCidrRouteIfIndex (u4FsIpRouteNextDest,
                                                      u4FsIpRouteNextMask,
                                                      i4FsIpRouteNextTos,
                                                      u4FsIpRouteNextHopNext,
                                                      &i4IpRouteIfIndex) ==
                            SNMP_SUCCESS)
                        {
                            if (i4IfaceIndex == i4IpRouteIfIndex)
                            {
                                /* break */
                                i1FoundFlag = TRUE;
                                i1GetNextFlag = FALSE;
                                break;
                            }
                        }
                    }
                    /* Go to the alternate path if it exists */
                    pRt = pRt->pNextAlternatepath;
                    if (pRt != NULL)
                    {
                        u4FsIpRouteNextDest = pRt->u4DestNet;
                        u4FsIpRouteNextMask = pRt->u4DestMask;
                        i4FsIpRouteNextTos = (INT4) pRt->u4Tos;
                        u4FsIpRouteNextHopNext = pRt->u4NextHop;
                    }
                    else
                    {
                        /* Scanned all alternate paths with this prefix */
                        i1GetNextFlag = FALSE;
                    }
                }
                while (pRt != NULL);
            }
        }
        else
        {

            if (CliIpGetNextRoute (u4FsIpRouteNextDest, u4FsIpRouteNextMask,
                                   &pRt) == SNMP_SUCCESS)
            {
                i1GetNextFlag = TRUE;
                u4FsIpRouteNextDest = pRt->u4DestNet;
                u4FsIpRouteNextMask = pRt->u4DestMask;
                i4FsIpRouteNextTos = (INT4) pRt->u4Tos;
                u4FsIpRouteNextHopNext = pRt->u4NextHop;
            }
            else
            {
                i1GetNextFlag = FALSE;
            }
        }
    }
    while (i1GetNextFlag == TRUE);    /* prefixes in RT */

    if (i1FoundFlag == FALSE)
    {
        CLI_SET_ERR (CLI_IP_NO_DEST_ROUTE_ERR);
        return (CLI_FAILURE);
    }
    if (nmhSetIpCidrRouteStatus (u4IpRouteDest, u4IpRouteMask,
                                 IP_DEF_TOS, u4IpRouteNextHop,
                                 IPFWD_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowArpInfoInCxt                                     */
/*                                                                           */
/* Description      : This function is invoked to display ARP configuration  */
/*                    information                                            */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4ArpCxtId    - VRF Context Id                      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowArpInfoInCxt (tCliHandle CliHandle, UINT4 u4ArpCxtId)
{
    INT4                i4IpArpCacheTimeout = 0;
    INT4                i4IpArpMaxRetries = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4CurArpCxtId = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
        i4RetVal = SNMP_SUCCESS;
    }
    else
    {
        /*Display all contexts' information */
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIArpTable ((INT4 *) &u4ArpCxtId);

    }

    if (i4RetVal == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nARP Configurations:\r\n");
        CliPrintf (CliHandle, "-------------------\r\n");
    }
    while (i4RetVal == SNMP_SUCCESS)
    {

        u4CurArpCxtId = u4ArpCxtId;

        nmhGetFsMIArpMaxRetries ((INT4) u4CurArpCxtId, &i4IpArpMaxRetries);

        nmhGetFsMIArpCacheTimeout ((INT4) u4CurArpCxtId, &i4IpArpCacheTimeout);

        VcmGetAliasName (u4CurArpCxtId, au1ContextName);

        CliPrintf (CliHandle, "VRF Name:  %s\r\n", au1ContextName);
        CliPrintf (CliHandle,
                   "\r\n Maximum number of ARP request retries is %d\r\n",
                   i4IpArpMaxRetries);

        CliPrintf (CliHandle, " ARP cache timeout is %d seconds\r\n\r\n",
                   i4IpArpCacheTimeout);

        i4RetVal =
            nmhGetNextIndexFsMIArpTable ((INT4) u4CurArpCxtId,
                                         (INT4 *) &u4ArpCxtId);

        if (u4ShowAllCxt == FALSE)
        {
            /*Info of one context is displayed */
            break;
        }

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowArpStatsInCxt                                    */
/*                                                                           */
/* Description      : This function is invoked to display ARP statistics     */
/*                    information                                            */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4ArpCxtId    - VRF Context Id                      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowArpStatsInCxt (tCliHandle CliHandle, UINT4 u4ArpCxtId)
{
    tArpCxt            *pArpCxt = NULL;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4CurArpCxtId = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
        i4RetVal = SNMP_SUCCESS;
    }
    else
    {
        /*Display all contexts' information */
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIArpTable ((INT4 *) &u4ArpCxtId);

    }
    if (u4ArpCxtId < MAX_ARP_ENTRIES_LIMIT)
    {
        pArpCxt = ArpGetGlblCxt (u4ArpCxtId);
    }
    else
    {
        return CLI_FAILURE;
    }
    if (i4RetVal == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nARP statistics:\r\n");
        CliPrintf (CliHandle, "-------------------\r\n");
    }
    while ((i4RetVal == SNMP_SUCCESS) && (pArpCxt))
    {

        u4CurArpCxtId = u4ArpCxtId;

        VcmGetAliasName (u4CurArpCxtId, au1ContextName);

        CliPrintf (CliHandle, "\nVRF Name:  %s\r\n", au1ContextName);
        CliPrintf (CliHandle,
                   "\r\nARP packets received              :%5d\n\r",
                   pArpCxt->Arp_stat.u4Total_in_pkts);
        CliPrintf (CliHandle,
                   "Requests for unsupported hardware :%5d\n\r",
                   pArpCxt->Arp_stat.u4Bad_type);
        CliPrintf (CliHandle,
                   "Rejects due to bad length         :%5d\n\r",
                   pArpCxt->Arp_stat.u4Bad_length);
        CliPrintf (CliHandle,
                   "Rejects due to bad address        :%5d\n\r",
                   pArpCxt->Arp_stat.u4Bad_address);
        CliPrintf (CliHandle,
                   "Rejected requests                 :%5d\n\r",
                   pArpCxt->Arp_stat.u4Req_discards);
        CliPrintf (CliHandle,
                   "Requests received                 :%5d\n\r",
                   pArpCxt->Arp_stat.u4In_requests);
        CliPrintf (CliHandle,
                   "Response packets received         :%5d\n\r",
                   pArpCxt->Arp_stat.u4Replies);
        CliPrintf (CliHandle,
                   "ARP requests sent                 :%5d\n\r",
                   pArpCxt->Arp_stat.u4Out_requests);
        CliPrintf (CliHandle,
                   "Packets dropped by ARP            :%5d\n\r",
                   pArpCxt->Arp_stat.u4Out_drops);
        CliPrintf (CliHandle,
                   "Packets replied by ARP            :%5d\n\r",
                   pArpCxt->Arp_stat.u4Out_Replies);
        i4RetVal =
            nmhGetNextIndexFsMIArpTable ((INT4) u4CurArpCxtId,
                                         (INT4 *) &u4ArpCxtId);

        if (u4ArpCxtId < MAX_ARP_ENTRIES_LIMIT)
        {
            pArpCxt = ArpGetGlblCxt (u4ArpCxtId);
        }
        else
        {
            return CLI_FAILURE;
        }

        if (u4ShowAllCxt == FALSE)
        {
            /*Info of one context is displayed */
            break;
        }

    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowArpInCxt                                         */
/*                                                                           */
/* Description      : This function is invoked to display IP ARP table       */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4ArpCxtId - VRF Id                                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowArpInCxt (tCliHandle CliHandle, UINT4 u4ArpCxtId)
{
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4RetVal = VCM_FAILURE;
    INT4                i4NextIpNetToMediaIfIndex;
    UINT4               u4NextCxtId = 0;
    UINT4               u4CurrCxtId = 0;
    UINT4               au4IpIfaces[IPIF_MAX_LOGICAL_IFACES + 1];
    INT4                i4IpNetToMediaType = 0;
    INT4                i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV4;
    CONST CHR1         *au1Status[] = {
        "Other", "Invalid", "Dynamic", "Static", "Pending", "evpn", "Disable"
    };
    UINT4               u4TempCxtId = VCM_INVALID_VC;
    CHR1               *pu1Address = NULL;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1IpAddress[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    UINT1               au1NetToPhysicalIp[MAX_ADDR_LEN];
    UINT1               au1NextNetToPhysicalIp[MAX_ADDR_LEN];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT2               u2Cntr = 0;
    UINT1               u1DisplayHdr = TRUE;

    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddress;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    MEMSET (au1NetToPhysicalIp, ZERO, MAX_ADDR_LEN);
    MEMSET (au1NextNetToPhysicalIp, ZERO, MAX_ADDR_LEN);
    MEMSET (au1PhyAddress, ZERO, MAX_ADDR_LEN);

    IpNetToPhysicalNetAddress.pu1_OctetList = au1NetToPhysicalIp;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetToPhysicalIp;

    IpNetToPhysicalNetAddress.i4_Length = IP_ZERO;
    NextIpNetToPhysicalNetAddress.i4_Length = IP_ZERO;
    OctetStrIpNetToMediaPhysAddress.i4_Length = IP_ZERO;

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[0];

    pu1Address = (CHR1 *) & au1Address[0];
    pi1InterfaceName = &ai1InterfaceName[0];

    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
        i4RetVal = VCM_SUCCESS;
        u4CurrCxtId = u4ArpCxtId;
    }
    else
    {
        /*If valid VRF Id is not passed, then display all contexts' information */
        i4RetVal = VcmGetFirstActiveL3Context (&u4CurrCxtId);
    }

    while (i4RetVal == VCM_SUCCESS)
    {
        if (u4ArpCxtId == VCM_INVALID_VC)
        {
            i4RetVal = VcmGetNextActiveL3Context (u4CurrCxtId, &u4NextCxtId);
        }
        else
        {
            i4RetVal = VCM_FAILURE;
        }

        MEMSET (au4IpIfaces, ZERO,
                (IPIF_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
        if (VcmGetCxtIpIfaceList (u4CurrCxtId, au4IpIfaces) == VCM_FAILURE)
        {
            u4CurrCxtId = u4NextCxtId;
            continue;
        }
        VcmGetAliasName (u4CurrCxtId, au1ContextName);
        u1DisplayHdr = TRUE;

        for (u2Cntr = 1; ((u2Cntr <= au4IpIfaces[0]) &&
                          (u2Cntr < IPIF_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
        {
            i4IpNetToMediaIfIndex = (INT4) au4IpIfaces[u2Cntr];
            i4NetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
            MEMSET (au1NetToPhysicalIp, ZERO, MAX_ADDR_LEN);
            IpNetToPhysicalNetAddress.i4_Length = 0;

            MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
            /* Call CfaCliGetIfName for getting 
             * Interface name corresponding to IfIndex */

            CfaCliGetIfName ((UINT4) i4IpNetToMediaIfIndex, pi1InterfaceName);

            while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
                   (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
                    i4NetToPhysicalNetAddressType,
                    &i4NextIpNetToPhysicalNetAddressType,
                    &IpNetToPhysicalNetAddress, &NextIpNetToPhysicalNetAddress)
                   == SNMP_SUCCESS)
            {
                if (i4IpNetToMediaIfIndex != i4NextIpNetToMediaIfIndex)
                {
                    break;
                }
                i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
                MEMCPY (IpNetToPhysicalNetAddress.pu1_OctetList,
                        NextIpNetToPhysicalNetAddress.pu1_OctetList,
                        NextIpNetToPhysicalNetAddress.i4_Length);
                i4NetToPhysicalNetAddressType
                    = i4NextIpNetToPhysicalNetAddressType;

                if (i4NextIpNetToPhysicalNetAddressType != INET_ADDR_TYPE_IPV4)
                {
                    continue;
                }
                nmhGetFsMIStdIpNetToPhysicalContextId
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, (INT4 *) &u4TempCxtId);

                if ((u4ArpCxtId != VCM_INVALID_VC)
                    && (u4TempCxtId != u4ArpCxtId))
                {
                    /* If ContextId of the current entry does not match with the
                     * specified one, get the next entry */
                    continue;
                }

                if (u1DisplayHdr == TRUE)
                {
                    CliPrintf (CliHandle, "\r\nVRF Id  : %d\r\n", u4CurrCxtId);
                    CliPrintf (CliHandle, "VRF Name: %s", au1ContextName);
                    CliPrintf (CliHandle,
                               "\r\nAddress          Hardware Address   Type  Interface  Mapping\r\n");
                    CliPrintf (CliHandle,
                               "\r-------          ----------------   ----  ---------  -------\r\n");
                    u1DisplayHdr = FALSE;
                }

                MEMSET (au1Address, 0, MAX_ADDR_LEN);
                MEMSET (au1IpAddress, 0, MAX_ADDR_LEN);
                MEMSET (au1PhyAddress, 0, MAX_ADDR_LEN);

                /* Get for PhysicalAddress and NetToMediaType will be
                 * sufficient as other params are already available from
                 * GetNextIndex call itself
                 */

                nmhGetFsMIStdIpNetToPhysicalPhysAddress
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress,
                     &OctetStrIpNetToMediaPhysAddress);

                nmhGetFsMIStdIpNetToPhysicalType
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, &i4IpNetToMediaType);

                /* Since the pending state is not supported in the mib 
                 * we will get the state as ARP_OTHER so that is converted to 
                 * pending state.
                 */
                if (i4IpNetToMediaType == ARP_OTHER)
                {
                    i4IpNetToMediaType = ARP_PENDING;
                }

                IP_GET_IP_ADDR (au1IpAddress,
                                NextIpNetToPhysicalNetAddress.pu1_OctetList);

                CliPrintf (CliHandle, "%-15s  ", au1IpAddress);

                /*Call macro for MAC Address to String format, to Print */
                MEMSET (au1Address, 0, MAX_ADDR_LEN);
                CLI_CONVERT_MAC_TO_DOT_STR (OctetStrIpNetToMediaPhysAddress.
                                            pu1_OctetList,
                                            (UINT1 *) pu1Address);

                CliPrintf (CliHandle, "%-17s  ", pu1Address);

                /* Print the type as ARPA */
                CliPrintf (CliHandle, "ARPA  ");

                /* Print the interface name */
                CliPrintf (CliHandle, "%-9s  ", pi1InterfaceName);

                /* Print the mapping */
                CliPrintf (CliHandle, "%-10s\r\n",
                           au1Status[i4IpNetToMediaType - 1]);
            }
        }
        u4CurrCxtId = u4NextCxtId;
    }
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowArpInterfaceInCxt                                */
/*                                                                           */
/* Description      : This function is invoked to display IP ARP table       */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4ArpCxtId - VRF Id                                 */
/*                    3. i4InterfaceIndex  - interface index                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowArpInterfaceInCxt (tCliHandle CliHandle, UINT4 u4ArpCxtId,
                         INT4 i4InterfaceIndex)
{
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex = 0;
    INT4                i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV4;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4NextArpCxtId = 0;
    UINT4               u4TempCxtId = VCM_INVALID_VC;
    INT4                i4IpNetToMediaType = 0;
    const CHR1         *au1Status[] =
        { "Other", "Invalid", "Dynamic", "Static", "Pending", "Disable" };
    CHR1               *pu1Address = NULL;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1IpAddress[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    UINT1               au1NextNetAddress[MAX_ADDR_LEN];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddress;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    MEMSET (au1NetAddress, ZERO, MAX_ADDR_LEN);
    MEMSET (au1NextNetAddress, ZERO, MAX_ADDR_LEN);

    IpNetToPhysicalNetAddress.pu1_OctetList = au1NetAddress;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetAddress;

    IpNetToPhysicalNetAddress.i4_Length = 0;
    NextIpNetToPhysicalNetAddress.i4_Length = 0;

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[0];

    pu1Address = (CHR1 *) & au1Address[0];
    pi1InterfaceName = &ai1InterfaceName[0];

    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
        (i4InterfaceIndex, &i4NextIpNetToMediaIfIndex,
         i4IpNetToPhysicalNetAddressType, &i4NextIpNetToPhysicalNetAddressType,
         &IpNetToPhysicalNetAddress,
         &NextIpNetToPhysicalNetAddress) == SNMP_SUCCESS)
    {
        /*Call CfaCliGetIfName for getting Interface name
         * corresponding to IfIndex */

        MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
        CfaCliGetIfName ((UINT4) i4NextIpNetToMediaIfIndex, pi1InterfaceName);

        do
        {
            i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
            MEMCPY (IpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.i4_Length);
            i4IpNetToPhysicalNetAddressType =
                i4NextIpNetToPhysicalNetAddressType;

            if (i4NextIpNetToMediaIfIndex != i4InterfaceIndex)
            {
                break;
            }
            if (i4NextIpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
            {
                if (nmhGetFsMIStdIpNetToPhysicalContextId
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, (INT4 *) &u4NextArpCxtId)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                if ((u4ArpCxtId != VCM_INVALID_VC)
                    && (u4NextArpCxtId != u4ArpCxtId))
                {
                    /*If ContextId of the current entry does not match with the
                     * specified one, get the next entry*/
                    continue;
                }
                if (u4NextArpCxtId != u4TempCxtId)
                {
                    VcmGetAliasName (u4NextArpCxtId, au1ContextName);
                    /*Display header */
                    CliPrintf (CliHandle, "\r\nVRF Name: %s\r\n",
                               au1ContextName);
                    CliPrintf (CliHandle,
                               "\r\nAddress          Hardware Address   Type  Interface  Mapping  \r\n");
                    CliPrintf (CliHandle,
                               "\r-------          ----------------   ----  ---------  -------  \r\n");
                    u4TempCxtId = u4NextArpCxtId;
                }

                MEMSET (au1Address, 0, MAX_ADDR_LEN);
                MEMSET (au1IpAddress, 0, MAX_ADDR_LEN);
                MEMSET (au1PhyAddress, 0, MAX_ADDR_LEN);

                /* Get for PhysicalAddress and NetToMediaType will be
                 * sufficient as other params are already available from
                 * GetNextIndex call itself
                 */

                nmhGetFsMIStdIpNetToPhysicalPhysAddress
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress,
                     &OctetStrIpNetToMediaPhysAddress);

                nmhGetFsMIStdIpNetToPhysicalType
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, &i4IpNetToMediaType);

                /* Since the pending state is not supported in the mib 
                 * we will get the state as ARP_OTHER so that is converted to 
                 * pending state.
                 */
                if (i4IpNetToMediaType == ARP_OTHER)
                {
                    i4IpNetToMediaType = ARP_PENDING;
                }

                IP_GET_IP_ADDR (au1IpAddress,
                                NextIpNetToPhysicalNetAddress.pu1_OctetList);
                CliPrintf (CliHandle, "%-15s  ", au1IpAddress);

                /*Call macro for MAC Address to String format, to Print */

                CLI_CONVERT_MAC_TO_DOT_STR (OctetStrIpNetToMediaPhysAddress.
                                            pu1_OctetList,
                                            (UINT1 *) pu1Address);

                CliPrintf (CliHandle, "%-17s  ", pu1Address);

                /* Print the type as ARPA */
                CliPrintf (CliHandle, "ARPA  ");

                /* Print the interface name */
                CliPrintf (CliHandle, "%-9s  ", pi1InterfaceName);

                /* Print the mapping */
                u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                                    "%-10s\r\n",
                                                    au1Status[i4IpNetToMediaType
                                                              - 1]);
                if (u4PagingStatus == CLI_FAILURE)
                {
                    /* User pressed 'q' at more prompt so break */
                    break;
                }
            }

        }
        while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
               (i4IpNetToMediaIfIndex,
                &i4NextIpNetToMediaIfIndex,
                i4IpNetToPhysicalNetAddressType,
                &i4NextIpNetToPhysicalNetAddressType,
                &IpNetToPhysicalNetAddress,
                &NextIpNetToPhysicalNetAddress) != SNMP_FAILURE);
    }
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowProxyArpInfo                                     */
/*                                                                           */
/* Description      : This function is invoked to display Proxy ARP status   */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowProxyArpInfoInCxt (tCliHandle CliHandle, UINT4 u4ArpCxtId)
{
#ifdef IP_WANTED
    INT4                i4CxtId = IP_ZERO;
    UINT4               u4ShowAllCxt = IP_ZERO;
#endif
    UINT4               u4PagingStatus = IP_ZERO;
    INT4                i4Index = IP_ZERO;
    INT4                i4PrevIndex = IP_ZERO;
    INT4                i4ProxyArp = IP_ZERO;
    INT1                ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
#ifdef IP_WANTED
    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /*Display all contexts' information */
        u4ShowAllCxt = TRUE;
    }
#endif
    if (nmhGetFirstIndexFsMIStdIpifTable (&i4Index) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nPROXY ARP Status \r\n");
    CliPrintf (CliHandle, "---------------- \r\n");
    do
    {

        if (nmhGetFsMIStdIpProxyArpAdminStatus (i4Index, &i4ProxyArp) ==
            SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        CfaCliGetIfName ((UINT4) i4Index, ai1InterfaceName);
#ifdef IP_WANTED
        nmhGetFsMIFsIpifContextId (i4Index, &i4CxtId);
        if ((u4ShowAllCxt == FALSE) && (i4CxtId != (INT4) u4ArpCxtId))
        {
            i4PrevIndex = i4Index;
            continue;
        }
#endif
        if (i4ProxyArp == ARP_PROXY_ENABLE)
        {
            u4PagingStatus =
                (UINT4) CliPrintf (CliHandle, "%-10s : Enabled\r\n",
                                   ai1InterfaceName);
        }
        else
        {
            u4PagingStatus =
                (UINT4) CliPrintf (CliHandle, "%-10s : Disabled\r\n",
                                   ai1InterfaceName);
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4PrevIndex = i4Index;
    }
    while (nmhGetNextIndexFsMIStdIpifTable (i4PrevIndex, &i4Index) !=
           SNMP_FAILURE);
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (u4ArpCxtId);

#endif

    CliPrintf (CliHandle, "--------------------- \r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowArpIpAddressInCxt                                */
/*                                                                           */
/* Description      : This function is invoked to display ARP table entries  */
/*                    for a specified IP address                             */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4ArpCxtId - VRF Id                                 */
/*                    3. u4IpAddress  - IP address                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowArpIpAddressInCxt (tCliHandle CliHandle, UINT4 u4ArpCxtId,
                         UINT4 u4IpAddress)
{
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex = 0;
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV4;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4IpNetToMediaType = 0;
    CHR1               *pu1Address = NULL;
    INT1               *pi1InterfaceName = NULL;
    const CHR1         *au1Status[] = {
        "Other", "Invalid", "Dynamic", "Static", "Pending", "Disable"
    };
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1IpAddress[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    UINT1               au1NetToPhysicalIp[MAX_ADDR_LEN];
    UINT1               au1NextNetToPhysicalIp[MAX_ADDR_LEN];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    INT1                i1UnLock = FALSE;
    INT1                i1PrintFlag = FALSE;
    UINT4               u4TempCxtId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4NextArpCxtId = 0;
    INT4                i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
    UINT2               u2Cntr = 0;
    UINT1               u1Flag = FALSE;
    UINT4               au4IpIfaces[IPIF_MAX_LOGICAL_IFACES + 1];

    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;
    tSNMP_OCTET_STRING_TYPE NetAddress;
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddress;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    MEMSET (au4IpIfaces, ZERO, (IPIF_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
    MEMSET (au1Address, 0, MAX_ADDR_LEN);
    MEMSET (au1PhyAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (au1NetToPhysicalIp, 0, MAX_ADDR_LEN);
    MEMSET (au1NextNetToPhysicalIp, 0, MAX_ADDR_LEN);

    NetAddress.pu1_OctetList = au1NetAddress;
    IpNetToPhysicalNetAddress.pu1_OctetList = au1NetToPhysicalIp;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetToPhysicalIp;

    NetAddress.i4_Length = 0;
    IpNetToPhysicalNetAddress.i4_Length = 0;
    NextIpNetToPhysicalNetAddress.i4_Length = 0;

    u4IpAddress = OSIX_HTONL (u4IpAddress);
    MEMSET (NetAddress.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (NetAddress.pu1_OctetList, &u4IpAddress, (sizeof (u4IpAddress)));
    NetAddress.i4_Length = sizeof (u4IpAddress);

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[0];

    pu1Address = (CHR1 *) & au1Address[0];
    pi1InterfaceName = &ai1InterfaceName[0];

    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /*If valid VRF Id is not passed, then display all contexts' information */
        u4ShowAllCxt = TRUE;
    }

    CliPrintf (CliHandle,
               "\r\nAddress          Hardware Address   Type  Interface  Mapping     VRF Name \r\n");
    CliPrintf (CliHandle,
               "-------          ----------------   ----  ---------  -------    --------\r\n");
    if (VcmGetCxtIpIfaceList (u4ArpCxtId, au4IpIfaces) == VCM_FAILURE)
    {
        return CLI_FAILURE;
    }

    for (u2Cntr = 1;
         ((u2Cntr <= au4IpIfaces[0]) && (u2Cntr < IPIF_MAX_LOGICAL_IFACES + 1));
         u2Cntr++)

    {
        if (u1Flag == TRUE)
        {
            break;
        }
        i4IpNetToMediaIfIndex = (INT4) au4IpIfaces[u2Cntr];
        i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
        MEMSET (au1NetToPhysicalIp, ZERO, MAX_ADDR_LEN);
        IpNetToPhysicalNetAddress.i4_Length = 0;

        while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
               (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
                i4IpNetToPhysicalNetAddressType,
                &i4NextIpNetToPhysicalNetAddressType,
                &IpNetToPhysicalNetAddress, &NextIpNetToPhysicalNetAddress)
               == SNMP_SUCCESS)

        {
            if (i4IpNetToMediaIfIndex != i4NextIpNetToMediaIfIndex)
            {
                break;
            }
            i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
            MEMCPY (IpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.i4_Length);
            i4IpNetToPhysicalNetAddressType
                = i4NextIpNetToPhysicalNetAddressType;

            if ((i4NextIpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
                && (MEMCMP (NextIpNetToPhysicalNetAddress.pu1_OctetList,
                            NetAddress.pu1_OctetList,
                            NextIpNetToPhysicalNetAddress.i4_Length) == 0))
            {
                u1Flag = TRUE;
                if (nmhGetFsMIStdIpNetToPhysicalContextId
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, (INT4 *) &u4NextArpCxtId)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (u4ShowAllCxt == FALSE && u4NextArpCxtId != u4ArpCxtId)
                {
                    /*If ContextId of the current entry does not match with the
                     * specified one, get the next entry*/
                    continue;
                }

                if (u4NextArpCxtId != u4TempCxtId)
                {
                    VcmGetAliasName (u4NextArpCxtId, au1ContextName);
                    /*Display header */
                    u4TempCxtId = u4NextArpCxtId;
                }
                i1PrintFlag = TRUE;

                /* Get for PhysicalAddress and NetToMediaType will be
                 * sufficient as other params are already available from
                 * GetNextIndex call itself
                 */

                nmhGetFsMIStdIpNetToPhysicalPhysAddress
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress,
                     &OctetStrIpNetToMediaPhysAddress);

                nmhGetFsMIStdIpNetToPhysicalType
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, &i4IpNetToMediaType);

                /* Since the pending state is not supported in the mib 
                 * we will get the state as ARP_OTHER so that is converted to 
                 * pending state.
                 */
                if (i4IpNetToMediaType == ARP_OTHER)
                {
                    i4IpNetToMediaType = ARP_PENDING;
                }

                MEMSET (au1IpAddress, 0, MAX_ADDR_LEN);
                IP_GET_IP_ADDR (au1IpAddress,
                                NextIpNetToPhysicalNetAddress.pu1_OctetList);
                CliPrintf (CliHandle, "%-15s  ", au1IpAddress);

                /*Call macro for MAC Address to String format, to Print */

                MEMSET (au1Address, 0, MAX_ADDR_LEN);
                CLI_CONVERT_MAC_TO_DOT_STR (OctetStrIpNetToMediaPhysAddress.
                                            pu1_OctetList,
                                            (UINT1 *) pu1Address);

                CliPrintf (CliHandle, "%-17s  ", pu1Address);

                /* Print the type as ARPA */
                CliPrintf (CliHandle, "ARPA  ");

                /*Call CfaCliGetIfName for getting Interface name 
                 * corresponding to IfIndex */
                CfaCliGetIfName ((UINT4) i4NextIpNetToMediaIfIndex,
                                 pi1InterfaceName);

                /* Print the interface name */
                CliPrintf (CliHandle, "%-9s  ", pi1InterfaceName);

                /* Print the mapping */
                CliPrintf (CliHandle, "%-10s",
                           au1Status[i4IpNetToMediaType - 1]);
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "  %s\r\n", au1ContextName);

                if (u4PagingStatus == CLI_FAILURE)
                {
                    i1UnLock = TRUE;
                    break;
                }
            }

        }
    }
    if ((CFA_OOB_MGMT_INTF_ROUTING == TRUE) && (i1UnLock == FALSE))
    {
        IpShowLinuxVlanArpIpAddress (CliHandle, u4IpAddress, i1PrintFlag);
    }
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowArpMacAddrInCxt                                  */
/*                                                                           */
/* Description      : This function is invoked to display ARP table entries  */
/*                    for a specified MAC address                            */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4ArpCxtId - VRF Id                                 */
/*                    3. pu1MacAddr - MAC address                            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4                IpShowArpMacAddressInCxt
    (tCliHandle CliHandle, UINT4 u4ArpCxtId, UINT1 *pu1MacAddr)
{
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4NextArpCxtId = 0;
    UINT4               u4TempCxtId = VCM_INVALID_VC;
    UINT4               u4ShowAllCxt = FALSE;
    INT4                i4IpNetToMediaType = 0;
    INT4                i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV4;
    const CHR1         *au1Status[] = {
        "Other", "Invalid", "Dynamic", "Static", "Pending", "Disable"
    };

    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    INT1                i1UnLock = FALSE;
    INT1                i1PrintFlag = FALSE;
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    UINT1               au1IpAddress[MAX_ADDR_LEN];
    UINT1               au1MacStr[MAX_ADDR_LEN];
    UINT1               au1MacAddress[MAC_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    UINT1               au1NextNetAddress[MAX_ADDR_LEN];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT2               u2Cntr = 0;
    UINT4               au4IpIfaces[IPIF_MAX_LOGICAL_IFACES + 1];
    UINT1               u1Flag = FALSE;

    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddress;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    MEMSET (au4IpIfaces, ZERO, (IPIF_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));
    MEMSET (au1PhyAddress, 0, MAX_ADDR_LEN);
    MEMSET (au1MacStr, 0, MAX_ADDR_LEN);
    MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (au1MacAddress, 0, MAC_LEN);
    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (au1NextNetAddress, 0, MAX_ADDR_LEN);

    IpNetToPhysicalNetAddress.pu1_OctetList = au1NetAddress;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetAddress;

    IpNetToPhysicalNetAddress.i4_Length = 0;
    NextIpNetToPhysicalNetAddress.i4_Length = 0;

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[0];

    MEMCPY (au1MacAddress, pu1MacAddr, MAC_LEN);

    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /*If valid VRF Id is not passed, then display all contexts' information */
        u4ShowAllCxt = TRUE;
    }

    CliPrintf (CliHandle,
               "\r\nAddress          Hardware Address   Type  Interface  Mapping     VRF Name\r\n");
    CliPrintf (CliHandle,
               "-------          ----------------   ----  ---------  -------    --------\r\n");

    if (VcmGetCxtIpIfaceList (u4ArpCxtId, au4IpIfaces) == VCM_FAILURE)
    {
        return CLI_FAILURE;
    }

    for (u2Cntr = 1;
         ((u2Cntr <= au4IpIfaces[0]) && (u2Cntr < IPIF_MAX_LOGICAL_IFACES + 1));
         u2Cntr++)
    {
        if (u1Flag == TRUE)
        {
            break;
        }
        i4IpNetToMediaIfIndex = (INT4) au4IpIfaces[u2Cntr];
        i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
        MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
        IpNetToPhysicalNetAddress.i4_Length = 0;

        while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
               (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
                i4IpNetToPhysicalNetAddressType,
                &i4NextIpNetToPhysicalNetAddressType,
                &IpNetToPhysicalNetAddress, &NextIpNetToPhysicalNetAddress)
               == SNMP_SUCCESS)

        {
            i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
            i4IpNetToPhysicalNetAddressType =
                i4NextIpNetToPhysicalNetAddressType;
            MEMCPY (IpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.i4_Length);

            if (i4NextIpNetToPhysicalNetAddressType == INET_ADDR_TYPE_IPV4)
            {
                if (nmhGetFsMIStdIpNetToPhysicalContextId
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, (INT4 *) &u4NextArpCxtId)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                if ((u4ShowAllCxt == FALSE) && (u4NextArpCxtId != u4ArpCxtId))
                {
                    /*If ContextId of the current entry does not match with the
                       specified one, get the next entry */
                    continue;
                }

                nmhGetFsMIStdIpNetToPhysicalPhysAddress
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress,
                     &OctetStrIpNetToMediaPhysAddress);

                if (MEMCMP (OctetStrIpNetToMediaPhysAddress.pu1_OctetList,
                            pu1MacAddr, MAC_LEN) != 0)
                {
                    continue;
                }
                MEMSET (ai1InterfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);

                if (u4NextArpCxtId != u4TempCxtId)
                {
                    VcmGetAliasName (u4NextArpCxtId, au1ContextName);
                    /*Display header */
                    u4TempCxtId = u4NextArpCxtId;
                }
                i1PrintFlag = TRUE;

                /* Get for PhysicalAddress and NetToMediaType will be
                 * sufficient as other params are already available from
                 * GetNextIndex call itself
                 */

                nmhGetFsMIStdIpNetToPhysicalType
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, &i4IpNetToMediaType);

                /* Since the pending state is not supported in the mib 
                 * we will get the state as ARP_OTHER so that is converted to 
                 * pending state.
                 */
                if (i4IpNetToMediaType == ARP_OTHER)
                {
                    i4IpNetToMediaType = ARP_PENDING;
                }

                MEMSET (au1IpAddress, 0, MAX_ADDR_LEN);
                IP_GET_IP_ADDR (au1IpAddress,
                                NextIpNetToPhysicalNetAddress.pu1_OctetList);
                CliPrintf (CliHandle, "%-15s  ", au1IpAddress);
                u1Flag = TRUE;
                /*Call macro for MAC Address to String format, to Print */
                CLI_CONVERT_MAC_TO_DOT_STR (OctetStrIpNetToMediaPhysAddress.
                                            pu1_OctetList, au1MacStr);

                CliPrintf (CliHandle, "%-17s  ", au1MacStr);

                /* Print the type as ARPA */
                CliPrintf (CliHandle, "ARPA  ");

                /*Call CfaCliGetIfName for getting 
                 * Interface name corresponding to IfIndex */
                CfaCliGetIfName ((UINT4) i4NextIpNetToMediaIfIndex,
                                 ai1InterfaceName);

                /* Print the interface name */
                CliPrintf (CliHandle, "%-9s  ", ai1InterfaceName);

                CliPrintf (CliHandle, "%-10s",
                           au1Status[i4IpNetToMediaType - 1]);
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "  %s\r\n", au1ContextName);

                if (u4PagingStatus == CLI_FAILURE)
                {
                    i1UnLock = TRUE;
                    break;
                }
            }

        }
    }
    if ((CFA_OOB_MGMT_INTF_ROUTING == TRUE) && (i1UnLock == FALSE))
    {
        IpShowLinuxVlanArpMacAddress (CliHandle, au1MacAddress, i1PrintFlag);
    }

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowArpSummaryInCxt                                  */
/*                                                                           */
/* Description      : This function is invoked to display IP ARP table       */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4ArpCxtId -VRF Id                                  */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowArpSummaryInCxt (tCliHandle CliHandle, UINT4 u4ArpCxtId)
{
    UINT4               u4PendArpEntryCount = 0;
    UINT4               u4ArpEntryCount = 0;
#ifdef IP_WANTED
    UINT4               u4LnxVlanPendArpEntryCount = 0;
    UINT4               u4LnxVlanArpEntryCount = 0;
#endif
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4ArpPrevCxtId = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    if (u4ArpCxtId != VCM_INVALID_VC)
    {
        if (VcmIsL3VcExist (u4ArpCxtId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF \r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /*Display all contexts' information */
        u4ShowAllCxt = TRUE;
        if (VcmGetFirstActiveL3Context (&u4ArpCxtId) == ARP_FAILURE)
        {
            return CLI_SUCCESS;
        }

    }

    do
    {
        u4ArpPrevCxtId = u4ArpCxtId;
        if (u4ArpCxtId < MAX_ARP_ENTRIES_LIMIT)
        {
            ArpGetPendingCacheEntryCountInCxt (u4ArpCxtId,
                                               &u4PendArpEntryCount);
            ArpGetCacheEntryCountInCxt (u4ArpCxtId, &u4ArpEntryCount);
        }
        else
        {
            return CLI_FAILURE;
        }
#ifdef IP_WANTED
        if (CFA_OOB_MGMT_INTF_ROUTING == TRUE)
        {
            IpGetLinuxArpCount (&u4LnxVlanArpEntryCount,
                                &u4LnxVlanPendArpEntryCount);
        }

        u4ArpEntryCount += u4LnxVlanArpEntryCount;
        u4PendArpEntryCount += u4LnxVlanPendArpEntryCount;
#endif
        VcmGetAliasName (u4ArpCxtId, au1ContextName);
        CliPrintf (CliHandle, "\r\nVRF Name:     %s\r\n", au1ContextName);

        CliPrintf (CliHandle,
                   "\r\n%u IP ARP entries, with %u of them incomplete\r\n\r\n",
                   u4ArpEntryCount, u4PendArpEntryCount);

        if (u4ShowAllCxt == FALSE)
        {
            /*Info of specified context is displayed */
            break;
        }

    }
    while (VcmGetNextActiveL3Context (u4ArpPrevCxtId, &u4ArpCxtId) !=
           VCM_FAILURE);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetArp                                               */
/*                                                                           */
/* Description      : This function is invoked to add an ARP cache entry     */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - interface index                      */
/*                    3. u4IpAddress - IP address                            */
/*                    4. pu1PhysAddr - MAC address                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetArp (tCliHandle CliHandle, INT4 i4IfaceIndex, UINT4 u4IpAddress,
          UINT1 *pu1PhysAddress, UINT4 u4CurrContext)
{
    tSNMP_OCTET_STRING_TYPE IpNetToMediaPhysAddr;
    UINT4               u4ErrorCode = 0;
    UINT1               au1PhyAddress[CLI_MAC_ADDR_SIZE];
    UINT4               u4ExIfIndex = 0;
    UINT4               u4ContextId = 0;
    tNetIpv4IfInfo      NetIpIfInfo;
    t_ARP_CACHE         ArpEntry;
    UINT4               u4Port = 0;

    MEMSET (au1PhyAddress, 0, CLI_MAC_ADDR_SIZE);

    CLI_CONVERT_DOT_STR_TO_ARRAY (pu1PhysAddress, au1PhyAddress);
    IpNetToMediaPhysAddr.pu1_OctetList = au1PhyAddress;

    /*
       CLI_CONVERT_DOT_STR_TO_ARRAY (pu1PhysAddress,
       IpNetToMediaPhysAddr.pu1_OctetList); 
     */

    IpNetToMediaPhysAddr.i4_Length = (INT4) CliOctetLen (pu1PhysAddress);

    if (u4CurrContext != VCM_INVALID_VC)
    {
        /* Get the current context and verify if the given interface index
         * is mapped to the set context */

        VcmGetContextIdFromCfaIfIndex ((UINT4) i4IfaceIndex, &u4ContextId);

        if (u4ContextId != u4CurrContext)
        {
            CliPrintf (CliHandle, "\r\n%% Configured interface is not mapped"
                       " to the given context\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IpNetToMediaPhysAddress (&u4ErrorCode,
                                          i4IfaceIndex,
                                          u4IpAddress,
                                          &IpNetToMediaPhysAddr) ==
        SNMP_FAILURE)

    {
        if (u4ErrorCode != SNMP_ERR_INCONSISTENT_NAME)
        {
            CliPrintf (CliHandle, "\r\n%% ARP entry addition failed\r\n");
            return (CLI_FAILURE);
        }
        if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfaceIndex,
                                       &u4Port) == NETIPV4_FAILURE)
        {
            CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
            return CLI_FAILURE;
        }
        if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) != NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
            return CLI_FAILURE;
        }

        ARP_IFACE_TYPE ((UINT1) NetIpIfInfo.u4IfType);
        if (ArpLookupWithIndex ((UINT4) i4IfaceIndex, u4IpAddress, &ArpEntry) ==
            ARP_FAILURE)
        {
            CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
            return CLI_FAILURE;
        }
        if (ArpEntry.u2Port == (UINT2) u4Port)
        {
            return (CLI_SUCCESS);
        }
        if (NetIpv4GetCfaIfIndexFromPort ((UINT4) ArpEntry.u2Port,
                                          &u4ExIfIndex) == NETIPV4_FAILURE)
        {
            CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
            return (CLI_FAILURE);
        }
        nmhSetIpNetToMediaType ((INT4) u4ExIfIndex, u4IpAddress, ARP_INVALID);
    }
    if (nmhSetIpNetToMediaPhysAddress (i4IfaceIndex,
                                       u4IpAddress,
                                       &IpNetToMediaPhysAddr) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% ARP entry addition failed\r\n");
        return (CLI_FAILURE);
    }

    /*Nothing To Set for NetToMediaNetAddress */
    if (nmhSetIpNetToMediaType (i4IfaceIndex,
                                u4IpAddress,
                                (INT4) (ARP_STATIC)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% ARP entry addition failed\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetNoArp                                             */
/*                                                                           */
/* Description      : This function is invoked to delete an ARP cache entry  */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4IpAddress - IP address                            */
/*                    3. u4ContextId - Context Id                            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetNoArp (tCliHandle CliHandle, UINT4 u4IpAddress, UINT4 u4ContextId)
{
    UINT4               u4IfIndex = 0;

    ArpGetIntfForAddr (u4ContextId, u4IpAddress, &u4IfIndex);

    if ((nmhSetIpNetToMediaType ((INT4) u4IfIndex, u4IpAddress,
                                 ARP_INVALID)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP_INVALID_ARP_ENTRY_ERR);
        return (CLI_FAILURE);
    }
    UNUSED_PARAM (CliHandle);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetArpCacheTimeout                                   */
/*                                                                           */
/* Description      : This function is invoked to set the ARP cache timeout  */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4ArpCacheTimeout - Timeout value to set            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetArpCacheTimeout (tCliHandle CliHandle, INT4 i4ArpCacheTimeout)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsArpCacheTimeout (&u4ErrorCode,
                                    i4ArpCacheTimeout) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsArpCacheTimeout (i4ArpCacheTimeout) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_FAILURE);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetArpMaxRetries                                     */
/*                                                                           */
/* Description      : This function is invoked to set the maximum number of  */
/*                    ARP max retries.                                       */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4ArpMaxRetries - Arp max retries value             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetArpMaxRetries (tCliHandle CliHandle, INT4 i4ArpMaxRetries)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsArpMaxRetries (&u4ErrorCode,
                                  i4ArpMaxRetries) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsArpMaxRetries (i4ArpMaxRetries) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowInfoInCxt                                             */
/*                                                                           */
/* Description      : This function is invoked to display IP configuration   */
/*                    parameters                                             */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowInfoInCxt (tCliHandle CliHandle, UINT4 u4VcId)
{
    INT4                i4GetVal = 0;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4PrevVcId = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    tIpCliInfo          IpCliInfo;

    if (u4VcId == VCM_INVALID_VC)
    {
        if (VcmGetFirstActiveL3Context (&u4VcId) == VCM_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = TRUE;
    }

    /*If valid VcId is given, the loop gets executed only once, otherwise it loops
     * through all the VRFs*/
    do
    {
        u4PrevVcId = u4VcId;

        VcmGetAliasName (u4VcId, au1ContextName);
        CliPrintf (CliHandle, "\r\nVRF  Name:     %s\r\n", au1ContextName);

        CliPrintf (CliHandle, "\r\nGlobal IP Configuration:\r\n");
        CliPrintf (CliHandle, "------------------------\r\n");

        nmhGetFsMIStdIpForwarding ((INT4) u4VcId, &i4GetVal);
        IpCliInfo.IpStats.i4IpForwEnable = i4GetVal;

        nmhGetFsMIStdIpDefaultTTL ((INT4) u4VcId, &i4GetVal);
        IpCliInfo.IpStats.i4DefaultTtl = i4GetVal;

        if (IpCliInfo.IpStats.i4IpForwEnable == IP_FORW_ENABLE)
        {
            CliPrintf (CliHandle, " IP routing is enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " IP routing is disabled\r\n");
        }

        CliPrintf (CliHandle, " Default TTL is %d\r\n",
                   IpCliInfo.IpStats.i4DefaultTtl);

#ifdef IP_WANTED
        FsIpShowInfoInCxt (CliHandle, (INT4) u4VcId);
#endif
    }
    while ((u4ShowAllCxt == TRUE) &&
           (VcmGetNextActiveL3Context (u4PrevVcId, &u4VcId) != VCM_FAILURE));
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpConfigProxyArp                                       */
/*                                                                           */
/* Description      : This function is invoked to enable/disable proxy       */
/*                    arp in the specified interface                         */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - Interface Index                      */
/*                    3. u1ConfigFlag - ARP_PROXY_ENABLE/ARP_PROXY_DISABLE   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpConfigProxyArp (tCliHandle CliHandle, INT4 i4IfaceIndex, UINT1 u1ConfigFlag)
{
    UINT4               u4ErrorCode;
#ifdef IP_WANTED
    INT4                i4LocalProxyArpStatus = IP_ZERO;
    nmhGetFsIpifLocalProxyArpAdminStatus (i4IfaceIndex, &i4LocalProxyArpStatus);
#endif

#ifdef IP_WANTED
    if ((u1ConfigFlag == ARP_PROXY_DISABLE) &&
        (i4LocalProxyArpStatus == ARP_PROXY_ENABLE))
    {
        CLI_SET_ERR (CLI_IP_INVALID_PARP_STATUS);
        return CLI_FAILURE;
    }
#endif
    if (nmhTestv2FsMIStdIpProxyArpAdminStatus (&u4ErrorCode, i4IfaceIndex,
                                               u1ConfigFlag) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIStdIpProxyArpAdminStatus (i4IfaceIndex, u1ConfigFlag) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpConfigProxyArpSubnetCheck                            */
/*                                                                           */
/* Description      : This function is invoked to enable/disable proxy       */
/*                    arp subnet check                                       */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u1ConfigFlag - ARP_PROXY_ENABLE/ARP_PROXY_DISABLE   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpConfigProxyArpSubnetOption (tCliHandle CliHandle, UINT1 u1ConfigFlag)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsMIStdIpProxyArpSubnetOption (&u4ErrorCode, u1ConfigFlag)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFsMIStdIpProxyArpSubnetOption (u1ConfigFlag);

    return (CLI_SUCCESS);
}

#ifdef IP_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpConfigLocalProxyArp                                  */
/*                                                                           */
/* Description      : This function is invoked to enable/disable local       */
/*                    proxy arp in the specified interface                   */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - Interface Index                      */
/*                    3. u1ConfigFlag - ARP_PROXY_ENABLE/ARP_PROXY_DISABLE   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpConfigLocalProxyArp (tCliHandle CliHandle, INT4 i4IfaceIndex,
                       UINT1 u1ConfigFlag)
{
    UINT4               u4ErrorCode;
    INT4                i4ProxyArpStatus = 0;

    UNUSED_PARAM (CliHandle);

    nmhGetFsIpifProxyArpAdminStatus (i4IfaceIndex, &i4ProxyArpStatus);
    if ((u1ConfigFlag == ARP_PROXY_ENABLE) &&
        (i4ProxyArpStatus == ARP_PROXY_DISABLE))
    {
        CLI_SET_ERR (CLI_IP_INVALID_LPARP_STATUS);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIpifLocalProxyArpAdminStatus (&u4ErrorCode, i4IfaceIndex,
                                                 u1ConfigFlag) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIpifLocalProxyArpAdminStatus (i4IfaceIndex, u1ConfigFlag) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpShowRunningConfigInCxt                           */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the FSIP  Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
IpShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
    IpShowRunningConfigScalarsInCxt (CliHandle, u4ContextId);
    IpShowRunningConfigTablesInCxt (CliHandle, u4ContextId);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IpShowRunningConfigScalarsInCxt                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Ip for    */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - VRF Id                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IpShowRunningConfigScalarsInCxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4RetVal = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               u1BangStatus = FALSE;

    CliRegisterLock (CliHandle, ArpProtocolLock, ArpProtocolUnLock);
    ARP_PROT_LOCK ();

    nmhGetFsMIArpMaxRetries ((INT4) u4ContextId, &i4RetVal);

    if (u4ContextId != IP_DEFAULT_CONTEXT)
    {
        /*Get context name from context Id */
        VcmGetAliasName (u4ContextId, au1ContextName);
    }
    if (i4RetVal != ARP_DEF_REQ_RETRIES)
    {
        u1BangStatus = TRUE;
        if (u4ContextId == ARP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "ip arp max-retries %d\r\n", i4RetVal);
        }
        else
        {
            CliPrintf (CliHandle, "ip arp vrf %s max-retries %d\r\n",
                       au1ContextName, i4RetVal);
        }
    }

    nmhGetFsMIArpCacheTimeout ((INT4) u4ContextId, &i4RetVal);
    if (i4RetVal != IP_DEF_ARP_TIMEOUT)
    {
        u1BangStatus = TRUE;
        if (u4ContextId == ARP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "arp timeout %d\r\n", i4RetVal);
        }
        else
        {
            CliPrintf (CliHandle, "arp vrf %s timeout %d\r\n", au1ContextName,
                       i4RetVal);
        }

    }

    CliUnRegisterLock (CliHandle);
    ARP_PROT_UNLOCK ();

    nmhGetFsMIStdIpForwarding ((INT4) u4ContextId, &i4RetVal);
    if (i4RetVal != IP_FORW_ENABLE)
    {
        u1BangStatus = TRUE;
        if (u4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "no ip routing\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "no ip routing vrf %s\r\n", au1ContextName);
        }

    }

    nmhGetFsMIStdIpDefaultTTL ((INT4) u4ContextId, &i4RetVal);
    if (i4RetVal != IP_DEF_TTL)
    {
        u1BangStatus = TRUE;
        if (u4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "ip default-ttl %d\r\n", i4RetVal);
        }
        else
        {
            CliPrintf (CliHandle, "ip default-ttl vrf %s %d\r\n",
                       au1ContextName, i4RetVal);
        }
    }
#ifdef IP_WANTED
    nmhGetFsIcmpSendRedirectEnable (&i4RetVal);
    if (i4RetVal != ICMP_SEND_REDIRECT_ENABLE)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, "no ip icmp redirects\r\n");
    }

    nmhGetFsMIFsIpProxyArpSubnetOption (&i4RetVal);
    if (i4RetVal != ARP_PROXY_ENABLE)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, "no ip proxyarp-subnetoption\r\n");
    }
#endif
    if (u1BangStatus == TRUE)
    {
        CliSetBangStatus (CliHandle, TRUE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IpShowRunningConfigTablesInCxt                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Ip for    */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - VRF Id                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
IpShowRunningConfigTablesInCxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT1               *pi1IfName = NULL;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4RetVal = 0;
    INT4                i4IpNetToMediaIfIndex = 1;
    INT4                i4NextIpNetToMediaIfIndex;
    CHR1               *pu1Address = NULL;
    CHR1               *pu1MaskAddress = NULL;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1PhyAddress[MAX_ADDR_LEN];
    UINT1               au1IpAddress[MAX_ADDR_LEN];
    UINT1               au1NullIpAddress[MAX_ADDR_LEN];
    UINT1               u1BangStatus = FALSE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE OctetStrIpNetToMediaPhysAddress;

    /*For RouteTable */
    UINT4               u4NextContextId = 0;
    UINT1               au1RouteDest[MAX_ADDR_LEN];
    UINT1               au1NextRouteDest[MAX_ADDR_LEN];
    UINT1               au1RouteNextHop[MAX_ADDR_LEN];
    UINT1               au1NextRouteNextHop[MAX_ADDR_LEN];
    INT4                i4RouteDestType = INET_ADDR_TYPE_IPV4;
    INT4                i4NextRouteDestType = INET_ADDR_TYPE_IPV4;
    tSNMP_OCTET_STRING_TYPE RouteDest;
    tSNMP_OCTET_STRING_TYPE NextRouteDest;
    tSNMP_OCTET_STRING_TYPE *pTempRouteDest = NULL;
    UINT4               u4RoutePfxLen = 0;
    UINT4               u4NextRoutePfxLen = 0;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4IpNetToMediaType = 0;

    tSNMP_OID_TYPE     *pu4RoutePolicy = NULL;
    tSNMP_OID_TYPE     *pu4NextRoutePolicy = NULL;

    UINT4               u4RtMask = 0;
    UINT4               u4RtDest = IPVX_ZERO;
    INT4                i4RtTos = IPVX_ZERO;
    UINT4               u4RtNextHop = IPVX_ZERO;
    INT4                i4RouteNextHopType = INET_ADDR_TYPE_IPV4;
    INT4                i4NextRouteNextHopType = INET_ADDR_TYPE_IPV4;
    INT4                i4IpAdminDistance = IP_ZERO;

    tSNMP_OCTET_STRING_TYPE RouteNextHop;
    tSNMP_OCTET_STRING_TYPE NextRouteNextHop;
    tSNMP_OCTET_STRING_TYPE *pTempRouteNextHop = NULL;

    /*For NetToPhysicalTable */
    UINT1               au1NetToPhysicalIp[MAX_ADDR_LEN];
    UINT1               au1NextNetToPhysicalIp[MAX_ADDR_LEN];
    INT4                i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
    INT4                i4NextIpNetToPhysicalNetAddressType =
        INET_ADDR_TYPE_IPV4;
#ifdef ICCH_WANTED
    UINT4               u4IcchPeerIp = 0;
#endif
    tSNMP_OCTET_STRING_TYPE IpNetToPhysicalNetAddress;
    tSNMP_OCTET_STRING_TYPE NextIpNetToPhysicalNetAddress;

    UINT2               u2Cntr = 0;
    UINT4               au4IpIfaces[IPIF_MAX_LOGICAL_IFACES + 1];

    MEMSET (au4IpIfaces, ZERO, (IPIF_MAX_LOGICAL_IFACES + 1) * sizeof (UINT4));

    pu4RoutePolicy = alloc_oid (MAX_OID_LEN);
    if (pu4RoutePolicy == NULL)
    {
        CliSetBangStatus (CliHandle, FALSE);
        return CLI_FAILURE;
    }
    pu4NextRoutePolicy = alloc_oid (MAX_OID_LEN);
    if (pu4NextRoutePolicy == NULL)
    {
        free_oid (pu4RoutePolicy);
        CliSetBangStatus (CliHandle, FALSE);
        return CLI_FAILURE;
    }
    pu4RoutePolicy->u4_Length = 0;
    pu4NextRoutePolicy->u4_Length = 0;
    MEMSET (pu4RoutePolicy->pu4_OidList, 0,
            sizeof (UINT4) * SNMP_MAX_OID_LENGTH);
    MEMSET (pu4NextRoutePolicy->pu4_OidList, 0,
            sizeof (UINT4) * SNMP_MAX_OID_LENGTH);
    MEMSET (au1NetToPhysicalIp, ZERO, MAX_ADDR_LEN);
    MEMSET (au1NextNetToPhysicalIp, ZERO, MAX_ADDR_LEN);

    MEMSET (au1RouteDest, ZERO, MAX_ADDR_LEN);
    MEMSET (au1NextRouteDest, ZERO, MAX_ADDR_LEN);
    MEMSET (au1RouteNextHop, ZERO, MAX_ADDR_LEN);
    MEMSET (au1NextRouteNextHop, ZERO, MAX_ADDR_LEN);

    MEMSET (au1ContextName, ZERO, VCM_ALIAS_MAX_LEN);
    MEMSET (au1NullIpAddress, ZERO, MAX_ADDR_LEN);

    IpNetToPhysicalNetAddress.pu1_OctetList = au1NetToPhysicalIp;
    NextIpNetToPhysicalNetAddress.pu1_OctetList = au1NextNetToPhysicalIp;

    RouteDest.pu1_OctetList = au1RouteDest;
    NextRouteDest.pu1_OctetList = au1NextRouteDest;
    RouteNextHop.pu1_OctetList = au1RouteNextHop;
    NextRouteNextHop.pu1_OctetList = au1NextRouteNextHop;

    IpNetToPhysicalNetAddress.i4_Length = 0;
    NextIpNetToPhysicalNetAddress.i4_Length = 0;

    RouteDest.i4_Length = i4RouteDestType;
    NextRouteDest.i4_Length = i4NextRouteDestType;
    RouteNextHop.i4_Length = i4RouteNextHopType;
    NextRouteNextHop.i4_Length = i4NextRouteNextHopType;

    MEMSET (au1Address, IP_ZERO, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, IP_ZERO, CFA_CLI_MAX_IF_NAME_LEN);

    pu1Address = (CHR1 *) & au1Address[IP_ZERO];
    pi1IfName = &ai1IfaceName[IP_ZERO];

    OctetStrIpNetToMediaPhysAddress.pu1_OctetList = &au1PhyAddress[IP_ZERO];

    /*Get context name from context Id */
    VcmGetAliasName (u4ContextId, au1ContextName);
    nmhGetFsMIRTMIpStaticRouteDistance ((INT4) u4ContextId, &i4IpAdminDistance);

    /* Skip Default administrative distance if not
     * not configured explicitly
     */

    if (i4IpAdminDistance != STATIC_DEFAULT_PREFERENCE)
    {
        u1BangStatus = TRUE;
        /* Default administrative distance for static IPv4 route */
        if (u4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "ip default-distance");

        }
        else
        {
            CliPrintf (CliHandle, "ip default-distance vrf %s", au1ContextName);

        }
        CliPrintf (CliHandle, " %d ", i4IpAdminDistance);
        CliPrintf (CliHandle, "\r\n");
    }

    if (VcmGetCxtIpIfaceList (u4ContextId, au4IpIfaces) == VCM_FAILURE)
    {
        free_oid (pu4RoutePolicy);
        free_oid (pu4NextRoutePolicy);
        CliSetBangStatus (CliHandle, FALSE);
        return CLI_FAILURE;
    }
    for (u2Cntr = 1; ((u2Cntr <= au4IpIfaces[0]) &&
                      (u2Cntr < IPIF_MAX_LOGICAL_IFACES + 1)); u2Cntr++)
    {
        i4IpNetToMediaIfIndex = (INT4) au4IpIfaces[u2Cntr];
        i4IpNetToPhysicalNetAddressType = INET_ADDR_TYPE_IPV4;
        MEMSET (au1NetToPhysicalIp, ZERO, MAX_ADDR_LEN);
        IpNetToPhysicalNetAddress.i4_Length = 0;

        while (nmhGetNextIndexFsMIStdIpNetToPhysicalTable
               (i4IpNetToMediaIfIndex, &i4NextIpNetToMediaIfIndex,
                i4IpNetToPhysicalNetAddressType,
                &i4NextIpNetToPhysicalNetAddressType,
                &IpNetToPhysicalNetAddress, &NextIpNetToPhysicalNetAddress)
               == SNMP_SUCCESS)
        {
            if (i4IpNetToMediaIfIndex != i4NextIpNetToMediaIfIndex)
            {
                break;
            }
            i4IpNetToMediaIfIndex = i4NextIpNetToMediaIfIndex;
            MEMCPY (IpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.pu1_OctetList,
                    NextIpNetToPhysicalNetAddress.i4_Length);
            i4IpNetToPhysicalNetAddressType
                = i4NextIpNetToPhysicalNetAddressType;

            if (i4NextIpNetToPhysicalNetAddressType != INET_ADDR_TYPE_IPV4)
            {
                continue;
            }

            nmhGetFsMIStdIpNetToPhysicalContextId
                (i4NextIpNetToMediaIfIndex,
                 i4NextIpNetToPhysicalNetAddressType,
                 &NextIpNetToPhysicalNetAddress, (INT4 *) &u4NextContextId);

            if (u4NextContextId != u4ContextId)
            {
                continue;
            }

            nmhGetFsMIStdIpNetToPhysicalType (i4NextIpNetToMediaIfIndex,
                                              i4NextIpNetToPhysicalNetAddressType,
                                              &NextIpNetToPhysicalNetAddress,
                                              &i4RetVal);

            if (i4RetVal == CLI_NET2MEDIA_STATIC_TYPE)
            {
                MEMSET (au1Address, IP_ZERO, MAX_ADDR_LEN);
                MEMSET (au1PhyAddress, IP_ZERO, MAX_ADDR_LEN);
                MEMSET (ai1IfaceName, IP_ZERO, CFA_CLI_MAX_IF_NAME_LEN);

                nmhGetFsMIStdIpNetToPhysicalPhysAddress
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress,
                     &OctetStrIpNetToMediaPhysAddress);
                nmhGetFsMIStdIpNetToPhysicalType
                    (i4NextIpNetToMediaIfIndex,
                     i4NextIpNetToPhysicalNetAddressType,
                     &NextIpNetToPhysicalNetAddress, &i4IpNetToMediaType);

                if ((i4IpNetToMediaType == (INT1) ARP_STATIC))
                {

                    CfaCliConfGetIfName ((UINT4) i4NextIpNetToMediaIfIndex,
                                         pi1IfName);

                    MEMSET (au1IpAddress, 0, MAX_ADDR_LEN);
                    IP_GET_IP_ADDR (au1IpAddress,
                                    NextIpNetToPhysicalNetAddress.
                                    pu1_OctetList);
#ifdef ICCH_WANTED
                    u4IcchPeerIp = IcchApiGetPeerAddress ();
                    if (u4IcchPeerIp == OSIX_NTOHL (INET_ADDR (au1IpAddress)))
                    {
                        continue;
                    }
#endif
                    u1BangStatus = TRUE;
                    if (u4ContextId == IP_DEFAULT_CONTEXT)
                    {
                        CliPrintf (CliHandle, "arp");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "arp vrf %s", au1ContextName);
                    }
                    MEMSET (au1IpAddress, 0, MAX_ADDR_LEN);
                    IP_GET_IP_ADDR (au1IpAddress,
                                    NextIpNetToPhysicalNetAddress.
                                    pu1_OctetList);

                    CliPrintf (CliHandle, " %s", au1IpAddress);

                    /*Call macro for MAC Address to String format, to Print */
                    MEMSET (au1Address, IP_ZERO, MAX_ADDR_LEN);
                    CLI_CONVERT_MAC_TO_DOT_STR (OctetStrIpNetToMediaPhysAddress.
                                                pu1_OctetList,
                                                (UINT1 *) pu1Address);

                    CliPrintf (CliHandle, " %s", pu1Address);

                    /* Print the interface name */

                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle, " %s\r\n", pi1IfName);
                }
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                break;
            }
        }

        IpvxShowRunningConfigIfDetails (CliHandle, (INT4) au4IpIfaces[u2Cntr]);
    }

    /*Get from FsMIStdInetCidrRouteTable */

    while (nmhGetNextIndexFsMIStdInetCidrRouteTable
           ((INT4) u4ContextId, (INT4 *) &u4NextContextId,
            i4RouteDestType, &i4NextRouteDestType,
            &RouteDest, &NextRouteDest,
            u4RoutePfxLen, &u4NextRoutePfxLen,
            pu4RoutePolicy, pu4NextRoutePolicy,
            i4RouteNextHopType, &i4RouteNextHopType,
            &RouteNextHop, &NextRouteNextHop) != SNMP_FAILURE)
    {
        if (u4NextContextId != u4ContextId)
        {
            /*No more entries belonging to this context */
            break;
        }
        if (i4NextRouteDestType != INET_ADDR_TYPE_IPV4)
        {
            u4ContextId = u4NextContextId;
            i4RouteDestType = i4NextRouteDestType;
            u4RoutePfxLen = u4NextRoutePfxLen;
            MEMCPY (RouteDest.pu1_OctetList,
                    NextRouteDest.pu1_OctetList, NextRouteDest.i4_Length);
            RouteDest.i4_Length = NextRouteDest.i4_Length;
            MEMSET (NextRouteDest.pu1_OctetList, 0, NextRouteDest.i4_Length);
            NextRouteDest.i4_Length = 0;

            MEMCPY (RouteNextHop.pu1_OctetList, NextRouteNextHop.pu1_OctetList,
                    NextRouteNextHop.i4_Length);
            RouteNextHop.i4_Length = NextRouteNextHop.i4_Length;
            MEMSET (NextRouteNextHop.pu1_OctetList, 0,
                    NextRouteNextHop.i4_Length);
            NextRouteNextHop.i4_Length = 0;

            MEMCPY (pu4RoutePolicy->pu4_OidList,
                    pu4NextRoutePolicy->pu4_OidList,
                    MEM_MAX_BYTES (pu4NextRoutePolicy->u4_Length, MAX_OID_LEN));
            pu4RoutePolicy->u4_Length = pu4NextRoutePolicy->u4_Length;
            MEMSET (pu4NextRoutePolicy->pu4_OidList, 0,
                    MEM_MAX_BYTES (pu4NextRoutePolicy->u4_Length, MAX_OID_LEN));
            pu4NextRoutePolicy->u4_Length = 0;

            continue;
        }
        if (UtilIpvxSetCxt (u4NextContextId) == SNMP_FAILURE)
        {
            free_oid (pu4RoutePolicy);
            free_oid (pu4NextRoutePolicy);
            CliSetBangStatus (CliHandle, FALSE);
            return CLI_FAILURE;
        }

        if (i4NextRouteDestType != i4RouteNextHopType)
        {
            UtilIpvxReleaseCxt ();
            free_oid (pu4RoutePolicy);
            free_oid (pu4NextRoutePolicy);
            CliSetBangStatus (CliHandle, FALSE);
            return CLI_FAILURE;
        }

        pTempRouteDest = &NextRouteDest;
        pTempRouteNextHop = &NextRouteNextHop;

        IPVX_UTIL_GET_IPV4_IDX (u4RtDest, pTempRouteDest,
                                u4RtMask, u4NextRoutePfxLen,
                                u4RtNextHop, pTempRouteNextHop);

        pTempRouteDest = NULL;
        pTempRouteNextHop = NULL;

        u4ContextId = (UINT4) IpvxGetCurrContext ();
        if (IpFwdTblGetObjectInCxt (u4ContextId,
                                    u4RtDest, u4RtMask,
                                    i4RtTos, u4RtNextHop,
                                    CIDR_STATIC_ID, &i4RetVal,
                                    IPFWD_ROUTE_ROW_STATUS) != SNMP_FAILURE)
        {
            if (i4RetVal == IPFWD_ACTIVE)
            {
                IpFwdTblGetObjectInCxt (u4ContextId,
                                        u4RtDest, u4RtMask,
                                        i4RtTos, u4RtNextHop,
                                        CIDR_STATIC_ID, &i4RetVal,
                                        IPFWD_ROUTE_IFINDEX);
                NetIpv4GetCfaIfIndexFromPort ((UINT4) i4RetVal, &u4CfaIfIndex);

                CfaCliConfGetIfName (u4CfaIfIndex, pi1IfName);

                IpFwdTblGetObjectInCxt (u4ContextId,
                                        u4RtDest, u4RtMask,
                                        i4RtTos, u4RtNextHop,
                                        CIDR_STATIC_ID, &i4RetVal,
                                        IPFWD_ROUTE_METRIC1);
                u1BangStatus = TRUE;
                if (u4ContextId == IP_DEFAULT_CONTEXT)
                {
                    CliPrintf (CliHandle, "ip route ");
                }
                else
                {
                    /*Get context name from context Id */
                    VcmGetAliasName (u4ContextId, au1ContextName);
                    CliPrintf (CliHandle, "ip route vrf %s ", au1ContextName);
                }
                CliPrintf (CliHandle, "%d.%d.%d.%d ",
                           NextRouteDest.pu1_OctetList[0],
                           NextRouteDest.pu1_OctetList[1],
                           NextRouteDest.pu1_OctetList[2],
                           NextRouteDest.pu1_OctetList[3]);
                u4RtMask = CfaGetCidrSubnetMask (u4NextRoutePfxLen);
                CLI_CONVERT_IPADDR_TO_STR (pu1MaskAddress, u4RtMask);
                CliPrintf (CliHandle, " %s ", pu1MaskAddress);

                if (MEMCMP
                    (au1NullIpAddress, NextRouteNextHop.pu1_OctetList,
                     MAX_ADDR_LEN) != 0)
                {
                    CliPrintf (CliHandle, "%d.%d.%d.%d",
                               NextRouteNextHop.pu1_OctetList[0],
                               NextRouteNextHop.pu1_OctetList[1],
                               NextRouteNextHop.pu1_OctetList[2],
                               NextRouteNextHop.pu1_OctetList[3]);
                }
                else
                {
                    CliPrintf (CliHandle, " %s ", pi1IfName);
                }

                /* Check for the static routes with current default distance
                 * and skip displaying AD for those routes
                 */

                if (i4RetVal != i4IpAdminDistance)
                {
                    CliPrintf (CliHandle, " %d ", i4RetVal);
                }
                u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r\n");

            }
        }
        UtilIpvxReleaseCxt ();

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* 
             * User pressed 'q' at more prompt, no more print required, exit 
             * 
             */
            break;
        }

        u4ContextId = u4NextContextId;
        i4RouteDestType = i4NextRouteDestType;
        u4RoutePfxLen = u4NextRoutePfxLen;
        MEMCPY (RouteDest.pu1_OctetList,
                NextRouteDest.pu1_OctetList, NextRouteDest.i4_Length);
        RouteDest.i4_Length = NextRouteDest.i4_Length;
        MEMSET (NextRouteDest.pu1_OctetList, 0, NextRouteDest.i4_Length);
        NextRouteDest.i4_Length = 0;

        MEMCPY (RouteNextHop.pu1_OctetList, NextRouteNextHop.pu1_OctetList,
                NextRouteNextHop.i4_Length);
        RouteNextHop.i4_Length = NextRouteNextHop.i4_Length;
        MEMSET (NextRouteNextHop.pu1_OctetList, 0, NextRouteNextHop.i4_Length);
        NextRouteNextHop.i4_Length = 0;

        MEMCPY (pu4RoutePolicy->pu4_OidList, pu4NextRoutePolicy->pu4_OidList,
                MEM_MAX_BYTES (pu4NextRoutePolicy->u4_Length, MAX_OID_LEN));
        pu4RoutePolicy->u4_Length = pu4NextRoutePolicy->u4_Length;
        MEMSET (pu4NextRoutePolicy->pu4_OidList, 0,
                MEM_MAX_BYTES (pu4NextRoutePolicy->u4_Length, MAX_OID_LEN));
        pu4NextRoutePolicy->u4_Length = 0;
    }
    free_oid (pu4RoutePolicy);
    free_oid (pu4NextRoutePolicy);
    if (u1BangStatus == TRUE)
    {
        CliSetBangStatus (CliHandle, TRUE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IpvxShowRunningConfigIfDetails                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the configurations related  */
/*                        to the object ipv4InterfaceEnableStatus            */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index - Interface Index                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IpvxShowRunningConfigIfDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT1               *pi1IfName = NULL;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4EnableStatus = 0;

    MEMSET (ai1IfaceName, IP_ZERO, CFA_CLI_MAX_IF_NAME_LEN);

    if (nmhValidateIndexInstanceIpv4InterfaceTable (i4Index) == SNMP_SUCCESS)
    {
        if (nmhGetIpv4InterfaceEnableStatus (i4Index, &i4EnableStatus)
            == SNMP_SUCCESS)
        {
            if (i4EnableStatus == IPIF_OPER_DISABLE)
            {
                pi1IfName = &ai1IfaceName[IP_ZERO];
                CfaCliConfGetIfName (i4Index, pi1IfName);
                CliPrintf (CliHandle, "!\r\n");
                CliPrintf (CliHandle, "interface %s\r\n", pi1IfName);
                CliPrintf (CliHandle, " no ipv4 enable \r\n");
                CliPrintf (CliHandle, "!\r\n");
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowLinuxVlanArpIpAddress                            */
/*                                                                           */
/* Description      : This function is invoked to display Dynamic/Pending    */
/*                    Arp Learnt over the Linux vlan/Oob interface           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. u4IpAddress - Arp entries matching this address     */
/*                                     will be printed                       */
/*                    3. i1PrintFlag - based on this flag Header for ARP     */
/*                                     Table will be printed                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  None                                                  */
/*****************************************************************************/
VOID
IpShowLinuxVlanArpIpAddress (tCliHandle CliHandle,
                             UINT4 u4IpAddress, INT1 i1PrintFlag)
{
    FILE               *pFile = NULL;
    UINT1               au1Ip[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1Hw[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1Mask[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1DevName[LNX_MAX_ARP_ENTRY_SIZE];
    CHR1                ac1Line[LNX_MAX_ARP_LINE_SIZE];
    UINT4               u4Type = 0, u4Flags = 0;
    UINT4               u4DestIpAddr = 0;
    INT1                ai1OobIntfName[CFA_MAX_PORT_NAME_LENGTH] = { "eth0" };
    UINT4               u4PagingStatus = CLI_SUCCESS;

    if ((pFile = FOPEN ("/proc/net/arp", "r")) != NULL)
    {
        if (fgets (ac1Line, sizeof (ac1Line), pFile) == NULL)
        {
            fclose (pFile);
            return;
        }
        MEMSET (ac1Line, 0, LNX_MAX_ARP_LINE_SIZE);
        while (fgets (ac1Line, sizeof (ac1Line), pFile) != NULL)
        {
            MEMSET (au1Ip, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1Hw, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1Mask, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1DevName, 0, LNX_MAX_ARP_ENTRY_SIZE);
            SSCANF (ac1Line, "%100s 0x%x 0x%x %100s %100s %100s\n",
                    au1Ip, &u4Type, &u4Flags, au1Hw, au1Mask, au1DevName);
            au1Ip[LNX_MAX_ARP_ENTRY_SIZE - 1] = '\0';
            u4DestIpAddr = OSIX_NTOHL (INET_ADDR (au1Ip));

            /* print dynamic arp entries learnt over Oob (eth0) 
               and linux vlan eth0.x interfaces only */
            /* Assumption: For Linux vlan/oob interfaces, static
               arp entries will be added through ISS Cli and not through 
               linux shell prompt. And these entries will printed 
               in IpShowArp function */
            if ((STRNCASECMP (au1DevName, CFA_LINUX_L3IPVLAN_NAME_PREFIX,
                              STRLEN (CFA_LINUX_L3IPVLAN_NAME_PREFIX)) == 0)
                && ((u4Flags == LNX_DYNAMIC_ARP) ||
                    (u4Flags == LNX_PENDING_ARP)))

            {
                if (u4DestIpAddr == u4IpAddress)
                {
                    if (i1PrintFlag == FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nAddress          Hardware Address   Type  Interface  Mapping  \r\n");
                        CliPrintf (CliHandle,
                                   "\r-------          ----------------   ----  ---------  -------  \r\n");
                    }

                    i1PrintFlag = TRUE;
                    CliPrintf (CliHandle, "%-15s  ", au1Ip);

                    CliPrintf (CliHandle, "%-17s  ", au1Hw);

                    /* Print the type as ARPA */
                    CliPrintf (CliHandle, "ARPA  ");

                    /* Print the oob interface name as Cpu0 */
                    if (STRCMP (au1DevName, ai1OobIntfName) == 0)
                    {
                        CliPrintf (CliHandle, "Cpu0        ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-9s  ", au1DevName);
                    }

                    /* Print the mapping */
                    if (u4Flags == LNX_DYNAMIC_ARP)
                    {
                        u4PagingStatus =
                            (UINT4) CliPrintf (CliHandle, "Dynamic   \r\n");
                    }
                    else
                    {
                        u4PagingStatus =
                            (UINT4) CliPrintf (CliHandle, "Pending   \r\n");
                    }

                    if (u4PagingStatus == CLI_FAILURE)
                    {
                        /* User pressed 'q' at more prompt so break */
                        break;
                    }
                }
            }
            MEMSET (ac1Line, 0, LNX_MAX_ARP_LINE_SIZE);
        }                        /* End of While */
        fclose (pFile);
        return;
    }
    else
    {
        perror (" cannot open the file /proc/net/arp");
        return;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowLinuxVlanArpMacAddress                           */
/*                                                                           */
/* Description      : This function is invoked to display ARP table entries  */
/*                    for a specified MAC address                            */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. pu1MacAddr - MAC address                            */
/*                    3. i1PrintFlag - based on this flag Header for ARP     */
/*                                     Table will be printed                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

VOID
IpShowLinuxVlanArpMacAddress (tCliHandle CliHandle, UINT1 *pu1MacAddr,
                              INT1 i1PrintFlag)
{
    FILE               *pFile = NULL;
    UINT1               au1Ip[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1Hw[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1Mask[LNX_MAX_ARP_ENTRY_SIZE];
    CHR1                ac1Line[LNX_MAX_ARP_LINE_SIZE];
    UINT1               au1DevName[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1MacAddr[MAC_LEN];
    UINT4               u4Type = 0, u4Flags = 0;
    INT1                ai1OobIntfName[CFA_MAX_PORT_NAME_LENGTH] = { "eth0" };
    UINT4               u4PagingStatus = CLI_SUCCESS;

    if ((pFile = FOPEN ("/proc/net/arp", "r")) != NULL)
    {
        if (fgets (ac1Line, sizeof (ac1Line), pFile) == NULL)
        {
            fclose (pFile);
            return;
        }
        MEMSET (ac1Line, 0, LNX_MAX_ARP_LINE_SIZE);
        while (fgets (ac1Line, sizeof (ac1Line), pFile) != NULL)
        {
            MEMSET (au1Ip, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1Hw, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1Mask, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1DevName, 0, LNX_MAX_ARP_ENTRY_SIZE);
            SSCANF (ac1Line, "%100s 0x%x 0x%x %100s %100s %100s\n",
                    au1Ip, &u4Type, &u4Flags, au1Hw, au1Mask, au1DevName);

            /* print dynamic arp entries learnt over Oob (eth0) 
               and linux vlan eth0.x interfaces only */
            /* Assumption: For Linux vlan/oob interfaces, static
               arp entries will be added through ISS Cli and not through 
               linux shell prompt. And these entries will printed 
               in IpShowArp function */
            if ((STRNCASECMP (au1DevName, CFA_LINUX_L3IPVLAN_NAME_PREFIX,
                              STRLEN (CFA_LINUX_L3IPVLAN_NAME_PREFIX)) == 0)
                && ((u4Flags == LNX_DYNAMIC_ARP) ||
                    (u4Flags == LNX_PENDING_ARP)))

            {
                MEMSET (au1MacAddr, 0, MAC_LEN);
                CLI_CONVERT_DOT_STR_TO_MAC (au1Hw, au1MacAddr);
                if (MEMCMP (au1MacAddr, pu1MacAddr, MAC_LEN) == 0)
                {
                    if (i1PrintFlag == FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "\r\nAddress          Hardware Address   Type  Interface  Mapping  \r\n");
                        CliPrintf (CliHandle,
                                   "\r-------          ----------------   ----  ---------  -------  \r\n");
                    }
                    i1PrintFlag = TRUE;
                    CliPrintf (CliHandle, "%-15s  ", au1Ip);

                    CliPrintf (CliHandle, "%-17s  ", au1Hw);

                    /* Print the type as ARPA */
                    CliPrintf (CliHandle, "ARPA  ");

                    /* Print the oob interface name as Cpu0 */
                    if (STRCMP (au1DevName, ai1OobIntfName) == 0)
                    {
                        CliPrintf (CliHandle, "Cpu0        ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "%-9s  ", au1DevName);
                    }

                    /* Print the mapping */
                    if (u4Flags == LNX_DYNAMIC_ARP)
                    {
                        u4PagingStatus =
                            (UINT4) CliPrintf (CliHandle, "Dynamic   \r\n");
                    }
                    else
                    {
                        u4PagingStatus =
                            (UINT4) CliPrintf (CliHandle, "Pending   \r\n");
                    }

                    if (u4PagingStatus == CLI_FAILURE)
                    {
                        /* User pressed 'q' at more prompt so break */
                        break;
                    }
                }
            }
            MEMSET (ac1Line, 0, LNX_MAX_ARP_LINE_SIZE);
        }                        /* End of While */
        fclose (pFile);
        return;
    }
    else
    {
        perror (" cannot open the file /proc/net/arp");
        return;
    }
}

VOID
IpGetLinuxArpCount (UINT4 *pu4LnxVlanArpEntryCount,
                    UINT4 *pu4LnxVlanPendArpEntryCount)
{
    FILE               *pFile = NULL;
    UINT1               au1Ip[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1Hw[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1Mask[LNX_MAX_ARP_ENTRY_SIZE];
    CHR1                ac1Line[LNX_MAX_ARP_LINE_SIZE];
    UINT1               au1DevName[LNX_MAX_ARP_ENTRY_SIZE];
    UINT1               au1MacStr[MAX_ADDR_LEN];
    UINT4               u4Type = 0, u4Flags = 0;
    UINT4               u4ArpCount = 0;
    UINT4               u4PendingArpCount = 0;

    MEMSET (au1MacStr, 0, MAX_ADDR_LEN);

    if ((pFile = FOPEN ("/proc/net/arp", "r")) != NULL)
    {
        if (fgets (ac1Line, sizeof (ac1Line), pFile) == NULL)
        {
            *pu4LnxVlanArpEntryCount = 0;
            *pu4LnxVlanPendArpEntryCount = 0;
            fclose (pFile);
            return;
        }
        MEMSET (ac1Line, 0, LNX_MAX_ARP_LINE_SIZE);
        while (fgets (ac1Line, sizeof (ac1Line), pFile) != NULL)
        {
            MEMSET (au1Ip, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1Hw, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1Mask, 0, LNX_MAX_ARP_ENTRY_SIZE);
            MEMSET (au1DevName, 0, LNX_MAX_ARP_ENTRY_SIZE);
            SSCANF (ac1Line, "%100s 0x%x 0x%x %100s %100s %100s\n",
                    au1Ip, &u4Type, &u4Flags, au1Hw, au1Mask, au1DevName);

            /* print dynamic arp entries learnt over Oob (eth0) 
               and linux vlan eth0.x interfaces only */
            /* Assumption: For Linux vlan/oob interfaces, static
               arp entries will be added through ISS Cli and not through 
               linux shell prompt. And these entries will printed 
               in IpShowArp function */
            if ((STRNCASECMP (au1DevName, CFA_LINUX_L3IPVLAN_NAME_PREFIX,
                              STRLEN (CFA_LINUX_L3IPVLAN_NAME_PREFIX)) == 0)
                && ((u4Flags == LNX_DYNAMIC_ARP) ||
                    (u4Flags == LNX_PENDING_ARP)))

            {
                /* Print the mapping */
                if (u4Flags == LNX_DYNAMIC_ARP)
                {
                    u4ArpCount++;
                }
                else
                {
                    u4ArpCount++;
                    u4PendingArpCount++;
                }

            }
            MEMSET (ac1Line, 0, LNX_MAX_ARP_LINE_SIZE);
        }                        /* End of While */
        fclose (pFile);
        *pu4LnxVlanArpEntryCount = u4ArpCount;
        *pu4LnxVlanPendArpEntryCount = u4PendingArpCount;
        return;
    }
    else
    {
        perror (" cannot open the file /proc/net/arp");
        *pu4LnxVlanArpEntryCount = 0;
        *pu4LnxVlanPendArpEntryCount = 0;
        return;
    }
}

/*********************************************************************
 *  Function Name : IpSetInterfaceAdminStatus
 *  Description   : Configuration of Ipv4 Interface
 *  Input(s)      : CliHandle
 *                  u4IpIfIndex - Interface Index
 *                  i4IpIfAdminStatus - Status to be set
 *  Output(s)     :
 *  Return Values : CLI_SUCCESS/CLI_FAILURE.
 **********************************************************************/

INT1
IpSetInterfaceAdminStatus (tCliHandle CliHandle,
                           UINT4 u4IpIfIndex, INT4 i4IpIfEnableStatus)
{
    UINT4               u4ErrorCode = IP_ZERO;

    /* ADMIN STATUS */
    if (nmhTestv2Ipv4InterfaceEnableStatus
        (&u4ErrorCode, (INT4) u4IpIfIndex, i4IpIfEnableStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP_INVALID_ADMIN_STATUS);
        return CLI_FAILURE;
    }

    if (nmhSetIpv4InterfaceEnableStatus
        ((INT4) u4IpIfIndex, i4IpIfEnableStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* Function Name     : IpAddrMatch                                            */
/*                                                                            */
/* Description       : This function compares the given two ip addresses based*/
/*                        on the prefix mask specified.                          */
/*                                                                            */
/* Input Parameters  : IP Addresses (u4Addr1, u4Addr2), PrefixMask (u4Mask)   */
/*                                                                              */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : TRUE - Address Matches, FALSE - Address does not match */
/******************************************************************************/

INT4
IpAddrMatch (UINT4 u4Addr1, UINT4 u4Addr2, UINT4 u4Mask)
{

    if ((u4Mask == 0) || ((u4Addr1 == 0) && (u4Addr2 == 0)))
    {
        return (TRUE);
    }
    if ((u4Addr1 ^ u4Addr2) & u4Mask)
    {
        return (FALSE);
    }
    return TRUE;
}

/******************************************************************************/
/*                                                                            */
/* Function Name     : IpObtainProtoType                                      */
/*                                                                            */
/* Description       : This function gives the protocol type for the given    */
/*                      route type                                            */
/*                                                                            */
/* Input Parameters  : i4Proto - Route protocol type                          */
/*                     i4ProtoType - Route type                               */
/*                                                                            */
/* Output Parameters : pi1Proto - Protocol type                               */
/*                                                                            */
/* Return Value      : None                                                   */
/******************************************************************************/
VOID
IpObtainProtoType (INT4 i4Proto, INT4 i4ProtoType, INT1 *pi1Proto)
{
    INT1               *pi1RtType = pi1Proto;
    switch (i4Proto)
    {
        case CIDR_LOCAL_ID:
            MEMCPY (pi1RtType, "C", IP_ONE);
            break;

        case CIDR_STATIC_ID:
            MEMCPY (pi1RtType, "S", IP_ONE);
            break;

        case RIP_ID:
            MEMCPY (pi1RtType, "R", IP_ONE);
            break;

        case BGP_ID:
            MEMCPY (pi1RtType, "B", IP_ONE);
            break;

        case OSPF_ID:
            MEMCPY (pi1RtType, "O", IP_ONE);
            pi1RtType++;
            switch (i4ProtoType)
            {
                case IP_OSPF_TYPE_1_EXT:
                    MEMCPY (pi1RtType, " E1", STRLEN (" E1"));
                    break;
                case IP_OSPF_TYPE_2_EXT:
                    MEMCPY (pi1RtType, " E2", STRLEN (" E2"));
                    break;
                case IP_OSPF_INTER_AREA:
                    MEMCPY (pi1RtType, " IA", STRLEN (" IA"));
                    break;
                case IP_OSPF_NSSA_1_EXT:
                    MEMCPY (pi1RtType, " N1", STRLEN (" N1"));
                    break;
                case IP_OSPF_NSSA_2_EXT:
                    MEMCPY (pi1RtType, " N2", STRLEN (" N2"));
                    break;
                default:
                    break;
            }
            break;
#ifdef ISIS_WANTED
        case ISIS_ID:
            MEMCPY (pi1Proto, "I", IP_ONE);
            pi1RtType++;
            switch (i4ProtoType)
            {
                case IP_ISIS_LEVEL1:
                    MEMCPY (pi1RtType, "L1", STRLEN ("L1"));
                    break;
                case IP_ISIS_LEVEL2:
                    MEMCPY (pi1RtType, "L2", STRLEN ("L2"));
                    break;
                case IP_ISIS_INTER_AREA:
                    MEMCPY (pi1RtType, "ia", STRLEN ("ia"));
                    break;
                default:
                    break;
            }
            break;
#endif
        case OTHERS_ID:
            MEMCPY (pi1RtType, " ", IP_ONE);
            break;
        default:
            break;
    }
    return;
}

/***************************************************************************/
/*   Function Name : IpArpSetDebugLevel                                    */
/*                                                                         */
/*   Description   :To Set ARP Trace level                                 */
/*                                                                         */
/*   Input(s)      :  1. u4ArpCxtId - CLI context ID                       */
/*                    2. CLI_ENABLE / CLI_DISABLE - to set / reset the     */
/*                       trace level                                       */
/*                    3. u4TraceLevel - trace level                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
IpArpSetDebugLevel (tCliHandle CliHandle, UINT4 u4ArpCxtId,
                    UINT4 u4Flag, UINT4 u4TraceLevel)
{
    INT4                i4OriginalTrace = 0;
    if (u4ArpCxtId == ARP_DEFAULT_CONTEXT)
    {
        if (nmhGetFsArpGlobalDebug (&i4OriginalTrace) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhGetFsMIArpContextDebug ((INT4) u4ArpCxtId, &i4OriginalTrace) !=
            SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
    }
    if (u4Flag == CLI_ENABLE)
    {
        u4TraceLevel |= (UINT4) i4OriginalTrace;
    }
    else
    {
        u4TraceLevel = (UINT4) i4OriginalTrace & (~u4TraceLevel);
    }
    if (u4ArpCxtId == ARP_DEFAULT_CONTEXT)
    {
        if (nmhSetFsArpGlobalDebug ((INT4) u4TraceLevel) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    else
    {
        if (nmhSetFsMIArpContextDebug ((INT4) u4ArpCxtId, (INT4) u4TraceLevel)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssIpArpShowDebugging                              */
/*                                                                           */
/*     DESCRIPTION      : This function prints the ARP  debug level          */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IssIpArpShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    nmhGetFsArpGlobalDebug (&i4DbgLevel);

    if (i4DbgLevel == 0)
    {
        return;
    }
    CliPrintf (CliHandle, "\rARP :");

    if ((i4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP init and shutdown debugging is on");
    }
    if ((i4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP management debugging is on");
    }
    if ((i4DbgLevel & DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP data path debugging is on");
    }
    if ((i4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP control path debugging is on");
    }
    if ((i4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP packet dump debugging is on");
    }
    if ((i4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP resources debugging is on");
    }
    if ((i4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP error debugging is on");
    }
    if ((i4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  ARP buffer debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : ArpCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
ArpCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return CLI_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) ARP_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
ArpCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[ARP_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, ARP_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return CLI_FAILURE;
    }
    MEMSET (ai1Buf, 0, ARP_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (ArpCliReadLineFromFile (i4Fd, ai1Buf, ARP_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != ARP_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', ARP_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ArpCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT1
ArpCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (ARP_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (ARP_CLI_EOF);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetClearArp                                          */
/*                                                                           */
/* Description      : This function is invoked to clear dynamic ARP cache    */
/*                    entries                                                */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle                                           */
/*                    2. u4CurrContext - context ID                          */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetClearArp (tCliHandle CliHandle, UINT4 u4CurrContext)
{

    UINT4               u4ErrorCode;
    INT4                i4ArpCacheFlushStatus = OSIX_TRUE;

    if (nmhTestv2FsArpCacheFlushStatus (&u4ErrorCode,
                                        i4ArpCacheFlushStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsArpCacheFlushStatus (i4ArpCacheFlushStatus,
                                     u4CurrContext) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetDefAdminDistance                                  */
/*                                                                           */
/* Description      : This function is used to set the default administrative*/
/*                    distance for static IPv4 route                         */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle                                           */
/*                    2. u4ContextId - context ID                            */
/*                    3. i4IpDefAdminDistance -Default AD Value              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetDefAdminDistance (tCliHandle CliHandle, UINT4 u4ContextId,
                       INT4 i4IpDefAdminDistance)
{
    UINT4               u4ErrorCode = IP_ZERO;
    UNUSED_PARAM (CliHandle);
    /* Test whether the given Distance value is valid or not */
    if (nmhTestv2FsMIRTMIpStaticRouteDistance (&u4ErrorCode,
                                               (INT4) u4ContextId,
                                               i4IpDefAdminDistance)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP_INVALID_DEF_DISTANCE);
        return CLI_FAILURE;
    }

    if (nmhSetFsMIRTMIpStaticRouteDistance ((INT4) u4ContextId,
                                            i4IpDefAdminDistance)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP_DISTANCE_SET_ERR);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpGetDefAdminDistance                                  */
/*                                                                           */
/* Description      : This function is used to get the static IPv4 route     */
/*                    default administrative distance of default and         */
/*                    user VRF.                                              */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle                                           */
/*                    2. u4ContextId - context ID                            */
/*                    3. pi4IpAdminDistance - pointer  to Default AD Value   */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpGetDefAdminDistance (UINT4 u4ContextId, INT4 *pi4IpAdminDistance)
{
    if (nmhGetFsMIRTMIpStaticRouteDistance ((INT4) u4ContextId,
                                            pi4IpAdminDistance) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_IP_DISTANCE_GET_ERR);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowDefAdminDistance                                 */
/*                                                                           */
/* Description      : This function is used to display the IPv4 static       */
/*                    default administrative distance configured for default */
/*                    and user VRF.                                          */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle                                           */
/*                    2. u4VcId - context ID                                 */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowDefAdminDistance (tCliHandle CliHandle, UINT4 u4VcId)
{
    INT4                i4IpContextId = IP_ZERO;
    INT4                i4NextIpContextId = IP_ZERO;
    INT4                i4IpAdminDistance = IP_ZERO;
    INT4                i4RetVal = SNMP_SUCCESS;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, ZERO, VCM_ALIAS_MAX_LEN);

    /* If no VRF/Context is specified, traverse through all
     * the available the contexts and display the default 
     * administrative distance of static IP route 
     */
    if (u4VcId == VCM_INVALID_VC)
    {
        if (nmhGetFirstIndexFsMIRtmTable (&i4IpContextId) == SNMP_SUCCESS)
        {
            while (i4RetVal == SNMP_SUCCESS)
            {
                if (nmhGetFsMIRTMIpStaticRouteDistance (i4IpContextId,
                                                        &i4IpAdminDistance)
                    == SNMP_SUCCESS)
                {
                    VcmGetAliasName ((UINT4) i4IpContextId, au1ContextName);
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "Vrf Name:          %s\r\n",
                               au1ContextName);
                    CliPrintf (CliHandle,
                               "IP Default Administrative distance: %d\r\n",
                               i4IpAdminDistance);
                }
                i4RetVal = nmhGetNextIndexFsMIRtmTable (i4IpContextId,
                                                        &i4NextIpContextId);
                i4IpContextId = i4NextIpContextId;
            }
        }
        return CLI_SUCCESS;
    }

    /* If context specified, display default administrative distance of the 
     * particular context
     */
    else
    {
        if (nmhGetFsMIRTMIpStaticRouteDistance ((INT4) u4VcId,
                                                &i4IpAdminDistance)
            == SNMP_SUCCESS)
        {
            VcmGetAliasName (u4VcId, au1ContextName);
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "Vrf Name:          %s\r\n", au1ContextName);
            CliPrintf (CliHandle, "IP Default Administrative distance: %d\r\n",
                       i4IpAdminDistance);
            return CLI_SUCCESS;
        }
    }
    return CLI_FAILURE;
}
#endif
