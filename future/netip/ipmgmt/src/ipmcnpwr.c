
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnpwr.c,v 1.3 2014/12/23 06:33:53 siva Exp $
 *
 * Description: This file contains the NP wrappers for Multicast module.
 *****************************************************************************/

#ifndef __IPMC_NPWR_C__
#define __IPMC_NPWR_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIpmcNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
IpmcNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIpmcNpModInfo = &(pFsHwNp->IpmcNpModInfo);

    if (NULL == pIpmcNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
#ifdef FS_NPAPI
#if defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
        case FS_NP_IPV4_MC_INIT:
        {
            u1RetVal = FsNpIpv4McInit ();
            break;
        }
        case FS_NP_IPV4_MC_DE_INIT:
        {
            u1RetVal = FsNpIpv4McDeInit ();
            break;
        }
        case FS_NP_IPV4_VLAN_MC_INIT:
        {
            tIpmcNpWrFsNpIpv4VlanMcInit *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4VlanMcInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpv4VlanMcInit (pEntry->u4VlanId);
            break;
        }
        case FS_NP_IPV4_VLAN_MC_DE_INIT:
        {
            tIpmcNpWrFsNpIpv4VlanMcDeInit *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4VlanMcDeInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpv4VlanMcDeInit (pEntry->u4Index);
            break;
        }
        case FS_NP_IPV4_RPORT_MC_INIT:
        {
            tIpmcNpWrFsNpIpv4RportMcInit *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4RportMcInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpv4RportMcInit (pEntry->PortInfo);
            break;
        }
        case FS_NP_IPV4_RPORT_MC_DE_INIT:
        {
            tIpmcNpWrFsNpIpv4RportMcDeInit *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4RportMcDeInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpv4RportMcDeInit (pEntry->PortInfo);
            break;
        }
        case FS_NP_IPV4_MC_ADD_ROUTE_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McAddRouteEntry *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McAddRouteEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4McAddRouteEntry (pEntry->u4VrId, pEntry->u4GrpAddr,
                                         pEntry->u4GrpPrefix,
                                         pEntry->u4SrcIpAddr,
                                         pEntry->u4SrcIpPrefix,
                                         pEntry->u1CallerId, pEntry->rtEntry,
                                         pEntry->u2NoOfDownStreamIf,
                                         pEntry->pDownStreamIf);
            break;
        }
        case FS_NP_IPV4_MC_DEL_ROUTE_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McDelRouteEntry *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McDelRouteEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4McDelRouteEntry (pEntry->u4VrId, pEntry->u4GrpAddr,
                                         pEntry->u4GrpPrefix,
                                         pEntry->u4SrcIpAddr,
                                         pEntry->u4SrcIpPrefix, pEntry->rtEntry,
                                         pEntry->u2NoOfDownStreamIf,
                                         pEntry->pDownStreamIf);
            break;
        }
        case FS_NP_IPV4_MC_CLEAR_ALL_ROUTES:
        {
            tIpmcNpWrFsNpIpv4McClearAllRoutes *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McClearAllRoutes;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv4McClearAllRoutes (pEntry->u4VrId);
            break;
        }
        case FS_NP_IPV4_MC_ADD_CPU_PORT:
        {
            tIpmcNpWrFsNpIpv4McAddCpuPort *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McAddCpuPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4McAddCpuPort (pEntry->u2GenRtrId, pEntry->u4GrpAddr,
                                      pEntry->u4SrcAddr, pEntry->rtEntry);
            break;
        }
        case FS_NP_IPV4_MC_DEL_CPU_PORT:
        {
            tIpmcNpWrFsNpIpv4McDelCpuPort *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McDelCpuPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4McDelCpuPort (pEntry->u2GenRtrId, pEntry->u4GrpAddr,
                                      pEntry->u4SrcAddr, pEntry->rtEntry);
            break;
        }
        case FS_NP_IPV4_MC_GET_HIT_STATUS:
        {
            tIpmcNpWrFsNpIpv4McGetHitStatus *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McGetHitStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv4McGetHitStatus (pEntry->u4SrcIpAddr, pEntry->u4GrpAddr,
                                    pEntry->u4Iif, pEntry->u2VlanId,
                                    pEntry->pu4HitStatus);
            break;
        }
        case FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McUpdateOifVlanEntry *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McUpdateOifVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv4McUpdateOifVlanEntry (pEntry->u4VrId, pEntry->u4GrpAddr,
                                          pEntry->u4SrcIpAddr, pEntry->rtEntry,
                                          pEntry->downStreamIf);
            break;
        }
        case FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY:
        {
            tIpmcNpWrFsNpIpv4McUpdateIifVlanEntry *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McUpdateIifVlanEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            FsNpIpv4McUpdateIifVlanEntry (pEntry->u4VrId, pEntry->u4GrpAddr,
                                          pEntry->u4SrcIpAddr, pEntry->rtEntry);
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMRouteStats *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4GetMRouteStats (pEntry->u4VrId, pEntry->u4GrpAddr,
                                        pEntry->u4SrcAddr, pEntry->i4StatType,
                                        pEntry->pu4RetValue);
            break;
        }
        case FS_NP_IPV4_GET_M_ROUTE_H_C_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMRouteHCStats *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteHCStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4GetMRouteHCStats (pEntry->u4VrId, pEntry->u4GrpAddr,
                                          pEntry->u4SrcAddr, pEntry->i4StatType,
                                          pEntry->pu8RetValue);
            break;
        }
        case FS_NP_IPV4_GET_M_NEXT_HOP_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMNextHopStats *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMNextHopStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4GetMNextHopStats (pEntry->u4VrId, pEntry->u4GrpAddr,
                                          pEntry->u4SrcAddr,
                                          pEntry->i4OutIfIndex,
                                          pEntry->i4StatType,
                                          pEntry->pu4RetValue);
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMIfaceStats *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4GetMIfaceStats (pEntry->u4VrId, pEntry->i4IfIndex,
                                        pEntry->i4StatType,
                                        pEntry->pu4RetValue);
            break;
        }
        case FS_NP_IPV4_GET_M_IFACE_H_C_STATS:
        {
            tIpmcNpWrFsNpIpv4GetMIfaceHCStats *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceHCStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4GetMIfaceHCStats (pEntry->u4VrId, pEntry->i4IfIndex,
                                          pEntry->i4StatType,
                                          pEntry->pu8RetValue);
            break;
        }
        case FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD:
        {
            tIpmcNpWrFsNpIpv4SetMIfaceTtlTreshold *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4SetMIfaceTtlTreshold;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4SetMIfaceTtlTreshold (pEntry->u4VrId, pEntry->i4IfIndex,
                                              pEntry->i4TtlTreshold);
            break;
        }
        case FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT:
        {
            tIpmcNpWrFsNpIpv4SetMIfaceRateLimit *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4SetMIfaceRateLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4SetMIfaceRateLimit (pEntry->u4VrId, pEntry->i4IfIndex,
                                            pEntry->i4RateLimit);
            break;
        }
        case FS_NP_IPV_X_HW_GET_MCAST_ENTRY:
        {
            tIpmcNpWrFsNpIpvXHwGetMcastEntry *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpvXHwGetMcastEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpvXHwGetMcastEntry (pEntry->pNpL3McastEntry);
            break;
        }
#ifdef PIM_WANTED
        case FS_PIM_NP_INIT_HW:
        {
            u1RetVal = FsPimNpInitHw ();
            break;
        }
        case FS_PIM_NP_DE_INIT_HW:
        {
            u1RetVal = FsPimNpDeInitHw ();
            break;
        }
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
        case FS_DVMRP_NP_INIT_HW:
        {
            u1RetVal = FsDvmrpNpInitHw ();
            break;
        }
        case FS_DVMRP_NP_DE_INIT_HW:
        {
            u1RetVal = FsDvmrpNpDeInitHw ();
            break;
        }
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED
        case FS_NP_IGMP_PROXY_INIT:
        {
            u1RetVal = FsNpIgmpProxyInit ();
            break;
        }
        case FS_NP_IGMP_PROXY_DE_INIT:
        {
            u1RetVal = FsNpIgmpProxyDeInit ();
            break;
        }
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED
        case FS_NP_IPV4_MC_CLEAR_HIT_BIT:
        {
            tIpmcNpWrFsNpIpv4McClearHitBit *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McClearHitBit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4McClearHitBit (pEntry->u2VlanId, pEntry->u4GrpAddr,
                                       pEntry->u4SrcAddr);
            break;
        }
#endif
        case FS_NP_IPV4_MC_RPF_D_F_INFO:
        {
            tIpmcNpWrFsNpIpv4McRpfDFInfo *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McRpfDFInfo;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpv4McRpfDFInfo (pEntry->pMcRpfDFInfo);
            break;
        }
#ifdef MBSM_WANTED
        case FS_NP_IPV4_MBSM_MC_INIT:
        {
            tIpmcNpWrFsNpIpv4MbsmMcInit *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4MbsmMcInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsNpIpv4MbsmMcInit (pEntry->pSlotInfo);
            break;
        }
        case FS_NP_IPV4_MBSM_MC_ADD_ROUTE_ENTRY:
        {
            tIpmcNpWrFsNpIpv4MbsmMcAddRouteEntry *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4MbsmMcAddRouteEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsNpIpv4MbsmMcAddRouteEntry (pEntry->u4VrId, pEntry->u4GrpAddr,
                                             pEntry->u4GrpPrefix,
                                             pEntry->u4SrcIpAddr,
                                             pEntry->u4SrcIpPrefix,
                                             pEntry->u1CallerId,
                                             pEntry->rtEntry,
                                             pEntry->u2NoOfDownStreamIf,
                                             pEntry->pDownStreamIf,
                                             pEntry->pSlotInfo);
            break;
        }
#ifdef PIM_WANTED
        case FS_PIM_MBSM_NP_INIT_HW:
        {
            tIpmcNpWrFsPimMbsmNpInitHw *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsPimMbsmNpInitHw;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsPimMbsmNpInitHw (pEntry->pSlotInfo);
            break;
        }
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED
        case FS_DVMRP_MBSM_NP_INIT_HW:
        {
            tIpmcNpWrFsDvmrpMbsmNpInitHw *pEntry = NULL;
            pEntry = &pIpmcNpModInfo->IpmcNpFsDvmrpMbsmNpInitHw;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsDvmrpMbsmNpInitHw (pEntry->pSlotInfo);
            break;
        }
#endif /* DVMRP_WANTED */
#endif
#endif
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif
