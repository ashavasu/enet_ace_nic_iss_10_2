/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: lip4ipvx.c,v 1.15 2015/07/29 10:24:31 siva Exp $
 *
 * Description:This file holds the CLI related functions 
 *             for IPv4 with LinuxIp.
 *
 *******************************************************************/
#ifndef __LIP4_IPVX_C__
#define __LIP4_IPVX_C__

#ifdef LNXIP4_WANTED

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "arp.h"
#include "rtm.h"
#include "lnxip.h"
#include "ipvx.h"
#include "lip4ipvx.h"
#include "lnxiputl.h"
#include "stdiplow.h"


UINT4               gu4Ip4IfTableLastChange = IPVX_ZERO;

/*  Functions for : InetCidrRouteTable   -- No Functions   */

/*  Functions for : Ipv4InterfaceTable       */

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4IfTblLastChange
 *
 *    DESCRIPTION      : This function Get the Time at which the ipv4 and 
 *                       ipv6 stats are updated recently. 
 *
 *    INPUT            : None.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SysUpTime                   
 *
 ****************************************************************************/
UINT4
IPvxGetIpv4IfTblLastChange (VOID)
{
    return (LNXIP_GET_IF_TBL_CHANGE_TIME ());
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxValidateIdxInsIpv4IfTable
 *
 *    DESCRIPTION      : This function validate the given ipv4 interface index
 *
 *    INPUT            : i4Ipv4InterfaceIfIndex   - ipv4 if index
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE 
 *
 ****************************************************************************/
INT1
IPvxValidateIdxInsIpv4IfTable (INT4 i4Ipv4InterfaceIfIndex)
{
    tIpConfigInfo       IpIfInfo;

    if (CfaIpIfGetIfInfo ((UINT4) i4Ipv4InterfaceIfIndex,
                          &IpIfInfo) == CFA_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetNextIndexIpv4InterfaceTable
 *
 *    DESCRIPTION      : This function give the next valid ipv4 interface index
 *                       from the given ipv4 interface index
 *
 *    INPUT            : i4Ipv4InterfaceIfIndex   - ipv4 if index
 *                     : pi4NextIpv4InterfaceIfIndex - Next ipv4 if index
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE 
 *
 ****************************************************************************/
INT1
IPvxGetNextIndexIpv4InterfaceTable (INT4 i4Ipv4InterfaceIfIndex,
                                    INT4 *pi4NextIpv4InterfaceIfIndex)
{
    INT4                i4RetVal = CFA_FAILURE;

    i4RetVal = CfaIpIfGetNextIndexIpIfTable (i4Ipv4InterfaceIfIndex,
                                             (UINT4 *)
                                             pi4NextIpv4InterfaceIfIndex);
    if (i4RetVal == CFA_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4IfEnableStatus    
 *
 *    DESCRIPTION      : This function get the IPv4 on the given interface 
 *
 *    INPUT            : i4Ipv4IfIndex        - Interface index. 
 *                     : i4Ipv4IfEnableStatus - Enable / Disable. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 *pi4Ipv4IfEnableStatus)
{
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    INT4                i4RetVal = NETIPV4_FAILURE;
    UINT4               u4IpPort = IPVX_ZERO;

    i4RetVal = NetIpv4GetPortFromIfIndex ((UINT4) i4Ipv4IfIndex, &u4IpPort);
    if (i4RetVal == NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    pPortMapNode = LnxIpGetPortMapNode (u4IpPort);
    if (pPortMapNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4Ipv4IfEnableStatus = (INT4) pPortMapNode->u1Ipv4EnableStatus;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIpv4IfEnableStatus    
 *
 *    DESCRIPTION      : This function enable/disable the IPv4 on the given
 *                       interface 
 *
 *    INPUT            : i4Ipv4IfIndex        - Interface index. 
 *                     : i4Ipv4IfEnableStatus - Enable / Disable. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 i4Ipv4IfEnableStatus)
{
    tCfaIfInfo          CfaIfInfo;
    t_IFACE_PARAMS      IfaceParams;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    UINT4               u4IpPort = IPVX_ZERO;
    INT4                i4RetVal = IP_FAILURE;
    UINT1               u1OperStatus = IPVX_ZERO;
    UINT1               u1Ipv4EnableStatus = IPVX_IPV4_ENABLE_STATUS_DOWN;
    UINT1               u1LnxOperStatus = CFA_IF_DOWN;
    UINT1               u1RtmOperStatus = RTM_OPER_DOWN_MSG;

    i4RetVal = NetIpv4GetPortFromIfIndex ((UINT4) i4Ipv4IfIndex, &u4IpPort);
    if (i4RetVal == NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    pPortMapNode = LnxIpGetPortMapNode (u4IpPort);
    if (pPortMapNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    u1OperStatus = pPortMapNode->u1OperState;
    u1Ipv4EnableStatus = pPortMapNode->u1Ipv4EnableStatus;

    if (i4Ipv4IfEnableStatus == u1Ipv4EnableStatus)
    {
        return (SNMP_SUCCESS);
    }

    if (u1OperStatus == IPIF_OPER_ENABLE)
    {
        /* If Ipv4EnableStatus is = Enabled (True) 
         *   send the OPER_UP Notification to HL
         * otherwise
         *   send the OPER_DOWN Notification to HL
         */

        MEMSET (&(IfaceParams), 0, sizeof (t_IFACE_PARAMS));
        IfaceParams.u2Port = (UINT2) u4IpPort;

        if (i4Ipv4IfEnableStatus == IPVX_IPV4_ENABLE_STATUS_UP)
        {
            u1LnxOperStatus = CFA_IF_UP;
            u1RtmOperStatus = RTM_OPER_UP_MSG;
        }
        else
        {
            u1LnxOperStatus = CFA_IF_DOWN;
            u1RtmOperStatus = RTM_OPER_DOWN_MSG;
        }

        MEMSET (&(CfaIfInfo), 0, sizeof (tCfaIfInfo));

        if (CfaGetIfInfo ((UINT4) i4Ipv4IfIndex, &CfaIfInfo) == CFA_FAILURE)
        {
            return (SNMP_FAILURE);
        }

	i4RetVal = LinuxIpUpdateInterfaceStatus (CfaIfInfo.au1IfName, CfaIfInfo.u1IfType,
                                                 u1LnxOperStatus);
        if (i4RetVal != OSIX_SUCCESS)
        {
            return (SNMP_FAILURE);
        }

        /* to notify the oper change to the higher layers */
        LnxIpNotifyIfInfo (u1RtmOperStatus, &IfaceParams);
    }

    /* Update the DS */
    pPortMapNode->u1Ipv4EnableStatus = (UINT1) i4Ipv4IfEnableStatus;

    LNXIP_SET_IF_TBL_CHANGE_TIME ();

    return (SNMP_SUCCESS);
}

/*  Functions for : IpSystemStatsTable       */
/*  Functions for : IpIfStatsTable           */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4Stats             
 *
 *    DESCRIPTION      : This function give the IP stats for the given type of
 *                       Ip Stats object
 *                       if the 'i4Ipv4IfIdx' is invalid it give the System 
 *                       level stas otherwise interface level stats.
 *                       This function use the old nmh to get the stats counter
 *                       who's Semantics are same as new Semantics and other 
 *                       counter and different Semantics counter are accessed
 *                       for the DataStructure directly.
 *
 *    INPUT            : i4Ipv4IfIdx     - Interface index or invalid if Index
 *                     : u4StatsType     - Type of the IP stats.      
 *                     : pu1Ipv4StatsObj - Ptr to the IP stats counter
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4Stats (INT4 i4Ipv4IfIdx, UINT4 u4StatsType, UINT1 *pu1Ipv4StatsObj)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4Stats = IPVX_ZERO;
    UINT4              *pu4Stats = (UINT4 *) (VOID *) pu1Ipv4StatsObj;
    tSNMP_COUNTER64_TYPE *pu8Stats =
        (tSNMP_COUNTER64_TYPE *) (VOID *) pu1Ipv4StatsObj;

    /* Interface Level is not supported so return 0 with SNMP_SUCCESS */
    if (i4Ipv4IfIdx != IPVX_INVALID_IFIDX)
    {
        *pu1Ipv4StatsObj = IPVX_ZERO;
        return (SNMP_SUCCESS);
    }

    switch (u4StatsType)
    {
        case IPVX_STATS_IN_RECEIVES:
            i1RetVal = nmhGetIpInReceives (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_HC_IN_RECEIVES:
            i1RetVal = nmhGetIpInReceives (&u4Stats);
            pu8Stats->lsn = u4Stats;
            pu8Stats->msn = IPVX_ZERO;
            break;

        case IPVX_STATS_IN_HDR_ERRS:
            i1RetVal = nmhGetIpInHdrErrors (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_IN_ADDR_ERRS:
            i1RetVal = nmhGetIpInAddrErrors (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_IN_FWD_DGRAMS:
            i1RetVal = nmhGetIpForwDatagrams (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_HC_IN_FWD_DGRAMS:
            i1RetVal = nmhGetIpForwDatagrams (&u4Stats);
            pu8Stats->lsn = u4Stats;
            pu8Stats->msn = IPVX_ZERO;
            break;

        case IPVX_STATS_IN_UNKNOWN_PROTOS:
            i1RetVal = nmhGetIpInUnknownProtos (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_IN_DISCARDS:
            i1RetVal = nmhGetIpInDiscards (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_IN_DELIVERS:
            i1RetVal = nmhGetIpInDelivers (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_HC_IN_DELIVERS:
            i1RetVal = nmhGetIpInDelivers (&u4Stats);
            pu8Stats->lsn = u4Stats;
            pu8Stats->msn = IPVX_ZERO;
            break;

        case IPVX_STATS_OUT_REQUESTS:
            i1RetVal = nmhGetIpOutRequests (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_HC_OUT_REQUESTS:
            i1RetVal = nmhGetIpOutRequests (&u4Stats);
            pu8Stats->lsn = u4Stats;
            pu8Stats->msn = IPVX_ZERO;
            break;

        case IPVX_STATS_OUT_DISCARDS:
            i1RetVal = nmhGetIpOutDiscards (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_OUT_NO_ROUTES:
            i1RetVal = nmhGetIpOutNoRoutes (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_REASM_REQDS:
            i1RetVal = nmhGetIpReasmReqds (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_REASM_OK:
            i1RetVal = nmhGetIpReasmOKs (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_REASM_FAILS:
            i1RetVal = nmhGetIpReasmFails (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_OUT_FRAG_OK:
            i1RetVal = nmhGetIpFragOKs (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_OUT_FRAG_FAILS:
            i1RetVal = nmhGetIpFragFails (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        case IPVX_STATS_OUT_FRAG_CRET:
            i1RetVal = nmhGetIpFragCreates (&u4Stats);
            *pu4Stats = u4Stats;
            break;

        default:
            *pu1Ipv4StatsObj = IPVX_ZERO;
            i1RetVal = SNMP_SUCCESS;
            break;
    }

    return (i1RetVal);
}

/*  Functions for : IpAddressPrefixTable     */

/*  Functions for : IpAddressTable           */

/*  Functions for : IpNetToPhysicalTable     */

/*  Functions for : IpDefaultRouterTable     */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxValIdxInstIpv4DefRtrTbl 
 *
 *    DESCRIPTION      : This function validate the given Ipv4 Default Router
 *                       on the given Interface 
 *
 *    INPUT            : u4Ipv4DefRtrAddr    - Default Routere address.
 *                     : i4Ipv4DefRtrIfIndex - The default Rtr learnt If.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxValIdxInstIpv4DefRtrTbl (UINT4 u4Ipv4DefRtrAddr, INT4 i4Ipv4DefRtrIfIndex)
{
    /* no support in linux IP */
    UNUSED_PARAM (u4Ipv4DefRtrAddr);
    UNUSED_PARAM (i4Ipv4DefRtrIfIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetNxtIdxIpv4DefRtrTbl  
 *
 *    DESCRIPTION      : This function get the  next valid Ipv4 Default Router
 *                       and Interface form the given values 
 *
 *    INPUT            : u4Ipv4DefRtrAddr     - Default Routere address.
 *                     : pu4NxtIpv4DefRtrAddr - Next Default Routere address.
 *                     : i4Ipv4DefRtrIfIndex  - The default Rtr learnt If.
 *                     : pi4NextIpv4DefRtrIfIndex - next default Rtr learnt If.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetNxtIdxIpv4DefRtrTbl (UINT4 u4Ipv4DefRtrAddr,
                            UINT4 *pu4NxtIpv4DefRtrAddr,
                            INT4 i4Ipv4DefRtrIfIndex,
                            INT4 *pi4NextIpv4DefRtrIfIndex)
{
    /* no support in linux IP */
    UNUSED_PARAM (u4Ipv4DefRtrAddr);
    UNUSED_PARAM (pu4NxtIpv4DefRtrAddr);
    UNUSED_PARAM (i4Ipv4DefRtrIfIndex);
    UNUSED_PARAM (pi4NextIpv4DefRtrIfIndex);
    return (SNMP_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4DefRtrLifetime 
 *
 *    DESCRIPTION      : This function give the Default Router Life time  for
 *                       given Ipv4 Default Router and Interface 
 *
 *    INPUT            : u4DefRtrAddr         - Default Routere address.
 *                     : i4IpDefRtrIfIndex    - The default Rtr learnt If.
 *                     : pu4IpDefRtrLifetime  - Life time of the default Rtr.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4DefRtrLifetime (UINT4 u4DefRtrAddr, INT4 i4IpDefRtrIfIndex,
                           UINT4 *pu4IpDefRtrLifetime)
{
    /* no support in linux IP */
    UNUSED_PARAM (u4DefRtrAddr);
    UNUSED_PARAM (i4IpDefRtrIfIndex);
    UNUSED_PARAM (pu4IpDefRtrLifetime);
    return (SNMP_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4DefRtrPreference
 *
 *    DESCRIPTION      : This function give the Default Router Preference for
 *                       given Ipv4 Default Router and Interface 
 *
 *    INPUT            : u4DefRtrAddr       - Default Routere address.
 *                     : i4IpDefRtrIfIndex  - The default Rtr learnt If.
 *                     : pu4IpDefRtrPref    - Preference of the default Rtr.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4DefRtrPreference (UINT4 u4DefRtrAddr, INT4 i4Ipv4DefRtrIfIndex,
                             UINT4 *pu4IpDefRtrPref)
{
    /* no support in linux IP */
    UNUSED_PARAM (u4DefRtrAddr);
    UNUSED_PARAM (i4Ipv4DefRtrIfIndex);
    UNUSED_PARAM (pu4IpDefRtrPref);
    return (SNMP_FAILURE);
}

/*  Functions for : IcmpStatsTable   -- No Functions     */

/*  Functions for : IcmpMsgStatsTable        */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxIpv4GetIcmpMsgStatsInPkts 
 *
 *    DESCRIPTION      : This function give the icmp stats for the given     
 *                       icmp Message Type in incomming Direction
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp Message.
 *                     : pu4RetValIcmpMsgStatsInPkts  - Ptr to Stats counter.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS 
 *
 ****************************************************************************/
INT1
IPvxIpv4GetIcmpMsgStatsInPkts (INT4 i4IcmpMsgStatsType,
                               UINT4 *pu4RetValIcmpMsgStatsInPkts)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4LnxIcmpMsgType = IPVX_ZERO;

    switch (i4IcmpMsgStatsType)
    {
        case IPVX_ICMP_ECHO_REPLY:
            u4LnxIcmpMsgType = LNX_ICMP_IN_ECHO_REPS;
            break;

        case IPVX_ICMP_DEST_UNREAC:
            u4LnxIcmpMsgType = LNX_ICMP_IN_DEST_UNREACHS;
            break;

        case IPVX_ICMP_SRC_QUENCH:
            u4LnxIcmpMsgType = LNX_ICMP_IN_SRC_QUENCHS;
            break;

        case IPVX_ICMP_REDIRECT:
            u4LnxIcmpMsgType = LNX_ICMP_IN_REDIRECTS;
            break;

        case IPVX_ICMP_ECHO:
            u4LnxIcmpMsgType = LNX_ICMP_IN_ECHOS;
            break;

        case IPVX_ICMP_TIME_EXCEEDED:
            u4LnxIcmpMsgType = LNX_ICMP_IN_TIME_EXCDS;
            break;

        case IPVX_ICMP_PARA_PROBLEM:
            u4LnxIcmpMsgType = LNX_ICMP_IN_PARM_PROBS;
            break;

        case IPVX_ICMP_TIME_STAMP:
            u4LnxIcmpMsgType = LNX_ICMP_IN_TIME_STAMPS;
            break;

        case IPVX_ICMP_TSTAMP_REPLY:
            u4LnxIcmpMsgType = LNX_ICMP_IN_TIME_STAM_REPS;
            break;

        case IPVX_ICMP_ADDR_MASK_RQST:
            u4LnxIcmpMsgType = LNX_ICMP_IN_ADDR_MASKS;
            break;

        case IPVX_ICMP_ADDR_MASK_REPLY:
            u4LnxIcmpMsgType = LNX_ICMP_IN_ADDR_MASK_REPS;
            break;

        default:
            *pu4RetValIcmpMsgStatsInPkts = IPVX_ZERO;
            return (SNMP_SUCCESS);
            break;
    }

    if (LnxIpGetObject (u4LnxIcmpMsgType, pu4RetValIcmpMsgStatsInPkts) ==
        SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxIpv4GetIcmpMsgStatsOutPkts
 *
 *    DESCRIPTION      : This function give the icmp stats for the given     
 *                       icmp Message Type in outgoing Direction
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp Message.
 *                     : pu4RetValIcmpMsgStatsOutPkts - Ptr to Stats counter.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS 
 *
 ****************************************************************************/

INT1
IPvxIpv4GetIcmpMsgStatsOutPkts (INT4 i4IcmpMsgStatsType,
                                UINT4 *pu4RetValIcmpMsgStatsOutPkts)
{
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4LnxIcmpMsgType = IPVX_ZERO;

    switch (i4IcmpMsgStatsType)
    {
        case IPVX_ICMP_ECHO_REPLY:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_ECHO_REPS;
            break;

        case IPVX_ICMP_DEST_UNREAC:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_DEST_UNREACHS;
            break;

        case IPVX_ICMP_SRC_QUENCH:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_SRC_QUENCHS;
            break;

        case IPVX_ICMP_REDIRECT:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_REDIRECTS;
            break;

        case IPVX_ICMP_ECHO:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_ECHOS;
            break;

        case IPVX_ICMP_TIME_EXCEEDED:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_TIME_EXCDS;
            break;

        case IPVX_ICMP_PARA_PROBLEM:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_PARM_PROBS;
            break;

        case IPVX_ICMP_TIME_STAMP:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_TIME_STAMPS;
            break;

        case IPVX_ICMP_ADDR_MASK_RQST:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_ADDR_MASKS;
            break;

        case IPVX_ICMP_ADDR_MASK_REPLY:
            u4LnxIcmpMsgType = LNX_ICMP_OUT_ADDR_MASK_REPS;
            break;

        default:
            *pu4RetValIcmpMsgStatsOutPkts = IPVX_ZERO;
            return (SNMP_SUCCESS);
            break;
    }

    if (LnxIpGetObject (u4LnxIcmpMsgType, pu4RetValIcmpMsgStatsOutPkts) ==
        SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

#endif

#endif /* __LIP4_IPVX_C__ */

/****************************   End of the File ***************************/
