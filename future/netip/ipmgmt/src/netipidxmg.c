/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: netipidxmg.c,v 1.1 2015/10/05 11:02:41 siva Exp $
 *
 * Description: This file contains functions that are associated
 *              with L2VpnIndex Table Manager.
 ********************************************************************/

 /*---------------------------------------------------------------------
 *  The NETIP Flow index table manager has been created to provide support for 
 *  the NETIP Flow Indexing requirements. However the usage of this library does 
 *  not restrict to this alone, rather it can be used by any application 
 *  which requires to maintain an unique set of identifiers, obtain and 
 *  release an identifier from that set.
 *
 *  The functions included in this file are 
 *  1) To initialise the index manager
 *  2) To shutdown the index manager
 *  3) Obtain a new index from an index space/group
 *  4) Obtain a specified  index from an index space/group
 *  5) Release an index to an index space/group
 *  
 *------------------------------------------------------------------------*/
#ifndef _NETIPIDXMG_C
#define _NETIPIDXMG_C

#include "lr.h"
#include "cfa.h"
#include "l2iwf.h"
#include "ip.h"
#include "rtm.h"
#include "ipvx.h"
#include "fssocket.h"
#include "chrdev.h"
#include "netdev.h"
#include "vcm.h"

#include "netipidxsz.h"
#include "netipidxgl.h"
#include "netipidxmg.h"

#define MAX_INDEXMGR_GRPS 1
extern UINT4 IssSzGetSizingParamsForModule (CHR1 * pu1ModName, CHR1 * pu1MacroName);
UINT1 NetipIndexManagerInit(VOID);

tOsixSemId   gNetipIndexMgrSemId;
tIndexMgrGrpInfo *gpNetipIndexMgrGrpInfo = NULL;
UINT4 au4NetipGrpMaxIndices[] = {
                          NETIP_MAX_FLOWS };

tIndexMgrGrpTable NetipIndexMgrGrpEntry[] = {
                                        { "NETIP", "NETIP_MAX_FLOWS" } 
                                       };
UINT4 aNetipAvlByteBmap[] = {
                        0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF
                       };
UINT4 aNetipAvlBitmap[] = {
   0x80000000, 0x40000000, 0x20000000, 0x10000000,
   0x08000000, 0x04000000, 0x02000000, 0x01000000,
   0x00800000, 0x00400000, 0x00200000, 0x00100000,
   0x00080000, 0x00040000, 0x00020000, 0x00010000,
   0x00008000, 0x00004000, 0x00002000, 0x00001000,
   0x00000800, 0x00000400, 0x00000200, 0x00000100,
   0x00000080, 0x00000040, 0x00000020, 0x00000010,
   0x00000008, 0x00000004, 0x00000002, 0x00000001
};
UINT4 aNetipAsgnBitmap[] = {
   0x7FFFFFFF, 0xBFFFFFFF, 0xDFFFFFFF, 0xEFFFFFFF,
   0xF7FFFFFF, 0xFBFFFFFF, 0xFDFFFFFF, 0xFEFFFFFF,
   0xFF7FFFFF, 0xFFBFFFFF, 0xFFDFFFFF, 0xFFEFFFFF,
   0xFFF7FFFF, 0xFFFBFFFF, 0xFFFDFFFF, 0xFFFEFFFF,
   0xFFFF7FFF, 0xFFFFBFFF, 0xFFFFDFFF, 0xFFFFEFFF,
   0xFFFFF7FF, 0xFFFFFBFF, 0xFFFFFDFF, 0xFFFFFEFF,
   0xFFFFFF7F, 0xFFFFFFBF, 0xFFFFFFDF, 0xFFFFFFEF,
   0xFFFFFFF7, 0xFFFFFFFB, 0xFFFFFFFD, 0xFFFFFFFE
};

/*---------------------------------------------------------------------------
 * Function    : NetipIndexMgrInitWithSem
 * Description : This funciton creates the index manager semaphore and 
 *               initialises the index manager.
 * Inputs      : None. 
 * Outputs     : The global variable gpNetipIndexMgrGrpInfo will be allocated
 *               memory and suitably updated in case of successful 
 *               allocation. Another Pool Id corresponds to the free memory
 *               pool assigned to all required chunks for every group.
 * Returns     : NETIP_SUCCESS on successful memory allocation 
 *               and initialisation. Returns NETIP_FAILURE o.w.
---------------------------------------------------------------------------*/
UINT1
NetipIndexMgrInitWithSem (VOID)
{
    /* Semaphore Create */
    if (OsixCreateSem (NETIP_INDEXMGR_SEM_NAME, NETIP_ONE, NETIP_ZERO, &gNetipIndexMgrSemId) !=
        OSIX_SUCCESS)
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR : Semaphore Create Failed\n");
        return NETIP_FAILURE;
    }
    if (NetipIndexManagerInit () == NETIP_FAILURE)
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR : Initialization Failed\n");
        return NETIP_FAILURE;
    }
    return NETIP_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : NetipIndexManagerInit
 * Description : This function allocates the necessary structures for the 
 *               Index Tbl manager. This function must be invoked before 
 *               using any other routines associated with the Index Manager.
 * Inputs      : None. 
 * Outputs     : The global variable gpNetipIndexMgrGrpInfo will be allocated
 *               memory and suitably updated in case of successful 
 *               allocation. Another pool id corresponds to the free memory
 *               pool assigned to all required chunks for every group.
 * Returns     : NETIP_SUCCESS on successful memory allocation 
 *               and initialisation. Returns NETIP_FAILURE o.w.
---------------------------------------------------------------------------*/
UINT1
NetipIndexManagerInit (VOID)
{
    UINT1               u1Index1 = NETIP_ZERO;
    UINT1               u1Index2 = NETIP_ZERO;
    UINT4               u4Index  = NETIP_ZERO;
    UINT4               u4TotalChunksReqd = NETIP_ZERO;    /* sum of maxchunks for every grp */
    UINT4               u4MaxIndices = NETIP_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = NULL;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;

    if (FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
        u4PreAllocatedUnits != MAX_INDEXMGR_INDEX_MGR_GRP)
    {
        FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
            u4StructSize
            = (sizeof (tIndexMgrGrpInfo) *
               FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
               u4PreAllocatedUnits);
        FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
            u4PreAllocatedUnits = MAX_INDEXMGR_INDEX_MGR_GRP;
    }

    if (FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
        u4PreAllocatedUnits != MAX_INDEXMGR_INDEX_MGR_CHUNK)
    {
        FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
            u4StructSize
            = (sizeof (tIndexMgrChunkInfo) *
               FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
               u4PreAllocatedUnits);
        FsNETIPINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
            u4PreAllocatedUnits = MAX_INDEXMGR_INDEX_MGR_CHUNK;
    }

    for (u1Index1 = NETIP_ZERO; u1Index1 < MAX_INDEXMGR_GRPS; u1Index1++)
    {
        u4MaxIndices = NETIP_ZERO;

        u4MaxIndices
            = IssSzGetSizingParamsForModule (NetipIndexMgrGrpEntry[u1Index1].
                                             pc1ModName,
                                             NetipIndexMgrGrpEntry[u1Index1].
                                             pc1GrpName);

        if (u4MaxIndices != NETIP_ZERO)
        {
            au4NetipGrpMaxIndices[u1Index1] = u4MaxIndices;
        }
    }

    /* Create Mempools for Index manager. */
    if (NetipIndexmgrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return NETIP_FAILURE;
    }

    /* Memory for Index Groups structure being allocated */
    if (NULL == (gpNetipIndexMgrGrpInfo = MemAllocMemBlk (GROUP_INFO_POOL_ID)))
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT,
                     "INDEXMGR : Index Group Info - Mem alloc Failure\n");
        return NETIP_FAILURE;
    }

    /* Now to initialize each group */
    if (NETIP_FAILURE == NetipIndexMgrInitGrps ())
    {
        /* Free all allocated memory & exit */
        MemReleaseMemBlock (GROUP_INFO_POOL_ID, (UINT1 *) gpNetipIndexMgrGrpInfo);
        gpNetipIndexMgrGrpInfo = NULL;
        NETIP_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR : Index Group Info Init Failure\n");
        return NETIP_FAILURE;
    }

    /* Now to allocate memory for the Max-chunk-info structs for each grp */
    pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
    for (u1Index1 = NETIP_ZERO; u1Index1 < MAX_INDEXMGR_GRPS;
         u1Index1++, pIndexMgrGrpInfo++)
    {
        if (NULL == (pIndexMgrGrpInfo->pIndexChunkInfoTbl =
                     MemAllocMemBlk (CHUNK_INFO_POOL_ID)))
        {
            /* Release all memory unto this group & exit */
            pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
            for (u1Index2 = NETIP_ZERO; u1Index2 < u1Index1;
                 u1Index2++, pIndexMgrGrpInfo++)
            {
                MemReleaseMemBlock (CHUNK_INFO_POOL_ID, (UINT1 *)
                                    pIndexMgrGrpInfo->pIndexChunkInfoTbl);
                pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;
            }

            MemReleaseMemBlock (GROUP_INFO_POOL_ID,
                                (UINT1 *) gpNetipIndexMgrGrpInfo);
            gpNetipIndexMgrGrpInfo = NULL;

            NETIP_FLOW_DBG (DBG_ERR_CRT,
                         "INDEXMGR : Chunk-info-table - Mem alloc Failure\n");

            return NETIP_FAILURE;
        }

        /* Initialize the chunk-info structure array */
        MEMSET ((VOID *) pIndexMgrGrpInfo->pIndexChunkInfoTbl, NETIP_ZERO,
                sizeof (tIndexMgrChunkInfo) *
                pIndexMgrGrpInfo->u4MaxChunksSprtd);

        u4TotalChunksReqd += pIndexMgrGrpInfo->u4MaxChunksSprtd;
    }

    /* Now to assign appropriate no. of chunks to each group */
    pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
    for (u1Index1 = NETIP_ZERO; u1Index1 < MAX_INDEXMGR_GRPS;
         u1Index1++, pIndexMgrGrpInfo++)
    {
        /* Now to allocate memory from pool for every chunk-info per group */
        pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
        for (u4Index = NETIP_ZERO; u4Index < pIndexMgrGrpInfo->u4MaxChunksSprtd;
             u4Index++, pIndexMgrChunkInfo++)
        {
            /* Allocate memory for the actual chunk */

            pIndexMgrChunkInfo->pu4IndexChunk = (UINT4 *)
                MemAllocMemBlk (CHUNK_TBL_POOL_ID);

            if (pIndexMgrChunkInfo->pu4IndexChunk == NULL)
            {
                NetipIndexMgrDeInit ();
                NETIP_FLOW_DBG (DBG_ERR_CRT,
                             "INDEXMGR : Init chunk mem-alloc failure.\n");
                return NETIP_FAILURE;
            }

            MEMSET ((VOID *) pIndexMgrChunkInfo->pu4IndexChunk,
                    0xFF, MAX_NETIPINDEXMGR_CHUNK_SIZE);
            /* All bits 1 => all avail */
            pIndexMgrGrpInfo->u4NumChunksInGrp++;
        }
    }
    return NETIP_SUCCESS;
}

/*------------------------------------------------------------------------
 * * Function    : NetipIndexMgrLock
 * * Description : This function takes the index manager lock.
 * * Inputs      : None
 * * Outputs     : None
 * * Returns     : None
 * ------------------------------------------------------------------------*/
VOID
NetipIndexMgrLock (VOID)
{
    OsixSemTake (gNetipIndexMgrSemId);
}

/*------------------------------------------------------------------------
 * * Function    : NetipIndexMgrUnLock
 * * Description : This function gives the index manager lock.
 * * Inputs      : None
 * * Outputs     : None
 * * Returns     : None
 * ------------------------------------------------------------------------*/
VOID
NetipIndexMgrUnLock (VOID)
{
    OsixSemGive (gNetipIndexMgrSemId);
}

/*------------------------------------------------------------------------
* Function    : NetipIndexMgrDeInit
* Description : This function releases all the memory allocated for the 
*               entire data-structure. (Use for graceful shutdown)
* Inputs      : None
* Outputs     : None
* Returns     : NETIP_SUCCESS/NETIP_FAILURE
------------------------------------------------------------------------*/

UINT1
NetipIndexMgrDeInit (VOID)
{
    UINT1               u1Index = NETIP_ZERO;    /* for groups */
    UINT4               u4Index = NETIP_ZERO;    /* for chunk infos per group */
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;

    /* Now to release the memory in reverse order of allocation */
    for (u1Index = NETIP_ZERO; u1Index < MAX_INDEXMGR_GRPS;
         u1Index++, pIndexMgrGrpInfo++)
    {
        /* Now to go thru' the chunk-info list */
        pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;

        for (u4Index = NETIP_ZERO; u4Index < pIndexMgrGrpInfo->u4NumChunksInGrp;
             u4Index++, pIndexMgrChunkInfo++)
        {
            /* Release memory for the actual chunks to the pool */
            if (NULL == pIndexMgrGrpInfo->pIndexChunkInfoTbl)
            {
                continue;        /* This will never happen actually */
            }

            MemReleaseMemBlock (CHUNK_TBL_POOL_ID,
                                (UINT1 *) pIndexMgrChunkInfo->pu4IndexChunk);
            pIndexMgrChunkInfo->pu4IndexChunk = NULL;
        }

        /* Now to release memory for every chunk-info per group */
        MemReleaseMemBlock (CHUNK_INFO_POOL_ID,
                            (UINT1 *) pIndexMgrGrpInfo->pIndexChunkInfoTbl);
        pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;
    }

    MemReleaseMemBlock (GROUP_INFO_POOL_ID, (UINT1 *) gpNetipIndexMgrGrpInfo);
    pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo = NULL;

    /* Delete Index Manager Memory Pools. */
    NetipIndexmgrSizingMemDeleteMemPools ();

    return NETIP_SUCCESS;
}

/*--------------------------------------------------------------------------
 * Function    : NetipIndexMgrGetIndexBasedOnFlag 
 * Description : This function returns a unique index for the specified 
 *               group. Returns 0 if all indices for the
 *               specified group have been exhausted.
 * Input       : GroupID (1 = NETIP Flow Indices). 
 *               Flag(FALSE = 0 ,TRUE=1) 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
NetipIndexMgrGetIndexBasedOnFlag (UINT1 u1GrpID, BOOL1 bFlag)
{
    UINT4               u4Index = NETIP_ZERO;
    UINT1               u1BytePos = NETIP_ZERO;
    UINT1               u1BitPos = NETIP_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    if (gpNetipIndexMgrGrpInfo == NULL)
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT,
                     "INDEXMGR :Index Manager is not initialized \n");
        return NETIP_ZERO;
    }

    /* Check if indices are available for the mentioned group */
    if (INDEXMGR_GRP_FULL == NetipIndexMgrCheckGrpFull (u1GrpID))
    {
        return NETIP_ZERO;
    }

    pIndexMgrGrpInfo += (u1GrpID - NETIP_ONE);
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += pIndexMgrGrpInfo->u4AvailChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

    /* Now to check each byte of it to get the avail-byte & bit positions */
    if (NETIP_FAILURE ==
        NetipIndexMgrGetAvailByteBitPos (*pu4AvailOffset, &u1BytePos, &u1BitPos))
    {
        /* This is never possible */
        NETIP_FLOW_DBG (DBG_ERR_CRT,
                     "INDEXMGR : GetIndex : Byte, bit pos. fail \n");
        return NETIP_ZERO;
    }

    /* Now to calculate the index value corresponding to this bit found */
    u4Index =
        (UINT4)((pIndexMgrGrpInfo->u4AvailChunkID * (UINT4)MAX_INDEXMGR_INDICES_PER_CHUNK)
        + (pIndexMgrChunkInfo->u4CurOffset * (UINT4)MAX_INDEXMGR_INDICES_PER_ENTRY)
        + ((UINT4)u1BytePos * (UINT4)INDEXMGR_BYTE_BLOCK_SIZE) +(UINT4)(u1BitPos));

    /* If Index Manager Flag is Set , mark the bit as allocated and adjust to the next available offset */

    if (bFlag == TRUE)
    {

        /* Now to mark that bit as allocated (make it zero) */
        *pu4AvailOffset &=
            aNetipAsgnBitmap[(u1BytePos * INDEXMGR_BYTE_BLOCK_SIZE) + u1BitPos];

        /* Now to increment the allocated-indices count */
        pIndexMgrChunkInfo->u4NumKeysAlloc++;
        pIndexMgrGrpInfo->u4TotalKeysAlloc++;

        /* Now to adjust the next available offset and/or block */
        if (NETIP_FAILURE == NetipIndexMgrUpdateChunkOffset (u1GrpID))
        {
            /* ideally, shouldn't fail */
            NETIP_FLOW_DBG (DBG_ERR_CRT,
                         "INDEXMGR : Offset Adjustment Failure \n");
            return NETIP_ZERO;
        }
    }

    /* as u4Index is from 1 (& not 0) to max. sprtd. by grp */
    return (u4Index + NETIP_ONE);
}

/*--------------------------------------------------------------------------
 * Function    : NetipIndexMgrGetAvailableIndex 
 * Description : This function returns a unique index for the specified 
 *               group. Returns 0 if all indices for the
 *               specified group have been exhausted.
 * Input       : GroupID (1 = NETIP FlowIndices). 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
NetipIndexMgrGetAvailableIndex (UINT1 u1GrpID)
{
    UINT4               u4Index = NETIP_ZERO;
    NETIP_INDEXMGR_LOCK ();
    u4Index = NetipIndexMgrGetIndexBasedOnFlag (u1GrpID, TRUE);
    NETIP_INDEXMGR_UNLOCK ();
    return u4Index;
}

/*--------------------------------------------------------------------------
 * Function    : NetipIndexMgrSetIndexBasedOnFlag 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs      : GroupID (1 = NETIP Flow Indices). 
 *               bFlag(FALSE = 0 ,TRUE=1)
 *               Index Value. 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/
UINT4
NetipIndexMgrSetIndexBasedOnFlag (UINT1 u1GrpID, UINT4 u4Index, BOOL1 bFlag)
{
    UINT4               u4BitPos = NETIP_ZERO;
    UINT4               u4ChunkID = NETIP_ZERO;
    UINT4               u4Offset = NETIP_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    /* First check if the group id  is valid */

    if ((u1GrpID < NETIP_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID \n");
        return NETIP_ZERO;
    }

    /* Check if the index to be set  is valid */
    if ((u4Index < NETIP_ONE) || (u4Index > au4NetipGrpMaxIndices[u1GrpID - NETIP_ONE]))
    {
        NETIP_FLOW_DBG2 (DBG_ERR_CRT,
                      "INDEXMGR : Set Index FAILURE - %d not in range for Group %d\n",
                      u4Index, u1GrpID);
        return NETIP_ZERO;
    }

    pIndexMgrGrpInfo += (u1GrpID - NETIP_ONE);

    /* To Calculate the appropriate bit position based on the Index Value  */

    u4ChunkID = ((u4Index - NETIP_ONE) / MAX_INDEXMGR_INDICES_PER_CHUNK);
    u4Offset =
        (((u4Index -
           NETIP_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) /
         MAX_INDEXMGR_INDICES_PER_ENTRY);
    u4BitPos =
        (((u4Index -
           NETIP_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) %
         MAX_INDEXMGR_INDICES_PER_ENTRY);

    /* Now to reach that offset */
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += u4ChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += u4Offset;

    /* To Check if this particular bit is already allocated or not. If not allocated , set it . 
     * Otherwise return failure */

    if ((*pu4AvailOffset & (~(aNetipAsgnBitmap[u4BitPos]))) == NETIP_ZERO)
    {
        /* Index is Already Allocated */
        return NETIP_ZERO;
    }

    else
    {
        if (bFlag == TRUE)
        {
            /* Now to mark that bit as allocated (make it zero) */
            (*pu4AvailOffset) &= aNetipAsgnBitmap[u4BitPos];

            /* Now to increment the allocated-indices count */
            pIndexMgrChunkInfo->u4NumKeysAlloc++;
            pIndexMgrGrpInfo->u4TotalKeysAlloc++;

            /* Now to adjust the next available offset and/or block */
            if (NETIP_ZERO == NetipIndexMgrUpdateChunkOffset (u1GrpID))
            {
                /* ideally, shouldn't fail */
                NETIP_FLOW_DBG (DBG_ERR_CRT,
                             "INDEXMGR : Offset Adjustment Failure \n");
                return NETIP_ZERO;
            }

        }

    }

    return u4Index;

}

/*--------------------------------------------------------------------------
 * Function    : NetipIndexMgrSetIndex 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.). 
 *               Index Value. 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
NetipIndexMgrSetIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4SetIndex = NETIP_ZERO;
    NETIP_INDEXMGR_LOCK ();
    u4SetIndex = NetipIndexMgrSetIndexBasedOnFlag (u1GrpID, u4Index, TRUE);
    NETIP_INDEXMGR_UNLOCK ();
    return u4SetIndex;
}

/*--------------------------------------------------------------------------
 * Function    : NetipIndexMgrCheckIndex 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
NetipIndexMgrCheckIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4CheckIndex = NETIP_ZERO;
    NETIP_INDEXMGR_LOCK ();
    u4CheckIndex = NetipIndexMgrSetIndexBasedOnFlag (u1GrpID, u4Index, FALSE);
    NETIP_INDEXMGR_UNLOCK ();
    return u4CheckIndex;
}

/*---------------------------------------------------------------------------
 * Function    : NetipIndexMgrRelIndex 
 * Description : This function releases an index to the specified group. 
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : NETIP_SUCCESS if able to 
 *               0 otherwise.
---------------------------------------------------------------------------*/

UINT1
NetipIndexMgrRelIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4BitPos  = NETIP_ZERO;
    UINT4               u4ChunkID = NETIP_ZERO;
    UINT4               u4Offset  = NETIP_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    /* First check if the group id  is valid */

    if ((u1GrpID < NETIP_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID  \n");
        return NETIP_ZERO;
    }

    NETIP_INDEXMGR_LOCK ();

    pIndexMgrGrpInfo += (u1GrpID - NETIP_ONE);

    /* Check if the index to be released is valid */
    if ((u4Index < NETIP_ONE) || (u4Index > au4NetipGrpMaxIndices[u1GrpID - NETIP_ONE]))
    {
        NETIP_FLOW_DBG2 (DBG_ERR_CRT,
                      "INDEXMGR : Rel Index FAILURE - %d not in range for Group %d\n",
                      u4Index, u1GrpID);
        NETIP_INDEXMGR_UNLOCK ();
        return NETIP_ZERO;
    }

    /* OK to release an already released index (situation shouldn't occur) */

    u4ChunkID = ((u4Index - NETIP_ONE) / MAX_INDEXMGR_INDICES_PER_CHUNK);
    u4Offset =
        (((u4Index -
           NETIP_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) /
         MAX_INDEXMGR_INDICES_PER_ENTRY);
    u4BitPos =
        (((u4Index -
           NETIP_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) %
         MAX_INDEXMGR_INDICES_PER_ENTRY);

    /* Now to reach that offset */
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += u4ChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += u4Offset;

    /* Now to make this particular bit zero */
    (*pu4AvailOffset) |= aNetipAvlBitmap[u4BitPos];

    /* Now to readjust the available-offset order of these 2 if's imp */
    if (MAX_INDEXMGR_INDICES_PER_CHUNK == pIndexMgrChunkInfo->u4NumKeysAlloc)
    {
        /* offset was positioned at start, so now adjust it */
        pIndexMgrChunkInfo->u4CurOffset = u4Offset;
    }

    if (u4Offset < (pIndexMgrChunkInfo->u4CurOffset))
    {
        pIndexMgrChunkInfo->u4CurOffset = u4Offset;
    }

    /* Now to readjust the available-chunkID (order of these 2 if's is imp) */
    if (pIndexMgrGrpInfo->u4TotalKeysAlloc == au4NetipGrpMaxIndices[u1GrpID - NETIP_ONE])
    {
        /* Available chunk was positioned at start, so now readjust */
        pIndexMgrGrpInfo->u4AvailChunkID = u4ChunkID;
    }

    if (u4ChunkID < (pIndexMgrGrpInfo->u4AvailChunkID))
    {
        pIndexMgrGrpInfo->u4AvailChunkID = u4ChunkID;
    }

    /* Now to decrement the no. of keys allocated */
    (pIndexMgrChunkInfo->u4NumKeysAlloc)--;
    (pIndexMgrGrpInfo->u4TotalKeysAlloc)--;

    NETIP_INDEXMGR_UNLOCK ();
    return NETIP_SUCCESS;
}

/*------------------------------------------------------------------------
* Function    : IndexMgrInitGroups
* Description : Initializes the fields of all the group structures involved
* Inputs      : None
* Outputs     : None
* Returns     : NETIP_SUCCESS/NETIP_FAILURE 
-------------------------------------------------------------------------*/

UINT1
NetipIndexMgrInitGrps ()
{
    UINT1               u1Index = NETIP_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;

    for (u1Index = 0; u1Index < MAX_INDEXMGR_GRPS;
         u1Index++, pIndexMgrGrpInfo++)
    {
        pIndexMgrGrpInfo->u4NumChunksInGrp = NETIP_ZERO;
        pIndexMgrGrpInfo->u4AvailChunkID = NETIP_ZERO;
        pIndexMgrGrpInfo->u4TotalKeysAlloc = NETIP_ZERO;
        pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;

        pIndexMgrGrpInfo->u4MaxChunksSprtd =
            (((au4NetipGrpMaxIndices[u1Index] % MAX_INDEXMGR_INDICES_PER_CHUNK) == NETIP_ZERO)
             ? (au4NetipGrpMaxIndices[u1Index] / MAX_INDEXMGR_INDICES_PER_CHUNK)
             : (au4NetipGrpMaxIndices[u1Index] / MAX_INDEXMGR_INDICES_PER_CHUNK +
                NETIP_ONE));
    }

    return NETIP_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : NetipIndexMgrUpdateChunkOffset 
 * Description : This function searches for a new offset for allocation of 
 *               index in the same chunk that delivered the latest index. 
 *               If this chunk is full, it proceeds with other chunks.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : NETIP_SUCCESS if updation successful 
 *               0 otherwise.
---------------------------------------------------------------------------*/

UINT1
NetipIndexMgrUpdateChunkOffset (UINT1 u1GrpID)
{
    UINT4               u4MaxIndices = NETIP_ZERO;
    UINT4               u4Index = NETIP_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    if ((u1GrpID < NETIP_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        return NETIP_FAILURE;
    }

    pIndexMgrGrpInfo += (u1GrpID - NETIP_ONE);
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += pIndexMgrGrpInfo->u4AvailChunkID;

    if (MAX_INDEXMGR_INDICES_PER_CHUNK == pIndexMgrChunkInfo->u4NumKeysAlloc)
    {
        /* This chunk is full */
        pIndexMgrChunkInfo->u4CurOffset = NETIP_ZERO;    /* reposition to 1st */

        u4MaxIndices = au4NetipGrpMaxIndices[u1GrpID - NETIP_ONE];

        if (u4MaxIndices == pIndexMgrGrpInfo->u4TotalKeysAlloc)
        {
            /* All chunks are full */
            pIndexMgrGrpInfo->u4AvailChunkID = NETIP_ZERO;
            /* Reposition to start */
            /* Nothing else needs to be done */
            return NETIP_SUCCESS;
        }
        else
        {
            /* Search for a new Chunk from next chunk */
            pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
            pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

            for (u4Index = ((pIndexMgrGrpInfo->u4AvailChunkID) + NETIP_ONE);
                 u4Index < pIndexMgrGrpInfo->u4NumChunksInGrp; u4Index++)
            {
                pIndexMgrChunkInfo++;

                if (pIndexMgrChunkInfo->u4NumKeysAlloc
                    < MAX_INDEXMGR_INDICES_PER_CHUNK)
                {
                    /* This chunk is available */
                    pIndexMgrGrpInfo->u4AvailChunkID = u4Index;
                    return NETIP_SUCCESS;
                }
            }
        }
    }
    else                        /* search for an offset in this chunk itself */
    {
        pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
        pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

        for (u4Index = pIndexMgrChunkInfo->u4CurOffset;
             u4Index < MAX_INDEXMGR_ENT_PER_CHUNK; u4Index++, pu4AvailOffset++)
        {
            if ((*pu4AvailOffset) != NETIP_ZERO)
            {
                pIndexMgrChunkInfo->u4CurOffset = u4Index;
                break;
            }
        }
    }

    return NETIP_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : NetipIndexMgrCheckGrpFull 
 * Description : This function checks if indices are available for the 
 *               specified group. 
 * Inputs      : GroupID (1 = Netip Flow Indices).
 *               Index value
 * Outputs     : None
 * Returns     : INDEXMGR_GRP_FULL if all indices allocated 
 *               INDEXMGR_GRP_VACANT otherwise.
---------------------------------------------------------------------------*/

UINT1
NetipIndexMgrCheckGrpFull (UINT1 u1GrpID)
{
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpNetipIndexMgrGrpInfo;

    /* First check if the group id  is valid */
    if ((u1GrpID < NETIP_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID \n");
        return INDEXMGR_GRP_FULL;
    }

    pIndexMgrGrpInfo += (u1GrpID - NETIP_ONE);

    if (pIndexMgrGrpInfo->u4TotalKeysAlloc == au4NetipGrpMaxIndices[u1GrpID - NETIP_ONE])
    {
        NETIP_FLOW_DBG (DBG_ERR_CRT,
                     "INDEXMGR : Index Unavailable : Group full \n");
        return INDEXMGR_GRP_FULL;
    }

    return INDEXMGR_GRP_VACANT;
}

/*---------------------------------------------------------------------------
 * Function    : NetipIndexMgrGetAvailByteBitPos
 * Description : This function inspects the UINT4 Value to find the 1st bit 
 *               position which is allocatable (1).
 * Inputs      : u4AvailIndexBitmap : U4 value to inspect in.
 * Outputs     : pu1BytePos : Shall contain position of the byte holding the
 *               1st allocatable bit pos. (0 to 3)
 *               pu1BitPos : Bit pos. of allocatable bit in the byte.(0-7)   
 * Returns     : NETIP_SUCCESS if able to 
 *               0 otherwise. (should never happen)
---------------------------------------------------------------------------*/

UINT1
NetipIndexMgrGetAvailByteBitPos (UINT4 u4AvailIndexBitmap, UINT1 *pu1BytePos,
                            UINT1 *pu1BitPos)
{
    /*if no bit available(u4AvailIndexBitmap == 0), return failure */
    if (NETIP_ZERO == u4AvailIndexBitmap)    /* Ideally should never occur */
    {
        return NETIP_ZERO;
    }

    /* Now to check every byte to get a (bit = 1) */
    for (*pu1BytePos = NETIP_ZERO; *pu1BytePos < sizeof (u4AvailIndexBitmap);
         (*pu1BytePos)++)
    {
        if (u4AvailIndexBitmap & aNetipAvlByteBmap[*pu1BytePos])
        {
            break;
        }
    }

    if (*pu1BytePos == sizeof (u4AvailIndexBitmap))
    {
        return NETIP_FAILURE;
    }

    /* Now to get the bit position */
    for (*pu1BitPos = NETIP_ZERO; *pu1BitPos < INDEXMGR_BYTE_BLOCK_SIZE; (*pu1BitPos)++)
    {
        if (u4AvailIndexBitmap &
            aNetipAvlBitmap[((*pu1BytePos) * INDEXMGR_BYTE_BLOCK_SIZE)
                       + (*pu1BitPos)])
        {
            break;
        }
    }

    return NETIP_SUCCESS;
}




/*--------------------------------------------------------------------------
                        End of file netipidxmg.c                           */
/*-------------------------------------------------------------------------*/
#endif
