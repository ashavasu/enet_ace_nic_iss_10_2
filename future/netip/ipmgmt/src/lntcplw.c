/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lntcplw.c,v 1.9 2011/05/04 13:26:40 siva Exp $
 *
 * Description:Network management routines for STDTCP mib.   
 *             Includes nmh routines to interface with LNXIP. 
 *
 *******************************************************************/
#ifndef __LNTCPLW_C__
#define __LNTCPLW_C__

#include "lr.h"
#include "stdtclow.h"
#include "lnxiputl.h"

/* LOW LEVEL Routines for Table : TcpConnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceTcpConnTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceTcpConnTable (UINT4 u4TcpConnLocalAddress,
                                      INT4 i4TcpConnLocalPort,
                                      UINT4 u4TcpConnRemAddress,
                                      INT4 i4TcpConnRemPort)
{
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexTcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstTcpConnTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexTcpConnTable (UINT4 *pu4TcpConnLocalAddress,
                              INT4 *pi4TcpConnLocalPort,
                              UINT4 *pu4TcpConnRemAddress,
                              INT4 *pi4TcpConnRemPort)
{
    return (nmhGetNextIndexTcpConnTable (0, pu4TcpConnLocalAddress,
                                         0, pi4TcpConnLocalPort,
                                         0, pu4TcpConnRemAddress,
                                         0, pi4TcpConnRemPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexTcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                nextTcpConnLocalAddress
                TcpConnLocalPort
                nextTcpConnLocalPort
                TcpConnRemAddress
                nextTcpConnRemAddress
                TcpConnRemPort
                nextTcpConnRemPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextTcpConnTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexTcpConnTable (UINT4 u4TcpConnLocalAddress,
                             UINT4 *pu4NextTcpConnLocalAddress,
                             INT4 i4TcpConnLocalPort,
                             INT4 *pi4NextTcpConnLocalPort,
                             UINT4 u4TcpConnRemAddress,
                             UINT4 *pu4NextTcpConnRemAddress,
                             INT4 i4TcpConnRemPort, INT4 *pi4NextTcpConnRemPort)
{
    tTcpConnEntry       tcpConnEntry;
    tTcpConnEntry       nextTcpConnEntry;

    tcpConnEntry.u4LocalIp = u4TcpConnLocalAddress;
    tcpConnEntry.u2LocalPort = i4TcpConnLocalPort;
    tcpConnEntry.u4RemoteIp = u4TcpConnRemAddress;
    tcpConnEntry.u2RemotePort = i4TcpConnRemPort;

    if (TcpConnTableGetNextEntry (&tcpConnEntry, &nextTcpConnEntry) ==
        SNMP_SUCCESS)
    {
        *pu4NextTcpConnLocalAddress = nextTcpConnEntry.u4LocalIp;
        *pi4NextTcpConnLocalPort = nextTcpConnEntry.u2LocalPort;
        *pu4NextTcpConnRemAddress = nextTcpConnEntry.u4RemoteIp;
        *pi4NextTcpConnRemPort = nextTcpConnEntry.u2RemotePort;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTcpConnState
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort

                The Object 
                retValTcpConnState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpConnState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpConnState (UINT4 u4TcpConnLocalAddress, INT4 i4TcpConnLocalPort,
                    UINT4 u4TcpConnRemAddress, INT4 i4TcpConnRemPort,
                    INT4 *pi4RetValTcpConnState)
{
    FILE               *pFd;
    PRIVATE UINT1       au1TcpConnEntry[LNX_MAX_ENTRY_SIZE];
    UINT1               au1Index[LNX_MAX_INDEX_SIZE];
    UINT1               au1Local[LNX_MAX_IP_PORT_SIZE];
    UINT1               au1Remote[LNX_MAX_IP_PORT_SIZE];
    INT1                FirstLn = FALSE;
    char               *token;
    tTcpConnEntry       tempTcpConnEntry;

    if ((pFd = FOPEN ("/proc/net/tcp", "r")) == NULL)
    {
        perror ("Cannot open file /proc/net/tcp ...\n");
        return 1;
    }

    /* Skipping the first line */
    FirstLn = TRUE;
    while (fgets (au1TcpConnEntry, sizeof (au1TcpConnEntry), pFd) != NULL)
    {
        if (FirstLn == TRUE)
        {
            FirstLn = FALSE;
            continue;
        }

        /* Extracting LocalIp LocalPort RemoteIp RemotePort and TcpState
         * from the file "/proc/net/tcp" */

        sscanf (au1TcpConnEntry, "%s %s %s %x", au1Index, au1Local, au1Remote,
                &tempTcpConnEntry.i4TcpState);
        token = STRTOK (au1Local, ":");
        tempTcpConnEntry.u4LocalIp = OSIX_NTOHL (strtoll (token, NULL,
                                                          LNX_HEX_BASE));
        token = STRTOK (NULL, ":");
        tempTcpConnEntry.u2LocalPort = strtol (token, NULL, LNX_HEX_BASE);

        token = STRTOK (au1Remote, ":");
        tempTcpConnEntry.u4RemoteIp = OSIX_NTOHL (strtoll (token, NULL,
                                                           LNX_HEX_BASE));
        token = STRTOK (NULL, ":");
        tempTcpConnEntry.u2RemotePort = strtol (token, NULL, LNX_HEX_BASE);

        if ((tempTcpConnEntry.u4LocalIp == u4TcpConnLocalAddress) &&
            (tempTcpConnEntry.u2LocalPort == i4TcpConnLocalPort) &&
            (tempTcpConnEntry.u4RemoteIp == u4TcpConnRemAddress) &&
            (tempTcpConnEntry.u2RemotePort == i4TcpConnRemPort))
        {
            *pi4RetValTcpConnState = tempTcpConnEntry.i4TcpState;
            fclose (pFd);
            return SNMP_SUCCESS;
        }
    }

    fclose (pFd);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTcpConnState
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort

                The Object 
                setValTcpConnState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetTcpConnState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetTcpConnState (UINT4 u4TcpConnLocalAddress, INT4 i4TcpConnLocalPort,
                    UINT4 u4TcpConnRemAddress, INT4 i4TcpConnRemPort,
                    INT4 i4SetValTcpConnState)
{
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    UNUSED_PARAM (i4SetValTcpConnState);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TcpConnState
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort

                The Object 
                testValTcpConnState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2TcpConnState (UINT4 *pu4ErrorCode, UINT4 u4TcpConnLocalAddress,
                       INT4 i4TcpConnLocalPort, UINT4 u4TcpConnRemAddress,
                       INT4 i4TcpConnRemPort, INT4 i4TestValTcpConnState)
{
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);

    if (i4TestValTcpConnState == LNX_DELETE_TCB)
        return SNMP_SUCCESS;
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TcpConnTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTcpRtoAlgorithm
 Input       :  The Indices

                The Object 
                retValTcpRtoAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpRtoAlgorithm (INT4 *pi4RetValTcpRtoAlgorithm)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_RTO_ALGM, (UINT4 *) pi4RetValTcpRtoAlgorithm)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpRtoMin
 Input       :  The Indices

                The Object 
                retValTcpRtoMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpRtoMin (INT4 *pi4RetValTcpRtoMin)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_RTO_MIN, (UINT4 *) pi4RetValTcpRtoMin)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpRtoMax
 Input       :  The Indices

                The Object 
                retValTcpRtoMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpRtoMax (INT4 *pi4RetValTcpRtoMax)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_RTO_MAX, (UINT4 *) pi4RetValTcpRtoMax)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpMaxConn
 Input       :  The Indices

                The Object 
                retValTcpMaxConn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpMaxConn (INT4 *pi4RetValTcpMaxConn)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_MAX_CONN, (UINT4 *) pi4RetValTcpMaxConn)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpActiveOpens
 Input       :  The Indices

                The Object 
                retValTcpActiveOpens
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpActiveOpens (UINT4 *pu4RetValTcpActiveOpens)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_ACTV_OPENS, pu4RetValTcpActiveOpens)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpPassiveOpens
 Input       :  The Indices

                The Object 
                retValTcpPassiveOpens
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpPassiveOpens (UINT4 *pu4RetValTcpPassiveOpens)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_PASSIVE_OPENS, pu4RetValTcpPassiveOpens)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpAttemptFails
 Input       :  The Indices

                The Object 
                retValTcpAttemptFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpAttemptFails (UINT4 *pu4RetValTcpAttemptFails)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_ATTEMPT_FAILS, pu4RetValTcpAttemptFails)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpEstabResets
 Input       :  The Indices

                The Object 
                retValTcpEstabResets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpEstabResets (UINT4 *pu4RetValTcpEstabResets)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_ESTAB_RESETS, pu4RetValTcpEstabResets)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpCurrEstab
 Input       :  The Indices

                The Object 
                retValTcpCurrEstab
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpCurrEstab (UINT4 *pu4RetValTcpCurrEstab)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_CURR_ESTAB, pu4RetValTcpCurrEstab)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpInSegs
 Input       :  The Indices

                The Object 
                retValTcpInSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpInSegs (UINT4 *pu4RetValTcpInSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_IN_SEGS, pu4RetValTcpInSegs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpOutSegs
 Input       :  The Indices

                The Object 
                retValTcpOutSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpOutSegs (UINT4 *pu4RetValTcpOutSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_OUT_SEGS, pu4RetValTcpOutSegs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpRetransSegs
 Input       :  The Indices

                The Object 
                retValTcpRetransSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpRetransSegs (UINT4 *pu4RetValTcpRetransSegs)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_RETANS_SEGS, pu4RetValTcpRetransSegs)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpInErrs
 Input       :  The Indices

                The Object 
                retValTcpInErrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpInErrs (UINT4 *pu4RetValTcpInErrs)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_IN_ERRS, pu4RetValTcpInErrs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetTcpOutRsts
 Input       :  The Indices

                The Object 
                retValTcpOutRsts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetTcpOutRsts (UINT4 *pu4RetValTcpOutRsts)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_TCP_OUT_RSTS, pu4RetValTcpOutRsts) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

#endif
