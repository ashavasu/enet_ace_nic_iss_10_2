/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fstcplw.c,v 1.6 2013/12/07 11:06:02 siva Exp $
 *
 * Description:Network management routines for STDTCP mib.   
 *             Includes nmh routines to interface with FSIP. 
 *
 *******************************************************************/
#ifndef __FSTCPLW_C__
#define __FSTCPLW_C__

#include "tcpinc.h"
#include "stdtclow.h"

/* LOW LEVEL Routines for Table : TcpConnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceTcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceTcpConnTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceTcpConnTable (UINT4 u4TcpConnLocalAddress,
                                      INT4 i4TcpConnLocalPort,
                                      UINT4 u4TcpConnRemAddress,
                                      INT4 i4TcpConnRemPort)
{
   /*** $$TRACE_LOG (ENTRY,"TcpConnLocalAddress = %u\n", u4TcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnLocalPort = %d\n", i4TcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"TcpConnRemAddress = %u\n", u4TcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnRemPort = %d\n", i4TcpConnRemPort); ***/

    UINT4               u4TcbIndex;
    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if ((GetTCBstate (u4TcbIndex) == TCPS_FREE) ||
            (i4TcpConnLocalPort != GetTCBlport (u4TcbIndex)) ||
            (i4TcpConnRemPort != GetTCBrport (u4TcbIndex)) ||
            (u4TcpConnRemAddress != GetTCBv4rip (u4TcbIndex)) ||
            (u4TcpConnLocalAddress != GetTCBv4lip (u4TcbIndex)) ||
            (GetTCBstate (u4TcbIndex) == TCPS_ABORT))
        {
            continue;
        }
        TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                     "Validate index called with \n");

        TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                          "local port = %d \nremote port = %d \n",
                          i4TcpConnLocalPort, i4TcpConnRemPort);

        TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                          "remote ip address = %x \nlocal ip address = %x\n",
                          u4TcpConnLocalAddress, u4TcpConnRemAddress);

        TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP", "returned success. \n ");

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
    TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                 "Validate index called with \n");
    TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "local port = %d \nremote port = %d \n",
                      i4TcpConnLocalPort, i4TcpConnRemPort);

    TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "remote ip address = %x \nlocal ip address = %x\n",
                      u4TcpConnLocalAddress, u4TcpConnRemAddress);
    TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP", "returned failure. \n ");

   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexTcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstTcpConnTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexTcpConnTable (UINT4 *pu4TcpConnLocalAddress,
                              INT4 *pi4TcpConnLocalPort,
                              UINT4 *pu4TcpConnRemAddress,
                              INT4 *pi4TcpConnRemPort)
{
   /*** $$TRACE_LOG (ENTRY,"TcpConnLocalAddress = %u\n", *pu4TcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnLocalPort = %d\n", *pi4TcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"TcpConnRemAddress = %u\n", *pu4TcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnRemPort = %d\n", *pi4TcpConnRemPort); ***/
    UINT4               u4TcbIndex;
    UINT4               u4FirstTcbIndex;
    UINT1               u1GotOne;

    u4FirstTcbIndex = 0;
    u1GotOne = FALSE;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }
        if (u1GotOne == FALSE)
        {
            u4FirstTcbIndex = u4TcbIndex;
            u1GotOne = TRUE;
        }
        if (u1GotOne == TRUE)
        {
            if (GetTCBv4lip (u4TcbIndex) < GetTCBv4lip (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBv4lip (u4TcbIndex) > GetTCBv4lip (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBlport (u4TcbIndex) < GetTCBlport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBlport (u4TcbIndex) > GetTCBlport (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBv4rip (u4TcbIndex) < GetTCBv4rip (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBv4rip (u4TcbIndex) > GetTCBv4rip (u4FirstTcbIndex))
            {
                continue;
            }
            if (GetTCBrport (u4TcbIndex) < GetTCBrport (u4FirstTcbIndex))
            {
                u4FirstTcbIndex = u4TcbIndex;
                continue;
            }
            if (GetTCBrport (u4TcbIndex) > GetTCBrport (u4FirstTcbIndex))
            {
                continue;
            }
        }
    }
    if (u1GotOne == FALSE)
    {
        TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                     "Get of first index in TCP ConnTable failed.\n");

   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
        return SNMP_FAILURE;
    }
    *pu4TcpConnLocalAddress = GetTCBv4lip (u4FirstTcbIndex);
    *pi4TcpConnLocalPort = GetTCBlport (u4FirstTcbIndex);
    *pu4TcpConnRemAddress = GetTCBv4rip (u4FirstTcbIndex);
    *pi4TcpConnRemPort = GetTCBrport (u4FirstTcbIndex);

    TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                 "Get of first index in TCP ConnTable returned \n");

    TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Local Address = %d \nLocal Port = %d \n",
                      *pu4TcpConnLocalAddress, *pi4TcpConnLocalPort);

    TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Remote Address = %d \nRemote Port = %d.\n",
                      *pu4TcpConnRemAddress, *pi4TcpConnRemPort);

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexTcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                nextTcpConnLocalAddress
                TcpConnLocalPort
                nextTcpConnLocalPort
                TcpConnRemAddress
                nextTcpConnRemAddress
                TcpConnRemPort
                nextTcpConnRemPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextTcpConnTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexTcpConnTable (UINT4 u4TcpConnLocalAddress,
                             UINT4 *pu4NextTcpConnLocalAddress,
                             INT4 i4TcpConnLocalPort,
                             INT4 *pi4NextTcpConnLocalPort,
                             UINT4 u4TcpConnRemAddress,
                             UINT4 *pu4NextTcpConnRemAddress,
                             INT4 i4TcpConnRemPort, INT4 *pi4NextTcpConnRemPort)
{
   /*** $$TRACE_LOG (ENTRY,"TcpConnLocalAddress = %u\n", *pu4TcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnLocalPort = %d\n", *pi4TcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"TcpConnRemAddress = %u\n", *pu4TcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnRemPort = %d\n", *pi4TcpConnRemPort); ***/

    UINT4               u4TcbIndex;
    UINT4               u4NextTcpConnLocalAddress;    /*Current getnext LocalAddr */
    UINT4               u4NextTcpConnRemAddress;    /*Current getnext RemoteAddr */
    UINT4               u4TcpCurLocalAddr;    /*Local Addresses in TCB */
    UINT4               u4TcpCurRemoteAddr;    /*Remote Addresses in TCB */
    INT4                i4NextTcpConnLocalPort;    /*Current getnext Local port */
    INT4                i4NextTcpConnRemPort;    /*Current getnext Remoteport */
    INT4                i4TcpCurRemotePort;    /*Remote port in TCB */
    INT4                i4TcpCurLocalPort;    /*Local port in TCB */
    INT4                i4Found = 0;

    /*Initialise the get next values to 0. We will update this value once we
     *identify an index greater than the requested index*/
    u4NextTcpConnLocalAddress = 0;
    i4NextTcpConnLocalPort = 0;
    u4NextTcpConnRemAddress = 0;
    i4NextTcpConnRemPort = 0;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) == TCPS_FREE ||
            GetTCBstate (u4TcbIndex) == TCPS_ABORT)
        {
            continue;
        }

        u4TcpCurLocalAddr = GetTCBv4lip (u4TcbIndex);
        i4TcpCurLocalPort = GetTCBlport (u4TcbIndex);
        u4TcpCurRemoteAddr = GetTCBv4rip (u4TcbIndex);
        i4TcpCurRemotePort = GetTCBrport (u4TcbIndex);

        /*Check whether this entry's indexes are less than the requested
         * indexes. If so, skip this entry*/
        if (u4TcpCurLocalAddr < u4TcpConnLocalAddress)
        {
            continue;
        }
        if ((u4TcpCurLocalAddr == u4TcpConnLocalAddress) &&
            (i4TcpCurLocalPort < i4TcpConnLocalPort))
        {
            continue;
        }
        if ((u4TcpCurLocalAddr == u4TcpConnLocalAddress) &&
            (i4TcpCurLocalPort == i4TcpConnLocalPort) &&
            (u4TcpCurRemoteAddr < u4TcpConnRemAddress))
        {
            continue;
        }
        if ((u4TcpCurLocalAddr == u4TcpConnLocalAddress) &&
            (i4TcpCurLocalPort == i4TcpConnLocalPort) &&
            (u4TcpCurRemoteAddr == u4TcpConnRemAddress) &&
            (i4TcpCurRemotePort <= i4TcpConnRemPort))
        {
            /*Last condition in if clause is <=, to avoid returning 
             * the incoming index!*/
            continue;
        }

        /*Now it is sure that current index is greater than the requested 
         * Index. If we didn't identified a getnext so far, we can initialise
         * the getnext value with current index.*/

        if (i4Found == 0)
        {
            u4NextTcpConnLocalAddress = u4TcpCurLocalAddr;
            i4NextTcpConnLocalPort = i4TcpCurLocalPort;
            u4NextTcpConnRemAddress = u4TcpCurRemoteAddr;
            i4NextTcpConnRemPort = i4TcpCurRemotePort;
            i4Found = 1;
            continue;
        }

        /*Check whether this entry is greater than the currently 
         * identified getnext index. If so, skip this entry*/

        if (u4TcpCurLocalAddr > u4NextTcpConnLocalAddress)
        {
            continue;
        }
        if ((u4TcpCurLocalAddr == u4NextTcpConnLocalAddress) &&
            (i4TcpCurLocalPort > i4NextTcpConnLocalPort))
        {
            continue;
        }
        if ((u4TcpCurLocalAddr == u4NextTcpConnLocalAddress) &&
            (i4TcpCurLocalPort == i4NextTcpConnLocalPort) &&
            (u4TcpCurRemoteAddr > u4NextTcpConnRemAddress))
        {
            continue;
        }
        if ((u4TcpCurLocalAddr == u4NextTcpConnLocalAddress) &&
            (i4TcpCurLocalPort == i4NextTcpConnLocalPort) &&
            (u4TcpCurRemoteAddr == u4NextTcpConnRemAddress) &&
            (i4TcpCurRemotePort > i4NextTcpConnRemPort))
        {
            continue;
        }

        /*Now it is sure that Current index is not greater than the
         * present get next value. So we need to take the new value 
         * for get next :-)*/

        u4NextTcpConnLocalAddress = u4TcpCurLocalAddr;
        i4NextTcpConnLocalPort = i4TcpCurLocalPort;
        u4NextTcpConnRemAddress = u4TcpCurRemoteAddr;
        i4NextTcpConnRemPort = i4TcpCurRemotePort;
    }
    if (i4Found == 0)
    {
        TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                     "Getnext of index returned failure.\n");
        return SNMP_FAILURE;
    }

    *pu4NextTcpConnLocalAddress = u4NextTcpConnLocalAddress;
    *pi4NextTcpConnLocalPort = i4NextTcpConnLocalPort;
    *pu4NextTcpConnRemAddress = u4NextTcpConnRemAddress;
    *pi4NextTcpConnRemPort = i4NextTcpConnRemPort;

    TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                 "GetNext in TCP ConnTable returned\n");
    TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Local Address = %d \nLocal Port = %d\n ",
                      *pu4NextTcpConnLocalAddress, *pi4NextTcpConnLocalPort);

    TCP_MOD_TRC_ARG2 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Remote Address = %d \nRemote Port = %d.\n",
                      *pu4NextTcpConnRemAddress, *pi4NextTcpConnRemPort);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTcpConnState
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort

                The Object 
                retValTcpConnState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpConnState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpConnState (UINT4 u4TcpConnLocalAddress, INT4 i4TcpConnLocalPort,
                    UINT4 u4TcpConnRemAddress, INT4 i4TcpConnRemPort,
                    INT4 *pi4RetValTcpConnState)
{
    UINT4               u4TcbIndex;

/*** $$TRACE_LOG (ENTRY,"TcpConnLocalAddress = %u\n", u4TcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnLocalPort = %d\n", i4TcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"TcpConnRemAddress = %u\n", u4TcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnRemPort = %d\n", i4TcpConnRemPort); ***/
    u4TcbIndex =
        tcpSnmpGetConnID (u4TcpConnLocalAddress, i4TcpConnLocalPort,
                          u4TcpConnRemAddress, i4TcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;
    *pi4RetValTcpConnState = GetTCBstate (u4TcbIndex);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of tcpConnState returned %d.\n",
                      *pi4RetValTcpConnState);

/*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetTcpConnState
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort

                The Object 
                setValTcpConnState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetTcpConnState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetTcpConnState (UINT4 u4TcpConnLocalAddress, INT4 i4TcpConnLocalPort,
                    UINT4 u4TcpConnRemAddress, INT4 i4TcpConnRemPort,
                    INT4 i4SetValTcpConnState)
{
   /*** $$TRACE_LOG (ENTRY,"TcpConnLocalAddress = %u\n", u4TcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnLocalPort = %d\n", i4TcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"TcpConnRemAddress = %u\n", u4TcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnRemPort = %d\n", i4TcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnState = %d\n", i4SetValTcpConnState); ***/

    UINT4               u4TcbIndex;

    /* Makefile Changes - <unused parameters> */
    UNUSED_PARAM (i4SetValTcpConnState);

    u4TcbIndex =
        tcpSnmpGetConnID (u4TcpConnLocalAddress, i4TcpConnLocalPort,
                          u4TcpConnRemAddress, i4TcpConnRemPort);
    if (u4TcbIndex == (UINT4) ERR)
        return SNMP_FAILURE;
    if (u4TcbIndex > MAX_NUM_OF_TCB)
    {
        TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                     "In set tcpConnState, out of bounds Tcb index\n");
        return SNMP_FAILURE;
    }
    OsmResetConn (u4TcbIndex);    /* Send RESET to the remote guy */
    /* Indicate connection aborted */
    FsmAbortConn (&TCBtable[u4TcbIndex], TCPE_MGMTRESET);
    TCP_MOD_TRC (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                 "In set tcpConnState, connection reset\n");
/*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2TcpConnState
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort

                The Object 
                testValTcpConnState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2TcpConnState ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2TcpConnState (UINT4 *pu4ErrorCode, UINT4 u4TcpConnLocalAddress,
                       INT4 i4TcpConnLocalPort, UINT4 u4TcpConnRemAddress,
                       INT4 i4TcpConnRemPort, INT4 i4TestValTcpConnState)
{
   /*** $$TRACE_LOG (ENTRY,"TcpConnLocalAddress = %u\n", u4TcpConnLocalAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnLocalPort = %d\n", i4TcpConnLocalPort); ***/
   /*** $$TRACE_LOG (ENTRY,"TcpConnRemAddress = %u\n", u4TcpConnRemAddress); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnRemPort = %d\n", i4TcpConnRemPort); ***/
   /*** $$TRACE_LOG (ENTRY, "TcpConnState = %d\n", i4TestValTcpConnState); ***/
    UNUSED_PARAM (u4TcpConnLocalAddress);
    UNUSED_PARAM (i4TcpConnLocalPort);
    UNUSED_PARAM (u4TcpConnRemAddress);
    UNUSED_PARAM (i4TcpConnRemPort);
    UNUSED_PARAM (pu4ErrorCode);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Connection state tested for value %d\n",
                      i4TestValTcpConnState);
    if (i4TestValTcpConnState == 12)
        return SNMP_SUCCESS;
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2TcpConnTable
 Input       :  The Indices
                TcpConnLocalAddress
                TcpConnLocalPort
                TcpConnRemAddress
                TcpConnRemPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2TcpConnTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetTcpRtoAlgorithm
 Input       :  The Indices

                The Object 
                retValTcpRtoAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpRtoAlgorithm ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpRtoAlgorithm (INT4 *pi4RetValTcpRtoAlgorithm)
{
    *pi4RetValTcpRtoAlgorithm = GetTCPstat (RtoAlgorithm);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpRtoAlogorithm returned %d\n",
                      *pi4RetValTcpRtoAlgorithm);
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

}

/****************************************************************************
 Function    :  nmhGetTcpRtoMin
 Input       :  The Indices

                The Object 
                retValTcpRtoMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpRtoMin ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpRtoMin (INT4 *pi4RetValTcpRtoMin)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValTcpRtoMin = GetTCPstat (RtoMin);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpRtoMin returned %d\n", *pi4RetValTcpRtoMin);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpRtoMax
 Input       :  The Indices

                The Object 
                retValTcpRtoMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpRtoMax ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpRtoMax (INT4 *pi4RetValTcpRtoMax)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValTcpRtoMax = GetTCPstat (RtoMax);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpRtoMax returned %d\n", *pi4RetValTcpRtoMax);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpMaxConn
 Input       :  The Indices

                The Object 
                retValTcpMaxConn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpMaxConn ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpMaxConn (INT4 *pi4RetValTcpMaxConn)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValTcpMaxConn = GetTCPstat (MaxConn);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpMaxConn returned %d\n", *pi4RetValTcpMaxConn);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpActiveOpens
 Input       :  The Indices

                The Object 
                retValTcpActiveOpens
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpActiveOpens ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpActiveOpens (UINT4 *pu4RetValTcpActiveOpens)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpActiveOpens = GetTCPstat (ActiveOpens);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpActiveOpens returned %d\n",
                      *pu4RetValTcpActiveOpens);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpPassiveOpens
 Input       :  The Indices

                The Object 
                retValTcpPassiveOpens
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpPassiveOpens ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpPassiveOpens (UINT4 *pu4RetValTcpPassiveOpens)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    *pu4RetValTcpPassiveOpens = GetTCPstat (PassiveOpens);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpPassiveOpens returned %d\n",
                      *pu4RetValTcpPassiveOpens);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpAttemptFails
 Input       :  The Indices

                The Object 
                retValTcpAttemptFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpAttemptFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpAttemptFails (UINT4 *pu4RetValTcpAttemptFails)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpAttemptFails = GetTCPstat (AttemptFails);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpAttemptFails returned %d\n",
                      *pu4RetValTcpAttemptFails);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpEstabResets
 Input       :  The Indices

                The Object 
                retValTcpEstabResets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpEstabResets ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpEstabResets (UINT4 *pu4RetValTcpEstabResets)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpEstabResets = GetTCPstat (EstabResets);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpEstabResets returned %d\n",
                      *pu4RetValTcpEstabResets);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpCurrEstab
 Input       :  The Indices

                The Object 
                retValTcpCurrEstab
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpCurrEstab ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpCurrEstab (UINT4 *pu4RetValTcpCurrEstab)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    INT4                i4Count;
    UINT4               u4TcbIndex;

    for (i4Count = u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB;
         u4TcbIndex++)
    {
        if (GetTCBstate (u4TcbIndex) == TCPS_ESTABLISHED ||
            GetTCBstate (u4TcbIndex) == TCPS_CLOSEWAIT)
        {
            i4Count++;
        }
    }
    *pu4RetValTcpCurrEstab = i4Count;
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpCurrEstab returned %d\n",
                      *pu4RetValTcpCurrEstab);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpInSegs
 Input       :  The Indices

                The Object 
                retValTcpInSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpInSegs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpInSegs (UINT4 *pu4RetValTcpInSegs)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpInSegs = GetTCPstat (InSegs);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpInSegs returned %d\n", *pu4RetValTcpInSegs);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpOutSegs
 Input       :  The Indices

                The Object 
                retValTcpOutSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpOutSegs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpOutSegs (UINT4 *pu4RetValTcpOutSegs)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpOutSegs = GetTCPstat (OutSegs);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpOutSegs returned %d\n", *pu4RetValTcpOutSegs);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpRetransSegs
 Input       :  The Indices

                The Object 
                retValTcpRetransSegs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpRetransSegs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpRetransSegs (UINT4 *pu4RetValTcpRetransSegs)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpRetransSegs = GetTCPstat (RetransSegs);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpRetransSegs returned %d\n",
                      *pu4RetValTcpRetransSegs);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpInErrs
 Input       :  The Indices

                The Object 
                retValTcpInErrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpInErrs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpInErrs (UINT4 *pu4RetValTcpInErrs)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpInErrs = GetTCPstat (InErrs);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpInErrs returned %d\n", *pu4RetValTcpInErrs);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetTcpOutRsts
 Input       :  The Indices

                The Object 
                retValTcpOutRsts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTcpOutRsts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetTcpOutRsts (UINT4 *pu4RetValTcpOutRsts)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pu4RetValTcpOutRsts = GetTCPstat (OutRsts);
    TCP_MOD_TRC_ARG1 (TCP_DBG_MAP, TCP_MGMT_TRC, "TCP",
                      "Get of TcpOutRsts returned %d\n", *pu4RetValTcpOutRsts);
    return SNMP_SUCCESS;

}

/**********************************************************************/

/*                                                                           */
/* Function     : TcpCompareIpAddress                                          */
/*                                                                           */
/* Description  : This procedure compares two ip addresses                   */
/*                                                                           */
/* Input        : addr1   :   IP address 1                                   */
/*                addr2   :   IP address 2                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TCP_EQUAL, if the IP addresses are equal                 */
/*                TCP_GREATER, if IP address 1 is greater                  */
/*                TCP_LESS, if IP address 2 is greater                     */
/*                                                                           */
/*****************************************************************************/

/**** $$TRACE_PROCEDURE_NAME = TcpCompareIpAddress ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW ****/

UINT1
TcpCompareIpAddress (UINT1 *addr1, UINT1 *addr2)
{

    UINT4               u4Addr1, u4Addr2;

    /*
     * returns TCP_EQUAL if they are equal
     * TCP_GREATER if addr1 is greater
     * TCP_LESS if addr1 is lesser
     */

    if (addr1 == NULL)
        return TCP_LESS;

    if (addr2 == NULL)
        return TCP_GREATER;

    u4Addr1 = TCP_DWFROMPDU (addr1);
    u4Addr2 = TCP_DWFROMPDU (addr2);

    if (u4Addr1 > u4Addr2)
        return TCP_GREATER;
    else if (u4Addr1 < u4Addr2)
        return TCP_LESS;

    return (TCP_EQUAL);
}

/******************************************************************************* ** RETURN  TCP_OK/ERR
 **
 ** FUNCTION
 ** Checks if a connection specified by the indices exist. It is done by a
 ** simple scan through the TCB table.
 **
 ** CALLING CONDITION
 **
 **
 ** GLOBAL VARIABLES AFFECTED none
******************************************************************************/
INT1
tcpSnmpConnTableEntryExists (u4LocalAddress, u4LocalPort, u4RemoteAddress,
                             u4RemotePort)
     UINT4               u4LocalAddress;
     UINT4               u4LocalPort;
     UINT4               u4RemoteAddress;
     UINT4               u4RemotePort;
{
    UINT4               u4TcbIndex;

    for (u4TcbIndex = INIT_COUNT; u4TcbIndex < MAX_NUM_OF_TCB; u4TcbIndex++)
    {
        if ((GetTCBstate (u4TcbIndex) == TCPS_FREE) ||
            (u4LocalPort != GetTCBlport (u4TcbIndex)) ||
            (u4RemotePort != GetTCBrport (u4TcbIndex)) ||
            (u4RemoteAddress != GetTCBv4rip (u4TcbIndex)) ||
            (u4LocalAddress != GetTCBv4lip (u4TcbIndex)) ||
            (GetTCBstate (u4TcbIndex) == TCPS_ABORT))
        {
            continue;
        }
        return TCP_OK;
    }
    return ERR;
}
#endif
