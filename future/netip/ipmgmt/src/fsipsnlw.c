/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsipsnlw.c,v 1.23 2013/07/04 13:12:53 siva Exp $
 *
 * Description:Network management routines for STDIP mib.   
 *             Includes nmh routines to interface with FSIP. 
 *             (IP stats, ICMP stats, UDP stats, etc..)   
 *
 *******************************************************************/
#ifndef __FSIPSNLW_C__
#define __FSIPSNLW_C__
#include "ipinc.h"
#include "stdiplow.h"
#include "ipcli.h"
#include "udpport.h"
#include "udpprtno.h"
#include "udpproto.h"
#include "fsmsipcli.h"

#ifdef NPAPI_WANTED
#include "ipnpwr.h"
#endif /* NPAPI_WANTED */

/****************************************************************************
 Function    :  nmhGetIpForwarding
 Input       :  The Indices

                The Object
                retValIpForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpForwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpForwarding (INT4 *pi4RetValIpForwarding)
#else
INT1
nmhGetIpForwarding (pi4RetValIpForwarding)
     INT4               *pi4RetValIpForwarding;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIpForwarding = IP_CFG_GET_FORWARDING (pIpCxt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpForwarding
 Input       :  The Indices

                The Object
                testValIpForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpForwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IpForwarding (UINT4 *pu4ErrorCode, INT4 i4TestValIpForwarding)
#else
INT1
nmhTestv2IpForwarding (pu4ErrorCode, i4TestValIpForwarding)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpForwarding;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIpForwarding == IP_FORW_ENABLE) ||
        (i4TestValIpForwarding == IP_FORW_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpForwarding
 Input       :  The Indices

                The Object
                setValIpForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpForwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIpForwarding (INT4 i4SetValIpForwarding)
#else
INT1
nmhSetIpForwarding (i4SetValIpForwarding)
     INT4                i4SetValIpForwarding;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    tIpCxt             *pIfCxt = NULL;
    UINT4               u4CxtId = 0;
    UINT4               u4FwdFlag = 0;
    UINT2               u2Index = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    /*
     * The following check is very important for the functionality of IRDP.
     * There should be an actual switch of state, only then the Irdp functions
     * should be called.
     */

    if (IP_CFG_GET_FORWARDING (pIpCxt) == (INT1) i4SetValIpForwarding)
    {
        return SNMP_SUCCESS;
    }

    IP_CFG_SET_FORWARDING (pIpCxt, (INT1) i4SetValIpForwarding);

    /* Enables routing per VR in the fast path */
    if (i4SetValIpForwarding == IP_FORW_ENABLE)
    {
        u4FwdFlag = IP_TRUE;
    }
    else
    {
        u4FwdFlag = IP_FALSE;
    }

    if (IpEnableRouting (u4CxtId, (UINT1) u4FwdFlag) != IP_SUCCESS)
    {
        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                         "Could not enable routing for VR = %d in the fast path\n",
                         IP_ZERO);
    }
    else
    {
        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                         "Enable routing on VR = %d in the fast path\n",
                         IP_ZERO);
    }

    if (pIpCxt->u4ContextId == IP_DEFAULT_CONTEXT)
    {
        if (IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_ENABLE)
        {
            IrdpSwitchToServer ();
        }
        else
        {
            IrdpSwitchToClient ();
        }
    }
    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    IPIF_LOGICAL_IFACES_SCAN (u2Index)
    {
        pIfCxt = UtilIpGetCxtFromIpPortNum (u2Index);
        if ((pIfCxt != NULL) && (u4CxtId != pIfCxt->u4ContextId))
        {
            if (gIpGlobalInfo.Ipif_config[u2Index].u1Oper != IPIF_OPER_ENABLE)
            {
                gIpGlobalInfo.Ipif_config[u2Index].u1IpForwardingEnable =
                    (UINT1) i4SetValIpForwarding;
            }
        }
    }

    IPFWD_DS_UNLOCK ();
    CfaHandleIpForwardingStatusUpdate ((UINT1) i4SetValIpForwarding);
    IncMsrForFsIpv4Scalars (i4SetValIpForwarding,
                            FsMIStdIpForwarding,
                            (sizeof (FsMIStdIpForwarding) / sizeof (UINT4)));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpDefaultTTL
 Input       :  The Indices

                The Object
                retValIpDefaultTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpDefaultTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpDefaultTTL (INT4 *pi4RetValIpDefaultTTL)
#else
INT1
nmhGetIpDefaultTTL (pi4RetValIpDefaultTTL)
     INT4               *pi4RetValIpDefaultTTL;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIpDefaultTTL = ((UINT4) IP_CFG_GET_TTL (u4CxtId));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpDefaultTTL
 Input       :  The Indices

                The Object
                testValIpDefaultTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpDefaultTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IpDefaultTTL (UINT4 *pu4ErrorCode, INT4 i4TestValIpDefaultTTL)
#else
INT1
nmhTestv2IpDefaultTTL (pu4ErrorCode, i4TestValIpDefaultTTL)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpDefaultTTL;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIpDefaultTTL >= IP_MIN_DEF_TTL)
        && (i4TestValIpDefaultTTL <= IP_MAX_DEF_TTL))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpDefaultTTL
 Input       :  The Indices

                The Object
                setValIpDefaultTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpDefaultTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIpDefaultTTL (INT4 i4SetValIpDefaultTTL)
#else
INT1
nmhSetIpDefaultTTL (i4SetValIpDefaultTTL)
     INT4                i4SetValIpDefaultTTL;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    IP_CFG_SET_TTL (pIpCxt, (UINT1) i4SetValIpDefaultTTL);

    IncMsrForFsIpv4Scalars (i4SetValIpDefaultTTL,
                            FsMIStdIpDefaultTTL,
                            (sizeof (FsMIStdIpDefaultTTL) / sizeof (UINT4)));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInReceives
 Input       :  The Indices

                The Object
                retValIpInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpInReceives (UINT4 *pu4RetValIpInReceives)
#else
INT1
nmhGetIpInReceives (pu4RetValIpInReceives)
     UINT4              *pu4RetValIpInReceives;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of packets received in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_RECIEVES, pu4RetValIpInReceives);
    *pu4RetValIpInReceives += pIpCxt->Ip_stats.u4In_rcvs;
    return SNMP_SUCCESS;
#endif /* NPAPI_WANTED */

    /* Total pkts received on interfaces */
    *pu4RetValIpInReceives = pIpCxt->Ip_stats.u4In_rcvs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInHdrErrors
 Input       :  The Indices

                The Object
                retValIpInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInHdrErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInHdrErrors (UINT4 *pu4RetValIpInHdrErrors)
#else
INT1
nmhGetIpInHdrErrors (pu4RetValIpInHdrErrors)
     UINT4              *pu4RetValIpInHdrErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of packets received
       due to Hdr Error in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_HDR_ERRORS, pu4RetValIpInHdrErrors);
    *pu4RetValIpInHdrErrors += pIpCxt->Ip_stats.u4In_hdr_err;
    return SNMP_SUCCESS;
#endif /* NPAPI_WANTED */

    *pu4RetValIpInHdrErrors = pIpCxt->Ip_stats.u4In_hdr_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInAddrErrors
 Input       :  The Indices

                The Object
                retValIpInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInAddrErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInAddrErrors (UINT4 *pu4RetValIpInAddrErrors)
#else
INT1
nmhGetIpInAddrErrors (pu4RetValIpInAddrErrors)
     UINT4              *pu4RetValIpInAddrErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    /* Address problems */
    *pu4RetValIpInAddrErrors = pIpCxt->Ip_stats.u4In_addr_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpForwDatagrams
 Input       :  The Indices

                The Object
                retValIpForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpForwDatagrams ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpForwDatagrams (UINT4 *pu4RetValIpForwDatagrams)
#else
INT1
nmhGetIpForwDatagrams (pu4RetValIpForwDatagrams)
     UINT4              *pu4RetValIpForwDatagrams;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of datagrams forwarded in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_FORW_DATAGRAMS, pu4RetValIpForwDatagrams);
    *pu4RetValIpForwDatagrams += pIpCxt->Ip_stats.u4Forw_attempts;
    return SNMP_SUCCESS;
#endif /* NPAPI_WANTED */

    /* Pkts considered eligible for forw */
    *pu4RetValIpForwDatagrams = pIpCxt->Ip_stats.u4Forw_attempts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInUnknownProtos
 Input       :  The Indices

                The Object
                retValIpInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInUnknownProtos ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInUnknownProtos (UINT4 *pu4RetValIpInUnknownProtos)
#else
INT1
nmhGetIpInUnknownProtos (pu4RetValIpInUnknownProtos)
     UINT4              *pu4RetValIpInUnknownProtos;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    /* Invalid high level protocol */
    *pu4RetValIpInUnknownProtos = pIpCxt->Ip_stats.u4Bad_proto;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInDiscards
 Input       :  The Indices

                The Object
                retValIpInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInDiscards (UINT4 *pu4RetValIpInDiscards)
#else
INT1
nmhGetIpInDiscards (pu4RetValIpInDiscards)
     UINT4              *pu4RetValIpInDiscards;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIpInDiscards = pIpCxt->Ip_stats.u4In_hdr_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInDelivers
 Input       :  The Indices

                The Object
                retValIpInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInDelivers ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInDelivers (UINT4 *pu4RetValIpInDelivers)
#else
INT1
nmhGetIpInDelivers (pu4RetValIpInDelivers)
     UINT4              *pu4RetValIpInDelivers;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* Pkts sent to high level protocols */
    *pu4RetValIpInDelivers = pIpCxt->Ip_stats.u4In_deliveries;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpOutRequests
 Input       :  The Indices

                The Object
                retValIpOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOutRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpOutRequests (UINT4 *pu4RetValIpOutRequests)
#else
INT1
nmhGetIpOutRequests (pu4RetValIpOutRequests)
     UINT4              *pu4RetValIpOutRequests;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* Total send requests made to IP    */
    *pu4RetValIpOutRequests = pIpCxt->Ip_stats.u4Out_requests;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpOutDiscards
 Input       :  The Indices

                The Object
                retValIpOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOutDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpOutDiscards (UINT4 *pu4RetValIpOutDiscards)
#else
INT1
nmhGetIpOutDiscards (pu4RetValIpOutDiscards)
     UINT4              *pu4RetValIpOutDiscards;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpOutDiscards = pIpCxt->Ip_stats.u4Out_no_routes +
        pIpCxt->Ip_stats.u4Out_gen_err + pIpCxt->Ip_stats.u4Out_frag_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpOutNoRoutes
 Input       :  The Indices

                The Object
                retValIpOutNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOutNoRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpOutNoRoutes (UINT4 *pu4RetValIpOutNoRoutes)
#else
INT1
nmhGetIpOutNoRoutes (pu4RetValIpOutNoRoutes)
     UINT4              *pu4RetValIpOutNoRoutes;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of packets discarded
       due to no route in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_DISCARDS, pu4RetValIpOutNoRoutes);
    *pu4RetValIpOutNoRoutes += pIpCxt->Ip_stats.u4Out_no_routes;
    return SNMP_SUCCESS;
#endif /* NPAPI_WANTED */

    /* Pkt discard due to no route       */
    *pu4RetValIpOutNoRoutes = pIpCxt->Ip_stats.u4Out_no_routes;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpReasmTimeout
 Input       :  The Indices

                The Object
                retValIpReasmTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmTimeout (INT4 *pi4RetValIpReasmTimeout)
#else
INT1
nmhGetIpReasmTimeout (pi4RetValIpReasmTimeout)
     INT4               *pi4RetValIpReasmTimeout;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpReasmTimeout = pIpCxt->Ip_cfg.i2Reasm_time;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpReasmReqds
 Input       :  The Indices

                The Object
                retValIpReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmReqds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmReqds (UINT4 *pu4RetValIpReasmReqds)
#else
INT1
nmhGetIpReasmReqds (pu4RetValIpReasmReqds)
     UINT4              *pu4RetValIpReasmReqds;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    /* Total reassembly requests         */
    *pu4RetValIpReasmReqds = pIpCxt->Ip_stats.u4Reasm_reqs;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpReasmOKs
 Input       :  The Indices

                The Object
                retValIpReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmOKs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmOKs (UINT4 *pu4RetValIpReasmOKs)
#else
INT1
nmhGetIpReasmOKs (pu4RetValIpReasmOKs)
     UINT4              *pu4RetValIpReasmOKs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* Total reassembly successes        */
    *pu4RetValIpReasmOKs = pIpCxt->Ip_stats.u4Reasm_oks;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpReasmFails
 Input       :  The Indices

                The Object
                retValIpReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmFails (UINT4 *pu4RetValIpReasmFails)
#else
INT1
nmhGetIpReasmFails (pu4RetValIpReasmFails)
     UINT4              *pu4RetValIpReasmFails;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* Reassembly failures               */
    *pu4RetValIpReasmFails = pIpCxt->Ip_stats.u4Reasm_fails;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpFragOKs
 Input       :  The Indices

                The Object
                retValIpFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpFragOKs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpFragOKs (UINT4 *pu4RetValIpFragOKs)
#else
INT1
nmhGetIpFragOKs (pu4RetValIpFragOKs)
     UINT4              *pu4RetValIpFragOKs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* No of outgoing pkts fragmented    */
    *pu4RetValIpFragOKs = pIpCxt->Ip_stats.u4Out_frag_pkts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpFragFails
 Input       :  The Indices

                The Object
                retValIpFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpFragFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpFragFails (UINT4 *pu4RetValIpFragFails)
#else
INT1
nmhGetIpFragFails (pu4RetValIpFragFails)
     UINT4              *pu4RetValIpFragFails;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* No of outgoing pkts fragmented    */
    *pu4RetValIpFragFails = pIpCxt->Ip_stats.u4Frag_fails;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpFragCreates
 Input       :  The Indices

                The Object
                retValIpFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpFragCreates ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpFragCreates (UINT4 *pu4RetValIpFragCreates)
#else
INT1
nmhGetIpFragCreates (pu4RetValIpFragCreates)
     UINT4              *pu4RetValIpFragCreates;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* No of outgoing pkts fragmented    */
    *pu4RetValIpFragCreates = pIpCxt->Ip_stats.u4Frag_creates;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpAddrTable
 Input       :  The Indices
                IpAdEntAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpAddrTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpAddrTable (UINT4 u4IpAdEntAddr)
#else
INT1
nmhValidateIndexInstanceIpAddrTable (u4IpAdEntAddr)
     UINT4               u4IpAdEntAddr;
#endif
{
    UINT4               u4CxtId = 0;
    UINT4               u4IfaceIndex = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    if (IpGetIfIndexFromAddrInCxt (u4CxtId, u4IpAdEntAddr, &u4IfaceIndex) ==
        IP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpAddrTable
 Input       :  The Indices
                IpAdEntAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpAddrTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIpAddrTable (UINT4 *pu4IpAdEntAddr)
#else
INT1
nmhGetFirstIndexIpAddrTable (pu4IpAdEntAddr)
     UINT4              *pu4IpAdEntAddr;
#endif
{
    if (nmhGetNextIndexIpAddrTable (0, pu4IpAdEntAddr) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpAddrTable
 Input       :  The Indices
                IpAdEntAddr
                nextIpAdEntAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIpAddrTable (UINT4 u4IpAdEntAddr, UINT4 *pu4NextIpAdEntAddr)
{
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    if (CfaIpIfGetNextIpAddrInCxt (u4CxtId, u4IpAdEntAddr, pu4NextIpAdEntAddr)
        == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntIfIndex
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAdEntIfIndex (UINT4 u4IpAdEntAddr, INT4 *pi4RetValIpAdEntIfIndex)
{
    INT4                i4Index = 0;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    if ((i4Index =
         IpGetPortFromAddrInCxt (u4CxtId, u4IpAdEntAddr)) != IP_FAILURE)
    {
        IPIF_CONFIG_GET_IF_INDEX (i4Index, *(UINT4 *) pi4RetValIpAdEntIfIndex);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntNetMask
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntNetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAdEntNetMask (UINT4 u4IpAdEntAddr, UINT4 *pu4RetValIpAdEntNetMask)
{
    UINT4               u4BcastAddr = 0;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();

    /* Get the netmask associated with the IP address */
    if (IpIfGetAddressInfoInCxt
        (u4CxtId, u4IpAdEntAddr, pu4RetValIpAdEntNetMask, &u4BcastAddr,
         &u4IfaceIndex) == IP_FAILURE)
    {
        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                         "IpIfGetAddressInfo failed for %x\n", u4IpAdEntAddr);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntBcastAddr
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntBcastAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIpAdEntBcastAddr (UINT4 u4IpAdEntAddr, INT4 *pi4RetValIpAdEntBcastAddr)
{
    UINT4               u4NetMask = 0;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4BcastAddr = 0;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();

    /* Get the Broadcast address associated with the IP address */
    if (IpIfGetAddressInfoInCxt (u4CxtId, u4IpAdEntAddr, &u4NetMask,
                                 &u4BcastAddr, &u4IfaceIndex) == IP_FAILURE)
    {
        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                         "IpIfGetAddressInfo failed for %x\n", u4IpAdEntAddr);
        return SNMP_FAILURE;
    }
    (u4BcastAddr == IP_GEN_BCAST_ADDR) ? (*pi4RetValIpAdEntBcastAddr = 1) :
        (*pi4RetValIpAdEntBcastAddr = 0);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntReasmMaxSize
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAdEntReasmMaxSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpAdEntReasmMaxSize (UINT4 u4IpAdEntAddr,
                           INT4 *pi4RetValIpAdEntReasmMaxSize)
#else
INT1
nmhGetIpAdEntReasmMaxSize (u4IpAdEntAddr, pi4RetValIpAdEntReasmMaxSize)
     UINT4               u4IpAdEntAddr;
     INT4               *pi4RetValIpAdEntReasmMaxSize;
#endif
{
    UINT4               u4Index = 0;
    /* Makefile Changes - comparison btwn signed and unsigned */
    INT4                i4RetVal = 0;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    i4RetVal = IpGetPortFromAddrInCxt (u4CxtId, u4IpAdEntAddr);
    if ((i4RetVal != IP_FAILURE) && (i4RetVal < (IPIF_MAX_LOGICAL_IFACES + 1)))
    {
        u4Index = (UINT4) i4RetVal;
        *pi4RetValIpAdEntReasmMaxSize =
            (INT4) IPIF_GLBTAB_REASM_MAX_SIZE (u4Index);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIpRoutingDiscards
 Input       :  The Indices

                The Object
                retValIpRoutingDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpRoutingDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpRoutingDiscards (UINT4 *pu4RetValIpRoutingDiscards)
#else
INT1
nmhGetIpRoutingDiscards (pu4RetValIpRoutingDiscards)
     UINT4              *pu4RetValIpRoutingDiscards;
#endif
{
    *pu4RetValIpRoutingDiscards = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteNumber
 Input       :  The Indices

                The Object 
                retValIpCidrRouteNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteNumber                    ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteNumber (UINT4 *pu4RetValIpCidrRouteNumber)
#else
INT1
nmhGetIpCidrRouteNumber (pu4RetValIpCidrRouteNumber)
     UINT4              *pu4RetValIpCidrRouteNumber;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    IpGetFwdTableRouteNumInCxt (u4ContextId, pu4RetValIpCidrRouteNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInMsgs
 Input       :  The Indices

                The Object
                retValIcmpInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInMsgs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInMsgs (UINT4 *pu4RetValIcmpInMsgs)
#else
INT1
nmhGetIcmpInMsgs (pu4RetValIcmpInMsgs)
     UINT4              *pu4RetValIcmpInMsgs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    INT4                i4Count = 0;
    UINT4               u4CxtId = 0;
    UINT4               u4Sum = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    for (i4Count = 0; i4Count < ICMP_TYPES; i4Count++)
    {
        u4Sum += pIpCxt->Icmp_stats.u4In[i4Count];
    }
    *pu4RetValIcmpInMsgs = u4Sum;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInErrors
 Input       :  The Indices

                The Object
                retValIcmpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInErrors (UINT4 *pu4RetValIcmpInErrors)
#else
INT1
nmhGetIcmpInErrors (pu4RetValIcmpInErrors)
     UINT4              *pu4RetValIcmpInErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIcmpInErrors =
        pIpCxt->Icmp_err.u4Cksum + pIpCxt->Icmp_err.u4Bcast +
        pIpCxt->Icmp_err.u4Type;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInDestUnreachs
 Input       :  The Indices

                The Object
                retValIcmpInDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInDestUnreachs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInDestUnreachs (UINT4 *pu4RetValIcmpInDestUnreachs)
#else
INT1
nmhGetIcmpInDestUnreachs (pu4RetValIcmpInDestUnreachs)
     UINT4              *pu4RetValIcmpInDestUnreachs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInDestUnreachs =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_DEST_UNREACH);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInTimeExcds
 Input       :  The Indices

                The Object
                retValIcmpInTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInTimeExcds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInTimeExcds (UINT4 *pu4RetValIcmpInTimeExcds)
#else
INT1
nmhGetIcmpInTimeExcds (pu4RetValIcmpInTimeExcds)
     UINT4              *pu4RetValIcmpInTimeExcds;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInTimeExcds =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_TIME_EXCEED);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInParmProbs
 Input       :  The Indices

                The Object
                retValIcmpInParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInParmProbs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInParmProbs (UINT4 *pu4RetValIcmpInParmProbs)
#else
INT1
nmhGetIcmpInParmProbs (pu4RetValIcmpInParmProbs)
     UINT4              *pu4RetValIcmpInParmProbs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInParmProbs =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_PARAM_PROB);
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIcmpInSrcQuenchs
 Input       :  The Indices

                The Object
                retValIcmpInSrcQuenchs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInSrcQuenchs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInSrcQuenchs (UINT4 *pu4RetValIcmpInSrcQuenchs)
#else
INT1
nmhGetIcmpInSrcQuenchs (pu4RetValIcmpInSrcQuenchs)
     UINT4              *pu4RetValIcmpInSrcQuenchs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInSrcQuenchs =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_QUENCH);
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetIcmpInRedirects
 Input       :  The Indices

                The Object
                retValIcmpInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInRedirects ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInRedirects (UINT4 *pu4RetValIcmpInRedirects)
#else
INT1
nmhGetIcmpInRedirects (pu4RetValIcmpInRedirects)
     UINT4              *pu4RetValIcmpInRedirects;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInRedirects =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_REDIRECT);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInEchos
 Input       :  The Indices

                The Object
                retValIcmpInEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInEchos ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInEchos (UINT4 *pu4RetValIcmpInEchos)
#else
INT1
nmhGetIcmpInEchos (pu4RetValIcmpInEchos)
     UINT4              *pu4RetValIcmpInEchos;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInEchos = ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt,
                                                                      ICMP_ECHO);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInEchoReps
 Input       :  The Indices

                The Object
                retValIcmpInEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInEchoReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInEchoReps (UINT4 *pu4RetValIcmpInEchoReps)
#else
INT1
nmhGetIcmpInEchoReps (pu4RetValIcmpInEchoReps)
     UINT4              *pu4RetValIcmpInEchoReps;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInEchoReps =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_ECHO_REPLY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInTimestamps
 Input       :  The Indices

                The Object
                retValIcmpInTimestamps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInTimestamps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInTimestamps (UINT4 *pu4RetValIcmpInTimestamps)
#else
INT1
nmhGetIcmpInTimestamps (pu4RetValIcmpInTimestamps)
     UINT4              *pu4RetValIcmpInTimestamps;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInTimestamps =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_TIMESTAMP);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInTimestampReps
 Input       :  The Indices

                The Object
                retValIcmpInTimestampReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInTimestampReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInTimestampReps (UINT4 *pu4RetValIcmpInTimestampReps)
#else
INT1
nmhGetIcmpInTimestampReps (pu4RetValIcmpInTimestampReps)
     UINT4              *pu4RetValIcmpInTimestampReps;
#endif

{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInTimestampReps =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_TIME_REPLY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInAddrMasks
 Input       :  The Indices

                The Object
                retValIcmpInAddrMasks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInAddrMasks ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInAddrMasks (UINT4 *pu4RetValIcmpInAddrMasks)
#else
INT1
nmhGetIcmpInAddrMasks (pu4RetValIcmpInAddrMasks)
     UINT4              *pu4RetValIcmpInAddrMasks;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInAddrMasks =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_MASK_RQST);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInAddrMaskReps
 Input       :  The Indices

                The Object
                retValIcmpInAddrMaskReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInAddrMaskReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInAddrMaskReps (UINT4 *pu4RetValIcmpInAddrMaskReps)
#else
INT1
nmhGetIcmpInAddrMaskReps (pu4RetValIcmpInAddrMaskReps)
     UINT4              *pu4RetValIcmpInAddrMaskReps;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInAddrMaskReps =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, ICMP_MASK_REPLY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutMsgs
 Input       :  The Indices

                The Object
                retValIcmpOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutMsgs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutMsgs (UINT4 *pu4RetValIcmpOutMsgs)
#else
INT1
nmhGetIcmpOutMsgs (pu4RetValIcmpOutMsgs)
     UINT4              *pu4RetValIcmpOutMsgs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    INT4                i4Count = 0;
    UINT4               u4CxtId = 0;
    UINT4               u4Sum = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    for (i4Count = 0; i4Count < ICMP_TYPES; i4Count++)
    {
        u4Sum += pIpCxt->Icmp_stats.u4Out[i4Count];
    }
    *pu4RetValIcmpOutMsgs = u4Sum;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutErrors
 Input       :  The Indices

                The Object
                retValIcmpOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutErrors (UINT4 *pu4RetValIcmpOutErrors)
#else
INT1
nmhGetIcmpOutErrors (pu4RetValIcmpOutErrors)
     UINT4              *pu4RetValIcmpOutErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIcmpOutErrors = pIpCxt->Icmp_err.u4Loop;
    return SNMP_SUCCESS;
}

/*
 * Following two procedures provide the statistics for individual
 * type of pkts. Mid level routines have to use type definitions in
 * icmptyps.h to call these procedures. The mapping is expected to be done
 * at Mid level routines.
 */
UINT4
ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, i4Type)
     tIpCxt             *pIpCxt;
     INT4                i4Type;
{
    if ((i4Type < 0) || (i4Type > ICMP_TYPES))
    {
        return SNMP_FAILURE;
    }
    return (pIpCxt->Icmp_stats.u4In[i4Type]);
}

UINT4
ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, i4Type)
     tIpCxt             *pIpCxt;
     INT4                i4Type;
{
    if ((i4Type < 0) || (i4Type > ICMP_TYPES))
    {
        return SNMP_FAILURE;
    }
    return (pIpCxt->Icmp_stats.u4Out[i4Type]);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutDestUnreachs
 Input       :  The Indices

                The Object
                retValIcmpOutDstUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutDestUnreachs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutDestUnreachs (UINT4 *pu4RetValIcmpOutDstUnreachs)
#else
INT1
nmhGetIcmpOutDestUnreachs (pu4RetValIcmpOutDstUnreachs)
     UINT4              *pu4RetValIcmpOutDstUnreachs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutDstUnreachs =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_DEST_UNREACH);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutTimeExcds
 Input       :  The Indices

                The Object
                retValIcmpOutTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutTimeExcds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutTimeExcds (UINT4 *pu4RetValIcmpOutTimeExcds)
#else
INT1
nmhGetIcmpOutTimeExcds (pu4RetValIcmpOutTimeExcds)
     UINT4              *pu4RetValIcmpOutTimeExcds;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutTimeExcds =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_TIME_EXCEED);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutParmProbs
 Input       :  The Indices

                The Object
                retValIcmpOutParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutParmProbs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutParmProbs (UINT4 *pu4RetValIcmpOutParmProbs)
#else
INT1
nmhGetIcmpOutParmProbs (pu4RetValIcmpOutParmProbs)
     UINT4              *pu4RetValIcmpOutParmProbs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutParmProbs =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_PARAM_PROB);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutSrcQuenchs
 Input       :  The Indices

                The Object
                retValIcmpOutSrcQuenchs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutSrcQuenchs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutSrcQuenchs (UINT4 *pu4RetValIcmpOutSrcQuenchs)
#else
INT1
nmhGetIcmpOutSrcQuenchs (pu4RetValIcmpOutSrcQuenchs)
     UINT4              *pu4RetValIcmpOutSrcQuenchs;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutSrcQuenchs =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_QUENCH);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutRedirects
 Input       :  The Indices

                The Object
                retValIcmpOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutRedirects ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutRedirects (UINT4 *pu4RetValIcmpOutRedirects)
#else
INT1
nmhGetIcmpOutRedirects (pu4RetValIcmpOutRedirects)
     UINT4              *pu4RetValIcmpOutRedirects;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutRedirects =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_REDIRECT);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutEchos
 Input       :  The Indices

                The Object
                retValIcmpOutEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutEchos ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutEchos (UINT4 *pu4RetValIcmpOutEchos)
#else
INT1
nmhGetIcmpOutEchos (pu4RetValIcmpOutEchos)
     UINT4              *pu4RetValIcmpOutEchos;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutEchos = ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt,
                                                                        ICMP_ECHO);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutEchoReps
 Input       :  The Indices

                The Object
                retValIcmpOutEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutEchoReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutEchoReps (UINT4 *pu4RetValIcmpOutEchoReps)
#else
INT1
nmhGetIcmpOutEchoReps (pu4RetValIcmpOutEchoReps)
     UINT4              *pu4RetValIcmpOutEchoReps;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutEchoReps =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_ECHO_REPLY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutTimestamps
 Input       :  The Indices

                The Object
                retValIcmpOutTimestamps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutTimestamps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutTimestamps (UINT4 *pu4RetValIcmpOutTimestamps)
#else
INT1
nmhGetIcmpOutTimestamps (pu4RetValIcmpOutTimestamps)
     UINT4              *pu4RetValIcmpOutTimestamps;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutTimestamps =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_TIMESTAMP);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutTimestampReps
 Input       :  The Indices

                The Object
                retValIcmpOutTimestampReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutTimestampReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutTimestampReps (UINT4 *pu4RetValIcmpOutTimestampReps)
#else
INT1
nmhGetIcmpOutTimestampReps (pu4RetValIcmpOutTimestampReps)
     UINT4              *pu4RetValIcmpOutTimestampReps;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutTimestampReps =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_TIME_REPLY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutAddrMasks
 Input       :  The Indices

                The Object
                retValIcmpOutAddrMasks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutAddrMasks ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutAddrMasks (UINT4 *pu4RetValIcmpOutAddrMasks)
#else
INT1
nmhGetIcmpOutAddrMasks (pu4RetValIcmpOutAddrMasks)
     UINT4              *pu4RetValIcmpOutAddrMasks;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutAddrMasks =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_MASK_RQST);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpOutAddrMaskReps
 Input       :  The Indices

                The Object
                retValIcmpOutAddrMaskReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutAddrMaskReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutAddrMaskReps (UINT4 *pu4RetValIcmpOutAddrMaskReps)
#else
INT1
nmhGetIcmpOutAddrMaskReps (pu4RetValIcmpOutAddrMaskReps)
     UINT4              *pu4RetValIcmpOutAddrMaskReps;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutAddrMaskReps =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, ICMP_MASK_REPLY);
    return SNMP_SUCCESS;
}

/*Remove this #ifdef when deleting the deprecated objects of
 *standard IP mib.
 *This code is provided to support backward comp.*/

/****************************************************************************
 Function    :  nmhGetUdpInDatagrams
 Input       :  The Indices

                The Object
                retValUdpInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpInDatagrams (UINT4 *pu4RetValUdpInDatagrams)
{
    *pu4RetValUdpInDatagrams = Udp_stat.u4InPkts;
    return SNMP_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  nmhGetUdpNoPorts
 Input       :  The Indices

                The Object
                retValUdpNoPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpNoPorts (UINT4 *pu4RetValUdpNoPorts)
{
    *pu4RetValUdpNoPorts = Udp_stat.u4InErrNoport;
    return SNMP_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  nmhGetUdpInErrors
 Input       :  The Indices

                The Object
                retValUdpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpInErrors (UINT4 *pu4RetValUdpInErrors)
{
    *pu4RetValUdpInErrors = Udp_stat.u4InErrNoport + Udp_stat.u4InErrCksum +
        Udp_stat.u4InTotalErr;
    return SNMP_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  nmhGetUdpOutDatagrams
 Input       :  The Indices

                The Object
                retValUdpOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpOutDatagrams (UINT4 *pu4RetValUdpOutDatagrams)
{
    *pu4RetValUdpOutDatagrams = Udp_stat.u4OutPkts;
    return SNMP_SUCCESS;
}
#endif
/*
 * UDP Application Port table.
 * The table contains following entries.
 * INDEX  u2U_port      --  Enrolled port.
 *        u4Local_addr  --  receive address specified during open.
 */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceUdpTable
 Input       :  The Indices
                UdpLocalAddress
                UdpLocalPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceUdpTable (UINT4 u4UdpLocalAddress, INT4 i4UdpLocalPort)
{
    t_UDP_CB            tUdp_cb;
    UINT1               u1Mode = 0;

    if (udp_get_cb ((UINT2) i4UdpLocalPort, u4UdpLocalAddress,
                    &tUdp_cb, &u1Mode) == SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexUdpTable
 Input       :  The Indices
                UdpLocalAddress
                UdpLocalPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexUdpTable (UINT4 *pu4UdpLocalAddress, INT4 *pi4UdpLocalPort)
{
    return (nmhGetNextIndexUdpTable (0, pu4UdpLocalAddress,
                                     0, pi4UdpLocalPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexUdpTable
 Input       :  The Indices
                UdpLocalAddress
                nextUdpLocalAddress
                UdpLocalPort
                nextUdpLocalPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexUdpTable (UINT4 u4UdpLocalAddress, UINT4 *pu4NextUdpLocalAddress,
                         INT4 i4UdpLocalPort, INT4 *pi4NextUdpLocalPort)
{
    UINT2               u2Port = 0;
    UINT2               u2NextPort = 0;

    u2Port = (UINT2) i4UdpLocalPort;

    if (udpGetNextIndexUdpTable (u4UdpLocalAddress, pu4NextUdpLocalAddress,
                                 u2Port, &u2NextPort) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextUdpLocalPort = (INT4) u2NextPort;
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpForwarding
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpForwarding (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IpDefaultTTL
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpDefaultTTL (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#endif
