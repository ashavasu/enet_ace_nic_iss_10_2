/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnipsnlw.c,v 1.27 2015/06/17 04:44:31 siva Exp $
 *
 * Description:Network management routines for STDIP mib.
 *             Includes nmh routines to interface with LNXIP.    
 *             (IP stats, ICMP stats, UDP stats, etc..)   
 *
 *******************************************************************/
#ifndef __LNIPSNLW_C__
#define __LNIPSNLW_C__
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "rtm.h"
#include "arp.h"
#include "ipcli.h"
#include "stdiplow.h"
#include "lnxiputl.h"
#include "ipvx.h"
#include "fsmsipcli.h"

#ifdef NPAPI_WANTED
#include "ipnpwr.h"
#endif /* NPAPI_WANTED */

/****************************************************************************
 Function    :  nmhGetIpForwarding
 Input       :  The Indices

                The Object
                retValIpForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpForwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpForwarding (INT4 *pi4RetValIpForwarding)
#else
INT1
nmhGetIpForwarding (pi4RetValIpForwarding)
     INT4               *pi4RetValIpForwarding;
#endif
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = IpvxGetCurrContext ();

    if(u4ContextId != IP_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if(pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS, (CHR1 *) pLnxVrfInfo->au1NameSpace);
        }
    }
#endif 
    if ((LnxIpGetForwarding ((UINT4 *) pi4RetValIpForwarding)) == SNMP_FAILURE)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if(u4ContextId != IP_DEFAULT_CONTEXT)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
#endif
        return SNMP_FAILURE;
    }

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(u4ContextId != IP_DEFAULT_CONTEXT)
    {
        LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID);
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpForwarding
 Input       :  The Indices

                The Object
                testValIpForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpForwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IpForwarding (UINT4 *pu4ErrorCode, INT4 i4TestValIpForwarding)
#else
INT1
nmhTestv2IpForwarding (pu4ErrorCode, i4TestValIpForwarding)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpForwarding;
#endif
{
    if (i4TestValIpForwarding == IP_FORW_ENABLE
        || i4TestValIpForwarding == IP_FORW_DISABLE)
        return SNMP_SUCCESS;
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpForwarding
 Input       :  The Indices

                The Object
                setValIpForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpForwarding ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIpForwarding (INT4 i4SetValIpForwarding)
#else
INT1
nmhSetIpForwarding (i4SetValIpForwarding)
     INT4                i4SetValIpForwarding;

#endif
{
    INT4                i4Status = 0;
#ifdef NPAPI_WANTED
    UINT4               u4FwdFlag;
#endif
    UINT4               u4CurStatus = 0;
    UINT4               u4ContextId;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif

    u4ContextId = IpvxGetCurrContext ();

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(u4ContextId != IP_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if(pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS, (CHR1 *) pLnxVrfInfo->au1NameSpace);
        }
    }
#endif
    if (i4SetValIpForwarding == IP_FORW_ENABLE)
    {
        i4Status = LNX_IP_FORW_ENABLE;
    }
    else if (i4SetValIpForwarding == IP_FORW_DISABLE)
    {
        i4Status = LNX_IP_FORW_DISABLE;
    }

    if (LnxIpGetForwarding (&u4CurStatus) == SNMP_FAILURE)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(u4ContextId != IP_DEFAULT_CONTEXT)
    {
        LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID);
    }
#endif
        return SNMP_FAILURE;
    }

    if (u4CurStatus == (UINT4) i4Status)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if(u4ContextId != IP_DEFAULT_CONTEXT)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
#endif
        return SNMP_SUCCESS;
    }

    if (LnxIpSetForwarding (i4Status) == IP_FAILURE)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if(u4ContextId != IP_DEFAULT_CONTEXT)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
#endif
        return SNMP_FAILURE;
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(u4ContextId != IP_DEFAULT_CONTEXT)
    {
        LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID);
    }
#endif
#ifdef NPAPI_WANTED
    /* Enables routing per VR in the fast path */
    if (i4SetValIpForwarding == IP_FORW_ENABLE)
    {
        u4FwdFlag = FNP_FORW_ENABLE;
    }
    else
    {
        u4FwdFlag = FNP_FORW_DISABLE;
    }

    if (IpEnableRouting (u4ContextId, u4FwdFlag) != IP_SUCCESS)
    {
        /* Could not enable routing for VR = IP_VRID in the fast path */
    }
    else
    {
        /* Enable routing on VR = IP_VRID in the fast path */
    }
#endif
    UNUSED_PARAM (u4ContextId);
    CfaHandleIpForwardingStatusUpdate ((UINT1) i4SetValIpForwarding);
    IncMsrForFsIpv4Scalars (i4SetValIpForwarding,
                            FsMIStdIpForwarding,
                            (sizeof (FsMIStdIpForwarding) / sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpDefaultTTL
 Input       :  The Indices

                The Object
                retValIpDefaultTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpDefaultTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpDefaultTTL (INT4 *pi4RetValIpDefaultTTL)
#else
INT1
nmhGetIpDefaultTTL (pi4RetValIpDefaultTTL)
     INT4               *pi4RetValIpDefaultTTL;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_DEFAULT_TTL, (UINT4 *) pi4RetValIpDefaultTTL)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhTestv2IpDefaultTTL
 Input       :  The Indices

                The Object
                testValIpDefaultTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpDefaultTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2IpDefaultTTL (UINT4 *pu4ErrorCode, INT4 i4TestValIpDefaultTTL)
#else
INT1
nmhTestv2IpDefaultTTL (pu4ErrorCode, i4TestValIpDefaultTTL)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpDefaultTTL;
#endif
{
    if (i4TestValIpDefaultTTL >= IP_MIN_DEF_TTL
        && i4TestValIpDefaultTTL <= IP_MAX_DEF_TTL)
        return SNMP_SUCCESS;
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpDefaultTTL
 Input       :  The Indices

                The Object
                setValIpDefaultTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpDefaultTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIpDefaultTTL (INT4 i4SetValIpDefaultTTL)
#else
INT1
nmhSetIpDefaultTTL (i4SetValIpDefaultTTL)
     INT4                i4SetValIpDefaultTTL;

#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;
    CHR1                ac1Command[LNX_MAX_COMMAND_LEN];

    /* set sysctl object value */
    SNPRINTF (ac1Command, LNX_MAX_COMMAND_LEN,
              "echo %d >/proc/sys/net/ipv4/ip_default_ttl",
              i4SetValIpDefaultTTL);
    system (ac1Command);

    IncMsrForFsIpv4Scalars (i4SetValIpDefaultTTL,
                            FsMIStdIpDefaultTTL,
                            (sizeof (FsMIStdIpDefaultTTL) / sizeof (UINT4)));
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpInReceives
 Input       :  The Indices

                The Object
                retValIpInReceives
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpInReceives (UINT4 *pu4RetValIpInReceives)
#else
INT1
nmhGetIpInReceives (pu4RetValIpInReceives)
     UINT4              *pu4RetValIpInReceives;
#endif
{
#ifdef NPAPI_WANTED
    UINT4               u4IpInRcvs = 0;
#endif /* NPAPI_WANTED */

    if (LnxIpGetObject (LNX_IP_IN_RECEIVES, pu4RetValIpInReceives)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of packets received in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_RECIEVES, &u4IpInRcvs);
    *pu4RetValIpInReceives += u4IpInRcvs;
#endif /* NPAPI_WANTED */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInHdrErrors
 Input       :  The Indices

                The Object
                retValIpInHdrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInHdrErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInHdrErrors (UINT4 *pu4RetValIpInHdrErrors)
#else
INT1
nmhGetIpInHdrErrors (pu4RetValIpInHdrErrors)
     UINT4              *pu4RetValIpInHdrErrors;
#endif
{
#ifdef NPAPI_WANTED
    UINT4               u4IpInHdrErrs = 0;
#endif /* NPAPI_WANTED */

    if (LnxIpGetObject (LNX_IP_IN_HDR_ERRS, pu4RetValIpInHdrErrors)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of packets received
       due to Hdr Error in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_HDR_ERRORS, &u4IpInHdrErrs);
    *pu4RetValIpInHdrErrors += u4IpInHdrErrs;
#endif /* NPAPI_WANTED */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInAddrErrors
 Input       :  The Indices

                The Object
                retValIpInAddrErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInAddrErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInAddrErrors (UINT4 *pu4RetValIpInAddrErrors)
#else
INT1
nmhGetIpInAddrErrors (pu4RetValIpInAddrErrors)
     UINT4              *pu4RetValIpInAddrErrors;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_IN_ADDR_ERRS, pu4RetValIpInAddrErrors)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpForwDatagrams
 Input       :  The Indices

                The Object
                retValIpForwDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpForwDatagrams ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpForwDatagrams (UINT4 *pu4RetValIpForwDatagrams)
#else
INT1
nmhGetIpForwDatagrams (pu4RetValIpForwDatagrams)
     UINT4              *pu4RetValIpForwDatagrams;
#endif
{
#ifdef NPAPI_WANTED
    UINT4               u4IpForwDatagrams = 0;
#endif /* NPAPI_WANTED */

    if (LnxIpGetObject (LNX_IP_FORW_DATAGRAMS, pu4RetValIpForwDatagrams)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of datagrams forwarded in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_FORW_DATAGRAMS, &u4IpForwDatagrams);
    *pu4RetValIpForwDatagrams += u4IpForwDatagrams;
#endif /* NPAPI_WANTED */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpInUnknownProtos
 Input       :  The Indices

                The Object
                retValIpInUnknownProtos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInUnknownProtos ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInUnknownProtos (UINT4 *pu4RetValIpInUnknownProtos)
#else
INT1
nmhGetIpInUnknownProtos (pu4RetValIpInUnknownProtos)
     UINT4              *pu4RetValIpInUnknownProtos;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_IN_UNK_PROTOS, pu4RetValIpInUnknownProtos)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpInDiscards
 Input       :  The Indices

                The Object
                retValIpInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInDiscards (UINT4 *pu4RetValIpInDiscards)
#else
INT1
nmhGetIpInDiscards (pu4RetValIpInDiscards)
     UINT4              *pu4RetValIpInDiscards;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_IN_DISCARDS, pu4RetValIpInDiscards)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpInDelivers
 Input       :  The Indices

                The Object
                retValIpInDelivers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInDelivers ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpInDelivers (UINT4 *pu4RetValIpInDelivers)
#else
INT1
nmhGetIpInDelivers (pu4RetValIpInDelivers)
     UINT4              *pu4RetValIpInDelivers;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_IN_DELIVERS, pu4RetValIpInDelivers)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpOutRequests
 Input       :  The Indices

                The Object
                retValIpOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOutRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpOutRequests (UINT4 *pu4RetValIpOutRequests)
#else
INT1
nmhGetIpOutRequests (pu4RetValIpOutRequests)
     UINT4              *pu4RetValIpOutRequests;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_OUT_REQUESTS, pu4RetValIpOutRequests)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpOutDiscards
 Input       :  The Indices

                The Object
                retValIpOutDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOutDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpOutDiscards (UINT4 *pu4RetValIpOutDiscards)
#else
INT1
nmhGetIpOutDiscards (pu4RetValIpOutDiscards)
     UINT4              *pu4RetValIpOutDiscards;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_OUT_DISCARDS, pu4RetValIpOutDiscards)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpOutNoRoutes
 Input       :  The Indices

                The Object
                retValIpOutNoRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOutNoRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpOutNoRoutes (UINT4 *pu4RetValIpOutNoRoutes)
#else
INT1
nmhGetIpOutNoRoutes (pu4RetValIpOutNoRoutes)
     UINT4              *pu4RetValIpOutNoRoutes;
#endif
{
#ifdef NPAPI_WANTED
    UINT4               u4IpOutNoRoutes = 0;
#endif /* NPAPI_WANTED */

    if (LnxIpGetObject (LNX_IP_OUT_NO_ROUTES, pu4RetValIpOutNoRoutes)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifdef NPAPI_WANTED
    /* The api gets the total number of packets discarded
       due to no route in the fast path */
    IpFsNpIpv4GetStats (NP_STAT_IP_IN_DISCARDS, &u4IpOutNoRoutes);
    *pu4RetValIpOutNoRoutes += u4IpOutNoRoutes;
#endif /* NPAPI_WANTED */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpReasmTimeout
 Input       :  The Indices

                The Object
                retValIpReasmTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmTimeout (INT4 *pi4RetValIpReasmTimeout)
#else
INT1
nmhGetIpReasmTimeout (pi4RetValIpReasmTimeout)
     INT4               *pi4RetValIpReasmTimeout;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_REASM_TIMEOUT,
                        (UINT4 *) pi4RetValIpReasmTimeout) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpReasmReqds
 Input       :  The Indices

                The Object
                retValIpReasmReqds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmReqds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmReqds (UINT4 *pu4RetValIpReasmReqds)
#else
INT1
nmhGetIpReasmReqds (pu4RetValIpReasmReqds)
     UINT4              *pu4RetValIpReasmReqds;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_REASM_REQDS, pu4RetValIpReasmReqds)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpReasmOKs
 Input       :  The Indices

                The Object
                retValIpReasmOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmOKs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmOKs (UINT4 *pu4RetValIpReasmOKs)
#else
INT1
nmhGetIpReasmOKs (pu4RetValIpReasmOKs)
     UINT4              *pu4RetValIpReasmOKs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_REASM_OKS, pu4RetValIpReasmOKs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpReasmFails
 Input       :  The Indices

                The Object
                retValIpReasmFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpReasmFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpReasmFails (UINT4 *pu4RetValIpReasmFails)
#else
INT1
nmhGetIpReasmFails (pu4RetValIpReasmFails)
     UINT4              *pu4RetValIpReasmFails;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_REASM_FAILS, pu4RetValIpReasmFails)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpFragOKs
 Input       :  The Indices

                The Object
                retValIpFragOKs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpFragOKs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpFragOKs (UINT4 *pu4RetValIpFragOKs)
#else
INT1
nmhGetIpFragOKs (pu4RetValIpFragOKs)
     UINT4              *pu4RetValIpFragOKs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_FRAG_OKS, pu4RetValIpFragOKs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpFragFails
 Input       :  The Indices

                The Object
                retValIpFragFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpFragFails ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpFragFails (UINT4 *pu4RetValIpFragFails)
#else
INT1
nmhGetIpFragFails (pu4RetValIpFragFails)
     UINT4              *pu4RetValIpFragFails;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_FRAG_FAILS, pu4RetValIpFragFails)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIpFragCreates
 Input       :  The Indices

                The Object
                retValIpFragCreates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpFragCreates ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpFragCreates (UINT4 *pu4RetValIpFragCreates)
#else
INT1
nmhGetIpFragCreates (pu4RetValIpFragCreates)
     UINT4              *pu4RetValIpFragCreates;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_IP_FRAG_CREATES, pu4RetValIpFragCreates)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }
    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpAddrTable
 Input       :  The Indices
                IpAdEntAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpAddrTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpAddrTable (UINT4 u4IpAdEntAddr)
#else
INT1
nmhValidateIndexInstanceIpAddrTable (u4IpAdEntAddr)
     UINT4               u4IpAdEntAddr;
#endif
{
    UINT4               u4IfIndex;

    if (NetIpv4GetIfIndexFromAddr (u4IpAdEntAddr,
                                   &u4IfIndex) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpAddrTable
 Input       :  The Indices
                IpAdEntAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpAddrTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIpAddrTable (UINT4 *pu4IpAdEntAddr)
#else
INT1
nmhGetFirstIndexIpAddrTable (pu4IpAdEntAddr)
     UINT4              *pu4IpAdEntAddr;
#endif
{
    return (nmhGetNextIndexIpAddrTable (0, pu4IpAdEntAddr));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpAddrTable
 Input       :  The Indices
                IpAdEntAddr
                nextIpAdEntAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpAddrTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexIpAddrTable (UINT4 u4IpAdEntAddr, UINT4 *pu4NextIpAdEntAddr)
#else
INT1
nmhGetNextIndexIpAddrTable (u4IpAdEntAddr, pu4NextIpAdEntAddr)
     UINT4               u4IpAdEntAddr;
     UINT4              *pu4NextIpAdEntAddr;
#endif
{
    if (CfaIpIfGetNextIpAddr (u4IpAdEntAddr, pu4NextIpAdEntAddr) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntIfIndex
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAdEntIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpAdEntIfIndex (UINT4 u4IpAdEntAddr, INT4 *pi4RetValIpAdEntIfIndex)
#else
INT1
nmhGetIpAdEntIfIndex (u4IpAdEntAddr, pi4RetValIpAdEntIfIndex)
     UINT4               u4IpAdEntAddr;
     INT4               *pi4RetValIpAdEntIfIndex;
#endif
{
    UINT4               u4NetMask = 0;
    UINT4               u4BcastAddr = 0;
    UINT4               u4IfIndex = 0;

    if (CfaIpIfGetIpAddressInfo (u4IpAdEntAddr, &u4NetMask,
                                 &u4BcastAddr, &u4IfIndex) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpAdEntIfIndex = (INT4) u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntNetMask
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntNetMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAdEntNetMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpAdEntNetMask (UINT4 u4IpAdEntAddr, UINT4 *pu4RetValIpAdEntNetMask)
#else
INT1
nmhGetIpAdEntNetMask (u4IpAdEntAddr, pu4RetValIpAdEntNetMask)
     UINT4               u4IpAdEntAddr;
     UINT4              *pu4RetValIpAdEntNetMask;
#endif
{
    UINT4               u4BcastAddr = 0;
    UINT4               u4IfIndex = 0;

    if (CfaIpIfGetIpAddressInfo (u4IpAdEntAddr, pu4RetValIpAdEntNetMask,
                                 &u4BcastAddr, &u4IfIndex) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntBcastAddr
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntBcastAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAdEntBcastAddr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpAdEntBcastAddr (UINT4 u4IpAdEntAddr, INT4 *pi4RetValIpAdEntBcastAddr)
#else
INT1
nmhGetIpAdEntBcastAddr (u4IpAdEntAddr, pi4RetValIpAdEntBcastAddr)
     UINT4               u4IpAdEntAddr;
     INT4               *pi4RetValIpAdEntBcastAddr;
#endif
{
    UINT4               u4BcastAddr = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4NetMask = 0;

    if (CfaIpIfGetIpAddressInfo (u4IpAdEntAddr, &u4NetMask,
                                 &u4BcastAddr, &u4IfIndex) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    (u4BcastAddr == IP_GEN_BCAST_ADDR) ? (*pi4RetValIpAdEntBcastAddr = 1) :
        (*pi4RetValIpAdEntBcastAddr = 0);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpAdEntReasmMaxSize
 Input       :  The Indices
                IpAdEntAddr

                The Object
                retValIpAdEntReasmMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAdEntReasmMaxSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpAdEntReasmMaxSize (UINT4 u4IpAdEntAddr,
                           INT4 *pi4RetValIpAdEntReasmMaxSize)
#else
INT1
nmhGetIpAdEntReasmMaxSize (u4IpAdEntAddr, pi4RetValIpAdEntReasmMaxSize)
     UINT4               u4IpAdEntAddr;
     INT4               *pi4RetValIpAdEntReasmMaxSize;
#endif
{
    UNUSED_PARAM (u4IpAdEntAddr);
    UNUSED_PARAM (*pi4RetValIpAdEntReasmMaxSize);

    /* TODO: To be obtained from LNXIP */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpRoutingDiscards
 Input       :  The Indices

                The Object
                retValIpRoutingDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpRoutingDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIpRoutingDiscards (UINT4 *pu4RetValIpRoutingDiscards)
#else
INT1
nmhGetIpRoutingDiscards (pu4RetValIpRoutingDiscards)
     UINT4              *pu4RetValIpRoutingDiscards;
#endif
{
    *pu4RetValIpRoutingDiscards = 0;

    /* TODO: To be obtained from LNXIP. 
     * No option available in LnxIP to get this statistics */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteNumber
 Input       :  The Indices

                The Object 
                retValIpCidrRouteNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteNumber                    ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteNumber (UINT4 *pu4RetValIpCidrRouteNumber)
#else
INT1
nmhGetIpCidrRouteNumber (pu4RetValIpCidrRouteNumber)
     UINT4              *pu4RetValIpCidrRouteNumber;
#endif
{
    UINT4               u4ContextId;

    u4ContextId = IpvxGetCurrContext ();

    IpGetFwdTableRouteNumInCxt (u4ContextId, pu4RetValIpCidrRouteNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIcmpInMsgs
 Input       :  The Indices

                The Object
                retValIcmpInMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInMsgs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInMsgs (UINT4 *pu4RetValIcmpInMsgs)
#else
INT1
nmhGetIcmpInMsgs (pu4RetValIcmpInMsgs)
     UINT4              *pu4RetValIcmpInMsgs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_MSGS, pu4RetValIcmpInMsgs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInErrors
 Input       :  The Indices

                The Object
                retValIcmpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInErrors (UINT4 *pu4RetValIcmpInErrors)
#else
INT1
nmhGetIcmpInErrors (pu4RetValIcmpInErrors)
     UINT4              *pu4RetValIcmpInErrors;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_ERRS, pu4RetValIcmpInErrors) ==
        SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInDestUnreachs
 Input       :  The Indices

                The Object
                retValIcmpInDestUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInDestUnreachs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInDestUnreachs (UINT4 *pu4RetValIcmpInDestUnreachs)
#else
INT1
nmhGetIcmpInDestUnreachs (pu4RetValIcmpInDestUnreachs)
     UINT4              *pu4RetValIcmpInDestUnreachs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_DEST_UNREACHS,
                        pu4RetValIcmpInDestUnreachs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInTimeExcds
 Input       :  The Indices

                The Object
                retValIcmpInTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInTimeExcds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInTimeExcds (UINT4 *pu4RetValIcmpInTimeExcds)
#else
INT1
nmhGetIcmpInTimeExcds (pu4RetValIcmpInTimeExcds)
     UINT4              *pu4RetValIcmpInTimeExcds;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_TIME_EXCDS, pu4RetValIcmpInTimeExcds)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInParmProbs
 Input       :  The Indices

                The Object
                retValIcmpInParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInParmProbs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInParmProbs (UINT4 *pu4RetValIcmpInParmProbs)
#else
INT1
nmhGetIcmpInParmProbs (pu4RetValIcmpInParmProbs)
     UINT4              *pu4RetValIcmpInParmProbs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_PARM_PROBS, pu4RetValIcmpInParmProbs)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInSrcQuenchs
 Input       :  The Indices

                The Object
                retValIcmpInSrcQuenchs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInSrcQuenchs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInSrcQuenchs (UINT4 *pu4RetValIcmpInSrcQuenchs)
#else
INT1
nmhGetIcmpInSrcQuenchs (pu4RetValIcmpInSrcQuenchs)
     UINT4              *pu4RetValIcmpInSrcQuenchs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_SRC_QUENCHS, pu4RetValIcmpInSrcQuenchs)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInRedirects
 Input       :  The Indices

                The Object
                retValIcmpInRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInRedirects ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInRedirects (UINT4 *pu4RetValIcmpInRedirects)
#else
INT1
nmhGetIcmpInRedirects (pu4RetValIcmpInRedirects)
     UINT4              *pu4RetValIcmpInRedirects;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_REDIRECTS, pu4RetValIcmpInRedirects)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInEchos
 Input       :  The Indices

                The Object
                retValIcmpInEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInEchos ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInEchos (UINT4 *pu4RetValIcmpInEchos)
#else
INT1
nmhGetIcmpInEchos (pu4RetValIcmpInEchos)
     UINT4              *pu4RetValIcmpInEchos;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_ECHOS, pu4RetValIcmpInEchos)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInEchoReps
 Input       :  The Indices

                The Object
                retValIcmpInEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInEchoReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInEchoReps (UINT4 *pu4RetValIcmpInEchoReps)
#else
INT1
nmhGetIcmpInEchoReps (pu4RetValIcmpInEchoReps)
     UINT4              *pu4RetValIcmpInEchoReps;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_ECHO_REPS, pu4RetValIcmpInEchoReps)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInTimestamps
 Input       :  The Indices

                The Object
                retValIcmpInTimestamps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInTimestamps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInTimestamps (UINT4 *pu4RetValIcmpInTimestamps)
#else
INT1
nmhGetIcmpInTimestamps (pu4RetValIcmpInTimestamps)
     UINT4              *pu4RetValIcmpInTimestamps;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_TIME_STAMPS, pu4RetValIcmpInTimestamps)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInTimestampReps
 Input       :  The Indices

                The Object
                retValIcmpInTimestampReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInTimestampReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInTimestampReps (UINT4 *pu4RetValIcmpInTimestampReps)
#else
INT1
nmhGetIcmpInTimestampReps (pu4RetValIcmpInTimestampReps)
     UINT4              *pu4RetValIcmpInTimestampReps;
#endif

{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_TIME_STAM_REPS,
                        pu4RetValIcmpInTimestampReps) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInAddrMasks
 Input       :  The Indices

                The Object
                retValIcmpInAddrMasks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInAddrMasks ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInAddrMasks (UINT4 *pu4RetValIcmpInAddrMasks)
#else
INT1
nmhGetIcmpInAddrMasks (pu4RetValIcmpInAddrMasks)
     UINT4              *pu4RetValIcmpInAddrMasks;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_ADDR_MASKS, pu4RetValIcmpInAddrMasks)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpInAddrMaskReps
 Input       :  The Indices

                The Object
                retValIcmpInAddrMaskReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInAddrMaskReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpInAddrMaskReps (UINT4 *pu4RetValIcmpInAddrMaskReps)
#else
INT1
nmhGetIcmpInAddrMaskReps (pu4RetValIcmpInAddrMaskReps)
     UINT4              *pu4RetValIcmpInAddrMaskReps;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_IN_ADDR_MASK_REPS,
                        pu4RetValIcmpInAddrMaskReps) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutMsgs
 Input       :  The Indices

                The Object
                retValIcmpOutMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutMsgs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutMsgs (UINT4 *pu4RetValIcmpOutMsgs)
#else
INT1
nmhGetIcmpOutMsgs (pu4RetValIcmpOutMsgs)
     UINT4              *pu4RetValIcmpOutMsgs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_MSGS, pu4RetValIcmpOutMsgs)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutErrors
 Input       :  The Indices

                The Object
                retValIcmpOutErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutErrors (UINT4 *pu4RetValIcmpOutErrors)
#else
INT1
nmhGetIcmpOutErrors (pu4RetValIcmpOutErrors)
     UINT4              *pu4RetValIcmpOutErrors;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_ERRS, pu4RetValIcmpOutErrors)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutDestUnreachs
 Input       :  The Indices

                The Object
                retValIcmpOutDstUnreachs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutDestUnreachs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutDestUnreachs (UINT4 *pu4RetValIcmpOutDstUnreachs)
#else
INT1
nmhGetIcmpOutDestUnreachs (pu4RetValIcmpOutDstUnreachs)
     UINT4              *pu4RetValIcmpOutDstUnreachs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_DEST_UNREACHS,
                        pu4RetValIcmpOutDstUnreachs) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutTimeExcds
 Input       :  The Indices

                The Object
                retValIcmpOutTimeExcds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutTimeExcds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutTimeExcds (UINT4 *pu4RetValIcmpOutTimeExcds)
#else
INT1
nmhGetIcmpOutTimeExcds (pu4RetValIcmpOutTimeExcds)
     UINT4              *pu4RetValIcmpOutTimeExcds;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_TIME_EXCDS, pu4RetValIcmpOutTimeExcds)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutParmProbs
 Input       :  The Indices

                The Object
                retValIcmpOutParmProbs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutParmProbs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutParmProbs (UINT4 *pu4RetValIcmpOutParmProbs)
#else
INT1
nmhGetIcmpOutParmProbs (pu4RetValIcmpOutParmProbs)
     UINT4              *pu4RetValIcmpOutParmProbs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_PARM_PROBS, pu4RetValIcmpOutParmProbs)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutSrcQuenchs
 Input       :  The Indices

                The Object
                retValIcmpOutSrcQuenchs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutSrcQuenchs ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutSrcQuenchs (UINT4 *pu4RetValIcmpOutSrcQuenchs)
#else
INT1
nmhGetIcmpOutSrcQuenchs (pu4RetValIcmpOutSrcQuenchs)
     UINT4              *pu4RetValIcmpOutSrcQuenchs;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_SRC_QUENCHS, pu4RetValIcmpOutSrcQuenchs)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutRedirects
 Input       :  The Indices

                The Object
                retValIcmpOutRedirects
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutRedirects ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutRedirects (UINT4 *pu4RetValIcmpOutRedirects)
#else
INT1
nmhGetIcmpOutRedirects (pu4RetValIcmpOutRedirects)
     UINT4              *pu4RetValIcmpOutRedirects;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_REDIRECTS, pu4RetValIcmpOutRedirects)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutEchos
 Input       :  The Indices

                The Object
                retValIcmpOutEchos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutEchos ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutEchos (UINT4 *pu4RetValIcmpOutEchos)
#else
INT1
nmhGetIcmpOutEchos (pu4RetValIcmpOutEchos)
     UINT4              *pu4RetValIcmpOutEchos;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_ECHOS, pu4RetValIcmpOutEchos)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutEchoReps
 Input       :  The Indices

                The Object
                retValIcmpOutEchoReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutEchoReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutEchoReps (UINT4 *pu4RetValIcmpOutEchoReps)
#else
INT1
nmhGetIcmpOutEchoReps (pu4RetValIcmpOutEchoReps)
     UINT4              *pu4RetValIcmpOutEchoReps;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_ECHO_REPS, pu4RetValIcmpOutEchoReps)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutTimestamps
 Input       :  The Indices

                The Object
                retValIcmpOutTimestamps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutTimestamps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutTimestamps (UINT4 *pu4RetValIcmpOutTimestamps)
#else
INT1
nmhGetIcmpOutTimestamps (pu4RetValIcmpOutTimestamps)
     UINT4              *pu4RetValIcmpOutTimestamps;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_TIME_STAMPS, pu4RetValIcmpOutTimestamps)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutTimestampReps
 Input       :  The Indices

                The Object
                retValIcmpOutTimestampReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutTimestampReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutTimestampReps (UINT4 *pu4RetValIcmpOutTimestampReps)
#else
INT1
nmhGetIcmpOutTimestampReps (pu4RetValIcmpOutTimestampReps)
     UINT4              *pu4RetValIcmpOutTimestampReps;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_TIME_STAMP_REPS,
                        pu4RetValIcmpOutTimestampReps) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutAddrMasks
 Input       :  The Indices

                The Object
                retValIcmpOutAddrMasks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutAddrMasks ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutAddrMasks (UINT4 *pu4RetValIcmpOutAddrMasks)
#else
INT1
nmhGetIcmpOutAddrMasks (pu4RetValIcmpOutAddrMasks)
     UINT4              *pu4RetValIcmpOutAddrMasks;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_ADDR_MASKS, pu4RetValIcmpOutAddrMasks)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/****************************************************************************
 Function    :  nmhGetIcmpOutAddrMaskReps
 Input       :  The Indices

                The Object
                retValIcmpOutAddrMaskReps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutAddrMaskReps ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetIcmpOutAddrMaskReps (UINT4 *pu4RetValIcmpOutAddrMaskReps)
#else
INT1
nmhGetIcmpOutAddrMaskReps (pu4RetValIcmpOutAddrMaskReps)
     UINT4              *pu4RetValIcmpOutAddrMaskReps;
#endif
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_ICMP_OUT_ADDR_MASK_REPS,
                        pu4RetValIcmpOutAddrMaskReps) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}

/*Remove this #ifdef when deleting the deprecated objects of
 *standard IP mib.
 *This code is provided to support backward comp.*/

/* Following statistics are provided from UDP */
/****************************************************************************
 Function    :  nmhGetUdpInDatagrams
 Input       :  The Indices

                The Object
                retValUdpInDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpInDatagrams (UINT4 *pu4RetValUdpInDatagrams)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_UDP_IN_DATAGRAMS, pu4RetValUdpInDatagrams)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}
#endif
/****************************************************************************
 Function    :  nmhGetUdpNoPorts
 Input       :  The Indices

                The Object
                retValUdpNoPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpNoPorts (UINT4 *pu4RetValUdpNoPorts)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_UDP_NO_PORTS, pu4RetValUdpNoPorts) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}
#endif
/****************************************************************************
 Function    :  nmhGetUdpInErrors
 Input       :  The Indices

                The Object
                retValUdpInErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpInErrors (UINT4 *pu4RetValUdpInErrors)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_UDP_IN_ERRS, pu4RetValUdpInErrors) == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}
#endif
/****************************************************************************
 Function    :  nmhGetUdpOutDatagrams
 Input       :  The Indices

                The Object
                retValUdpOutDatagrams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpOutDatagrams (UINT4 *pu4RetValUdpOutDatagrams)
{
    INT1                i1RetVal = SNMP_SUCCESS;

    if (LnxIpGetObject (LNX_UDP_OUT_DATAGRAMS, pu4RetValUdpOutDatagrams)
        == SNMP_FAILURE)
    {
        i1RetVal = SNMP_FAILURE;
    }

    return (i1RetVal);
}
#endif
/*
 * UDP Application Port table.
 * The table contains following entries.
 * INDEX  u2U_port      --  Enrolled port.
 *        u4Local_addr  --  receive address specified during open.
 */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceUdpTable
 Input       :  The Indices
                UdpLocalAddress
                UdpLocalPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceUdpTable (UINT4 u4UdpLocalAddress, INT4 i4UdpLocalPort)
{
    UNUSED_PARAM (u4UdpLocalAddress);
    UNUSED_PARAM (i4UdpLocalPort);

    /* TODO: To be done for LNX UDP */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexUdpTable
 Input       :  The Indices
                UdpLocalAddress
                UdpLocalPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexUdpTable (UINT4 *pu4UdpLocalAddress, INT4 *pi4UdpLocalPort)
{
    return (nmhGetNextIndexUdpTable (0, pu4UdpLocalAddress,
                                     0, pi4UdpLocalPort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexUdpTable
 Input       :  The Indices
                UdpLocalAddress
                nextUdpLocalAddress
                UdpLocalPort
                nextUdpLocalPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexUdpTable (UINT4 u4UdpLocalAddress, UINT4 *pu4NextUdpLocalAddress,
                         INT4 i4UdpLocalPort, INT4 *pi4NextUdpLocalPort)
{
    tUdpEntry           udpEntry;
    tUdpEntry           nextUdpEntry;

    udpEntry.u4LocalIp = u4UdpLocalAddress;
    udpEntry.u2LocalPort = i4UdpLocalPort;

    if (UdpTableGetNextEntry (&udpEntry, &nextUdpEntry) == SNMP_SUCCESS)
    {
        *pu4NextUdpLocalAddress = nextUdpEntry.u4LocalIp;
        *pi4NextUdpLocalPort = nextUdpEntry.u2LocalPort;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpForwarding
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpForwarding (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2IpDefaultTTL
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpDefaultTTL (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#endif
