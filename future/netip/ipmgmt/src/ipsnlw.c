
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipsnlw.c,v 1.68 2018/01/03 11:28:52 siva Exp $
 *
 * Description:Network management routines for the tables in STDIP 
 *             mib common to FSIP & LNXIP.   
 *             Includes IpCidrRouteTable & IpNetToMediaTable                      
 *
 *******************************************************************/
#ifndef __IPSNLW_C__
#define __IPSNLW_C__
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "ipcli.h"
#include "arp.h"
#include "stdiplow.h"
#include "rtm.h"
#include "ipvx.h"
#include "iss.h"
#include "rmgr.h"
#include "fsmsipcli.h"

INT1
 
 
 
 
 
 
 
 IPvxGetARPEntry (INT4 i4IPv4IfIndex, UINT4 u4Ipv4Addr,
                  t_ARP_CACHE * pArpEntry);

#ifdef NPAPI_WANTED
#include "ipnpwr.h"
#endif /* NPAPI_WANTED */

#define   OBJECT_SET                  1
#define   OBJECT_NOT_SET              0

#define   IP_IS_ENET_MAC_MCAST(pHwAddr) \
          ((*((UINT1 *)pHwAddr) & 0x01)? (1):(0))
#define   IP_IS_ENET_MAC_BCAST(pHwAddr) \
          ((*((UINT1 *)pHwAddr) & 0xff) == 0xff ? (1):(0))

extern t_ARP_HW_TABLE *(*arp_get_hw_entry) PROTO ((INT2 i2Hardware)),
    *(*arp_get_free_hw_entry) PROTO ((INT2 i2Hardware));

UINT4               gu4PrevRouteInvalid = OSIX_FALSE;
INT4                gi4ValidRouteRetVal = NETIPV4_SUCCESS;

PRIVATE VOID
 
 
 
 IncMsrForIpv4NetToPhyTable (INT4, UINT4, CHR1, VOID *, UINT4 *, UINT4, UINT1);

PRIVATE VOID
 
 
 
 IncMsrForIpv4RouteTable (UINT4, UINT4, UINT4, UINT4, INT4,
                          UINT4 *, UINT4, UINT1);
INT4
 
             RtmUtilCheckForDefRouteWithNoNextHop (tRtInfoQueryMsg * pRtQuery);
extern UINT4        IssSzGetSizingParamsForModule (CHR1 * pu1ModName,
                                                   CHR1 * pu1MacroName);
/* LOW LEVEL Routines for Table : IpCidrRouteTable. */
/* GET_EXACT Validate Index Instance Routine. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpCidrRouteTable
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpCidrRouteTable   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpCidrRouteTable (UINT4 u4IpCidrRouteDest,
                                          UINT4 u4IpCidrRouteMask,
                                          INT4 i4IpCidrRouteTos,
                                          UINT4 u4IpCidrRouteNextHop)
#else
INT1
nmhValidateIndexInstanceIpCidrRouteTable (u4IpCidrRouteDest,
                                          u4IpCidrRouteMask,
                                          i4IpCidrRouteTos,
                                          u4IpCidrRouteNextHop)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
#endif
{

    if ((IP_IS_ADDR_CLASS_D (u4IpCidrRouteDest)) ||
        (IP_IS_ADDR_CLASS_E (u4IpCidrRouteDest)) ||
        (IP_IS_ADDR_CLASS_D (u4IpCidrRouteNextHop)) ||
        (((u4IpCidrRouteDest & u4IpCidrRouteMask) != u4IpCidrRouteDest)))
    {
        return SNMP_FAILURE;
    }

    if (i4IpCidrRouteTos < 0)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexIpCidrRouteTable
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFirstIndexIpCidrRouteTable (UINT4 *pu4IpCidrRouteDest,
                                  UINT4 *pu4IpCidrRouteMask,
                                  INT4 *pi4IpCidrRouteTos,
                                  UINT4 *pu4IpCidrRouteNextHop)
#else
INT1
nmhGetFirstIndexIpCidrRouteTable (pu4IpCidrRouteDest, pu4IpCidrRouteMask,
                                  pi4IpCidrRouteTos, pu4IpCidrRouteNextHop)
     UINT4              *pu4IpCidrRouteDest;
     UINT4              *pu4IpCidrRouteMask;
     INT4               *pi4IpCidrRouteTos;
     UINT4              *pu4IpCidrRouteNextHop;
#endif
{
    INT4                i4OutCome = 0;
    tNetIpv4RtInfo      OutRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    MEMSET (&OutRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    /* Get the Default Route if present. */
    RtQuery.u4DestinationIpAddress = 0;
    RtQuery.u4DestinationSubnetMask = 0;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_EXACT_DEST;
    RtQuery.u4ContextId = u4ContextId;

    /* RTM Lock should have been take already before calling the nmh routine */
    if (RtmUtilIpv4GetRoute (&RtQuery, &OutRtInfo) == IP_SUCCESS)
    {
        if ((OutRtInfo.u4NextHop == 0) ||
            (RtmUtilCheckForDefRouteWithNoNextHop (&RtQuery) == IP_SUCCESS))
        {
            /* Default route is present with Out interface instead of Nexthop */
            *pu4IpCidrRouteDest = 0;
            *pu4IpCidrRouteMask = 0;
            *pi4IpCidrRouteTos = (INT4) OutRtInfo.u4Tos;
            *pu4IpCidrRouteNextHop = 0;
            return (SNMP_SUCCESS);
        }
        else
        {
            /* Here multiple nexthops can be there for default route
             * So we are calling GetNext to safely get correct
             * (least) nexthop entry for the default route
             */
            i4OutCome = (INT4) (nmhGetNextIndexIpCidrRouteTable
                                (0, pu4IpCidrRouteDest, 0, pu4IpCidrRouteMask,
                                 0, pi4IpCidrRouteTos, 0,
                                 pu4IpCidrRouteNextHop));
            if (i4OutCome == SNMP_SUCCESS)
            {
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        /* Default route is not present. */
        i4OutCome =
            (INT4) (nmhGetNextIndexIpCidrRouteTable
                    (0, pu4IpCidrRouteDest, 0, pu4IpCidrRouteMask, 0,
                     pi4IpCidrRouteTos, 0, pu4IpCidrRouteNextHop));
        if (i4OutCome == SNMP_SUCCESS)
        {
            return (SNMP_SUCCESS);
        }
    }

    return (SNMP_FAILURE);
}

/* GET_NEXT Routine.  */
/****************************************************************************
 Function    :  nmhGetNextIndexIpCidrRouteTable
 Input       :  The Indices
                IpCidrRouteDest
                nextIpCidrRouteDest
                IpCidrRouteMask
                nextIpCidrRouteMask
                IpCidrRouteTos
                nextIpCidrRouteTos
                IpCidrRouteNextHop
                nextIpCidrRouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpCidrRouteTable                 ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetNextIndexIpCidrRouteTable (UINT4 u4IpCidrRouteDest,
                                 UINT4 *pu4NextIpCidrRouteDest,
                                 UINT4 u4IpCidrRouteMask,
                                 UINT4 *pu4NextIpCidrRouteMask,
                                 INT4 i4IpCidrRouteTos,
                                 INT4 *pi4NextIpCidrRouteTos,
                                 UINT4 u4IpCidrRouteNextHop,
                                 UINT4 *pu4NextIpCidrRouteNextHop)
#else
INT1
nmhGetNextIndexIpCidrRouteTable (u4IpCidrRouteDest,
                                 pu4NextIpCidrRouteDest,
                                 u4IpCidrRouteMask,
                                 pu4NextIpCidrRouteMask,
                                 i4IpCidrRouteTos,
                                 i4NextIpCidrRouteTos,
                                 u4IpCidrRouteNextHop,
                                 pu4NextIpCidrRouteNextHop)
     UINT4               u4IpCidrRouteDest;
     UINT4              *pu4NextIpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     UINT4              *pu4NextIpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     INT4               *pi4NextIpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     UINT4              *pu4NextIpCidrRouteNextHop;
#endif
{

    tNetIpv4RtInfo      InRtInfo;
    tNetIpv4RtInfo      OutRtInfo1;
    tNetIpv4RtInfo      OutRtInfo2;
    static tNetIpv4RtInfo PrevRtInfo;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal1 = NETIPV4_FAILURE;
    INT4                i4RetVal2 = NETIPV4_FAILURE;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    MEMSET (&InRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&OutRtInfo1, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&OutRtInfo2, 0, sizeof (tNetIpv4RtInfo));
    InRtInfo.u4DestNet = u4IpCidrRouteDest;
    InRtInfo.u4DestMask = u4IpCidrRouteMask;
    InRtInfo.u4Tos = (UINT4) i4IpCidrRouteTos;
    InRtInfo.u4NextHop = u4IpCidrRouteNextHop;
    InRtInfo.u4ContextId = u4ContextId;

    if ((u4IpCidrRouteDest == 0) && (u4IpCidrRouteMask == 0)
        && (i4IpCidrRouteTos == 0) && (u4IpCidrRouteNextHop == 0))
    {
        MEMSET (&PrevRtInfo, 0, sizeof (PrevRtInfo));
        gu4PrevRouteInvalid = OSIX_FALSE;
        gi4ValidRouteRetVal = NETIPV4_SUCCESS;
        i4RetVal1 = RtmUtilGetNextFwdTableRtEntry (&InRtInfo, &OutRtInfo1);
        i4RetVal2 = RtmUtilGetNextInactiveRtEntry (&InRtInfo, &OutRtInfo2);
    }
    else
    {
        if (gu4PrevRouteInvalid == OSIX_TRUE)
        {
            if (gi4ValidRouteRetVal == NETIPV4_SUCCESS)
            {
                OutRtInfo1.u4DestNet = PrevRtInfo.u4DestNet;
                OutRtInfo1.u4DestMask = PrevRtInfo.u4DestMask;
                OutRtInfo1.u4Tos = PrevRtInfo.u4Tos;
                OutRtInfo1.u4NextHop = PrevRtInfo.u4NextHop;

                i4RetVal1 = NETIPV4_SUCCESS;
                i4RetVal2 =
                    RtmUtilGetNextInactiveRtEntry (&InRtInfo, &OutRtInfo2);
            }
            else
            {
                i4RetVal1 = NETIPV4_FAILURE;
                i4RetVal2 =
                    RtmUtilGetNextInactiveRtEntry (&InRtInfo, &OutRtInfo2);
            }

        }
        else
        {
            i4RetVal1 = RtmUtilGetNextFwdTableRtEntry (&InRtInfo, &OutRtInfo1);
            i4RetVal2 = RtmUtilGetNextInactiveRtEntry (&InRtInfo, &OutRtInfo2);
        }

    }

    if ((i4RetVal1 == NETIPV4_SUCCESS) && (i4RetVal2 == NETIPV4_SUCCESS))
    {
        /* As both OutRtInfo1 and OutRtInfo1 are greater than the given input
         * Copy route which is lesser*/
        if (RtmUtilCheckIsRouteGreater (&OutRtInfo1, &OutRtInfo2) == IP_SUCCESS)
        {
            *pu4NextIpCidrRouteDest = OutRtInfo2.u4DestNet;
            *pu4NextIpCidrRouteMask = OutRtInfo2.u4DestMask;
            *pi4NextIpCidrRouteTos = (INT4) OutRtInfo2.u4Tos;
            *pu4NextIpCidrRouteNextHop = OutRtInfo2.u4NextHop;
            PrevRtInfo.u4DestNet = OutRtInfo1.u4DestNet;
            PrevRtInfo.u4DestMask = OutRtInfo1.u4DestMask;
            PrevRtInfo.u4Tos = OutRtInfo1.u4Tos;
            PrevRtInfo.u4NextHop = OutRtInfo1.u4NextHop;
            gi4ValidRouteRetVal = NETIPV4_SUCCESS;
            gu4PrevRouteInvalid = OSIX_TRUE;
            return (SNMP_SUCCESS);
        }
        else
        {
            *pu4NextIpCidrRouteDest = OutRtInfo1.u4DestNet;
            *pu4NextIpCidrRouteMask = OutRtInfo1.u4DestMask;
            *pi4NextIpCidrRouteTos = (INT4) OutRtInfo1.u4Tos;
            *pu4NextIpCidrRouteNextHop = OutRtInfo1.u4NextHop;
            gu4PrevRouteInvalid = OSIX_FALSE;
            return (SNMP_SUCCESS);
        }
    }

    if ((i4RetVal1 == NETIPV4_SUCCESS) && (i4RetVal2 == NETIPV4_FAILURE))
    {
        *pu4NextIpCidrRouteDest = OutRtInfo1.u4DestNet;
        *pu4NextIpCidrRouteMask = OutRtInfo1.u4DestMask;
        *pi4NextIpCidrRouteTos = (INT4) OutRtInfo1.u4Tos;
        *pu4NextIpCidrRouteNextHop = OutRtInfo1.u4NextHop;
        gu4PrevRouteInvalid = OSIX_FALSE;
        return (SNMP_SUCCESS);
    }

    if ((i4RetVal1 == NETIPV4_FAILURE) && (i4RetVal2 == NETIPV4_SUCCESS))
    {
        *pu4NextIpCidrRouteDest = OutRtInfo2.u4DestNet;
        *pu4NextIpCidrRouteMask = OutRtInfo2.u4DestMask;
        *pi4NextIpCidrRouteTos = (INT4) OutRtInfo2.u4Tos;
        *pu4NextIpCidrRouteNextHop = OutRtInfo2.u4NextHop;
        gi4ValidRouteRetVal = NETIPV4_FAILURE;
        gu4PrevRouteInvalid = OSIX_TRUE;
        return (SNMP_SUCCESS);
    }
    MEMSET (&PrevRtInfo, 0, sizeof (PrevRtInfo));
    gi4ValidRouteRetVal = NETIPV4_SUCCESS;
    gu4PrevRouteInvalid = OSIX_FALSE;
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetIpCidrRouteIfIndex
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***    $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteIfIndex                ***/
/***    $$TRACE_PROCEDURE_LEVEL = LOW                                    ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteIfIndex (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 *pi4RetValIpCidrRouteIfIndex)
#else
INT1
nmhGetIpCidrRouteIfIndex (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          pi4RetValIpCidrRouteIfIndex)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteIfIndex;
#endif
{
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId, u4IpCidrRouteDest,
                                u4IpCidrRouteMask, i4IpCidrRouteTos,
                                u4IpCidrRouteNextHop, INVALID_PROTO,
                                pi4RetValIpCidrRouteIfIndex,
                                IPFWD_ROUTE_IFINDEX) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    /*Inactive routes ifindex may become valid later */
    /* So for inactive routes u4CfaIfIndex is 0 */
    NetIpv4GetCfaIfIndexFromPort ((UINT4) *pi4RetValIpCidrRouteIfIndex,
                                  &u4CfaIfIndex);

    *pi4RetValIpCidrRouteIfIndex = (INT4) u4CfaIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteType
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteType (UINT4 u4IpCidrRouteDest,
                       UINT4 u4IpCidrRouteMask,
                       INT4 i4IpCidrRouteTos,
                       UINT4 u4IpCidrRouteNextHop,
                       INT4 *pi4RetValIpCidrRouteType)
#else
INT1
nmhGetIpCidrRouteType (u4IpCidrRouteDest, u4IpCidrRouteMask,
                       i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                       pi4RetValIpCidrRouteType)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteType;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, pi4RetValIpCidrRouteType,
                                IPFWD_ROUTE_TYPE) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteProto
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***    $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteProto                  ***/
/***    $$TRACE_PROCEDURE_LEVEL = LOW                                    ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteProto (UINT4 u4IpCidrRouteDest,
                        UINT4 u4IpCidrRouteMask,
                        INT4 i4IpCidrRouteTos,
                        UINT4 u4IpCidrRouteNextHop,
                        INT4 *pi4RetValIpCidrRouteProto)
#else
INT1
nmhGetIpCidrRouteProto (u4IpCidrRouteDest, u4IpCidrRouteMask,
                        i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                        pi4RetValIpCidrRouteProto)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteProto;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, pi4RetValIpCidrRouteProto,
                                IPFWD_ROUTE_PROTO) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteAge
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***   $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteAge                     ***/
/***   $$TRACE_PROCEDURE_LEVEL = LOW                                     ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteAge (UINT4 u4IpCidrRouteDest, UINT4 u4IpCidrRouteMask,
                      INT4 i4IpCidrRouteTos,
                      UINT4 u4IpCidrRouteNextHop, INT4 *pi4RetValIpCidrRouteAge)
#else
INT1
nmhGetIpCidrRouteAge (u4IpCidrRouteDest, u4IpCidrRouteMask,
                      i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                      pi4RetValIpCidrRouteAge)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteAge;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, pi4RetValIpCidrRouteAge,
                                IPFWD_ROUTE_AGE) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteInfo
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteInfo                      ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteInfo (UINT4 u4IpCidrRouteDest,
                       UINT4 u4IpCidrRouteMask,
                       INT4 i4IpCidrRouteTos,
                       UINT4 u4IpCidrRouteNextHop,
                       tSNMP_OID_TYPE * pRetValIpCidrRouteInfo)
#else
INT1
nmhGetIpCidrRouteInfo (u4IpCidrRouteDest, u4IpCidrRouteMask,
                       i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                       pRetValIpCidrRouteInfo)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     tSNMP_OID_TYPE     *pRetValIpCidrRouteInfo;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    u4Unused = u4ContextId;

    /* Return NULL VarBind and the memory Allocated by the Agent */
    (*pRetValIpCidrRouteInfo).u4_Length = 2;
    (*pRetValIpCidrRouteInfo).pu4_OidList[0] = 0;
    (*pRetValIpCidrRouteInfo).pu4_OidList[1] = 0;

    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteNextHopAS
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteNextHopAS (UINT4 u4IpCidrRouteDest,
                            UINT4 u4IpCidrRouteMask,
                            INT4 i4IpCidrRouteTos,
                            UINT4 u4IpCidrRouteNextHop,
                            INT4 *pi4RetValIpCidrRouteNextHopAS)
#else
INT1
nmhGetIpCidrRouteNextHopAS (u4IpCidrRouteDest, u4IpCidrRouteMask,
                            i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                            pi4RetValIpCidrRouteNextHopAS)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteNextHopAS;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, pi4RetValIpCidrRouteNextHopAS,
                                IPFWD_ROUTE_NEXTHOPAS) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteHWStatus
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object
                retValIpCidrRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteHWStatus                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteHWStatus (UINT4 u4IpCidrRouteDest,
                           UINT4 u4IpCidrRouteMask,
                           INT4 i4IpCidrRouteTos,
                           UINT4 u4IpCidrRouteNextHop,
                           INT4 *pi4RetValIpCidrRouteHWStatus)
#else
INT1
nmhGetIpCidrRouteHWStatus (u4IpCidrRouteDest, u4IpCidrRouteMask,
                           i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                           pi4RetValIpCidrRouteStatus)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteHWStatus;
#endif
{
    UINT4               u4ContextId = 0;
    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, pi4RetValIpCidrRouteHWStatus,
                                IPFWD_ROUTE_STATUS) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteMetric1
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteMetric1                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteMetric1 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 *pi4RetValIpCidrRouteMetric1)
#else
INT1
nmhGetIpCidrRouteMetric1 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          pi4RetValIpCidrRouteMetric1)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteMetric1;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, pi4RetValIpCidrRouteMetric1,
                                IPFWD_ROUTE_METRIC1) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteMetric2
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteMetric2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteMetric2                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteMetric2 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 *pi4RetValIpCidrRouteMetric2)
#else
INT1
nmhGetIpCidrRouteMetric2 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          pi4RetValIpCidrRouteMetric2)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteMetric2;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    u4Unused = u4ContextId;

    *pi4RetValIpCidrRouteMetric2 = -1;
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteMetric3
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteMetric3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteMetric3 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 *pi4RetValIpCidrRouteMetric3)
#else
INT1
nmhGetIpCidrRouteMetric3 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          pi4RetValIpCidrRouteMetric3)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteMetric3;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    u4Unused = u4ContextId;

    *pi4RetValIpCidrRouteMetric3 = -1;
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteMetric4
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteMetric4
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteMetric4                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteMetric4 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 *pi4RetValIpCidrRouteMetric4)
#else
INT1
nmhGetIpCidrRouteMetric4 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          pi4RetValIpCidrRouteMetric4)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteMetric4;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    u4Unused = u4ContextId;

    *pi4RetValIpCidrRouteMetric4 = -1;
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteMetric5
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteMetric5
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteMetric5                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteMetric5 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 *pi4RetValIpCidrRouteMetric5)
#else
INT1
nmhGetIpCidrRouteMetric5 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          pi4RetValIpCidrRouteMetric5)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteMetric5;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    u4Unused = u4ContextId;

    *pi4RetValIpCidrRouteMetric5 = -1;
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIpCidrRouteStatus
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                retValIpCidrRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpCidrRouteStatus                    ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpCidrRouteStatus (UINT4 u4IpCidrRouteDest,
                         UINT4 u4IpCidrRouteMask,
                         INT4 i4IpCidrRouteTos,
                         UINT4 u4IpCidrRouteNextHop,
                         INT4 *pi4RetValIpCidrRouteStatus)
#else
INT1
nmhGetIpCidrRouteStatus (u4IpCidrRouteDest, u4IpCidrRouteMask,
                         i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                         pi4RetValIpCidrRouteStatus)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4               *pi4RetValIpCidrRouteStatus;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, pi4RetValIpCidrRouteStatus,
                                IPFWD_ROUTE_ROW_STATUS) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIpCidrRouteIfIndex
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***   $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteIfIndex                 ***/
/***   $$TRACE_PROCEDURE_LEVEL = LOW                                     ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteIfIndex (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 i4SetValIpCidrRouteIfIndex)
#else
INT1
nmhSetIpCidrRouteIfIndex (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          i4SetValIpCidrRouteIfIndex)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteIfIndex;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, &i4SetValIpCidrRouteIfIndex,
                                IPFWD_ROUTE_IFINDEX) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv4RouteTable (u4ContextId, u4IpCidrRouteDest,
                             u4IpCidrRouteMask, u4IpCidrRouteNextHop,
                             i4SetValIpCidrRouteIfIndex,
                             FsMIStdInetCidrRouteIfIndex,
                             sizeof (FsMIStdInetCidrRouteIfIndex) /
                             sizeof (UINT4), FALSE);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteType
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteType                      ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteType (UINT4 u4IpCidrRouteDest,
                       UINT4 u4IpCidrRouteMask,
                       INT4 i4IpCidrRouteTos,
                       UINT4 u4IpCidrRouteNextHop, INT4 i4SetValIpCidrRouteType)
#else
INT1
nmhSetIpCidrRouteType (u4IpCidrRouteDest,
                       u4IpCidrRouteMask,
                       i4IpCidrRouteTos,
                       u4IpCidrRouteNextHop, i4SetValIpCidrRouteType)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteType;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, &i4SetValIpCidrRouteType,
                                IPFWD_ROUTE_TYPE) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv4RouteTable (u4ContextId, u4IpCidrRouteDest,
                             u4IpCidrRouteMask, u4IpCidrRouteNextHop,
                             i4SetValIpCidrRouteType,
                             FsMIStdInetCidrRouteType,
                             sizeof (FsMIStdInetCidrRouteType) /
                             sizeof (UINT4), FALSE);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteInfo
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteInfo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteInfo ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteInfo (UINT4 u4IpCidrRouteDest,
                       UINT4 u4IpCidrRouteMask,
                       INT4 i4IpCidrRouteTos,
                       UINT4 u4IpCidrRouteNextHop,
                       tSNMP_OID_TYPE * pSetValIpCidrRouteInfo)
#else
INT1
nmhSetIpCidrRouteInfo (u4IpCidrRouteDest, u4IpCidrRouteMask,
                       i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                       pSetValIpCidrRouteInfo)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     tSNMP_OID_TYPE     *pSetValIpCidrRouteInfo;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    u4Unused = u4ContextId;

    if (pSetValIpCidrRouteInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Unused);
    UNUSED_PARAM (u4Unused);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteNextHopAS
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteNextHopAS                 ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteNextHopAS (UINT4 u4IpCidrRouteDest,
                            UINT4 u4IpCidrRouteMask,
                            INT4 i4IpCidrRouteTos,
                            UINT4 u4IpCidrRouteNextHop,
                            INT4 i4SetValIpCidrRouteNextHopAS)
#else
INT1
nmhSetIpCidrRouteNextHopAS (u4IpCidrRouteDest, u4IpCidrRouteMask,
                            i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                            i4SetValIpCidrRouteNextHopAS)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteNextHopAS;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;

    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4SetValIpCidrRouteNextHopAS;
    u4Unused = u4ContextId;

    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteMetric1
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteMetric1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteMetric1                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteMetric1 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 i4SetValIpCidrRouteMetric1)
#else
INT1
nmhSetIpCidrRouteMetric1 (u4IpCidrRouteDest,
                          u4IpCidrRouteMask,
                          i4IpCidrRouteTos,
                          u4IpCidrRouteNextHop, i4SetValIpCidrRouteMetric1)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteMetric1;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, &i4SetValIpCidrRouteMetric1,
                                IPFWD_ROUTE_METRIC1) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    IncMsrForIpv4RouteTable (u4ContextId, u4IpCidrRouteDest,
                             u4IpCidrRouteMask, u4IpCidrRouteNextHop,
                             i4SetValIpCidrRouteMetric1,
                             FsMIStdInetCidrRouteMetric1,
                             sizeof (FsMIStdInetCidrRouteMetric1) /
                             sizeof (UINT4), FALSE);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteMetric2
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteMetric2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteMetric2                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteMetric2 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 i4SetValIpCidrRouteMetric2)
#else
INT1
nmhSetIpCidrRouteMetric2 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          i4SetValIpCidrRouteMetric2)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteMetric2;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4SetValIpCidrRouteMetric2;
    u4Unused = u4ContextId;

    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteMetric3
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteMetric3
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteMetric3                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteMetric3 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 i4SetValIpCidrRouteMetric3)
#else
INT1
nmhSetIpCidrRouteMetric3 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          i4SetValIpCidrRouteMetric3)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteMetric3;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4SetValIpCidrRouteMetric3;
    u4Unused = u4ContextId;

    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteMetric4
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteMetric4
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteMetric4                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteMetric4 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 i4SetValIpCidrRouteMetric4)
#else
INT1
nmhSetIpCidrRouteMetric4 (u4IpCidrRouteDest, u4IpCidrRouteMask,
                          i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                          i4SetValIpCidrRouteMetric4)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteMetric4;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4SetValIpCidrRouteMetric4;
    u4Unused = u4ContextId;

    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteMetric5
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteMetric5
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteMetric5                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteMetric5 (UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 i4SetValIpCidrRouteMetric5)
#else
INT1
nmhSetIpCidrRouteMetric5 (u4IpCidrRouteDest,
                          u4IpCidrRouteMask,
                          i4IpCidrRouteTos,
                          u4IpCidrRouteNextHop, i4SetValIpCidrRouteMetric5)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteMetric5;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4SetValIpCidrRouteMetric5;
    u4Unused = u4ContextId;

    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetIpCidrRouteStatus
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                setValIpCidrRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpCidrRouteStatus                    ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpCidrRouteStatus (UINT4 u4IpCidrRouteDest,
                         UINT4 u4IpCidrRouteMask,
                         INT4 i4IpCidrRouteTos,
                         UINT4 u4IpCidrRouteNextHop,
                         INT4 i4SetValIpCidrRouteStatus)
#else
INT1
nmhSetIpCidrRouteStatus (u4IpCidrRouteDest, u4IpCidrRouteMask,
                         i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                         i4SetValIpCidrRouteStatus)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4SetValIpCidrRouteStatus;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4IpCidrRouteDest, u4IpCidrRouteMask,
                                i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                INVALID_PROTO, &i4SetValIpCidrRouteStatus,
                                IPFWD_ROUTE_ROW_STATUS) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    IncMsrForIpv4RouteTable (u4ContextId, u4IpCidrRouteDest,
                             u4IpCidrRouteMask, u4IpCidrRouteNextHop,
                             i4SetValIpCidrRouteStatus,
                             FsMIStdInetCidrRouteStatus,
                             sizeof (FsMIStdInetCidrRouteStatus) /
                             sizeof (UINT4), TRUE);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteIfIndex
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteIfIndex                  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteIfIndex (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                             UINT4 u4IpCidrRouteMask,
                             INT4 i4IpCidrRouteTos,
                             UINT4 u4IpCidrRouteNextHop,
                             INT4 i4TestValIpCidrRouteIfIndex)
#else
INT1                nmhTestv2IpCidrRouteIfIndex (pu4ErrorCode,
                                                 u4IpCidrRouteDest,
                                                 u4IpCidrRouteMask,
                                                 UINT4 *pu4ErrorCode;
                                                 i4IpCidrRouteTos,
                                                 u4IpCidrRouteNextHop,
                                                 i4TestValIpCidrRouteIfIndex)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteIfIndex;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblTestObjectInCxt
        (u4ContextId, u4IpCidrRouteDest, u4IpCidrRouteMask, i4IpCidrRouteTos,
         u4IpCidrRouteNextHop, INVALID_PROTO, &i4TestValIpCidrRouteIfIndex,
         IPFWD_ROUTE_IFINDEX, pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteType
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteType                     ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteType (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          INT4 i4TestValIpCidrRouteType)
#else
INT1                nmhTestv2IpCidrRouteType (pu4ErrorCode, u4IpCidrRouteDest,
                                              u4IpCidrRouteMask,
                                              UINT4 *pu4ErrorCode;
                                              i4IpCidrRouteTos,
                                              u4IpCidrRouteNextHop,
                                              i4TestValIpCidrRouteType)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteType;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblTestObjectInCxt
        (u4ContextId, u4IpCidrRouteDest, u4IpCidrRouteMask, i4IpCidrRouteTos,
         u4IpCidrRouteNextHop, INVALID_PROTO, &i4TestValIpCidrRouteType,
         IPFWD_ROUTE_TYPE, pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteInfo
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteInfo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteInfo                     ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteInfo (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                          UINT4 u4IpCidrRouteMask,
                          INT4 i4IpCidrRouteTos,
                          UINT4 u4IpCidrRouteNextHop,
                          tSNMP_OID_TYPE * pTestValIpCidrRouteInfo)
#else
INT1                nmhTestv2IpCidrRouteInfo (pu4ErrorCode, u4IpCidrRouteDest,
                                              u4IpCidrRouteMask,
                                              UINT4 *pu4ErrorCode;
                                              i4IpCidrRouteTos,
                                              u4IpCidrRouteNextHop,
                                              pTestValIpCidrRouteInfo)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     tSNMP_OID_TYPE     *pTestValIpCidrRouteInfo;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;

    /* Invalid Mask and network combination */
    if (((u4IpCidrRouteDest & u4IpCidrRouteMask) != u4IpCidrRouteDest))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP_INVALID_IP_MASK_ERR);
        return SNMP_FAILURE;
    }

    /* NextHop Ip addr configured for a static route 
     * should not be our local interface ip addr */
    if ((u4IpCidrRouteNextHop != 0) &&
        (NetIpv4IfIsOurAddressInCxt (u4ContextId, u4IpCidrRouteNextHop) ==
         NETIPV4_SUCCESS))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_IP_INVALID_NEXTHOP_ERR);
        return SNMP_FAILURE;
    }

    /* NextHop Ip addr configured for a static route
     * should not be in the same network as the 
     * destination */
    if ((u4IpCidrRouteDest != 0) && ((u4IpCidrRouteNextHop & u4IpCidrRouteMask)
                                     == u4IpCidrRouteDest))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP_INVALID_NEXTHOP_ERR);
        return SNMP_FAILURE;
    }
    if (i4IpCidrRouteTos != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValIpCidrRouteInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    UNUSED_PARAM (i4Unused);
    UNUSED_PARAM (u4Unused);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteNextHopAS
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteNextHopAS                ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteNextHopAS (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                               UINT4 u4IpCidrRouteMask,
                               INT4 i4IpCidrRouteTos,
                               UINT4 u4IpCidrRouteNextHop,
                               INT4 i4TestValIpCidrRouteNextHopAS)
#else
INT1                nmhTestv2IpCidrRouteNextHopAS (pu4ErrorCode,
                                                   u4IpCidrRouteDest,
                                                   u4IpCidrRouteMask,
                                                   UINT4 *pu4ErrorCode;
                                                   i4IpCidrRouteTos,
                                                   u4IpCidrRouteNextHop,
                                                   i4TestValIpCidrRouteNextHopAS)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteNextHopAS;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4IpCidrRouteDest, u4IpCidrRouteMask,
                                 i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                 INVALID_PROTO, &i4TestValIpCidrRouteNextHopAS,
                                 IPFWD_ROUTE_NEXTHOPAS,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteMetric1
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteMetric1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteMetric1                  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteMetric1 (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                             UINT4 u4IpCidrRouteMask,
                             INT4 i4IpCidrRouteTos,
                             UINT4 u4IpCidrRouteNextHop,
                             INT4 i4TestValIpCidrRouteMetric1)
#else
INT1                nmhTestv2IpCidrRouteMetric1 (pu4ErrorCode,
                                                 u4IpCidrRouteDest,
                                                 u4IpCidrRouteMask,
                                                 UINT4 *pu4ErrorCode;
                                                 i4IpCidrRouteTos,
                                                 u4IpCidrRouteNextHop,
                                                 i4TestValIpCidrRouteMetric1)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteMetric1;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4IpCidrRouteDest, u4IpCidrRouteMask,
                                 i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                 INVALID_PROTO, &i4TestValIpCidrRouteMetric1,
                                 IPFWD_ROUTE_METRIC1,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteMetric2
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteMetric2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteMetric2                  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteMetric2 (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                             UINT4 u4IpCidrRouteMask,
                             INT4 i4IpCidrRouteTos,
                             UINT4 u4IpCidrRouteNextHop,
                             INT4 i4TestValIpCidrRouteMetric2)
#else
INT1                nmhTestv2IpCidrRouteMetric2 (pu4ErrorCode,
                                                 u4IpCidrRouteDest,
                                                 u4IpCidrRouteMask,
                                                 UINT4 *pu4ErrorCode;
                                                 i4IpCidrRouteTos,
                                                 u4IpCidrRouteNextHop,
                                                 i4TestValIpCidrRouteMetric2)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteMetric2;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4TestValIpCidrRouteMetric2;

    if (IpFwdTblTestObjectInCxt
        (u4ContextId, u4IpCidrRouteDest, u4IpCidrRouteMask, i4IpCidrRouteTos,
         u4IpCidrRouteNextHop, INVALID_PROTO, &i4TestValIpCidrRouteMetric2,
         IPFWD_ROUTE_METRIC2, pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteMetric3
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteMetric3
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteMetric3                  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteMetric3 (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                             UINT4 u4IpCidrRouteMask,
                             INT4 i4IpCidrRouteTos,
                             UINT4 u4IpCidrRouteNextHop,
                             INT4 i4TestValIpCidrRouteMetric3)
#else
INT1                nmhTestv2IpCidrRouteMetric3 (pu4ErrorCode,
                                                 u4IpCidrRouteDest,
                                                 u4IpCidrRouteMask,
                                                 UINT4 *pu4ErrorCode;
                                                 i4IpCidrRouteTos,
                                                 u4IpCidrRouteNextHop,
                                                 i4TestValIpCidrRouteMetric3)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteMetric3;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4TestValIpCidrRouteMetric3;

    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4IpCidrRouteDest, u4IpCidrRouteMask,
                                 i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                 INVALID_PROTO, &i4TestValIpCidrRouteMetric3,
                                 IPFWD_ROUTE_METRIC3,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteMetric4
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteMetric4
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteMetric4                  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteMetric4 (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                             UINT4 u4IpCidrRouteMask,
                             INT4 i4IpCidrRouteTos,
                             UINT4 u4IpCidrRouteNextHop,
                             INT4 i4TestValIpCidrRouteMetric4)
#else
INT1                nmhTestv2IpCidrRouteMetric4 (pu4ErrorCode,
                                                 u4IpCidrRouteDest,
                                                 u4IpCidrRouteMask,
                                                 UINT4 *pu4ErrorCode;
                                                 i4IpCidrRouteTos,
                                                 u4IpCidrRouteNextHop,
                                                 i4TestValIpCidrRouteMetric4)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteMetric4;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4TestValIpCidrRouteMetric4;

    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4IpCidrRouteDest, u4IpCidrRouteMask,
                                 i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                 INVALID_PROTO, &i4TestValIpCidrRouteMetric4,
                                 IPFWD_ROUTE_METRIC4,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteMetric5
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteMetric5
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteMetric5                  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteMetric5 (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                             UINT4 u4IpCidrRouteMask,
                             INT4 i4IpCidrRouteTos,
                             UINT4 u4IpCidrRouteNextHop,
                             INT4 i4TestValIpCidrRouteMetric5)
#else
INT1                nmhTestv2IpCidrRouteMetric5 (pu4ErrorCode,
                                                 u4IpCidrRouteDest,
                                                 u4IpCidrRouteMask,
                                                 UINT4 *pu4ErrorCode;
                                                 i4IpCidrRouteTos,
                                                 u4IpCidrRouteNextHop,
                                                 i4TestValIpCidrRouteMetric5)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteMetric5;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    /* Makefile Changes - unused parameter */
    u4Unused = u4IpCidrRouteDest;
    u4Unused = u4IpCidrRouteMask;
    i4Unused = i4IpCidrRouteTos;
    u4Unused = u4IpCidrRouteNextHop;
    i4Unused = i4TestValIpCidrRouteMetric5;

    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4IpCidrRouteDest, u4IpCidrRouteMask,
                                 i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                 INVALID_PROTO, &i4TestValIpCidrRouteMetric5,
                                 IPFWD_ROUTE_METRIC5,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpCidrRouteStatus
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop

                The Object 
                testValIpCidrRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpCidrRouteStatus                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpCidrRouteStatus (UINT4 *pu4ErrorCode, UINT4 u4IpCidrRouteDest,
                            UINT4 u4IpCidrRouteMask,
                            INT4 i4IpCidrRouteTos,
                            UINT4 u4IpCidrRouteNextHop,
                            INT4 i4TestValIpCidrRouteStatus)
#else
INT1                nmhTestv2IpCidrRouteStatus (pu4ErrorCode, u4IpCidrRouteDest,
                                                u4IpCidrRouteMask,
                                                UINT4 *pu4ErrorCode;
                                                i4IpCidrRouteTos,
                                                u4IpCidrRouteNextHop,
                                                i4TestValIpCidrRouteStatus)
     UINT4               u4IpCidrRouteDest;
     UINT4               u4IpCidrRouteMask;
     INT4                i4IpCidrRouteTos;
     UINT4               u4IpCidrRouteNextHop;
     INT4                i4TestValIpCidrRouteStatus;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4IpCidrRouteDest, u4IpCidrRouteMask,
                                 i4IpCidrRouteTos, u4IpCidrRouteNextHop,
                                 INVALID_PROTO, &i4TestValIpCidrRouteStatus,
                                 IPFWD_ROUTE_ROW_STATUS,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name             : IpEnableRouting                              */
/*  Description               : Enable unicast IPv4 routing on a virtual     */
/*  router                                                                   */
/*  Input(s)                  : u4VrId   - The virtual router identifier.    */
/*                              u1Status - FNP_TRUE - enable unicast routing */
/*                                         on the specified VR.              */
/*                                         FNP_FALSE - disable unicast       */
/*                                         routing on the specified VR       */
/*  Output(s)                 : None                                         */
/*  Global Variables Referred : None                                         */
/*  Global variables Modified : None                                         */
/*  Exceptions                : None                                         */
/*  Use of Recursion          : None                                         */
/*  Returns                   : IP_SUCCESS/IP_FAILURE                        */
/*****************************************************************************/
INT4
IpEnableRouting (UINT4 u4VrId, UINT1 u1Status)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal;

    if (u1Status == IP_FORW_ENABLE)
    {

        u1Status = FNP_TRUE;
    }
    else
    {
        u1Status = FNP_FALSE;
    }
    i4RetVal = IpFsNpIpv4SetForwardingStatus (u4VrId, u1Status);
    if (i4RetVal == FNP_FAILURE)

    {
        return IP_FAILURE;
    }
#endif
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (u4VrId);

    return IP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpFwdTblGetObjectInCxt                           */
/*                                                                       */
/* Description       :  This fuction does a GET Object in  the  IP Cidr  */
/*                      forwarding table based on the Object type        */
/*                      specified.                                       */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  u4contextId - RTM context ID                     */
/*                      u4Dest - The Destination address.                */
/*                      u4Mask - The Desination Mask                     */
/*                      i4Tos  - The Type of service                     */
/*                      u4NextHop - The Next hop gateway address         */
/*                      i4Proto -   The routing protocol                 */
/*                      u1ObjType - The Object type being accessed       */
/*                                                                       */
/* Output(s)         :  *pi4Val - The Value of the object for which the  */
/*                      GET has been performed                           */
/*                                                                       */
/* Returns           :  SNMP_SUCCESS | SNMP_FAILURE                                */
/*************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = IpFwdTblGetObjectInCxt                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                     ***/
/*************************************************************************/
#ifdef __STDC__
INT4
IpFwdTblGetObjectInCxt (UINT4 u4ContextId, UINT4 u4Dest,
                        UINT4 u4Mask, INT4 i4Tos,
                        UINT4 u4NextHop, INT4 i4Proto,
                        INT4 *pi4Val, UINT1 u1ObjType)
#else
INT4
IpFwdTblGetObjectInCxt (u4ContextId, u4Dest, u4Mask, i4Tos, u4NextHop,
                        i4Proto, pVal, u1ObjType)
     UINT4               u4ContextId;
     UINT4               u4Dest;
     UINT4               u4Mask;
     INT4                i4Tos;
     UINT4               u4NextHop;
     INT4                i4Proto;
     INT4               *pi4Val;
     UINT1               u1ObjType;
#endif
{
    if (IpTableGetObjectInCxt (u4ContextId, u4Dest, u4Mask, i4Tos, u4NextHop,
                               i4Proto, pi4Val, u1ObjType) == IP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/*************************************************************************/
/* Function Name     :  IpFwdTblSetObjectInCxt                           */
/*                                                                       */
/* Description       :  This fuction does a SET Object in  the  IP Cidr  */
/*                      forwarding table based on the Object type        */
/*                      specified.                                       */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  u4ContextId - RTM Context ID                     */
/*                   :  u4Dest - The Destination address.                */
/*                      u4Mask - The Desination Mask                     */
/*                      i4Tos  - The Type of service                     */
/*                      u4NextHop - The Next hop gatewa address          */
/*                      i4Proto -   The routing protocol                 */
/*                      u1ObjType - The Object type being accessed       */
/*                                                                       */
/* Output(s)         :  *pi4Val - The Value of the object for which the  */
/*                                SET is performed                       */
/*                                                                       */
/* Returns           :  SNMP_SUCCESS | SNMP_FAILURE                                */
/*************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = IpFwdTblSetObjectInCxt                   ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                     ***/
/*************************************************************************/
#ifdef __STDC__
INT4
IpFwdTblSetObjectInCxt (UINT4 u4ContextId, UINT4 u4Dest,
                        UINT4 u4Mask, INT4 i4Tos, UINT4 u4NextHop,
                        INT4 i4Proto, INT4 *pi4Val, UINT1 u1ObjType)
#else
INT4
IpFwdTblSetObjectInCxt (u4ContextId, u4Dest, u4Mask, i4Tos,
                        u4NextHop, i4Proto, pi4Val, u1ObjType)
     UINT4               u4ContextId;
     UINT4               u4Dest;
     UINT4               u4Mask;
     INT4                i4Tos;
     UINT4               u4NextHop;
     INT4                i4Proto;
     INT4               *pi4Val;
     UINT1               u1ObjType;
#endif
{
    if (IpCidrTblSetObjectInCxt (u4ContextId, u4Dest, u4Mask, i4Tos,
                                 u4NextHop, i4Proto, pi4Val,
                                 u1ObjType) == RTM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpFwdTblTestObjectInCxt                          */
/*                                                                       */
/* Description       :  This fuction does a TEST Object in  the  IP Cidr */
/*                      forwarding table based on the Object type        */
/*                      specified.                                       */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  u4ContextId - RTM Context ID                     */
/*                   :  u4Dest - The Destination address.                */
/*                      u4Mask - The Desination Mask                     */
/*                      i4Tos  - The Type of service                     */
/*                      u4NextHop - The Next hop gatewa address          */
/*                      i4Proto -   The routing protocol                 */
/*                      u1ObjType - The Object type being accessed       */
/*                                                                       */
/* Output(s)         :  *pi4Val - The Value of the object for which the  */
/*                                TEST is performed                      */
/*                                                                       */
/* Returns           :  SNMP_SUCCESS | SNMP_FAILURE                                */
/*************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = IpFwdTblTestObjectInCxt                  ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                     ***/
/*************************************************************************/
#ifdef __STDC__
INT4
IpFwdTblTestObjectInCxt (UINT4 u4ContextId, UINT4 u4Dest, UINT4 u4Mask,
                         INT4 i4Tos, UINT4 u4NextHop, INT4 i4Proto,
                         INT4 *pi4Val, UINT1 u1ObjType, UINT4 *pu4ErrorCode)
#else
INT4
IpFwdTblTestObjectInCxt (u4ContextId, u4Dest, u4Mask, i4Tos, u4NextHop,
                         i4Proto, pi4Val, u1ObjType, pu4ErrorCode)
     UINT4               u4ContextId;
     UINT4               u4Dest;
     UINT4               u4Mask;
     INT4                i4Tos;
     UINT4               u4NextHop;
     INT4                i4Proto;
     INT4               *pi4Val;
     UINT1               u1ObjType;
     UINT4              *pu4ErrorCode;
#endif
{
    if (IpCidrTblTestObjectInCxt (u4ContextId, u4Dest, u4Mask, i4Tos, u4NextHop,
                                  i4Proto, pi4Val, u1ObjType,
                                  pu4ErrorCode) == RTM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*************************  ARP CACHE ************************************
 * Following Are The Fields in the ARP Cache Table.
 *
 *  INDEX      Iface_Id     --   Interface Id
 *  INDEX      u4Ip_addr    --   IP Address for the Entry
 *  READ-WRITE i1pHw_addr   --   Physical Address
 *  READ-WRITE i1Hwalen     --   Hardware address length
 *  READ-WRITE i1Palen      --   Protocol address length
 *  READ-WRITE i1State      --   Status Of the Entry
 **************************************************************************/

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpNetToMediaTable
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpNetToMediaTable (INT4 i4IpNetToMediaIfIndex,
                                           UINT4 u4IpNetToMediaNetAddress)
#else
INT1
nmhValidateIndexInstanceIpNetToMediaTable (i4IpNetToMediaIfIndex,
                                           u4IpNetToMediaNetAddress)
     INT4                i4IpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
#endif
{
    t_ARP_CACHE         tArp_cache;
    tNetIpv4IfInfo      NetIpIfInfo;
    INT2                i2Hardware = 0;
    UINT4               u4Port = 0;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IpNetToMediaIfIndex, &u4Port) ==
        NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) != NETIPV4_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    i2Hardware = ARP_IFACE_TYPE ((UINT1) NetIpIfInfo.u4IfType);

    if (arp_get_hw_entry (i2Hardware) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ArpLookupWithIndex
        ((UINT4) i4IpNetToMediaIfIndex, u4IpNetToMediaNetAddress,
         &tArp_cache) == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpNetToMediaTable
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

#ifdef __STDC__
INT1
nmhGetFirstIndexIpNetToMediaTable (INT4 *pi4IpNetToMediaIfIndex,
                                   UINT4 *pu4IpNetToMediaNetAddress)
#else
INT1
nmhGetFirstIndexIpNetToMediaTable (pi4IpNetToMediaIfIndex,
                                   pu4IpNetToMediaNetAddress)
     INT4               *pi4IpNetToMediaIfIndex;
     UINT4              *pu4IpNetToMediaNetAddress;
#endif
{
    return (nmhGetNextIndexIpNetToMediaTable (0, pi4IpNetToMediaIfIndex,
                                              0, pu4IpNetToMediaNetAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpNetToMediaTable
 Input       :  The Indices
                IpNetToMediaIfIndex
                nextIpNetToMediaIfIndex
                IpNetToMediaNetAddress
                nextIpNetToMediaNetAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
#ifdef __STDC__
INT1
nmhGetNextIndexIpNetToMediaTable (INT4 i4IpNetToMediaIfIndex,
                                  INT4 *pi4NextIpNetToMediaIfIndex,
                                  UINT4 u4IpNetToMediaNetAddress,
                                  UINT4 *pu4NextIpNetToMediaNetAddress)
#else
INT1
nmhGetNextIndexIpNetToMediaTable (i4IpNetToMediaIfIndex,
                                  pi4NextIpNetToMediaIfIndex,
                                  u4IpNetToMediaNetAddress,
                                  pu4NextIpNetToMediaNetAddress)
     INT4                i4IpNetToMediaIfIndex;
     INT4               *pi4NextIpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
     UINT4              *pu4NextIpNetToMediaNetAddress;
#endif
{
    if (i4IpNetToMediaIfIndex < 0)
    {
        return SNMP_FAILURE;
    }

    if (ArpGetNextFromArpcacheTable (i4IpNetToMediaIfIndex,
                                     pi4NextIpNetToMediaIfIndex,
                                     u4IpNetToMediaNetAddress,
                                     pu4NextIpNetToMediaNetAddress) ==
        ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*********************** GET routines for ARP CACHE **************************/
/****************************************************************************
 Function    :  nmhGetIpNetToMediaPhysAddress
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object
                retValIpNetToMediaPhysAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpNetToMediaPhysAddress (INT4 i4IpNetToMediaIfIndex,
                               UINT4 u4IpNetToMediaNetAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIpNetToMediaPhysAddress)
#else
INT1
nmhGetIpNetToMediaPhysAddress (i4IpNetToMediaIfIndex,
                               u4IpNetToMediaNetAddress,
                               pRetValIpNetToMediaPhysAddress)
     INT4                i4IpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
     tSNMP_OCTET_STRING_TYPE *pRetValIpNetToMediaPhysAddress;
#endif
{
    t_ARP_CACHE         tArp_cache;
    UINT4               u4Port = 0;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IpNetToMediaIfIndex, &u4Port) ==
        NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (ArpLookupWithIndex
        ((UINT4) i4IpNetToMediaIfIndex, u4IpNetToMediaNetAddress,
         &tArp_cache) == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMCPY ((INT1 *) pRetValIpNetToMediaPhysAddress->pu1_OctetList,
            tArp_cache.i1Hw_addr, tArp_cache.i1Hwalen);

    pRetValIpNetToMediaPhysAddress->i4_Length = tArp_cache.i1Hwalen;
    return SNMP_SUCCESS;
}

/*
 * Routine to get status (type) field of cache entry.
 */
/****************************************************************************
 Function    :  nmhGetIpNetToMediaType
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object
                retValIpNetToMediaType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetIpNetToMediaType (INT4 i4IpNetToMediaIfIndex,
                        UINT4 u4IpNetToMediaNetAddress,
                        INT4 *pi4RetValIpNetToMediaType)
#else
INT1
nmhGetIpNetToMediaType (i4IpNetToMediaIfIndex,
                        u4IpNetToMediaNetAddress, pi4RetValIpNetToMediaType)
     INT4                i4IpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
     INT4               *pi4RetValIpNetToMediaType;
#endif
{
    t_ARP_CACHE         tArp_cache;

    if (ArpLookupWithIndex
        ((UINT4) i4IpNetToMediaIfIndex, u4IpNetToMediaNetAddress,
         &tArp_cache) == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((tArp_cache.i1State == ARP_PENDING) ||
        (tArp_cache.i1State == ARP_AGEOUT))
    {
        *pi4RetValIpNetToMediaType = (INT4) ARP_OTHER;
        return SNMP_SUCCESS;
    }
    else
    {
        /* Static ARP is maintained in Invalid state when interface
         * is down */
        if ((tArp_cache.i1State == ARP_INVALID) ||
            (tArp_cache.i1State == ARP_STATIC_NOT_IN_SERVICE))
        {
            tArp_cache.i1State = ARP_STATIC;
        }
        *pi4RetValIpNetToMediaType = tArp_cache.i1State;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetIpNetToMediaPhysAddress
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object
                setValIpNetToMediaPhysAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpNetToMediaPhysAddress (INT4 i4IpNetToMediaIfIndex,
                               UINT4 u4IpNetToMediaNetAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIpNetToMediaPhysAddress)
#else
INT1
nmhSetIpNetToMediaPhysAddress (i4IpNetToMediaIfIndex,
                               u4IpNetToMediaNetAddress,
                               pSetValIpNetToMediaPhysAddress)
     INT4                i4IpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
     tSNMP_OCTET_STRING_TYPE *pSetValIpNetToMediaPhysAddress;

#endif
{
    tNetIpv4IfInfo      NetIpIfInfo;
    INT2                i2Hardware = 0;
    UINT4               u4Port = 0;
    tARP_DATA           tArpData;
    t_ARP_CACHE         tArp_cache;
    UINT1               u1EncapType = 0;
    INT4                i4RSValue = 0;

    MEMSET (&tArpData, 0, sizeof (tARP_DATA));

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IpNetToMediaIfIndex, &u4Port) ==
        NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i2Hardware = ARP_IFACE_TYPE ((UINT1) NetIpIfInfo.u4IfType);

    if (ArpLookupWithIndex
        ((UINT4) i4IpNetToMediaIfIndex, u4IpNetToMediaNetAddress,
         &tArp_cache) == ARP_SUCCESS)
    {
        if ((tArp_cache.i1State != (INT1) ARP_STATIC) &&
            (tArp_cache.i1State != ARP_INVALID))
        {
            /* Remove old dynamic entry and add new static entry */
            ArpModifyWithIndex ((UINT4) i4IpNetToMediaIfIndex,
                                u4IpNetToMediaNetAddress, ARP_INVALID);
        }
    }
    u1EncapType = NetIpIfInfo.u1EncapType;

    if (u1EncapType == ARP_ENET_AUTO_ENCAP)
    {
        u1EncapType = ARP_ENET_V2_ENCAP;
    }

    tArpData.i2Hardware = i2Hardware;
    tArpData.u4IpAddr = u4IpNetToMediaNetAddress;
    tArpData.u2Port = (UINT2) u4Port;
    tArpData.i1Hwalen = (INT1) pSetValIpNetToMediaPhysAddress->i4_Length,
        MEMCPY (tArpData.i1Hw_addr,
                (INT1 *) pSetValIpNetToMediaPhysAddress->pu1_OctetList,
                tArpData.i1Hwalen);
    tArpData.u1EncapType = u1EncapType;
    tArpData.i1State = (INT1) ARP_STATIC;
    tArpData.u1RowStatus = ACTIVE;

    if (ArpAddArpEntry (tArpData, NULL, 0) == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }

#ifdef LNXIP4_WANTED
    /* If the Interface OperStatus is down,dont add the static ARP 
     * Entry to Kernel. It will be added when the interface 
     * comes up */
    if (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
    {
        /* To add a static entry in the kernel arp cache */
        if (ArpUpdateKernelEntry (ARP_STATIC,
                                  pSetValIpNetToMediaPhysAddress->pu1_OctetList,
                                  pSetValIpNetToMediaPhysAddress->i4_Length,
                                  u4IpNetToMediaNetAddress,
                                  NetIpIfInfo.au1IfName) == ARP_FAILURE)
        {
            ArpDeleteArpCacheWithIndex (i4IpNetToMediaIfIndex,
                                        u4IpNetToMediaNetAddress);
            return SNMP_FAILURE;
        }
    }
#endif /* LNXIP4_WANTED */

    IncMsrForIpv4NetToPhyTable (i4IpNetToMediaIfIndex,
                                u4IpNetToMediaNetAddress,
                                's', pSetValIpNetToMediaPhysAddress,
                                FsMIStdIpNetToPhysicalPhysAddress,
                                (sizeof (FsMIStdIpNetToPhysicalPhysAddress) /
                                 sizeof (UINT4)), FALSE);

    /* For the NetIpToMediaTable, there is no separate rowstatus object.
     * When the physical address is configured, the rowstatus for the entry
     * is set to STATIC_NOT_IN_SERVICE. To make the entry rowstatus to active
     * a separate notification is sent for the object 
     * FsMIStdIpNetToPhysicalRowStatus */

    i4RSValue = ACTIVE;
    IncMsrForIpv4NetToPhyTable (i4IpNetToMediaIfIndex,
                                u4IpNetToMediaNetAddress,
                                'i', &i4RSValue,
                                FsMIStdIpNetToPhysicalRowStatus,
                                (sizeof (FsMIStdIpNetToPhysicalRowStatus) /
                                 sizeof (UINT4)), TRUE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIpNetToMediaType
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object
                setValIpNetToMediaType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetIpNetToMediaType (INT4 i4IpNetToMediaIfIndex,
                        UINT4 u4IpNetToMediaNetAddress,
                        INT4 i4SetValIpNetToMediaType)
#else
INT1
nmhSetIpNetToMediaType (i4IpNetToMediaIfIndex,
                        u4IpNetToMediaNetAddress, i4SetValIpNetToMediaType)
     INT4                i4IpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
     INT4                i4SetValIpNetToMediaType;
#endif
{
    tNetIpv4IfInfo      NetIpIfInfo;
    t_ARP_CACHE         ArpEntry;
    UINT4               u4Port;
    INT1                i1EntryType = (INT1) i4SetValIpNetToMediaType;

    /* As the ARP cache table indices are modified to use the ifIndex and
     * IpAddress, the nmhSet routine should be invoked only with a valid
     * ifIndex value, the CFA_INVALID_INDEX is not supported. Cli
     * should be modified to provide valid indices. */

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IpNetToMediaIfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (ArpLookupWithIndex
        ((UINT4) i4IpNetToMediaIfIndex, u4IpNetToMediaNetAddress,
         &ArpEntry) == ARP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* If dynamic entry exists, overwite with static entries */
    if ((i1EntryType == ARP_INVALID) ||
        ((ArpEntry.i1State != ARP_STATIC) && (ArpEntry.i1State != ARP_INVALID)))
    {
        /* If i1EntryType is set to INVALID, this function deletes the entry 
         * in user space ARP cache. */
        /* Or if the existing state is DYNAMIC or STATIC_NOT_IN_SERVICE 
         * modify the state */
        ArpModifyWithIndex ((UINT4) i4IpNetToMediaIfIndex,
                            u4IpNetToMediaNetAddress, i1EntryType);
    }

#ifdef LNXIP4_WANTED
    /* If the Interface OperStatus is down,No need to update to KERNEL.
     * Entry should have been removed from KERNEL */
    if (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
    {
        /* To update the entry in the kernel arp cache */
        if (ArpUpdateKernelEntry (i1EntryType, (UINT1 *) (ArpEntry.i1Hw_addr),
                                  ArpEntry.i1Hwalen,
                                  u4IpNetToMediaNetAddress,
                                  NetIpIfInfo.au1IfName) == ARP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    IncMsrForIpv4NetToPhyTable (i4IpNetToMediaIfIndex,
                                u4IpNetToMediaNetAddress,
                                'i', &i4SetValIpNetToMediaType,
                                FsMIStdIpNetToPhysicalType,
                                (sizeof (FsMIStdIpNetToPhysicalType) /
                                 sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpNetToMediaPhysAddress
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The Object
                testValIpNetToMediaPhysAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpNetToMediaPhysAddress (UINT4 *pu4ErrorCode,
                                  INT4 i4IpNetToMediaIfIndex,
                                  UINT4 u4IpNetToMediaNetAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIpNetToMediaPhysAddress)
#else
INT1
nmhTestv2IpNetToMediaPhysAddress (pu4ErrorCode, i4IpNetToMediaIfIndex,
                                  u4IpNetToMediaNetAddress,
                                  pTestValIpNetToMediaPhysAddress)
     UINT4              *pu4ErrorCode;
     INT4                i4IpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
     tSNMP_OCTET_STRING_TYPE *pTestValIpNetToMediaPhysAddress;
#endif
{
    tNetIpv4IfInfo      NetIpIfInfo;
    t_ARP_HW_TABLE     *pHw_entry = NULL;
    INT4                i4RetVal;
    UINT4               u4Port;
    INT2                i2Hardware;
    UINT4               u4NumEntries = IP_ZERO;
    UINT4               u4ContextId = 0;
    UINT1               au1MacAllZero[] = { IP_ZERO, IP_ZERO, IP_ZERO,
        IP_ZERO, IP_ZERO, IP_ZERO
    };

    u4NumEntries = ArpGetEntryCount ();

    if (u4NumEntries >= ARP_MAX_ARP_ENTRIES)
    {
        CLI_SET_ERR (CLI_IP_ARP_MAX_ENTRIES_ERR);
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        return SNMP_FAILURE;
    }

    i4RetVal =
        NetIpv4GetPortFromIfIndex ((UINT4) i4IpNetToMediaIfIndex, &u4Port);
    if (i4RetVal != NETIPV4_SUCCESS)
    {
        CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (u4IpNetToMediaNetAddress == 0)
    {
        CLI_SET_ERR (CLI_IP_INVALID_NEXTHOP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValIpNetToMediaPhysAddress == NULL)
    {
        CLI_SET_ERR (CLI_IP_INVALID_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) != NETIPV4_SUCCESS)
    {
        CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
        return SNMP_FAILURE;
    }

    /* Arp IP  Address should be in the same networks
       as of the Interfaces */
    if ((NetIpIfInfo.u4Addr & NetIpIfInfo.u4NetMask) !=
        (u4IpNetToMediaNetAddress & NetIpIfInfo.u4NetMask))
    {
        CLI_SET_ERR (CLI_IP_INVALID_NEXTHOP_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    u4ContextId = NetIpIfInfo.u4ContextId;

    if (NetIpv4IfIsOurAddressInCxt (u4ContextId,
                                    u4IpNetToMediaNetAddress) ==
        NETIPV4_SUCCESS)
    {
        CLI_SET_ERR (CLI_IP_LOCAL_IPADDR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i2Hardware = ARP_IFACE_TYPE ((UINT1) NetIpIfInfo.u4IfType);
    pHw_entry = arp_get_hw_entry (i2Hardware);

    if (pHw_entry == NULL)
    {
        CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pHw_entry->get_hw_address == NULL)
    {
        CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (((UINT1) NetIpIfInfo.u4IfType != CFA_ENET) &&
        ((UINT1) NetIpIfInfo.u4IfType != CFA_PSEUDO_WIRE)
#ifdef WGS_WANTED
        && ((UINT1) NetIpIfInfo.u4IfType != CFA_L2VLAN)
#else
        && ((UINT1) NetIpIfInfo.u4IfType != CFA_L3IPVLAN)
        && ((UINT1) NetIpIfInfo.u4IfType != CFA_L3SUB_INTF)
        && ((UINT1) NetIpIfInfo.u4IfType != CFA_LAGG)
#endif /* WGS_WANTED */
        )
    {
        CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    else if (pTestValIpNetToMediaPhysAddress->i4_Length != CFA_ENET_ADDR_LEN)
    {
        CLI_SET_ERR (CLI_IP_INVALID_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Check the the given Mac is all Zero. */
    if ((MEMCMP (&(au1MacAllZero),
                 pTestValIpNetToMediaPhysAddress->pu1_OctetList,
                 pTestValIpNetToMediaPhysAddress->i4_Length)) == IP_ZERO)
    {
        CLI_SET_ERR (CLI_IP_INVALID_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    if ((IP_IS_ENET_MAC_MCAST
         ((tEnetV2Header *) (VOID *) pTestValIpNetToMediaPhysAddress->
          pu1_OctetList))
        ||
        (IP_IS_ENET_MAC_BCAST
         ((tEnetV2Header *) (VOID *) pTestValIpNetToMediaPhysAddress->
          pu1_OctetList)))
    {
        CLI_SET_ERR (CLI_IP_INVALID_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpNetToMediaType
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress

                The AObject
                testValIpNetToMediaType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2IpNetToMediaType (UINT4 *pu4ErrorCode, INT4 i4IpNetToMediaIfIndex,
                           UINT4 u4IpNetToMediaNetAddress,
                           INT4 i4TestValIpNetToMediaType)
#else
INT1
nmhTestv2IpNetToMediaType (pu4ErrorCode, i4IpNetToMediaIfIndex,
                           u4IpNetToMediaNetAddress, i4TestValIpNetToMediaType)
     UINT4              *pu4ErrorCode;
     INT4                i4IpNetToMediaIfIndex;
     UINT4               u4IpNetToMediaNetAddress;
     INT4                i4TestValIpNetToMediaType;
#endif
{
    t_ARP_CACHE         ArpEntry;

    if ((i4TestValIpNetToMediaType != ARP_INVALID) &&
        (i4TestValIpNetToMediaType != ARP_STATIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ArpLookupWithIndex
        ((UINT4) i4IpNetToMediaIfIndex, u4IpNetToMediaNetAddress,
         &ArpEntry) == ARP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;    /* What to do with status alone */
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpNetToMediaTable
 Input       :  The Indices
                IpNetToMediaIfIndex
                IpNetToMediaNetAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpNetToMediaTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IpCidrRouteTable
 Input       :  The Indices
                IpCidrRouteDest
                IpCidrRouteMask
                IpCidrRouteTos
                IpCidrRouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IpCidrRouteTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* The followig Function are stdipvx.mib Lowlevel Mapping which is called 
 * form the netipvx module
 */
INT1
IPvxGetARPEntry (INT4 i4IPv4IfIndex, UINT4 u4Ipv4Addr, t_ARP_CACHE * pArpEntry)
{
    if (ArpLookupWithIndex ((UINT4) i4IPv4IfIndex, u4Ipv4Addr, pArpEntry)
        == ARP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIPv4NetToPhyLastUpdated 
 *
 *    DESCRIPTION      : This function get ARP Cache entry last updated time. 
 *
 *    INPUT            : i4Ipv4IfIndex              - IPv4 ifindex  
 *                     : pIpv4Addr                  - Ipv4 address         
 *                     : pu4IPv4NDCTblLastUpdated   - ptr to last updated time
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIPv4NetToPhyLastUpdated (INT4 i4IPv4IfIndex, UINT4 u4Ipv4Addr,
                                UINT4 *pu4IPv4ARPTblLastUpdated)
{
    t_ARP_CACHE         ArpEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ArpEntry, IP_ZERO, sizeof (t_ARP_CACHE));
    i1RetVal = IPvxGetARPEntry (i4IPv4IfIndex, u4Ipv4Addr, &ArpEntry);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    *pu4IPv4ARPTblLastUpdated = ArpEntry.u4LastUpdatedTime;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4NetToPhyRowStatus 
 *
 *    DESCRIPTION      : This function get ARP Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv4IfIndex          - IPv4 ifindex  
 *                     : pIpv4Addr              - Ipv4 address         
 *                     : pi4Ipv4GetRowStatus    - ptr to entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4NetToPhyRowStatus (INT4 i4Ipv4IfIndex, UINT4 u4Ipv4Addr,
                              INT4 *pi4Ipv4GetRowStatus)
{
    t_ARP_CACHE         ArpEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ArpEntry, IP_ZERO, sizeof (t_ARP_CACHE));
    i1RetVal = IPvxGetARPEntry (i4Ipv4IfIndex, u4Ipv4Addr, &ArpEntry);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    *pi4Ipv4GetRowStatus = ArpEntry.u1RowStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIPv4NetToPhyPhyAddr        
 *
 *    DESCRIPTION      : This function set ARP Cache entry type . 
 *
 *    INPUT            : i4Ipv4IfIndex                - IPv4 ifindex  
 *                     : pIpv4Addr                    - Ipv4 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIPv4NetToPhyPhyAddr (INT4 i4Ipv4IfIndex, UINT4 u4Ipv4Addr,
                            tSNMP_OCTET_STRING_TYPE * pPhysAddr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    tARP_DATA           tArpData;
    t_ARP_CACHE         ArpEntry;
    INT2                i2Hardware = IP_ZERO;
    UINT4               u4Port = IP_ZERO;
    UINT1               u1EncapType = IP_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4Ipv4IfIndex, &u4Port) ==
        NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&(NetIpIfInfo), IP_ZERO, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    i2Hardware = ARP_IFACE_TYPE ((UINT1) NetIpIfInfo.u4IfType);

    MEMSET (&tArpData, IP_ZERO, sizeof (tARP_DATA));
    MEMSET (&ArpEntry, IP_ZERO, sizeof (t_ARP_CACHE));

    i1RetVal =
        (INT1) ArpLookupWithIndex ((UINT4) i4Ipv4IfIndex, u4Ipv4Addr,
                                   &ArpEntry);
    if (i1RetVal == ARP_FAILURE)
    {
        /* Read Create : Add ARP Entry with RowStatus as NOT_IN_SERVICE */
        u1EncapType = NetIpIfInfo.u1EncapType;
        if (u1EncapType == ARP_ENET_AUTO_ENCAP)
        {
            u1EncapType = ARP_ENET_V2_ENCAP;
        }
        tArpData.i2Hardware = i2Hardware;
        tArpData.u4IpAddr = u4Ipv4Addr;
        tArpData.u2Port = (UINT2) u4Port;
        tArpData.u1RowStatus = NOT_IN_SERVICE;
        tArpData.u1EncapType = u1EncapType;
        tArpData.i1State = (INT1) ARP_STATIC_NOT_IN_SERVICE;
        tArpData.i1Hwalen = (INT1) pPhysAddr->i4_Length;
        MEMCPY (tArpData.i1Hw_addr, (INT1 *) pPhysAddr->pu1_OctetList,
                tArpData.i1Hwalen);

        i1RetVal = (INT1) ArpAddArpEntry (tArpData, NULL, 0);
        if (i1RetVal == ARP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        return (SNMP_SUCCESS);
    }

    if ((ArpEntry.i1State != (INT1) ARP_STATIC) &&
        (ArpEntry.i1State != ARP_STATIC_NOT_IN_SERVICE) &&
        (ArpEntry.i1State != ARP_INVALID))
    {
        /* Remove old dynamic entry and add new static entry */
        i1RetVal =
            (INT1) ArpModifyWithIndex ((UINT4) i4Ipv4IfIndex, u4Ipv4Addr,
                                       ARP_INVALID);
        if (i1RetVal == ARP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

    u1EncapType = NetIpIfInfo.u1EncapType;

    if (u1EncapType == ARP_ENET_AUTO_ENCAP)
    {
        u1EncapType = ARP_ENET_V2_ENCAP;
    }
    tArpData.i2Hardware = i2Hardware;
    tArpData.u4IpAddr = u4Ipv4Addr;
    tArpData.u2Port = (UINT2) u4Port;
    tArpData.u1EncapType = u1EncapType;

    if (ArpEntry.u1RowStatus == ACTIVE)
    {
        /* Dynamic entry update */
        tArpData.u1RowStatus = ACTIVE;
        tArpData.i1State = (INT1) ARP_STATIC;
    }
    else
    {
        /* CreateAndWait to Mac Address assignment / Mac update */
        tArpData.u1RowStatus = NOT_IN_SERVICE;
        tArpData.i1State = (INT1) ARP_STATIC_NOT_IN_SERVICE;
    }

    tArpData.i1Hwalen = (INT1) pPhysAddr->i4_Length;
    MEMCPY (tArpData.i1Hw_addr, (INT1 *) pPhysAddr->pu1_OctetList,
            tArpData.i1Hwalen);

    i1RetVal = (INT1) ArpAddArpEntry (tArpData, NULL, 0);
    if (i1RetVal == ARP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

#ifdef LNXIP4_WANTED
    /* If the Interface OperStatus is down,dont add the static ARP 
     * Entry to Kernel. It will be added when the interface 
     * comes up */
    if ((NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE) &&
        (tArpData.i1State == ARP_STATIC))
    {
        /* To add a static entry in the kernel arp cache */
        if (ArpUpdateKernelEntry (ARP_STATIC,
                                  pPhysAddr->pu1_OctetList,
                                  pPhysAddr->i4_Length,
                                  u4Ipv4Addr,
                                  NetIpIfInfo.au1IfName) == ARP_FAILURE)
        {
            ArpDeleteArpCacheWithIndex (i4Ipv4IfIndex, u4Ipv4Addr);
            return SNMP_FAILURE;
        }
    }
#endif /* LNXIP4_WANTED */

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIPv4NetToPhyType        
 *
 *    DESCRIPTION      : This function set ARP Cache entry type . 
 *
 *    INPUT            : i4Ipv4IfIndex                - IPv4 ifindex  
 *                     : pIpv4Addr                    - Ipv4 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIPv4NetToPhyType (INT4 i4Ipv4IfIndex, UINT4 u4Ipv4Addr,
                         INT4 i4TestValIpNetToPhysicalType)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    t_ARP_CACHE         ArpEntry;
    tARP_DATA           tArpData;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4Port = IP_ZERO;
    UINT4               u4IfType = IP_ZERO;
    INT2                i2Hardware = IP_ZERO;
    UINT1               u1EncapType = IP_ZERO;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4Ipv4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&(NetIpIfInfo), IP_ZERO, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    u4IfType = NetIpIfInfo.u4IfType;

    i2Hardware = ARP_IFACE_TYPE ((UINT1) u4IfType);

    MEMSET (&(tArpData), IP_ZERO, sizeof (tARP_DATA));
    MEMSET (&ArpEntry, IP_ZERO, sizeof (t_ARP_CACHE));

    i1RetVal =
        (INT1) ArpLookupWithIndex ((UINT4) i4Ipv4IfIndex, u4Ipv4Addr,
                                   &ArpEntry);
    if (i1RetVal == ARP_FAILURE)
    {
        /* Read Create Support */
        if (i4TestValIpNetToPhysicalType == IPVX_NET_TO_PHY_TYPE_STATIC)
        {
            u1EncapType = NetIpIfInfo.u1EncapType;

            if (u1EncapType == ARP_ENET_AUTO_ENCAP)
            {
                u1EncapType = ARP_ENET_V2_ENCAP;
            }
            tArpData.i2Hardware = i2Hardware;
            tArpData.u4IpAddr = u4Ipv4Addr;
            tArpData.u2Port = (UINT2) u4Port;
            tArpData.u1RowStatus = NOT_READY;
            tArpData.u1EncapType = u1EncapType;
            tArpData.i1State = (INT1) ARP_STATIC_NOT_IN_SERVICE;
            /* Mac Addr Null */

            i1RetVal = (INT1) ArpAddArpEntry (tArpData, NULL, 0);
            if (i1RetVal == ARP_SUCCESS)
            {
                return (SNMP_SUCCESS);
            }
        }
        return (SNMP_FAILURE);
    }

    /* If the options is delete */
    if (i4TestValIpNetToPhysicalType == IPVX_NET_TO_PHY_TYPE_INVALID)
    {
        i1RetVal = (INT1) ArpModifyWithIndex ((UINT4) i4Ipv4IfIndex, u4Ipv4Addr,
                                              ARP_INVALID);
        if (i1RetVal == ARP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        return (SNMP_SUCCESS);
    }

    if ((ArpEntry.i1State != ARP_STATIC) && (ArpEntry.i1State != ARP_INVALID)
        && (ArpEntry.i1State != ARP_STATIC_NOT_IN_SERVICE))
    {
        /* If dynamic entry exists, overwite with static entries */
        u1EncapType = NetIpIfInfo.u1EncapType;
        if (u1EncapType == ARP_ENET_AUTO_ENCAP)
        {
            u1EncapType = ARP_ENET_V2_ENCAP;
        }

        tArpData.i2Hardware = i2Hardware;
        tArpData.u4IpAddr = u4Ipv4Addr;
        tArpData.u2Port = (UINT2) u4Port;
        tArpData.u1RowStatus = ACTIVE;
        tArpData.u1EncapType = u1EncapType;
        tArpData.i1State = (INT1) ARP_STATIC;
        tArpData.i1Hwalen = ArpEntry.i1Hwalen;
        MEMCPY (&(tArpData.i1Hw_addr), &(ArpEntry.i1Hw_addr),
                tArpData.i1Hwalen);

        i1RetVal = (INT1) ArpAddArpEntry (tArpData, NULL, 0);
        if (i1RetVal == ARP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

#ifdef LNXIP4_WANTED
    if ((ArpEntry.i1State == ARP_STATIC) && (ArpEntry.i1State == ARP_INVALID)
        && (ArpEntry.i1State == ARP_STATIC_NOT_IN_SERVICE))
    {
        return (SNMP_SUCCESS);
    }

    /* If the Interface OperStatus is down,No need to update to KERNEL.
     * Entry should have been removed from KERNEL */
    if (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
    {
        /* To update the entry in the kernel arp cache */
        if (ArpUpdateKernelEntry (ARP_STATIC, (UINT1 *) (ArpEntry.i1Hw_addr),
                                  ArpEntry.i1Hwalen,
                                  u4Ipv4Addr,
                                  NetIpIfInfo.au1IfName) == ARP_FAILURE)
        {
            ArpDeleteArpCacheWithIndex (i4Ipv4IfIndex, u4Ipv4Addr);
            return (SNMP_FAILURE);
        }
    }
#endif

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIpv4NetToPhyRowStatus 
 *
 *    DESCRIPTION      : This function set ARP Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv4IfIndex          - IPv4 ifindex  
 *                     : pIpv4Addr              - Ipv4 address         
 *                     : i4Ipv4SetRowStatus     - entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIpv4NetToPhyRowStatus (INT4 i4Ipv4IfIndex, UINT4 u4Ipv4Addr,
                              INT4 i4Ipv4SetRowStatus)
{

    tNetIpv4IfInfo      NetIpIfInfo;
    t_ARP_CACHE         ArpEntry;
    tARP_DATA           tArpData;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4Port = IP_ZERO;
    UINT4               u4IfType = IP_ZERO;
    INT2                i2Hardware = IP_ZERO;
    UINT1               u1EncapType = IP_ZERO;

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4Ipv4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&(NetIpIfInfo), IP_ZERO, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    u4IfType = NetIpIfInfo.u4IfType;

    i2Hardware = ARP_IFACE_TYPE ((UINT1) u4IfType);

    MEMSET (&(tArpData), IP_ZERO, sizeof (tARP_DATA));
    MEMSET (&ArpEntry, IP_ZERO, sizeof (t_ARP_CACHE));

    i1RetVal = (INT1) ArpLookupWithIndex ((UINT4) i4Ipv4IfIndex, u4Ipv4Addr,
                                          &(ArpEntry));
    if (i1RetVal == ARP_FAILURE)
    {
        if (i4Ipv4SetRowStatus != CREATE_AND_WAIT)
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        /* Entry found in the Table */
        if (ArpEntry.u1RowStatus == i4Ipv4SetRowStatus)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4Ipv4SetRowStatus)
    {
            /* Modify an Entry */
        case ACTIVE:
            /* 1. Set the Entry as ARP_STATIC */
            u1EncapType = NetIpIfInfo.u1EncapType;
            if (u1EncapType == ARP_ENET_AUTO_ENCAP)
            {
                u1EncapType = ARP_ENET_V2_ENCAP;
            }
            tArpData.i2Hardware = i2Hardware;
            tArpData.u4IpAddr = u4Ipv4Addr;
            tArpData.u2Port = (UINT2) u4Port;
            tArpData.u1RowStatus = ACTIVE;
            tArpData.u1EncapType = u1EncapType;
            tArpData.i1State = (INT1) ARP_STATIC;
            tArpData.i1Hwalen = ArpEntry.i1Hwalen;
            MEMCPY (&(tArpData.i1Hw_addr), &(ArpEntry.i1Hw_addr),
                    tArpData.i1Hwalen);

            i1RetVal = (INT1) ArpAddArpEntry (tArpData, NULL, 0);
            if (i1RetVal == ARP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
#ifdef LNXIP4_WANTED
            /* If the Interface OperStatus is down,dont add the static ARP 
             * Entry to Kernel. It will be added when the interface 
             * comes up */
            if (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
            {
                /* To add a static entry in the kernel arp cache */
                if (ArpUpdateKernelEntry (ARP_STATIC,
                                          (UINT1 *) (ArpEntry.i1Hw_addr),
                                          ArpEntry.i1Hwalen,
                                          u4Ipv4Addr,
                                          NetIpIfInfo.au1IfName) == ARP_FAILURE)
                {
                    ArpDeleteArpCacheWithIndex (i4Ipv4IfIndex, u4Ipv4Addr);
                    return SNMP_FAILURE;
                }
            }
#endif /* LNXIP4_WANTED */
            break;

        case NOT_IN_SERVICE:
            /* 1. Set the Entry as ARP_STATIC_NOT_IN_SERVICE */
            u1EncapType = NetIpIfInfo.u1EncapType;
            if (u1EncapType == ARP_ENET_AUTO_ENCAP)
            {
                u1EncapType = ARP_ENET_V2_ENCAP;
            }
            tArpData.i2Hardware = i2Hardware;
            tArpData.u4IpAddr = u4Ipv4Addr;
            tArpData.u2Port = (UINT2) u4Port;
            tArpData.u1RowStatus = NOT_IN_SERVICE;
            tArpData.u1EncapType = u1EncapType;
            tArpData.i1State = (INT1) ARP_STATIC_NOT_IN_SERVICE;
            tArpData.i1Hwalen = ArpEntry.i1Hwalen;
            MEMCPY (&(tArpData.i1Hw_addr), &(ArpEntry.i1Hw_addr),
                    tArpData.i1Hwalen);

            i1RetVal = (INT1) ArpAddArpEntry (tArpData, NULL, 0);
            if (i1RetVal == ARP_FAILURE)
            {
                return (SNMP_FAILURE);
            }

#ifdef LNXIP4_WANTED
            /* If the Interface OperStatus is down,dont add the static ARP 
             * Entry to Kernel. It will be added when the interface 
             * comes up */
            if (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
            {
                /* To add a static entry in the kernel arp cache */
                if (ArpUpdateKernelEntry (ARP_INVALID,
                                          (UINT1 *) (ArpEntry.i1Hw_addr),
                                          ArpEntry.i1Hwalen,
                                          u4Ipv4Addr,
                                          NetIpIfInfo.au1IfName) == ARP_FAILURE)
                {
                    ArpDeleteArpCacheWithIndex (i4Ipv4IfIndex, u4Ipv4Addr);
                    return (SNMP_FAILURE);
                }
            }
#endif /* LNXIP4_WANTED */
            break;

        case CREATE_AND_WAIT:
            /* 1.Create an entry with ARPState as ARP_STATIC_NOT_IN_SERVICE
             *  -- staticaly configured entry which is not used = 
             *  ARP_STATIC_NOT_IN_SERVICE 
             */
            u1EncapType = NetIpIfInfo.u1EncapType;
            if (u1EncapType == ARP_ENET_AUTO_ENCAP)
            {
                u1EncapType = ARP_ENET_V2_ENCAP;
            }
            tArpData.i2Hardware = i2Hardware;
            tArpData.u4IpAddr = u4Ipv4Addr;
            tArpData.u2Port = (UINT2) u4Port;
            tArpData.u1RowStatus = NOT_READY;
            tArpData.u1EncapType = u1EncapType;
            tArpData.i1State = (INT1) ARP_STATIC_NOT_IN_SERVICE;
            /* Mac Addr Null */

            i1RetVal = (INT1) ArpAddArpEntry (tArpData, NULL, 0);
            if (i1RetVal == ARP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:
            /* In this function LnxIp Entry also will be deleted */
            i1RetVal =
                (INT1) ArpModifyWithIndex ((UINT4) i4Ipv4IfIndex, u4Ipv4Addr,
                                           ARP_INVALID);
            if (i1RetVal == ARP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
            break;

        case CREATE_AND_GO:
            return (SNMP_FAILURE);

        default:
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxTestIPv4NetToPhyType       
 *
 *    DESCRIPTION      : This function test ARP Cache entry type . 
 *
 *    INPUT            : pu4ErrorCode                 - ptr to error code
 *                     : i4Ipv4IfIndex                - IPv4 ifindex  
 *                     : pIpv4Addr                    - Ipv4 address         
 *                     : i4TestValIpNetToPhysicalType - Entry type 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxTestIPv4NetToPhyType (UINT4 *pu4ErrorCode, INT4 i4Ipv4IfIndex,
                          UINT4 u4Ipv4Addr, INT4 i4TestValIpNetToPhysicalType)
{
    t_ARP_CACHE         ArpEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4TestValIpNetToPhysicalType != IPVX_NET_TO_PHY_TYPE_INVALID) &&
        (i4TestValIpNetToPhysicalType != IPVX_NET_TO_PHY_TYPE_STATIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    MEMSET (&ArpEntry, IP_ZERO, sizeof (t_ARP_CACHE));
    /* Entry not present; cann't set INVALID */
    i1RetVal = IPvxGetARPEntry (i4Ipv4IfIndex, u4Ipv4Addr, &ArpEntry);
    if (i1RetVal == SNMP_FAILURE)
    {
        if (i4TestValIpNetToPhysicalType == IPVX_NET_TO_PHY_TYPE_INVALID)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxTestIpv4NetToPhyRowStatus
 *
 *    DESCRIPTION      : This function test ARP Cache entry Rowstatus. 
 *
 *    INPUT            : i4Ipv4IfIndex          - IPv4 ifindex  
 *                     : pIpv4Addr              - Ipv4 address         
 *                     : i4Ipv4TestRowStatus    - entry rowstatus
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxTestIpv4NetToPhyRowStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv4IfIndex,
                               UINT4 u4Ipv4Addr, INT4 i4Ipv4TestRowStatus)
{
    t_ARP_CACHE         ArpEntry;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4NumEntries = IP_ZERO;

    MEMSET (&ArpEntry, IP_ZERO, sizeof (t_ARP_CACHE));
    i1RetVal = IPvxGetARPEntry (i4Ipv4IfIndex, u4Ipv4Addr, &ArpEntry);
    if (i1RetVal == SNMP_FAILURE)
    {
        /* Entry Not Found In The Table */
        switch (i4Ipv4TestRowStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);

            case DESTROY:
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);

            case CREATE_AND_WAIT:

                u4NumEntries = ArpGetEntryCount ();

                if (u4NumEntries >= ARP_MAX_ARP_ENTRIES)
                {
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    return (SNMP_FAILURE);
                }
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4Ipv4TestRowStatus)
        {
            case ACTIVE:
                if ((ArpEntry.u1RowStatus == NOT_READY) ||
                    (ArpEntry.i1Hwalen == IP_ZERO))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case NOT_IN_SERVICE:
                /* Static entries are can be cofigured as NOT_IN_SERVICE */
                if ((ArpEntry.u1RowStatus == NOT_READY) ||
                    ((ArpEntry.i1State != ARP_STATIC_NOT_IN_SERVICE) &&
                     (ArpEntry.i1State != ARP_INVALID) &&
                     (ArpEntry.i1State != ARP_STATIC)))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }                        /* End of switch */
    }                            /* End of if */

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *  Function    :  IncMsrForIpv4NetToPhyTable    
 *
 *   Input       :  i4IfIndex   - The interface index where the entry is added
 *                  u4IpAddress - IP address of the cache entry
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM 
 *                 for the NetToMedia Table.
 * 
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 * 
 * ****************************************************************************/

PRIVATE VOID
IncMsrForIpv4NetToPhyTable (INT4 i4IfIndex, UINT4 u4IpAddress, CHR1 cDatatype,
                            VOID *pSetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                            UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE *pSetString = NULL;
    tSNMP_OCTET_STRING_TYPE IpAddress;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1IpAddr[MAX_ADDR_LEN];

    MEMSET (au1IpAddr, 0, MAX_ADDR_LEN);
    IpAddress.pu1_OctetList = au1IpAddr;
    u4IpAddress = OSIX_HTONL (u4IpAddress);
    MEMSET (IpAddress.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (IpAddress.pu1_OctetList, &u4IpAddress, (sizeof (u4IpAddress)));
    IpAddress.i4_Length = sizeof (u4IpAddress);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = ArpProtocolLock;
    SnmpNotifyInfo.pUnLockPointer = ArpProtocolUnLock;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i",
                              i4IfIndex, INET_ADDR_TYPE_IPV4,
                              &IpAddress, i4SetValue));
            break;
        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %s",
                              i4IfIndex, INET_ADDR_TYPE_IPV4,
                              &IpAddress, pSetString));
            break;
        default:
            break;
    }

#ifndef ISS_WANTED
    /* For Stack compilation warnings removal */
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (IpAddress);
#endif
    return;
}

/****************************************************************************
 *  Function    :  IncMsrForIpv4RouteTable    
 *
 *   Input       :  i4IfIndex   - The interface index where the entry is added
 *                  u4IpAddress - IP address of the cache entry
 *                  cDataType   - Datatype of the configured value.
 *                  pSetVal     - Configured value that has to be notified.
 *                  pu4ObjectId - Object ID.
 *                  u4OIDLen    - Object Id length.
 *                  IsRowStatus - Rowstatus (TRUE/FALSE)
 *
 * Description :  This function sends notifications to MSR and RM 
 *                 for the NetToMedia Table.
 * 
 * Output      :  None.
 *
 * Returns     :  IPVX_SUCCESS or IPVX_FAILURE
 * 
 * ****************************************************************************/

PRIVATE VOID
IncMsrForIpv4RouteTable (UINT4 u4ContextId, UINT4 u4IpCidrRouteDest,
                         UINT4 u4IpCidrRouteMask, UINT4 u4IpCidrRouteNextHop,
                         INT4 i4SetVal, UINT4 *pu4ObjectId,
                         UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    UINT4               u4Policy[IPVX_TWO];
    INT4                i4PrefixLen;
    tSNMP_OCTET_STRING_TYPE Dest;
    tSNMP_OCTET_STRING_TYPE NextHop;
    tSNMP_OID_TYPE      RoutePolicy, *pRoutePolicy = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT1               au1Dest[MAX_ADDR_LEN];
    UINT1               au1NextHop[MAX_ADDR_LEN];

    RTM_PROT_UNLOCK ();
    IpvxUnLock ();
    MEMSET (au1Dest, 0, MAX_ADDR_LEN);
    MEMSET (au1NextHop, 0, MAX_ADDR_LEN);
    Dest.pu1_OctetList = au1Dest;
    NextHop.pu1_OctetList = au1NextHop;
    u4IpCidrRouteDest = OSIX_HTONL (u4IpCidrRouteDest);
    MEMSET (Dest.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (Dest.pu1_OctetList, &u4IpCidrRouteDest,
            (sizeof (u4IpCidrRouteDest)));
    Dest.i4_Length = sizeof (u4IpCidrRouteDest);

    u4IpCidrRouteNextHop = OSIX_HTONL (u4IpCidrRouteNextHop);
    MEMSET (NextHop.pu1_OctetList, 0, IP_ADDR);
    MEMCPY (NextHop.pu1_OctetList, &u4IpCidrRouteNextHop,
            (sizeof (u4IpCidrRouteNextHop)));
    NextHop.i4_Length = sizeof (u4IpCidrRouteNextHop);

    RoutePolicy.pu4_OidList = u4Policy;
    pRoutePolicy = &RoutePolicy;
    IPVX_GET_NULL_ROUTE_POLICY (pRoutePolicy);
    i4PrefixLen = CfaGetCidrSubnetMaskIndex (u4IpCidrRouteMask);

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_SEVEN;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u %o %i %s %i",
                      (INT4) u4ContextId, INET_ADDR_TYPE_IPV4,
                      &Dest, i4PrefixLen, pRoutePolicy, INET_ADDR_TYPE_IPV4,
                      &NextHop, i4SetVal));
    IpvxLock ();
    RTM_PROT_LOCK ();
#ifndef ISS_WANTED
    /* For Stack compilation warnings removal */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4SetVal);
#endif

    return;
}

/*******************************************************************************
 * Function Name   : RtmUtilCheckForDefRouteWithNoNextHop
 * Description     : An util to check for default route with no next hop 
 *                 exists in the route table
 * Input           : pInRtInfo - route for verification
 * Output          : returns IP_SUCCESS if the pInRtInfo is default.
 *                     returns IP_FAILURE otherwise.
 *
 * Returns         : IP_SUCCESS or IP_FAILURE
 *******************************************************************************/

INT4
RtmUtilCheckForDefRouteWithNoNextHop (tRtInfoQueryMsg * pRtQuery)
{

    INT4                i4Cnt = 0;
    INT4                i1RetVal = 0;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *paAppSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    PRIVATE tRtInfo     aAppSpecInfo[MAX_ROUTING_PROTOCOLS + 1];

    MEMSET (aAppSpecInfo, 0, ((MAX_ROUTING_PROTOCOLS + 1) *
                              (sizeof (tRtInfo))));

    for (i4Cnt = 0; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
    {
        paAppSpecInfo[i4Cnt] = (tRtInfo *) & aAppSpecInfo[i4Cnt];
    }
    i1RetVal = (INT1) IpForwardingTableGetRouteInfoInCxt (pRtQuery->u4ContextId,
                                                          pRtQuery->
                                                          u4DestinationIpAddress,
                                                          pRtQuery->
                                                          u4DestinationSubnetMask,
                                                          ALL_ROUTING_PROTOCOL,
                                                          (tRtInfo **) &
                                                          paAppSpecInfo[0]);
    if (i1RetVal == IP_SUCCESS)
    {
        for (i4Cnt = 1; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
        {
            pTmpRt = paAppSpecInfo[i4Cnt];
            while (pTmpRt != NULL)
            {
                if ((pTmpRt->u4NextHop == 0) &&
                    (pTmpRt->u4DestNet == 0) &&
                    (pTmpRt->u4DestMask == 0) &&
                    (pTmpRt->u4Tos == 0) && (pTmpRt->u2RtType != 0))
                {
                    return (IP_SUCCESS);
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }
        }
    }
    return (IP_FAILURE);
}

#endif
