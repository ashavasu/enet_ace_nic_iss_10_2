#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "arp.h"
#include "fssnmp.h"
#include "stdipwr.h"
#include "stdiplow.h"
#include "stdipdb.h"

    /*
     * Remove this #ifdef when deleting the deprecated objects at 
     * standard ip mib to support backward comp.
     * This RegisterSTDIP is replaced by RegisterSTDIPvx in futrure/netipvx 
     */
#ifdef REMOVE_ON_STDIP_DEPR_OBJ_DELETE

VOID
RegisterSTDIP ()
{
    SNMPRegisterMib (&stdipOID, &stdipEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdipOID, (const UINT1 *) "STDIP");
}

INT4
IpForwardingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2IpForwarding (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpForwardingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetIpForwarding (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpForwardingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpForwarding (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpDefaultTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2IpDefaultTTL (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpDefaultTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetIpDefaultTTL (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpDefaultTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpDefaultTTL (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpInReceivesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpInReceives (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpInHdrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpInHdrErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpInAddrErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpInAddrErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpForwDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpForwDatagrams (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpInUnknownProtosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpInUnknownProtos (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpInDiscards (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpInDeliversGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpInDelivers (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpOutRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpOutRequests (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpOutDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpOutDiscards (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpOutNoRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpOutNoRoutes (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpReasmTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpReasmTimeout (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpReasmReqdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpReasmReqds (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpReasmOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpReasmOKs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpReasmFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpReasmFails (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpFragOKsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpFragOKs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpFragFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpFragFails (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpFragCreatesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpFragCreates (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpAdEntAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpAdEntIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpAdEntIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpAdEntNetMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpAdEntNetMask (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpAdEntBcastAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpAdEntBcastAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpAdEntReasmMaxSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpAddrTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpAdEntReasmMaxSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpNetToMediaIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    ARP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpNetToMediaPhysAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2IpNetToMediaPhysAddress (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->pOctetStrValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpNetToMediaPhysAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhSetIpNetToMediaPhysAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->pOctetStrValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpNetToMediaPhysAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetIpNetToMediaPhysAddress (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->pOctetStrValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpNetToMediaNetAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    ARP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpNetToMediaTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2IpNetToMediaType (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpNetToMediaTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetIpNetToMediaType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpNetToMediaTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpNetToMediaTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpNetToMediaType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpRoutingDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIpRoutingDiscards (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    RTM_PROT_LOCK ();

    i4RetVal = nmhGetIpCidrRouteNumber (&pMultiData->u4_ULongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpCidrRouteMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpCidrRouteTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpCidrRouteNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[3].u4_ULongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpCidrRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteIfIndex (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteProto (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteAge (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteInfoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteInfo (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->pOidValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteInfoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->pOidValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->pOidValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteNextHopASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteNextHopAS (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[3].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteNextHopASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteNextHopAS (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteNextHopASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteNextHopAS (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric1Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteMetric1 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric1Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteMetric1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteMetric1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric2Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteMetric2 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric2Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteMetric2 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric2Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteMetric2 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric3Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteMetric3 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric3Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteMetric3 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteMetric3 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric4Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteMetric4 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric4Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteMetric4 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric4Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteMetric4 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric5Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteMetric5 (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric5Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteMetric5 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteMetric5Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteMetric5 (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2IpCidrRouteStatus (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetIpCidrRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceIpCidrRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetIpCidrRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &pMultiData->i4_SLongValue);
    RTM_PROT_UNLOCK ();

    return i4RetVal;
}

INT4
IcmpInMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInMsgs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInDestUnreachsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInDestUnreachs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInTimeExcdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInTimeExcds (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInParmProbsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInParmProbs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInSrcQuenchsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInSrcQuenchs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInRedirects (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInEchosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInEchos (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInEchoRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInEchoReps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInTimestampsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInTimestamps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInTimestampRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInTimestampReps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInAddrMasksGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInAddrMasks (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpInAddrMaskRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpInAddrMaskReps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutMsgsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutMsgs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutDestUnreachsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutDestUnreachs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutTimeExcdsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutTimeExcds (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutParmProbsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutParmProbs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutSrcQuenchsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutSrcQuenchs (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutRedirectsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutRedirects (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutEchosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutEchos (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutEchoRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutEchoReps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutTimestampsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutTimestamps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutTimestampRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutTimestampReps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutAddrMasksGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutAddrMasks (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IcmpOutAddrMaskRepsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetIcmpOutAddrMaskReps (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

/*
* Remove the code placed under #ifdef when deleting 
* the deprecated objects of standard IP mib.
* This code is provided to support backward comp.*/

#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT4
UdpInDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    UDP_PROT_LOCK ();

    i4RetVal = nmhGetUdpInDatagrams (&pMultiData->u4_ULongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
UdpNoPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    UDP_PROT_LOCK ();

    i4RetVal = nmhGetUdpNoPorts (&pMultiData->u4_ULongValue);
    UDP_PROT_UNLOCK ();

    return i4RetVal;
}

INT4
UdpInErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    UDP_PROT_LOCK ();

    i4RetVal = nmhGetUdpInErrors (&pMultiData->u4_ULongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
UdpOutDatagramsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    UDP_PROT_LOCK ();

    i4RetVal = nmhGetUdpOutDatagrams (&pMultiData->u4_ULongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}
#endif
INT4
UdpLocalAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceUdpTable (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue)
        == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
UdpLocalPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceUdpTable (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue)
        == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexUdpTable (tSnmpIndex * pFirstMultiIndex,
                      tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4udpLocalAddress;
    INT4                i4udpLocalPort;
    UDP_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexUdpTable (&u4udpLocalAddress,
                                      &i4udpLocalPort) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexUdpTable (pFirstMultiIndex->pIndex[0].u4_ULongValue,
                                     &u4udpLocalAddress,
                                     pFirstMultiIndex->pIndex[1].i4_SLongValue,
                                     &i4udpLocalPort) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4udpLocalAddress;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4udpLocalPort;

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpCidrRouteTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4ipCidrRouteDest;
    UINT4               u4ipCidrRouteMask;
    INT4                i4ipCidrRouteTos;
    UINT4               u4ipCidrRouteNextHop;

    RTM_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpCidrRouteTable (&u4ipCidrRouteDest,
                                              &u4ipCidrRouteMask,
                                              &i4ipCidrRouteTos,
                                              &u4ipCidrRouteNextHop)
            == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpCidrRouteTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4ipCidrRouteDest,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4ipCidrRouteMask,
             pFirstMultiIndex->pIndex[2].i4_SLongValue, &i4ipCidrRouteTos,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &u4ipCidrRouteNextHop) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4ipCidrRouteDest;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4ipCidrRouteMask;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4ipCidrRouteTos;
    pNextMultiIndex->pIndex[3].u4_ULongValue = u4ipCidrRouteNextHop;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpNetToMediaTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    INT4                i4ipNetToMediaIfIndex;
    UINT4               u4ipNetToMediaNetAddress;
    ARP_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpNetToMediaTable (&i4ipNetToMediaIfIndex,
                                               &u4ipNetToMediaNetAddress)
            == SNMP_FAILURE)
        {
            ARP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpNetToMediaTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4ipNetToMediaIfIndex,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4ipNetToMediaNetAddress) == SNMP_FAILURE)
        {
            ARP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4ipNetToMediaIfIndex;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4ipNetToMediaNetAddress;

    ARP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIpAddrTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4ipAdEntAddr;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIpAddrTable (&u4ipAdEntAddr) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIpAddrTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4ipAdEntAddr) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4ipAdEntAddr;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
IpForwardingDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2IpForwarding (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpDefaultTTLDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2IpDefaultTTL (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpNetToMediaTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhDepv2IpNetToMediaTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
IpCidrRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal =
        nmhDepv2IpCidrRouteTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

#endif /* REMOVE_ON_STDIP_DEPR_OBJ_DELETE */
