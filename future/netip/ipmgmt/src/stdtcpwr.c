#include "lr.h"
#include "fssnmp.h"
#include "stdtcpwr.h"
#include "stdtclow.h"
#include "stdtcpdb.h"

VOID
RegisterSTDTCP ()
{
    SNMPRegisterMib (&stdtcpOID, &stdtcpEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdtcpOID, (const UINT1 *) "stdtcp");
}

INT4
TcpRtoAlgorithmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRtoAlgorithm (&pMultiData->i4_SLongValue));
}

INT4
TcpRtoMinGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRtoMin (&pMultiData->i4_SLongValue));
}

INT4
TcpRtoMaxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRtoMax (&pMultiData->i4_SLongValue));
}

INT4
TcpMaxConnGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpMaxConn (&pMultiData->i4_SLongValue));
}

INT4
TcpActiveOpensGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpActiveOpens (&pMultiData->u4_ULongValue));
}

INT4
TcpPassiveOpensGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpPassiveOpens (&pMultiData->u4_ULongValue));
}

INT4
TcpAttemptFailsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpAttemptFails (&pMultiData->u4_ULongValue));
}

INT4
TcpEstabResetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpEstabResets (&pMultiData->u4_ULongValue));
}

INT4
TcpCurrEstabGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpCurrEstab (&pMultiData->u4_ULongValue));
}

INT4
TcpInSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpInSegs (&pMultiData->u4_ULongValue));
}

INT4
TcpOutSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpOutSegs (&pMultiData->u4_ULongValue));
}

INT4
TcpRetransSegsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpRetransSegs (&pMultiData->u4_ULongValue));
}

INT4
TcpInErrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpInErrs (&pMultiData->u4_ULongValue));
}

INT4
TcpOutRstsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetTcpOutRsts (&pMultiData->u4_ULongValue));
}

INT4
TcpConnStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{

    return (nmhTestv2TcpConnState (pu4Error,
                                   pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiIndex->pIndex[2].u4_ULongValue,
                                   pMultiIndex->pIndex[3].i4_SLongValue,
                                   pMultiData->i4_SLongValue));
}

INT4
TcpConnStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetTcpConnState (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].i4_SLongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].i4_SLongValue,
                                pMultiData->i4_SLongValue));
}

INT4
TcpConnStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetTcpConnState (pMultiIndex->pIndex[0].u4_ULongValue,
                                pMultiIndex->pIndex[1].i4_SLongValue,
                                pMultiIndex->pIndex[2].u4_ULongValue,
                                pMultiIndex->pIndex[3].i4_SLongValue,
                                &pMultiData->i4_SLongValue));
}

INT4
TcpConnLocalAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
TcpConnLocalPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
TcpConnRemAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[2].u4_ULongValue;
    return SNMP_SUCCESS;
}

INT4
TcpConnRemPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceTcpConnTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[3].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexTcpConnTable (tSnmpIndex * pFirstMultiIndex,
                          tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4tcpConnLocalAddress;
    INT4                i4tcpConnLocalPort;
    UINT4               u4tcpConnRemAddress;
    INT4                i4tcpConnRemPort;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexTcpConnTable (&u4tcpConnLocalAddress,
                                          &i4tcpConnLocalPort,
                                          &u4tcpConnRemAddress,
                                          &i4tcpConnRemPort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexTcpConnTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4tcpConnLocalAddress,
             pFirstMultiIndex->pIndex[1].i4_SLongValue, &i4tcpConnLocalPort,
             pFirstMultiIndex->pIndex[2].u4_ULongValue, &u4tcpConnRemAddress,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &i4tcpConnRemPort) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4tcpConnLocalAddress;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4tcpConnLocalPort;
    pNextMultiIndex->pIndex[2].u4_ULongValue = u4tcpConnRemAddress;
    pNextMultiIndex->pIndex[3].i4_SLongValue = i4tcpConnRemPort;
    return SNMP_SUCCESS;
}

INT4
TcpConnTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2TcpConnTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
