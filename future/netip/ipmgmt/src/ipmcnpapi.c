/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcnpapi.c,v 1.6 2015/03/18 13:39:09 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of IP Multicast module.
 ******************************************************************************/

#ifndef _IPMCNPAPI_C_
#define _IPMCNPAPI_C_

#include "nputil.h"

#ifdef FS_NPAPI
#if defined (PIM_WANTED) || defined (DVMRP_WANTED) || defined (IGMPPRXY_WANTED)
/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McInit ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Init Failed for IpmcNp\n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McDeInit ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    	          "Entering IpmcFsNpIpv4McDeInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "DeInit Failed for IpmcNp\n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McDeInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4VlanMcInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VlanMcInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VlanMcInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4VlanMcInit (UINT4 u4VlanId)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4VlanMcInit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4VlanMcInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_VLAN_MC_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4VlanMcInit;

    pEntry->u4VlanId = u4VlanId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "VlanMc Init Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4VlanMcInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4VlanMcDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VlanMcDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VlanMcDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4VlanMcDeInit (UINT4 u4Index)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4VlanMcDeInit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4VlanMcDeInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_VLAN_MC_DE_INIT,    /* Function/OpCode */
                         u4Index,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4VlanMcDeInit;

    pEntry->u4Index = u4Index;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "VlanMc DeInit Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4VlanMcDeInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4RportMcInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4RportMcInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4RportMcInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4RportMcInit (tRPortInfo PortInfo)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4RportMcInit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4RportMcInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_RPORT_MC_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4RportMcInit;

    pEntry->PortInfo = PortInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Ipv4Rport Init Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4RportMcInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4RportMcDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4RportMcDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4RportMcDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4RportMcDeInit (tRPortInfo PortInfo)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4RportMcDeInit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4RportMcDeInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_RPORT_MC_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4RportMcDeInit;

    pEntry->PortInfo = PortInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Ipv4Rport DeInit Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4RportMcDeInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McAddRouteEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McAddRouteEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McAddRouteEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McAddRouteEntry (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4GrpPrefix,
                             UINT4 u4SrcIpAddr, UINT4 u4SrcIpPrefix,
                             UINT1 u1CallerId, tMcRtEntry rtEntry,
                             UINT2 u2NoOfDownStreamIf,
                             tMcDownStreamIf * pDownStreamIf)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McAddRouteEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    	          "Entering IpmcFsNpIpv4McAddRouteEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_ADD_ROUTE_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McAddRouteEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->u4SrcIpAddr = u4SrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->u1CallerId = u1CallerId;
    pEntry->rtEntry = rtEntry;
    pEntry->u2NoOfDownStreamIf = u2NoOfDownStreamIf;
    pEntry->pDownStreamIf = pDownStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Route Entry Addition Failed"  
		      "The No. of DownstreamIf is %u\n" "The VrId is %u\n" "The CallerId is %u\n",
                      (pEntry->u2NoOfDownStreamIf), (pEntry->u4VrId), 
		      (pEntry->u1CallerId));        
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McAddRouteEntry\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McDelRouteEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McDelRouteEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McDelRouteEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McDelRouteEntry (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4GrpPrefix,
                             UINT4 u4SrcIpAddr, UINT4 u4SrcIpPrefix,
                             tMcRtEntry rtEntry, UINT2 u2NoOfDownStreamIf,
                             tMcDownStreamIf * pDownStreamIf)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McDelRouteEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McDelRouteEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_DEL_ROUTE_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McDelRouteEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->u4SrcIpAddr = u4SrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->rtEntry = rtEntry;
    pEntry->u2NoOfDownStreamIf = u2NoOfDownStreamIf;
    pEntry->pDownStreamIf = pDownStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Route Entry Deletion Failed" 
		       "The No. of DownstreamIf is %u\n" "The VrId is %u\n",
                      (pEntry->u2NoOfDownStreamIf), (pEntry->u4VrId));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McDelRouteEntry\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McClearAllRoutes                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McClearAllRoutes
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McClearAllRoutes
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McClearAllRoutes (UINT4 u4VrId)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McClearAllRoutes *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McClearAllRoutes\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_CLEAR_ALL_ROUTES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McClearAllRoutes;

    pEntry->u4VrId = u4VrId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "ClearAllRoutes Failed \n " "The VrId is %u\n", (pEntry->u4VrId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McClearAllRoutes\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McAddCpuPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McAddCpuPort
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McAddCpuPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McAddCpuPort (UINT2 u2GenRtrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                          tMcRtEntry rtEntry)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McAddCpuPort *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McAddCpuPort\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_ADD_CPU_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McAddCpuPort;

    pEntry->u2GenRtrId = u2GenRtrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;
    pEntry->rtEntry = rtEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Add Cpu Port Failed" 
		       "The GenRtrId is %u\n",
                      (pEntry->u2GenRtrId));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsNpIpv4McAddCpuPort\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McDelCpuPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McDelCpuPort
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McDelCpuPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McDelCpuPort (UINT2 u2GenRtrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                          tMcRtEntry rtEntry)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McDelCpuPort *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McDelCpuPort\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_DEL_CPU_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McDelCpuPort;

    pEntry->u2GenRtrId = u2GenRtrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;
    pEntry->rtEntry = rtEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Del Cpu Port Failed" 
		      "The GenRtrId is %u\n",
                      (pEntry->u2GenRtrId));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		 "Exiting IpmcFsNpIpv4McDelCpuPort\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McGetHitStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McGetHitStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McGetHitStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McGetHitStatus (UINT4 u4SrcIpAddr, UINT4 u4GrpAddr, UINT4 u4Iif,
                            UINT2 u2VlanId, UINT4 *pu4HitStatus)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McGetHitStatus *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI,
    		 "Entering IpmcFsNpIpv4McGetHitStatus\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_GET_HIT_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McGetHitStatus;

    pEntry->u4SrcIpAddr = u4SrcIpAddr;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4Iif = u4Iif;
    pEntry->u2VlanId = u2VlanId;
    pEntry->pu4HitStatus = pu4HitStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "McGetHitStatus Failed" 
		      "The Iif is %u\n"  "The VlanId is %u\n",
                      (pEntry->u4Iif),(pEntry->u2VlanId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McGetHitStatus\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McUpdateOifVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McUpdateOifVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McUpdateOifVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McUpdateOifVlanEntry (UINT4 u4VrId, UINT4 u4GrpAddr,
                                  UINT4 u4SrcIpAddr, tMcRtEntry rtEntry,
                                  tMcDownStreamIf downStreamIf)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McUpdateOifVlanEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McUpdateOifVlanEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_UPDATE_OIF_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McUpdateOifVlanEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcIpAddr = u4SrcIpAddr;
    pEntry->rtEntry = rtEntry;
    pEntry->downStreamIf = downStreamIf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Updation of OifVlanEntry Failed" 
		      "The VrId is %u\n",
                      (pEntry->u4VrId));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McUpdateOifVlanEntry\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McUpdateIifVlanEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McUpdateIifVlanEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McUpdateIifVlanEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McUpdateIifVlanEntry (UINT4 u4VrId, UINT4 u4GrpAddr,
                                  UINT4 u4SrcIpAddr, tMcRtEntry rtEntry)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McUpdateIifVlanEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McUpdateIifVlanEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_UPDATE_IIF_VLAN_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McUpdateIifVlanEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcIpAddr = u4SrcIpAddr;
    pEntry->rtEntry = rtEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG1 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Updation of IifVlanEntry Failed" 
		      "The VrId is %u\n",
                      (pEntry->u4VrId));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McUpdateOifVlanEntry\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4GetMRouteStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetMRouteStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetMRouteStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4GetMRouteStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                            INT4 i4StatType, UINT4 *pu4RetValue)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4GetMRouteStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4GetMRouteStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_M_ROUTE_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteStats;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;
    pEntry->i4StatType = i4StatType;
    pEntry->pu4RetValue = pu4RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "RouteStats Failed" 
		      "The VrId is %u\n" "The StatType is %d\n",
                      (pEntry->u4VrId),(pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsNpIpv4GetMRouteStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4GetMRouteHCStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetMRouteHCStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetMRouteHCStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4GetMRouteHCStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                              INT4 i4StatType,
                              tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4GetMRouteHCStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4GetMRouteHCStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_M_ROUTE_H_C_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMRouteHCStats;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;
    pEntry->i4StatType = i4StatType;
    pEntry->pu8RetValue = pu8RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
	MCAST_NP_DBG2 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "RouteHCStats Failed" 
		      "The VrId is %u\n" "The StatType is %d\n",
                      (pEntry->u4VrId), (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsNpIpv4GetMRouteHCStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4GetMNextHopStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetMNextHopStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetMNextHopStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4GetMNextHopStats (UINT4 u4VrId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                              INT4 i4OutIfIndex, INT4 i4StatType,
                              UINT4 *pu4RetValue)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4GetMNextHopStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4GetMNextHopStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_M_NEXT_HOP_STATS,    /* Function/OpCode */
                         i4OutIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMNextHopStats;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;
    pEntry->i4OutIfIndex = i4OutIfIndex;
    pEntry->i4StatType = i4StatType;
    pEntry->pu4RetValue = pu4RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "NextHopStats Failed" 
		      "The VrId is %u\n" "The StatType is %d\n" "The OutIfIndex is %d\n",
                      (pEntry->u4VrId), (pEntry->i4StatType) ,(pEntry->i4OutIfIndex));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsNpIpv4GetMNextHopStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4GetMIfaceStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetMIfaceStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetMIfaceStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4GetMIfaceStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                            UINT4 *pu4RetValue)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4GetMIfaceStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4GetMIfaceStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_M_IFACE_STATS,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceStats;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4StatType = i4StatType;
    pEntry->pu4RetValue = pu4RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Getting MIfaceStats Failed \n " "The VrId is %u\n" "The IfIndex is %d\n"
		      "The StatType is %d\n",(pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsNpIpv4GetMIfaceStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4GetMIfaceHCStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetMIfaceHCStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetMIfaceHCStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4GetMIfaceHCStats (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4StatType,
                              tSNMP_COUNTER64_TYPE * pu8RetValue)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4GetMIfaceHCStats *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4GetMIfaceHCStats\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_M_IFACE_H_C_STATS,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4GetMIfaceHCStats;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4StatType = i4StatType;
    pEntry->pu8RetValue = pu8RetValue;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Getting MIfaceHCStats Failed \n " "The VrId is %u\n" "The IfIndex is %d\n"
		      "The StatType is %d\n", (pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4StatType));
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4GetMIfaceHCStats\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4SetMIfaceTtlTreshold                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4SetMIfaceTtlTreshold
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4SetMIfaceTtlTreshold
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4SetMIfaceTtlTreshold (UINT4 u4VrId, INT4 i4IfIndex,
                                  INT4 i4TtlTreshold)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4SetMIfaceTtlTreshold *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4SetMIfaceTtlTreshold\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_SET_M_IFACE_TTL_TRESHOLD,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4SetMIfaceTtlTreshold;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4TtlTreshold = i4TtlTreshold;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Set IfaceTtlTreshold Failed \n " "The VrId is %u\n" "The IfIndex is %d\n" 
		      "The TtlTreshold is %d\n", (pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4TtlTreshold));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4SetMIfaceTtlTreshold\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4SetMIfaceRateLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4SetMIfaceRateLimit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4SetMIfaceRateLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4SetMIfaceRateLimit (UINT4 u4VrId, INT4 i4IfIndex, INT4 i4RateLimit)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4SetMIfaceRateLimit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4SetMIfaceRateLimit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_SET_M_IFACE_RATE_LIMIT,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4SetMIfaceRateLimit;

    pEntry->u4VrId = u4VrId;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4RateLimit = i4RateLimit;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Set IfaceRateLimit Failed \n " "The VrId is %u\n" "The IfIndex is %d\n"
		      "The RateLimit is %d\n", (pEntry->u4VrId), (pEntry->i4IfIndex), (pEntry->i4RateLimit));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4SetMIfaceRateLimit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpvXHwGetMcastEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpvXHwGetMcastEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpvXHwGetMcastEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpvXHwGetMcastEntry (tNpL3McastEntry * pNpL3McastEntry)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpvXHwGetMcastEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI,
    		  "Entering IpmcFsNpIpvXHwGetMcastEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV_X_HW_GET_MCAST_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpvXHwGetMcastEntry;

    pEntry->pNpL3McastEntry = pNpL3McastEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Getting McastEntry Failed \n ");
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpvXHwGetMcastEntry\n ");
    return (FNP_SUCCESS);
}

#ifdef PIM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsPimNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsPimNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsPimNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsPimNpInitHw ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Entering IpmcFsPimNpInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_PIM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np InitHw Failed \n ");
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsPimNpInitHw\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsPimNpDeInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsPimNpDeInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsPimNpDeInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsPimNpDeInitHw ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		 "Entering IpmcFsPimNpDeInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_PIM_NP_DE_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np DeInitHw Failed \n ");
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsPimNpDeInitHw\n ");
    return (FNP_SUCCESS);
}
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsDvmrpNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDvmrpNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsDvmrpNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsDvmrpNpInitHw ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsDvmrpNpInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_DVMRP_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np InitHw Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsDvmrpNpInitHw\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsDvmrpNpDeInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDvmrpNpDeInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsDvmrpNpDeInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsDvmrpNpDeInitHw ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsDvmrpNpDeInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_DVMRP_NP_DE_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np DeInitHw Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsDvmrpNpDeInitHw\n ");
    return (FNP_SUCCESS);
}
#endif /* DVMRP_WANTED */
#ifdef IGMPPRXY_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIgmpProxyInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIgmpProxyInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIgmpProxyInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIgmpProxyInit ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIgmpProxyInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IGMP_PROXY_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np ProxyInit Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsNpIgmpProxyInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIgmpProxyDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIgmpProxyDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIgmpProxyDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIgmpProxyDeInit ()
{
    tFsHwNp             FsHwNp;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI,
    	          "Entering IpmcFsNpIgmpProxyDeInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IGMP_PROXY_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Proxy DeInit Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIgmpProxyDeInit\n ");
    return (FNP_SUCCESS);
}
#endif /* IGMPPRXY_WANTED */
#ifdef PIM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McClearHitBit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McClearHitBit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McClearHitBit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McClearHitBit (UINT2 u2VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McClearHitBit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4McClearHitBit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_CLEAR_HIT_BIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McClearHitBit;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "IPv4 McClear Hit Bit Failure\n ");
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4McClearHitBit\n ");
    return (FNP_SUCCESS);
}
#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4McRpfDFInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4McRpfDFInfo
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4McRpfDFInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4McRpfDFInfo (tMcRpfDFInfo * pMcRpfDFInfo)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4McRpfDFInfo *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI,
    		  "Entering IpmcFsNpIpv4McRpfDFInfo\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MC_RPF_D_F_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4McRpfDFInfo;

    pEntry->pMcRpfDFInfo = pMcRpfDFInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np RpfDFInfo Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsNpIpv4McRpfDFInfo\n ");
    return (FNP_SUCCESS);
}
#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4MbsmMcInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4MbsmMcInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4MbsmMcInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4MbsmMcInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4MbsmMcInit *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI,
    		  "Entering IpmcFsNpIpv4MbsmMcInit\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MBSM_MC_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4MbsmMcInit;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Mbsm Init Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsNpIpv4MbsmMcInit\n ");
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsNpIpv4MbsmMcAddRouteEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4MbsmMcAddRouteEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4MbsmMcAddRouteEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsNpIpv4MbsmMcAddRouteEntry (UINT4 u4VrId, UINT4 u4GrpAddr,
                                 UINT4 u4GrpPrefix, UINT4 u4SrcIpAddr,
                                 UINT4 u4SrcIpPrefix, UINT1 u1CallerId,
                                 tMcRtEntry rtEntry, UINT2 u2NoOfDownStreamIf,
                                 tMcDownStreamIf * pDownStreamIf,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsNpIpv4MbsmMcAddRouteEntry *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI, 
    		  "Entering IpmcFsNpIpv4MbsmMcAddRouteEntry\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_NP_IPV4_MBSM_MC_ADD_ROUTE_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsNpIpv4MbsmMcAddRouteEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4GrpPrefix = u4GrpPrefix;
    pEntry->u4SrcIpAddr = u4SrcIpAddr;
    pEntry->u4SrcIpPrefix = u4SrcIpPrefix;
    pEntry->u1CallerId = u1CallerId;
    pEntry->rtEntry = rtEntry;
    pEntry->u2NoOfDownStreamIf = u2NoOfDownStreamIf;
    pEntry->pDownStreamIf = pDownStreamIf;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        MCAST_NP_DBG3 (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI,
                      "Np Mbsm Hw Route Entry Addition Failed" 
		      "The VrId is %u\n" "The CallerId is %u\n" "The No. Of Down Stream If is %u\n",
                      (pEntry->u4VrId), (pEntry->u1CallerId), (pEntry->u2NoOfDownStreamIf));
	return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    	          "Exiting IpmcFsNpIpv4MbsmMcAddRouteEntry\n ");
    return (FNP_SUCCESS);
}

#ifdef PIM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsPimMbsmNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsPimMbsmNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsPimMbsmNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsPimMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsPimMbsmNpInitHw *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Entering IpmcFsPimMbsmNpInitHw\n ");        
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_PIM_MBSM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsPimMbsmNpInitHw;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Init Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI, 
    		  "Exiting IpmcFsPimMbsmNpInitHw\n ");
    return (FNP_SUCCESS);
}
#endif /* PIM_WANTED */
#ifdef DVMRP_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpmcFsDvmrpMbsmNpInitHw                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDvmrpMbsmNpInitHw
 *                                                                          
 *    Input(s)            : Arguments of FsDvmrpMbsmNpInitHw
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpmcFsDvmrpMbsmNpInitHw (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpmcNpModInfo     *pIpmcNpModInfo = NULL;
    tIpmcNpWrFsDvmrpMbsmNpInitHw *pEntry = NULL;
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_ENTRY, MCAST_NP_DBG_NPAPI,
    		  "Entering IpmcFsDvmrpMbsmNpInitHw\n ");
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPMC_MOD,    /* Module ID */
                         FS_DVMRP_MBSM_NP_INIT_HW,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpmcNpModInfo = &(FsHwNp.IpmcNpModInfo);
    pEntry = &pIpmcNpModInfo->IpmcNpFsDvmrpMbsmNpInitHw;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
    	MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_FAILURE, MCAST_NP_DBG_NPAPI, 
		      "Np Mbsm Init Failed \n ");
        return (FNP_FAILURE);
    }
    MCAST_NP_DBG (MCAST_NP_DBG_FLAG, MCAST_NP_DBG_EXIT, MCAST_NP_DBG_NPAPI,
    		  "Exiting IpmcFsDvmrpMbsmNpInitHw\n ");
    return (FNP_SUCCESS);
}
#endif /* DVMRP_WANTED */
#endif
#endif
#endif
#endif
