/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnpapi.c,v 1.14 2017/11/14 07:31:12 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of IP module.
 ******************************************************************************/

#ifndef __IP_NPAPI_C__
#define __IP_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IP_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpOspfInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpOspfInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpOspfInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpOspfInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_OSPF_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpOspfDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpOspfDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpOspfDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpOspfDeInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_OSPF_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpDhcpSrvInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpSrvInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpSrvInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpDhcpSrvInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_SRV_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpDhcpSrvDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpSrvDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpSrvDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpDhcpSrvDeInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_SRV_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpDhcpRlyInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpRlyInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpRlyInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpDhcpRlyInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_RLY_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpDhcpRlyDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpDhcpRlyDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpDhcpRlyDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpDhcpRlyDeInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_DHCP_RLY_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpRipInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpRipInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpRipInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpRipInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_RIP_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpRipDeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpRipDeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpRipDeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpRipDeInit (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_RIP_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4CreateIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4CreateIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4CreateIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4CreateIpInterface (UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4CfaIfIndex,
                             UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                             UINT2 u2VlanId, UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4CreateIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CREATE_IP_INTERFACE,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4CreateIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ModifyIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ModifyIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ModifyIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ModifyIpInterface (UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4CfaIfIndex,
                             UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                             UINT2 u2VlanId, UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ModifyIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_MODIFY_IP_INTERFACE,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ModifyIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4DeleteIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4DeleteIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4DeleteIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4DeleteIpInterface (UINT4 u4VrId, UINT1 *pu1IfName, UINT4 u4IfIndex,
                             UINT2 u2VlanId)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4DeleteIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_DELETE_IP_INTERFACE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u2VlanId = u2VlanId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4UpdateIpInterfaceStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4UpdateIpInterfaceStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4UpdateIpInterfaceStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4UpdateIpInterfaceStatus (UINT4 u4VrId, UINT1 *pu1IfName,
                                   UINT4 u4CfaIfIndex, UINT4 u4IpAddr,
                                   UINT4 u4IpSubNetMask, UINT2 u2VlanId,
                                   UINT1 *au1MacAddr, UINT4 u4Status)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4UpdateIpInterfaceStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4UpdateIpInterfaceStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1IfName = pu1IfName;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;
    pEntry->u4Status = u4Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4SetForwardingStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4SetForwardingStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4SetForwardingStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4SetForwardingStatus (UINT4 u4VrId, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4SetForwardingStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_SET_FORWARDING_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4SetForwardingStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ClearRouteTable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ClearRouteTable
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ClearRouteTable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ClearRouteTable (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CLEAR_ROUTE_TABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ClearArpTable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ClearArpTable
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ClearArpTable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ClearArpTable (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CLEAR_ARP_TABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4UcDelRoute                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4UcDelRoute
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4UcDelRoute
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4UcDelRoute (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
                      tFsNpNextHopInfo routeEntry, INT4 *pi4FreeDefIpB4Del)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4UcDelRoute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_UC_DEL_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcDelRoute;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpDestAddr = u4IpDestAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->routeEntry = routeEntry;
    pEntry->pi4FreeDefIpB4Del = pi4FreeDefIpB4Del;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ArpAdd (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                  UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State,
                  UINT4 *pu4TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_ARP_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpAdd;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pu4TblFull = pu4TblFull;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ArpModify                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ArpModify
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ArpModify
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ArpModify (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                     UINT4 u4PhyIfIndex, UINT1 *pMacAddr, UINT1 *pu1IfName,
                     INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ArpModify *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_ARP_MODIFY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpModify;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4PhyIfIndex = u4PhyIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ArpDel                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ArpDel
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ArpDel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ArpDel (UINT4 u4IpAddr, UINT1 *pu1IfName, INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ArpDel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_ARP_DEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpDel;

    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4CheckHitOnArpEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4CheckHitOnArpEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4CheckHitOnArpEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4CheckHitOnArpEntry (UINT4 u4IpAddress, UINT1 u1NextHopFlag)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4CheckHitOnArpEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CHECK_HIT_ON_ARP_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4CheckHitOnArpEntry;

    pEntry->u4IpAddress = u4IpAddress;
    pEntry->u1NextHopFlag = u1NextHopFlag;
    pEntry->u1HitBitStatus = 0;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4SyncVlanAndL3Info                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4SyncVlanAndL3Info
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4SyncVlanAndL3Info
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4SyncVlanAndL3Info (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4UcAddRoute                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4UcAddRoute
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4UcAddRoute
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4UcAddRoute (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
#ifndef NPSIM_WANTED
                      tFsNpNextHopInfo * pRouteEntry,
#else
                      tFsNpNextHopInfo routeEntry,
#endif
                      UINT1 *pbu1TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4UcAddRoute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_UC_ADD_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcAddRoute;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpDestAddr = u4IpDestAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
#ifndef NPSIM_WANTED
    pEntry->pRouteEntry = pRouteEntry;
#else
    pEntry->routeEntry = routeEntry;
#endif
    pEntry->pbu1TblFull = pbu1TblFull;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                     UINT4 u4IpAddr, UINT1 *pMacAddr, UINT1 *pu1IfName,
                     INT1 i1State, UINT4 *pu4TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pu4TblFull = pu4TblFull;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfArpModify                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpModify
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpModify
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfArpModify (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IfIndex,
                        UINT4 u4PhyIfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr,
                        UINT1 *pu1IfName, INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpModify *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_MODIFY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpModify;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4PhyIfIndex = u4PhyIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfArpDel                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpDel
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpDel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfArpDel (UINT4 u4VrId, UINT4 u4IpAddr, UINT1 *pu1IfName,
                     INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpDel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_DEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpDel;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfCheckHitOnArpEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfCheckHitOnArpEntry
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfCheckHitOnArpEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfCheckHitOnArpEntry (UINT4 u4VrId, UINT4 u4IpAddress,
                                 UINT1 u1NextHopFlag)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfCheckHitOnArpEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfCheckHitOnArpEntry;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpAddress = u4IpAddress;
    pEntry->u1NextHopFlag = u1NextHopFlag;
    pEntry->u1HitBitStatus = 0;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfClearArpTable                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfClearArpTable
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfClearArpTable
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfClearArpTable (UINT4 u4VrId)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfClearArpTable *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_CLEAR_ARP_TABLE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfClearArpTable;

    pEntry->u4VrId = u4VrId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfGetSrcMovedIpAddr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfGetSrcMovedIpAddr
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfGetSrcMovedIpAddr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfGetSrcMovedIpAddr (UINT4 *pu4VrId, UINT4 *pu4IpAddress)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfGetSrcMovedIpAddr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_GET_SRC_MOVED_IP_ADDR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfGetSrcMovedIpAddr;

    pEntry->pu4VrId = pu4VrId;
    pEntry->pu4IpAddress = pu4IpAddress;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpCfaVrfSetDlfStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpCfaVrfSetDlfStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpCfaVrfSetDlfStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpCfaVrfSetDlfStatus (UINT4 u4VrfId, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpCfaVrfSetDlfStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_CFA_VRF_SET_DLF_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpCfaVrfSetDlfStatus;

    pEntry->u4VrfId = u4VrfId;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpL3Ipv4VrfArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpL3Ipv4VrfArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpL3Ipv4VrfArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpL3Ipv4VrfArpAdd (UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                       UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State,
                       UINT1 *pbu1TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpL3Ipv4VrfArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_L3_IPV4_VRF_ARP_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpAdd;

    pEntry->u4VrfId = u4VrfId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pbu1TblFull = pbu1TblFull;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpL3Ipv4VrfArpModify                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpL3Ipv4VrfArpModify
 *                                                                          
 *    Input(s)            : Arguments of FsNpL3Ipv4VrfArpModify
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpL3Ipv4VrfArpModify (UINT4 u4VrfId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                          UINT1 *pMacAddr, UINT1 *pu1IfName, INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpL3Ipv4VrfArpModify *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_L3_IPV4_VRF_ARP_MODIFY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpModify;

    pEntry->u4VrfId = u4VrfId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfArpGet                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpGet
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpGet
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfArpGet (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpGet *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_GET,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGet;

    pEntry->ArpNpInParam = ArpNpInParam;
    pEntry->pArpNpOutParam = pArpNpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrfArpGetNext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrfArpGetNext
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrfArpGetNext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrfArpGetNext (tNpArpInput ArpNpInParam,
                         tNpArpOutput * pArpNpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrfArpGetNext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRF_ARP_GET_NEXT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGetNext;

    pEntry->ArpNpInParam = ArpNpInParam;
    pEntry->pArpNpOutParam = pArpNpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef VRRP_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrrpIntfCreateWr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrrpIntfCreateWr
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrrpIntfCreateWr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrrpIntfCreateWr (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                            UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrrpIntfCreateWr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRRP_INTF_CREATE_WR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrrpIntfCreateWr;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4CreateVrrpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4CreateVrrpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4CreateVrrpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4CreateVrrpInterface (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                               UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4CreateVrrpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CREATE_VRRP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4CreateVrrpInterface;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrrpIntfDeleteWr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrrpIntfDeleteWr
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrrpIntfDeleteWr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrrpIntfDeleteWr (tNpVlanId u2VlanId, UINT4 u4IfIndex, UINT4 u4IpAddr,
                            UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrrpIntfDeleteWr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRRP_INTF_DELETE_WR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrrpIntfDeleteWr;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4DeleteVrrpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4DeleteVrrpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4DeleteVrrpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4DeleteVrrpInterface (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                               UINT1 *au1MacAddr)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4DeleteVrrpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_DELETE_VRRP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteVrrpInterface;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->au1MacAddr = au1MacAddr;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4GetVrrpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetVrrpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetVrrpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4GetVrrpInterface (INT4 i4IfIndex, INT4 i4VrId)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4GetVrrpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_VRRP_INTERFACE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetVrrpInterface;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4VrId = i4VrId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrrpInstallFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrrpInstallFilter
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrrpInstallFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrrpInstallFilter (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRRP_INSTALL_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrrpRemoveFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrrpRemoveFilter
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrrpRemoveFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrrpRemoveFilter (VOID)
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRRP_REMOVE_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* VRRP_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4IsRtPresentInFastPath                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4IsRtPresentInFastPath
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4IsRtPresentInFastPath
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4IsRtPresentInFastPath (UINT4 u4DestAddr, UINT4 u4Mask)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4IsRtPresentInFastPath *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_IS_RT_PRESENT_IN_FAST_PATH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4IsRtPresentInFastPath;

    pEntry->u4DestAddr = u4DestAddr;
    pEntry->u4Mask = u4Mask;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4GetStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetStats
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4GetStats (INT4 i4StatType, UINT4 *pu4RetVal)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4GetStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetStats;

    pEntry->i4StatType = i4StatType;
    pEntry->pu4RetVal = pu4RetVal;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4VrmEnableVr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4VrmEnableVr
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4VrmEnableVr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4VrmEnableVr (UINT4 u4VrId, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4VrmEnableVr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_VRM_ENABLE_VR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrmEnableVr;

    pEntry->u4VrId = u4VrId;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4BindIfToVrId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4BindIfToVrId
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4BindIfToVrId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4BindIfToVrId (UINT4 u4IfIndex, UINT4 u4VrId)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4BindIfToVrId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_BIND_IF_TO_VR_ID,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4BindIfToVrId;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4VrId = u4VrId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4GetNextHopInfo                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetNextHopInfo
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetNextHopInfo
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4GetNextHopInfo (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                          tFsNpNextHopInfo * pNextHopInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4GetNextHopInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_NEXT_HOP_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetNextHopInfo;

    pEntry->u4VrId = u4VrId;
    pEntry->u4Dest = u4Dest;
    pEntry->u4DestMask = u4DestMask;
    pEntry->pNextHopInfo = pNextHopInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4UcAddTrap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4UcAddTrap
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4UcAddTrap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4UcAddTrap (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                     tFsNpNextHopInfo nextHopInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4UcAddTrap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_UC_ADD_TRAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcAddTrap;

    pEntry->u4VrId = u4VrId;
    pEntry->u4Dest = u4Dest;
    pEntry->u4DestMask = u4DestMask;
    pEntry->nextHopInfo = nextHopInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ClearFowardingTbl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ClearFowardingTbl
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ClearFowardingTbl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ClearFowardingTbl (UINT4 u4VrId)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ClearFowardingTbl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_CLEAR_FOWARDING_TBL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ClearFowardingTbl;

    pEntry->u4VrId = u4VrId;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4GetSrcMovedIpAddr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4GetSrcMovedIpAddr
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4GetSrcMovedIpAddr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4GetSrcMovedIpAddr (UINT4 *pu4IpAddress)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4GetSrcMovedIpAddr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_SRC_MOVED_IP_ADDR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetSrcMovedIpAddr;

    pEntry->pu4IpAddress = pu4IpAddress;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4MapVlansToIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4MapVlansToIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4MapVlansToIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4MapVlansToIpInterface (tNpIpVlanMappingInfo * pPvlanMappingInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4MapVlansToIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4MapVlansToIpInterface;

    pEntry->pPvlanMappingInfo = pPvlanMappingInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef NAT_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpNatDisableOnIntf                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpNatDisableOnIntf
 *                                                                          
 *    Input(s)            : Arguments of FsNpNatDisableOnIntf
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpNatDisableOnIntf (INT4 i4Intf)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpNatDisableOnIntf *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_NAT_DISABLE_ON_INTF,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpNatDisableOnIntf;

    pEntry->i4Intf = i4Intf;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpNatEnableOnIntf                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpNatEnableOnIntf
 *                                                                          
 *    Input(s)            : Arguments of FsNpNatEnableOnIntf
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpNatEnableOnIntf (INT4 i4Intf)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpNatEnableOnIntf *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_NAT_ENABLE_ON_INTF,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpNatEnableOnIntf;

    pEntry->i4Intf = i4Intf;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* NAT_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4IntfStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4IntfStatus
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4IntfStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4IntfStatus (UINT4 u4VrId, UINT4 u4IfStatus,
                      tFsNpIp4IntInfo * pIpIntInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4IntfStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_INTF_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4IntfStatus;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IfStatus = u4IfStatus;
    pEntry->pIpIntInfo = pIpIntInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *
 *    Name                : IpFsNpVrrpHwProgram
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpVrrpHwProgram.
 *
 *    Input(s)            : Arguments of FsNpVrrpHwProgram
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
#ifdef VRRP_WANTED
UINT1
IpFsNpVrrpHwProgram (UINT1 u1NpAction, tVrrpNwIntf * pVrrpNwIntf)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpVrrpHwProgram *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_VRRP_HW_PROGRAM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpVrrpHwProgram;

    pEntry->u1NpAction = u1NpAction;
    pEntry->pVrrpNwIntf = pVrrpNwIntf;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ArpGet                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ArpGet
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ArpGet
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ArpGet (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ArpGet *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_ARP_GET,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpGet;

    pEntry->ArpNpInParam = ArpNpInParam;
    pEntry->pArpNpOutParam = pArpNpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4ArpGetNext                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4ArpGetNext
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4ArpGetNext
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4ArpGetNext (tNpArpInput ArpNpInParam, tNpArpOutput * pArpNpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4ArpGetNext *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_ARP_GET_NEXT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpGetNext;

    pEntry->ArpNpInParam = ArpNpInParam;
    pEntry->pArpNpOutParam = pArpNpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4UcGetRoute                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4UcGetRoute
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4UcGetRoute
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4UcGetRoute (tNpRtmInput RtmNpInParam, tNpRtmOutput * pRtmNpOutParam)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4UcGetRoute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_UC_GET_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcGetRoute;

    pEntry->RtmNpInParam = RtmNpInParam;
    pEntry->pRtmNpOutParam = pRtmNpOutParam;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpL3Ipv4ArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpL3Ipv4ArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpL3Ipv4ArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpL3Ipv4ArpAdd (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr,
                    UINT1 *pu1IfName, INT1 i1State, UINT1 *pbu1TblFull)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpL3Ipv4ArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_L3_IPV4_ARP_ADD,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4ArpAdd;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pbu1TblFull = pbu1TblFull;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpL3Ipv4ArpModify                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpL3Ipv4ArpModify
 *                                                                          
 *    Input(s)            : Arguments of FsNpL3Ipv4ArpModify
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpL3Ipv4ArpModify (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 *pMacAddr,
                       UINT1 *pu1IfName, INT1 i1State)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpL3Ipv4ArpModify *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_L3_IPV4_ARP_MODIFY,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4ArpModify;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpIpv4L3IpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4L3IpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4L3IpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpIpv4L3IpInterface (tL3Action L3Action, tFsNpL3IfInfo * pFsNpL3IfInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4L3IpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_L3_IP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4L3IpInterface;

    pEntry->L3Action = L3Action;
    pEntry->pFsNpL3IfInfo = pFsNpL3IfInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef ISIS_WANTED
/***************************************************************************
 *
 *    Function Name       : IpFsNpIsisProgram
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIsisProgram
 *
 *    Input(s)            : Arguments of FsNpIsisProgram
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
IpFsNpIsisProgram (UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIsisHwProgram *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_ISIS_HW_PROGRAM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIsisHwProgram;

    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* ISIS_WANTED */

#ifdef MBSM_WANTED

#ifdef ISIS_WANTED

/***************************************************************************
 *
 *    Function Name       : IpFsNpMbsmIsisProgram
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIsisProgram
 *
 *    Input(s)            : Arguments of FsNpMbsmIsisProgram
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIsisProgram (tMbsmSlotInfo * pSlotInfo, UINT1 u1Status)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIsisHwProgram *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_ISIS_HW_PROGRAM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIsisHwProgram;

    pEntry->pSlotInfo = pSlotInfo;
    pEntry->u1Status = u1Status;

    return (NpUtilHwProgram (&FsHwNp));
}

#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IP_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpInit;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmOspfInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmOspfInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmOspfInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmOspfInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmOspfInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_OSPF_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmOspfInit;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmDhcpSrvInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmDhcpSrvInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmDhcpSrvInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmDhcpSrvInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmDhcpSrvInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_DHCP_SRV_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmDhcpSrvInit;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmDhcpRlyInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmDhcpRlyInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmDhcpRlyInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmDhcpRlyInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmDhcpRlyInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_DHCP_RLY_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmDhcpRlyInit;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4CreateIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4CreateIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4CreateIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4CreateIpInterface (UINT4 u4VrId, UINT4 u4CfaIfIndex,
                                 UINT4 u4IpAddr, UINT4 u4IpSubNetMask,
                                 UINT2 u2VlanId, UINT1 *au1MacAddr,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4CreateIpInterface *pEntry = NULL;
    tIpNpWrFsNpIpv4UpdateIpInterfaceStatus *pUpdtEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE,    /* Function/OpCode */
                         u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4CreateIpInterface;

    pEntry->u4VrId = u4VrId;
    pEntry->u4CfaIfIndex = u4CfaIfIndex;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->u2VlanId = u2VlanId;
    pEntry->au1MacAddr = au1MacAddr;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo);

    /*Give an indication to Other node, if the status is UP */
    if (CfaIfInfo.u1IfOperStatus == CFA_IF_UP)
    {
        NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                             NP_IP_MOD,    /* Module ID */
                             FS_NP_MBSM_IPV4_UPDATE_IP_INTERFACE_STATUS,    /* Function/OpCode */
                             u4CfaIfIndex,    /* IfIndex value if applicable */
                             0,    /* No. of Port Params */
                             0);    /* No. of PortList Parms */
        pIpNpModInfo = &(FsHwNp.IpNpModInfo);
        pUpdtEntry = &pIpNpModInfo->IpNpFsNpIpv4UpdateIpInterfaceStatus;

        pUpdtEntry->u4VrId = u4VrId;
        pUpdtEntry->pu1IfName = CfaIfInfo.au1IfName;
        pUpdtEntry->u4CfaIfIndex = u4CfaIfIndex;
        pUpdtEntry->u4IpAddr = u4IpAddr;
        pUpdtEntry->u4IpSubNetMask = u4IpSubNetMask;
        pUpdtEntry->u2VlanId = u2VlanId;
        pUpdtEntry->au1MacAddr = au1MacAddr;
        pUpdtEntry->u4Status = CFA_IF_UP;

        NpUtilHwProgram (&FsHwNp);
    }

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4ArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4ArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4ArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4ArpAdd (tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 *pMacAddr,
                      UINT1 *pbu1TblFull, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4ArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_ARP_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4ArpAdd;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pbu1TblFull = pbu1TblFull;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4UcAddRoute                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4UcAddRoute
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4UcAddRoute
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4UcAddRoute (UINT4 u4VrId, UINT4 u4IpDestAddr,
                          UINT4 u4IpSubNetMask,
#ifndef NPSIM_WANTED
                          tFsNpNextHopInfo * pRouteEntry,
#else
                          tFsNpNextHopInfo routeEntry,
#endif
                          UINT1 *pbu1TblFull, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4UcAddRoute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_UC_ADD_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4UcAddRoute;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpDestAddr = u4IpDestAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
#ifndef NPSIM_WANTED
    pEntry->pRouteEntry = pRouteEntry;
#else
    pEntry->routeEntry = routeEntry;
#endif
    pEntry->pbu1TblFull = pbu1TblFull;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4VrmEnableVr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4VrmEnableVr
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4VrmEnableVr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4VrmEnableVr (UINT4 u4VrId, UINT1 u1Status,
                           tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4VrmEnableVr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_VRM_ENABLE_VR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrmEnableVr;

    pEntry->u4VrId = u4VrId;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4BindIfToVrId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4BindIfToVrId
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4BindIfToVrId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4BindIfToVrId (UINT4 u4IfIndex, UINT4 u4VrId,
                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4BindIfToVrId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_BIND_IF_TO_VR_ID,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4BindIfToVrId;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u4VrId = u4VrId;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4UcAddTrap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4UcAddTrap
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4UcAddTrap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4UcAddTrap (UINT4 u4VrId, UINT4 u4Dest, UINT4 u4DestMask,
                         tFsNpNextHopInfo nextHopInfo,
                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4UcAddTrap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_UC_ADD_TRAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4UcAddTrap;

    pEntry->u4VrId = u4VrId;
    pEntry->u4Dest = u4Dest;
    pEntry->u4DestMask = u4DestMask;
    pEntry->nextHopInfo = nextHopInfo;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4MapVlansToIpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4MapVlansToIpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4MapVlansToIpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4MapVlansToIpInterface (tNpIpVlanMappingInfo * pPvlanMappingInfo,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4MapVlansToIpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4MapVlansToIpInterface;

    pEntry->pPvlanMappingInfo = pPvlanMappingInfo;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4VrfArpAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4VrfArpAdd
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4VrfArpAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4VrfArpAdd (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                         UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4VrfArpAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_VRF_ARP_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrfArpAdd;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pbu1TblFull = pbu1TblFull;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef VRRP_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4CreateVrrpInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4CreateVrrpInterface
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4CreateVrrpInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4CreateVrrpInterface (tNpVlanId u2VlanId, UINT4 u4IpAddr,
                                   UINT1 *au1MacAddr, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4CreateVrrpInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_CREATE_VRRP_INTERFACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4CreateVrrpInterface;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->au1MacAddr = au1MacAddr;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4VrrpInstallFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4VrrpInstallFilter
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4VrrpInstallFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4VrrpInstallFilter (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4VrrpInstallFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_VRRP_INSTALL_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrrpInstallFilter;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* VRRP_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4ArpAddition                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4ArpAddition
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4ArpAddition
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4ArpAddition (tNpVlanId u2VlanId, UINT4 u4IpAddr, UINT1 *pMacAddr,
                           UINT1 *pbu1TblFull, UINT4 u4IfIndex,
                           UINT1 *pu1IfName, INT1 i1State,
                           tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4ArpAddition *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_ARP_ADDITION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4ArpAddition;

    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pbu1TblFull = pbu1TblFull;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsNpMbsmIpv4VrfArpAddition                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmIpv4VrfArpAddition
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmIpv4VrfArpAddition
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsNpMbsmIpv4VrfArpAddition (UINT4 u4VrId, tNpVlanId u2VlanId, UINT4 u4IpAddr,
                              UINT1 *pMacAddr, UINT1 *pbu1TblFull,
                              UINT4 u4IfIndex, UINT1 *pu1IfName, INT1 i1State,
                              tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpMbsmIpv4VrfArpAddition *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_MBSM_IPV4_VRF_ARP_ADDITION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrfArpAddition;

    pEntry->u4VrId = u4VrId;
    pEntry->u2VlanId = u2VlanId;
    pEntry->u4IpAddr = u4IpAddr;
    pEntry->pMacAddr = pMacAddr;
    pEntry->pbu1TblFull = pbu1TblFull;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pu1IfName = pu1IfName;
    pEntry->i1State = i1State;
    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */

#ifdef CFA_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : IpFsCfaHwRemoveIpNetRcvdDlfInHash                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwRemoveIpNetRcvdDlfInHash
 *                                                                          
 *    Input(s)            : Arguments of IpFsCfaHwRemoveIpNetRcvdDlfInHash
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IpFsCfaHwRemoveIpNetRcvdDlfInHash (UINT4 u4ContextId, UINT4 u4IpNet,
                                   UINT4 u4IpMask)
{
    tFsHwNp             FsHwNp;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;
    tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_CFA_MOD,    /* Module ID */
                         FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pCfaNpModInfo = &(FsHwNp.CfaNpModInfo);
    pEntry = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIpNetRcvdDlfInHash;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4IpNet = u4IpNet;
    pEntry->u4IpMask = u4IpMask;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#endif /* CFA_WANTED */

/***************************************************************************
 *
 *    Function Name       : IpFsNpIpv4GetEcmpGroups
 *
 *    Description         : This function using its arguments populates the
 *                         generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                         parameters invokes FsNpIpv4GetEcmpGroups
 *
 *    Input(s)            : Arguments of FsNpIpv4GetEcmpGroups
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 ******************************************************************************/

UINT1
IpFsNpIpv4GetEcmpGroups (UINT4 *pu4EcmpGroups)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4GetEcmpGroups *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_GET_ECMP_GROUPS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetEcmpGroups;
    pEntry->pu4EcmpGroups = pu4EcmpGroups;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* __IP_NPAPI_C__ */
