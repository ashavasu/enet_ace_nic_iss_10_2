/* $Id: ip4ipvx.c,v 1.20 2016/05/20 10:11:00 siva Exp $*/
#ifndef __IP4_IPVX_C__
#define __IP4_IPVX_C__

#ifdef IP_WANTED
#include "ipinc.h"
#include "irdpextn.h"
#include "arp.h"

#include "stdiplow.h"

/*  Functions for : InetCidrRouteTable   -- No Functions   */

/*  Functions for : Ipv4InterfaceTable       */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4IfTblLastChange
 *
 *    DESCRIPTION      : This function Get the Time at which the ipv4 and 
 *                       ipv6 stats are updated recently. 
 *
 *    INPUT            : None.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SysUpTime                   
 *
 ****************************************************************************/
UINT4
IPvxGetIpv4IfTblLastChange (VOID)
{
    return (IP_CFG_GET_IF_TBL_CHANGE_TIME ());
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4IfEnableStatus    
 *
 *    DESCRIPTION      : This function get the IPv4 on the given interface 
 *
 *    INPUT            : i4Ipv4IfIndex        - Interface index. 
 *                     : i4Ipv4IfEnableStatus - Enable / Disable. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 *pi4Ipv4IfEnableStatus)
{
    UINT2               u2Port = IPVX_ZERO;
    INT4                i4RetVal = IP_FAILURE;

    i4RetVal = IpGetPortFromIfIndex ((UINT4) i4Ipv4IfIndex, &u2Port);
    if ((i4RetVal == IP_SUCCESS) && (u2Port < IPIF_MAX_LOGICAL_IFACES))
    {
        IPFWD_DS_LOCK ();
        *pi4Ipv4IfEnableStatus =
            gIpGlobalInfo.Ipif_config[u2Port].u1Ipv4EnableStatus;
        IPFWD_DS_UNLOCK ();
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxSetIpv4IfEnableStatus    
 *
 *    DESCRIPTION      : This function enable/disable the IPv4 on the given
 *                       interface 
 *
 *    INPUT            : i4Ipv4IfIndex        - Interface index. 
 *                     : i4Ipv4IfEnableStatus - Enable / Disable. 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxSetIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 i4Ipv4IfEnableStatus)
{
    UINT2               u2Port = IPVX_ZERO;
    INT4                i4RetVal = IP_FAILURE;
    UINT1               u1OperStatus = IPIF_OPER_DISABLE;
    UINT1               u1Status = IPIF_OPER_DISABLE;
    UINT4               u4BitMap = IPVX_ZERO;
    UINT1               u1Ipv4EnableStatus = IPVX_IPV4_ENABLE_STATUS_DOWN;
    UINT4               u4CxtId = IPVX_ZERO;

    i4RetVal = IpGetPortFromIfIndex ((UINT4) i4Ipv4IfIndex, &u2Port);
    if ((i4RetVal != IP_SUCCESS) || (u2Port >= IPIF_MAX_LOGICAL_IFACES))
    {
#ifdef RM_WANTED
        /* In case of standby node, we can ignore the default status enable
         * syncup failure, since its already updated in standby */
        if ((i4Ipv4IfEnableStatus == IPVX_IPV4_ENABLE_STATUS_UP)
            && (RmGetNodeState () == RM_STANDBY))
        {
            return SNMP_SUCCESS;
        }
#endif
        return (SNMP_FAILURE);
    }

    IPFWD_DS_LOCK ();
    u1OperStatus = gIpGlobalInfo.Ipif_config[u2Port].u1Oper;
    u1Ipv4EnableStatus = gIpGlobalInfo.Ipif_config[u2Port].u1Ipv4EnableStatus;
    u4CxtId = gIpGlobalInfo.Ipif_glbtab[u2Port].pIpCxt->u4ContextId;
    IPFWD_DS_UNLOCK ();

    if (i4Ipv4IfEnableStatus == u1Ipv4EnableStatus)
    {
        return (SNMP_SUCCESS);
    }

    /* If Ipv4EnableStatus is = Enabled (True) 
     *   send the OPER_UP Notification to HL
     * otherwise
     *   send the OPER_DOWN Notification to HL
     */

    if (i4Ipv4IfEnableStatus == IPVX_IPV4_ENABLE_STATUS_UP)
    {
        u1Status = IPIF_OPER_ENABLE;
    }
    else
    {
        u1Status = IPIF_OPER_DISABLE;
    }

    if (u1OperStatus == u1Status)
    {
        return (SNMP_SUCCESS);
    }

    /* Change in Oper Status */
    if (u1OperStatus != u1Status)
    {
        u4BitMap |= OPER_STATE;

        if (u1Status == IPIF_OPER_DISABLE)
        {
            IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, CONTROL_PLANE_TRC, IP_NAME,
                             "oper status changed to down for interface %d\n",
                             i4Ipv4IfIndex);
            if (gIpGlobalInfo.Ipif_glbtab[u2Port].pIpCxt->u4ContextId ==
                IP_DEFAULT_CONTEXT)
            {
                IrdpDisableInterface (u2Port);
            }
        }
        else
        {
            IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, CONTROL_PLANE_TRC, IP_NAME,
                             "oper status changed to up for interface %d\n",
                             i4Ipv4IfIndex);

            if (gIpGlobalInfo.Ipif_glbtab[u2Port].pIpCxt->u4ContextId ==
                IP_DEFAULT_CONTEXT)
            {
                IrdpEnableInterface (u2Port);
            }
        }

        /* Reflects the status of interfaces on the routes whose gateway can be
         * reached on those interface. 
         */
        IPIF_CONFIG_SET_OPER_STATUS (u2Port, u1Status);
        RtmProcessRouteForIfStatChange (u2Port, (INT1) u1Status);

        /* notify other protocols who has registered */
        IpIfStChgNotify (u2Port, u4BitMap);
        IpIfStateChngNotify (u2Port, u4BitMap);
    }

    IPFWD_DS_LOCK ();
    gIpGlobalInfo.Ipif_config[u2Port].u1Ipv4EnableStatus =
        (UINT1) i4Ipv4IfEnableStatus;
    IPFWD_DS_UNLOCK ();

    IP_CFG_SET_IF_TBL_CHANGE_TIME ();

    return (SNMP_SUCCESS);
}

/*  Functions for : IpSystemStatsTable       */
/*  Functions for : IpIfStatsTable           */

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4Stats             
 *
 *    DESCRIPTION      : This function give the IP stats for the given type of
 *                       Ip Stats object
 *                       if the 'i4Ipv4IfIdx' is invalid it give the System 
 *                       level stas otherwise interface level stats.
 *                       This function use the old nmh to get the stats counter
 *                       who's Semantics are same as new Semantics and other 
 *                       counter and different Semantics counter are accessed
 *                       for the DataStructure directly.
 *
 *    INPUT            : i4Ipv4IfIdx     - Interface index or invalid if Index
 *                     : u4StatsType     - Type of the IP stats.      
 *                     : pu1Ipv4StatsObj - Ptr to the IP stats counter
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4Stats (INT4 i4Ipv4IfIdx, UINT4 u4StatsType, UINT1 *pu1Ipv4StatsObj)
{
    tIpCxt             *pIpCxt = NULL;
    UINT4              *pu4Stats = (UINT4 *) (VOID *) pu1Ipv4StatsObj;
    INT4                i4RetVal = IP_FAILURE;
    UINT4               u4IpSysStats = IPVX_ZERO;
    UINT4               u4CxtId = 0;
    UINT2               u2Port = IPIF_INVALID_INDEX;
    tSNMP_COUNTER64_TYPE *pu8Stats = (tSNMP_COUNTER64_TYPE *) (VOID *)
        pu1Ipv4StatsObj;
    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);

    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    if (i4Ipv4IfIdx != IPVX_INVALID_IFIDX)
    {
        i4RetVal = IpGetPortFromIfIndex ((UINT4) i4Ipv4IfIdx, &u2Port);
        if ((i4RetVal != IP_SUCCESS) || (u2Port >= IPIF_MAX_LOGICAL_IFACES))
        {
            return (SNMP_FAILURE);
        }
    }

    /* Ptr to ObjStats = (u2Port == IPIF_INVALID_INDEX)? 
     * System Stats : If Stats; */
    switch (u4StatsType)
    {
        case IPVX_STATS_IN_RECEIVES:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpInReceives (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats = gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs;
            }

            break;

        case IPVX_STATS_HC_IN_RECEIVES:

            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.u4HCIn_rcvs :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_rcvs;

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpInReceives (&u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIpCxt->Ip_stats.u4In_rcvs)
                {
                    pu8Stats->msn += IPVX_ONE;
                }
            }
            else
            {
                pu8Stats->lsn =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs;
            }

            break;

        case IPVX_STATS_IN_OCTETS:
            *pu4Stats = (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4InOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InOctets;
            break;

        case IPVX_STATS_HC_IN_OCTETS:
            pu8Stats->lsn =
                (u2Port == IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.u4InOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InOctets;
            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.u4HCInOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInOctets;
            break;

        case IPVX_STATS_IN_HDR_ERRS:
            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpInHdrErrors (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_hdr_err;
            }
            break;

        case IPVX_STATS_IN_NO_ROUTE:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4In_no_routes : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4In_no_routes;
            break;

        case IPVX_STATS_IN_ADDR_ERRS:
            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpInAddrErrors (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_addr_err;
            }
            break;

        case IPVX_STATS_IN_UNKNOWN_PROTOS:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpInUnknownProtos (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Bad_proto;
            }

            break;

        case IPVX_STATS_IN_TRUN_PKTS:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4In_Trun_Pkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4In_Trun_Pkts;
            break;

        case IPVX_STATS_IN_FWD_DGRAMS:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpForwDatagrams (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts;
            }
            break;

        case IPVX_STATS_HC_IN_FWD_DGRAMS:

            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4HCInForwDgrams :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInForwDgrams;

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpForwDatagrams (&u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIpCxt->Ip_stats.u4Forw_attempts)
                {
                    pu8Stats->msn += IPVX_ONE;
                }
            }
            else
            {
                pu8Stats->lsn =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts;
            }

            break;

        case IPVX_STATS_REASM_REQDS:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpReasmReqds (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_reqs;
            }

            break;

        case IPVX_STATS_REASM_OK:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpReasmOKs (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_oks;
            }

            break;

        case IPVX_STATS_REASM_FAILS:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpReasmFails (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_fails;
            }

            break;

        case IPVX_STATS_IN_DISCARDS:
            *pu4Stats = (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4InDiscards :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InDiscards;
            break;

        case IPVX_STATS_IN_DELIVERS:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpInDelivers (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries;
            }

            break;

        case IPVX_STATS_HC_IN_DELIVERS:

            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4HCIn_deliveries :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_deliveries;

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpInDelivers (&u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIpCxt->Ip_stats.u4In_deliveries)
                {
                    pu8Stats->msn += IPVX_ONE;
                }
            }
            else
            {
                pu8Stats->lsn =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries;
            }

            break;

        case IPVX_STATS_OUT_REQUESTS:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpOutRequests (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests;
            }

            break;

        case IPVX_STATS_HC_OUT_REQUESTS:

            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4HCOut_requests :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOut_requests;

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpOutRequests (&u4IpSysStats);
                pu8Stats->lsn = u4IpSysStats;
                if (u4IpSysStats < pIpCxt->Ip_stats.u4Out_requests)
                {
                    pu8Stats->msn += IPVX_ONE;
                }
            }
            else
            {
                pu8Stats->lsn =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests;
            }

            break;

        case IPVX_STATS_OUT_NO_ROUTES:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4OutNoRoutes : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4OutNoRoutes;
            break;

        case IPVX_STATS_OUT_FWD_DGRAMS:
            *pu4Stats = (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutForwds :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutForwds;
            break;

        case IPVX_STATS_HC_OUT_FWD_DGRAMS:
            pu8Stats->lsn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutForwds :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutForwds;
            pu8Stats->msn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4HCOutForwds : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4HCOutForwds;
            break;

        case IPVX_STATS_OUT_DISCARDS:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4OutDiscards : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4OutDiscards;
            break;

        case IPVX_STATS_OUT_FRAG_REQDS:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4Out_frag_Reqs : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4Out_frag_Reqs;
            break;

        case IPVX_STATS_OUT_FRAG_OK:

            *pu4Stats = (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutFragOKs :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutFragOKs;

            break;

        case IPVX_STATS_OUT_FRAG_FAILS:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpFragFails (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_fails;
            }

            break;

        case IPVX_STATS_OUT_FRAG_CRET:

            if (u2Port == IPIF_INVALID_INDEX)
            {
                nmhGetIpFragCreates (&u4IpSysStats);
                *pu4Stats = u4IpSysStats;
            }
            else
            {
                *pu4Stats =
                    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_creates;
            }

            break;

        case IPVX_STATS_OUT_TRANS:
            *pu4Stats = (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutTrans :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutTrans;
            break;

        case IPVX_STATS_HC_OUT_TRANS:
            pu8Stats->lsn =
                (u2Port == IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.u4OutTrans :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutTrans;
            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4HCOutTrans :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutTrans;
            break;

        case IPVX_STATS_OUT_OCTETS:
            *pu4Stats = (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutOctets;
            break;

        case IPVX_STATS_HC_OUT_OCTETS:
            pu8Stats->lsn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutOctets;
            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4HCOutOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutOctets;
            break;

        case IPVX_STATS_IN_MCAST_PKTS:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.u4InMcastPkts :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts;
            break;

        case IPVX_STATS_HC_IN_MCAST_PKTS:
            pu8Stats->lsn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4InMcastPkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4InMulticastPkts;
            pu8Stats->msn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4HCInMcastPkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4HCInMcastPkts;
            break;

        case IPVX_STATS_IN_MCAST_OCTETS:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4InMcastOctets : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4InMcastOctets;
            break;

        case IPVX_STATS_HC_IN_MCAST_OCTETS:
            pu8Stats->lsn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4InMcastOctets : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4InMcastOctets;
            pu8Stats->msn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4HCInMcastOctets : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4HCInMcastOctets;
            break;

        case IPVX_STATS_OUT_MCAST_PKTS:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4OutMcastPkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4OutMulticastPkts;
            break;

        case IPVX_STATS_HC_OUT_MCAST_PKTS:
            pu8Stats->lsn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4OutMcastPkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4OutMulticastPkts;
            pu8Stats->msn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4HCInBcastPkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4OutHCMCastPkts;
            break;

        case IPVX_STATS_OUT_MCAST_OCTETS:
            *pu4Stats =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutMcastOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMcastOctets;
            break;

        case IPVX_STATS_HC_OUT_MCAST_OCTETS:
            pu8Stats->lsn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutMcastOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMcastOctets;
            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4HCOutMcastOctets :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutMcastOctets;
            break;

        case IPVX_STATS_IN_BCAST_PKTS:
            *pu4Stats = (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4In_bcasts :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts;
            break;

        case IPVX_STATS_HC_IN_BCAST_PKTS:
            pu8Stats->lsn =
                (u2Port == IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.u4In_bcasts :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts;
            pu8Stats->msn =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4HCInBcastPkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4InHCBCastPkts;
            break;

        case IPVX_STATS_OUT_BCAST_PKTS:
            *pu4Stats =
                (u2Port ==
                 IPIF_INVALID_INDEX) ? pIpCxt->Ip_stats.
                u4OutBcastPkts : gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->
                u4OutBroadcastPkts;
            break;

        case IPVX_STATS_HC_OUT_BCAST_PKTS:
            pu8Stats->lsn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4OutBcastPkts :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts;
            pu8Stats->msn =
                (u2Port == IPIF_INVALID_INDEX) ?
                pIpCxt->Ip_stats.u4HCOutBcastPkts :
                gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutHCBCastPkts;
            break;

        default:
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/*  Functions for : IpAddressPrefixTable     */

/*  Functions for : IpAddressTable           */

/*  Functions for : IpNetToPhysicalTable     */

/*  Functions for : IpDefaultRouterTable     */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxValIdxInstIpv4DefRtrTbl 
 *
 *    DESCRIPTION      : This function validate the given Ipv4 Default Router
 *                       on the given Interface 
 *
 *    INPUT            : u4Ipv4DefRtrAddr    - Default Routere address.
 *                     : i4Ipv4DefRtrIfIndex - The default Rtr learnt If.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxValIdxInstIpv4DefRtrTbl (UINT4 u4Ipv4DefRtrAddr, INT4 i4Ipv4DefRtrIfIndex)
{
    UINT2               u2Index = IPVX_ZERO;

    for (u2Index = IPVX_ZERO; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if ((gaIrdpRtrList[u2Index].u4RouterAddress == u4Ipv4DefRtrAddr) &&
            (gaIrdpRtrList[u2Index].u2Iface == i4Ipv4DefRtrIfIndex))
        {
            return (SNMP_SUCCESS);
        }
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetNxtIdxIpv4DefRtrTbl  
 *
 *    DESCRIPTION      : This function get the  next valid Ipv4 Default Router
 *                       and Interface form the given values 
 *
 *    INPUT            : u4Ipv4DefRtrAddr     - Default Routere address.
 *                     : pu4NxtIpv4DefRtrAddr - Next Default Routere address.
 *                     : i4Ipv4DefRtrIfIndex  - The default Rtr learnt If.
 *                     : pi4NextIpv4DefRtrIfIndex - next default Rtr learnt If.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetNxtIdxIpv4DefRtrTbl (UINT4 u4Ipv4DefRtrAddr,
                            UINT4 *pu4NxtIpv4DefRtrAddr,
                            INT4 i4Ipv4DefRtrIfIndex,
                            INT4 *pi4NextIpv4DefRtrIfIndex)
{
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (i4Ipv4DefRtrIfIndex);

    i1RetVal = nmhGetNextIndexFsIpRtrLstTable (u4Ipv4DefRtrAddr,
                                               pu4NxtIpv4DefRtrAddr);
    if (i1RetVal == SNMP_SUCCESS)
    {
        i1RetVal = nmhGetFsIpRtrLstIface (*pu4NxtIpv4DefRtrAddr,
                                          pi4NextIpv4DefRtrIfIndex);
    }

    return (i1RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4DefRtrLifetime 
 *
 *    DESCRIPTION      : This function give the Default Router Life time  for
 *                       given Ipv4 Default Router and Interface 
 *
 *    INPUT            : u4DefRtrAddr         - Default Routere address.
 *                     : i4IpDefRtrIfIndex    - The default Rtr learnt If.
 *                     : pu4IpDefRtrLifetime  - Life time of the default Rtr.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4DefRtrLifetime (UINT4 u4DefRtrAddr, INT4 i4IpDefRtrIfIndex,
                           UINT4 *pu4IpDefRtrLifetime)
{
    UINT2               u2Index = IPVX_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    for (u2Index = IPVX_ZERO; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if ((gaIrdpRtrList[u2Index].u1RowStatus != IPVX_ZERO) &&
            (gaIrdpRtrList[u2Index].u4RouterAddress == u4DefRtrAddr) &&
            (gaIrdpRtrList[u2Index].u2Iface == i4IpDefRtrIfIndex))
        {
            TmrGetRemainingTime (gIpGlobalInfo.IpTimerListId,
                                 &(gaIrdpRtrList[u2Index].Timer.Timer_node),
                                 pu4IpDefRtrLifetime);
            /* systicks to second */
            *pu4IpDefRtrLifetime /= (LR_NUM_OF_SYS_TIME_UNITS_IN_A_SEC);

            i1RetVal = SNMP_SUCCESS;
            break;
        }
    }

    return (i1RetVal);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxGetIpv4DefRtrPreference
 *
 *    DESCRIPTION      : This function give the Default Router Preference for
 *                       given Ipv4 Default Router and Interface 
 *
 *    INPUT            : u4DefRtrAddr       - Default Routere address.
 *                     : i4IpDefRtrIfIndex  - The default Rtr learnt If.
 *                     : pu4IpDefRtrPref    - Preference of the default Rtr.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS / SNMP_FAILURE
 *
 ****************************************************************************/
INT1
IPvxGetIpv4DefRtrPreference (UINT4 u4DefRtrAddr, INT4 i4Ipv4DefRtrIfIndex,
                             UINT4 *pu4IpDefRtrPref)
{
    /* RFC 4191 - NOt Implemented so always medium (0) 
     * for Both IPV4, IPV6 */
    UNUSED_PARAM (u4DefRtrAddr);
    UNUSED_PARAM (i4Ipv4DefRtrIfIndex);

    *pu4IpDefRtrPref = IPVX_DEF_ROUTER_PREF_MEDIUM;

    return (SNMP_SUCCESS);
}

/*  Functions for : IcmpStatsTable   -- No Functions     */

/*  Functions for : IcmpMsgStatsTable        */
/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxIpv4GetIcmpMsgStatsInPkts 
 *
 *    DESCRIPTION      : This function give the icmp stats for the given     
 *                       icmp Message Type in incomming Direction
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp Message.
 *                     : pu4RetValIcmpMsgStatsInPkts  - Ptr to Stats counter.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS 
 *
 ****************************************************************************/
INT1
IPvxIpv4GetIcmpMsgStatsInPkts (INT4 i4IcmpMsgStatsType,
                               UINT4 *pu4RetValIcmpMsgStatsInPkts)
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);

    if (pIpCxt == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValIcmpMsgStatsInPkts =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt, i4IcmpMsgStatsType);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxIpv4GetIcmpMsgStatsOutPkts
 *
 *    DESCRIPTION      : This function give the icmp stats for the given     
 *                       icmp Message Type in outgoing Direction
 *
 *    INPUT            : i4IcmpMsgStatsType - Type of Icmp Message.
 *                     : pu4RetValIcmpMsgStatsOutPkts - Ptr to Stats counter.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS 
 *
 ****************************************************************************/
INT1
IPvxIpv4GetIcmpMsgStatsOutPkts (INT4 i4IcmpMsgStatsType,
                                UINT4 *pu4RetValIcmpMsgStatsOutPkts)
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);

    if (pIpCxt == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValIcmpMsgStatsOutPkts =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt, i4IcmpMsgStatsType);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxClearIpv4SystemStats
 *
 *    DESCRIPTION      : Clears the IPSystemStatistics
 *
 *    INPUT            : u4ContextId.
 *                     
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS
 *
 **************************************************************************/
VOID
IPvxClearIpv4SystemStats (UINT4 u4ContextId, INT4 i4Ipv4IfIdx)
{
    tIpCxt             *pIpCxt = NULL;
    INT4                i4RetVal = 0;
    UINT2               u2Port = IPIF_INVALID_INDEX;

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return;
    }
    if (i4Ipv4IfIdx != IPVX_INVALID_IFIDX)
    {
        i4RetVal = IpGetPortFromIfIndex ((UINT4) i4Ipv4IfIdx, &u2Port);
        if ((i4RetVal != IP_SUCCESS) || (u2Port >= IPIF_MAX_LOGICAL_IFACES))
        {
            return;
        }
    }

    if (u2Port == IPIF_INVALID_INDEX)
    {
        IPFWD_DS_LOCK ();
        pIpCxt->Ip_stats.u4In_rcvs = 0;
        pIpCxt->Ip_stats.u4HCIn_rcvs = 0;
        pIpCxt->Ip_stats.u4InOctets = 0;
        pIpCxt->Ip_stats.u4HCInOctets = 0;
        pIpCxt->Ip_stats.u4In_hdr_err = 0;
        pIpCxt->Ip_stats.u4In_no_routes = 0;
        pIpCxt->Ip_stats.u4In_addr_err = 0;
        pIpCxt->Ip_stats.u4Forw_attempts = 0;
        pIpCxt->Ip_stats.u4Bad_proto = 0;
        pIpCxt->Ip_stats.u4In_Trun_Pkts = 0;
        pIpCxt->Ip_stats.u4HCInForwDgrams = 0;
        pIpCxt->Ip_stats.u4Reasm_reqs = 0;
        pIpCxt->Ip_stats.u4Reasm_oks = 0;
        pIpCxt->Ip_stats.u4Reasm_fails = 0;
        pIpCxt->Ip_stats.u4InDiscards = 0;
        pIpCxt->Ip_stats.u4In_deliveries = 0;
        pIpCxt->Ip_stats.u4HCIn_deliveries = 0;
        pIpCxt->Ip_stats.u4Out_requests = 0;
        pIpCxt->Ip_stats.u4HCOut_requests = 0;
        pIpCxt->Ip_stats.u4OutNoRoutes = 0;
        pIpCxt->Ip_stats.u4OutForwds = 0;
        pIpCxt->Ip_stats.u4HCOutForwds = 0;
        pIpCxt->Ip_stats.u4OutDiscards = 0;
        pIpCxt->Ip_stats.u4Out_frag_Reqs = 0;
        pIpCxt->Ip_stats.u4OutFragOKs = 0;
        pIpCxt->Ip_stats.u4Frag_fails = 0;
        pIpCxt->Ip_stats.u4Frag_creates = 0;
        pIpCxt->Ip_stats.u4OutTrans = 0;
        pIpCxt->Ip_stats.u4HCOutTrans = 0;
        pIpCxt->Ip_stats.u4OutOctets = 0;
        pIpCxt->Ip_stats.u4HCOutOctets = 0;
        pIpCxt->Ip_stats.u4InMcastPkts = 0;
        pIpCxt->Ip_stats.u4HCInMcastPkts = 0;
        pIpCxt->Ip_stats.u4InMcastOctets = 0;
        pIpCxt->Ip_stats.u4HCInMcastOctets = 0;
        pIpCxt->Ip_stats.u4OutMcastPkts = 0;
        pIpCxt->Ip_stats.u4HCInBcastPkts = 0;
        pIpCxt->Ip_stats.u4OutMcastOctets = 0;
        pIpCxt->Ip_stats.u4HCOutMcastOctets = 0;
        pIpCxt->Ip_stats.u4In_bcasts = 0;
        pIpCxt->Ip_stats.u4HCInBcastPkts = 0;
        pIpCxt->Ip_stats.u4OutBcastPkts = 0;
        pIpCxt->Ip_stats.u4HCOutBcastPkts = 0;
        IPFWD_DS_UNLOCK ();

    }

    else
    {
        IPFWD_DS_LOCK ();
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_rcvs = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_hdr_err = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_no_routes = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_addr_err = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Bad_proto = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_Trun_Pkts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInForwDgrams = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_reqs = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_oks = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_fails = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InDiscards = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_deliveries = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOut_requests = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutNoRoutes = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutForwds = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutForwds = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutDiscards = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_frag_Reqs = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutFragOKs = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_fails = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_creates = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutTrans = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutTrans = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInMcastPkts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMcastOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInMcastOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMulticastPkts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutHCMCastPkts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMcastOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutMcastOctets = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InHCBCastPkts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts = 0;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutHCBCastPkts = 0;
        IPFWD_DS_UNLOCK ();
    }
    return;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxClearIcmpStats
 *
 *    DESCRIPTION      : Clears the ICMPStatistics
 *
 *    INPUT            : u4ContextId.
 *
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 **************************************************************************/
VOID
IPvxClearIpv4IcmpStats (UINT4 u4ContextId)
{

    tIpCxt             *pIpCxt = NULL;

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return;
    }
    IPFWD_DS_LOCK ();
    MEMSET (pIpCxt->Icmp_stats.u4Out, 0, (sizeof (UINT4) * (ICMP_TYPES + 1)));
    MEMSET (pIpCxt->Icmp_stats.u4In, 0, (sizeof (UINT4) * (ICMP_TYPES + 1)));
    pIpCxt->Icmp_err.u4Cksum = 0;
    pIpCxt->Icmp_err.u4Bcast = 0;
    pIpCxt->Icmp_err.u4Type = 0;
    pIpCxt->Icmp_err.u4Loop = 0;
    IPFWD_DS_UNLOCK ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxiTstPopulateIpv4SystemStats
 *
 *    DESCRIPTION      : Populate the IPSystemStatistics for testing
 *
 *    INPUT            : u4ContextId.
 *                     
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS
 *
 **************************************************************************/
VOID
IPvxTstPopulateIpv4SystemStats (UINT4 u4ContextId, INT4 i4Ipv4IfIdx)
{
    tIpCxt             *pIpCxt = NULL;
    INT4                i4RetVal = 0;
    UINT2               u2Port = IPIF_INVALID_INDEX;
    UINT4               u4Value = 5;

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return;
    }
    if (i4Ipv4IfIdx != IPVX_INVALID_IFIDX)
    {
        i4RetVal = IpGetPortFromIfIndex ((UINT4) i4Ipv4IfIdx, &u2Port);
        if ((i4RetVal != IP_SUCCESS) || (u2Port >= IPIF_MAX_LOGICAL_IFACES))
        {
            return;
        }
    }

    if (u2Port == IPIF_INVALID_INDEX)
    {
        IPFWD_DS_LOCK ();
        pIpCxt->Ip_stats.u4In_rcvs = u4Value;
        pIpCxt->Ip_stats.u4HCIn_rcvs = u4Value;
        pIpCxt->Ip_stats.u4InOctets = u4Value;
        pIpCxt->Ip_stats.u4HCInOctets = u4Value;
        pIpCxt->Ip_stats.u4In_hdr_err = u4Value;
        pIpCxt->Ip_stats.u4In_no_routes = u4Value;
        pIpCxt->Ip_stats.u4In_addr_err = u4Value;
        pIpCxt->Ip_stats.u4Forw_attempts = u4Value;
        pIpCxt->Ip_stats.u4Bad_proto = u4Value;
        pIpCxt->Ip_stats.u4In_Trun_Pkts = u4Value;
        pIpCxt->Ip_stats.u4HCInForwDgrams = u4Value;
        pIpCxt->Ip_stats.u4Reasm_reqs = u4Value;
        pIpCxt->Ip_stats.u4Reasm_oks = u4Value;
        pIpCxt->Ip_stats.u4Reasm_fails = u4Value;
        pIpCxt->Ip_stats.u4InDiscards = u4Value;
        pIpCxt->Ip_stats.u4In_deliveries = u4Value;
        pIpCxt->Ip_stats.u4HCIn_deliveries = u4Value;
        pIpCxt->Ip_stats.u4Out_requests = u4Value;
        pIpCxt->Ip_stats.u4HCOut_requests = u4Value;
        pIpCxt->Ip_stats.u4OutNoRoutes = u4Value;
        pIpCxt->Ip_stats.u4OutForwds = u4Value;
        pIpCxt->Ip_stats.u4HCOutForwds = u4Value;
        pIpCxt->Ip_stats.u4OutDiscards = u4Value;
        pIpCxt->Ip_stats.u4Out_frag_Reqs = u4Value;
        pIpCxt->Ip_stats.u4OutFragOKs = u4Value;
        pIpCxt->Ip_stats.u4Frag_fails = u4Value;
        pIpCxt->Ip_stats.u4Frag_creates = u4Value;
        pIpCxt->Ip_stats.u4OutTrans = u4Value;
        pIpCxt->Ip_stats.u4HCOutTrans = u4Value;
        pIpCxt->Ip_stats.u4OutOctets = u4Value;
        pIpCxt->Ip_stats.u4HCOutOctets = u4Value;
        pIpCxt->Ip_stats.u4InMcastPkts = u4Value;
        pIpCxt->Ip_stats.u4HCInMcastPkts = u4Value;
        pIpCxt->Ip_stats.u4InMcastOctets = u4Value;
        pIpCxt->Ip_stats.u4HCInMcastOctets = u4Value;
        pIpCxt->Ip_stats.u4OutMcastPkts = u4Value;
        pIpCxt->Ip_stats.u4HCInBcastPkts = u4Value;
        pIpCxt->Ip_stats.u4OutMcastOctets = u4Value;
        pIpCxt->Ip_stats.u4HCOutMcastOctets = u4Value;
        pIpCxt->Ip_stats.u4In_bcasts = u4Value;
        pIpCxt->Ip_stats.u4HCInBcastPkts = u4Value;
        pIpCxt->Ip_stats.u4OutBcastPkts = u4Value;
        pIpCxt->Ip_stats.u4HCOutBcastPkts = u4Value;
        IPFWD_DS_UNLOCK ();

    }

    else
    {
        IPFWD_DS_LOCK ();
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_rcvs = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_hdr_err = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_no_routes = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_addr_err = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Bad_proto = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_Trun_Pkts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInForwDgrams = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_reqs = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_oks = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_fails = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InDiscards = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_deliveries = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOut_requests = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutNoRoutes = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutForwds = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutForwds = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutDiscards = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_frag_Reqs = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutFragOKs = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_fails = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_creates = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutTrans = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutTrans = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInMcastPkts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMcastOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInMcastOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMulticastPkts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutHCMCastPkts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMcastOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutMcastOctets = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InHCBCastPkts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts = u4Value;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutHCBCastPkts = u4Value;
        IPFWD_DS_UNLOCK ();
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IPvxTstPopulateIpv4IcmpStats 
 *
 *    DESCRIPTION      : Clears the ICMPStatistics
 *
 *    INPUT            : u4ContextId.
 *
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 **************************************************************************/
VOID
IPvxTstPopulateIpv4IcmpStats (UINT4 u4ContextId)
{

    tIpCxt             *pIpCxt = NULL;
    UINT4               u4Value = 5;
    INT4                i4Count = 0;

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return;
    }
    IPFWD_DS_LOCK ();

    for (i4Count = 0; i4Count < ICMP_TYPES; i4Count++)
    {
        pIpCxt->Icmp_stats.u4Out[i4Count] = u4Value;
        pIpCxt->Icmp_stats.u4In[i4Count] = u4Value;
    }
    pIpCxt->Icmp_err.u4Cksum = u4Value;
    pIpCxt->Icmp_err.u4Bcast = u4Value;
    pIpCxt->Icmp_err.u4Type = u4Value;
    pIpCxt->Icmp_err.u4Loop = u4Value;
    IPFWD_DS_UNLOCK ();
    return;
}


 /***************************************************************************************
 *                                                                                      *
 *    FUNCTION NAME    : IPvxSetProxyArpAdminStatus                                       *
 *                                                                                      *
 *    DESCRIPTION      : This functions sets the Proxy Arp Admin Status for the given   *
 *                       Interface                                                      *
 *                                                                                      *
 *    INPUT            : i4FsIpifIndex, i4SetValFsIpifProxyArpAdminStatus               *               
 *                                                                                      *
 *    OUTPUT           : None                                                           *
 *                                                                                      *
 *    RETURNS          : None                                                           *
 *                                                                                      *
 ***************************************************************************************/
INT1
IPvxSetProxyArpAdminStatus (INT4 i4FsIpifIndex, INT4 i4SetValFsIpifProxyArpAdminStatus)
{
    UINT2               u2Index;

    tIP_INTERFACE       InterfaceId;    /* Interface index  */


    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_CONFIG_SET_PROXY_ARP_ADMIN_STATUS (u2Index,
                                                (UINT1)
                                                i4SetValFsIpifProxyArpAdminStatus);

        return (IP_SUCCESS);
    }
  
    return (IP_FAILURE);
}

/*-------------------------------------------------------------------+
 *
 * Function           : IPvxHandleIpv4IfEnableStatus
 *
 * AKA                : IPV4 Enabling  Function
 *
 * Input(s)           : i4Ipv4IfIndex, i4Ipv4EnableStatus
 *
 * Output(s)          : None.
 *
 * Returns            : IP_SUCCESS - event posted successully.
 *                    : IP_FAILURE
 *
 * Action :
 * This procedure is called to post a self event to IP for enabling/disabling
 * IPV4 on a ip interface.
 +--------------------------------------------------------------------*/
INT4
IPvxHandleIpv4IfEnableStatus (INT4 i4Ipv4IfIndex, INT4 i4Ipv4EnableStatus)
{
    tIfaceQMsg         *pIfaceQMsg = NULL;

    if ((pIfaceQMsg =
         (tIfaceQMsg *) IP_ALLOCATE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.
                                               IfacePoolId)) == NULL)
    {
        return IP_FAILURE;
    }

    pIfaceQMsg->u2IfIndex = (UINT2)i4Ipv4IfIndex;

    if (i4Ipv4EnableStatus == IPVX_IPV4_ENABLE_STATUS_UP)
    {
        pIfaceQMsg->u1MsgType = IPV4_ENABLE;
    }

    else
    {
         pIfaceQMsg->u1MsgType = IPV4_DISABLE;
    }


    if (OsixQueSend (gIpGlobalInfo.IpCfaIfQId, (UINT1 *) &pIfaceQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IfacePoolId, pIfaceQMsg);
        return IP_FAILURE;
    }

    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_IPV4_ENABLE_EVENT);
    return IP_SUCCESS;
}
/*****************************************************************************/
/*                                                                           */
/* Function Name    : IPvxDeleteIpv4LeakedStaticRouteInCxt                   */
/*                                                                           */
/* Description      : This function is invoked to delete all leaked static   */
/*                    routes when vrf-route-leaking is disabled globally     */
/*                                                                           */
/* Input Parameters : i1Protocol   - Protocol ID                             */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  IPVX_SUCCESS/IPVX_FAILURE                             */
/*****************************************************************************/
INT4
IPvxDeleteIpv4LeakedStaticRouteInCxt (INT1 i1Protocol, UINT4 u4VcId, UINT2 u2IfIndex)
{
    INT4                i4RetVal = IP_FAILURE;
    INT4                i4OutCome = 0;
    INT4                i4IpRouteIfIndex = 0;
    UINT4               u4IpRouteMetric1 = 0;
    UINT4               u4FsIpRouteNextDest = 0;
    UINT4               u4FsIpRouteNextMask = 0;
    UINT4               u4FsIpRouteNextHopNext = 0;
    UINT4               u4Cnt = 0;
    tRtInfo            *pRt = NULL;
    tRtInfo            *pNextRt = NULL;
    tRtInfo            *pAltRt = NULL;
    tRtInfo             InRtInfo;
    INT1                i1GetNextFlag = FALSE;
    INT1                i1FoundFlag = FALSE;
    INT1                i1NextCxtFlag = FALSE;
    UINT4               u4NextCxtId = 0;
    UINT4               u4CfaIndex = 0;
    UINT4               u4NextRtDestType = 0;
    INT1                ai1IfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1NetAddress[MAX_ADDR_LEN];

    tSNMP_OCTET_STRING_TYPE *pCurrDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextDestAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrNextHopAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextNextHopAddr = NULL;
    tSNMP_OID_TYPE     *pCurrRoutePolicy = NULL;
    tSNMP_OID_TYPE     *pNextRoutePolicy = NULL;
    UINT4               u4CurrRtDestType = INET_ADDR_TYPE_IPV4;
    UINT4               u4TempCxtId = 0;
    UINT4               u4CurrPrefixLength = 0;
    UINT4               u4NextPrefixLength = 0;
    UINT4               u4CurrNextHopType = INET_ADDR_TYPE_IPV4;
    UINT4               u4NextNextHopType = INET_ADDR_TYPE_IPV4;
    tIpConfigInfo       IpIfInfo;
    INT4                i4RoutePreference = IP_ZERO;
    UINT4               u4ContextId = 0;
    if (IpAllocateMemory
        (&pNextDestAddr, &pNextNextHopAddr, &pNextRoutePolicy, &pCurrDestAddr,
         &pCurrNextHopAddr, &pCurrRoutePolicy) != OSIX_SUCCESS)
    {
        return IPVX_FAILURE;
    }

    MEMSET (&IpIfInfo, 0, sizeof (IpIfInfo));

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));

    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    MEMSET (ai1IfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
    
    /* 1. Get first prefix. */
    if (u4VcId == VCM_INVALID_VC)
    {
        if (VcmGetFirstActiveL3Context (&u4VcId) == VCM_FAILURE)
        {
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return IPVX_SUCCESS;
        }
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetFirstIndexFsMIStdInetCidrRouteTable
            ((INT4 *) &u4NextCxtId,
             (INT4 *) &u4NextRtDestType, pNextDestAddr,
             &u4NextPrefixLength, pNextRoutePolicy,
             (INT4 *) &u4NextNextHopType, pNextNextHopAddr);
        RTM_PROT_LOCK ();

    }
    else
    {
        RTM_PROT_UNLOCK ();
        i4OutCome = nmhGetNextIndexFsMIStdInetCidrRouteTable
            ((INT4) u4VcId, (INT4 *) &u4NextCxtId,
             (INT4) u4CurrRtDestType, (INT4 *) &u4NextRtDestType,
             pCurrDestAddr, pNextDestAddr,
             u4CurrPrefixLength, &u4NextPrefixLength,
             pCurrRoutePolicy, pNextRoutePolicy,
             (INT4) u4CurrNextHopType, (INT4 *) &u4NextNextHopType,
             pCurrNextHopAddr, pNextNextHopAddr);
        RTM_PROT_LOCK ();
        if (u4VcId != u4NextCxtId)
        {
            /*No entries exist for this context */
            IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr,
                                pNextRoutePolicy, pCurrDestAddr,
                                pCurrNextHopAddr, pCurrRoutePolicy);
            return IPVX_SUCCESS;
        }
    }

    if (i4OutCome == SNMP_FAILURE)
    {
        IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                            pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);

        return (IPVX_SUCCESS);
    }

    IP_OCTETSTRING_TO_INTEGER ((pNextDestAddr), u4FsIpRouteNextDest);
    u4FsIpRouteNextMask = CfaGetCidrSubnetMask (u4NextPrefixLength);
    InRtInfo.u4DestNet = u4FsIpRouteNextDest;
    InRtInfo.u4DestMask = u4FsIpRouteNextMask;
    RtmApiGetBestRouteEntryInCxt (u4NextCxtId, InRtInfo, &pNextRt);

    do /* untill we have prefixes in routing table */
    {
        /* pRt is the current prefix to be processed,
         * pNextRt is the subsequent prefix to be processed */
        pRt = pNextRt;
        /* Get next Prefix and the route info for preferred protocol 
         * since deletion will alter the addresses pointed */
        i1NextCxtFlag = FALSE;


        MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
        InRtInfo.u4DestNet =u4FsIpRouteNextDest;
        InRtInfo.u4DestMask = u4FsIpRouteNextMask;

        i4RetVal = RtmApiGetNextBestRouteEntryInCxt(u4NextCxtId,
                                    InRtInfo, &pNextRt);


        u4TempCxtId = u4NextCxtId;
        while (i4RetVal == IP_FAILURE)
        {
            u4ContextId = u4TempCxtId;
            if (VcmGetNextActiveL3Context (u4ContextId, &u4TempCxtId)
                    == VCM_FAILURE)
            {
                i1GetNextFlag = FALSE;
                break;
            }

            MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
            i4RetVal = RtmApiGetNextBestRouteEntryInCxt(u4TempCxtId,
                                                 InRtInfo, &pNextRt);

            i1NextCxtFlag = TRUE;
        }

        if ((pRt != NULL) && (i1Protocol == pRt->u2RtProto))
        {
            do /* untill we have an alternate routes for above prefix */
            {
                u4Cnt++;
                u4FsIpRouteNextDest = pRt->u4DestNet;
                u4FsIpRouteNextMask = pRt->u4DestMask;
                u4FsIpRouteNextHopNext = pRt->u4NextHop;
                u4NextRtDestType = INET_ADDR_TYPE_IPV4;
                IP_INTEGER_TO_OCTETSTRING (pRt->u4DestNet, (pNextDestAddr));
                u4NextPrefixLength =
                    CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
                u4NextNextHopType = INET_ADDR_TYPE_IPV4;

                IP_INTEGER_TO_OCTETSTRING (pRt->u4NextHop,
                        (pNextNextHopAddr));
                RTM_PROT_UNLOCK ();
                nmhGetFsMIStdInetCidrRouteIfIndex
                    ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                     pNextDestAddr, u4NextPrefixLength,
                     pNextRoutePolicy, (INT4) u4NextNextHopType,
                     pNextNextHopAddr, &i4IpRouteIfIndex);

                nmhGetFsMIStdInetCidrRouteMetric1
                    ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                     pNextDestAddr, u4NextPrefixLength,
                     pNextRoutePolicy, (INT4) u4NextNextHopType,
                     pNextNextHopAddr, (INT4 *) &u4IpRouteMetric1);


                IpFwdTblGetObjectInCxt(u4NextCxtId,
                        u4FsIpRouteNextDest,
                        u4FsIpRouteNextMask,
                        IP_ZERO,
                        u4FsIpRouteNextHopNext,
                        INVALID_PROTO,
                        &i4RoutePreference,
                        IPFWD_ROUTE_PREFERENCE);
                RTM_PROT_LOCK ();

                if (IpIfInfo.u4Addr == 0)
                {
                    /*router learned on unnumbered interface */
                    u4FsIpRouteNextHopNext = 0;
                }
                VcmGetContextIdFromCfaIfIndex ((UINT4) i4IpRouteIfIndex, 
                        &u4ContextId);
                /* Check for alterate route for the prefix */
                pAltRt = pRt->pNextAlternatepath;
                if ((pAltRt != NULL) && (pAltRt->i4Metric1 == (INT4) u4IpRouteMetric1))
                {
                    i1FoundFlag = TRUE;
                }
                else
                {
                    i1FoundFlag = FALSE;
                }
                if ((u4ContextId != u4NextCxtId) &&
                        (pRt->u1LeakRoute == RTM_VRF_LEAKED_ROUTE))
                {
                    /* Destroy the Leaked Static routes */
                    RTM_PROT_UNLOCK ();
                    if (u2IfIndex == CFA_INVALID_INDEX)
                    {
                        nmhSetFsMIStdInetCidrRouteStatus
                            ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                             pNextDestAddr, u4NextPrefixLength,
                             pNextRoutePolicy, (INT4) u4NextNextHopType,
                             pNextNextHopAddr, IPFWD_DESTROY);
                    }
                    else
                    {
                        if (NetIpv4GetPortFromIfIndex ((UINT4) i4IpRouteIfIndex, &u4CfaIndex) == NETIPV4_SUCCESS)
                        {
                            if (u2IfIndex == (UINT2)u4CfaIndex)
                            {
                    nmhSetFsMIStdInetCidrRouteStatus
                        ((INT4) u4NextCxtId, (INT4) u4NextRtDestType,
                         pNextDestAddr, u4NextPrefixLength,
                         pNextRoutePolicy, (INT4) u4NextNextHopType,
                         pNextNextHopAddr, IPFWD_DESTROY);
                            }
                        }
                    }
                    RTM_PROT_LOCK ();
                }
                if (i1FoundFlag == TRUE)
                {
                    pRt = pAltRt;

                }
            }
            while (i1FoundFlag == TRUE);    /* alt path for pfx exists */
        }

        /* Populate the route info with the next prefix obtained */
        if (i4RetVal == IP_SUCCESS)
        {
            i1GetNextFlag = TRUE;
            u4FsIpRouteNextDest = pNextRt->u4DestNet;
            u4FsIpRouteNextMask = pNextRt->u4DestMask;
            u4FsIpRouteNextHopNext = pNextRt->u4NextHop;
            u4NextRtDestType = INET_ADDR_TYPE_IPV4;
            IP_INTEGER_TO_OCTETSTRING (pNextRt->u4DestNet, (pNextDestAddr));
            u4NextPrefixLength = CfaGetCidrSubnetMaskIndex (pNextRt->u4DestMask);
            u4NextNextHopType = INET_ADDR_TYPE_IPV4;
            IP_INTEGER_TO_OCTETSTRING (pNextRt->u4NextHop, (pNextNextHopAddr));
        }
        /* If next prefix is in a different context, then use the new context */
        if (i1NextCxtFlag == TRUE)
        {
            u4NextCxtId = u4TempCxtId;
        }
    }
    while (i1GetNextFlag == TRUE);    /* prefixes in RT */
    IpDeAllocateMemory (pNextDestAddr, pNextNextHopAddr, pNextRoutePolicy,
                        pCurrDestAddr, pCurrNextHopAddr, pCurrRoutePolicy);

    return (IPVX_SUCCESS);

}
#endif /* IP_WANTED */

#endif /* __IP4_IPVX_C__ */

/****************************   End of the File ***************************/
