/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: netipidxsz.c,v 1.1 2015/10/05 11:02:41 siva Exp $
 *
 * Description: This file contains functions that are associated
 *              with L2VpnIndex Table Manager.
 ********************************************************************/

#define _NETIPIDXSZ_C

#include "netipidxsz.h"
#include "netipidxgl.h"

extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
INT4  NetipIndexmgrSizingMemCreateMemPools()
{
    UINT4 u4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < INDEXMGR_MAX_SIZING_ID; i4SizingId++) {
        u4RetVal = MemCreateMemPool( 
                          FsNETIPINDEXMGRSizingParams[i4SizingId].u4StructSize,
                          FsNETIPINDEXMGRSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(NETIPINDEXMGRMemPoolIds[ i4SizingId]));
        if( u4RetVal == MEM_FAILURE) {
            NetipIndexmgrSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   NetipIndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
      IssSzRegisterModuleSizingParams( pu1ModName, FsNETIPINDEXMGRSizingParams); 
      return OSIX_SUCCESS; 
}


VOID  NetipIndexmgrSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < INDEXMGR_MAX_SIZING_ID; i4SizingId++) {
        if(NETIPINDEXMGRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( NETIPINDEXMGRMemPoolIds[ i4SizingId] );
            NETIPINDEXMGRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
