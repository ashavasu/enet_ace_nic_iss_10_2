/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxiputl.c,v 1.24 2015/05/14 12:53:32 siva Exp $
 *
 * Description: Contains routines to GET STDIP/STDTCP mib objects.
 *              These routines are called by nmh routines of LNXIP.    
 *             (IP stats, ICMP stats, TCP stats, UDP stats, etc..)   
 *
 *******************************************************************/
#ifndef __LNXIPUTL_C__
#define __LNXIPUTL_C__

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "lnxiputl.h"
#include "ipvx.h"
#include "rmgr.h"

/* structure to store the /proc/net/snmp file information */
struct
{
    UINT4               u4LineNo;
    UINT4               u4MibGroup;
    UINT4               u4LineLen;
}
FileFormat[] =
{
    {
    1, LNX_IP_MIB_OBJECTS, LNX_IP_STATS_HDR_LEN}
    ,
    {
    2, LNX_IP_MIB_OBJECTS, LNX_MAX_LINE_LEN}
    ,
    {
    3, LNX_ICMP_MIB_OBJECTS, LNX_ICMP_STATS_HDR_LEN}
    ,
    {
    4, LNX_ICMP_MIB_OBJECTS, LNX_MAX_LINE_LEN}
    ,
    {
    5, LNX_ICMP_MIB_OBJECTS, LNX_ICMP_STATS_MSG_HDR_LEN}
    ,
    {
    6, LNX_ICMP_MIB_OBJECTS, LNX_MAX_LINE_LEN}
    ,
    {
    7, LNX_TCP_MIB_OBJECTS, LNX_TCP_STATS_HDR_LEN}
    ,
    {
    8, LNX_TCP_MIB_OBJECTS, LNX_MAX_LINE_LEN}
    ,
    {
    9, LNX_UDP_MIB_OBJECTS, LNX_UDP_STATS_HDR_LEN}
    ,
    {
    10, LNX_UDP_MIB_OBJECTS, LNX_MAX_LINE_LEN}
    ,
    {
    11, LNX_UDP_MIB_OBJECTS, LNX_UDP_STATS_LITE_HDR_LEN}
    ,
    {
    12, LNX_UDP_MIB_OBJECTS, LNX_MAX_LINE_LEN}
    ,
    {
    0, 0, 0}
};

extern UINT4 IssGetMgmtPortFromNvRam PROTO ((VOID));

/****************************************************************************
 Function    : LnxIpGetObject 

 Description : Function used to get the Object value.

 Input       : u4ObjectName - Name of the Object
                
 Output      : *pu4RetVal - Value of the object 
                
 Returns     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT1
LnxIpGetObject (UINT4 u4ObjectName, UINT4 *pu4RetVal)
{

    FILE               *pFd;
    UINT1               au1Line[LNX_MAX_LINE_LEN];
    UINT1              *pu1Stats, *pu1Start = au1Line;
    UINT4               u4Len;
    UINT4               u4LineCount = 0;
    UINT4               u4ObjectGroup;
    INT1                i1RetVal = SNMP_SUCCESS;

    if ((u4ObjectName > 0) && (u4ObjectName <= LNX_IP_STATS_MAX_OBJECTS))
    {
        u4ObjectGroup = LNX_IP_MIB_OBJECTS;
    }
    else if (u4ObjectName <= (LNX_IP_STATS_MAX_OBJECTS +
                              LNX_ICMP_STATS_MAX_OBJECTS))
    {
        u4ObjectGroup = LNX_ICMP_MIB_OBJECTS;
    }
    else if (u4ObjectName <= (LNX_IP_STATS_MAX_OBJECTS +
                              LNX_ICMP_STATS_MAX_OBJECTS +
                              LNX_TCP_STATS_MAX_OBJECTS))
    {
        u4ObjectGroup = LNX_TCP_MIB_OBJECTS;
    }
    else if (u4ObjectName <= (LNX_IP_STATS_MAX_OBJECTS +
                              LNX_ICMP_STATS_MAX_OBJECTS +
                              LNX_TCP_STATS_MAX_OBJECTS +
                              LNX_UDP_STATS_MAX_OBJECTS))
    {
        u4ObjectGroup = LNX_UDP_MIB_OBJECTS;
    }
    else
    {
        return (SNMP_FAILURE);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    system("cat /proc/net/snmp > Local_net_snmp");
    pFd = FOPEN ("Local_net_snmp", "r");
#else 
   pFd = FOPEN (LNXIP_STATS_FILE_NAME, "r");
#endif

    if (!pFd)
    {
        perror ("Cannot open file /proc/net/snmp ...\n");
        return (SNMP_FAILURE);
    }

    /*
     * skip header, but make sure it's the length we expect...
     */
    while (fgets ((CHR1 *) au1Line, sizeof (au1Line), pFd) != NULL)
    {
        u4LineCount++;
        u4Len = STRLEN (au1Line);
        /*
         * This file provides the statistics for each systemstats.
         * Read in each Line in turn, isolate the systemstats name
         * and retrieve (or create) the corresponding data.
         */
        pu1Start = (UINT1 *) fgets ((CHR1 *) au1Line, sizeof (au1Line), pFd);
        u4LineCount++;

        if ((pu1Start) &&
            (u4ObjectGroup == FileFormat[u4LineCount - 1].u4MibGroup))
        {
            u4Len = STRLEN (au1Line);
            if (au1Line[u4Len - 1] == '\n')
            {
                au1Line[u4Len - 1] = '\0';
            }

            while (*pu1Start && *pu1Start == ' ')
            {
                pu1Start++;
            }

            if ((!*pu1Start)
                || ((pu1Stats = (UINT1 *) strrchr ((CHR1 *) pu1Start, ':')) ==
                    NULL))
            {
                /* systemstats data format error */
                i1RetVal = SNMP_FAILURE;
                break;
            }

            *pu1Stats++ = 0;    /* null terminate name */
            while (*pu1Stats == ' ')    /* skip spaces before stats */
            {
                pu1Stats++;
            }

            i1RetVal = LnxIpParseLine (u4ObjectName, u4ObjectGroup,
                                       pu1Stats, pu4RetVal);
            break;
        }
    }
    fclose (pFd);
    unlink("Local_net_snmp");
    return (i1RetVal);
}

/****************************************************************************
 Function    : LnxIpParseLine 

 Description : Function parse the line read from the file

 Input       : u4ObjectName - Name of the object
               u4ObjectGroup - Group to which the object belongs to
               *pu1Stats - Pointer to the line read from the file
                
 Output      : *pu4RetVal - Value of the object obtained 
                
 Returns     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT1
LnxIpParseLine (UINT4 u4ObjectName, UINT4 u4ObjectGroup,
                UINT1 *pu1Stats, UINT4 *pu4RetVal)
{
    UINT4               au4ScanVals[LNXIP_MAX_OBJECTS_COUNT];
    UINT4               u4ScanCount;
    INT1                i1ArrayIdx = -1;

    /*
     * OK - we've now got (or created) the data structure for
     *      this systemstats, including any "static" information.
     * Now parse the rest of the line (i.e. starting from 'stats')
     *      to extract the relevant statistics.
     */

    MEMSET (au4ScanVals, 0x0, sizeof (au4ScanVals));
    u4ScanCount = sscanf ((CHR1 *) pu1Stats,
                          "%d %d %d %d %d %d %d %d %d %d"
                          "%d %d %d %d %d %d %d %d %d %d"
                          "%d %d %d %d %d %d",
                          &au4ScanVals[0], &au4ScanVals[1], &au4ScanVals[2],
                          &au4ScanVals[3], &au4ScanVals[4], &au4ScanVals[5],
                          &au4ScanVals[6], &au4ScanVals[7], &au4ScanVals[8],
                          &au4ScanVals[9], &au4ScanVals[10], &au4ScanVals[11],
                          &au4ScanVals[12], &au4ScanVals[13], &au4ScanVals[14],
                          &au4ScanVals[15], &au4ScanVals[16], &au4ScanVals[17],
                          &au4ScanVals[18], &au4ScanVals[19], &au4ScanVals[20],
                          &au4ScanVals[21], &au4ScanVals[22], &au4ScanVals[23],
                          &au4ScanVals[24], &au4ScanVals[25]);
    UNUSED_PARAM (u4ScanCount);
    if (u4ObjectGroup == LNX_IP_MIB_OBJECTS)
    {
        i1ArrayIdx = u4ObjectName;
    }
    else if (u4ObjectGroup == LNX_ICMP_MIB_OBJECTS)
    {
        i1ArrayIdx = u4ObjectName - LNX_IP_STATS_MAX_OBJECTS;
    }
    else if (u4ObjectGroup == LNX_TCP_MIB_OBJECTS)
    {
        i1ArrayIdx = u4ObjectName - (LNX_IP_STATS_MAX_OBJECTS +
                                     LNX_ICMP_STATS_MAX_OBJECTS);
    }
    else if (u4ObjectGroup == LNX_UDP_MIB_OBJECTS)
    {
        i1ArrayIdx = u4ObjectName - (LNX_IP_STATS_MAX_OBJECTS +
                                     LNX_ICMP_STATS_MAX_OBJECTS +
                                     LNX_TCP_STATS_MAX_OBJECTS);
    }

    if (i1ArrayIdx != -1)
    {
        *pu4RetVal = au4ScanVals[i1ArrayIdx - 1];
        return (SNMP_SUCCESS);
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    : LnxIpSetForwarding 

 Description : Enable/Disable Ip Forwarding

 Input       : u1Status - Status to be set
                
OutPut       :  None
 Returns     :  IP_SUCCESS/IP_FAILURE
****************************************************************************/
INT1
LnxIpSetForwarding (INT4 i4SetVal)
{

    FILE               *pFd;
    UINT1               au1Str[2];
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4OobIfIndex;

    pFd = FOPEN ("/proc/sys/net/ipv4/ip_forward", "r+");
    if (pFd == NULL)
    {
        perror ("File open error /proc/sys/net/ipv4/ip_forward\r\n");
        return (IP_FAILURE);
    }

    SPRINTF ((CHR1 *) au1Str, "%d", (INT2) i4SetVal);

    if (fputs ((CHR1 *) au1Str, pFd) < 0)
    {
        perror ("File write error /proc/sys/net/ipv4/ip_forward\r\n");
        fclose (pFd);
        return (IP_FAILURE);
    }

    fclose (pFd);

    if (IssGetMgmtPortFromNvRam () == TRUE)
    {
        u4OobIfIndex = CfaGetDefaultRouterIfIndex ();
        if (CfaGetIfInfo (u4OobIfIndex, &CfaIfInfo) == CFA_FAILURE)
        {
            return (IP_FAILURE);
        }
        if (CFA_OOB_MGMT_INTF_ROUTING == FALSE)
        {
            /* Disable interface specific IP forwarding on OOB interface, 
               when routing over the oob is not wanted */
            if (LnxIpSetIfForwarding (CfaIfInfo.au1IfName,
                                      IP_FORW_DISABLE) == IP_FAILURE)
            {
                return (IP_FAILURE);
            }
        }
    }

    return IP_SUCCESS;
}

/****************************************************************************
 Function    : LnxIpGetForwarding 

 Description : Function used to GET IpForwarding object

 Input       : u1Status - Status to be set
                
OutPut       :  None
 Returns     :  IP_SUCCESS/IP_FAILURE
****************************************************************************/
INT1
LnxIpGetForwarding (UINT4 *pu4IpForwarding)
{
    /* ipForwarding mib object value get is done from the proc file
       /proc/net/snmp. 
       When ipForwarding is enabled, GET returns 1 (i.e., IP_FORW_ENABLE) 
       When ipForwarding is disabled, GET returns 2 (i.e., IP_FORW_DISABLE)
     */
    return (LnxIpGetObject (LNX_IP_FORWARDING, pu4IpForwarding));
}

/****************************************************************************
 Function    : LnxIpSetIfForwarding 

 Description : Enable/Disable Interface specific Ip Forwarding

 Input       : u1Status - Status to be set
                
 OutPut      : None

 Returns     : IP_SUCCESS/IP_FAILURE
****************************************************************************/
INT1
LnxIpSetIfForwarding (UINT1 *pu1IfName, INT4 i4SetVal)
{
    FILE               *pFd;
    UINT1               au1Str[2];
    UINT1               au1FileName[50];
    INT4                i4Status = LNX_IP_FORW_DISABLE;

    if (i4SetVal == IP_FORW_ENABLE)
    {
        i4Status = LNX_IP_FORW_ENABLE;
    }
    else if (i4SetVal == IP_FORW_DISABLE)
    {
        i4Status = LNX_IP_FORW_DISABLE;
    }

    MEMSET (au1FileName, 0, sizeof (au1FileName));
    SPRINTF ((CHR1 *) au1FileName, "/proc/sys/net/ipv4/conf/%s/forwarding",
             pu1IfName);

    pFd = FOPEN ((CHR1 *) au1FileName, "r+");
    if (pFd == NULL)
    {
        perror
            ("File open error /proc/sys/net/ipv4/conf/if_name/forwarding\r\n");
        return (IP_FAILURE);
    }

    SPRINTF ((CHR1 *) au1Str, "%d", (INT2) i4Status);

    if (fputs ((CHR1 *) au1Str, pFd) < 0)
    {
        perror
            ("File write error /proc/sys/net/ipv4/conf/if_name/forwarding\r\n");
        fclose (pFd);
        return (IP_FAILURE);
    }

    fclose (pFd);

    return IP_SUCCESS;
}

/****************************************************************************
 Function    : TcpTableIndexCompare    

 Description : Function used to get the next entry from the file /proc/net/tcp
               given an entry.

 Input       : pTcpConnEntry1
               pTcpConnEntry2
                
 Output      : LNX_LESSER LNX_GREATER or LNX_EQUAL 
                
 Returns     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
TcpTableIndexCompare (tTcpConnEntry * pTcpConnEntry1,
                      tTcpConnEntry * pTcpConnEntry2)
{
    /*Comparing Local Address type */
    if (pTcpConnEntry1->i4LocalAddrType < pTcpConnEntry2->i4LocalAddrType)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->i4LocalAddrType > pTcpConnEntry2->i4LocalAddrType)
    {
        return LNX_GREATER;
    }
    /*Comparing Local IP */
    if (pTcpConnEntry1->u4LocalIp < pTcpConnEntry2->u4LocalIp)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->u4LocalIp > pTcpConnEntry2->u4LocalIp)
    {
        return LNX_GREATER;
    }

    /*else comparing Local Port */
    if (pTcpConnEntry1->u2LocalPort < pTcpConnEntry2->u2LocalPort)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->u2LocalPort > pTcpConnEntry2->u2LocalPort)
    {
        return LNX_GREATER;
    }
    /*Comparing Remote Address type */
    if (pTcpConnEntry1->i4RemoteAddrType < pTcpConnEntry2->i4RemoteAddrType)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->i4RemoteAddrType >
             pTcpConnEntry2->i4RemoteAddrType)
    {
        return LNX_GREATER;
    }

    /*else comparing Remote IP */
    if (pTcpConnEntry1->u4RemoteIp < pTcpConnEntry2->u4RemoteIp)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->u4RemoteIp > pTcpConnEntry2->u4RemoteIp)
    {
        return LNX_GREATER;
    }

    /*else comparing Remote Port */
    if (pTcpConnEntry1->u2RemotePort < pTcpConnEntry2->u2RemotePort)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->u2RemotePort > pTcpConnEntry2->u2RemotePort)
    {
        return LNX_GREATER;
    }

    return LNX_EQUAL;
}

/****************************************************************************
 Function    : TcpConnTableGetNextEntry

 Description : Function used to get the next entry from the file /proc/net/tcp
               given an entry.

 Input       : pTcpConnEntry
               pNextTcpConnEntry
                
 Output      : *pNextTcpConnEntry
                
 Returns     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
TcpConnTableGetNextEntry (tTcpConnEntry * pTcpConnEntry,
                          tTcpConnEntry * pNextTcpConnEntry)
{
    FILE               *pFd;
    PRIVATE CHR1        ac1TcpConnEntry[LNX_MAX_ENTRY_SIZE];
    UINT1               au1Index[LNX_MAX_INDEX_SIZE];
    UINT1               au1Local[LNX_MAX_IP_PORT_SIZE];
    UINT1               au1Remote[LNX_MAX_IP_PORT_SIZE];
    INT1                i1FirstLn = FALSE;
    INT1                i1Found = FALSE;
    CHR1               *token;
    tTcpConnEntry       TcpConnEntry;
    tTcpConnEntry       tmpTcpConnEntry = *pTcpConnEntry;

    if ((pFd = FOPEN ("/proc/net/tcp", "r")) == NULL)
    {
        perror ("Cannot open file /proc/net/tcp ...\n");
        return SNMP_FAILURE;
    }

    /* Skipping the first line */
    i1FirstLn = TRUE;
    while (fgets (ac1TcpConnEntry, sizeof (ac1TcpConnEntry), pFd) != NULL)
    {
        if (i1FirstLn == TRUE)
        {
            i1FirstLn = FALSE;
            continue;
        }

        /* Extracting LocalIp LocalPort RemoteIp RemotePort and TcpState
         * from the file "/proc/net/tcp" */

        sscanf (ac1TcpConnEntry, "%s %s %s ", au1Index, au1Local, au1Remote);

        token = strtok ((CHR1 *) au1Local, ":");
        TcpConnEntry.u4LocalIp =
            OSIX_NTOHL ((UINT4) strtoll (token, NULL, LNX_HEX_BASE));

        TcpConnEntry.i4LocalAddrType = IPV4_ADD_TYPE;

        token = strtok (NULL, ":");
        TcpConnEntry.u2LocalPort = strtol (token, NULL, LNX_HEX_BASE);

        token = strtok ((CHR1 *) au1Remote, ":");
        TcpConnEntry.u4RemoteIp =
            OSIX_NTOHL ((UINT4) strtoll (token, NULL, LNX_HEX_BASE));

        TcpConnEntry.i4RemoteAddrType = IPV4_ADD_TYPE;

        token = strtok (NULL, ":");
        TcpConnEntry.u2RemotePort = strtol (token, NULL, LNX_HEX_BASE);
        TcpConnEntry.u4ProcessID = LNX_ZERO;
        if (i1Found == FALSE)
        {
            if (TcpTableIndexCompare (&TcpConnEntry, pTcpConnEntry) ==
                LNX_GREATER)
            {
                tmpTcpConnEntry = TcpConnEntry;
                i1Found = TRUE;
            }
        }
        if (i1Found == TRUE)
        {
            if ((TcpTableIndexCompare (&TcpConnEntry, pTcpConnEntry) ==
                 LNX_GREATER)
                && (TcpTableIndexCompare (&TcpConnEntry, &tmpTcpConnEntry) ==
                    LNX_LESSER))
            {
                tmpTcpConnEntry = TcpConnEntry;
            }
        }
    }
    fclose (pFd);

    if (i1Found == TRUE)
    {
        *pNextTcpConnEntry = tmpTcpConnEntry;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    : TcpListTableGetNextEntry

 Description : Function used to get the next entry from the file /proc/net/tcp
               given an entry.

 Input       : pTcpConnEntry
               pNextTcpConnEntry
                
 Output      : *pNextTcpConnEntry
                
 Returns     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
TcpListTableGetNextEntry (tTcpConnEntry * pTcpConnEntry,
                          tTcpConnEntry * pNextTcpConnEntry)
{
    FILE               *pFd;
    PRIVATE CHR1        ac1TcpConnEntry[LNX_MAX_ENTRY_SIZE];
    UINT1               au1Index[LNX_MAX_INDEX_SIZE];
    UINT1               au1Local[LNX_MAX_IP_PORT_SIZE];
    UINT1               au1Remote[LNX_MAX_IP_PORT_SIZE];
    INT1                i1FirstLn = FALSE;
    INT1                i1Found = FALSE;
    CHR1               *token;
    INT4                i4State;
    tTcpConnEntry       TcpConnEntry;
    tTcpConnEntry       tmpTcpConnEntry = *pTcpConnEntry;

    if ((pFd = FOPEN ("/proc/net/tcp", "r")) == NULL)
    {
        perror ("Cannot open file /proc/net/tcp ...\n");
        return SNMP_FAILURE;
    }

    /* Skipping the first line */
    i1FirstLn = TRUE;
    while (fgets (ac1TcpConnEntry, sizeof (ac1TcpConnEntry), pFd) != NULL)
    {
        if (i1FirstLn == TRUE)
        {
            i1FirstLn = FALSE;
            continue;
        }

        /* Extracting LocalIp LocalPort RemoteIp RemotePort and TcpState
         * from the file "/proc/net/tcp" */

        sscanf (ac1TcpConnEntry, "%s%s%s%d", au1Index, au1Local,
                au1Remote, &i4State);
        /*Conn should be listen state */
        if (i4State != 2)
        {
            continue;
        }
        token = strtok ((CHR1 *) au1Local, ":");
        TcpConnEntry.u4LocalIp =
            OSIX_NTOHL (strtoll (token, NULL, LNX_HEX_BASE));

        TcpConnEntry.i4LocalAddrType = IPV4_ADD_TYPE;

        token = strtok (NULL, ":");
        TcpConnEntry.u2LocalPort = strtol (token, NULL, LNX_HEX_BASE);
        TcpConnEntry.u4ProcessID = LNX_ZERO;
        if (i1Found == FALSE)
        {
            if (TcpListTableIndexCompare (&TcpConnEntry, pTcpConnEntry)
                == LNX_GREATER)
            {
                tmpTcpConnEntry = TcpConnEntry;
                i1Found = TRUE;
            }
        }
        if (i1Found == TRUE)
        {
            if ((TcpListTableIndexCompare (&TcpConnEntry, pTcpConnEntry)
                 == LNX_GREATER)
                && (TcpTableIndexCompare (&TcpConnEntry, &tmpTcpConnEntry)
                    == LNX_LESSER))
            {
                tmpTcpConnEntry = TcpConnEntry;
            }
        }
    }
    fclose (pFd);

    if (i1Found == TRUE)
    {
        *pNextTcpConnEntry = tmpTcpConnEntry;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    : TcpListTableIndexCompare    

 Description : Function used to get the next entry from the file /proc/net/tcp
               given an entry.

 Input       : pTcpConnEntry1
               pTcpConnEntry2
                
 Output      : LNX_LESSER LNX_GREATER or LNX_EQUAL 
                
 Returns     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
TcpListTableIndexCompare (tTcpConnEntry * pTcpConnEntry1,
                          tTcpConnEntry * pTcpConnEntry2)
{
    /*Comparing Local Address type */
    if (pTcpConnEntry1->i4LocalAddrType < pTcpConnEntry2->i4LocalAddrType)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->i4LocalAddrType > pTcpConnEntry2->i4LocalAddrType)
    {
        return LNX_GREATER;
    }
    /*Comparing Local IP */
    if (pTcpConnEntry1->u4LocalIp < pTcpConnEntry2->u4LocalIp)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->u4LocalIp > pTcpConnEntry2->u4LocalIp)
    {
        return LNX_GREATER;
    }

    /*else comparing Local Port */
    if (pTcpConnEntry1->u2LocalPort < pTcpConnEntry2->u2LocalPort)
    {
        return LNX_LESSER;
    }
    else if (pTcpConnEntry1->u2LocalPort > pTcpConnEntry2->u2LocalPort)
    {
        return LNX_GREATER;
    }
    return LNX_EQUAL;
}

/****************************************************************************
 Function    : UdpTableGetNextEntry

 Description : Function used to get the next entry from the file /proc/net/udp
               given an entry.

 Input       : *pUdpEntry - Name of the Object
                
 Output      : *pNextUdpEntry - Name of the Object
                
 Returns     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
UdpTableGetNextEntry (tUdpEntry * pUdpEntry, tUdpEntry * pNextUdpEntry)
{
    FILE               *pFd;
    PRIVATE UINT1       au1UdpEntry[LNX_MAX_ENTRY_SIZE];
    UINT1               au1Index[LNX_MAX_INDEX_SIZE];
    UINT1               au1Local[LNX_MAX_IP_PORT_SIZE];
    UINT1               au1Remote[LNX_MAX_IP_PORT_SIZE];
    INT1                i1FirstLn = FALSE;
    INT1                i1Found = FALSE;
    CHR1               *token;
    tUdpEntry           tmpUdpEntry;
    tUdpEntry           UdpEntry;

    MEMCPY (&tmpUdpEntry, pUdpEntry, LNX_UDP_TABLE_INDEX_SIZE);

    if ((pFd = FOPEN ("/proc/net/udp", "r")) == NULL)
    {
        perror ("Cannot open file /proc/net/udp ...\n");
        return SNMP_FAILURE;
    }

    /* Skipping the first line */
    i1FirstLn = TRUE;
    while (fgets ((CHR1 *) au1UdpEntry, sizeof (au1UdpEntry), pFd) != NULL)
    {
        if (i1FirstLn == TRUE)
        {
            i1FirstLn = FALSE;
            continue;
        }
        /*Extract the Local IP,Local Port,Remote IP and Remote Port from 
         * /proc/net/udp file*/
        sscanf ((CHR1 *) au1UdpEntry, "%s%s%s", au1Index, au1Local, au1Remote);
        token = STRTOK (au1Local, ":");
        UdpEntry.u4LocalIp = OSIX_NTOHL (strtoll (token, NULL, LNX_HEX_BASE));
        token = STRTOK (NULL, ":");
        UdpEntry.u2LocalPort = strtol (token, NULL, LNX_HEX_BASE);
        UdpEntry.i4LocalAddrType = IPV4_ADD_TYPE;

        token = STRTOK (au1Remote, ":");
        UdpEntry.u4RemoteIp = OSIX_NTOHL (strtoll (token, NULL, LNX_HEX_BASE));
        token = STRTOK (NULL, ":");
        UdpEntry.u2Remoteport = strtol (token, NULL, LNX_HEX_BASE);
        UdpEntry.i4RemoteAddrType = IPV4_ADD_TYPE;
        UdpEntry.u4Instance = LNX_ZERO;

        if (MEMCMP (&UdpEntry, pUdpEntry, LNX_UDP_TABLE_INDEX_SIZE) > 0)
        {
            MEMCPY (&tmpUdpEntry, &UdpEntry, LNX_UDP_TABLE_INDEX_SIZE);
            i1Found = TRUE;
        }

        if (i1Found == TRUE)
        {
            if ((MEMCMP (&UdpEntry,
                         pUdpEntry, LNX_UDP_TABLE_INDEX_SIZE) > 0) &&
                (MEMCMP (&UdpEntry,
                         &tmpUdpEntry, LNX_UDP_TABLE_INDEX_SIZE) < 0))
            {
                MEMCPY (&tmpUdpEntry, &UdpEntry, LNX_UDP_TABLE_INDEX_SIZE);
            }
        }
    }
    fclose (pFd);
    if (i1Found == TRUE)
    {
        MEMCPY (pNextUdpEntry, &tmpUdpEntry, LNX_UDP_TABLE_INDEX_SIZE);
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/***************************************************************/
/*  Function Name   : UdpGetFirstCxtId                         */
/*  Description     : Gets the UDP First Context ID            */
/*  Input(s)        : None                                     */
/*  Output(s)       : pu4UdpCxtId - UDP context ID             */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpGetFirstCxtId (UINT4 *pu4UdpCxtId)
{
    *pu4UdpCxtId = IP_DEFAULT_CONTEXT;
    return SNMP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : UdpGetNextCxtId                          */
/*  Description     : Gets the next UDP Context ID             */
/*  Input(s)        : u4UdpCxtId - UDP context ID              */
/*  Output(s)       : pu4UdpCxtId - UDP context ID             */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpGetNextCxtId (UINT4 u4UdpCxtId, UINT4 *pu4NextUdpCxtId)
{
    UNUSED_PARAM (u4UdpCxtId);
    UNUSED_PARAM (pu4NextUdpCxtId);
    return SNMP_FAILURE;
}

/*********************************************************************
 * Function           : IpIsCxtExist
 *
 * Description        : This function checks if a context exist in IP with the
 *                      specified context id.
 *
 * Input(s)           : u4ContextId
 *
 * Output(s)          : None
 *
 * Returns            : IP_TRUE/IP_FALSE
 *
*********************************************************************/
INT1
IpIsCxtExist (UINT4 u4ContextId)
{
    if (u4ContextId == IP_DEFAULT_CONTEXT)
    {
        return IP_TRUE;
    }
    return IP_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpSetCxt                                               */
/*                                                                           */
/* Description  : This function sets the global context variable             */
/*                                                                           */
/* Input        :  u4ContextId : The ip context identifier.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
INT4
UtilIpSetCxt (UINT4 u4ContextId)
{
    return (UtilIpvxSetCxt (u4ContextId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpReleaseCxt                                           */
/*                                                                           */
/* Description  : This function clear the global context variable            */
/*                                                                           */
/* Input        :  u4ContextId : The context identifier.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
VOID
UtilIpReleaseCxt (VOID)
{
    UtilIpvxReleaseCxt ();
    return;
}

/***************************************************************/
/*  Function Name   : UdpIsValidCxtId                          */
/*  Description     :Checks whether the UDP Context ID is Valid*/
/*  Input(s)        : u4UdpCxtId - UDP context ID              */
/*  Output(s)       : None                                     */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpIsValidCxtId (UINT4 u4UdpContextId)
{
    if (u4UdpContextId == IP_DEFAULT_CONTEXT)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4ForScalars

 Input       :  i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID

 Description :  This function performs Inc Msr functionality for
                FsIpv4 scalars.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4Scalars (INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetVal));

#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4SetVal);
#endif
    return IP_SUCCESS;
}

#endif
