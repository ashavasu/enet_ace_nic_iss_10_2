
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnpwr.c,v 1.11 2016/10/03 10:34:43 siva Exp $
 *
 * Description: This file contains the NP wrappers for IP module.
 *****************************************************************************/

#ifndef __IP_NPWR_C__
#define __IP_NPWR_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : IpNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIpNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
IpNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIpNpModInfo       *pIpNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIpNpModInfo = &(pFsHwNp->IpNpModInfo);

    if (NULL == pIpNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_NP_IP_INIT:
        {
            FsNpIpInit ();
            break;
        }
        case FS_NP_OSPF_INIT:
        {
            u1RetVal = FsNpOspfInit ();
            break;
        }
        case FS_NP_OSPF_DE_INIT:
        {
            FsNpOspfDeInit ();
            break;
        }
        case FS_NP_DHCP_SRV_INIT:
        {
            u1RetVal = FsNpDhcpSrvInit ();
            break;
        }
        case FS_NP_DHCP_SRV_DE_INIT:
        {
            FsNpDhcpSrvDeInit ();
            break;
        }
        case FS_NP_DHCP_RLY_INIT:
        {
            u1RetVal = FsNpDhcpRlyInit ();
            break;
        }
        case FS_NP_DHCP_RLY_DE_INIT:
        {
            FsNpDhcpRlyDeInit ();
            break;
        }
        case FS_NP_RIP_INIT:
        {
            u1RetVal = FsNpRipInit ();
            break;
        }
        case FS_NP_RIP_DE_INIT:
        {
            FsNpRipDeInit ();
            break;
        }
        case FS_NP_IPV4_CREATE_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4CreateIpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4CreateIpInterface;
            u1RetVal =
                FsNpIpv4CreateIpInterface (pEntry->u4VrId, pEntry->pu1IfName,
                                           pEntry->u4CfaIfIndex,
                                           pEntry->u4IpAddr,
                                           pEntry->u4IpSubNetMask,
                                           pEntry->u2VlanId,
                                           pEntry->au1MacAddr);
            break;
        }
        case FS_NP_IPV4_CREATE_L3SUB_INTERFACE:
        {
            tIpNpWrFsNpIpv4CreateL3SubInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4CreateL3SubInterface;
            u1RetVal =
                (UINT1)(FsNpIpv4CreateL3SubInterface (pEntry->u4VrId, pEntry->pu1IfName,
                                           pEntry->u4CfaIfIndex,
                                           pEntry->u4IpAddr,
                                           pEntry->u4IpSubNetMask,
                                           pEntry->u2VlanId,
                                           pEntry->au1MacAddr,
                                           pEntry->u4ParentIfIndex));
            break;
        }

        case FS_NP_IPV4_DELETE_L3SUB_INTERFACE:
        {
            tIpNpWrFsNpIpv4DeleteL3SubInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteL3SubInterface;
            u1RetVal =
                (UINT1)(FsNpIpv4DeleteL3SubInterface (pEntry->u4IfIndex));
            break;
        }

        case FS_NP_IPV4_MODIFY_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4ModifyIpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4ModifyIpInterface;
            u1RetVal =
                FsNpIpv4ModifyIpInterface (pEntry->u4VrId, pEntry->pu1IfName,
                                           pEntry->u4CfaIfIndex,
                                           pEntry->u4IpAddr,
                                           pEntry->u4IpSubNetMask,
                                           pEntry->u2VlanId,
                                           pEntry->au1MacAddr);
            break;
        }
        case FS_NP_IPV4_DELETE_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4DeleteIpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteIpInterface;
            u1RetVal =
                FsNpIpv4DeleteIpInterface (pEntry->u4VrId, pEntry->pu1IfName,
                                           pEntry->u4IfIndex, pEntry->u2VlanId);
            break;
        }
        case FS_NP_IPV4_UPDATE_IP_INTERFACE_STATUS:
        {
            tIpNpWrFsNpIpv4UpdateIpInterfaceStatus *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4UpdateIpInterfaceStatus;
            u1RetVal =
                FsNpIpv4UpdateIpInterfaceStatus (pEntry->u4VrId,
                                                 pEntry->pu1IfName,
                                                 pEntry->u4CfaIfIndex,
                                                 pEntry->u4IpAddr,
                                                 pEntry->u4IpSubNetMask,
                                                 pEntry->u2VlanId,
                                                 pEntry->au1MacAddr,
                                                 pEntry->u4Status);
            break;
        }
        case FS_NP_IPV4_SET_FORWARDING_STATUS:
        {
            tIpNpWrFsNpIpv4SetForwardingStatus *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4SetForwardingStatus;
            u1RetVal =
                FsNpIpv4SetForwardingStatus (pEntry->u4VrId, pEntry->u1Status);
            break;
        }
        case FS_NP_IPV4_CLEAR_ROUTE_TABLE:
        {
            u1RetVal = FsNpIpv4ClearRouteTable ();
            break;
        }
        case FS_NP_IPV4_CLEAR_ARP_TABLE:
        {
            u1RetVal = FsNpIpv4ClearArpTable ();
            break;
        }
        case FS_NP_IPV4_UC_DEL_ROUTE:
        {
            tIpNpWrFsNpIpv4UcDelRoute *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcDelRoute;
            u1RetVal =
                FsNpIpv4UcDelRoute (pEntry->u4VrId, pEntry->u4IpDestAddr,
                                    pEntry->u4IpSubNetMask, pEntry->routeEntry,
                                    (int *) pEntry->pi4FreeDefIpB4Del);
            break;
        }
        case FS_NP_IPV4_ARP_ADD:
        {
            tIpNpWrFsNpIpv4ArpAdd *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpAdd;
            u1RetVal =
                FsNpIpv4ArpAdd (pEntry->u2VlanId, pEntry->u4IfIndex,
                                pEntry->u4IpAddr, pEntry->pMacAddr,
                                pEntry->pu1IfName, pEntry->i1State,
                                pEntry->pu4TblFull);
            break;
        }
        case FS_NP_IPV4_ARP_MODIFY:
        {
            tIpNpWrFsNpIpv4ArpModify *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpModify;
            u1RetVal =
                FsNpIpv4ArpModify (pEntry->u2VlanId, pEntry->u4IfIndex,
                                   pEntry->u4PhyIfIndex,
                                   pEntry->u4IpAddr, pEntry->pMacAddr,
                                   pEntry->pu1IfName, pEntry->i1State);
            break;
        }
        case FS_NP_IPV4_ARP_DEL:
        {
            tIpNpWrFsNpIpv4ArpDel *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpDel;
            u1RetVal =
                FsNpIpv4ArpDel (pEntry->u4IpAddr, pEntry->pu1IfName,
                                pEntry->i1State);
            break;
        }
        case FS_NP_IPV4_CHECK_HIT_ON_ARP_ENTRY:
        {
            tIpNpWrFsNpIpv4CheckHitOnArpEntry *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4CheckHitOnArpEntry;
            pEntry->u1HitBitStatus =
                FsNpIpv4CheckHitOnArpEntry (pEntry->u4IpAddress,
                                            pEntry->u1NextHopFlag);
            u1RetVal = FNP_SUCCESS;
            break;
        }
        case FS_NP_IPV4_SYNC_VLAN_AND_L3_INFO:
        {
            FsNpIpv4SyncVlanAndL3Info ();
            break;
        }
        case FS_NP_IPV4_UC_ADD_ROUTE:
        {
            tIpNpWrFsNpIpv4UcAddRoute *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcAddRoute;
            u1RetVal =
                FsNpIpv4UcAddRoute (pEntry->u4VrId, pEntry->u4IpDestAddr,
                                    pEntry->u4IpSubNetMask, 
#ifndef NPSIM_WANTED
                                    pEntry->pRouteEntry,
#else
                                    pEntry->routeEntry,
#endif
                                    pEntry->pbu1TblFull);
            break;
        }
        case FS_NP_IPV4_VRF_ARP_ADD:
        {
            tIpNpWrFsNpIpv4VrfArpAdd *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpAdd;
            u1RetVal =
                FsNpIpv4VrfArpAdd (pEntry->u4VrId, pEntry->u2VlanId,
                                   pEntry->u4IfIndex, pEntry->u4IpAddr,
                                   pEntry->pMacAddr, pEntry->pu1IfName,
                                   pEntry->i1State, pEntry->pu4TblFull);
            break;
        }
        case FS_NP_IPV4_VRF_ARP_MODIFY:
        {
            tIpNpWrFsNpIpv4VrfArpModify *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpModify;
            u1RetVal =
                FsNpIpv4VrfArpModify (pEntry->u4VrId, pEntry->u2VlanId,
                                      pEntry->u4IfIndex, pEntry->u4PhyIfIndex,
                                      pEntry->u4IpAddr, pEntry->pMacAddr,
                                      pEntry->pu1IfName, pEntry->i1State);
            break;
        }
        case FS_NP_IPV4_VRF_ARP_DEL:
        {
            tIpNpWrFsNpIpv4VrfArpDel *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpDel;
            u1RetVal =
                FsNpIpv4VrfArpDel (pEntry->u4VrId, pEntry->u4IpAddr,
                                   pEntry->pu1IfName, pEntry->i1State);
            break;
        }
        case FS_NP_IPV4_VRF_CHECK_HIT_ON_ARP_ENTRY:
        {
            tIpNpWrFsNpIpv4VrfCheckHitOnArpEntry *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfCheckHitOnArpEntry;
            pEntry->u1HitBitStatus =
                FsNpIpv4VrfCheckHitOnArpEntry (pEntry->u4VrId,
                                               pEntry->u4IpAddress,
                                               pEntry->u1NextHopFlag);
            u1RetVal = FNP_SUCCESS;
            break;
        }
        case FS_NP_IPV4_VRF_CLEAR_ARP_TABLE:
        {
            tIpNpWrFsNpIpv4VrfClearArpTable *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfClearArpTable;
            u1RetVal = FsNpIpv4VrfClearArpTable (pEntry->u4VrId);
            break;
        }
        case FS_NP_IPV4_VRF_GET_SRC_MOVED_IP_ADDR:
        {
            tIpNpWrFsNpIpv4VrfGetSrcMovedIpAddr *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfGetSrcMovedIpAddr;
            u1RetVal =
                FsNpIpv4VrfGetSrcMovedIpAddr (pEntry->pu4VrId,
                                              pEntry->pu4IpAddress);
            break;
        }
        case FS_NP_CFA_VRF_SET_DLF_STATUS:
        {
            tIpNpWrFsNpCfaVrfSetDlfStatus *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpCfaVrfSetDlfStatus;
            u1RetVal =
                FsNpCfaVrfSetDlfStatus (pEntry->u4VrfId, pEntry->u1Status);
            break;
        }
        case FS_NP_L3_IPV4_VRF_ARP_ADD:
        {
            tIpNpWrFsNpL3Ipv4VrfArpAdd *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpAdd;
            u1RetVal =
                FsNpL3Ipv4VrfArpAdd (pEntry->u4VrfId, pEntry->u4IfIndex,
                                     pEntry->u4IpAddr, pEntry->pMacAddr,
                                     pEntry->pu1IfName, pEntry->i1State,
                                     pEntry->pbu1TblFull);
            break;
        }
        case FS_NP_L3_IPV4_VRF_ARP_MODIFY:
        {
            tIpNpWrFsNpL3Ipv4VrfArpModify *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4VrfArpModify;
            u1RetVal =
                FsNpL3Ipv4VrfArpModify (pEntry->u4VrfId, pEntry->u4IfIndex,
                                        pEntry->u4IpAddr, pEntry->pMacAddr,
                                        pEntry->pu1IfName, pEntry->i1State);
            break;
        }
        case FS_NP_IPV4_VRF_ARP_GET:
        {
            tIpNpWrFsNpIpv4VrfArpGet *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGet;
            u1RetVal =
                FsNpIpv4VrfArpGet (pEntry->ArpNpInParam,
                                   pEntry->pArpNpOutParam);
            break;
        }
        case FS_NP_IPV4_VRF_ARP_GET_NEXT:
        {
            tIpNpWrFsNpIpv4VrfArpGetNext *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrfArpGetNext;
            u1RetVal =
                FsNpIpv4VrfArpGetNext (pEntry->ArpNpInParam,
                                       pEntry->pArpNpOutParam);
            break;
        }
#ifdef VRRP_WANTED
        case FS_NP_IPV4_VRRP_INTF_CREATE_WR:
        {
            tIpNpWrFsNpIpv4VrrpIntfCreateWr *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrrpIntfCreateWr;
            u1RetVal =
                FsNpIpv4VrrpIntfCreateWr (pEntry->u2VlanId, pEntry->u4IfIndex,
                                          pEntry->u4IpAddr, pEntry->au1MacAddr);
            break;
        }
        case FS_NP_IPV4_CREATE_VRRP_INTERFACE:
        {
            tIpNpWrFsNpIpv4CreateVrrpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4CreateVrrpInterface;
            u1RetVal =
                FsNpIpv4CreateVrrpInterface (pEntry->u2VlanId, pEntry->u4IpAddr,
                                             pEntry->au1MacAddr);
            break;
        }
        case FS_NP_IPV4_VRRP_INTF_DELETE_WR:
        {
            tIpNpWrFsNpIpv4VrrpIntfDeleteWr *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrrpIntfDeleteWr;
            u1RetVal =
                FsNpIpv4VrrpIntfDeleteWr (pEntry->u2VlanId, pEntry->u4IfIndex,
                                          pEntry->u4IpAddr, pEntry->au1MacAddr);
            break;
        }
        case FS_NP_IPV4_DELETE_VRRP_INTERFACE:
        {
            tIpNpWrFsNpIpv4DeleteVrrpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4DeleteVrrpInterface;
            u1RetVal =
                FsNpIpv4DeleteVrrpInterface (pEntry->u2VlanId, pEntry->u4IpAddr,
                                             pEntry->au1MacAddr);
            break;
        }
        case FS_NP_IPV4_GET_VRRP_INTERFACE:
        {
            tIpNpWrFsNpIpv4GetVrrpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetVrrpInterface;
            u1RetVal =
                FsNpIpv4GetVrrpInterface (pEntry->i4IfIndex, pEntry->i4VrId);
            break;
        }
        case FS_NP_IPV4_VRRP_INSTALL_FILTER:
        {
            u1RetVal = FsNpIpv4VrrpInstallFilter ();
            break;
        }
        case FS_NP_IPV4_VRRP_REMOVE_FILTER:
        {
            u1RetVal = FsNpIpv4VrrpRemoveFilter ();
            break;
        }
        case FS_NP_VRRP_HW_PROGRAM:
        {
            tIpNpWrFsNpVrrpHwProgram *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpVrrpHwProgram;
            u1RetVal =
                FsNpVrrpHwProgram (pEntry->u1NpAction, pEntry->pVrrpNwIntf);
            break;
        }
#endif /* VRRP_WANTED */
        case FS_NP_IPV4_IS_RT_PRESENT_IN_FAST_PATH:
        {
            tIpNpWrFsNpIpv4IsRtPresentInFastPath *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4IsRtPresentInFastPath;
            u1RetVal =
                FsNpIpv4IsRtPresentInFastPath (pEntry->u4DestAddr,
                                               pEntry->u4Mask);
            break;
        }
        case FS_NP_IPV4_GET_STATS:
        {
            tIpNpWrFsNpIpv4GetStats *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetStats;
            u1RetVal = FsNpIpv4GetStats (pEntry->i4StatType, pEntry->pu4RetVal);
            break;
        }
        case FS_NP_IPV4_VRM_ENABLE_VR:
        {
            tIpNpWrFsNpIpv4VrmEnableVr *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4VrmEnableVr;
            u1RetVal = FsNpIpv4VrmEnableVr (pEntry->u4VrId, pEntry->u1Status);
            break;
        }
        case FS_NP_IPV4_BIND_IF_TO_VR_ID:
        {
            tIpNpWrFsNpIpv4BindIfToVrId *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4BindIfToVrId;
            u1RetVal = FsNpIpv4BindIfToVrId (pEntry->u4IfIndex, pEntry->u4VrId);
            break;
        }
        case FS_NP_IPV4_GET_NEXT_HOP_INFO:
        {
            tIpNpWrFsNpIpv4GetNextHopInfo *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetNextHopInfo;
            u1RetVal =
                FsNpIpv4GetNextHopInfo (pEntry->u4VrId, pEntry->u4Dest,
                                        pEntry->u4DestMask,
                                        pEntry->pNextHopInfo);
            break;
        }
        case FS_NP_IPV4_UC_ADD_TRAP:
        {
            tIpNpWrFsNpIpv4UcAddTrap *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcAddTrap;
            u1RetVal =
                FsNpIpv4UcAddTrap (pEntry->u4VrId, pEntry->u4Dest,
                                   pEntry->u4DestMask, pEntry->nextHopInfo);
            break;
        }
        case FS_NP_IPV4_CLEAR_FOWARDING_TBL:
        {
            tIpNpWrFsNpIpv4ClearFowardingTbl *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4ClearFowardingTbl;
            u1RetVal = FsNpIpv4ClearFowardingTbl (pEntry->u4VrId);
            break;
        }
        case FS_NP_IPV4_GET_SRC_MOVED_IP_ADDR:
        {
            tIpNpWrFsNpIpv4GetSrcMovedIpAddr *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetSrcMovedIpAddr;
            u1RetVal = FsNpIpv4GetSrcMovedIpAddr (pEntry->pu4IpAddress);
            break;
        }
        case FS_NP_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4MapVlansToIpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4MapVlansToIpInterface;
            u1RetVal =
                FsNpIpv4MapVlansToIpInterface (pEntry->pPvlanMappingInfo);
            break;
        }
#ifdef NAT_WANTED
        case FS_NP_NAT_DISABLE_ON_INTF:
        {
            tIpNpWrFsNpNatDisableOnIntf *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpNatDisableOnIntf;
            FsNpNatDisableOnIntf (pEntry->i4Intf);
            break;
        }
        case FS_NP_NAT_ENABLE_ON_INTF:
        {
            tIpNpWrFsNpNatEnableOnIntf *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpNatEnableOnIntf;
            u1RetVal = FsNpNatEnableOnIntf (pEntry->i4Intf);
            break;
        }
#endif /* NAT_WANTED */
        case FS_NP_IPV4_INTF_STATUS:
        {
            tIpNpWrFsNpIpv4IntfStatus *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4IntfStatus;
            u1RetVal =
                FsNpIpv4IntfStatus (pEntry->u4VrId, pEntry->u4IfStatus,
                                    pEntry->pIpIntInfo);
            break;
        }
        case FS_NP_IPV4_ARP_GET:
        {
            tIpNpWrFsNpIpv4ArpGet *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpGet;
            u1RetVal =
                FsNpIpv4ArpGet (pEntry->ArpNpInParam, pEntry->pArpNpOutParam);
            break;
        }
        case FS_NP_IPV4_ARP_GET_NEXT:
        {
            tIpNpWrFsNpIpv4ArpGetNext *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4ArpGetNext;
            u1RetVal =
                FsNpIpv4ArpGetNext (pEntry->ArpNpInParam,
                                    pEntry->pArpNpOutParam);
            break;
        }
        case FS_NP_IPV4_UC_GET_ROUTE:
        {
            tIpNpWrFsNpIpv4UcGetRoute *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcGetRoute;
            u1RetVal =
                FsNpIpv4UcGetRoute (pEntry->RtmNpInParam,
                                    pEntry->pRtmNpOutParam);
            break;
        }
        case FS_NP_L3_IPV4_ARP_ADD:
        {
            tIpNpWrFsNpL3Ipv4ArpAdd *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4ArpAdd;
            u1RetVal =
                FsNpL3Ipv4ArpAdd (pEntry->u4IfIndex, pEntry->u4IpAddr,
                                  pEntry->pMacAddr, pEntry->pu1IfName,
                                  pEntry->i1State, pEntry->pbu1TblFull);
            break;
        }
        case FS_NP_L3_IPV4_ARP_MODIFY:
        {
            tIpNpWrFsNpL3Ipv4ArpModify *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpL3Ipv4ArpModify;
            u1RetVal =
                FsNpL3Ipv4ArpModify (pEntry->u4IfIndex, pEntry->u4IpAddr,
                                     pEntry->pMacAddr, pEntry->pu1IfName,
                                     pEntry->i1State);
            break;
        }
        case FS_NP_IPV4_L3_IP_INTERFACE:
        {
            tIpNpWrFsNpIpv4L3IpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4L3IpInterface;
            u1RetVal =
                FsNpIpv4L3IpInterface (pEntry->L3Action, pEntry->pFsNpL3IfInfo);
            break;
        }
		
        case FS_NP_IPV4_GET_ECMP_GROUPS:
        {
            tIpNpWrFsNpIpv4GetEcmpGroups *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIpv4GetEcmpGroups;
            u1RetVal = 
                (UINT1)FsNpIpv4GetEcmpGroups (pEntry->pu4EcmpGroups);
            break;
        }
		
#ifdef ISIS_WANTED
        case FS_NP_ISIS_HW_PROGRAM:
        {
            tIpNpWrFsNpIsisHwProgram *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpIsisHwProgram;
            u1RetVal = FsNpIsisHwProgram (pEntry->u1Status);
            break;
        }
#endif /* ISIS_WANTED */
#ifdef MBSM_WANTED
        case FS_NP_MBSM_IP_INIT:
        {
            tIpNpWrFsNpMbsmIpInit *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpInit;
            FsNpMbsmIpInit (pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_ISIS_HW_PROGRAM:
        {
            tIpNpWrFsNpMbsmIsisHwProgram *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIsisHwProgram;
            u1RetVal = FsNpMbsmIsisHwProgram (pEntry->pSlotInfo, pEntry->u1Status);
            break;
        }
        case FS_NP_MBSM_OSPF_INIT:
        {
            tIpNpWrFsNpMbsmOspfInit *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmOspfInit;
            u1RetVal = FsNpMbsmOspfInit (pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_DHCP_SRV_INIT:
        {
            tIpNpWrFsNpMbsmDhcpSrvInit *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmDhcpSrvInit;
            u1RetVal = FsNpMbsmDhcpSrvInit (pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_DHCP_RLY_INIT:
        {
            tIpNpWrFsNpMbsmDhcpRlyInit *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmDhcpRlyInit;
            u1RetVal = FsNpMbsmDhcpRlyInit (pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_CREATE_IP_INTERFACE:
        {
            tIpNpWrFsNpMbsmIpv4CreateIpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4CreateIpInterface;
            u1RetVal =
                FsNpMbsmIpv4CreateIpInterface (pEntry->u4VrId,
                                               pEntry->u4CfaIfIndex,
                                               pEntry->u4IpAddr,
                                               pEntry->u4IpSubNetMask,
                                               pEntry->u2VlanId,
                                               pEntry->au1MacAddr,
                                               pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_ARP_ADD:
        {
            tIpNpWrFsNpMbsmIpv4ArpAdd *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4ArpAdd;
            u1RetVal =
                FsNpMbsmIpv4ArpAdd (pEntry->u2VlanId, pEntry->u4IpAddr,
                                    pEntry->pMacAddr, pEntry->pbu1TblFull,
                                    pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_UC_ADD_ROUTE:
        {
            tIpNpWrFsNpMbsmIpv4UcAddRoute *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4UcAddRoute;
            u1RetVal =
                FsNpMbsmIpv4UcAddRoute (pEntry->u4VrId, pEntry->u4IpDestAddr,
                                        pEntry->u4IpSubNetMask,
#ifndef NPSIM_WANTED
                                        pEntry->pRouteEntry, 
#else
                                        pEntry->routeEntry, 
#endif
                                        pEntry->pbu1TblFull,
                                        pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_VRM_ENABLE_VR:
        {
            tIpNpWrFsNpMbsmIpv4VrmEnableVr *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrmEnableVr;
            u1RetVal =
                FsNpMbsmIpv4VrmEnableVr (pEntry->u4VrId, pEntry->u1Status,
                                         pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_BIND_IF_TO_VR_ID:
        {
            tIpNpWrFsNpMbsmIpv4BindIfToVrId *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4BindIfToVrId;
            u1RetVal =
                FsNpMbsmIpv4BindIfToVrId (pEntry->u4IfIndex, pEntry->u4VrId,
                                          pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_UC_ADD_TRAP:
        {
            tIpNpWrFsNpMbsmIpv4UcAddTrap *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4UcAddTrap;
            u1RetVal =
                FsNpMbsmIpv4UcAddTrap (pEntry->u4VrId, pEntry->u4Dest,
                                       pEntry->u4DestMask, pEntry->nextHopInfo,
                                       pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_MAP_VLANS_TO_IP_INTERFACE:
        {
            tIpNpWrFsNpMbsmIpv4MapVlansToIpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4MapVlansToIpInterface;
            u1RetVal =
                FsNpMbsmIpv4MapVlansToIpInterface (pEntry->pPvlanMappingInfo,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_VRF_ARP_ADD:
        {
            tIpNpWrFsNpMbsmIpv4VrfArpAdd *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrfArpAdd;
            u1RetVal =
                FsNpMbsmIpv4VrfArpAdd (pEntry->u4VrId, pEntry->u2VlanId,
                                       pEntry->u4IpAddr, pEntry->pMacAddr,
                                       pEntry->pbu1TblFull, pEntry->pSlotInfo);
            break;
        }
#ifdef VRRP_WANTED
        case FS_NP_MBSM_IPV4_CREATE_VRRP_INTERFACE:
        {
            tIpNpWrFsNpMbsmIpv4CreateVrrpInterface *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4CreateVrrpInterface;
            u1RetVal =
                FsNpMbsmIpv4CreateVrrpInterface (pEntry->u2VlanId,
                                                 pEntry->u4IpAddr,
                                                 pEntry->au1MacAddr,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_VRRP_INSTALL_FILTER:
        {
            tIpNpWrFsNpMbsmIpv4VrrpInstallFilter *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrrpInstallFilter;
            u1RetVal = FsNpMbsmIpv4VrrpInstallFilter (pEntry->pSlotInfo);
            break;
        }
#endif /* VRRP_WANTED */
        case FS_NP_MBSM_IPV4_ARP_ADDITION:
        {
            tIpNpWrFsNpMbsmIpv4ArpAddition *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4ArpAddition;
            u1RetVal =
                FsNpMbsmIpv4ArpAddition (pEntry->u2VlanId, pEntry->u4IpAddr,
                                         pEntry->pMacAddr, pEntry->pbu1TblFull,
                                         pEntry->u4IfIndex, pEntry->pu1IfName,
                                         pEntry->i1State, pEntry->pSlotInfo);
            break;
        }
        case FS_NP_MBSM_IPV4_VRF_ARP_ADDITION:
        {
            tIpNpWrFsNpMbsmIpv4VrfArpAddition *pEntry = NULL;
            pEntry = &pIpNpModInfo->IpNpFsNpMbsmIpv4VrfArpAddition;
            u1RetVal =
                FsNpMbsmIpv4VrfArpAddition (pEntry->u4VrId, pEntry->u2VlanId,
                                            pEntry->u4IpAddr, pEntry->pMacAddr,
                                            pEntry->pbu1TblFull,
                                            pEntry->u4IfIndex,
                                            pEntry->pu1IfName, pEntry->i1State,
                                            pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __IP_NPWR_C__ */
