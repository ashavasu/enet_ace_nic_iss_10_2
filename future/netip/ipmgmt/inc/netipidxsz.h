/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: netipidxsz.h,v 1.1 2015/10/05 11:02:40 siva Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/
#include "lr.h"
#include "cfa.h"
#include "l2iwf.h"
#include "ip.h"
#include "rtm.h"
#include "ipvx.h"
#include "fssocket.h"
#include "chrdev.h"
#include "netdev.h"
#include "vcm.h"

#include "netipidxmg.h"
extern tIndexMgrGrpInfo *gpNetipIndexMgrGrpInfo;
enum {
    MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID,
    MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID,
    MAX_INDEXMGR_INDEX_TBL_CHUNK_SIZING_ID,
    INDEXMGR_MAX_SIZING_ID
};


#ifdef  _NETIPIDXSZ_C
tMemPoolId NETIPINDEXMGRMemPoolIds[INDEXMGR_MAX_SIZING_ID];
INT4  NetipIndexmgrSizingMemCreateMemPools(VOID);
VOID  NetipIndexmgrSizingMemDeleteMemPools(VOID);
INT4  NetipIndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _NETIPIDXSZ_C  */
extern tMemPoolId NETIPINDEXMGRMemPoolIds[ ];
extern INT4  NetipIndexmgrSizingMemCreateMemPools(VOID);
extern VOID  NetipIndexmgrSizingMemDeleteMemPools(VOID);
extern INT4  NetipIndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _NETIPIDXSZ_C  */


#ifdef  _NETIPIDXSZ_C
tFsModSizingParams FsNETIPINDEXMGRSizingParams [] = {
{ "tIndexMgrChunkInfoSize", "MAX_INDEXMGR_INDEX_MGR_CHUNK", sizeof(tIndexMgrChunkInfoSize),MAX_INDEXMGR_INDEX_MGR_CHUNK, MAX_INDEXMGR_INDEX_MGR_CHUNK,0 },
{ "tIndexMgrGrpInfoSize", "MAX_INDEXMGR_INDEX_MGR_GRP", sizeof(tIndexMgrGrpInfoSize),MAX_INDEXMGR_INDEX_MGR_GRP, MAX_INDEXMGR_INDEX_MGR_GRP,0 },
{ "tIndexMgrChunkSize", "MAX_INDEXMGR_INDEX_TBL_CHUNK", sizeof(tIndexMgrChunkSize),MAX_INDEXMGR_INDEX_TBL_CHUNK, MAX_INDEXMGR_INDEX_TBL_CHUNK,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _NETIPIDXSZ_C  */
extern tFsModSizingParams FsNETIPINDEXMGRSizingParams [];
#endif /*  _NETIPIDXSZ_C  */

