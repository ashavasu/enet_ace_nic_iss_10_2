
#ifndef _STDTCPWRAP_H 
#define _STDTCPWRAP_H 
VOID RegisterSTDTCP(VOID);
INT4 TcpRtoAlgorithmGet (tSnmpIndex *, tRetVal *);
INT4 TcpRtoMinGet (tSnmpIndex *, tRetVal *);
INT4 TcpRtoMaxGet (tSnmpIndex *, tRetVal *);
INT4 TcpMaxConnGet (tSnmpIndex *, tRetVal *);
INT4 TcpActiveOpensGet (tSnmpIndex *, tRetVal *);
INT4 TcpPassiveOpensGet (tSnmpIndex *, tRetVal *);
INT4 TcpAttemptFailsGet (tSnmpIndex *, tRetVal *);
INT4 TcpEstabResetsGet (tSnmpIndex *, tRetVal *);
INT4 TcpCurrEstabGet (tSnmpIndex *, tRetVal *);
INT4 TcpInSegsGet (tSnmpIndex *, tRetVal *);
INT4 TcpOutSegsGet (tSnmpIndex *, tRetVal *);
INT4 TcpRetransSegsGet (tSnmpIndex *, tRetVal *);
INT4 TcpInErrsGet (tSnmpIndex *, tRetVal *);
INT4 TcpOutRstsGet (tSnmpIndex *, tRetVal *);
INT4 TcpConnStateTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 TcpConnStateSet (tSnmpIndex *, tRetVal *);
INT4 TcpConnStateGet (tSnmpIndex *, tRetVal *);
INT4 TcpConnLocalAddressGet (tSnmpIndex *, tRetVal *);
INT4 TcpConnLocalPortGet (tSnmpIndex *, tRetVal *);
INT4 TcpConnRemAddressGet (tSnmpIndex *, tRetVal *);
INT4 TcpConnRemPortGet (tSnmpIndex *, tRetVal *);

 /*  GetNext Function Prototypes */

INT4 GetNextIndexTcpConnTable( tSnmpIndex *, tSnmpIndex *);

INT4 TcpConnTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif
