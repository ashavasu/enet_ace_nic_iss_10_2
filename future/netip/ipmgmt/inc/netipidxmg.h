
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: netipidxmg.h,v 1.1 2015/10/05 11:02:40 siva Exp $
 *              
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *---------------------------------------------------------------------------
 *    FILE  NAME             : netipidxmg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : NETIP FLOWTABLE - Utility    
 *    MODULE NAME            : NETIP Flow Index Tbl Mgr. Flow Table Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                        
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains typedefintions, constant
 *                             definitions, global declarations that are
 *                             associated with Index Space (Group-Key) Mngr. 
 *-------------------------------------------------------------------------*/

#ifndef _NETIPIDXMG_H
#define _NETIPIDXMG_H

#define MAX_NETIPINDEXMGR_MAX_OF_CHUNKS_PER_GROUP 1
#define MAX_NETIPINDEXMGR_GRPS_SPRTD                11
#define MAX_INDEX_MGR_GROUP_INFO_SIZE    (MAX_NETIPINDEXMGR_GRPS_SPRTD * sizeof (tIndexMgrGrpInfo))
#define MAX_INDEX_MGR_CHUNK_INFO_SIZE    (MAX_NETIPINDEXMGR_MAX_OF_CHUNKS_PER_GROUP * \
                                      sizeof (tIndexMgrChunkInfo))
/*============================================*/   

/* debugs definition - dummy */   
#define INDEXMGR_SUPPRESS_WARNING(x) x = x; 

#define DBG_ERR_CRT          0xffffffff   
#define NETIP_FLOW_DBG_FLAG    0xffffffff

#define NETIP_MAX_FLOWS IPIF_MAX_LOGICAL_IFACES

#define NETIP_ONE 1
#define NETIP_ZERO 0
#define NETIP_SUCCESS 1
#define NETIP_FAILURE 0
#define NETIP_FLOW_DBG(u4Value, pu1Format) \
        if (u4Value == (u4Value & NETIP_FLOW_DBG_FLAG)) \
           UtlTrcLog (NETIP_FLOW_DBG_FLAG, u4Value, "NETIPFLOW", pu1Format)

#define NETIP_FLOW_DBG2(u4Value, pu1Format, Arg1, Arg2) \
        if (u4Value == (u4Value & NETIP_FLOW_DBG_FLAG)) \
         UtlTrcLog (NETIP_FLOW_DBG_FLAG, u4Value, "NETIPFLOW", pu1Format,Arg1,Arg2)        
/* debug definitions end */    
  
/*  Definition of memory type */
#define INDEXMGR_MEM_MODE         MEM_DEFAULT_MEMORY_TYPE

#define INDEXMGR_BYTE_BLOCK_SIZE  8 /* 1 byte = 8 bits */ 

#define INDEXMGR_SOME_AVAIL_BMAP  0xFFFFFFFF
         /* Mask to check if at least 1 bit of UINT4 is 1 */     

/*Helps in determining at least 1 bit is available(=1) in the UINT4 bitmap*/

#define INDEXMGR_GRP_FULL         1
#define INDEXMGR_GRP_VACANT       0

 /* Index Manager  semaphore name */
#define NETIP_INDEXMGR_SEM_NAME (UINT1 *)"NETIP"

#define MAX_NETIPINDEXMGR_CHUNK_SIZE                  (MAX_NETIPINDEXMGR_ENT_PER_CHUNK * 4)
#define MAX_NETIPINDEXMGR_ENT_PER_CHUNK                1536 /* UINT4 entries per chunk */

/* Typedefinitions used for the Group Key Manager. */
/* structure to hold "INDEX_CHUNK_SIZE" memory chunks holding bitmaps */
typedef struct IndexMgrChunkInfo
{
   UINT4          u4NumKeysAlloc; /* Number of keys allocated*/
                                  /* UINT2 is enough...yet to play-safe */ 
   UINT4          u4CurOffset;    /* Current offset in the chunk */
   UINT4         *pu4IndexChunk;  /* pointer to continuous memory chunk */
} tIndexMgrChunkInfo;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */

typedef struct _IndexMgrChunkInfoSize
{
   UINT1          au1IndexMgrChunkInfo[MAX_INDEX_MGR_CHUNK_INFO_SIZE];
}
tIndexMgrChunkInfoSize;

/* structure to hold information about an index-group */
typedef struct IndexMgrGrpInfo
{
   UINT4          u4NumChunksInGrp; /* Number of chunks in this group */
                                 /*  UINT2 suffices, yet to play-safe */
   UINT4          u4MaxChunksSprtd; /* Max chunks supported for this group*/
   UINT4          u4AvailChunkID; /* Chunk-ID wherefrom key-alloc is made */
   UINT4          u4TotalKeysAlloc; /* Total Keys allocated for this grp */
   tIndexMgrChunkInfo *pIndexChunkInfoTbl;
                                    /* Ptr to arr of chunk-info's */ 
} tIndexMgrGrpInfo; 


typedef struct _IndexMgrGrpTable
{
    CHR1         *pc1ModName;        /* Module Name to this group belongs to */
    CHR1         *pc1GrpName;        /* Group Name */

} tIndexMgrGrpTable;

/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _IndexMgrGrpInfoSize
{
   UINT1          au1IndexMgrGrpInfo[MAX_INDEX_MGR_GROUP_INFO_SIZE];
}
tIndexMgrGrpInfoSize;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _IndexMgrChunkSize
{
    UINT1         au1IndexMgrChunk[MAX_NETIPINDEXMGR_CHUNK_SIZE];
}
tIndexMgrChunkSize;


/* Function Prototypes */
/* External Functions : Available for calling by other modules. */
UINT1 NetipIndexMgrInitWithSem ARG_LIST ((VOID));
UINT1 NetipIndexMgrDeInit ARG_LIST ((VOID));

/* Internal functions : Internally called by this module only */
UINT4 NetipIndexMgrGetIndexBasedOnFlag ARG_LIST ((UINT1 u1GrpID,BOOL1 bFlag));
UINT4 NetipIndexMgrGetAvailableIndex ARG_LIST ((UINT1 u1GrpID));

UINT4 NetipIndexMgrSetIndexBasedOnFlag ARG_LIST ((UINT1 u1GrpID,UINT4 u4Index,BOOL1 bFlag));
UINT4 NetipIndexMgrSetIndex ARG_LIST ((UINT1 u1GrpID,UINT4 u4Index));
UINT4 NetipIndexMgrCheckIndex ARG_LIST ((UINT1 u1GrpID,UINT4 u4Index));
 
UINT1 NetipIndexMgrRelIndex ARG_LIST ((UINT1 u1GrpID, UINT4 u4Index));

UINT1 NetipIndexMgrInitGrps ARG_LIST ((VOID));
UINT1 NetipIndexMgrCheckGrpFull ARG_LIST ((UINT1 u1GrpID));
UINT1 NetipIndexMgrGetAvailByteBitPos 
  ARG_LIST ((UINT4 u4AvailIndexBitmap, UINT1 *pu1BytePos, UINT1 *pu1BitPos));
UINT1 NetipIndexMgrUpdateChunkOffset ARG_LIST ((UINT1 u1GrpID));  
/* Reference of SRM MEM Function */

#endif /*_NETIPIDXMG_H */
/*-------------------------------------------------------------------------
                        End of file netipidxmg.h                             
------------------------------------------------------------------------*/
