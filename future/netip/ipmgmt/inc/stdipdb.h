/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdipdb.h,v 1.4 2008/12/30 09:50:31 prabuc-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDIPDB_H
#define _STDIPDB_H

    /*
     * Remove this #ifdef when deleting the deprecated objects at 
     * standard ip mib to support backward comp.
     * This RegisterSTDIP is replaced by RegisterSTDIPvx,RegisterSTDTCP and
     * RegisterSTDUDP in futrure/netipvx 
     */
#ifdef REMOVE_ON_STDIP_DEPR_OBJ_DELETE

UINT1 IpAddrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpNetToMediaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IpCidrRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 UdpTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdip [] ={1,3,6,1,2,1,4};
tSNMP_OID_TYPE stdipOID = {7, stdip};


UINT4 IpForwarding [ ] ={1,3,6,1,2,1,4,1};
UINT4 IpDefaultTTL [ ] ={1,3,6,1,2,1,4,2};
UINT4 IpInReceives [ ] ={1,3,6,1,2,1,4,3};
UINT4 IpInHdrErrors [ ] ={1,3,6,1,2,1,4,4};
UINT4 IpInAddrErrors [ ] ={1,3,6,1,2,1,4,5};
UINT4 IpForwDatagrams [ ] ={1,3,6,1,2,1,4,6};
UINT4 IpInUnknownProtos [ ] ={1,3,6,1,2,1,4,7};
UINT4 IpInDiscards [ ] ={1,3,6,1,2,1,4,8};
UINT4 IpInDelivers [ ] ={1,3,6,1,2,1,4,9};
UINT4 IpOutRequests [ ] ={1,3,6,1,2,1,4,10};
UINT4 IpOutDiscards [ ] ={1,3,6,1,2,1,4,11};
UINT4 IpOutNoRoutes [ ] ={1,3,6,1,2,1,4,12};
UINT4 IpReasmTimeout [ ] ={1,3,6,1,2,1,4,13};
UINT4 IpReasmReqds [ ] ={1,3,6,1,2,1,4,14};
UINT4 IpReasmOKs [ ] ={1,3,6,1,2,1,4,15};
UINT4 IpReasmFails [ ] ={1,3,6,1,2,1,4,16};
UINT4 IpFragOKs [ ] ={1,3,6,1,2,1,4,17};
UINT4 IpFragFails [ ] ={1,3,6,1,2,1,4,18};
UINT4 IpFragCreates [ ] ={1,3,6,1,2,1,4,19};
UINT4 IpAdEntAddr [ ] ={1,3,6,1,2,1,4,20,1,1};
UINT4 IpAdEntIfIndex [ ] ={1,3,6,1,2,1,4,20,1,2};
UINT4 IpAdEntNetMask [ ] ={1,3,6,1,2,1,4,20,1,3};
UINT4 IpAdEntBcastAddr [ ] ={1,3,6,1,2,1,4,20,1,4};
UINT4 IpAdEntReasmMaxSize [ ] ={1,3,6,1,2,1,4,20,1,5};
UINT4 IpNetToMediaIfIndex [ ] ={1,3,6,1,2,1,4,22,1,1};
UINT4 IpNetToMediaPhysAddress [ ] ={1,3,6,1,2,1,4,22,1,2};
UINT4 IpNetToMediaNetAddress [ ] ={1,3,6,1,2,1,4,22,1,3};
UINT4 IpNetToMediaType [ ] ={1,3,6,1,2,1,4,22,1,4};
UINT4 IpRoutingDiscards [ ] ={1,3,6,1,2,1,4,23};
UINT4 IpCidrRouteNumber [ ] ={1,3,6,1,2,1,4,24,3};
UINT4 IpCidrRouteDest [ ] ={1,3,6,1,2,1,4,24,4,1,1};
UINT4 IpCidrRouteMask [ ] ={1,3,6,1,2,1,4,24,4,1,2};
UINT4 IpCidrRouteTos [ ] ={1,3,6,1,2,1,4,24,4,1,3};
UINT4 IpCidrRouteNextHop [ ] ={1,3,6,1,2,1,4,24,4,1,4};
UINT4 IpCidrRouteIfIndex [ ] ={1,3,6,1,2,1,4,24,4,1,5};
UINT4 IpCidrRouteType [ ] ={1,3,6,1,2,1,4,24,4,1,6};
UINT4 IpCidrRouteProto [ ] ={1,3,6,1,2,1,4,24,4,1,7};
UINT4 IpCidrRouteAge [ ] ={1,3,6,1,2,1,4,24,4,1,8};
UINT4 IpCidrRouteInfo [ ] ={1,3,6,1,2,1,4,24,4,1,9};
UINT4 IpCidrRouteNextHopAS [ ] ={1,3,6,1,2,1,4,24,4,1,10};
UINT4 IpCidrRouteMetric1 [ ] ={1,3,6,1,2,1,4,24,4,1,11};
UINT4 IpCidrRouteMetric2 [ ] ={1,3,6,1,2,1,4,24,4,1,12};
UINT4 IpCidrRouteMetric3 [ ] ={1,3,6,1,2,1,4,24,4,1,13};
UINT4 IpCidrRouteMetric4 [ ] ={1,3,6,1,2,1,4,24,4,1,14};
UINT4 IpCidrRouteMetric5 [ ] ={1,3,6,1,2,1,4,24,4,1,15};
UINT4 IpCidrRouteStatus [ ] ={1,3,6,1,2,1,4,24,4,1,16};
UINT4 IcmpInMsgs [ ] ={1,3,6,1,2,1,5,1};
UINT4 IcmpInErrors [ ] ={1,3,6,1,2,1,5,2};
UINT4 IcmpInDestUnreachs [ ] ={1,3,6,1,2,1,5,3};
UINT4 IcmpInTimeExcds [ ] ={1,3,6,1,2,1,5,4};
UINT4 IcmpInParmProbs [ ] ={1,3,6,1,2,1,5,5};
UINT4 IcmpInSrcQuenchs [ ] ={1,3,6,1,2,1,5,6};
UINT4 IcmpInRedirects [ ] ={1,3,6,1,2,1,5,7};
UINT4 IcmpInEchos [ ] ={1,3,6,1,2,1,5,8};
UINT4 IcmpInEchoReps [ ] ={1,3,6,1,2,1,5,9};
UINT4 IcmpInTimestamps [ ] ={1,3,6,1,2,1,5,10};
UINT4 IcmpInTimestampReps [ ] ={1,3,6,1,2,1,5,11};
UINT4 IcmpInAddrMasks [ ] ={1,3,6,1,2,1,5,12};
UINT4 IcmpInAddrMaskReps [ ] ={1,3,6,1,2,1,5,13};
UINT4 IcmpOutMsgs [ ] ={1,3,6,1,2,1,5,14};
UINT4 IcmpOutErrors [ ] ={1,3,6,1,2,1,5,15};
UINT4 IcmpOutDestUnreachs [ ] ={1,3,6,1,2,1,5,16};
UINT4 IcmpOutTimeExcds [ ] ={1,3,6,1,2,1,5,17};
UINT4 IcmpOutParmProbs [ ] ={1,3,6,1,2,1,5,18};
UINT4 IcmpOutSrcQuenchs [ ] ={1,3,6,1,2,1,5,19};
UINT4 IcmpOutRedirects [ ] ={1,3,6,1,2,1,5,20};
UINT4 IcmpOutEchos [ ] ={1,3,6,1,2,1,5,21};
UINT4 IcmpOutEchoReps [ ] ={1,3,6,1,2,1,5,22};
UINT4 IcmpOutTimestamps [ ] ={1,3,6,1,2,1,5,23};
UINT4 IcmpOutTimestampReps [ ] ={1,3,6,1,2,1,5,24};
UINT4 IcmpOutAddrMasks [ ] ={1,3,6,1,2,1,5,25};
UINT4 IcmpOutAddrMaskReps [ ] ={1,3,6,1,2,1,5,26};
UINT4 UdpInDatagrams [ ] ={1,3,6,1,2,1,7,1};
UINT4 UdpNoPorts [ ] ={1,3,6,1,2,1,7,2};
UINT4 UdpInErrors [ ] ={1,3,6,1,2,1,7,3};
UINT4 UdpOutDatagrams [ ] ={1,3,6,1,2,1,7,4};
UINT4 UdpLocalAddress [ ] ={1,3,6,1,2,1,7,5,1,1};
UINT4 UdpLocalPort [ ] ={1,3,6,1,2,1,7,5,1,2};


tMbDbEntry stdipMibEntry[]= {

{{8,IpForwarding}, NULL, IpForwardingGet, IpForwardingSet, IpForwardingTest, IpForwardingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,IpDefaultTTL}, NULL, IpDefaultTTLGet, IpDefaultTTLSet, IpDefaultTTLTest, IpDefaultTTLDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{8,IpInReceives}, NULL, IpInReceivesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInHdrErrors}, NULL, IpInHdrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInAddrErrors}, NULL, IpInAddrErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpForwDatagrams}, NULL, IpForwDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInUnknownProtos}, NULL, IpInUnknownProtosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInDiscards}, NULL, IpInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpInDelivers}, NULL, IpInDeliversGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpOutRequests}, NULL, IpOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpOutDiscards}, NULL, IpOutDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpOutNoRoutes}, NULL, IpOutNoRoutesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmTimeout}, NULL, IpReasmTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmReqds}, NULL, IpReasmReqdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmOKs}, NULL, IpReasmOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpReasmFails}, NULL, IpReasmFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpFragOKs}, NULL, IpFragOKsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpFragFails}, NULL, IpFragFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IpFragCreates}, NULL, IpFragCreatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,IpAdEntAddr}, GetNextIndexIpAddrTable, IpAdEntAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpAddrTableINDEX, 1, 0, 0, NULL},

{{10,IpAdEntIfIndex}, GetNextIndexIpAddrTable, IpAdEntIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpAddrTableINDEX, 1, 0, 0, NULL},

{{10,IpAdEntNetMask}, GetNextIndexIpAddrTable, IpAdEntNetMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpAddrTableINDEX, 1, 0, 0, NULL},

{{10,IpAdEntBcastAddr}, GetNextIndexIpAddrTable, IpAdEntBcastAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpAddrTableINDEX, 1, 0, 0, NULL},

{{10,IpAdEntReasmMaxSize}, GetNextIndexIpAddrTable, IpAdEntReasmMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpAddrTableINDEX, 1, 0, 0, NULL},

{{10,IpNetToMediaIfIndex}, GetNextIndexIpNetToMediaTable, IpNetToMediaIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpNetToMediaTableINDEX, 2, 0, 0, NULL},

{{10,IpNetToMediaPhysAddress}, GetNextIndexIpNetToMediaTable, IpNetToMediaPhysAddressGet, IpNetToMediaPhysAddressSet, IpNetToMediaPhysAddressTest, IpNetToMediaTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IpNetToMediaTableINDEX, 2, 0, 0, NULL},

{{10,IpNetToMediaNetAddress}, GetNextIndexIpNetToMediaTable, IpNetToMediaNetAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpNetToMediaTableINDEX, 2, 0, 0, NULL},

{{10,IpNetToMediaType}, GetNextIndexIpNetToMediaTable, IpNetToMediaTypeGet, IpNetToMediaTypeSet, IpNetToMediaTypeTest, IpNetToMediaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpNetToMediaTableINDEX, 2, 0, 0, NULL},

{{8,IpRoutingDiscards}, NULL, IpRoutingDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,IpCidrRouteNumber}, NULL, IpCidrRouteNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,IpCidrRouteDest}, GetNextIndexIpCidrRouteTable, IpCidrRouteDestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 0, 0, NULL},

{{11,IpCidrRouteMask}, GetNextIndexIpCidrRouteTable, IpCidrRouteMaskGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 0, 0, NULL},

{{11,IpCidrRouteTos}, GetNextIndexIpCidrRouteTable, IpCidrRouteTosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 0, 0, NULL},

{{11,IpCidrRouteNextHop}, GetNextIndexIpCidrRouteTable, IpCidrRouteNextHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 0, 0, NULL},

{{11,IpCidrRouteIfIndex}, GetNextIndexIpCidrRouteTable, IpCidrRouteIfIndexGet, IpCidrRouteIfIndexSet, IpCidrRouteIfIndexTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, "0"},

{{11,IpCidrRouteType}, GetNextIndexIpCidrRouteTable, IpCidrRouteTypeGet, IpCidrRouteTypeSet, IpCidrRouteTypeTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, NULL},

{{11,IpCidrRouteProto}, GetNextIndexIpCidrRouteTable, IpCidrRouteProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 0, 0, NULL},

{{11,IpCidrRouteAge}, GetNextIndexIpCidrRouteTable, IpCidrRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IpCidrRouteTableINDEX, 4, 0, 0, "0"},

{{11,IpCidrRouteInfo}, GetNextIndexIpCidrRouteTable, IpCidrRouteInfoGet, IpCidrRouteInfoSet, IpCidrRouteInfoTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, NULL},

{{11,IpCidrRouteNextHopAS}, GetNextIndexIpCidrRouteTable, IpCidrRouteNextHopASGet, IpCidrRouteNextHopASSet, IpCidrRouteNextHopASTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, "0"},

{{11,IpCidrRouteMetric1}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric1Get, IpCidrRouteMetric1Set, IpCidrRouteMetric1Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, "-1"},

{{11,IpCidrRouteMetric2}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric2Get, IpCidrRouteMetric2Set, IpCidrRouteMetric2Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, "-1"},

{{11,IpCidrRouteMetric3}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric3Get, IpCidrRouteMetric3Set, IpCidrRouteMetric3Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, "-1"},

{{11,IpCidrRouteMetric4}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric4Get, IpCidrRouteMetric4Set, IpCidrRouteMetric4Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, "-1"},

{{11,IpCidrRouteMetric5}, GetNextIndexIpCidrRouteTable, IpCidrRouteMetric5Get, IpCidrRouteMetric5Set, IpCidrRouteMetric5Test, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 0, "-1"},

{{11,IpCidrRouteStatus}, GetNextIndexIpCidrRouteTable, IpCidrRouteStatusGet, IpCidrRouteStatusSet, IpCidrRouteStatusTest, IpCidrRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IpCidrRouteTableINDEX, 4, 0, 1, NULL},

{{8,IcmpInMsgs}, NULL, IcmpInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInErrors}, NULL, IcmpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInDestUnreachs}, NULL, IcmpInDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInTimeExcds}, NULL, IcmpInTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInParmProbs}, NULL, IcmpInParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInSrcQuenchs}, NULL, IcmpInSrcQuenchsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInRedirects}, NULL, IcmpInRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInEchos}, NULL, IcmpInEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInEchoReps}, NULL, IcmpInEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInTimestamps}, NULL, IcmpInTimestampsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInTimestampReps}, NULL, IcmpInTimestampRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInAddrMasks}, NULL, IcmpInAddrMasksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpInAddrMaskReps}, NULL, IcmpInAddrMaskRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutMsgs}, NULL, IcmpOutMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutErrors}, NULL, IcmpOutErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutDestUnreachs}, NULL, IcmpOutDestUnreachsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutTimeExcds}, NULL, IcmpOutTimeExcdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutParmProbs}, NULL, IcmpOutParmProbsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutSrcQuenchs}, NULL, IcmpOutSrcQuenchsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutRedirects}, NULL, IcmpOutRedirectsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutEchos}, NULL, IcmpOutEchosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutEchoReps}, NULL, IcmpOutEchoRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutTimestamps}, NULL, IcmpOutTimestampsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutTimestampReps}, NULL, IcmpOutTimestampRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutAddrMasks}, NULL, IcmpOutAddrMasksGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,IcmpOutAddrMaskReps}, NULL, IcmpOutAddrMaskRepsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
{{8,UdpInDatagrams}, NULL, UdpInDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,UdpNoPorts}, NULL, UdpNoPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,UdpInErrors}, NULL, UdpInErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,UdpOutDatagrams}, NULL, UdpOutDatagramsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
{{10,UdpLocalAddress}, GetNextIndexUdpTable, UdpLocalAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, UdpTableINDEX, 2, 0, 0, NULL},

{{10,UdpLocalPort}, GetNextIndexUdpTable, UdpLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, UdpTableINDEX, 2, 0, 0, NULL},
};
tMibData stdipEntry = { 78, stdipMibEntry };

#endif /* REMOVE_ON_STDIP_DEPR_OBJ_DELETE */

#endif /* _STDIPDB_H */

