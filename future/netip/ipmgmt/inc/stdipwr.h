
#ifndef _STDIPWRAP_H 
#define _STDIPWRAP_H 
INT4 IpForwardingTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpForwardingSet (tSnmpIndex *, tRetVal *);
INT4 IpForwardingGet (tSnmpIndex *, tRetVal *);
INT4 IpForwardingDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 IpDefaultTTLTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpDefaultTTLSet (tSnmpIndex *, tRetVal *);
INT4 IpDefaultTTLGet (tSnmpIndex *, tRetVal *);
INT4 IpDefaultTTLDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 IpInReceivesGet (tSnmpIndex *, tRetVal *);
INT4 IpInHdrErrorsGet (tSnmpIndex *, tRetVal *);
INT4 IpInAddrErrorsGet (tSnmpIndex *, tRetVal *);
INT4 IpForwDatagramsGet (tSnmpIndex *, tRetVal *);
INT4 IpInUnknownProtosGet (tSnmpIndex *, tRetVal *);
INT4 IpInDiscardsGet (tSnmpIndex *, tRetVal *);
INT4 IpInDeliversGet (tSnmpIndex *, tRetVal *);
INT4 IpOutRequestsGet (tSnmpIndex *, tRetVal *);
INT4 IpOutDiscardsGet (tSnmpIndex *, tRetVal *);
INT4 IpOutNoRoutesGet (tSnmpIndex *, tRetVal *);
INT4 IpReasmTimeoutGet (tSnmpIndex *, tRetVal *);
INT4 IpReasmReqdsGet (tSnmpIndex *, tRetVal *);
INT4 IpReasmOKsGet (tSnmpIndex *, tRetVal *);
INT4 IpReasmFailsGet (tSnmpIndex *, tRetVal *);
INT4 IpFragOKsGet (tSnmpIndex *, tRetVal *);
INT4 IpFragFailsGet (tSnmpIndex *, tRetVal *);
INT4 IpFragCreatesGet (tSnmpIndex *, tRetVal *);
INT4 IpAdEntAddrGet (tSnmpIndex *, tRetVal *);
INT4 IpAdEntIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 IpAdEntNetMaskGet (tSnmpIndex *, tRetVal *);
INT4 IpAdEntBcastAddrGet (tSnmpIndex *, tRetVal *);
INT4 IpAdEntReasmMaxSizeGet (tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaPhysAddressTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaPhysAddressSet (tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaPhysAddressGet (tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaNetAddressGet (tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaTypeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaTypeSet (tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaTypeGet (tSnmpIndex *, tRetVal *);
INT4 IpNetToMediaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 IpRoutingDiscardsGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteNumberGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteDestGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMaskGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteTosGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteNextHopGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteIfIndexTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteIfIndexSet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteTypeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteTypeSet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteTypeGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteProtoGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteAgeGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteInfoTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteInfoSet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteInfoGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteNextHopASTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteNextHopASSet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteNextHopASGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric1Test ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric1Set (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric1Get (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric2Test ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric2Set (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric2Get (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric3Test ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric3Set (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric3Get (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric4Test ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric4Set (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric4Get (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric5Test ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric5Set (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteMetric5Get (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteStatusSet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteStatusGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInMsgsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInErrorsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInDestUnreachsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInTimeExcdsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInParmProbsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInSrcQuenchsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInRedirectsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInEchosGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInEchoRepsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInTimestampsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInTimestampRepsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInAddrMasksGet (tSnmpIndex *, tRetVal *);
INT4 IcmpInAddrMaskRepsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutMsgsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutErrorsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutDestUnreachsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutTimeExcdsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutParmProbsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutSrcQuenchsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutRedirectsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutEchosGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutEchoRepsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutTimestampsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutTimestampRepsGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutAddrMasksGet (tSnmpIndex *, tRetVal *);
INT4 IcmpOutAddrMaskRepsGet (tSnmpIndex *, tRetVal *);
/*
 * Remove this #ifdef when deleting the deprecated objects of 
 * standard IP mib.
 * This code is provided to support backward comp.
 */

#ifdef  REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT4 UdpInDatagramsGet (tSnmpIndex *, tRetVal *);
INT4 UdpNoPortsGet (tSnmpIndex *, tRetVal *);
INT4 UdpInErrorsGet (tSnmpIndex *, tRetVal *);
INT4 UdpOutDatagramsGet (tSnmpIndex *, tRetVal *);
#endif
INT4 UdpLocalAddressGet (tSnmpIndex *, tRetVal *);
INT4 UdpLocalPortGet (tSnmpIndex *, tRetVal *);
INT4 IpCidrRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);











 /*  GetNext Function Prototypes */

INT4 GetNextIndexUdpTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpCidrRouteTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpNetToMediaTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexIpAddrTable( tSnmpIndex *, tSnmpIndex *);
#endif
