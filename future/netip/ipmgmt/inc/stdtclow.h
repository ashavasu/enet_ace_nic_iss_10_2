
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTcpRtoAlgorithm ARG_LIST((INT4 *));

INT1
nmhGetTcpRtoMin ARG_LIST((INT4 *));

INT1
nmhGetTcpRtoMax ARG_LIST((INT4 *));

INT1
nmhGetTcpMaxConn ARG_LIST((INT4 *));

INT1
nmhGetTcpActiveOpens ARG_LIST((UINT4 *));

INT1
nmhGetTcpPassiveOpens ARG_LIST((UINT4 *));

INT1
nmhGetTcpAttemptFails ARG_LIST((UINT4 *));

INT1
nmhGetTcpEstabResets ARG_LIST((UINT4 *));

INT1
nmhGetTcpCurrEstab ARG_LIST((UINT4 *));

INT1
nmhGetTcpInSegs ARG_LIST((UINT4 *));

INT1
nmhGetTcpOutSegs ARG_LIST((UINT4 *));

INT1
nmhGetTcpRetransSegs ARG_LIST((UINT4 *));

INT1
nmhGetTcpInErrs ARG_LIST((UINT4 *));

INT1
nmhGetTcpOutRsts ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for TcpConnTable. */
INT1
nmhValidateIndexInstanceTcpConnTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for TcpConnTable  */

INT1
nmhGetFirstIndexTcpConnTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexTcpConnTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetTcpConnState ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetTcpConnState ARG_LIST((UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2TcpConnState ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2TcpConnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
