
 /* $Id: ip4ipvx.h,v 1.9 2016/02/27 10:14:49 siva Exp $*/

#ifndef __IP4_IPVX_H__
#define __IP4_IPVX_H__

#include "arp.h"
#include "iss.h"
#include "ipvx.h"
/* System Stats ... */

#define   ARP_AUDIT_SHOW_CMD    "end;show ip arp > "
#define   ARP_CLI_MAX_GROUPS_LINE_LEN    200
#define   ARP_CLI_EOF                  2
#define   ARP_CLI_NO_EOF                 1
#define   ARP_CLI_RDONLY               OSIX_FILE_RO
#define   ARP_CLI_WRONLY               OSIX_FILE_WO
#define ARP_AUDIT_FILE_ACTIVE "/tmp/arp_output_file_active"
#define ARP_AUDIT_FILE_STDBY "/tmp/arp_output_file_stdby"

#define IP4SYS_INC_IN_RECEVES(u4CxtId) \
if (u4CxtId < IP_SIZING_CONTEXT_COUNT)\
{\
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_rcvs + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_rcvs) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCIn_rcvs++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_rcvs++); \
}

#define IP4SYS_INC_IN_OCTETS(u4Oct, u4CxtId) \
if (u4CxtId < IP_SIZING_CONTEXT_COUNT)\
{\
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InOctets + u4Oct) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InOctets) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCInOctets++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InOctets += u4Oct);\
}

#define IP4SYS_INC_IN_HDR_ERRS(u4CxtId) \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_hdr_err++)

#define IP4SYS_INC_IN_NO_ROUTES(u4CxtId) \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_no_routes++)

#define IP4SYS_INC_IN_ADDR_ERRS(u4CxtId) \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_addr_err++)

#define IP4SYS_INC_IN_UNKNOWN_PROTOS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Bad_proto++)

#define IP4SYS_INC_IN_TRUNCATED_PKTS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_Trun_Pkts++)

#define IP4SYS_INC_IN_FORW_DGRAMS(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Forw_attempts + 1) \
    < (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Forw_attempts) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCInForwDgrams++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Forw_attempts++)

#define IP4SYS_INC_REASM_REQDS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Reasm_reqs++)

#define IP4SYS_INC_REASM_OKS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Reasm_oks++)

#define IP4SYS_INC_REASM_FAILS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Reasm_fails++)

#define IP4SYS_INC_IN_DISCARDS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InDiscards++)

#define IP4SYS_INC_IN_DELIVERIES(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_deliveries + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_deliveries) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCIn_deliveries++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_deliveries++)

#define IP4SYS_INC_OUT_REQUESTS(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Out_requests + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Out_requests) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCOut_requests++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Out_requests++)

#define IP4SYS_INC_OUT_NO_ROUTES(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutNoRoutes++)

#define IP4SYS_INC_OUT_FORW_DGRAMS(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutForwds + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutForwds) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCOutForwds++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutForwds++)

#define IP4SYS_INC_OUT_DISCARDS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutDiscards++)

#define IP4SYS_INC_OUT_FRAG_REQDS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Out_frag_Reqs++)

#define IP4SYS_INC_OUT_FRAG_OKS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutFragOKs++)

#define IP4SYS_INC_OUT_FRAG_FAILS(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Frag_fails++)

#define IP4SYS_INC_OUT_FRAG_CREATES(u4CxtId)  \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4Frag_creates++)


#define IP4SYS_INC_OUT_TRANSMITS(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutTrans + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutTrans) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCOutTrans++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutTrans++)

#define IP4SYS_INC_OUT_OCTETS(u4Oct, u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutOctets + u4Oct) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutOctets) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCOutOctets++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutOctets += u4Oct)

#define IP4SYS_INC_IN_MCAST_PKT(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InMcastPkts + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InMcastPkts) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCInMcastPkts++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InMcastPkts++)

#define IP4SYS_INC_IN_MCAST_OCTETS(u4Oct, u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InMcastOctets + u4Oct) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InMcastOctets) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCInMcastOctets++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4InMcastOctets += u4Oct)

#define IP4SYS_INC_OUT_MCAST_PKT(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutMcastPkts + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutMcastPkts) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCOutMcastPkts++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutMcastPkts++)

#define IP4SYS_INC_OUT_MCAST_OCTETS(u4Oct, u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutMcastOctets + u4Oct) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutMcastOctets) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCOutMcastOctets++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutMcastOctets += u4Oct)

#define IP4SYS_INC_IN_BCAST_PKT(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_bcasts + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_bcasts) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCInBcastPkts++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4In_bcasts++)

#define IP4SYS_INC_OUT_BCAST_PKT(u4CxtId) \
if (((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutBcastPkts + 1) < \
    (gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutBcastPkts) \
{ \
    ((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4HCOutBcastPkts++); \
} \
((gIpGlobalInfo.apIpCxt[u4CxtId])->Ip_stats.u4OutBcastPkts++)

/* Interface Stats ... */

#define IP4IF_INC_IN_RECEVES(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_rcvs++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_OCTETS(u2Port, u4Oct) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InOctets + u4Oct) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InOctets) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInOctets++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_IN_HDR_ERRS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_hdr_err++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_NO_ROUTES(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_no_routes++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_ADDR_ERRS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_addr_err++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_UNKNOWN_PROTOS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Bad_proto++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_TRUNCATED_PKTS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_Trun_Pkts++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_FORW_DGRAMS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInForwDgrams++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_REASM_REQDS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_reqs++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_REASM_OKS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_oks++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_REASM_FAILS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_fails++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_DISCARDS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InDiscards++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_IN_DELIVERIES(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCIn_deliveries++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries++); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_OUT_REQUESTS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOut_requests++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_NO_ROUTES(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutNoRoutes++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_FORW_DGRAMS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutForwds+ 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutForwds) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutForwds++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutForwds++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_DISCARDS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutDiscards++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_FRAG_REQDS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_frag_Reqs++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_FRAG_OKS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutFragOKs++); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_OUT_FRAG_FAILS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_fails++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_FRAG_CREATES(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_creates++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_TRANSMITS(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutTrans+ 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutTrans) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutTrans++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutTrans++); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_OUT_OCTETS(u2Port,u4Oct) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutOctets + u4Oct) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutOctets) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutOctets++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_IN_MCAST_PKT(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInMcastPkts++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts++); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_IN_MCAST_OCTETS(u2Port, u4Oct) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMcastOctets + u4Oct) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMcastOctets) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCInMcastOctets++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMcastOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_OUT_MCAST_PKT(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMulticastPkts + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMulticastPkts) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutHCMCastPkts++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMulticastPkts++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_MCAST_OCTETS(u2Port, u4Oct) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMcastOctets + u4Oct) < \
        gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMcastOctets) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4HCOutMcastOctets++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMcastOctets += u4Oct); \
    IPvxUpdateIfStatsTblLstChange(); \
} 

#define IP4IF_INC_IN_BCAST_PKT(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InHCBCastPkts++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts++); \
    IPvxUpdateIfStatsTblLstChange(); \
}

#define IP4IF_INC_OUT_BCAST_PKT(u2Port) \
if ((u2Port < IPIF_MAX_LOGICAL_IFACES) && (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)) \
{ \
    if ((gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts + 1) < gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts) \
    { \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutHCBCastPkts++); \
    } \
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts++); \
    IPvxUpdateIfStatsTblLstChange(); \
}



/* Icmp Codes for stats */

  #define  IPVX_ICMP_ECHO_REPLY         0
  #define  IPVX_ICMP_UNASSIGNED1        1 
  #define  IPVX_ICMP_UNASSIGNED2        2
  #define  IPVX_ICMP_DEST_UNREAC        3
  #define  IPVX_ICMP_SRC_QUENCH         4
  #define  IPVX_ICMP_REDIRECT           5
  #define  IPVX_ICMP_ALT_HOST_ADDR      6
  #define  IPVX_ICMP_UNASSIGNED3        7
  #define  IPVX_ICMP_ECHO               8
  #define  IPVX_ICMP_RTR_ADV            9
  #define  IPVX_ICMP_RTR_SOL           10
  #define  IPVX_ICMP_TIME_EXCEEDED     11
  #define  IPVX_ICMP_PARA_PROBLEM      12
  #define  IPVX_ICMP_TIME_STAMP        13
  #define  IPVX_ICMP_TSTAMP_REPLY      14
  #define  IPVX_ICMP_INFO_REQUEST      15
  #define  IPVX_ICMP_INFO_REPLY        16
  #define  IPVX_ICMP_ADDR_MASK_RQST    17
  #define  IPVX_ICMP_ADDR_MASK_REPLY   18
  #define  IPVX_ICMP_RESERVED1         19
  #define  IPVX_ICMP_RESERVED2_S       20
  #define  IPVX_ICMP_RESERVED3_E       29
  #define  IPVX_ICMP_TRACEROUTE        30  
  #define  IPVX_ICMP_DGRAM_CONV_ERR    31 
  #define  IPVX_ICMP_MOBILE_HOST_RDIR  32 
  #define  IPVX_ICMP_IPV6_WAY          33 
  #define  IPVX_ICMP_IPV6_IMHERE       34 
  #define  IPVX_ICMP_MOBILE_REG_RQST   35 
  #define  IPVX_ICMP_MOBILE_REG_REPLY  36 
  #define  IPVX_ICMP_DOMAIN_NAME_RQST  37 
  #define  IPVX_ICMP_DOMAIN_NAME_REPLY 38 
  #define  IPVX_ICMP_SKIP              39 
  #define  IPVX_ICMP_PHOTURIS          40 
  #define  IPVX_ICMP_ICMP_MSG_EXP      41 


/* OSPF route types */

#define IP_OSPF_INTRA_AREA    1
#define IP_OSPF_INTER_AREA    2
#define IP_OSPF_TYPE_1_EXT    3
#define IP_OSPF_TYPE_2_EXT    4
#define IP_OSPF_NSSA_1_EXT    5
#define IP_OSPF_NSSA_2_EXT    6


extern t_ARP_HW_TABLE *(*arp_get_hw_entry) PROTO ((INT2 i2Hardware));
/*
        *(*arp_get_free_hw_entry) PROTO ((INT2 i2Hardware));*/
/*fsmsiplw.h*/

extern INT1
nmhGetFsMIStdIpv4InterfaceTableLastChange ARG_LIST((UINT4 *));
extern INT1
nmhGetFsMIStdIpv6InterfaceTableLastChange ARG_LIST((UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsTableLastChange ARG_LIST((UINT4 *));
extern INT1
nmhValidateIndexInstanceFsMIStdIpGlobalTable ARG_LIST((INT4 ));
extern INT1
nmhGetFirstIndexFsMIStdIpGlobalTable ARG_LIST((INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpGlobalTable ARG_LIST((INT4 , INT4 *));
extern INT1
nmhGetFsMIStdIpForwarding ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpDefaultTTL ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpReasmTimeout ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpv6IpForwarding ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpv6IpDefaultHopLimit ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdInetCidrRouteNumber ARG_LIST((INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdInetCidrRouteDiscards ARG_LIST((INT4 ,UINT4 *));
extern INT1
nmhSetFsMIStdIpForwarding ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetFsMIStdIpDefaultTTL ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetFsMIStdIpv6IpForwarding ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetFsMIStdIpv6IpDefaultHopLimit ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpDefaultTTL ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpv6IpForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpv6IpDefaultHopLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhDepv2FsMIStdIpGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
extern INT1
nmhValidateIndexInstanceFsMIStdIpv4InterfaceTable ARG_LIST((INT4 ));
extern INT1
nmhGetFirstIndexFsMIStdIpv4InterfaceTable ARG_LIST((INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpv4InterfaceTable ARG_LIST((INT4 , INT4 *));
extern INT1
nmhGetFsMIStdIpv4InterfaceReasmMaxSize ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpv4InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpv4InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpv4IfContextId ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhSetFsMIStdIpv4InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpv4InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhDepv2FsMIStdIpv4InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, 
                    tSNMP_VAR_BIND*));
extern INT1
nmhValidateIndexInstanceFsMIStdIpv6InterfaceTable ARG_LIST((INT4 ));
extern INT1
nmhGetFirstIndexFsMIStdIpv6InterfaceTable ARG_LIST((INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpv6InterfaceTable ARG_LIST((INT4 , INT4 *));
extern INT1
nmhGetFsMIStdIpv6InterfaceReasmMaxSize ARG_LIST((INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpv6InterfaceIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMIStdIpv6InterfaceEnableStatus ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpv6InterfaceReachableTime ARG_LIST((INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpv6InterfaceRetransmitTime ARG_LIST((INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpv6InterfaceForwarding ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIStdIpv6IfContextId ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhSetFsMIStdIpv6InterfaceEnableStatus ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetFsMIStdIpv6InterfaceForwarding ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpv6InterfaceEnableStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpv6InterfaceForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhDepv2FsMIStdIpv6InterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, 
                 tSNMP_VAR_BIND*));
extern INT1
nmhValidateIndexInstanceFsMIStdIpSystemStatsTable ARG_LIST((INT4  , INT4 ));
extern INT1
nmhGetFirstIndexFsMIStdIpSystemStatsTable ARG_LIST((INT4 * , INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpSystemStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInReceives ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCInReceives ARG_LIST((INT4  , INT4 ,
               tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,
          tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCInDelivers ARG_LIST((INT4  , INT4 ,
                  tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCOutRequests ARG_LIST((INT4  , INT4 ,
                        tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,
tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,
                    tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,
                                tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,
                       tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,
                  tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 ,
                      tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,
                               tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,
                          tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpSystemStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpSystemStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhValidateIndexInstanceFsMIStdIpIfStatsTable ARG_LIST((INT4  , INT4 ));
extern INT1
nmhGetFirstIndexFsMIStdIpIfStatsTable ARG_LIST((INT4 * , INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpIfStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInReceives ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCInReceives ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCInOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsInHdrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInNoRoutes ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInAddrErrors ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInUnknownProtos ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInTruncatedPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCInForwDatagrams ARG_LIST((INT4  , INT4 ,
                  tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsReasmReqds ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsReasmOKs ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsReasmFails ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsInDelivers ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCInDelivers ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsOutRequests ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCOutRequests ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsOutForwDatagrams ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCOutForwDatagrams ARG_LIST((INT4  , INT4 ,
                        tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsOutDiscards ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsOutFragReqds ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsOutFragOKs ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsOutFragFails ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsOutFragCreates ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsOutTransmits ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCOutTransmits ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCOutOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsInMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCInMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsInMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCInMcastOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsOutMcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCOutMcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
nmhGetFsMIStdIpIfStatsOutMcastOctets ARG_LIST((INT4  , INT4 ,UINT4 *));
extern INT1
nmhGetFsMIStdIpIfStatsHCOutMcastOctets ARG_LIST((INT4  , INT4 
                  ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsInBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCInBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsOutBcastPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsHCOutBcastPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetFsMIStdIpIfStatsDiscontinuityTime ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsRefreshRate ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpIfStatsContextId ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1
nmhValidateIndexInstanceFsMIStdIpAddressPrefixTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));


extern INT1
nmhGetFirstIndexFsMIStdIpAddressPrefixTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));


extern INT1
nmhGetNextIndexFsMIStdIpAddressPrefixTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));


extern INT1
nmhGetFsMIStdIpAddressPrefixOrigin ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressPrefixOnLinkFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressPrefixAutonomousFlag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressPrefixAdvPreferredLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpAddressPrefixAdvValidLifetime ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpAddressContextId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdIpAddressTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdIpAddressTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpAddressTable  */

extern INT1
nmhGetFirstIndexFsMIStdIpAddressTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdIpAddressTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdIpAddressIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressPrefix ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

extern INT1
nmhGetFsMIStdIpAddressOrigin ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressCreated ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMIStdIpAddressLastChanged ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMIStdIpAddressRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpAddressStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMIStdIpAddressIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdIpAddressType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdIpAddressStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdIpAddressRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdIpAddressStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMIStdIpAddressIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpAddressType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpAddressStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpAddressRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpAddressStorageType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsMIStdIpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpNetToPhysicalTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdIpNetToPhysicalTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpNetToPhysicalTable  */

extern INT1
nmhGetFirstIndexFsMIStdIpNetToPhysicalTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdIpNetToPhysicalTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIStdIpNetToPhysicalLastUpdated ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMIStdIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpNetToPhysicalState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdIpNetToPhysicalContextId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMIStdIpNetToPhysicalPhysAddress ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMIStdIpNetToPhysicalType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdIpNetToPhysicalRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMIStdIpNetToPhysicalPhysAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMIStdIpNetToPhysicalType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpNetToPhysicalRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsMIStdIpNetToPhysicalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIpv6ScopeZoneIndexTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdIpv6ScopeZoneIndexTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpv6ScopeZoneIndexTable  */

extern INT1
nmhGetFirstIndexFsMIStdIpv6ScopeZoneIndexTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdIpv6ScopeZoneIndexTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexLinkLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndex3 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexAdminLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexSiteLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndex6 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndex7 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndex9 ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexA ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexB ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexC ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneIndexD ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6ScopeZoneContextId ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdIpDefaultRouterTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdIpDefaultRouterTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpDefaultRouterTable  */

extern INT1
nmhGetFirstIndexFsMIStdIpDefaultRouterTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdIpDefaultRouterTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdIpDefaultRouterLifetime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpDefaultRouterPreference ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdIpv6RouterAdvertTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdIpv6RouterAdvertTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIpv6RouterAdvertTable  */

extern INT1
nmhGetFirstIndexFsMIStdIpv6RouterAdvertTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdIpv6RouterAdvertTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdIpv6RouterAdvertSendAdverts ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertMaxInterval ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertMinInterval ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertManagedFlag ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertLinkMTU ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertReachableTime ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertRetransmitTime ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertCurHopLimit ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertRowStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIStdIpv6RouterAdvertContextId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMIStdIpv6RouterAdvertSendAdverts ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertMaxInterval ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertMinInterval ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertManagedFlag ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertLinkMTU ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertReachableTime ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertRetransmitTime ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertCurHopLimit ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsMIStdIpv6RouterAdvertRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertSendAdverts ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertMaxInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertMinInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertManagedFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertOtherConfigFlag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertLinkMTU ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertReachableTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertRetransmitTime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertCurHopLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertDefaultLifetime ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsMIStdIpv6RouterAdvertTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdIcmpStatsTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdIcmpStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIcmpStatsTable  */

extern INT1
nmhGetFirstIndexFsMIStdIcmpStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdIcmpStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdIcmpStatsInMsgs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIcmpStatsInErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIcmpStatsOutMsgs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIcmpStatsOutErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIStdIcmpMsgStatsTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdIcmpMsgStatsTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdIcmpMsgStatsTable  */

extern INT1
nmhGetFirstIndexFsMIStdIcmpMsgStatsTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdIcmpMsgStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdIcmpMsgStatsInPkts ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsMIStdIcmpMsgStatsOutPkts ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIStdInetCidrRouteTable. */
extern INT1
nmhValidateIndexInstanceFsMIStdInetCidrRouteTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdInetCidrRouteTable  */

extern INT1
nmhGetFirstIndexFsMIStdInetCidrRouteTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OID_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsMIStdInetCidrRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIStdInetCidrRouteIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteProto ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteAge ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteNextHopAS ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteMetric1 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteMetric2 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteMetric3 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteMetric4 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteMetric5 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMIStdInetCidrRouteIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteNextHopAS ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteMetric1 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteMetric2 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteMetric3 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteMetric4 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteMetric5 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMIStdInetCidrRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteNextHopAS ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteMetric1 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteMetric2 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteMetric3 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteMetric4 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteMetric5 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsMIStdInetCidrRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*,
                        tSNMP_VAR_BIND*));


INT1
IPvxGetARPEntry (INT4  i4IPv4IfIndex, UINT4 u4Ipv4Addr, t_ARP_CACHE *pArpEntry);

extern INT1
nmhValidateIndexInstanceFsMIArpTable ARG_LIST((INT4 ));
extern INT1
nmhGetFirstIndexFsMIArpTable ARG_LIST((INT4 *));
extern INT1
nmhGetNextIndexFsMIArpTable ARG_LIST((INT4 , INT4 *));
extern INT1
nmhGetFsMIArpCacheTimeout ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIArpCachePendTime ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIArpMaxRetries ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhSetFsMIArpCacheTimeout ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetFsMIArpCachePendTime ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetFsMIArpMaxRetries ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIArpCacheTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIArpCachePendTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIArpMaxRetries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhGetFsMIStdIpProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhTestv2FsMIStdIpProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhSetFsMIStdIpProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIStdIpProxyArpSubnetOption ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhSetFsMIStdIpProxyArpSubnetOption ARG_LIST((INT4 ));
extern INT1
nmhGetFirstIndexFsMIStdIpifTable ARG_LIST((INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpifTable ARG_LIST((INT4 , INT4 *));





INT4
ArpCliGetShowCmdOutputToFile (UINT1 *);

INT4
ArpCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
ArpCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

#endif /* __IP4_IPVX_H__ */

/****************************   End of the File ***************************/

