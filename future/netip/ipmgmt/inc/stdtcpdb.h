/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtcpdb.h,v 1.3 2008/08/20 15:18:03 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDTCPDB_H
#define _STDTCPDB_H

UINT1 TcpConnTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};

UINT4 stdtcp [] ={1,3,6,1,2,1,6};
tSNMP_OID_TYPE stdtcpOID = {7, stdtcp};


UINT4 TcpRtoAlgorithm [ ] ={1,3,6,1,2,1,6,1};
UINT4 TcpRtoMin [ ] ={1,3,6,1,2,1,6,2};
UINT4 TcpRtoMax [ ] ={1,3,6,1,2,1,6,3};
UINT4 TcpMaxConn [ ] ={1,3,6,1,2,1,6,4};
UINT4 TcpActiveOpens [ ] ={1,3,6,1,2,1,6,5};
UINT4 TcpPassiveOpens [ ] ={1,3,6,1,2,1,6,6};
UINT4 TcpAttemptFails [ ] ={1,3,6,1,2,1,6,7};
UINT4 TcpEstabResets [ ] ={1,3,6,1,2,1,6,8};
UINT4 TcpCurrEstab [ ] ={1,3,6,1,2,1,6,9};
UINT4 TcpInSegs [ ] ={1,3,6,1,2,1,6,10};
UINT4 TcpOutSegs [ ] ={1,3,6,1,2,1,6,11};
UINT4 TcpRetransSegs [ ] ={1,3,6,1,2,1,6,12};
UINT4 TcpConnState [ ] ={1,3,6,1,2,1,6,13,1,1};
UINT4 TcpConnLocalAddress [ ] ={1,3,6,1,2,1,6,13,1,2};
UINT4 TcpConnLocalPort [ ] ={1,3,6,1,2,1,6,13,1,3};
UINT4 TcpConnRemAddress [ ] ={1,3,6,1,2,1,6,13,1,4};
UINT4 TcpConnRemPort [ ] ={1,3,6,1,2,1,6,13,1,5};
UINT4 TcpInErrs [ ] ={1,3,6,1,2,1,6,14};
UINT4 TcpOutRsts [ ] ={1,3,6,1,2,1,6,15};


tMbDbEntry stdtcpMibEntry[]= {

{{8,TcpRtoAlgorithm}, NULL, TcpRtoAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpRtoMin}, NULL, TcpRtoMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpRtoMax}, NULL, TcpRtoMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpMaxConn}, NULL, TcpMaxConnGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpActiveOpens}, NULL, TcpActiveOpensGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpPassiveOpens}, NULL, TcpPassiveOpensGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpAttemptFails}, NULL, TcpAttemptFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpEstabResets}, NULL, TcpEstabResetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpCurrEstab}, NULL, TcpCurrEstabGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpInSegs}, NULL, TcpInSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpOutSegs}, NULL, TcpOutSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpRetransSegs}, NULL, TcpRetransSegsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,TcpConnState}, GetNextIndexTcpConnTable, TcpConnStateGet, TcpConnStateSet, TcpConnStateTest, TcpConnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, TcpConnTableINDEX, 4, 0, 0, NULL},

{{10,TcpConnLocalAddress}, GetNextIndexTcpConnTable, TcpConnLocalAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, TcpConnTableINDEX, 4, 0, 0, NULL},

{{10,TcpConnLocalPort}, GetNextIndexTcpConnTable, TcpConnLocalPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, TcpConnTableINDEX, 4, 0, 0, NULL},

{{10,TcpConnRemAddress}, GetNextIndexTcpConnTable, TcpConnRemAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, TcpConnTableINDEX, 4, 0, 0, NULL},

{{10,TcpConnRemPort}, GetNextIndexTcpConnTable, TcpConnRemPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, TcpConnTableINDEX, 4, 0, 0, NULL},

{{8,TcpInErrs}, NULL, TcpInErrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{8,TcpOutRsts}, NULL, TcpOutRstsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData stdtcpEntry = { 19, stdtcpMibEntry };
#endif /* _STDTCPDB_H */

