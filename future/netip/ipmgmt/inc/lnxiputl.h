/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxiputl.h,v 1.14 2014/03/11 14:13:03 siva Exp $
 *
 * Description: Contains macros used by lnipsnlw.c.
 *
 *******************************************************************/
#ifndef __LNXIPUTL_H__
#define __LNXIPUTL_H__

#include "fssocket.h"
#include "cfa.h"
#include "lnxip.h"

#define LNX_IP_FORW_ENABLE   1
#define LNX_IP_FORW_DISABLE  0 

#define LNX_IP_MIB_OBJECTS   1
#define LNX_ICMP_MIB_OBJECTS 2
#define LNX_TCP_MIB_OBJECTS  3
#define LNX_UDP_MIB_OBJECTS  4

#define LNXIP_STATS_FILE_NAME "/proc/net/snmp"
  
#define LNX_MAX_LINE_LEN  1024
#define LNX_MAX_COMMAND_LEN  80

#define LNX_IP_STATS_HDR_LEN 224
#define LNX_IP_STATS_MAX_OBJECTS 19

#define LNX_ICMP_STATS_HDR_LEN 329 
#define LNX_ICMP_STATS_MSG_HDR_LEN 34 
#define LNX_ICMP_STATS_MAX_OBJECTS 26 

#define LNX_TCP_STATS_HDR_LEN 142
#define LNX_TCP_STATS_MAX_OBJECTS 14

#define LNX_UDP_STATS_HDR_LEN 47
#define LNX_UDP_STATS_LITE_HDR_LEN 77
#define LNX_UDP_STATS_MAX_OBJECTS 4

#define LNXIP_MAX_OBJECTS_COUNT LNX_ICMP_STATS_MAX_OBJECTS

/* IP mib objects - 19 */
/* Ip: Forwarding DefaultTTL InReceives InHdrErrors InAddrErrors ForwDatagrams InUnknownProtos InDiscards InDelivers OutRequests OutDiscards OutNoRoutes ReasmTimeout ReasmReqds ReasmOKs ReasmFails FragOKs FragFails FragCreates */

#define LNX_IP_FORWARDING     1
#define LNX_IP_DEFAULT_TTL    2
#define LNX_IP_IN_RECEIVES    3
#define LNX_IP_IN_HDR_ERRS    4
#define LNX_IP_IN_ADDR_ERRS   5
#define LNX_IP_FORW_DATAGRAMS 6
#define LNX_IP_IN_UNK_PROTOS  7
#define LNX_IP_IN_DISCARDS    8
#define LNX_IP_IN_DELIVERS    9
#define LNX_IP_OUT_REQUESTS   10
#define LNX_IP_OUT_DISCARDS   11
#define LNX_IP_OUT_NO_ROUTES  12
#define LNX_IP_REASM_TIMEOUT  13
#define LNX_IP_REASM_REQDS    14
#define LNX_IP_REASM_OKS      15
#define LNX_IP_REASM_FAILS    16
#define LNX_IP_FRAG_OKS       17
#define LNX_IP_FRAG_FAILS     18
#define LNX_IP_FRAG_CREATES   19

/* ICMP mib objects - 26 */
/* Icmp: InMsgs InErrors InDestUnreachs InTimeExcds InParmProbs InSrcQuenchs InRedirects InEchos InEchoReps InTimestamps InTimestampReps InAddrMasks InAddrMaskReps OutMsgs OutErrors OutDestUnreachs OutTimeExcds OutParmProbs OutSrcQuenchs OutRedirects OutEchos OutEchoReps OutTimestamps OutTimestampReps OutAddrMasks OutAddrMaskReps */

#define LNX_ICMP_IN_MSGS         20
#define LNX_ICMP_IN_ERRS         21
#define LNX_ICMP_IN_DEST_UNREACHS   22
#define LNX_ICMP_IN_TIME_EXCDS   23
#define LNX_ICMP_IN_PARM_PROBS   24
#define LNX_ICMP_IN_SRC_QUENCHS  25
#define LNX_ICMP_IN_REDIRECTS    26
#define LNX_ICMP_IN_ECHOS        27
#define LNX_ICMP_IN_ECHO_REPS    28
#define LNX_ICMP_IN_TIME_STAMPS  29
#define LNX_ICMP_IN_TIME_STAM_REPS 30
#define LNX_ICMP_IN_ADDR_MASKS     31
#define LNX_ICMP_IN_ADDR_MASK_REPS 32
#define LNX_ICMP_OUT_MSGS          33
#define LNX_ICMP_OUT_ERRS          34
#define LNX_ICMP_OUT_DEST_UNREACHS 35
#define LNX_ICMP_OUT_TIME_EXCDS    36
#define LNX_ICMP_OUT_PARM_PROBS    37
#define LNX_ICMP_OUT_SRC_QUENCHS   38
#define LNX_ICMP_OUT_REDIRECTS     39
#define LNX_ICMP_OUT_ECHOS         40
#define LNX_ICMP_OUT_ECHO_REPS     41
#define LNX_ICMP_OUT_TIME_STAMPS   42
#define LNX_ICMP_OUT_TIME_STAMP_REPS 43
#define LNX_ICMP_OUT_ADDR_MASKS     44
#define LNX_ICMP_OUT_ADDR_MASK_REPS 45

/* TCP mib objects - 14 */
/* Tcp: RtoAlgorithm RtoMin RtoMax MaxConn ActiveOpens PassiveOpens AttemptFails EstabResets CurrEstab InSegs OutSegs RetransSegs InErrs OutRsts */

#define LNX_TCP_RTO_ALGM      46
#define LNX_TCP_RTO_MIN       47
#define LNX_TCP_RTO_MAX       48
#define LNX_TCP_MAX_CONN      49
#define LNX_TCP_ACTV_OPENS    50
#define LNX_TCP_PASSIVE_OPENS 51
#define LNX_TCP_ATTEMPT_FAILS 52
#define LNX_TCP_ESTAB_RESETS  53
#define LNX_TCP_CURR_ESTAB    54
#define LNX_TCP_IN_SEGS       55
#define LNX_TCP_OUT_SEGS      56
#define LNX_TCP_RETANS_SEGS   57
#define LNX_TCP_IN_ERRS       58
#define LNX_TCP_OUT_RSTS      59

/* UDP mib objects - 4 */
/* Udp: InDatagrams NoPorts InErrors OutDatagrams */

#define LNX_UDP_IN_DATAGRAMS  60
#define LNX_UDP_NO_PORTS      61
#define LNX_UDP_IN_ERRS       62
#define LNX_UDP_OUT_DATAGRAMS 63

#define LNX_TCP_CONN_TABLE_INDEX_SIZE 12
#define LNX_DELETE_TCB                12
#define LNX_UDP_TABLE_INDEX_SIZE      24
#define LNX_MAX_ENTRY_SIZE            160
#define LNX_MAX_INDEX_SIZE            8
#define LNX_MAX_IP_PORT_SIZE          16
#define LNX_HEX_BASE                  16
#define LNX_GREATER                   1
#define LNX_LESSER                    -1
#define LNX_EQUAL                     0
#define LNX_ZERO      LNX_EQUAL
typedef struct TCPEntry {
    UINT2    u2LocalPort;  /* local port                        */
    UINT2    u2RemotePort; /* remote port                       */
    INT4     i4TcpState;   /* TCP state                         */
    INT4     i4LocalAddrType;  /* Local IP address type  */
    INT4     i4RemoteAddrType; /* Remote IP address type */
    UINT4    u4LocalIp;    /* Local  Address                    */
    UINT4    u4RemoteIp;   /* remote Address                    */
    UINT4    u4ProcessID;      /* Process ID associated  */
                               /* with the connection    */
} tTcpConnEntry ;

typedef struct UDPEntry {
    UINT2    u2LocalPort;  /* local port                        */
    UINT2    u2Remoteport;    /* Remote Port of the conn */
    INT4     i4LocalAddrType; /* Local Address type of the conn */
    INT4     i4RemoteAddrType;/* Remote Address type of the conn */
    UINT4    u4LocalIp;       /* Local  Address                    */
    UINT4    u4RemoteIp;      /* Remote Address */
    UINT4    u4Instance;      /* Instance associated with the conn */
        
} tUdpEntry ;

#define  LNX_IPV4_LEN     4

/* Function prototypes */
INT1 LnxIpGetObject PROTO ((UINT4 u4ObjectName, UINT4 *pu4RetVal));
INT1 LnxIpParseLine PROTO ((UINT4 u4ObjectName, UINT4 u4ObjectGroup, 
                            UINT1 *pu1Stats, UINT4 *pu4RetVal));
INT1  LnxIpSetForwarding PROTO ((INT4 i4Status));
INT1  LnxIpGetForwarding PROTO ((UINT4 *pu4IpForwarding));
INT4 TcpTableIndexCompare PROTO ((tTcpConnEntry *pTcpConnEntry1,
                           tTcpConnEntry *pTcpConnEntry2));
INT4  TcpConnTableGetNextEntry PROTO ((tTcpConnEntry *tcpConnEntry,
                          tTcpConnEntry *pNextTcpConnEntry));
INT4 UdpTableGetNextEntry (tUdpEntry *udpEntry, tUdpEntry *pNextUdpEntry);
INT4 TcpListTableGetNextEntry PROTO ((tTcpConnEntry *tcpConnEntry,
                                          tTcpConnEntry *pNextTcpConnEntry));
INT4 TcpListTableIndexCompare PROTO ((tTcpConnEntry *pTcpConnEntry1,
                                      tTcpConnEntry *pTcpConnEntry2));
INT1 IncMsrForFsIpv4Scalars (INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen);
#endif
