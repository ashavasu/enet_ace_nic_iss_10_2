
/********************************************************************
 *                                                                  *
 * $Id: netipidxgl.h,v 1.1 2015/10/05 11:02:40 siva Exp $
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *---------------------------------------------------------------------------
 *    FILE  NAME             : netipidxgl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : NETIP - Utility    
 *    MODULE NAME            : Flow Index Tbl Mgr : Flow Table Module
 *    LANGUAGE               : ANSI-C
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains global declarations that 
 *                             are associated with Flow Index Table Manager
 *                             
 *-------------------------------------------------------------------------*/

#ifndef _NETIPIDXGL_H
#define _NETIPIDXGL_H

#include "netipidxmg.h"

/* Global ptr to the table of index groups */
/* Global Pool Id for Memory Pool of bitmap chunks */
#define CHUNK_TBL_POOL_ID  NETIPINDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_TBL_CHUNK_SIZING_ID]
#define GROUP_INFO_POOL_ID NETIPINDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID]
#define CHUNK_INFO_POOL_ID NETIPINDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID]
/* Index manager Global Semaphore */
extern tOsixSemId   gNetipIndexMgrSemId;
#define     NETIP_INDEXMGR_LOCK NetipIndexMgrLock
#define     NETIP_INDEXMGR_UNLOCK NetipIndexMgrUnLock
PUBLIC VOID NetipIndexMgrLock(VOID);
PUBLIC VOID NetipIndexMgrUnLock(VOID);
/* ==== Start : To add a group, define it & append it to the array ==== */
/* The foll. 3 constants need to be configured as per requirement */



/* Define new entries here... */
extern tIndexMgrGrpTable NetipIndexMgrGrpEntry[];
extern UINT4 au4NetipGrpMaxIndices[];
                
                          /* Append newly defined entries here, in this array */
                          /* Max Groups supported is MAXUINT1 = 255*/
      
                          /* Last one is same as Max Indices for
                             Grp No. INDEXMGR_MAX_GRPS_SPRTD */


/* ==== End : To add a group, define it & append it to the array ====== */

/* Global Variables used by Index Resource Manager */
/* Global Array Variable initialised with Key available indication bit maps */

extern UINT4 aNetipAvlByteBmap[];

extern UINT4 aNetipAvlBitmap[];

extern UINT4 aNetipAsgnBitmap[];

#endif /*_NETIPIDXGL_H */

/*-------------------------------------------------------------------------
*                        End of file netipidxgl.h                             
-------------------------------------------------------------------------*/
