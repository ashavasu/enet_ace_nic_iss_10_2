
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdiplow.h,v 1.11 2015/09/15 06:46:54 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpForwarding ARG_LIST((INT4 *));

INT1
nmhGetIpDefaultTTL ARG_LIST((INT4 *));

INT1
nmhGetIpInReceives ARG_LIST((UINT4 *));

INT1
nmhGetIpInHdrErrors ARG_LIST((UINT4 *));

INT1
nmhGetIpInAddrErrors ARG_LIST((UINT4 *));

INT1
nmhGetIpForwDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetIpInUnknownProtos ARG_LIST((UINT4 *));

INT1
nmhGetIpInDiscards ARG_LIST((UINT4 *));

INT1
nmhGetIpInDelivers ARG_LIST((UINT4 *));

INT1
nmhGetIpOutRequests ARG_LIST((UINT4 *));

INT1
nmhGetIpOutDiscards ARG_LIST((UINT4 *));

INT1
nmhGetIpOutNoRoutes ARG_LIST((UINT4 *));

INT1
nmhGetIpReasmTimeout ARG_LIST((INT4 *));

INT1
nmhGetIpReasmReqds ARG_LIST((UINT4 *));

INT1
nmhGetIpReasmOKs ARG_LIST((UINT4 *));

INT1
nmhGetIpReasmFails ARG_LIST((UINT4 *));

INT1
nmhGetIpFragOKs ARG_LIST((UINT4 *));

INT1
nmhGetIpFragFails ARG_LIST((UINT4 *));

INT1
nmhGetIpFragCreates ARG_LIST((UINT4 *));

INT1
nmhGetIpRoutingDiscards ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpForwarding ARG_LIST((INT4 ));

INT1
nmhSetIpDefaultTTL ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpForwarding ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IpDefaultTTL ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpForwarding ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IpDefaultTTL ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IpAddrTable. */
INT1
nmhValidateIndexInstanceIpAddrTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpAddrTable  */

INT1
nmhGetFirstIndexIpAddrTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpAddrTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpAdEntIfIndex ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIpAdEntNetMask ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIpAdEntBcastAddr ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIpAdEntReasmMaxSize ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for IpNetToMediaTable. */
INT1
nmhValidateIndexInstanceIpNetToMediaTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpNetToMediaTable  */

INT1
nmhGetFirstIndexIpNetToMediaTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpNetToMediaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpNetToMediaPhysAddress ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIpNetToMediaType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpNetToMediaPhysAddress ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIpNetToMediaType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpNetToMediaPhysAddress ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IpNetToMediaType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpNetToMediaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCidrRouteNumber ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for IpCidrRouteTable. */
INT1
nmhValidateIndexInstanceIpCidrRouteTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IpCidrRouteTable  */

INT1
nmhGetFirstIndexIpCidrRouteTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIpCidrRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIpCidrRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteProto ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteAge ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetIpCidrRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteHWStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric2 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric3 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric4 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteMetric5 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIpCidrRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIpCidrRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetIpCidrRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric2 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric3 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric4 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteMetric5 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIpCidrRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IpCidrRouteIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteInfo ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2IpCidrRouteNextHopAS ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric2 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric3 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric4 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteMetric5 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IpCidrRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IpCidrRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIcmpInMsgs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInDestUnreachs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInTimeExcds ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInParmProbs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInSrcQuenchs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInRedirects ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInEchos ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInEchoReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInTimestamps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInTimestampReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInAddrMasks ARG_LIST((UINT4 *));

INT1
nmhGetIcmpInAddrMaskReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutMsgs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutErrors ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutDestUnreachs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutTimeExcds ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutParmProbs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutSrcQuenchs ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutRedirects ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutEchos ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutEchoReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutTimestamps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutTimestampReps ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutAddrMasks ARG_LIST((UINT4 *));

INT1
nmhGetIcmpOutAddrMaskReps ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* * Remove the code placed under #ifdef when deleting
 * * the deprecated objects of standard IP mib.
 * * This code is provided to support backward comp.*/

#ifdef REMOVE_ON_STDUDP_DEPR_OBJ_DELETE
INT1
nmhGetUdpInDatagrams ARG_LIST((UINT4 *));

INT1
nmhGetUdpNoPorts ARG_LIST((UINT4 *));

INT1
nmhGetUdpInErrors ARG_LIST((UINT4 *));

INT1
nmhGetUdpOutDatagrams ARG_LIST((UINT4 *));
#endif
/* Proto Validate Index Instance for UdpTable. */
INT1
nmhValidateIndexInstanceUdpTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for UdpTable  */

INT1
nmhGetFirstIndexUdpTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexUdpTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* extern definitions of FSARP objects (being used in ipcli.c) */
extern INT1 nmhGetFsArpCacheTimeout ARG_LIST((INT4 *));
extern INT1 nmhGetFsArpMaxRetries ARG_LIST((INT4 *));
extern INT1 nmhSetFsArpCacheTimeout ARG_LIST((INT4 ));
extern INT1 nmhSetFsArpCacheFlushStatus ARG_LIST((INT4 ,UINT4));
extern INT1 nmhTestv2FsArpCacheFlushStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetFsArpMaxRetries ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsArpCacheTimeout ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsArpMaxRetries ARG_LIST((UINT4 *  ,INT4 ));
extern INT4 FsArpCacheTimeoutSet ARG_LIST((tSnmpIndex *, tRetVal *));
extern INT4 FsArpCachePendTimeSet ARG_LIST((tSnmpIndex *, tRetVal *));
extern INT4 FsArpMaxRetriesSet ARG_LIST((tSnmpIndex *, tRetVal *));
extern INT1 nmhGetFsArpGlobalDebug ARG_LIST((INT4 *));
extern INT1 nmhSetFsArpGlobalDebug ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsArpGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));
extern INT4 FsArpGlobalDebugGet(tSnmpIndex *, tRetVal *);
extern INT4 FsArpGlobalDebugSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsMIArpContextDebug ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsMIArpContextDebug ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIArpContextDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 FsMIArpContextDebugGet(tSnmpIndex *, tRetVal *);
extern INT4 FsMIArpContextDebugSet(tSnmpIndex *, tRetVal *);
