
/* $Id: lip4ipvx.h,v 1.4 2011/11/14 12:05:11 siva Exp $*/

#ifndef __LIP4_IPVX_H__
#define __LIP4_IPVX_H__

/* Icmp Codes for stats */

  #define  IPVX_ICMP_ECHO_REPLY         0
  #define  IPVX_ICMP_UNASSIGNED1        1 
  #define  IPVX_ICMP_UNASSIGNED2        2
  #define  IPVX_ICMP_DEST_UNREAC        3
  #define  IPVX_ICMP_SRC_QUENCH         4
  #define  IPVX_ICMP_REDIRECT           5
  #define  IPVX_ICMP_ALT_HOST_ADDR      6
  #define  IPVX_ICMP_UNASSIGNED3        7
  #define  IPVX_ICMP_ECHO               8
  #define  IPVX_ICMP_RTR_ADV            9
  #define  IPVX_ICMP_RTR_SOL           10
  #define  IPVX_ICMP_TIME_EXCEEDED     11
  #define  IPVX_ICMP_PARA_PROBLEM      12
  #define  IPVX_ICMP_TIME_STAMP        13
  #define  IPVX_ICMP_TSTAMP_REPLY      14
  #define  IPVX_ICMP_INFO_REQUEST      15
  #define  IPVX_ICMP_INFO_REPLY        16
  #define  IPVX_ICMP_ADDR_MASK_RQST    17
  #define  IPVX_ICMP_ADDR_MASK_REPLY   18
  #define  IPVX_ICMP_RESERVED1         19
  #define  IPVX_ICMP_RESERVED2_S       20
  #define  IPVX_ICMP_RESERVED3_E       29
  #define  IPVX_ICMP_TRACEROUTE        30  
  #define  IPVX_ICMP_DGRAM_CONV_ERR    31 
  #define  IPVX_ICMP_MOBILE_HOST_RDIR  32 
  #define  IPVX_ICMP_IPV6_WAY          33 
  #define  IPVX_ICMP_IPV6_IMHERE       34 
  #define  IPVX_ICMP_MOBILE_REG_RQST   35 
  #define  IPVX_ICMP_MOBILE_REG_REPLY  36 
  #define  IPVX_ICMP_DOMAIN_NAME_RQST  37 
  #define  IPVX_ICMP_DOMAIN_NAME_REPLY 38 
  #define  IPVX_ICMP_SKIP              39 
  #define  IPVX_ICMP_PHOTURIS          40 
  #define  IPVX_ICMP_ICMP_MSG_EXP      41 

extern t_ARP_HW_TABLE *(*arp_get_hw_entry) PROTO ((INT2 i2Hardware)),
        *(*arp_get_free_hw_entry) PROTO ((INT2 i2Hardware));
INT1
IPvxGetARPEntry (INT4  i4IPv4IfIndex, UINT4 u4Ipv4Addr, 
                 t_ARP_CACHE *pArpEntry);

extern INT4
CfaIpIfGetNextIndexIpIfTable (UINT4, UINT4 *);

#endif /* __IP4_IPVX_H__ */

/****************************   End of the File ***************************/

