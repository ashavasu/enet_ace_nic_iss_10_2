#!/bin/csh
#* $Id: make.h,v 1.6 2015/10/05 10:45:30 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   :                                               |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the mgmt module        |
# |                                                                          |
# +--------------------------------------------------------------------------+

# Set the PROJ_BASE_DIR as the directory where you untar the project files
NETIP_BASE_DIR = ${BASE_DIR}/netip
IPMGMT_BASE_DIR  = ${NETIP_BASE_DIR}/ipmgmt
IPMGMT_SRCD      = ${IPMGMT_BASE_DIR}/src
IPMGMT_INCD      = ${IPMGMT_BASE_DIR}/inc
IPMGMT_OBJD      = ${IPMGMT_BASE_DIR}/obj

RTM_BASE_DIR   = ${NETIP_BASE_DIR}/rtm
RTM_INCD       = ${RTM_BASE_DIR}/inc

IPMGMT_INCL_DIRS = ${COMMON_INCLUDE_DIRS} \
                   -I ${IPMGMT_INCD} \
                   -I ${RTM_INCD}

ifeq (${FSIP4}, YES)
IPMGMT_INCL_DIRS += ${IP_GLOBAL_INCLUDES}

IP_TCPINCD        = ${BASE_DIR}/tcp
IPMGMT_INCL_DIRS += -I ${IP_TCPINCD}
endif

IPMGMT_DPNDS = ${COMMON_DEPENDENCIES} \
               ${IPMGMT_BASE_DIR}/make.h \
               ${IPMGMT_BASE_DIR}/Makefile \
               ${IPMGMT_INCD}/stdipwr.h \
               ${IPMGMT_INCD}/stdiplow.h \
               ${IPMGMT_INCD}/stdipdb.h \
               ${IPMGMT_INCD}/lnxiputl.h \
               ${IPMGMT_INCD}/netipidxsz.h \
               ${IPMGMT_INCD}/netipidxmg.h \
               ${IPMGMT_INCD}/netipidxgl.h \
               ${COMN_INCL_DIR}/cli/ipcli.h \
               ${COMN_INCL_DIR}/ip.h \
               ${COMN_INCL_DIR}/arp.h \
               ${COMN_INCL_DIR}/rtm.h

C_FLAGS = $(CC_FLAGS) \
          $(GENERAL_COMPILATION_SWITCHES) \
          ${SYSTEM_COMPILATION_SWITCHES} \
          ${IPMGMT_INCL_DIRS} 
