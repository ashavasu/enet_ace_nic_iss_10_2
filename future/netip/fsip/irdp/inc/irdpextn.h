/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdpextn.h,v 1.3 2007/02/01 14:58:00 iss Exp $
 *
 * Description: Contains externs used by IRDP submodule
 *
 *******************************************************************/
extern UINT4   gu4IrdpInSolicitCount;
extern UINT4   gu4IrdpInAdvertCount;
extern UINT4   gu4IrdpOutAdvertCount;
extern UINT4   gu4IrdpOutSolicitCount;

extern tIrdpIfaceRec       gaIrdpIfaceRec[IPIF_MAX_LOGICAL_IFACES];
extern tIrdpStateCntlRec   gaIrdpCntlRec[IPIF_MAX_LOGICAL_IFACES];
extern tIrdpRouterList     gaIrdpRtrList[IRDP_MAX_DEFAULT_ROUTERS];
extern tIrdpQueue          gaIrdpQueue[IRDP_MAX_Q_SIZE];
