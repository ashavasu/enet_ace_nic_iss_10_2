/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdpport.h,v 1.3 2013/07/04 13:12:01 siva Exp $
 *
 * Description: Contains wrapper definitions for IRDP Module
 *
 *******************************************************************/
#define IRDP_LINK_TIMER(ListPtr, NodePtr, NoOfTicks) \
           TmrStartTimer((ListPtr),(tTMO_APP_TIMER *) (NodePtr), \
           LR_NUM_OF_SYS_TIME_UNITS_IN_A_SEC * (NoOfTicks))

#define IRDP_UNLINK_TIMER(ListPtr, NodePtr) \
           TmrStopTimer((ListPtr), (tTMO_APP_TIMER *) (NodePtr))

/* Left out the last semi-colon, coz one is available in the called place */
#define IRDP_GET_4_BYTES(pBuf, offset, value) \
           CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *)&value, (offset), 4); \
           value = IP_NTOHL((UINT4) value)

#define IRDP_ALLOC_BUFFER(BufferSize, ValidOffset) \
           IP_ALLOCATE_BUF((BufferSize), (ValidOffset))

#define IRDP_PUT_4_BYTES(pBuf, Offset, Value) \
        {\
          UINT4 _u4Value; \
          _u4Value = IP_HTONL((UINT4) (Value)); \
          CRU_BUF_Copy_OverBufChain ((pBuf), (UINT1 *)&_u4Value, (Offset), 4); \
          (Offset) = (Offset) + 4; \
        }

#define IRDP_GET_TICK(pSysTime) OsixGetSysTime((pSysTime))

/* The following macros are still available in FSAP2 */
#define IRDP_MOVE_TO_DATA(pBuf) \
           IP_BUF_MOVE_VALID_OFFSET(pBuf, ICMP_HDR_LEN)

#define IRDP_MOVE_TO_NEXT_TUPLE(pBuf, TupleSize) \
           IP_BUF_MOVE_VALID_OFFSET(pBuf, (TupleSize))

#define IRDP_MOVE_BACK_TO_ICMP_HDR(pBuf) \
           IP_BUF_MOVE_BACK_VALID_OFFSET(pBuf, ICMP_HDR_LEN)
