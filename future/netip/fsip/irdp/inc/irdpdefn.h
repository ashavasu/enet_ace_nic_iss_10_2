/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdpdefn.h,v 1.3 2007/02/01 14:58:00 iss Exp $
 *
 * Description: Contains defined constants for IRDP module.
 *
 *******************************************************************/
#define   IPIF_MAX_ADDR_PER_IFACE     IP_DEV_MAX_ADDR_PER_IFACE
#define   IRDP_MAX_DEFAULT_ROUTERS                    10
#define   IRDP_MAX_Q_SIZE                              5
#define   IRDP_MAX_RESP_DELAY                          2
#define   IRDP_LEAST_PREFERENCE               0x80000000
#define   IRDP_DYNAMIC                                 2
#define   IRDP_ADVERT_TIMER_ID                         9
#define   IRDP_SOLICIT_TIMER_ID                       10
#define   IRDP_ROUTER_LIST_TIMER_ID                   11
#define   IRDP_MAX_INITIAL_ADVERTISEMENTS              3
#define   IRDP_MAX_INITIAL_ADVERT_INTERVAL            16
#define   IRDP_MAX_SOLICITATIONS                       3
#define   IRDP_INVALID_IFACE                      0xffff
#define   IRDP_BYTES_PER_ENTRY                         4
#define   IRDP_PADDING_WORD                            8
#define   IRDP_ADVERTISE                               1
#define   IRDP_DISCOVER                                1
#define   IRDP_DONT_DISCOVER                           2
#define   IRDP_DONT_ADVERTISE                          2
#define   IRDP_DEF_MAX_INTERVAL                      600
#define   IRDP_DEF_MIN_INTERVAL                      450
#define   IRDP_DEF_ADVERT_LTIME                     1800
#define   IRDP_DEF_ADVERT_ADDR                0xe0000001
#define   IRDP_DEF_SOLICIT_ADDR               0xe0000002
#define   IRDP_SOLICITATION_INTERVAL                   3
#define   IRDP_MAX_SOLICITATION_DELAY                  5
#define   IRDP_ADDR_ENTRY_SIZE                         2
#define   IRDP_ACTIVE                                  1
#define   IRDP_NOT_IN_SERVICE                          2
#define   IRDP_NOT_READY                               3
#define   IRDP_CREATE_AND_WAIT                         5
#define   IRDP_DESTROY                                 6
#define   IRDP_STATIC                                  1
#define   IRDP_NO_SPACE                                0
#define   IRDP_ALL_SYSTEMS_MULTI              0xe0000001
#define   IRDP_ALL_ROUTERS_MULTI              0xe0000002
#define   IRDP_NOT_KNOWN_YET                          -1
#define   ENOTNEIGHBOR                                -1
#define   EPKTQUED                                    -2
