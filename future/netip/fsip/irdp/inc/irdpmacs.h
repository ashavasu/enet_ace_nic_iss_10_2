/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdpmacs.h,v 1.3 2009/08/24 13:11:42 prabuc Exp $
 *
 * Description: Contains Macro definitions for IRDP Module.
 *
 *******************************************************************/
#define IRDP_SET_ADVERTFLAG(u2Iface, u2Index, value) \
if ((u2Iface < IPIF_MAX_LOGICAL_IFACES) && (u2Index < IPIF_MAX_ADDR_PER_IFACE))\
{\
    gaIrdpIfaceRec[(u2Iface)].aAddrTable[(u2Index)].bAdvertFlag=(value);\
}

#define IRDP_SET_PREFLEVEL(u2Iface, u2Index, value) \
if ((u2Iface < IPIF_MAX_LOGICAL_IFACES) && (u2Index < IPIF_MAX_ADDR_PER_IFACE))\
{\
    gaIrdpIfaceRec[(u2Iface)].aAddrTable[(u2Index)].i4PrefLevel=(value);\
}

#define IRDP_GET_ROUTER_ADDR(Index) \
((Index < IRDP_MAX_DEFAULT_ROUTERS) ? gaIrdpRtrList[(Index)].u4RouterAddress:0)

#define IRDP_GET_PREFLEVEL(u2Iface, u2Index) \
(((u2Iface < IPIF_MAX_ADDR_PER_IFACE) && (u2Index < IPIF_MAX_ADDR_PER_IFACE)) ?\
 gaIrdpIfaceRec[(u2Iface)].aAddrTable[(u2Index)].i4PrefLevel : 0)

#define IRDP_PERFORM_ROUTER_DISCOVERY(u2Iface) \
           gaIrdpIfaceRec[(u2Iface)].bPerformRouterDiscovery

#define IRDP_GET_ADDRESS(u2Iface, u2Index) \
(((u2Iface < IPIF_MAX_LOGICAL_IFACES) && (u2Index < IPIF_MAX_ADDR_PER_IFACE))? \
 gaIrdpIfaceRec[(u2Iface)].aAddrTable[(u2Index)].u4Address : 0)

#define IRDP_GET_ADVERTFLAG(u2Iface, u2Index) \
(((u2Iface < IPIF_MAX_LOGICAL_IFACES) && (u2Index < IPIF_MAX_ADDR_PER_IFACE))? \
 gaIrdpIfaceRec[(u2Iface)].aAddrTable[(u2Index)].bAdvertFlag : 0)
