/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdptdfs.h,v 1.2 2007/02/01 14:58:00 iss Exp $
 *
 * Description: Contains structure declarations of IRDP.
 *
 *******************************************************************/
typedef struct
{
    BOOLEAN  bAdvertFlag;
    UINT1    u1RowStatus;
    UINT2    u2AlignmentWord;
    UINT4    u4Address;
    INT4     i4PrefLevel;
} tIrdpAddrRec;

typedef struct
{
    t_IP_TIMER    Timer;
    BOOLEAN       bPerformRouterDiscovery;
    UINT1         u1AlignmentByte;
    UINT2         u2AlignmentWord;
    UINT2         u2Iface;
    UINT2         u2MaxAdvertInterval;
    UINT2         u2MinAdvertInterval;
    UINT2         u2AdvertLifeTime;
    UINT4         u4SolicitAddr;
    UINT4         u4AdvertAddr;
    tIrdpAddrRec  aAddrTable[IPIF_MAX_ADDR_PER_IFACE];
} tIrdpIfaceRec;

typedef struct
{
    UINT1    u1InitAdvertCount;
    UINT1    u1OutSolicitCount;
    BOOLEAN  bSendAdvert;
    BOOLEAN  bSendSolicit;
    BOOLEAN  bBcastFlag;
    BOOLEAN  bProcessAdvert;
    UINT2    u2AlignmentWord;
} tIrdpStateCntlRec;

typedef struct
{
    t_IP_TIMER  Timer;
    BOOLEAN     bDynamicEntry;
    UINT1       u1RowStatus;
    UINT2       u2Iface;
    UINT4       u4RouterAddress;
    INT4        i4PrefLevel;
} tIrdpRouterList;

typedef struct
{
    tIP_BUF_CHAIN_HEADER  *pBuf;
    UINT2                 u2AlignmentWord;
    UINT2                 u2Iface;
    t_ICMP                IcmpHdr;
} tIrdpQueue;
