/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdpprot.h,v 1.5 2011/10/25 09:53:36 siva Exp $
 *
 * Description: Contains Prototype definitions in IRDP Module.
 *
 *******************************************************************/
VOID IpifGetAllAddr PROTO ((UINT2 u2Iface, tIrdpAddrRec * pAddrTable));

INT4 IrdpSolicitationHandler PROTO ((UINT2 u2Iface, UINT4 u4SrcAddr,
                                     UINT4 u4DestAddr));

INT4 IrdpAdvertisementHandler PROTO ((tIP_BUF_CHAIN_HEADER * pBuf,
                                      t_ICMP * pIcmp, UINT2 u2Iface));

VOID IrdpTimerExpiryHandler PROTO ((t_IP_TIMER * pTimer));
VOID IrdpInitialize PROTO ((VOID));
VOID IrdpAddInterface PROTO ((UINT2 u2Iface));
VOID IrdpDeleteInterface PROTO ((UINT2 u2Iface));
VOID IrdpDisableInterface PROTO ((UINT2 u2Iface));
VOID IrdpEnableInterface PROTO ((UINT2 u2Iface));
VOID IrdpSwitchToServer PROTO ((VOID));
VOID IrdpSwitchToClient PROTO ((VOID));
INT4 IrdpMapIndices PROTO ((UINT4 u4RtrAddress, UINT2 * pu2Iface,
                            UINT2 * pu2Index));

VOID IrdpInitRouterList PROTO ((VOID));
VOID IrdpServerInit PROTO ((UINT2 u2Iface));
VOID IrdpClientInit PROTO ((UINT2 u2Iface));
VOID IrdpInitQueue PROTO ((VOID));
INT1 IrdpGetStaticCount PROTO ((VOID));
INT1 IrdpGetFreeSpaceInRtrList PROTO ((VOID));
INT1 IrdpGetFreeSpaceInQueue PROTO ((VOID));
BOOLEAN IrdpCheckNeighborHood PROTO ((UINT4 u4SrcAddr, UINT2 u2Iface));
UINT2 IrdpSearchRtrList PROTO ((UINT4 u4RouterAddr));
UINT2 IrdpGetFreeIndexInRtrList PROTO ((BOOLEAN bEntryType));
UINT2 IrdpGetFreeIndexInQueue PROTO ((VOID));
VOID IrdpDeleteEntryFromRtrList PROTO ((UINT2 u2Index));
VOID IrdpDeleteEntryFromQueue PROTO ((UINT2 u2Index));
VOID IrdpProcessQueue PROTO ((UINT2 u2Iface));

UINT4 IrdpGetRandom PROTO ((UINT2 u2Iface,
                            UINT2 u2LeastAllowdValue, UINT2 u2MaxAllowdValue));

VOID IrdpSendNotAvailableNotification PROTO ((UINT2 u2Iface, UINT2 u2Index));

UINT2 IrdpGetIndexToLeastPreferedRouter PROTO ((VOID));
INT4 IrdpGetDefaultRoute PROTO ((UINT2 * pu2Port, UINT4 * pu4Gateway));


/* Prototypes for low level routines --- IP Router List */
INT1 nmhSetFsIpRtrLstAddress PROTO ((UINT4, UINT4));
INT1 nmhTestv2FsIpRtrLstAddress PROTO ((UINT4 *, UINT4, UINT4));


