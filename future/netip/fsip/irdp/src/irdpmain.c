/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdpmain.c,v 1.11 2014/02/24 11:27:56 siva Exp $
 *
 * DESCRIPTION:CONTAINS FUNCTIONS FOR IRDP CLIENT AND SERVER
 *
 *******************************************************************/
#include "irdpinc.h"

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/
PRIVATE VOID IrdpAddrTableInit PROTO ((UINT2 u2Iface));
PRIVATE INT4 IrdpGenerateSolicitation PROTO ((UINT2 u2Iface));
PRIVATE INT4 IrdpGenerateAdvertisement PROTO ((UINT4 u4DestAddr,
                                               UINT2 u2Iface));

PRIVATE VOID        IrdpProcessAdvertisement (tIP_BUF_CHAIN_HEADER * pBuf,
                                              t_ICMP * pIcmp, UINT2 u2Iface);
#ifndef SLI_WANTED
#ifndef SLI_EMEMFAIL
#define  SLI_EMEMFAIL                          -15
#endif
#endif

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
/* Statistics Counters */
UINT4               gu4IrdpInSolicitCount;
UINT4               gu4IrdpInAdvertCount;
UINT4               gu4IrdpOutAdvertCount;
UINT4               gu4IrdpOutSolicitCount;

/* Main data structures for IRDP module */

tIrdpIfaceRec       gaIrdpIfaceRec[IPIF_MAX_LOGICAL_IFACES];
tIrdpStateCntlRec   gaIrdpCntlRec[IPIF_MAX_LOGICAL_IFACES];
tIrdpRouterList     gaIrdpRtrList[IRDP_MAX_DEFAULT_ROUTERS];
tIrdpQueue          gaIrdpQueue[IRDP_MAX_Q_SIZE];

/**************************************************************************/

/*-------------------------------------------------------------------+
 * Function           : IrdpSolicitationHandler 
 *
 * Input(s)           : u2Iface, u4SrcAddr, u4DestAddr
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Does the following :
 *  
 * + Validates the Solicitation
 * + If valid solicitation, 
 *      + sends a unicast reply to a unicast request immediately.
 *      + sends a multicast or broadcast reply after IRDP_MAX_RESP_DELAY
 * 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT INT4
IrdpSolicitationHandler (UINT2 u2Iface, UINT4 u4SrcAddr, UINT4 u4DestAddr)
#else
EXPORT INT4
IrdpSolicitationHandler (u2Iface, u4SrcAddr, u4DestAddr)
     UINT2               u2Iface;    /* Interface on which the packet arrived. */
     UINT4               u4SrcAddr;    /* Source IP address of the Solicitation. */
     UINT4               u4DestAddr;    /* Destination IP address of the Solicitation. */
#endif
{
    UINT4               u4TransIntvl;
    INT4                i4RetVal;

    IP_TRC_ARG1 (ICMP_MOD_TRC, DATA_PATH_TRC, ICMP_NAME,
                 "IRDP Solicitation received from %x \n", u4SrcAddr);

    if (IrdpCheckNeighborHood (u4SrcAddr, u2Iface) == FALSE &&
        u4SrcAddr != IP_ANY_ADDR)
    {
        return ENOTNEIGHBOR;
    }

    gu4IrdpInSolicitCount++;

    i4RetVal = SUCCESS;

    if (gaIrdpCntlRec[u2Iface].bSendAdvert == TRUE)
    {
        if (u4SrcAddr == IP_ANY_ADDR)
        {                        /* ^ 0.0.0.0 */
            /*
             * Even if the Timer was not linked earlier to the list
             * this call is not harmless since the function checks 
             * this possibility and exists gracefully.
             */
            IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                               &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);

            gaIrdpIfaceRec[u2Iface].Timer.Timer_node.u4Data = u2Iface;
            gaIrdpIfaceRec[u2Iface].Timer.u1Id = IRDP_ADVERT_TIMER_ID;

            u4TransIntvl = IrdpGetRandom (u2Iface, 0, IRDP_MAX_RESP_DELAY);
            IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                             &gaIrdpIfaceRec[u2Iface].Timer.Timer_node,
                             u4TransIntvl);
            /* 
             * If the Solicitation was broadcasted the Reply should also be 
             * broadcasted. 
             */

            if (u4DestAddr == IP_GEN_BCAST_ADDR)
            {                    /* ^ 0xffffffff */
                gaIrdpCntlRec[u2Iface].bBcastFlag = TRUE;
            }

        }
        else
        {
            /*
             * Solicitation was send to a unicast address so sending an
             * immediate unicast Advertisement.
             */

            i4RetVal = IrdpGenerateAdvertisement (u4SrcAddr, u2Iface);

            if (i4RetVal == SUCCESS)
            {
                gu4IrdpOutAdvertCount++;
            }
        }
    }
    return i4RetVal;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpAdvertisementHandler 
 *
 * Input(s)           : pBuf, pIcmp, u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Does the following : 
 *
 * If Perform Router Discovery is TRUE on the received interface
 *  
 * + If IP address on the received interface is known
 *      + Processes the Advertisement and updates the Router List by
 *        calling IrdpProcessAdvertisement.
 * + else if Queue is not Full
 *      + Required informations for processing the Advertisement are 
 *        stored in the Queue.
 *
 * NOTE : When the Packet is Queued Buffer is not released. It will be 
 *        released only when IrdpProcessQueue is called. Whoever learns the 
 *        IP address on that interface has to call this function.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT INT4
IrdpAdvertisementHandler (tIP_BUF_CHAIN_HEADER * pBuf,
                          t_ICMP * pIcmp, UINT2 u2Iface)
#else
EXPORT INT4
IrdpAdvertisementHandler (pBuf, pIcmp, u2Iface)
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Points to the data part of the ICMP pkt */
     t_ICMP             *pIcmp;
     UINT2               u2Iface;    /* Interface on which the packet arrived. */
#endif
{
    INT4                i4RetVal;    /* Return Value of this function */
    UINT2               u2Index;

    i4RetVal = SUCCESS;

    if (gaIrdpIfaceRec[u2Iface].bPerformRouterDiscovery != IRDP_DONT_DISCOVER)
    {
        gu4IrdpInAdvertCount++;

        IP_TRC_ARG3 (ICMP_MOD_TRC, DATA_PATH_TRC, "IRDP",
                     "rcvd advertisement numaddr=%d, size=%d, lifetime=%d.\n",
                     pIcmp->args.Irdp.u1NumAddr,
                     pIcmp->args.Irdp.u1AddrEntrySize,
                     pIcmp->args.Irdp.u2LifeTime);

        if (gaIrdpCntlRec[u2Iface].bProcessAdvert == TRUE)
        {
            IrdpProcessAdvertisement (pBuf, pIcmp, u2Iface);
        }
        else if (IrdpGetFreeSpaceInQueue () != IRDP_NO_SPACE)
        {
            /* 
             * IP address on this interface is not yet known so
             * store the parameters in Queue.
             */

            u2Index = IrdpGetFreeIndexInQueue ();
            if (u2Index < IRDP_MAX_Q_SIZE)
            {
                gaIrdpQueue[u2Index].pBuf = pBuf;
                gaIrdpQueue[u2Index].u2Iface = u2Iface;

                gaIrdpQueue[u2Index].IcmpHdr.i1Type = pIcmp->i1Type;
                gaIrdpQueue[u2Index].IcmpHdr.i1Code = pIcmp->i1Code;

                gaIrdpQueue[u2Index].IcmpHdr.args.Irdp.u1NumAddr =
                    pIcmp->args.Irdp.u1NumAddr;
                gaIrdpQueue[u2Index].IcmpHdr.args.Irdp.u1AddrEntrySize =
                    pIcmp->args.Irdp.u1AddrEntrySize;
                gaIrdpQueue[u2Index].IcmpHdr.args.Irdp.u2LifeTime =
                    pIcmp->args.Irdp.u2LifeTime;

                /*
                 * EPKTQUED does not mean error. It just informs ICMP 
                 * not to release the pBuf. 
                 */

                i4RetVal = EPKTQUED;
            }
        }
    }
    return i4RetVal;

}

/*-------------------------------------------------------------------+
 * Function           : IrdpProcessAdvertisement 
 *
 * Input(s)           : pBuf, pIcmp, u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Does the following :
 *  
 * + Checks if free space is available for Dynamic Entries.
 * + For every tuple in the Advertisement does the following 
 *   
 *    - If its already in the list and not statically configured
 *      updates the preference level and resets the timer.
 *
 *    - If not in the list, and the list has free space, adds the new entry
 *      to the list.
 *
 *    - If free space is not available and if the new entry is more 
 *      prefered than the least prefered entry in the list, the least 
 *      prefered entry is replaced with the new entry.
+-------------------------------------------------------------------*/
#ifdef __STDC__
PRIVATE VOID
IrdpProcessAdvertisement (tIP_BUF_CHAIN_HEADER * pBuf,
                          t_ICMP * pIcmp, UINT2 u2Iface)
#else
PRIVATE VOID
IrdpProcessAdvertisement (pBuf, pIcmp, u2Iface)
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Points to the ICMP header */
     t_ICMP             *pIcmp;    /* ICMP Header information   */
     UINT2               u2Iface;    /* Interface on which the packet arrived. */
#endif
{
    UINT1               u1TupleSize;    /* Address Entry Size */
    UINT1               u1TupleCnt;    /* Number of IP addresses advertised in this packet */
    UINT2               u2LifeTime;    /* Hold time after which the addr. should be purged */
    UINT1               u1TupleNo;    /* Counter */
    UINT4               u4RtrAddr;    /* Advertised Router Address */
    INT4                i4PrefLevel;    /* Preference Level of the Advertised Address */
    UINT4               u4TempPrefLevel = 0;
    UINT2               u2Index;

    /*
     * i2IndexToLeastPrefered has the index into the router list pointing 
     * to the least prefered Router in the neighborhood. If List is full and 
     * a new entry arrives with more preference, the least prefered is deleted
     * and the new entry is added.
     */

    static INT2         i2IndexToLeastPrefered = IRDP_NOT_KNOWN_YET;
    /* ^ -1 */

    if (u2Iface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return;
    }
    if (IrdpGetStaticCount () < IRDP_MAX_DEFAULT_ROUTERS)
    {                            /* ^ 10 */
        u1TupleCnt = pIcmp->args.Irdp.u1NumAddr;
        u1TupleSize = (UINT1) (pIcmp->args.Irdp.u1AddrEntrySize *
                               IRDP_BYTES_PER_ENTRY);
        u2LifeTime = pIcmp->args.Irdp.u2LifeTime;    /* ^ 4 */

        IRDP_MOVE_TO_DATA (pBuf);

        for (u1TupleNo = 0; u1TupleNo < u1TupleCnt; u1TupleNo++)
        {
            IRDP_GET_4_BYTES (pBuf, 0, u4RtrAddr);
            IRDP_GET_4_BYTES (pBuf, 4, u4TempPrefLevel);
            i4PrefLevel = (INT4) u4TempPrefLevel;

            IP_TRC_ARG2 (ICMP_MOD_TRC, DATA_PATH_TRC, "IRDP",
                         "address = %x, preference = %d.\n", u4RtrAddr,
                         i4PrefLevel);

            if (IrdpCheckNeighborHood (u4RtrAddr, u2Iface) == TRUE)
            {
                /* 
                 * The Router is in the NeighborHood. Now searching the 
                 * router list.
                 */

                u2Index = IrdpSearchRtrList (u4RtrAddr);

                if (u2Index < IRDP_MAX_DEFAULT_ROUTERS)
                {
                    /* 
                     * The Router has an entry already in the 
                     * default router list. If the entry was statically 
                     * configured it wouldn't be touched.
                     */

                    if (gaIrdpRtrList[u2Index].bDynamicEntry == IRDP_DYNAMIC)
                    {
                        /*
                         * The Router Entry was dynamically learned so 
                         * the entry can be updated.
                         */

                        if (i4PrefLevel == (INT4) IRDP_LEAST_PREFERENCE)
                        {
                            /*
                             * The latest advertisement advises not to 
                             * use this Router. So deleting it from the 
                             * default router list.
                             */

                            IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                               &gaIrdpRtrList[u2Index].Timer.
                                               Timer_node);
                            IrdpDeleteEntryFromRtrList (u2Index);
                        }
                        else
                        {
                            /*
                             * Updating the preference level and 
                             * resetting the timer to the new value.
                             */

                            gaIrdpRtrList[u2Index].i4PrefLevel = i4PrefLevel;

                            IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                               &gaIrdpRtrList[u2Index].Timer.
                                               Timer_node);
                            gaIrdpRtrList[u2Index].Timer.u1Id =
                                IRDP_ROUTER_LIST_TIMER_ID;

                            gaIrdpRtrList[u2Index].Timer.Timer_node.u4Data =
                                u2Index;
                            IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                             &gaIrdpRtrList[u2Index].Timer.
                                             Timer_node, u2LifeTime);
                            if (i4PrefLevel <
                                gaIrdpRtrList[i2IndexToLeastPrefered].
                                i4PrefLevel)
                            {
                                i2IndexToLeastPrefered = (INT2) u2Index;
                            }
                        }
                    }
                }
                else
                {
                    if (i4PrefLevel != (INT4) IRDP_LEAST_PREFERENCE)
                    {
                        /*
                         * Have discovered a router in the neighborhood
                         * so stop sending the Solicitations and just listen
                         * to further advertisements.
                         */

                        gaIrdpCntlRec[u2Iface].bSendSolicit = FALSE;

                        if (IrdpGetFreeSpaceInRtrList () != IRDP_NO_SPACE)
                        {
                            u2Index = IrdpGetFreeIndexInRtrList (IRDP_DYNAMIC);

                            if (u2Index >= IRDP_MAX_DEFAULT_ROUTERS)
                            {
                                return;
                            }

                            gaIrdpRtrList[u2Index].bDynamicEntry = IRDP_DYNAMIC;

                            gaIrdpRtrList[u2Index].u2Iface = u2Iface;
                            gaIrdpRtrList[u2Index].u4RouterAddress = u4RtrAddr;
                            gaIrdpRtrList[u2Index].i4PrefLevel = i4PrefLevel;
                            gaIrdpRtrList[u2Index].u1RowStatus = IRDP_ACTIVE;

                            gaIrdpRtrList[u2Index].Timer.u1Id =
                                IRDP_ROUTER_LIST_TIMER_ID;

                            gaIrdpRtrList[u2Index].Timer.Timer_node.u4Data =
                                u2Index;
                            IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                             &gaIrdpRtrList[u2Index].Timer.
                                             Timer_node, u2LifeTime);
                            if (i2IndexToLeastPrefered < 0)
                            {
                                i2IndexToLeastPrefered = (INT2) u2Index;
                            }
                            else if (i4PrefLevel <
                                     gaIrdpRtrList
                                     [i2IndexToLeastPrefered].i4PrefLevel)
                            {
                                i2IndexToLeastPrefered = (INT2) u2Index;
                            }
                        }
                        else if (i4PrefLevel >
                                 gaIrdpRtrList
                                 [i2IndexToLeastPrefered].i4PrefLevel)
                        {
                            /* 
                             * List is FULL and we have a new entry, which has
                             * higher preference than the least prefered router 
                             * in the list. So delete the least prefered router 
                             * and add the new entry.
                             */

                            IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                               &gaIrdpRtrList
                                               [i2IndexToLeastPrefered].Timer.
                                               Timer_node);

                            IrdpDeleteEntryFromRtrList ((UINT2)
                                                        i2IndexToLeastPrefered);

                            u2Index = IrdpGetFreeIndexInRtrList (IRDP_DYNAMIC);

                            if (u2Index >= IRDP_MAX_DEFAULT_ROUTERS)
                            {
                                return;
                            }

                            gaIrdpRtrList[u2Index].bDynamicEntry = IRDP_DYNAMIC;

                            gaIrdpRtrList[u2Index].u2Iface = u2Iface;
                            gaIrdpRtrList[u2Index].u4RouterAddress = u4RtrAddr;
                            gaIrdpRtrList[u2Index].i4PrefLevel = i4PrefLevel;

                            gaIrdpRtrList[u2Index].Timer.u1Id =
                                IRDP_ROUTER_LIST_TIMER_ID;

                            gaIrdpRtrList[u2Index].Timer.Timer_node.u4Data =
                                u2Index;

                            IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                             &gaIrdpRtrList[u2Index].Timer.
                                             Timer_node, u2LifeTime);
                            /* Get the next least prefered router */
                            i2IndexToLeastPrefered =
                                (INT2) IrdpGetIndexToLeastPreferedRouter ();

                        }        /* End of i1FreeSpace not EMPTY Check */
                    }
                }                /* End of address in List Check */
            }
            IRDP_MOVE_TO_NEXT_TUPLE (pBuf, u1TupleSize);
        }                        /* End of for loop */
    }                            /* End of i1StaticEntryCount Check */

}

/*-------------------------------------------------------------------+
 * Function           : IrdpTimerExpiryHandler 
 *
 * Input(s)           : pTimer
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Handles three types of timers :
 *   + Timer for Sending Advertisement.
 *   + Timer for Sending Solicitation.
 *   + Timer for Purging Entries in the Router List.
 *  
 * 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpTimerExpiryHandler (t_IP_TIMER * pTimer)
#else
EXPORT VOID
IrdpTimerExpiryHandler (pTimer)
     t_IP_TIMER         *pTimer;
#endif
{
    UINT2               u2Index;
    UINT4               u4TransIntvl;
    INT4                i4ReturnedVal;

    u2Index = (UINT2) pTimer->Timer_node.u4Data;

    if (u2Index >= IPIF_MAX_LOGICAL_IFACES)
    {
        return;
    }

    switch (pTimer->u1Id)
    {

        case IRDP_ADVERT_TIMER_ID:

            if (gaIrdpCntlRec[u2Index].bSendAdvert == TRUE)
            {
                i4ReturnedVal = IrdpGenerateAdvertisement (0, u2Index);

                if (i4ReturnedVal == SUCCESS)
                {
                    IP_TRC_ARG1 (ICMP_MOD_TRC, DATA_PATH_TRC, ICMP_NAME,
                                 "IRDP sent advertisement on interface %d \n",
                                 u2Index);

                    gu4IrdpOutAdvertCount++;
                    gaIrdpCntlRec[u2Index].u1InitAdvertCount++;
                }

                u4TransIntvl = IrdpGetRandom (u2Index,
                                              gaIrdpIfaceRec
                                              [u2Index].u2MinAdvertInterval,
                                              gaIrdpIfaceRec
                                              [u2Index].u2MaxAdvertInterval);

                /* 
                 * Check if the Server is in Initial Mode. If so the 
                 * transmission interval should not be more than 16 seconds. 
                 * If it is greater than 16 secs, set it to 16 secs.
                 */

                if (gaIrdpCntlRec[u2Index].u1InitAdvertCount <
                    IRDP_MAX_INITIAL_ADVERTISEMENTS)
                {
                    if (u4TransIntvl > IRDP_MAX_INITIAL_ADVERT_INTERVAL)
                        u4TransIntvl = IRDP_MAX_INITIAL_ADVERT_INTERVAL;
                }

                /* Set the timer identity */

                gaIrdpIfaceRec[u2Index].Timer.u1Id = IRDP_ADVERT_TIMER_ID;
                gaIrdpIfaceRec[u2Index].Timer.Timer_node.u4Data = u2Index;

                IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                 &gaIrdpIfaceRec[u2Index].Timer.Timer_node,
                                 u4TransIntvl);
            }
            break;

        case IRDP_SOLICIT_TIMER_ID:

            if ((gaIrdpCntlRec[u2Index].bSendSolicit == TRUE) &&
                (gaIrdpCntlRec[u2Index].u1OutSolicitCount <
                 IRDP_MAX_SOLICITATIONS))
            {
                i4ReturnedVal =
                    IrdpGenerateSolicitation (gaIrdpIfaceRec[u2Index].u2Iface);

                if (i4ReturnedVal == SUCCESS)
                {
                    IP_TRC_ARG1 (ICMP_MOD_TRC, DATA_PATH_TRC, ICMP_NAME,
                                 "IRDP sending solicitation on interface %d \n",
                                 u2Index);

                    gaIrdpCntlRec[u2Index].u1OutSolicitCount++;
                    gu4IrdpOutSolicitCount++;
                }

                /* Set the timer identity */

                gaIrdpIfaceRec[u2Index].Timer.u1Id = IRDP_SOLICIT_TIMER_ID;

                gaIrdpIfaceRec[u2Index].Timer.Timer_node.u4Data = u2Index;

                IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                 &gaIrdpIfaceRec[u2Index].Timer.Timer_node,
                                 IRDP_SOLICITATION_INTERVAL);
            }                    /* ^ 3 secs. */

            break;

        case IRDP_ROUTER_LIST_TIMER_ID:

            if (u2Index < IRDP_MAX_DEFAULT_ROUTERS)
            {
                IrdpDeleteEntryFromRtrList (u2Index);
            }
            else
            {
                return;
            }
            break;

        default:

            break;

    }                            /* End for switch statement */
}

/*-------------------------------------------------------------------+
 * Function           : IrdpInitialize 
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *   + Initializes all the data structures related to IRDP.  
 * 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpInitialize (VOID)
#else
EXPORT VOID
IrdpInitialize ()
#endif
{
    UINT2               u2Index;

    /* Initialize the statistics variables */
    gu4IrdpInAdvertCount = 0;
    gu4IrdpOutAdvertCount = 0;
    gu4IrdpInSolicitCount = 0;
    gu4IrdpOutSolicitCount = 0;

    /* Initialize the Interface Record */
    for (u2Index = 0; u2Index < IPIF_MAX_LOGICAL_IFACES; u2Index++)
    {
        gaIrdpIfaceRec[u2Index].u2Iface = IRDP_INVALID_IFACE;

        gaIrdpIfaceRec[u2Index].u2Iface = u2Index;
        gaIrdpIfaceRec[u2Index].u2MaxAdvertInterval = IRDP_DEF_MAX_INTERVAL;
        gaIrdpIfaceRec[u2Index].u2MinAdvertInterval = IRDP_DEF_MIN_INTERVAL;
        gaIrdpIfaceRec[u2Index].u2AdvertLifeTime = IRDP_DEF_ADVERT_LTIME;
        IRDP_PERFORM_ROUTER_DISCOVERY (u2Index) = IRDP_DISCOVER;
        gaIrdpIfaceRec[u2Index].u4AdvertAddr = IRDP_DEF_ADVERT_ADDR;
        gaIrdpIfaceRec[u2Index].u4SolicitAddr = IRDP_DEF_SOLICIT_ADDR;
    }
    /* Initialize the Router List */
    IrdpInitRouterList ();

    IrdpInitQueue ();

}

/*-------------------------------------------------------------------+
 * Function           : IrdpAddInterface
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + Initializes the interface specific variables
 *  + Starts the IRDP Server or IRDP Client depending on the
 *    value  of ipForwarding.
+-------------------------------------------------------------------*/
EXPORT VOID
IrdpAddInterface (UINT2 u2Iface)
{
    UINT1               u1OperStatus;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4RetVal = 0;
    tIpConfigInfo       IpInfo;
    tIpCxt             *pIpCxt = gIpGlobalInfo.apIpCxt[IP_DEFAULT_CONTEXT];

    if (NULL == pIpCxt)
    {
        return;
    }

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    u1OperStatus = gIpGlobalInfo.Ipif_config[u2Iface].u1Oper;
    u4CfaIfIndex = gIpGlobalInfo.Ipif_config[u2Iface].InterfaceId.u4IfIndex;
    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    /* Get the Primary ip address from the IP interface table */
    MEMSET (&IpInfo, 0, sizeof (tIpConfigInfo));
    i4RetVal = CfaIpIfGetIfInfo (u4CfaIfIndex, &IpInfo);

    gaIrdpIfaceRec[u2Iface].u2Iface = u2Iface;
    gaIrdpCntlRec[u2Iface].u1InitAdvertCount = 0;
    gaIrdpCntlRec[u2Iface].u1OutSolicitCount = 0;
    gaIrdpCntlRec[u2Iface].bBcastFlag = FALSE;

    /*
     * Initialize all the interface specific configurables to their default
     * values.
     */

    gaIrdpIfaceRec[u2Iface].u2MaxAdvertInterval = IRDP_DEF_MAX_INTERVAL;
    gaIrdpIfaceRec[u2Iface].u2MinAdvertInterval = IRDP_DEF_MIN_INTERVAL;
    gaIrdpIfaceRec[u2Iface].u2AdvertLifeTime = IRDP_DEF_ADVERT_LTIME;
    gaIrdpIfaceRec[u2Iface].u4AdvertAddr = IRDP_DEF_ADVERT_ADDR;
    IRDP_PERFORM_ROUTER_DISCOVERY (u2Iface) = IRDP_DISCOVER;
    gaIrdpIfaceRec[u2Iface].u4SolicitAddr = IRDP_DEF_SOLICIT_ADDR;

    /* 
     * Initialize the IP Address Table which has the IRDP specific
     * configurables.
     */

    IrdpAddrTableInit (u2Iface);

    if (u1OperStatus != IPIF_OPER_ENABLE)
    {
        gaIrdpCntlRec[u2Iface].bSendAdvert = FALSE;
        return;
    }

    if (IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_ENABLE)
    {
        gaIrdpCntlRec[u2Iface].bSendAdvert = TRUE;
        IrdpServerInit (u2Iface);
    }
    else
    {
        /* If IP Address is not known this will remain FALSE */
        gaIrdpCntlRec[u2Iface].bProcessAdvert = FALSE;
        gaIrdpCntlRec[u2Iface].bSendSolicit = TRUE;

        if (IpInfo.u4Addr != IP_ANY_ADDR)
        {                        /* ^ 0.0.0.0 */
            gaIrdpCntlRec[u2Iface].bProcessAdvert = TRUE;
        }
        IrdpClientInit (u2Iface);
    }
    UNUSED_PARAM (i4RetVal);
}

/*-------------------------------------------------------------------+
 * Function           : IrdpDeleteInterface
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *
 * + Destroys all the IRDP specific resources on this interface.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpDeleteInterface (UINT2 u2Iface)
#else
EXPORT VOID
IrdpDeleteInterface (u2Iface)
     UINT2               u2Iface;
#endif
{
    UINT2               u2Index;

    IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                       &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);
    gaIrdpIfaceRec[u2Iface].u2Iface = IRDP_INVALID_IFACE;

    /* Delete u2Iface specific information in router list */
    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (gaIrdpRtrList[u2Index].u2Iface == u2Iface)
        {
            IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                               &gaIrdpRtrList[u2Index].Timer.Timer_node);
            IrdpDeleteEntryFromRtrList (u2Index);
        }
    }

    /* Delete u2Iface specific information in Queue */
    for (u2Index = 0; u2Index < IRDP_MAX_Q_SIZE; u2Index++)
    {
        if ((gaIrdpQueue[u2Index].pBuf != NULL) &&
            (gaIrdpQueue[u2Index].u2Iface == u2Iface))
        {
            IrdpDeleteEntryFromQueue (u2Index);
        }
    }
}

/*-------------------------------------------------------------------+
 * Function           : IrdpDisableInterface
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Does the following :
 *  
 *  + Disables the IRDP activity on the specified interface.
 *  + Called by IPIF module whenever an interface was administratively
 *    Disabled, when it was previously administratively Enabled.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpDisableInterface (UINT2 u2Iface)
#else
EXPORT VOID
IrdpDisableInterface (u2Iface)
     UINT2               u2Iface;
#endif
{
    gaIrdpCntlRec[u2Iface].bSendSolicit = FALSE;
    gaIrdpCntlRec[u2Iface].bSendAdvert = FALSE;

    IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                       &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);
}

/*-------------------------------------------------------------------+
 * Function           : IrdpEnableInterface
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + When the interface is enabled administratively starts up the IRDP
 *    Server or IRDP Client on the interface depending on the value of 
 *    ipForwarding mib object.
 *
 *  + Called by IPIF module whenever an interface was administratively 
 *    enabled, when it was previously administratively disabled.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpEnableInterface (UINT2 u2Iface)
#else
EXPORT VOID
IrdpEnableInterface (u2Iface)
     UINT2               u2Iface;
#endif
{
    UINT2               u2Index;
    tIpCxt             *pIpCxt = gIpGlobalInfo.apIpCxt[IP_DEFAULT_CONTEXT];

    if (NULL == pIpCxt)
    {
        return;
    }

    if (IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_ENABLE)
    {
        for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
        {
            if (IRDP_GET_ADVERTFLAG (u2Iface, u2Index) == IRDP_ADVERTISE)
            {
                gaIrdpCntlRec[u2Iface].bSendAdvert = TRUE;
                gaIrdpCntlRec[u2Iface].u1InitAdvertCount = 0;
                IrdpServerInit (u2Iface);
                break;
            }
        }
    }
    else if (IRDP_PERFORM_ROUTER_DISCOVERY (u2Iface) == IRDP_DISCOVER)
    {
        gaIrdpCntlRec[u2Iface].bSendSolicit = TRUE;
        IrdpClientInit (u2Iface);
    }
}

/*-------------------------------------------------------------------+
 * Function           : IrdpSwitchToServer
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 * 
 * + Stops the IRDP Client on all the interfaces.
 * + Starts the IRDP Server on all the interfaces.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpSwitchToServer (VOID)
#else
EXPORT VOID
IrdpSwitchToServer ()
#endif
{
    UINT2               u2Iface;
    UINT2               u2Index;

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2Iface)
    {
        if (IPIF_GLBTAB_IFCONFIG_OF (u2Iface) == NULL)
        {
            /* Unlock it */
            IPFWD_DS_UNLOCK ();
            return;
        }
        gaIrdpCntlRec[u2Iface].bSendSolicit = FALSE;

        IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                           &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);
        if ((gIpGlobalInfo.Ipif_config[u2Iface].u1Admin == IPIF_ADMIN_ENABLE) &&
            (gIpGlobalInfo.Ipif_config[u2Iface].u1Oper == IPIF_OPER_ENABLE))
        {
            for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
            {
                if (IRDP_GET_ADVERTFLAG (u2Iface, u2Index) == IRDP_ADVERTISE)
                {
                    gaIrdpCntlRec[u2Iface].bSendAdvert = TRUE;
                    gaIrdpCntlRec[u2Iface].u1InitAdvertCount = 0;
                    IrdpServerInit (u2Iface);
                    break;
                }
            }
        }                        /* End of AdminStatus & OperStatus Enabled Check */
    }                            /* End of Logical Ifaces Scan */
    /* Unlock it */
    IPFWD_DS_UNLOCK ();

}

/*-------------------------------------------------------------------+
 * Function           : IrdpSwitchToClient
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 * + Stops the IRDP Client on all the interfaces.
 * + Starts the IRDP Server on all the interfaces.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpSwitchToClient (VOID)
#else
EXPORT VOID
IrdpSwitchToClient ()
#endif
{
    UINT2               u2Iface;

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2Iface)
    {
        if (IPIF_GLBTAB_IFCONFIG_OF (u2Iface) == NULL)
        {
            /* Unlock it */
            IPFWD_DS_UNLOCK ();
            return;
        }
        gaIrdpCntlRec[u2Iface].bSendAdvert = FALSE;

        IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                           &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);

        if ((gIpGlobalInfo.Ipif_config[u2Iface].u1Admin == IPIF_ADMIN_ENABLE) &&
            (gIpGlobalInfo.Ipif_config[u2Iface].u1Oper == IPIF_OPER_ENABLE) &&
            (IRDP_PERFORM_ROUTER_DISCOVERY (u2Iface) == IRDP_DISCOVER))
        {
            gaIrdpCntlRec[u2Iface].bSendSolicit = TRUE;
            gaIrdpCntlRec[u2Iface].bProcessAdvert = TRUE;
            IrdpClientInit (u2Iface);
        }
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();
}

/*-------------------------------------------------------------------+
 * Function           : IrdpServerInit
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + Starts the IRDP Server in the INIT MODE.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpServerInit (UINT2 u2Iface)
#else
EXPORT VOID
IrdpServerInit (u2Iface)
     UINT2               u2Iface;
#endif
{
    UINT4               u4TransIntvl;

    if (u2Iface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return;
    }

    if (gaIrdpCntlRec[u2Iface].bSendAdvert == TRUE)
    {
        gaIrdpCntlRec[u2Iface].u1InitAdvertCount = 0;

        u4TransIntvl = IrdpGetRandom (u2Iface,
                                      gaIrdpIfaceRec
                                      [u2Iface].u2MinAdvertInterval,
                                      gaIrdpIfaceRec
                                      [u2Iface].u2MaxAdvertInterval);

        if (u4TransIntvl > IRDP_MAX_INITIAL_ADVERT_INTERVAL)
        {                        /* ^ 16 secs. */
            u4TransIntvl = IRDP_MAX_INITIAL_ADVERT_INTERVAL;
        }

        gaIrdpIfaceRec[u2Iface].Timer.u1Id = IRDP_ADVERT_TIMER_ID;
        gaIrdpIfaceRec[u2Iface].Timer.Timer_node.u4Data = u2Iface;

        IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                         &gaIrdpIfaceRec[u2Iface].Timer.Timer_node,
                         u4TransIntvl);
    }
}

/*-------------------------------------------------------------------+
 * Function           : IrdpClientInit
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + Starts the IRDP Client in the INIT MODE.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpClientInit (UINT2 u2Iface)
#else
EXPORT VOID
IrdpClientInit (u2Iface)
     UINT2               u2Iface;
#endif
{
    UINT4               u4TransIntvl;

    if (u2Iface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return;
    }

    if ((gaIrdpCntlRec[u2Iface].bSendSolicit == TRUE) &&
        (gaIrdpIfaceRec[u2Iface].u2Iface != IRDP_INVALID_IFACE))
    {
        gaIrdpCntlRec[u2Iface].u1OutSolicitCount = 0;

        u4TransIntvl = IrdpGetRandom (u2Iface, 1, IRDP_MAX_SOLICITATION_DELAY);
        /* ^ using btwn 1 and 2 since 0, 2 sometimes gives 0 */

        gaIrdpIfaceRec[u2Iface].Timer.u1Id = IRDP_SOLICIT_TIMER_ID;
        gaIrdpIfaceRec[u2Iface].Timer.Timer_node.u4Data = u2Iface;

        IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                         &gaIrdpIfaceRec[u2Iface].Timer.Timer_node,
                         u4TransIntvl);
    }
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGenerateSolicitation
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 * + Generates a ICMP Router Solicitaion message and sends it out 
 *   by calling icmp_output.
+-------------------------------------------------------------------*/
#ifdef __STDC__
PRIVATE INT4
IrdpGenerateSolicitation (UINT2 u2Iface)
#else
PRIVATE INT4
IrdpGenerateSolicitation (u2Iface)
     UINT2               u2Iface;
#endif
{
    t_ICMP              Icmp;
    tIP_BUF_CHAIN_HEADER *pBuf;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4IfaceIndex = 0;

    pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);
    if (NULL == pIpCxt)
    {
        return FAILURE;
    }

    if (u2Iface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return FAILURE;
    }
    IPIF_CONFIG_GET_IF_INDEX (u2Iface, u4IfaceIndex);

    pBuf = IRDP_ALLOC_BUFFER (IP_MAC_HDR_LEN + ICMP_HDR_LEN + IRDP_PADDING_WORD,
                              IP_MAC_HDR_LEN + ICMP_HDR_LEN);

    if (pBuf == NULL)
    {
        return SLI_EMEMFAIL;
    }
    /*
     * Just fill the padding bytes to zero. If u dont do this these padding
     * bytes are not sent out. 
     */
    IRDP_PUT_4_BYTES (pBuf, u4Offset, 0);
    IRDP_PUT_4_BYTES (pBuf, u4Offset, 0);

    IRDP_MOVE_BACK_TO_ICMP_HDR (pBuf);

    Icmp.i1Type = ICMP_ROUTER_SOLCT;
    Icmp.i1Code = 0;

    Icmp.args.u4Unused = 0;

    IP_TRC_ARG1 (ICMP_MOD_TRC, DATA_PATH_TRC, "IRDP",
                 "sending solicitation on interface %x.\n", u4IfaceIndex);

    return icmp_output_InCxt (pIpCxt, gaIrdpIfaceRec[u2Iface].u4SolicitAddr,
                              0, pBuf, IRDP_PADDING_WORD, &Icmp, 0, u2Iface);
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGenerateAdvertisement
 *
 * Input(s)           : u4DestAddr, u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + Collects all the IP addresses on the interface that are advertisable
 *    and sends out the advertisement by calling icmp_output.
+-------------------------------------------------------------------*/
#ifdef __STDC__
PRIVATE INT4
IrdpGenerateAdvertisement (UINT4 u4DestAddr, UINT2 u2Iface)
#else
PRIVATE INT4
IrdpGenerateAdvertisement (u4DestAddr, u2Iface)
     UINT4               u4DestAddr;
     UINT2               u2Iface;
#endif
{
    t_ICMP              Icmp;
    tIP_BUF_CHAIN_HEADER *pBuf;
    tIpCxt             *pIpCxt = NULL;
    UINT2               u2DataLen;
    UINT2               u2Index;
    UINT4               u4Offset = 0;
    UINT4               u4IfaceIndex = 0;

    pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);
    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }

    if (u2Iface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return IP_FAILURE;
    }
    IPIF_CONFIG_GET_IF_INDEX (u2Iface, u4IfaceIndex);

    Icmp.args.Irdp.u1NumAddr = 0;

    /* Find out how much memory has to be allocated for the buffer */
    for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
    {
        if ((IRDP_GET_ADDRESS (u2Iface, u2Index) != IP_ANY_ADDR) &&
            (IRDP_GET_ADVERTFLAG (u2Iface, u2Index) != IRDP_DONT_ADVERTISE))
        {
            Icmp.args.Irdp.u1NumAddr++;
        }
    }

    u2DataLen = (UINT2) (Icmp.args.Irdp.u1NumAddr *
                         IRDP_ADDR_ENTRY_SIZE * IRDP_BYTES_PER_ENTRY);
    /* ^ 2 *//* ^ 4 */

    pBuf =
        IRDP_ALLOC_BUFFER ((UINT4) (IP_MAC_HDR_LEN + ICMP_HDR_LEN + u2DataLen),
                           (UINT4) (IP_MAC_HDR_LEN + ICMP_HDR_LEN));
    if (pBuf == NULL)
    {
        return SLI_EMEMFAIL;
    }

    /* Fill the Tuples */
    for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
    {
        if ((IRDP_GET_ADDRESS (u2Iface, u2Index) != IP_ANY_ADDR) &&
            (IRDP_GET_ADVERTFLAG (u2Iface, u2Index) != IRDP_DONT_ADVERTISE))
        {
            IRDP_PUT_4_BYTES (pBuf, u4Offset,
                              IRDP_GET_ADDRESS (u2Iface, u2Index));
            IRDP_PUT_4_BYTES (pBuf, u4Offset,
                              IRDP_GET_PREFLEVEL (u2Iface, u2Index));
        }
    }

    IRDP_MOVE_BACK_TO_ICMP_HDR (pBuf);

    Icmp.i1Type = ICMP_ROUTER_ADVT;
    Icmp.i1Code = 0;

    Icmp.args.Irdp.u1AddrEntrySize = IRDP_ADDR_ENTRY_SIZE;
    Icmp.args.Irdp.u2LifeTime = gaIrdpIfaceRec[u2Iface].u2AdvertLifeTime;

    if (u4DestAddr == IP_ANY_ADDR)
    {
        if (gaIrdpCntlRec[u2Iface].bBcastFlag == TRUE)
        {
            /* Send a broadcast reply to a request that was broadcasted */
            gaIrdpCntlRec[u2Iface].bBcastFlag = FALSE;
            u4DestAddr = IP_GEN_BCAST_ADDR;
        }
        else
        {
            /* else use the configured address */
            u4DestAddr = gaIrdpIfaceRec[u2Iface].u4AdvertAddr;
        }
    }

    IP_TRC_ARG3 (ICMP_MOD_TRC, DATA_PATH_TRC, "IRDP",
                 "sending advertisement on interface %d, numaddr=%d, "
                 "lifetime=%d.\n", u4IfaceIndex, Icmp.args.Irdp.u1NumAddr,
                 Icmp.args.Irdp.u2LifeTime);

    return icmp_output_InCxt (pIpCxt, u4DestAddr, 0, pBuf, u2DataLen, &Icmp,
                              0, u2Iface);
}

/*-------------------------------------------------------------------+
 * Function           : IrdpAddrTableInit
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + Initialize the IP address table which has the IRDP specific 
 *    configurations like Advertise Flag.
+-------------------------------------------------------------------*/
#ifdef __STDC__
PRIVATE VOID
IrdpAddrTableInit (UINT2 u2Iface)
#else
PRIVATE VOID
IrdpAddrTableInit (u2Iface)
     UINT2               u2Iface;
#endif
{
    UINT2               u2Index;

    if (u2Iface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return;
    }
    /* Calling the IPIF module to get all the addresses on the interface */
    IpifGetAllAddr (u2Iface,
                    (tIrdpAddrRec *) & gaIrdpIfaceRec[u2Iface].aAddrTable);

    for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
    {
        IRDP_SET_ADVERTFLAG (u2Iface, u2Index, IRDP_DONT_ADVERTISE);
        IRDP_SET_PREFLEVEL (u2Iface, u2Index, 0);
    }
}

/*-------------------------------------------------------------------+
 * Function           : IrdpSendNotAvailableNotification
 *
 * Input(s)           : u2Iface, u2Index
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + Sends and ICMP Router advertisement containing only the specified
 *    IP address and with life time field of the ICMP header set to 0.
 *  + Called from the Low Level routine for SNMP interface, whenever the 
 *    Advertise flag of an IP address is set to FALSE.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpSendNotAvailableNotification (UINT2 u2Iface, UINT2 u2Index)
#else
EXPORT VOID
IrdpSendNotAvailableNotification (u2Iface, u2Index)
     UINT2               u2Iface;
     UINT2               u2Index;
#endif
{
    t_ICMP              Icmp;
    tIP_BUF_CHAIN_HEADER *pBuf;
    tIpCxt             *pIpCxt = NULL;
    UINT2               u2DataLen;
    INT4                i4ReturnedVal;
    UINT4               u4Offset = 0;

    pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);
    if (pIpCxt == NULL)
    {
        return;
    }
    Icmp.i1Type = ICMP_ROUTER_ADVT;
    Icmp.i1Code = 0;
    Icmp.args.Irdp.u1NumAddr = 1;
    Icmp.args.Irdp.u1AddrEntrySize = IRDP_ADDR_ENTRY_SIZE;
    Icmp.args.Irdp.u2LifeTime = 0;

    u2DataLen = (UINT2)
        (Icmp.args.Irdp.u1NumAddr * IRDP_ADDR_ENTRY_SIZE *
         IRDP_BYTES_PER_ENTRY);
    /* ^ 2 *//* ^ 4 */
    pBuf =
        IRDP_ALLOC_BUFFER ((UINT4) (IP_MAC_HDR_LEN + ICMP_HDR_LEN + u2DataLen),
                           (UINT4) (IP_MAC_HDR_LEN + ICMP_HDR_LEN));
    if (pBuf != NULL)
    {
        IRDP_PUT_4_BYTES (pBuf, u4Offset, IRDP_GET_ADDRESS (u2Iface, u2Index));
        IRDP_PUT_4_BYTES (pBuf, u4Offset,
                          IRDP_GET_PREFLEVEL (u2Iface, u2Index));

        IRDP_MOVE_BACK_TO_ICMP_HDR (pBuf);
        i4ReturnedVal = icmp_output_InCxt (pIpCxt,
                                           gaIrdpIfaceRec[u2Iface].u4AdvertAddr,
                                           0, pBuf, u2DataLen, &Icmp, 0,
                                           u2Iface);
        if (i4ReturnedVal == SUCCESS)
        {
            gu4IrdpOutAdvertCount++;
        }
    }
}

/*-------------------------------------------------------------------+
 * Function           : IrdpProcessQueue
 *
 * Input(s)           : u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 * + Checks if there are any Advertisements arrived on the specified
 *   interface is waiting in the Queue to get processed.
 * + If one found, Calls the IrdpProcessAdvertisement.
 * + Should be Called by a module which is going to get the IP  address on a 
 *   interface if one not known by default.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpProcessQueue (UINT2 u2Iface)
#else
EXPORT VOID
IrdpProcessQueue (u2Iface)
     UINT2               u2Iface;
#endif
{
    tIP_BUF_CHAIN_HEADER *pBuf;
    t_ICMP             *pIcmp;
    UINT2               u2Index;

    /* 
     * Validate the interface and check if anything is there in the 
     * Queue. If Queue is empty then no point in searching for a entry. 
     */
    if ((u2Iface > 0 && u2Iface < IPIF_MAX_LOGICAL_IFACES) &&
        (IrdpGetFreeSpaceInQueue () != IRDP_MAX_Q_SIZE))
    {
        for (u2Index = 0; u2Index < IRDP_MAX_Q_SIZE; u2Index++)
        {
            if ((gaIrdpQueue[u2Index].pBuf != NULL) &&
                (gaIrdpQueue[u2Index].u2Iface == u2Iface))
            {
                pBuf = gaIrdpQueue[u2Index].pBuf;
                pIcmp = (t_ICMP *) & gaIrdpQueue[u2Index].IcmpHdr;

                IrdpProcessAdvertisement (pBuf, pIcmp, u2Iface);

                IP_RELEASE_BUF (pBuf, FALSE);

                IrdpDeleteEntryFromQueue (u2Index);
            }
        }
    }
}
