/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdpsnif.c,v 1.13 2014/06/30 11:02:23 siva Exp $
 *
 * Description: Contains Low level routines for IRDP. 
 *
 *******************************************************************/
#include "irdpinc.h"
#include "fsiplow.h"
#include "fsmpipcli.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
static BOOLEAN      gbIrdpSendAdvertEnable = IRDP_ADVERTISE;
VOID               *gIrdpFilePtr = NULL;

/**************************************************************************/

/* LOW LEVEL Routines for Table : IpAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpAddressTable
 Input       :  The Indices
                IpAddrTabAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpAddressTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIpAddressTable (UINT4 u4IpAddrTabAddress)
#else
INT1
nmhValidateIndexInstanceFsIpAddressTable (u4IpAddrTabAddress)
     UINT4               u4IpAddrTabAddress;
#endif
{
    UINT2               u2Index;
    UINT2               u2Iface;

    IPIF_LOGICAL_IFACES_SCAN (u2Iface)
    {
        for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
        {
            if (u4IpAddrTabAddress == IRDP_GET_ADDRESS (u2Iface, u2Index))
            {
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpAddressTable
 Input       :  The Indices
                IpAddrTabAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpAddressTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIpAddressTable (UINT4 *pu4IpAddrTabAddress)
#else
INT1
nmhGetFirstIndexFsIpAddressTable (pu4IpAddrTabAddress)
     UINT4              *pu4IpAddrTabAddress;
#endif
{
    if (u2Start_index != IPIF_MAX_LOGICAL_IFACES)
    {
        *pu4IpAddrTabAddress = IRDP_GET_ADDRESS (u2Start_index, 0);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpAddressTable
 Input       :  The Indices
                IpAddrTabAddress
                nextIpAddrTabAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpAddressTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIpAddressTable (UINT4 u4IpAddrTabAddress,
                                 UINT4 *pu4NextIpAddrTabAddress)
#else
INT1
nmhGetNextIndexFsIpAddressTable (u4IpAddrTabAddress, pu4NextIpAddrTabAddress)
     UINT4               u4IpAddrTabAddress;
     UINT4              *pu4NextIpAddrTabAddress;
#endif
{
    UINT2               u2Iface;
    UINT2               u2Index;
    UINT2               u2NextIface = 0;
    INT4                i4RetVal;
    i4RetVal = IrdpMapIndices (u4IpAddrTabAddress, &u2Iface, &u2Index);

    if (i4RetVal == FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (u2Index + 1 != IPIF_MAX_ADDR_PER_IFACE)
    {
        *pu4NextIpAddrTabAddress = IRDP_GET_ADDRESS (u2Iface, u2Index + 1);

        return SNMP_SUCCESS;
    }

    IPIF_CONFIG_GET_NEXT (u2Index, u2NextIface);
    if (u2NextIface != IPIF_MAX_LOGICAL_IFACES)
    {
        u2Iface = u2NextIface;
        *pu4NextIpAddrTabAddress = IRDP_GET_ADDRESS (u2Iface, 0);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpAddrTabIfaceId
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                retValIpAddrTabIfaceId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAddrTabIfaceId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpAddrTabIfaceId (UINT4 u4IpAddrTabAddress,
                          INT4 *pi4RetValIpAddrTabIfaceId)
#else
INT1
nmhGetIpAddrTabIfaceId (u4IpAddrTabAddress, pi4RetValIpAddrTabIfaceId)
     UINT4               u4IpAddrTabAddress;
     INT4               *pi4RetValIpAddrTabIfaceId;
#endif
{
    UINT2               u2Iface;
    UINT2               u2Index;
    INT4                i4RetVal;

    i4RetVal = IrdpMapIndices (u4IpAddrTabAddress, &u2Iface, &u2Index);

    if (i4RetVal == SUCCESS)
    {
        IPIF_CONFIG_GET_IF_INDEX (u2Iface,
                                  *(UINT4 *) pi4RetValIpAddrTabIfaceId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpAddrTabAdvertise
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                retValIpAddrTabAdvertise
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAddrTabAdvertise ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpAddrTabAdvertise (UINT4 u4IpAddrTabAddress,
                            INT4 *pi4RetValIpAddrTabAdvertise)
#else
INT1
nmhGetFsIpAddrTabAdvertise (u4IpAddrTabAddress, pi4RetValIpAddrTabAdvertise)
     UINT4               u4IpAddrTabAddress;
     INT4               *pi4RetValIpAddrTabAdvertise;
#endif
{
    UINT2               u2Iface;
    UINT2               u2Index;
    INT4                i4RetVal;

    i4RetVal = IrdpMapIndices (u4IpAddrTabAddress, &u2Iface, &u2Index);

    if (i4RetVal == SUCCESS)
    {
        *pi4RetValIpAddrTabAdvertise = IRDP_GET_ADVERTFLAG (u2Iface, u2Index);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpAddrTabPreflevel
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                retValIpAddrTabPreflevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAddrTabPreflevel ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpAddrTabPreflevel (UINT4 u4IpAddrTabAddress,
                            INT4 *pi4RetValIpAddrTabPreflevel)
#else
INT1
nmhGetFsIpAddrTabPreflevel (u4IpAddrTabAddress, pi4RetValIpAddrTabPreflevel)
     UINT4               u4IpAddrTabAddress;
     INT4               *pi4RetValIpAddrTabPreflevel;
#endif
{
    UINT2               u2Iface;
    UINT2               u2Index;
    INT4                i4RetVal;

    i4RetVal = IrdpMapIndices (u4IpAddrTabAddress, &u2Iface, &u2Index);

    if (i4RetVal == SUCCESS)
    {
        *pi4RetValIpAddrTabPreflevel = IRDP_GET_PREFLEVEL (u2Iface, u2Index);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpAddrTabStatus
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                retValIpAddrTabStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpAddrTabStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpAddrTabStatus (UINT4 u4IpAddrTabAddress,
                         INT4 *pi4RetValIpAddrTabStatus)
#else
INT1
nmhGetFsIpAddrTabStatus (u4IpAddrTabAddress, pi4RetValIpAddrTabStatus)
     UINT4               u4IpAddrTabAddress;
     INT4               *pi4RetValIpAddrTabStatus;
#endif
{
    UNUSED_PARAM (u4IpAddrTabAddress);

    *pi4RetValIpAddrTabStatus = IRDP_ACTIVE;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpAddrTabAdvertise
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                setValIpAddrTabAdvertise
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpAddrTabAdvertise ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpAddrTabAdvertise (UINT4 u4IpAddrTabAddress,
                            INT4 i4SetValIpAddrTabAdvertise)
#else
INT1
nmhSetFsIpAddrTabAdvertise (u4IpAddrTabAddress, i4SetValIpAddrTabAdvertise)
     UINT4               u4IpAddrTabAddress;
     INT4                i4SetValIpAddrTabAdvertise;

#endif
{
    tIpCxt             *pIpCxt = gIpGlobalInfo.apIpCxt[IP_DEFAULT_CONTEXT];
    UINT2               u2Iface = 0;
    UINT2               u2Index = 0;
    UINT2               u2MappedIndex = 0;
    INT4                i4RetVal;
    UINT1               u1AdminStatus = IPIF_ADMIN_INVALID;
    UINT1               u1OperStatus = 0;

    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    i4RetVal = IrdpMapIndices (u4IpAddrTabAddress, &u2Iface, &u2MappedIndex);

    if (u2Iface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return SNMP_FAILURE;
    }

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    u1AdminStatus = gIpGlobalInfo.Ipif_config[u2Iface].u1Admin;
    u1OperStatus = gIpGlobalInfo.Ipif_config[u2Iface].u1Oper;

    /* Unlock it */
    IPFWD_DS_UNLOCK ();
    if (i4RetVal == SUCCESS)
    {
        if ((i4SetValIpAddrTabAdvertise == IRDP_ADVERTISE) &&
            (IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_ENABLE) &&
            (gaIrdpCntlRec[u2Iface].bSendAdvert == FALSE) &&
            (u1AdminStatus == IPIF_ADMIN_ENABLE) &&
            (u1OperStatus == IPIF_OPER_ENABLE) &&
            (gbIrdpSendAdvertEnable == IRDP_ADVERTISE))
        {
            for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
            {
                if (IRDP_GET_ADVERTFLAG (u2Iface, u2Index) == IRDP_ADVERTISE)
                    break;
            }
            if (u2Index == IPIF_MAX_ADDR_PER_IFACE)
            {
                IRDP_SET_ADVERTFLAG (u2Iface, u2MappedIndex,
                                     (BOOLEAN) i4SetValIpAddrTabAdvertise);
                gaIrdpCntlRec[u2Iface].bSendAdvert = TRUE;
                gaIrdpCntlRec[u2Iface].u1InitAdvertCount = 0;
                IrdpServerInit (u2Iface);
            }
        }
        else if ((i4SetValIpAddrTabAdvertise == IRDP_DONT_ADVERTISE) &&
                 (gaIrdpCntlRec[u2Iface].bSendAdvert == TRUE))
        {
            IRDP_SET_ADVERTFLAG (u2Iface, u2MappedIndex,
                                 (BOOLEAN) i4SetValIpAddrTabAdvertise);
            IrdpSendNotAvailableNotification (u2Iface, u2MappedIndex);
            for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
            {
                if (IRDP_GET_ADVERTFLAG (u2Iface, u2Index) == IRDP_ADVERTISE)
                    break;
            }
            if (u2Index == IPIF_MAX_ADDR_PER_IFACE)
            {
                gaIrdpCntlRec[u2Iface].bSendAdvert = FALSE;
                IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                   &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);
            }

        }

        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Advertise flag for address %x is set to %d \n",
                     u4IpAddrTabAddress, i4SetValIpAddrTabAdvertise);

        IncMsrForFsIpv4AddressTable (u4IpAddrTabAddress,
                                     i4SetValIpAddrTabAdvertise,
                                     FsMIFsIpAddrTabAdvertise,
                                     (sizeof (FsMIFsIpAddrTabAdvertise) /
                                      sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpAddrTabPreflevel
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                setValIpAddrTabPreflevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpAddrTabPreflevel ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpAddrTabPreflevel (UINT4 u4IpAddrTabAddress,
                            INT4 i4SetValIpAddrTabPreflevel)
#else
INT1
nmhSetFsIpAddrTabPreflevel (u4IpAddrTabAddress, i4SetValIpAddrTabPreflevel)
     UINT4               u4IpAddrTabAddress;
     INT4                i4SetValIpAddrTabPreflevel;

#endif
{
    UINT2               u2Iface;
    UINT2               u2Index;
    INT4                i4RetVal;

    i4RetVal = IrdpMapIndices (u4IpAddrTabAddress, &u2Iface, &u2Index);

    if (i4RetVal == SUCCESS)
    {
        IRDP_SET_PREFLEVEL (u2Iface, u2Index, i4SetValIpAddrTabPreflevel);

        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Preference level for address %x is set to %d \n",
                     u4IpAddrTabAddress, i4SetValIpAddrTabPreflevel);

        IncMsrForFsIpv4AddressTable (u4IpAddrTabAddress,
                                     i4SetValIpAddrTabPreflevel,
                                     FsMIFsIpAddrTabPreflevel,
                                     (sizeof (FsMIFsIpAddrTabPreflevel) /
                                      sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpAddrTabAdvertise
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                testValIpAddrTabAdvertise
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpAddrTabAdvertise ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpAddrTabAdvertise (UINT4 *pu4ErrorCode, UINT4 u4IpAddrTabAddress,
                               INT4 i4TestValIpAddrTabAdvertise)
#else
INT1
nmhTestv2FsIpAddrTabAdvertise (pu4ErrorCode, u4IpAddrTabAddress,
                               i4TestValIpAddrTabAdvertise)
     UINT4              *pu4ErrorCode;
     UINT4               u4IpAddrTabAddress;
     INT4                i4TestValIpAddrTabAdvertise;
#endif
{
    INT4                i4RetVal;

    i4RetVal = nmhValidateIndexInstanceFsIpAddressTable (u4IpAddrTabAddress);

    if ((i4RetVal == SNMP_SUCCESS) &&
        ((i4TestValIpAddrTabAdvertise == IRDP_ADVERTISE) ||
         (i4TestValIpAddrTabAdvertise == IRDP_DONT_ADVERTISE)))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                 "Set for IRDP advertise for address %x to %d "
                 "is failed for bad value \n",
                 u4IpAddrTabAddress, i4TestValIpAddrTabAdvertise);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpAddrTabPreflevel
 Input       :  The Indices
                IpAddrTabAddress

                The Object 
                testValIpAddrTabPreflevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpAddrTabPreflevel ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpAddrTabPreflevel (UINT4 *pu4ErrorCode, UINT4 u4IpAddrTabAddress,
                               INT4 i4TestValIpAddrTabPreflevel)
#else
INT1
nmhTestv2FsIpAddrTabPreflevel (pu4ErrorCode, u4IpAddrTabAddress,
                               i4TestValIpAddrTabPreflevel)
     UINT4              *pu4ErrorCode;
     UINT4               u4IpAddrTabAddress;
     INT4                i4TestValIpAddrTabPreflevel;
#endif
{
    UNUSED_PARAM (i4TestValIpAddrTabPreflevel);

    if (nmhValidateIndexInstanceFsIpAddressTable (u4IpAddrTabAddress) !=
        SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "Set for IRDP preference level for address %x to %d "
                     "is failed for bad index \n",
                     u4IpAddrTabAddress, i4TestValIpAddrTabPreflevel);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IpRouterList. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpRtrLstTable
 Input       :  The Indices
                IpRtrLstAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIpRtrLstTable (UINT4 u4IpRtrLstAddress)
#else
INT1
nmhValidateIndexInstanceFsIpRtrLstTable (u4IpRtrLstAddress)
     UINT4               u4IpRtrLstAddress;
#endif
{
    UINT2               u2Index;

    if (u4IpRtrLstAddress == IP_ANY_ADDR)
    {
        return SNMP_FAILURE;
    }

    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if ((gaIrdpRtrList[u2Index].u1RowStatus != 0) &&
            (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress))
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpRouterList
 Input       :  The Indices
                IpRtrLstAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpRouterList ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIpRtrLstTable (UINT4 *pu4IpRtrLstAddress)
#else
INT1
nmhGetFirstIndexFsIpRtrLstTable (pu4IpRtrLstAddress)
     UINT4              *pu4IpRtrLstAddress;
#endif
{
    return (nmhGetNextIndexFsIpRtrLstTable (0, pu4IpRtrLstAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpRouterList
 Input       :  The Indices
                IpRtrLstAddress
                nextIpRtrLstAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpRouterList ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIpRtrLstTable (UINT4 u4IpRtrLstAddress,
                                UINT4 *pu4NextIpRtrLstAddress)
#else
INT1
nmhGetNextIndexFsIpRtrLstTable (u4IpRtrLstAddress, pu4NextIpRtrLstAddress)
     UINT4               u4IpRtrLstAddress;
     UINT4              *pu4NextIpRtrLstAddress;
#endif
{
    UINT4               u4NextLstAddr = 0xffffffff;
    UINT2               u2Index;
    INT1                i1IsFound = IP_FAILURE;

    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if ((gaIrdpRtrList[u2Index].u1RowStatus != 0) &&
            (gaIrdpRtrList[u2Index].u4RouterAddress > u4IpRtrLstAddress) &&
            (gaIrdpRtrList[u2Index].u4RouterAddress < u4NextLstAddr))
        {
            i1IsFound = IP_SUCCESS;
            u4NextLstAddr = IRDP_GET_ROUTER_ADDR (u2Index);
        }
    }

    if (i1IsFound == IP_SUCCESS)
    {
        *pu4NextIpRtrLstAddress = u4NextLstAddr;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpRtrLstIface
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                retValIpRtrLstIface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpRtrLstIface ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRtrLstIface (UINT4 u4IpRtrLstAddress, INT4 *pi4RetValIpRtrLstIface)
#else
INT1
nmhGetFsIpRtrLstIface (u4IpRtrLstAddress, pi4RetValIpRtrLstIface)
     UINT4               u4IpRtrLstAddress;
     INT4               *pi4RetValIpRtrLstIface;
#endif
{
    UINT2               u2Index;
    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            *pi4RetValIpRtrLstIface = gaIrdpRtrList[u2Index].u2Iface;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpRtrLstPreflevel
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                retValIpRtrLstPreflevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpRtrLstPreflevel ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRtrLstPreflevel (UINT4 u4IpRtrLstAddress,
                           INT4 *pi4RetValIpRtrLstPreflevel)
#else
INT1
nmhGetFsIpRtrLstPreflevel (u4IpRtrLstAddress, pi4RetValIpRtrLstPreflevel)
     UINT4               u4IpRtrLstAddress;
     INT4               *pi4RetValIpRtrLstPreflevel;
#endif
{
    UINT2               u2Index;
    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            *pi4RetValIpRtrLstPreflevel = gaIrdpRtrList[u2Index].i4PrefLevel;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpRtrLstStatic
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                retValIpRtrLstStatic
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpRtrLstStatic ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRtrLstStatic (UINT4 u4IpRtrLstAddress, INT4 *pi4RetValIpRtrLstStatic)
#else
INT1
nmhGetFsIpRtrLstStatic (u4IpRtrLstAddress, pi4RetValIpRtrLstStatic)
     UINT4               u4IpRtrLstAddress;
     INT4               *pi4RetValIpRtrLstStatic;
#endif
{
    UINT2               u2Index;
    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            *pi4RetValIpRtrLstStatic = gaIrdpRtrList[u2Index].bDynamicEntry;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpRtrLstStatus
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                retValIpRtrLstStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpRtrLstStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRtrLstStatus (UINT4 u4IpRtrLstAddress, INT4 *pi4RetValIpRtrLstStatus)
#else
INT1
nmhGetFsIpRtrLstStatus (u4IpRtrLstAddress, pi4RetValIpRtrLstStatus)
     UINT4               u4IpRtrLstAddress;
     INT4               *pi4RetValIpRtrLstStatus;
#endif
{
    UINT2               u2Index;
    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            *pi4RetValIpRtrLstStatus = gaIrdpRtrList[u2Index].u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpRtrLstIface
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                setValIpRtrLstIface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpRtrLstIface ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRtrLstIface (UINT4 u4IpRtrLstAddress, INT4 i4SetValIpRtrLstIface)
#else
INT1
nmhSetFsIpRtrLstIface (u4IpRtrLstAddress, i4SetValIpRtrLstIface)
     UINT4               u4IpRtrLstAddress;
     INT4                i4SetValIpRtrLstIface;

#endif
{
    UINT2               u2Index;

    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            gaIrdpRtrList[u2Index].u2Iface = (UINT2) i4SetValIpRtrLstIface;
            IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                         "IRDP Iface is set to %d for address %x \n",
                         i4SetValIpRtrLstIface, u4IpRtrLstAddress);

            IncMsrForFsIpv4RtrLstTable (u4IpRtrLstAddress,
                                        i4SetValIpRtrLstIface,
                                        FsMIFsIpRtrLstIface,
                                        (sizeof (FsMIFsIpRtrLstIface) /
                                         sizeof (UINT4)), FALSE);

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpRtrLstAddress
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                setValIpRtrLstAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpRtrLstAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRtrLstAddress (UINT4 u4IpRtrLstAddress, UINT4 u4SetValIpRtrLstAddress)
#else
INT1
nmhSetFsIpRtrLstAddress (u4IpRtrLstAddress, u4SetValIpRtrLstAddress)
     UINT4               u4IpRtrLstAddress;
     UINT4               u4SetValIpRtrLstAddress;

#endif
{
   /*** $$TRACE_LOG (ENTRY,"IpRtrLstAddress = %u\n", u4IpRtrLstAddress); ***/
   /*** $$TRACE_LOG (ENTRY,"IpRtrLstAddress = %u\n", u4SetValIpRtrLstAddress); ***/

    UNUSED_PARAM (u4IpRtrLstAddress);
    UNUSED_PARAM (u4SetValIpRtrLstAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpRtrLstPreflevel
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                setValIpRtrLstPreflevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpRtrLstPreflevel ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRtrLstPreflevel (UINT4 u4IpRtrLstAddress,
                           INT4 i4SetValIpRtrLstPreflevel)
#else
INT1
nmhSetFsIpRtrLstPreflevel (u4IpRtrLstAddress, i4SetValIpRtrLstPreflevel)
     UINT4               u4IpRtrLstAddress;
     INT4                i4SetValIpRtrLstPreflevel;

#endif
{
    UINT2               u2Index;
    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            gaIrdpRtrList[u2Index].i4PrefLevel = i4SetValIpRtrLstPreflevel;

            IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                         "IRDP Pref level is set to %d for address %x \n",
                         i4SetValIpRtrLstPreflevel, u4IpRtrLstAddress);

            IncMsrForFsIpv4RtrLstTable (u4IpRtrLstAddress,
                                        i4SetValIpRtrLstPreflevel,
                                        FsMIFsIpRtrLstPreflevel,
                                        (sizeof (FsMIFsIpRtrLstPreflevel) /
                                         sizeof (UINT4)), FALSE);

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpRtrLstStatus
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                setValIpRtrLstStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpRtrLstStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRtrLstStatus (UINT4 u4IpRtrLstAddress, INT4 i4SetValIpRtrLstStatus)
#else
INT1
nmhSetFsIpRtrLstStatus (u4IpRtrLstAddress, i4SetValIpRtrLstStatus)
     UINT4               u4IpRtrLstAddress;
     INT4                i4SetValIpRtrLstStatus;

#endif
{
    UINT2               u2Index;

    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            switch (i4SetValIpRtrLstStatus)
            {
                case IRDP_ACTIVE:
                    if ((gaIrdpRtrList[u2Index].u1RowStatus ==
                         IRDP_NOT_IN_SERVICE) ||
                        (gaIrdpRtrList[u2Index].u1RowStatus == IRDP_NOT_READY))
                    {
                        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                                     "IRDP Router list status is set to %d  "
                                     "for address %x \n",
                                     i4SetValIpRtrLstStatus, u4IpRtrLstAddress);

                        gaIrdpRtrList[u2Index].u1RowStatus = IRDP_ACTIVE;
                    }
                    break;

                case IRDP_NOT_IN_SERVICE:
                    if (gaIrdpRtrList[u2Index].u1RowStatus == IRDP_ACTIVE)
                    {
                        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                                     "IRDP Router list status is set to %d  "
                                     "for address %x \n",
                                     i4SetValIpRtrLstStatus, u4IpRtrLstAddress);

                        gaIrdpRtrList[u2Index].u1RowStatus =
                            IRDP_NOT_IN_SERVICE;
                    }

                    break;

                case IRDP_DESTROY:
                    if (gaIrdpRtrList[u2Index].bDynamicEntry == IRDP_DYNAMIC)
                    {
                        IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                           &gaIrdpRtrList[u2Index].Timer.
                                           Timer_node);
                    }
                    gaIrdpRtrList[u2Index].u1RowStatus = IRDP_DESTROY;
                    IrdpDeleteEntryFromRtrList (u2Index);
                    IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                                 "IRDP Router list entry is set to %d "
                                 "for address %x \n",
                                 i4SetValIpRtrLstStatus, u4IpRtrLstAddress);

                    break;
                default:
                    break;
            }
            IncMsrForFsIpv4RtrLstTable (u4IpRtrLstAddress,
                                        i4SetValIpRtrLstStatus,
                                        FsMIFsIpRtrLstStatus,
                                        (sizeof (FsMIFsIpRtrLstStatus) /
                                         sizeof (UINT4)), TRUE);

            return SNMP_SUCCESS;
        }
    }

    u2Index = IrdpGetFreeIndexInRtrList (IRDP_STATIC);
    if (u2Index >= IRDP_MAX_DEFAULT_ROUTERS)
    {
       /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
        return SNMP_FAILURE;
    }
    gaIrdpRtrList[u2Index].bDynamicEntry = IRDP_STATIC;
    gaIrdpRtrList[u2Index].u4RouterAddress = u4IpRtrLstAddress;
    gaIrdpRtrList[u2Index].u1RowStatus = IRDP_NOT_READY;
    gaIrdpRtrList[u2Index].u2Iface = IRDP_INVALID_IFACE;
    gaIrdpRtrList[u2Index].i4PrefLevel = 0;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpRtrLstIface
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                testValIpRtrLstIface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpRtrLstIface (UINT4 *pu4ErrorCode, UINT4 u4IpRtrLstAddress,
                          INT4 i4TestValIpRtrLstIface)
{
    UINT2               u2Index;

    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (gaIrdpRtrList[u2Index].u2Iface == i4TestValIpRtrLstIface)
        {
            if (CfaIpIfIsLocalNetOnInterface ((UINT4) i4TestValIpRtrLstIface,
                                              u4IpRtrLstAddress) == CFA_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpRtrLstAddress
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                testValIpRtrLstAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpRtrLstAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRtrLstAddress (UINT4 *pu4ErrorCode, UINT4 u4IpRtrLstAddress,
                            UINT4 u4TestValIpRtrLstAddress)
#else
INT1
nmhTestv2FsIpRtrLstAddress (pu4ErrorCode, u4IpRtrLstAddress,
                            u4TestValIpRtrLstAddress)
     UINT4              *pu4ErrorCode;
     UINT4               u4IpRtrLstAddress;
     UINT4               u4TestValIpRtrLstAddress;
#endif
{
    UNUSED_PARAM (u4IpRtrLstAddress);
    UNUSED_PARAM (u4TestValIpRtrLstAddress);
    *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpRtrLstPreflevel
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                testValIpRtrLstPreflevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpRtrLstPreflevel ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRtrLstPreflevel (UINT4 *pu4ErrorCode, UINT4 u4IpRtrLstAddress,
                              INT4 i4TestValIpRtrLstPreflevel)
#else
INT1
nmhTestv2FsIpRtrLstPreflevel (pu4ErrorCode, u4IpRtrLstAddress,
                              i4TestValIpRtrLstPreflevel)
     UINT4              *pu4ErrorCode;
     UINT4               u4IpRtrLstAddress;
     INT4                i4TestValIpRtrLstPreflevel;
#endif
{
    UINT2               u2Index;

    if (i4TestValIpRtrLstPreflevel < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            if (gaIrdpRtrList[u2Index].bDynamicEntry == IRDP_STATIC)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
                IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                             "Set of Preference level is failed for "
                             "address %x \n", u4IpRtrLstAddress);

                return SNMP_FAILURE;
            }
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                 "Set of Preference  level is failed for address %x \n",
                 u4IpRtrLstAddress);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpRtrLstStatus
 Input       :  The Indices
                IpRtrLstAddress

                The Object 
                testValIpRtrLstStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpRtrLstStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRtrLstStatus (UINT4 *pu4ErrorCode, UINT4 u4IpRtrLstAddress,
                           INT4 i4TestValIpRtrLstStatus)
#else
INT1
nmhTestv2FsIpRtrLstStatus (pu4ErrorCode, u4IpRtrLstAddress,
                           i4TestValIpRtrLstStatus)
     UINT4              *pu4ErrorCode;
     UINT4               u4IpRtrLstAddress;
     INT4                i4TestValIpRtrLstStatus;
#endif
{
    UINT2               u2Index;

    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        if (u4IpRtrLstAddress == gaIrdpRtrList[u2Index].u4RouterAddress)
        {
            switch (i4TestValIpRtrLstStatus)
            {
                case IRDP_ACTIVE:
                case IRDP_NOT_IN_SERVICE:
                case IRDP_DESTROY:
                    return SNMP_SUCCESS;
                default:
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
            }
        }
    }
    if ((IrdpGetFreeSpaceInRtrList () != IRDP_NO_SPACE) &&
        (i4TestValIpRtrLstStatus == IRDP_CREATE_AND_WAIT) &&
        (IpVerifyAddr (u4IpRtrLstAddress, IP_ADDR_SRC) == SUCCESS))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                 "Set of Router list status is failed for address %x "
                 "for bad value \n", u4IpRtrLstAddress);

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIrdpInAdvertisements
 Input       :  The Indices

                The Object 
                retValIrdpInAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpInAdvertisements ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpInAdvertisements (UINT4 *pu4RetValIrdpInAdvertisements)
#else
INT1
nmhGetFsIrdpInAdvertisements (pu4RetValIrdpInAdvertisements)
     UINT4              *pu4RetValIrdpInAdvertisements;
#endif
{
    *pu4RetValIrdpInAdvertisements = gu4IrdpInAdvertCount;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpInSolicitations
 Input       :  The Indices

                The Object 
                retValIrdpInSolicitations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpInSolicitations ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpInSolicitations (UINT4 *pu4RetValIrdpInSolicitations)
#else
INT1
nmhGetFsIrdpInSolicitations (pu4RetValIrdpInSolicitations)
     UINT4              *pu4RetValIrdpInSolicitations;
#endif
{
    *pu4RetValIrdpInSolicitations = gu4IrdpInSolicitCount;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpOutAdvertisements
 Input       :  The Indices

                The Object 
                retValIrdpOutAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpOutAdvertisements ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpOutAdvertisements (UINT4 *pu4RetValIrdpOutAdvertisements)
#else
INT1
nmhGetFsIrdpOutAdvertisements (pu4RetValIrdpOutAdvertisements)
     UINT4              *pu4RetValIrdpOutAdvertisements;
#endif
{
    *pu4RetValIrdpOutAdvertisements = gu4IrdpOutAdvertCount;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpOutSolicitations
 Input       :  The Indices

                The Object 
                retValIrdpOutSolicitations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpOutSolicitations ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpOutSolicitations (UINT4 *pu4RetValIrdpOutSolicitations)
#else
INT1
nmhGetFsIrdpOutSolicitations (pu4RetValIrdpOutSolicitations)
     UINT4              *pu4RetValIrdpOutSolicitations;
#endif
{
    *pu4RetValIrdpOutSolicitations = gu4IrdpOutSolicitCount;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpSendAdvertisementsEnable
 Input       :  The Indices

                The Object 
                retValIrdpSendAdvertisementsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpSendAdvertisementsEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpSendAdvertisementsEnable (INT4
                                      *pi4RetValIrdpSendAdvertisementsEnable)
#else
INT1
nmhGetFsIrdpSendAdvertisementsEnable (pi4RetValIrdpSendAdvertisementsEnable)
     INT4               *pi4RetValIrdpSendAdvertisementsEnable;
#endif
{
    *pi4RetValIrdpSendAdvertisementsEnable = gbIrdpSendAdvertEnable;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIrdpSendAdvertisementsEnable
 Input       :  The Indices

                The Object 
                setValIrdpSendAdvertisementsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIrdpSendAdvertisementsEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIrdpSendAdvertisementsEnable (INT4 i4SetValIrdpSendAdvertisementsEnable)
#else
INT1
nmhSetFsIrdpSendAdvertisementsEnable (i4SetValIrdpSendAdvertisementsEnable)
     INT4                i4SetValIrdpSendAdvertisementsEnable;

#endif
{
    UINT2               u2Iface;
    UINT2               u2Index;
    tIpCxt             *pIpCxt = NULL;
    tIpCxt             *pIfIpCxt = NULL;
    /* Take a Data Structure Lock */

    pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    IPFWD_DS_LOCK ();

    if (gbIrdpSendAdvertEnable != i4SetValIrdpSendAdvertisementsEnable)
    {
        if ((gbIrdpSendAdvertEnable == IRDP_ADVERTISE) &&
            (IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_ENABLE))
        {
            IPIF_LOGICAL_IFACES_SCAN (u2Iface)
            {
                if (NULL != (pIfIpCxt = UtilIpGetCxtFromIpPortNum (u2Iface)))
                {
                    if (pIfIpCxt->u4ContextId != IP_DEFAULT_CONTEXT)
                    {
                        continue;
                    }

                    if ((gIpGlobalInfo.Ipif_config[u2Iface].u1Admin ==
                         IPIF_ADMIN_ENABLE) &&
                        (gIpGlobalInfo.Ipif_config[u2Iface].u1Oper ==
                         IPIF_OPER_ENABLE))
                    {
                        for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE;
                             u2Index++)
                        {
                            if (IRDP_GET_ADVERTFLAG (u2Iface, u2Index) ==
                                IRDP_ADVERTISE)
                            {
                                gaIrdpCntlRec[u2Iface].bSendAdvert = TRUE;
                                gaIrdpCntlRec[u2Iface].u1InitAdvertCount = 0;
                                IrdpServerInit (u2Iface);
                                break;
                            }
                        }
                    }
                }
            }
        }
        else if (i4SetValIrdpSendAdvertisementsEnable == IRDP_DONT_ADVERTISE)
        {
            IPIF_LOGICAL_IFACES_SCAN (u2Iface)
            {
                IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                                   &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);
                gaIrdpCntlRec[u2Iface].bSendAdvert = FALSE;
            }
        }
        gbIrdpSendAdvertEnable = (BOOLEAN) i4SetValIrdpSendAdvertisementsEnable;
    }

    IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                 "IRDP Send Advertisements is set to %d \n ",
                 i4SetValIrdpSendAdvertisementsEnable);

    /* Unlock it */
    IPFWD_DS_UNLOCK ();
    IncMsrForFsIpv4Scalars (i4SetValIrdpSendAdvertisementsEnable,
                            FsMIFsIrdpSendAdvertisementsEnable,
                            (sizeof (FsMIFsIrdpSendAdvertisementsEnable) /
                             sizeof (UINT4)));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIrdpSendAdvertisementsEnable
 Input       :  The Indices

                The Object 
                testValIrdpSendAdvertisementsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IrdpSendAdvertisementsEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIrdpSendAdvertisementsEnable (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValIrdpSendAdvertisementsEnable)
#else
INT1
nmhTestv2FsIrdpSendAdvertisementsEnable (pu4ErrorCode,
                                         i4TestValIrdpSendAdvertisementsEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIrdpSendAdvertisementsEnable;
#endif
{
    if ((i4TestValIrdpSendAdvertisementsEnable == IRDP_ADVERTISE) ||
        (i4TestValIrdpSendAdvertisementsEnable == IRDP_DONT_ADVERTISE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    IP_TRC (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
            "Set request to IRDP Send Advertisements is failed "
            "due to bad value. \n");

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : IrdpIfConfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIrdpIfConfTable
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIrdpIfConfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIrdpIfConfTable (INT4 i4IrdpIfConfIfNum,
                                           INT4 i4IrdpIfConfSubref)
#else
INT1
nmhValidateIndexInstanceFsIrdpIfConfTable (i4IrdpIfConfIfNum,
                                           i4IrdpIfConfSubref)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
#endif
{
    return nmhValidateIndexInstanceIpIfTable (i4IrdpIfConfIfNum,
                                              i4IrdpIfConfSubref);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIrdpIfConfTable
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIrdpIfConfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIrdpIfConfTable (INT4 *pi4IrdpIfConfIfNum,
                                   INT4 *pi4IrdpIfConfSubref)
#else
INT1
nmhGetFirstIndexFsIrdpIfConfTable (pi4IrdpIfConfIfNum, pi4IrdpIfConfSubref)
     INT4               *pi4IrdpIfConfIfNum;
     INT4               *pi4IrdpIfConfSubref;
#endif
{
    return nmhGetFirstIndexIpIfTable (pi4IrdpIfConfIfNum, pi4IrdpIfConfSubref);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIrdpIfConfTable
 Input       :  The Indices
                IrdpIfConfIfNum
                nextIrdpIfConfIfNum
                IrdpIfConfSubref
                nextIrdpIfConfSubref
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIrdpIfConfTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIrdpIfConfTable (INT4 i4IrdpIfConfIfNum,
                                  INT4 *pi4NextIrdpIfConfIfNum,
                                  INT4 i4IrdpIfConfSubref,
                                  INT4 *pi4NextIrdpIfConfSubref)
#else
INT1
nmhGetNextIndexFsIrdpIfConfTable (i4IrdpIfConfIfNum, pi4NextIrdpIfConfIfNum,
                                  i4IrdpIfConfSubref, pi4NextIrdpIfConfSubref)
     INT4                i4IrdpIfConfIfNum;
     INT4               *pi4NextIrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4               *pi4NextIrdpIfConfSubref;
#endif
{
    if (i4IrdpIfConfIfNum < 0)
    {
        return SNMP_FAILURE;
    }
    return nmhGetNextIndexIpIfTable (i4IrdpIfConfIfNum,
                                     pi4NextIrdpIfConfIfNum,
                                     i4IrdpIfConfSubref,
                                     pi4NextIrdpIfConfSubref);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIrdpIfConfAdvertisementAddress
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                retValIrdpIfConfAdvertisementAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpIfConfAdvertisementAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpIfConfAdvertisementAddress (INT4 i4IrdpIfConfIfNum,
                                        INT4 i4IrdpIfConfSubref,
                                        UINT4
                                        *pu4RetValIrdpIfConfAdvertisementAddress)
#else
INT1
nmhGetFsIrdpIfConfAdvertisementAddress (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        pu4RetValIrdpIfConfAdvertisementAddress)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     UINT4              *pu4RetValIrdpIfConfAdvertisementAddress;
#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {

        *pu4RetValIrdpIfConfAdvertisementAddress =
            gaIrdpIfaceRec[u2Index].u4AdvertAddr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpIfConfMaxAdvertisementInterval
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                retValIrdpIfConfMaxAdvertisementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpIfConfMaxAdvertisementInterval ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpIfConfMaxAdvertisementInterval (INT4 i4IrdpIfConfIfNum,
                                            INT4 i4IrdpIfConfSubref,
                                            INT4
                                            *pi4RetValIrdpIfConfMaxAdvertisementInterval)
#else
INT1
nmhGetFsIrdpIfConfMaxAdvertisementInterval (i4IrdpIfConfIfNum,
                                            i4IrdpIfConfSubref,
                                            pi4RetValIrdpIfConfMaxAdvertisementInterval)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4               *pi4RetValIrdpIfConfMaxAdvertisementInterval;
#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        *pi4RetValIrdpIfConfMaxAdvertisementInterval =
            gaIrdpIfaceRec[u2Index].u2MaxAdvertInterval;
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpIfConfMinAdvertisementInterval
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                retValIrdpIfConfMinAdvertisementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpIfConfMinAdvertisementInterval ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpIfConfMinAdvertisementInterval (INT4 i4IrdpIfConfIfNum,
                                            INT4 i4IrdpIfConfSubref,
                                            INT4
                                            *pi4RetValIrdpIfConfMinAdvertisementInterval)
#else
INT1
nmhGetFsIrdpIfConfMinAdvertisementInterval (i4IrdpIfConfIfNum,
                                            i4IrdpIfConfSubref,
                                            pi4RetValIrdpIfConfMinAdvertisementInterval)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4               *pi4RetValIrdpIfConfMinAdvertisementInterval;
#endif
{

    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        *pi4RetValIrdpIfConfMinAdvertisementInterval =
            gaIrdpIfaceRec[u2Index].u2MinAdvertInterval;
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpIfConfAdvertisementLifetime
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                retValIrdpIfConfAdvertisementLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpIfConfAdvertisementLifetime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpIfConfAdvertisementLifetime (INT4 i4IrdpIfConfIfNum,
                                         INT4 i4IrdpIfConfSubref,
                                         INT4
                                         *pi4RetValIrdpIfConfAdvertisementLifetime)
#else
INT1
nmhGetFsIrdpIfConfAdvertisementLifetime (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                         pi4RetValIrdpIfConfAdvertisementLifetime)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4               *pi4RetValIrdpIfConfAdvertisementLifetime;
#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        *pi4RetValIrdpIfConfAdvertisementLifetime =
            gaIrdpIfaceRec[u2Index].u2AdvertLifeTime;
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsIrdpIfConfPerformRouterDiscovery
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                retValIrdpIfConfPerformRouterDiscovery
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpIfConfPerformRouterDiscovery ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpIfConfPerformRouterDiscovery (INT4 i4IrdpIfConfIfNum,
                                          INT4 i4IrdpIfConfSubref,
                                          INT4
                                          *pi4RetValIrdpIfConfPerformRouterDiscovery)
#else
INT1
nmhGetFsIrdpIfConfPerformRouterDiscovery (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                          pi4RetValIrdpIfConfPerformRouterDiscovery)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4               *pi4RetValIrdpIfConfPerformRouterDiscovery;
#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        *pi4RetValIrdpIfConfPerformRouterDiscovery =
            gaIrdpIfaceRec[u2Index].bPerformRouterDiscovery;

       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIrdpIfConfSolicitationAddress
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                retValIrdpIfConfSolicitationAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIrdpIfConfSolicitationAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIrdpIfConfSolicitationAddress (INT4 i4IrdpIfConfIfNum,
                                       INT4 i4IrdpIfConfSubref,
                                       UINT4
                                       *pu4RetValIrdpIfConfSolicitationAddress)
#else
INT1
nmhGetFsIrdpIfConfSolicitationAddress (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                       pu4RetValIrdpIfConfSolicitationAddress)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     UINT4              *pu4RetValIrdpIfConfSolicitationAddress;
#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        *pu4RetValIrdpIfConfSolicitationAddress =
            gaIrdpIfaceRec[u2Index].u4SolicitAddr;
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIrdpIfConfAdvertisementAddress
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                setValIrdpIfConfAdvertisementAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIrdpIfConfAdvertisementAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIrdpIfConfAdvertisementAddress (INT4 i4IrdpIfConfIfNum,
                                        INT4 i4IrdpIfConfSubref,
                                        UINT4
                                        u4SetValIrdpIfConfAdvertisementAddress)
#else
INT1
nmhSetFsIrdpIfConfAdvertisementAddress (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        u4SetValIrdpIfConfAdvertisementAddress)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     UINT4               u4SetValIrdpIfConfAdvertisementAddress;

#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        gaIrdpIfaceRec[u2Index].u4AdvertAddr =
            u4SetValIrdpIfConfAdvertisementAddress;
        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Conf. Advertisement address for interface %d "
                     "is set to %x \n",
                     i4IrdpIfConfIfNum, u4SetValIrdpIfConfAdvertisementAddress);

        IncMsrForFsIpv4IrdpIfConfTable (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        'p',
                                        &u4SetValIrdpIfConfAdvertisementAddress,
                                        FsMIFsIrdpIfConfAdvertisementAddress,
                                        (sizeof
                                         (FsMIFsIrdpIfConfAdvertisementAddress)
                                         / sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIrdpIfConfMaxAdvertisementInterval
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                setValIrdpIfConfMaxAdvertisementInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIrdpIfConfMaxAdvertisementInterval ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIrdpIfConfMaxAdvertisementInterval (INT4 i4IrdpIfConfIfNum,
                                            INT4 i4IrdpIfConfSubref,
                                            INT4
                                            i4SetValIrdpIfConfMaxAdvertisementInterval)
#else
INT1
nmhSetFsIrdpIfConfMaxAdvertisementInterval (i4IrdpIfConfIfNum,
                                            i4IrdpIfConfSubref,
                                            i4SetValIrdpIfConfMaxAdvertisementInterval)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4SetValIrdpIfConfMaxAdvertisementInterval;

#endif
{
    UINT2               u2Iface;
    UINT4               u4TransIntvl;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Iface = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Iface != IRDP_INVALID_IFACE) && (u2Iface < IPIF_MAX_LOGICAL_IFACES))
    {
        gaIrdpIfaceRec[u2Iface].u2MaxAdvertInterval =
            (UINT2) i4SetValIrdpIfConfMaxAdvertisementInterval;
        if (gaIrdpIfaceRec[u2Iface].u2MinAdvertInterval >
            i4SetValIrdpIfConfMaxAdvertisementInterval)
        {
            gaIrdpIfaceRec[u2Iface].u2MinAdvertInterval =
                (UINT2) ((i4SetValIrdpIfConfMaxAdvertisementInterval * 3) / 4);
        }
        if (gaIrdpIfaceRec[u2Iface].u2AdvertLifeTime <
            i4SetValIrdpIfConfMaxAdvertisementInterval)
        {
            gaIrdpIfaceRec[u2Iface].u2AdvertLifeTime =
                (UINT2) (3 * i4SetValIrdpIfConfMaxAdvertisementInterval);
        }
        IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                           &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);
        u4TransIntvl = IrdpGetRandom (u2Iface,
                                      gaIrdpIfaceRec
                                      [u2Iface].u2MinAdvertInterval,
                                      gaIrdpIfaceRec
                                      [u2Iface].u2MaxAdvertInterval);

        if (gaIrdpCntlRec[u2Iface].u1InitAdvertCount <
            IRDP_MAX_INITIAL_ADVERTISEMENTS)
        {
            if (u4TransIntvl > IRDP_MAX_INITIAL_ADVERT_INTERVAL)
                u4TransIntvl = IRDP_MAX_INITIAL_ADVERT_INTERVAL;
        }
        gaIrdpIfaceRec[u2Iface].Timer.u1Id = IRDP_ADVERT_TIMER_ID;
        gaIrdpIfaceRec[u2Iface].Timer.Timer_node.u4Data = u2Iface;

        IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                         &gaIrdpIfaceRec[u2Iface].Timer.Timer_node,
                         u4TransIntvl);

        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Conf.Max.  "
                     "Advertisement interval for interface %d is set to %x \n",
                     i4IrdpIfConfIfNum,
                     i4SetValIrdpIfConfMaxAdvertisementInterval);

        IncMsrForFsIpv4IrdpIfConfTable (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        'i',
                                        &i4SetValIrdpIfConfMaxAdvertisementInterval,
                                        FsMIFsIrdpIfConfMaxAdvertisementInterval,
                                        (sizeof
                                         (FsMIFsIrdpIfConfMaxAdvertisementInterval)
                                         / sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIrdpIfConfMinAdvertisementInterval
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                setValIrdpIfConfMinAdvertisementInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIrdpIfConfMinAdvertisementInterval ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIrdpIfConfMinAdvertisementInterval (INT4 i4IrdpIfConfIfNum,
                                            INT4 i4IrdpIfConfSubref,
                                            INT4
                                            i4SetValIrdpIfConfMinAdvertisementInterval)
#else
INT1
nmhSetFsIrdpIfConfMinAdvertisementInterval (i4IrdpIfConfIfNum,
                                            i4IrdpIfConfSubref,
                                            i4SetValIrdpIfConfMinAdvertisementInterval)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4SetValIrdpIfConfMinAdvertisementInterval;

#endif
{
    UINT2               u2Iface;
    UINT4               u4TransIntvl;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Iface = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Iface != IRDP_INVALID_IFACE) && (u2Iface < IPIF_MAX_LOGICAL_IFACES))
    {
        gaIrdpIfaceRec[u2Iface].u2MinAdvertInterval =
            (UINT2) i4SetValIrdpIfConfMinAdvertisementInterval;
        IRDP_UNLINK_TIMER (gIpGlobalInfo.IpTimerListId,
                           &gaIrdpIfaceRec[u2Iface].Timer.Timer_node);
        u4TransIntvl = IrdpGetRandom (u2Iface,
                                      gaIrdpIfaceRec
                                      [u2Iface].u2MinAdvertInterval,
                                      gaIrdpIfaceRec
                                      [u2Iface].u2MaxAdvertInterval);

        if (gaIrdpCntlRec[u2Iface].u1InitAdvertCount <
            IRDP_MAX_INITIAL_ADVERTISEMENTS)
        {
            if (u4TransIntvl > IRDP_MAX_INITIAL_ADVERT_INTERVAL)
                u4TransIntvl = IRDP_MAX_INITIAL_ADVERT_INTERVAL;
        }
        gaIrdpIfaceRec[u2Iface].Timer.u1Id = IRDP_ADVERT_TIMER_ID;
        gaIrdpIfaceRec[u2Iface].Timer.Timer_node.u4Data = u2Iface;

        IRDP_LINK_TIMER (gIpGlobalInfo.IpTimerListId,
                         &gaIrdpIfaceRec[u2Iface].Timer.Timer_node,
                         u4TransIntvl);

        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Conf.Max. "
                     "Advertisement interval for interface %d is set to %x \n",
                     i4IrdpIfConfIfNum,
                     i4SetValIrdpIfConfMinAdvertisementInterval);

        IncMsrForFsIpv4IrdpIfConfTable (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        'i',
                                        &i4SetValIrdpIfConfMinAdvertisementInterval,
                                        FsMIFsIrdpIfConfMinAdvertisementInterval,
                                        (sizeof
                                         (FsMIFsIrdpIfConfMinAdvertisementInterval)
                                         / sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIrdpIfConfAdvertisementLifetime
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                setValIrdpIfConfAdvertisementLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIrdpIfConfAdvertisementLifetime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIrdpIfConfAdvertisementLifetime (INT4 i4IrdpIfConfIfNum,
                                         INT4 i4IrdpIfConfSubref,
                                         INT4
                                         i4SetValIrdpIfConfAdvertisementLifetime)
#else
INT1
nmhSetFsIrdpIfConfAdvertisementLifetime (i4IrdpIfConfIfNum,
                                         i4IrdpIfConfSubref,
                                         i4SetValIrdpIfConfAdvertisementLifetime)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4SetValIrdpIfConfAdvertisementLifetime;

#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        gaIrdpIfaceRec[u2Index].u2AdvertLifeTime =
            (UINT2) i4SetValIrdpIfConfAdvertisementLifetime;

        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Conf Advertisement lifetime for interface %d "
                     "is set to %x \n", i4IrdpIfConfIfNum,
                     i4SetValIrdpIfConfAdvertisementLifetime);
        IncMsrForFsIpv4IrdpIfConfTable (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        'i',
                                        &i4SetValIrdpIfConfAdvertisementLifetime,
                                        FsMIFsIrdpIfConfAdvertisementLifetime,
                                        (sizeof
                                         (FsMIFsIrdpIfConfAdvertisementLifetime)
                                         / sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIrdpIfConfPerformRouterDiscovery
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                setValIrdpIfConfPerformRouterDiscovery
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIrdpIfConfPerformRouterDiscovery ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIrdpIfConfPerformRouterDiscovery (INT4 i4IrdpIfConfIfNum,
                                          INT4 i4IrdpIfConfSubref,
                                          INT4
                                          i4SetValIrdpIfConfPerformRouterDiscovery)
#else
INT1
nmhSetFsIrdpIfConfPerformRouterDiscovery (i4IrdpIfConfIfNum,
                                          i4IrdpIfConfSubref,
                                          i4SetValIrdpIfConfPerformRouterDiscovery)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4SetValIrdpIfConfPerformRouterDiscovery;

#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        gaIrdpIfaceRec[u2Index].bPerformRouterDiscovery =
            (BOOLEAN) i4SetValIrdpIfConfPerformRouterDiscovery;

        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Conf perform router discovery for interface %d "
                     "is set to %x \n", i4IrdpIfConfIfNum,
                     i4SetValIrdpIfConfPerformRouterDiscovery);
        IncMsrForFsIpv4IrdpIfConfTable (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        'i',
                                        &i4SetValIrdpIfConfPerformRouterDiscovery,
                                        FsMIFsIrdpIfConfPerformRouterDiscovery,
                                        (sizeof
                                         (FsMIFsIrdpIfConfPerformRouterDiscovery)
                                         / sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIrdpIfConfSolicitationAddress
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                setValIrdpIfConfSolicitationAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIrdpIfConfSolicitationAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIrdpIfConfSolicitationAddress (INT4 i4IrdpIfConfIfNum,
                                       INT4 i4IrdpIfConfSubref,
                                       UINT4
                                       u4SetValIrdpIfConfSolicitationAddress)
#else
INT1
nmhSetFsIrdpIfConfSolicitationAddress (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                       u4SetValIrdpIfConfSolicitationAddress)
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     UINT4               u4SetValIrdpIfConfSolicitationAddress;

#endif
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if ((u2Index != IRDP_INVALID_IFACE) && (u2Index < IPIF_MAX_LOGICAL_IFACES))
    {
        gaIrdpIfaceRec[u2Index].u4SolicitAddr =
            u4SetValIrdpIfConfSolicitationAddress;

        IP_TRC_ARG2 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                     "IRDP Conf solicitation address for interface %d "
                     "is set to %x \n", i4IrdpIfConfIfNum,
                     u4SetValIrdpIfConfSolicitationAddress);
        IncMsrForFsIpv4IrdpIfConfTable (i4IrdpIfConfIfNum, i4IrdpIfConfSubref,
                                        'p',
                                        &u4SetValIrdpIfConfSolicitationAddress,
                                        FsMIFsIrdpIfConfSolicitationAddress,
                                        (sizeof
                                         (FsMIFsIrdpIfConfSolicitationAddress) /
                                         sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIrdpIfConfAdvertisementAddress
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                testValIrdpIfConfAdvertisementAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IrdpIfConfAdvertisementAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIrdpIfConfAdvertisementAddress (UINT4 *pu4ErrorCode,
                                           INT4 i4IrdpIfConfIfNum,
                                           INT4 i4IrdpIfConfSubref,
                                           UINT4
                                           u4TestValIrdpIfConfAdvertisementAddress)
#else
INT1
nmhTestv2FsIrdpIfConfAdvertisementAddress (pu4ErrorCode, i4IrdpIfConfIfNum,
                                           i4IrdpIfConfSubref,
                                           u4TestValIrdpIfConfAdvertisementAddress)
     UINT4              *pu4ErrorCode;
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     UINT4               u4TestValIrdpIfConfAdvertisementAddress;
#endif
{
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (i4IrdpIfConfIfNum);
    UNUSED_PARAM (i4IrdpIfConfSubref);

    if ((u4TestValIrdpIfConfAdvertisementAddress == IP_GEN_BCAST_ADDR) ||
        (u4TestValIrdpIfConfAdvertisementAddress == IRDP_ALL_SYSTEMS_MULTI))
    {
        return SNMP_SUCCESS;
    }

    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IrdpIfConfIfNum, &u4ContextId)
        == VCM_SUCCESS)
    {
        if (u4ContextId == IP_DEFAULT_CONTEXT)
        {
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                 "Set to IRDP Conf advertisement address for interface %d "
                 "is failed for bad value \n", i4IrdpIfConfIfNum);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIrdpIfConfMaxAdvertisementInterval
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                testValIrdpIfConfMaxAdvertisementInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IrdpIfConfMaxAdvertisementInterval ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIrdpIfConfMaxAdvertisementInterval (UINT4 *pu4ErrorCode,
                                               INT4 i4IrdpIfConfIfNum,
                                               INT4 i4IrdpIfConfSubref,
                                               INT4
                                               i4TestValIrdpIfConfMaxAdvertisementInterval)
#else
INT1
nmhTestv2FsIrdpIfConfMaxAdvertisementInterval (pu4ErrorCode, i4IrdpIfConfIfNum,
                                               i4IrdpIfConfSubref,
                                               i4TestValIrdpIfConfMaxAdvertisementInterval)
     UINT4              *pu4ErrorCode;
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4TestValIrdpIfConfMaxAdvertisementInterval;
#endif
{
    tIP_INTERFACE       InterfaceId;    /* Interface index  */
    UINT4               u4ContextId = 0;
    UINT2               u2Index = 0;

    MEMSET (&InterfaceId, 0, sizeof (tIP_INTERFACE));
    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IrdpIfConfIfNum, &u4ContextId)
        == VCM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4ContextId != IP_DEFAULT_CONTEXT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);

    if (u2Index != IRDP_INVALID_IFACE)
    {
        if ((i4TestValIrdpIfConfMaxAdvertisementInterval < 4) ||
            (i4TestValIrdpIfConfMaxAdvertisementInterval > 1800))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                         "Set to IRDP Conf Max advertisement interval"
                         "for interface %d is failed for bad value \n",
                         i4IrdpIfConfIfNum);

            return SNMP_FAILURE;

        }
        else
        {
            if (i4TestValIrdpIfConfMaxAdvertisementInterval <
                gaIrdpIfaceRec[u2Index].u2MinAdvertInterval)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                             "Set to IRDP Conf Max advertisement interval for"
                             " interface %d is failed for inconsistent value \n",
                             i4IrdpIfConfIfNum);
                return SNMP_FAILURE;
            }

            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsIrdpIfConfMinAdvertisementInterval
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                testValIrdpIfConfMinAdvertisementInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IrdpIfConfMinAdvertisementInterval ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIrdpIfConfMinAdvertisementInterval (UINT4 *pu4ErrorCode,
                                               INT4 i4IrdpIfConfIfNum,
                                               INT4 i4IrdpIfConfSubref,
                                               INT4
                                               i4TestValIrdpIfConfMinAdvertisementInterval)
#else
INT1
nmhTestv2FsIrdpIfConfMinAdvertisementInterval (pu4ErrorCode, i4IrdpIfConfIfNum,
                                               i4IrdpIfConfSubref,
                                               i4TestValIrdpIfConfMinAdvertisementInterval)
     UINT4              *pu4ErrorCode;
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4TestValIrdpIfConfMinAdvertisementInterval;
#endif
{
    tIP_INTERFACE       InterfaceId;    /* Interface index  */
    UINT4               u4ContextId = 0;
    UINT2               u2Index;

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IrdpIfConfIfNum, &u4ContextId)
        == VCM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4ContextId != IP_DEFAULT_CONTEXT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if (u2Index != IRDP_INVALID_IFACE)
    {
        if ((i4TestValIrdpIfConfMinAdvertisementInterval < 4) ||
            (i4TestValIrdpIfConfMinAdvertisementInterval > 1800))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                         "Set to IRDP Conf Min advertisement interval for"
                         " interface %d is failed for bad value \n",
                         i4IrdpIfConfIfNum);

            return SNMP_FAILURE;

        }
        else
        {
            if (i4TestValIrdpIfConfMinAdvertisementInterval >
                gaIrdpIfaceRec[u2Index].u2MaxAdvertInterval)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                             "Set to IRDP Conf Min advertisement interval for"
                             " interface %d is failed for inconsistent value \n",
                             i4IrdpIfConfIfNum);
                return SNMP_FAILURE;
            }

            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIrdpIfConfAdvertisementLifetime
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                testValIrdpIfConfAdvertisementLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IrdpIfConfAdvertisementLifetime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIrdpIfConfAdvertisementLifetime (UINT4 *pu4ErrorCode,
                                            INT4 i4IrdpIfConfIfNum,
                                            INT4 i4IrdpIfConfSubref,
                                            INT4
                                            i4TestValIrdpIfConfAdvertisementLifetime)
#else
INT1
nmhTestv2FsIrdpIfConfAdvertisementLifetime (pu4ErrorCode, i4IrdpIfConfIfNum,
                                            i4IrdpIfConfSubref,
                                            i4TestValIrdpIfConfAdvertisementLifetime)
     UINT4              *pu4ErrorCode;
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4TestValIrdpIfConfAdvertisementLifetime;
#endif
{
    tIP_INTERFACE       InterfaceId;    /* Interface index  */
    UINT4               u4ContextId = 0;
    UINT2               u2Index;

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IrdpIfConfIfNum, i4IrdpIfConfSubref, &InterfaceId);

    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IrdpIfConfIfNum, &u4ContextId)
        == VCM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4ContextId != IP_DEFAULT_CONTEXT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId);
    if (u2Index != IRDP_INVALID_IFACE)
    {
        if ((i4TestValIrdpIfConfAdvertisementLifetime <
             IRDP_DEF_ADVERT_LTIME) ||
            (i4TestValIrdpIfConfAdvertisementLifetime > 9000))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                         "Set to IRDP Conf advertisement lifetime for "
                         "interface %d is failed for bad value \n",
                         i4IrdpIfConfIfNum);

            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIrdpIfConfPerformRouterDiscovery
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                testValIrdpIfConfPerformRouterDiscovery
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IrdpIfConfPerformRouterDiscovery ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIrdpIfConfPerformRouterDiscovery (UINT4 *pu4ErrorCode,
                                             INT4 i4IrdpIfConfIfNum,
                                             INT4 i4IrdpIfConfSubref,
                                             INT4
                                             i4TestValIrdpIfConfPerformRouterDiscovery)
#else
INT1
nmhTestv2FsIrdpIfConfPerformRouterDiscovery (pu4ErrorCode, i4IrdpIfConfIfNum,
                                             i4IrdpIfConfSubref,
                                             i4TestValIrdpIfConfPerformRouterDiscovery)
     UINT4              *pu4ErrorCode;
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     INT4                i4TestValIrdpIfConfPerformRouterDiscovery;
#endif
{
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (i4IrdpIfConfSubref);
    UNUSED_PARAM (i4IrdpIfConfIfNum);

    if ((i4TestValIrdpIfConfPerformRouterDiscovery != IRDP_DONT_DISCOVER) &&
        (i4TestValIrdpIfConfPerformRouterDiscovery != IRDP_DISCOVER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IrdpIfConfIfNum, &u4ContextId)
        == VCM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4ContextId != IP_DEFAULT_CONTEXT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                 "Set to IRDP Conf perform router discovery for interface %d "
                 "is failed for bad value \n", i4IrdpIfConfIfNum);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIrdpIfConfSolicitationAddress
 Input       :  The Indices
                IrdpIfConfIfNum
                IrdpIfConfSubref

                The Object 
                testValIrdpIfConfSolicitationAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IrdpIfConfSolicitationAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIrdpIfConfSolicitationAddress (UINT4 *pu4ErrorCode,
                                          INT4 i4IrdpIfConfIfNum,
                                          INT4 i4IrdpIfConfSubref,
                                          UINT4
                                          u4TestValIrdpIfConfSolicitationAddress)
#else
INT1
nmhTestv2FsIrdpIfConfSolicitationAddress (pu4ErrorCode, i4IrdpIfConfIfNum,
                                          i4IrdpIfConfSubref,
                                          u4TestValIrdpIfConfSolicitationAddress)
     UINT4              *pu4ErrorCode;
     INT4                i4IrdpIfConfIfNum;
     INT4                i4IrdpIfConfSubref;
     UINT4               u4TestValIrdpIfConfSolicitationAddress;
#endif
{
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (i4IrdpIfConfSubref);
    UNUSED_PARAM (i4IrdpIfConfIfNum);

    if ((u4TestValIrdpIfConfSolicitationAddress != IP_GEN_BCAST_ADDR) &&
        (u4TestValIrdpIfConfSolicitationAddress != IRDP_ALL_ROUTERS_MULTI))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4IrdpIfConfIfNum, &u4ContextId)
        == VCM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4ContextId != IP_DEFAULT_CONTEXT)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    IP_TRC_ARG1 (ICMP_MOD_TRC, MGMT_TRC, ICMP_NAME,
                 "Set to IRDP Conf solicitation address for interface %d "
                 "is failed for bad value \n", i4IrdpIfConfIfNum);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIrdpSendAdvertisementsEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIrdpSendAdvertisementsEnable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIrdpIfConfTable
 Input       :  The Indices
                FsIrdpIfConfIfNum
                FsIrdpIfConfSubref
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIrdpIfConfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpAddressTable
 Input       :  The Indices
                FsIpAddrTabAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpAddressTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpRtrLstTable
 Input       :  The Indices
                FsIpRtrLstAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpRtrLstTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
