/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: irdputls.c,v 1.4 2009/08/24 13:11:43 prabuc Exp $
 *
 * Description: Common routines utilised by the IRDP sub modules.
 *
 *******************************************************************/
#include "irdpinc.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
static INT1         i1StaticEntryCount;
static INT1         i1FreeSpace;
static UINT1        u1QuedPktCnt;    /* Queued packets count */

/**************************************************************************/

/*-------------------------------------------------------------------+
 * Function           : IpifGetAllAddr 
 *
 * Input(s)           : u2Iface, pAddrTable
 *
 * Output(s)          : All the IP addresses of the interface.
 *
 * Returns            : None.
 *
 * Should do the following :
 *  
 * + Get all the IP addresses on an interface.
 *
 * NOTE : Currently FutureIP doesnot support assigning multiple IP addresses 
 *        to an interface. When the feature is added to IP this function 
 *        should be changed so that all the IP address are stored in the 
 *        IP address table.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IpifGetAllAddr (UINT2 u2Iface, tIrdpAddrRec * pAddrTable)
#else
EXPORT VOID
IpifGetAllAddr (u2Iface, pAddrTable)
     UINT2               u2Iface;
     tIrdpAddrRec       *pAddrTable;
#endif
{
    UINT4               u4IpAddr;
    UINT4               u4NetMask;
    UINT4               u4IpCount = 0;
    tNetIpv4IfInfo      IpIntfInfo;

    /* Act on Local routes for interface state change */
    if (NetIpv4GetIfInfo (u2Iface, &IpIntfInfo) == NETIPV4_FAILURE)
    {
        return;
    }
    u4IpAddr = IpIntfInfo.u4Addr;

    /* Configure IRDP address table for the interface using the
     * IP addresses configured */
    do
    {
        pAddrTable[u4IpCount++].u4Address = u4IpAddr;
    }
    while (NetIpv4GetNextSecondaryAddress (u2Iface, u4IpAddr,
                                           &u4IpAddr,
                                           &u4NetMask) == CFA_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : IrdpMapIndices
 *
 * Input(s)           : u4RtrAddress, pu2Iface, pu2Index
 *
 * Output(s)          : None.
 *
 * Returns            : Indices that identify the IP Address uniquely.
 *
 * Does the following :
 *  
 * + Maps the Router address to its index into the structure. 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT INT4
IrdpMapIndices (UINT4 u4RtrAddress, UINT2 *pu2Iface, UINT2 *pu2Index)
#else
EXPORT INT4
IrdpMapIndices (u4RtrAddress, pu2Iface, pu2Index)
     UINT4               u4RtrAddress;
     UINT2              *pu2Iface;
     UINT2              *pu2Index;
#endif
{
    UINT2               u2Index;
    UINT2               u2Iface;

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    if (u4RtrAddress != IP_ANY_ADDR)
    {                            /* ^ 0.0.0.0 */
        IPIF_LOGICAL_IFACES_SCAN (u2Iface)
        {
            for (u2Index = 0; u2Index < IPIF_MAX_ADDR_PER_IFACE; u2Index++)
            {
                if (u4RtrAddress == IRDP_GET_ADDRESS (u2Iface, u2Index))
                {
                    *pu2Iface = u2Iface;
                    *pu2Index = u2Index;
                    /* Unlock it */
                    IPFWD_DS_UNLOCK ();
                    return SUCCESS;
                }
            }
        }
    }
    /* Unlock it */
    IPFWD_DS_UNLOCK ();
    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpCheckNeighborHood
 *
 * Input(s)           : u4SrcAddr, u2Iface
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE or FALSE.
 *
 * Does the following :
 *  
 * + Checks if the IP address is one of our neighbours.
 * 
 * NOTE : This function should be changed when support for multiple
 *        IP addresses is added.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT BOOLEAN
IrdpCheckNeighborHood (UINT4 u4SrcAddr, UINT2 u2Iface)
#else
EXPORT BOOLEAN
IrdpCheckNeighborHood (u4SrcAddr, u2Iface)
     UINT4               u4SrcAddr;
     UINT2               u2Iface;
#endif
{
    UINT4               u4CfaIfIndex = 0;

    IPIF_CONFIG_GET_IF_INDEX (u2Iface, u4CfaIfIndex);
    if (CfaIpIfIsLocalNetOnInterface (u4CfaIfIndex, u4SrcAddr) == CFA_SUCCESS)
    {
        return TRUE;
    }
    return FALSE;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpInitRouterList
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 *  + Initializes the Default Router List. 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpInitRouterList (VOID)
#else
EXPORT VOID
IrdpInitRouterList ()
#endif
{
    UINT2               u2Index;

    i1StaticEntryCount = 0;
    i1FreeSpace = IRDP_MAX_DEFAULT_ROUTERS;

    for (u2Index = 0; u2Index < IRDP_MAX_DEFAULT_ROUTERS; u2Index++)
    {
        gaIrdpRtrList[u2Index].u1RowStatus = 0;
        gaIrdpRtrList[u2Index].u2Iface = IRDP_INVALID_IFACE;
    }

}

/*-------------------------------------------------------------------+
 * Function           : IrdpInitQueue
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 * + Initializes the Queue. 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpInitQueue (VOID)
#else
EXPORT VOID
IrdpInitQueue ()
#endif
{
    UINT2               u2Index;

    u1QuedPktCnt = 0;

    for (u2Index = 0; u2Index < IRDP_MAX_Q_SIZE; u2Index++)
    {
        gaIrdpQueue[u2Index].pBuf = NULL;
        gaIrdpQueue[u2Index].u2Iface = IRDP_INVALID_IFACE;
    }
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetStaticCount
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : Number of static entries in the Default Router List.
 *
 * Does the following :
 *  
 * + Returns the number of static entry counts in the Router List. 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT INT1
IrdpGetStaticCount (VOID)
#else
EXPORT INT1
IrdpGetStaticCountInRtrList ()
#endif
{
    return i1StaticEntryCount;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetFreeSpaceInRtrList
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : Free space left in the default router list.
 *
 * Does the following :
 *  
 * + Returns the Free Space left in the Router List. 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT INT1
IrdpGetFreeSpaceInRtrList (VOID)
#else
EXPORT INT1
IrdpGetFreeSpaceInRtrList ()
#endif
{
    return i1FreeSpace;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetFreeSpaceInQueue
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : Free space left in the Queue.
 *
 * Does the following :
 *  
 * + Calculates the free space left in the Queue and returns the value.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT INT1
IrdpGetFreeSpaceInQueue (VOID)
#else
EXPORT INT1
IrdpGetFreeSpaceInQueue ()
#endif
{
    return (INT1) (IRDP_MAX_Q_SIZE - u1QuedPktCnt);
}

/*-------------------------------------------------------------------+
 * Function           : IrdpSearchRtrList
 *
 * Input(s)           : u4RouterAddr
 *
 * Output(s)          : None.
 *
 * Returns            : Index of the row which has u4RouterAddr.
 *
 * Does the following :
 *  
 * 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT UINT2
IrdpSearchRtrList (UINT4 u4RouterAddr)
#else
EXPORT UINT2
IrdpSearchRtrList (u4RouterAddr)
     UINT4               u4RouterAddr;
#endif
{
    UINT2               u2Index;
    UINT1               u1MaxEntries;    /* Total number of entries in the router list */
    UINT2               u2RetVal;

    u2RetVal = IRDP_MAX_DEFAULT_ROUTERS;
    u1MaxEntries = (UINT1) (IRDP_MAX_DEFAULT_ROUTERS - i1FreeSpace);

    for (u2Index = 0; ((u2Index < u1MaxEntries) &&
                       (u2Index < IRDP_MAX_DEFAULT_ROUTERS)); u2Index++)
    {
        if (gaIrdpRtrList[u2Index].u4RouterAddress == u4RouterAddr)
        {
            u2RetVal = u2Index;
        }
    }

    return u2RetVal;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetFreeIndexInRtrList
 *
 * Input(s)           : bEntryType
 *
 * Output(s)          : None.
 *
 * Returns            : An index into the Default Router List.
 *
 * Does the following :
 *  
 * + Returns an index into the default router list, that is free. 
 * + if no free space is left returns IRDP_MAX_DEFAULT_ROUTERS.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT UINT2
IrdpGetFreeIndexInRtrList (BOOLEAN bEntryType)
#else
EXPORT UINT2
IrdpGetFreeIndexInRtrList (bEntryType)
     BOOLEAN             bEntryType;
#endif
{
    UINT2               u2Index = 0;

    u2Index = (UINT2) (IRDP_MAX_DEFAULT_ROUTERS - i1FreeSpace);

    if (u2Index < IRDP_MAX_DEFAULT_ROUTERS)
    {
        i1FreeSpace--;
        if (bEntryType == IRDP_STATIC)
        {
            i1StaticEntryCount++;
        }
    }
    return u2Index;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetFreeIndexInQueue
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : An index into the Queue
 *
 * Does the following :
 *  
 * + Scan the Queue, when pBuf for an entry is NULL return its index. 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT UINT2
IrdpGetFreeIndexInQueue (VOID)
#else
EXPORT UINT2
IrdpGetFreeIndexInQueue ()
#endif
{
    UINT2               u2Index;

    if (u1QuedPktCnt != IRDP_MAX_Q_SIZE)
    {
        for (u2Index = 0; u2Index < IRDP_MAX_Q_SIZE; u2Index++)
        {
            if (gaIrdpQueue[u2Index].pBuf == NULL)
            {
                u1QuedPktCnt++;
                break;
            }
        }
    }
    else
    {
        /* Queue is Full */
        u2Index = IRDP_MAX_Q_SIZE;
    }
    return u2Index;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpDeleteEntryFromRtrList
 *
 * Input(s)           : u2Index
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 * + Delete an entry from the router list 
 * + Move up all the entry to compact the list.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpDeleteEntryFromRtrList (UINT2 u2Index)
#else
EXPORT VOID
IrdpDeleteEntryFromRtrList (u2Index)
     UINT2               u2Index;
#endif
{
    UINT2               u2LastEntry;

    u2LastEntry = (UINT2) (IRDP_MAX_DEFAULT_ROUTERS - (i1FreeSpace + 1));

    if (gaIrdpRtrList[u2Index].bDynamicEntry == IRDP_STATIC)
    {
        i1StaticEntryCount--;
    }

    /*
     * Just moving up all the data.
     * Since Router list would not have more than 10 entries the 
     * following implementation wouldnt be inefficient. 
     * Worst Case : Deleting the 1st Entry, would cause 9 loops.
     */

    for (;
         ((u2Index < u2LastEntry) && (u2Index < IRDP_MAX_DEFAULT_ROUTERS - 1));
         u2Index++)
    {
        gaIrdpRtrList[u2Index].bDynamicEntry =
            gaIrdpRtrList[u2Index + 1].bDynamicEntry;
        gaIrdpRtrList[u2Index].u1RowStatus =
            gaIrdpRtrList[u2Index + 1].u1RowStatus;
        gaIrdpRtrList[u2Index].u2Iface = gaIrdpRtrList[u2Index + 1].u2Iface;
        gaIrdpRtrList[u2Index].u4RouterAddress =
            gaIrdpRtrList[u2Index + 1].u4RouterAddress;
        gaIrdpRtrList[u2Index].i4PrefLevel =
            gaIrdpRtrList[u2Index + 1].i4PrefLevel;
    }

    gaIrdpRtrList[u2Index].u1RowStatus = 0;
    gaIrdpRtrList[u2Index].u4RouterAddress = 0;
    gaIrdpRtrList[u2Index].i4PrefLevel = 0;

    i1FreeSpace++;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpDeleteEntryFromQueue
 *
 * Input(s)           : u2Index
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Does the following :
 *  
 * + Deletes an entry by setting pBuf to NULL. 
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT VOID
IrdpDeleteEntryFromQueue (UINT2 u2Index)
#else
EXPORT VOID
IrdpDeleteEntryFromQueue (u2Index)
     UINT2               u2Index;
#endif
{
    gaIrdpQueue[u2Index].pBuf = NULL;
    gaIrdpQueue[u2Index].u2Iface = IRDP_INVALID_IFACE;
    u1QuedPktCnt--;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetRandom 
 *
 * Input(s)           : u2Iface, u2LeastAllowdValue, u2MaxAllowdValue
 *
 * Output(s)          : None.
 *
 * Returns            : A random value between u2LeastAllowdValue and
 *                      u2MaxAllowdValue.
 *
 * Does the following :
 *  
 * + Generates a random number.
 * + If generated value is not within the range then do some manipulation 
 *   to get it within the specified range.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT UINT4
IrdpGetRandom (UINT2 u2Iface, UINT2 u2LeastAllowdValue, UINT2 u2MaxAllowdValue)
#else
EXPORT UINT4
IrdpGetRandom (u2Iface, u2LeastAllowdValue, u2MaxAllowdValue)
     UINT2               u2Iface;
     UINT2               u2LeastAllowdValue;
     UINT2               u2MaxAllowdValue;
#endif
{
    UINT4               u4Ticks;
    UINT4               u4Rand;

    IRDP_GET_TICK (&u4Ticks);

    u4Rand = IRDP_GET_ADDRESS (u2Iface, 0) & u4Ticks;

    while (u4Rand < u2LeastAllowdValue || u4Rand > u2MaxAllowdValue)
    {
        if (u4Rand < u2LeastAllowdValue)
        {
            u4Rand = u4Rand + u2LeastAllowdValue;
        }
        else if (u4Rand > u2MaxAllowdValue)
        {
            u4Rand = u4Rand - u2MaxAllowdValue;
        }
    }

    return u4Rand;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetLeastPreferedRouter
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : Index to the router list pointing to the least 
 *                      prefered router in the neighborhood.
 *
 * Does the following :
 *  
 * + Scans the Router list and gives the index to a least prefered router.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT UINT2
IrdpGetIndexToLeastPreferedRouter (VOID)
#else
EXPORT UINT2
IrdpGetIndexToLeastPreferedRouter ()
#endif
{
    UINT2               u2IndexToLeastPrefered;
    UINT2               u2Index;
    INT1                i1MaxEntries;

    u2IndexToLeastPrefered = 0;
    i1MaxEntries = (INT1) (IRDP_MAX_DEFAULT_ROUTERS - i1FreeSpace);

    for (u2Index = 0; ((u2Index < i1MaxEntries) &&
                       (u2Index < IRDP_MAX_DEFAULT_ROUTERS)); u2Index++)
    {
        if (gaIrdpRtrList[u2IndexToLeastPrefered].i4PrefLevel >
            gaIrdpRtrList[u2Index].i4PrefLevel)
        {
            u2IndexToLeastPrefered = u2Index;
        }
    }

    return u2IndexToLeastPrefered;
}

/*-------------------------------------------------------------------+
 * Function           : IrdpGetDefaultRoute
 *
 * Input(s)           : pu2Port, pu4Gateway
 *
 * Output(s)          : *pu2Port - Logical interface on which the gateway can be
 *                               reached.
 *                      *pu4Gateway - Default Gateway to which the packet should
 *                                    be forwarded.
 *
 * Returns            : SUCCESS - if a default route is there.
 *                      FAILURE - if one doesnt exist.
 *
 * Does the following :
 *  
 * + Scans the Router list and finds the router which has the highest preference
 *   Level.
 * + Fills the port number and the gateway address and returns SUCCESS if a 
 *   default route is available.
 * + If the list is empty returns FAILURE.
+-------------------------------------------------------------------*/
#ifdef __STDC__
EXPORT INT4
IrdpGetDefaultRoute (UINT2 *pu2Port, UINT4 *pu4Gateway)
#else
EXPORT INT4
IrdpGetDefaultRoute (pu2Port, pu4Gateway)
     UINT2              *pu2Port;
     UINT4              *pu4Gateway;
#endif
{
    UINT2               u2IndexToDefaultRouter;
    UINT2               u2Index;
    UINT1               u1MaxEntries;

    if ((pu2Port == NULL) || (pu4Gateway == NULL))
    {
        return FAILURE;
    }

    u2IndexToDefaultRouter = 0;
    u1MaxEntries = (UINT1) (IRDP_MAX_DEFAULT_ROUTERS - (UINT1) i1FreeSpace);

    if (u1MaxEntries != 0)
    {
        for (u2Index = 0; ((u2Index < u1MaxEntries) &&
                           (u2Index < IRDP_MAX_DEFAULT_ROUTERS)); u2Index++)
        {
            if (gaIrdpRtrList[u2IndexToDefaultRouter].i4PrefLevel <
                gaIrdpRtrList[u2Index].i4PrefLevel)
            {
                u2IndexToDefaultRouter = u2Index;
            }
        }

        *pu2Port = gaIrdpRtrList[u2IndexToDefaultRouter].u2Iface;
        *pu4Gateway = gaIrdpRtrList[u2IndexToDefaultRouter].u4RouterAddress;

        if ((*pu2Port == IPIF_INVALID_INDEX) || (*pu4Gateway == 0))
        {
            return FAILURE;
        }
        return SUCCESS;
    }
    else
    {
        return FAILURE;
    }
}
