/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udptrcrt.h,v 1.4 2009/08/24 13:11:46 prabuc Exp $
 *
 * Description: Trace utility header file.
 *
 *******************************************************************/
#ifndef   __IP_TRACE_H__
#define   __IP_TRACE_H__

/* This macro finds the probe number for a HOP
 * Input is number of request. There are TRACE_MAX_PROBES number of request
 * per HOP so probe number for HOP will be modulus of this constant.
 */
#define   TRACE_PROBE_NO(i2Req_no)   (i2Req_no % TRACE_MAX_PROBES)

/*
 * Given the number of trial this macro returns which HOP it belonged to.
 * Validity check should be done outside.
 */
#define   TRACE_HOP_NO(i2Req_no)   ((i2Req_no / TRACE_MAX_PROBES) + 1)

#define   TRACE_MIN_TTL              1
#define   TRACE_MAX_TTL             99
#define   TRACE_DEF_TTL             15

#define   TRACE_DEF_TIMEOUT          1
#define   TRACE_MIN_TIMEOUT          1
#define   TRACE_MAX_TIMEOUT        100

#define   IP_TRACE_TIMER_ID        101
#define   IP_TRACE_END_TIMER_ID    102

/* This is the macro to find end timeout for an entry */
#define   TRACE_ADMIN_DISABLE             2
#define   TRACE_ADMIN_ENABLE              1

#define   TRACE_OPER_IN_PROGRESS          1
#define   TRACE_OPER_NOT_IN_PROGRESS      2

#define   TRACE_MIN_MTU                   1
#define   TRACE_MAX_MTU                 100
#define   TRACE_MAX_PROBES                3

#define   TRACE_END_TIME(pTrace)   (4 * (pTrace)->i2Timeout)
#define   TRACE_DEF_MTU            TRACE_MIN_MTU

/*
 * The per destination trace info/config record.
 * Currently single structure is maintained but can be extended to allocate
 * each time a trace is to be sent.
 */
typedef struct
{
    t_IP_TIMER  Timer;
    tUDP_SLL    Info_list;     /* Per hop info list on the way this 
                                * destination 
                                */
    UINT4       u4Dest;        /* Destination address to which trace is sent */
    UINT4       u4Start_time;  /* Time at which the request is sent */
    INT2        i2Timeout;     /* Timeout to be used for tracing */
    INT2        i2Max_ttl;     /* Number of hops to look for */
    INT2        i2Min_ttl;     /* Number of hops to start with */
    INT2        i2Mtu;         /* Size of the data to be sent with trace */
    INT2        i2Id;          /* Used internally to match response to 
                                * requeest 
                                */
    INT2        i2Cur_hop;
    INT2        i2Start_id;    /* Starting IP ID used for this trace */
    INT2        i2Cur_probe;   /* Current attempt number for a ttl */
    INT1        i1Admin;       /* Start or Stop the Trace operation */
    INT1        i1Oper;        /* Indicates if the trace is in progress */
    INT2        i2Align;       /* FOr 4 Byte Word Align */
} t_TRACE;

typedef struct
{
    tUDP_SLL_NODE  Link;
    UINT4          u4Gw;
    INT2           i2Time[TRACE_MAX_PROBES + 1];
} t_TRACE_INFO;

#define   UDP_PORT_TRACE    0x53

#endif          /*__IP_TRACE_H__*/
