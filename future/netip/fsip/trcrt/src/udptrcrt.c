/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udptrcrt.c,v 1.9 2014/01/20 12:15:44 siva Exp $
 *
 * Description:Trace Route Facility supported as an UDP     
 *             Application. The application sits at a       
 *             designated UDP port which is configurable.   
 *
 *******************************************************************/
#include "trcrtinc.h"
#include "fsiplow.h"

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/
static t_TRACE     *trace_creat PROTO ((UINT4 u4Dest));
static VOID trace_delete PROTO ((t_TRACE * pTrace));
static t_TRACE     *trace_get_entry PROTO ((UINT4 u4Dest));
static t_TRACE     *trace_record PROTO ((UINT4 u4Trace_dest, UINT4 u4Src,
                                         INT2 i2Id, INT2 *i2pTry_no));
static INT4 trace_last_hop_reply PROTO ((UINT4 u4Trace_dest, UINT4 u4Src,
                                         INT2 i2Id));
static VOID trace_reset_rec PROTO ((t_TRACE * pTrace));
static INT4 tr_task_udp_open PROTO ((UINT2 u2Port, UINT4 u4Addr));

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/

/*
 * Trace id is assigned as IP id. Each packets will have their own id
 * and a block is allocated per trace process.
 */
static INT2         i2Trace_id = 0;

/*
 * Local port used by TRACE
 */
static UINT2        u2Trace_local_port = UDP_PORT_TRACE;

/*
 * Structure used for Trace Route.
 */
static t_TRACE      Trace;

/**************************************************************************/

/* Initialize the trace table */
#ifdef __STDC__

static t_TRACE     *
trace_creat (UINT4 u4Dest)
#else

static t_TRACE     *
trace_creat (u4Dest)
     UINT4               u4Dest;
#endif
{
    t_TRACE            *pTrace;

    if (Trace.u4Dest != u4Dest && Trace.u4Dest != 0)
    {
        return NULL;
    }

    pTrace = &Trace;

    pTrace->Timer.u1Id = IP_TRACE_TIMER_ID;
    pTrace->Timer.pArg = (VOID *) pTrace;
    return pTrace;
}

/**** $$TRACE_PROCEDURE_NAME  = trace_delete   ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
#ifdef __STDC__

static VOID
trace_delete (t_TRACE * pTrace)
#else

static VOID
trace_delete (pTrace)
     t_TRACE            *pTrace;
#endif
{
    if (pTrace->i1Oper == TRACE_OPER_IN_PROGRESS)
    {
        UDP_STOP_TIMER (gUdpGblInfo.UdpIcmpTimerListId,
                        &(pTrace->Timer.Timer_node));
    }

/* Will be updated while doing TRCRT changes 
    trtask_udp_close (u2Trace_local_port, IP_ANY_ADDR); */
    trace_reset_rec (&Trace);

    if (i2Trace_id > 32760)        /* Just to avoid overflow half way through */
        i2Trace_id = 0;            /* Replaced 60000 by 32760 */

}

/**** $$TRACE_PROCEDURE_NAME  = trace_get_entry   ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
#ifdef __STDC__

static t_TRACE     *
trace_get_entry (UINT4 u4Dest)
#else

static t_TRACE     *
trace_get_entry (u4Dest)
     UINT4               u4Dest;
#endif
{
    if (u4Dest == Trace.u4Dest)
        return &Trace;

    return NULL;
}

/*
 * The Trace table contains following entries
 * INDEX u4Dest;          ** Destination address to which trace is sent     **
 * RW    i1Admin;         ** Start or Stop the Trace operation              **
 * RW    i2Timeout;       ** Timeout to be used for traceing                **
 * RW    i2Max_ttl;       ** Maximum distance of this destination in ttl    **
 * RW    i2Min_ttl;       ** Minimum ttl to start the trace                 **
 * RO    i1Oper;          ** Indicates if the trace is in progress          **
 * RW    i2Mtu;           ** Size of the data to be sent with trace         **
 *
 * Currently there is only one entry structure and user will have to delete
 * previous trace entry before doing a new trace. (Otherwise the set routine
 * will fail.
 */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpTraceConfigTable
 Input       :  The Indices
                IpTraceConfigDest
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIpTraceConfigTable (UINT4 u4IpTraceConfigDest)
#else
INT1
nmhValidateIndexInstanceFsIpTraceConfigTable (u4IpTraceConfigDest)
     UINT4               u4IpTraceConfigDest;
#endif
{
    if (IP_IS_ADDR_CLASS_D (u4IpTraceConfigDest) ||
        IP_IS_ADDR_CLASS_E (u4IpTraceConfigDest))
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpTraceConfigTable
 Input       :  The Indices
                IpTraceConfigDest
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpTraceConfigTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIpTraceConfigTable (UINT4 *pu4IpTraceConfigDest)
#else
INT1
nmhGetFirstIndexFsIpTraceConfigTable (pu4IpTraceConfigDest)
     UINT4              *pu4IpTraceConfigDest;
#endif
{
    if (nmhGetNextIndexFsIpTraceConfigTable (0, pu4IpTraceConfigDest)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpTraceConfigTable
 Input       :  The Indices
                IpTraceConfigDest
                nextIpTraceConfigDest
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpTraceConfigTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIpTraceConfigTable (UINT4 u4IpTraceConfigDest,
                                     UINT4 *pu4NextIpTraceConfigDest)
#else
INT1
nmhGetNextIndexFsIpTraceConfigTable (u4IpTraceConfigDest,
                                     pu4NextIpTraceConfigDest)
     UINT4               u4IpTraceConfigDest;
     UINT4              *pu4NextIpTraceConfigDest;
#endif
{
    /* scan here if many entries are there */
    if (Trace.u4Dest > u4IpTraceConfigDest)
    {
        *pu4NextIpTraceConfigDest = Trace.u4Dest;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Admin Status */
/****************************************************************************
 Function    :  nmhGetFsIpTraceConfigAdminStatus
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                retValIpTraceConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceConfigAdminStatus ***/
/** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceConfigAdminStatus (UINT4 u4IpTraceConfigDest,
                                  INT4 *pi4RetValIpTraceConfigAdminStatus)
#else
INT1
nmhGetFsIpTraceConfigAdminStatus (u4IpTraceConfigDest,
                                  pi4RetValIpTraceConfigAdminStatus)
     UINT4               u4IpTraceConfigDest;
     INT4               *pi4RetValIpTraceConfigAdminStatus;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) == NULL)
        return SNMP_FAILURE;

    *pi4RetValIpTraceConfigAdminStatus = (INT4) pTrace->i1Admin;
    return SNMP_SUCCESS;
}

/***************************************************************************
 Function    :  nmhTestv2FsIpTraceConfigAdminStatus
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                testValIpTraceConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpTraceConfigAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpTraceConfigAdminStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4IpTraceConfigDest,
                                     INT4 i4TestValIpTraceConfigAdminStatus)
#else
INT1                nmhTestv2FsIpTraceConfigAdminStatus (pu4ErrorCode,
                                                         u4IpTraceConfigDest,
                                                         UINT4 *pu4ErrorCode;
                                                         i4TestValIpTraceConfigAdminStatus)
     UINT4               u4IpTraceConfigDest;
     INT4                i4TestValIpTraceConfigAdminStatus;
#endif
{
    t_TRACE            *pTrace;

    pTrace = trace_get_entry (u4IpTraceConfigDest);

    if (i4TestValIpTraceConfigAdminStatus == TRACE_ADMIN_DISABLE)
    {
        if (pTrace != NULL)
            return SNMP_SUCCESS;
    }
    else
    {
        if (i4TestValIpTraceConfigAdminStatus == TRACE_ADMIN_ENABLE)
        {
            if (pTrace != NULL)
                if (pTrace->i1Oper == TRACE_OPER_IN_PROGRESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                                 "Set to Trace config Admin status to %x "
                                 "failed - Trace operation in progress \n",
                                 u4IpTraceConfigDest);

                    return SNMP_FAILURE;
                }
            return SNMP_SUCCESS;    /* In progress dont allow modifications */
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Set to Trace config Admin status to %x failed - Bad value \n",
                 u4IpTraceConfigDest);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpTraceConfigAdminStatus
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                setValIpTraceConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpTraceConfigAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpTraceConfigAdminStatus (UINT4 u4IpTraceConfigDest,
                                  INT4 i4SetValIpTraceConfigAdminStatus)
#else
INT1
nmhSetFsIpTraceConfigAdminStatus (u4IpTraceConfigDest,
                                  i4SetValIpTraceConfigAdminStatus)
     UINT4               u4IpTraceConfigDest;
     INT4                i4SetValIpTraceConfigAdminStatus;

#endif
{
    t_TRACE            *pTrace;

    pTrace = trace_get_entry (u4IpTraceConfigDest);

    if (i4SetValIpTraceConfigAdminStatus == TRACE_ADMIN_DISABLE)
    {
        if (pTrace != NULL)
        {
            trace_delete (pTrace);
            IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                         "Trace entry to Dest - %x is Deleted \n",
                         u4IpTraceConfigDest);
        }
    }
    else
    {
        if (pTrace == NULL)
        {
            if ((pTrace = trace_creat (u4IpTraceConfigDest)) != NULL)
            {
                pTrace->u4Dest = u4IpTraceConfigDest;
                pTrace->i1Admin = (INT1) i4SetValIpTraceConfigAdminStatus;
                UDP_START_TIMER (gUdpGblInfo.UdpIcmpTimerListId,
                                 &(pTrace->Timer.Timer_node),
                                 pTrace->i2Timeout);

                IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                             "Trace operation to - Dest %x is started \n",
                             u4IpTraceConfigDest);

            }
            else
            {
                return SNMP_FAILURE;
            }
        }

        pTrace->i1Admin = (INT1) i4SetValIpTraceConfigAdminStatus;
        /* Open the local port now */
        if (tr_task_udp_open (u2Trace_local_port, IP_ANY_ADDR) == IP_FAILURE)
        {
            IP_TRC_ARG1 (UDP_MOD_TRC, ALL_FAILURE_TRC, UDP_NAME,
                         "Trace route-not able to open the local port at %d \n",
                         u2Trace_local_port);
            return SNMP_FAILURE;
        }

        pTrace->i2Start_id = (INT2) (i2Trace_id + 1);
        pTrace->i2Id = i2Trace_id;
        i2Trace_id =
            (INT2) (i2Trace_id + (pTrace->i2Max_ttl * TRACE_MAX_PROBES));
        pTrace->i1Oper = TRACE_OPER_IN_PROGRESS;
        OsixGetSysTime (&pTrace->u4Start_time);

        trace_send (pTrace);

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceConfigTimeout
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                retValIpTraceConfigTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceConfigTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceConfigTimeout (UINT4 u4IpTraceConfigDest,
                              INT4 *pi4RetValIpTraceConfigTimeout)
#else
INT1
nmhGetFsIpTraceConfigTimeout (u4IpTraceConfigDest,
                              pi4RetValIpTraceConfigTimeout)
     UINT4               u4IpTraceConfigDest;
     INT4               *pi4RetValIpTraceConfigTimeout;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        *pi4RetValIpTraceConfigTimeout = (INT4) pTrace->i2Timeout;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/***************************************************************************
 Function    :  nmhTestv2FsIpTraceConfigTimeout
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                testValIpTraceConfigTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpTraceConfigTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpTraceConfigTimeout (UINT4 *pu4ErrorCode,
                                 UINT4 u4IpTraceConfigDest,
                                 INT4 i4TestValIpTraceConfigTimeout)
#else
INT1                nmhTestv2FsIpTraceConfigTimeout (pu4ErrorCode,
                                                     u4IpTraceConfigDest,
                                                     UINT4 *pu4ErrorCode;
                                                     i4TestValIpTraceConfigTimeout)
     UINT4               u4IpTraceConfigDest;
     INT4                i4TestValIpTraceConfigTimeout;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
        if (pTrace->i1Oper == TRACE_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

            IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                         "Set to Trace config timeout interval for Dest %x "
                         "failed - Operation in progress \n",
                         u4IpTraceConfigDest);

            return SNMP_FAILURE;    /* In progress dont allow modifications */
        }

    if (i4TestValIpTraceConfigTimeout >= TRACE_MIN_TIMEOUT &&
        i4TestValIpTraceConfigTimeout <= TRACE_MAX_TIMEOUT)
        return SNMP_SUCCESS;

    IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Set to Trace config timeout interval for Dest %x failed - "
                 "Bad value \n", u4IpTraceConfigDest);

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpTraceConfigTimeout
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                setValIpTraceConfigTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpTraceConfigTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpTraceConfigTimeout (UINT4 u4IpTraceConfigDest,
                              INT4 i4SetValIpTraceConfigTimeout)
#else
INT1
nmhSetFsIpTraceConfigTimeout (u4IpTraceConfigDest, i4SetValIpTraceConfigTimeout)
     UINT4               u4IpTraceConfigDest;
     INT4                i4SetValIpTraceConfigTimeout;

#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
        pTrace->i2Timeout = (INT2) i4SetValIpTraceConfigTimeout;
    else
    {
        if ((pTrace = trace_creat (u4IpTraceConfigDest)) != NULL)
        {
            pTrace->u4Dest = u4IpTraceConfigDest;
            pTrace->i2Timeout = (INT2) i4SetValIpTraceConfigTimeout;
        }
        else
            return SNMP_FAILURE;
    }

    IP_TRC_ARG2 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Trace route timeout is set to %d for Destination %x \n",
                 i4SetValIpTraceConfigTimeout, u4IpTraceConfigDest);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceConfigMinTTL
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                retValIpTraceConfigMinTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceConfigMinTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceConfigMinTTL (UINT4 u4IpTraceConfigDest,
                             INT4 *pi4RetValIpTraceConfigMinTTL)
#else
INT1
nmhGetFsIpTraceConfigMinTTL (u4IpTraceConfigDest, pi4RetValIpTraceConfigMinTTL)
     UINT4               u4IpTraceConfigDest;
     INT4               *pi4RetValIpTraceConfigMinTTL;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        *pi4RetValIpTraceConfigMinTTL = (INT4) pTrace->i2Min_ttl;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpTraceConfigMinTTL
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                testValIpTraceConfigMinTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpTraceConfigMinTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpTraceConfigMinTTL (UINT4 *pu4ErrorCode, UINT4 u4IpTraceConfigDest,
                                INT4 i4TestValIpTraceConfigMinTTL)
#else
INT1                nmhTestv2FsIpTraceConfigMinTTL (pu4ErrorCode,
                                                    u4IpTraceConfigDest,
                                                    UINT4 *pu4ErrorCode;
                                                    i4TestValIpTraceConfigMinTTL)
     UINT4               u4IpTraceConfigDest;
     INT4                i4TestValIpTraceConfigMinTTL;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        if (pTrace->i1Oper == TRACE_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

            IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                         "Set to Trace config Minimum TTL for Dest %x failed- "
                         "Operation in progress \n", u4IpTraceConfigDest);
            return SNMP_FAILURE;    /* In progress dont allow modifications */
        }

        if (i4TestValIpTraceConfigMinTTL >= pTrace->i2Max_ttl)
        {
            IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                         "Set to Trace config minimum TTL for Dest %x failed - "
                         "Bad value \n", u4IpTraceConfigDest);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (i4TestValIpTraceConfigMinTTL >= TRACE_MIN_TTL &&
        i4TestValIpTraceConfigMinTTL <= TRACE_MAX_TTL)
        return SNMP_SUCCESS;

    IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Set to Trace config minimum TTL for Dest %x failed - "
                 "Bad value \n", u4IpTraceConfigDest);

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpTraceConfigMinTTL
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                setValIpTraceConfigMinTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpTraceConfigMinTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpTraceConfigMinTTL (UINT4 u4IpTraceConfigDest,
                             INT4 i4SetValIpTraceConfigMinTTL)
#else
INT1
nmhSetFsIpTraceConfigMinTTL (u4IpTraceConfigDest, i4SetValIpTraceConfigMinTTL)
     UINT4               u4IpTraceConfigDest;
     INT4                i4SetValIpTraceConfigMinTTL;

#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {

        if (i4SetValIpTraceConfigMinTTL < pTrace->i2Max_ttl)
            pTrace->i2Min_ttl = (INT2) i4SetValIpTraceConfigMinTTL;
        else
            return SNMP_FAILURE;
    }
    else
    {
        if ((pTrace = trace_creat (u4IpTraceConfigDest)) != NULL)
        {
            pTrace->u4Dest = u4IpTraceConfigDest;
            pTrace->i2Min_ttl = (INT2) i4SetValIpTraceConfigMinTTL;
        }
        else
            return SNMP_FAILURE;
    }
    IP_TRC_ARG2 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Trace route min TTL is set to %d for Destination %x \n",
                 i4SetValIpTraceConfigMinTTL, u4IpTraceConfigDest);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceConfigMaxTTL
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                retValIpTraceConfigMaxTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceConfigMaxTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceConfigMaxTTL (UINT4 u4IpTraceConfigDest,
                             INT4 *pi4RetValIpTraceConfigMaxTTL)
#else
INT1
nmhGetFsIpTraceConfigMaxTTL (u4IpTraceConfigDest, pi4RetValIpTraceConfigMaxTTL)
     UINT4               u4IpTraceConfigDest;
     INT4               *pi4RetValIpTraceConfigMaxTTL;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        *pi4RetValIpTraceConfigMaxTTL = (INT4) pTrace->i2Max_ttl;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpTraceConfigMaxTTL
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                testValIpTraceConfigMaxTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpTraceConfigMaxTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpTraceConfigMaxTTL (UINT4 *pu4ErrorCode, UINT4 u4IpTraceConfigDest,
                                INT4 i4TestValIpTraceConfigMaxTTL)
#else
INT1                nmhTestv2FsIpTraceConfigMaxTTL (pu4ErrorCode,
                                                    u4IpTraceConfigDest,
                                                    UINT4 *pu4ErrorCode;
                                                    i4TestValIpTraceConfigMaxTTL)
     UINT4               u4IpTraceConfigDest;
     INT4                i4TestValIpTraceConfigMaxTTL;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        if (pTrace->i1Oper == TRACE_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

            IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                         "Set to Trace config Maximum TTL for Dest %x failed - "
                         "Operation in progress \n", u4IpTraceConfigDest);
            return SNMP_FAILURE;    /* In progress dont allow modifications */
        }
        if (i4TestValIpTraceConfigMaxTTL < pTrace->i2Min_ttl)
        {
            IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                         "Set to Trace config Maximum TTL for Dest %x failed - "
                         "Bad value \n", u4IpTraceConfigDest);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (i4TestValIpTraceConfigMaxTTL >= TRACE_MIN_TTL &&
        i4TestValIpTraceConfigMaxTTL <= TRACE_MAX_TTL)
        return SNMP_SUCCESS;

    IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Set to Trace config Maximum TTL for Dest %x failed - "
                 "Bad value \n", u4IpTraceConfigDest);

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpTraceConfigMaxTTL
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                setValIpTraceConfigMaxTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpTraceConfigMaxTTL ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpTraceConfigMaxTTL (UINT4 u4IpTraceConfigDest,
                             INT4 i4SetValIpTraceConfigMaxTTL)
#else
INT1
nmhSetFsIpTraceConfigMaxTTL (u4IpTraceConfigDest, i4SetValIpTraceConfigMaxTTL)
     UINT4               u4IpTraceConfigDest;
     INT4                i4SetValIpTraceConfigMaxTTL;

#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        if (i4SetValIpTraceConfigMaxTTL > pTrace->i2Min_ttl)
            pTrace->i2Max_ttl = (INT2) i4SetValIpTraceConfigMaxTTL;
        else
            return SNMP_FAILURE;
    }
    else
    {
        if ((pTrace = trace_creat (u4IpTraceConfigDest)) != NULL)
        {
            pTrace->u4Dest = u4IpTraceConfigDest;
            pTrace->i2Max_ttl = (INT2) i4SetValIpTraceConfigMaxTTL;
        }
        else
            return SNMP_FAILURE;
    }

    IP_TRC_ARG2 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Trace route Max TTL is set to %d for Destination %x \n",
                 i4SetValIpTraceConfigMaxTTL, u4IpTraceConfigDest);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceConfigOperStatus
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                retValIpTraceConfigOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceConfigOperStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceConfigOperStatus (UINT4 u4IpTraceConfigDest,
                                 INT4 *pi4RetValIpTraceConfigOperStatus)
#else
INT1
nmhGetFsIpTraceConfigOperStatus (u4IpTraceConfigDest,
                                 pi4RetValIpTraceConfigOperStatus)
     UINT4               u4IpTraceConfigDest;
     INT4               *pi4RetValIpTraceConfigOperStatus;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        *pi4RetValIpTraceConfigOperStatus = (INT4) pTrace->i1Oper;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceConfigMtu
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                retValIpTraceConfigMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceConfigMtu ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceConfigMtu (UINT4 u4IpTraceConfigDest,
                          INT4 *pi4RetValIpTraceConfigMtu)
#else
INT1
nmhGetFsIpTraceConfigMtu (u4IpTraceConfigDest, pi4RetValIpTraceConfigMtu)
     UINT4               u4IpTraceConfigDest;
     INT4               *pi4RetValIpTraceConfigMtu;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        *pi4RetValIpTraceConfigMtu = (INT4) pTrace->i2Mtu;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpTraceConfigMtu
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                testValIpTraceConfigMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpTraceConfigMtu ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpTraceConfigMtu (UINT4 *pu4ErrorCode, UINT4 u4IpTraceConfigDest,
                             INT4 i4TestValIpTraceConfigMtu)
#else
INT1                nmhTestv2FsIpTraceConfigMtu (pu4ErrorCode,
                                                 u4IpTraceConfigDest,
                                                 UINT4 *pu4ErrorCode;
                                                 i4TestValIpTraceConfigMtu)
     UINT4               u4IpTraceConfigDest;
     INT4                i4TestValIpTraceConfigMtu;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
        if (pTrace->i1Oper == TRACE_OPER_IN_PROGRESS)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

            IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                         "Set to Trace config MTU for Dest %x failed - "
                         "Operation in progress \n", u4IpTraceConfigDest);
            return SNMP_FAILURE;    /* In progress dont allow modifications */
        }

    if (i4TestValIpTraceConfigMtu >= TRACE_MIN_MTU &&
        i4TestValIpTraceConfigMtu <= TRACE_MAX_MTU)
        return SNMP_SUCCESS;

    IP_TRC_ARG1 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Set to Trace config MTU for Dest %x failed - Bad value \n",
                 u4IpTraceConfigDest);

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpTraceConfigMtu
 Input       :  The Indices
                IpTraceConfigDest

                The Object
                setValIpTraceConfigMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpTraceConfigMtu ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpTraceConfigMtu (UINT4 u4IpTraceConfigDest,
                          INT4 i4SetValIpTraceConfigMtu)
#else
INT1
nmhSetFsIpTraceConfigMtu (u4IpTraceConfigDest, i4SetValIpTraceConfigMtu)
     UINT4               u4IpTraceConfigDest;
     INT4                i4SetValIpTraceConfigMtu;

#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceConfigDest)) != NULL)
    {
        pTrace->i2Mtu = (INT2) i4SetValIpTraceConfigMtu;
    }
    else
    {
        if ((pTrace = trace_creat (u4IpTraceConfigDest)) != NULL)
        {
            pTrace->u4Dest = u4IpTraceConfigDest;
            pTrace->i2Mtu = (INT2) i4SetValIpTraceConfigMtu;
        }
        else
            return SNMP_FAILURE;
    }

    IP_TRC_ARG2 (UDP_MOD_TRC, MGMT_TRC, UDP_NAME,
                 "Trace route MTU is set to %d for Destination %x \n",
                 i4SetValIpTraceConfigMtu, u4IpTraceConfigDest);
    return SNMP_SUCCESS;
}

/* Timeout Routine to be called for TRACE_TIMER_ID */
/**** $$TRACE_PROCEDURE_NAME  = trace_send   ****/
/*** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
#ifdef __STDC__

INT4
trace_send (t_TRACE * pTrace)
#else

INT4
trace_send (pTrace)
     t_TRACE            *pTrace;
#endif
{
    UNUSED_PARAM (pTrace);
    return SNMP_SUCCESS;
}

/*
 * For trace the only possible data that can arrive is the echo response from
 * the final echo server. So end out trace here.
 */
/**** $$TRACE_PROCEDURE_NAME  = trace_input   ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/

#ifdef __STDC__

INT4
trace_input (UINT2 u2Port, tCRU_BUF_CHAIN_HEADER * pBuf, INT2 i2Len,
             UINT4 u4Source, UINT2 u2Identification)
#else

INT4
trace_input (u2Port, pBuf, i2Len, u4Source, u2Identification)
     UINT2               u2Port;
     tCRU_BUF_CHAIN_HEADER *pBuf;
     INT2                i2Len;
     UINT4               u4Source;
     UINT2               u2Identification;

#endif
{

    if (u2Port > IPIF_MAX_LOGICAL_IFACES)
    {
        return SNMP_FAILURE;
    }

    /* Makefile Changes - unused parameters */
    UNUSED_PARAM (i2Len);

    IP_RELEASE_BUF (pBuf, FALSE);
    trace_last_hop_reply (u4Source, u4Source, (INT2) u2Identification);

    IP_TRC_ARG1 (UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                 "Trace route got reply from %x \n", u4Source);

    return SNMP_SUCCESS;
}

/*
 * Receive ICMP error message for the packet that we have sent.
 * The routine records the info if time exceed message is received.
 * If port unreachable message is obtained then last hop statistics are
 * recorded.  No action is taken for all other messages.
 */
/**** $$TRACE_PROCEDURE_NAME  =  trace_error  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
#ifdef __STDC__

INT4
trace_error (tCRU_BUF_CHAIN_HEADER * pBuf, INT1 i1Type, INT1 i1Code,
             UINT4 u4Source)
#else

INT4
trace_error (pBuf, i1Type, i1Code, u4Source)
     tCRU_BUF_CHAIN_HEADER *pBuf;
     INT1                i1Type;
     INT1                i1Code;
     UINT4               u4Source;
#endif
{
    UINT4               u4Trace_dest;
    UINT2               u2Identification;

    /* Get desired fields from IP header of the packet */

    IP_PKT_GET_DEST (pBuf, u4Trace_dest);
    IP_PKT_GET_ID (pBuf, u2Identification);

    IP_RELEASE_BUF (pBuf, FALSE);
    switch (i1Type)
    {
        case ICMP_TIME_EXCEED:
        {
            /* The intermediate node timed out pkt according to ttl */
            if (trace_record
                (u4Trace_dest, u4Source, (INT2) u2Identification, NULL) != NULL)
            {
                IP_TRC_ARG2 (UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                             "Trace Route got ICMP Time exceeded message "
                             "from %x for Destination %x \n",
                             u4Source, u4Trace_dest);
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;
        }
        case ICMP_DEST_UNREACH:
        {
            /* Probably the echo port is not active on the other host */
            if (i1Code == ICMP_PORT_UNREACH)
            {
                IP_TRC_ARG1 (UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                             "Trace route got ICMP Port unreachable from "
                             "Destination %x \n", u4Source);

                return (trace_last_hop_reply
                        (u4Trace_dest, u4Source, (INT2) u2Identification));
            }
            return SNMP_FAILURE;
        }
        default:
            break;
    }
    return SNMP_FAILURE;
}

/*
 * This procedure takes in source and id. Then it finds out the
 * proper record to update and updates it.
 */

/**** $$TRACE_PROCEDURE_NAME  = trace_record   ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/

#ifdef __STDC__

static t_TRACE     *
trace_record (UINT4 u4Trace_dest, UINT4 u4Src, INT2 i2Id, INT2 *i2pTry_no)
#else

static t_TRACE     *
trace_record (u4Trace_dest, u4Src, i2Id, i2pTry_no)
     UINT4               u4Trace_dest;
     UINT4               u4Src;
     INT2                i2Id;
     INT2               *i2pTry_no;
#endif
{
    t_TRACE            *pTrace;
    t_TRACE_INFO       *pTrace_info;
    INT2                i2Rep_no;
    UINT4               u4SysTime = 0;

    /* We got a reply from intermediate gateway saying that ttl is 0 */

    if ((pTrace = trace_get_entry (u4Trace_dest)) == NULL)
        return NULL;

    /* Reject any old replies */
    if (pTrace->i1Oper == TRACE_OPER_NOT_IN_PROGRESS)
    {
        return NULL;
    }

    /* It should be for one of our IDs */
    if (i2Id > pTrace->i2Id || i2Id < pTrace->i2Start_id)
    {
        return NULL;
    }

    i2Rep_no = (INT2) (i2Id - pTrace->i2Start_id);
    /* Which of our try got reply ? */

    pTrace_info = (t_TRACE_INFO *) UDP_SLL_Nth (&(pTrace->Info_list), (UINT4)
                                                TRACE_HOP_NO (i2Rep_no));
    if (pTrace_info == NULL)
    {
        return NULL;
    }

    if (i2Rep_no == 0)
        OsixGetSysTime (&u4SysTime);

    pTrace_info->i2Time[TRACE_PROBE_NO (i2Rep_no)] = (INT1) (u4SysTime -
                                                             ((pTrace)->
                                                              u4Start_time +
                                                              (UINT4) ((pTrace)->i2Timeout * (i2Rep_no - 1))));

    pTrace_info->u4Gw = u4Src;    /* Record this address */

    if (i2pTry_no != NULL)
        *i2pTry_no = i2Rep_no;

    return pTrace;
}

/*
 * This procedure is called whenever a reply is received from the
 * final destination.
 * There are two ways by which we can decide that it is the last hop
 * 1. When we receive response for the packet we sent to ECHO port of
 *    the destination.
 * 2. We get a PORT_UNREACHABLE icmp error message from final host.
 *
 * Both the cases the trace end timer is activated if one is not set.
 * Enough time is allowed for all three probes to complete.
 * If there are any nodes after this hop they will be removed and
 * reguler timer will be unlinked.
 */
/**** $$TRACE_PROCEDURE_NAME  = trace_last_hop_reply   ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
#ifdef __STDC__

static INT4
trace_last_hop_reply (UINT4 u4Trace_dest, UINT4 u4Src,    /* Source that sent the reply to our message */
                      INT2 i2Id    /* Identifier field in IP header             */
    )
#else

static INT4
trace_last_hop_reply (u4Trace_dest, u4Src, i2Id)
     UINT4               u4Trace_dest;
     UINT4               u4Src;    /* Source that sent the reply to our message */
     INT2                i2Id;    /* Identifier field in IP header             */
#endif
{
    INT2                i2Rep_no;
    t_TRACE            *pTrace;
    t_TRACE_INFO       *pTrace_info;
    INT4                i;

    if ((pTrace = trace_record (u4Trace_dest, u4Src, i2Id, &i2Rep_no)) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pTrace->i2Cur_hop > TRACE_HOP_NO (i2Rep_no))
    {
        /* We have already gone beyond this node. Let us be happy with
         * whatever number of replies we got.
         * Stop trace process here.
         */

        UDP_STOP_TIMER (gUdpGblInfo.UdpIcmpTimerListId,
                        &(pTrace->Timer.Timer_node));
        pTrace->i1Oper = TRACE_OPER_NOT_IN_PROGRESS;
        /* Remove all the invalid nodes */
        for (i = TRACE_HOP_NO (i2Rep_no) + 1; i <= pTrace->i2Cur_hop; i++)
        {
            pTrace_info =
                (t_TRACE_INFO *) UDP_SLL_Nth (&(pTrace->Info_list), (UINT4) i);
            UDP_SLL_Delete (&(pTrace->Info_list), &pTrace_info->Link);
            TRACE_INFO_FREE (pTrace_info);
        }

        /* This should not be necessary */
        pTrace->i2Cur_probe = (INT2) TRACE_MAX_PROBES;
        pTrace->i2Cur_hop = (INT2) (TRACE_HOP_NO (i2Rep_no));
        return SNMP_SUCCESS;
    }
    if ((pTrace->i2Cur_hop == TRACE_HOP_NO (i2Rep_no) &&
         pTrace->i2Cur_probe == TRACE_MAX_PROBES - 1))
    {
        /* There is some reply after our last try to final destination.
         * Give some time to get any unreceived replies and then make
         * Oper as not in progress.
         */
        pTrace->Timer.u1Id = IP_TRACE_END_TIMER_ID;
        pTrace->Timer.pArg = (VOID *) pTrace;
        UDP_RESTART_TIMER (gUdpGblInfo.UdpIcmpTimerListId,
                           &(pTrace->Timer.Timer_node),
                           (UINT4) TRACE_END_TIME (pTrace));
    }
    return SNMP_SUCCESS;
}

/*
 * This is called as timeout routine for IP_TRACE_TIMER_END_ID.
 * The routine makes the trace node as non operational. (We will not receive
 * replies any longer).
 */
/**** $$TRACE_PROCEDURE_NAME  = trace_end_timeout   ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
#ifdef __STDC__

INT4
trace_end_timeout (t_TRACE * pTrace)
#else

INT4
trace_end_timeout (pTrace)
     t_TRACE            *pTrace;
#endif
{
    UDP_STOP_TIMER (gUdpGblInfo.UdpIcmpTimerListId,
                    &(pTrace->Timer.Timer_node));
    pTrace->i1Oper = TRACE_OPER_NOT_IN_PROGRESS;
    IP_TRC_ARG1 (UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                 "Trace Route operation completed - "
                 "Timeout for Destination %x \n", pTrace->u4Dest);
    return SNMP_SUCCESS;
}

/*
 * Trace Information table
 * Following are the entries in this table
 * INDEX  u4Dest
 * INDEX  i2Hop
 * RO     u4Gw
 * RO     u4Time-1
 * RO     u4Time-2
 * RO     u4Time-3
 *
 * Trace info is maintained as a linked list in the Trace table.
 * There will be a list per destination and the Nth node indicates the
 * information from Nth hop.
 */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpTraceTable
 Input       :  The Indices
                IpTraceDest
                IpTraceHopCount
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpTraceTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIpTraceTable (UINT4 u4IpTraceDest,
                                        INT4 i4IpTraceHopCount)
#else
INT1
nmhValidateIndexInstanceFsIpTraceTable (u4IpTraceDest, i4IpTraceHopCount)
     UINT4               u4IpTraceDest;
     INT4                i4IpTraceHopCount;
#endif
{
    t_TRACE            *pTrace;

    if ((pTrace = trace_get_entry (u4IpTraceDest)) == NULL)
        return SNMP_FAILURE;
    if (UDP_SLL_Nth (&(pTrace->Info_list), (UINT4) i4IpTraceHopCount) == NULL)
        return SNMP_FAILURE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpTraceTable
 Input       :  The Indices
                IpTraceDest
                IpTraceHopCount
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpTraceTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIpTraceTable (UINT4 *pu4IpTraceDest, INT4 *pi4IpTraceHopCount)
#else
INT1
nmhGetFirstIndexFsIpTraceTable (pu4IpTraceDest, pi4IpTraceHopCount)
     UINT4              *pu4IpTraceDest;
     INT4               *pi4IpTraceHopCount;
#endif
{
    return (INT1) trace_info_get_next_index (0, 0,
                                             pu4IpTraceDest,
                                             (INT2 *) pi4IpTraceHopCount);

}

#ifdef __STDC__

INT4
trace_info_get_next_index (UINT4 u4Dest, INT2 i2Hop, UINT4 *u4pDest_ret,
                           INT2 *i2pHop_ret)
#else

INT4
trace_info_get_next_index (u4Dest, i2Hop, u4pDest_ret, i2pHop_ret)
     UINT4               u4Dest;
     INT2                i2Hop;
     UINT4              *u4pDest_ret;
     INT2               *i2pHop_ret;
#endif
{
    /*t_TRACE_INFO       *pTrace_info; */

    /* I have to scan here if many entries are there */
    if (Trace.u4Dest >= u4Dest)
    {
        if (((t_TRACE_INFO *) UDP_SLL_Nth (&(Trace.Info_list),
                                           (UINT4) (i2Hop + 1))) == NULL)
        {
            return SNMP_FAILURE;
        }
        *u4pDest_ret = Trace.u4Dest;
        *i2pHop_ret = (INT2) (i2Hop + 1);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****n***********************************************************************
 Function    :  nmhGetNextIndexFsIpTraceTable
 Input       :  The Indices
                IpTraceDest
                nextIpTraceDest
                IpTraceHopCount
                nextIpTraceHopCount
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpTraceTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIpTraceTable (UINT4 u4IpTraceDest, UINT4 *pu4NextIpTraceDest,
                               INT4 i4IpTraceHopCount,
                               INT4 *pi4NextIpTraceHopCount)
#else
INT1
nmhGetNextIndexFsIpTraceTable (u4IpTraceDest, pu4NextIpTraceDest,
                               i4IpTraceHopCount, pi4NextIpTraceHopCount)
     UINT4               u4IpTraceDest;
     UINT4              *pu4NextIpTraceDest;
     INT4                i4IpTraceHopCount;
     INT4               *pi4NextIpTraceHopCount;
#endif
{
    t_TRACE_INFO       *pTrace_info;

    UNUSED_PARAM (pTrace_info);
    /* I have to scan here if many entries are there */
    if (Trace.u4Dest >= u4IpTraceDest)
    {
        if (((t_TRACE_INFO *) UDP_SLL_Nth (&(Trace.Info_list),
                                           (UINT4) i4IpTraceHopCount + 1)) ==
            NULL)
        {
            return SNMP_FAILURE;
        }
        *pu4NextIpTraceDest = Trace.u4Dest;
        *pi4NextIpTraceHopCount = i4IpTraceHopCount + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/***************************************************************************
 Function    :  nmhGetFsIpTraceIntermHop
 Input       :  The Indices
                IpTraceDest
                IpTraceHopCount

                The Object
                retValIpTraceIntermHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceIntermHop ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceIntermHop (UINT4 u4IpTraceDest, INT4 i4IpTraceHopCount,
                          UINT4 *pu4RetValIpTraceIntermHop)
#else
INT1
nmhGetFsIpTraceIntermHop (u4IpTraceDest, i4IpTraceHopCount,
                          pu4RetValIpTraceIntermHop)
     UINT4               u4IpTraceDest;
     INT4                i4IpTraceHopCount;
     UINT4              *pu4RetValIpTraceIntermHop;
#endif
{
    t_TRACE            *pTrace;
    t_TRACE_INFO       *pTrace_info;

    if ((pTrace = trace_get_entry (u4IpTraceDest)) != NULL)
    {
        if ((pTrace_info = (t_TRACE_INFO *) UDP_SLL_Nth (&(pTrace->Info_list),
                                                         (UINT4)
                                                         (i4IpTraceHopCount)))
            != NULL)
        {
            *pu4RetValIpTraceIntermHop = pTrace_info->u4Gw;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceReachTime1
 Input       :  The Indices
                IpTraceDest
                IpTraceHopCount

                The Object
                retValIpTraceReachTime1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceReachTime1 ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceReachTime1 (UINT4 u4IpTraceDest, INT4 i4IpTraceHopCount,
                           INT4 *pi4RetValIpTraceReachTime1)
#else
INT1
nmhGetFsIpTraceReachTime1 (u4IpTraceDest, i4IpTraceHopCount,
                           pi4RetValIpTraceReachTime1)
     UINT4               u4IpTraceDest;
     INT4                i4IpTraceHopCount;
     INT4               *pi4RetValIpTraceReachTime1;
#endif
{
    t_TRACE            *pTrace;
    t_TRACE_INFO       *pTrace_info;

    if ((pTrace = trace_get_entry (u4IpTraceDest)) != NULL)
    {
        if ((pTrace_info =
             (t_TRACE_INFO *) UDP_SLL_Nth (&(pTrace->Info_list),
                                           (UINT4) i4IpTraceHopCount)) != NULL)
        {
            *pi4RetValIpTraceReachTime1 = pTrace_info->i2Time[1];
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceReachTime2
 Input       :  The Indices
                IpTraceDest
                IpTraceHopCount

                The Object
                retValIpTraceReachTime2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceReachTime2 ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceReachTime2 (UINT4 u4IpTraceDest, INT4 i4IpTraceHopCount,
                           INT4 *pi4RetValIpTraceReachTime2)
#else
INT1
nmhGetFsIpTraceReachTime2 (u4IpTraceDest, i4IpTraceHopCount,
                           pi4RetValIpTraceReachTime2)
     UINT4               u4IpTraceDest;
     INT4                i4IpTraceHopCount;
     INT4               *pi4RetValIpTraceReachTime2;
#endif
{
    t_TRACE            *pTrace;
    t_TRACE_INFO       *pTrace_info;

    if ((pTrace = trace_get_entry (u4IpTraceDest)) != NULL)
    {
        if ((pTrace_info =
             (t_TRACE_INFO *) UDP_SLL_Nth (&(pTrace->Info_list),
                                           (UINT4) i4IpTraceHopCount)) != NULL)
        {
            *pi4RetValIpTraceReachTime2 = pTrace_info->i2Time[2];
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpTraceReachTime3
 Input       :  The Indices
                IpTraceDest
                IpTraceHopCount

                The Object
                retValIpTraceReachTime3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpTraceReachTime3 ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpTraceReachTime3 (UINT4 u4IpTraceDest, INT4 i4IpTraceHopCount,
                           INT4 *pi4RetValIpTraceReachTime3)
#else
INT1
nmhGetFsIpTraceReachTime3 (u4IpTraceDest, i4IpTraceHopCount,
                           pi4RetValIpTraceReachTime3)
     UINT4               u4IpTraceDest;
     INT4                i4IpTraceHopCount;
     INT4               *pi4RetValIpTraceReachTime3;
#endif
{
    t_TRACE            *pTrace;
    t_TRACE_INFO       *pTrace_info;

    if ((pTrace = trace_get_entry (u4IpTraceDest)) != NULL)
    {
        if ((pTrace_info =
             (t_TRACE_INFO *) UDP_SLL_Nth (&(pTrace->Info_list),
                                           (UINT4) i4IpTraceHopCount)) != NULL)
        {
            *pi4RetValIpTraceReachTime3 = pTrace_info->i2Time[3];
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

#ifdef __STDC__

static VOID
trace_reset_rec (t_TRACE * pTrace)
#else

static VOID
trace_reset_rec (pTrace)
     t_TRACE            *pTrace;
#endif
{
    pTrace->u4Dest = 0;
    pTrace->i1Admin = TRACE_ADMIN_DISABLE;
    pTrace->i2Timeout = TRACE_DEF_TIMEOUT;
    pTrace->i2Max_ttl = TRACE_DEF_TTL;
    pTrace->i2Min_ttl = TRACE_MIN_TTL;
    pTrace->i1Oper = TRACE_OPER_NOT_IN_PROGRESS;
    pTrace->i2Mtu = TRACE_DEF_MTU;
    pTrace->i2Id = 0;
    pTrace->i2Cur_hop = 0;
    pTrace->u4Start_time = 0;
    pTrace->i2Start_id = 0;
    pTrace->i2Cur_probe = 0;
    UDP_SLL_Clear (&pTrace->Info_list);
}

#ifdef __STDC__

VOID
trace_init_rec (VOID)
#else

VOID
trace_init_rec ()
#endif
{
    UDP_SLL_Init ((tUDP_SLL *) & Trace.Info_list);
    trace_reset_rec (&Trace);
}

static INT4
tr_task_udp_open (UINT2 u2Port, UINT4 u4Addr)
{
    return (udp_trcrt_open (u2Port, u4Addr));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpTraceConfigTable
 Input       :  The Indices
                FsIpTraceConfigDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpTraceConfigTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
