/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    v4ov6tun.c
 *
 *    PRINCIPAL AUTHOR             :    A. SivaramaKrishnan
 *
 *    SUBSYSTEM NAME               :    V4 OVER V6 TUNNEL
 *
 *    MODULE NAME                  :    IP Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    May-22-2002
 *
 *    DESCRIPTION                  :    This file contains API's for handling
 *                                      V4OverV6 Tunnel Interface.
 *-----------------------------------------------------------------------------
 */
#include "v4ov6tun.h"

t_V4OVERV6_TUN      gaV4OverV6TunIf[IPIF_MAX_V4OVERV6_TUN_IFACES];

/****************************************************************************
 Function    :  Ipv4ov6TunlInit
 Input       :  None.
 Output      :  None.
 Returns     :  None.
****************************************************************************/
VOID
Ipv4ov6TunlInit (VOID)
{
    /* Register with IP6 for Packet Reception */
#ifdef IP6_WANTED
    if (NetIpv6RegisterHigherLayerProtocol (IP_PROTO_ID,
                                            NETIPV6_APPLICATION_RECEIVE,
                                            (VOID *) IpRegHandler) ==
        NETIPV6_FAILURE)
    {
        return;
    }
#endif
}

/**************************************************************************** 
    Function    :  IpRegHandler 
    Input       :  pIfInfo 
    Output      :  None. 
    Returns     :  None. 
    Description :  Receives the IPv4 Packets from IPv6 
****************************************************************************/
VOID
IpRegHandler (tNetIpv6HliParams * pIfInfo)
{
    Ipv4ov6TunlRcv (pIfInfo->unIpv6HlCmdType.AppRcv.pBuf,
                    pIfInfo->unIpv6HlCmdType.AppRcv.u4PktLength,
                    (UINT2) pIfInfo->unIpv6HlCmdType.AppRcv.u4Index,
                    (UINT1) pIfInfo->unIpv6HlCmdType.AppRcv.u4Type);
    return;
}

/****************************************************************************
 Function    :  Ipv4ov6TunlCreate
 Input       :  i4TunlIndex (Tunl Table Index), u2IfIndex (IP IF Index.)
 Output      :  None.
 Returns     :  None.
****************************************************************************/
INT4
Ipv4ov6TunlCreate (INT4 i4TunlIndex, UINT2 u2IfIndex)
{
    UINT2               u2Port = 0;
    t_IFACE_PARAMS      IfaceParams;

    /* check for Interface Availability with CFA before creation at IP Level */
    MEMSET (&IfaceParams, ZERO, sizeof (t_IFACE_PARAMS));

    IfaceParams.u2IfIndex = u2IfIndex;
    IfaceParams.u1IfType = IP_V4OVERV6_TUNNEL_INTERFACE_TYPE;
    IfaceParams.u4IpAddr = 0;
    IfaceParams.u4SubnetMask = 0;
    IfaceParams.u4BcastAddr = 0;
    IfaceParams.u4Mtu = IP_V4OVERV6_TUN_MTU (i4TunlIndex);
    IfaceParams.u1Persistence = 0;
    IfaceParams.u4PeerIpAddr = 0;
    IfaceParams.u1EncapType = CFA_ENCAP_OTHER;
    IfaceParams.u2Port = u2Port;

    IpRegisterNetworkInterface (&IfaceParams);

    IP_V4OVERV6_TUN_IP_IFINDEX (i4TunlIndex) = u2IfIndex;

    IP_V4OVERV6_TUN_ADMINSTATUS (i4TunlIndex) = ADMIN_STATUS_ACTIVE;

    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  Ipv4ov6TunlDelete
 Input       :  (i4TunlIndex) Tunnel Table Index.
 Output      :  None.
 Returns     :  SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Ipv4ov6TunlDelete (INT4 i4TunlIndex)
{
    UINT2               u2Port;
    INT4                i4RetVal;

    switch (IP_V4OVERV6_TUN_ADMINSTATUS (i4TunlIndex))
    {
        case ADMIN_STATUS_ACTIVE:

            IpGetPortFromIfIndex (IP_V4OVERV6_TUN_IP_IFINDEX (i4TunlIndex),
                                  &u2Port);

            /* Delete the Entry From IP Global Interface Table 'Ipif_glbtab' */

            IpDeRegisterNetworkInterface (u2Port);

            /* No break here */

        case IPFWD_CREATE_AND_WAIT:

            IP_V4OVERV6_TUN_MTU (i4TunlIndex) = IP_V4OVERV6_TUN_DEF_MTU;
            IP_V4OVERV6_TUN_ADMINSTATUS (i4TunlIndex)
                = IPFWD_ROW_DOES_NOT_EXIST;
            IP_V4OVERV6_TUN_IP_IFINDEX (i4TunlIndex) = IPIF_INVALID_INDEX;
            i4RetVal = IP_SUCCESS;
            break;

        default:

            i4RetVal = IP_FAILURE;
            break;
    }
    return i4RetVal;

}

/****************************************************************************
 Function    :  Ipv4ov6TunlSend
 Input       :  u2Port (Ip Logical Index), u2Len & pBuf. 
 Output      :  None.
 Returns     :  IP_SUCCESS/IP_FAILURE
****************************************************************************/
INT1
Ipv4ov6TunlSend (UINT2 u2Port, UINT2 u2Len, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tHlToIp6Params      Ip6Params;

    Ip6Params.u1Cmd = IP6_LAYER4_DATA;
    Ip6Params.u1Hlim = IP6_DEF_HOP_LIMIT;
    Ip6Params.u1Proto = IP_PROTO_ID;
    Ip6Params.u4Index = 0;
    Ip6Params.u4Len = u2Len;

    IpGetTEPv6Addr (IpGetIfIndexFromPort (u2Port), &Ip6Params.Ip6DstAddr);

    if (IP6_SRCADDR_FOR_DEST_ADDR (&Ip6Params.Ip6DstAddr,
                                   &Ip6Params.Ip6SrcAddr) == IP6_FAILURE)
    {
        return IP_FAILURE;
    }

    if (IP6RCVHL (pBuf, &Ip6Params) == IP6_SUCCESS)
    {
        return IP_SUCCESS;
    }

    return IP_FAILURE;
}

/* function to receive packet from IP6 to IP */
/****************************************************************************
 Function    :  Ipv4ov6TunlRcv
 Input       :  None.
                But u4Len, u2Index & u1Type are passed dummy values, since the
        IPv6 Registration routine expects these parameters.
 Output      :  None.
 Returns     :  None.
 Description :  Receives the IP Packets from IP6 & enques to IP Task.
****************************************************************************/
VOID
Ipv4ov6TunlRcv (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Len, UINT2 u2Index,
                UINT1 u1Type)
{
    t_IP                Ip;
    UINT2               u2RtPort;
    UINT4               u4RtGw;
    UINT4               u4Offset;

    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (u2Index);

    /* Remove the Processed (IP6) Data from the Buffer */
    u4Offset = CRU_BUF_Get_ChainValidByteCount (pBuf) - u4Len;
    CRU_BUF_Move_ValidOffset (pBuf, u4Offset);

    if ((IpExtractHdr (&Ip, pBuf) == IP_SUCCESS) &&
        (IpGetRoute (Ip.u4Dest, 0, -1, 0, &u2RtPort, &u4RtGw) == IP_SUCCESS))
    {
        /* Enque to IP Task */
        if (IpHandlePktFromCfa (pBuf, u2RtPort, 0) == IP_FAILURE)
        {
            IP_TRC (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                    "Ipv4ov6TunlRcv: rel Buf. \n");
            IP_RELEASE_BUF (pBuf, FALSE);
        }
    }
    else
    {
        IP_TRC (IP_MOD_TRC, BUFFER_TRC, IP_NAME, "Ipv4ov6TunlRcv: rel Buf. \n");
        IP_RELEASE_BUF (pBuf, FALSE);

        if ((ipif_is_our_address (Ip.u4Src) == TRUE) ||
            (Ip.u4Src == IP_ANY_ADDR))
        {
            IP4SYS_INC_OUT_NO_ROUTES ();
            IP4IF_INC_OUT_NO_ROUTES (u2RtPort);
        }
    }
}

/****************************************************************************
 Function    :  IpGetTEPv6Addr
 Input       :  u2Index (IP Logical IF INDEX, which is of Type TUNL)
 Output      :  pIp6Addr (The TEP IPv6 Address of the TUNL is copied here)
 Returns     :  None.
****************************************************************************/
VOID
IpGetTEPv6Addr (UINT4 u4Index, tIp6Addr * pIp6Addr)
{
    INT1                i1Count;

    SET_ADDR_UNSPECIFIED (*pIp6Addr);
    for (i1Count = 0; i1Count < IPIF_MAX_V4OVERV6_TUN_IFACES; i1Count++)
    {
        if (u4Index == IP_V4OVERV6_TUN_IP_IFINDEX (i1Count))
        {
            Ip6AddrCopy (pIp6Addr, &gaV4OverV6TunIf[i1Count].TEPv6Addr);
            break;
        }
    }
}

/****************************************************************************
 Function    :  IpCheckTEPExists
 Input       :  Ip6Addr
 Output      :  None.
 Returns     :  SUCCESS/FAILURE
****************************************************************************/
INT4
IpCheckTEPExists (tIp6Addr * pIp6Addr)
{
    INT1                i;

    for (i = 0; i < IPIF_MAX_V4OVERV6_TUN_IFACES; i++)
    {

        if (Ip6AddrMatch (pIp6Addr, IP_V4OVERV6_TUN_TEP_V6ADDR (i),
                          IP6_ADDR_MAX_PREFIX) == TRUE)
            return IP_SUCCESS;

    }
    return IP_FAILURE;

}
