 #ifndef _V4OV6TUN_H_
 #define _V4OV6TUN_H_

 /*****************Include files*********************************/
 #include "lr.h"
 #include "cruport.h"
 #include "tmoport.h"
 #include "ip.h"
 #include "cfa.h"
 #include "ipv6.h"
 #include "ipport.h"
 #include "ippdudfs.h"
 #include "other.h"
 #include "ipreg.h"
 #include "ipflttyp.h"
 #include "snmctdfs.h"
 #include "iptrace.h"
 #include "ip6util.h"
 #include "ipprotos.h"
 
/******************Macros for LLR *******************************/
#define ADMIN_STATUS_ACTIVE           1
#define ADMIN_STATUS_NOT_IN_SER       2
#define ADMIN_STATUS_NOT_READY        3
#define ADMIN_STATUS_CREATE_AND_GO    4
#define ADMIN_STATUS_CREATE_AND_WAIT  5
#define ADMIN_STATUS_DESTROY          6

 /*************IP6 Calls Called By this Module*******************/
 #define IP6REGHL                   Ip6RegHl 
 #define IP6_SRCADDR_FOR_DEST_ADDR  Ip6SrcAddrForDestAddr 
 #define IP6RCVHL	            Ip6RcvFromHl 
 
 /***********Macros to Access V4OverV6 Tunnel Table**************/
 #define IP_V4OVERV6_TUN_IP_IFINDEX(i)  (gaV4OverV6TunIf[i].u2IpIfIndex)
 #define IP_V4OVERV6_TUN_TEP_V6ADDR(i)  (&gaV4OverV6TunIf[i].TEPv6Addr)
 #define IP_V4OVERV6_TUN_MTU(i)         (gaV4OverV6TunIf[i].u2Mtu)
 #define IP_V4OVERV6_TUN_ADMINSTATUS(i) (gaV4OverV6TunIf[i].u1AdminStatus)
 
 /********************Other Macros***************************************/
 #define IPIF_MAX_V4OVERV6_TUN_IFACES        10
 #define IP_V4OVERV6_TUN_DEF_MTU             IP6_DEFAULT_MTU
 #define IP_V4OVERV6_TUNNEL_INTERFACE_TYPE   201	
 #define IP6_ADDR_LEN_IN_BYTES               16
/*************************Data Structures *******************************/
 typedef struct
 {
     UINT2  u2Mtu;
     UINT2  u2IpIfIndex; /* IP IF INDEX */
                         /* Refers Ipif_glbtab[.].InterfaceId.u1_InterfaceNum*/ 
     tIp6Addr  TEPv6Addr;  /* IP6 Addr of the Tunnel End Point (TEP) */
     UINT1  u1AdminStatus;
     UINT1  u1Reserved;
     UINT2  u2Reserved;
 }t_V4OVERV6_TUN;
 
extern t_V4OVERV6_TUN    gaV4OverV6TunIf[IPIF_MAX_V4OVERV6_TUN_IFACES];

/********************Function Prototypes************************/
VOID Ipv4ov6TunlRcv    PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4Len, 
                               UINT2 u2Index, UINT1 u1Type));
VOID Ipv4ov6TunlInit   PROTO ((VOID));
INT4 Ipv4ov6TunlCreate PROTO ((INT4 i4TunlIndex, UINT2 u2IfIndex));
INT4 Ipv4ov6TunlDelete PROTO ((INT4 i4TunlIndex));
INT1 Ipv4ov6TunlSend   PROTO ((UINT2 u2Port,UINT2  u2Len,
                               tCRU_BUF_CHAIN_HEADER *pBuf));
VOID IpGetTEPv6Addr    PROTO ((UINT4 u4Index, tIp6Addr *pIp6Addr));
INT4 IpCheckTEPExists  PROTO ((tIp6Addr * pIp6Addr));
VOID IpRegHandler   PROTO ((tNetIpv6HliParams * pIfInfo)); 
#endif
