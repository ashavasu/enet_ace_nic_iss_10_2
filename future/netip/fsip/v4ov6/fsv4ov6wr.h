
#ifndef _FSV4OV6WRAP_H 
#define _FSV4OV6WRAP_H 
VOID RegisterFSV4OV6(VOID);
INT4 Fsv4overv6tunlIndexGet (tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlIpIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlTEPv6AddrTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlTEPv6AddrSet (tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlTEPv6AddrGet (tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlMtuTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlMtuSet (tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlMtuGet (tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlAdminStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlAdminStatusSet (tSnmpIndex *, tRetVal *);
INT4 Fsv4overv6tunlAdminStatusGet (tSnmpIndex *, tRetVal *);

 /*  GetNext Function Prototypes */

INT4 GetNextIndexFsv4overv6tunlTable( tSnmpIndex *, tSnmpIndex *);
#endif
