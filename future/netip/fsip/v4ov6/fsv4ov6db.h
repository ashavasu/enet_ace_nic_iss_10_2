#ifndef __FSIPRIPMBDB_H__
#define __FSIPRIPMBDB_H__

/* INDEX Table Entries */
UINT1 Fsv4overv6tunlTableINDEX [ ] = {SNMP_DATA_TYPE_INTEGER};

/*  MIB information  */
UINT4 fsv4ov6 [ ] = { 1,3,6,1,4,1,2076,2,21 };
tSNMP_OID_TYPE fsv4ov6OID = {9, fsv4ov6};


/*  OID Description*/
UINT4 Fsv4overv6tunlIndex [ ] = {1,3,6,1,4,1,2076,2,21,1,1,1};
UINT4 Fsv4overv6tunlIpIfIndex [ ] = {1,3,6,1,4,1,2076,2,21,1,1,2};
UINT4 Fsv4overv6tunlTEPv6Addr [ ] = {1,3,6,1,4,1,2076,2,21,1,1,3};
UINT4 Fsv4overv6tunlMtu [ ] = {1,3,6,1,4,1,2076,2,21,1,1,4};
UINT4 Fsv4overv6tunlAdminStatus [ ] = {1,3,6,1,4,1,2076,2,21,1,1,5};

/* MBDB entry list */
tMbDbEntry fsv4ov6MibEntry[]= {
{{12, Fsv4overv6tunlIndex},   GetNextIndexFsv4overv6tunlTable, Fsv4overv6tunlIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsv4overv6tunlTableINDEX, 1},

{{12, Fsv4overv6tunlIpIfIndex},   GetNextIndexFsv4overv6tunlTable, Fsv4overv6tunlIpIfIndexGet, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsv4overv6tunlTableINDEX, 1},

{{12, Fsv4overv6tunlTEPv6Addr},   GetNextIndexFsv4overv6tunlTable, Fsv4overv6tunlTEPv6AddrGet, Fsv4overv6tunlTEPv6AddrSet, Fsv4overv6tunlTEPv6AddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READCREATE, Fsv4overv6tunlTableINDEX, 1},

{{12, Fsv4overv6tunlMtu},   GetNextIndexFsv4overv6tunlTable, Fsv4overv6tunlMtuGet, Fsv4overv6tunlMtuSet, Fsv4overv6tunlMtuTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READCREATE, Fsv4overv6tunlTableINDEX, 1},

{{12, Fsv4overv6tunlAdminStatus},   GetNextIndexFsv4overv6tunlTable, Fsv4overv6tunlAdminStatusGet, Fsv4overv6tunlAdminStatusSet, Fsv4overv6tunlAdminStatusTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsv4overv6tunlTableINDEX, 1},


};

tMibData fsv4ov6Entry = { 5, fsv4ov6MibEntry };
#endif /* __MBDB_H__ */
