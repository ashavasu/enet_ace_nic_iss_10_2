# include  "include.h"
# include  "fsv4ov6low.h"
# include  "v4ov6tun.h"

/* LOW LEVEL Routines for Table : Fsv4overv6tunlTable. */

/* GET_EXACT Validate Index Instance Routine. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsv4overv6tunlTable
 Input       :  The Indices
                Fsv4overv6tunlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsv4overv6tunlTable (INT4 i4Fsv4overv6tunlIndex)
#else
INT1
nmhValidateIndexInstanceFsv4overv6tunlTable (i4Fsv4overv6tunlIndex)
     INT4                i4Fsv4overv6tunlIndex;
#endif
{
    if ((i4Fsv4overv6tunlIndex < IPIF_MAX_V4OVERV6_TUN_IFACES) &&
        (i4Fsv4overv6tunlIndex >= 0))
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsv4overv6tunlTable
 Input       :  The Indices
                Fsv4overv6tunlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsv4overv6tunlTable (INT4 *pi4Fsv4overv6tunlIndex)
#else
INT1
nmhGetFirstIndexFsv4overv6tunlTable (pi4Fsv4overv6tunlIndex)
     INT4               *pi4Fsv4overv6tunlIndex;
#endif
{
    INT1                i1Count;

    for (i1Count = 0; i1Count < IPIF_MAX_V4OVERV6_TUN_IFACES; i1Count++)
    {
        if (IP_V4OVERV6_TUN_ADMINSTATUS (i1Count) == ADMIN_STATUS_ACTIVE)
        {
            *pi4Fsv4overv6tunlIndex = i1Count;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* GET_NEXT Routine.  */
/****************************************************************************
 Function    :  nmhGetNextIndexFsv4overv6tunlTable
 Input       :  The Indices
                Fsv4overv6tunlIndex
                nextFsv4overv6tunlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetNextIndexFsv4overv6tunlTable (INT4 i4Fsv4overv6tunlIndex,
                                    INT4 *pi4NextFsv4overv6tunlIndex)
#else
INT1
nmhGetNextIndexFsv4overv6tunlTable (i4Fsv4overv6tunlIndex,
                                    pi4NextFsv4overv6tunlIndex)
     INT4                i4Fsv4overv6tunlIndex;
     INT4               *pi4NextFsv4overv6tunlIndex;
#endif
{
    INT1                i1Count;

    for (i1Count = i4Fsv4overv6tunlIndex + 1;
         i1Count < IPIF_MAX_V4OVERV6_TUN_IFACES; i1Count++)
    {
        if ((IP_V4OVERV6_TUN_ADMINSTATUS (i1Count) == ADMIN_STATUS_ACTIVE))
        {
            *pi4NextFsv4overv6tunlIndex = i1Count;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 *  Function    :  nmhGetFsv4overv6tunlIpIfIndex
 *  Input       :  The Indices
 *                 Fsv4overv6tunlIndex
 *
 *  The Object retValFsv4overv6tunlIpIfIndex                                
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                  store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsv4overv6tunlIpIfIndex (INT4 i4Fsv4overv6tunlIndex,
                               INT4 *pi4RetValFsv4overv6tunlIpIfIndex)
#else
INT1
nmhGetFsv4overv6tunlIpIfIndex (i4Fsv4overv6tunlIndex,
                               pi4RetValFsv4overv6tunlIpIfIndex)
     INT4                i4Fsv4overv6tunlIndex;
     INT4               *pi4RetValFsv4overv6tunlIpIfIndex;
#endif
{
    if ((IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) == IPFWD_NOT_READY)
        || (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) ==
            ADMIN_STATUS_ACTIVE))
    {
        *pi4RetValFsv4overv6tunlIpIfIndex =
            IP_V4OVERV6_TUN_IP_IFINDEX (i4Fsv4overv6tunlIndex);
        return SNMP_SUCCESS;
    }
    else
        return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsv4overv6tunlTEPv6Addr
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                retValFsv4overv6tunlTEPv6Addr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsv4overv6tunlTEPv6Addr (INT4 i4Fsv4overv6tunlIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsv4overv6tunlTEPv6Addr)
#else
INT1
nmhGetFsv4overv6tunlTEPv6Addr (i4Fsv4overv6tunlIndex,
                               pRetValFsv4overv6tunlTEPv6Addr)
     INT4                i4Fsv4overv6tunlIndex;
     tSNMP_OCTET_STRING_TYPE *pRetValFsv4overv6tunlTEPv6Addr;
#endif
{

    if ((IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) == IPFWD_NOT_READY)
        || (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) ==
            ADMIN_STATUS_ACTIVE))
    {
        STRNCPY (pRetValFsv4overv6tunlTEPv6Addr->pu1_OctetList,
                 IP_V4OVERV6_TUN_TEP_V6ADDR (i4Fsv4overv6tunlIndex),
                 IP6_ADDR_LEN_IN_BYTES);
        pRetValFsv4overv6tunlTEPv6Addr->i4_Length = IP6_ADDR_LEN_IN_BYTES;
        return SNMP_SUCCESS;
    }
    else
        return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsv4overv6tunlMtu
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                retValFsv4overv6tunlMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsv4overv6tunlMtu (INT4 i4Fsv4overv6tunlIndex,
                         INT4 *pi4RetValFsv4overv6tunlMtu)
#else
INT1
nmhGetFsv4overv6tunlMtu (i4Fsv4overv6tunlIndex, pi4RetValFsv4overv6tunlMtu)
     INT4                i4Fsv4overv6tunlIndex;
     INT4               *pi4RetValFsv4overv6tunlMtu;
#endif
{

    if ((IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) == IPFWD_NOT_READY)
        || (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) ==
            ADMIN_STATUS_ACTIVE))
    {
        *pi4RetValFsv4overv6tunlMtu =
            IP_V4OVERV6_TUN_MTU (i4Fsv4overv6tunlIndex);
        return SNMP_SUCCESS;
    }
    else
        return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsv4overv6tunlAdminStatus
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                retValFsv4overv6tunlAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsv4overv6tunlAdminStatus (INT4 i4Fsv4overv6tunlIndex,
                                 INT4 *pi4RetValFsv4overv6tunlAdminStatus)
#else
INT1
nmhGetFsv4overv6tunlAdminStatus (i4Fsv4overv6tunlIndex,
                                 pi4RetValFsv4overv6tunlAdminStatus)
     INT4                i4Fsv4overv6tunlIndex;
     INT4               *pi4RetValFsv4overv6tunlAdminStatus;
#endif
{
    if ((IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) <=
         ADMIN_STATUS_DESTROY) &&
        (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) >=
         ADMIN_STATUS_ACTIVE))
    {
        *pi4RetValFsv4overv6tunlAdminStatus =
            IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex);
        return SNMP_SUCCESS;
    }
    else
        return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsv4overv6tunlTEPv6Addr
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                setValFsv4overv6tunlTEPv6Addr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsv4overv6tunlTEPv6Addr ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsv4overv6tunlTEPv6Addr (INT4 i4Fsv4overv6tunlIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsv4overv6tunlTEPv6Addr)
#else
INT1
nmhSetFsv4overv6tunlTEPv6Addr (i4Fsv4overv6tunlIndex,
                               pSetValFsv4overv6tunlTEPv6Addr)
     INT4                i4Fsv4overv6tunlIndex;
     tSNMP_OCTET_STRING_TYPE *pSetValFsv4overv6tunlTEPv6Addr;
#endif
{
    /* Check if a Tunnel already exists to this End Point */
    if (IpCheckTEPExists
        ((tIp6Addr *) pSetValFsv4overv6tunlTEPv6Addr->pu1_OctetList) ==
        IP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) == IPFWD_NOT_READY)
    {
        Ip6AddrCopy (IP_V4OVERV6_TUN_TEP_V6ADDR (i4Fsv4overv6tunlIndex),
                     (tIp6Addr *) pSetValFsv4overv6tunlTEPv6Addr->
                     pu1_OctetList);
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhSetFsv4overv6tunlMtu
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                setValFsv4overv6tunlMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsv4overv6tunlMtu (INT4 i4Fsv4overv6tunlIndex,
                         INT4 i4SetValFsv4overv6tunlMtu)
#else
INT1
nmhSetFsv4overv6tunlMtu (i4Fsv4overv6tunlIndex, i4SetValFsv4overv6tunlMtu)
     INT4                i4Fsv4overv6tunlIndex;
     INT4                i4SetValFsv4overv6tunlMtu;

#endif
{
    if (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) == IPFWD_NOT_READY)
    {
        IP_V4OVERV6_TUN_MTU (i4Fsv4overv6tunlIndex) =
            (UINT2) i4SetValFsv4overv6tunlMtu;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsv4overv6tunlAdminStatus
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                setValFsv4overv6tunlAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsv4overv6tunlAdminStatus (INT4 i4Fsv4overv6tunlIndex,
                                 INT4 i4SetValFsv4overv6tunlAdminStatus)
#else
INT1
nmhSetFsv4overv6tunlAdminStatus (i4Fsv4overv6tunlIndex,
                                 i4SetValFsv4overv6tunlAdminStatus)
     INT4                i4Fsv4overv6tunlIndex;
     INT4                i4SetValFsv4overv6tunlAdminStatus;
#endif
{
    UINT2               u2CfaLogIfIndex;
    INT1                i1RetVal = SNMP_SUCCESS;

    switch (i4SetValFsv4overv6tunlAdminStatus)
    {
        case ADMIN_STATUS_ACTIVE:
        {

            if (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) !=
                IPFWD_NOT_READY)
            {
                i1RetVal = SNMP_FAILURE;
                break;
            }

            if (CfaGetFreeInterfaceIndex ((UINT4 *) &u2CfaLogIfIndex,
                                          CFA_TUNNEL) == CFA_SUCCESS)
            {
                if (Ipv4ov6TunlCreate (i4Fsv4overv6tunlIndex,
                                       u2CfaLogIfIndex) == IP_FAILURE)
                {
                    i1RetVal = SNMP_FAILURE;
                }
            }
            else
            {
                i1RetVal = SNMP_FAILURE;
            }
            break;
        }

        case ADMIN_STATUS_NOT_IN_SER:
        case ADMIN_STATUS_NOT_READY:
        case ADMIN_STATUS_CREATE_AND_GO:

            i1RetVal = SNMP_FAILURE;
            break;

        case ADMIN_STATUS_CREATE_AND_WAIT:

            if (IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex) ==
                ADMIN_STATUS_ACTIVE)
            {
                i1RetVal = SNMP_FAILURE;
                break;
            }
            IP_V4OVERV6_TUN_MTU (i4Fsv4overv6tunlIndex)
                = IP_V4OVERV6_TUN_DEF_MTU;
            IP_V4OVERV6_TUN_ADMINSTATUS (i4Fsv4overv6tunlIndex)
                = IPFWD_NOT_READY;
            IP_V4OVERV6_TUN_IP_IFINDEX (i4Fsv4overv6tunlIndex)
                = IPIF_INVALID_INDEX;
            break;

        case ADMIN_STATUS_DESTROY:

            if (Ipv4ov6TunlDelete (i4Fsv4overv6tunlIndex) == IP_FAILURE)
            {
                i1RetVal = SNMP_FAILURE;
            }
            break;

    }
    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Fsv4overv6tunlTEPv6Addr
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                testValFsv4overv6tunlTEPv6Addr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2Fsv4overv6tunlTEPv6Addr (UINT4 *pu4ErrorCode,
                                  INT4 i4Fsv4overv6tunlIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsv4overv6tunlTEPv6Addr)
#else
INT1
nmhTestv2Fsv4overv6tunlTEPv6Addr (pu4ErrorCode, i4Fsv4overv6tunlIndex,
                                  pTestValFsv4overv6tunlTEPv6Addr)
     UINT4              *pu4ErrorCode;
     INT4                i4Fsv4overv6tunlIndex;
     tSNMP_OCTET_STRING_TYPE *pTestValFsv4overv6tunlTEPv6Addr;
#endif
{
    tIp6Addr            ip6Addr;

    if ((i4Fsv4overv6tunlIndex >= (INT4) IPIF_MAX_V4OVERV6_TUN_IFACES)
        || (i4Fsv4overv6tunlIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) pTestValFsv4overv6tunlTEPv6Addr->pu1_OctetList);

    if (IS_ADDR_MULTI (ip6Addr) || IS_ADDR_UNSPECIFIED (ip6Addr))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsv4overv6tunlMtu
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                testValFsv4overv6tunlMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2Fsv4overv6tunlMtu (UINT4 *pu4ErrorCode, INT4 i4Fsv4overv6tunlIndex,
                            INT4 i4TestValFsv4overv6tunlMtu)
#else
INT1
nmhTestv2Fsv4overv6tunlMtu (pu4ErrorCode, i4Fsv4overv6tunlIndex,
                            i4TestValFsv4overv6tunlMtu)
     UINT4              *pu4ErrorCode;
     INT4                i4Fsv4overv6tunlIndex;
     INT4                i4TestValFsv4overv6tunlMtu;
#endif
{
    if ((i4Fsv4overv6tunlIndex >= (INT4) IPIF_MAX_V4OVERV6_TUN_IFACES)
        || (i4Fsv4overv6tunlIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsv4overv6tunlMtu > IP_V4OVERV6_TUN_DEF_MTU) ||
        (i4TestValFsv4overv6tunlMtu < 1280))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsv4overv6tunlAdminStatus
 Input       :  The Indices
                Fsv4overv6tunlIndex

                The Object 
                testValFsv4overv6tunlAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2Fsv4overv6tunlAdminStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4Fsv4overv6tunlIndex,
                                    INT4 i4TestValFsv4overv6tunlAdminStatus)
#else
INT1
nmhTestv2Fsv4overv6tunlAdminStatus (pu4ErrorCode, i4Fsv4overv6tunlIndex,
                                    i4TestValFsv4overv6tunlAdminStatus)
     UINT4              *pu4ErrorCode;
     INT4                i4Fsv4overv6tunlIndex;
     INT4                i4TestValFsv4overv6tunlAdminStatus;
#endif
{
    if ((i4Fsv4overv6tunlIndex >= (INT4) IPIF_MAX_V4OVERV6_TUN_IFACES)
        || (i4Fsv4overv6tunlIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsv4overv6tunlAdminStatus > ADMIN_STATUS_DESTROY)
        || (i4TestValFsv4overv6tunlAdminStatus < ADMIN_STATUS_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
