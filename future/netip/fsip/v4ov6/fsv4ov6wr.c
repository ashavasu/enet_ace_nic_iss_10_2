#include "lr.h"
#include "fssnmp.h"
#include "fsv4ov6wr.h"
#include "fsv4ov6low.h"
#include "fsv4ov6db.h"

VOID
RegisterFSV4OV6 ()
{
    SNMPRegisterMib (&fsv4ov6OID, &fsv4ov6Entry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsv4ov6OID, (const UINT1 *) "fsiprip");
}

INT4
Fsv4overv6tunlIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsv4overv6tunlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;
    return SNMP_SUCCESS;
}

INT4
Fsv4overv6tunlIpIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsv4overv6tunlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsv4overv6tunlIpIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &pMultiData->i4_SLongValue));
}

INT4
Fsv4overv6tunlTEPv6AddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{

    return (nmhTestv2Fsv4overv6tunlTEPv6Addr (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));
}

INT4
Fsv4overv6tunlTEPv6AddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsv4overv6tunlTEPv6Addr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));
}

INT4
Fsv4overv6tunlTEPv6AddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsv4overv6tunlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsv4overv6tunlTEPv6Addr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));
}

INT4
Fsv4overv6tunlMtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    return (nmhTestv2Fsv4overv6tunlMtu (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
}

INT4
Fsv4overv6tunlMtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsv4overv6tunlMtu (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
Fsv4overv6tunlMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsv4overv6tunlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsv4overv6tunlMtu (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &pMultiData->i4_SLongValue));
}

INT4
Fsv4overv6tunlAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    return (nmhTestv2Fsv4overv6tunlAdminStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));
}

INT4
Fsv4overv6tunlAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsv4overv6tunlAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
Fsv4overv6tunlAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsv4overv6tunlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsv4overv6tunlAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
GetNextIndexFsv4overv6tunlTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4fsv4overv6tunlIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsv4overv6tunlTable (&i4fsv4overv6tunlIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsv4overv6tunlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4fsv4overv6tunlIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4fsv4overv6tunlIndex;
    return SNMP_SUCCESS;
}
