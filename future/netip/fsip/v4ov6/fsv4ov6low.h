
/* Proto Validate Index Instance for Fsv4overv6tunlTable. */
INT1
nmhValidateIndexInstanceFsv4overv6tunlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsv4overv6tunlTable  */

INT1
nmhGetFirstIndexFsv4overv6tunlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsv4overv6tunlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsv4overv6tunlIpIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsv4overv6tunlTEPv6Addr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsv4overv6tunlMtu ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsv4overv6tunlAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsv4overv6tunlTEPv6Addr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsv4overv6tunlMtu ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsv4overv6tunlAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsv4overv6tunlTEPv6Addr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fsv4overv6tunlMtu ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsv4overv6tunlAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
