/* $Id: udpsz.c,v 1.5 2013/11/29 11:04:15 siva Exp $*/
#define _UDPSZ_C
#include "udpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
UdpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < UDP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsUDPSizingParams[i4SizingId].u4StructSize,
                                     FsUDPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(UDPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            UdpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
UdpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsUDPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, UDPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
UdpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < UDP_MAX_SIZING_ID; i4SizingId++)
    {
        if (UDPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (UDPMemPoolIds[i4SizingId]);
            UDPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
