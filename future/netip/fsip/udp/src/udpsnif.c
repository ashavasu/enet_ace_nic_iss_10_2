/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udpsnif.c,v 1.5 2013/08/26 12:22:20 siva Exp $
 *
 * Description:Network management support for UDP.          
 *
 *******************************************************************/

#include "udpinc.h"
#include "fsiplow.h"

/****************************************************************************
 Function    :  nmhGetFsUdpInBcast
 Input       :  The Indices

                The Object
                retValUdpInBcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUdpInBcast (UINT4 *pu4RetValUdpInBcast)
{
    UDP_DS_LOCK ();
    *pu4RetValUdpInBcast = gUdpGblInfo.UdpStat.u4InBcast;
    UDP_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUdpInErrCksum
 Input       :  The Indices

                The Object
                retValUdpInErrCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUdpInErrCksum (UINT4 *pu4RetValUdpInErrCksum)
{
    UDP_DS_LOCK ();
    *pu4RetValUdpInErrCksum = gUdpGblInfo.UdpStat.u4InErrCksum;
    UDP_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUdpInIcmpErr
 Input       :  The Indices

                The Object
                retValUdpInIcmpErr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUdpInIcmpErr (pu4RetValUdpInIcmpErr)
     UINT4              *pu4RetValUdpInIcmpErr;
{
    UDP_DS_LOCK ();
    *pu4RetValUdpInIcmpErr = gUdpGblInfo.UdpStat.u4InIcmpErr;
    UDP_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsUdpInNoCksum
 Input       :  The Indices

                The Object
                retValUdpInNoCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsUdpInNoCksum (pu4RetValUdpInNoCksum)
     UINT4              *pu4RetValUdpInNoCksum;
{
    UDP_DS_LOCK ();
    *pu4RetValUdpInNoCksum = gUdpGblInfo.UdpStat.u4InNoCksum;
    UDP_DS_UNLOCK ();
    return SNMP_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    :  ClearUdpDatagrams
 *
 * DESCRIPTION      : This function clear the Stats UDPdatagrams
 *
 * INPUT            : u4ContextId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
ClearUdpDatagrams (UINT4 u4ContextId)
{

    if (gUdpGblInfo.apUdpContextInfo[u4ContextId] == NULL)
    {
        return;
    }
    UDP_DS_LOCK ();
    gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4OutPkts = 0;
    gUdpGblInfo.UdpStat.u4OutPkts = 0;
    gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InPkts = 0;
    gUdpGblInfo.UdpStat.u4InPkts = 0;
    UDP_DS_UNLOCK ();
    return;
}

/******************************************************************************
 * FUNCTION NAME    :  UdpTstPopulateDatagrams
 *
 * DESCRIPTION      : This function clear the Stats UDPdatagrams
 *
 * INPUT            : u4ContextId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
UdpTstPopulateDatagrams (UINT4 u4ContextId)
{

    UINT4               u4Value = 5;
    if (gUdpGblInfo.apUdpContextInfo[u4ContextId] == NULL)
    {
        return;
    }
    UDP_DS_LOCK ();
    gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4OutPkts = u4Value;
    gUdpGblInfo.UdpStat.u4OutPkts = u4Value;
    gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InPkts = u4Value;
    gUdpGblInfo.UdpStat.u4InPkts = u4Value;
    UDP_DS_UNLOCK ();
    return;
}
