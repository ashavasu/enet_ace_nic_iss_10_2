/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udptskmg.c,v 1.15 2014/07/18 12:46:56 siva Exp $
 *
 * Description:Contains UDP task related functions and      
 *             functions related to Trace Route.         
 *
 *******************************************************************/
#include "udpinc.h"

static VOID Udp_timer_expiry_handler PROTO ((VOID));
extern tMemPoolId   gUdp4MemPoolId;
/*-------------------------------------------------------------------+
 * Function           : Ip_Udp_Task_Init
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * All the initializations of IP UDP task.
+-------------------------------------------------------------------*/
INT4
Ip_Udp_Task_Init (VOID)
{
    if (fs_udp_init () == FAILURE)
    {
        return FAILURE;
    }

    /* Call the Initailisation fn. of TRACE_ROUTE */
    trace_init_rec ();

    if (OsixQueCrt ((UINT1 *) UDP_CFG_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    UDP_CFG_Q_DEPTH, &gUdpGblInfo.UdpCfgQId) != OSIX_SUCCESS)
    {
        IP_TRC (UDP_MOD_TRC, ALL_FAILURE_TRC | OS_RESOURCE_TRC, UDP_NAME,
                "Failure in the creation of UDP CFG queue ...\n");
        UdpMemDeInit ();
        return FAILURE;
    }
    /* Create the UDP packet arrival Q */
    if (OsixQueCrt ((UINT1 *) UDP_INPUT_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    UDP_INPUT_Q_DEPTH,
                    &gUdpGblInfo.UdpInputQId) != OSIX_SUCCESS)
    {
        IP_TRC (UDP_MOD_TRC, ALL_FAILURE_TRC | OS_RESOURCE_TRC, UDP_NAME,
                "Failure in the creation of UDP queue ...\n");

        UdpMemDeInit ();
        return FAILURE;
    }

    /* Create the UDP packet arrival Q for Trace route */
    if (OsixQueCrt ((UINT1 *) UDP_TRCRT_INPUT_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    UDP_TRCRT_INPUT_Q_DEPTH,
                    &gUdpGblInfo.UdpTrcrtInputQId) != OSIX_SUCCESS)
    {
        IP_TRC (UDP_MOD_TRC, ALL_FAILURE_TRC | OS_RESOURCE_TRC, UDP_NAME,
                "Failure in the creation of UDP-TraceRoute input queue ...\n");

        UdpMemDeInit ();
        return FAILURE;
    }

    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : Ip_Udp_Task_Main
 *
 * Input(s)           : pDummy - Dummy pointer.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 * Main task processing in UDP.
+-------------------------------------------------------------------*/
VOID
Ip_Udp_Task_Main (INT1 *pDummy)
{
    tVcmRegInfo         VcmRegInfo;
    tNetIpRegInfo       RegInfo;
    UINT4               u4Event = 0;
    UINT4               u4RetVal = 0;
    UNUSED_PARAM (pDummy);

    if (OsixTskIdSelf (&gUdpGblInfo.UdpTaskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        IP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (Ip_Udp_Task_Init () != SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        IP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Register With VCM Module */
    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = UdpVcmCallbackFn;
    VcmRegInfo.u1InfoMask |= (VCM_IF_MAP_CHG_REQ | VCM_CXT_STATUS_CHG_REQ);
    VcmRegInfo.u1ProtoId = UDP_PROTOCOL_ID;
    VcmRegisterHLProtocol (&VcmRegInfo);

    /* Register UDP with IP */
    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));
    RegInfo.u1ProtoId = UDP_PTCL;
    RegInfo.u2InfoMask |= NETIPV4_PROTO_PKT_REQ;
    RegInfo.pProtoPktRecv = iptask_udp_input;
    RegInfo.pIfStChng = NULL;
    RegInfo.pRtChng = NULL;

    if (NetIpv4RegisterHigherLayerProtocol (&RegInfo) == NETIPV4_FAILURE)
    {
        UdpMemDeInit ();
        /* Indicate the status of initialization to the main routine */
        IP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

   /** Udp_Icmp_timer_list For Trace Module **/
    u4RetVal = UDP_CREATE_TMR_LIST ((const UINT1 *) UDP_TASK_NAME,
                                    UDP_ICMP_TIMER_0_EVENT,
                                    &gUdpGblInfo.UdpIcmpTimerListId);

    /* Indicate the status of initialization to the main routine */
    IP_INIT_COMPLETE (OSIX_SUCCESS);

    FOREVER
    {
        OsixEvtRecv (gUdpGblInfo.UdpTaskId,
                     (UDP_INPUT_Q_EVENT | UDP_ICMP_TIMER_0_EVENT |
                      UDP_TRCRT_INPUT_Q_EVENT | UDP_VCM_MSG_EVENT),
                     UDP_EVENT_WAIT_FLAGS, &u4Event);

        if (u4Event & UDP_INPUT_Q_EVENT)
        {
            UDP_Process_Input_Q_Msg ();
        }

        if (u4Event & UDP_TRCRT_INPUT_Q_EVENT)
        {
            UDP_Process_Input_Q_Event ();
        }

        if (u4Event & UDP_ICMP_TIMER_0_EVENT)
        {
            Udp_timer_expiry_handler ();
        }
        if (u4Event & UDP_VCM_MSG_EVENT)
        {
            /* Read VCM msg in separate Queue */
            UdpProcessCfgEvent ();

        }

    }
    UNUSED_PARAM (u4RetVal);
}

/*-------------------------------------------------------------------+
 * Function          : udp_task_enueue_to_applications_in_cxt
 *
 * Input(s)          : u4ContextId, u2Port, pBuf, i2Len, u2Task_id, u2Rcv_q_id,
                       u2Rcv_subq_id
 *
 * Output(s)         : None.
 *
 * Returns           : UDP_OK if success or UDP_NOT_OK if failure
 *
 * Action :
 *
 * Enqueues the  UDP packets to the applications depending
 * on the task and queue ids specified by them during udp_open.
 * Enqueuing depends on the target Operating system facilities.
 * Subqueue ID is used as an additional parameter. Currently it is put in
 * the destination module id field.
+-------------------------------------------------------------------*/
INT4
udp_task_enqueue_to_applications_in_cxt (UINT4 u4ContextId, UINT2 u2Port,
                                         tIpBuf * pBuf, INT2 i2Len,
                                         t_UDP_CB * pCtrlBlk, UINT4 u4SrcIp,
                                         UINT4 u4DstIp, UINT2 u2IpDatagramId,
                                         t_UDP * pUdpHdr)
{
    t_UDP_TO_APP_MSG_PARMS *pParms = NULL;

    pParms = (t_UDP_TO_APP_MSG_PARMS *) MemAllocMemBlk (gUdp4MemPoolId);
    if (pParms == NULL)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    MEMSET (pParms, 0, sizeof (t_UDP_TO_APP_MSG_PARMS));
    pParms->u4ContextId = u4ContextId;
    pParms->pBuf = pBuf;
    pParms->u1Cmd = IP_APP_DATA;
    pParms->u2Len = (UINT2) i2Len;
    /* Fill in context specific information */
    pParms->u1Type_of_msg = UDP_APP_DATA;
    pParms->Parms.Data.u2Port = u2Port;
    pParms->u4SrcIpAddr = u4SrcIp;
    pParms->u4DstIpAddr = u4DstIp;
    pParms->u2IpIdentification = u2IpDatagramId;
    pParms->u2SrcUdpPort = pUdpHdr->u2Src_u_port;
    pParms->u2DstUdpPort = pUdpHdr->u2Dest_u_port;

    if (OsixQueSend (pCtrlBlk->RcvQId,
                     (UINT1 *) &pParms, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        MemReleaseMemBlock (gUdp4MemPoolId, (UINT1 *) pParms);
        return IP_FAILURE;
    }

#ifdef SLI_WANTED
    if (pCtrlBlk->i4SockDesc >= 0)
    {
        OsixSemGive (pCtrlBlk->SemId);
        SliSelectScanList (pCtrlBlk->i4SockDesc, SELECT_READ);
    }
    else
#endif
    {
        OsixEvtSend (pCtrlBlk->TaskId, pCtrlBlk->u4RcvQEvent);
    }

    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function         : udp_task_enqueue_error_to_applications_in_cxt
 *
 * Input(s)         : u4ContextId, pBuf, i1Type, i1Code, u2Task_id, u2Err_q_id, 
                      u2Err_subq_id
 *
 * Output(s)        : None.
 *
 * Returns          : UDP_OK if success or UDP_NOT_OK if failure
 *
 * Action :
 *
 * Sends ICMP error messages to the respective applications.
 * Called only if the application is willing to accept the
 * error messages (as indicated during the enroll).
 * Sub queue ID is considered as module ID in this version.
+-------------------------------------------------------------------*/
INT4
udp_task_enqueue_error_to_applications_in_cxt (UINT4 u4ContextId, tIpBuf * pBuf,
                                               INT1 i1Type,
                                               INT1 i1Code,
                                               t_UDP_CB * pCtrlBlk,
                                               UINT4 u4SrcIpAddr)
{
    t_UDP_TO_APP_MSG_PARMS *pParms = NULL;

    pParms = (t_UDP_TO_APP_MSG_PARMS *) MemAllocMemBlk (gUdp4MemPoolId);
    if (pParms == NULL)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return FAILURE;
    }

    MEMSET (pParms, 0, sizeof (t_UDP_TO_APP_MSG_PARMS));
    pParms->u4ContextId = u4ContextId;
    pParms->pBuf = pBuf;

    /* Fill in context specific information */
    pParms->u1Type_of_msg = UDP_APP_ERROR;

    pParms->Parms.Err.i1Type = i1Type;
    pParms->Parms.Err.i1Code = i1Code;

    pParms->u4SrcIpAddr = u4SrcIpAddr;

    if (OsixQueSend (pCtrlBlk->RcvQId,
                     (UINT1 *) &pParms, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {

        IP_CXT_TRC (u4ContextId, UDP_MOD_TRC,
                    ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                    "Error in enqueuing the ICMP Error message to applications\n");
        IP_RELEASE_BUF (pBuf, FALSE);
        MemReleaseMemBlock (gUdp4MemPoolId, (UINT1 *) pParms);
        return FAILURE;
    }
#ifdef SLI_WANTED
    if (pCtrlBlk->i4SockDesc >= 0)
    {
        OsixSemGive (pCtrlBlk->SemId);
        SliSelectScanList (pCtrlBlk->i4SockDesc, SELECT_READ);
    }
#endif
    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : udp_task_ip_send_in_cxt
 *
 * Input(s)           :   u4ContextId - Context Identifier
                          u4Src      - Source IP Address                
                          u4Dest     - Destination IP Address  
                          i1Proto    - Protocol sending data  
                          i1Ttl      - Initial time to live   
                          i1Tos      - Type of Service Needed  
                          pBuf       - Allocated message buffer 
                          i2Len      - Length of Data          
                          i2Id       - Identification if needed 
                          i1Df       - Don't fragment flag     
                          i2Olen     - Length of IP options.   
                          u2Port     - IP handle of iface index 
 * Output(s)          : None.
 *
 * Returns            : SUCCESS/FAILURE
 *
 * Action :
 *
 * Send Routine to IP. Constructs the IP header and then
 * enqueues it to the IP.
 * Calls ip_construct_header directly to Fill up the header it -
 * may have to be avoided if the target OS does not support the
 * direct procedure calls.
+-------------------------------------------------------------------*/
INT4
udp_task_ip_send_in_cxt (UINT4 u4ContextId, UINT4 u4Src, UINT4 u4Dest,
                         INT1 i1Proto, INT1 i1Tos, INT1 i1Ttl,
                         tUDP_BUF_CHAIN_HEADER * pBuf, INT2 i2Len, INT2 i2Id,
                         INT1 i1Df, INT2 i2Olen, UINT2 u2Port)
{
    t_IP_SEND_PARMS    *pParms = NULL;

    pParms = (t_IP_SEND_PARMS *) IP_GET_MODULE_DATA_PTR (pBuf);
    pParms->u4ContextId = u4ContextId;
    pParms->u1Cmd = IP_LAYER4_DATA;
    pParms->u2Len = (UINT2) i2Len;

    pParms->u4Src = u4Src;
    pParms->u4Dest = u4Dest;
    pParms->u1Proto = (UINT1) i1Proto;
    pParms->u1Tos = (UINT1) i1Tos;
    pParms->u1Ttl = (UINT1) i1Ttl;
    pParms->u2Id = (UINT2) i2Id;
    pParms->u1Df = (UINT1) i1Df;
    pParms->u1Olen = (UINT1) i2Olen;
    pParms->u2Port = u2Port;

    if (IpEnquePktToIpFromHLWithCxtId (pBuf) != IP_SUCCESS)
    {
        return IP_FAILURE;
    }
    IP_CXT_TRC_ARG2 (u4ContextId, UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                     "UDP Packet of length %d to Destination %x "
                     "sent successfully \n", u4Dest, i2Len);

    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : udp_task_icmp_error_msg_in_cxt
 *
 * Input(s)           : pBuf   - Data to be attached with ICMP message  
                        i1Type - Type of this ICMP message            
                        u4Parm1- Parameters specific to msg type        
                        u4ContextId - Context Identifier
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *    Called when UDP wants to send an ICMP error message.
 *
+-------------------------------------------------------------------*/
INT4
udp_task_icmp_error_msg_in_cxt (UINT4 u4ContextId, tUDP_BUF_CHAIN_HEADER * pBuf,
                                INT1 i1Type, INT1 i1Code,
                                UINT4 u4Parm1, UINT4 u4Parm2)
{
    t_ICMP_MSG_PARMS   *pParms = NULL;

    pParms = (t_ICMP_MSG_PARMS *) IP_GET_MODULE_DATA_PTR (pBuf);
    pParms->u1Cmd = IP_ICMP_DATA;
    pParms->u4ContextId = u4ContextId;

    pParms->i1Error_or_request = TRUE;

    pParms->Msg.Err.i1Type = i1Type;
    pParms->Msg.Err.i1Code = i1Code;
    pParms->Msg.Err.u4Parm1 = u4Parm1;
    pParms->Msg.Err.u4Parm2 = u4Parm2;

    if (IpEnquePktToIpFromHL (pBuf) != IP_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }
    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : Udp_timer_expiry_handler
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Invoked when timer expiry event occurs.
 * Finds the expired timers and invokes the corresponding timer
 * routines. It is responsible for handling trace route
 * timers. Therefore it interfaces with ICMP routines and also TRACE Route
 * routines.
+-------------------------------------------------------------------*/
static VOID
Udp_timer_expiry_handler (VOID)
{
    tTMO_APP_TIMER     *pList_head = NULL;
    tTMO_APP_TIMER     *pTimer = NULL;
    UINT1               u1Id = 0;

    UDP_PROT_LOCK ();

    UDP_GET_EXPIRED_TIMERS (gUdpGblInfo.UdpIcmpTimerListId, &pList_head);

    while (pList_head != NULL)
    {
        pTimer = pList_head;
        pList_head = UDP_NEXT_TIMER (gUdpGblInfo.UdpIcmpTimerListId);    /* Save it */

        u1Id = ((t_IP_TIMER *) pTimer)->u1Id;

        switch (u1Id)
        {
            case IP_TRACE_END_TIMER_ID:
            {
                t_TRACE            *pTrace = NULL;

                pTrace = (t_TRACE *) ((t_IP_TIMER *) pTimer)->pArg;
                trace_end_timeout (pTrace);
                break;
            }

            case IP_TRACE_TIMER_ID:
            {
                t_TRACE            *pTrace = NULL;

                pTrace = (t_TRACE *) ((t_IP_TIMER *) pTimer)->pArg;
                trace_send (pTrace);
                break;
            }

            default:
                break;
        }
    }
    UDP_PROT_UNLOCK ();
}

/*-------------------------------------------------------------------+
 *
 * Function           : iptask_udp_input
 *
 * Input(s)           : pBuf, i2Len, u4Port, InterfaceId, u1Flag, u4Iface_id
                        pBuf -  Data buffer .. contains IP header also   
                        u2Len - Length of IP data .. excluding IP header 
                                    and options 
                        u4Port - Interface index   .. IP Handle
                        InterfaceId -  Actual Iface structure  
                        u1Flag - mode of receive ;  broadcast, multicast 
                                      or unicast   
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :  Sets up parameters and queues msg to UDP
 *
+-------------------------------------------------------------------*/
VOID
iptask_udp_input (tUDP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, UINT4 u4Port,
                  tUDP_INTERFACE InterfaceId, UINT1 u1Flag)
{
    t_IP_TO_HLMS_PARMS *pParms = NULL;

    UNUSED_PARAM (InterfaceId);

    pParms = (t_IP_TO_HLMS_PARMS *) MemAllocMemBlk (gUdpGblInfo.UdpInputPoolId);
    if (pParms == NULL)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    MEMSET (pParms, 0, sizeof (t_IP_TO_HLMS_PARMS));
    pParms->pBuf = pBuf;
    VcmGetContextInfoFromIpIfIndex (u4Port, &(pParms->u4ContextId));
    pParms->u1Cmd = IP_PROTOCOL_ID;
    pParms->u2Len = u2Len;
    pParms->u2Port = (UINT2) u4Port;
    pParms->u1Flag = u1Flag;

    if (OsixQueSend (gUdpGblInfo.UdpInputQId, (UINT1 *) &pParms,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_CXT_TRC_ARG1 (pParms->u4ContextId, UDP_MOD_TRC,
                         ALL_FAILURE_TRC | BUFFER_TRC, UDP_NAME,
                         "Failure in Enqueuing the packet from IP to UDP - "
                         "Releasing Buffer %x \n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        MemReleaseMemBlock (gUdpGblInfo.UdpInputPoolId, (UINT1 *) pParms);
        return;
    }

    OsixEvtSend (gUdpGblInfo.UdpTaskId, UDP_INPUT_Q_EVENT);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : iptask_icmp_udp_in_cxt
 *
 * Input(s)           : u4contextId, pBuf, u4Src, i1Type, i1Code
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :
 * Enqueues message to UDP
 *
+-------------------------------------------------------------------*/
INT4
iptask_icmp_udp_in_cxt (UINT4 u4ContextId,
                        tUDP_BUF_CHAIN_HEADER * pBuf, UINT4 u4Src, INT1 i1Type,
                        INT1 i1Code)
{
    t_ICMP_ERR_TO_HLMS_PARMS *pParms = NULL;

    pParms =
        (t_ICMP_ERR_TO_HLMS_PARMS *) MemAllocMemBlk (gUdpGblInfo.
                                                     UdpInputPoolId);
    if (pParms == NULL)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return FAILURE;
    }

    MEMSET (pParms, 0, sizeof (t_ICMP_ERR_TO_HLMS_PARMS));
    pParms->pBuf = pBuf;
    pParms->u4ContextId = u4ContextId;
    pParms->u1Cmd = ICMP_PROTOCOL_ID;
    pParms->u4Src = u4Src;
    pParms->i1Type = i1Type;
    pParms->i1Code = i1Code;

    if (OsixQueSend (gUdpGblInfo.UdpInputQId, (UINT1 *) &pParms,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {

        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC,
                         UDP_NAME,
                         "Failure in Enqueuing the ICMP error packet from IP "
                         "to UDP - Releasing Buffer %x \n", pBuf);
        IP_RELEASE_BUF (pBuf, FALSE);
        MemReleaseMemBlock (gUdpGblInfo.UdpInputPoolId, (UINT1 *) pParms);
        return IP_FAILURE;
    }

    OsixEvtSend (gUdpGblInfo.UdpTaskId, UDP_INPUT_Q_EVENT);
    return SUCCESS;
}

VOID
UDP_Process_Input_Q_Msg (VOID)
{
    tIpBuf             *pBuf = NULL;
    t_IP_TO_HLMS_PARMS *pParms = NULL;

    while (OsixQueRecv (gUdpGblInfo.UdpInputQId,
                        (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {

        pParms = (t_IP_TO_HLMS_PARMS *) pBuf;
        UDP_PROT_LOCK ();

        switch (pParms->u1Cmd)
        {

            case IP_PROTOCOL_ID:
            {
                tUDP_INTERFACE      IfId;
                MEMSET (&IfId, 0, sizeof (tUDP_INTERFACE));

                /* InterfaceId is not used by fs_udp_input at all 
                 * just to fill in some thing
                 */
                IP_GET_IF_INDEX (IfId) = (UINT4) pParms->u2Port;

                fs_udp_input_in_cxt (pParms->u4ContextId, pParms->pBuf,
                                     (INT2) pParms->u2Len, pParms->u2Port, IfId,
                                     pParms->u1Flag);

                break;
            }

            case ICMP_PROTOCOL_ID:
            {
                t_ICMP_ERR_TO_HLMS_PARMS *pIcmpParms;

                pIcmpParms = (t_ICMP_ERR_TO_HLMS_PARMS *) pBuf;

                udp_icmp_input_in_cxt (pIcmpParms->u4ContextId,
                                       pIcmpParms->pBuf, pIcmpParms->u4Src,
                                       pIcmpParms->i1Type, pIcmpParms->i1Code);

                break;
            }

            default:
                break;

        }
        MemReleaseMemBlock (gUdpGblInfo.UdpInputPoolId, (UINT1 *) pBuf);
        UDP_PROT_UNLOCK ();
    }
}

VOID
UDP_Process_Input_Q_Event (VOID)
{
    tIpBuf             *pBuf = NULL;

    while (OsixQueRecv (gUdpGblInfo.UdpTrcrtInputQId,
                        (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        t_UDP_TO_APP_MSG_PARMS *pParms = NULL;

        pParms = (t_UDP_TO_APP_MSG_PARMS *) IP_GET_MODULE_DATA_PTR (pBuf);

        UDP_PROT_LOCK ();

        switch (pParms->u1Type_of_msg)
        {

            case UDP_APP_DATA:

                trace_input (pParms->Parms.Data.u2Port, pBuf,
                             (INT2) (IP_GET_BUF_LENGTH (pBuf) -
                                     UDP_HDR_LEN), pParms->u4SrcIpAddr,
                             pParms->u2IpIdentification);
                break;
            case UDP_APP_ERROR:

                trace_error (pBuf, pParms->Parms.Err.i1Type,
                             pParms->Parms.Err.i1Code, pParms->u4SrcIpAddr);
                break;

            default:
                IP_RELEASE_BUF (pBuf, FALSE);
                break;
        }

        UDP_PROT_UNLOCK ();

    }                            /* while */
}

/*-------------------------------------------------------------------+
 * Function           : UdpProcessCfgEvent
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action             : Process the message received from VCM.
 *
+-------------------------------------------------------------------*/

VOID
UdpProcessCfgEvent (VOID)
{
    tIpBuf             *pBuf = NULL;
    tUdpCfgMsg         *pParms = NULL;

    UDP_PROT_LOCK ();
    while (OsixQueRecv (gUdpGblInfo.UdpCfgQId,
                        (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        pParms = (tUdpCfgMsg *) pBuf;

        if (pParms->u4MsgType != UDP_VCM_MSG_RCVD)
        {
            UDP_PROT_UNLOCK ();
            return;
        }

        switch (pParms->u1BitMap)
        {
            case VCM_CXT_STATUS_CHG_REQ:

                UdpProcessCxtDeletion (pParms->u4ContextId);
                break;

            default:
                break;
        }
        MemReleaseMemBlock (gUdpGblInfo.UdpCfgPoolId, (UINT1 *) pParms);
        UDP_PROT_UNLOCK ();
        return;
    }
    UDP_PROT_UNLOCK ();
    return;
}

/*-------------------------------------------------------------------+
 * Function           : UdpVcmCallbackFn
 *
 * Input(s)           : U4IfIndex - Interface index
 *                      u4VcmCxtId - ContextId
 *                      u1BitMap - Message Type
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action             : Callback function registered with VCM.
 *
+-------------------------------------------------------------------*/
VOID
UdpVcmCallbackFn (UINT4 u4IfIndex, UINT4 u4VcmCxtId, UINT1 u1BitMap)
{
    tUdpCfgMsg         *pParms = NULL;
    tNetIpRegInfo       RegInfo;

    UNUSED_PARAM (u4IfIndex);

    if (u1BitMap & VCM_CONTEXT_CREATE)
    {
        /* Register UDP with IP */
        MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));
        RegInfo.u1ProtoId = UDP_PTCL;
        RegInfo.u2InfoMask |= NETIPV4_PROTO_PKT_REQ;
        RegInfo.pProtoPktRecv = iptask_udp_input;
        RegInfo.pIfStChng = NULL;
        RegInfo.pRtChng = NULL;
        RegInfo.u4ContextId = u4VcmCxtId;

        NetIpv4RegisterHigherLayerProtocol (&RegInfo);
        return;
    }

    pParms = (tUdpCfgMsg *) MemAllocMemBlk (gUdpGblInfo.UdpCfgPoolId);
    if (pParms == NULL)
    {
        return;
    }
    MEMSET (pParms, 0, sizeof (tUdpCfgMsg));
    pParms->u4ContextId = u4VcmCxtId;
    pParms->u1BitMap = u1BitMap;
    pParms->u4MsgType = UDP_VCM_MSG_RCVD;

    if (OsixQueSend (gUdpGblInfo.UdpCfgQId, (UINT1 *) &pParms,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_CXT_TRC_ARG1 (u4VcmCxtId, UDP_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC,
                         UDP_NAME,
                         "Failure in Enqueuing the packet from VCM to UDP - "
                         "Releasing Buffer %x \n", pParms);
        MemReleaseMemBlock (gUdpGblInfo.UdpCfgPoolId, (UINT1 *) pParms);
        return;
    }

    OsixEvtSend (gUdpGblInfo.UdpTaskId, UDP_VCM_MSG_EVENT);
}
