/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udpmain.c,v 1.26 2014/01/20 12:15:44 siva Exp $
 *
 * Description: User Datagram Protocol. Contains send,
 *              data receive, error receive functionalities.  
 *
 *******************************************************************/
#include "udpinc.h"

static INT4 udp_put_hdr PROTO ((INT2 u2Len, UINT4 u4Dest,
                                UINT4 u4Src_addr, t_UDP * pUdpHdr,
                                tUDP_BUF_CHAIN_HEADER * pBuf, UINT1 u1CkOpt));
static INT4 udp_extract_hdr PROTO ((t_UDP * pUdp,
                                    tUDP_BUF_CHAIN_HEADER * pBuf));
static UINT2 udp_cksum PROTO ((INT2 i2Len, UINT4 u4Dest,
                               UINT4 u4Src, t_UDP * pUdpHdr,
                               tUDP_BUF_CHAIN_HEADER * pBuf));

static UINT2 GetFreeLocalUdpPort PROTO ((VOID));
tUdpGblInfo         gUdpGblInfo;
/*-------------------------------------------------------------------+
 * Function           : fs_udp_init
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action             : Initialization for UDP.
 *
+-------------------------------------------------------------------*/
INT4
fs_udp_init (VOID)
{
    INT4                i4Ret = OSIX_FAILURE;
    UINT4               u4CxtId = 0;
    tUdpCxt            *pUdpCxt = NULL;

    /* Initialise UDP global structures */
    gUdpGblInfo.u4UdpCurrentCxtId = VCM_INVALID_VC;
    for (u4CxtId = 0; u4CxtId <= UDP_SIZING_CONTEXT_COUNT; u4CxtId++)
    {
        gUdpGblInfo.apUdpContextInfo[u4CxtId] = NULL;
    }
    gUdpGblInfo.pUdpCurrentCxt = NULL;
    TMO_SLL_Init (&(gUdpGblInfo.UdpCbEntry));
    MEMSET (&(gUdpGblInfo.UdpStat), 0, sizeof (t_UDP_STAT));
    gUdpGblInfo.UdpCBPoolId = 0;
    gUdpGblInfo.UdpCxtPoolId = 0;
    gUdpGblInfo.UdpCfgPoolId = 0;
    gUdpGblInfo.UdpDSem = 0;
    gUdpGblInfo.UdpSem = 0;
    gUdpGblInfo.UdpIcmpTimerListId = 0;
    gUdpGblInfo.UdpInputQId = 0;
    gUdpGblInfo.UdpTrcrtInputQId = 0;
    gUdpGblInfo.UdpCfgQId = 0;
    gUdpGblInfo.u4DefSockMode = SOCK_GLOBAL_MODE;

    /* Create UDP semaphores */
    if (OsixSemCrt (UDP_CTRLTBL_SEMAPHORE, &gUdpGblInfo.UdpDSem) !=
        OSIX_SUCCESS)
    {
        IP_TRC (UDP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, UDP_NAME,
                "UDP DataSem Creation failed. \n");
        return FAILURE;
    }
    OsixSemGive (gUdpGblInfo.UdpDSem);

    if (OsixSemCrt (UDP_PROTOCOL_SEMAPHORE, &gUdpGblInfo.UdpSem) !=
        OSIX_SUCCESS)
    {
        IP_TRC (UDP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, UDP_NAME,
                "UDP Task Sem Creation failed. \n");
        OsixSemDel (gUdpGblInfo.UdpDSem);
        return FAILURE;
    }
    OsixSemGive (gUdpGblInfo.UdpSem);

    i4Ret = UdpSizingMemCreateMemPools ();
    if (i4Ret == OSIX_FAILURE)
    {
        return FAILURE;
    }
    gUdpGblInfo.UdpCxtPoolId = UDPMemPoolIds[MAX_UDP_CONTEXTS_SIZING_ID];
    gUdpGblInfo.UdpCBPoolId = UDPMemPoolIds[MAX_UDP_CB_TAB_SIZE_SIZING_ID];
    gUdpGblInfo.UdpCfgPoolId = UDPMemPoolIds[MAX_UDP_CFG_MSGS_SIZING_ID];
    gUdpGblInfo.UdpInputPoolId = UDPMemPoolIds[MAX_UDP_HL_PARMS_MSGS_SIZING_ID];

    /* Create entry for Default context */
    if (UDP_ALLOC_CXT_ENTRY (pUdpCxt) == NULL)
    {
        UdpMemDeInit ();
        return FAILURE;
    }
    /* Intialize the Default context control block entry */
    MEMSET (&(pUdpCxt->UdpStat), 0, sizeof (t_UDP_STAT));
    TMO_SLL_Init (&(pUdpCxt->UdpCxtCbEntry));
    pUdpCxt->u4ContextId = VCM_DEFAULT_CONTEXT;
    gUdpGblInfo.apUdpContextInfo[VCM_DEFAULT_CONTEXT] = pUdpCxt;
    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : udp_trcrt_open
 *
 * Input(s)           : u2U_port, u4Local_addr
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * Validates the Local Address and Open a Port for TraceRoute Utility
+-------------------------------------------------------------------*/
INT4
udp_trcrt_open (UINT2 u2U_port, UINT4 u4Local_addr)
{
    t_UDP_CB           *ptUdp_cb;
    UINT4               u4ContextId = 0;
    UINT1               u1UdpCBMode = 0;

    /* If local address is specified it should be a valid one */
    if ((u4Local_addr != 0) &&
        ((udp_task_ip_verify_addr (u4Local_addr, IP_ADDR_SRC) == FAILURE) ||
         (udp_task_ip_verify_addr (u4Local_addr, IP_ADDR_DEST) == FAILURE)))
    {

        IP_TRC_ARG1 (UDP_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                     "%x is a invalid Local Address \n", u4Local_addr);

        return FAILURE;
    }

    if (udp_get_cb_in_cxt (u4ContextId, u2U_port, u4Local_addr,
                           &ptUdp_cb, &u1UdpCBMode) == FAILURE)
    {
        udp_open_trcrt_port (u2U_port, u4Local_addr);

        IP_TRC_ARG2 (UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                     "Udp Open for port %d and address %x is Successful \n",
                     u2U_port, u4Local_addr);

        return SUCCESS;
    }

    IP_TRC_ARG1 (UDP_MOD_TRC, ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                 "Port %d is Busy ...\n", u2U_port);

    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : udp_open_trcrt_port
 * 
 * Input(s)           : u2U_port, u4Local_addr
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action             : Open the UDP port for Trace Route Facility
 *
+-------------------------------------------------------------------*/
INT4
udp_open_trcrt_port (UINT2 u2U_port, UINT4 u4Local_addr)
{
    UNUSED_PARAM (u2U_port);
    UNUSED_PARAM (u4Local_addr);
    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : udp_close_srcaddr_in_cxt
 *
 * Input(s)           : u4ContextId, u2U_port, u4Local_addr
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * De-enrolls application from UDP.
 *
+-------------------------------------------------------------------*/
INT4
udp_close_srcaddr_in_cxt (UINT4 u4ContextId, UINT2 u2U_port, UINT4 u4Local_addr)
{
    t_UDP_CB           *pUdp_cb;
    UINT1               u1UdpCBMode = 0;
    INT4                i4RetVal = 0;
    if (u2U_port == 0)
    {
        return SUCCESS;
    }

    /* Get UDP entry for this local port number and 
     * local ip address */
    i4RetVal = udp_get_cb_in_cxt (u4ContextId, u2U_port, u4Local_addr, &pUdp_cb,
                                  &u1UdpCBMode);

    /* The UDP entry will be in UDP global table
     * or UDP context specific table */
    if (u1UdpCBMode == SOCK_UNIQUE_MODE)
    {
        if (UdpDeleteCBNodeFromCxtTable (u4ContextId, pUdp_cb) == SUCCESS)
        {
            return SUCCESS;
        }
    }
    else
    {
        if (UdpDeleteCBNodeFromGlbTable (pUdp_cb) == SUCCESS)
        {
            return SUCCESS;
        }
    }
    UNUSED_PARAM (i4RetVal);
    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : fs_udp_input_in_cxt
 *
 * Input(s)           : u4ContextId, pBuf, i2Len, u2Port, InterfaceId, u1Flag
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * Called from IP whenever a packet arrives with UDP as protocol.
 * Calculates checksum, finds out application for this port
 * and then queues it for that application.
 *
 * The format of the pBuf expected is
 *
 *                 <------------ i2Len ---------------->
 * +---------------+----------------+------------------+
 * | IP HDR FILLED | UDP HDR FILLED | UDP DATA         |
 * +---------------+----------------+------------------+
+-------------------------------------------------------------------*/
INT4
fs_udp_input_in_cxt (UINT4 u4ContextId, tUDP_BUF_CHAIN_HEADER * pBuf,
                     INT2 i2Len, UINT2 u2Port,
                     tUDP_INTERFACE InterfaceId, UINT1 u1Flag)
{
    t_UDP_CB           *ptUdp_cb;
    t_UDP               UdpHdr;
    UINT4               u4Ip_dest = 0;
    UINT4               u4Ip_src = 0;
    UINT1               au1Ip_verlen_src_dest[IP_HDR_LEN];
    INT1                i1Ver4Hlen4 = 0;
    UINT2               u2IpDatagramId = 0;
    UINT2               u2TotLen = 0;
    UINT1               u1UdpCBMode = 0;
    INT1                i1Status = FALSE;
    UINT4               u4PktLen = 0;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (InterfaceId.u1_InterfaceType);

    /* This Macro Gets the First 5 Bytes in the IP Header. */
    IP_PKT_GET_VERLEN_SRC_DEST (pBuf, au1Ip_verlen_src_dest);

    /*  Extracting the Version and the Header Length. */
    i1Ver4Hlen4 = (INT1) (IP_BYTE_ALL_ONE & au1Ip_verlen_src_dest[0]);

    /* Extracting total length of the packet */
    MEMCPY (&u2TotLen, (&au1Ip_verlen_src_dest[IP_PKT_OFF_LEN]),
            sizeof (UINT2));
    u2TotLen = IP_NTOHS (u2TotLen);

    /*  Extracting the Source Ip Address  */
    MEMCPY (&u4Ip_src, (&au1Ip_verlen_src_dest[IP_PKT_OFF_SRC]),
            sizeof (UINT4));
    u4Ip_src = IP_NTOHL (u4Ip_src);

    /*  Extracting the Dest Ip Address. */
    MEMCPY (&u4Ip_dest, (&au1Ip_verlen_src_dest[IP_PKT_OFF_DEST]),
            sizeof (UINT4));
    u4Ip_dest = IP_NTOHL (u4Ip_dest);
    MEMCPY (&u2IpDatagramId, (&au1Ip_verlen_src_dest[IP_PKT_OFF_ID]),
            sizeof (UINT2));
    u2IpDatagramId = IP_NTOHS (u2IpDatagramId);

    /* Get Packet buffer length */
    u4PktLen = IP_GET_BUF_LENGTH (pBuf);

    /* Moves the Read Offset to the Starting of the Udp Header. */
    IP_BUF_MOVE_VALID_OFFSET (pBuf, (UINT4) ((i1Ver4Hlen4 & 0x0f) * IP_FOUR));

    /* Extracting the Header. */
    udp_extract_hdr (&UdpHdr, pBuf);

    /* Get control block entry for this port and address */
    if (udp_get_cb_in_cxt (u4ContextId, UdpHdr.u2Dest_u_port,
                           u4Ip_dest, &ptUdp_cb, &u1UdpCBMode) == SUCCESS)
    {
        i1Status = TRUE;
    }
    else if (udp_get_cb_in_cxt (u4ContextId, UdpHdr.u2Dest_u_port,
                                0, &ptUdp_cb, &u1UdpCBMode) == SUCCESS)
    {
        i1Status = TRUE;
    }

    if ((u4PktLen) < ((UINT4) ((i1Ver4Hlen4 & 0x0f) * IP_FOUR + UDP_HDR_LEN)))
    {
        UDP_STAT_ERR_INC (u1UdpCBMode, u4ContextId);
        UDP_STAT_HC_IN_INC (u1UdpCBMode, u4ContextId);
        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC,
                         UDP_NAME, "Bad length - Releasing the buffer %x \n",
                         pBuf);

        IP_RELEASE_BUF (pBuf, FALSE);
        return FAILURE;
    }

    IP_BUF_MOVE_VALID_OFFSET (pBuf, UDP_HDR_LEN);

    if ((UdpHdr.u2Len < UDP_HDR_LEN) || (UdpHdr.u2Len >= u2TotLen))
    {
        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC,
                         UDP_NAME,
                         "UDP header length %d is invalid - Releasing the buffer \n",
                         UdpHdr.u2Len);

        IP_RELEASE_BUF (pBuf, TRUE);
        UDP_STAT_ERR_INC (u1UdpCBMode, u4ContextId);
        return FAILURE;
    }

    /* calculate the checksum Only If there is cksum */
    if (UdpHdr.u2Cksum != UDP_NO_CHECKSUM)
    {
        if (udp_cksum ((INT2) UdpHdr.u2Len, u4Ip_dest, u4Ip_src, &UdpHdr, pBuf)
            != UDP_VALID_CHECKSUM)
        {

            IP_CXT_TRC_ARG3 (u4ContextId, UDP_MOD_TRC,
                             ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                             "Checksum failed for UDP Packet arrived on Port - %d "
                             "from SRC - %x to UDP Port - %d \n", u2Port,
                             u4Ip_src, UdpHdr.u2Dest_u_port);

            IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                             ALL_FAILURE_TRC | BUFFER_TRC, UDP_NAME,
                             "Bad Checksum - Releasing the buffer %x \n", pBuf);

            IP_RELEASE_BUF (pBuf, TRUE);
            UDP_STAT_ERR_CHKSUM_INC (u1UdpCBMode, u4ContextId);
            return FAILURE;
        }
    }
    else
    {
        UDP_STAT_NO_CHKSUM_INC (u1UdpCBMode, u4ContextId);
    }

    if ((u1Flag == IP_BCAST) || (u1Flag == IP_MCAST))
    {
        UDP_STAT_BCAST_INC (u1UdpCBMode, u4ContextId);
    }

    /*
     * We need not care about multicast packets because when 
     * it comes here it will be addressed to a particuler port.
     */
    if (i1Status == TRUE)
    {
        /* If control block is in context mode, update
         * the statistics in that particular context
         * Otherwise update the global statistics */
        UDP_STAT_IN_INC (u1UdpCBMode, u4ContextId);
        UDP_STAT_HC_IN_INC (u1UdpCBMode, u4ContextId);
        UDP_STAT_SUCC_INC (u1UdpCBMode, u4ContextId);
        i2Len = (INT2) (i2Len - UDP_HDR_LEN);
        i4RetVal = udp_task_enqueue_to_applications_in_cxt
            (u4ContextId, u2Port, pBuf, i2Len, ptUdp_cb, u4Ip_src,
             u4Ip_dest, u2IpDatagramId, &UdpHdr);
        return i4RetVal;
    }
    else
    {
        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                         ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                         "Port %d is unreachable \n", UdpHdr.u2Dest_u_port);
        UDP_STAT_NO_PORT_INC (u1UdpCBMode, u4ContextId);
    }

    /* Checking whether the Given address is a Broadcast address */
    if (IpGetDefaultBroadcastAddr (u4Ip_dest) == u4Ip_dest)
    {
        /* 
         * Since the Destination address is Broadcast there is
         * no need to send the ICMP error to the Src Application.
         */

        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC, ALL_FAILURE_TRC | BUFFER_TRC,
                         UDP_NAME,
                         "No ICMP Error for unreachable port "
                         "to a Broadcast address .Releasing the buffer %x \n",
                         pBuf);

        IP_RELEASE_BUF (pBuf, FALSE);
        return FAILURE;
    }

    /* Requested port is not present */
    IP_PREPEND_BUF (pBuf, NULL, (UINT4) ((i1Ver4Hlen4 & 0x0f) * IP_FOUR));
    IP_PREPEND_BUF (pBuf, NULL, UDP_HDR_LEN);

    udp_task_icmp_error_msg_in_cxt (u4ContextId, pBuf, ICMP_DEST_UNREACH,
                                    ICMP_PORT_UNREACH, 0, 0);

    IP_CXT_TRC_ARG2 (u4ContextId, UDP_MOD_TRC, DATA_PATH_TRC, UDP_NAME,
                     "Sending Port unreachable for Port - %d and Dest - %x \n",
                     UdpHdr.u2Dest_u_port, u4Ip_src);

    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : udp_send_srcaddr_in_cxt
 *
 * Input(s)           : u4contextId, u4Src, u2Src_u_port, u4Dest, 
 *                      u2Dest_u_port, pBuf, i2Len,
 *                      i1Tos, i1Ttl, i2Id, i1Df, i2Olen, u2Port
 *                      The Buffer's valid offset should be set to start
 *                      of UDP Data.
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * Called from an application whenever an UDP datagram is to be sent.
 * Fills in UDP header and then calls ip_send to send it to IP.
 * Enough buffer should be allocated to acomodate
 * UDP header, IP header and data. i2Olen should contain 0 if there are no
 * options specified.
 *
 * Format of pBuf expected is
 *                    <---- i2Olen ---->                      <-- i2Len --->
 * +------------------+----------------+----------------------+------------+
 * | Space for IP HDR | Options Filled | Space for UDP header | Appln Data |
 * +------------------+----------------+----------------------+------------+
+-------------------------------------------------------------------*/
INT4
udp_send_srcaddr_in_cxt (UINT4 u4ContextId, UINT4 u4Src, UINT2 u2Src_u_port,
                         UINT4 u4Dest, UINT2 u2Dest_u_port,
                         tUDP_BUF_CHAIN_HEADER * pBuf, INT2 i2Len, INT1 i1Tos,
                         INT1 i1Ttl, INT2 i2Id, INT1 i1Df, INT2 i2Olen,
                         UINT2 u2Port, UINT1 u1_Cksum_wanted)
{
    tIfConfigRecord     tIpIfInfo;
    t_UDP               UdpHdr;
    t_UDP_CB           *ptUdp_cb;
    UINT4               u4Src_addr = u4Src;
    UINT1               u1UdpCBMode = 0;
    INT4                i4RetVal = FAILURE;

    if (((u2Dest_u_port == 0)
         || (udp_task_ip_verify_addr (u4Dest, IP_ADDR_DEST) == FAILURE)))
    {
        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                         ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                         "Invalid Dest Addr-Port %d \n", u2Port);
        IP_RELEASE_BUF (pBuf, FALSE);
        return FAILURE;
    }

    UdpHdr.u2Dest_u_port = u2Dest_u_port;
    UdpHdr.u2Len = (UINT2) (i2Len + UDP_HDR_LEN);
    UdpHdr.u2Cksum = 0;

    /* Allocate euphemeral port */
    if (u2Src_u_port == 0)
    {
        u2Src_u_port = GetFreeLocalUdpPort ();
        UdpHdr.u2Src_u_port = u2Src_u_port;
    }
    else
    {
        UdpHdr.u2Src_u_port = u2Src_u_port;
    }

    i4RetVal = udp_get_cb_in_cxt (u4ContextId, u2Src_u_port, u4Src,
                                  &ptUdp_cb, &u1UdpCBMode);
    if (u2Port != IPIF_INVALID_INDEX)
    {
        /* We know the interface Find out address on that */
        if (IpGetIfConfigRecord (u2Port, &tIpIfInfo) != IP_SUCCESS)
        {
            IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                             ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                             "Invalid Interface Index %d \n", u2Port);
            IP_RELEASE_BUF (pBuf, FALSE);
            return FAILURE;
        }

        if (u4Src == 0)
        {
            u4Src_addr = tIpIfInfo.u4Addr;
        }
    }
    else
    {
        /* Udp Control table lookup with source port & Src Addr */
        if ((i4RetVal == SUCCESS) && (ptUdp_cb->u4Local_addr != 0))
        {
            u4Src_addr = ptUdp_cb->u4Local_addr;
        }
        else
        {
            /*
             * IP will have to find route and then return
             * the address of the iface of that route
             */
            if (udp_task_ip_src_addr_to_use_for_dest_in_cxt
                (u4ContextId, u4Dest, &u4Src_addr) == FAILURE)
            {
                IP_RELEASE_BUF (pBuf, FALSE);
                IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                                 ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                                 "Packet to %x dropped-No route to destination \n",
                                 u4Dest);
                return FAILURE;
            }
        }
    }

    /* Pass the information of pseudo header to calculate the checksum */
    udp_put_hdr ((INT2) UdpHdr.u2Len, u4Dest, u4Src_addr, &UdpHdr, pBuf,
                 (UINT1) (u1_Cksum_wanted > 0));

    UDP_STAT_OUT_INC (u1UdpCBMode, u4ContextId);
    UDP_STAT_HC_OUT_INC (u1UdpCBMode, u4ContextId);

    return (udp_task_ip_send_in_cxt
            (u4ContextId, u4Src_addr, u4Dest, UDP_PTCL, i1Tos, i1Ttl, pBuf,
             (INT2) UdpHdr.u2Len, i2Id, i1Df, i2Olen, u2Port));
}

/*-------------------------------------------------------------------+
 * Function           : udp_get_port
 *
 * Input(s)           : u2U_port
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS / FAILURE
 *
 * Action :
 *
 * Routine to get the entry corresponding to the udp port.
+-------------------------------------------------------------------*/
INT4
udp_get_port (UINT2 u2U_port, t_UDP_CB * ptUdp_cb)
{
    t_UDP_CB           *pUdp_cb = NULL;
    tUdpCxt            *pUdpCxt = NULL;
    UINT4               u4ContextId = 0;

    UDP_DS_LOCK ();
    for (u4ContextId = 0; u4ContextId <= UDP_SIZING_CONTEXT_COUNT;
         u4ContextId++)
    {
        pUdpCxt = UdpGetCxtEntryFromCxtId (u4ContextId);
        if (pUdpCxt != NULL)
        {
            /* Search the CB entry in Context UDP CB table */
            TMO_SLL_Scan (&(pUdpCxt->UdpCxtCbEntry), pUdp_cb, t_UDP_CB *)
            {
                if (pUdp_cb->u2U_port == u2U_port)
                {
                    MEMCPY (ptUdp_cb, pUdp_cb, sizeof (t_UDP_CB));
                    UDP_DS_UNLOCK ();
                    return SUCCESS;
                }

            }
        }
    }
    /* Search the CB entry in Global UDP CB table */
    TMO_SLL_Scan (&(gUdpGblInfo.UdpCbEntry), pUdp_cb, t_UDP_CB *)
    {
        if (pUdp_cb->u2U_port == u2U_port)
        {
            MEMCPY (ptUdp_cb, pUdp_cb, sizeof (t_UDP_CB));
            UDP_DS_UNLOCK ();
            return SUCCESS;
        }
    }
    /* u2U_port is free port */
    UDP_DS_UNLOCK ();
    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : udp_get_cb
 *
 * Input(s)           : u2U_port - UDP port number
 *                      u4LocalAddr - Local IP address
 *
 * Output(s)          : ptUdp_cb - pointer to the UDP entries
 *                      pu1UdpCBMode - global or unique mode
 *
 * Returns            : SUCCESS/FAILURE
 *
 * Routine to get the entry corresponding to the udp port.
+-------------------------------------------------------------------*/
INT4
udp_get_cb (UINT2 u2U_port, UINT4 u4LocalAddr,
            t_UDP_CB * ptUdp_cb, UINT1 *pu1UdpCBMode)
{
    t_UDP_CB           *pUdp_cb = NULL;
    tUdpCxt            *pUdpCxt = NULL;

    if (gUdpGblInfo.pUdpCurrentCxt != NULL)
    {
        pUdpCxt = gUdpGblInfo.pUdpCurrentCxt;
        /* Search the CB entry in Context UDP CB table */
        TMO_SLL_Scan (&(pUdpCxt->UdpCxtCbEntry), pUdp_cb, t_UDP_CB *)
        {
            if ((pUdp_cb->u2U_port == u2U_port) &&
                (pUdp_cb->u4Local_addr == u4LocalAddr))
            {
                MEMCPY (ptUdp_cb, pUdp_cb, sizeof (t_UDP_CB));
                *pu1UdpCBMode = SOCK_UNIQUE_MODE;    /* Context mode */
                return SUCCESS;
            }
        }
    }
    /* Search the CB entry in Global UDP CB table */
    TMO_SLL_Scan (&(gUdpGblInfo.UdpCbEntry), pUdp_cb, t_UDP_CB *)
    {
        if ((pUdp_cb->u2U_port == u2U_port) &&
            (pUdp_cb->u4Local_addr == u4LocalAddr))
        {
            MEMCPY (ptUdp_cb, pUdp_cb, sizeof (t_UDP_CB));
            *pu1UdpCBMode = SOCK_GLOBAL_MODE;    /* Global mode */
            return SUCCESS;
        }

    }

    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : udp_get_cb_in_cxt
 *
 * Input(s)           : u4ContextId, u2U_port, u4LocalAddr
 *
 * Output(s)          : None.
 *
 * Returns            : Control Block table
 *                      NULL if failure
 *
 * Action :
 *
 * Routine to get the entry corresponding to the udp port.
+-------------------------------------------------------------------*/

INT4
udp_get_cb_in_cxt (UINT4 u4ContextId, UINT2 u2U_port, UINT4 u4LocalAddr,
                   t_UDP_CB ** ptUdp_cb, UINT1 *pu1UdpCBMode)
{
    t_UDP_CB           *pUdp_cb = NULL;
    tUdpCxt            *pUdpCxt = NULL;

    UDP_DS_LOCK ();

    pUdpCxt = UdpGetCxtEntryFromCxtId (u4ContextId);
    if (pUdpCxt != NULL)
    {
        if (pUdpCxt->u4ContextId == u4ContextId)
        {
            /* Search the CB entry in Context UDP CB table */
            TMO_SLL_Scan (&(pUdpCxt->UdpCxtCbEntry), pUdp_cb, t_UDP_CB *)
            {
                if ((pUdp_cb->u2U_port == u2U_port) &&
                    (pUdp_cb->u4Local_addr == u4LocalAddr))
                {
                    *ptUdp_cb = pUdp_cb;
                    *pu1UdpCBMode = SOCK_UNIQUE_MODE;    /* Context mode */
                    UDP_DS_UNLOCK ();
                    return SUCCESS;
                }
            }
        }
    }
    /* Search the CB entry in Global UDP CB table */
    TMO_SLL_Scan (&(gUdpGblInfo.UdpCbEntry), pUdp_cb, t_UDP_CB *)
    {
        if ((pUdp_cb->u2U_port == u2U_port) &&
            (pUdp_cb->u4Local_addr == u4LocalAddr))
        {
            *ptUdp_cb = pUdp_cb;
            *pu1UdpCBMode = SOCK_GLOBAL_MODE;    /* Global mode */
            UDP_DS_UNLOCK ();
            return SUCCESS;
        }

    }

    UDP_DS_UNLOCK ();
    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : udp_get_ipv4_cb
 *
 * Input(s)           : u2U_port, u4LocalAddr, 
 *                      u2U_Rport, u4RemoteAddr
 *
 * Output(s)          : None.
 *
 * Returns            : Control Block table
 *                      NULL if failure
 *
 * Action :
 *
 * Routine to get the entry corresponding to the udp port.
+-------------------------------------------------------------------*/
INT4
udp_get_ipv4_cb (UINT2 u2U_Lport, UINT4 u4LocalAddr,
                 UINT2 u2U_Rport, UINT4 u4RemoteAddr, t_UDP_CB * ptUdp_cb)
{
    t_UDP_CB           *pUdp_cb = NULL;
    tUdpCxt            *pUdpCxt = NULL;
    UINT4               u4ContextId = 0;

    for (u4ContextId = 0; u4ContextId <= UDP_SIZING_CONTEXT_COUNT;
         u4ContextId++)
    {
        pUdpCxt = UdpGetCxtEntryFromCxtId (u4ContextId);
        if (pUdpCxt != NULL)
        {
            if (pUdpCxt->u4ContextId == u4ContextId)
            {
                /* Search the CB entry in Context UDP CB table */
                TMO_SLL_Scan (&(pUdpCxt->UdpCxtCbEntry), pUdp_cb, t_UDP_CB *)
                {
                    if ((pUdp_cb->u2U_port == u2U_Lport) &&
                        (pUdp_cb->u4Local_addr == u4LocalAddr) &&
                        (pUdp_cb->u2Remote_port == u2U_Rport) &&
                        (pUdp_cb->u4Remote_addr == u4RemoteAddr))
                    {
                        MEMCPY (ptUdp_cb, pUdp_cb, sizeof (t_UDP_CB));
                        return SUCCESS;
                    }

                }
            }
        }
    }

    TMO_SLL_Scan (&(gUdpGblInfo.UdpCbEntry), pUdp_cb, t_UDP_CB *)
    {
        if ((pUdp_cb->u2U_port == u2U_Lport) &&
            (pUdp_cb->u4Local_addr == u4LocalAddr) &&
            (pUdp_cb->u2Remote_port == u2U_Rport) &&
            (pUdp_cb->u4Remote_addr == u4RemoteAddr))
        {
            MEMCPY (ptUdp_cb, pUdp_cb, sizeof (t_UDP_CB));
            return SUCCESS;
        }

    }

    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : udp_put_hdr
 *
 * Input(s)           : u2Port, pBuf
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * Puts the UDP header in the pre allocated message buffer and calculates
 * the UDP checksum.
 * Assumes the current offset in the message is at UDP header.
 *  +------------------+---------+------------+------------------+
 *  | Space for IP HDR | IP OPTS |  UDP HDR   |  UDP DATA        |
 *  +------------------+---------+------------+------------------+
 *                               ^ Current offset
+-------------------------------------------------------------------*/
static INT4
udp_put_hdr (INT2 i2Len, UINT4 u4Dest, UINT4 u4Src,
             t_UDP * pUdpHdr, tUDP_BUF_CHAIN_HEADER * pBuf, UINT1 u1CkOpt)
{
    UINT2               u2Cksum = 0;
    UINT2               au2UdpHdr[IP_FOUR];

    /* Initializing the Checksum Field Zero Indicating No Checksum */
    pUdpHdr->u2Cksum = 0;
    UNUSED_PARAM (i2Len);

    /*
     * All zeros is same as all 1s in ones complement format.
     * This property is used to distinguish all zero checksum
     * from no checksum condition.
     */
    if (u1CkOpt == TRUE)
    {
        if ((u2Cksum = udp_cksum ((INT2) pUdpHdr->u2Len, u4Dest, u4Src, pUdpHdr,
                                  pBuf)) == 0)
        {
            /* If The CheckSum is Zero Assign 0xffff */
            u2Cksum = IP_BIT_ALL;
        }
        /* Assigning the Check Sum. */
        pUdpHdr->u2Cksum = u2Cksum;
    }

    /* Copying the Udp Header Structure to a Linear Buffer. */
    au2UdpHdr[0] = IP_HTONS (pUdpHdr->u2Src_u_port);
    au2UdpHdr[1] = IP_HTONS (pUdpHdr->u2Dest_u_port);
    au2UdpHdr[2] = IP_HTONS (pUdpHdr->u2Len);
    au2UdpHdr[3] = IP_HTONS (pUdpHdr->u2Cksum);

    /* Copying the Linear Buffer into the Chain Buffer. */
    IP_PREPEND_BUF (pBuf, (UINT1 *) au2UdpHdr, UDP_HDR_LEN);

    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : udp_extract_hdr
 *
 * Input(s)           : pUdp, pBuf
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS
 *
 * Action :
 *
 * Extracts the UDP header information from the buffer.
 * Assumes that current offset is set to UDP header offset.
 *  +------------+------------+------------------+
 *  | IP HDR     |  UDP HDR   |  UDP DATA        |
 *  +------------+------------+------------------+
 *               ^ Current offset
+-------------------------------------------------------------------*/
static INT4
udp_extract_hdr (t_UDP * pUdpHdr, tUDP_BUF_CHAIN_HEADER * pBuf)
{
    t_UDP               TmpUdpHdr;
    t_UDP              *pTmpUdpHdr = NULL;

    /* Copying From the Buffer Chain. */
    if ((pTmpUdpHdr =
         (t_UDP *) (VOID *) IP_GET_DATA_PTR_IF_LINEAR (pBuf, 0, UDP_HDR_LEN))
        == NULL)
    {
        pTmpUdpHdr = &TmpUdpHdr;
        IP_COPY_FROM_BUF (pBuf, (UINT1 *) pTmpUdpHdr, 0, UDP_HDR_LEN);
    }

    pUdpHdr->u2Src_u_port = IP_NTOHS (pTmpUdpHdr->u2Src_u_port);
    pUdpHdr->u2Dest_u_port = IP_NTOHS (pTmpUdpHdr->u2Dest_u_port);
    pUdpHdr->u2Len = IP_NTOHS (pTmpUdpHdr->u2Len);
    pUdpHdr->u2Cksum = IP_NTOHS (pTmpUdpHdr->u2Cksum);

    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : udp_cksum
 *
 * Input(s)           : i2Len, u4Dest, u4Src, UdpHdr, pBuf
 *
 * Output(s)          : None.
 *
 * Returns            : Checksum (in host byte order)
 *
 * Action :
 *
 * Calculates the UDP checksum.
 * Gets the pseudo header information as parameter and UDP header
 * and data as message buffer.
+-------------------------------------------------------------------*/
static UINT2
udp_cksum (INT2 i2Len, UINT4 u4Dest, UINT4 u4Src, t_UDP * pUdpHdr,
           tUDP_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp;

    /* Adding the Pseudo Header */
    u4Sum += (u4Src >> 16);
    u4Sum += (u4Src & IP_BIT_ALL);
    u4Sum += (u4Dest >> 16);
    u4Sum += (u4Dest & IP_BIT_ALL);
    u4Sum += (UINT2) UDP_PTCL;    /* 00-PROTO */
    u4Sum += (UINT2) i2Len;

    /* Adding the Udp Header. */
    u4Sum += (pUdpHdr->u2Src_u_port);
    u4Sum += (pUdpHdr->u2Dest_u_port);
    u4Sum += (pUdpHdr->u2Len);
    u4Sum += (pUdpHdr->u2Cksum);

    /* Calculating the CheckSum for the Udp DATA Alone */
    u2Tmp =
        (UINT2) (~(IpCalcCheckSum (pBuf, (UINT4) (i2Len - UDP_HDR_LEN), 0)));

    /* Converting to Host Order is not required. as it is in the host order */
    u4Sum += u2Tmp;

    /* One's Complement Addition */
    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffffL);
    u2Tmp = (UINT2) ((u4Sum >> 16) + (u4Sum & 0xffffL));

    return ((UINT2) (~u2Tmp));
}

/*-------------------------------------------------------------------+
 * Function           : udp_icmp_input_in_cxt
 *
 * Input(s)           : u4ContextId, pBuf, u4Src, i1Type, i1Code
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * Called from ICMP whenever an error is encountered for
 * an UDP packet sent.
 *                       <------------  ICMP DATA  ---------------->
 * +----------+----------+---------+-------------------------------+
 * | IP_HDR   | ICMP_HDR | IP_HDR  | HIGH LEVEL PROTO HDR AND DATA |
 * +----------+----------+---------+-------------------------------+
 *                       ^ Current offset
+-------------------------------------------------------------------*/
INT4
udp_icmp_input_in_cxt (UINT4 u4ContextId, tUDP_BUF_CHAIN_HEADER * pBuf,
                       UINT4 u4Src, INT1 i1Type, INT1 i1Code)
{
    t_UDP_CB           *ptUdp_cb;
    INT1                i1Hlen = 0;
    UINT2               u2SrcUdpPort = 0;
    UINT1               u1UdpCBMode = 0;
    INT4                i4RetVal = 0;

    UDP_STAT_ERR_ICMP_INC (u1UdpCBMode, u4ContextId);
    IP_PKT_GET_HLEN (pBuf, i1Hlen);
    i1Hlen = (INT1) ((i1Hlen & 0x0f) * IP_FOUR);

    if (IP_GET_BUF_LENGTH (pBuf) < (UINT2) (i1Hlen + UDP_HDR_LEN))
    {

        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                         ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                         "Length Missing in the ICMPmessage from %x \n", u4Src);

        IP_RELEASE_BUF (pBuf, FALSE);
        return FAILURE;
    }

    IP_COPY_FROM_BUF (pBuf, &u2SrcUdpPort, (UINT4) i1Hlen, sizeof (UINT2));
    u2SrcUdpPort = IP_NTOHS (u2SrcUdpPort);
    if (udp_get_cb_in_cxt (u4ContextId, u2SrcUdpPort, u4Src,
                           &ptUdp_cb, &u1UdpCBMode) != SUCCESS)
    {
        i4RetVal = udp_get_cb_in_cxt (u4ContextId, u2SrcUdpPort, 0,
                                      &ptUdp_cb, &u1UdpCBMode);

    }

    /* If an application is prepared to receive the error message pass it on */
    if ((i4RetVal == SUCCESS) && (ptUdp_cb->u1Give_errors == TRUE))
    {

        IP_CXT_TRC_ARG2 (u4ContextId, UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                         "ICMP Error from %x is enqueued to the application "
                         "at UDP port %d \n", u4Src, u2SrcUdpPort);

        i4RetVal = udp_task_enqueue_error_to_applications_in_cxt
            (u4ContextId, pBuf, i1Type, i1Code, ptUdp_cb, u4Src);
        return i4RetVal;
    }
    IP_RELEASE_BUF (pBuf, FALSE);
    return FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : udp_open_sli_port_in_cxt
 * 
 * Input(s)           : u2U_port, u4Local_addr
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action : Opens a  UDP Port as Requested by the application over SLI
 *
+-------------------------------------------------------------------*/
INT4
udp_open_sli_port_in_cxt (UINT1 u1FdMode, UINT4 u4ContextId, UINT2 u2U_port,
                          UINT4 u4Local_addr, UINT2 u2U_Rport,
                          UINT4 u4Remote_addr, INT4 i4SockDesc,
                          tOsixSemId SemId, tOsixQId RcvQId)
{

    t_UDP_CB           *pUdpCbNode = NULL;

    UDP_DS_LOCK ();
    pUdpCbNode = (t_UDP_CB *) MemAllocMemBlk (gUdpGblInfo.UdpCBPoolId);
    if (pUdpCbNode != NULL)
    {
        MEMSET (pUdpCbNode, 0, sizeof (t_UDP_CB));
        TMO_SLL_Init_Node (&(pUdpCbNode->NextCBNode));
        pUdpCbNode->u2U_port = u2U_port;
        pUdpCbNode->u4Local_addr = u4Local_addr;
        pUdpCbNode->SemId = SemId;
        pUdpCbNode->RcvQId = RcvQId;
        pUdpCbNode->u4Local_addr = u4Local_addr;
        pUdpCbNode->i4SockDesc = i4SockDesc;
        pUdpCbNode->i4LocalAddrType = UDP_IPV4_ADDR_TYPE;
        pUdpCbNode->i4RemoteAddrType = UDP_IPV4_ADDR_TYPE;
        pUdpCbNode->u4Remote_addr = u4Remote_addr;
        pUdpCbNode->u2Remote_port = u2U_Rport;
        pUdpCbNode->u4Instance = UDP_ZERO;
        pUdpCbNode->u1Give_errors = TRUE;
        if (u1FdMode == SOCK_UNIQUE_MODE)
        {
            if (UdpAddCBNodeToCxtTable (u4ContextId, pUdpCbNode) == FAILURE)
            {

                MemReleaseMemBlock (gUdpGblInfo.UdpCBPoolId,
                                    (UINT1 *) pUdpCbNode);
                UDP_DS_UNLOCK ();
                return FAILURE;
            }
        }
        else
        {
            UdpAddCBNodeToGlbTable (pUdpCbNode);
        }
    }
    else
    {
        UDP_DS_UNLOCK ();
        return FAILURE;
    }
    UDP_DS_UNLOCK ();
    KW_FALSEPOSITIVE_FIX (pUdpCbNode);
    return SUCCESS;

}

/*-------------------------------------------------------------------+
 * Function           : udp_sli_open_in_cxt
 * Input(s)           : u4ContextId, u2U_port, u4Local_addr, u1SemName, 
 *                      u2Sem_id, RecvQName, u2Rcv_q_id,
 * Output(s)          : None.
 * Returns            : SUCCESS or FAILURE
 * Action :  Allows an application to enrol to UDP.
 * Application can receive data only after this call.
 * If an application is willing to receive ICMP error messages generated for
 * the packets sent from it, then it should indicate so by setting
 * u1Give_errorsflag.
+-------------------------------------------------------------------*/
INT4
udp_sli_open_in_cxt (UINT1 u1FdMode, UINT4 u4ContextId, INT4 i4SockDesc,
                     UINT2 *u2U_port, UINT4 u4Local_addr,
                     UINT2 *u2U_Rport, UINT4 u4Remote_addr,
                     tOsixSemId SemId, tOsixQId RcvQId)
{
    t_UDP_CB           *ptUdp_cb;
    UINT1               u1UdpCBMode = 0;

    /* If local address is specified it should be a valid one */
    if ((u4Local_addr != 0) &&
        ((udp_task_ip_verify_addr (u4Local_addr, IP_ADDR_SRC) == FAILURE) ||
         (udp_task_ip_verify_addr (u4Local_addr, IP_ADDR_DEST) == FAILURE)))
    {

        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                         ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                         "%x is a invalid Local Address \n", u4Local_addr);
        return FAILURE;
    }

    if (*u2U_port == 0)
    {
        *u2U_port = GetFreeLocalUdpPort ();
    }
    else if (udp_get_cb_in_cxt (u4ContextId, *u2U_port, u4Local_addr,
                                &ptUdp_cb, &u1UdpCBMode) == SUCCESS)
    {
        IP_CXT_TRC_ARG1 (u4ContextId, UDP_MOD_TRC,
                         ALL_FAILURE_TRC | CONTROL_PLANE_TRC, UDP_NAME,
                         "Port %d is Busy ...\n", *u2U_port);
        return FAILURE;
    }

    if (udp_open_sli_port_in_cxt
        (u1FdMode, u4ContextId, *u2U_port, u4Local_addr, *u2U_Rport,
         u4Remote_addr, i4SockDesc, SemId, RcvQId) == FAILURE)
    {
        return FAILURE;
    }

    IP_CXT_TRC_ARG2 (u4ContextId, UDP_MOD_TRC, CONTROL_PLANE_TRC, UDP_NAME,
                     "Udp Open for port %d and address %x is Successful \n",
                     u2U_port, u4Local_addr);

    return SUCCESS;
}

static UINT2
GetFreeLocalUdpPort (VOID)
{
    UINT2               u2UdpPort = 0;
    t_UDP_CB            tUdp_cb;

    for (u2UdpPort = PVT_UDP_PORT_BEGIN; u2UdpPort <= PVT_UDP_PORT_END;
         u2UdpPort++)
    {
        if (udp_get_port (u2UdpPort, &tUdp_cb) == FAILURE)
        {
            return u2UdpPort;
        }
    }
    return ((UINT2) IP_FAILURE);
}

/*-------------------------------------------------------------------+
 * Function           : udpGetNextIndexUdpTable
 *
 * Input(s)           :u4UdpLocalAddress, u2UdpLocalPort
 *
 * Output(s)          : *pu4NextLocalAddress -Next Higher LocalAddress
 *                      *pu2NextPort         - Next Higher Udp Port
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action : Returns Next Lexicographically higher INdex.
 *
+-------------------------------------------------------------------*/
INT4
udpGetNextIndexUdpTable (UINT4 u4UdpLocalAddress, UINT4
                         *pu4NextLocalAddress, UINT2 u2UdpLocalPort,
                         UINT2 *pu2NextPort)
{
    t_UDP_CB           *pNextUdpCb = NULL;
    t_UDP_CB           *pCurrUdpCb = NULL;

    TMO_SLL_Scan (&(gUdpGblInfo.UdpCbEntry), pCurrUdpCb, t_UDP_CB *)
    {
        if ((pCurrUdpCb->u4Local_addr == u4UdpLocalAddress) &&
            (pCurrUdpCb->u2U_port == u2UdpLocalPort))
        {
            pNextUdpCb = (t_UDP_CB *)
                TMO_SLL_Next (&(gUdpGblInfo.UdpCbEntry),
                              &(pCurrUdpCb->NextCBNode));
            if (pNextUdpCb != NULL)
            {
                *pu4NextLocalAddress = pNextUdpCb->u4Local_addr;
                *pu2NextPort = pNextUdpCb->u2U_port;
                UDP_DS_UNLOCK ();
                return (SUCCESS);
            }
        }
    }
    return (FAILURE);

}

/*-------------------------------------------------------------------+
 * Function           : udpCxtGetFirstIndexEndpointTable
 *
 * Input(s)           : None
 *
 * Output(s)          : *pFirstLocalAddress - First LocalAddress
 *                      *pu4FirstPort         - First Udp Port
 *                      *pFirstRemoteAddress - First remote address
 *                      *pu4FirstRemotePort - First remote port
 *                      *pu4FirstInstance - First udp instance
 *                    
 * Returns            : SUCCESS or FAILURE
 *
 * Action : Returns first valid index in context specific udp 
 *          endpoint table.
 * Note   : LOCK is taken before calling this function         
 +-------------------------------------------------------------------*/

INT4
udpCxtGetFirstIndexEndpointTable (tSNMP_OCTET_STRING_TYPE * pFirstLocalAddress,
                                  UINT4 *pu4FirstPort,
                                  tSNMP_OCTET_STRING_TYPE * pFirstRemoteAddress,
                                  UINT4 *pu4FirstRemotePort,
                                  UINT4 *pu4FirstInstance)
{
    t_UDP_CB           *pFirstUdpCb = NULL;
    tUdpCxt            *pUdpCxt = gUdpGblInfo.pUdpCurrentCxt;
    UINT4               u4LocalIp = 0;
    UINT4               u4RemoteIp = 0;

    if (pUdpCxt == NULL)
    {
        return (SNMP_FAILURE);
    }
    pFirstUdpCb = (t_UDP_CB *) TMO_SLL_First (&(pUdpCxt->UdpCxtCbEntry));

    if (pFirstUdpCb != NULL)
    {
        *pu4FirstPort = (UINT4) pFirstUdpCb->u2U_port;
        *pu4FirstRemotePort = (UINT4) pFirstUdpCb->u2Remote_port;
        *pu4FirstInstance = (UINT4) pFirstUdpCb->u4Instance;

        u4LocalIp = OSIX_HTONL (pFirstUdpCb->u4Local_addr);
        MEMCPY (pFirstLocalAddress->pu1_OctetList,
                (UINT1 *) &u4LocalIp, sizeof (UINT4));
        pFirstLocalAddress->i4_Length = sizeof (UINT4);
        u4RemoteIp = OSIX_HTONL (pFirstUdpCb->u4Remote_addr);
        MEMCPY (pFirstRemoteAddress->pu1_OctetList,
                (UINT1 *) &u4RemoteIp, sizeof (UINT4));
        pFirstRemoteAddress->i4_Length = sizeof (UINT4);
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/*-------------------------------------------------------------------+
 * Function           : udpCxtGetNextIndexEndpointTable
 *
 * Input(s)           :pUdpLocalAddress, u4UdpLocalPort,pUdpRemoteAddress
 *                     u4UdpRemotePort, u4Instance 
 *
 * Output(s)          : *pNextLocalAddress -Next Higher LocalAddress
 *                      *pu2NextPort         - Next Higher Udp Port
 *                      *pNextRemoteAddress - Next Higher remote address
 *                      *pu4NextRemotePort - Next Higher remote port
 *                      *pu4Instance - Next Higher instance
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action : Returns Next Lexicographically higher Index in 
 *          Context specific UDP endpoint table.
 * Note   : LOCK is taken before calling this function
+-------------------------------------------------------------------*/

INT4
udpCxtGetNextIndexEndpointTable (tSNMP_OCTET_STRING_TYPE * pUdpLocalAddress,
                                 tSNMP_OCTET_STRING_TYPE * pNextLocalAddress,
                                 UINT4 u4UdpLocalPort,
                                 UINT4 *pu4NextPort,
                                 tSNMP_OCTET_STRING_TYPE * pUdpRemoteAddress,
                                 tSNMP_OCTET_STRING_TYPE * pNextRemoteAddress,
                                 UINT4 u4UdpRemotePort,
                                 UINT4 *pu4NextRemotePort,
                                 UINT4 u4Instance, UINT4 *pu4Instance)
{
    t_UDP_CB           *pCurrUdpCb = NULL;
    t_UDP_CB           *pNextUdpCb = NULL;
    tUdpCxt            *pUdpCxt = gUdpGblInfo.pUdpCurrentCxt;
    UINT4               u4LocalIp = 0;
    UINT4               u4RemoteIp = 0;
    UINT4               u4NextLocalIp = 0;
    UINT4               u4NextRemoteIp = 0;
    UINT1               u1Len = 0;

    if (pUdpCxt != NULL)
    {
        u1Len = (UINT1) MEM_MAX_BYTES ((UINT4) pUdpLocalAddress->i4_Length,
                                       sizeof (UINT4));
        MEMCPY ((UINT1 *) &u4LocalIp, pUdpLocalAddress->pu1_OctetList, u1Len);
        u4LocalIp = OSIX_NTOHL (u4LocalIp);
        MEMCPY ((UINT1 *) &u4RemoteIp, pUdpRemoteAddress->pu1_OctetList, u1Len);
        u4RemoteIp = OSIX_NTOHL (u4RemoteIp);

        TMO_SLL_Scan (&(pUdpCxt->UdpCxtCbEntry), pCurrUdpCb, t_UDP_CB *)
        {
            if ((pCurrUdpCb->u4Local_addr == u4LocalIp) &&
                (pCurrUdpCb->u2U_port == (UINT2) u4UdpLocalPort) &&
                (pCurrUdpCb->u4Remote_addr == u4RemoteIp) &&
                (pCurrUdpCb->u2Remote_port == (UINT4) u4UdpRemotePort) &&
                (pCurrUdpCb->u4Instance == u4Instance))

            {
                pNextUdpCb = (t_UDP_CB *)
                    TMO_SLL_Next (&(pUdpCxt->UdpCxtCbEntry),
                                  &(pCurrUdpCb->NextCBNode));
                if (pNextUdpCb != NULL)
                {
                    *pu4NextPort = (UINT4) pNextUdpCb->u2U_port;
                    *pu4NextRemotePort = (UINT4) pNextUdpCb->u2Remote_port;
                    *pu4Instance = pNextUdpCb->u4Instance;

                    u4NextLocalIp = OSIX_HTONL (pNextUdpCb->u4Local_addr);
                    MEMCPY (pNextLocalAddress->pu1_OctetList,
                            (UINT1 *) &u4NextLocalIp, sizeof (UINT4));

                    u4NextRemoteIp = OSIX_HTONL (pNextUdpCb->u4Remote_addr);
                    MEMCPY (pNextRemoteAddress->pu1_OctetList,
                            (UINT1 *) &u4NextRemoteIp, sizeof (UINT4));
                    return (SNMP_SUCCESS);
                }
                else
                {
                    /* Next CB node does not exist */
                    return (SNMP_FAILURE);
                }
            }
        }
    }
    return (SNMP_FAILURE);
}

/*-------------------------------------------------------------------+
 * Function           : udpGetNextIndexIpvxUdpTable
 *
 * Input(s)           : u4UdpLocalAddress, u2UdpLocalPort,
 *                      u4UdpRemoteAddress, u2UdpRemotePort
 *
 * Output(s)          : *pu4NextLocalAddress -Next Higher LocalAddress
 *                      *pu2NextPort         - Next Higher Udp Port
 *                      *pu4NextLocalAddress -Next Higher RemoteAddress
 *                      *pu2NextPort         - Next Higher Remote Udp Port
 *
 * Returns            : SUCCESS or FAILURE
 * Action             : This function compares the previous UDP4 entry 
 *                      with all the other entries in the UDP4 table and 
 *                      finds the entry which is Lexicographically greater 
 *                      than the previous entry.
 *  Note              : UDP_DS_LOCK is taken before calling this function
+-------------------------------------------------------------------*/
INT4
udpGetNextIndexIpvxUdpTable (UINT4 u4UdpLocalAddress,
                             UINT4 *pu4NextLocalAddress,
                             UINT2 u2UdpLocalPort,
                             UINT2 *pu2NextPort,
                             UINT4 u4UdpRemoteAddress,
                             UINT4 *pu4NextRemoteAddress,
                             UINT2 u2UdpRemotePort, UINT2 *pu2NextRemotePort)
{
    t_UDP_CB           *pFirstUdpCb = NULL;
    t_UDP_CB           *pCurrUdpCb = NULL;
    t_UDP_CB           *pNextUdpCb = NULL;

    if (u2UdpLocalPort == 0)
    {
        pFirstUdpCb = (t_UDP_CB *) TMO_SLL_First (&(gUdpGblInfo.UdpCbEntry));

        if (pFirstUdpCb != NULL)
        {
            *pu4NextLocalAddress = pFirstUdpCb->u4Local_addr;
            *pu2NextPort = pFirstUdpCb->u2U_port;
            *pu4NextRemoteAddress = pFirstUdpCb->u4Remote_addr;
            *pu2NextRemotePort = pFirstUdpCb->u2Remote_port;
            return (SUCCESS);
        }

    }
    else
    {
        TMO_SLL_Scan (&(gUdpGblInfo.UdpCbEntry), pCurrUdpCb, t_UDP_CB *)
        {
            if ((pCurrUdpCb->u4Local_addr == u4UdpLocalAddress) &&
                (pCurrUdpCb->u2U_port == u2UdpLocalPort) &&
                (pCurrUdpCb->u4Remote_addr == u4UdpRemoteAddress) &&
                (pCurrUdpCb->u2Remote_port == u2UdpRemotePort))
            {
                pNextUdpCb = (t_UDP_CB *)
                    TMO_SLL_Next (&(gUdpGblInfo.UdpCbEntry),
                                  &(pCurrUdpCb->NextCBNode));
                if (pNextUdpCb != NULL)
                {
                    *pu4NextLocalAddress = pNextUdpCb->u4Local_addr;
                    *pu2NextPort = pNextUdpCb->u2U_port;
                    *pu4NextRemoteAddress = pNextUdpCb->u4Remote_addr;
                    *pu2NextRemotePort = pNextUdpCb->u2Remote_port;
                    return (SUCCESS);
                }
            }
        }
    }
    return (FAILURE);
}

/*****************************************************************************/
/* Function     : UdpCtrlTblLock                                             */
/*                                                                           */
/*  Description : Takes the Semaphore for Protecting UDP Control Table       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
UdpCtrlTblLock (VOID)
{
    if (OsixSemTake (gUdpGblInfo.UdpDSem) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : UdpCtrlTblUnLock                                           */
/*                                                                           */
/*  Description : Releases the Semaphore for Protecting UDP Control Table    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
UdpCtrlTblUnLock (VOID)
{
    OsixSemGive (gUdpGblInfo.UdpDSem);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : UdpProtocolLock                                            */
/*                                                                           */
/*  Description : Takes the UDP Protocol Semaphore                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
UdpProtocolLock (VOID)
{
    if (OsixSemTake (gUdpGblInfo.UdpSem) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : UdpProtocolUnLock                                          */
/*                                                                           */
/*  Description : Releases the UDP Protocol Semaphore                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/
INT4
UdpProtocolUnLock (VOID)
{
    OsixSemGive (gUdpGblInfo.UdpSem);
    return SNMP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : UdpDeleteCxtEntry                        */
/*  Description     : Deletes an UDP context specific entry    */
/*                    in Global UDP context structure          */
/*  Input(s)        : pUdpCxtEntry - UDP Context specific Entry*/
/*  Output(s)       : None                                     */
/*  Returns         : SUCESS/FAILURE                           */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpDeleteCxtEntry (tUdpCxt * pUdpCxtEntry)
{
    UINT4               u4ContextId = 0;

    if (pUdpCxtEntry == NULL)
    {
        return FAILURE;
    }
    /* Delete the context control block entry */
    MEMSET (&(pUdpCxtEntry->UdpStat), 0, sizeof (t_UDP_STAT));
    u4ContextId = pUdpCxtEntry->u4ContextId;
    gUdpGblInfo.apUdpContextInfo[u4ContextId] = NULL;
    UDP_FREE_CXT_ENTRY (pUdpCxtEntry);
    return SUCCESS;
}

/***************************************************************/
/*  Function Name   : UdpAddCBNodeToCxtTable                   */
/*  Description     : Adds a contol block node in UDP context  */
/*                    table.                                   */
/*  Input(s)        : u4ContextId - Context Identifier         */
/*                    pUdpCbNode - UDP Control Block Node      */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS/FAILURE                          */
/*  Note            : UDP_DS_LOCK is taken before calling      */
/*                    this function                            */
/***************************************************************/
INT4
UdpAddCBNodeToCxtTable (UINT4 u4ContextId, t_UDP_CB * pUdpCbNode)
{
    t_UDP_CB           *pPrevUdpEntry = NULL;
    t_UDP_CB           *pCurrUdpEntry = NULL;
    tUdpCxt            *pUdpCxt = NULL;

    pUdpCxt = UdpGetCxtEntryFromCxtId (u4ContextId);
    if (pUdpCxt == NULL)
    {
        /* When adding first CB node in context entry,
         * create memory for this context from 
         * context pool */
        if (u4ContextId >= (UINT4) UDP_SIZING_CONTEXT_COUNT)
        {
            return FAILURE;
        }
        /* Create entry for a valid context */
        if (UDP_ALLOC_CXT_ENTRY (pUdpCxt) == NULL)
        {
            return FAILURE;
        }
        /* Intialize the context control block entry */
        MEMSET (&(pUdpCxt->UdpStat), 0, sizeof (t_UDP_STAT));
        TMO_SLL_Init (&(pUdpCxt->UdpCxtCbEntry));
        pUdpCxt->u4ContextId = u4ContextId;
        gUdpGblInfo.apUdpContextInfo[u4ContextId] = pUdpCxt;
    }

    if (pUdpCxt != NULL)
    {
        /* Currently u4Instance is invalid index */

        /* Add control block node in sorted order */
        TMO_SLL_Scan (&(pUdpCxt->UdpCxtCbEntry), pCurrUdpEntry, t_UDP_CB *)
        {
            if ((pCurrUdpEntry->i4LocalAddrType == pUdpCbNode->i4LocalAddrType)
                && (pCurrUdpEntry->i4RemoteAddrType ==
                    pUdpCbNode->i4RemoteAddrType))
            {
                if (pUdpCbNode->u4Local_addr < pCurrUdpEntry->u4Local_addr)
                {
                    break;
                }
                else if (pUdpCbNode->u4Local_addr ==
                         pCurrUdpEntry->u4Local_addr)
                {
                    if (pUdpCbNode->u2U_port < pCurrUdpEntry->u2U_port)
                    {
                        break;
                    }
                    else if (pUdpCbNode->u2U_port == pCurrUdpEntry->u2U_port)
                    {
                        if (pUdpCbNode->u4Remote_addr <
                            pCurrUdpEntry->u4Remote_addr)
                        {
                            break;
                        }
                        else if (pUdpCbNode->u4Remote_addr ==
                                 pCurrUdpEntry->u4Remote_addr)
                        {
                            if (pUdpCbNode->u2Remote_port <=
                                pCurrUdpEntry->u2Remote_port)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            pPrevUdpEntry = pCurrUdpEntry;
        }
        /* Add pUdpCbNode in Cxt structure next to pPrevUdpEntry */
        TMO_SLL_Insert (&(pUdpCxt->UdpCxtCbEntry),
                        &(pPrevUdpEntry->NextCBNode),
                        &(pUdpCbNode->NextCBNode));
    }
    return SUCCESS;
}

/***************************************************************/
/*  Function Name   : UdpDeleteCBNodeFromCxtTable              */
/*  Description     : Deletes the control block node in UDP    */
/*                    Context table.                           */
/*  Input(s)        : u4ContextId - Context Identifier         */
/*                    pUdpCbNode - UDP Control Block Node      */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS/FAILURE                          */
/***************************************************************/
INT4
UdpDeleteCBNodeFromCxtTable (UINT4 u4ContextId, t_UDP_CB * pUdpCbNode)
{
    tUdpCxt            *pUdpCxt = NULL;

    UDP_DS_LOCK ();
    pUdpCxt = UdpGetCxtEntryFromCxtId (u4ContextId);
    if ((pUdpCxt == NULL) || (pUdpCbNode == NULL))
    {
        UDP_DS_UNLOCK ();
        return FAILURE;
    }
    TMO_SLL_Delete (&(pUdpCxt->UdpCxtCbEntry), &(pUdpCbNode->NextCBNode));
    TMO_SLL_Init_Node (&(pUdpCbNode->NextCBNode));
    MemReleaseMemBlock (gUdpGblInfo.UdpCBPoolId, (UINT1 *) pUdpCbNode);
    if ((TMO_SLL_Count (&(pUdpCxt->UdpCxtCbEntry))) <= 0)
    {
        UdpDeleteCxtEntry (pUdpCxt);
    }
    UDP_DS_UNLOCK ();
    return SUCCESS;
}

/***************************************************************/
/*  Function Name   : UdpAddCBNodeToGlbTable                   */
/*  Description     : Adds the control block node in UDP       */
/*                    global table.                            */
/*  Input(s)        : pUdpCbNode - UDP Control Block Node      */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS/FAILURE                          */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpAddCBNodeToGlbTable (t_UDP_CB * pUdpCbNode)
{
    t_UDP_CB           *pPrevUdpEntry = NULL;
    t_UDP_CB           *pCurrUdpEntry = NULL;

    /* Currently Instance is invalid index */
    if (pUdpCbNode == NULL)
    {
        return FAILURE;
    }
    /* Add UDP CB node in sorted order */
    TMO_SLL_Scan (&(gUdpGblInfo.UdpCbEntry), pCurrUdpEntry, t_UDP_CB *)
    {
        if ((pCurrUdpEntry->i4LocalAddrType == pUdpCbNode->i4LocalAddrType) &&
            (pCurrUdpEntry->i4RemoteAddrType == pUdpCbNode->i4RemoteAddrType))
        {
            if (pUdpCbNode->u4Local_addr < pCurrUdpEntry->u4Local_addr)
            {
                break;
            }
            else if (pUdpCbNode->u4Local_addr == pCurrUdpEntry->u4Local_addr)
            {
                if (pUdpCbNode->u2U_port < pCurrUdpEntry->u2U_port)
                {
                    break;
                }
                else if (pUdpCbNode->u2U_port == pCurrUdpEntry->u2U_port)
                {
                    if (pUdpCbNode->u4Remote_addr <
                        pCurrUdpEntry->u4Remote_addr)
                    {
                        break;
                    }
                    else if (pUdpCbNode->u4Remote_addr ==
                             pCurrUdpEntry->u4Remote_addr)
                    {
                        if (pUdpCbNode->u2Remote_port <=
                            pCurrUdpEntry->u2Remote_port)
                        {
                            break;
                        }
                    }
                }
            }
        }
        pPrevUdpEntry = pCurrUdpEntry;
    }
    /* Insert pUdpCbNode in global UDP structre next to pPrevUdpEntry */
    TMO_SLL_Insert (&(gUdpGblInfo.UdpCbEntry),
                    (&(pPrevUdpEntry->NextCBNode)),
                    (&(pUdpCbNode->NextCBNode)));
    return SUCCESS;
}

/***************************************************************/
/*  Function Name   : UdpDeleteCBNodeFromGlbTable              */
/*  Description     : Deletes control block node in UDP        */
/*                    global table.                            */
/*  Input(s)        : pUdpCbNode - UDP control block Node      */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS/FAILURE                          */
/***************************************************************/
INT4
UdpDeleteCBNodeFromGlbTable (t_UDP_CB * pUdpCbNode)
{
    UDP_DS_LOCK ();
    if (pUdpCbNode == NULL)
    {
        UDP_DS_UNLOCK ();
        return FAILURE;
    }
    /* Delete the pUdpCbNode from global table and 
     * free that memory */
    TMO_SLL_Delete (&(gUdpGblInfo.UdpCbEntry), &(pUdpCbNode->NextCBNode));
    TMO_SLL_Init_Node (&(pUdpCbNode->NextCBNode));
    MemReleaseMemBlock ((gUdpGblInfo.UdpCBPoolId), (UINT1 *) (pUdpCbNode));
    UDP_DS_UNLOCK ();
    return SUCCESS;
}

/***************************************************************/
/*  Function Name   : UdpMemDeInit                             */
/*  Description     : Release the UDP memory pools and sem     */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
UdpMemDeInit (VOID)
{
    if (gUdpGblInfo.UdpDSem != 0)
        OsixSemDel (gUdpGblInfo.UdpDSem);
    if (gUdpGblInfo.UdpSem != 0)
        OsixSemDel (gUdpGblInfo.UdpSem);
    gUdpGblInfo.UdpDSem = 0;
    gUdpGblInfo.UdpSem = 0;
    UdpSizingMemDeleteMemPools ();
    return;
}

/***************************************************************/
/*  Function Name   : UdpProcessCxtDeletion                    */
/*  Description     : Delete the UDP  node from context table  */
/*  Input(s)        : u4ContextId - Context Identifier         */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS                                  */
/***************************************************************/
INT4
UdpProcessCxtDeletion (UINT4 u4ContextId)
{
    t_UDP_CB           *pUdpCbNode = NULL;
    tUdpCxt            *pUdpCxt = NULL;

    UDP_DS_LOCK ();
    pUdpCxt = UdpGetCxtEntryFromCxtId (u4ContextId);
    if (pUdpCxt == NULL)
    {
        UDP_DS_UNLOCK ();
        return FAILURE;
    }
    MEMSET (&(pUdpCxt->UdpStat), 0, sizeof (t_UDP_STAT));
    while ((pUdpCbNode = (t_UDP_CB *)
            TMO_SLL_Get (&(pUdpCxt->UdpCxtCbEntry))) != NULL)
    {
        MemReleaseMemBlock ((gUdpGblInfo.UdpCBPoolId), (UINT1 *) (pUdpCbNode));
    }
    gUdpGblInfo.apUdpContextInfo[u4ContextId] = NULL;
    UDP_FREE_CXT_ENTRY (pUdpCxt);
    UDP_DS_UNLOCK ();
    return SUCCESS;

}

/***************************************************************/
/*  Function Name   : UdpGetStatEntry                          */
/*  Description     : Get the UDP statistics entry             */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : pUdpStatEntry - UDP Stats Entry          */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
t_UDP_STAT         *
UdpGetStatEntry (VOID)
{
    t_UDP_STAT         *pUdpStatEntry = NULL;

    pUdpStatEntry = &gUdpGblInfo.UdpStat;
    return pUdpStatEntry;
}

/***************************************************************/
/*  Function Name   : UdpGetCurrCxtStatEntry                   */
/*  Description     : Gets the UDP current stats entry from UDP*/
/*                    global table.                            */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : pUdpStatEntry - UDP Stats Entry          */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
t_UDP_STAT         *
UdpGetCurrCxtStatEntry (VOID)
{
    t_UDP_STAT         *pUdpStatEntry = NULL;

    if (gUdpGblInfo.pUdpCurrentCxt != NULL)
    {
        pUdpStatEntry = &gUdpGblInfo.pUdpCurrentCxt->UdpStat;
    }
    return pUdpStatEntry;
}

/***************************************************************/
/*  Function Name   : UdpGetCxtEntryFromCxtId                  */
/*  Description     : Gets the current context entry from UDP  */
/*                    global table.                            */
/*  Input(s)        : u4ContextId - context Identifier         */
/*  Output(s)       : pUdpCxtEntry - Udp Context entry         */
/*  Returns         : None                                     */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
tUdpCxt            *
UdpGetCxtEntryFromCxtId (UINT4 u4ContextId)
{
    tUdpCxt            *pUdpCxtEntry = NULL;

    if (u4ContextId >= (UINT4) UDP_SIZING_CONTEXT_COUNT)
    {
        return NULL;
    }
    pUdpCxtEntry = gUdpGblInfo.apUdpContextInfo[u4ContextId];
    return pUdpCxtEntry;
}

/***************************************************************/
/*  Function Name   : UdpSelectContext                         */
/*  Description     : Set the current UDP context pointer      */
/*  Input(s)        : u4UdpContextId - UDP Context Identifier  */
/*  Output(s)       : None                                     */
/*  Returns         : SUCCESS/FAILURE                          */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpSelectContext (UINT4 u4UdpContextId)
{
    if (VcmIsL3VcExist (u4UdpContextId) != VCM_FALSE)
    {
        gUdpGblInfo.u4UdpCurrentCxtId = u4UdpContextId;
        gUdpGblInfo.pUdpCurrentCxt = UdpGetCxtEntryFromCxtId (u4UdpContextId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/***************************************************************/
/*  Function Name   : UdpReleaseContext                        */
/*  Description     : Reset the current UDP context pointer    */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
VOID
UdpReleaseContext (VOID)
{
    gUdpGblInfo.u4UdpCurrentCxtId = UDP_INVALID_CXT_ID;
    gUdpGblInfo.pUdpCurrentCxt = NULL;
    return;
}

/***************************************************************/
/*  Function Name   : UdpIsValidCxtId                          */
/*  Description     :Checks whether the UDP Context ID is Valid*/
/*  Input(s)        : u4UdpCxtId - UDP context ID              */
/*  Output(s)       : None                                     */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpIsValidCxtId (UINT4 u4UdpContextId)
{
    if (u4UdpContextId < UDP_SIZING_CONTEXT_COUNT)
    {
        if (gUdpGblInfo.apUdpContextInfo[u4UdpContextId] != NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/***************************************************************/
/*  Function Name   : UdpGetFirstCxtId                         */
/*  Description     : Gets the UDP First Context ID            */
/*  Input(s)        : None                                     */
/*  Output(s)       : pu4UdpCxtId - UDP context ID             */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpGetFirstCxtId (UINT4 *pu4UdpCxtId)
{
    UINT4               u4UdpCxtId = 0;

    for (u4UdpCxtId = 0; u4UdpCxtId < UDP_SIZING_CONTEXT_COUNT; u4UdpCxtId++)
    {
        if ((gUdpGblInfo.apUdpContextInfo[u4UdpCxtId]) != NULL)
        {
            *pu4UdpCxtId = u4UdpCxtId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/***************************************************************/
/*  Function Name   : UdpGetNextCxtId                          */
/*  Description     : Gets the next UDP Context ID             */
/*  Input(s)        : u4UdpCxtId - UDP context ID              */
/*  Output(s)       : pu4UdpCxtId - UDP context ID             */
/*  Returns         : SNMP_SUCCESS/SNMP_FAILURE                */
/*  Note            : UDP_DS_LOCK is taken before              */
/*                    calling this function                    */
/***************************************************************/
INT4
UdpGetNextCxtId (UINT4 u4UdpCxtId, UINT4 *pu4NextUdpCxtId)
{
    UINT4               u4NextUdpCxtId = 0;

    u4UdpCxtId++;
    for (u4NextUdpCxtId = u4UdpCxtId;
         u4NextUdpCxtId < UDP_SIZING_CONTEXT_COUNT; u4NextUdpCxtId++)
    {
        if ((gUdpGblInfo.apUdpContextInfo[u4NextUdpCxtId]) != NULL)
        {
            *pu4NextUdpCxtId = u4NextUdpCxtId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}
