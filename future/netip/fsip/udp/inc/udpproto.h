
 /* $Id: udpproto.h,v 1.7 2011/10/25 09:53:38 siva Exp $*/
/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/

INT4 fs_udp_init PROTO ((VOID));
INT4 fs_udp_input_in_cxt PROTO ((UINT4 u4ContextId,
                       tCRU_BUF_CHAIN_HEADER * pBuf,
                       INT2 i2Len, UINT2 u2Port,
                       tCRU_INTERFACE InterfaceId, UINT1 u1Flag));

INT4 udp_get_cb PROTO ((UINT2 u2U_port, UINT4 u4LocalAddr, 
                               t_UDP_CB *, UINT1 * pu1CBMode));

INT4 udp_get_cb_in_cxt PROTO ((UINT4 u4ContextId, UINT2 u2U_port, UINT4 u4LocalAddr, 
                               t_UDP_CB **, UINT1 * pu1CBMode));

INT4 udp_get_port PROTO ((UINT2 u2U_port, t_UDP_CB *));

INT4 udp_icmp_input_in_cxt PROTO ((UINT4 u4contextId, tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 u4Src, INT1 i1Type, INT1 i1Code));

INT4 Ip_Udp_Task_Init PROTO ((VOID));

INT4 udp_task_enqueue_to_applications_in_cxt PROTO ((UINT4, UINT2, tIpBuf *, INT2,
                                              t_UDP_CB *, UINT4, UINT4,
                                              UINT2, t_UDP *));

INT4 udp_task_enqueue_error_to_applications_in_cxt
PROTO (
       (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pBuf, INT1 i1Type, INT1 i1Code, t_UDP_CB * pUdp,
        UINT4 u4IpAddr));

INT4 udp_task_ip_send_in_cxt PROTO ((UINT4 u4ContextId, UINT4 u4Src, UINT4 u4Dest,
                              INT1 i1Proto, INT1 i1Ttl,
                              INT1 i1Tos,
                              tCRU_BUF_CHAIN_HEADER * pBuf,
                              INT2 i2Len, INT2 i2Id, INT1 i1Df,
                              INT2 i2Olen, UINT2 u2Port));

INT4 udp_task_icmp_error_msg_in_cxt PROTO ((UINT4 u4ContextId,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     INT1 i1Type,
                                     INT1 i1Code, UINT4 u4Parm1,
                                     UINT4 u4Parm2));

VOID iptask_udp_input PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT2 u2Len,
                              UINT4 u4Port,
                              tCRU_INTERFACE InterfaceId, UINT1 u1Flag));
INT4 iptask_icmp_udp_in_cxt PROTO ((UINT4 u4ContextId,
                             tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4Src, INT1 i1Type, INT1 i1Code));

INT4 udp_trcrt_open(UINT2 u2U_port,UINT4 u4Local_addr);

INT4 udp_open_trcrt_port(UINT2 u2U_port,UINT4 u4Local_addr);

INT4 udp_open_sli_port_in_cxt (UINT1 u1FdUsage, UINT4 u4ContextId,
                       UINT2 u2U_port,UINT4 u4Local_addr,
                       UINT2 u2U_Rport,UINT4 u4Remote_addr,INT4 i4SockDesc,
                       tOsixSemId SemId, tOsixQId RcvQId);

INT4 udpGetNextIndexUdpTable(UINT4 u4UdpLocalAddress, UINT4 *pu4NextLocalAddress, UINT2 u2UdpLocalPort,UINT2 *pu2NextPort);


VOID UDP_Process_Input_Q_Msg (VOID);
VOID UDP_Process_Input_Q_Event (VOID);

VOID UdpVcmCallbackFn (UINT4 u4IfIndex, UINT4 u4VcmCxtId, UINT1 u1BitMap);
VOID UdpCreateCxtEntry (tUdpCxt * pUdpCxtEntry);
INT4 UdpDeleteCxtEntry (tUdpCxt * pUdpCxtEntry);
INT4 UdpAddCBNodeToCxtTable (UINT4 u4ContextId, t_UDP_CB  * pUdpCbNode);
INT4 UdpDeleteCBNodeFromCxtTable (UINT4 u4ContextId, t_UDP_CB  * pUdpCbNode);
INT4 UdpAddCBNodeToGlbTable (t_UDP_CB  * pUdpCbNode);
INT4 UdpDeleteCBNodeFromGlbTable (t_UDP_CB  * pUdpCbNode);
VOID UdpMemDeInit (VOID);
INT4 UdpProcessCxtDeletion (UINT4 u4ContextId);
VOID UdpProcessCfgEvent (VOID);
t_UDP_STAT * UdpGetCurrCxtStatEntry (VOID);
t_UDP_STAT * UdpGetStatEntry (VOID);
tUdpCxt * UdpGetCxtEntryFromCxtId (UINT4 u4ContextId);
