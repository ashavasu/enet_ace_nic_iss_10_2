enum {
    MAX_UDP_CB_TAB_SIZE_SIZING_ID,
    MAX_UDP_CFG_MSGS_SIZING_ID,
    MAX_UDP_CONTEXTS_SIZING_ID,
    MAX_UDP_HL_PARMS_MSGS_SIZING_ID,
    UDP_MAX_SIZING_ID
};


#ifdef  _UDPSZ_C
tMemPoolId UDPMemPoolIds[ UDP_MAX_SIZING_ID];
INT4  UdpSizingMemCreateMemPools(VOID);
VOID  UdpSizingMemDeleteMemPools(VOID);
INT4  UdpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _UDPSZ_C  */
extern tMemPoolId UDPMemPoolIds[ ];
extern INT4  UdpSizingMemCreateMemPools(VOID);
extern VOID  UdpSizingMemDeleteMemPools(VOID);
extern INT4  UdpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _UDPSZ_C  */


#ifdef  _UDPSZ_C
tFsModSizingParams FsUDPSizingParams [] = {
{ "t_UDP_CB", "MAX_UDP_CB_TAB_SIZE", sizeof(t_UDP_CB),MAX_UDP_CB_TAB_SIZE, MAX_UDP_CB_TAB_SIZE,0 },
{ "tUdpCfgMsg", "MAX_UDP_CFG_MSGS", sizeof(tUdpCfgMsg),MAX_UDP_CFG_MSGS, MAX_UDP_CFG_MSGS,0 },
{ "tUdpCxt", "MAX_UDP_CONTEXTS", sizeof(tUdpCxt),MAX_UDP_CONTEXTS, MAX_UDP_CONTEXTS,0 },
{ "t_IP_TO_HLMS_PARMS", "MAX_UDP_HL_PARMS_MSGS", sizeof(t_IP_TO_HLMS_PARMS),MAX_UDP_HL_PARMS_MSGS, MAX_UDP_HL_PARMS_MSGS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _UDPSZ_C  */
extern tFsModSizingParams FsUDPSizingParams [];
#endif /*  _UDPSZ_C  */


