/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udppdudf.h,v 1.6 2013/05/02 11:29:00 siva Exp $
 *
 * Description:Type Definitions and Constant Definitions for
 *             User Datagram Protocol.                      
 *
 *******************************************************************/
#ifndef   __IP_UDP_H__
#define   __IP_UDP_H__


#define   UDP_CB_TAB_SIZE       50
#define   UDP_HDR_LEN            8
#define   UDP_NO_CHECKSUM        0
#define   UDP_VALID_CHECKSUM     0

#define   UDP_VCM_MSG_RCVD       1 
#define UDP_SIZING_CONTEXT_COUNT    FsUDPSizingParams[MAX_UDP_CONTEXTS_SIZING_ID].u4PreAllocatedUnits
/*
 * This defines the structure of an UDP header.
 * The header is extracted on the arrival of each message.
 */
typedef struct
{
    UINT2     u2Src_u_port;
    UINT2     u2Dest_u_port;
    UINT2     u2Len;
    UINT2     u2Cksum;
} t_UDP;


/* Well known UDP ports */
#define   UDP_RIP_PORT                 520
#define   UDP_ECHO_PORT                  7
#define   UDP_PKT_SRC_U_PORT_OFFSET      0
#define   UDP_PKT_CKSUM_OFFSET           6
INT4 udp_get_ipv4_cb PROTO ((UINT2 u2U_Lport, UINT4 u4LocalAddr, 
                        UINT2 u2U_Rport, UINT4 u4RemoteAddr, t_UDP_CB *));
INT4 udpGetNextIndexIpvxUdpTable (UINT4 u4UdpLocalAddress,
                                  UINT4 *pu4NextLocalAddresss,
                                  UINT2 u2UdpLocalPort,
                                  UINT2 *pu2NextPort,
                                  UINT4 u4UdpRemoteAddress,
                                  UINT4 *pu4NextRemoteAddress,
                                  UINT2 u2UdpRemotePort,
                                  UINT2 *pu2NextRemotePort);
#endif          /*__IP_UDP_H__*/
