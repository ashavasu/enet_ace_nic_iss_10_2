/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udpprtno.h,v 1.3 2008/12/30 09:50:27 prabuc-iss Exp $
 *
 * Description: UDP Port Numbers
 *
 *******************************************************************/
#ifndef   __UDP_PRTNO_H__
#define   __UDP_PRTNO_H__

/* DEFINITIONS */
/* ----------- */
#define   MAX_RECORD_OPT           9
#define   IP_OPT_RECORD_ROUTE      7
#define   IP_OPT_STRICT_ROUTE    137
#define   IP_OPT_LOOSE_ROUTE     131
#define   IP_OPT_TIMESTAMP        68
#define   PVT_UDP_PORT_BEGIN      49152
#define   PVT_UDP_PORT_END        65534
#define   UDP_ONE                 1
#define   UDP_ZERO                0
#define   UDP_IPV4_ADDR_TYPE      1
#define   UDP_IPV6_ADDR_TYPE      2
/* EXTERNS */
/* ------- */
extern t_UDP_STAT Udp_stat;               /* Udp statistics                */
extern t_UDP_CB *pUdp_cb_table;    /* Hash table to store user info */
extern tUdpTmrListId gUdpIcmpTimerListId;

#endif          /*__UDP_PRTNO_H__*/
