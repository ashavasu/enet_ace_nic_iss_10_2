/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: udpport.h,v 1.9 2013/07/04 13:12:51 siva Exp $
 *
 * Description: Definitions that have to be changed when    
 *              porting  
 *
 *******************************************************************/
#ifndef   __IP_UDPPORT_H__
#define   __IP_UDPPORT_H__

/* TYPE DEFINITIONS */
/* ---------------- */
typedef tTimerListId tUdpTmrListId;
typedef tTmrAppTimer tUdpTimer;


/* DEFINITIONS */
/* ----------- */
#define   UDP_TMR_SUCCESS        TMR_SUCCESS
#define   UDP_TMR_FAILURE        TMR_FAILURE
#define   UDP_EVENT_WAIT_FLAGS   (OSIX_WAIT | OSIX_EV_ANY)
#define   UDP_TASK_NAME          "UDP"

#define   UDP_EVENT_WAIT_TIMEOUT              0
#define   UDP_ICMP_TIMER_0_EVENT     0x00000002
#define   UDP_TRCRT_INPUT_Q_EVENT    0x00000004
#define   UDP_VCM_MSG_EVENT          0x00000008

#define   UDP_APP_DATA                        0
#define   UDP_APP_ERROR                       1


/* MACROS */
/* ------ */
#define   UDP_TIMER_DATA(pTimer)   (pTimer)->u4Data
#define   UDP_CALLOC_CB()          MEM_CALLOC (sizeof(t_UDP_CB), 1, t_UDP_CB)
#define   UDP_FREE_CB(pUdp_cb)     MEM_FREE(pUdp_cb)

#define UDP_CREATE_TMR_LIST(au1TaskName, u4Event, pTmrListId)  \
   TmrCreateTimerList((au1TaskName), (u4Event), NULL, (pTmrListId))

#define UDP_START_TIMER(TimerListId, pTmrRef, u4Duration)  \
   TmrStartTimer((TimerListId), (pTmrRef), \
           ((UINT4) (u4Duration) * LR_NUM_OF_SYS_TIME_UNITS_IN_A_SEC ))

#define UDP_STOP_TIMER(TimerListId, pTmrRef)  \
   TmrStopTimer((TimerListId), (pTmrRef))

#define UDP_RESIZE_TIMER(TimerListId, pTmrRef, u4Duration)   \
   TmrResizeTimer((TimerListId), (pTmrRef), (u4Duration))

#define UDP_RESTART_TIMER(TimerListId, pTmrRef, u4Duration)  \
   {\
      TmrStopTimer((TimerListId), (pTmrRef));   \
      TmrStartTimer((TimerListId), (pTmrRef), \
           LR_NUM_OF_SYS_TIME_UNITS_IN_A_SEC * (u4Duration)); \
   }

#define UDP_GET_EXPIRED_TIMERS(TimerListId, ppTmrRef)  \
   TmrGetExpiredTimers((TimerListId), (ppTmrRef))

#define UDP_NEXT_TIMER(TimerListId)  \
   TmrGetNextExpiredTimer((TimerListId))

#define UDP_CTRLTBL_SEMAPHORE    ((UINT1 *) "UDDS")
#define UDP_PROTOCOL_SEMAPHORE   ((UINT1 *) "UDPS")
 
INT4 UdpCtrlTblLock(VOID);
INT4 UdpCtrlTblUnLock(VOID);

#define  UDP_DS_LOCK     UdpCtrlTblLock   
#define  UDP_DS_UNLOCK   UdpCtrlTblUnLock 

typedef struct
{
    UINT4             u4UdpCurrentCxtId;
    tUdpCxt           *apUdpContextInfo [SYS_DEF_MAX_NUM_CONTEXTS];
    tUdpCxt           *pUdpCurrentCxt;
    tTMO_SLL          UdpCbEntry;   /* For global UDP sockets */
    t_UDP_STAT        UdpStat; /* For Global UDP statistics */
    tMemPoolId        UdpCBPoolId;
    tMemPoolId        UdpCxtPoolId;
    tMemPoolId        UdpCfgPoolId;
    tMemPoolId        UdpInputPoolId;
    tOsixSemId        UdpDSem;
    tOsixSemId        UdpSem;
    tUdpTmrListId     UdpIcmpTimerListId;
    tOsixTaskId       UdpTaskId;
    tOsixQId          UdpInputQId;
    tOsixQId          UdpTrcrtInputQId;
    tOsixQId          UdpCfgQId;
    UINT4             u4DefSockMode;
}tUdpGblInfo;

extern tUdpGblInfo gUdpGblInfo;

#define UDP_INVALID_CXT_ID        0xFFFFFFFF
#define UDP_DEFAULT_CXT_ID        0

typedef struct UdpVcmQMsg
{
    UINT4         u4ContextId;
    UINT4         u4MsgType;
    UINT1         u1BitMap;
    UINT1         au1Pad[3];
}
tUdpCfgMsg;

#define   UDP_ALLOC_CXT_ENTRY(x)\
          (x = (tUdpCxt*) IP_ALLOCATE_MEM_BLOCK(gUdpGblInfo.UdpCxtPoolId)) 

#define   UDP_FREE_CXT_ENTRY(x)\
          IP_RELEASE_MEM_BLOCK (gUdpGblInfo.UdpCxtPoolId, x)

#define  UDP_STAT_OUT_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4OutPkts++ : \
              gUdpGblInfo.UdpStat.u4OutPkts++)

#define  UDP_STAT_IN_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InPkts++ : \
              gUdpGblInfo.UdpStat.u4InPkts++)

#define  UDP_STAT_SUCC_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InSuccess++ : \
              gUdpGblInfo.UdpStat.u4InSuccess++)

#define  UDP_STAT_BCAST_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InBcast++ : \
              gUdpGblInfo.UdpStat.u4InBcast++)

#define  UDP_STAT_ERR_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InTotalErr++ : \
              gUdpGblInfo.UdpStat.u4InTotalErr++)

#define  UDP_STAT_NO_PORT_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InErrNoport++ : \
              gUdpGblInfo.UdpStat.u4InErrNoport++)

#define  UDP_STAT_ERR_CHKSUM_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InErrCksum++ : \
              gUdpGblInfo.UdpStat.u4InErrCksum++)

#define  UDP_STAT_NO_CHKSUM_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InNoCksum++ : \
              gUdpGblInfo.UdpStat.u4InNoCksum++)

#define  UDP_STAT_ERR_ICMP_INC(u1Mode, u4ContextId)  \
             ((u1Mode == SOCK_UNIQUE_MODE) ?  \
              gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u4InIcmpErr++ : \
              gUdpGblInfo.UdpStat.u4InIcmpErr++)

#define  UDP_STAT_HC_IN_INC(u1Mode, u4ContextId)  \
     if (u1Mode == SOCK_UNIQUE_MODE) {\
          FSAP_U8_INC (&( gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u8HcInPkts)); }\
          else {\
            FSAP_U8_INC (&( gUdpGblInfo.UdpStat.u8HcInPkts));} 

#define  UDP_STAT_HC_OUT_INC(u1Mode, u4ContextId)  \
     if (u1Mode == SOCK_UNIQUE_MODE) {\
          FSAP_U8_INC (&( gUdpGblInfo.apUdpContextInfo[u4ContextId]->UdpStat.u8HcOutPkts)); }\
          else {\
            FSAP_U8_INC (&( gUdpGblInfo.UdpStat.u8HcOutPkts));} 

/* DATA STRUCTURES */
/* --------------- */
/* This is the additional information passed to the applications
 * along with the message buffer.
 */
typedef struct
{
    union
    {
        struct
        {
            UINT2     u2Port;    /* Interface Index ; IP Handle */
        } Data;
        struct
        {
            INT1      i1Type;    /* Error Type  */
            INT1      i1Code;    /* Error Code  */
        } Err;
    } Parms;
    UINT1  u1Type_of_msg;
    UINT1  u1Pad;
} t_UDP_APP_MSG;

#define  UDP_APP_MSG_MALLOC() \
         MEM_MALLOC (sizeof (t_UDP_APP_MSG), t_UDP_APP_MSG)

#define udp_task_ip_src_addr_to_use_for_dest_in_cxt(u4ContextId,u4Dest,u4pSrc_addr) \
        ip_src_addr_to_use_for_dest_InCxt (u4ContextId,u4Dest,u4pSrc_addr)

/* If direct procedure calls are not allowed between  tasks then these
 * two procedures will have to be duplicated.
 */
#define udp_task_ip_verify_addr(u4Addr,i1Type)\
                 IpVerifyAddr(u4Addr,i1Type)

/* UDP Task creation info */
#define   UDP_TASK_PRIORITY     40                     
#define   UDP_TASK_STACK_SIZE   0x8000                 
#define   UDP_TASK_MODE         OSIX_DEFAULT_TASK_MODE 

/* UDP packet input Q info */
#define   UDP_INPUT_Q_NAME      "UDPQ"
#define   UDP_INPUT_Q_MODE      OSIX_GLOBAL
#define   UDP_INPUT_Q_DEPTH     100

/* Trace route to UDP packet input Q info */
#define   UDP_TRCRT_INPUT_Q_NAME      "UTRQ"
#define   UDP_TRCRT_INPUT_Q_MODE      OSIX_GLOBAL
#define   UDP_TRCRT_INPUT_Q_DEPTH     5

/* UDP - VCM input Q info */
#define   UDP_CFG_Q_NAME      "UCFQ"
#define   UDP_CFG_Q_DEPTH      VCM_MAX_L3_CONTEXT
#endif          /*__IP_UDPPORT_H__*/
