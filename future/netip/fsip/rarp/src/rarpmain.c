/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rarpmain.c,v 1.7 2013/07/04 13:12:04 siva Exp $
 *
 * Description:Contains routines which are common to both     
 *             RARP Client & Server.                      
 *
 *******************************************************************/
#include "rarpinc.h"

/*   define global variables here */
tRarpConfigTable    RarpConfigTable;
tRarpStatisTable    RarpStatisTable;
tRARP_SLL           ServerAddrList;    /* Server Address mapping List */

UINT1               gu1RarpClientState;
UINT1               gu1CurrentRetry;

/*****************************************************************************/
/*   Function Name      : RarpInit                                           */
/*   Description        : Initializes the Data structures for RARP Client    */
/*                      : & Server.Also initialises all global variables.    */
/*                                                                           */
/*   Input(s)           : None.                                              */
/*   Output(s)          : None.                                              */
/*   Return(s)          : None.                                              */
/*****************************************************************************/
#ifdef __STDC__
VOID
RarpInit ()
#else
VOID
RarpInit (VOID)
#endif
{

    /*  The Statistics variable are initialised to zero. */

    MEMSET (&RarpStatisTable, 0, sizeof (tRarpStatisTable));

    /*  Initialising the config variables */

    RarpConfigTable.u1ClientStatus = RARP_DEF_CLIENT_STATUS;
    RarpConfigTable.u1ServerStatus = RARP_DEF_SERVER_STATUS;
    RarpConfigTable.u4TimeoutInt = RARP_DEF_CLIENT_TIMEOUT;
    RarpConfigTable.u1MaxRetries = RARP_DEF_CLIENT_NUMBER_OF_RETRANS;
    RarpConfigTable.u1MaxEntries = RARP_DEF_SERVER_TABLE_ENTRIES;
    /* initialise the Server Address list */

    RARP_SLL_Init (&ServerAddrList);

    /* initailise all global variables */

    gu1RarpClientState = RARP_IDLE;
    gu1CurrentRetry = 0;
    RarpSizingMemCreateMemPools ();

}

/*****************************************************************************/
/*   Function Name      : RarpDeInit                                         */
/*   Description        : Deletes the mempool created for RARP               */
/*                                                                           */
/*   Input(s)           : None.                                              */
/*   Output(s)          : None.                                              */
/*   Return(s)          : None.                                              */
/*****************************************************************************/
#ifdef __STDC__
VOID
RarpDeInit ()
#else
VOID
RarpDeInit (VOID)
#endif
{
    if (RARP_ENTRY_POOL_ID != 0)
    {

        RarpSizingMemDeleteMemPools ();
    }

}

/*****************************************************************************/
/*   Function Name      : RarpInput                                          */
/*   Description        : This function is invoked by ARP whenever it        */
/*                        receives a packet with frame type as RARP.         */
/*                        This function calls appropriate functions based on */
/*                        the opcode in the arrived packet.                  */
/*                                                                           */
/*   Input(s)           : pBuf - The pointer to the incoming buffer.         */
/*                        u2Port - The port in which the packet was received.*/
/*                                                                           */
/*   Output(s)          : None.                                              */
/*   Return(s)          : IP_SUCCESS / IP_FAILURE.                            */
/*****************************************************************************/
#ifdef __STDC__
INT4
RarpInput (tRARP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port)
#else
INT4
RarpInput (pBuf, u2Port)
     tRARP_BUF_CHAIN_HEADER *pBuf;
     UINT2               u2Port;
#endif
{
    tRarpPacket         RarpPacket;
    INT1                i1RetVal;

    /* Extract the incoming buffer in to the local structure */

    if (RarpExtractHeader (&RarpPacket, pBuf) != IP_SUCCESS)
    {
        RARP_RELEASE_BUFFER (pBuf);
        return (IP_FAILURE);
    }

    /* release the  buffer */
    IP_TRC_ARG1 (RARP_MOD_TRC, BUFFER_TRC, RARP_NAME,
                 "Buffer at %x released after extraction", pBuf);

    RARP_RELEASE_BUFFER (pBuf);

    /* Based on the opcode call appropriate function */

    switch (RarpPacket.u2Opcode)
    {
        case RARP_REQUEST_REVERSE:

            IP_TRC_ARG1 (RARP_MOD_TRC, DATA_PATH_TRC, RARP_NAME,
                         "RARP request received on Port %d \n", u2Port);

            i1RetVal = RarpServerHandler (&RarpPacket, u2Port);
            if (i1RetVal != SUCCESS)
            {
                /* the request is discarded by the server */
                /* increment the statistics counter */
                RarpStatisTable.u2ReqsDiscarded++;
                return IP_FAILURE;
            }
            break;
        case RARP_REPLY_REVERSE:
            IP_TRC_ARG1 (RARP_MOD_TRC, DATA_PATH_TRC, RARP_NAME,
                         "RARP Reply received on Port %d \n", u2Port);

            i1RetVal = RarpClientHandler (&RarpPacket, u2Port);
            if (i1RetVal != SUCCESS)
            {
                /* the reply packet is discarded by the client */
                /* increment the statistics counter */

                RarpStatisTable.u2RepDiscarded++;
                return IP_FAILURE;
            }
            break;

        default:
            /* RARP packet with illegal opcode */
            return IP_FAILURE;
    }
    return (IP_SUCCESS);
}

/*****************************************************************************/
/*     Function Name         : RarpExtractHeader                             */
/*     Description           : This function will extract the buffer in to   */
/*                             RARP packet structure.                        */
/*                                                                           */
/*     Input(s)              : pBuf  - Pointer to the incoming buffer.       */
/*                                                                           */
/*     Output(s)             : pRarpPacket - pointer to the extracted packet.*/
/*     Return(s)             : IP_SUCCESS / IP_FAILURE.                       */
/*****************************************************************************/
#ifdef __STDC__
INT4
RarpExtractHeader (tRarpPacket * pRarpPacket, tRARP_BUF_CHAIN_HEADER * pBuf)
#else
INT4
RarpExtractHeader (pRarpPacket, pBuf)
     tRarpPacket        *pRarpPacket;
     tRARP_BUF_CHAIN_HEADER *pBuf;
#endif
{
    UINT1               au1RarpHdr[ARP_PACKET_LEN];
    UINT1               u1HwaLen = 0;
    UINT1               u1PaLen = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4TgtIp = 0;
    UINT4               u4Offset = 0;

    tRarpHeader        *pRarpHdr = NULL;

    if ((pRarpHdr =
         (tRarpHeader *) (VOID *) IP_GET_DATA_PTR_IF_LINEAR (pBuf, 0,
                                                             sizeof
                                                             (tRarpHeader))) ==
        NULL)
    {
        pRarpHdr = (tRarpHeader *) (VOID *) au1RarpHdr;
        IP_COPY_FROM_BUF (pBuf, (UINT1 *) pRarpHdr, 0, sizeof (tRarpHeader));
    }

    u1HwaLen = pRarpHdr->u1HwaLen;
    if (u1HwaLen > RARP_MAX_HW_ADDR_LEN)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP request dropped - Hardware Address Length is invalid \n");
        return (IP_FAILURE);
    }
    u1PaLen = pRarpHdr->u1PaLen;
    if (u1PaLen > RARP_MAX_PROTO_ADDR_LEN)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP request dropped - Protocol Address Length is invalid \n");
        return (IP_FAILURE);
    }

    pRarpPacket->u2HwAddrType = IP_NTOHS (pRarpHdr->u2HwType);
    pRarpPacket->u2ProtocolType = IP_NTOHS (pRarpHdr->u2ProtType);
    pRarpPacket->u1HwAddrLen = u1HwaLen;
    pRarpPacket->u1ProtocolAddrLen = u1PaLen;
    pRarpPacket->u2Opcode = IP_NTOHS (pRarpHdr->u2Opcode);

    /* Copy the remaining of the packet from pBuf */
    u4Offset = sizeof (tRarpHeader);

    IP_COPY_FROM_BUF (pBuf, &au1RarpHdr[u4Offset], u4Offset,
                      (UINT4) ((u1HwaLen + u1PaLen) * 2));

    /* Copy the Sender Hardware address */

    MEMCPY (pRarpPacket->au1ShwAddr, &au1RarpHdr[u4Offset], u1HwaLen);
    u4Offset += u1HwaLen;

    /* Copy the Sender Protocol address */

    MEMCPY (&u4SrcIp, &au1RarpHdr[u4Offset], u1PaLen);
    u4Offset += u1PaLen;

    /* Copy the Target Hardware address */

    MEMCPY (pRarpPacket->au1ThwAddr, &au1RarpHdr[u4Offset], u1HwaLen);
    u4Offset += u1HwaLen;

    /* Copy the Target Protocol address */

    MEMCPY (&u4TgtIp, &au1RarpHdr[u4Offset], u1PaLen);
    u4Offset += u1PaLen;

    pRarpPacket->u4SprotocolAddr = IP_NTOHL (u4SrcIp);
    pRarpPacket->u4TprotocolAddr = IP_NTOHL (u4TgtIp);

    return (IP_SUCCESS);
}

/*****************************************************************************/
/*     Function Name         : RarpPutHeader                                 */
/*     Description           : This function will put the RARP packet in to  */
/*                             a linear buffer.                              */
/*                                                                           */
/*     Input(s)              : pRarpPacket - Pointer to the RARP packet      */
/*                             which is to be put in to the buffer   .       */
/*                                                                           */
/*     Output(s)             : pBuf - The Buffer to be sent.                 */
/*     Return(s)             : None.                                         */
/*****************************************************************************/
#ifdef __STDC__
VOID
RarpPutHeader (tRarpPacket * pRarpPacket, tRARP_BUF_CHAIN_HEADER * pBuf)
#else
VOID
RarpPutHeader (pRarpPacket, pBuf)
     tRarpPacket        *pRarpPacket;
     tRARP_BUF_CHAIN_HEADER *pBuf;
#endif
{
    UINT1               au1RarpHdr[ARP_PACKET_LEN];
    UINT1               u1HwaLen = 0;
    UINT1               u1PaLen = 0;
    UINT4               u4SrcIp = 0;
    UINT4               u4TgtIp = 0;
    UINT4               u4Offset = 0;

    tRarpHeader        *pRarpHdr = NULL;

    pRarpHdr = (tRarpHeader *) (VOID *) au1RarpHdr;

    u1HwaLen = (UINT1) RARP_MAX_BYTES (pRarpPacket->u1HwAddrLen,
                                       CFA_ENET_ADDR_LEN);
    u1PaLen = (UINT1) RARP_MAX_BYTES (pRarpPacket->u1ProtocolAddrLen,
                                      ARP_MAX_PROTO_ADDR_LEN);

    pRarpHdr->u2HwType = IP_HTONS (pRarpPacket->u2HwAddrType);
    pRarpHdr->u2ProtType = IP_HTONS (pRarpPacket->u2ProtocolType);
    pRarpHdr->u1HwaLen = u1HwaLen;
    pRarpHdr->u1PaLen = u1PaLen;
    pRarpHdr->u2Opcode = IP_HTONS (pRarpPacket->u2Opcode);

    u4SrcIp = IP_HTONL (pRarpPacket->u4SprotocolAddr);
    u4TgtIp = IP_HTONL (pRarpPacket->u4TprotocolAddr);

    /* Copy the Sender Hardware address */

    u4Offset = sizeof (tRarpHeader);
    MEMCPY (&au1RarpHdr[u4Offset], pRarpPacket->au1ShwAddr, u1HwaLen);
    u4Offset += u1HwaLen;

    /* Copy the Sender Protocol address */

    MEMCPY (&au1RarpHdr[u4Offset], &u4SrcIp, u1PaLen);
    u4Offset += u1PaLen;

    /* Copy the Target Hardware address */

    MEMCPY (&au1RarpHdr[u4Offset], pRarpPacket->au1ThwAddr, u1HwaLen);
    u4Offset += u1HwaLen;

    /* Copy the Target Protocol address */

    MEMCPY (&au1RarpHdr[u4Offset], &u4TgtIp, u1PaLen);
    u4Offset += u1PaLen;

    /* Copy the message to the buffer, The size of Msg will be u4Offset */

    IP_COPY_TO_BUF (pBuf, au1RarpHdr, 0, u4Offset);

}
