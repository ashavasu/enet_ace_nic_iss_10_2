/* $Id: rarpsz.c,v 1.4 2013/11/29 11:04:15 siva Exp $*/
#define _RARPSZ_C
#include "rarpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RarpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RARP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsRARPSizingParams[i4SizingId].
                                     u4StructSize,
                                     FsRARPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(RARPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RarpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RarpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRARPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RARPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RarpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RARP_MAX_SIZING_ID; i4SizingId++)
    {
        if (RARPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RARPMemPoolIds[i4SizingId]);
            RARPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
