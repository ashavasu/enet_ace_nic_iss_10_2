/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rarpsnif.c,v 1.12 2013/12/14 11:29:32 siva Exp $
 *
 * Description: Contains low level routines 
 *
 *******************************************************************/

#include "rarpinc.h"
#include "fsiplow.h"
#include "fsmpipcli.h"

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsRarpClientRetransmissionTimeout
 Input       :  The Indices

                The Object 
                retValRarpClientRetransmissionTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRarpClientRetransmissionTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRarpClientRetransmissionTimeout (INT4
                                         *pi4RetValRarpClientRetransmissionTimeout)
#else
INT1                nmhGetFsRarpClientRetransmissionTimeout
    (pi4RetValRarpClientRetransmissionTimeout)
     INT4               *pi4RetValRarpClientRetransmissionTimeout;
#endif
{
    *pi4RetValRarpClientRetransmissionTimeout =
        (INT4) RarpConfigTable.u4TimeoutInt;
    return (SNMP_SUCCESS);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsRarpClientMaxRetries
 Input       :  The Indices

                The Object 
                retValRarpClientMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRarpClientMaxRetries ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRarpClientMaxRetries (INT4 *pi4RetValRarpClientMaxRetries)
#else
INT1
nmhGetFsRarpClientMaxRetries (pi4RetValRarpClientMaxRetries)
     INT4               *pi4RetValRarpClientMaxRetries;
#endif
{
    *pi4RetValRarpClientMaxRetries = (INT4) RarpConfigTable.u1MaxRetries;
    return (SNMP_SUCCESS);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsRarpClientPktsDiscarded
 Input       :  The Indices

                The Object 
                retValRarpClientPktsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRarpClientPktsDiscarded ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRarpClientPktsDiscarded (UINT4 *pu4RetValRarpClientPktsDiscarded)
#else
INT1
nmhGetFsRarpClientPktsDiscarded (pu4RetValRarpClientPktsDiscarded)
     UINT4              *pu4RetValRarpClientPktsDiscarded;
#endif
{
    *pu4RetValRarpClientPktsDiscarded = (UINT4) RarpStatisTable.u2RepDiscarded;
    return (SNMP_SUCCESS);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRarpClientRetransmissionTimeout
 Input       :  The Indices

                The Object 
                setValRarpClientRetransmissionTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRarpClientRetransmissionTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRarpClientRetransmissionTimeout (INT4
                                         i4SetValRarpClientRetransmissionTimeout)
#else
INT1                nmhSetFsRarpClientRetransmissionTimeout
    (i4SetValRarpClientRetransmissionTimeout)
     INT4                i4SetValRarpClientRetransmissionTimeout;
#endif
{

    RarpConfigTable.u4TimeoutInt =
        (UINT4) i4SetValRarpClientRetransmissionTimeout;

    IP_TRC_ARG1 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                 "RarpClientRetransmissionTimeout is set to %d \n",
                 i4SetValRarpClientRetransmissionTimeout);

    IncMsrForFsIpv4Scalars (i4SetValRarpClientRetransmissionTimeout,
                            FsMIFsRarpClientRetransmissionTimeout,
                            (sizeof (FsMIFsRarpClientRetransmissionTimeout) /
                             sizeof (UINT4)));

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsRarpClientMaxRetries
 Input       :  The Indices

                The Object 
                setValRarpClientMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRarpClientMaxRetries ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRarpClientMaxRetries (INT4 i4SetValRarpClientMaxRetries)
#else
INT1
nmhSetFsRarpClientMaxRetries (i4SetValRarpClientMaxRetries)
     INT4                i4SetValRarpClientMaxRetries;

#endif
{
    RarpConfigTable.u1MaxRetries = (UINT1) i4SetValRarpClientMaxRetries;

    IP_TRC_ARG1 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                 "Max retries of RARP request is set to %d \n",
                 i4SetValRarpClientMaxRetries);

    IncMsrForFsIpv4Scalars (i4SetValRarpClientMaxRetries,
                            FsMIFsRarpClientMaxRetries,
                            (sizeof (FsMIFsRarpClientMaxRetries) /
                             sizeof (UINT4)));

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRarpClientRetransmissionTimeout
 Input       :  The Indices

                The Object 
                testValRarpClientRetransmissionTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRarpClientRetransmissionTimeout ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRarpClientRetransmissionTimeout (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValRarpClientRetransmissionTimeout)
#else
INT1
nmhTestv2FsRarpClientRetransmissionTimeout (pu4ErrorCode,
                                            i4TestValRarpClientRetransmissionTimeout)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValRarpClientRetransmissionTimeout;
#endif
{

    if ((i4TestValRarpClientRetransmissionTimeout >= RARP_MIN_CLIENT_TIMEOUT)
        && (i4TestValRarpClientRetransmissionTimeout <=
            RARP_MAX_CLIENT_TIMEOUT))
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
            "Set on client retransmission timeout failed - "
            "Value out of range \n");

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2FsRarpClientMaxRetries
 Input       :  The Indices

                The Object 
                testValRarpClientMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRarpClientMaxRetries ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRarpClientMaxRetries (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValRarpClientMaxRetries)
#else
INT1
nmhTestv2FsRarpClientMaxRetries (pu4ErrorCode, i4TestValRarpClientMaxRetries)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValRarpClientMaxRetries;
#endif
{
    if ((i4TestValRarpClientMaxRetries >= RARP_MIN_CLIENT_NUMBER_OF_RETRANS)
        && (i4TestValRarpClientMaxRetries <= RARP_MAX_CLIENT_NUMBER_OF_RETRANS))
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
            "Set on client maximum retries failed - Value out of range \n");

    return (SNMP_FAILURE);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRarpServerStatus
 Input       :  The Indices

                The Object 
                retValRarpServerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRarpServerStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRarpServerStatus (INT4 *pi4RetValRarpServerStatus)
#else
INT1
nmhGetFsRarpServerStatus (pi4RetValRarpServerStatus)
     INT4               *pi4RetValRarpServerStatus;
#endif
{
    *pi4RetValRarpServerStatus = (INT4) RarpConfigTable.u1ServerStatus;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsRarpServerPktsDiscarded
 Input       :  The Indices

                The Object 
                retValRarpServerPktsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRarpServerPktsDiscarded ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRarpServerPktsDiscarded (UINT4 *pu4RetValRarpServerPktsDiscarded)
#else
INT1
nmhGetFsRarpServerPktsDiscarded (pu4RetValRarpServerPktsDiscarded)
     UINT4              *pu4RetValRarpServerPktsDiscarded;
#endif
{
    *pu4RetValRarpServerPktsDiscarded = (UINT4) RarpStatisTable.u2ReqsDiscarded;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 * Function    :  nmhGetFsRarpServerTableMaxEntries
 *  Input       :  The Indices
 *
 *                  The Object
 *                  retValRarpServerTableMaxEntries
 *  Output      :  The Get Low Lev Routine
 *                 Take the Indices &
 *                 store the Value requested
 *                 in the Return val.
 *  Returns     :
 *                SNMP_SUCCESS or
 *                SNMP_FAILURE
 *****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRarpServerTableMaxEntries ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRarpServerTableMaxEntries (INT4 *pi4RetValRarpServerTableMaxEntries)
#else
INT1
nmhGetFsRarpServerTableMaxEntries (pi4RetValRarpServerTableMaxEntries)
     INT4               *pi4RetValRarpServerTableMaxEntries;
#endif
{
    *pi4RetValRarpServerTableMaxEntries = RarpConfigTable.u1MaxEntries;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRarpServerStatus
 Input       :  The Indices

                The Object 
                setValRarpServerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRarpServerStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRarpServerStatus (INT4 i4SetValRarpServerStatus)
#else
INT1
nmhSetFsRarpServerStatus (i4SetValRarpServerStatus)
     INT4                i4SetValRarpServerStatus;
#endif
{

    RarpConfigTable.u1ServerStatus = (UINT1) i4SetValRarpServerStatus;

    IP_TRC_ARG1 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                 "RARP Server status is set to %d "
                 "(1 - Enabled and 2- Disabled) \n", i4SetValRarpServerStatus);

    IncMsrForFsIpv4Scalars (i4SetValRarpServerStatus,
                            FsMIFsRarpServerStatus,
                            (sizeof (FsMIFsRarpServerStatus) / sizeof (UINT4)));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 * Function    :  nmhSetFsRarpServerTableMaxEntries
 *  Input       :  The Indices
 *  
 *                  The Object
 *                  setValRarpServerTableMaxEntries
 *  Output      :  The Set Low Lev Routine
 *                 Take the Indices &
 *                 Sets the Value
 *                  accordingly.
 *  Returns     :
 *                 SNMP_SUCCESS or
 *                 SNMP_FAILURE
 *************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRarpServerTableMaxEntries ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRarpServerTableMaxEntries (INT4 i4SetValRarpServerTableMaxEntries)
#else
INT1
nmhSetFsRarpServerTableMaxEntries (i4SetValRarpServerTableMaxEntries)
     INT4                i4SetValRarpServerTableMaxEntries;
#endif
{
    RarpConfigTable.u1MaxEntries = (UINT1) i4SetValRarpServerTableMaxEntries;

    IP_TRC_ARG1 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                 "RARP Server max client entries is set to %d \n",
                 i4SetValRarpServerTableMaxEntries);

    IncMsrForFsIpv4Scalars (i4SetValRarpServerTableMaxEntries,
                            FsMIFsRarpServerTableMaxEntries,
                            (sizeof (FsMIFsRarpServerTableMaxEntries) /
                             sizeof (UINT4)));

    return SNMP_SUCCESS;
}

    /* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRarpServerStatus
 Input       :  The Indices

                The Object 
                testValRarpServerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRarpServerStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRarpServerStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValRarpServerStatus)
#else
INT1
nmhTestv2FsRarpServerStatus (pu4ErrorCode, i4TestValRarpServerStatus)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValRarpServerStatus;
#endif
{
    if ((i4TestValRarpServerStatus == RARP_SERVER_STATUS_ENABLE)
        || (i4TestValRarpServerStatus == RARP_SERVER_STATUS_DISABLE))
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
            "Set on RARP Server status failed - Value out of range \n");

    return (SNMP_FAILURE);
}

/****************************************************************************
 * Function    :  nmhTestv2FsRarpServerTableMaxEntries
 *  Input       :  The Indices
 *
 *                  The Object
 *                      testValRarpServerTableMaxEntries
 *  Output      :  The Test Low Lev Routine
 *                 Take the Indices &
 *                 Test whether that Value
 *                 is Valid Input for Set.
 *  Returns     :
 *                 SNMP_SUCCESS or
 *                 SNMP_FAILURE
 *****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRarpServerTableMaxEntries ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRarpServerTableMaxEntries (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValRarpServerTableMaxEntries)
#else
INT1
nmhTestv2FsRarpServerTableMaxEntries (pu4ErrorCode,
                                      i4TestValRarpServerTableMaxEntries)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValRarpServerTableMaxEntries;
#endif
{
    if ((i4TestValRarpServerTableMaxEntries >= RARP_MIN_SERVER_TABLE_ENTRIES)
        && (i4TestValRarpServerTableMaxEntries <=
            RARP_MAX_SERVER_TABLE_ENTRIES))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

    IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
            "Set on RARP Server maximum table entries failed - "
            "Value out of range \n");

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : RarpServerDatabaseTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRarpServerDatabaseTable
 Input       :  The Indices
                HardwareAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceRarpServerDatabaseTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsRarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE *
                                                   pHardwareAddress)
#else
INT1
nmhValidateIndexInstanceFsRarpServerDatabaseTable (pHardwareAddress)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
#endif
{
    tServerAddrNode    *pNode;

    pNode = CheckThisIndex (pHardwareAddress);
    if (pNode == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRarpServerDatabaseTable
 Input       :  The Indices
                HardwareAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFsFirstRarpServerDatabaseTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsRarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE *
                                           pHardwareAddress)
#else
INT1
nmhGetFirstIndexFsRarpServerDatabaseTable (pHardwareAddress)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
#endif
{
    tSNMP_OCTET_STRING_TYPE *pHwAddress = NULL;

    pHwAddress = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring
        (RARP_MAX_HW_ADDR_LEN);
    if (pHwAddress == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pHwAddress->pu1_OctetList, 0, RARP_MAX_HW_ADDR_LEN);

    if (nmhGetNextIndexFsRarpServerDatabaseTable (pHwAddress,
                                                  pHardwareAddress) ==
        SNMP_SUCCESS)
    {
        free_octetstring (pHwAddress);
        return SNMP_SUCCESS;
    }
    free_octetstring (pHwAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRarpServerDatabaseTable
 Input       :  The Indices
                HardwareAddress
                nextHardwareAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetFsNextRarpServerDatabaseTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsRarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE *
                                          pHardwareAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextHardwareAddress)
#else
INT1
nmhGetNextIndexFsRarpServerDatabaseTable (pHardwareAddress,
                                          pNextHardwareAddress)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     tSNMP_OCTET_STRING_TYPE *pNextHardwareAddress;
#endif
{
    tServerAddrNode    *pNode;
    tSNMP_OCTET_STRING_TYPE *pNextHwAddress = NULL;
    UINT1               u1Found = FALSE;

    pNextHwAddress = (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring
        (RARP_MAX_HW_ADDR_LEN);
    if (pNextHwAddress == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pNextHwAddress->pu1_OctetList, 0xff, RARP_MAX_HW_ADDR_LEN);
    RARP_SLL_Scan (&ServerAddrList, pNode, tServerAddrNode *)
    {
        if (pNode != NULL)
        {
            if ((MEMCMP (pNode->au1HwAddr, pNextHwAddress->pu1_OctetList,
                         RARP_MAX_BYTES (pNode->u1HwAddrLen,
                                         CFA_ENET_ADDR_LEN)) < 0)
                &&
                (MEMCMP
                 (pNode->au1HwAddr, pHardwareAddress->pu1_OctetList,
                  RARP_MAX_BYTES (pNode->u1HwAddrLen, CFA_ENET_ADDR_LEN)) > 0))
            {
                MEMCPY (pNextHwAddress->pu1_OctetList, pNode->au1HwAddr,
                        RARP_MAX_BYTES (pNode->u1HwAddrLen, CFA_ENET_ADDR_LEN));
                pNextHwAddress->i4_Length = (INT4) pNode->u1HwAddrLen;
                u1Found = TRUE;
            }
        }
    }
    if (u1Found == TRUE)
    {
        MEMCPY (pNextHardwareAddress->pu1_OctetList,
                pNextHwAddress->pu1_OctetList, pNextHwAddress->i4_Length);
        pNextHardwareAddress->i4_Length = pNextHwAddress->i4_Length;

        free_octetstring (pNextHwAddress);
        return (SNMP_SUCCESS);
    }
    free_octetstring (pNextHwAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsHardwareAddrLen
 Input       :  The Indices
                HardwareAddress

                The Object 
                retValHardwareAddrLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsHardwareAddrLen ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsHardwareAddrLen (tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                         INT4 *pi4RetValHardwareAddrLen)
#else
INT1
nmhGetFsHardwareAddrLen (pHardwareAddress, pi4RetValHardwareAddrLen)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     INT4               *pi4RetValHardwareAddrLen;
#endif
{
    tServerAddrNode    *pNode;

    pNode = CheckThisIndex (pHardwareAddress);

    if (pNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* now we can return the hardware address length */

    *pi4RetValHardwareAddrLen = (INT4) pNode->u1HwAddrLen;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsProtocolAddress
 Input       :  The Indices
                HardwareAddress

                The Object 
                retValProtocolAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsProtocolAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsProtocolAddress (tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                         UINT4 *pu4RetValProtocolAddress)
#else
INT1
nmhGetFsProtocolAddress (pHardwareAddress, pu4RetValProtocolAddress)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     UINT4              *pu4RetValProtocolAddress;
#endif
{
    tServerAddrNode    *pNode;

    pNode = CheckThisIndex (pHardwareAddress);
    if (pNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValProtocolAddress = pNode->u4ProtocolAddr;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsEntryStatus
 Input       :  The Indices
                HardwareAddress

                The Object 
                retValEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsEntryStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsEntryStatus (tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                     INT4 *pi4RetValEntryStatus)
#else
INT1
nmhGetFsEntryStatus (pHardwareAddress, pi4RetValEntryStatus)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     INT4               *pi4RetValEntryStatus;
#endif
{
    tServerAddrNode    *pNode;

    pNode = CheckThisIndex (pHardwareAddress);

    if (pNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    if ((pNode->u1EntryStatus == ACTIVE)
        || (pNode->u1EntryStatus == NOT_IN_SERVICE))
    {
        *pi4RetValEntryStatus = (INT4) pNode->u1EntryStatus;
        return (SNMP_SUCCESS);
    }
    else
    {
        *pi4RetValEntryStatus = NOT_READY;
        return (SNMP_SUCCESS);
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsHardwareAddrLen
 Input       :  The Indices
                HardwareAddress

                The Object 
                setValHardwareAddrLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsHardwareAddrLen ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsHardwareAddrLen (tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                         INT4 i4SetValHardwareAddrLen)
#else
INT1
nmhSetFsHardwareAddrLen (pHardwareAddress, i4SetValHardwareAddrLen)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     INT4                i4SetValHardwareAddrLen;
#endif
{
    tServerAddrNode    *pNode;

    pNode = NULL;

    /* check whether we have an entry with this hardware address as index */

    pNode = CheckThisIndex (pHardwareAddress);
    if (pNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* check whether the entry status is Not-In-Service */

    if (pNode->u1EntryStatus == NOT_IN_SERVICE)
    {
        pNode->u1HwAddrLen = (UINT1) i4SetValHardwareAddrLen;

        IP_TRC_ARG2 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                     "Set of hardware address length for Hardware address %s  "
                     "to %d is success \n",
                     pHardwareAddress->pu1_OctetList, i4SetValHardwareAddrLen);

        IncMsrForIpv4RarpServerDatabaseTable (pHardwareAddress, 'i',
                                              (VOID *) &i4SetValHardwareAddrLen,
                                              FsMIFsHardwareAddrLen,
                                              (sizeof (FsMIFsHardwareAddrLen) /
                                               sizeof (UINT4)), FALSE);
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhSetFsProtocolAddress
 Input       :  The Indices
                HardwareAddress

                The Object 
                setValProtocolAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsProtocolAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsProtocolAddress (tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                         UINT4 u4SetValProtocolAddress)
#else
INT1
nmhSetFsProtocolAddress (pHardwareAddress, u4SetValProtocolAddress)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     UINT4               u4SetValProtocolAddress;

#endif
{
    tServerAddrNode    *pNode;

    /* wheck whether we have an entry for this hardware address */

    pNode = CheckThisIndex (pHardwareAddress);

    if (pNode == NULL)
    {
        return (SNMP_FAILURE);
    }
    /* check whether the entry status is Not-In-Service  */

    if (pNode->u1EntryStatus == NOT_IN_SERVICE)
    {
        pNode->u4ProtocolAddr = u4SetValProtocolAddress;

        IP_TRC_ARG2 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                     "Set of protocol address for Hardware address %s to %x  "
                     "is success \n",
                     pHardwareAddress->pu1_OctetList, u4SetValProtocolAddress);

        IncMsrForIpv4RarpServerDatabaseTable (pHardwareAddress, 'p',
                                              (VOID *) &u4SetValProtocolAddress,
                                              FsMIFsProtocolAddress,
                                              (sizeof (FsMIFsProtocolAddress) /
                                               sizeof (UINT4)), FALSE);

        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetFsEntryStatus
 Input       :  The Indices
                HardwareAddress

                The Object 
                setValEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsEntryStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsEntryStatus (tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                     INT4 i4SetValEntryStatus)
#else
INT1
nmhSetFsEntryStatus (pHardwareAddress, i4SetValEntryStatus)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     INT4                i4SetValEntryStatus;

#endif
{
    tServerAddrNode    *pNode;
    UINT4               u4NodeCount;
    UINT1               u1PhysAddrLength;

    if (RarpConfigTable.u1ServerStatus != ENABLED)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValEntryStatus)
    {
        case CREATE_AND_WAIT:
            /* get the node count in the server address list */
            u4NodeCount = RARP_SLL_Count (&ServerAddrList);

            /* check whether the node count is less than the
             * RARP_MAX_SERVER_ADDRESS_ENTRIES */

            if (u4NodeCount < RarpConfigTable.u1MaxEntries)
            {
                /* node should be added */
                pNode = (tServerAddrNode *) MemAllocMemBlk (RARP_ENTRY_POOL_ID);
                if (pNode == NULL)
                {
                    IP_TRC (RARP_MOD_TRC, ALL_FAILURE_TRC, RARP_NAME,
                            "Memory allocation failed for Node \r\n");
                    return SNMP_FAILURE;
                }

                RARP_SLL_Init_Node (&(pNode->NextServerAddrNode));
                u1PhysAddrLength = (UINT1) pHardwareAddress->i4_Length;

                MEMCPY (pNode->au1HwAddr, pHardwareAddress->pu1_OctetList,
                        RARP_MAX_BYTES (u1PhysAddrLength, CFA_ENET_ADDR_LEN));

                pNode->u1EntryStatus = (UINT1) NOT_IN_SERVICE;
                pNode->u1HwAddrLen = u1PhysAddrLength;

                pNode->u4ProtocolAddr = 0;
                RARP_SLL_Add (&ServerAddrList, &(pNode->NextServerAddrNode));

                IP_TRC_ARG2 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                             "Set of entry status for Hardware address %s  "
                             "to %d is success \n",
                             pHardwareAddress->pu1_OctetList,
                             i4SetValEntryStatus);
            }
            else
            {
                /*  number of entries exceeded the maximum count allowed */
                return (SNMP_FAILURE);
            }
            break;

        case ACTIVE:
            pNode = CheckThisIndex (pHardwareAddress);
            if ((pNode != NULL) && (pNode->u4ProtocolAddr != 0))
            {
                /* all entries are filled */
                pNode->u1EntryStatus = ACTIVE;
                IP_TRC_ARG2 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                             "Set of entry status for Hardware address %s  "
                             "to %d is success \n",
                             pHardwareAddress->pu1_OctetList,
                             i4SetValEntryStatus);
            }
            else
            {
                /* entries not filled */
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:
            pNode = CheckThisIndex (pHardwareAddress);
            RARP_SLL_Delete (&ServerAddrList, &(pNode->NextServerAddrNode));
            MemReleaseMemBlock (RARP_ENTRY_POOL_ID, (UINT1 *) pNode);

            IP_TRC_ARG2 (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                         "Set of entry status for Hardware address %s "
                         "to %d is success \n",
                         pHardwareAddress->pu1_OctetList, i4SetValEntryStatus);
            break;

        default:
            return (SNMP_FAILURE);

    }

    IncMsrForIpv4RarpServerDatabaseTable (pHardwareAddress, 'i',
                                          (VOID *) &i4SetValEntryStatus,
                                          FsMIFsEntryStatus,
                                          (sizeof (FsMIFsEntryStatus) /
                                           sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsHardwareAddrLen
 Input       :  The Indices
                HardwareAddress

                The Object 
                testValHardwareAddrLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsHardwareAddrLen ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsHardwareAddrLen (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                            INT4 i4TestValHardwareAddrLen)
#else
INT1
nmhTestv2FsHardwareAddrLen (pu4ErrorCode, pHardwareAddress,
                            i4TestValHardwareAddrLen)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     INT4                i4TestValHardwareAddrLen;
#endif
{
    tServerAddrNode    *pNode;

    pNode = CheckThisIndex (pHardwareAddress);

    if (pNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    /* check whether the entry status is Not-In-Service  */
    /* 19 May 2004 - SC_L3_ADD - */
    if (pNode->u1EntryStatus == NOT_IN_SERVICE)
    {
        if ((pHardwareAddress->i4_Length == i4TestValHardwareAddrLen)
            && (pHardwareAddress->i4_Length == RARP_MAX_HW_ADDR_LEN))
        {
            return (SNMP_SUCCESS);
        }
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
            "Set on RARP Server table -Hardware address length failed - "
            "Value out of range \n");

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2FsProtocolAddress
 Input       :  The Indices
                HardwareAddress

                The Object 
                testValProtocolAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsProtocolAddress ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsProtocolAddress (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                            UINT4 u4TestValProtocolAddress)
#else
INT1
nmhTestv2FsProtocolAddress (pu4ErrorCode, pHardwareAddress,
                            u4TestValProtocolAddress)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     UINT4               u4TestValProtocolAddress;
#endif
{

    tServerAddrNode    *pNode;

    UNUSED_PARAM (u4TestValProtocolAddress);

    pNode = CheckThisIndex (pHardwareAddress);

    if (pNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }
    /* check whether the entry status is Not-In-Service  */
    /* 19 May 2004 - SC_L3_ADD - */
    if (pNode->u1EntryStatus == NOT_IN_SERVICE)
    {
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsEntryStatus
 Input       :  The Indices
                HardwareAddress

                The Object 
                testValEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsEntryStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsEntryStatus (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                        INT4 i4TestValEntryStatus)
#else
INT1
nmhTestv2FsEntryStatus (pu4ErrorCode, pHardwareAddress, i4TestValEntryStatus)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
     INT4                i4TestValEntryStatus;
#endif
{
    tServerAddrNode    *pNode;
    UINT4               u4NodeCount;
    pNode = CheckThisIndex (pHardwareAddress);
    if (pNode == NULL)
    {
        /* New Entry is to be created */
        /* So Check whether the Entry Status is CREATE & WAIT */
        if (i4TestValEntryStatus == CREATE_AND_WAIT)
        {
            /* get the node count in the server address list */
            u4NodeCount = RARP_SLL_Count (&ServerAddrList);

            if (u4NodeCount == RarpConfigTable.u1MaxEntries)
            {
                *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                        "Set on RARP Server table -Entry Creation failed - "
                        "Max Entries already reached \n");

                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;

            IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                    "Set on RARP Server table -Entry status failed - "
                    "Value out of range \n");

            return SNMP_FAILURE;
        }
    }
    else
    {
        /* we already have an entry for the index.  */
        /* check whether the entry status is ACTIVE or DESTROY */

        if ((i4TestValEntryStatus == ACTIVE)
            || (i4TestValEntryStatus == DESTROY))
        {
            return (SNMP_SUCCESS);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            IP_TRC (RARP_MOD_TRC, MGMT_TRC, RARP_NAME,
                    "Set on RARP Server table -Entry status failed - "
                    "Value out of range \n");

            return SNMP_FAILURE;
        }
    }
}

/*****************************************************************************/
/*    Function Name          : CheckThisIndex                                */
/*    Description            : Checks whether we have an entry in the server */
/*                             address list with this hardware address.      */
/*    Input(s)               : pHardwareAddress - Pointer to the incoming    */
/*                             hardware address.                             */
/*    Output(s)              : pNode - the pointer to this hardware address  */
/*                             in the server address list.                   */
/*    Return(s)              : SNMP_SUCCESS - If an entry is found with this */
/*                             hardware address.                             */
/*                             SNMP_FAILURE  -If not found.                  */
/*****************************************************************************/
#ifdef __STDC__
tServerAddrNode    *
CheckThisIndex (tSNMP_OCTET_STRING_TYPE * pHardwareAddress)
#else
tServerAddrNode    *
CheckThisIndex (pHardwareAddress)
     tSNMP_OCTET_STRING_TYPE *pHardwareAddress;
#endif
{
    UINT1               u1Length;
    tServerAddrNode    *pNode;

    u1Length = (UINT1) pHardwareAddress->i4_Length;

    RARP_SLL_Scan (&ServerAddrList, pNode, tServerAddrNode *)
    {
        /* if the hardware address length is not equal then skip this entry */
        if (pNode->u1HwAddrLen == u1Length)
        {
            /* compare the hardware address */
            if (MEMCMP
                (pHardwareAddress->pu1_OctetList, pNode->au1HwAddr,
                 RARP_MAX_BYTES (pNode->u1HwAddrLen, CFA_ENET_ADDR_LEN)) == 0)
            {
                /* this is the searched hardware address */
                /*        return(SNMP_SUCCESS); */
                return (pNode);
            }
        }
    }
    return (NULL);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRarpClientRetransmissionTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRarpClientRetransmissionTimeout (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRarpClientMaxRetries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRarpClientMaxRetries (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRarpServerStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRarpServerStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRarpServerTableMaxEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRarpServerTableMaxEntries (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRarpServerDatabaseTable
 Input       :  The Indices
                FsHardwareAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRarpServerDatabaseTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
