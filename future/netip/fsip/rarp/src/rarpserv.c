/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rarpserv.c,v 1.5 2013/07/04 13:12:04 siva Exp $
 *
 * Description: Contains functions of RARP SERVER .  
 *
 *******************************************************************/
#include "rarpinc.h"

/*****************************************************************************/
/*  Function Name        : RarpServerHandler                                 */
/*                                                                           */
/*  Description       : This function is invoked by the RarpInput module  */
/*                       whenever it receives a RARP packet with Opcode    */
/*                         as RARP Request.                                  */
/*                         This function will validate the request and it    */
/*                         will send the reply to the client.                */
/*                                                                           */
/* Input(s)              : pRarpRequest - Pointer to the incoming RARP       */
/*                         u2Port - the port in which this packet arrived.   */
/*                                                                           */
/*  Output(s)            : None                                              */
/*  Return(s)            :                                                   */
/*****************************************************************************/
#ifdef __STDC__
INT1
RarpServerHandler (tRarpPacket * pRarpRequest, UINT2 u2Port)
#else
INT1
RarpServerHandler (pRarpRequest, u2Port)
     tRarpPacket        *pRarpRequest;
     UINT2               u2Port;
#endif
{
    UINT1               au1ThwAddr[RARP_MAX_ENET_ADDR_LEN];
    UINT4               u4TprotocolAddr;
    INT2                i2Hardware;
    tIfConfigRecord     tIpIfInfo;
    tCfaIfInfo          CfaIfInfo;
    tRARP_BUF_CHAIN_HEADER *pBuf;

    if (RarpConfigTable.u1ServerStatus != ENABLED)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP request dropped - Server is Disabled \n");

        return (RARP_SERVER_NOT_ENABLED);
    }

    /* Get the HardwareType  from port */
    if (IpGetIfConfigRecord (u2Port, &tIpIfInfo) != IP_SUCCESS)
    {
        return IP_FAILURE;
    }

    i2Hardware = ARP_IFACE_TYPE (tIpIfInfo.InterfaceId.u1_InterfaceType);

    /*check the hardware type in the request */

    if (pRarpRequest->u2HwAddrType != (UINT2) i2Hardware)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP request dropped - Invalid hardware address type \n");

        return (INVALID_HARDWARE_ADDRESS_TYPE);
    }

    /* check the protocol type */

    if (pRarpRequest->u2ProtocolType != IP_PROTOCOL_TYPE)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP request dropped - Invalid protocol address type \n");

        return (INVALID_PROTOCOL_ADDRESS_TYPE);
    }

    /* check the hardware address length in the request packet */

    if (pRarpRequest->u1HwAddrLen != CFA_ENET_ADDR_LEN)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP request dropped - Invalid hardware address length \n");

        return (INVALID_HARDWARE_ADDRESS_LENGTH);
    }
    /* check the protocol address length */

    if (pRarpRequest->u1ProtocolAddrLen != IP_PROTO_ADDR_LEN)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP request dropped - Invalid protocol address length \n");

        return (INVALID_PROTOCOL_ADDRESS_LENGTH);
    }
    /* Copy the Target Hardware address in the request */

    MEMCPY (au1ThwAddr, pRarpRequest->au1ThwAddr, pRarpRequest->u1HwAddrLen);
    /* check whether we have entry for this hardware address */

    if (GetProtocolAddress
        (au1ThwAddr, &u4TprotocolAddr, pRarpRequest->u1HwAddrLen) == FAILURE)
    {
        /* we don't have entry for this hardware address */

        IP_TRC_ARG1 (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                     "RARP request dropped - No entry for this client %s \n",
                     au1ThwAddr);

        return (HARDWARE_ADDRESS_NOT_FOUND);
    }
    /* now we should send the reply packet */

    /* change the opcode from REQUEST to REPLY . */

    pRarpRequest->u2Opcode = RARP_REPLY_REVERSE;

    /* now copy the source hardware address in the request */
    MEMCPY (au1ThwAddr, pRarpRequest->au1ShwAddr, pRarpRequest->u1HwAddrLen);

    /* change the source hardware address in the request the hardware address */
    /* of the requested interface */

    if (CfaGetIfInfo (u2Port, &CfaIfInfo) != CFA_SUCCESS)
    {
        return FAILURE;
    }

    MEMCPY (pRarpRequest->au1ShwAddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);

    pRarpRequest->u4SprotocolAddr = tIpIfInfo.u4Addr;
    /* put the target protocol address */
    pRarpRequest->u4TprotocolAddr = u4TprotocolAddr;

    /* alloacte the buffer */
    pBuf =
        RARP_BUF_GET_MSG ((UINT4) (RARP_HDR_SIZE
                                   (pRarpRequest->u1HwAddrLen,
                                    pRarpRequest->u1ProtocolAddrLen) +
                                   RARP_MAX_ENCAPS_ADDRESS_LENGTH),
                          (UINT4) RARP_MAX_ENCAPS_ADDRESS_LENGTH);

    if (pBuf == NULL)
    {
        IP_TRC_ARG1 (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC | BUFFER_TRC,
                     RARP_NAME, "RARP request dropped - Buffer alloc. error \n",
                     au1ThwAddr);

        return (BUFFER_ALLOCATION_ERROR);
    }
    /* send the packet */
    RarpPutHeader (pRarpRequest, pBuf);
    if (CfaHandlePktFromIp (pBuf, tIpIfInfo.InterfaceId.u4IfIndex, 0,
                            au1ThwAddr, RARP_PROTOCOL, 0,
                            ARP_ENET_V2_ENCAP) != CFA_SUCCESS)
    {
        RARP_RELEASE_BUFFER (pBuf);
        return (RARP_PACKET_NOT_SENT);
    }

    IP_TRC_ARG2 (RARP_MOD_TRC, DATA_PATH_TRC, RARP_NAME,
                 "RARP reply sent successfully "
                 "to the client - %s on interface \n",
                 au1ThwAddr, tIpIfInfo.InterfaceId.u4IfIndex);

    return (SUCCESS);
}

/****************************************************************************/
/* Function Name        : GetProtocolAddress                                */
/* `Description          : Searches the global linked list maintained        */
/*                        by the server for the given hardware address      */
/*                        If found,then it will set the corresponding       */
/*                        protocol address configured for that H/W        */
/*                        address.                                          */
/*                                                                          */
/*     Input(s)      : au1ThwAddr - the hardware address to be           */
/*                        searched in the list.                             */
/*                        u1HwLen - Hardware address length of the          */
/*                        of the specified hardware address.                */
/*                                                                          */
/*     Output(s)    : pProtocolAddr - if an entry is found for the      */
/*                        given hardware address then the pointer to        */
/*                        the corresponding protocol address is stored      */
/*                                                                          */
/*     Return(s)        : SUCCESS - if the hardware address is found        */
/*                        in the list.                                      */
/*                        FAILURE - if the hardware address is not          */
/*                        found.                                            */
/****************************************************************************/
#ifdef __STDC__
INT1
GetProtocolAddress (UINT1 *pu1ThwAddr, UINT4 *pu4ProtocolAddr, UINT1 u1HwLen)
#else
INT1
GetProtocolAddress (pu1ThwAddr, pu4ProtocolAddr, u1HwLen)
     UINT1              *pu1ThwAddr;
     UINT4              *pu4ProtocolAddr;
     UINT1               u1HwLen;
#endif
{
    tServerAddrNode    *pAddrEntry;

    /* scan through the server address list */

    RARP_SLL_Scan (&ServerAddrList, pAddrEntry, tServerAddrNode *)
    {
        /* Check the hardware address length for this entry. */
        /* if not equal don't compare this */
        if (pAddrEntry->u1HwAddrLen == u1HwLen)
        {
            /* Check the hardware address */
            if (MEMCMP (pAddrEntry->au1HwAddr, pu1ThwAddr,
                        RARP_MAX_BYTES (u1HwLen, CFA_ENET_ADDR_LEN)) == 0)
            {
                /* this is the required address */
                *pu4ProtocolAddr = pAddrEntry->u4ProtocolAddr;
                return (SUCCESS);
            }
        }
    }
    return (FAILURE);
}
