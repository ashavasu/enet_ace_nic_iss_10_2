/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rarpcli.c,v 1.13 2013/07/04 13:12:04 siva Exp $
 *
 * Description: Contains functions for RARP Client .
 *
 *******************************************************************/
#include "rarpinc.h"

static t_IP_TIMER   RarpTimer;
static UINT2        gu2RarpIfIndex = 0;
UINT1               gu1RarpAddrConfFlag = FALSE;

extern tArpGblInfo  gArpGblInfo;

/****************************************************************************/
/* Function name      : RarpClientSendRequest                               */
/* Description        : This is the API provided for sending RARP Request   */
/*                      packets.This is called by an application whenever   */
/*                      it needs to find the IP Address of the interface.   */
/*                      This function will create the RARP request packet   */
/*                      for  the specified interface.                       */
/*                                                                          */
/*   Input(s)         : u2Port - the port for which the client has to send  */
/*                      the RARP request.                                   */
/*   Output(s)        : None.                                               */
/*   Return(s)        :    RARP_CLIENT_NOT_ENABLED                             */
/*                      RARP_CLIENT_ALREADY_ACTIVE                          */
/*                      BUFFER_ALLOCATION_ERROR                             */
/*                      RARP_PACKET_NOT_SENT                                */
/*                      SUCCESS                                             */
/****************************************************************************/
#ifdef __STDC__
INT1
RarpClientSendRequest (UINT2 u2Port)
#else
INT1
RarpClientSendRequest (u2Port)
     UINT2               u2Port;    /*the interface for which we have to send the request */
#endif
{
    tRarpPacket         RarpRequest;
    tRARP_BUF_CHAIN_HEADER *pBuf;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN] =
        { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

    /* clear the packet */
    MEMSET (&RarpRequest, 0, sizeof (RarpRequest));

    RarpRequest.u2HwAddrType = RARP_ENET_HW_TYPE;

    RarpRequest.u2ProtocolType = IP_PROTOCOL_TYPE;

    RarpRequest.u1HwAddrLen = RARP_MAX_HW_ADDR_LEN;

    RarpRequest.u1ProtocolAddrLen = IP_PROTO_ADDR_LEN;

    RarpRequest.u2Opcode = RARP_REQUEST_REVERSE;

    /* Copy the Hardware Address of the interface in which we the request  */
    /* will be sent to the Source Hardware Address field                   */

    /* it is assumed that RARP will be used only for the 
     *  default router interface i.e eth0.
     */

    /* Get the hardware address from CFA, since CFA only maintains 
     * the interfaces table.
     */
    /* Copy the hardware address of the requested port in to the target */
    /* hardware address field */

    /* Get the hardware address from CFA, since CFA only maintains 
     * the interfaces table.
     */

    if (CfaGetIfInfo (u2Port, &CfaIfInfo) == CFA_FAILURE)
    {
        return FAILURE;
    }

    MEMCPY (RarpRequest.au1ShwAddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    MEMCPY (RarpRequest.au1ThwAddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    /* the source and target protocol address in the request packet is   */
    /* left undefined. */

    pBuf =
        CRU_BUF_GET_MSG ((UINT4) (RARP_HDR_SIZE
                                  (RarpRequest.u1HwAddrLen,
                                   RarpRequest.u1ProtocolAddrLen) +
                                  RARP_MAX_ENCAPS_ADDRESS_LENGTH),
                         (UINT4) RARP_MAX_ENCAPS_ADDRESS_LENGTH);

    if (pBuf == NULL)
    {
        return (BUFFER_ALLOCATION_ERROR);
    }

    /* Put the local  structure in to the buffer */
    RarpPutHeader (&RarpRequest, pBuf);

    /* Using CFA's API to send out the packet. 
     * by passing RarpSendPktBelow since, we need not put LL header */

    /*  Send the packet in the default router interface eth 0 */

    if (CfaHandlePktFromIp
        (pBuf, u2Port, 0, au1HwAddr, RARP_PROTOCOL, 0,
         ARP_ENET_V2_ENCAP) != CFA_SUCCESS)
    {
        RARP_RELEASE_BUFFER (pBuf);
        return (RARP_PACKET_NOT_SENT);
    }

    /* Start timer here */
    RarpTimer.u1Id = RARP_CLIENT_TIMER_ID;

    if (gu1CurrentRetry == 0)
    {
        /* Sending the request for the first time */

        IP_TRC_ARG1 (RARP_MOD_TRC, DATA_PATH_TRC, RARP_NAME,
                     "RARP Request sent successfully on interface %d",
                     gu2RarpIfIndex);

        TmrStartTimer (ARP_TIMER_LIST_ID, &RarpTimer.Timer_node,
                       RarpConfigTable.u4TimeoutInt);
    }
    else
    {

        IP_TRC_ARG1 (RARP_MOD_TRC, DATA_PATH_TRC, RARP_NAME,
                     "RARP Request retransmitted successfully on interface %d",
                     gu2RarpIfIndex);

        IP_RESTART_TIMER (ARP_TIMER_LIST_ID, &RarpTimer.Timer_node,
                          RarpConfigTable.u4TimeoutInt);
    }

    gu1RarpAddrConfFlag = FALSE;
    gu2RarpIfIndex = u2Port;
    gu1RarpClientState = WAIT;

    /* the request was sent successfully */
    /* set the rarp client state to WAIT */

    return (SUCCESS);

}

/****************************************************************************/
/*  Function name    : RarpClientHandler                                    */
/*  Description      : This function will be invoked by the RarpInput       */
/*                     Function whenever it receives a RARP reply packet.   */
/*                     This function will validate the reply packet and     */
/*                     it will store the IP Address of the requested        */
/*                     interface in the IP Interface table.                 */
/*   Input(S)        : pRarpReply - contains the pointer to the Reply       */
/*                     packet                                                */
/*                     u2Port - the interface in which this packet is       */
/*                     received.                                            */
/*   Output(s)       : None.                                                */
/*   Return(s)       : RARP_CLIENT_NOT_ENABLED                              */
/*                     RARP_CLIENT_IS_IDLE                                  */
/*                     REPLY_FROM_INVALID_PORT                              */
/*                     INVALID_HARDWARE_ADDRESS_TYPE                        */
/*                     INVALID_PROTOCOL_ADDRESS_TYPE                        */
/*                     INVALID_HARDWARE_ADDRESS_LENGTH                      */
/*                     INVALID_PROTOCOL_ADDRESS_LENGTH                      */
/*                     HARDWARE_ADDRESS_MISMATCH                            */
/*                     SUCCESS                                              */
/****************************************************************************/
#ifdef __STDC__
INT1
RarpClientHandler (tRarpPacket * pRarpReply, UINT2 u2Port)
#else
INT1
RarpClientHandler (pRarpReply, u2Port)
     tRarpPacket        *pRarpRequest;
     UINT2               u2Port;
#endif
{
    tARP_DATA           tArpData;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1PhysAddr[RARP_MAX_HW_ADDR_LEN];

    UNUSED_PARAM (u2Port);
    MEMSET (&tArpData, 0, sizeof (tARP_DATA));
    if (gu1RarpClientState != WAIT)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP Reply dropped - RARP Client is IDLE \n");
        return (RARP_CLIENT_IS_IDLE);
    }

    if (pRarpReply->u2HwAddrType != RARP_ENET_HW_TYPE)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP Reply dropped - Invalid hardware address type \n");

        return (INVALID_HARDWARE_ADDRESS_TYPE);
    }
    /* check the protocol  address type */

    if (pRarpReply->u2ProtocolType != IP_PROTOCOL_TYPE)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP Reply dropped - Invalid protocol address type \n");

        return (INVALID_PROTOCOL_ADDRESS_TYPE);
    }

    /* check the hardware address length */

    if (pRarpReply->u1HwAddrLen != RARP_MAX_HW_ADDR_LEN)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP Reply dropped - Invalid hardware address length \n");

        return (INVALID_HARDWARE_ADDRESS_LENGTH);
    }

    /* check the protocol address length */

    if (pRarpReply->u1ProtocolAddrLen != IP_PROTO_ADDR_LEN)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP Reply dropped - Invalid protocol address length \n");

        return (INVALID_PROTOCOL_ADDRESS_LENGTH);
    }

    /* check the target hardware address in the reply . if it isnot equal to
     * the hardware address of the requested port then the request should be
     * discarded */
    /* copy the hardware address of the requested interface in to the local
     * variable */

    if (CfaGetIfInfo (gu2RarpIfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        return FAILURE;
    }

    MEMCPY (au1PhysAddr, CfaIfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
    /* compare this address with the target hardware address in the reply. */

    if (MEMCMP (au1PhysAddr, pRarpReply->au1ThwAddr, pRarpReply->u1HwAddrLen) !=
        SUCCESS)
    {
        IP_TRC (RARP_MOD_TRC, DATA_PATH_TRC | ALL_FAILURE_TRC, RARP_NAME,
                "RARP Reply dropped - Invalid hardware address \n");

        return (HARDWARE_ADDRESS_MISMATCH);
    }
    /* now the reply is meant for us */
    /* store the target protocol address in the reply in to the IP interface
     * table */

    /* check whether we have received a class D address  */

    if (IN_CLASSD (pRarpReply->u4TprotocolAddr))
    {
        return (RECEIVED_CLASSD_ADDRESS);
    }
    /* add this entry in to IPIF table and update the local route */

    RarpTimer.u1Id = RARP_CLIENT_TIMER_ID;

    TmrStopTimer (ARP_TIMER_LIST_ID, &(RarpTimer.Timer_node));

    gu1RarpAddrConfFlag = TRUE;

    IP_TRC_ARG1 (RARP_MOD_TRC, DATA_PATH_TRC, RARP_NAME,
                 "Valid RARP Reply received - "
                 "IP address of Def. interface is %x \n",
                 pRarpReply->u4TprotocolAddr);

    /* entry added in to the table */
    /* Change the client state to IDLE */
    gu1RarpClientState = RARP_IDLE;

    /* add the server's IP address & hardware address to the ARP Cache */
    /* This is to avoid unneccessary ARP Broadcast of the ARP Request */

    tArpData.i2Hardware = (INT2) pRarpReply->u1HwAddrLen;
    tArpData.u4IpAddr = pRarpReply->u4SprotocolAddr;
    tArpData.u2Port = u2Port;
    tArpData.i1Hwalen = (INT1) pRarpReply->u1HwAddrLen;
    MEMCPY (tArpData.i1Hw_addr,
            (INT1 *) pRarpReply->au1ShwAddr, tArpData.i1Hwalen);
    tArpData.u1EncapType = ARP_ENET_V2_ENCAP;
    tArpData.i1State = (INT1) ARP_DYNAMIC;
    tArpData.u1RowStatus = ACTIVE;

    if (arp_add (tArpData) == IP_FAILURE)
    {
        return FAILURE;
    }

    return (SUCCESS);
}

/****************************************************************************/
/*     Function Name    : RarpClientTimerExpiryHandler                      */
/*     Description        : This function will be called whenever the         */
/*                        RARP timer expires.                               */
/*     Input(s)         : NONE                                              */
/*     Output(s)        : NONE                                              */
/*     Return(s)        : NONE                                              */
/****************************************************************************/
#ifdef __STDC__
VOID
RarpClientTimerExpiryHandler ()
#else
VOID
RarpClientTimerExpiryHandler ()
#endif
{
    gu1CurrentRetry++;
    if (gu1CurrentRetry < RarpConfigTable.u1MaxRetries)
    {
        RarpClientSendRequest (gu2RarpIfIndex);
    }
    else
    {
        /* Unlink the timer here */
        RarpTimer.u1Id = RARP_CLIENT_TIMER_ID;

        TmrStopTimer (ARP_TIMER_LIST_ID, &(RarpTimer.Timer_node));
    }
}

/****************************************************************************/
/*     Function Name    : RarpEnableRarpClient                              */
/*     Description      : This function will be called to Enable RARP Client*/
/*     Input(s)         : u2IfIndex - CFA Interface Index                   */
/*     Output(s)        : NONE                                              */
/*     Return(s)        : NONE                                              */
/****************************************************************************/
INT4
RarpEnableRarpClient (UINT2 u2IfIndex)
{
    tArpQMsg            ArpQMsg;

    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    ArpQMsg.u4MsgType = RARP_CLIENT_ENABLE;
    ArpQMsg.u2Port = u2IfIndex;

    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        return (IP_FAILURE);
    }
    return (IP_SUCCESS);
}
