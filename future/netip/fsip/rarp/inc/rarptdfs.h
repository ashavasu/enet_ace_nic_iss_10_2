/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rarptdfs.h,v 1.3 2011/08/16 08:36:04 siva Exp $
 *
 * Description: Contains type definitions of both RARP Client
 *              and RARP Server.
 *
 *******************************************************************/
#ifndef _rarptdfs_h
#define _rarptdfs_h

#include "rarpdefn.h"
#include "utlhdr.h"

   /* Declaration of RARP Packet structure */

typedef struct
{
    UINT4  u4TprotocolAddr;                   /* Target Protocol Address */
    UINT4  u4SprotocolAddr;                   /* protocol address of the
                                               * sender ot the 
                                               */
    UINT2  u2HwAddrType;                      /* Hardware Address Space */
    UINT2  u2ProtocolType;                    /* Protocol address type */
    UINT2  u2Opcode;                          /* Opcode of the packet */
    UINT1  u1HwAddrLen;                       /* Hardware Address length */
    UINT1  u1ProtocolAddrLen;                 /* protocol address length */
    UINT1  au1ShwAddr[RARP_MAX_HW_ADDR_LEN];
    UINT1  au1ThwAddr[RARP_MAX_HW_ADDR_LEN];
} tRarpPacket;

/* Declaration of RARP config table */
typedef struct
{
    UINT1  u1ClientStatus;  /* Status of the RARP client by which it can be */
    UINT1  u1ServerStatus;  /* Status of the RARP server */
    UINT1  u1MaxRetries;    /* The maximum retransmissions of RARP Request */
    UINT1  u1MaxEntries;
    UINT4  u4TimeoutInt;    /* Timeout interval for retransmission of RARP */
} tRarpConfigTable;

/* Declaration of RARP Statistics table */
typedef struct
{
    UINT2  u2ReqsDiscarded;  /* Total number of RARP requests discarded by */
    UINT2  u2RepDiscarded;   /* Total number RARP replies discarded by this */
} tRarpStatisTable;

/* declaration of RARP server address mapping list */
typedef struct
{
    tRARP_SLL_NODE  NextServerAddrNode;               /* the address of the 
                                                       * next node 
                                                       */
    UINT1           au1HwAddr[RARP_MAX_HW_ADDR_LEN];  /* hardware address */
    UINT1           u1HwAddrLen;                      /* hardware addr length */
    UINT1           u1EntryStatus;
    UINT4           u4ProtocolAddr;                   /* protocol address */
} tServerAddrNode;

#define   RARP_STANDARD_HDR_SIZE    8
#define RARP_PACKET_LEN (RARP_STANDARD_HDR_SIZE +         \
                        (RARP_MAX_HW_ADDR_LEN * 2) +     \
                        (RARP_MAX_PROTO_ADDR_LEN * 2))
#define RARP_MAX_PROTO_ADDR_LEN IP_PROTO_ADDR_LEN

typedef struct _StandardRarpHdr
{
    UINT2  u2HwType;
    UINT2  u2ProtType;
    UINT1  u1HwaLen;
    UINT1  u1PaLen;
    UINT2  u2Opcode;
} tRarpHeader;

extern UINT1              gu1RarpClientState;
extern UINT1              gu1CurrentRetry;
extern UINT2              gu2RequestPort;
extern tRARP_SLL          ServerAddrList;
extern tRarpStatisTable   RarpStatisTable;
extern tRarpConfigTable   RarpConfigTable;
extern UINT1 gu1RarpAddrConfFlag;

extern tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring      PROTO((INT4));

extern VOID
free_octetstring          PROTO((tSNMP_OCTET_STRING_TYPE *));


#endif /*  _rarptdfs_h  */
