/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: rarpsz.h,v 1.1 2010/10/12 14:22:07 kumar Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/
enum {
    MAX_RARP_SERVER_ADDRESS_ENTRIES_SIZING_ID,
    RARP_MAX_SIZING_ID
};


#ifdef  _RARPSZ_C
tMemPoolId RARPMemPoolIds[ RARP_MAX_SIZING_ID];
INT4  RarpSizingMemCreateMemPools(VOID);
VOID  RarpSizingMemDeleteMemPools(VOID);
INT4  RarpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RARPSZ_C  */
extern tMemPoolId RARPMemPoolIds[ ];
extern INT4  RarpSizingMemCreateMemPools(VOID);
extern VOID  RarpSizingMemDeleteMemPools(VOID);
extern INT4  RarpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _RARPSZ_C  */


#ifdef  _RARPSZ_C
tFsModSizingParams FsRARPSizingParams [] = {
{ "tServerAddrNode", "MAX_RARP_SERVER_ADDRESS_ENTRIES", sizeof(tServerAddrNode),MAX_RARP_SERVER_ADDRESS_ENTRIES, MAX_RARP_SERVER_ADDRESS_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RARPSZ_C  */
extern tFsModSizingParams FsRARPSizingParams [];
#endif /*  _RARPSZ_C  */


