/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rarpdefn.h,v 1.6 2014/02/24 11:27:57 siva Exp $
 *
 * Description: Defines constants used in RARP modules.
 *
 *******************************************************************/
#ifndef _rarpdefn_h_
#define _rarpdefn_h_

/* Client State */
#define   RARP_IDLE    1
#define   WAIT         2

/*Constants used for ERROR Reporting by RARP Client Send request function */
#define   RARP_CLIENT_NOT_ENABLED       -2
#define   RARP_CLIENT_ALREADY_ACTIVE    -3
#define   REQUESTED_PORT_IS_INVALID     -4
#define   INTERFACE_TYPE_MISMATCH       -6

/*  Constants used by RARP Client handler function  */
#define   RARP_CLIENT_IS_IDLE          -7
#define   REPLY_FROM_INVALID_PORT      -8
#define   HARDWARE_ADDRESS_MISMATCH    -9

/*  Constants used by RARP Server Handler function. */
#define   RARP_SERVER_NOT_ENABLED       -10
#define   HARDWARE_ADDRESS_NOT_FOUND    -11

/*  Constants common to RARP Client & Server . */
#define   INVALID_HARDWARE_ADDRESS_TYPE      -12
#define   INVALID_PROTOCOL_ADDRESS_TYPE      -13
#define   INVALID_HARDWARE_ADDRESS_LENGTH    -14
#define   INVALID_PROTOCOL_ADDRESS_LENGTH    -15
#define   BUFFER_ALLOCATION_ERROR            -16
#define   RARP_PACKET_NOT_SENT               -17
#define   RECEIVED_CLASSD_ADDRESS            -18
#define   RARP_REQUEST_NOT_REQUIRED          -19

/* Constants used bt RARP Client Timer Expiry Handler function . */
#define   RETRIES_EXCEEDED                  -18
#define   NO_RARP_REQUEST                   -19
#define   RARP_REQUEST_NOT_RETRANSMITTED    -20

/* protocol address type &  length  of IP */
#define   IP_PROTOCOL_TYPE          IP_ENET_TYPE            
#define   RARP_MAX_LOGICAL_IFACES   IPIF_MAX_LOGICAL_IFACES 

#define   IP_PROTO_ADDR_LEN    4

/* RARP opcodes */
#define   RARP_REQUEST_REVERSE    3
#define   RARP_REPLY_REVERSE      4

/* constants for rarp client status */
#define   RARP_CLIENT_STATUS_ENABLE    1
#define   RARP_CLIENT_STATUS_DISABLE    2
#define   RARP_DEF_CLIENT_STATUS    1

/* constants for the timeout interval of the RARP request */
#define   RARP_MIN_CLIENT_TIMEOUT      30 
#define   RARP_MAX_CLIENT_TIMEOUT      3000
#define   RARP_DEF_CLIENT_TIMEOUT      100   

/* constants for the maximum retransmissions of RARP Request */
#define   RARP_MIN_CLIENT_NUMBER_OF_RETRANS     2
#define   RARP_MAX_CLIENT_NUMBER_OF_RETRANS    10 
#define   RARP_DEF_CLIENT_NUMBER_OF_RETRANS     4

/* constants for rarp server status */
#define   RARP_SERVER_STATUS_ENABLE           1
#define   RARP_SERVER_STATUS_DISABLE          2
#define   RARP_DEF_SERVER_STATUS              2

#define   RARP_MIN_SERVER_TABLE_ENTRIES       0
#define   RARP_DEF_SERVER_TABLE_ENTRIES      20
#define   RARP_MAX_SERVER_TABLE_ENTRIES      25

#define   RARP_MAX_SERVER_ADDRESS_ENTRIES    20

/* constant for rarp client packet transmit port */
/* the port of the client in which the server is connected */
#define   RARP_DEF_CLIENT_PACKET_TRANSMIT_PORT     0
#define   RARP_MAX_ENET_ADDR_LEN                   6
#define   RARP_ENET_HDR_LEN                       14
#define   RARP_MAX_HW_ADDR_LEN                    RARP_MAX_ENET_ADDR_LEN
#define   RARP_HDR_SIZE(u1HwLen,u1ProtocolLen) (8+(2*u1HwLen)+(2*u1ProtocolLen))
#define   ENET_PROTOCOL_FIELD_OFFSET    12
#define   RARP_ENET_HW_TYPE              1
#define   RARP_DEF_ROUTER_INTERFACE      1
#define   RARP_MAX_BYTES(u4Bytes, max)  \
          (((u4Bytes) <= max) ? (u4Bytes) : (max))

/* time interval between Request Retransmissions*/
#define  RARP_RETRANSMISSION_INTERVAL        100

#define RARP_ENTRY_POOL_ID      (RARPMemPoolIds[MAX_RARP_SERVER_ADDRESS_ENTRIES - 1])
#endif /* _rarpdefn_h  */
