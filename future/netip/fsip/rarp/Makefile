#####################################################################
#                                                                  #
# $RCSfile: Makefile,v $                                          #
#                                                                  #
# $Date: 2010/10/13 04:23:12 $                                     #
#                                                                  #
# $Revision: 1.6 $                                                 #
#                                                                  #
#####################################################################
#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved
    ####
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile                      |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  IP  Protocol Router            |##
##|                                                               |##
##|    MODULE NAME             ::  RARP                           |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  UNIX                           |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  10-JAN-2000                    |##
##|                                                               |##
##|              DESCRIPTION   ::  Make file for Future IP        |##
##|                                Router RARP Module             |##
##|                                This file will be invoked by   |##
##|                                the top level makefile.        |##
##|###############################################################|##
#### CHANGE RECORD ::                                            ####
##|#######|###############|#######################################|##
##|VERSION|  DATE/AUTHOR  |  DESCRIPTION OF CHANGE                |##
##|#######|###############|#######################################|##
##|  1.0  |  10-JAN-2000  |   Create Original                     |##
##|       |Purushothaman.N|                                       |##
##|#######|###############|#######################################|##

include ../../../LR/make.h
include ../../../LR/make.rule
include ../../make.h

.PHONY :  START ECHO DONE clean

LOCAL_INCLUDES = -I${IP_RARPINCD}

ARP_INCLUDES   = -I${BASE_DIR}/arp/inc

INCLUDES       = ${LOCAL_INCLUDES} ${GLOBAL_INCLUDES} ${ARP_INCLUDES}

LOCAL_OPNS     = ${DEFINE_OPNS} ${SWITCHES} 

TOTAL_OPNS     = ${LOCAL_OPNS} ${GLOBAL_OPNS}

ISS_C_FLAGS         = ${CC_FLAGS} ${CC_DEBUG_OPTIONS} ${TOTAL_OPNS} ${INCLUDES}

RARP_DPNDS = ${COMMON_DEPENDENCIES} \
            ${IP_BASE_DIR}/make.h \
            ${IP_BASE_DIR}/Makefile \
            ${IP_INCD}/tmoport.h \
            ${IP_INCD}/cruport.h \
            ${IP_INCD}/other.h \
	         ${IP_IPIFINCD}/ipifdfs.h \
            ${IP_IPINCD}/iptmrdfs.h \
            ${COMN_INCL_DIR}/lr.h \
            ${IP_RARPINCD}/rarptdfs.h \
            ${IP_RARPINCD}/rarpdefn.h  \
	    ${IP_RARPINCD}/rarpsz.h\
	         ${IP_RARPD}/Makefile


#Object Module List 

RARP_OBJS  = ${IP_RARPOBJD}/rarpmain.o \
            ${IP_RARPOBJD}/rarpcli.o \
            ${IP_RARPOBJD}/rarpserv.o \
            ${IP_RARPOBJD}/rarpsnif.o\
            ${IP_RARPOBJD}/rarpsz.o

#Final Object

IP_RARP_OBJ   = ${IP_RARPOBJD}/rarp.o

START : ECHO ${IP_RARP_OBJ} DONE

ECHO  :
	@echo Making RARP Submodule...

DONE  :
	@echo RARP Submodule done.

${IP_RARP_OBJ}   : obj ${RARP_OBJS}
	${LD} ${LD_FLAGS} -o ${IP_RARP_OBJ} ${RARP_OBJS} ${CC_COMMON_OPTIONS}

obj:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) ${IP_RARPOBJD}
endif

#Object Module Dependency

${IP_RARPOBJD}/rarpmain.o: ${IP_RARPSRCD}/rarpmain.c \
    ${RARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${IP_RARPSRCD}/rarpmain.c -o $@

${IP_RARPOBJD}/rarpcli.o: ${IP_RARPSRCD}/rarpcli.c \
    ${RARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${IP_RARPSRCD}/rarpcli.c -o $@

${IP_RARPOBJD}/rarpserv.o: ${IP_RARPSRCD}/rarpserv.c \
    ${RARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${IP_RARPSRCD}/rarpserv.c -o $@

${IP_RARPOBJD}/rarpsnif.o: ${IP_RARPSRCD}/rarpsnif.c \
    ${RARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${IP_RARPSRCD}/rarpsnif.c -o $@

${IP_RARPOBJD}/rarpsz.o: ${IP_RARPSRCD}/rarpsz.c \
    ${RARP_DPNDS}
	${CC} ${ISS_C_FLAGS} ${IP_RARPSRCD}/rarpsz.c -o $@

clean :
	@echo "Cleaning the RARP object files"
	${RM} ${RM_FLAGS} ${IP_RARP_OBJ} ${RARP_OBJS}
