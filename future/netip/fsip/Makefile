#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved                          ####
#####################################################################
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  IP  Protocol Router            |##
##|                                                               |##
##|    MODULE NAME             ::  TOP most level Makefile        |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  UNIX                           |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  11-1-2000                      |##
##|                                                               |##
##|            DESCRIPTION     ::                                 |##
##|                                MAKEFILE for Future            |##
##|                                IP Router Final Object         |##
##|                                                               |##
##|###############################################################|##

.PHONY  : clean
include ../../LR/make.h
include ../../LR/make.rule
include ../make.h

#Object Module List 

IP_OBJS  =  ${IP_IPOBJD}/ipcore.o \
            ${IP_IPIFOBJD}/ipif.o \
            ${IP_ICMPOBJD}/icmp.o \
            ${IP_RARPOBJD}/rarp.o \
            ${IP_UDPOBJD}/udp.o \
            ${IP_TRCRTOBJD}/udptrcrt.o \
            ${IP_IRDPOBJD}/irdp.o
            
#Dummy compilation switch
ifeq (${V4OV6}, YES)
IP_OBJS += ${IP_V4OV6OBJD}/V4OV6.o
endif

IP6_LIB = ${IP6LIBD}/libip6util.a

#Final Object

#IP_FINAL_OBJ = ${IP_OBJD}/FSIP.o 
IP_FINAL_OBJ = ${FSIP_OBJD}/FSIP.o 

IP_MAKE_HEAD    = ${IP_BASE_DIR}/make.h
IP_MAKE_FILE    = ${IP_BASE_DIR}/Makefile

DEPENDENCIES_FOR_IP = ${IP_MAKE_HEAD} ${IP_MAKE_FILE} ${IP_OBJD} ${IP_OBJS} ${COMMON_DEPENDENCIES}

${IP_FINAL_OBJ}   : ${DEPENDENCIES_FOR_IP}
	${LD} ${LD_FLAGS} -o ${IP_FINAL_OBJ} ${IP_OBJS} ${CC_COMMON_OPTIONS}

${IP_OBJD} :
	$(MKDIR)  $(MKDIR_FLAGS) ${IP_OBJD}

${IP_IPOBJD}/ipcore.o : FORCE
	$(MKDIR) $(MKDIR_FLAGS) ${IP_IPOBJD}
	${MAKE} -C ${IP_IPD} -f Makefile

${IP_IPIFOBJD}/ipif.o : FORCE
	$(MKDIR) $(MKDIR_FLAGS) ${IP_IPIFOBJD}
	${MAKE} -C ${IP_IPIFD} -f Makefile

${IP_ICMPOBJD}/icmp.o : FORCE
	$(MKDIR)  $(MKDIR_FLAGS) ${IP_ICMPOBJD}
	${MAKE} -C ${IP_ICMPD} -f Makefile

${IP_V4OV6OBJD}/V4OV6.o : FORCE
	$(MKDIR) $(MKDIR_FLAGS) ${IP_V4OV6OBJD}
	${MAKE} -C ${IP_V4OV6D} -f Makefile

${IP_UDPOBJD}/udp.o : FORCE
	$(MKDIR)  $(MKDIR_FLAGS) ${IP_UDPOBJD}
	${MAKE} -C ${IP_UDPD} -f Makefile

${IP_TRCRTOBJD}/udptrcrt.o : FORCE
	$(MKDIR)  $(MKDIR_FLAGS) ${IP_TRCRTOBJD}
	${MAKE} -C ${IP_TRCRTD} -f Makefile

${IP_IRDPOBJD}/irdp.o : FORCE
	$(MKDIR)  $(MKDIR_FLAGS) ${IP_IRDPOBJD}
	${MAKE} -C ${IP_IRDPD} -f Makefile

${IP_RARPOBJD}/rarp.o : FORCE
	$(MKDIR)  $(MKDIR_FLAGS) ${IP_RARPOBJD}
	${MAKE} -C ${IP_RARPD} -f Makefile

FORCE :
FORCE :

clean : 
	${MAKE} -C ${IP_IPD}  -f Makefile clean
	${MAKE} -C ${IP_IPIFD} -f Makefile clean
	${MAKE} -C ${IP_ICMPD} -f Makefile clean
ifeq (${V4OV6}, YES)
	${MAKE} -C ${IP_V4OV6D} -f Makefile clean
endif
	${MAKE} -C ${IP_PINGD} -f Makefile clean
	${MAKE} -C ${IP_UDPD} -f Makefile clean
	${MAKE} -C ${IP_TRCRTD} -f Makefile clean
	${MAKE} -C ${IP_IRDPD} -f Makefile clean
	${MAKE} -C ${IP_RARPD} -f Makefile clean 
	${RM} ${RM_FLAGS} ${IP_FINAL_OBJ}
