/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: icmpmain.c,v 1.22 2016/03/22 12:32:44 siva Exp $
 *
 * Description:This file contains the implementation of     
 *             Internet Control Message Protocol. This includes
 *             the support routine for other layers to  
 *             send ICMP messages, processing of ICMP packets, 
 *             and Initialising ICMP configuration params.   
 *
 *******************************************************************/
#include "icmpinc.h"
#include "udpproto.h"
/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/

static INT4 icmp_put_hdr PROTO ((t_ICMP * pIcmp,
                                 tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len));
static INT4 icmp_extract_hdr PROTO ((t_ICMP * pIcmp,
                                     tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len));
static INT1 icmp_prcs_ip_options PROTO ((t_IP * pIp, UINT1 *au1_DstOption,
                                         UINT1 u1_SrcRt_Only));
static UINT2 icmp_reply_update_buffer PROTO ((tIP_BUF_CHAIN_HEADER * pBuf,
                                              t_IP * pIpHdr));

#ifdef IPSEC_STUB_WANTED
static VOID IpsecFailureHandler PROTO ((tIP_BUF_CHAIN_HEADER * pBuf,
                                        t_ICMP * pIcmp));

static VOID IpsecSendErrorMessage PROTO ((UINT4 u4Dest,
                                          UINT1 u1Tos,
                                          tIP_BUF_CHAIN_HEADER * pBuf,
                                          UINT2 u2Len, UINT1 u1Optlen));
#endif

#ifdef DIQUERY_STUB_WANTED
static VOID DnsDomainNameReplyHandlerInCxt PROTO ((tIpCxt * pIpCxt,
                                                   tIP_BUF_CHAIN_HEADER *
                                                   pBuf));
#endif
/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/

t_ICMP_CFG          Icmp_cfg;    /* ICMP Configuration Record */
t_ICMP_STATS        Icmp_stats;    /* ICMP Statistics Record */
t_ICMP_ERR          Icmp_err;    /* ICMP Packet Error Statistics Record */

UINT1               gau1DomainName[ICMP_MAX_DOMAIN_NAME_SIZE];
INT4                gi4TimeToLive;
/**************************************************************************/

                 /**** $$TRACE_MODULE_NAME     = IP_FWD   ****/
                 /**** $$TRACE_SUB_MODULE_NAME = ICMP     ****/

/**** $$TRACE_PROCEDURE_NAME  = fs_icmp_input  ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN        ****/
/*-------------------------------------------------------------------+
 * Function           : fs_icmp_input
 *
 * Input(s)           : pBuf, u2Len, u2Port, InterfaceId, u1Flag
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :
 *
 * Validates the packet and then processes ICMP messages.
 * If message type is used by high level protocol then passes it up.
 *
 * Called from IpDeliverHigherLayerProtocol() whenever there is a packet 
 * with protocol field indicating ICMP.
 * No action is taken for the message types which are not
 * supported (only statistics is maintained).
+-------------------------------------------------------------------*/
#ifdef __STDC__

VOID
fs_icmp_input (tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, UINT4 u4Port,
               tIP_INTERFACE InterfaceId, UINT1 u1Flag)
#else
VOID
fs_icmp_input (pBuf, u2Len, u4Port, InterfaceId, u1Flag)
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Received data buffer.. contains IP header also */
     UINT2               u2Len;    /* Length of IP data .. excluding IP header and options */
     UINT4               u4Port;    /* Interface index   .. IP Handle                       */
     tIP_INTERFACE       InterfaceId;    /* Actual Iface structure                     */
     UINT1               u1Flag;    /* mode of receive :  broadcast, multicast or unicast   */
#endif
{
    t_ICMP              Icmp;
    UINT1               u1Ip_tos = 0;
    UINT4               u4Ip_src = 0, u4Ip_dest = 0;
    UINT1               u1_df_set = 0;
    INT2                i2_Optlen = 0;
    t_IP                OrigIphdr;
    INT4                i4RetVal = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4BcastAddr = 0;
    UINT4               u4CxtId = 0;
    UINT4               u4RetVal = 0;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4Address = 0;
    pIpCxt = UtilIpGetCxtFromIpPortNum (u4Port);
    if (NULL == pIpCxt)
    {
        IP_BUF_RELEASE_MSG (pBuf, FALSE);
        return;
    }

    u4CxtId = pIpCxt->u4ContextId;
    /* Makefile Changes - unused parameter */
    InterfaceId.u4IfIndex = 0;

    if (IpExtractHdr (&OrigIphdr, pBuf) == IP_FAILURE)
    {
        IP_BUF_RELEASE_MSG (pBuf, FALSE);
        return;
    }

    u1Ip_tos = OrigIphdr.u1Tos;
    u4Ip_src = OrigIphdr.u4Src;
    u4Ip_dest = OrigIphdr.u4Dest;
    u1_df_set = (IP_DF_SET (OrigIphdr.u2Fl_offs) == 0) ? FALSE : TRUE;

    ICMP_MOVE_TO_ICMP_START (pBuf);

    if (icmp_extract_hdr (&Icmp, pBuf, u2Len) == IP_FAILURE)
    {
        IP_BUF_RELEASE_MSG (pBuf, FALSE);
        return;
    }
    if ((u1Flag != IP_FOR_THIS_NODE) && (u1Flag != IP_ADDR_LOOP_BACK))
    {
        if ((Icmp.i1Type != ICMP_ROUTER_ADVT) &&    /* These are either broadcast */
            (Icmp.i1Type != ICMP_ROUTER_SOLCT) &&    /* or mulicasted */
            (Icmp.i1Type != ICMP_MASK_RQST))
        {
            /* Broadcast and multicast packets are not accepted */
            pIpCxt->Icmp_err.u4Bcast++;
            IP_BUF_RELEASE_MSG (pBuf, FALSE);
            return;
        }
    }

    if ((Icmp.i1Type >= 0) && (Icmp.i1Type < ICMP_TYPES))
    {
        pIpCxt->Icmp_stats.u4In[(UINT2) Icmp.i1Type]++;
    }
    else
    {
        pIpCxt->Icmp_err.u4Type++;
        IP_BUF_RELEASE_MSG (pBuf, FALSE);
        return;
    }

    i4RetVal = SUCCESS;

    /* Take action depending on the message type */

    switch (Icmp.i1Type)
    {
        case ICMP_REDIRECT:
            /* This can be ignored */
            IP_BUF_RELEASE_MSG (pBuf, FALSE);
            break;

        case ICMP_TIMESTAMP:

            /* send back our time */
            if (pIpCxt->Icmp_cfg.i1Time_reply == ICMP_TIME_REPLY_ENABLE)
            {
                UINT4               u4_RcvdTime = 0;
                OsixGetSysTime (&u4_RcvdTime);
                u4_RcvdTime =
                    ((u4_RcvdTime - ICMP_DELAY_FACTOR) | IP_TIMESTAMP_VAL);

                i2_Optlen = (INT2) icmp_reply_update_buffer (pBuf, &OrigIphdr);
                if (i2_Optlen < 0)
                {IP_BUF_RELEASE_MSG (pBuf, FALSE);
                    return;
                }
                /* icmp_reply_update_buffer may have changed u4Src */
                u4Ip_src = OrigIphdr.u4Src;

                Icmp.i1Type = ICMP_TIME_REPLY;
                pIpCxt->Icmp_stats.u4Out[ICMP_TIME_REPLY]++;

                /* Received timestamp */

                u4_RcvdTime = IP_HTONL (u4_RcvdTime);
                IP_COPY_TO_BUF (pBuf, &u4_RcvdTime,
                                (ICMP_TIMESTAMP_OFF + IP_FOUR), sizeof (UINT4));

                /* Transmit timestamp */
                {
                    UINT4               u4TxTime = 0;
                    OsixGetSysTime (&u4TxTime);
                    u4TxTime =
                        ((u4TxTime - ICMP_DELAY_FACTOR) | IP_TIMESTAMP_VAL);
                    u4TxTime = IP_HTONL (u4TxTime);
                    IP_COPY_TO_BUF (pBuf, &u4TxTime, ICMP_TIMESTAMP_OFF +
                                    BYTE_LENGTH, sizeof (UINT4));
                }

                IP_CXT_TRC_ARG1 (u4CxtId, ICMP_MOD_TRC, DATA_PATH_TRC, "ICMP",
                                 "sending timestamp reply to %x.\n", u4Ip_src);

                icmp_put_hdr (&Icmp, pBuf, u2Len);
                ip_send_InCxt (pIpCxt, u4Ip_dest, u4Ip_src, ICMP_PTCL, u1Ip_tos,
                               0, pBuf, u2Len, 0, u1_df_set, (UINT2) i2_Optlen,
                               IPIF_INVALID_INDEX);
            }
            else
            {
                IP_BUF_RELEASE_MSG (pBuf, FALSE);
            }
            break;

        case ICMP_TIME_REPLY:
            /* Can be ignored */
            IP_BUF_RELEASE_MSG (pBuf, FALSE);
            break;

        case ICMP_ECHO:

            if (pIpCxt->Icmp_cfg.i1Echo_reply == ICMP_ECHO_REPLY_ENABLE)
            {
                /* Change the request field to reply and send it back */
                i2_Optlen = (INT2) icmp_reply_update_buffer (pBuf, &OrigIphdr);
                if (i2_Optlen < 0)
                {

	            IP_BUF_RELEASE_MSG (pBuf, FALSE);
                    return;
                }

                /* icmp_reply_update_buffer may have changed u4Src */
                u4Ip_src = OrigIphdr.u4Src;

                Icmp.i1Type = ICMP_ECHO_REPLY;
                pIpCxt->Icmp_stats.u4Out[ICMP_ECHO_REPLY]++;
                icmp_put_hdr (&Icmp, pBuf, u2Len);

                IP_CXT_TRC_ARG1 (u4CxtId, ICMP_MOD_TRC, DATA_PATH_TRC, "ICMP",
                                 "sending echo reply to %x.\n", u4Ip_src);

                /* This is the traffic destined to this node 
                 * so it can fragmented and sent back irrespective 
                 * of the status how it is received.*/
                ip_send_InCxt (pIpCxt, u4Ip_dest, u4Ip_src, ICMP_PTCL, u1Ip_tos,
                               0, pBuf, u2Len, 0, FALSE, (UINT2) i2_Optlen,
                               IPIF_INVALID_INDEX);
            }
            else
            {
                IP_BUF_RELEASE_MSG (pBuf, FALSE);
            }
            break;

        case ICMP_MASK_RQST:

            if (pIpCxt->Icmp_cfg.i1Nmask_reply == ICMP_NMASK_REPLY_ENABLE)
            {
                UINT4               u4NetMask = 0;
                /*
                 * Mask requests are usually broadcasts and have significance
                 * only in directly connected networks. Therefore IP options
                 * such as Source Route, Time stamp, etc. need not be 
                 * considered. */

                Icmp.i1Type = ICMP_MASK_REPLY;

                /* Get the netmask associated with the IP address */
                if (u4Ip_dest == IP_GEN_BCAST_ADDR)
                {
                    u4RetVal =
                        IpGetSrcAddressOnInterface ((UINT2) u4Port, u4Ip_src,
                                                    &u4Address);
                    u4Ip_dest = u4Address;

                }
                if (IpIfGetAddressInfoInCxt (pIpCxt->u4ContextId, u4Ip_dest,
                                             &u4NetMask, &u4BcastAddr,
                                             &u4IfIndex) == IP_FAILURE)
                {
                    IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                                     "IpIfGetAddressInfoInCxt failed for %x\n",
                                     u4Ip_dest);
                }

                IP_COPY_TO_BUF (pBuf, &u4NetMask, ICMP_SUBNET_MASK_OFF,
                                sizeof (UINT4));

                pIpCxt->Icmp_stats.u4Out[ICMP_MASK_REPLY]++;

                icmp_put_hdr (&Icmp, pBuf, u2Len);

                IP_CXT_TRC_ARG1 (u4CxtId, ICMP_MOD_TRC, DATA_PATH_TRC, "ICMP",
                                 "sending mask reply to %x.\n", u4Ip_src);

                ip_send_InCxt (pIpCxt, u4Ip_dest, u4Ip_src, ICMP_PTCL, u1Ip_tos,
                               0, pBuf, u2Len, 0, u1_df_set, 0,
                               IPIF_INVALID_INDEX);
            }
            else
            {
                IP_BUF_RELEASE_MSG (pBuf, FALSE);
            }
            break;

        case ICMP_MASK_REPLY:

            /* Receiver will have to free the buffer */
            iptask_icmp_mask_reply_handler_InCxt (pIpCxt, pBuf, u4Ip_src,
                                                  u4Ip_dest,
                                                  Icmp.args.Identification.i2Id,
                                                  Icmp.args.Identification.
                                                  i2Seq);
            break;

        case ICMP_DOMAIN_NAME_REQUEST:

            if (Icmp.i1Code != 0)
            {
                pIpCxt->Icmp_err.u4Type++;
                IP_BUF_RELEASE_MSG (pBuf, FALSE);
                return;
            }

            if (pIpCxt->Icmp_cfg.i1DirectQueryStatus == ICMP_DIRECTQUERY_ENABLE)
            {
                pIpCxt->Icmp_stats.u4In[(UINT2) Icmp.i1Type]++;
                IcmpGenerateDomainNameReplyInCxt (pIpCxt, u4Ip_dest, u4Ip_src,
                                                  &Icmp);
            }
            IP_BUF_RELEASE_MSG (pBuf, FALSE);
            break;

#ifdef DIQUERY_STUB_WANTED        /*  for OSE porting  */
        case ICMP_DOMAIN_NAME_REPLY:
            if (Icmp.i1Code != 0)
            {
                pIpCxt->Icmp_err.u4Type++;
                IP_BUF_RELEASE_MSG (pBuf, FALSE);
                return;
            }
            if (pIpCxt->Icmp_cfg.i1DirectQueryStatus == ICMP_DIRECTQUERY_ENABLE)
            {
                pIpCxt->Icmp_stats.u4In[(UINT2) Icmp.i1Type]++;
                DnsDomainNameReplyHandlerInCxt (pIpCxt, pBuf);
            }
            IP_BUF_RELEASE_MSG (pBuf, FALSE);
            break;
#endif /* DIQUERY_STUB_WANTED */

#ifdef IPSEC_STUB_WANTED        /* for OSE porting */
        case ICMP_SECURITY_FAILURE:
            Icmp.u4ContextId = pIpCxt->u4ContextId;
            if (pIpCxt->Icmp_cfg.i1RecvSecurityFailure ==
                ICMP_RECV_SEC_FAILURE_ENABLE)
            {
                IP_CXT_TRC_ARG2 (u4CxtId, ICMP_MOD_TRC, DATA_PATH_TRC, "ICMP",
                                 "rcvd security failure msg from %x, code = %d.\n",
                                 u4Ip_src, Icmp.i1Code);

                IpsecFailureHandler (pBuf, &Icmp);
            }
            IP_BUF_RELEASE_MSG (pBuf, FALSE);
            break;
#endif /* IPSEC_STUB_WANTED  */

        case ICMP_ROUTER_ADVT:
        {

            /* 
             * An Advertisement can be declared invalid if even one of the 
             * following case is not satisfied :
             *   
             * + ICMP Code should be zero
             * + ICMP Num Addrs is greater than or equal to 1.
             * + ICMP Addr Entry Size is greater than or equal to 2.
             * + ICMP length is greater than or equal to
             *        8 + (Num Addrs * Addr Entry Size * 4) octets.
             */

            if ((Icmp.i1Code != 0) ||
                (Icmp.args.Irdp.u1NumAddr < 1) ||
                (Icmp.args.Irdp.u1AddrEntrySize < IRDP_ADDR_ENTRY_SIZE) ||
                (u2Len < (ICMP_HDR_LEN +
                          (Icmp.args.Irdp.u1NumAddr *
                           Icmp.args.Irdp.u1AddrEntrySize * IP_FOUR))))
            {
                pIpCxt->Icmp_err.u4Type++;
                i4RetVal = FAILURE;
            }
            else if (IP_CFG_GET_FORWARDING (pIpCxt) != IP_FORW_ENABLE)
            {
                IP_CXT_TRC_ARG1 (u4CxtId, ICMP_MOD_TRC, DATA_PATH_TRC,
                                 ICMP_NAME,
                                 " Received IRDP advertisement from %x.\n",
                                 u4Ip_src);

                i4RetVal =
                    IrdpAdvertisementHandler (pBuf, &Icmp, (UINT2) u4Port);
            }

            if (i4RetVal != EPKTQUED)
            {
                /* Processing is Over so releasing the buffer */
                IP_BUF_RELEASE_MSG (pBuf, FALSE);
            }
            break;
        }

        case ICMP_ROUTER_SOLCT:
        {
            /* We Dont require the packet any more */
            IP_BUF_RELEASE_MSG (pBuf, FALSE);

            if ((Icmp.i1Code != 0) ||    /* Code Should be zero */
                (u2Len < ICMP_HDR_LEN))    /* ICMP length should be atleast 8 */
            {
                pIpCxt->Icmp_err.u4Type++;
                i4RetVal = FAILURE;
            }
            else if (IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_ENABLE)
            {
                IP_CXT_TRC_ARG1 (u4CxtId, ICMP_MOD_TRC, DATA_PATH_TRC,
                                 ICMP_NAME,
                                 "Received IRDP solicitation from %x.\n",
                                 u4Ip_src);

                i4RetVal = IrdpSolicitationHandler ((UINT2) u4Port,
                                                    u4Ip_src, u4Ip_dest);
                if (i4RetVal == ENOTNEIGHBOR)
                {
                    pIpCxt->Icmp_err.u4Type++;
                    i4RetVal = FAILURE;
                }
            }

            break;
        }

            /* Added for PMTU_DISCOVERY */
        case ICMP_DEST_UNREACH:
        {
            t_IP_HEADER         DropdIpHdr;
            UINT4               u4DestAddr = 0;
            UINT1               u1IpTos = 0;

            if ((pIpCxt->u2PmtuStatusFlag == PMTU_ENABLED) &&
                (Icmp.i1Code == ICMP_FRAG_NEEDED))
            {

                UINT2               u2NewMtu = 0, u2OldMtu = 0;
                tPmtuInfo          *pEntry = NULL;

                u2NewMtu = ICMP_NH_MTU (Icmp);
                /* copy the IP header of the dropped packet */
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &DropdIpHdr,
                                           ICMP_HDR_LEN, IP_HDR_LEN);

                u2OldMtu = IP_NTOHS (DropdIpHdr.u2Totlen);
                u4DestAddr = IP_NTOHL (DropdIpHdr.u4Dest);
                u1IpTos = DropdIpHdr.u1Tos;

                IP_CXT_TRC_ARG3 (u4CxtId, ICMP_MOD_TRC, DATA_PATH_TRC, "ICMP",
                                 "rcvd dest %x unreachable from %x, code = %d.\n",
                                 u4DestAddr, u4Ip_src, Icmp.i1Code);

                if ((u2NewMtu < PMTU_MIN_MTU) || (u2NewMtu >= u2OldMtu))
                {
                    u2OldMtu = (UINT2) (u2OldMtu - IP_MAX_HDR_LEN);
                    u2NewMtu = PmtuEstimateMtu (u2OldMtu);
                }

                pEntry = PmtuLookupEntryInCxt (pIpCxt, u4DestAddr, u1IpTos);
                if (pEntry == NULL)
                {
                    pEntry =
                        (tPmtuInfo *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.
                                                      PmtuPoolId);
                    if (NULL != pEntry)
                    {
                        pEntry->u4PmtuDest = u4DestAddr;
                        pEntry->u1PmtuTos = u1IpTos;
                        pEntry->u1PmtuRowStatus = PMTU_ACTIVE;
                        pEntry->u1PmtuDiscFlag = IPPMTUDISC_DO;
                        pEntry->u4PMtu = u2NewMtu;
                        pEntry->pIpCxt = pIpCxt;

                        OsixGetSysTime (&pEntry->PmtuTstamp);
                        u4RetVal =
                            RBTreeAdd (gIpGlobalInfo.PmtuTblRoot, pEntry);
                        /*
                         * Notify the change in PMTU to applications.
                         * For new entries alone it will be done here.
                         */

                        /* 
                         * Call the function in TCP to handle the change in MTU.
                         * Currently this function is not there in TCP.
                         */

#ifdef SLI_WANTED
                        SliHandleChangeInPmtuInCxt (pIpCxt->u4ContextId,
                                                    u4DestAddr, u1IpTos,
                                                    (UINT2) (u2NewMtu -
                                                             IP_HDR_LEN));
#endif
                    }
                    else
                    {
                        IP_CXT_TRC_ARG2 (u4CxtId, ICMP_MOD_TRC,
                                         ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                                         CONTROL_PLANE_TRC, ICMP_NAME,
                                         "Table Full. "
                                         "Dropping Entry for Dest %u, Tos %d",
                                         u4DestAddr, u1IpTos);
                    }
                }

                if (pEntry != NULL)
                {
                    /* 
                     * Only if there is a change in PMTU we have
                     * to nofity the applications.
                     */
                    if (u2NewMtu < pEntry->u4PMtu)
                    {

                        pEntry->u4PMtu = u2NewMtu;

                        /* 
                         * Call the function in TCP to handle the change in MTU.
                         * Currently this function is not there in TCP.
                         */

#ifdef SLI_WANTED
                        SliHandleChangeInPmtuInCxt (pIpCxt->u4ContextId,
                                                    u4DestAddr, u1IpTos,
                                                    (UINT2) (u2NewMtu -
                                                             IP_HDR_LEN));
#endif
                    }
                    pEntry->u1PmtuAge = (UINT1) pIpCxt->u2PmtuCfgEntryAge;
                }
            }
        }
            /* Fall Through */

        case ICMP_PARAM_PROB:    /* Let error messages reach end applications */
        case ICMP_TIME_EXCEED:
        case ICMP_QUENCH:
        {
            INT1                i1Err_pkt_proto = 0;

            /* The pBuf now looks like
             *                       <--------------  ICMP DATA  -------------->
             * +----------+----------+---------+-------------------------------+
             * | IP_HDR   | ICMP_HDR | IP_HDR  | HIGH LEVEL PROTO HDR AND DATA |
             * +----------+----------+---------+-------------------------------+
             *            ^ Current offset
             */

            ICMP_MOVE_TO_DATA (pBuf);
            IP_PKT_GET_PROTO (pBuf, i1Err_pkt_proto);
            switch (i1Err_pkt_proto)
            {
                case TCP_PTCL:
                    /* TCP's icmp handler is expected to extract src and dest
                     * it can use current offset in pBuf which points to IP_HDR
                     * of ICMP data.
                     */
#ifdef TCP_WANTED
                    iptask_icmp_tcp_in_cxt (u4CxtId, pBuf, u4Ip_src,
                                            Icmp.i1Type, Icmp.i1Code);
#else
                    IP_RELEASE_BUFFER (pBuf);
#endif

                    break;

                case UDP_PTCL:
                    /* UDP's icmp handler is expected to extract src and dest
                     * it can use current offset in pBuf which points to IP_HDR
                     * of ICMP data.
                     *
                     * This allows any application over UDP to monitor the fate
                     * of the packet sent.
                     */
                    iptask_icmp_udp_in_cxt (u4CxtId, pBuf, u4Ip_src,
                                            Icmp.i1Type, Icmp.i1Code);

                    break;

                default:
                    IP_BUF_RELEASE_MSG (pBuf, FALSE);
                    break;
            }
            break;
        }
        default:
            IP_BUF_RELEASE_MSG (pBuf, FALSE);
            break;
    }
    UNUSED_PARAM (InterfaceId);
    UNUSED_PARAM (u4RetVal);
    return;
}

/**** $$TRACE_PROCEDURE_NAME  = icmp_output  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD        ****/
/*-------------------------------------------------------------------+
 * Function           : icmp_output_InCxt
 *
 * Input(s)           : pIpCxt, u4Dest, i1Tos, pBuf, u2Len, Icmp, u1OptLen
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * Any module, when it wants to send ICMP message uses icmp_output_InCxt
 * procedure. Depending on type and code this procedure constructs the
 * ICMP message and sends it using ip_send_InCxt. pArgs contains the data for
 * the second four bytes of the ICMP header, depending on the type of the
 * message.
 * Caller should fill the Mask in Data Part for Only Subnet Mask
 * Request and Reply
 *
 * pBuf is not freed inside this procedure. The caller is expected to
 * free it. Further it is also assumed that IP and ICMP both reside in the
 * same task. Otherwise passing pointer to IP header may not be efficient.
 *
 * Structure of pBuf is
 *
 *  +--------------+----------------+-------------------------+
 *  | IP HDR space | ICMP HDR space | Data to send or old pkt |
 *  +--------------+----------------+-------------------------+
 *                 ^
 *                 | Pointer Should Be Here
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
icmp_output_InCxt (tIpCxt * pIpCxt, UINT4 u4Dest, UINT1 u1Tos,
                   tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, t_ICMP * pIcmp,
                   UINT1 u1Optlen, UINT2 u2Port)
#else

INT4
icmp_output (pIpCxt, u4Dest, u1Tos, pBuf, u2Len, t_ICMP * pIcmp, u1Optlen,
             u2Port)
     tIpCxt             *pIpCxt;
     UINT4               u4Dest;    /* Destination to send                    */
     UINT1               u1Tos;    /* Type of service if any                 */
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Data to be attached with ICMP message  */
     UINT2               u2Len;    /* Length of packet to be sent with msg   */
     t_ICMP             *pIcmp;    /* ICMP header informations */
     UINT1               u1Optlen;
     UINT2               u2Port;
#endif
{
    if (((pIcmp->i1Type >= ICMP_ECHO_REPLY) &&
         (pIcmp->i1Type <= ICMP_MASK_REPLY)) ||
        (pIcmp->i1Type == ICMP_SECURITY_FAILURE))
    {
        pIpCxt->Icmp_stats.u4Out[(UINT2) pIcmp->i1Type]++;
    }

    icmp_put_hdr (pIcmp, pBuf, (UINT2) (u2Len + ICMP_HDR_LEN));

    /* Send it to IP. ttl restricts redirect messages to directly connected
     * networks.
     * IP should fill in the source address depending on the route interface.
     */

#ifdef IPSEC_STUB_WANTED        /* for OSE porting */
    if (pIcmp->i1Type == ICMP_SECURITY_FAILURE)
    {
        IpsecSendErrorMessage (u4Dest, u1Tos, pBuf,
                               (UINT2) (u2Len + ICMP_HDR_LEN), u1Optlen);
    }
#endif

    ip_send_InCxt (pIpCxt, IP_ANY_ADDR, u4Dest, ICMP_PTCL, u1Tos, 0, pBuf,
                   (UINT2) (u2Len + ICMP_HDR_LEN), 0, FALSE, u1Optlen, u2Port);
    return SUCCESS;
}

/**** $$TRACE_PROCEDURE_NAME  = icmp_error_msg  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD           ****/
/*-------------------------------------------------------------------+
 * Function           : icmp_error_msg
 *
 * Input(s)           : pBuf, pIcmp
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS or FAILURE
 *
 * Action :
 *
 * Generates an ICMP error message.
 * Allocates buffer, copies part of old pkt into it and calls icmp_output.
 * Additional checks such as loop are done here before calling icmp_output
 * Broadcast check is not done - caller has to take care.
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
icmp_error_msg (tIP_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp)
#else

INT4
icmp_error_msg (pBuf, pIcmp)
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Data to be attached with ICMP message  */
     t_ICMP             *pIcmp;
#endif
{
    t_IP                OrigIphdr;
    tIP_BUF_CHAIN_HEADER *pTmp_buf = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4IpDest = 0;
    UINT4               u4CxtId = 0;
    UINT4               u4Ip_src = 0;
    INT2                i2Ip_Fl_offs = 0;
    UINT2               u2Data_len = 0;
    UINT1               u1Ip_proto = 0;
    UINT1               u1Ip_tos = 0;
    UINT1               u1Hlen = 0;
    UINT1               u1MsgType = 0;
    INT1                i1_Optlen = 0;
    UINT1               au1_IpOption[IP_OPTIONS_LEN];
    UINT2               u2Ip_Fl_offs = 0;

    u4CxtId = pIcmp->u4ContextId;
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (NULL == pIpCxt)
    {
        return FAILURE;
    }

    if ((pIcmp->i1Type == ICMP_REDIRECT) &&
        (pIpCxt->Icmp_cfg.i1Send_redirect != ICMP_SEND_REDIRECT_ENABLE))
    {
        return FAILURE;
    }

    if ((pIcmp->i1Type == ICMP_DEST_UNREACH) &&
        (pIpCxt->Icmp_cfg.i1Send_unreachable != ICMP_SEND_UNREACHABLE_ENABLE))
    {
        return FAILURE;
    }

    IP_PKT_GET_PROTO (pBuf, u1Ip_proto);

    if (u1Ip_proto == ICMP_PTCL)
    {
        IP_PKT_GET_HLEN (pBuf, u1Hlen);
        u1Hlen = (UINT1) ((u1Hlen & IP_HEADER_LEN_MASK) *
                          (UINT1) (sizeof (UINT4)));
        IP_COPY_FROM_BUF (pBuf, (UINT1 *) &u1MsgType, u1Hlen, sizeof (UINT1));

        /* Add new error messages to this list */
        switch (u1MsgType)
        {
            case ICMP_DEST_UNREACH:
            case ICMP_REDIRECT:
            case ICMP_QUENCH:
            case ICMP_TIME_EXCEED:
            case ICMP_SECURITY_FAILURE:
            case ICMP_PARAM_PROB:
            {
                /* No ICMP Error Msgs for ICMP error Msgs */
                pIpCxt->Icmp_err.u4Loop++;
                return FAILURE;
            }
            default:
                break;

        }
    }

    IP_PKT_GET_FLAGSOFFSET (pBuf, u2Ip_Fl_offs);
    i2Ip_Fl_offs = (INT2) u2Ip_Fl_offs;
    if (IP_FIRST_FRAG (i2Ip_Fl_offs) == FALSE)
    {
        return SUCCESS;
    }

    IP_PKT_GET_DEST (pBuf, u4IpDest);

    if ((IP_IS_ADDR_CLASS_D (u4IpDest))
        || (u4IpDest == LIMITED_BROADCAST_ADDRESS)
        || ((IpifIsBroadCastLocalInCxt (pIpCxt->u4ContextId, u4IpDest)
             == IP_SUCCESS)
            && (Ipv4IsLoopbackAddress (pIpCxt->u4ContextId, u4IpDest) !=
                IP_SUCCESS)))
    {
        return FAILURE;
    }

    i1_Optlen = 0;

    if (pIcmp->i1Type != ICMP_PARAM_PROB)
    {
        if (IpExtractHdr (&OrigIphdr, pBuf) == FAILURE)
        {
            return FAILURE;
        }

        if (OrigIphdr.u2Olen != 0)
        {
            i1_Optlen = icmp_prcs_ip_options (&OrigIphdr, au1_IpOption, TRUE);
            if (i1_Optlen == FAILURE)
            {
                return FAILURE;
            }

        }

        /* icmp_prcs_ip_options may change the u4Src */
        u4Ip_src = OrigIphdr.u4Src;
        u1Ip_tos = OrigIphdr.u1Tos;
    }
    else
    {
        IP_PKT_GET_TOS (pBuf, u1Ip_tos);
        IP_PKT_GET_SRC (pBuf, u4Ip_src);
    }

    /* Send some part of old packet */

    /* Makefile Changes - comparison btwn signed and unsigned variales */

    u2Data_len =
        (UINT2)
        ICMP_MIN ((UINT4)
                  (ICMP_MAX_DATA - IP_HDR_LEN - i1_Optlen - ICMP_HDR_LEN),
                  IP_GET_BUF_LENGTH (pBuf));

    if ((pTmp_buf =
         IP_ALLOCATE_BUF ((UINT4) (IP_MAC_HDR_LEN + i1_Optlen + ICMP_HDR_LEN +
                                   u2Data_len), IP_MAC_HDR_LEN)) == NULL)
    {
        return FAILURE;
    }

    if (((i1_Optlen) != 0) && (pIcmp->i1Type != ICMP_PARAM_PROB))
    {
        IP_COPY_TO_BUF (pTmp_buf, au1_IpOption, 0, (UINT1) i1_Optlen);
        IP_BUF_MOVE_VALID_OFFSET (pTmp_buf, i1_Optlen);
    }

    if (u2Data_len != 0)
    {
        UINT1              *pu1ErrPkt = NULL;

        pu1ErrPkt = IP_GET_DATA_PTR_IF_LINEAR (pBuf, 0, u2Data_len);
        if (pu1ErrPkt == NULL)
        {

            /* Copy 16 bytes by 16 bytes (Better than byte by byte) */

            UINT1               au1CpyBytes[16];
            UINT4               u4CpyOffset = 0;
            UINT4               u4NumBytes = 0;

            u4NumBytes = u2Data_len;
            while (u4NumBytes >= IP_SIXTEEN)
            {
                IP_COPY_FROM_BUF (pBuf, au1CpyBytes, u4CpyOffset, IP_SIXTEEN);
                IP_COPY_TO_BUF (pTmp_buf, au1CpyBytes,
                                ICMP_HDR_LEN + u4CpyOffset, IP_SIXTEEN);
                u4NumBytes -= IP_SIXTEEN;
                u4CpyOffset += IP_SIXTEEN;
            }
            if (u4NumBytes)
            {
                IP_COPY_FROM_BUF (pBuf, au1CpyBytes, u4CpyOffset, u4NumBytes);
                IP_COPY_TO_BUF (pTmp_buf, au1CpyBytes,
                                ICMP_HDR_LEN + u4CpyOffset, u4NumBytes);
            }
        }
        else
        {
            IP_COPY_TO_BUF (pTmp_buf, pu1ErrPkt, ICMP_HDR_LEN, u2Data_len);
        }

    }

    if (pIcmp->i1Type != ICMP_QUENCH)
    {
        u1Ip_tos = (u1Ip_tos & 0xf8) | IP_PRD_NWCTRL;
    }

    return (icmp_output_InCxt (pIpCxt, u4Ip_src, u1Ip_tos, pTmp_buf, u2Data_len,
                               pIcmp, (UINT1) i1_Optlen, IPIF_INVALID_INDEX));
}

/*-------------------------------------------------------------------+
 * Function           : icmp_put_hdr
 *
 * Input(s)           : pIcmp, pBuf, u2Len
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Appends the header to the buffer and puts in the checksum.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
static INT4
icmp_put_hdr (t_ICMP * pIcmp, tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len)
#else
static INT4
icmp_put_hdr (pIcmp, pBuf, u2Len)
     t_ICMP             *pIcmp;
     tIP_BUF_CHAIN_HEADER *pBuf;
     UINT2               u2Len;
#endif
{
    tIcmpHdr           *pIcmpHdr = NULL;
    tIcmpHdr            TmpIcmpHdr;

    pIcmpHdr = &TmpIcmpHdr;

    pIcmpHdr->i1Type = pIcmp->i1Type;
    pIcmpHdr->i1Code = pIcmp->i1Code;
    pIcmpHdr->u2Cksum = 0;        /* Clear Checksum */

    switch (pIcmp->i1Type)
    {
            /* 
             * If the Second Four Bytes are split into Two - 2 Bytes
             */
        case ICMP_ECHO:
        case ICMP_ECHO_REPLY:
        case ICMP_TIMESTAMP:
        case ICMP_TIME_REPLY:
        case ICMP_MASK_RQST:
        case ICMP_MASK_REPLY:
        case ICMP_DOMAIN_NAME_REQUEST:
        case ICMP_DOMAIN_NAME_REPLY:
        case ICMP_SECURITY_FAILURE:
            /* Added for PMTU_DISCOVERY */
        case ICMP_DEST_UNREACH:
            pIcmpHdr->args.Identification.i2Id =
                (INT2) IP_HTONS (pIcmp->args.Identification.i2Id);
            pIcmpHdr->args.Identification.i2Seq =
                (INT2) IP_HTONS (pIcmp->args.Identification.i2Seq);
            break;

            /*
             * If the Second Four Bytes are not split ie. One - 4 Bytes
             */
        case ICMP_ROUTER_SOLCT:
        case ICMP_TIME_EXCEED:
        case ICMP_QUENCH:
        case ICMP_REDIRECT:
            pIcmpHdr->args.u4Address = CRU_HTONL (pIcmp->args.u4Address);
            break;
            /*
             * If the Second Four Bytes are split into 
             * Two - 1 Byte and One - 2 Bytes
             * 
             * NOTE : ICMP Parameter Problem type has only the first byte used. The
             * remaining three bytes are unused so i have split them into
             * One - 1 Byte and One - 2 Bytes. If in the future the unused fields
             * are split into Three - 1 Bytes then this code has to be changed.
             * 
             */

        case ICMP_PARAM_PROB:
        case ICMP_ROUTER_ADVT:
            pIcmpHdr->args.Irdp.u1NumAddr = pIcmp->args.Irdp.u1NumAddr;
            pIcmpHdr->args.Irdp.u1AddrEntrySize =
                pIcmp->args.Irdp.u1AddrEntrySize;

            pIcmpHdr->args.Irdp.u2LifeTime =
                CRU_HTONS (pIcmp->args.Irdp.u2LifeTime);

            break;
        default:
            break;
    }

    IP_COPY_TO_BUF (pBuf, pIcmpHdr, 0, sizeof (tIcmpHdr));

    pIcmpHdr->u2Cksum = CRU_HTONS (IpCalcCheckSum (pBuf, u2Len, 0));
    /* Fill Checksum */

    IP_COPY_TO_BUF (pBuf, &pIcmpHdr->u2Cksum, ICMP_PKT_CKSUM_OFF,
                    sizeof (UINT2));

    return SUCCESS;

}

/*-------------------------------------------------------------------+
 * Function           : icmp_extract_hdr
 *
 * Input(s)           : pBuf
 *
 * Output(s)          : pIcmp
 *
 * Returns            : SUCCESS.
 *
 * Action :
 *
 * Extracts header information from the input buffer.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
static INT4
icmp_extract_hdr (t_ICMP * pIcmp, tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len)
#else
static INT4
icmp_extract_hdr (pIcmp, pBuf)
     t_ICMP             *pIcmp;    /* ICMP header structure */
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Input buffer */
#endif
{
    tIcmpHdr           *pIcmpHdr = NULL;
    tIcmpHdr            TmpIcmpHdr;

    pIcmpHdr =
        (tIcmpHdr *) (VOID *) IP_GET_DATA_PTR_IF_LINEAR (pBuf, 0,
                                                         sizeof (tIcmpHdr));

    if (pIcmpHdr == NULL)
    {
        pIcmpHdr = &TmpIcmpHdr;

        IP_COPY_FROM_BUF (pBuf, (UINT1 *) pIcmpHdr, 0, sizeof (tIcmpHdr));
    }

    pIcmp->i1Type = pIcmpHdr->i1Type;
    pIcmp->i1Code = pIcmpHdr->i1Code;

    if (IpCalcCheckSum (pBuf, u2Len, 0) != IP_VALID_CHECKSUM)
    {
        IP_TRC (ICMP_MOD_TRC, ALL_FAILURE_TRC, "ICMP",
                " Validating checksum failed \r\n");
        return IP_FAILURE;
    }

    switch (pIcmp->i1Type)
    {
            /* 
             * If the Second Four Bytes are split into Two - 2 Bytes
             */
        case ICMP_ECHO:
        case ICMP_ECHO_REPLY:
        case ICMP_TIMESTAMP:
        case ICMP_TIME_REPLY:
        case ICMP_MASK_RQST:
        case ICMP_MASK_REPLY:
        case ICMP_DOMAIN_NAME_REQUEST:
        case ICMP_DOMAIN_NAME_REPLY:
        case ICMP_SECURITY_FAILURE:
            /* Added for PMTU_DISCOVERY */
        case ICMP_DEST_UNREACH:
            pIcmp->args.Identification.i2Id =
                (INT2) IP_NTOHS (pIcmpHdr->args.Identification.i2Id);
            pIcmp->args.Identification.i2Seq =
                (INT2) IP_NTOHS (pIcmpHdr->args.Identification.i2Seq);
            break;

            /*
             * If the Second Four Bytes are not split ie. One - 4 Bytes
             */
        case ICMP_ROUTER_SOLCT:
        case ICMP_TIME_EXCEED:
        case ICMP_QUENCH:
        case ICMP_REDIRECT:
            pIcmp->args.u4Address = IP_NTOHL (pIcmpHdr->args.u4Address);
            break;
            /*
             * If the Second Four Bytes are split into 
             * Two - 1 Byte and One - 2 Bytes
             * 
             * NOTE : ICMP Parameter Problem type has only the first byte used. The
             * remaining three bytes are unused so i have split them into
             * Two - 1 Byte and One - 2 Bytes. If in the future the unused fields
             * are split into Three - 1 Bytes then this code has to be changed.
             * 
             */
        case ICMP_PARAM_PROB:
        case ICMP_ROUTER_ADVT:
            pIcmp->args.Irdp.u1NumAddr = pIcmpHdr->args.Irdp.u1NumAddr;
            pIcmp->args.Irdp.u1AddrEntrySize =
                pIcmpHdr->args.Irdp.u1AddrEntrySize;
            pIcmp->args.Irdp.u2LifeTime =
                IP_NTOHS (pIcmpHdr->args.Irdp.u2LifeTime);
        default:
            break;
    }

    return SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : Ip_Icmp_Cfg_Init
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS
 *
 * Action :
 * Initializes Icmp_cfg structure.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
Ip_Icmp_Cfg_Init_InCxt (tIpCxt * pIpCxt)
#else

INT4
Ip_Icmp_Cfg_Init_InCxt (pIpCxt)
     tIpCxt             *pIpCxt;

#endif
{
    tNetIpRegInfo       NetIpRegInfo;
    UINT2               u2Index = 0;

    MEMSET (&NetIpRegInfo, 0, sizeof (tNetIpRegInfo));
    NetIpRegInfo.u1ProtoId = ICMP_PTCL;
    NetIpRegInfo.u2InfoMask = NetIpRegInfo.u2InfoMask | NETIPV4_PROTO_PKT_REQ;
    NetIpRegInfo.pProtoPktRecv = fs_icmp_input;
    NetIpRegInfo.u4ContextId = pIpCxt->u4ContextId;
    IPFWD_CXT_UNLOCK ();
    if (NetIpv4RegisterHigherLayerProtocol (&NetIpRegInfo) == NETIPV4_FAILURE)
    {
        return IP_FAILURE;
    }

    IPFWD_CXT_LOCK ();

    for (u2Index = 0; u2Index < ICMP_MAX_DOMAIN_NAME_SIZE; u2Index++)
    {
        pIpCxt->au1DomainName[u2Index] = '\0';
    }

    pIpCxt->i4TimeToLive = ICMP_DEFAULT_TIME_TO_LIVE;

    /* Set the back pointer to IP context */
    pIpCxt->Icmp_err.pIpCxt = pIpCxt;
    pIpCxt->Icmp_err.u4Type = 0;
    pIpCxt->Icmp_err.u4Loop = 0;
    pIpCxt->Icmp_err.u4Cksum = 0;
    pIpCxt->Icmp_err.u4Bcast = 0;

    for (u2Index = 0; u2Index < ICMP_TYPES; u2Index++)
    {
        pIpCxt->Icmp_stats.u4Out[u2Index] = 0;
        pIpCxt->Icmp_stats.u4In[u2Index] = 0;
    }
    MEMSET (&pIpCxt->Icmp_stats, 0, sizeof (t_ICMP_STATS));
    pIpCxt->Icmp_stats.pIpCxt = pIpCxt;

    pIpCxt->Icmp_cfg.pIpCxt = pIpCxt;
    pIpCxt->Icmp_cfg.i1Echo_reply = ICMP_ECHO_REPLY_ENABLE;
    pIpCxt->Icmp_cfg.i1Nmask_reply = ICMP_NMASK_REPLY_ENABLE;
    pIpCxt->Icmp_cfg.i1Time_reply = ICMP_TIME_REPLY_ENABLE;
    pIpCxt->Icmp_cfg.i1Send_redirect = ICMP_SEND_REDIRECT_ENABLE;
    pIpCxt->Icmp_cfg.i1Send_unreachable = ICMP_SEND_UNREACHABLE_ENABLE;
    pIpCxt->Icmp_cfg.i1DirectQueryStatus = ICMP_DIRECTQUERY_DISABLE;
    pIpCxt->Icmp_cfg.i1SendSecurityFailure = ICMP_SEND_SEC_FAILURE_ENABLE;
    pIpCxt->Icmp_cfg.i1RecvSecurityFailure = ICMP_RECV_SEC_FAILURE_ENABLE;

    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : icmp_reply_update_buffer
 *
 * Input(s)           : pBuf, pIpHdr
 *
 * Output(s)          : None
 *
 * Returns            : Length of the IP option to be sent.
 *
 * Action :
 *    Function updates the Buffer with appropriate IP Options for reply.
 *    It takes care of adjusting the buffer offsets.
 *    Calls icmp_prcs_ip_options to process the IP Options.
+-------------------------------------------------------------------*/
#ifdef __STDC__

static UINT2
icmp_reply_update_buffer (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIpHdr)
#else

static UINT2
icmp_reply_update_buffer (pBuf, pIpHdr)
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIpHdr;
#endif
{
    UINT1               au1_IpOption[IP_OPTIONS_LEN];
    INT1                i1_Optlen = 0;

    if (pIpHdr->u2Olen == 0)
    {
        return 0;                /* Nothing to do */
    }

    /*   Get the processed options into au1_IpOption   */
    i1_Optlen = icmp_prcs_ip_options (pIpHdr, au1_IpOption, FALSE);

    if (i1_Optlen >= 0)
    {
        /* Move in reverse to IP option area, copy the options and 
         * restore offset to ICMP header
         */
        IP_BUF_MOVE_BACK_VALID_OFFSET (pBuf, i1_Optlen);
        IP_COPY_TO_BUF (pBuf, au1_IpOption, 0, (UINT1) i1_Optlen);
        IP_BUF_MOVE_VALID_OFFSET (pBuf, (UINT4) i1_Optlen);

    }
    return (UINT2) i1_Optlen;
}

/**** $$TRACE_PROCEDURE_NAME  = icmp_prcs_ip_options  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD                 ****/
/*-------------------------------------------------------------------+
 * Function           : icmp_prcs_ip_options
 *
 * Input(s)           : pIp, u1_SrcRt_Only
 *
 * Output(s)          : au1_DstOption, pIp->u4Src
 *
 * Returns            : Length of the IP option to be sent.
 *
 * Action :
 *    Function copies IP options present in original datagram (pIp)
 *    into au1_DstOption. u1_SrcRt_Only controls which options are
 *    to be copied.
 *    pIp->u4Src may get changed when ip_reverse_src_route_option
 *    is called.
+-------------------------------------------------------------------*/
#ifdef __STDC__

static INT1
icmp_prcs_ip_options (t_IP * pIp, UINT1 *au1_DstOption, UINT1 u1_SrcRt_Only)
#else

static INT1
icmp_prcs_ip_options (pIp, au1_DstOption, u1_SrcRt_Only)
     t_IP               *pIp;
     UINT1              *au1_DstOption;
     UINT1               u1_SrcRt_Only;
#endif
{
    UINT1               u1Index = 0;
    UINT1              *pu1_IpOption = pIp->au1Options;
    UINT1               u1Opt_len = 0;
    UINT1               u1Total_len = 0;
    UINT1               u1Opt = 0;

    while ((u1Index < pIp->u2Olen) &&
           ((u1Opt = IP_OPT_NUMBER (*pu1_IpOption)) != IP_OPT_EOL))
    {
        u1Opt_len = (UINT1) ((u1Opt == IP_OPT_NOP) ? 1 : *(pu1_IpOption + 1));

        if (u1Opt_len > pIp->u2Olen)
        {
            return FAILURE;
        }

        switch (u1Opt)
        {
            case IP_OPT_NOP:
                break;

            case IP_OPT_SSROUTE:
            case IP_OPT_LSROUTE:
                u1Total_len =
                    (UINT1) (u1Total_len +
                             ip_reverse_src_route_option (&au1_DstOption
                                                          [u1Total_len],
                                                          pu1_IpOption,
                                                          pIp->u4Src,
                                                          &pIp->u4Src));
                break;

            case IP_OPT_RROUTE:
            case IP_OPT_TSTAMP:
                if (u1_SrcRt_Only == FALSE)
                {
                    MEMCPY (&au1_DstOption[u1Total_len], pu1_IpOption,
                            u1Opt_len);
                    u1Total_len = (UINT1) (u1Total_len + u1Opt_len);
                }
                break;
            default:
                break;
        }
        u1Index = (UINT1) (u1Index + u1Opt_len);
        pu1_IpOption += u1Opt_len;
    }

    /* Terminate with End Of Options
     *   if the Option length is non-zero and the length is not in a
     *   word boundary, then put the End of Option List type & set the
     *   header length to a word boundary.
     */
    if ((u1Total_len != 0) && ((u1Total_len % IP_FOUR) != 0))
    {
        au1_DstOption[u1Total_len] = IP_OPT_EOL;
        u1Total_len = (UINT1) (((u1Total_len >> IP_TWO) + 1) << IP_TWO);
    }
    return ((INT1) u1Total_len);
}

/**** $$TRACE_PROCEDURE_NAME  =  iptask_icmp_mask_reply_handler_InCxt ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/

/*-------------------------------------------------------------------+
 *
 * Function           : iptask_icmp_mask_reply_handler_InCxt
 *
 * Input(s)           : pIpCxt, pBuf, u4Ip_src, u4Ip_dest, i2Icmp_Id, i2Seq
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :
 *
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

VOID
iptask_icmp_mask_reply_handler_InCxt (tIpCxt * pIpCxt,
                                      tIP_BUF_CHAIN_HEADER * pBuf,
                                      UINT4 u4Ip_src, UINT4 u4Ip_dest,
                                      INT2 i2Icmp_Id, INT2 i2Seq)
#else

VOID
iptask_icmp_mask_reply_handler_InCxt (pIpCxt, pBuf, u4Ip_src, u4Ip_dest,
                                      i2Icmp_Id, i2Seq)
     tIpCxt             *pIpCxt;
     tIP_BUF_CHAIN_HEADER *pBuf;
     UINT4               u4Ip_src;
     UINT4               u4Ip_dest;
     INT2                i2Icmp_Id;
     INT2                i2Seq;
#endif
{
    /* Check here for wrong address mask replies.
     * Refer RFC1812, Sec. 4.3.3.9  for more details.
     */

    /* Makefile Changes - unused parameter */
    UNUSED_PARAM (i2Icmp_Id);
    UNUSED_PARAM (u4Ip_src);
    UNUSED_PARAM (u4Ip_dest);
    UNUSED_PARAM (i2Seq);
    UNUSED_PARAM (pIpCxt);
    IP_BUF_RELEASE_MSG (pBuf, FALSE);
    return;
}

#ifdef IPSEC_STUB_WANTED

/*-------------------------------------------------------------------+
 *
 * Function           : IpsecFailureHandler 
 *
 * Input(s)           : pBuf, pIcmp
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action :
 *
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
static VOID
IpsecFailureHandler (tIP_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp)
#else
static VOID
IpsecFailureHandler (pBuf, pIcmp)
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_ICMP             *pIcmp;
#endif
{
    INT1                i1Type = 0;

    /* Makefile Changes - unused parameter */
    if (pBuf != NULL)
    {
        i1Type = pIcmp->i1Type;
    }
    UNUSED_PARAM (i1Type);

}

/*-------------------------------------------------------------------+
 *
 * Function           : IpsecSendErrorMessage 
 *
 * Input(s)           : u4Dest, pBuf, u1Tos, u2Len, u1OptLen
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action :
 *
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
IpsecSendErrorMessage (UINT4 u4Dest,
                       UINT1 u1Tos,
                       tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, UINT1 u1OptLen)
#else
VOID
IpsecSendErrorMessage (u4Dest, u1Tos, pBuf, u2Len, u1OptLen)
     UINT4               u4Dest;
     UINT1               u1Tos;
     tIP_BUF_CHAIN_HEADER *pBuf;
     UINT2               u2Len;
     UINT1               u1OptLen;
#endif
{

    /* Makefile Changes - unused parameter */
    if (pBuf != NULL)
    {
        UNUSED_PARAM (u4Dest);
    }
    u1Tos = 0;
    u2Len = 0;
    u1OptLen = 0;
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (u1OptLen);
    UNUSED_PARAM (u1Tos);

}
#endif

/*-------------------------------------------------------------------+
 *
 * Function           : IcmpGenerateDomainNameReplyInCxt
 *
 * Input(s)           : pIpCxt, u4ReplySrc, u4ReplyDest, pIcmp
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action :
 *
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
IcmpGenerateDomainNameReplyInCxt (tIpCxt * pIpCxt, UINT4 u4ReplySrc,
                                  UINT4 u4ReplyDest, t_ICMP * pIcmp)
#else
VOID
IcmpGenerateDomainNameReplyInCxt (pIpCxt, u4ReplySrc, u4ReplyDest, pIcmp)
     tIpCxt             *pIpCxt;
     UINT4               u4ReplySrc;
     UINT4               u4ReplyDest;
     t_ICMP             *pIcmp;
#endif
{
    UINT1               u1StringLen = 0;
    UINT1               u1DataLen = 0;
    UINT1               u1LabelLen = 0;
    UINT2               u2Index = 0;
    UINT2               u2SubIndex = 0;
    UINT4               u4Val = 0;
    UINT4               u4Offset = 0;

    tIP_BUF_CHAIN_HEADER *pBuf = NULL;

    pIcmp->i1Type = ICMP_DOMAIN_NAME_REPLY;

    u1StringLen = (UINT1) STRLEN (pIpCxt->au1DomainName);

    /* 
     * The 2 bytes -- 2 * ICMP_LABEL_LENGTH -- added below is for the starting 
     * label field and the trailing lable field.
     */

    u1DataLen = (UINT1) (u1StringLen + ICMP_TTL_FIELD_SIZE
                         + IP_TWO * ICMP_LABEL_LENGTH);
    pBuf = IP_ALLOCATE_BUF ((UINT4) (IP_MAC_HDR_LEN + ICMP_HDR_LEN + u1DataLen),
                            (UINT4) (IP_MAC_HDR_LEN + ICMP_HDR_LEN));
    if (pBuf == NULL)
    {
        IP_CXT_TRC (pIpCxt->u4ContextId, ICMP_MOD_TRC, ALL_FAILURE_TRC, "ICMP",
                    " Allocation failed for Domain name reply Buffer\n");
        return;
    }

    if (pIpCxt->au1DomainName[0] == '\0')
    {
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Val, 0, IP_FOUR);
        u4Offset += IP_FOUR;
    }
    else
    {
        u4Val = (UINT4) IP_HTONL ((UINT4) (pIpCxt->i4TimeToLive));
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Val, u4Offset, IP_FOUR);
        u4Offset += IP_FOUR;
    }

    ICMP_MOVE_FORW_TO (pBuf, sizeof (pIpCxt->i4TimeToLive));

    for (u2Index = 0; u2Index < u1StringLen;
         u2Index = (UINT2) (u1LabelLen + 1 + u2Index))
    {
        u4Val = 0;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Val, u4Offset, 1);
        u4Offset++;

        u1LabelLen = 0;
        u2SubIndex = u2Index;
        while ((pIpCxt->au1DomainName[u2SubIndex] != '.') &&
               (u2SubIndex < u1StringLen))
        {
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *)
                                       &pIpCxt->au1DomainName[u2SubIndex],
                                       u4Offset, 1);
            u4Offset++;

            u2SubIndex++;
            u1LabelLen++;
        }
        u4Offset = 0;            /* Equivalent to ICMP_MOVE_BACK_TO_LENGTH_FIELD */
        CRU_BUF_Copy_OverBufChain (pBuf, &u1LabelLen, u4Offset, 1);
        u4Offset++;

        ICMP_MOVE_FORW_TO (pBuf, (UINT4) (u1LabelLen + 1));
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Val, u4Offset, 1);
    u4Offset++;

    if (pIpCxt->au1DomainName[0] == '\0')
    {
        u4Val = 0;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Val, u4Offset, 1);
        u4Offset++;
    }
    ICMP_MOVE_FORW_TO (pBuf, 1);

    ICMP_MOVE_BACK_TO (pBuf, (UINT4) (ICMP_HDR_LEN + u1DataLen));

    icmp_put_hdr (pIcmp, pBuf, (UINT2) (ICMP_HDR_LEN + u1DataLen));

    IP_CXT_TRC_ARG1 (pIpCxt->u4ContextId, ICMP_MOD_TRC, DATA_PATH_TRC, "ICMP",
                     "sending domain name reply to %x.\n", u4ReplyDest);

    ip_send_InCxt (pIpCxt, u4ReplySrc, u4ReplyDest, ICMP_PTCL, 0, 0, pBuf,
                   (UINT2) (u1DataLen + ICMP_HDR_LEN), 0, FALSE, 0,
                   IPIF_INVALID_INDEX);

    pIpCxt->Icmp_stats.u4Out[(UINT2) pIcmp->i1Type]++;
}

#ifdef DIQUERY_STUB_WANTED
/*Remove this stub when DNS supports Direct Query */

/*-------------------------------------------------------------------+
 *
 * Function           : DnsDomainNameReplyHandlerInCxt 
 *
 * Input(s)           : pIpCxt, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action :
 *
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
DnsDomainNameReplyHandlerInCxt (tIpCxt * pIpCxt, tIP_BUF_CHAIN_HEADER * pBuf)
#else
VOID
DnsDomainNameReplyHandlerInCxt (tIpCxt * pIpCxt, tIP_BUF_CHAIN_HEADER * pBuf)
#endif
{
    UINT4               u4Unused = 0;

    UNUSED_PARAM (pIpCxt);
    /* Makefile Changes - unused parameter */
    if (pBuf != NULL)
    {
        UNUSED_PARAM (u4Unused);
    }
}
#endif
