/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: icmpsnif.c,v 1.10 2016/04/29 09:10:20 siva Exp $
 *
 * Description: This file contains the implementation of      
 *              Internet Control Message Protocol management  
 *              support routines.                            
 *
 *******************************************************************/
#include "icmpinc.h"
#include "fsmpipcli.h"
#include "fsiplow.h"
/**************************************************************************/

/**** $$TRACE_MODULE_NAME     = IP_FWD     ****/
/**** $$TRACE_SUB_MODULE_NAME = ICMP     ****/

/****************** ICMP CONFIGURATION ************************/

/* Echo reply */
/****************************************************************************
 Function    :  nmhGetFsIcmpSendEchoReplyEnable
 Input       :  The Indices

                The Object
                retValIcmpSendEchoReplyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpSendEchoReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpSendEchoReplyEnable (INT4 *pi4RetValIcmpSendEchoReplyEnable)
#else
INT1
nmhGetFsIcmpSendEchoReplyEnable (pi4RetValIcmpSendEchoReplyEnable)
     INT4               *pi4RetValIcmpSendEchoReplyEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpSendEchoReplyEnable = pIpCxt->Icmp_cfg.i1Echo_reply;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpSendEchoReplyEnable
 Input       :  The Indices

                The Object
                testValIcmpSendEchoReplyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpSendEchoReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpSendEchoReplyEnable (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValIcmpSendEchoReplyEnable)
#else
INT1
nmhTestv2FsIcmpSendEchoReplyEnable (pu4ErrorCode,
                                    i4TestValIcmpSendEchoReplyEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpSendEchoReplyEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpSendEchoReplyEnable == ICMP_ECHO_REPLY_ENABLE) ||
        (i4TestValIcmpSendEchoReplyEnable == ICMP_ECHO_REPLY_DISABLE))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpSendEchoReplyEnable
 Input       :  The Indices

                The Object
                setValIcmpSendEchoReplyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpSendEchoReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpSendEchoReplyEnable (INT4 i4SetValIcmpSendEchoReplyEnable)
#else
INT1
nmhSetFsIcmpSendEchoReplyEnable (i4SetValIcmpSendEchoReplyEnable)
     INT4                i4SetValIcmpSendEchoReplyEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1Echo_reply = (INT1) i4SetValIcmpSendEchoReplyEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpSendEchoReplyEnable,
                                  FsMIFsIcmpSendEchoReplyEnable,
                                  (sizeof (FsMIFsIcmpSendEchoReplyEnable) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/* Net Mask reply */
/****************************************************************************
 Function    :  nmhGetFsIcmpNetMaskReplyEnable
 Input       :  The Indices

                The Object
                retValIcmpNetMaskReplyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpNetMaskReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpNetMaskReplyEnable (INT4 *pi4RetValIcmpNetMaskReplyEnable)
#else
INT1
nmhGetFsIcmpNetMaskReplyEnable (pi4RetValIcmpNetMaskReplyEnable)
     INT4               *pi4RetValIcmpNetMaskReplyEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpNetMaskReplyEnable = (INT4) pIpCxt->Icmp_cfg.i1Nmask_reply;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpNetMaskReplyEnable
 Input       :  The Indices

                The Object
                testValIcmpNetMaskReplyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpNetMaskReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpNetMaskReplyEnable (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValIcmpNetMaskReplyEnable)
#else
INT1
nmhTestv2FsIcmpNetMaskReplyEnable (pu4ErrorCode,
                                   i4TestValIcmpNetMaskReplyEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpNetMaskReplyEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpNetMaskReplyEnable == ICMP_NMASK_REPLY_ENABLE) ||
        (i4TestValIcmpNetMaskReplyEnable == ICMP_NMASK_REPLY_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpNetMaskReplyEnable
 Input       :  The Indices

                The Object
                setValIcmpNetMaskReplyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpNetMaskReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpNetMaskReplyEnable (INT4 i4SetValIcmpNetMaskReplyEnable)
#else
INT1
nmhSetFsIcmpNetMaskReplyEnable (i4SetValIcmpNetMaskReplyEnable)
     INT4                i4SetValIcmpNetMaskReplyEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1Nmask_reply = (INT1) i4SetValIcmpNetMaskReplyEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpNetMaskReplyEnable,
                                  FsMIFsIcmpNetMaskReplyEnable,
                                  (sizeof (FsMIFsIcmpNetMaskReplyEnable) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/* Time Stamp */
/****************************************************************************
 Function    :  nmhGetFsIcmpTimeStampReplyEnable
 Input       :  The Indices

                The Object
                retValIcmpTimeStampReplyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpTimeStampReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpTimeStampReplyEnable (INT4 *pi4RetValIcmpTimeStampReplyEnable)
#else
INT1
nmhGetFsIcmpTimeStampReplyEnable (pi4RetValIcmpTimeStampReplyEnable)
     INT4               *pi4RetValIcmpTimeStampReplyEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpTimeStampReplyEnable = (INT4) pIpCxt->Icmp_cfg.i1Time_reply;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpTimeStampReplyEnable
 Input       :  The Indices

                The Object
                testValIcmpTimeStampReplyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpTimeStampReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpTimeStampReplyEnable (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValIcmpTimeStampReplyEnable)
#else
INT1
nmhTestv2FsIcmpTimeStampReplyEnable (pu4ErrorCode,
                                     i4TestValIcmpTimeStampReplyEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpTimeStampReplyEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpTimeStampReplyEnable == ICMP_TIME_REPLY_ENABLE) ||
        (i4TestValIcmpTimeStampReplyEnable == ICMP_TIME_REPLY_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpTimeStampReplyEnable
 Input       :  The Indices

                The Object
                setValIcmpTimeStampReplyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpTimeStampReplyEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpTimeStampReplyEnable (INT4 i4SetValIcmpTimeStampReplyEnable)
#else
INT1
nmhSetFsIcmpTimeStampReplyEnable (i4SetValIcmpTimeStampReplyEnable)
     INT4                i4SetValIcmpTimeStampReplyEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1Time_reply = (INT1) i4SetValIcmpTimeStampReplyEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpTimeStampReplyEnable,
                                  FsMIFsIcmpTimeStampReplyEnable,
                                  (sizeof (FsMIFsIcmpTimeStampReplyEnable) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/* Send Redirect */
/****************************************************************************
 Function    :  nmhGetFsIcmpSendRedirectEnable
 Input       :  The Indices

                The Object
                retValIcmpSendRedirectEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpSendRedirectEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpSendRedirectEnable (INT4 *pi4RetValIcmpSendRedirectEnable)
#else
INT1
nmhGetFsIcmpSendRedirectEnable (pi4RetValIcmpSendRedirectEnable)
     INT4               *pi4RetValIcmpSendRedirectEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpSendRedirectEnable = (INT4) pIpCxt->Icmp_cfg.i1Send_redirect;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpSendRedirectEnable
 Input       :  The Indices

                The Object
                testValIcmpSendRedirectEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpSendRedirectEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpSendRedirectEnable (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValIcmpSendRedirectEnable)
#else
INT1
nmhTestv2FsIcmpSendRedirectEnable (pu4ErrorCode,
                                   i4TestValIcmpSendRedirectEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpSendRedirectEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpSendRedirectEnable == ICMP_SEND_REDIRECT_ENABLE) ||
        (i4TestValIcmpSendRedirectEnable == ICMP_SEND_REDIRECT_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpSendRedirectEnable
 Input       :  The Indices

                The Object
                setValIcmpSendRedirectEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpSendRedirectEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpSendRedirectEnable (INT4 i4SetValIcmpSendRedirectEnable)
#else
INT1
nmhSetFsIcmpSendRedirectEnable (i4SetValIcmpSendRedirectEnable)
     INT4                i4SetValIcmpSendRedirectEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1Send_redirect = (INT1) i4SetValIcmpSendRedirectEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpSendRedirectEnable,
                                  FsMIFsIcmpSendRedirectEnable,
                                  (sizeof (FsMIFsIcmpSendRedirectEnable) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/* Send UnReachable */
/****************************************************************************
 Function    :  nmhGetFsIcmpSendUnreachableEnable
 Input       :  The Indices

                The Object
                retValIcmpSendUnreachableEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpSendUnreachableEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpSendUnreachableEnable (INT4 *pi4RetValIcmpSendUnreachableEnable)
#else
INT1
nmhGetFsIcmpSendUnreachableEnable (pi4RetValIcmpSendUnreachableEnable)
     INT4               *pi4RetValIcmpSendUnreachableEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpSendUnreachableEnable = pIpCxt->Icmp_cfg.i1Send_unreachable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpSendUnreachableEnable
 Input       :  The Indices

                The Object
                testValIcmpSendUnreachableEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpSendUnreachableEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpSendUnreachableEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValIcmpSendUnreachableEnable)
#else
INT1
nmhTestv2FsIcmpSendUnreachableEnable (pu4ErrorCode,
                                      i4TestValIcmpSendUnreachableEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpSendUnreachableEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpSendUnreachableEnable == ICMP_SEND_UNREACHABLE_ENABLE) ||
        (i4TestValIcmpSendUnreachableEnable == ICMP_SEND_UNREACHABLE_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpSendUnreachableEnable
 Input       :  The Indices

                The Object
                setValIcmpSendUnreachableEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpSendUnreachableEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpSendUnreachableEnable (INT4 i4SetValIcmpSendUnreachableEnable)
#else
INT1
nmhSetFsIcmpSendUnreachableEnable (i4SetValIcmpSendUnreachableEnable)
     INT4                i4SetValIcmpSendUnreachableEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1Send_unreachable =
        (INT1) i4SetValIcmpSendUnreachableEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpSendUnreachableEnable,
                                  FsMIFsIcmpSendUnreachableEnable,
                                  (sizeof (FsMIFsIcmpSendUnreachableEnable) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/* Security Failure Messages */
/****************************************************************************
 Function    :  nmhGetFsIcmpInSecurityFailures
 Input       :  The Indices

                The Object 
                retValIcmpInSecurityFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInSecurityFailures ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpInSecurityFailures (UINT4 *pu4RetValIcmpInSecurityFailures)
#else
INT1
nmhGetFsIcmpInSecurityFailures (pu4RetValIcmpInSecurityFailures)
     UINT4              *pu4RetValIcmpInSecurityFailures;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIcmpInSecurityFailures =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt,
                                                  ICMP_SECURITY_FAILURE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIcmpOutSecurityFailures
 Input       :  The Indices

                The Object 
                retValIcmpOutSecurityFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutSecurityFailures ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpOutSecurityFailures (UINT4 *pu4RetValIcmpOutSecurityFailures)
#else
INT1
nmhGetFsIcmpOutSecurityFailures (pu4RetValIcmpOutSecurityFailures)
     UINT4              *pu4RetValIcmpOutSecurityFailures;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutSecurityFailures =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt,
                                                   ICMP_SECURITY_FAILURE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcmpSendSecurityFailuresEnable
 Input       :  The Indices

                The Object 
                retValIcmpSendSecurityFailuresEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpSendSecurityFailuresEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpSendSecurityFailuresEnable (INT4
                                        *pi4RetValIcmpSendSecurityFailuresEnable)
#else
INT1
nmhGetFsIcmpSendSecurityFailuresEnable (pi4RetValIcmpSendSecurityFailuresEnable)
     INT4               *pi4RetValIcmpSendSecurityFailuresEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpSendSecurityFailuresEnable =
        pIpCxt->Icmp_cfg.i1SendSecurityFailure;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpSendSecurityFailuresEnable
 Input       :  The Indices

                The Object 
                setValIcmpSendSecurityFailuresEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpSendSecurityFailuresEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpSendSecurityFailuresEnable (INT4
                                        i4SetValIcmpSendSecurityFailuresEnable)
#else
INT1
nmhSetFsIcmpSendSecurityFailuresEnable (i4SetValIcmpSendSecurityFailuresEnable)
     INT4                i4SetValIcmpSendSecurityFailuresEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1SendSecurityFailure =
        (INT1) i4SetValIcmpSendSecurityFailuresEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpSendSecurityFailuresEnable,
                                  FsMIFsIcmpSendSecurityFailuresEnable,
                                  (sizeof (FsMIFsIcmpSendSecurityFailuresEnable)
                                   / sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpSendSecurityFailuresEnable
 Input       :  The Indices

                The Object 
                testValIcmpSendSecurityFailuresEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpSendSecurityFailuresEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpSendSecurityFailuresEnable (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValIcmpSendSecurityFailuresEnable)
#else
INT1
nmhTestv2FsIcmpSendSecurityFailuresEnable (pu4ErrorCode,
                                           i4TestValIcmpSendSecurityFailuresEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpSendSecurityFailuresEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpSendSecurityFailuresEnable ==
         ICMP_SEND_SEC_FAILURE_ENABLE)
        || (i4TestValIcmpSendSecurityFailuresEnable ==
            ICMP_SEND_SEC_FAILURE_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIcmpRecvSecurityFailuresEnable
 Input       :  The Indices

                The Object 
                retValIcmpRecvSecurityFailuresEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpRecvSecurityFailuresEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpRecvSecurityFailuresEnable (INT4
                                        *pi4RetValIcmpRecvSecurityFailuresEnable)
#else
INT1
nmhGetFsIcmpRecvSecurityFailuresEnable (pi4RetValIcmpRecvSecurityFailuresEnable)
     INT4               *pi4RetValIcmpRecvSecurityFailuresEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpRecvSecurityFailuresEnable =
        pIpCxt->Icmp_cfg.i1RecvSecurityFailure;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpRecvSecurityFailuresEnable
 Input       :  The Indices

                The Object 
                setValIcmpRecvSecurityFailuresEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpRecvSecurityFailuresEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpRecvSecurityFailuresEnable (INT4
                                        i4SetValIcmpRecvSecurityFailuresEnable)
#else
INT1
nmhSetFsIcmpRecvSecurityFailuresEnable (i4SetValIcmpRecvSecurityFailuresEnable)
     INT4                i4SetValIcmpRecvSecurityFailuresEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1RecvSecurityFailure =
        (INT1) i4SetValIcmpRecvSecurityFailuresEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpRecvSecurityFailuresEnable,
                                  FsMIFsIcmpRecvSecurityFailuresEnable,
                                  (sizeof (FsMIFsIcmpRecvSecurityFailuresEnable)
                                   / sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpRecvSecurityFailuresEnable
 Input       :  The Indices

                The Object 
                testValIcmpRecvSecurityFailuresEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpRecvSecurityFailuresEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpRecvSecurityFailuresEnable (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValIcmpRecvSecurityFailuresEnable)
#else
INT1
nmhTestv2FsIcmpRecvSecurityFailuresEnable (pu4ErrorCode,
                                           i4TestValIcmpRecvSecurityFailuresEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpRecvSecurityFailuresEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpRecvSecurityFailuresEnable ==
         ICMP_RECV_SEC_FAILURE_ENABLE) ||
        (i4TestValIcmpRecvSecurityFailuresEnable ==
         ICMP_RECV_SEC_FAILURE_DISABLE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* ICMP Domain Name Messages */
/****************************************************************************
 Function    :  nmhGetFsIcmpInDomainNameRequests
 Input       :  The Indices

                The Object 
                retValIcmpInDomainNameRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInDomainNameRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpInDomainNameRequests (UINT4 *pu4RetValIcmpInDomainNameRequests)
#else
INT1
nmhGetFsIcmpInDomainNameRequests (pu4RetValIcmpInDomainNameRequests)
     UINT4              *pu4RetValIcmpInDomainNameRequests;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInDomainNameRequests =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt,
                                                  ICMP_DOMAIN_NAME_REQUEST);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcmpInDomainNameReply
 Input       :  The Indices

                The Object 
                retValIcmpInDomainNameReply
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpInDomainNameReply ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpInDomainNameReply (UINT4 *pu4RetValIcmpInDomainNameReply)
#else
INT1
nmhGetFsIcmpInDomainNameReply (pu4RetValIcmpInDomainNameReply)
     UINT4              *pu4RetValIcmpInDomainNameReply;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpInDomainNameReply =
        ip_icmp_mgmt_get_stats_in_for_type_InCxt (pIpCxt,
                                                  ICMP_DOMAIN_NAME_REPLY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcmpOutDomainNameRequests
 Input       :  The Indices

                The Object 
                retValIcmpOutDomainNameRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutDomainNameRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpOutDomainNameRequests (UINT4 *pu4RetValIcmpOutDomainNameRequests)
#else
INT1
nmhGetFsIcmpOutDomainNameRequests (pu4RetValIcmpOutDomainNameRequests)
     UINT4              *pu4RetValIcmpOutDomainNameRequests;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutDomainNameRequests =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt,
                                                   ICMP_DOMAIN_NAME_REQUEST);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcmpOutDomainNameReply
 Input       :  The Indices

                The Object 
                retValIcmpOutDomainNameReply
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpOutDomainNameReply ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpOutDomainNameReply (UINT4 *pu4RetValIcmpOutDomainNameReply)
#else
INT1
nmhGetFsIcmpOutDomainNameReply (pu4RetValIcmpOutDomainNameReply)
     UINT4              *pu4RetValIcmpOutDomainNameReply;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValIcmpOutDomainNameReply =
        ip_icmp_mgmt_get_stats_out_for_type_InCxt (pIpCxt,
                                                   ICMP_DOMAIN_NAME_REPLY);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIcmpDirectQueryEnable
 Input       :  The Indices

                The Object 
                retValIcmpDirectQueryEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIcmpDirectQueryEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIcmpDirectQueryEnable (INT4 *pi4RetValIcmpDirectQueryEnable)
#else
INT1
nmhGetFsIcmpDirectQueryEnable (pi4RetValIcmpDirectQueryEnable)
     INT4               *pi4RetValIcmpDirectQueryEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIcmpDirectQueryEnable = pIpCxt->Icmp_cfg.i1DirectQueryStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIcmpDirectQueryEnable
 Input       :  The Indices

                The Object 
                setValIcmpDirectQueryEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIcmpDirectQueryEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIcmpDirectQueryEnable (INT4 i4SetValIcmpDirectQueryEnable)
#else
INT1
nmhSetFsIcmpDirectQueryEnable (i4SetValIcmpDirectQueryEnable)
     INT4                i4SetValIcmpDirectQueryEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->Icmp_cfg.i1DirectQueryStatus = (INT1) i4SetValIcmpDirectQueryEnable;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValIcmpDirectQueryEnable,
                                  FsMIFsIcmpDirectQueryEnable,
                                  (sizeof (FsMIFsIcmpDirectQueryEnable) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIcmpDirectQueryEnable
 Input       :  The Indices

                The Object 
                testValIcmpDirectQueryEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IcmpDirectQueryEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIcmpDirectQueryEnable (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValIcmpDirectQueryEnable)
#else
INT1
nmhTestv2FsIcmpDirectQueryEnable (pu4ErrorCode, i4TestValIcmpDirectQueryEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIcmpDirectQueryEnable;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "IcmpDirectQueryEnable = %d\n", i4TestValIcmpDirectQueryEnable); ***/

    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIcmpDirectQueryEnable == ICMP_DIRECTQUERY_ENABLE) ||
        (i4TestValIcmpDirectQueryEnable == ICMP_DIRECTQUERY_DISABLE))
    {
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDomainName
 Input       :  The Indices

                The Object 
                retValDomainName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDomainName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsDomainName (tSNMP_OCTET_STRING_TYPE * pRetValDomainName)
#else
INT1
nmhGetFsDomainName (pRetValDomainName)
     tSNMP_OCTET_STRING_TYPE *pRetValDomainName;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    if (pIpCxt->au1DomainName[0] != '\0')
    {
        pRetValDomainName->i4_Length = (INT4) STRLEN (pIpCxt->au1DomainName);
        STRNCPY (pRetValDomainName->pu1_OctetList, pIpCxt->au1DomainName,
                 pRetValDomainName->i4_Length);
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    }
    else
    {
        pRetValDomainName->i4_Length = 0;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDomainName
 Input       :  The Indices

                The Object 
                setValDomainName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetDomainName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsDomainName (tSNMP_OCTET_STRING_TYPE * pSetValDomainName)
#else
INT1
nmhSetFsDomainName (pSetValDomainName)
     tSNMP_OCTET_STRING_TYPE *pSetValDomainName;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (pIpCxt->au1DomainName, pSetValDomainName->pu1_OctetList,
             pSetValDomainName->i4_Length);
    pIpCxt->au1DomainName[pSetValDomainName->i4_Length] = '\0';
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 's',
                                  pSetValDomainName,
                                  FsMIFsIcmpDomainName,
                                  (sizeof (FsMIFsIcmpDomainName) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDomainName
 Input       :  The Indices

                The Object 
                testValDomainName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2DomainName ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsDomainName (UINT4 *pu4ErrorCode,
                       tSNMP_OCTET_STRING_TYPE * pTestValDomainName)
#else
INT1
nmhTestv2FsDomainName (pu4ErrorCode, pTestValDomainName)
     UINT4              *pu4ErrorCode;
     tSNMP_OCTET_STRING_TYPE *pTestValDomainName;
#endif
{
    UINT2               u2NumOfCharsInLable = 0;
    UINT2               u2Index = 0;

    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    u2NumOfCharsInLable = 0;

    if ((pTestValDomainName->i4_Length < 0) ||
        (pTestValDomainName->i4_Length >= ICMP_MAX_DOMAIN_NAME_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars (pTestValDomainName->pu1_OctetList,
                              pTestValDomainName->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif

    for (u2Index = 0; u2Index < pTestValDomainName->i4_Length; u2Index++)
    {
        if (pTestValDomainName->pu1_OctetList[u2Index] == '.')
        {
            if (u2NumOfCharsInLable > ICMP_MAX_FS_DOMAIN_NAME_LEN)
            {
              /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            u2NumOfCharsInLable = 0;
        }
        u2NumOfCharsInLable++;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsTimeToLive
 Input       :  The Indices

                The Object 
                retValTimeToLive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetTimeToLive ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsTimeToLive (INT4 *pi4RetValTimeToLive)
#else
INT1
nmhGetFsTimeToLive (pi4RetValTimeToLive)
     INT4               *pi4RetValTimeToLive;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValTimeToLive = pIpCxt->i4TimeToLive;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsTimeToLive
 Input       :  The Indices

                The Object 
                setValTimeToLive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetTimeToLive ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsTimeToLive (INT4 i4SetValTimeToLive)
#else
INT1
nmhSetFsTimeToLive (i4SetValTimeToLive)
     INT4                i4SetValTimeToLive;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    pIpCxt->i4TimeToLive = i4SetValTimeToLive;
    IncMsrForIpv4IcmpGlobalTable ((INT4) u4ContextId, 'i',
                                  &i4SetValTimeToLive,
                                  FsMIFsIcmpTimeToLive,
                                  (sizeof (FsMIFsIcmpTimeToLive) /
                                   sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsTimeToLive
 Input       :  The Indices

                The Object 
                testValTimeToLive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2TimeToLive ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsTimeToLive (UINT4 *pu4ErrorCode, INT4 i4TestValTimeToLive)
#else
INT1
nmhTestv2FsTimeToLive (pu4ErrorCode, i4TestValTimeToLive)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValTimeToLive;
#endif
{
   /*** $$TRACE_LOG (ENTRY, "TimeToLive = %d\n", i4TestValTimeToLive); ***/
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4TestValTimeToLive >= 0)
    {
       /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Failure\n"); ***/
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIcmpSendRedirectEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpSendRedirectEnable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcmpSendUnreachableEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpSendUnreachableEnable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcmpSendEchoReplyEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpSendEchoReplyEnable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcmpNetMaskReplyEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpNetMaskReplyEnable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcmpTimeStampReplyEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpTimeStampReplyEnable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcmpDirectQueryEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpDirectQueryEnable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDomainName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDomainName (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTimeToLive
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTimeToLive (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcmpSendSecurityFailuresEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpSendSecurityFailuresEnable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIcmpRecvSecurityFailuresEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIcmpRecvSecurityFailuresEnable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
