/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: icmpprot.h,v 1.8 2011/10/25 09:53:28 siva Exp $
 *
 * Description: Contains Prototype definitions for ICMP module.
 *
 *******************************************************************/
#include "icmptyps.h"

VOID fs_icmp_input PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                        UINT2                  u2Len,
                        UINT4                  u4Port,
                        tCRU_INTERFACE         InterfaceId, 
                        UINT1                  u1Flag));

INT4 icmp_output_InCxt PROTO ((tIpCxt                *pIpCxt,
                               UINT4                  u4Dest,
                         UINT1                  u1Tos,
                         tCRU_BUF_CHAIN_HEADER *pBuf,
                         UINT2                  u2Len,
                         t_ICMP                *pIcmp, 
                         UINT1                  u1Optlen, 
                         UINT2                  u2Port));


INT4 Ip_Icmp_Cfg_Init_InCxt PROTO ((tIpCxt *pIpCxt));

VOID IcmpGenerateDomainNameReplyInCxt PROTO ((tIpCxt *pIpCxt, UINT4   u4Dest,
                                         UINT4   u4Src, 
                                         t_ICMP *pIcmp));

UINT4 ip_icmp_mgmt_get_stats_in_for_type_InCxt PROTO ((tIpCxt *pIpCxt, 
                                                       INT4 i4Type));

UINT4 ip_icmp_mgmt_get_stats_out_for_type_InCxt PROTO ((tIpCxt *pIpCxt, 
                                                        INT4 i4Type));
/*INT1 nmhGetIcmpInMsgs PROTO ((UINT4 *));
INT1 nmhGetIcmpInErrors PROTO ((UINT4 *));
INT1 nmhGetIcmpOutMsgs PROTO ((UINT4 *));
INT1 nmhGetIcmpOutErrors PROTO ((UINT4 *));
INT1 nmhGetIcmpOutSrcQuenchs PROTO ((UINT4 *));
INT1 nmhGetIcmpInEchos PROTO ((UINT4 *));
INT1 nmhGetIcmpInEchoReps PROTO ((UINT4 *));
INT1 nmhGetIcmpInTimestamps PROTO ((UINT4 *));
INT1 nmhGetIcmpInTimestampReps PROTO ((UINT4 *));
INT1 nmhGetIcmpInAddrMasks PROTO ((UINT4 *));
INT1 nmhGetIcmpInAddrMaskReps PROTO ((UINT4 *));
INT1 nmhGetIcmpInDestUnreachs PROTO ((UINT4 *));
INT1 nmhGetIcmpInTimeExcds PROTO ((UINT4 *));
INT1 nmhGetIcmpInParmProbs PROTO ((UINT4 *));
INT1 nmhGetIcmpInSrcQuenchs PROTO ((UINT4 *));
INT1 nmhGetIcmpInRedirects PROTO ((UINT4 *));
INT1 nmhGetIcmpOutDestUnreachs PROTO ((UINT4 *));
INT1 nmhGetIcmpOutTimeExcds PROTO ((UINT4 *));
INT1 nmhGetIcmpOutParmProbs PROTO ((UINT4 *));
INT1 nmhGetIcmpOutRedirects PROTO ((UINT4 *));
INT1 nmhGetIcmpOutEchos PROTO ((UINT4 *));
INT1 nmhGetIcmpOutEchoReps PROTO ((UINT4 *));
INT1 nmhGetIcmpOutTimestamps PROTO ((UINT4 *));
INT1 nmhGetIcmpOutAddrMasks PROTO ((UINT4 *));
INT1 nmhGetIcmpOutAddrMaskReps PROTO ((UINT4 *));
INT1 nmhGetIcmpOutTimestampReps PROTO ((UINT4 *));
*/
