/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: icmptyps.h,v 1.5 2009/10/12 15:18:25 prabuc Exp $
 *
 * Description: Header file for ICMP module.Contains message
 *              types,codes,stat structures etc.
 *******************************************************************/
#ifndef   __IP_ICMP_H__
#define   __IP_ICMP_H__

/* DEFINITIONS */
/* ----------- */
/* Message types */
#define   ICMP_ECHO_REPLY              0    /* Echo Reply              */
#define   ICMP_REDIRECT                5    /* Redirect                */
#define   ICMP_ECHO                    8    /* Echo Request            */
#define   ICMP_ROUTER_ADVT             9    /* Router Advertisement    */
#define   ICMP_ROUTER_SOLCT           10    /* Router Solicitation     */
#define   ICMP_TIMESTAMP              13    /* Timestamp               */
#define   ICMP_TIME_REPLY             14    /* Timestamp Reply         */
#define   ICMP_MASK_RQST              17    /* Information Request     */
#define   ICMP_MASK_REPLY             18    /* Information Reply       */

#define   ICMP_DOMAIN_NAME_REQUEST    37    /* Domain Name Request     */
#define   ICMP_DOMAIN_NAME_REPLY      38    /* Domain Name Reply       */
#define   ICMP_SECURITY_FAILURE       40    /* Security Failure        */
#define   ICMP_HDR_LEN                 8    /* Length of ICMP header on
                                             * the net 
                                             */

/* Destination Unreachable codes */
#define   ICMP_ROUTE_FAIL       5    /* Source route failed             */

/* Time Exceeded codes */
#define   ICMP_FRAG_EXCEED      1    /* Fragment reassembly time exceeded */

/* Redirect message codes */
#define   ICMP_REDR_NET         0    /* Redirect for the network          */
#define   ICMP_REDR_HOST        1    /* Redirect for the host             */
#define   ICMP_REDR_TOS         2    /* Redirect for TOS, or-ed with prev */

/* Parameter problem message codes */
#define   ICMP_PARAM_ERR_AT_PTR         0
#define   ICMP_PARAM_PTR_NOT_PRESENT    1

/* ICMP Domain Name Messages */
#define   ICMP_TTL_FIELD_SIZE     4
#define   ICMP_LABEL_LENGTH       1
#define   ICMP_TYPES             41


/*
 * ICMP should send as much of the original datagram as possible without
 * the length of the ICMP datagram exceeding 576 bytes.
 *     Refer RFC1812, Section 4.3.2.3
 */
#define   ICMP_MAX_DATA    576        /* Max data bytes in ICMP data portion */

/* The following Macro finds the IP_PROTOCOL field in the IP_HEADER of
 * an error packet sent through ICMP.
 */
#define   ICMP_MAX_DOMAIN_NAME_SIZE        256
#define   ICMP_DEFAULT_TIME_TO_LIVE        600

#define   ICMP_IP_PROTOCOL                
#define   ICMP_ECHO_REPLY_ENABLE             1
#define   ICMP_ECHO_REPLY_DISABLE            2

#define   ICMP_NMASK_REPLY_ENABLE            1
#define   ICMP_NMASK_REPLY_DISABLE           2

#define   ICMP_TIME_REPLY_ENABLE             1
#define   ICMP_TIME_REPLY_DISABLE            2

#define   ICMP_SEND_REDIRECT_ENABLE          1
#define   ICMP_SEND_REDIRECT_DISABLE         2

#define   ICMP_SEND_UNREACHABLE_ENABLE       1
#define   ICMP_SEND_UNREACHABLE_DISABLE      2

#define   ICMP_SEND_SEC_FAILURE_ENABLE       1
#define   ICMP_SEND_SEC_FAILURE_DISABLE      2

#define   ICMP_RECV_SEC_FAILURE_ENABLE       1
#define   ICMP_RECV_SEC_FAILURE_DISABLE      2

#define   ICMP_DIRECTQUERY_ENABLE            1
#define   ICMP_DIRECTQUERY_DISABLE           2
#define   ICMP_DELAY_FACTOR                  3
#define   ICMP_ERRTYPE_HDR_LEN               8
#define   ICMP_SUBNET_MASK_OFF               8
#define   ICMP_PKT_CKSUM_OFF                 2

#define   ICMP_MAX_FS_DOMAIN_NAME_LEN        63

/* MACROS */
/* ------ */
#define   ICMP_MOVE_TO_ICMP_START(pBuf)\
          IP_PKT_MOVE_TO_DATA(pBuf)

#define   ICMP_MOVE_TO_DATA(pBuf)   \
          IP_BUF_MOVE_VALID_OFFSET (pBuf,ICMP_ERRTYPE_HDR_LEN)

#define   ICMP_MOVE_BACK_TO(pBuf, Offset) \
          CRU_BUF_Prepend_BufChain((pBuf), NULL, (Offset))

#define   ICMP_MOVE_FORW_TO(pBuf, Offset) \
          IP_BUF_MOVE_VALID_OFFSET((pBuf), (Offset))

#define   ICMP_MOVE_BACK_TO_LENGTH_FIELD(pBuf)\
          CRU_BMC_Set_WriteOffset((pBuf), 0)

#define   ICMP_TIMESTAMP_OFF   (ICMP_HDR_LEN)    /* Timestamp begins at data */

#define   ICMP_PKT_ASSIGN_CKSUM(pBuf,i2Cksum)\
          IP_BMC_ASSIGN_2_BYTE (pBuf,ICMP_PKT_CKSUM_OFF,i2Cksum)

#define   ICMP_MIN(x,y)   ((x) <= (y) ? (x) : (y))

/* Added for PMTU_DISCOVERY */
#define   ICMP_NH_MTU(Icmp)   Icmp.args.SecFailure.u2Pointer


/* DATA STRUCTURES */
/* --------------- */
typedef struct _IcmpHeader
{
    INT1       i1Type;
    INT1       i1Code;
    UINT2      u2Cksum;
    tIcmpArgs  args;
} tIcmpHdr;

/* ICMP errors */

typedef struct
{
    tIpCxt    *pIpCxt;   /* Context back pointer */
    UINT4  u4Cksum;  /* ICMP Checksum errors */
    UINT4  u4Loop;   /* No ICMP in response to an ICMP */
    UINT4  u4Bcast;  /* Ignore broadcast ICMPs */
    UINT4  u4Type;   /* Bad types */
} t_ICMP_ERR;


typedef struct
{
    tIpCxt    *pIpCxt;   /* Context back pointer */
    UINT4  u4In[ICMP_TYPES + 1];   /* ICMP input stats by type */
    UINT4  u4Out[ICMP_TYPES + 1];  /* ICMP output stats by type */
} t_ICMP_STATS;

typedef struct
{
    tIpCxt    *pIpCxt;   /* Context back pointer */
    INT1  i1Echo_reply;
    INT1  i1Nmask_reply;
    INT1  i1Time_reply;
    INT1  i1Send_redirect;
    INT1  i1Send_unreachable;
    INT1  i1SendSecurityFailure;
    INT1  i1RecvSecurityFailure;
    INT1  i1DirectQueryStatus;
} t_ICMP_CFG;

#endif          /*__IP_ICMP_H__*/
