/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifextn.h,v 1.4 2011/10/25 09:53:34 siva Exp $
 *
 * Description: Extern definitions used in IP interface
 *
 *******************************************************************/
#ifndef   __IPIF_EXTN_H
#define   __IPIF_EXTN_H

extern UINT4 gu4IfaceEntryCount;

#endif /* __IPIF_EXTN_H */
