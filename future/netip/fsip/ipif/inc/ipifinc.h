/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifinc.h,v 1.6 2016/02/27 10:15:43 siva Exp $
 *
 * Description: Common includes of IPIF module
 *
 *******************************************************************/
#ifndef   __IPIF_INC_H
#define   __IPIF_INC_H

#include "lr.h"
#include "cruport.h"
#include "tmoport.h"
#include "cfa.h"
#include "ip.h"
#include "ipport.h"
#include "iptmrdfs.h"
#include "ippdudfs.h"
#include "ipifdfs.h"
#include "ipifrtif.h"
#include "ipifutls.h"
#include "ipsndfs.h"
#include "other.h"
#include "ipreg.h"
#include "ipflttyp.h"
#include "snmctdfs.h"
#include "ipprotos.h"
#include "ipprmdfs.h"
#include "icmptyps.h"
#include "irdpdefn.h"
#include "irdptdfs.h"
#include "irdpprot.h"
#include "ipcfaif.h"
#include "ipifextn.h"
#include "ipcidrcf.h"
#include "ipextn.h"
#include "iptdfs.h"
#include "ipglob.h"
#include "cfa.h"
#include "snmccons.h"

#ifdef NPAPI_WANTED
#include "ipnp.h"
#endif  /* NPAPI_WANTED */
#include "iptrace.h"

#include "ipvx.h"

#endif /* __IPIF_INC_H */
