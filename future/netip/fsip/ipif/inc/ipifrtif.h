/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifrtif.h,v 1.3 2007/02/01 14:57:57 iss Exp $
 *
 * Description: Routing/Forwarding related data structures and 
 *              constants
 *
 *******************************************************************/
#ifndef   __IP_IPRT_H__
#define   __IP_IPRT_H__

#define   IP_DEF_METRIC    -1

/*
 * These are the values taken by u2RtType field of tRtInfo.
 * They have been decided by the values specified in the Static Route table
 * of FutureIP MIB.
 */
#define   IP_STRT_INVALID    0x0a    /*  this is used for deletion */

/* Return values for broadcast verify procedure */

#define   IP_GET_A_COPY    0x01
#define   IP_FORWARD       0x02

#define   IP_NOT_LOCALLY_GENERATED    FALSE

#define   IP_DUPLICATE_DATA            TRUE    /* used for IP dup msg */

#endif          /*__IP_IPRT_H__*/
