/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifutls.h,v 1.5 2011/10/25 09:53:34 siva Exp $
 *
 * Description: The header file for IP interface utilities.
 *
 *******************************************************************/
#ifndef   __IP_IPIF_UTLS_H__
#define   __IP_IPIF_UTLS_H__

/*
 * These are the bit maps for the interface state change notification to the
 * applications that have registered with IP
 */

#define   SUBNET_MASK         0x00000008
#define   LOCAL_BCAST_ADDR    0x00000010
#define   IFACE_MTU           0x00000040

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/

UINT1     IpifGetIfOperStatus (UINT4 u4Port);
UINT1     IpifGetIfAdminStatus (UINT4 u4Port);
VOID      IpifGetIfId (UINT2 u2Port, tIP_INTERFACE * pIfId);
INT1      IpifGetFirstConfigPort (UINT2 *pu2Port);

#endif /* __IP_IPIF_UTLS_H__ */
