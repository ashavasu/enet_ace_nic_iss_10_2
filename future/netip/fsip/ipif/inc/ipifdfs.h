/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifdfs.h,v 1.9 2010/07/27 07:22:25 prabuc Exp $
 *
 * Description: The header file for IP interface module.
 *
 *******************************************************************/

#ifndef   __IP_IPIF_H__
#define   __IP_IPIF_H__

#define   IP_DEF_NET_MASK                  0xffffffff
#define   IP_CLASS_A_DEF_MASK              0xff000000
#define   IP_CLASS_A_DEF_BROADCAST_MASK      0xffffff
#define   IP_CLASS_B_DEF_MASK              0xffff0000
#define   IP_CLASS_B_DEF_BROADCAST_MASK        0xffff
#define   IP_CLASS_C_DEF_MASK              0xffffff00
#define   IP_CLASS_C_DEF_BROADCAST_MASK          0xff

#define   IPIF_MIN_REASM_SIZE                    1024

/* Allow reassembly of 20k size-- from ip_config_iface */
#define   IPIF_DEF_REASM_SIZE             20480

#define   IPIF_MAX_REASM_SIZE             33280

#define   IPIF_DEF_MTU                     1500
#define   IPIF_MAX_MTU                     2080

#define   IPIF_MAX_FILT_LISTS                99

#define   ETHERNET_IFACE                      0
#define   X25_IFACE                           1
#define   FR_IFACE                            2
#define   MAX_IFACE_TYPES                     3

#define   IPFORWARD_ENABLE                    1
#define   IPFORWARD_DISABLE                   2

#define   ICMP_REDIRECT_ENABLE                1
#define   ICMP_REDIRECT_DISABLE               2

#define   IPIF_DRTD_BCAST_FWD_ENABLE          1
#define   IPIF_DRTD_BCAST_FWD_DISABLE         2

#define   IPIF_VIRTUAL_GROUP_INDEX            0
#define   IPIF_NOTA_GROUP                     0
#define   IPIF_GROUP                          1

#define   IPIF_IF_INDEX_MASK             0xefff

#define   IPIF_GRP_END_OF_TAB            0xffff
#define   IPIF_IF_END_OF_TAB             0xffff

#define   IPIF_NONGRP_INDEX                   0
#define   IPIF_INVALID_MEMBER            0xffff

#define   IPIF_ETHER_ADDR_LEN                 6
#define   IPIF_X25_ADDR_LEN                   4

 /*constatnt declarations for ipIftable */
#define   IPIFTABADMINSTATUS                   (3)
#define   IPIFTABOPERSTATUS                    (4)
#define   IPIFTABADDR                          (5)
#define   IPIFTABNETMASK                       (6)
#define   IPIFTABBCASTADDR                     (7)
#define   IPIFTABREASMMAXSIZE                  (8)
#define   IPIFTABMTU                           (9)
#define   IPIFTABFILTERLISTNUMBER             (10)
#define   IPIFIPFORWARDENABLE                 (41)
#define   IPIFICMPREDIRECTENABLE              (42)
#define   IPIFTABUNNUMBEREDENABLEORDISABLE    (43)

/*
 * This structure indicates the list of interfaces on which a broadcast packet
 * is to be forwarded. The broadcast verify module uses this structure to
 * inform ip to send on multiple interfaces.
 */
typedef struct
{
    tIP_SLL_NODE  Link;
    UINT2         u2Port;
    UINT2         u2ReservedWord;
} t_IF_LIST_NODE;

#define   UNNUMBERED                            1
#define   NUMBERED                              0

        
#define   IPIF_IFACE_STATUS(u2Index, u1InterfaceType)   \
if (u2Index < IPIF_MAX_LOGICAL_IFACES)\
{ \
    IPFWD_DS_LOCK ();    \
    u1InterfaceType = gIpGlobalInfo.Ipif_config[u2Index].u1IfaceType; \
    IPFWD_DS_UNLOCK ();    \
}
#define IPIF_IS_UNNUMBERED(u2Index) \
(((IPIF_IFACE_STATUS(u2Index)) == UNNUMBERED) ? TRUE : FALSE)


#define  IPIF_IF_TYPE(IfId) (IfId.u1_InterfaceType)

#define  IPIF_CONFIG_ID_OF(u2Port, IfaceId)       \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        IfaceId = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_GET_IF_INDEX(u2Port, u4IfaceIndex)       \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u4IfaceIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_GET_ADMIN(u2Port, u1AdminStatus)    \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u1AdminStatus = gIpGlobalInfo.Ipif_config[u2Port].u1Admin;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_GET_OPER(u2Port, u1OperStatus) \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u1OperStatus = gIpGlobalInfo.Ipif_config[u2Port].u1Oper;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_GET_MTU(u2Port, u4MTU)\
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u4MTU = gIpGlobalInfo.Ipif_config[u2Port].u4Mtu; \
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_GET_NEXT(u2Index, u2NextIndex) \
if (u2Index < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u2NextIndex = gIpGlobalInfo.Ipif_config[u2Index].u2Next;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_GET_ROUTING_PROTOCOL(u2Port, u2RtProtocol)\
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u2RtProtocol = gIpGlobalInfo.Ipif_config[(u2Port)].u2Routing_Protocol;\
        IPFWD_DS_UNLOCK ();\
}


#define  IPIF_CONFIG_GET_SUBREF (u2Index, u2SubRef) \
if (u2Index < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u2SubRef = gIpGlobalInfo.Ipif_config[u2Index].InterfaceId.u2_SubReferenceNum;\
        IPFWD_DS_UNLOCK ();\
}

#define IPIF_CONFIG_GET_IPFORWARDING_ENABLE(u2Index, u1IpFwdEnable) \
if (u2Index < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u1IpFwdEnable = gIpGlobalInfo.Ipif_config[u2Index].u1IpForwardingEnable;\
        IPFWD_DS_UNLOCK ();\
}

#define IPIF_CONFIG_GET_DIRECTED_BCAST_FWDING_ENABLE(u2Port, u1IpDrtBcastFwdEnable) \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
       IPFWD_DS_LOCK ();\
       u1IpDrtBcastFwdEnable = gIpGlobalInfo.Ipif_config[u2Port].u1IpDrtdBcastFwdingEnable;\
       IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_GET_ENCAP_TYPE(u2Port, u1Encap)\
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        u1Encap = gIpGlobalInfo.Ipif_config[u2Port].u1EncapType;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_ADMIN_STATUS(u2Port, u1AdminStatus)    \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u1Admin = u1AdminStatus;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_OPER_STATUS(u2Port, u1OperStatus)    \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u1Oper = u1OperStatus;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_MTU(u2Port, u4MTU)    \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u4Mtu = u4MTU;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_SET_IFACE_STATUS(u2Port, u1InterfaceType)    \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u1IfaceType = u1InterfaceType;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_NEXT_OF(u2Port, u2NextPort)    \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u2Next = u2NextPort;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_REASM_MAX_SIZE(u2Port, u4Reasmmaxsize)    \
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u4Reasm_max_size = u4Reasmmaxsize;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_DIRECTED_BCAST_FWDING_ENABLE(u2Port, u1IpDrtdBcastFwdEnable)\
if (u2Port < IPIF_MAX_LOGICAL_IFACES)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u1IpDrtdBcastFwdingEnable = u1IpDrtdBcastFwdEnable;\
        IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_PROXY_ARP_ADMIN_STATUS(u2Port, u1ProxyArpCfgVal)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u1ProxyArpAdminStatus = u1ProxyArpCfgVal;\
        IPFWD_DS_UNLOCK ();\
}

#define IPIF_CONFIG_GET_PROXY_ARP_ADMIN_STATUS(u2Port, u1ProxyArpCfgVal) \
{\
       IPFWD_DS_LOCK ();\
       u1ProxyArpCfgVal = gIpGlobalInfo.Ipif_config[u2Port].u1ProxyArpAdminStatus;\
       IPFWD_DS_UNLOCK ();\
}

#define  IPIF_CONFIG_SET_LOCAL_PROXY_ARP_STATUS(u2Port, u1LocalProxyArpCfgVal)\
{\
        IPFWD_DS_LOCK ();\
        gIpGlobalInfo.Ipif_config[u2Port].u1LocalProxyArpStatus = u1LocalProxyArpCfgVal;\
        IPFWD_DS_UNLOCK ();\
}

#define IPIF_CONFIG_GET_LOCAL_PROXY_ARP_STATUS(u2Port, u1LocalProxyArpCfgVal) \
{\
       IPFWD_DS_LOCK ();\
       u1LocalProxyArpCfgVal = gIpGlobalInfo.Ipif_config[u2Port].u1LocalProxyArpStatus;\
       IPFWD_DS_UNLOCK ();\
}

#define  IPIF_IF_INDEX_TO_GLB(u2Index)   (u2Index)

#define  IPIF_PHYS_ADDR(u2Port)    \
         (gIpGlobalInfo.Ipif_glbtab[(u2Port)].pIf->au1PhysAddr)
        
#define  IPIF_GLBTAB_REASM_MAX_SIZE(u2Port)\
         (gIpGlobalInfo.Ipif_glbtab[u2Port].pIf->u4Reasm_max_size)\

#define  IPIF_PHYS_ADDR_LENGTH(u2Port)   \
         (gIpGlobalInfo.Ipif_glbtab[(u2Port)].pIf->u4PhysLength)

#define  IPIF_GLBTAB_IFCONFIG_OF(u2Index)\
         (gIpGlobalInfo.Ipif_glbtab[u2Index].pIf)
#define  IPIF_GLBTAB_STATS_OF(u2Index)   (gIpGlobalInfo.Ipif_glbtab[u2Index].pStat)


#define IPIF_IN_RCVS(u2Port)       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs)
#define IPIF_IN_LEN_ERR(u2Port)    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_len_err)
#define IPIF_IN_CKSUM_ERR(u2Port)  (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_cksum_err)
#define IPIF_IN_HDR_ERR(u2Port)    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_hdr_err)
#define IPIF_IN_VER_ERR(u2Port)    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_ver_err)
#define IPIF_IN_TTL_ERR(u2Port)    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_ttl_err)
#define IPIF_IN_ADDR_ERR(u2Port)   (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_addr_err)
#define IPIF_IN_OPT_ERR(u2Port)    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_opt_err)
#define IPIF_IN_DELIVERIES(u2Port) (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries)
#define IPIF_OUT_PKTS(u2Port)      (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_pkts)
#define IPIF_OUT_REQUESTS(u2Port)  (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests)
#define IPIF_OUT_NO_ROUTES(u2Port) (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_no_routes)
#define IPIF_OUT_GEN_ERR(u2Port)   (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_gen_err)
#define IPIF_OUT_FRAG_ERR(u2Port)  (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_frag_err)
#define IPIF_TOTAL_FILTD(u2Port)   (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Total_filtered)
#define IPIF_FORW_ATTEMPTS(u2Port) (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts)
#define IPIF_BAD_PROTO(u2Port)     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Bad_proto)
#define IPIF_REASM_REQS(u2Port)    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_reqs)
#define IPIF_REASM_OKS(u2Port)     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_oks)
#define IPIF_REASM_FAILS(u2Port)   (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_fails)
#define IPIF_REASM_TIMEOUT(u2Port) (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_timeout)
#define IPIF_OUT_FRAG_PKTS(u2Port) (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_frag_pkts)
#define IPIF_FRAG_FAILS(u2Port)    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_fails)
#define IPIF_FRAG_CREATES(u2Port)      \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_creates)
#define IPIF_GLBTAB_INMCST_PKT(u2Port) \
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts)
#define  IPIF_GLBTAB_INBCST_PKT(u2Port) \
                     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts)
#define  IPIF_GLBTAB_OUTMCST_PKT(u2Port) \
                     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMulticastPkts)
#define  IPIF_GLBTAB_OUTBCST_PKT(u2Port) \
                     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts)

#define  IPIF_FILTER_LIST(u2Port)\
         (gIpGlobalInfo.Ipif_glbtab[(u2Port)].pIf->u2Filter_list)


#define IPIF_ICMPREDIRECT_ENABLE(u2Index) (gIpGlobalInfo.Ipif_glbtab[u2Index].pIf->u1IcmpRedirectEnable)
        
#define IPIF_SEND_FN(u2Rt_port,pBuf,i2Len,u4Gw,u1Flag)\
        Iface_fn[IPIF_IF_TYPE (gIpGlobalInfo.Ipif_glbtab[u2Rt_port].InterfaceId)].pSend(pBuf,\
        i2Len,u4Gw,u1Flag)

#define IPIF_INC_IN_RCVS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES )) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_rcvs++);\
	}

#define IPIF_INC_IN_LEN_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES )) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_len_err++);\
	}

#define IPIF_INC_IN_CKSUM_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_cksum_err++);\
	}

#define IPIF_INC_IN_HDR_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_hdr_err++);\
	}

#define IPIF_INC_IN_VER_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_ver_err++);\
    }
#define IPIF_INC_IN_TTL_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_ttl_err++);\
	}

#define IPIF_INC_IN_ADDR_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_addr_err++);\
	}

#define IPIF_INC_IN_OPT_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_opt_err++);\
	}

#define IPIF_INC_IN_IFACE_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
       (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_iface_err++);\
	}

#define IPIF_INC_IN_DELIVERIES(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
      (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_deliveries++);\
	}

#define IPIF_INC_IN_BCASTS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
        (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4In_bcasts++);\
    }

#define IPIF_INC_OUT_PKTS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_pkts++);\
	}

#define IPIF_INC_OUT_REQUESTS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_requests++);\
	}

#define IPIF_INC_OUT_NO_ROUTES(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_no_routes++);\
	}

#define IPIF_INC_OUT_GEN_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_gen_err++);\
	}

#define IPIF_INC_OUT_FRAG_ERR(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_frag_err++);\
	}

#define IPIF_INC_TOTAL_FILTERED(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Total_filtered++);\
	}

#define IPIF_INC_FORW_ATTEMPTS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Forw_attempts++);\
	}

#define IPIF_INC_BAD_PROTO(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Bad_proto++);\
	}

#define IPIF_INC_REASM_REQS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_reqs++);\
	}

#define IPIF_INC_REASM_OKS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_oks++);\
	}

#define IPIF_INC_REASM_FAILS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_fails++);\
	}

#define IPIF_INC_REASM_TIMEOUT(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Reasm_timeout++);\
	}

#define IPIF_INC_OUT_FRAG_PKTS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Out_frag_pkts++);\
	}

#define IPIF_INC_FRAG_FAILS(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_fails++);\
	}

#define IPIF_INC_FRAG_CREATES(u2Port)\
    if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
    (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4Frag_creates++);\
	}

    /* Routines Added to support the fsip_mib 17/11/98 */

#define IPIF_INC_INMCST_PKT(u2Port)\
     if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4InMulticastPkts++);\
	}

#define IPIF_INC_OUTMCST_PKT(u2Port)\
     if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutMulticastPkts++);\
	}

#define IPIF_INC_OUTBCST_PKT(u2Port)\
     if (u2Port < (IPIF_MAX_LOGICAL_IFACES)) \
    {\
		if (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat != NULL)\
     (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat->u4OutBroadcastPkts++);\
	}

#define GET_BCASTADDR_FROM_NETADDR(u4Addr, u4Mask) u4Addr | ~u4Mask

#define   IPIF_LOGICAL_IFACES_SCAN(u2Index)                         \
          for (u2Index=gIpGlobalInfo.u2FirstIpifIndex; ((u2Index != IPIF_INVALID_INDEX) && (u2Index < IPIF_MAX_LOGICAL_IFACES)); \
			            u2Index = gIpGlobalInfo.Ipif_config[u2Index].u2Next)

#endif          /*__IP_IPIF_H__*/
