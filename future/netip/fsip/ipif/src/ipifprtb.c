/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifprtb.c,v 1.8 2013/03/28 12:29:39 siva Exp $
 *
 * Description:IP Interface routines                        
 *             -ing tables.          
 *
 *******************************************************************/

#include "ipifinc.h"
#include "arp.h"

/*-------------------------------------------------------------------+
 * Function           : ipifipIfaceConfigInit
 *
 * Input(s)           : pIf
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 * Initialises Interface information.
 *
+-------------------------------------------------------------------*/
VOID
ipifipIfaceConfigInit (UINT2 u2Port)
{
    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    gIpGlobalInfo.Ipif_config[u2Port].u4Mtu = IPIF_DEF_MTU;
    gIpGlobalInfo.Ipif_config[u2Port].u2Routing_Protocol =
        IP_ROUTING_PROTO_NONE;
    gIpGlobalInfo.Ipif_config[u2Port].u1IpDrtdBcastFwdingEnable =
        IPIF_DRTD_BCAST_FWD_DISABLE;
    gIpGlobalInfo.Ipif_config[u2Port].u1IfaceType = UNNUMBERED;    /* Default value. */
    gIpGlobalInfo.Ipif_config[u2Port].u1Oper = IPIF_OPER_DISABLE;    /* Default value. */
    gIpGlobalInfo.Ipif_config[u2Port].u1Admin = IPIF_ADMIN_DISABLE;    /* Default value. */
    gIpGlobalInfo.Ipif_config[u2Port].u1ProxyArpAdminStatus = ARP_PROXY_DISABLE;    /* Default value. */
    gIpGlobalInfo.Ipif_config[u2Port].u1LocalProxyArpStatus = ARP_PROXY_DISABLE;    /* Default value. */
    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    gIpGlobalInfo.Ipif_glbtab[u2Port].pIf->u2Filter_list = 0;
    gIpGlobalInfo.Ipif_glbtab[u2Port].pIf->u1IcmpRedirectEnable =
        ICMP_REDIRECT_ENABLE;
    gIpGlobalInfo.Ipif_glbtab[u2Port].pIf->u4Reasm_max_size =
        IPIF_DEF_REASM_SIZE;
}

/*-------------------------------------------------------------------+
 * Function           : ipifipIfaceAllocError
 *
 * Input(s)           : u2Index
 *
 * Output(s)          : None.
 *
 * Returns            : FAILURE
 *
 * Action :
 * Frees any allocated buffer.
 *
+-------------------------------------------------------------------*/
INT4
ipifipIfaceAllocError (UINT2 u2Index)
{
    if (IPIF_GLBTAB_IFCONFIG_OF (u2Index) != NULL)
    {
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpIfInfoPoolId,
                            (UINT1 *) gIpGlobalInfo.Ipif_glbtab[u2Index].pIf);
        gIpGlobalInfo.Ipif_glbtab[u2Index].pIf = NULL;
    }

    if (IPIF_GLBTAB_STATS_OF (u2Index) != NULL)
    {
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpIfStatPoolId,
                            (UINT1 *) gIpGlobalInfo.Ipif_glbtab[u2Index].pStat);
        gIpGlobalInfo.Ipif_glbtab[u2Index].pStat = NULL;
    }

    return FAILURE;
}
