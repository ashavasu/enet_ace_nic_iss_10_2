/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifmain.c,v 1.12 2013/07/04 13:12:00 siva Exp $
 *
 * Description: IP interface miscellanous routines.
 *
 *******************************************************************/
/*****************************************************************************/
/****             $$TRACE_MODULE_NAME     = IP_FWD                        ****/
/****             $$TRACE_SUB_MODULE_NAME = IF                            ****/
/*****************************************************************************/
#include "ipifinc.h"
#include "ipvx.h"

UINT2               u2Start_index = 0;

/********************************************************************/
/****      $$TRACE_PROCEDURE_NAME  = ipif_init                   ****/
/****      $$TRACE_PROCEDURE_LEVEL = MAIN                        ****/
/*-------------------------------------------------------------------+
 * Function           : ipif_init
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Initializes IP Interface data structures.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

VOID
ipif_init (VOID)
#else

VOID
ipif_init ()
#endif
{
    UINT2               u2Index = 0;

   /**** $$TRACE_LOG (ENTRY, "Ip interface initialization\n"); ****/

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    gIpGlobalInfo.u2FirstIpifIndex = IPIF_INVALID_INDEX;

    for (u2Index = 0; u2Index < IPIF_MAX_LOGICAL_IFACES; u2Index++)
    {
        /* Initialize Iface table */
        IPIF_GLBTAB_IFCONFIG_OF (u2Index) = NULL;
        IPIF_GLBTAB_STATS_OF (u2Index) = NULL;
        gIpGlobalInfo.Ipif_config[u2Index].InterfaceId.u1_InterfaceType =
            IP_IF_TYPE_INVALID;
        gIpGlobalInfo.Ipif_config[u2Index].u2Next = IPIF_INVALID_INDEX;
        gIpGlobalInfo.Ipif_config[u2Index].InterfaceId.u4IfIndex =
            (UINT4) IPIF_INVALID_INDEX;
        gIpGlobalInfo.Ipif_config[u2Index].u1Ipv4EnableStatus =
            IPVX_IPV4_ENABLE_STATUS_UP;
        gIpGlobalInfo.Ipif_config[u2Index].pIpCxt = NULL;
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    u2Start_index = IPIF_MAX_LOGICAL_IFACES;

   /**** $$TRACE_LOG (EXIT, "Initialization over\n"); ****/
}

/********************************************************************/
/****   $$TRACE_PROCEDURE_NAME  = ipif_get_tab_index             ****/
/****   $$TRACE_PROCEDURE_LEVEL = LOW                            ****/
/*-------------------------------------------------------------------+
 * Function           : ipif_get_tab_index
 *
 * Input(s)           : InterfaceId
 *
 * Output(s)          : None.
 *
 * Returns            : Index to the iface/group record table
 *                      IP_FAILURE, if the ID is not found.
 *
 * Action :
 *
 * This routine is called from IP after receiving a packet from
 * LL.
 * The returned index is used throughout the code so that
 * the reference and conversions are faster.
 * The return value also indicates if it is an index into group table or
 * iface table.
+-------------------------------------------------------------------*/
#ifdef __STDC__

UINT4
ipif_get_tab_index (tIP_INTERFACE InterfaceId)
#else

UINT4
ipif_get_tab_index (InterfaceId)
     tIP_INTERFACE       InterfaceId;    /* Actual interface ID                 */
#endif
{
    UINT2               u2TabIndex = 0;
    tIfId               IfId;

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2TabIndex)
    {
        IfId = gIpGlobalInfo.Ipif_config[u2TabIndex].InterfaceId;

        if (IfId.u1_InterfaceType != IP_IF_TYPE_INVALID)
        {
            if ((IfId.u4IfIndex == InterfaceId.u4IfIndex) &&
                (IfId.u2_SubReferenceNum == InterfaceId.u2_SubReferenceNum))
            {
                /* UnLock */
                IPFWD_DS_UNLOCK ();
                return u2TabIndex;
            }
        }
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();
    return IPIF_INVALID_INDEX;
}

/*-------------------------------------------------------------------+
 * Function           : ipif_is_our_address
 *
 * Input(s)           : u4Addr
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE if IP_SUCCESS
 *                      FALSE otherwise.
 *
 * Action :
 *
 * Takes an IP address and finds out if the address belongs to
 * our host. All the address nodes mapped to the default context
 * are scanned for this purpose.
 *
+-------------------------------------------------------------------*/
INT4
ipif_is_our_address (UINT4 u4Addr)
{
    return (ipif_is_our_address_InCxt (IP_DEFAULT_CONTEXT, u4Addr));
}

/********************************************************************/
/****    $$TRACE_PROCEDURE_NAME  = ipif_is_our_address           ****/
/****    $$TRACE_PROCEDURE_LEVEL = INTMD                         ****/
/*-------------------------------------------------------------------+
 * Function           : ipif_is_our_address_InCxt
 *
 * Input(s)           : u4ContextId, u4Addr
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE if IP_SUCCESS
 *                      FALSE otherwise.
 *
 * Action :
 *
 * Takes an IP address and finds out if the address belongs to
 * our host. All the address nodes mapped to the specified context 
 * are scanned for this purpose.
 *
+-------------------------------------------------------------------*/
INT4
ipif_is_our_address_InCxt (UINT4 u4ContextId, UINT4 u4Addr)
{
    return (IpifIsOurAddressInCxt (u4ContextId, u4Addr));
}

/*-------------------------------------------------------------------+
 * Function           : ipif_is_ok_to_forward
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE if it can be forwarded
 *                      FALSE otherwise.
 *
 * Action :
 * Checks the validity of a given interface. And decides if
 * it is active.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ipif_is_ok_to_forward (UINT2 u2Port)
#else

INT4
ipif_is_ok_to_forward (u2Port)
     UINT2               u2Port;
#endif
{

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

   /**** $$TRACE_LOG (ENTRY, "Checking validity of port %dAdmin %d oper %d\n",
      u2Port,IPIF_CONFIG_ADMIN_OF(u2Port) ,
      IPIF_CONFIG_OPER_OF(u2Port)) ; ****/
    if (u2Port >= IPIF_MAX_LOGICAL_IFACES)
    {
        /* Unlock it */
        IPFWD_DS_UNLOCK ();
        return FALSE;
    }

    /* Oper is taken care inside admin */
    if ((gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType
         == IP_IF_TYPE_INVALID) ||
        (gIpGlobalInfo.Ipif_config[u2Port].u1Admin != IPIF_ADMIN_ENABLE) ||
        (gIpGlobalInfo.Ipif_config[u2Port].u1Oper != IPIF_OPER_ENABLE) ||
        (gIpGlobalInfo.Ipif_config[u2Port].u1Ipv4EnableStatus !=
         IPVX_IPV4_ENABLE_STATUS_UP))
    {

      /**** $$TRACE_LOG (EXIT, "Admin or Oper is Down\n"); ****/
        /* Unlock it */
        IPFWD_DS_UNLOCK ();
        return FALSE;
    }
    else
    {
      /**** $$TRACE_LOG (EXIT, "It can be forwarded on this port\n"); ****/
        /* Unlock it */
        IPFWD_DS_UNLOCK ();
        return TRUE;
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : ipif_get_handle_from_addr
 *
 * Input(s)           : IP address
 *
 * Output(s)          : None
 *
 * Returns            : IP handle, IP_FAILURE
 *
 * Action :
 *   Returns the IP handle (index into the ipif_glbtab) of the record
 *   containing IP address, u4Addr.
 *
+-------------------------------------------------------------------*/
INT4
ipif_get_handle_from_addr_InCxt (UINT4 u4ContextId, UINT4 u4Addr)
{
    return (IpGetPortFromAddrInCxt (u4ContextId, u4Addr));
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpifGetIfOperStatus
 *
 * Input(s)           : u2Port - Logical interface index
 *
 * Output(s)          : None
 *
 * Returns            : NULL              if invalid port
 *                      IPIF_OPER_ENABLE  if up
 *                      IPIF_OPER_DISABLE if down
 *
 * Action :
 *   Returns the operational status of the logical interface provided.
 *
+-------------------------------------------------------------------*/
UINT1
IpifGetIfOperStatus (UINT4 u4Port)
{
    UINT1               u1OperStatus = 0;

    IPIF_CONFIG_GET_OPER (u4Port, u1OperStatus);

    return u1OperStatus;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpifGetIfAdminStatus
 *
 * Input(s)           : u2Port - Logical interface index
 *
 * Output(s)          : None
 *
 * Returns            : NULL               if invalid port
 *                      IPIF_ADMIN_ENABLE  if up
 *                      IPIF_ADMIN_DISABLE if down
 *                      IPIF_ADMIN_INVALID if invalid
 *
 * Action :
 *   Returns the admin status of the logical interface provided.
 *
+-------------------------------------------------------------------*/

UINT1
IpifGetIfAdminStatus (UINT4 u4Port)
{
    UINT1               u1AdminStatus = 0;

    IPIF_CONFIG_GET_ADMIN (u4Port, u1AdminStatus);
    return u1AdminStatus;
}

/********************************************************************/
/****     $$TRACE_PROCEDURE_NAME  = IpifGetIfId                  ****/
/****     $$TRACE_PROCEDURE_LEVEL = LOW                          ****/
/*-------------------------------------------------------------------+
 *
 * Function           : IpifGetIfId
 *
 * Input(s)           : u2Port - Logical interface index
 *
 * Output(s)          : Interface ID
 *
 * Returns            : tIP_INTERFACE of the interface
 *
 * Action :
 *   Returns tIP_INTERFACE associated with the logical interface provided.
 *
+-------------------------------------------------------------------*/

#ifdef __STDC__

VOID
IpifGetIfId (UINT2 u2Port, tIP_INTERFACE * pIfId)
#else

VOID
IpifGetIfId (u2Port, pIfId)
     UINT2               u2Port;
     tIP_INTERFACE      *pIfId;
#endif
{
    if (u2Port < IPIF_MAX_LOGICAL_IFACES)
    {
        IPIF_CONFIG_ID_OF (u2Port, *pIfId);
    }
    else
    {
        IPIF_INVALIDATE_IF_ID (pIfId);
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : ipifConvertIpIfToCRU
 *
 * Input(s)           : i4IfNum, i4Subref
 
 * Output(s)          : tIP_INTERFACE
 *
 * Returns            : None
 *
 * Action :
 * Forms Interface ID from Interface number and Subreference.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
ipifConvertIpIfToCRU (INT4 i4IfNum, INT4 i4Subref, tIP_INTERFACE * pIfId)
#else
VOID
ipifConvertIpIfToCRU (i4IfNum, i4Subref, pIfId)
     INT4                i4IfNum;
     INT4                i4Subref;
     tIP_INTERFACE      *pIfId;
#endif
{
    UINT2               u2Port = 0;
    /*
     * Makefile Changes - function call has aggregate value
     *
     * Now the Interface structure is not returned, instead
     * one calls this function has to give pointer 
     * to the interface, where this function wud write the 
     * information.
     */

    if ((i4IfNum == 0) && (i4Subref == 0))
    {
        IP_GET_IF_INDEX (*pIfId) = (UINT4) i4IfNum;
        IP_GET_IFACE_SUBREF (*pIfId) = (UINT2) i4Subref;
        IPIF_INVALIDATE_IF_ID (pIfId);
        return;
    }
   /****
   $$TRACE_LOG (ENTRY, "Entry [%d] [%d]\n", u1IfNum, u2Subref);
   ****/
    IP_GET_IF_INDEX (*pIfId) = (UINT4) i4IfNum;
    IP_GET_IFACE_SUBREF (*pIfId) = (UINT2) i4Subref;

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2Port)
    {
        if (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex ==
            (UINT4) i4IfNum)
        {
            break;
        }
    }

    if ((u2Port != IPIF_INVALID_INDEX) && (u2Port < IPIF_MAX_LOGICAL_IFACES))
    {
        pIfId->u1_InterfaceType =
            gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType;
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

   /****
   $$TRACE_LOG (EXIT, "Interface-Id [%d] [%d] [%d]\n",
   pIfId->u1_InterfaceType, pIfId->u4IfIndex,
   pIfId->u2_SubReferenceNum);
   ****/
}

/*-------------------------------------------------------------------+
 *
 * Function           : ipifConvertIfIdFromCRU
 *
 * Input(s)           : value
 *
 * Output(s)          : None
 *
 * Returns            : i4IfNum
 *
 * Action :
 * Gets the IfIndex number from Interface ID.
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ipifConvertIfIdFromCRU (tIP_INTERFACE InterfaceId)
#else

INT4
ipifConvertIfIdFromCRU (InterfaceId)
     tIP_INTERFACE       InterfaceId;
#endif
{
    return ((INT4) IP_GET_IF_INDEX (InterfaceId));
}

/*-------------------------------------------------------------------+
 *
 * Function           : ipifConvertSubrefFromCRU
 *
 * Input(s)           : InterfaceId
 *
 * Output(s)          : None
 *
 * Returns            : u4SubRefNum 
 *
 * Action :
 * Gets the SubRef  number from Interface ID.
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ipifConvertSubrefFromCRU (tIP_INTERFACE InterfaceId)
#else

INT4
ipifConvertSubrefFromCRU (InterfaceId)
     tIP_INTERFACE       InterfaceId;
#endif
{
    INT4                i4SubRefNum = 0;

    i4SubRefNum = IP_GET_IFACE_SUBREF (InterfaceId);

    return i4SubRefNum;
}

#ifdef __STDC__
UINT4
IpGetIfIndexFromPort (UINT2 u2Port)
#else
UINT4
IpGetIfIndexFromPort (u2Port)
     UINT2               u2Port;
#endif
{

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    if (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType ==
        IP_IF_TYPE_INVALID)
    {
        /* Unlock it */
        IPFWD_DS_UNLOCK ();
        return 0;
    }
    else
    {
        /* Unlock it */
        IPFWD_DS_UNLOCK ();
        return (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex);
    }
}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpifGetInterfaceType                                |
*|                                                                     |
*| Description   : Gets the interface type from the port number        |  
*|                                                                     |  
*| Input         : u2Port - Logical Interface index                    |
*|                                                                     |  
*| Output        : None                                                |  
*|                                                                     |  
*| Returns       : Interface Type                                      |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
UINT1
IpifGetInterfaceType (UINT2 u2Port)
#else
UINT1
IpifGetInterfaceType (u2Port)
     UINT2               u2Port;
#endif
{

    UINT1               u1IfType = 0;
    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    u1IfType = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType;

    /* Unlock it */
    IPFWD_DS_UNLOCK ();
    return (u1IfType);
}

INT1
IpifGetFirstConfigPort (UINT2 *pu2Port)
{
    UINT2               u2Port = 0;

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2Port)
    {
        *pu2Port = u2Port;
        /* Unlock it */
        IPFWD_DS_UNLOCK ();
        return IP_SUCCESS;
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();
    return IP_FAILURE;
}

#ifdef __STDC__
INT1
IpGetNextPort (UINT2 u2Port, UINT2 *pu2Port)
#else
INT1
IpGetNextPort (u2Port, *pu2Port)
     UINT2               u2Port;
     UINT2              *pu2Port;
#endif
{
    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    *pu2Port = gIpGlobalInfo.Ipif_config[u2Port].u2Next;
    IPFWD_DS_UNLOCK ();

    if ((*pu2Port == IPIF_INVALID_INDEX) ||
        (*pu2Port == IPIF_MAX_LOGICAL_IFACES))
    {
        /* Unlock it */
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpifGetPortFromIfIndex                                |
*|                                                                     |
*| Description   : Get the port number from IfIndex.                   |  
*|                 local net.                                          |  
*|                                                                     |  
*| Input         : u2IfIndex.                                          |
*|                                                                     |  
*| Output        : u2Port of the Interface                             |  
*|                                                                     |  
*| Returns       : IP_SUCCESS / IP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
IpifGetPortFromIfIndex (UINT4 u4IfIndex, UINT2 *pu2Port)
{
    tCfaIfInfo          IfInfo;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_FAILURE)
    {
        if (IfInfo.u1BridgedIface == CFA_ENABLED)
        {
            return IP_FAILURE;
        }
    }

    *pu2Port = (UINT2) IfInfo.i4IpPort;
    if (*pu2Port < IPIF_MAX_LOGICAL_IFACES)
    {
        return IP_SUCCESS;
    }

    return IP_FAILURE;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpifGetHighIpAddressInCxt                           |
*|                                                                     |
*| Description   : Gets the highest address of the active interfaces   |
*|                 mapped to the specified context                     |
*|                                                                     |
*| Input         : None                                                |
*|                                                                     |
*| Output        : u4ContextId - ContextId                             |  
*|                 pu4IfAddress - Highest IP address                   |
*|                                                                     |
*| Returns       : IP_SUCCESS or IP_FAILURE                            |
*|                                                                     |
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
VOID
IpifGetHighIpAddressInCxt (UINT4 u4ContextId, UINT4 *pu4IfAddress)
#else
VOID
IpifGetHighIpAddressInCxt (u4ContextId, pu4IfAddress)
     UINT1               u4ContextId;
     UINT1              *pu4IfAddress;
#endif
{
    CfaIpIfGetHighestIpAddrInCxt (u4ContextId, pu4IfAddress);
}
