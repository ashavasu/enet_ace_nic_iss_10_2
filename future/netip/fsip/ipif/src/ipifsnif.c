/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipifsnif.c,v 1.15 2016/03/03 10:17:49 siva Exp $
 *
 * Description:The IP interface management functions.      
 *             Both GDT and IFTAB operations are done in    
 *             this file.                             
 *
 *******************************************************************/
#include "ipifinc.h"
#include "fsiplow.h"
#include "fsmpipcli.h"
#include "arp.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/

/**************************************************************************/

/**** $$TRACE_MODULE_NAME     = IP_FWD       ****/
/**** $$TRACE_SUB_MODULE_NAME = IF           ****/

/************************ IP IFTABLE ********************************
 * IP IFTABLE
 * INDEX u4Id             -- Logical interface ID
 * RW    u4Addr           -- IP address for this logical if
 * RW    u4Mask           -- Address Mask
 * RW    u4Bcast_addr     -- Broadcast Address for this iface
 * RW    u4Mtu            -- Maximum transttable units
 * RW    u4Reasm_max_size -- Maximum reassembly size expected
 * RW    i2Filter_list    -- Filter list associated with this
 * RW    u1Admin          -- Admin Status
 * RO    u1Oper           -- Operational Status
 * RO    u4In_pkts;
 * RO    u4In_ucast_pkts;
 * RO    u4In_nucast_pkts;
 * RO    u4In_discards;
 * RO    u4In_errors;
 * RO    u4In_unknown_protos;

 * RO    u4Out_pkts;
 * RO    u4Out_ucast_pkts;
 * RO    u4Out_nucast_pkts;
 * RO    u4Out_discards;
 * RO    u4Out_errors;
 * RO    u4Out_unknown_protos;
 *******************************************************************/

/**** $$TRACE_PROCEDURE_NAME  =  ipif_mgmt_iftab_get_entry ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
#ifdef __STDC__

UINT4
ipif_mgmt_iftab_get_entry (tIP_INTERFACE IfaceId)
#else

UINT4
ipif_mgmt_iftab_get_entry (IfaceId)
     tIP_INTERFACE       IfaceId;    /* Interface index */
#endif
{
    UINT4               i4Index = 0;
   /**** $$TRACE_LOG (ENTRY, "For (%d, %d, %d) \n",
         IP_GET_INTERFACE_TYPE(InterfaceId),
         IP_GET_INTERFACE_NUM(InterfaceId),
         IP_GET_INTERFACE_VCNUM(InterfaceId) );****/

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (i4Index)
    {
        if (gIpGlobalInfo.Ipif_config[i4Index].InterfaceId.u1_InterfaceType ==
            IP_IF_TYPE_INVALID)
        {
            continue;
        }
        if ((IP_GET_IF_INDEX (IfaceId) ==
             IP_GET_IF_INDEX (gIpGlobalInfo.Ipif_config[i4Index].InterfaceId))
            && (IP_GET_IFACE_SUBREF (IfaceId) ==
                IP_GET_IFACE_SUBREF (gIpGlobalInfo.Ipif_config[i4Index].
                                     InterfaceId)))
        {
            /* Unlock it */
            IPFWD_DS_UNLOCK ();
            return i4Index;
        }

        /* we have provided the functionality of the nacro below her only in 
         * order to avoid dead lock */
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

/**** $$TRACE_LOG (EXIT, "Not Found\n");****/
    return IPIF_INVALID_INDEX;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIpIfTable
 Input       :  The Indices
                i4IpIftabIfNum
                IpIftabSubref
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceIpIfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceIpIfTable (INT4 i4IpIftabIfNum, INT4 i4IpIftabSubref)
#else
INT1
nmhValidateIndexInstanceIpIfTable (i4IpIftabIfNum, i4IpIftabSubref)
     INT4                i4IpIftabIfNum;
     INT4                i4IpIftabSubref;
#endif
{
    tIP_INTERFACE       InterfaceId;    /* Interface index */
   /*** $$TRACE_LOG (ENTRY, "i4IpIftabIfNum = %d\n", i4IpIftabIfNum); ***/
   /*** $$TRACE_LOG (ENTRY, "IpIftabSubref = %d\n", i4IpIftabSubref); ***/

    /* Makefile Changes - function call has aggregate value */
    ipifConvertIpIfToCRU (i4IpIftabIfNum, i4IpIftabSubref, &InterfaceId);

    if (ipif_mgmt_iftab_get_entry (InterfaceId) != IPIF_INVALID_INDEX)
    {
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIpIfTable
 Input       :  The Indices
                i4IpIftabIfNum
                IpIftabSubref
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpIfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexIpIfTable (INT4 *pi4IpIftabIfNum, INT4 *pi4IpIftabSubref)
#else
INT1
nmhGetFirstIndexIpIfTable (pi4IpIftabIfNum, pi4IpIftabSubref)
     INT4               *pi4IpIftabIfNum;
     INT4               *pi4IpIftabSubref;
#endif
{
 /*** $$TRACE_LOG (ENTRY, "i4IpIftabIfNum = %d\n", *pi4IpIftabIfNum); ***/
 /*** $$TRACE_LOG (ENTRY, "IpIftabSubref = %d\n", *pi4IpIftabSubref); ***/

    if (nmhGetNextIndexIpIfTable (0, pi4IpIftabIfNum, 0,
                                  pi4IpIftabSubref) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIpIfTable
 Input       :  The Indices
                i4IpIftabIfNum
                nextIpIftabIfIndex
                IpIftabSubref
                nextIpIftabSubref
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpIfTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexIpIfTable (INT4 i4IpIftabIfNum, INT4 *pi4IpIftabIfIndex,
                          INT4 i4IpIftabSubref, INT4 *pi4NextIpIftabSubref)
#else
INT1
nmhGetNextIndexIpIfTable (i4IpIftabIfNum, pi4IpIftabIfIndex,
                          i4IpIftabSubref, pi4NextIpIftabSubref)
     INT4                i4IpIftabIfNum;
     INT4               *pi4IpIftabIfIndex;
     INT4                i4IpIftabSubref;
     INT4               *pi4NextIpIftabSubref;
#endif
{
    UINT4               u4Index = 0;
    UINT2               u2NextPort = 0;
    INT4                i4NextIndex = i4IpIftabIfNum;
    INT4                i4CurrIndex = IPIF_INVALID_INDEX;
    INT4                i4Found = FALSE;

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    UNUSED_PARAM (i4IpIftabSubref);
    IPIF_LOGICAL_IFACES_SCAN (u4Index)
    {
        i4CurrIndex =
            (INT4) IP_GET_IF_INDEX (gIpGlobalInfo.Ipif_config[u4Index].
                                    InterfaceId);

        if (i4Found == FALSE)
        {
            if (i4CurrIndex > i4IpIftabIfNum)
            {
                u2NextPort = (UINT2) u4Index;
                i4NextIndex = i4CurrIndex;
                i4Found = TRUE;
            }
        }
        else
        {
            if ((i4CurrIndex > i4IpIftabIfNum) && (i4CurrIndex < i4NextIndex))
            {
                u2NextPort = (UINT2) u4Index;
                i4NextIndex = i4CurrIndex;
            }
        }
    }

    if (i4Found == TRUE)
    {
        *pi4IpIftabIfIndex =
            (INT4) IP_GET_IF_INDEX (gIpGlobalInfo.Ipif_config[u2NextPort].
                                    InterfaceId);
        *pi4NextIpIftabSubref =
            IP_GET_IFACE_SUBREF (gIpGlobalInfo.Ipif_config[u2NextPort].
                                 InterfaceId);
        IPFWD_DS_UNLOCK ();
        return SNMP_SUCCESS;
    }
    /* Unlock it */
    IPFWD_DS_UNLOCK ();
    return SNMP_FAILURE;
}

   /* IP Interface table */
/* LOW LEVEL Routines for Table : FsIpifTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpifTable
 Input       :  The Indices
                FsIpifIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIpifTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIpifTable (INT4 i4FsIpifIndex)
#else
INT1
nmhValidateIndexInstanceFsIpifTable (i4FsIpifIndex)
     INT4                i4FsIpifIndex;
#endif
{
    return (nmhValidateIndexInstanceIpIfTable (i4FsIpifIndex, 0));

   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpifTable
 Input       :  The Indices
                FsIpifIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIpifTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIpifTable (INT4 *pi4FsIpifIndex)
#else
INT1
nmhGetFirstIndexFsIpifTable (pi4FsIpifIndex)
     INT4               *pi4FsIpifIndex;
#endif
{
    if (IpvxUtilGetFirstIndexFsMIStdIpifTable(pi4FsIpifIndex) == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", *pi4FsIpifIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpifTable
 Input       :  The Indices
                FsIpifIndex
                nextFsIpifIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIpifTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIpifTable (INT4 i4FsIpifIndex, INT4 *pi4NextFsIpifIndex)
#else
INT1
nmhGetNextIndexFsIpifTable (i4FsIpifIndex, pi4NextFsIpifIndex)
     INT4                i4FsIpifIndex;
     INT4               *pi4NextFsIpifIndex;
#endif
{
    if (IpvxUtilGetNextIndexFsMIStdIpifTable 
        (i4FsIpifIndex, pi4NextFsIpifIndex)== IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", *pi4FsIpifIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpifMaxReasmSize
 Input       :  The Indices
                FsIpifIndex

                The Object
                retValFsIpifMaxReasmSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpifMaxReasmSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpifMaxReasmSize (INT4 i4FsIpifIndex, INT4 *pi4RetValFsIpifMaxReasmSize)
#else
INT1
nmhGetFsIpifMaxReasmSize (i4FsIpifIndex, pi4RetValFsIpifMaxReasmSize)
     INT4                i4FsIpifIndex;
     INT4               *pi4RetValFsIpifMaxReasmSize;
#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        *pi4RetValFsIpifMaxReasmSize =
            (INT4) IPIF_GLBTAB_REASM_MAX_SIZE (u2Index);
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsIpifIcmpRedirectEnable
 Input       :  The Indices
                FsIpifIndex

                The Object
                retValFsIpifIcmpRedirectEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpifIcmpRedirectEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpifIcmpRedirectEnable (INT4 i4FsIpifIndex,
                                INT4 *pi4RetValFsIpifIcmpRedirectEnable)
#else
INT1
nmhGetFsIpifIcmpRedirectEnable (i4FsIpifIndex,
                                pi4RetValFsIpifIcmpRedirectEnable)
     INT4                i4FsIpifIndex;
     INT4               *pi4RetValFsIpifIcmpRedirectEnable;
#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        *pi4RetValFsIpifIcmpRedirectEnable =
            (INT4) IPIF_ICMPREDIRECT_ENABLE (u2Index);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsIpifDrtBcastFwdingEnable
 Input       :  The Indices
                FsIpifIndex

                The Object
                retValFsIpifDrtBcastFwdingEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpifDrtBcastFwdingEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpifDrtBcastFwdingEnable (INT4 i4FsIpifIndex,
                                  INT4 *pi4RetValFsIpifDrtBcastFwdingEnable)
#else
INT1
nmhGetFsIpifDrtBcastFwdingEnable (i4FsIpifIndex,
                                  pi4RetValFsIpifDrtBcastFwdingEnable)
     INT4                i4FsIpifIndex;
     INT4               *pi4RetValFsIpifDrtBcastFwdingEnable;
#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_CONFIG_GET_DIRECTED_BCAST_FWDING_ENABLE (u2Index,
                                                      *pi4RetValFsIpifDrtBcastFwdingEnable);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpifMaxReasmSize
 Input       :  The Indices
                FsIpifIndex

                The Object
                setValFsIpifMaxReasmSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpifMaxReasmSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpifMaxReasmSize (INT4 i4FsIpifIndex, INT4 i4SetValFsIpifMaxReasmSize)
#else
INT1
nmhSetFsIpifMaxReasmSize (i4FsIpifIndex, i4SetValFsIpifMaxReasmSize)
     INT4                i4FsIpifIndex;
     INT4                i4SetValFsIpifMaxReasmSize;

#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_GLBTAB_REASM_MAX_SIZE (u2Index) =
            (UINT4) i4SetValFsIpifMaxReasmSize;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
        IP_CFG_SET_IF_TBL_CHANGE_TIME ();
        IncMsrForFsIpv4IpifTable (i4FsIpifIndex,
                                  i4SetValFsIpifMaxReasmSize,
                                  FsMIFsIpifMaxReasmSize,
                                  (sizeof (FsMIFsIpifMaxReasmSize) /
                                   sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FsIpifMaxReasmSize = %d\n", i4SetValFsIpifMaxReasmSize); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsIpifIcmpRedirectEnable
 Input       :  The Indices
                FsIpifIndex

                The Object
                setValFsIpifIcmpRedirectEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpifIcmpRedirectEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpifIcmpRedirectEnable (INT4 i4FsIpifIndex,
                                INT4 i4SetValFsIpifIcmpRedirectEnable)
#else
INT1
nmhSetFsIpifIcmpRedirectEnable (i4FsIpifIndex, i4SetValFsIpifIcmpRedirectEnable)
     INT4                i4FsIpifIndex;
     INT4                i4SetValFsIpifIcmpRedirectEnable;

#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_ICMPREDIRECT_ENABLE (u2Index) =
            (UINT1) i4SetValFsIpifIcmpRedirectEnable;
        IncMsrForFsIpv4IpifTable (i4FsIpifIndex,
                                  i4SetValFsIpifIcmpRedirectEnable,
                                  FsMIFsIpifIcmpRedirectEnable,
                                  (sizeof (FsMIFsIpifIcmpRedirectEnable) /
                                   sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FsIpifIcmpRedirectEnable = %d\n", i4SetValFsIpifIcmpRedirectEnable); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsIpifDrtBcastFwdingEnable
 Input       :  The Indices
                FsIpifIndex

                The Object
                setValFsIpifDrtBcastFwdingEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpifDrtBcastFwdingEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpifDrtBcastFwdingEnable (INT4 i4FsIpifIndex,
                                  INT4 i4SetValFsIpifDrtBcastFwdingEnable)
#else
INT1
nmhSetFsIpifDrtBcastFwdingEnable (i4FsIpifIndex,
                                  i4SetValFsIpifDrtBcastFwdingEnable)
     INT4                i4FsIpifIndex;
     INT4                i4SetValFsIpifDrtBcastFwdingEnable;

#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_CONFIG_SET_DIRECTED_BCAST_FWDING_ENABLE (u2Index,
                                                      (UINT1)
                                                      i4SetValFsIpifDrtBcastFwdingEnable);
        IncMsrForFsIpv4IpifTable (i4FsIpifIndex,
                                  i4SetValFsIpifDrtBcastFwdingEnable,
                                  FsMIFsIpifDrtBcastFwdingEnable,
                                  (sizeof (FsMIFsIpifDrtBcastFwdingEnable) /
                                   sizeof (UINT4)), FALSE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FsIpifDrtBcastFwdingEnable = %d\n", i4SetValFsIpifDrtBcastFwdingEnable); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpifMaxReasmSize
 Input       :  The Indices
                FsIpifIndex

                The Object
                testValFsIpifMaxReasmSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpifMaxReasmSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpifMaxReasmSize (UINT4 *pu4ErrorCode, INT4 i4FsIpifIndex,
                             INT4 i4TestValFsIpifMaxReasmSize)
#else
INT1
nmhTestv2FsIpifMaxReasmSize (*pu4ErrorCode, i4FsIpifIndex,
                             i4TestValFsIpifMaxReasmSize)
     UINT4              *pu4ErrorCode;
     INT4                i4FsIpifIndex;
     INT4                i4TestValFsIpifMaxReasmSize;
#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) ==
        IPIF_INVALID_INDEX)
    {
        /* Invalid Interface Index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIpifMaxReasmSize < IPIF_MIN_REASM_SIZE) ||
        (i4TestValFsIpifMaxReasmSize > IPIF_MAX_REASM_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsIpifIcmpRedirectEnable
 Input       :  The Indices
                FsIpifIndex

                The Object
                testValFsIpifIcmpRedirectEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpifIcmpRedirectEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpifIcmpRedirectEnable (UINT4 *pu4ErrorCode, INT4 i4FsIpifIndex,
                                   INT4 i4TestValFsIpifIcmpRedirectEnable)
#else
INT1
nmhTestv2FsIpifIcmpRedirectEnable (*pu4ErrorCode, i4FsIpifIndex,
                                   i4TestValFsIpifIcmpRedirectEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4FsIpifIndex;
     INT4                i4TestValFsIpifIcmpRedirectEnable;
#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) ==
        IPIF_INVALID_INDEX)
    {
        /* Invalid Interface Index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIpifIcmpRedirectEnable != ICMP_REDIRECT_ENABLE) &&
        (i4TestValFsIpifIcmpRedirectEnable != ICMP_REDIRECT_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FsIpifIcmpRedirectEnable = %d\n", i4TestValFsIpifIcmpRedirectEnable); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsIpifDrtBcastFwdingEnable
 Input       :  The Indices
                FsIpifIndex

                The Object
                testValFsIpifDrtBcastFwdingEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpifDrtBcastFwdingEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpifDrtBcastFwdingEnable (UINT4 *pu4ErrorCode, INT4 i4FsIpifIndex,
                                     INT4 i4TestValFsIpifDrtBcastFwdingEnable)
#else
INT1
nmhTestv2FsIpifDrtBcastFwdingEnable (*pu4ErrorCode, i4FsIpifIndex,
                                     i4TestValFsIpifDrtBcastFwdingEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4FsIpifIndex;
     INT4                i4TestValFsIpifDrtBcastFwdingEnable;
#endif
{
    UINT2               u2Index = 0;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) ==
        IPIF_INVALID_INDEX)
    {
        /* Invalid Interface Index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIpifDrtBcastFwdingEnable != IPIF_DRTD_BCAST_FWD_ENABLE) &&
        (i4TestValFsIpifDrtBcastFwdingEnable != IPIF_DRTD_BCAST_FWD_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", i4FsIpifIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "FsIpifDrtBcastFwdingEnable = %d\n", i4TestValFsIpifDrtBcastFwdingEnable); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsIpifProxyArpAdminStatus
 Input       :  The Indices
                FsIpifIndex

                The Object
                retValFsIpifProxyArpAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpifProxyArpAdminStatus (INT4 i4FsIpifIndex,
                                 INT4 *pi4RetValFsIpifProxyArpAdminStatus)
{
    if (IpvxUtilGetFsMIStdIpProxyArpAdminStatus (i4FsIpifIndex, 
                                        pi4RetValFsIpifProxyArpAdminStatus) 
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpifProxyArpAdminStatus
 Input       :  The Indices
                FsIpifIndex

                The Object
                setValFsIpifProxyArpAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpifProxyArpAdminStatus (INT4 i4FsIpifIndex,
                                 INT4 i4SetValFsIpifProxyArpAdminStatus)
{


    if (IpvxUtilSetFsMIStdIpProxyArpAdminStatus 
        (i4FsIpifIndex, i4SetValFsIpifProxyArpAdminStatus) 
        == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsIpifProxyArpAdminStatus
 Input       :  The Indices
                FsIpifIndex

                The Object
                testValFsIpifProxyArpAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpifProxyArpAdminStatus (UINT4 *pu4ErrorCode, INT4 i4FsIpifIndex,
                                    INT4 i4TestValFsIpifProxyArpAdminStatus)
{
    if ( IpvxUtilTestv2FsMIStdIpProxyArpAdminStatus 
         (pu4ErrorCode,i4FsIpifIndex,
          i4TestValFsIpifProxyArpAdminStatus)== IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpifLocalProxyArpAdminStatus
 Input       :  The Indices
                FsIpifIndex

                The Object
                retValFsIpifLocalProxyArpAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpifLocalProxyArpAdminStatus (INT4 i4FsIpifIndex,
                                      INT4
                                      *pi4RetValFsIpifLocalProxyArpAdminStatus)
{
    if (IpvxUtilGetFsMIStdIpLocalProxyArpAdminStatus
        (i4FsIpifIndex, 
         pi4RetValFsIpifLocalProxyArpAdminStatus) == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIpifLocalProxyArpAdminStatus
 Input       :  The Indices
                FsIpifIndex

                The Object
                setValFsIpifLocalProxyArpAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpifLocalProxyArpAdminStatus (INT4 i4FsIpifIndex,
                                      INT4
                                      i4SetValFsIpifLocalProxyArpAdminStatus)
{

if (IpvxUtilSetFsMIStdIpLocalProxyArpAdminStatus
    (i4FsIpifIndex, 
     i4SetValFsIpifLocalProxyArpAdminStatus) == IPVX_FAILURE)
{
    return SNMP_FAILURE;
}
return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsIpifLocalProxyArpAdminStatus
 Input       :  The Indices
                FsIpifIndex

                The Object
                testValFsIpifLocalProxyArpAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpifLocalProxyArpAdminStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsIpifIndex,
                                         INT4
                                         i4TestValFsIpifLocalProxyArpAdminStatus)
{
    if (IpvxUtilTestv2FsMIStdIpLocalProxyArpAdminStatus
        (pu4ErrorCode, i4FsIpifIndex, 
         i4TestValFsIpifLocalProxyArpAdminStatus) == IPVX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpifTable
 Input       :  The Indices
                FsIpifIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpifTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IpGetFirstIndexFsIpifTable
 Input       :  pi4FsIpifIndex
 Output      :  i4SubrefNum
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/


INT1 IpGetFirstIndexFsIpifTable (INT4 *pi4FsIpifIndex)
{
    INT4                i4SubrefNum;
    if (nmhGetNextIndexIpIfTable (0, pi4FsIpifIndex, 0,
                                  &i4SubrefNum) == SNMP_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;

}

/****************************************************************************
 Function    :  IpGetNextIndexFsIpifTable
 Input       :  i4FsIpifIndex
 Output      :  pi4NextFsIpifIndex
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/


INT1 IpGetNextIndexFsIpifTable (INT4 i4FsIpifIndex, INT4 *pi4NextFsIpifIndex)
{
    INT4                i4NextSubrefNum = 0;

    if (i4FsIpifIndex < 0)
    {
        return IP_FAILURE;
    }
    if (nmhGetNextIndexIpIfTable (i4FsIpifIndex, pi4NextFsIpifIndex, 0,
                                  &i4NextSubrefNum) == SNMP_FAILURE)
    {
        return IP_FAILURE;
    }

   /*** $$TRACE_LOG (ENTRY, "FsIpifIndex = %d\n", *pi4FsIpifIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    return IP_SUCCESS;

}

/****************************************************************************
 Function    :  IpGetFsIpifProxyArpAdminStatus 
 Input       :  i4FsIpifIndex 
 Output      :  pi4RetValFsIpifProxyArpAdminStatus 
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/



INT1 IpGetFsIpifProxyArpAdminStatus (INT4 i4FsIpifIndex,
                                    INT4 *pi4RetValFsIpifProxyArpAdminStatus)
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_CONFIG_GET_PROXY_ARP_ADMIN_STATUS (u2Index,
                                                *pi4RetValFsIpifProxyArpAdminStatus);
        return IP_SUCCESS;
    }

    return IP_FAILURE;

}

/****************************************************************************
 Function    :  IpGetFsIpifLocalProxyArpAdminStatus 
 Input       :  i4FsIpifIndex
 Output      :  pi4RetValFsIpifLocalProxyArpAdminStatus 
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/


INT1
IpGetFsIpifLocalProxyArpAdminStatus(INT4 i4FsIpifIndex,
                                  INT4 *pi4RetValFsIpifLocalProxyArpAdminStatus)
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_CONFIG_GET_LOCAL_PROXY_ARP_STATUS (u2Index,
                                                *pi4RetValFsIpifLocalProxyArpAdminStatus);
        return IP_SUCCESS;
    }

    return IP_FAILURE;

}

/****************************************************************************
 Function    :  IpSetFsIpifProxyArpAdminStatus 
 Input       :  i4FsIpifIndex , i4SetValFsIpifProxyArpAdminStatus
 Output      :  NONE 
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/

INT1
IpSetFsIpifProxyArpAdminStatus( INT4 i4FsIpifIndex,
                                INT4 i4SetValFsIpifProxyArpAdminStatus)
{
    tIfaceQMsg         *IfaceQMsg = NULL;


    if ((IfaceQMsg =
         (tIfaceQMsg *) IP_ALLOCATE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.
                                               IfacePoolId)) == NULL)
    {
        return IP_FAILURE;
    }

    IfaceQMsg->u2IfIndex = (UINT2) i4FsIpifIndex;
    if (i4SetValFsIpifProxyArpAdminStatus == ARP_PROXY_ENABLE)
    {
        IfaceQMsg->u1MsgType = IP_PROXYARP_ADMIN_STATUS_ENABLE;
    }
    else if (i4SetValFsIpifProxyArpAdminStatus == ARP_PROXY_DISABLE)
    {
    IfaceQMsg->u1MsgType = IP_PROXYARP_ADMIN_STATUS_DISABLE;
    }

    if (OsixQueSend (gIpGlobalInfo.IpCfaIfQId, (UINT1 *) &IfaceQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IfacePoolId, IfaceQMsg);
        return IP_FAILURE;
    }
    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_PROXYARP_ADMIN_STATUS_EVENT);

    IncMsrForFsIpv4IpifTable (i4FsIpifIndex,
            i4SetValFsIpifProxyArpAdminStatus,
            FsMIFsIpifProxyArpAdminStatus,
            (sizeof (FsMIFsIpifProxyArpAdminStatus) /
             sizeof (UINT4)), FALSE);

    return IP_SUCCESS;

}

/****************************************************************************
 Function    :  IpSetFsIpifLocalProxyArpAdminStatus 
 Input       :  i4FsIpifIndex , i4SetValFsIpifLocalProxyArpAdminStatus 
 Output      :  NONE
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/

INT1
IpSetFsIpifLocalProxyArpAdminStatus (INT4 i4FsIpifIndex,
                                      INT4
                                      i4SetValFsIpifLocalProxyArpAdminStatus)
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) !=
        IPIF_INVALID_INDEX)
    {
        IPIF_CONFIG_SET_LOCAL_PROXY_ARP_STATUS (u2Index,
                                                (UINT1)
                                                i4SetValFsIpifLocalProxyArpAdminStatus);
        IncMsrForFsIpv4IpifTable (i4FsIpifIndex,
                                  i4SetValFsIpifLocalProxyArpAdminStatus,
                                  FsMIFsIpifLocalProxyArpAdminStatus,
                                  (sizeof (FsMIFsIpifLocalProxyArpAdminStatus) /
                                   sizeof (UINT4)), FALSE);

        return IP_SUCCESS;
    }

    return IP_FAILURE;

}

/****************************************************************************
 Function    :  IpTestv2FsIpifProxyArpAdminStatus 
 Input       :  i4FsIpifIndex , i4TestValFsIpifProxyArpAdminStatus
 Output      :  pu4ErrorCode
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/


INT1 IpTestv2FsIpifProxyArpAdminStatus 
    (UINT4 *pu4ErrorCode, INT4 i4FsIpifIndex,
    INT4 i4TestValFsIpifProxyArpAdminStatus)
{

    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */
    INT4                i4LocalProxyArpStatus;

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) ==
        IPIF_INVALID_INDEX)
    {
        /* Invalid Interface Index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return IP_FAILURE;
    }

    if (((UINT1) i4TestValFsIpifProxyArpAdminStatus != ARP_PROXY_ENABLE) &&
        ((UINT1) i4TestValFsIpifProxyArpAdminStatus != ARP_PROXY_DISABLE))
    {
        /* Invalid Value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP_FAILURE;
    }

    IPIF_CONFIG_GET_LOCAL_PROXY_ARP_STATUS (u2Index, i4LocalProxyArpStatus);
    if ((i4LocalProxyArpStatus == ARP_PROXY_ENABLE)
        && (i4TestValFsIpifProxyArpAdminStatus == ARP_PROXY_DISABLE))
    {
        /* local Proxy arp should be disabled before disabling
         * proxy arp */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/****************************************************************************
 Function    : IpTestv2FsIpifLocalProxyArpAdminStatus 
 Input       :  i4FsIpifIndex , i4TestValFsIpifLocalProxyArpAdminStatus 
 Output      :  pu4ErrorCode
 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IpTestv2FsIpifLocalProxyArpAdminStatus (UINT4 *pu4ErrorCode,
                                         INT4 i4FsIpifIndex,
                                         INT4
                                         i4TestValFsIpifLocalProxyArpAdminStatus)
{
    UINT2               u2Index;
    tIP_INTERFACE       InterfaceId;    /* Interface index  */
    INT4                i4ProxyArpAdminStatus;

    ipifConvertIpIfToCRU (i4FsIpifIndex, 0, &InterfaceId);

    if ((u2Index = (UINT2) ipif_mgmt_iftab_get_entry (InterfaceId)) ==
        IPIF_INVALID_INDEX)
    {
        /* Invalid Interface Index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return IP_FAILURE;
    }

    if (((UINT1) i4TestValFsIpifLocalProxyArpAdminStatus
         != ARP_PROXY_ENABLE) &&
        ((UINT1) i4TestValFsIpifLocalProxyArpAdminStatus != ARP_PROXY_DISABLE))
    {
        /* Invalid Value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP_FAILURE;
    }

    IPIF_CONFIG_GET_PROXY_ARP_ADMIN_STATUS (u2Index, i4ProxyArpAdminStatus);
    if ((i4TestValFsIpifLocalProxyArpAdminStatus == ARP_PROXY_ENABLE)
        && (i4ProxyArpAdminStatus == ARP_PROXY_DISABLE))
    {
        /* Proxy arp should be enabled before enabling
         * local proxy arp */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP_FAILURE;
    }
    return IP_SUCCESS;

}

