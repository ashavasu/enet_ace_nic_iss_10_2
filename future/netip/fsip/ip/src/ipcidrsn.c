/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcidrsn.c,v 1.13 2015/11/09 09:41:41 siva Exp $
 *
 * Description:This file contains Low level routines for     
 *             CIDR Configuration Tables                 
 *
 *******************************************************************/
#include "ipinc.h"
#include "fsmpipcli.h"

/* **************************************************************************
 * *********************G L O B A L  V A R I A B L E S **********************
 * **************************************************************************
 */
UINT1               gu1AggrRegnId;

VOID                IpAddAggEntryToListInCxt
PROTO ((tIpCxt * pIpCxt, tCidrAggrRtInfo * pAggRtEntry));

VOID IpAddExplRtEntryToListInCxt PROTO ((tCidrAggrRtInfo * pAggRtEntry,
                                         tCidrExplRts * pExplRtEntry));

/**************************************************************************/
/* Function Name     :  IpAggrRtInit                                      */
/*                                                                        */
/* Description       :  This function registers Aggregate Route entry with*/
/*                      RTM                                               */
/* Input(s)          :  None                                              */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  SUCCESS/FAILURE.                                  */
/**************************************************************************/
#ifdef __STDC__
INT4
IpAggrRtInitInCxt (UINT4 u4ContextId)
#else
INT4
IpAggrRtInitInCxt (u4ContextId)
     UINT4               u4ContextId;
#endif
{
    tRtmRegnId          RegnId;
    INT4                i4RetVal = IP_FAILURE;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
    RegnId.u4ContextId = u4ContextId;
    RegnId.u2ProtoId = OTHERS_ID;

    i4RetVal = RtmRegister (&RegnId, 0, NULL);
    if (i4RetVal == RTM_FAILURE)
    {
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  IpCidrCfgInitInCxt                                */
/*                                                                        */
/* Description       :  This function initialises the CIDR configuration  */
/*                      data structure in the specified context.          */
/*                                                                        */
/* Input(s)          :  None                                              */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/
/***         $$TRACE_PROCEDURE_NAME =  IpCidrCfgInit                    ***/
/***         $$TRACE_PROCEDURE_LEVEL = LOW                              ***/
/**************************************************************************/
#ifdef __STDC__
VOID
IpCidrCfgInitInCxt (tIpCxt * pIpCxt)
#else
VOID
IpCidrCfgInitInCxt (pIpCxt)
     tIpCxt             *pIpCxt;
#endif
{
    TMO_DLL_Init (&pIpCxt->CidrAggRtList);
    return;
}

/**************************************************************************/
/* Function Name     :  IpAddAggEntryToListInCxt                          */
/*                                                                        */
/* Description       :  This function adds the agg entry to list in a     */
/*                      sorted order.                                     */
/*                                                                        */
/* Input(s)          :  pIpCxt - IP context pointer                       */
/*                      pAggRtEntry - The aggregated route entry           */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/
VOID
IpAddAggEntryToListInCxt (tIpCxt * pIpCxt, tCidrAggrRtInfo * pAggRtEntry)
{
    tCidrAggrRtInfo    *pAggTmpNode = NULL;

    if (TMO_DLL_Count (&pIpCxt->CidrAggRtList) == 0)
    {
        /* First entry being inserted */
        TMO_DLL_Add (&pIpCxt->CidrAggRtList, &pAggRtEntry->NextAggRtNode);
        return;
    }

    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggTmpNode, tCidrAggrRtInfo *)
    {
        if ((pAggTmpNode->u4AggrRtAddr > pAggRtEntry->u4AggrRtAddr) ||
            (pAggTmpNode->u4AggrRtMask > pAggRtEntry->u4AggrRtMask))
        {
            /* This node have aggregated address greated than the incoming 
             * value. So inser the incoming entry befor this node.
             */
            TMO_DLL_Insert_In_Middle (&pIpCxt->CidrAggRtList,
                                      (&pAggTmpNode->NextAggRtNode)->pPrev,
                                      &pAggRtEntry->NextAggRtNode,
                                      &pAggTmpNode->NextAggRtNode);
            return;
        }
    }
    TMO_DLL_Add (&pIpCxt->CidrAggRtList, &pAggRtEntry->NextAggRtNode);
    return;
}

/**************************************************************************/
/* Function Name     :  IpAddExplRtEntryToListInCxt                       */
/*                                                                        */
/* Description       :  This function adds the agg entry to list in a     */
/*                      sorted order.                                     */
/*                                                                        */
/* Input(s)          :  pIpCxt - IP context pointer                       */
/*                      pExplRtEntry - The explicitly advertised entry    */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  None                                              */
/**************************************************************************/
VOID
IpAddExplRtEntryToListInCxt (tCidrAggrRtInfo * pAggRtEntry,
                             tCidrExplRts * pExplRtEntry)
{
    tCidrExplRts       *pExplTmpNode = NULL;

    if (TMO_DLL_Count (&pAggRtEntry->ExplRtList) == 0)
    {
        /* First entry being inserted */
        TMO_DLL_Add (&pAggRtEntry->ExplRtList, &pExplRtEntry->NextExplRtNode);
        return;
    }

    TMO_DLL_Scan (&pAggRtEntry->ExplRtList, pExplTmpNode, tCidrExplRts *)
    {
        if ((pExplTmpNode->u4ExplRtAddr > pExplRtEntry->u4ExplRtAddr) ||
            (pExplTmpNode->u4ExplRtMask > pExplRtEntry->u4ExplRtMask))
        {
            /* This node have address greated than the incoming 
             * value. So insert the incoming entry befor this node.
             */
            TMO_DLL_Insert_In_Middle (&pAggRtEntry->ExplRtList,
                                      (&pExplTmpNode->NextExplRtNode)->pPrev,
                                      &pExplRtEntry->NextExplRtNode,
                                      &pExplTmpNode->NextExplRtNode);
            return;
        }
    }
    TMO_DLL_Add (&pAggRtEntry->ExplRtList, &pExplRtEntry->NextExplRtNode);
    return;
}

/* LOW LEVEL Routines for Table : CidrAggTable. */
/* GET_EXACT Validate Index Instance Routine. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCidrAggTable
 Input       :  The Indices
                CidrAggAddress
                CidrAggAddressMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsCidrAggTable (UINT4 u4CidrAggAddress,
                                        UINT4 u4CidrAggAddressMask)
#else
INT1
nmhValidateIndexInstanceFsCidrAggTable (u4CidrAggAddress, u4CidrAggAddressMask)
     UINT4               u4CidrAggAddress;
     UINT4               u4CidrAggAddressMask;
#endif
{
    if (IP_IS_ADDR_CLASS_D (u4CidrAggAddress))
    {
        return SNMP_FAILURE;
    }

    if ((u4CidrAggAddress == 0) && (u4CidrAggAddressMask == 0))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsCidrAggTable
 Input       :  The Indices
                CidrAggAddress
                CidrAggAddressMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsCidrAggTable (UINT4 *pu4CidrAggAddress,
                                UINT4 *pu4CidrAggAddressMask)
#else
INT1
nmhGetFirstIndexFsCidrAggTable (pu4CidrAggAddress, pu4CidrAggAddressMask)
     UINT4              *pu4CidrAggAddress;
     UINT4              *pu4CidrAggAddressMask;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggRtInfo = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAggRtInfo = (tCidrAggrRtInfo *) TMO_DLL_First (&pIpCxt->CidrAggRtList);
    if (pAggRtInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4CidrAggAddress = pAggRtInfo->u4AggrRtAddr;
    *pu4CidrAggAddressMask = pAggRtInfo->u4AggrRtMask;
    return SNMP_SUCCESS;

}

/* GET_NEXT Routine.  */
/****************************************************************************
 Function    :  nmhGetNextIndexFsCidrAggTable
 Input       :  The Indices
                CidrAggAddress
                nextCidrAggAddress
                CidrAggAddressMask
                nextCidrAggAddressMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***        $$TRACE_PROCEDURE_NAME = nmhGetNextCidrAggTable              ***/
/***        $$TRACE_PROCEDURE_LEVEL = LOW                                ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetNextIndexFsCidrAggTable (UINT4 u4CidrAggAddress,
                               UINT4 *pu4NextCidrAggAddress,
                               UINT4 u4CidrAggAddressMask,
                               UINT4 *pu4NextCidrAggAddressMask)
#else
INT1
nmhGetNextIndexFsCidrAggTable (u4CidrAggAddress, pu4NextCidrAggAddress,
                               u4CidrAggAddressMask, pu4NextCidrAggAddressMask)
     UINT4               u4CidrAggAddress;
     UINT4              *pu4NextCidrAggAddress;
     UINT4               u4CidrAggAddressMask;
     UINT4              *pu4NextCidrAggAddressMask;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggRtInfo = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAggRtInfo = (tCidrAggrRtInfo *) TMO_DLL_First (&pIpCxt->CidrAggRtList);
    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggRtInfo, tCidrAggrRtInfo *)
    {
        if ((pAggRtInfo->u4AggrRtAddr > u4CidrAggAddress) ||
            (pAggRtInfo->u4AggrRtMask > u4CidrAggAddressMask))
        {
            *pu4NextCidrAggAddress = pAggRtInfo->u4AggrRtAddr;
            *pu4NextCidrAggAddressMask = pAggRtInfo->u4AggrRtMask;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsCidrAggStatus
 Input       :  The Indices
                CidrAggAddress
                CidrAggAddressMask

                The Object 
                retValCidrAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***       $$TRACE_PROCEDURE_NAME = nmhGetCidrAggStatus                  ***/
/***       $$TRACE_PROCEDURE_LEVEL = LOW                                 ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsCidrAggStatus (UINT4 u4CidrAggAddress, UINT4 u4CidrAggAddressMask,
                       INT4 *pi4RetValCidrAggStatus)
#else
INT1
nmhGetFsCidrAggStatus (u4CidrAggAddress, u4CidrAggAddressMask,
                       pi4RetValCidrAggStatus)
     UINT4               u4CidrAggAddress;
     UINT4               u4CidrAggAddressMask;
     INT4               *pi4RetValCidrAggStatus;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggTmpNode = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValCidrAggStatus = 0;
    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggTmpNode, tCidrAggrRtInfo *)
    {
        if ((pAggTmpNode->u4AggrRtAddr == u4CidrAggAddress) &&
            (pAggTmpNode->u4AggrRtMask == u4CidrAggAddressMask))
        {
            *pi4RetValCidrAggStatus = pAggTmpNode->u1AggrRtStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsCidrAggStatus
 Input       :  The Indices
                CidrAggAddress
                CidrAggAddressMask

                The Object 
                setValCidrAggStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***           $$TRACE_PROCEDURE_NAME = nmhSetCidrAggStatus              ***/
/***           $$TRACE_PROCEDURE_LEVEL = LOW                             ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsCidrAggStatus (UINT4 u4CidrAggAddress, UINT4 u4CidrAggAddressMask,
                       INT4 i4SetValCidrAggStatus)
#else
INT1
nmhSetFsCidrAggStatus (u4CidrAggAddress, u4CidrAggAddressMask,
                       i4SetValCidrAggStatus)
     UINT4               u4CidrAggAddress;
     UINT4               u4CidrAggAddressMask;
     INT4                i4SetValCidrAggStatus;
#endif
{
    tNetIpv4RtInfo      NetRtInfo;
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggRtNode = NULL;
    tCidrExplRts       *pExplRt = NULL;
    UINT4               u4CxtId = 0;
    UINT1               u1FoundFlag = FALSE;
    UINT1               u1AddFlag = FALSE;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggRtNode, tCidrAggrRtInfo *)
    {
        if ((pAggRtNode->u4AggrRtAddr == u4CidrAggAddress) &&
            (pAggRtNode->u4AggrRtMask == u4CidrAggAddressMask))
        {
            u1FoundFlag = TRUE;
            break;
        }
    }

    if (i4SetValCidrAggStatus == IP_CIDR_AGGR_ENABLE)
    {
        if (u1FoundFlag == FALSE)
        {
            /* New entry addition. Allocate memory. */
            pAggRtNode = (tCidrAggrRtInfo *)
                MemAllocMemBlk (gIpGlobalInfo.Ip_mems.AggRtPoolId);
            if (pAggRtNode == NULL)
            {
                return SNMP_FAILURE;
            }
            TMO_DLL_Init_Node (&pAggRtNode->NextAggRtNode);
            TMO_DLL_Init (&pAggRtNode->ExplRtList);
            pAggRtNode->u4AggrRtAddr = u4CidrAggAddress;
            pAggRtNode->u4AggrRtMask = u4CidrAggAddressMask;
            pAggRtNode->u1AggrRtStatus = (UINT1) i4SetValCidrAggStatus;
            IpAddAggEntryToListInCxt (pIpCxt, pAggRtNode);
            gIpGlobalInfo.u1AggRtEntryCount++;
            u1AddFlag = TRUE;
        }
        else
        {
            /* Entry Exist */
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (u1FoundFlag == TRUE)
        {
            /* Entry deletion */
            while ((pExplRt = (tCidrExplRts *) TMO_DLL_First
                    (&pAggRtNode->ExplRtList)))
            {
                TMO_DLL_Delete (&pAggRtNode->ExplRtList,
                                &pExplRt->NextExplRtNode);
                MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.AggAdvRtPoolId,
                                    (UINT1 *) pExplRt);
            }

            TMO_DLL_Delete (&pIpCxt->CidrAggRtList, &pAggRtNode->NextAggRtNode);
            MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.AggRtPoolId,
                                (UINT1 *) pAggRtNode);
            gIpGlobalInfo.u1AggRtEntryCount--;
            u1AddFlag = FALSE;
        }
        else
        {
            /* Trying to delete an entry that is not present */
            return SNMP_SUCCESS;
        }
    }

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
    NetRtInfo.u4DestNet = u4CidrAggAddress;
    NetRtInfo.u4DestMask = u4CidrAggAddressMask;
    NetRtInfo.u4NextHop = IPIF_INVALID_NEXTHOP;
    NetRtInfo.u4RtIfIndx = IPIF_INVALID_INDEX;
    NetRtInfo.u2RtType = (UINT2) FSIP_LOCAL;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u2RtProto = OTHERS_ID;
    NetRtInfo.u4ContextId = pIpCxt->u4ContextId;
    NetRtInfo.u2Weight = IP_DEFAULT_WEIGHT;

    if (u1AddFlag == FALSE)
    {
        /* u1AddFlag is false. So deleting the route */
        NetRtInfo.u4RowStatus = IPFWD_DESTROY;
        SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_STATUS);
        if (NetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &NetRtInfo) ==
            NETIPV4_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* u1AddFlag is true. So adding the route */
        NetRtInfo.u2RtType = (UINT2) REJECT;
        NetRtInfo.u4RowStatus = IPFWD_ACTIVE;
        SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_STATUS);
        if (NetIpv4LeakRoute (NETIPV4_ADD_ROUTE, &NetRtInfo) == NETIPV4_FAILURE)
        {
            KW_FALSEPOSITIVE_FIX (pAggRtNode);
            return SNMP_FAILURE;
        }
    }

    IncMsrForFsIpv4CidrAggTable ((INT4) u4CxtId, u4CidrAggAddress,
                                 u4CidrAggAddressMask, i4SetValCidrAggStatus,
                                 FsMIFsIpCidrAggStatus,
                                 (sizeof (FsMIFsIpCidrAggStatus) /
                                  sizeof (UINT4)), TRUE);

    KW_FALSEPOSITIVE_FIX (pAggRtNode);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsCidrAggStatus
 Input       :  The Indices
                CidrAggAddress
                CidrAggAddressMask

                The Object 
                testValCidrAggStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***         $$TRACE_PROCEDURE_NAME = nmhTestv2CidrAggStatus               ***/
/***         $$TRACE_PROCEDURE_LEVEL = LOW                               ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsCidrAggStatus (UINT4 *pu4ErrorCode, UINT4 u4CidrAggAddress,
                          UINT4 u4CidrAggAddressMask,
                          INT4 i4TestValCidrAggStatus)
#else
INT1                nmhTestv2FsCidrAggStatus (pu4ErrorCode, u4CidrAggAddress,
                                              u4CidrAggAddressMask,
                                              UINT4 *pu4ErrorCode;
                                              i4TestValCidrAggStatus)
     UINT4               u4CidrAggAddress;
     UINT4               u4CidrAggAddressMask;
     INT4                i4TestValCidrAggStatus;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggTmpNode = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (gIpGlobalInfo.u1AggRtEntryCount == MAX_IP_AGG_ROUTES)
    {
        *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
        return SNMP_FAILURE;
    }

    if ((i4TestValCidrAggStatus != IP_CIDR_AGGR_ENABLE) &&
        (i4TestValCidrAggStatus != IP_CIDR_AGGR_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggTmpNode, tCidrAggrRtInfo *)
    {
        if ((pAggTmpNode->u4AggrRtMask & u4CidrAggAddress) ==
            pAggTmpNode->u4AggrRtAddr)
        {
            if ((pAggTmpNode->u4AggrRtAddr != u4CidrAggAddress) ||
                (pAggTmpNode->u4AggrRtMask != u4CidrAggAddressMask))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : CidrAdvertTable. */
/* GET_EXACT Validate Index Instance Routine. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCidrAdvertTable
 Input       :  The Indices
                CidrAdvertAddress
                CidrAdvertAddressMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***     $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceCidrAdvertTable ***/
/***     $$TRACE_PROCEDURE_LEVEL = LOW                                    ***/
/****************************************************************************/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsCidrAdvertTable (UINT4 u4CidrAdvertAddress,
                                           UINT4 u4CidrAdvertAddressMask)
#else
INT1
nmhValidateIndexInstanceFsCidrAdvertTable (u4CidrAdvertAddress,
                                           u4CidrAdvertAddressMask)
     UINT4               u4CidrAdvertAddress;
     UINT4               u4CidrAdvertAddressMask;
#endif
{
    if (IP_IS_ADDR_CLASS_D (u4CidrAdvertAddress))
    {
        return SNMP_FAILURE;
    }

    if ((u4CidrAdvertAddress == 0) && (u4CidrAdvertAddressMask == 0))
    {
        return IP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* GET_FIRST Routine. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFsCidrAdvertTable
 Input       :  The Indices
                CidrAdvertAddress
                CidrAdvertAddressMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***         $$TRACE_PROCEDURE_NAME = nmhGetFirstCidrAdvertTable         ***/
/***         $$TRACE_PROCEDURE_LEVEL = LOW                               ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsCidrAdvertTable (UINT4 *pu4CidrAdvertAddress,
                                   UINT4 *pu4CidrAdvertAddressMask)
#else
INT1
nmhGetFirstIndexFsCidrAdvertTable (pu4CidrAdvertAddress,
                                   pu4CidrAdvertAddressMask)
     UINT4              *pu4CidrAdvertAddress;
     UINT4              *pu4CidrAdvertAddressMask;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggRtInfo = NULL;
    tCidrExplRts       *pExplRt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAggRtInfo = (tCidrAggrRtInfo *) TMO_DLL_First (&pIpCxt->CidrAggRtList);
    if (pAggRtInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pExplRt = (tCidrExplRts *) TMO_DLL_First (&pAggRtInfo->ExplRtList);
    if (pExplRt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4CidrAdvertAddress = pExplRt->u4ExplRtAddr;
    *pu4CidrAdvertAddressMask = pExplRt->u4ExplRtMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsCidrAdvertTable
 Input       :  The Indices
                CidrAdvertAddress
                nextCidrAdvertAddress
                CidrAdvertAddressMask
                nextCidrAdvertAddressMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/***            $$TRACE_PROCEDURE_NAME = nmhGetNextCidrAdvertTable       ***/
/***            $$TRACE_PROCEDURE_LEVEL = LOW                            ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetNextIndexFsCidrAdvertTable (UINT4 u4CidrAdvertAddress,
                                  UINT4 *pu4NextCidrAdvertAddress,
                                  UINT4 u4CidrAdvertAddressMask,
                                  UINT4 *pu4NextCidrAdvertAddressMask)
#else
INT1
nmhGetNextIndexFsCidrAdvertTable (u4CidrAdvertAddress,
                                  pu4NextCidrAdvertAddress,
                                  u4CidrAdvertAddressMask,
                                  pu4NextCidrAdvertAddressMask)
     UINT4               u4CidrAdvertAddress;
     UINT4              *pu4NextCidrAdvertAddress;
     UINT4               u4CidrAdvertAddressMask;
     UINT4              *pu4NextCidrAdvertAddressMask;
#endif
{

    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggTmpNode = NULL;
    tCidrExplRts       *pExplRt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggTmpNode, tCidrAggrRtInfo *)
    {
        if ((pAggTmpNode->u4AggrRtMask & u4CidrAdvertAddress)
            == pAggTmpNode->u4AggrRtAddr)
        {
            break;
        }
    }

    if (pAggTmpNode == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pAggTmpNode->ExplRtList, pExplRt, tCidrExplRts *)
    {
        if ((pExplRt->u4ExplRtAddr > u4CidrAdvertAddress) ||
            (pExplRt->u4ExplRtMask > u4CidrAdvertAddressMask))
        {
            *pu4NextCidrAdvertAddress = pExplRt->u4ExplRtAddr;
            *pu4NextCidrAdvertAddressMask = pExplRt->u4ExplRtMask;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsCidrAdvertStatus
 Input       :  The Indices
                CidrAdvertAddress
                CidrAdvertAddressMask

                The Object 
                retValCidrAdvertStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetCidrAdvertStatus                     ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsCidrAdvertStatus (UINT4 u4CidrAdvertAddress,
                          UINT4 u4CidrAdvertAddressMask,
                          INT4 *pi4RetValCidrAdvertStatus)
#else
INT1
nmhGetFsCidrAdvertStatus (u4CidrAdvertAddress, u4CidrAdvertAddressMask,
                          pi4RetValCidrAdvertStatus)
     UINT4               u4CidrAdvertAddress;
     UINT4               u4CidrAdvertAddressMask;
     INT4               *pi4RetValCidrAdvertStatus;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggTmpNode = NULL;
    tCidrExplRts       *pExplRt = NULL;
    UINT4               u4CxtId = 0;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggTmpNode, tCidrAggrRtInfo *)
    {
        if ((pAggTmpNode->u4AggrRtMask & u4CidrAdvertAddress)
            == pAggTmpNode->u4AggrRtAddr)
        {
            break;
        }
    }

    if (pAggTmpNode == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pAggTmpNode->ExplRtList, pExplRt, tCidrExplRts *)
    {
        if ((pExplRt->u4ExplRtAddr == u4CidrAdvertAddress) &&
            (pExplRt->u4ExplRtMask == u4CidrAdvertAddressMask))
        {
            *pi4RetValCidrAdvertStatus = pExplRt->i1ExplRtStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsCidrAdvertStatus
 Input       :  The Indices
                CidrAdvertAddress
                CidrAdvertAddressMask

                The Object 
                setValCidrAdvertStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetCidrAdvertStatus                     ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsCidrAdvertStatus (UINT4 u4CidrAdvertAddress,
                          UINT4 u4CidrAdvertAddressMask,
                          INT4 i4SetValCidrAdvertStatus)
#else
INT1
nmhSetFsCidrAdvertStatus (u4CidrAdvertAddress, u4CidrAdvertAddressMask,
                          i4SetValCidrAdvertStatus)
     UINT4               u4CidrAdvertAddress;
     UINT4               u4CidrAdvertAddressMask;
     INT4                i4SetValCidrAdvertStatus;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggTmpNode = NULL;
    tCidrExplRts       *pExplRt = NULL;
    UINT4               u4CxtId = 0;
    UINT1               u1FoundFlag = FALSE;

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggTmpNode, tCidrAggrRtInfo *)
    {
        if ((pAggTmpNode->u4AggrRtMask & u4CidrAdvertAddress)
            == pAggTmpNode->u4AggrRtAddr)
        {
            break;
        }
    }

    if (pAggTmpNode == NULL)
    {
        /* No aggregation present */
        return SNMP_FAILURE;
    }

    TMO_DLL_Scan (&pAggTmpNode->ExplRtList, pExplRt, tCidrExplRts *)
    {
        if ((pExplRt->u4ExplRtAddr == u4CidrAdvertAddress) &&
            (pExplRt->u4ExplRtMask == u4CidrAdvertAddressMask))
        {
            u1FoundFlag = TRUE;
            break;
        }
    }

    if (i4SetValCidrAdvertStatus == IP_CIDR_EXPL_ENABLE)
    {
        if (u1FoundFlag == FALSE)
        {
            /* New entry addition. Allocate memory. */
            pExplRt = (tCidrExplRts *)
                MemAllocMemBlk (gIpGlobalInfo.Ip_mems.AggAdvRtPoolId);
            if (pExplRt == NULL)
            {
                return SNMP_FAILURE;
            }
            TMO_DLL_Init_Node (&pExplRt->NextExplRtNode);
            pExplRt->u4ExplRtAddr = u4CidrAdvertAddress;
            pExplRt->u4ExplRtMask = u4CidrAdvertAddressMask;
            pExplRt->i1ExplRtStatus = (INT1) i4SetValCidrAdvertStatus;
            IpAddExplRtEntryToListInCxt (pAggTmpNode, pExplRt);
        }
    }
    else
    {
        if (u1FoundFlag == TRUE)
        {
            /* Entry to be deleted. */
            TMO_DLL_Delete (&pAggTmpNode->ExplRtList, &pExplRt->NextExplRtNode);
            MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.AggAdvRtPoolId,
                                (UINT1 *) pExplRt);
        }
    }
    IncMsrForFsIpv4CidrAdvertTable ((INT4) u4CxtId, u4CidrAdvertAddress,
                                    u4CidrAdvertAddressMask,
                                    i4SetValCidrAdvertStatus,
                                    FsMIFsCidrAdvertStatus,
                                    (sizeof (FsMIFsCidrAdvertStatus) /
                                     sizeof (UINT4)), TRUE);
    KW_FALSEPOSITIVE_FIX (pExplRt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsCidrAdvertStatus
 Input       :  The Indices
                CidrAdvertAddress
                CidrAdvertAddressMask

                The Object 
                testValCidrAdvertStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2CidrAdvertStatus                    ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW                                       ***/
/***************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsCidrAdvertStatus (UINT4 *pu4ErrorCode, UINT4 u4CidrAdvertAddress,
                             UINT4 u4CidrAdvertAddressMask,
                             INT4 i4TestValCidrAdvertStatus)
#else
INT1                nmhTestv2FsCidrAdvertStatus (pu4ErrorCode,
                                                 u4CidrAdvertAddress,
                                                 UINT4 *pu4ErrorCode;
                                                 u4CidrAdvertAddressMask,
                                                 i4TestValCidrAdvertStatus)
     UINT4               u4CidrAdvertAddress;
     UINT4               u4CidrAdvertAddressMask;
     INT4                i4TestValCidrAdvertStatus;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tCidrAggrRtInfo    *pAggTmpNode = NULL;
    UINT4               u4CxtId = 0;

    UNUSED_PARAM (u4CidrAdvertAddressMask);

    u4CxtId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4CxtId);
    if (pIpCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValCidrAdvertStatus != IP_CIDR_EXPL_ENABLE) &&
        (i4TestValCidrAdvertStatus != IP_CIDR_EXPL_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if aggregation is present */
    TMO_DLL_Scan (&pIpCxt->CidrAggRtList, pAggTmpNode, tCidrAggrRtInfo *)
    {
        if ((pAggTmpNode->u4AggrRtMask & u4CidrAdvertAddress)
            == pAggTmpNode->u4AggrRtAddr)
        {
            break;
        }
    }

    if (pAggTmpNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/**************************************************************************/
/* Function Name     :  IpCidrCompareIndics                               */
/*                                                                        */
/* Description       :  This function is compares the given two Indices   */
/*                                                                        */
/* Input(s)          :  1. u4Addr1 - The   address for which the          */
/*                      2. u4Mask1 - The Mask                             */
/*                      3. u4Addr2                                        */
/*                      4. u4Mask2                                        */
/*                                                                        */
/* Output(s)         :  None                                              */
/*                                                                        */
/* Returns           :  ONE_GT_TWO | ONE_LT_TWO | ONE_EQ_TWO              */
/**************************************************************************/
#ifdef __STDC__
INT4
IpCidrCompareIndics (UINT4 u4Addr1, UINT4 u4Addr2, UINT4 u4Mask1, UINT4 u4Mask2)
#else
INT4
IpCidrCompareIndics (u4Addr1, u4Addr2, u4Mask1, u4Mask2)
     UINT4               u4Addr1;
     UINT4               u4Addr2;
     UINT4               u4Mask1;
     UINT4               u4Mask2;
#endif
{

    if (u4Addr1 > u4Addr2)
    {
        return ONE_GT_TWO;
    }
    if (u4Addr1 < u4Addr2)
    {
        return ONE_LT_TWO;
    }
    if (u4Mask1 > u4Mask2)
    {
        return ONE_GT_TWO;
    }
    if (u4Mask1 < u4Mask2)
    {
        return ONE_LT_TWO;
    }
    return ONE_EQ_TWO;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsCidrAggTable
 Input       :  The Indices
                FsCidrAggAddress
                FsCidrAggAddressMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsCidrAggTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsCidrAdvertTable
 Input       :  The Indices
                FsCidrAdvertAddress
                FsCidrAdvertAddressMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsCidrAdvertTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
