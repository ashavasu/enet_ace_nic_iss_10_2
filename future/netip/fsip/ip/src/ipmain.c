/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmain.c,v 1.22 2015/05/20 14:57:26 siva Exp $
 *
 * Description:Consists of send and receive functions     
 *             for higher level protocols. It also contains 
 *             some functions for manipulating the IP       
 *             addresses.                                
 *
 *******************************************************************/
#include "ipinc.h"
t_IP_STATS          Ip_stats;    /* IP statistics record */
/**************************************************************************/

     /**** $$TRACE_MODULE_NAME     = IP_FWD ****/
     /**** $$TRACE_SUB_MODULE_NAME = MAIN   ****/

/**** $$TRACE_PROCEDURE_NAME  =   ip_send ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/

/**********************************************************************/

/* -------------------------------------------------------------------+
 * Function           : ip_send_InCxt
 *
 * Input(s)           : pIpCxt, u4Src, u4Dest, i1Proto, i1Tos, i1Ttl, pBuf,
 *                      i2Len, i2Id, i1Df, i2Olen, u2Rt_port)
 *
 * Output(s)          : None.
 *
 * Returns            : IP_SUCCESS / IP_FAILURE
 *
 * Action :
 *
 * Constructs an IP datagram. Modelled after the example interface
 * on p 32 of RFC 791.
 *
 * IP Options are expected to be filled already at proper offset.
 *
 * This function is called when high level protocols want to send an IP pkt for
 * a particular context. Before calling this the header should be constructed 
 * by using those protocols. Parameter u2Rt_port is optional -- 0 indicates 
 * no interface specified. This parameter is necessary to support multicast 
 * on a specific interface.
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ip_send_InCxt (tIpCxt * pIpCxt, UINT4 u4Src, UINT4 u4Dest, UINT1 u1Proto,
               UINT1 u1Tos, UINT1 u1Ttl, tIP_BUF_CHAIN_HEADER * pBuf,
               UINT2 u2Len, UINT2 u2Id, UINT1 u1Df, UINT2 u2Olen,
               UINT2 u2Rt_port)
#else

INT4
ip_send_InCxt (pIpCxt, u4Src, u4Dest, u1Proto, u1Tos, u1Ttl, pBuf, u2Len,
               u2Id, u1Df, u2Olen, u2Rt_port)
     tIpCxt             *pIpCxt;    /* The context within which the packet
                                       will be processed */
     UINT4               u4Src;    /* Source IP Address       */
     UINT4               u4Dest;    /* Destination IP Address  */
     UINT1               u1Proto;    /* Protocol sending data   */
     UINT1               u1Tos;    /* Type of Service Needed  */
     UINT1               u1Ttl;    /* Initial time to live    */
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Allocated message buffer */
     UINT2               u2Len;    /* Length of Data          */
     UINT2               u2Id;    /* Identification if needed */
     UINT1               u1Df;    /* Don't fragment flag     */
     UINT2               u2Olen;    /* Length of IP options.   */
     UINT2               u2Rt_port;    /* Route Port -- Optional  */
#endif
{
    t_IP                Ip;        /* Pointer to IP header   */
#ifndef SLI_WANTED
    static UINT2        u2Id_cntr = 0;    /* Datagram serial number */
#endif
    tPmtuInfo          *pPmtuEntry = NULL;
    UINT4               u4CxtId = 0;

    IP_CXT_PKT_DUMP (pIpCxt->u4ContextId, IP_MOD_TRC, DUMP_TRC, IP_NAME, pBuf,
                     u2Len, "IP pkt Rx from HL.\n");
    u4CxtId = pIpCxt->u4ContextId;

    /*** Destn. cannot be 0.0.0.0 ***/
    if (u4Dest == 0)
    {
       /**** Generate some statistics here ****/
        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    /*    
     * Src must be one of routers IP address.
     * and must not be a bcast address
     */
    if ((u4Src == IP_GEN_BCAST_ADDR) ||    /* No broadcast addresses    */
        ((u4Src != IP_ANY_ADDR) &&    /* Allow IP_ANY_ADDR address */
         (ipif_is_our_address_InCxt (u4CxtId, u4Src) == FALSE)))
    {
       /**** Generate some statistics here ****/
        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    if ((UINT4) (u2Len + IP_HDR_LEN + u2Olen) > IP_BIT_ALL)
    {

        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    /* Move Back For the IP Header */
    IP_PKT_MOVE_BACK_TO_HEADER (pBuf, u2Olen);
   /**** $$TRACE_LOG (ENTRY, "SRC %x Dst %x Id %d \n", u4Src, u4Dest,
                      u2Id);****/
    /* ip_put_hdr will take care for HDR_LEN */
    /* Fill in IP header */

    /* Maintain Dont Fragment Bit */
    u1Df &= IP_DF_BIT;

    if (pIpCxt->u2PmtuStatusFlag == PMTU_ENABLED)
    {
        pPmtuEntry = PmtuLookupEntryInCxt (pIpCxt, u4Dest, u1Tos);

        if (pPmtuEntry != NULL)
        {
            if (pPmtuEntry->u1PmtuDiscFlag & IPPMTUDISC_DO)
            {
                if ((IP_HDR_LEN + u2Len + u2Olen) > (UINT2) pPmtuEntry->u4PMtu)
                {
                    /* Higher layer trying to send out some data, whose
                     * length is greater than PMTU value. Indicate to the
                     * higher layer about the Path MTU value. */
                    t_ICMP              Icmp;
                    Icmp.i1Type = ICMP_DEST_UNREACH;
                    Icmp.i1Code = ICMP_FRAG_NEEDED;
                    Icmp.args.u4Unused = 0;
                    ICMP_NH_MTU (Icmp) = (UINT2) pPmtuEntry->u4PMtu;
                    Icmp.u4ContextId = u4CxtId;
                    icmp_error_msg (pBuf, &Icmp);
                    if ((u1Df & IP_DF_BIT) != IP_DF_BIT)
                    {
                        /* Higher layer has not requested for Dont Fragment
                         * option. So this packet can be forwarded to the
                         * destination after fragmentation. */
                        pPmtuEntry->u1PmtuDiscFlag |= IPPMTUDISC_ALLOW_FRAG;
                    }
                }
            }
            /* Always Set the DONT FRAGMENT BIT if Path MTU is enabled. */
            u1Df |= IPPMTUDISC_DO;
        }
    }

    Ip.u1Version = IP_VERSION_4;
    Ip.u1Tos = u1Tos;
    Ip.u2Len = (UINT2) (IP_HDR_LEN + u2Len + u2Olen);
#ifdef SLI_WANTED
    IP4SYS_INC_OUT_REQUESTS (u4CxtId);
    Ip.u2Id =
        (UINT2) ((u2Id == 0) ? (UINT2) pIpCxt->Ip_stats.u4Out_requests : u2Id);
#else /* Do not remove the else part */
    Ip.u2Id = (u2Id == 0) ? u2Id_cntr++ : u2Id;
#endif
    Ip.u1Ttl = (UINT1) ((u1Ttl == 0) ? IP_CFG_GET_TTL (u4CxtId) : u1Ttl);
    Ip.u2Fl_offs = (u1Df & (IP_DF_BIT | IPPMTUDISC_DO)) ? IP_DF : 0;
    Ip.u1Proto = u1Proto;
    Ip.u4Src = u4Src;
    Ip.u4Dest = u4Dest;
    Ip.u2Olen = u2Olen;

    /* Calculate Cksum and Put IP header into the pBuf.
     * Also copies IP Options in buffer into Ip structure.
     */

    ip_put_hdr (pBuf, &Ip, FALSE);
#ifndef SLI_WANTED
    IP4SYS_INC_OUT_REQUESTS (u4CxtId);
#endif

    if (u2Rt_port != IPIF_INVALID_INDEX)
    {
        if (IpifGetIfOperStatus (u2Rt_port) != IPIF_OPER_ENABLE)
        {
            IP_RELEASE_BUF (pBuf, FALSE);
            return IP_FAILURE;
        }
    }

    if (IpOutputProcessPktInCxt (pIpCxt, pBuf, &Ip, u2Rt_port) == IP_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : IpDeliverHigherLayerProtocol
 *
 * Input(s)           : pBuf, pIp, u1Flag, u2Port 
 *
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Action :
 *
 * Reassemble incoming IP fragments and dispatch completed datagrams
 * to the proper transport module. Called from ip_route procedure.
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
IpDeliverHigherLayerProtocol (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp,
                              UINT1 u1Flag, UINT2 u2Port)
#else

INT4
IpDeliverHigherLayerProtocol (pBuf, pIp, u1Flag, u2Port)
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Allocated mbuf      */
     t_IP               *pIp;    /* Extracted IP header */
     UINT1               u1Flag;    /* mode of receive either - broadcast, 
                                       multicast or unicast */
     UINT2               u2Port;    /* Interface index     */
#endif
{
    tIP_INTERFACE       IfId;
    tIpCxt             *pIpCxt = NULL;

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }

    if (pIp->u1Proto < IP_MAX_PROTOCOLS)
    {
        if (pIpCxt->au1RegTable[(pIp->u1Proto)] == REGISTER)
        {
            /* ICMP packets are processed in the IP thread only. Also,
             * ICMP is not deregistered from IP.
             * When self-ping is initiated, this function is called twice
             * in the same flow since source IP and destination IP are
             * the Node's IP itself. It leads to double lock.
             * So, HL Registration lock is not required for ICMP alone. */
            if (pIp->u1Proto != ICMP_PROTOCOL_ID)
            {
                IP_HLREG_LOCK ();
            }

            if (gIpGlobalInfo.aHLProtoRegFn[(pIp->u1Proto)].pProtoPktRecv !=
                NULL)
            {
                /*
                 * If we have a complete packet, call the next layer
                 * to handle the packet.
                 */
                if (((pBuf = ipReassemble (pIp, pBuf, u2Port)) != NULL) &&
                    (pIp->u1Proto < IP_MAX_PROTOCOLS))
                {
                    /* It takes only one as count for all reassembled datagrams. */
                    IP4SYS_INC_IN_DELIVERIES (pIpCxt->u4ContextId);
                    IP4IF_INC_IN_DELIVERIES (u2Port);

                    IpifGetIfId (u2Port, &IfId);
                    gIpGlobalInfo.aHLProtoRegFn[pIp->u1Proto].pProtoPktRecv
                        (pBuf,
                         (UINT2) (pIp->u2Len - (IP_HDR_LEN + pIp->u2Olen)),
                         (UINT4) u2Port, IfId, u1Flag);
                }

                if (pIp->u1Proto != ICMP_PROTOCOL_ID)
                {
                    IP_HLREG_UNLOCK ();
                }

                return IP_SUCCESS;
            }

            if (gIpGlobalInfo.aProtoRegFn[(pIp->u1Proto)].pAppRcv != NULL)
            {
                /*
                 * If we have a complete packet, call the next layer
                 * to handle the packet.
                 */

                if (((pBuf = ipReassemble (pIp, pBuf, u2Port)) != NULL) &&
                    (pIp->u1Proto < IP_MAX_PROTOCOLS))
                {
                    /* It takes only one as count for all reassembled 
                     * datagrams. */
                    IP4SYS_INC_IN_DELIVERIES (pIpCxt->u4ContextId);
                    IP4IF_INC_IN_DELIVERIES (u2Port);

                    IpifGetIfId (u2Port, &IfId);
                    gIpGlobalInfo.aProtoRegFn[pIp->u1Proto].pAppRcv
                        (pBuf,
                         (UINT2) (pIp->u2Len - (IP_HDR_LEN + pIp->u2Olen)),
                         u2Port, IfId, u1Flag);
                }

                if (pIp->u1Proto != ICMP_PROTOCOL_ID)
                {
                    IP_HLREG_UNLOCK ();
                }

                return IP_SUCCESS;
            }

            if (pIp->u1Proto != ICMP_PROTOCOL_ID)
            {
                IP_HLREG_UNLOCK ();
            }
        }
    }
    return IP_FAILURE;
}

/**************************************************************************
 * Function Name    :   IpHandleUnKnownProtosInCxt
 * Description      :   This function sends protocol Unreachable message
 *                      for the Buffer Received in the context in which the
 *                      buffer was received.
 * Inputs           :   pIpCxt, pBuf
 *                      u2Port - IP Port Number
 *                      i1Flag - Flag
 * Outputs          :   None
 **************************************************************************/
VOID
IpHandleUnKnownProtosInCxt (tIpCxt * pIpCxt, tIP_BUF_CHAIN_HEADER * pBuf,
                            UINT2 u2Port, INT1 i1Flag)
{
    t_ICMP              Icmp;

    IP4SYS_INC_IN_UNKNOWN_PROTOS (pIpCxt->u4ContextId);
    IP4IF_INC_IN_UNKNOWN_PROTOS (u2Port);

    if (i1Flag == IP_FOR_THIS_NODE)
    {
        Icmp.i1Type = ICMP_DEST_UNREACH;
        Icmp.i1Code = ICMP_PROTO_UNREACH;
        Icmp.args.u4Unused = 0;
        Icmp.u4ContextId = pIpCxt->u4ContextId;
        icmp_error_msg (pBuf, &Icmp);
    }
}

/**** $$TRACE_PROCEDURE_NAME  = ip_validate_net_mask   ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
/*-------------------------------------------------------------------+
 *
 * Function           : ip_validate_net_mask
 *
 * Input(s)           : u4Addr, u4Value
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 * Checks the Netmask for a particular interface according to
 * its Address
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ip_validate_net_mask (UINT4 u4Addr, UINT4 u4Value)
#else

INT4
ip_validate_net_mask (u4Addr, u4Value)
     UINT4               u4Addr;
     UINT4               u4Value;
#endif
{
    UINT4               u4Tmp_addr = 0;
   /**** $$TRACE_LOG (ENTRY, "For Addr  %x Mask %x\n",
                              u4Addr, u4Value);****/
    u4Tmp_addr = IpGetDefaultNetmask (u4Addr);
    if ((u4Tmp_addr & u4Value) == u4Tmp_addr)
    {
      /**** $$TRACE_LOG (EXIT, "Success For Net Mask %x\n", u4Tmp_addr);****/
        return IP_SUCCESS;
    }
   /**** $$TRACE_LOG (EXIT, "Failure For Net Mask %x\n", u4Tmp_addr);****/
    return IP_FAILURE;
}

/**** $$TRACE_PROCEDURE_NAME  = ip_validate_bcast_addr   ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
/*-------------------------------------------------------------------+
 *
 * Function           : ip_validate_bcast_addr
 *
 * Input(s)           : u4Addr, u4Value
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 * Checks the broadcast address
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ip_validate_bcast_addr (UINT4 u4Addr, UINT4 u4Value)
#else

INT4
ip_validate_bcast_addr (u4Addr, u4Value)
     UINT4               u4Addr;
     UINT4               u4Value;
#endif
{
    UINT4               u4Tmp_addr = 0;
  /**** $$TRACE_LOG (ENTRY, "For Addr  %x Mask %x\n",
                             u4Addr, u4Value);****/
    u4Tmp_addr = IpGetDefaultNetmask (u4Addr);
    if ((u4Tmp_addr & u4Addr) == (u4Tmp_addr & u4Value))
    {
      /**** $$TRACE_LOG (EXIT, "Success For Net Mask %x\n", u4Tmp_addr);****/
        return IP_SUCCESS;
    }
  /**** $$TRACE_LOG (EXIT, "Failure For Net Mask %x\n", u4Tmp_addr);****/
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IP_Are_Interface_IDs_Equal
 *
 * Input(s)           : pIfId1, pIfId2
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 * Compares both Interface Ids. It also checks if they are invalid or not.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
IP_Are_Interface_IDs_Equal (tIP_INTERFACE * pIfId1, tIP_INTERFACE * pIfId2)
#else

INT4
IP_Are_Interface_IDs_Equal (pIfId1, pIfId2)
     tIP_INTERFACE      *pIfId1;
     tIP_INTERFACE      *pIfId2;
#endif
{

    if ((IP_GET_IFACE_TYPE (*pIfId1) == IP_IF_TYPE_INVALID) ||
        (IP_GET_IFACE_TYPE (*pIfId2) == IP_IF_TYPE_INVALID))
    {
        return 0;
    }

    return (ipif_get_tab_index (*pIfId1) == ipif_get_tab_index (*pIfId2));
}

/*-------------------------------------------------------------------+
 * Function           : ip_reverse_src_route_option
 *
 * Input(s)           : pu1_SrcOpt, u4Src
 *
 * Output(s)          : pu1_DstOpt
 *
 * Returns            : None
 *
 * Action :
 *    Reverses IP Source Route option.
 *    pu1_SrcOpt points to the start of the Option, Option Type.
 *    The reversed option is put in pu1_DstOpt with same type and length.
 *    u4Src is the Source that sent this message.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

UINT1
ip_reverse_src_route_option (UINT1 *pu1_DstOpt, UINT1 *pu1_SrcOpt,
                             UINT4 u4Src, UINT4 *pu4Dest)
#else

UINT1
ip_reverse_src_route_option (pu1_DstOpt, pu1_SrcOpt, u4Src, pu4Dest)
     UINT1              *pu1_DstOpt;
     UINT1              *pu1_SrcOpt;
     UINT4               u4Src;
     UINT4              *pu4Dest;
#endif
{
    UINT1               u1_Optlen = 0;
    UINT1               u1_RouteDatalen = 0;
    UNUSED_PARAM (pu4Dest);

    /*
     * S - Source orignating the datagram, u4Src
     * D - Destination receiving the datagram.
     *
     *               Src. Dest   Option
     *  Incoming   :  S,   D,   pu1_SrcOpt:   {    G1, G2, .... Gn >> }
     *  Changed to :  D,   Gn,  pu1_DstOpt:   { >> Gn-1, .. G1, S     }
     *
     *                                         >> - pointer
     *                                         Gx - Intermediate gateways
     */

    *pu1_DstOpt++ = pu1_SrcOpt[0];    /* Copy the Type          */
    *pu1_DstOpt++ = u1_Optlen = pu1_SrcOpt[1];    /* Update the length      */
    *pu1_DstOpt++ = pu1_SrcOpt[2];    /* Initialise the Pointer */

    pu1_SrcOpt += IP_THREE;

    /* Start copying from Gn-1,
     *       Only Gn-2 gateways have to be copied.
     *       predecrementing takes care of hopcounts
     */
    u1_RouteDatalen = (UINT1) (u1_Optlen - IP_THREE);
    while (u1_RouteDatalen--)
    {
        *(pu1_DstOpt)++ = *(pu1_SrcOpt)++;
    }

    /* Now put the final destination */

    pu1_DstOpt++;
    u4Src = IP_HTONL (u4Src);
    MEMCPY (pu1_DstOpt, &u4Src, sizeof (UINT4));

    return (u1_Optlen);            /* Return the option length */
}

/* -------------------------------------------------------------------+
 * Function           : IpSendMcastPktInCxt
 *
 * Input(s)           : pIpCxt - Pointer to IP context structure.
 *                      pBuf - Pointer to the buffer.
 *                      pIpMcastParams - Pointer to the multicast params. 
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action :
 *
 * Constructs an IP datagram. Modelled after the example interface
 * on p 32 of RFC 791.
 *
 * IP Options are expected to be filled already at proper offset.
 * This function is called when PIM Protocol wants to send an IP packet on one
 * or more interfaces. If the packet is to be sent on many interfaces then the  
 * buffer is duplicated and sent out.
+-------------------------------------------------------------------*/
#ifdef __STDC__
INT4
IpSendMcastPktInCxt (tIpCxt * pIpCxt, tIP_BUF_CHAIN_HEADER * pBuf,
                     tMcastIpSendParams * pIpMcastParams)
#else
INT4
IpSendMcastPktInCxt (pIpCxt, pBuf, pIpMcastParams)
     tIpCxt             *pIpCxt;
     tIP_BUF_CHAIN_HEADER *pBuf;
     tMcastIpSendParams *pIpMcastParams;
#endif
{
    t_IP_HEADER         IpHdr;
    t_IP                Ip;
    t_IP_HEADER        *pIpHdr = NULL;
    tIP_BUF_CHAIN_HEADER *pOutBuf = NULL;
    UINT4               u4RtPort = 0;
    UINT4               u4Sum = 0;
    UINT4               u4Iface = 0;
    UINT4               u4TotNumIface = 0;    /* No of interfaces on which this 
                                               packet is to be forwarded. */
    if (pIpCxt->u4ContextId != IP_DEFAULT_CONTEXT)
    {
        return IP_FAILURE;
    }

    /* This is done because IpOutput expects such a parameter */
    if (IpExtractHdr (&Ip, pBuf) == IP_FAILURE)
    {
        return IP_FAILURE;
    }

    /*** Destn. cannot be 0.0.0.0 ***/
    if (pIpMcastParams->u4DestAddr == 0)
    {
       /**** Generate some statistics here ****/
        return IP_FAILURE;
    }

    pIpHdr = (t_IP_HEADER *) (VOID *)
        CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, IP_HDR_LEN);

    if (pIpHdr == NULL)
    {
        IP_COPY_FROM_BUF (pBuf, (UINT1 *) &IpHdr, 0, IP_HDR_LEN);
        IpHdr.u1Ttl--;
        u4Sum = (UINT4) (IP_NTOHS (IpHdr.u2Cksum) + 0x100);
        IpHdr.u2Cksum = (UINT2) IP_HTONS (u4Sum + (u4Sum >> IP_SIXTEEN));
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IpHdr, 0, IP_HDR_LEN);
    }
    else
    {
        pIpHdr->u1Ttl--;
        u4Sum = (UINT4) (IP_NTOHS (pIpHdr->u2Cksum) + 0x100);
        pIpHdr->u2Cksum = (UINT2) IP_HTONS (u4Sum + (u4Sum >> IP_SIXTEEN));
    }

    u4TotNumIface = pIpMcastParams->pu4OIfList[0];

    if (u4TotNumIface >= IPIF_MAX_LOGICAL_IFACES)
    {
        return IP_FAILURE;
    }

    for (u4Iface = 0; u4Iface < u4TotNumIface; u4Iface++)
    {
        u4RtPort = pIpMcastParams->pu4OIfList[u4Iface + IP_TWO];
        /* Adding 2 becos 1 for the no of interfaces and
           the other for the incoming interface index. */

        if ((gIpGlobalInfo.Ipif_glbtab[u4RtPort].pIpCxt == NULL) ||
            (gIpGlobalInfo.Ipif_glbtab[u4RtPort].pIpCxt->u4ContextId
             != IP_DEFAULT_CONTEXT))
        {
            continue;
        }

        if ((INT2) ipif_is_ok_to_forward ((UINT2) u4RtPort) == FALSE)
        {
            /* Forwarding is disabled or this interface is not up */
            IPIF_INC_OUT_GEN_ERR (u4RtPort);
            pIpCxt->Ip_stats.u4Out_gen_err++;
            continue;
        }
        pOutBuf = NULL;
        if (IpDuplicateBuffer (pBuf, &pOutBuf, Ip.u2Len) == IP_SUCCESS)
        {
            if ((INT2)
                IpOutput ((UINT2) u4RtPort, Ip.u4Dest, &Ip, pOutBuf,
                          IP_MCAST) == IP_FAILURE)
            {

                IP_RELEASE_BUF (pOutBuf, FALSE);
            }
        }
    }

    /* Free the malloc'ed OifList and the incoming Buffer */

    UtlShMemFreeOifList (pIpMcastParams->pu4OIfList);
    IP_RELEASE_BUF (pBuf, FALSE);
    return IP_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : IpFwdProtocolLock
 *
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Takes a task Sempaphore  
+-------------------------------------------------------------------*/

INT4
IpFwdProtocolLock (VOID)
{
    if (OsixSemTake (gIpGlobalInfo.IpProtSemId) != OSIX_SUCCESS)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : IpFwdProtocolUnLock
 *
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Takes a task Sempaphore  
+-------------------------------------------------------------------*/

INT4
IpFwdProtocolUnlock (VOID)
{
    OsixSemGive (gIpGlobalInfo.IpProtSemId);
    return IP_SUCCESS;
}

INT4
IpFwdDataLock (VOID)
{
    if (OsixSemTake (gIpGlobalInfo.IpDataSemId) != OSIX_SUCCESS)
    {
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

INT4
IpFwdDataUnlock (VOID)
{
    OsixSemGive (gIpGlobalInfo.IpDataSemId);
    return IP_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : IpFwdCxtLock
 *
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Takes a task Sempaphore  
+-------------------------------------------------------------------*/

INT4
IpFwdCxtLock (VOID)
{
    if (OsixSemTake (gIpGlobalInfo.IpCxtSemId) != OSIX_SUCCESS)
    {
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : IpFwdCxtUnLock
 *
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Takes a task Sempaphore  
+-------------------------------------------------------------------*/

INT4
IpFwdCxtUnlock (VOID)
{
    OsixSemGive (gIpGlobalInfo.IpCxtSemId);
    return IP_SUCCESS;
}

/* --------------------------------------------------------------------+
 * Function           : IpHLRegLock
 *
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Takes IP Higher Layer Registration Semaphore
+---------------------------------------------------------------------*/

INT4
IpHLRegLock (VOID)
{
    if (OsixSemTake (gIpGlobalInfo.IpHLRegSemId) != OSIX_SUCCESS)
    {
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/* ---------------------------------------------------------------------+
 * Function           : IpHLRegUnLock
 *
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Releases IP Higher Layer Registration Semaphore
+----------------------------------------------------------------------*/

INT4
IpHLRegUnLock (VOID)
{
    OsixSemGive (gIpGlobalInfo.IpHLRegSemId);
    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : IpDuplicateBuffer
 *
 * Input(s)           : pBuf - Pointer to the cru buffer which needs
 *                             to be duplicated
 *                    : u4PktSize - Size of the cru buffer to be duplicated
 *
 * Output(s)          : pDupBuf - Pointer to the duplicated buffer
 *
 * Returns            : IP_SUCCESS/IP_FAILURE
 *
 * Action :
 *    This function duplicates the data given in the CRU buffer "pBuf"
 *    New cru buffer "pDupBuf" is allocated, and the data in the 
 *    pBuf is copied to the newly allocated CRU Buffer "pDupBuf".
 *
+-------------------------------------------------------------------*/
INT4
IpDuplicateBuffer (tCRU_BUF_CHAIN_HEADER * pBuf,
                   tCRU_BUF_CHAIN_HEADER ** pDupBuf, UINT4 u4PktSize)
{
    UINT1              *pu1DataBuf = NULL;
    PRIVATE UINT1       au1DupBuf[(IPIF_MAX_MTU + IPIF_CUST_MAX_MTU) * IP_TWO];

    pu1DataBuf = au1DupBuf;
    MEMSET (pu1DataBuf, 0, ((IPIF_MAX_MTU + IPIF_CUST_MAX_MTU) * IP_TWO));

    /* Allocate the Buffer chain for the Duplicate buffer */
    if (((*pDupBuf) = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                                     IPIF_MAX_MTU + IPIF_CUST_MAX_MTU), 0)) == NULL)
    {
        return IP_FAILURE;
    }
    else
    {
        /* Copy from the pBuf to the Linear buffer */
        if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataBuf, 0, u4PktSize)
            == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);

            return IP_FAILURE;
        }
        else
        {
            /* Copy from linear buffer to the CRU buffer */
            if (CRU_BUF_Copy_OverBufChain
                ((*pDupBuf), pu1DataBuf, 0, u4PktSize) == CFA_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain ((*pDupBuf), FALSE);

                return IP_FAILURE;
            }
        }

    }

    return IP_SUCCESS;
}
