/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ippmtud.c,v 1.7 2013/07/04 13:10:59 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#include "ipinc.h"

/* Plateau table as in RFC 1191 */
static UINT2        gau2IpMtuPlateau[] =
    { 32000, 17914, 8166, 4352, 2002, 1492, 1006, 508, 296 };
extern UINT4        FsMIFsIpPmtuEntryStatus[12];

/*
*+---------------------------------------------------------------------+
*| Function Name : PmtuInitInCxt                                       |
*|                                                                     |
*| Description   : Initialises Path MTU discovery in the specified     |  
*|                 context                                             |  
*|                                                                     |  
*| Input         : pIpCxt - the ip context structure in which pmtu is  |
*|                 getting enabled                                     |  
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : IP_SUCCESS / IP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
INT4
PmtuInitInCxt (tIpCxt * pIpCxt)
#else
INT4
PmtuInitInCxt (pIpCxt)
     tIpCxt             *pIpCxt;
#endif
{

    pIpCxt->u2PmtuStatusFlag = PMTU_ENABLED;
    if (gIpGlobalInfo.u1PmtuEnableCount > 0)
    {
        /* PMTU is already enabled in some context. So no need to start the
         * timer.
         */
        gIpGlobalInfo.u1PmtuEnableCount++;
        return IP_SUCCESS;
    }

    /* PMTU is getting enabled in the first context. Start the garbage timer and
     * age timer.
     */
    gIpGlobalInfo.u1PmtuEnableCount++;
    gIpGlobalInfo.PmtuGarbageTimer.u1Id = IP_PMTU_TIMER_ID;
    gIpGlobalInfo.PmtuGarbageTimer.Timer_node.u4Data = PMTU_GRBG_TIMER;
    IP_START_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_GARBAGE_TIMER,
                    PMTU_GRBG_CLXN_INTVL);

    gIpGlobalInfo.PmtuAgeingTimer.u1Id = IP_PMTU_TIMER_ID;
    gIpGlobalInfo.PmtuAgeingTimer.Timer_node.u4Data = PMTU_AGEING_TIMER;
    IP_START_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_INCR_TIMER,
                    PMTU_SCAN_INTVL);

    IP_CXT_TRC (pIpCxt->u4ContextId, IP_MOD_TRC,
                INIT_SHUT_TRC | CONTROL_PLANE_TRC, IP_NAME,
                "Pmtu Init Complete.\n");
    return IP_SUCCESS;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : PmtuShutInCxt                                       |
*|                                                                     |
*| Description   : Closes PMTU-D operation in the router in the        |  
*|                 specified context.                                  |  
*|                                                                     |  
*| Input         : pIpCxt - pointer to the ip context structure in     |
*|                 which pmtu discovery is disabled.                   |  
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : IP_SUCCESS / IP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
INT4
PmtuShutInCxt (tIpCxt * pIpCxt)
#else
INT4
PmtuShutInCxt (pIpCxt)
     tIpCxt             *pIpCxt;
#endif
{
    tPmtuInfo          *pPmtuEntry = NULL;
    tPmtuInfo          *pNextPmtuEntry = NULL;
    tPmtuInfo           DummyPmtuEntry;

    MEMSET (&DummyPmtuEntry, 0, sizeof (tPmtuInfo));

    /* Delete all the pmtu entries learnt in this context */
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry =
        RBTreeGetNext (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry, NULL);
    while (pPmtuEntry != NULL)
    {
        pNextPmtuEntry = RBTreeGetNext (gIpGlobalInfo.PmtuTblRoot, pPmtuEntry,
                                        NULL);
        if (pPmtuEntry->pIpCxt->u4ContextId != pIpCxt->u4ContextId)
        {
            /* No entry is present in this context. */
            break;
        }
        RBTreeRemove (gIpGlobalInfo.PmtuTblRoot, pPmtuEntry);

        /* Give indication to MSR to remove the stored pmtu entry */
        IncMsrForFsIpv4PathMtuTable ((INT4) pPmtuEntry->pIpCxt->u4ContextId,
                                     pPmtuEntry->u4PmtuDest,
                                     pPmtuEntry->u1PmtuTos,
                                     DESTROY,
                                     FsMIFsIpPmtuEntryStatus,
                                     (sizeof (FsMIFsIpPmtuEntryStatus) /
                                      sizeof (UINT4)), TRUE);

        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.PmtuPoolId,
                            (UINT1 *) pPmtuEntry);
        pPmtuEntry = pNextPmtuEntry;
    }

    gIpGlobalInfo.u1PmtuEnableCount--;
    pIpCxt->u2PmtuStatusFlag = PMTU_DISABLED;
    if (0 == gIpGlobalInfo.u1PmtuEnableCount)
    {
        /* PMTU is disabled in all the contexts. So stop the global timers. 
         */
        IP_STOP_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_GARBAGE_TIMER);
        IP_STOP_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_INCR_TIMER);
    }

    IP_CXT_TRC (pIpCxt->u4ContextId, IP_MOD_TRC,
                INIT_SHUT_TRC | CONTROL_PLANE_TRC, IP_NAME, "Shut Complete\n");
    return IP_SUCCESS;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : PmtuLookupEntryInCxt                                |
*|                                                                     |
*| Description   : Search for Entry in PMTU RBTree                     |  
*|                                                                     |  
*| Input         : pIpCxt - pointer to IP context
*|                 u4IpDest - Destination IP address of the path       |
*|                 u1IpTos  - IP Type of Service of the path           |
*|                                                                     |  
*| Output        : None                                                |  
*|                                                                     |  
*| Returns       : NULL / tPmtuInfo *                                  |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
tPmtuInfo          *
PmtuLookupEntryInCxt (tIpCxt * pIpCxt, UINT4 u4IpDest, UINT1 u1IpTos)
#else
tPmtuInfo          *
PmtuLookupEntryInCxt (pIpCxt, u4IpDest, u1IpTos)
     tIpCxt             *pIpCxt;
     UINT4               u4IpDest;
     UINT1               u1IpTos;
#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;

    if (pIpCxt->u2PmtuStatusFlag != PMTU_ENABLED)
    {
        return NULL;
    }

    MEMSET (&DummyPmtuEntry, 0, sizeof (tPmtuInfo));
    DummyPmtuEntry.pIpCxt = pIpCxt;
    DummyPmtuEntry.u4PmtuDest = u4IpDest;
    DummyPmtuEntry.u1PmtuTos = u1IpTos;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);

    if (NULL == pPmtuEntry)
    {
        return NULL;
    }

    return pPmtuEntry;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : PmtuTimerExpiryHandler                              |
*|                                                                     |
*| Description   : For detecting Increase in PMTU and to remove        |  
*|                 entries to in-active paths.                         |  
*|                                                                     |
*| Input         : pTimer - Expired Timer Node.                        |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : None.                                               |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
VOID
PmtuTimerExpiryHandler (t_IP_TIMER * pTimer)
#else
VOID
PmtuTimerExpiryHandler (pTimer)
     t_IP_TIMER         *pTimer;
#endif
{
    tOsixSysTime        CurTime;
    tPmtuInfo          *pPmtuEntry = NULL;
    tPmtuInfo          *pNextPmtuEntry = NULL;
    INT2                i2Index = 0;

    switch (pTimer->Timer_node.u4Data)
    {
        case PMTU_GRBG_TIMER:
        {
            OsixGetSysTime (&CurTime);

            pPmtuEntry = RBTreeGetFirst (gIpGlobalInfo.PmtuTblRoot);
            while (pPmtuEntry != NULL)
            {
                pNextPmtuEntry = RBTreeGetNext (gIpGlobalInfo.PmtuTblRoot,
                                                pPmtuEntry, NULL);

                /* we should not delete the entries configured via SNMP
                 * so check if time stamp is PMTU_INFINITY.
                 */
                if ((pPmtuEntry->PmtuTstamp != PMTU_INFINITY) &&
                    (CurTime - pPmtuEntry->PmtuTstamp >= PMTU_GRBG_CLXN_INTVL))
                {

                    IP_CXT_TRC_ARG2 (pPmtuEntry->pIpCxt->u4ContextId,
                                     IP_MOD_TRC, CONTROL_PLANE_TRC, IP_NAME,
                                     "Deleting Inactive Entry Dest %u, Tos %d\n",
                                     pPmtuEntry->u4PmtuDest,
                                     pPmtuEntry->u1PmtuTos);

                    RBTreeRemove (gIpGlobalInfo.PmtuTblRoot, pPmtuEntry);
                    MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.PmtuPoolId,
                                        (UINT1 *) pPmtuEntry);
                }
                pPmtuEntry = pNextPmtuEntry;
            }

            gIpGlobalInfo.PmtuGarbageTimer.u1Id = IP_PMTU_TIMER_ID;
            gIpGlobalInfo.PmtuGarbageTimer.Timer_node.u4Data = PMTU_GRBG_TIMER;
            IP_START_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_GARBAGE_TIMER,
                            PMTU_GRBG_CLXN_INTVL);
            break;
        }

        case PMTU_AGEING_TIMER:
        {
            pPmtuEntry = RBTreeGetFirst (gIpGlobalInfo.PmtuTblRoot);
            while (pPmtuEntry != NULL)
            {
                pNextPmtuEntry = RBTreeGetNext (gIpGlobalInfo.PmtuTblRoot,
                                                pPmtuEntry, NULL);

                if (pPmtuEntry->u1PmtuAge != (UINT1) PMTU_INFINITY)
                {
                    if (pPmtuEntry->u1PmtuAge < PMTU_SCAN_INTVL)
                    {
                        pPmtuEntry->u1PmtuAge = 0;
                    }
                    else
                    {
                        pPmtuEntry->u1PmtuAge =
                            (UINT1) (pPmtuEntry->u1PmtuAge - PMTU_SCAN_INTVL);
                    }
                    if (pPmtuEntry->u1PmtuAge == 0)
                    {

                        for (i2Index = IP_PMTU_PLATEAU_SIZE - 1; i2Index >= 0;
                             i2Index--)
                        {
                            if (gau2IpMtuPlateau[i2Index] > pPmtuEntry->u4PMtu)
                            {
                                break;
                            }
                        }

                        /* If the MTU is > 32000, let the PMTU is 32000 */
                        if (i2Index < 0)
                        {
                            i2Index = 0;
                        }
                        IP_CXT_TRC_ARG4 (pPmtuEntry->pIpCxt->u4ContextId,
                                         IP_MOD_TRC, CONTROL_PLANE_TRC, IP_NAME,
                                         "Increasing PMTU Estimate for Dest %u, "
                                         "Tos %d \n MTU Old  %d, New %d \n",
                                         pPmtuEntry->u4PmtuDest,
                                         pPmtuEntry->u1PmtuTos,
                                         pPmtuEntry->u4PMtu,
                                         gau2IpMtuPlateau[i2Index]);

                        pPmtuEntry->u4PMtu = gau2IpMtuPlateau[i2Index];
                        pPmtuEntry->u1PmtuAge =
                            (UINT1) (pPmtuEntry->pIpCxt->u2PmtuCfgEntryAge / 2);

                        /* Call the fuction provided by TCP to handle the
                         * change in MTU */
#ifdef SLI_WANTED
                        SliHandleChangeInPmtuInCxt (pPmtuEntry->pIpCxt->
                                                    u4ContextId,
                                                    pPmtuEntry->u4PmtuDest,
                                                    pPmtuEntry->u1PmtuTos,
                                                    (UINT2) (pPmtuEntry->
                                                             u4PMtu -
                                                             IP_HDR_LEN));
#endif
                    }
                }
                else
                {
                    /*
                     * If configure age is not infinity this would start 
                     * detection in increase of PMTU. This should be done only for
                     * entries which are not configured via SNMP.
                     */
                    if (pPmtuEntry->PmtuTstamp != PMTU_INFINITY)
                    {
                        pPmtuEntry->u1PmtuAge =
                            (UINT1) (pPmtuEntry->pIpCxt->u2PmtuCfgEntryAge);
                    }

                }

                pPmtuEntry = pNextPmtuEntry;
            }
            gIpGlobalInfo.PmtuAgeingTimer.u1Id = IP_PMTU_TIMER_ID;
            gIpGlobalInfo.PmtuAgeingTimer.Timer_node.u4Data = PMTU_AGEING_TIMER;
            IP_START_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_INCR_TIMER,
                            PMTU_SCAN_INTVL);

            break;
        }

        default:
            return;
    }
}

/*
*+---------------------------------------------------------------------+
*| Function Name : PmtuDeleteEntry                                     |
*|                                                                     |
*| Description   : Deletes and releases the memory for the entry.      |  
*|                                                                     |
*| Input         : pHashEntry - Entry to be deleted.                   |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : None.                                               |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
VOID
PmtuDeleteEntry (tPmtuInfo * pPmtuEntry)
#else
VOID
PmtuDeleteEntry (pPmtuEntry)
     tPmtuInfo          *pPmtuEntry;
#endif
{
    RBTreeRemove (gIpGlobalInfo.PmtuTblRoot, pPmtuEntry);
    MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.PmtuPoolId, (UINT1 *) pPmtuEntry);
}

/*
*+---------------------------------------------------------------------+
*| Function Name : PmtuEstimateMtu                                     |
*|                                                                     |
*| Description   : Makes an estimate of the PMTU. This function is     |  
*|                 called when DTB message does not contain the MTU    |  
*|                 information.                                        |  
*|                                                                     |  
*| Input         : Length of the packet dropped. u2OldMtu              |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : New estimate of the PMTU.                           |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
#ifdef __STDC__
UINT2
PmtuEstimateMtu (UINT2 u2OldMtu)
#else
UINT2
PmtuEstimateMtu (u2OldMtu)
     UINT2               u2OldMtu;
#endif
{
    UINT2               u2Index = 0;

    for (u2Index = 0;
         u2Index < sizeof (gau2IpMtuPlateau) / sizeof (gau2IpMtuPlateau[0]);
         u2Index++)
    {
        if (u2OldMtu > gau2IpMtuPlateau[u2Index])
        {
            return gau2IpMtuPlateau[u2Index];
        }
    }
    return PMTU_MIN_MTU;
    /* ^ 68 */
}

/*****************************************************************************/
/* Function Name      : PmtuRBTreeCmpFn                                      */
/*                                                                           */
/* Description        : This routine is used for comparing two keys used in  */
/*                      pmtu RB Tree.                                        */
/*                                                                           */
/* Input(s)           : pmtuEntry1   - Key 1                                 */
/*                      pmtuEntry2   - Key 2                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
PmtuRBTreeCmpFn (tRBElem * pElem1, tRBElem * pElem2)
{
    tPmtuInfo          *pPmtuEntry1 = pElem1;
    tPmtuInfo          *pPmtuEntry2 = pElem2;

    if (pPmtuEntry1->pIpCxt->u4ContextId < pPmtuEntry2->pIpCxt->u4ContextId)
    {
        return -1;
    }
    else if (pPmtuEntry1->pIpCxt->u4ContextId >
             pPmtuEntry2->pIpCxt->u4ContextId)
    {
        return 1;
    }

    if (pPmtuEntry1->u4PmtuDest < pPmtuEntry2->u4PmtuDest)
    {
        return -1;
    }
    else if (pPmtuEntry1->u4PmtuDest > pPmtuEntry2->u4PmtuDest)
    {
        return 1;
    }

    if (pPmtuEntry1->u1PmtuTos < pPmtuEntry2->u1PmtuTos)
    {
        return -1;
    }
    else if (pPmtuEntry1->u1PmtuTos > pPmtuEntry2->u1PmtuTos)
    {
        return 1;
    }

    return 0;
}
