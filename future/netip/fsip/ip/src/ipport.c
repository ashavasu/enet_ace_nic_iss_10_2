/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipport.c,v 1.11 2013/07/04 13:11:58 siva Exp $
 *
 * Description: System Resizing related functions. 
 *
 *******************************************************************/
#include "ipinc.h"
#include "fsmpipcli.h"

/****************************************************************************
 Function    :  nmhGetFsNoOfStaticRoutes
 Input       :  The Indices

                The Object
                retValFsNoOfStaticRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsNoOfStaticRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsNoOfStaticRoutes (INT4 *pi4RetValFsNoOfStaticRoutes)
#else
INT1
nmhGetFsNoOfStaticRoutes (pi4RetValFsNoOfStaticRoutes)
     INT4               *pi4RetValFsNoOfStaticRoutes;
#endif
{
    tIpSystemSize       IpSystemSize;
    GetIpSizingParams (&IpSystemSize);

    *pi4RetValFsNoOfStaticRoutes = (INT4) IpSystemSize.u4IpMaxStaticRoutes;

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsNoOfAggregatedRoutes
 Input       :  The Indices

                The Object
                retValFsNoOfAggregatedRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsNoOfAggregatedRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsNoOfAggregatedRoutes (INT4 *pi4RetValFsNoOfAggregatedRoutes)
#else
INT1
nmhGetFsNoOfAggregatedRoutes (pi4RetValFsNoOfAggregatedRoutes)
     INT4               *pi4RetValFsNoOfAggregatedRoutes;
#endif
{
    *pi4RetValFsNoOfAggregatedRoutes =
        (INT4) FsIPSizingParams[0].u4PreAllocatedUnits;

    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsNoOfRoutes
 Input       :  The Indices

                The Object
                retValFsNoOfRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsNoOfRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsNoOfRoutes (INT4 *pi4RetValFsNoOfRoutes)
#else
INT1
nmhGetFsNoOfRoutes (pi4RetValFsNoOfRoutes)
     INT4               *pi4RetValFsNoOfRoutes;
#endif
{
    tIpSystemSize       IpSystemSize;
    GetIpSizingParams (&IpSystemSize);

    *pi4RetValFsNoOfRoutes = (INT4) IpSystemSize.u4IpMaxRoutes;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsNoOfReassemblyLists
 Input       :  The Indices

                The Object 
                retValFsNoOfReassemblyLists
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsNoOfReassemblyLists ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsNoOfReassemblyLists (INT4 *pi4RetValFsNoOfReassemblyLists)
#else
INT1
nmhGetFsNoOfReassemblyLists (pi4RetValFsNoOfReassemblyLists)
     INT4               *pi4RetValFsNoOfReassemblyLists;
#endif
{
    tIpSystemSize       IpSystemSize;
    GetIpSizingParams (&IpSystemSize);

    *pi4RetValFsNoOfReassemblyLists = (INT4) IpSystemSize.u4IpMaxReasmLists;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsNoOfFragmentsPerList
 Input       :  The Indices

                The Object 
                retValFsNoOfFragmentsPerList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsNoOfFragmentsPerList ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsNoOfFragmentsPerList (INT4 *pi4RetValFsNoOfFragmentsPerList)
#else
INT1
nmhGetFsNoOfFragmentsPerList (pi4RetValFsNoOfFragmentsPerList)
     INT4               *pi4RetValFsNoOfFragmentsPerList;
#endif
{
    tIpSystemSize       IpSystemSize;
    GetIpSizingParams (&IpSystemSize);

    *pi4RetValFsNoOfFragmentsPerList = (INT4) IpSystemSize.u4IpMaxFragPerList;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsNoOfStaticRoutes
 Input       :  The Indices

                The Object 
                setValFsNoOfStaticRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsNoOfStaticRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsNoOfStaticRoutes (INT4 i4SetValFsNoOfStaticRoutes)
#else
INT1
nmhSetFsNoOfStaticRoutes (i4SetValFsNoOfStaticRoutes)
     INT4                i4SetValFsNoOfStaticRoutes;

#endif
{
    tIpSystemSize       IpSystemSizeInfo;

    /* Get the Previous Value */
    GetIpSizingParams (&IpSystemSizeInfo);

    /* Modify the No. of Static Routes */
    IpSystemSizeInfo.u4IpMaxStaticRoutes = (UINT4) i4SetValFsNoOfStaticRoutes;

    /* Set the values */
    SetIpSizingParams (&IpSystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNoOfAggregatedRoutes
 Input       :  The Indices

                The Object 
                setValFsNoOfAggregatedRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsNoOfAggregatedRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsNoOfAggregatedRoutes (INT4 i4SetValFsNoOfAggregatedRoutes)
#else
INT1
nmhSetFsNoOfAggregatedRoutes (i4SetValFsNoOfAggregatedRoutes)
     INT4                i4SetValFsNoOfAggregatedRoutes;

#endif
{
    FsIPSizingParams[0].u4PreAllocatedUnits =
        (UINT4) i4SetValFsNoOfAggregatedRoutes;

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsNoOfAggregatedRoutes = %d\n", i4SetValFsNoOfAggregatedRoutes); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsNoOfRoutes
 Input       :  The Indices

                The Object 
                setValFsNoOfRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsNoOfRoutes (INT4 i4SetValFsNoOfRoutes)
#else
INT1
nmhSetFsNoOfRoutes (i4SetValFsNoOfRoutes)
     INT4                i4SetValFsNoOfRoutes;
#endif
{
    tIpSystemSize       IpSystemSizeInfo;

    /* Get the Previous Value */
    GetIpSizingParams (&IpSystemSizeInfo);

    /* Modify the No. of Static Routes */
    IpSystemSizeInfo.u4IpMaxRoutes = (UINT4) i4SetValFsNoOfRoutes;

    /* Set the values */
    SetIpSizingParams (&IpSystemSizeInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsNoOfReassemblyLists
 Input       :  The Indices

                The Object 
                setValFsNoOfReassemblyLists
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsNoOfReassemblyLists ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsNoOfReassemblyLists (INT4 i4SetValFsNoOfReassemblyLists)
#else
INT1
nmhSetFsNoOfReassemblyLists (i4SetValFsNoOfReassemblyLists)
     INT4                i4SetValFsNoOfReassemblyLists;
#endif
{
    tIpSystemSize       IpSystemSizeInfo;

    /* Get the Previous Value */
    GetIpSizingParams (&IpSystemSizeInfo);

    /* Modify the No. of Static Routes */
    IpSystemSizeInfo.u4IpMaxReasmLists = (UINT4) i4SetValFsNoOfReassemblyLists;

    /* Set the values */
    SetIpSizingParams (&IpSystemSizeInfo);

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsNoOfReassemblyLists = %d\n", i4SetValFsNoOfReassemblyLists); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsNoOfFragmentsPerList
 Input       :  The Indices

                The Object 
                setValFsNoOfFragmentsPerList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsNoOfFragmentsPerList ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsNoOfFragmentsPerList (INT4 i4SetValFsNoOfFragmentsPerList)
#else
INT1
nmhSetFsNoOfFragmentsPerList (i4SetValFsNoOfFragmentsPerList)
     INT4                i4SetValFsNoOfFragmentsPerList;
#endif
{
    tIpSystemSize       IpSystemSizeInfo;

    /* Get the Previous Value */
    GetIpSizingParams (&IpSystemSizeInfo);

    /* Modify the No. of Static Routes */
    IpSystemSizeInfo.u4IpMaxFragPerList =
        (UINT4) i4SetValFsNoOfFragmentsPerList;

    /* Set the values */
    SetIpSizingParams (&IpSystemSizeInfo);

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (ENTRY, "FsNoOfFragmentsPerList = %d\n", i4SetValFsNoOfFragmentsPerList); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsNoOfStaticRoutes
 Input       :  The Indices

                The Object 
                testValFsNoOfStaticRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsNoOfStaticRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsNoOfStaticRoutes (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsNoOfStaticRoutes)
#else
INT1
nmhTestv2FsNoOfStaticRoutes (*pu4ErrorCode, i4TestValFsNoOfStaticRoutes)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsNoOfStaticRoutes;
#endif
{
    tIpSystemSize       IpSystemSizeInfo;

    /* Get the Previous Value */
    GetIpSizingParams (&IpSystemSizeInfo);

    if ((i4TestValFsNoOfStaticRoutes < IP_MIN_STATIC_ROUTES) ||
        (i4TestValFsNoOfStaticRoutes > (INT4) IpSystemSizeInfo.u4IpMaxRoutes))
    {
        IP_TRC (IP_MOD_TRC, MGMT_TRC, IP_NAME,
                "No Of Static routes should be "
                "greater than Minimum value (20) \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsNoOfAggregatedRoutes
 Input       :  The Indices

                The Object 
                testValFsNoOfAggregatedRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsNoOfAggregatedRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsNoOfAggregatedRoutes (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsNoOfAggregatedRoutes)
#else
INT1
nmhTestv2FsNoOfAggregatedRoutes (*pu4ErrorCode, i4TestValFsNoOfAggregatedRoutes)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsNoOfAggregatedRoutes;
#endif
{
    if ((i4TestValFsNoOfAggregatedRoutes < IP_MIN_AGGREGATED_ROUTES) ||
        (i4TestValFsNoOfAggregatedRoutes >
         (INT4) FsIPSizingParams[0].u4PreAllocatedUnits))
    {
        IP_TRC (IP_MOD_TRC, MGMT_TRC, IP_NAME,
                "No Of Aggregated routes should be "
                "greater than Minimum value (5) \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsNoOfRoutes
 Input       :  The Indices

                The Object 
                testValFsNoOfRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsNoOfRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsNoOfRoutes (UINT4 *pu4ErrorCode, INT4 i4TestValFsNoOfRoutes)
#else
INT1
nmhTestv2FsNoOfRoutes (*pu4ErrorCode, i4TestValFsNoOfRoutes)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsNoOfRoutes;
#endif
{
    tIpSystemSize       IpSystemSizeInfo;

    /* Get the Previous Value */
    GetIpSizingParams (&IpSystemSizeInfo);

    if ((i4TestValFsNoOfRoutes < (INT4) IpSystemSizeInfo.u4IpMaxAggregateRoutes)
        || (i4TestValFsNoOfRoutes <
            (INT4) IpSystemSizeInfo.u4IpMaxStaticRoutes))
    {
        IP_TRC (IP_MOD_TRC, MGMT_TRC, IP_NAME,
                "No Of routing table entries should be "
                "greater than Minimum value (500) \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsNoOfReassemblyLists
 Input       :  The Indices

                The Object 
                testValFsNoOfReassemblyLists
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsNoOfReassemblyLists ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsNoOfReassemblyLists (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsNoOfReassemblyLists)
#else
INT1
nmhTestv2FsNoOfReassemblyLists (*pu4ErrorCode, i4TestValFsNoOfReassemblyLists)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsNoOfReassemblyLists;
#endif
{
    if ((i4TestValFsNoOfReassemblyLists > IP_MAX_REASSEMBLY_LISTS) ||
        (i4TestValFsNoOfReassemblyLists <= 0))
    {
        IP_TRC (IP_MOD_TRC, MGMT_TRC, IP_NAME,
                "No Of reassembly lists should not be "
                "greater than Maximum value (100) \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsNoOfFragmentsPerList
 Input       :  The Indices

                The Object 
                testValFsNoOfFragmentsPerList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsNoOfFragmentsPerList ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsNoOfFragmentsPerList (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsNoOfFragmentsPerList)
#else
INT1
nmhTestv2FsNoOfFragmentsPerList (*pu4ErrorCode, i4TestValFsNoOfFragmentsPerList)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsNoOfFragmentsPerList;
#endif
{
    if ((i4TestValFsNoOfFragmentsPerList > IP_MAX_FRAGMENTS_PER_LIST) ||
        (i4TestValFsNoOfFragmentsPerList < 0))
    {
        IP_TRC (IP_MOD_TRC, MGMT_TRC, IP_NAME,
                "No Of Fragments per lists should not be "
                "greater than Maximum value (50) \n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsIpGlobalDebug
 *  Input       :  The Indices
 *   
 *                 The Object
 *                     retValFsIpGlobalDebug
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpGlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpGlobalDebug (INT4 *pi4RetValFsIpGlobalDebug)
#else
INT1
nmhGetFsIpGlobalDebug (pi4RetValFsIpGlobalDebug)
     INT4               *pi4RetValFsIpGlobalDebug;
#endif
{
    *pi4RetValFsIpGlobalDebug = (INT4) gIpGlobalInfo.u4IpDbg;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 *  Function    :  nmhSetFsIpGlobalDebug
 *  Input       :  The Indices
 *   
 *                   The Object
 *                   setValFsIpGlobalDebug
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpGlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpGlobalDebug (INT4 i4SetValFsIpGlobalDebug)
#else
INT1
nmhSetFsIpGlobalDebug (i4SetValFsIpGlobalDebug)
     INT4                i4SetValFsIpGlobalDebug;
#endif
{
    INT1                i1RetVal = SNMP_FAILURE;
    gIpGlobalInfo.u4IpDbg = (UINT4) i4SetValFsIpGlobalDebug;
    ArpSetArpDebug (VCM_INVALID_VC, gIpGlobalInfo.u4IpDbg);
    PingEnableDebug (gIpGlobalInfo.u4IpDbg);
    i1RetVal = IncMsrForFsIpv4Scalars (i4SetValFsIpGlobalDebug,
                                       FsMIFsIpGlobalDebug,
                                       (sizeof (FsMIFsIpGlobalDebug) /
                                        sizeof (UINT4)));

    if (i1RetVal == IP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 *  Function    :  nmhTestv2FsIpGlobalDebug
 *  Input       :  The Indices
 *   
 *                   The Object
 *                       testValFsIpGlobalDebug
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpGlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpGlobalDebug (UINT4 *pu4ErrorCode, INT4 i4TestValFsIpGlobalDebug)
#else
INT1
nmhTestv2FsIpGlobalDebug (*pu4ErrorCode, i4TestValFsIpGlobalDebug)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsIpGlobalDebug;
#endif
{
    UNUSED_PARAM (i4TestValFsIpGlobalDebug);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsNoOfStaticRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsNoOfStaticRoutes (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNoOfAggregatedRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsNoOfAggregatedRoutes (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNoOfRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsNoOfRoutes (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNoOfReassemblyLists
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsNoOfReassemblyLists (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsNoOfFragmentsPerList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsNoOfFragmentsPerList (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpGlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
