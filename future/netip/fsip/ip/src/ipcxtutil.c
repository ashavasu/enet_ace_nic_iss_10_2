/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcxtutil.c,v 1.12 2016/02/27 10:14:48 siva Exp $
 *
 * Description: This file contains the various utilities to retrieve  
 *             the IP context structure.                              
 *
 *******************************************************************/

#include "ipinc.h"
#include  "rmgr.h"
#include "fsmpipcli.h"

PRIVATE VOID        IpClearCidrAdvTable (UINT4 u4ContextId);
PRIVATE VOID        IpClearPmtuTable (UINT4 u4ContextId);
PRIVATE VOID        IpClearCidrAggTable (UINT4 u4ContextId);
PRIVATE VOID        IpClearIcmpContextInfo (UINT4 u4ContextId);

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpGetCxt                                               */
/*                                                                           */
/* Description  : This function finds and returns the pointer to the         */
/*                IP context structure for the given context ID              */
/*                                                                           */
/* Input        :  u4ContextId : The ip context identifier.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
tIpCxt             *
UtilIpGetCxt (UINT4 u4ContextId)
{
    if (u4ContextId >= IP_SIZING_CONTEXT_COUNT)
    {
        return NULL;
    }
    if ((UtilIpVcmIsVcExist (u4ContextId) == IP_FAILURE) ||
        (gIpGlobalInfo.apIpCxt[u4ContextId] == NULL))
    {
        return NULL;
    }
    return (gIpGlobalInfo.apIpCxt[u4ContextId]);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpGetCxtFromIpPortNum                                  */
/*                                                                           */
/* Description  : This function finds and returns the pointer to the         */
/*                IP context structure for the given IP interface index      */
/*                                                                           */
/* Input        :  u4IpPort    : The ip port number                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
tIpCxt             *
UtilIpGetCxtFromIpPortNum (UINT4 u4IpPort)
{
    UINT4               u4CxtId = 0;

    if (u4IpPort >= IPIF_MAX_LOGICAL_IFACES)
    {
        return NULL;
    }
    if (NULL == gIpGlobalInfo.Ipif_glbtab[u4IpPort].pIpCxt)
    {
        return NULL;
    }

    u4CxtId = gIpGlobalInfo.Ipif_glbtab[u4IpPort].pIpCxt->u4ContextId;

    if (UtilIpVcmIsVcExist (u4CxtId) == IP_FAILURE)
    {
        return NULL;
    }

    return (gIpGlobalInfo.Ipif_glbtab[u4IpPort].pIpCxt);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpVcmIsVcExist                                         */
/*                                                                           */
/* Description  : This function finds and checks whether the given           */
/*               context ID is valid and available in VCM                    */
/*                                                                           */
/* Input        : u4ContextId  : IP context id                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP_SUCCESS/IP_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
UtilIpVcmIsVcExist (UINT4 u4ContextId)
{
    if (VcmGetSystemMode (IPFWD_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
        {
            return IP_FAILURE;
        }
        return IP_SUCCESS;
    }
    else
    {
        if (u4ContextId == IP_DEFAULT_CONTEXT)
        {
            return IP_SUCCESS;
        }
        return IP_FAILURE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpSetCxt                                               */
/*                                                                           */
/* Description  : This function sets the global context variable             */
/*                                                                           */
/* Input        :  u4ContextId : The ip context identifier.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
INT4
UtilIpSetCxt (UINT4 u4ContextId)
{
    return (UtilIpvxSetCxt (u4ContextId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIpReleaseCxt                                           */
/*                                                                           */
/* Description  : This function clear the global context variable            */
/*                                                                           */
/* Input        :  u4ContextId : The context identifier.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to IP  context structure, if context is found      */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
VOID
UtilIpReleaseCxt (VOID)
{
    UtilIpvxReleaseCxt ();
    return;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4GlobalTable

 Input       :  i4ContextId - IP context id.
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                FsMIFsIpGlobalTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4GlobalTable (INT4 i4ContextId,
                            INT4 i4SetVal, UINT4 *pu4ObjectId,
                            UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4ContextId, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (i4SetVal);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4ForScalars

 Input       :  i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID

 Description :  This function performs Inc Msr functionality for
                FsIpv4 scalars.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4Scalars (INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IP_ZERO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetVal));

#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4SetVal);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4TraceConfigTable

 Input       :  u4IpTraceConfigDest - Destination address
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                TraceConfigTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4TraceConfigTable (UINT4 u4IpTraceConfigDest,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %i", u4IpTraceConfigDest, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4IpTraceConfigDest);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4RtrLstTable

 Input       :  u4RtrLstAddress - Router Address
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                FsIpv4RtrLstTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4RtrLstTable (UINT4 u4RtrLstAddress,
                            INT4 i4SetVal, UINT4 *pu4ObjectId,
                            UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %i", u4RtrLstAddress, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4RtrLstAddress);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4AddressTable

 Input       :  u4TabAddress - Router Address
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsIpAddressTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4AddressTable (UINT4 u4TabAddress,
                             INT4 i4SetVal, UINT4 *pu4ObjectId,
                             UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p %i", u4TabAddress, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4TabAddress);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4PathMtuTable

 Input       :  i4ContextId - VRF Id
                u4PmtuDestination - Destination Ip
                i4PmtuTos - Type of service
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsIpPathMtuTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4PathMtuTable (INT4 i4ContextId, UINT4 u4PmtuDestination,
                             INT4 i4PmtuTos,
                             INT4 i4SetVal, UINT4 *pu4ObjectId,
                             UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i",
                      i4ContextId, u4PmtuDestination, i4PmtuTos, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4PmtuDestination);
    UNUSED_PARAM (i4PmtuTos);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4CommonRoutingTable

 Input       :  i4ContextId - VRF Id
                u4RouteDest - Destination Ip
                u4RouteMask - Mask
                i4RouteTos  - Type of service
                u4RouteNextHop - Next Hop Address
                RouteProto - Route Protocol
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsIpCommonRoutingTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4CommonRoutingTable (UINT4 i4ContextId, UINT4 u4RouteDest,
                                   UINT4 u4RouteMask, INT4 i4RouteTos,
                                   UINT4 u4RouteNextHop, INT4 RouteProto,
                                   INT4 i4SetVal, UINT4 *pu4ObjectId,
                                   UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_SIX;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p %i %i",
                      i4ContextId, u4RouteDest, u4RouteMask, i4RouteTos,
                      u4RouteNextHop, RouteProto, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4RouteDest);
    UNUSED_PARAM (u4RouteMask);
    UNUSED_PARAM (i4RouteTos);
    UNUSED_PARAM (u4RouteNextHop);
    UNUSED_PARAM (RouteProto);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4IpifTable

 Input       :  i4IpifIndex - Ip interface index
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsIpifTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4IpifTable (INT4 i4IpifIndex,
                          INT4 i4SetVal, UINT4 *pu4ObjectId,
                          UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IpifIndex, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (i4IpifIndex);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4IcmpGlobalTable

 Input       :  i4ContextId - VRF index
                cDatatype - a flag to determine whether
                            integer or string value is passed
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsIcmpGlobalTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/

INT1
IncMsrForIpv4IcmpGlobalTable (INT4 i4ContextId, CHR1 cDatatype,
                              VOID *pSetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                              UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    tSNMP_OCTET_STRING_TYPE *pSetString = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4ContextId, i4SetValue));
            break;
        case 's':
            pSetString = (tSNMP_OCTET_STRING_TYPE *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s",
                              i4ContextId, pSetString));
            break;
        default:
            break;
    }
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4ContextId);
#endif

    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4CidrAggTable

 Input       :  i4ContextId - VRF Id
                u4AggAddress - Aggregate Ip
                u4AddressMask - Aggregate mask
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsIpCidrAggTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4CidrAggTable (INT4 i4ContextId, UINT4 u4AggAddress,
                             UINT4 u4AddressMask,
                             INT4 i4SetVal, UINT4 *pu4ObjectId,
                             UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      i4ContextId, u4AggAddress, u4AddressMask, i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4AggAddress);
    UNUSED_PARAM (u4AddressMask);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4CidrAdvertTable

 Input       :  i4ContextId - VRF Id
                u4AggAddress - Aggregate Ip
                u4AddressMask - Aggregate mask
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsCidrAdvertTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/
INT1
IncMsrForFsIpv4CidrAdvertTable (INT4 i4ContextId, UINT4 u4AdvertAddress,
                                UINT4 u4AdvertAddressMask,
                                INT4 i4SetVal, UINT4 *pu4ObjectId,
                                UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IP_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      i4ContextId, u4AdvertAddress, u4AdvertAddressMask,
                      i4SetVal));
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (u4AdvertAddress);
    UNUSED_PARAM (u4AdvertAddressMask);
#endif
    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4IrdpIfConfTable

 Input       :  i4IrdpIfConfIfNum - IRDP Interface number
                IrdpIfConfSubref  - Sub reference
      
                cDatatype - a flag to determine whether
                            integer or string value is passed
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsIrdpIfConfTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/

INT1
IncMsrForFsIpv4IrdpIfConfTable (INT4 i4IrdpIfConfIfNum, INT4 IrdpIfConfSubref,
                                CHR1 cDatatype, VOID *pSetVal,
                                UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                                UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    UINT4               u4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                              i4IrdpIfConfIfNum, IrdpIfConfSubref, i4SetValue));
            break;
        case 'p':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %p",
                              i4IrdpIfConfIfNum, IrdpIfConfSubref, u4SetValue));
            break;
        default:
            break;
    }
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (IrdpIfConfSubref);
    UNUSED_PARAM (i4IrdpIfConfIfNum);
#endif

    return IP_SUCCESS;
}

/****************************************************************************
 Function    :  IncMsrForFsIpv4RarpServerDatabaseTable

 Input       :  pHardwareAddress - HardwareAddress
                cDatatype - a flag to determine whether
                            integer or string value is passed
                i4SetVal - Value which should be stored.
                pu4ObjectId - Object ID.
                u4OIdLen - Object Id length.
                IsRowStatus - Rowstatus (TRUE/FALSE)

 Description :  This function performs Inc Msr functionality for
                fsMIFsRarpServerDatabaseTable table.

 Output      :  None.

 Returns     :  IP_SUCCESS or IP_FAILURE
****************************************************************************/

INT1
IncMsrForIpv4RarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE *
                                      pHardwareAddress, CHR1 cDatatype,
                                      VOID *pSetVal, UINT4 *pu4ObjectId,
                                      UINT4 u4OIdLen, UINT1 IsRowStatus)
{
    UINT4               u4SeqNum = IPVX_ZERO;
    INT4                i4SetValue = IPVX_ZERO;
    UINT4               u4SetValue = IPVX_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OIdLen;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = IsRowStatus;
    SnmpNotifyInfo.pLockPointer = NULL;
    SnmpNotifyInfo.pUnLockPointer = NULL;
    SnmpNotifyInfo.u4Indices = IPVX_ONE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    switch (cDatatype)
    {
        case 'i':
            i4SetValue = *(INT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i",
                              pHardwareAddress, i4SetValue));
            break;
        case 'p':
            u4SetValue = *(UINT4 *) pSetVal;
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %p",
                              pHardwareAddress, u4SetValue));
            break;
        default:
            break;
    }
#ifndef ISS_WANTED
    /* To avoid Warnings in stack Compilation */
    UNUSED_PARAM (pHardwareAddress);
#endif

    return IP_SUCCESS;
}

/****************************************************************************
* Function     : IpUtilGetTraceFlag     
*                                            
* Description  : This function to get DebugTrace value  for the given context.
*
* Input        : u4IpCxtId - The context Id 
*
* Output       : None                         
*
* Returns      : Configured trace value for the given context.                      
*
***************************************************************************/

UINT4
IpUtilGetTraceFlag (UINT4 u4IpCxtId)
{
    if (u4IpCxtId == VCM_INVALID_VC)
    {
        return (gIpGlobalInfo.u4IpDbg);
    }

    if (u4IpCxtId >= IP_SIZING_CONTEXT_COUNT)
    {
        return 0;
    }

    if ((UtilIpVcmIsVcExist (u4IpCxtId) == IP_FAILURE) ||
        (gIpGlobalInfo.apIpCxt[u4IpCxtId] == NULL))
    {
        return 0;
    }
    return (gIpGlobalInfo.apIpCxt[u4IpCxtId]->u4IpDbg);

}

#ifndef ARP_WANTED
/****************************************************************************
* Function     : ArpSetArpDebug     
*                                            
* Description  : Stub function for the configuring Arp Debug flag.
*
* Input        : u4ContextId - The context Id 
*                u4TrcValue  - The configured trace value.
*
* Output       : None                         
*
* Returns      : None                      
*
***************************************************************************/
VOID
ArpSetArpDebug (UINT4 u4ContextId, UINT4 u4TrcValue)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4TrcValue);
}
#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearContextInfo                                 */
/*                                                                           */
/*     DESCRIPTION      : This function clears the IP context based          */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IP_SUCCESS / IP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
IpClearContextInfo (UINT4 u4ContextId)
{
    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable ((INT4) u4ContextId)
        == SNMP_FAILURE)
    {
        return IP_FAILURE;
    }
    nmhSetFsMIFsIpOptProcEnable ((INT4) u4ContextId, IP_OPT_PROC_ENABLE);
    nmhSetFsMIFsIpNumMultipath ((INT4) u4ContextId, DEF_NUM_MULTIPATH);
    nmhSetFsMIFsIpLoadShareEnable ((INT4) u4ContextId, IP_LOADSHARE_DISABLE);
    nmhSetFsMIFsIpEnablePMTUD ((INT4) u4ContextId, PMTU_DISABLED);
    nmhSetFsMIFsIpPmtuEntryAge ((INT4) u4ContextId, (PMTU_DEF_AGE / 60));
    nmhSetFsMIFsIpContextDebug ((INT4) u4ContextId, IP_ZERO);
    IpClearIcmpContextInfo (u4ContextId);
    IpClearCidrAggTable (u4ContextId);
    IpClearCidrAdvTable (u4ContextId);
    IpClearPmtuTable (u4ContextId);
    IPFWD_PROT_UNLOCK ();
    IpvxClearIpContextEntries (u4ContextId);
    IPFWD_PROT_LOCK ();

    return IP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearIcmpContextInfo                             */
/*                                                                           */
/*     DESCRIPTION      : This function clears the ICMP context based        */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpClearIcmpContextInfo (UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE DomainName;
    UINT1               au1Name[ICMP_MAX_DOMAIN_NAME_SIZE];

    MEMSET (au1Name, IP_ZERO, ICMP_MAX_DOMAIN_NAME_SIZE);
    DomainName.pu1_OctetList = au1Name;
    DomainName.i4_Length = ICMP_MAX_DOMAIN_NAME_SIZE - 1;
    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable ((INT4) u4ContextId)
        == SNMP_FAILURE)
    {
        return;
    }
    nmhSetFsMIFsIcmpSendRedirectEnable ((INT4) u4ContextId,
                                        ICMP_SEND_REDIRECT_ENABLE);
    nmhSetFsMIFsIcmpSendUnreachableEnable ((INT4) u4ContextId,
                                           ICMP_SEND_UNREACHABLE_ENABLE);
    nmhSetFsMIFsIcmpSendEchoReplyEnable ((INT4) u4ContextId,
                                         ICMP_ECHO_REPLY_ENABLE);
    nmhSetFsMIFsIcmpNetMaskReplyEnable ((INT4) u4ContextId,
                                        ICMP_NMASK_REPLY_ENABLE);
    nmhSetFsMIFsIcmpTimeStampReplyEnable ((INT4) u4ContextId,
                                          ICMP_TIME_REPLY_ENABLE);
    nmhSetFsMIFsIcmpDirectQueryEnable ((INT4) u4ContextId,
                                       ICMP_DIRECTQUERY_DISABLE);
    nmhSetFsMIFsIcmpDomainName ((INT4) u4ContextId, &DomainName);
    nmhSetFsMIFsIcmpTimeToLive ((INT4) u4ContextId, ICMP_DEFAULT_TIME_TO_LIVE);
    nmhSetFsMIFsIcmpSendSecurityFailuresEnable ((INT4) u4ContextId,
                                                ICMP_SEND_SEC_FAILURE_ENABLE);
    nmhSetFsMIFsIcmpRecvSecurityFailuresEnable ((INT4) u4ContextId,
                                                ICMP_RECV_SEC_FAILURE_ENABLE);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearCidrAggTable                                */
/*                                                                           */
/*     DESCRIPTION      : This function clears the CIDR Aggregate Table      */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpClearCidrAggTable (UINT4 u4ContextId)
{
    INT4                i4NxtContextId;
    UINT4               u4IpAddress = 0;
    UINT4               u4NxtIpAddress = 0;
    UINT4               u4AggMask = 0;
    UINT4               u4NxtAggMask = 0;

    while (nmhGetNextIndexFsMIFsIpCidrAggTable ((INT4) u4ContextId,
                                                &i4NxtContextId, u4IpAddress,
                                                &u4NxtIpAddress, u4AggMask,
                                                &u4NxtAggMask) == SNMP_SUCCESS)
    {
        if ((INT4) u4ContextId < i4NxtContextId)
        {
            break;
        }
        u4IpAddress = u4NxtIpAddress;
        u4AggMask = u4NxtAggMask;
        nmhSetFsMIFsIpCidrAggStatus (i4NxtContextId, u4NxtIpAddress,
                                     u4NxtAggMask, IP_CIDR_AGGR_DISABLE);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearCidrAdvTable                                */
/*                                                                           */
/*     DESCRIPTION      : This function clears the CIDR Advertisement Table  */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpClearCidrAdvTable (UINT4 u4ContextId)
{
    INT4                i4NxtContextId;
    UINT4               u4IpAddress = 0;
    UINT4               u4NxtIpAddress = 0;
    UINT4               u4AdvMask = 0;
    UINT4               u4NxtAdvMask = 0;

    while (nmhGetNextIndexFsMIFsCidrAdvertTable ((INT4) u4ContextId,
                                                 &i4NxtContextId, u4IpAddress,
                                                 &u4NxtIpAddress, u4AdvMask,
                                                 &u4NxtAdvMask) == SNMP_SUCCESS)
    {
        if ((INT4) u4ContextId < i4NxtContextId)
        {
            break;
        }
        u4IpAddress = u4NxtIpAddress;
        u4AdvMask = u4NxtAdvMask;
        nmhSetFsMIFsCidrAdvertStatus (i4NxtContextId, u4NxtIpAddress,
                                      u4NxtAdvMask, IP_CIDR_EXPL_DISABLE);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearPmtuTable                                   */
/*                                                                           */
/*     DESCRIPTION      : This function clears the path MTU Table           */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IpClearPmtuTable (UINT4 u4ContextId)
{
    INT4                i4NxtContextId;
    UINT4               u4IpAddress = 0;
    UINT4               u4NxtIpAddress = 0;
    INT4                i4PmtuTos = 0;
    INT4                i4NxtPmtuTos = 0;

    while (nmhGetNextIndexFsMIFsIpPathMtuTable ((INT4) u4ContextId,
                                                &i4NxtContextId, u4IpAddress,
                                                &u4NxtIpAddress, i4PmtuTos,
                                                &i4NxtPmtuTos) == SNMP_SUCCESS)
    {
        if ((INT4) u4ContextId < i4NxtContextId)
        {
            break;
        }
        u4IpAddress = u4NxtIpAddress;
        i4PmtuTos = i4NxtPmtuTos;
        nmhSetFsMIFsIpPmtuEntryStatus (i4NxtContextId, u4NxtIpAddress,
                                       i4NxtPmtuTos, PMTU_DESTROY);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IpClearInterfaceInfo                               */
/*                                                                           */
/*     DESCRIPTION      : This function clears the IP interface based        */
/*                        configurations done for the given interface.       */
/*                                                                           */
/*     INPUT            : u4IfIndex  - Interface index.                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : IP_SUCCESS / IP_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
IpClearInterfaceInfo (UINT4 u4IfIndex)
{
    /* As the ip protocol lock will be taken inside the netipvx
     * nmh routines, the protocol lock is not taken here */
    IpvxClearIpInterfaceEntries (u4IfIndex);
    return IP_SUCCESS;
}

/*****************************************************************************/
/* Function           : Ipv4IsLoopbackAddress                                */
/*                      Checks the IP Address is a Loopback Address          */
/*                                                  */
/* Input(s)           :  u4ContextId - context id                            */
/*                       u4IpAddress - Ip Address                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                         */
/* Returns            : IP_SUCCESS - If u4IpAddress is Loopback              */
/*                                        address                            */
/*                      IP_FAILURE - If u4IpAddress is not                   */
/*                                        Loopback address                   */
/* Action             : Checks the IP Address is a Loopback Address          */
/*****************************************************************************/
INT4
Ipv4IsLoopbackAddress (UINT4 u4ContextId, UINT4 u4IpAddress)
{

    UINT4               u4CfaIfIndex = 0;
    tCfaIfInfo          CfaIfInfo;

    MEMSET ((UINT1 *) &CfaIfInfo, 0, sizeof (CfaIfInfo));

    /* Use the API provided for IP interface table and get the
     * CFA interface index */
    if (CfaIpIfGetIfIndexFromIpAddressInCxt (u4ContextId, u4IpAddress,
                                             &u4CfaIfIndex) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }

    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
    {
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/*****************************************************************************/
/* Function           : IpGetFsIpProxyArpSubnetOption                        */
/*                                                                           */
/*                                                                           */
/* Input(s)           :  u4ContextId - context id                            */
/*                       u4IpAddress - Ip Address                            */
/*                                                                           */
/* Output(s)          : pi4RetValFsIpProxyArpSubnetOption                    */
/*                                                                           */
/* Returns            : IP_SUCCESS                                           */
/*                                                                           */
/*                      IP_FAILURE                                           */
/*                                                                           */
/* Action             : To get The ProxyArpSubnetoption status               */
/*****************************************************************************/


INT1
IpGetFsIpProxyArpSubnetOption (INT4 *pi4RetValFsIpProxyArpSubnetOption)
{
    *pi4RetValFsIpProxyArpSubnetOption = gIpGlobalInfo.u1ProxyArp_SubnetOption;
    return IP_SUCCESS;
}


/*****************************************************************************/
/* Function           : IpSetFsIpProxyArpSubnetOption                        */
/*                                                                           */
/*                                                                           */
/* Input(s)           :                                                      */ 
/*                                                                           */
/* Output(s)          : 4SetValFsIpProxyArpSubnetOption                      */
/*                                                                           */
/* Returns            : IP_SUCCESS                                           */
/*                                                                           */
/*                      IP_FAILURE                                           */
/*                                                                           */
/* Action             : To Set The ProxyArpSubnetoption status               */
/*****************************************************************************/


INT1
IpSetFsIpProxyArpSubnetOption (INT4 i4SetValFsIpProxyArpSubnetOption)
{
    gIpGlobalInfo.u1ProxyArp_SubnetOption =
        (UINT1) i4SetValFsIpProxyArpSubnetOption;
    IncMsrForFsIpv4Scalars (i4SetValFsIpProxyArpSubnetOption,
                            FsMIFsIpProxyArpSubnetOption,
                            (sizeof (FsMIFsIpProxyArpSubnetOption) /
                             sizeof (UINT4)));
    return IP_SUCCESS;

}


/*****************************************************************************/
/* Function           : IpSetFsIpProxyArpSubnetOption                        */
/*                                                                           */
/*                                                                           */
/* Input(s)           : i4TestValFsIpProxyArpSubnetOption                    */
/*                                                                           */
/* Output(s)          : pu4ErrorCode                                         */
/*                                                                           */
/* Returns            : IP_SUCCESS                                           */
/*                                                                           */
/*                      IP_FAILURE                                           */
/*                                                                           */
/* Action             : To Test The ProxyArpSubnetoption status for a valid  */
/*                      Value                                                */
/*****************************************************************************/


INT1
IpTestv2FsIpProxyArpSubnetOption (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsIpProxyArpSubnetOption)
{
    if (((UINT1) i4TestValFsIpProxyArpSubnetOption != ARP_PROXY_ENABLE) &&
        ((UINT1) i4TestValFsIpProxyArpSubnetOption != ARP_PROXY_DISABLE))
    {
        /* Invalid Value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return IP_FAILURE;
    }

    return IP_SUCCESS;

}
