/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ippmtusn.c,v 1.11 2013/07/04 13:11:58 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#include "ipinc.h"
#include "fsmpipcli.h"
/****************************************************************************
 Function    :  nmhGetFsIpEnablePMTUD
 Input       :  The Indices

                The Object 
                retValFsIpEnablePMTUD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpEnablePMTUD ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpEnablePMTUD (INT4 *pi4RetValFsIpEnablePMTUD)
#else
INT1
nmhGetFsIpEnablePMTUD (pi4RetValFsIpEnablePMTUD)
     INT4               *pi4RetValFsIpEnablePMTUD;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsIpEnablePMTUD = pIpCxt->u2PmtuStatusFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpPmtuEntryAge
 Input       :  The Indices

                The Object 
                retValFsIpPmtuEntryAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpPmtuEntryAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpPmtuEntryAge (INT4 *pi4RetValFsIpPmtuEntryAge)
#else
INT1
nmhGetFsIpPmtuEntryAge (pi4RetValFsIpPmtuEntryAge)
     INT4               *pi4RetValFsIpPmtuEntryAge;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsIpPmtuEntryAge = pIpCxt->u2PmtuCfgEntryAge / PMTU_SCAN_INTVL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpPmtuTableSize
 Input       :  The Indices

                The Object 
                retValFsIpPmtuTableSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpPmtuTableSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpPmtuTableSize (INT4 *pi4RetValFsIpPmtuTableSize)
#else
INT1
nmhGetFsIpPmtuTableSize (pi4RetValFsIpPmtuTableSize)
     INT4               *pi4RetValFsIpPmtuTableSize;
#endif
{
    *pi4RetValFsIpPmtuTableSize = (INT4) IP_DEF_MAX_PMTU_ENTRY;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpEnablePMTUD
 Input       :  The Indices

                The Object 
                setValFsIpEnablePMTUD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpEnablePMTUD ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpEnablePMTUD (INT4 i4SetValFsIpEnablePMTUD)
#else
INT1
nmhSetFsIpEnablePMTUD (i4SetValFsIpEnablePMTUD)
     INT4                i4SetValFsIpEnablePMTUD;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsIpEnablePMTUD != pIpCxt->u2PmtuStatusFlag)
    {
        if (i4SetValFsIpEnablePMTUD == PMTU_ENABLED)
        {
            PmtuInitInCxt (pIpCxt);
        }
        else
        {
            PmtuShutInCxt (pIpCxt);
        }
        pIpCxt->u2PmtuStatusFlag = (UINT2) i4SetValFsIpEnablePMTUD;
    }

    IncMsrForFsIpv4GlobalTable ((INT4) u4ContextId, i4SetValFsIpEnablePMTUD,
                                FsMIFsIpEnablePMTUD,
                                (sizeof (FsMIFsIpEnablePMTUD) /
                                 sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpPmtuEntryAge
 Input       :  The Indices

                The Object 
                setValFsIpPmtuEntryAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpPmtuEntryAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpPmtuEntryAge (INT4 i4SetValFsIpPmtuEntryAge)
#else
INT1
nmhSetFsIpPmtuEntryAge (i4SetValFsIpPmtuEntryAge)
     INT4                i4SetValFsIpPmtuEntryAge;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    pIpCxt->u2PmtuCfgEntryAge =
        (UINT2) (i4SetValFsIpPmtuEntryAge * PMTU_SCAN_INTVL);
    IncMsrForFsIpv4GlobalTable ((INT4) u4ContextId, i4SetValFsIpPmtuEntryAge,
                                FsMIFsIpPmtuEntryAge,
                                (sizeof (FsMIFsIpPmtuEntryAge) /
                                 sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpPmtuTableSize
 Input       :  The Indices

                The Object 
                setValFsIpPmtuTableSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpPmtuTableSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpPmtuTableSize (INT4 i4SetValFsIpPmtuTableSize)
#else
INT1
nmhSetFsIpPmtuTableSize (i4SetValFsIpPmtuTableSize)
     INT4                i4SetValFsIpPmtuTableSize;

#endif
{
    UNUSED_PARAM (i4SetValFsIpPmtuTableSize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpEnablePMTUD
 Input       :  The Indices

                The Object 
                testValFsIpEnablePMTUD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpEnablePMTUD ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpEnablePMTUD (UINT4 *pu4ErrorCode, INT4 i4TestValFsIpEnablePMTUD)
#else
INT1
nmhTestv2FsIpEnablePMTUD (pu4ErrorCode, i4TestValFsIpEnablePMTUD)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsIpEnablePMTUD;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if ((i4TestValFsIpEnablePMTUD < PMTU_ENABLED) ||
        (i4TestValFsIpEnablePMTUD > PMTU_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (NULL == UtilIpGetCxt (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpPmtuEntryAge
 Input       :  The Indices

                The Object 
                testValFsIpPmtuEntryAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpPmtuEntryAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpPmtuEntryAge (UINT4 *pu4ErrorCode, INT4 i4TestValFsIpPmtuEntryAge)
#else
INT1
nmhTestv2FsIpPmtuEntryAge (pu4ErrorCode, i4TestValFsIpPmtuEntryAge)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsIpPmtuEntryAge;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if ((i4TestValFsIpPmtuEntryAge > PMTU_MAX_ENTRY_AGE)
        || (i4TestValFsIpPmtuEntryAge < PMTU_MIN_ENTRY_AGE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (NULL == UtilIpGetCxt (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpPmtuTableSize
 Input       :  The Indices

                The Object 
                testValFsIpPmtuTableSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpPmtuTableSize ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpPmtuTableSize (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsIpPmtuTableSize)
#else
INT1
nmhTestv2FsIpPmtuTableSize (pu4ErrorCode, i4TestValFsIpPmtuTableSize)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsIpPmtuTableSize;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    /* 18 May 2004 - SC_L3_ADD - */
    if ((i4TestValFsIpPmtuTableSize < PMTU_TABLE_MIN_SIZE) ||
        (i4TestValFsIpPmtuTableSize > PMTU_TABLE_MAX_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (NULL == UtilIpGetCxt (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIpPathMtuTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpPathMtuTable
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIpPathMtuTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIpPathMtuTable (UINT4 u4FsIpPmtuDestination,
                                          INT4 i4FsIpPmtuTos)
#else
INT1
nmhValidateIndexInstanceFsIpPathMtuTable (u4FsIpPmtuDestination, i4FsIpPmtuTos)
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tPmtuInfo          *pPmtuEntry = NULL;
    tPmtuInfo           DummyPmtuEntry;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    if (pIpCxt->u2PmtuStatusFlag != PMTU_ENABLED)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&DummyPmtuEntry, 0, sizeof (tPmtuInfo));
    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);

    if (pPmtuEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsIpPathMtuTable
Input       :  The Indices
FsIpPmtuDestination
FsIpPmtuTos
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIpPathMtuTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIpPathMtuTable (UINT4 *pu4FsIpPmtuDestination,
                                  INT4 *pi4FsIpPmtuTos)
#else
INT1
nmhGetFirstIndexFsIpPathMtuTable (pu4FsIpPmtuDestination, pi4FsIpPmtuTos)
     UINT4              *pu4FsIpPmtuDestination;
     INT4               *pi4FsIpPmtuTos;
#endif
{
    if (nmhGetNextIndexFsIpPathMtuTable (0, pu4FsIpPmtuDestination, 0,
                                         pi4FsIpPmtuTos) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpPathMtuTable
 Input       :  The Indices
                FsIpPmtuDestination
                nextFsIpPmtuDestination
                FsIpPmtuTos
                nextFsIpPmtuTos
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIpPathMtuTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIpPathMtuTable (UINT4 u4FsIpPmtuDestination,
                                 UINT4 *pu4NextFsIpPmtuDestination,
                                 INT4 i4FsIpPmtuTos, INT4 *pi4NextFsIpPmtuTos)
#else
INT1
nmhGetNextIndexFsIpPathMtuTable (u4FsIpPmtuDestination,
                                 pu4NextFsIpPmtuDestination, i4FsIpPmtuTos,
                                 pi4NextFsIpPmtuTos)
     UINT4               u4FsIpPmtuDestination;
     UINT4              *pu4NextFsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4               *pi4NextFsIpPmtuTos;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tPmtuInfo          *pPmtuEntry = NULL;
    tPmtuInfo           DummyPmtuEntry;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);

    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    MEMSET (&DummyPmtuEntry, 0, sizeof (tPmtuInfo));
    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry =
        RBTreeGetNext (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry, NULL);

    if (pPmtuEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pPmtuEntry->pIpCxt->u4ContextId != pIpCxt->u4ContextId)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFsIpPmtuDestination = pPmtuEntry->u4PmtuDest;
    *pi4NextFsIpPmtuTos = (INT4) pPmtuEntry->u1PmtuTos;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpPathMtu
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                retValFsIpPathMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpPathMtu ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpPathMtu (UINT4 u4FsIpPmtuDestination, INT4 i4FsIpPmtuTos,
                   INT4 *pi4RetValFsIpPathMtu)
#else
INT1
nmhGetFsIpPathMtu (u4FsIpPmtuDestination, i4FsIpPmtuTos, pi4RetValFsIpPathMtu)
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4               *pi4RetValFsIpPathMtu;
#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsIpPathMtu = (INT4) pPmtuEntry->u4PMtu;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpPmtuDisc
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                retValFsIpPmtuDisc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpPmtuDisc ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpPmtuDisc (UINT4 u4FsIpPmtuDestination, INT4 i4FsIpPmtuTos,
                    INT4 *pi4RetValFsIpPmtuDisc)
#else
INT1
nmhGetFsIpPmtuDisc (u4FsIpPmtuDestination, i4FsIpPmtuTos, pi4RetValFsIpPmtuDisc)
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4               *pi4RetValFsIpPmtuDisc;
#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsIpPmtuDisc = (pPmtuEntry->u1PmtuDiscFlag == IPPMTUDISC_DO) ?
        PMTU_ENABLED : PMTU_DISABLED;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpPmtuEntryStatus
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                retValFsIpPmtuEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpPmtuEntryStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpPmtuEntryStatus (UINT4 u4FsIpPmtuDestination, INT4 i4FsIpPmtuTos,
                           INT4 *pi4RetValFsIpPmtuEntryStatus)
#else
INT1
nmhGetFsIpPmtuEntryStatus (u4FsIpPmtuDestination, i4FsIpPmtuTos,
                           pi4RetValFsIpPmtuEntryStatus)
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4               *pi4RetValFsIpPmtuEntryStatus;
#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsIpPmtuEntryStatus = pPmtuEntry->u1PmtuRowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpPathMtu
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                setValFsIpPathMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpPathMtu ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpPathMtu (UINT4 u4FsIpPmtuDestination, INT4 i4FsIpPmtuTos,
                   INT4 i4SetValFsIpPathMtu)
#else
INT1
nmhSetFsIpPathMtu (u4FsIpPmtuDestination, i4FsIpPmtuTos, i4SetValFsIpPathMtu)
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4                i4SetValFsIpPathMtu;

#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPmtuEntry->u4PMtu = (UINT4) i4SetValFsIpPathMtu;
    /* The path mtu for this path is set manually hence  the path
       discovery has to be disabled */
    if (IPPMTUDISC_DO == pPmtuEntry->u1PmtuDiscFlag)
    {
        pPmtuEntry->u1PmtuDiscFlag = IPPMTUDISC_DONT;
        /* Call the function provided by TCP to handle the change in PTMU */

#ifdef SLI_WANTED
        SliHandleChangeInPmtuInCxt (pIpCxt->u4ContextId, u4FsIpPmtuDestination,
                                    (UINT1) i4FsIpPmtuTos,
                                    (UINT2) i4SetValFsIpPathMtu);
#endif
        pPmtuEntry->u1PmtuAge = PMTU_INFINITY;
        pPmtuEntry->PmtuTstamp = PMTU_INFINITY;
    }

    IncMsrForFsIpv4PathMtuTable ((INT4) u4ContextId, u4FsIpPmtuDestination,
                                 i4FsIpPmtuTos, i4SetValFsIpPathMtu,
                                 FsMIFsIpPathMtu,
                                 (sizeof (FsMIFsIpPathMtu) / sizeof (UINT4)),
                                 FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpPmtuDisc
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                setValFsIpPmtuDisc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpPmtuDisc ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpPmtuDisc (UINT4 u4FsIpPmtuDestination, INT4 i4FsIpPmtuTos,
                    INT4 i4SetValFsIpPmtuDisc)
#else
INT1
nmhSetFsIpPmtuDisc (u4FsIpPmtuDestination, i4FsIpPmtuTos, i4SetValFsIpPmtuDisc)
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4                i4SetValFsIpPmtuDisc;

#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pPmtuEntry->u1PmtuDiscFlag = (PMTU_ENABLED == i4SetValFsIpPmtuDisc) ?
        IPPMTUDISC_DO : IPPMTUDISC_DONT;

    if (PMTU_NOT_READY == pPmtuEntry->u1PmtuRowStatus)
    {
        pPmtuEntry->u1PmtuRowStatus = PMTU_NOT_IN_SERVICE;
    }
    if (pPmtuEntry->u1PmtuDiscFlag == IPPMTUDISC_DONT)
    {
        pPmtuEntry->PmtuTstamp = PMTU_INFINITY;
    }
    else
    {
        pPmtuEntry->u1PmtuAge = 0;
    }

    IncMsrForFsIpv4PathMtuTable ((INT4) u4ContextId, u4FsIpPmtuDestination,
                                 i4FsIpPmtuTos, i4SetValFsIpPmtuDisc,
                                 FsMIFsIpPmtuDisc,
                                 (sizeof (FsMIFsIpPmtuDisc) / sizeof (UINT4)),
                                 FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpPmtuEntryStatus
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                setValFsIpPmtuEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpPmtuEntryStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpPmtuEntryStatus (UINT4 u4FsIpPmtuDestination, INT4 i4FsIpPmtuTos,
                           INT4 i4SetValFsIpPmtuEntryStatus)
#else
INT1
nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDestination, i4FsIpPmtuTos,
                           i4SetValFsIpPmtuEntryStatus)
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4                i4SetValFsIpPmtuEntryStatus;

#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry != NULL)
    {
        switch (i4SetValFsIpPmtuEntryStatus)
        {

            case PMTU_ACTIVE:
            case PMTU_NOT_IN_SERVICE:
                pPmtuEntry->u1PmtuRowStatus =
                    (UINT1) i4SetValFsIpPmtuEntryStatus;
                break;
            case PMTU_DESTROY:
                PmtuDeleteEntry (pPmtuEntry);
            default:
                break;
        }
    }
    else
    {
        if (i4SetValFsIpPmtuEntryStatus == PMTU_CREATE_N_WAIT)
        {
            pPmtuEntry =
                (tPmtuInfo *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.PmtuPoolId);
            if (NULL == pPmtuEntry)
            {
                return SNMP_FAILURE;
            }

            pPmtuEntry->u4PmtuDest = u4FsIpPmtuDestination;
            pPmtuEntry->u1PmtuTos = (UINT1) i4FsIpPmtuTos;
            pPmtuEntry->u1PmtuDiscFlag = IPPMTUDISC_DONT;
            pPmtuEntry->u1PmtuRowStatus = PMTU_NOT_READY;
            pPmtuEntry->u1PmtuAge = PMTU_INFINITY;
            pPmtuEntry->u4PMtu = PMTU_INFINITY;
            pPmtuEntry->PmtuTstamp = PMTU_INFINITY;
            pPmtuEntry->pIpCxt = pIpCxt;

            if (RBTreeAdd (gIpGlobalInfo.PmtuTblRoot, pPmtuEntry) == RB_FAILURE)
            {
                MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.PmtuPoolId,
                                    (UINT1 *) pPmtuEntry);
                return SNMP_FAILURE;
            }

        }
    }

    IncMsrForFsIpv4PathMtuTable ((INT4) u4ContextId, u4FsIpPmtuDestination,
                                 i4FsIpPmtuTos, i4SetValFsIpPmtuEntryStatus,
                                 FsMIFsIpPmtuEntryStatus,
                                 (sizeof (FsMIFsIpPmtuEntryStatus) /
                                  sizeof (UINT4)), TRUE);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpPathMtu
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                testValFsIpPathMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpPathMtu ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpPathMtu (UINT4 *pu4ErrorCode, UINT4 u4FsIpPmtuDestination,
                      INT4 i4FsIpPmtuTos, INT4 i4TestValFsIpPathMtu)
#else
INT1
nmhTestv2FsIpPathMtu (pu4ErrorCode, u4FsIpPmtuDestination, i4FsIpPmtuTos,
                      i4TestValFsIpPathMtu)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4                i4TestValFsIpPathMtu;
#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIpCxt->u2PmtuStatusFlag != PMTU_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pPmtuEntry->u1PmtuRowStatus != PMTU_NOT_IN_SERVICE) &&
        (pPmtuEntry->u1PmtuRowStatus != PMTU_NOT_READY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsIpPathMtu < PMTU_MIN_MTU) ||
        (i4TestValFsIpPathMtu > PMTU_MAX_MTU))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpPmtuDisc
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                testValFsIpPmtuDisc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpPmtuDisc ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpPmtuDisc (UINT4 *pu4ErrorCode, UINT4 u4FsIpPmtuDestination,
                       INT4 i4FsIpPmtuTos, INT4 i4TestValFsIpPmtuDisc)
#else
INT1
nmhTestv2FsIpPmtuDisc (pu4ErrorCode, u4FsIpPmtuDestination, i4FsIpPmtuTos,
                       i4TestValFsIpPmtuDisc)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4                i4TestValFsIpPmtuDisc;
#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIpCxt->u2PmtuStatusFlag != PMTU_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pPmtuEntry->u1PmtuRowStatus != PMTU_NOT_IN_SERVICE) &&
        (pPmtuEntry->u1PmtuRowStatus != PMTU_NOT_READY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* reusing the macros of global enable and disable */
    if ((i4TestValFsIpPmtuDisc != PMTU_ENABLED) &&
        (i4TestValFsIpPmtuDisc != PMTU_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpPmtuEntryStatus
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos

                The Object 
                testValFsIpPmtuEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpPmtuEntryStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpPmtuEntryStatus (UINT4 *pu4ErrorCode, UINT4 u4FsIpPmtuDestination,
                              INT4 i4FsIpPmtuTos,
                              INT4 i4TestValFsIpPmtuEntryStatus)
#else
INT1
nmhTestv2FsIpPmtuEntryStatus (pu4ErrorCode, u4FsIpPmtuDestination,
                              i4FsIpPmtuTos, i4TestValFsIpPmtuEntryStatus)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsIpPmtuDestination;
     INT4                i4FsIpPmtuTos;
     INT4                i4TestValFsIpPmtuEntryStatus;
#endif
{
    tPmtuInfo           DummyPmtuEntry;
    tPmtuInfo          *pPmtuEntry = NULL;
    tIpCxt             *pIpCxt = NULL;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT4               u4Count = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIpCxt->u2PmtuStatusFlag != PMTU_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    DummyPmtuEntry.u4PmtuDest = u4FsIpPmtuDestination;
    DummyPmtuEntry.u1PmtuTos = (UINT1) i4FsIpPmtuTos;
    DummyPmtuEntry.pIpCxt = pIpCxt;

    pPmtuEntry = RBTreeGet (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry);
    if (pPmtuEntry != NULL)
    {
        switch (i4TestValFsIpPmtuEntryStatus)
        {

            case PMTU_ACTIVE:
            case PMTU_NOT_IN_SERVICE:
            case PMTU_DESTROY:
                i4RetVal = SNMP_SUCCESS;
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        if (i4TestValFsIpPmtuEntryStatus == PMTU_CREATE_N_WAIT)
        {
            RBTreeCount (gIpGlobalInfo.PmtuTblRoot, &u4Count);
            if (u4Count < IP_DEF_MAX_PMTU_ENTRY)
            {
                i4RetVal = SNMP_SUCCESS;
            }
            else
            {

                IP_CXT_TRC_ARG2 (u4ContextId, IP_MOD_TRC,
                                 ALL_FAILURE_TRC | OS_RESOURCE_TRC | MGMT_TRC,
                                 IP_NAME,
                                 "Table Full.Dropping Mgmt Req for Dest %u,Tos %d",
                                 u4FsIpPmtuDestination, (UINT1) i4FsIpPmtuTos);
                *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        }
    }
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpEnablePMTUD
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpEnablePMTUD (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpPmtuEntryAge
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpPmtuEntryAge (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpPmtuTableSize
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpPmtuTableSize (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpPathMtuTable
 Input       :  The Indices
                FsIpPmtuDestination
                FsIpPmtuTos
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpPathMtuTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
