/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmcast.c,v 1.14 2016/09/28 07:48:54 siva Exp $
 *
 * Description:This file contains the functions related to  
 *             processing of incoming and outgoing multicast
 *             IP datagrams.                
 *
 *******************************************************************/
#include "ipinc.h"

/*
 * ****************************************************************************
 * Function Name     :  IpInputPrcsMcastPkt
 *
 * Description       :  This function Processes an incoming multicast packet.
 *
 * Global Variables  :  Ip_stats, Ipif_glbtab[]
 *    Affected
 *
 * Input(s)          :  pBuf   : The datagram in IP buffer.
 *                      pIp    : The IP header parameters.
 *                      i1Flag : Flag indicating the type of destination address *                      u2Port : Interface index on which datagram is received
 *
 * Output(s)         :  None
 *
 * Returns           :  IP_SUCCESS or IP_FAILURE
 * ****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpInputPrcsMcastPkt
 * $$TRACE_PROCEDURE_LEVEL = INTMD
 * *****************************************************************************
 */

#ifdef __STDC__
INT1
IpInputPrcsMcastPkt (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, INT1 i1Flag,
                     UINT2 u2Port)
#else
INT1
IpInputPrcsMcastPkt (pBuf, pIp, i1Flag, u2Port)
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
     INT1                i1Flag;
     UINT2               u2Port;
#endif

{
    UINT1               u1Index = 0;
    UINT4               u4IfaceIndex = 0;
    INT4                i4HLDelvStatus = 0;
#ifdef SLI_WANTED
    INT4                i4SliEnqStatus = 0;
#endif
    tIpBuf             *pDupBuf = NULL;
    tIpCxt             *pIpCxt = NULL;
#ifdef VXLAN_WANTED
    INT4                i4VxlanStatus = VXLAN_FAILURE;
#endif

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }

    IPIF_CONFIG_GET_IF_INDEX (u2Port, u4IfaceIndex);

    if (((IP_IS_RES_MCAST_ADDR (pIp->u4Dest) == TRUE)
         || (pIp->u1Proto == IGMP_PTCL))
        || (pIp->u4Dest == PTP_MCAST_ADDR)
        || ((pIp->u4Dest == IP_IS_RES_MCAST_NTP_ADDR)
            && (pIp->u1Proto == UDP_PTCL)))
    {
#ifdef SLI_WANTED
        i4SliEnqStatus = SliEnqueueIpv4RawPacketInCxt (pIpCxt->u4ContextId,
                                                       pIp->u1Proto, pIp->u2Len,
                                                       pIp->u4Src, pIp->u4Dest,
                                                       pBuf);
#endif

        IP_CXT_TRC_ARG4 (pIpCxt->u4ContextId, IP_MOD_TRC, DATA_PATH_TRC,
                         IP_NAME,
                         "s = %x, d = %x, len %d, rcvd on interface %d.\n",
                         pIp->u4Src, pIp->u4Dest, pIp->u2Len, u4IfaceIndex);

        i4HLDelvStatus = IpDeliverHigherLayerProtocol (pBuf, pIp,
                                                       (UINT1) i1Flag, u2Port);
        if (i4HLDelvStatus == IP_FAILURE)
        {
#ifdef SLI_WANTED
            if (i4SliEnqStatus == SLI_FAILURE)
#endif
            {
                IpHandleUnKnownProtosInCxt (pIpCxt, pBuf, u2Port, i1Flag);
            }
            IP_RELEASE_BUF (pBuf, FALSE);
        }

        return IP_SUCCESS;
    }
    if (pIp->u4Src == IP_ANY_ADDR)
    {
        /* ANVL FIX - Src Addr in the fwded pkts should not be 0.0.0.0 */
        return IP_FAILURE;
    }
    /* CFA Changes.
     * Verification is done in IpInputProcessPkt itself. 
     */
    IpVerifyTtl (pIp, IP_MCAST, u2Port, pBuf);

#ifdef VXLAN_WANTED
    i4VxlanStatus = VxlanPortCheckMcastVxlanPkt (pBuf, FALSE);

    /* If the multicast packet is a VXLAN packet, then packet needs to be 
     * given to VXLAN application .
     * If multicast protocol is enabled on this node, packet will be 
     * given to the protocol also
     * */

    if (i4VxlanStatus == VXLAN_SUCCESS)
    {
        i4HLDelvStatus = IpDeliverHigherLayerProtocol (pBuf, pIp,
                                                       (UINT1) i1Flag, u2Port);
        if (i4HLDelvStatus == IP_FAILURE)
        {
            IpHandleUnKnownProtosInCxt (pIpCxt, pBuf, u2Port, i1Flag);
        }
    }
#endif

    /* what ever protocols registered dor getting the multicast packets 
     * should be given the multicast packets by duplicating the buffer.
     * So the protocols receiving this buffer should take care that they 
     * are releasing the buffer not forcefully.
     */
    for (u1Index = 0; u1Index < IP_MAX_PROTOCOLS; u1Index++)
    {
    	IPFWD_CXT_LOCK ();
        if (gIpGlobalInfo.aMCastProtoRegTable[u1Index].pAppRcv != NULL)
        {
            pDupBuf = IP_DUPLICATE_BUF (pBuf);
            if (pDupBuf != NULL)
            {
                gIpGlobalInfo.aMCastProtoRegTable[u1Index].pAppRcv (pDupBuf);
            }
        }
    	IPFWD_CXT_UNLOCK ();
    }

    /* The buffer which is duplicated will be released in the received module.
     * Here, we will release the actual buffer. It is a success case. In 
     * failure case the buffer will be released by the calling module.
     */
#ifdef VXLAN_WANTED
    if (i4VxlanStatus != VXLAN_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
    }
    else if (i4HLDelvStatus == IP_FAILURE)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
    }
#else
    IP_RELEASE_BUF (pBuf, FALSE);
#endif
    IP4IF_INC_IN_MCAST_PKT (u2Port);

    return IP_SUCCESS;
}
