/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipprcpdu.c,v 1.5 2013/07/04 13:11:58 siva Exp $
 *
 * Description:Consists of procedures that handle IP        
 *             packets such as extracting header, putting   
 *             headers, and computing checksums.            
 *
 *******************************************************************/
#include "ipinc.h"

/**************************************************************************/

     /**** $$TRACE_MODULE_NAME     = IP_FWD ****/
     /**** $$TRACE_SUB_MODULE_NAME = UTIL   ****/

/*-------------------------------------------------------------------+
 *
 * Function           : ip_put_hdr
 *
 * Input(s)           : pBuf, pIp, i1flag
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * This routine is used to put an IP header in structure (in internal format)
 * into the message buffer specified. Assumes that the valid data offset is
 * the beginning of the IP header.
 *
 *   if i1flag is FALSE & the IP packet contains IP Options, then this
 *      function copies IP Options into the IP header structure(pIp).
 *   if i1flag is TRUE  & the IP packet contains IP Options, then this
 *      function copies IP Options from the IP header structure(pIp) into
 *      the buffer.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

VOID
ip_put_hdr (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, INT1 i1flag)
#else

VOID
ip_put_hdr (pBuf, pIp, i1flag)
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
     INT1                i1flag;
#endif
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT1              *pu1Options = NULL;
    UINT1               u1LinearBuf = FALSE;
    UINT4               u4TempSrc = 0, u4TempDest = 0;

    if ((pIpHdr = (t_IP_HEADER *) (VOID *) IP_GET_DATA_PTR_IF_LINEAR (pBuf,
                                                                      0,
                                                                      sizeof
                                                                      (t_IP_HEADER)
                                                                      - 1)) ==
        NULL)
    {
        pIpHdr = &TmpIpHdr;
    }
    else
    {
        u1LinearBuf = TRUE;
    }

    pIpHdr->u1Ver_hdrlen =
        (UINT1) IP_VERS_AND_HLEN (pIp->u1Version, pIp->u2Olen);
    pIpHdr->u1Tos = pIp->u1Tos;
    pIpHdr->u2Totlen = CRU_HTONS (pIp->u2Len);
    pIpHdr->u2Id = CRU_HTONS (pIp->u2Id);
    pIpHdr->u2Fl_offs = CRU_HTONS (pIp->u2Fl_offs);
    pIpHdr->u1Ttl = pIp->u1Ttl;
    pIpHdr->u1Proto = pIp->u1Proto;
    pIpHdr->u2Cksum = 0;        /* Clear Checksum */

    MEMCPY (&u4TempSrc, &pIp->u4Src, sizeof (UINT4));
    MEMCPY (&u4TempDest, &pIp->u4Dest, sizeof (UINT4));

    u4TempSrc = CRU_NTOHL (u4TempSrc);
    u4TempDest = CRU_NTOHL (u4TempDest);

    MEMCPY (&pIpHdr->u4Src, &u4TempSrc, sizeof (UINT4));
    MEMCPY (&pIpHdr->u4Dest, &u4TempDest, sizeof (UINT4));

    if (u1LinearBuf == FALSE)
    {
        IP_COPY_TO_BUF (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    pu1Options = IP_GET_DATA_PTR_IF_LINEAR (pBuf, IP_HDR_LEN, pIp->u2Olen);
    if (pu1Options != NULL)
    {
        if (i1flag == TRUE)
        {
            /* options need to be copied to the buffer */

            MEMCPY (pu1Options, pIp->au1Options,
                    IP_MAX_BYTES (pIp->u2Olen, sizeof (pIp->au1Options)));
        }
        else
        {
            /* options need to be copied from the buffer */

            MEMCPY (pIp->au1Options, pu1Options,
                    IP_MAX_BYTES (pIp->u2Olen, sizeof (pIp->au1Options)));
        }
    }
    else
    {
        if (i1flag == TRUE)
        {
            IP_COPY_TO_BUF (pBuf, pIp->au1Options, IP_HDR_LEN, pIp->u2Olen);
        }
        else
        {
            IP_COPY_FROM_BUF (pBuf, pIp->au1Options, IP_HDR_LEN, pIp->u2Olen);
        }
    }

    pIp->u2Cksum = IpCalcCheckSum (pBuf, (UINT4) (IP_HDR_LEN + pIp->u2Olen), 0);

    if (u1LinearBuf == TRUE)
    {
        pIpHdr->u2Cksum = IP_HTONS (pIp->u2Cksum);
    }
    else
    {
        IP_PKT_ASSIGN_CKSUM (pBuf, pIp->u2Cksum);
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_update_cksum
 *
 * Input(s)           : pBuf, u2Len
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS always
 *
 * Action :
 * Incremental checksum calculation mechanism.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

UINT2
ip_update_cksum (t_IP * pIp, UINT4 u4Value)
#else
UINT2
ip_update_cksum (pIp, u4Value)
     t_IP               *pIp;
     UINT4               u4Value;
#endif
{
    UINT4               u4TmpCkSum = 0;

    u4TmpCkSum =
        (u4Value >> 16) + (u4Value & 0xffffL) + ((~pIp->u2Cksum) & 0xffffL);

    /*
     * because worst case result is 0x2fffd when all fields
     * are 0xffff.
     */

    u4TmpCkSum = (u4TmpCkSum >> 16) + (u4TmpCkSum & 0xffffL);
    return (UINT2) (~u4TmpCkSum);
}
