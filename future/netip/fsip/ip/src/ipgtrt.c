/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipgtrt.c,v 1.9 2009/08/24 13:11:39 prabuc Exp $
 *
 * Description:This file contains the routines for getting  
 *             the  Route entries from Trie .              
 *
 *******************************************************************/
#include "ipinc.h"
#include "trieapif.h"

extern UINT1        gaPreference[MAX_ROUTING_PROTOCOLS];

/*************************************************************************/
/* Function Name     :  IpGetAdvancedRouteInfo                           */
/*                                                                       */
/* Description       :  This function finds the Best Route for given     */
/*                      Destination and Mask                             */
/*                      It returns failure when Trie returns sink route  */
/*                      for the destination.                             */
/*                                                                       */
/* Input(s)          :  1. u4Dest - The destination address for which the*/
/*                                  route has to be found                */
/*                                                                       */
/* Output(s)         :  1. pOutRtInfo - The Route Info for the Dest.     */
/*                      2. pu4Preference  - Preference for the Proto     */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                          */
/*************************************************************************/
#ifdef __STDC__
INT4
IpGetAdvancedRouteInfo (UINT4 u4Dest, tRtInfo * pOutRtInfo,
                        UINT4 *pu4Preference)
#else
INT4
IpGetAdvancedRouteInfo (u4Dest, pOutRtInfo, pu4Preference)
     UINT4               u4Dest;    /* Destination for which route is needed  */
     tRtInfo            *pOutRtInfo;
     UINT4              *pu4Preference;
#endif
{
    return (IpGetAdvancedRouteInfoInCxt (IP_DEFAULT_CONTEXT, u4Dest, pOutRtInfo,
                                         pu4Preference));
}

/*************************************************************************/
/* Function Name     :  IpGetAdvancedRouteInfoInCxt                      */
/*                                                                       */
/* Description       :  This function finds the Best Route for given     */
/*                      Destination and Mask in the specified context    */
/*                      It returns failure when Trie returns sink route  */
/*                      for the destination.                             */
/*                                                                       */
/* Input(s)          :  1. u4Context id - the context identified.        */
/*                      2. u4Dest - The destination address for which the*/
/*                                  route has to be found                */
/*                                                                       */
/* Output(s)         :  1. pOutRtInfo - The Route Info for the Dest.     */
/*                      2. pu4Preference  - Preference for the Proto     */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                          */
/*************************************************************************/
#ifdef __STDC__
INT4
IpGetAdvancedRouteInfoInCxt (UINT4 u4ContextId, UINT4 u4Dest,
                             tRtInfo * pOutRtInfo, UINT4 *pu4Preference)
#else
INT4
IpGetAdvancedRouteInfoInCxt (u4ContextId, u4Dest, pOutRtInfo, pu4Preference)
     UINT4               u4ContextId;
     UINT4               u4Dest;    /* Destination for which route is needed  */
     tRtInfo            *pOutRtInfo;
     UINT4              *pu4Preference;
#endif
{
    UINT4               u4DestMask = 0;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    IpGetMaskFromAddr (u4Dest, &u4DestMask);

    RtQuery.u4DestinationIpAddress = u4Dest;
    RtQuery.u4DestinationSubnetMask = u4DestMask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4ContextId;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        pOutRtInfo = NULL;
        *pu4Preference = 0;
        return IP_FAILURE;
    }
    *pu4Preference = NetIpRtInfo.u1Preference;
    pOutRtInfo->u4DestNet = NetIpRtInfo.u4DestNet;
    pOutRtInfo->u4DestMask = NetIpRtInfo.u4DestMask;
    pOutRtInfo->u4NextHop = NetIpRtInfo.u4NextHop;
    pOutRtInfo->u4RtIfIndx = NetIpRtInfo.u4RtIfIndx;
    pOutRtInfo->u4RouteTag = NetIpRtInfo.u4RouteTag;
    pOutRtInfo->i4Metric1 = NetIpRtInfo.i4Metric1;
    pOutRtInfo->u4RtAge = NetIpRtInfo.u4RtAge;
    pOutRtInfo->u4RowStatus = NetIpRtInfo.u4RowStatus;
    pOutRtInfo->u4Tos = NetIpRtInfo.u4Tos;
    pOutRtInfo->u2RtType = NetIpRtInfo.u2RtType;
    pOutRtInfo->u2RtProto = NetIpRtInfo.u2RtProto;
    pOutRtInfo->u1Preference = NetIpRtInfo.u1Preference;
    return IP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpGetMaskFromAddr                                */
/*                                                                       */
/* Description       :  This function finds the mask for the  given      */
/*                      destination.                                     */
/*                                                                       */
/* Input(s)          :  1. u4DestNet - The destination address for which */
/*                                   mask has to be found                */
/*                                                                       */
/* Output(s)         :  1. pu4DestMask - The destination mask            */
/*                                                                       */
/* Returns           :  None                                             */
/*************************************************************************/
#ifdef __STDC__
VOID
IpGetMaskFromAddr (UINT4 u4DestNet, UINT4 *pu4DestMask)
#else
VOID
IpGetMaskFromAddr (u4DestNet, pu4DestMask)
     UINT4               u4DestNet;
     UINT4              *pu4DestMask;
#endif
{
    UINT4               u4Index = u4DestNet;
    UINT1               u1Index = 1;

    *pu4DestMask = 0;

    if (u4DestNet != 0)
    {
        *pu4DestMask = *pu4DestMask + 1;
    }
    else
    {
        return;
    }
    for (u1Index = 1; u1Index < MAX_ADDR_LEN; u1Index++)
    {
        *pu4DestMask = *pu4DestMask << 1;
        u4Index = u4Index << 1;
        if (u4Index != 0)
        {
            *pu4DestMask = *pu4DestMask + 1;
        }
    }
}

/*************************************************************************/
/* Function Name     :  IpGetRoute                                       */
/*                                                                       */
/* Description       :  This function finds the route for given          */
/*                      destination in default context                   */
/*                      It returns failure when Trie returns sink route  */
/*                      for the destination.                             */
/*                                                                       */
/* Input(s)          :  1. u4Dest - The destination address for which the*/
/*                                  route has to be found                */
/*                      2. u1Tos  - The Type of Service                  */
/*                                                                       */
/* Output(s)         :  1. pu2RtPort - The interface index through which */
/*                                      the packet has to be forwarded   */
/*                      2. pu4RtGw   - The address of the next hop       */
/*                                      gateway                          */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                */
/*************************************************************************/
INT4
IpGetRoute (UINT4 u4Dest, UINT4 u4Src, UINT1 u1Proto, UINT1 u1Tos,
            UINT2 *pu2RtPort, UINT4 *pu4RtGw)
{
    return (IpGetRouteInCxt (IP_DEFAULT_CONTEXT, u4Dest, u4Src, u1Proto, u1Tos,
                             pu2RtPort, pu4RtGw));

}

/*************************************************************************/
/* Function Name     :  IpGetRouteInCxt                                  */
/*                                                                       */
/* Description       :  This function finds the route for given          */
/*                      destination in the specified context             */
/*                      It returns failure when Trie returns sink route  */
/*                      for the destination.                             */
/*                                                                       */
/* Input(s)          :  1. u4ContextId - the context identifier.         */
/*                      2. u4Dest - The destination address for which the*/
/*                                  route has to be found                */
/*                      3. u1Tos  - The Type of Service                  */
/*                                                                       */
/* Output(s)         :  1. pu2RtPort - The interface index through which */
/*                                      the packet has to be forwarded   */
/*                      2. pu4RtGw   - The address of the next hop       */
/*                                      gateway                          */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                */
/*************************************************************************/
#ifdef __STDC__
INT4
IpGetRouteInCxt (UINT4 u4ContextId, UINT4 u4Dest, UINT4 u4Src, UINT1 u1Proto,
                 UINT1 u1Tos, UINT2 *pu2RtPort, UINT4 *pu4RtGw)
#else
INT4
IpGetRouteInCxt (u4ContextId, u4Dest, u4Src, u1Proto, u1Tos, pu2RtPort, pu4RtGw)
     UINT4               u4ContextId;
     UINT4               u4Dest;    /* Destination for which route is needed  */
     UINT4               u4Src;
     UINT1               u1Proto;
     UINT1               u1Tos;    /* Type of service is ignored currently   */
     UINT2              *pu2RtPort;    /* Interface for this route               */
     UINT4              *pu4RtGw;    /* Next hop to be used for this host      */
#endif
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tNetIpv4RtInfo      NextRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tIpCxt             *pIpCxt = NULL;
    UINT4               au4GW[MAX_PATHS];
    UINT4               au4IfIndex[MAX_PATHS];
    UINT1               u1NoPaths = 0;
    INT1                i1Flow = 0;

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }
    UNUSED_PARAM (u1Tos);
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&NextRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4Dest;
    RtQuery.u4DestinationSubnetMask = IP_DEF_NET_MASK;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4ContextId;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        return IP_FAILURE;
    }

    if (IP_CFG_GET_LOADSHARE_ENABLE (pIpCxt) == IP_LOADSHARE_ENABLE)
    {
        au4GW[u1NoPaths] = NetIpRtInfo.u4NextHop;

        au4IfIndex[u1NoPaths] = NetIpRtInfo.u4RtIfIndx;

        u1NoPaths++;

        while (NetIpv4GetNextFwdTableRouteEntry (&NetIpRtInfo,
                                                 &NextRtInfo) ==
               NETIPV4_SUCCESS)
        {
            if ((NetIpRtInfo.u4DestNet != NextRtInfo.u4DestNet) ||
                (NetIpRtInfo.u4DestMask != NextRtInfo.u4DestMask))
            {
                break;
            }

            if (u1NoPaths >= MAX_PATHS)
            {
                break;
            }

            au4GW[u1NoPaths] = NetIpRtInfo.u4NextHop;

            au4IfIndex[u1NoPaths] = NetIpRtInfo.u4RtIfIndx;

            MEMCPY (&NetIpRtInfo, &NextRtInfo, sizeof (tNetIpv4RtInfo));

            u1NoPaths++;

        }
        IP_GET_LOAD_SHARE_FLOW (u4Dest, u4Src, u1Proto, u1NoPaths, i1Flow);

        *pu4RtGw = (au4GW[i1Flow] == 0) ? u4Dest : au4GW[i1Flow];

        *pu2RtPort = (UINT2) au4IfIndex[i1Flow];

        return IP_SUCCESS;
    }

    *pu4RtGw = (NetIpRtInfo.u4NextHop == 0) ? u4Dest : NetIpRtInfo.u4NextHop;

    *pu2RtPort = (UINT2) (NetIpRtInfo.u4RtIfIndx);

    return IP_SUCCESS;
}
