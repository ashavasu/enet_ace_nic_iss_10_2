/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipsnfilt.c,v 1.4 2007/10/12 06:52:58 iss Exp $
 *
 * Description:This file contains the network management    
 *             support routines for filter data structures. 
 *
 *******************************************************************/
#include "ipinc.h"
#include "fsiplow.h"

/********************* IP FILTER TABLE TEST OPERATIONS **********************/

/**** $$TRACE_PROCEDURE_NAME  = ip_filt_mgmt_basic_tests  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW                       ****/

#ifdef __STDC__

INT4
ip_filt_mgmt_basic_tests (INT4 i4List, UINT4 u4Src, UINT4 u4Dest)
#else

INT4
ip_filt_mgmt_basic_tests (i4List, u4Src, u4Dest)
     INT4                i4List;    /* List Number         */
     UINT4               u4Src;    /* Source Address      */
     UINT4               u4Dest;    /* Destination address */
#endif
{
   /**** $$TRACE_LOG (ENTRY, "Validation for list %d  Src %x  Dest %x\n",
                      i4List, u4Src, u4Dest); ****/

    /* Makefile Changes - unused parameter */
    u4Src = 0;
    u4Dest = 0;

    if (i4List <= 0 || i4List > IP_FILT_MAX_LISTS)
    {
      /**** $$TRACE_LOG (EXIT, "List Invalid\n"); ****/
        return SNMP_FAILURE;
    }
   /**** $$TRACE_LOG (EXIT, "Validated OK\n"); ****/
    return SNMP_SUCCESS;
}

#ifdef __STDC__

INT4
ip_filt_mgmt_list_test (INT4 i4List, UINT4 u4Src, UINT4 u4Dest, INT4 i4Value)
#else

INT4
ip_filt_mgmt_list_test (i4List, u4Src, u4Dest, i4Value)
     INT4                i4List;    /* List Number         */
     UINT4               u4Src;    /* Source Address      */
     UINT4               u4Dest;    /* Destination address */
     INT4                i4Value;
#endif
{
    if (ip_filt_mgmt_basic_tests (i4List, u4Src, u4Dest) == SNMP_FAILURE ||
        i4List != i4Value)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
