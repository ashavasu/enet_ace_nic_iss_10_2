/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcfaif.c,v 1.45 2014/08/23 11:57:56 siva Exp $
 *
 * Description:
 *
 *******************************************************************/
#include "ipinc.h"

UINT2               gu2VlanId = 0;
UINT1               gu1IfType = 0;

/**************************************************************************/

/**** $$TRACE_MODULE_NAME     = IP_FWD     ****/
/**** $$TRACE_SUB_MODULE_NAME = CFAIF      ****/
/**** $$TRACE_ENABLE  ****/

/********************************************************************/
/******************** IP LL ROUTINES *******************************/
/********************************************************************/

/********************************************************
**** IP Protocol Interface Routines STUB Begins Here ****
*********************************************************/
/*
 * These routines are called from LL.
 */
/*-------------------------------------------------------------------+
 *
 * Function           : IpHandlePktFromCfa
 *
 * AKA                : IP Forwarding Function
 *
 * Input(s)           : pBuf, 
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS always.
 *
 * Action :
 * This procedure is called by LL whenever a packet need to be enqueued 
 * to IP. Returns failure whenever a Pkt could not be successfully 
 * enqueued to IP.
 +--------------------------------------------------------------------*/
INT4
IpHandlePktFromCfa (tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, UINT1 u1LinkType)
{
    tIpParms           *pParms = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;
    UINT1               u1Ipv4EnableStatus = IPVX_IPV4_ENABLE_STATUS_DOWN;

    IPFWD_CXT_LOCK ();
    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    u4CxtId = pIpCxt->u4ContextId;
    IPFWD_CXT_UNLOCK ();

    pParms = (tIpParms *) IP_GET_MODULE_DATA_PTR (pBuf);

    if (u2Port >= IPIF_MAX_LOGICAL_IFACES)
    {
        /* CFA will release the buffer */
        IP4SYS_INC_IN_DISCARDS (u4CxtId);
        return (IP_FAILURE);
    }

    IPFWD_DS_LOCK ();
    u1Ipv4EnableStatus = gIpGlobalInfo.Ipif_config[u2Port].u1Ipv4EnableStatus;
    IPFWD_DS_UNLOCK ();

    if (u1Ipv4EnableStatus != IPVX_IPV4_ENABLE_STATUS_UP)
    {
        /* CFA will release the buffer */
        IP4SYS_INC_IN_DISCARDS (u4CxtId);
        IP4IF_INC_IN_DISCARDS (u2Port);
        return (IP_FAILURE);
    }

    pParms->u1Cmd = IP_LAYER2_DATA;
    pParms->u2Port = u2Port;
    pParms->u1LinkType = u1LinkType;

    if (OsixQueSend (gIpGlobalInfo.IpPktQId, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        /* CFA will release the buffer */
        IP4SYS_INC_IN_DISCARDS (u4CxtId);
        IP4IF_INC_IN_DISCARDS (u2Port);
        return IP_FAILURE;
    }
    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_PACKET_ARRIVAL_EVENT);
    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : IpRegisterNetworkInterface
 *
 * Input(s)           : pIfaceQMsg
 *
 * Output(s)          : None.
 *
 * Returns            : IP_SUCCESS
 *
 * Action :
 * The routine creates the  IP interface entry at u2Port.
 *
+-------------------------------------------------------------------*/

INT4
IpRegisterNetworkInterface (tIfaceQMsg * pIfaceQMsg)
{
    tCfaIfInfo          CfaIfInfo;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4IpAddress = 0;
    UINT4               u4Mtu = 0;
    UINT4               u4Speed = 0;
    UINT4               u4PeerAddress = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2IfIndex = 0;
    UINT2               u2Port = 0, u2FirstPort = 0;
    UINT1               u1IfaceType = 0;
    UINT1               u1PersistenceValue = 0;
    UINT1               u1IfType = 0;
    UINT1               u1ForwardEnable = IPFORWARD_ENABLE;
    UINT1               u1EncapType = 0;

    /* Get free index from ipif_glbtab
     * if no free index is found return ETABLEFULL
     */
    if (VcmGetContextInfoFromIpIfIndex ((UINT4) pIfaceQMsg->u2Port,
                                        &u4ContextId) == VCM_FAILURE)
    {
        /* Interface to context mapping is not present in VCM */
        return IP_FAILURE;
    }

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }

    u2IfIndex = pIfaceQMsg->u2IfIndex;
    u4IpAddress = pIfaceQMsg->u4Address;
    u4Mtu = pIfaceQMsg->u4Mtu;
    u4Speed = pIfaceQMsg->u4IfSpeed;
    u4PeerAddress = pIfaceQMsg->u4PeerAddr;
    u1PersistenceValue = pIfaceQMsg->u1Persistence;
    u1IfType = pIfaceQMsg->u1IfaceType;
    u1EncapType = pIfaceQMsg->u1EncapType;
    u2Port = pIfaceQMsg->u2Port;

    if (u2Port > IPIF_MAX_LOGICAL_IFACES)
    {
        return IP_FAILURE;
    }

    /* Take a IP DS  Lock */
    IPFWD_DS_LOCK ();

    if (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType !=
        IP_IF_TYPE_INVALID)
    {
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                    "Interface Already Created \n");
        IPFWD_DS_UNLOCK ();
        return IP_FAILURE;
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    /* allocate memory for pIf, pIp, pStat
     * if memory fails for any one of these, then release other allocated 
     * memory and return FAILURE. 
     */
    if (IpifGetFirstConfigPort (&u2FirstPort) != IP_SUCCESS)
    {
        u2FirstPort = IPIF_INVALID_INDEX;
    }

    gIpGlobalInfo.Ipif_glbtab[u2Port].pIf =
        (t_IP_IFACE_INFO *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.
                                            IpIfInfoPoolId);
    gIpGlobalInfo.Ipif_glbtab[u2Port].pStat =
        (t_IP_IF_STAT *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.IpIfStatPoolId);

    if ((IPIF_GLBTAB_IFCONFIG_OF (u2Port) == NULL)
        || (IPIF_GLBTAB_STATS_OF (u2Port) == NULL))
    {
        ipifipIfaceAllocError (u2Port);
        return IP_FAILURE;
    }
    else
    {
        MEMSET (gIpGlobalInfo.Ipif_glbtab[u2Port].pIf, 0,
                sizeof (t_IP_IFACE_INFO));
        MEMSET (gIpGlobalInfo.Ipif_glbtab[u2Port].pStat, 0,
                sizeof (t_IP_IF_STAT));
        ipifipIfaceConfigInit (u2Port);
        /* Set the context back pointer */
        gIpGlobalInfo.Ipif_config[u2Port].pIpCxt = pIpCxt;
        gIpGlobalInfo.Ipif_glbtab[u2Port].pIpCxt = pIpCxt;
        /* Dont Enable forwarding on OOB Port */
        if (CfaIsMgmtPort ((UINT4) u2IfIndex) == TRUE)
        {
            if (CFA_OOB_MGMT_INTF_ROUTING == TRUE)
            {
                u1ForwardEnable = IPFORWARD_ENABLE;
            }
            else
            {
                u1ForwardEnable = IPFORWARD_DISABLE;
            }
        }
        /* Take a Data Structure Lock */
        IPFWD_DS_LOCK ();

        gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex = u2IfIndex;
        gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u2_SubReferenceNum = 0;
        gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType =
            u1IfType;
        gIpGlobalInfo.Ipif_config[u2Port].u1IpForwardingEnable =
            u1ForwardEnable;
        gIpGlobalInfo.Ipif_config[u2Port].u1Ipv4EnableStatus =
            IPVX_IPV4_ENABLE_STATUS_UP;
        /* Unlock it */
        IPFWD_DS_UNLOCK ();

        if ((u1IfType == IP_ENET_PHYS_INTERFACE_TYPE) ||
            (u1IfType == IP_OTHER_PHYS_INTERFACE_TYPE) ||
            (u1IfType == IP_L3IPVLAN_PHYS_INTERFACE_TYPE)
            || (u1IfType == IP_LAGG_INTERFACE_TYPE)
#ifdef WGS_WANTED
            || (u1IfType == IP_L2VLAN_PHYS_INTERFACE_TYPE)
#endif /* WGS_WANTED */
            )
        {
            /* 
             * Register all muliticast addresses required with 
             * CFA.
             * RIP                     - 0xE0000009
             * All Host Multi (IRDP)   - 0xE0000001
             * All Router Multi (IRDP) - 0xE0000002
             * OSPF ALL_SPF_RTR        - 0xE0000005
             * OSPF ALL_DR_RTR         - 0xE0000006
             */

            CfaIfmEnetConfigMcastAddr (ALL_MULTI, u2IfIndex, 0, NULL);

            /* Just to be backward compatible */
            IPIF_PHYS_ADDR_LENGTH (u2Port) = IP_ENET_ADDR_LEN;
            if (CfaGetIfInfo (u2IfIndex, &CfaIfInfo) == CFA_FAILURE)
            {
                return IP_FAILURE;
            }

            MEMCPY (IPIF_PHYS_ADDR (u2Port),
                    CfaIfInfo.au1MacAddr, IP_ENET_ADDR_LEN);
        }

#ifdef IP6_WANTED
        else if (u1IfType == IP_V4OVERV6_TUNNEL_INTERFACE_TYPE)
        {
            IPIF_CONFIG_SET_OPER_STATUS (u2Port, IPIF_OPER_ENABLE);
            IPIF_CONFIG_SET_ADMIN_STATUS (u2Port, IPIF_ADMIN_ENABLE);
        }
#endif
#ifdef TUNNEL_WANTED
        IPFWD_DS_LOCK ();
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf = NULL;

        if (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType ==
            IP_TUNNEL_INTERFACE_TYPE)
        {
            /* New Tunnel interface created. Allocate Memory for New interface */
            gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf =
                (tIpTunlIf *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.
                                              IpTnlIfPoolId);
        }
        IPFWD_DS_UNLOCK ();
#endif /* TUNNEL_WANTED */

        /* Take a Data Structure Lock */
        IPFWD_DS_LOCK ();

        if (u2FirstPort == IPIF_INVALID_INDEX)
        {
            /* this is the only & first interface */
            gIpGlobalInfo.u2FirstIpifIndex = u2Port;
            gIpGlobalInfo.Ipif_config[u2Port].u2Next = IPIF_INVALID_INDEX;
        }
        else if (u2FirstPort < IPIF_MAX_LOGICAL_IFACES)
        {
            gIpGlobalInfo.Ipif_config[u2Port].u2Next =
                gIpGlobalInfo.Ipif_config[u2FirstPort].u2Next;

            gIpGlobalInfo.Ipif_config[u2FirstPort].u2Next = u2Port;
        }

        /* UnLock it */
        IPFWD_DS_UNLOCK ();

        /* A logical interface is considered unnumbered, when
         * the IP address is zero.
         * the IP address is one of the other interface's.  */

        if ((ipif_is_our_address_InCxt (u4ContextId, u4IpAddress)
             == FALSE) && (u4IpAddress != IP_ANY_ADDR))
        {
            IPIF_SET_IFACE_STATUS (u2Port, NUMBERED);

#ifdef NPAPI_WANTED
            /* The following check is neccesary as the default
             * interface is used by manager alone for configuration
             * and would not be visible to NP as one of its 
             * interface and hence its not neccesary to bind this 
             * to VR or enable forwarding on this interface
             */
            if (u2Port != IP_DEFAULT_ROUTER_ID)
            {
                /* If the Port is OOB Port,Dont Program in HardWare */
                if (CfaIsMgmtPort ((UINT4) u2IfIndex) == FALSE)
                {
#ifndef VR_SUPPORT
                    /*If VR is supported an interface should be bounded to an 
                     * VR before enabling forwarding on a VR,this api should 
                     * be moved to the appropriate place in the VR enabled 
                     * code,at present this is required for Tego for which it 
                     * is neccesary to bind and interface to VR before enabling 
                     * forwarding on that interface  */
                    if (IpFsNpIpv4BindIfToVrId ((UINT4) u2IfIndex, IP_VRID)
                        == FNP_FAILURE)
                    {
                        IP_CXT_TRC_ARG2 (u4ContextId, IP_MOD_TRC, MGMT_TRC,
                                         IP_NAME,
                                         "Could not bind"
                                         "interface=%d to virtual router=%d\n",
                                         u2IfIndex, IP_VRID);
                    }
                    else
                    {
                        IP_CXT_TRC_ARG2 (u4ContextId, IP_MOD_TRC, MGMT_TRC,
                                         IP_NAME,
                                         "Interface= %d"
                                         "is bounded to virtual router=%d\n",
                                         u2IfIndex, IP_VRID);
                    }
                }
#endif /* VR_SUPPORT */
                /* Enables routing per VR in fast path only if in IP 
                 * Forwarding is enabled which is accesible through 
                 * Ip_cfg struct
                 */
                if ((IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_ENABLE))
                {
                    if (IpEnableRouting (u4ContextId, FNP_ACTIVE) != IP_SUCCESS)
                    {
                        IP_CXT_TRC_ARG1 (u4ContextId, IP_MOD_TRC, MGMT_TRC,
                                         IP_NAME,
                                         "Could not enable routing for VR = %d in the fast"
                                         "path\n", IP_VRID);
                    }
                    else
                    {
                        IP_CXT_TRC_ARG1 (u4ContextId, IP_MOD_TRC, MGMT_TRC,
                                         IP_NAME,
                                         "Enabled"
                                         "routing for VR = %d in the fast path\n",
                                         IP_VRID);
                    }
                }
            }
#endif /* NPAPI_WANTED */

        }

        /* Take a Data Structure Lock */
        IPFWD_DS_LOCK ();
        gIpGlobalInfo.Ipif_config[u2Port].u4Mtu = u4Mtu;
        gIpGlobalInfo.Ipif_config[u2Port].u4IfSpeed = u4Speed;
        gIpGlobalInfo.Ipif_config[u2Port].u1EncapType = u1EncapType;
        gIpGlobalInfo.Ipif_config[u2Port].u4PeerAddress = u4PeerAddress;
        gIpGlobalInfo.Ipif_config[u2Port].u1Persistence = u1PersistenceValue;
        u1IfaceType = gIpGlobalInfo.Ipif_config[u2Port].u1IfaceType;
        /* Unlock it */
        IPFWD_DS_UNLOCK ();

        /* Irdp will work on interfaces mapped to default context. */
        if ((u1IfaceType == NUMBERED) && (u4ContextId == IP_DEFAULT_CONTEXT))
        {
            IrdpAddInterface (u2Port);
        }
    }

    IP_CFG_SET_IF_TBL_CHANGE_TIME ();

    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : IpDeRegisterNetworkInterface
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : IP_SUCCESS
 *
 * Action :
 * The routine removes the VCid entry at u2Port.
 * The associated information pointers are freed and InterfaceId field
 * is marked as free.
 *
+-------------------------------------------------------------------*/
/********************************************************************/
VOID
IpDeRegisterNetworkInterface (UINT2 u2Port)
{
    tIpCxt             *pIpCxt = NULL;

    if (u2Port >= IPIF_MAX_LOGICAL_IFACES)
    {
        return;
    }

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (pIpCxt == NULL)
    {
        return;
    }

    /* Delete static routes associated with this interface  */
    RtmActOnStaticRoutesForIfDeleteInCxt (pIpCxt->u4ContextId, (UINT4) u2Port);
    IPIF_CONFIG_SET_ADMIN_STATUS (u2Port, IPIF_ADMIN_INVALID);

    if (u2Port == gIpGlobalInfo.u2FirstIpifIndex)
    {
        gIpGlobalInfo.u2FirstIpifIndex =
            gIpGlobalInfo.Ipif_config[u2Port].u2Next;
    }
    else
    {
        UINT2               u2CurrPort = 0;

        /* Take a Data Structure Lock */
        IPFWD_DS_LOCK ();

        IPIF_LOGICAL_IFACES_SCAN (u2CurrPort)
        {
            if (gIpGlobalInfo.Ipif_config[u2CurrPort].u2Next == u2Port)
            {
                gIpGlobalInfo.Ipif_config[u2CurrPort].u2Next =
                    gIpGlobalInfo.Ipif_config[u2Port].u2Next;
                break;
            }
        }

        /* Unlock it */
        IPFWD_DS_UNLOCK ();
    }
    IpIfStateChngNotify (u2Port, IFACE_DELETED);

    if (pIpCxt->u4ContextId == IP_DEFAULT_CONTEXT)
    {
        IrdpDeleteInterface (u2Port);
    }
    IpIfStChgNotify (u2Port, IFACE_DELETED);

    /* Indication given to VCM for IP INTERFACE Delation */

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
#ifdef TUNNEL_WANTED
    if (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType ==
        IP_TUNNEL_INTERFACE_TYPE)
    {
        /* Free the memory allocated for tunnel interface */
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpTnlIfPoolId,
                            (UINT1 *) gIpGlobalInfo.Ipif_config[u2Port].
                            pIpTunlIf);
    }
#endif /* TUNNEL_WANTED */

    gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType =
        IP_IF_TYPE_INVALID;
    gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex =
        (UINT4) IPIF_INVALID_INDEX;
    gIpGlobalInfo.Ipif_config[u2Port].u1Oper = IPIF_OPER_INVALID;
    gIpGlobalInfo.Ipif_config[u2Port].u2Next = IPIF_INVALID_INDEX;
    /* Reseting the context pointer */
    gIpGlobalInfo.Ipif_config[u2Port].pIpCxt = NULL;
    gIpGlobalInfo.Ipif_glbtab[u2Port].pIpCxt = NULL;

    /* Free the memory allocated for interface info and interface stats */
    if (IPIF_GLBTAB_IFCONFIG_OF (u2Port) != NULL)
    {
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpIfInfoPoolId,
                            (UINT1 *) gIpGlobalInfo.Ipif_glbtab[u2Port].pIf);
    }

    if (IPIF_GLBTAB_STATS_OF (u2Port) != NULL)
    {
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpIfStatPoolId,
                            (UINT1 *) gIpGlobalInfo.Ipif_glbtab[u2Port].pStat);
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    IP_CFG_SET_IF_TBL_CHANGE_TIME ();

    return;
}

VOID
IpNetworkInterfaceConfig (tIfaceQMsg * pIfaceQMsg)
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4BitMap = 0;
    UINT4               u4OldMtu = 0;
    UINT4               u4OldIfSpeed = 0;
    UINT1               u1InterfaceType = 0;
    UINT2               u2Port = pIfaceQMsg->u2Port;
    UINT4               u4IpAddress = pIfaceQMsg->u4Address;
    UINT4               u4SubnetMask = pIfaceQMsg->u4Netmask;
    UINT4               u4BcastAddress = pIfaceQMsg->u4BcastAddr;
    UINT4               u4Mtu = pIfaceQMsg->u4Mtu;
    UINT4               u4IfSpeed = pIfaceQMsg->u4IfSpeed;

    UNUSED_PARAM (u4SubnetMask);
    UNUSED_PARAM (u4BcastAddress);

    /* Take a Data Structure Lock */

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        return;
    }

    IPFWD_DS_LOCK ();
    u1InterfaceType = gIpGlobalInfo.Ipif_config[u2Port].u1IfaceType;
    u4OldMtu = gIpGlobalInfo.Ipif_config[u2Port].u4Mtu;
    u4OldIfSpeed = gIpGlobalInfo.Ipif_config[u2Port].u4IfSpeed;
    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    if (u1InterfaceType != NUMBERED)
    {
        if (u4IpAddress != 0)
        {
            u1InterfaceType = NUMBERED;
        }
    }

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    gIpGlobalInfo.Ipif_config[u2Port].u4Mtu = u4Mtu;
    gIpGlobalInfo.Ipif_config[u2Port].u4IfSpeed = u4IfSpeed;
    gIpGlobalInfo.Ipif_config[u2Port].u1IfaceType = u1InterfaceType;
    IPFWD_DS_UNLOCK ();

    /* When the IP address of an interface is changed,give indication to the 
     * higher layer protocols by setting the IP_Address bitmask. The higher 
     * layer protocols should handle this case accordingly.
     */
    if (u4OldMtu != u4Mtu)
    {
        u4BitMap |= IF_MTU_BIT_MASK;
    }
    else if (u4OldIfSpeed != u4IfSpeed)
    {
        u4BitMap |= IF_SPEED_BIT_MASK;
    }
    else
    {
        u4BitMap |= IP_ADDR_BIT_MASK;
    }

    IpIfStChgNotify (u2Port, u4BitMap);
    IpIfStateChngNotify (u2Port, u4BitMap);

    return;
}

/**** $$TRACE_PROCEDURE_NAME  =  IpUpdateInterfaceStatus  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
/*-------------------------------------------------------------------+
 *
 * Function           : IpUpdateInterfaceStatus 
 *
 * Input(s)           : u2Port, u1OperStatus
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action             : Invoked by CFA
 *                      Enqueues to IP task regarding change of interface.
 *
 *
+-------------------------------------------------------------------*/
INT4
IpUpdateInterfaceStatus (UINT2 u2Port, UINT1 *pu1IfName, UINT1 u1OperStatus)
{

    tIfaceQMsg         *pIfaceQMsg = NULL;

    UNUSED_PARAM (pu1IfName);
    if ((pIfaceQMsg =
         (tIfaceQMsg *) IP_ALLOCATE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.
                                               IfacePoolId)) == NULL)
    {
        return IP_FAILURE;
    }

    pIfaceQMsg->u2Port = u2Port;

    switch (u1OperStatus)
    {
        case CFA_IF_UP:
        {
            pIfaceQMsg->u1MsgType = IP_INTERFACE_UP;
            break;
        }
        case CFA_IF_DOWN:
        {
            pIfaceQMsg->u1MsgType = IP_INTERFACE_DOWN;
            break;
        }
        default:
            IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IfacePoolId,
                                  pIfaceQMsg);
            return IP_FAILURE;
    }

    if (OsixQueSend (gIpGlobalInfo.IpCfaIfQId, (UINT1 *) &pIfaceQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IfacePoolId, pIfaceQMsg);
        return IP_FAILURE;
    }

    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_CFA_INTERFACE_EVENT);
    return IP_SUCCESS;
}

INT4
IpGenerateIcmpErrorMesg (tIP_BUF_CHAIN_HEADER * pBuf, t_ICMP Icmp, INT1 i1Flag)
{
    t_ICMP_MSG_PARMS   *pParms = NULL;

    pParms = (t_ICMP_MSG_PARMS *) IP_GET_MODULE_DATA_PTR (pBuf);
    pParms->u1Cmd = IP_ICMP_DATA;
    pParms->u4ContextId = Icmp.u4ContextId;

    pParms->Msg.Err.i1Type = Icmp.i1Type;
    pParms->Msg.Err.i1Code = Icmp.i1Code;

    if (i1Flag == TRUE)
    {
        pParms->i1Error_or_request = TRUE;
        pParms->Msg.Err.u4Parm1 = Icmp.u4Mtu;
    }
    else
    {
        pParms->i1Error_or_request = FALSE;
    }

    if (OsixQueSend (gIpGlobalInfo.IpPktQId, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_PACKET_ARRIVAL_EVENT);
    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpHandleInterfaceMsgFromCfa
 *
 * AKA                : IP Forwarding Function
 *
 * Input(s)           : pBuf, 
 *
 * Output(s)          : None.
 *
 * Returns            : SUCCESS always.
 *
 * Action :
 * This procedure is called by CFA to post a message to IP on Creation
 * Updation and deletion of Interface at CFA  
 +--------------------------------------------------------------------*/
INT4
IpHandleInterfaceMsgFromCfa (t_IFACE_PARAMS * pIfaceParams, UINT1 u1Command)
{
    tIfaceQMsg         *pIfaceQMsg = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    if (VcmGetContextInfoFromIpIfIndex ((UINT4) pIfaceParams->u2Port,
                                        &u4ContextId) == VCM_FAILURE)
    {
        /* Interface to context mapping is not present in VCM */
        return IP_FAILURE;
    }

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }

    if ((pIfaceQMsg =
         (tIfaceQMsg *) IP_ALLOCATE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.
                                               IfacePoolId)) == NULL)
    {
        return IP_FAILURE;
    }
    pIfaceQMsg->u1MsgType = u1Command;
    pIfaceQMsg->u4Address = pIfaceParams->u4IpAddr;
    pIfaceQMsg->u4Netmask = pIfaceParams->u4SubnetMask;
    pIfaceQMsg->u4BcastAddr = pIfaceParams->u4BcastAddr;
    pIfaceQMsg->u4Mtu = pIfaceParams->u4Mtu;
    pIfaceQMsg->u4IfSpeed = pIfaceParams->u4IfSpeed;
    pIfaceQMsg->u4PeerAddr = pIfaceParams->u4PeerIpAddr;
    pIfaceQMsg->u2IfIndex = pIfaceParams->u2IfIndex;
    pIfaceQMsg->u2Port = pIfaceParams->u2Port;
    pIfaceQMsg->u1IfaceType = pIfaceParams->u1IfType;
    pIfaceQMsg->u1EncapType = pIfaceParams->u1EncapType;
    pIfaceQMsg->u1Persistence = pIfaceParams->u1Persistence;
    pIfaceQMsg->u4ContextId = u4ContextId;
    pIfaceQMsg->u2VlanId = pIfaceParams->u2VlanId;

    gu2VlanId = pIfaceQMsg->u2VlanId;
    gu1IfType = pIfaceParams->u1IfType;

    if (OsixQueSend (gIpGlobalInfo.IpCfaIfQId, (UINT1 *) &pIfaceQMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IfacePoolId, pIfaceQMsg);
        return IP_FAILURE;
    }
    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_CFA_INTERFACE_EVENT);
    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : Ip4TnlIfUpdateNotifyFromCfa
 *
 * Input(s)           : pBuf, 
 *
 * Output(s)          : None.
 *
 * Returns            : IP_SUCCESS/IP_FAILURE.
 *
 * Action :
 * This function sets the necessary parameters to update the tunnel in 
 * formation and calls Ip4TunlIfUpdate.
 +--------------------------------------------------------------------*/

INT4
Ip4TnlIfUpdateNotifyFromCfa (UINT4 u4IfIndex, tTnlIfInfo * pCfaTnlIfInfo)
{
#ifdef TUNNEL_WANTED
    tIpTunlIf           IpTnlInfo;
    UINT4               u4Mask = 0;

    MEMSET (&IpTnlInfo, 0, sizeof (tIpTunlIf));
    if (pCfaTnlIfInfo->u4TnlMask & TNL_DIRECTION_CHG)
        u4Mask |= TNL_DIRECTION_CHG;

    if (pCfaTnlIfInfo->u4TnlMask & TNL_DIR_FLAG_CHG)
        u4Mask |= TNL_DIR_FLAG_CHG;

    if (pCfaTnlIfInfo->u4TnlMask & TNL_ENCAP_OPT_CHG)
        u4Mask |= TNL_ENCAP_OPT_CHG;

    if (pCfaTnlIfInfo->u4TnlMask & TNL_ENCAP_LIMIT_CHG)
        u4Mask |= TNL_ENCAP_LIMIT_CHG;

    if (pCfaTnlIfInfo->u4TnlMask & TNL_ENCAP_ADDR_CHG)
        u4Mask |= TNL_ENCAP_ADDR_CHG;

    if (pCfaTnlIfInfo->u4TnlMask & TNL_HOP_LIMIT_CHG)
        u4Mask |= TNL_HOP_LIMIT_CHG;

    IpTnlInfo.u1TunlDir = (UINT1) pCfaTnlIfInfo->u4TnlDir;
    IpTnlInfo.u1TunlFlag = (UINT1) pCfaTnlIfInfo->u4TnlDirFlag;
    IpTnlInfo.u1EncapFlag = (UINT1) pCfaTnlIfInfo->u4TnlEncapOpt;
    IpTnlInfo.u1TunlEncaplmt = (UINT1) pCfaTnlIfInfo->u4TnlEncapLmt;
    IpTnlInfo.u2HopLimit = (UINT2) pCfaTnlIfInfo->u4TnlHopLimit;
    IpTnlInfo.u4tunlSrc = pCfaTnlIfInfo->u4LocalAddr;
    IpTnlInfo.u4tunlDst = pCfaTnlIfInfo->u4RemoteAddr;
    IpTnlInfo.u1TunlType = (UINT1) pCfaTnlIfInfo->u4TnlType;
    if (Ip4TunlIfUpdate (u4IfIndex, u4Mask, &IpTnlInfo) == IP_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pCfaTnlIfInfo);
    return IP_FAILURE;
#endif
}
