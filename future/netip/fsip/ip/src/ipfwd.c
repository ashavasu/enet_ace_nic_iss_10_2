/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipfwd.c,v 1.21 2017/09/25 11:59:59 siva Exp $
 *
 * Description:This file contains functions for IP unicast,     
 *             Mulicast,Broadcast forwarding .              
 *
 *******************************************************************/

#include "ipinc.h"

/*****************************************************************************/
/*************** V A R I A B L E   D E C L A R A T I O N S *******************/
/*****************************************************************************/

/*****************************************************************************/
/* Function Name     :  IpForward                                            */
/*                                                                           */
/* Description       :  This function does the forwarding of Unicast IP      */
/*                      datagrams.It does the following steps .              */
/*                      1. Find the next hop gateway to forward,if the       */
/*                         destination does not belong to any locally        */
/*                         attached network to the router.                   */
/*                      2. Check if the forwarding interface is equal to the */
/*                         received interface then ICMP Redirect message is  */
/*                         generated.                                        */
/*                      3. Check if the forwarding interface is              */
/*                         Adminstratively and Operationally up and also     */
/*                         if the system is set to act as router.            */
/*                      4. Deliver the packet for Output processing .        */
/*                                                                           */
/* Global Variables  :  Ip_stats - IP statistics variable                    */
/*                                                                           */
/* Input(s)          :  pBuf- The IP buffer pointer                         */
/*                      pIp - The IP header structure pointer                */
/*                      i1Flag - The flag denoting the mode of reception     */
/*                               (IP_FOR_THIS_NODE or IP_UCAST)              */
/*                      u2Port -  The Logical port through which the         */
/*                                 datagram was received                     */
/*                      u2Rt_port - The forwarding logical port Index        */
/*                                  address                                  */
/*                      u2Rt_gw   - The next hop gateway or destination      */
/*                                  address                                  */
/*                       u2Port   - The received port Index                  */
/*                                                                           */
/* Output(s)         :  None                                                 */
/*                                                                           */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                    */
/*****************************************************************************/
#ifdef __STDC__
INT4
IpForward (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp,
           INT1 i1Flag, UINT2 u2Rt_port, UINT4 u4Rt_gw, UINT2 u2Port)
#else
INT4
IpForward (pBuf, pIp, i1Flag, u2Rt_port, u4Rt_gw, u2Port)
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
     INT1                i1Flag;
     UINT2               u2Rt_port;
     UINT4               u4Rt_gw;
     UINT2               u2Port;
#endif
{
#ifdef ARP_WANTED
    tArpQMsg            ArpQMsg;
#endif
    t_ICMP              Icmp;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CxtId = 0;
    UINT4               u4CfaIfIndex = 0;
    INT4                i4LocalProxyArp = ARP_PROXY_DISABLE;

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    IP4SYS_INC_IN_FORW_DGRAMS (u4CxtId);
    IP4IF_INC_IN_FORW_DGRAMS (u2Port);

    /* 
     * If the forwarding is not forced by options,then find the route 
     */

    if ((u4Rt_gw == 0) && (u2Rt_port == 0) &&
        IpGetRouteInCxt (u4CxtId, pIp->u4Dest, pIp->u4Src, pIp->u1Proto,
                         pIp->u1Tos, &u2Rt_port, &u4Rt_gw) == IP_FAILURE)
    {
        pIpCxt->Ip_stats.u4Out_no_routes++;
        IPIF_INC_OUT_NO_ROUTES (u2Port);
        if ((ipif_is_our_address_InCxt (u4CxtId, pIp->u4Src) == TRUE) ||
            (pIp->u4Src == IP_ANY_ADDR))
        {
            IP4SYS_INC_OUT_NO_ROUTES (u4CxtId);
            IP4IF_INC_OUT_NO_ROUTES (u2Port);
        }
        else
        {
            IP4SYS_INC_IN_NO_ROUTES (u4CxtId);
            IP4IF_INC_IN_NO_ROUTES (u2Port);
        }

        /* In L3_Switching enabled case, the data packet is given to
         * ARP module to disable DLF setting. It helps in preventing
         * CPU getting flooded with data traffic */
#ifdef ARP_WANTED
        MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
        ArpQMsg.u4MsgType = IP_PKT_FOR_RESOLVE;
        ArpQMsg.u2Protocol = CFA_ENET_IPV4;
        ArpQMsg.u4EncapType = CFA_ENCAP_ENETV2;
        ArpQMsg.pBuf = pBuf;
        ArpQMsg.u2Port = u2Port;

        if (ArpEnqueueIpPkt (&ArpQMsg) != ARP_SUCCESS)
        {
            IP_CXT_TRC (u4CxtId, IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                        IP_NAME, "IP Packet eneque to ARP failed \r\n");
            return IP_FAILURE;
        }
        return IP_SUCCESS;
#endif
    }

    /* 
     * There exists an other gateway on same network which is
     * nearer to destination. Send icmp redirect message so that
     * endnode can update its table.
     */

    if (NetIpv4GetCfaIfIndexFromPort (u2Port, &u4CfaIfIndex) != NETIPV4_FAILURE)
    {
        IpGetFsIpifLocalProxyArpAdminStatus ((INT4) u4CfaIfIndex,
                                             &i4LocalProxyArp);
    }

    if (u2Port == u2Rt_port && (i4LocalProxyArp != ARP_PROXY_ENABLE))
    {

        UINT4               u4Tmp_gw = 0;
        UINT2               u2Tmp_port = 0;
        if ((IpGetRouteInCxt
             (u4CxtId, pIp->u4Src, 0, 0, pIp->u1Tos, &u2Tmp_port,
              &u4Tmp_gw)) == IP_SUCCESS)
        {

            if (u4Tmp_gw == pIp->u4Src)
            {                    /* Only if the host is on direct net */

                Icmp.i1Type = ICMP_REDIRECT;
                Icmp.i1Code = ICMP_REDR_NET;
                Icmp.u4ContextId = u4CxtId;
                Icmp.args.u4Address = u4Rt_gw;

                icmp_error_msg (pBuf, &Icmp);
            }
        }
    }

    if (IpVerifyTtl (pIp, i1Flag, u2Port, pBuf) == IP_FAILURE)
    {
        return IP_FAILURE;
    }

    /* 
     * Forwarding is done only if the neccessary objects are set 
     */

    /* Makefile Changes - unused parameter pIp removed it */
    if (IpForwardCheckInterface (u2Rt_port, i1Flag, pBuf) == IP_FAILURE)
    {

        IP_CXT_TRC_ARG3 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "s = %x, d = %x, len %d, dropped (forwarding disabled).\n",
                         pIp->u4Src, pIp->u4Dest, pIp->u2Len);

        return IP_FAILURE;
    }

    if (IpOutput (u2Rt_port, u4Rt_gw, pIp, pBuf, i1Flag) == IP_FAILURE)
    {
        return IP_FAILURE;
    }
    IP4SYS_INC_OUT_FORW_DGRAMS (u4CxtId);
    IP4IF_INC_OUT_FORW_DGRAMS (u2Rt_port);

    return IP_SUCCESS;
}

/*****************************************************************************/
/* Function Name     :  IpForwardBcast                                       */
/*                                                                           */
/* Description       :  This function scans through IP Interface table and   */
/*                      check whether broadcast address of any of the        */
/*                      interfaces matches with destination broadcast        */
/*                      address and then forwards the datagram               */
/*                                                                           */
/* Global Variables  :  None                                                 */
/*                                                                           */
/* Input(s)          :  pBuf - The IP buffer pointer                        */
/*                      pIp  - The IP header pointer                         */
/*                      u2Port - The logical port through which the packet   */
/*                               was received                                */
/*                                                                           */
/* Output(s)         :   None                                                */
/*                                                                           */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                    */
/*****************************************************************************/
#ifdef __STDC__
INT4
IpForwardBcast (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp, UINT2 u2Port)
#else
INT4
IpForwardBcast (pBuf, pIp, u2Port)
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
     UINT2               u2Port;
#endif
{
    tIP_SLL             BcastIfList;
    t_IF_LIST_NODE     *pBcastIfNode = NULL;
    tIP_BUF_CHAIN_HEADER *pOutBuf = NULL, *pInBuf = NULL;
    tIpCxt             *pIpCxt = NULL;
    INT4                i4HLDelvStatus = 0;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4CxtId = 0;
    UINT2               u2Index = 0;
    UINT2               u2FwdIfaces = 0;
    UINT2               u2Rt_port = IPIF_INVALID_INDEX;

    IP_SLL_Init (&BcastIfList);

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    /* Take a Data Structure Lock  */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2Index)
    {
        if ((gIpGlobalInfo.Ipif_config[u2Index].InterfaceId.u1_InterfaceType
             == IP_IF_TYPE_INVALID) || (u2Index == u2Port))
        {
            continue;
        }

        if (gIpGlobalInfo.Ipif_config[u2Index].pIpCxt == NULL)
        {
            /* Interface is not mapped */
            continue;
        }

        if (gIpGlobalInfo.Ipif_config[u2Index].pIpCxt->u4ContextId != u4CxtId)
        {
            continue;
        }

        u4CfaIfIndex = gIpGlobalInfo.Ipif_config[u2Index].InterfaceId.u4IfIndex;

        if ((CfaIpIfCheckInterfaceBcastAddr
             (u4CfaIfIndex, pIp->u4Dest) == CFA_SUCCESS) &&
            (gIpGlobalInfo.Ipif_config[u2Index].u1IpDrtdBcastFwdingEnable ==
             IPIF_DRTD_BCAST_FWD_ENABLE))
        {
            /*
             * Broadcast is not through the interfaces thro' which 
             * the pkt has been received
             */
            if ((pBcastIfNode = (t_IF_LIST_NODE *)
                 IP_ALLOCATE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IpIfPoolId)) ==
                NULL)
            {
                IP4SYS_INC_OUT_DISCARDS (u4CxtId);
                IP4IF_INC_OUT_DISCARDS (u2Port);
                continue;
            }
            pBcastIfNode->u2Port = u2Index;
            IP_CXT_TRC_ARG2 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "Directed Broadcast From %x "
                             "to be forwarded on Port %d \n", pIp->u4Src,
                             u2Index);

            IP_SLL_Add (&BcastIfList, &(pBcastIfNode->Link));
        }
    }

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    if ((u2FwdIfaces = (UINT2) IP_SLL_Count (&BcastIfList)) == 0)
    {
        return IP_FAILURE;
    }

    pInBuf = IP_DUPLICATE_BUF (pBuf);

    i4HLDelvStatus = IpDeliverHigherLayerProtocol (pInBuf, pIp,
                                                   IP_BCAST, u2Port);

    if (i4HLDelvStatus == IP_FAILURE)
    {
        IpHandleUnKnownProtosInCxt (pIpCxt, pInBuf, u2Port, IP_BCAST);
        IP_RELEASE_BUF (pInBuf, FALSE);
    }

    if (IpVerifyTtl (pIp, IP_BCAST, u2Port, pBuf) == IP_FAILURE)
    {
        if (pBcastIfNode != NULL)
            IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IpIfPoolId,
                                  pBcastIfNode);
        return IP_FAILURE;
    }

    while (u2FwdIfaces-- > 0)
    {
        pBcastIfNode = (t_IF_LIST_NODE *) IP_SLL_Get (&BcastIfList);
        if (pBcastIfNode == NULL)
        {
            continue;
        }
        u2Rt_port = pBcastIfNode->u2Port;

        IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IpIfPoolId, pBcastIfNode);

        /* Makefile Changes - unused parameter pIp removed it */
        if (IpForwardCheckInterface (u2Rt_port, IP_BCAST, pBuf) == IP_FAILURE)
        {

            IP_CXT_TRC_ARG3 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "s = %x, d = %x, len %d, "
                             "dropped (forwarding disabled).\n",
                             pIp->u4Src, pIp->u4Dest, pIp->u2Len);

            return IP_FAILURE;
        }

        if (u2FwdIfaces != 0)
        {
            pOutBuf = IP_DUPLICATE_BUF (pBuf);
        }
        else
        {
            pOutBuf = pBuf;
        }

        if (IpOutput (u2Rt_port, pIp->u4Dest, pIp, pOutBuf, IP_BCAST) ==
            IP_FAILURE)
        {
            return IP_FAILURE;
        }

    }
    return IP_SUCCESS;
}
