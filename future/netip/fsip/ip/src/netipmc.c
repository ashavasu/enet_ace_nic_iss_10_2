/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: netipmc.c,v 1.2 2010/07/21 14:11:14 prabuc Exp $
 *
 * Description:This file holds the APIs for IPv4 multicast routing
 *             support with FsIp.
 *             All APIs in this file are stubs and are not required to be
 *             ported for FSIP
 *
 *******************************************************************/

#include "ipinc.h"
/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4SetMcStatusOnPort 
 *
 *    DESCRIPTION      : Enables/Disables multicast routing on an IpPort
 *                       according to u4McastStatus. 
 *
 *    INPUT            : pNetIpMcastInfo: contains the ipport and the 
 *                       corresponding multicast routing information 
 *                       u4McastStatus : ENABLED/DISABLED
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4SetMcStatusOnPort (tNetIpMcastInfo * pNetIpMcastInfo,
                          UINT4 u4McastStatus)
{
    UNUSED_PARAM (pNetIpMcastInfo);
    UNUSED_PARAM (u4McastStatus);
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :NetIpv4McastRouteUpdate 
 *
 *    DESCRIPTION      :Adds/deletes multicast route as per u4RouteFlag.  
 *
 *    INPUT            : pIpMcastRoute: Route information
 *                      u4RouteFlag : NETIPV4_ADD_ROUTE/NETIPV4_DELETE_ROUTE
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McastRouteUpdate (tNetIpMcRouteInfo * pIpMcastRoute, UINT4 u4RouteFlag)
{
    UNUSED_PARAM (pIpMcastRoute);
    UNUSED_PARAM (u4RouteFlag);

    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : NetIpv4RegisterMcPacket
 *
 *    DESCRIPTION      : API to register for receiving Multicast control
 *                       or/and data packet from higher layer modules
 *
 *    INPUT            : pNetIpMcRegInfo  - Pointer having Multicast
 *                           registration info
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ****************************************************************************/
INT4
NetIpv4RegisterMcPacket (tNetIpMcRegInfo * pNetIpMcRegInfo)
{
    /* This API is not required to be ported in FSIP */
    UNUSED_PARAM (pNetIpMcRegInfo);
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4DeRegisterMcPacket 
 *
 *    DESCRIPTION      : Routine to deregister for receiving Multicast control
 *                       or/and data packet from higher layer modules
 *
 *    INPUT            : u4ProtocolId - Protocol id to register
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4DeRegisterMcPacket (UINT4 u4ProtocolId)
{
    /* This API is not required to be ported in FSIP */
    UNUSED_PARAM (u4ProtocolId);
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :NetIpv4McastUpdateCpuPortStatus
 *
 *    DESCRIPTION      : Creates/Deletes CPU Port into/from LinuxIp when PIM 
 *                       is enabled/disabled, as per the value of 
 *                       u4CpuPortStatus.
 *
 *    INPUT            : u4CpuPortStatus: ENABLED - Create  Cpu Port in LnxIp,
 *                                  i.e.,Create  a VIF in LnxIp  for CpuPort.
 *                                        DISABLED -Delete CpuPort from LnxIp,
 *                                  i.e.,Delete the VIF for CPUPort from LnxIp.  
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McastUpdateCpuPortStatus (UINT4 u4CpuPortStatus)
{
    /* This API is not required to be ported in FSIP */
    UNUSED_PARAM (u4CpuPortStatus);
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McastUpdateRouteCpuPort  
 *
 *    DESCRIPTION      : Adds/Deletes CPU Port into/from LinuxIp for the 
 *                       i/p routeentry as per the value of u4CpuPortStatus. 
 *
 *    INPUT            : pIpMcastRoute : Multicast route info as follows:
 *                                        (u4SrcAddr: Source Address
 *                                         u4GrpAddr: Multicast Group Address
 *                                         u4Iif    : Incoming Interface
 *                                         u4OifCnt : No.of outgoing i/fs
 *                                         *pOIf    : OifList )
 *                       u4CpuPortStatus: ENABLED - Add Cpu Port to 
 *                                                  multicast route in LnxIp,
 *                                        DISABLED -Delete CpuPort from 
 *                                                  multicast route in LnxIp,
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McastUpdateRouteCpuPort (tNetIpMcRouteInfo * pIpMcastRoute,
                                UINT4 u4CpuPortStatus)
{
    /* This API is not required to be ported in FSIP */
    UNUSED_PARAM (pIpMcastRoute);
    UNUSED_PARAM (u4CpuPortStatus);
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McGetRouteStats
 *
 *    DESCRIPTION      : Gets the Multicast route stats
 *
 *    INPUT            : u4GrpAddr: Multicast Group Address
 *                       u4SrcAddr: Source Address
 *                       u4StatType:Type of the stats
 *
 *    OUTPUT           : pu4StatVal : Stat value
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McGetRouteStats (UINT4 u4GrpAddr, UINT4 u4SrcAddr, UINT4 u4StatType,
                        UINT4 *pu4StatVal)
{
    /* This API is not required to be ported in FSIP */
    UNUSED_PARAM (u4GrpAddr);
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (u4StatType);

    *pu4StatVal = 0;
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McGetIfaceStats
 *
 *    DESCRIPTION      : Gets the Multicast enabled interface  stats
 *
 *    INPUT            : i4IfIndex : Interface index
 *                       u4StatType:Type of the stats
 *
 *    OUTPUT           : pu4StatVal : Stat value
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McGetIfaceStats (INT4 i4IfIndex, UINT4 u4StatType, UINT4 *pu4StatVal)
{
    /* This API is not required to be ported in FSIP */
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4StatType);

    *pu4StatVal = 0;
    return NETIPV4_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  netipmc.c                      */
/*-----------------------------------------------------------------------*/
