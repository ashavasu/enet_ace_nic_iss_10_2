/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpiplw.c,v 1.12 2013/07/31 13:55:51 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "ipinc.h"
# include  "fssnmp.h"
# include  "fsmpipcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpGlobalDebug
 Input       :  The Indices

                The Object 
                retValFsMIFsIpGlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpGlobalDebug (INT4 *pi4RetValFsMIFsIpGlobalDebug)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpGlobalDebug (pi4RetValFsMIFsIpGlobalDebug);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsMIFsIpGlobalDebug
 Input       :  The Indices

                The Object 
                setValFsMIFsIpGlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpGlobalDebug (INT4 i4SetValFsMIFsIpGlobalDebug)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpGlobalDebug (i4SetValFsMIFsIpGlobalDebug);
    return i1Return;

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpGlobalDebug
 Input       :  The Indices

                The Object 
                testValFsMIFsIpGlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpGlobalDebug (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMIFsIpGlobalDebug)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpGlobalDebug (pu4ErrorCode, i4TestValFsMIFsIpGlobalDebug);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpGlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpGlobalDebug (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIpGlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpGlobalTable (INT4 i4FsMIStdIpContextId)
{
    if ((i4FsMIStdIpContextId < IP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }

    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpGlobalTable (INT4 *pi4FsMIStdIpContextId)
{
    if (VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilIpGetCxt ((UINT4) *pi4FsMIStdIpContextId) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpGlobalTable (INT4 i4FsMIStdIpContextId,
                                    INT4 *pi4NextFsMIStdIpContextId)
{
    if (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                   (UINT4 *) pi4NextFsMIStdIpContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (UtilIpGetCxt ((UINT4) *pi4NextFsMIStdIpContextId) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpInLengthErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpInLengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpInLengthErrors (INT4 i4FsMIStdIpContextId,
                              UINT4 *pu4RetValFsMIFsIpInLengthErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsIpInLengthErrors (pu4RetValFsMIFsIpInLengthErrors);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpInCksumErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpInCksumErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpInCksumErrors (INT4 i4FsMIStdIpContextId,
                             UINT4 *pu4RetValFsMIFsIpInCksumErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsIpInCksumErrors (pu4RetValFsMIFsIpInCksumErrors);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpInVersionErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpInVersionErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpInVersionErrors (INT4 i4FsMIStdIpContextId,
                               UINT4 *pu4RetValFsMIFsIpInVersionErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsIpInVersionErrors (pu4RetValFsMIFsIpInVersionErrors);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpInTTLErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpInTTLErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpInTTLErrors (INT4 i4FsMIStdIpContextId,
                           UINT4 *pu4RetValFsMIFsIpInTTLErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpInTTLErrors (pu4RetValFsMIFsIpInTTLErrors);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpInOptionErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpInOptionErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpInOptionErrors (INT4 i4FsMIStdIpContextId,
                              UINT4 *pu4RetValFsMIFsIpInOptionErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpInOptionErrors (pu4RetValFsMIFsIpInOptionErrors);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpInBroadCasts
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpInBroadCasts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpInBroadCasts (INT4 i4FsMIStdIpContextId,
                            UINT4 *pu4RetValFsMIFsIpInBroadCasts)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpInBroadCasts (pu4RetValFsMIFsIpInBroadCasts);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpOutGenErrors
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpOutGenErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpOutGenErrors (INT4 i4FsMIStdIpContextId,
                            UINT4 *pu4RetValFsMIFsIpOutGenErrors)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpOutGenErrors (pu4RetValFsMIFsIpOutGenErrors);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpOptProcEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpOptProcEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpOptProcEnable (INT4 i4FsMIStdIpContextId,
                             INT4 *pi4RetValFsMIFsIpOptProcEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpOptProcEnable (pi4RetValFsMIFsIpOptProcEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpNumMultipath
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpNumMultipath
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpNumMultipath (INT4 i4FsMIStdIpContextId,
                            INT4 *pi4RetValFsMIFsIpNumMultipath)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpNumMultipath (pi4RetValFsMIFsIpNumMultipath);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpLoadShareEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpLoadShareEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpLoadShareEnable (INT4 i4FsMIStdIpContextId,
                               INT4 *pi4RetValFsMIFsIpLoadShareEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhGetFsIpLoadShareEnable (pi4RetValFsMIFsIpLoadShareEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpEnablePMTUD
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpEnablePMTUD
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpEnablePMTUD (INT4 i4FsMIStdIpContextId,
                           INT4 *pi4RetValFsMIFsIpEnablePMTUD)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpEnablePMTUD (pi4RetValFsMIFsIpEnablePMTUD);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpPmtuEntryAge
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpPmtuEntryAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpPmtuEntryAge (INT4 i4FsMIStdIpContextId,
                            INT4 *pi4RetValFsMIFsIpPmtuEntryAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIpPmtuEntryAge (pi4RetValFsMIFsIpPmtuEntryAge);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIpContextDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpContextDebug (INT4 i4FsMIStdIpContextId,
                            INT4 *pi4RetValFsMIFsIpContextDebug)
{
    tIpCxt             *pIpCxt = NULL;

    pIpCxt = UtilIpGetCxt ((UINT4) i4FsMIStdIpContextId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIFsIpContextDebug = (INT4) pIpCxt->u4IpDbg;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpOptProcEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIpOptProcEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpOptProcEnable (INT4 i4FsMIStdIpContextId,
                             INT4 i4SetValFsMIFsIpOptProcEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsIpOptProcEnable (i4SetValFsMIFsIpOptProcEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpNumMultipath
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIpNumMultipath
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpNumMultipath (INT4 i4FsMIStdIpContextId,
                            INT4 i4SetValFsMIFsIpNumMultipath)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsIpNumMultipath (i4SetValFsMIFsIpNumMultipath);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpLoadShareEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIpLoadShareEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpLoadShareEnable (INT4 i4FsMIStdIpContextId,
                               INT4 i4SetValFsMIFsIpLoadShareEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsIpLoadShareEnable (i4SetValFsMIFsIpLoadShareEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpEnablePMTUD
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIpEnablePMTUD
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpEnablePMTUD (INT4 i4FsMIStdIpContextId,
                           INT4 i4SetValFsMIFsIpEnablePMTUD)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsIpEnablePMTUD (i4SetValFsMIFsIpEnablePMTUD);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpPmtuEntryAge
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIpPmtuEntryAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpPmtuEntryAge (INT4 i4FsMIStdIpContextId,
                            INT4 i4SetValFsMIFsIpPmtuEntryAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsIpPmtuEntryAge (i4SetValFsMIFsIpPmtuEntryAge);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIpContextDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpContextDebug (INT4 i4FsMIStdIpContextId,
                            INT4 i4SetValFsMIFsIpContextDebug)
{
    tIpCxt             *pIpCxt = NULL;

    pIpCxt = UtilIpGetCxt ((UINT4) i4FsMIStdIpContextId);
    if (pIpCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pIpCxt->u4IpDbg = (UINT4) i4SetValFsMIFsIpContextDebug;
    PingEnableDebug ((UINT4) i4SetValFsMIFsIpContextDebug);
    IncMsrForFsIpv4GlobalTable (i4FsMIStdIpContextId,
                                i4SetValFsMIFsIpContextDebug,
                                FsMIFsIpContextDebug,
                                (sizeof (FsMIFsIpContextDebug) /
                                 sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpOptProcEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIpOptProcEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpOptProcEnable (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                                INT4 i4TestValFsMIFsIpOptProcEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpOptProcEnable (pu4ErrorCode,
                                    i4TestValFsMIFsIpOptProcEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpNumMultipath
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIpNumMultipath
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpNumMultipath (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               INT4 i4TestValFsMIFsIpNumMultipath)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpNumMultipath (pu4ErrorCode, i4TestValFsMIFsIpNumMultipath);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpLoadShareEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIpLoadShareEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpLoadShareEnable (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpContextId,
                                  INT4 i4TestValFsMIFsIpLoadShareEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpLoadShareEnable (pu4ErrorCode,
                                      i4TestValFsMIFsIpLoadShareEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpEnablePMTUD
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIpEnablePMTUD
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpEnablePMTUD (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4TestValFsMIFsIpEnablePMTUD)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpEnablePMTUD (pu4ErrorCode, i4TestValFsMIFsIpEnablePMTUD);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpPmtuEntryAge
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIpPmtuEntryAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpPmtuEntryAge (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               INT4 i4TestValFsMIFsIpPmtuEntryAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpPmtuEntryAge (pu4ErrorCode, i4TestValFsMIFsIpPmtuEntryAge);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpContextDebug
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIpContextDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpContextDebug (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               INT4 i4TestValFsMIFsIpContextDebug)
{
    tIpCxt             *pIpCxt = NULL;

    UNUSED_PARAM (i4TestValFsMIFsIpContextDebug);

    pIpCxt = UtilIpGetCxt ((UINT4) i4FsMIStdIpContextId);
    if (pIpCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpGlobalTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIpTraceConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpTraceConfigTable
 Input       :  The Indices
                FsMIFsIpTraceConfigDest
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpTraceConfigTable (UINT4
                                                  u4FsMIFsIpTraceConfigDest)
{
    return (nmhValidateIndexInstanceFsIpTraceConfigTable
            (u4FsMIFsIpTraceConfigDest));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpTraceConfigTable
 Input       :  The Indices
                FsMIFsIpTraceConfigDest
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpTraceConfigTable (UINT4 *pu4FsMIFsIpTraceConfigDest)
{
    return (nmhGetFirstIndexFsIpTraceConfigTable (pu4FsMIFsIpTraceConfigDest));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpTraceConfigTable
 Input       :  The Indices
                FsMIFsIpTraceConfigDest
                nextFsMIFsIpTraceConfigDest
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpTraceConfigTable (UINT4 u4FsMIFsIpTraceConfigDest,
                                         UINT4 *pu4NextFsMIFsIpTraceConfigDest)
{
    return (nmhGetNextIndexFsIpTraceConfigTable (u4FsMIFsIpTraceConfigDest,
                                                 pu4NextFsMIFsIpTraceConfigDest));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceConfigAdminStatus
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                retValFsMIFsIpTraceConfigAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceConfigAdminStatus (UINT4 u4FsMIFsIpTraceConfigDest,
                                      INT4
                                      *pi4RetValFsMIFsIpTraceConfigAdminStatus)
{
    return (nmhGetFsIpTraceConfigAdminStatus
            (u4FsMIFsIpTraceConfigDest,
             pi4RetValFsMIFsIpTraceConfigAdminStatus));
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceConfigMaxTTL
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                retValFsMIFsIpTraceConfigMaxTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceConfigMaxTTL (UINT4 u4FsMIFsIpTraceConfigDest,
                                 INT4 *pi4RetValFsMIFsIpTraceConfigMaxTTL)
{
    return (nmhGetFsIpTraceConfigMaxTTL (u4FsMIFsIpTraceConfigDest,
                                         pi4RetValFsMIFsIpTraceConfigMaxTTL));
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceConfigMinTTL
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                retValFsMIFsIpTraceConfigMinTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceConfigMinTTL (UINT4 u4FsMIFsIpTraceConfigDest,
                                 INT4 *pi4RetValFsMIFsIpTraceConfigMinTTL)
{
    return (nmhGetFsIpTraceConfigMinTTL (u4FsMIFsIpTraceConfigDest,
                                         pi4RetValFsMIFsIpTraceConfigMinTTL));
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceConfigOperStatus
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                retValFsMIFsIpTraceConfigOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceConfigOperStatus (UINT4 u4FsMIFsIpTraceConfigDest,
                                     INT4
                                     *pi4RetValFsMIFsIpTraceConfigOperStatus)
{
    return (nmhGetFsIpTraceConfigOperStatus
            (u4FsMIFsIpTraceConfigDest,
             pi4RetValFsMIFsIpTraceConfigOperStatus));
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceConfigTimeout
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                retValFsMIFsIpTraceConfigTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceConfigTimeout (UINT4 u4FsMIFsIpTraceConfigDest,
                                  INT4 *pi4RetValFsMIFsIpTraceConfigTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpTraceConfigTimeout (u4FsMIFsIpTraceConfigDest,
                                             pi4RetValFsMIFsIpTraceConfigTimeout);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceConfigMtu
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                retValFsMIFsIpTraceConfigMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceConfigMtu (UINT4 u4FsMIFsIpTraceConfigDest,
                              INT4 *pi4RetValFsMIFsIpTraceConfigMtu)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpTraceConfigMtu (u4FsMIFsIpTraceConfigDest,
                                         pi4RetValFsMIFsIpTraceConfigMtu);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpTraceConfigAdminStatus
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                setValFsMIFsIpTraceConfigAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpTraceConfigAdminStatus (UINT4 u4FsMIFsIpTraceConfigDest,
                                      INT4
                                      i4SetValFsMIFsIpTraceConfigAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpTraceConfigAdminStatus (u4FsMIFsIpTraceConfigDest,
                                                 i4SetValFsMIFsIpTraceConfigAdminStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpTraceConfigMaxTTL
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                setValFsMIFsIpTraceConfigMaxTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpTraceConfigMaxTTL (UINT4 u4FsMIFsIpTraceConfigDest,
                                 INT4 i4SetValFsMIFsIpTraceConfigMaxTTL)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpTraceConfigMaxTTL (u4FsMIFsIpTraceConfigDest,
                                            i4SetValFsMIFsIpTraceConfigMaxTTL);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpTraceConfigMinTTL
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                setValFsMIFsIpTraceConfigMinTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpTraceConfigMinTTL (UINT4 u4FsMIFsIpTraceConfigDest,
                                 INT4 i4SetValFsMIFsIpTraceConfigMinTTL)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpTraceConfigMinTTL (u4FsMIFsIpTraceConfigDest,
                                            i4SetValFsMIFsIpTraceConfigMinTTL);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpTraceConfigTimeout
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                setValFsMIFsIpTraceConfigTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpTraceConfigTimeout (UINT4 u4FsMIFsIpTraceConfigDest,
                                  INT4 i4SetValFsMIFsIpTraceConfigTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpTraceConfigTimeout (u4FsMIFsIpTraceConfigDest,
                                             i4SetValFsMIFsIpTraceConfigTimeout);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpTraceConfigMtu
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                setValFsMIFsIpTraceConfigMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpTraceConfigMtu (UINT4 u4FsMIFsIpTraceConfigDest,
                              INT4 i4SetValFsMIFsIpTraceConfigMtu)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpTraceConfigMtu (u4FsMIFsIpTraceConfigDest,
                                         i4SetValFsMIFsIpTraceConfigMtu);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpTraceConfigAdminStatus
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                testValFsMIFsIpTraceConfigAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpTraceConfigAdminStatus (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMIFsIpTraceConfigDest,
                                         INT4
                                         i4TestValFsMIFsIpTraceConfigAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpTraceConfigAdminStatus (pu4ErrorCode,
                                                    u4FsMIFsIpTraceConfigDest,
                                                    i4TestValFsMIFsIpTraceConfigAdminStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpTraceConfigMaxTTL
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                testValFsMIFsIpTraceConfigMaxTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpTraceConfigMaxTTL (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIFsIpTraceConfigDest,
                                    INT4 i4TestValFsMIFsIpTraceConfigMaxTTL)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpTraceConfigMaxTTL (pu4ErrorCode,
                                               u4FsMIFsIpTraceConfigDest,
                                               i4TestValFsMIFsIpTraceConfigMaxTTL);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpTraceConfigMinTTL
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                testValFsMIFsIpTraceConfigMinTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpTraceConfigMinTTL (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMIFsIpTraceConfigDest,
                                    INT4 i4TestValFsMIFsIpTraceConfigMinTTL)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpTraceConfigMinTTL (pu4ErrorCode,
                                               u4FsMIFsIpTraceConfigDest,
                                               i4TestValFsMIFsIpTraceConfigMinTTL);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpTraceConfigTimeout
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                testValFsMIFsIpTraceConfigTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpTraceConfigTimeout (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMIFsIpTraceConfigDest,
                                     INT4 i4TestValFsMIFsIpTraceConfigTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpTraceConfigTimeout (pu4ErrorCode,
                                                u4FsMIFsIpTraceConfigDest,
                                                i4TestValFsMIFsIpTraceConfigTimeout);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpTraceConfigMtu
 Input       :  The Indices
                FsMIFsIpTraceConfigDest

                The Object 
                testValFsMIFsIpTraceConfigMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpTraceConfigMtu (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsMIFsIpTraceConfigDest,
                                 INT4 i4TestValFsMIFsIpTraceConfigMtu)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpTraceConfigMtu (pu4ErrorCode,
                                            u4FsMIFsIpTraceConfigDest,
                                            i4TestValFsMIFsIpTraceConfigMtu);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpTraceConfigTable
 Input       :  The Indices
                FsMIFsIpTraceConfigDest
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpTraceConfigTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIpTraceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpTraceTable
 Input       :  The Indices
                FsMIFsIpTraceDest
                FsMIFsIpTraceHopCount
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpTraceTable (UINT4 u4FsMIFsIpTraceDest,
                                            INT4 i4FsMIFsIpTraceHopCount)
{
    return (nmhValidateIndexInstanceFsIpTraceTable (u4FsMIFsIpTraceDest,
                                                    i4FsMIFsIpTraceHopCount));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpTraceTable
 Input       :  The Indices
                FsMIFsIpTraceDest
                FsMIFsIpTraceHopCount
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpTraceTable (UINT4 *pu4FsMIFsIpTraceDest,
                                    INT4 *pi4FsMIFsIpTraceHopCount)
{
    return (nmhGetFirstIndexFsIpTraceTable (pu4FsMIFsIpTraceDest,
                                            pi4FsMIFsIpTraceHopCount));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpTraceTable
 Input       :  The Indices
                FsMIFsIpTraceDest
                nextFsMIFsIpTraceDest
                FsMIFsIpTraceHopCount
                nextFsMIFsIpTraceHopCount
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpTraceTable (UINT4 u4FsMIFsIpTraceDest,
                                   UINT4 *pu4NextFsMIFsIpTraceDest,
                                   INT4 i4FsMIFsIpTraceHopCount,
                                   INT4 *pi4NextFsMIFsIpTraceHopCount)
{
    return (nmhGetNextIndexFsIpTraceTable (u4FsMIFsIpTraceDest,
                                           pu4NextFsMIFsIpTraceDest,
                                           i4FsMIFsIpTraceHopCount,
                                           pi4NextFsMIFsIpTraceHopCount));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceIntermHop
 Input       :  The Indices
                FsMIFsIpTraceDest
                FsMIFsIpTraceHopCount

                The Object 
                retValFsMIFsIpTraceIntermHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceIntermHop (UINT4 u4FsMIFsIpTraceDest,
                              INT4 i4FsMIFsIpTraceHopCount,
                              UINT4 *pu4RetValFsMIFsIpTraceIntermHop)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpTraceIntermHop (u4FsMIFsIpTraceDest,
                                         i4FsMIFsIpTraceHopCount,
                                         pu4RetValFsMIFsIpTraceIntermHop);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceReachTime1
 Input       :  The Indices
                FsMIFsIpTraceDest
                FsMIFsIpTraceHopCount

                The Object 
                retValFsMIFsIpTraceReachTime1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceReachTime1 (UINT4 u4FsMIFsIpTraceDest,
                               INT4 i4FsMIFsIpTraceHopCount,
                               INT4 *pi4RetValFsMIFsIpTraceReachTime1)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpTraceReachTime1 (u4FsMIFsIpTraceDest, i4FsMIFsIpTraceHopCount,
                                   pi4RetValFsMIFsIpTraceReachTime1);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceReachTime2
 Input       :  The Indices
                FsMIFsIpTraceDest
                FsMIFsIpTraceHopCount

                The Object 
                retValFsMIFsIpTraceReachTime2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceReachTime2 (UINT4 u4FsMIFsIpTraceDest,
                               INT4 i4FsMIFsIpTraceHopCount,
                               INT4 *pi4RetValFsMIFsIpTraceReachTime2)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpTraceReachTime2 (u4FsMIFsIpTraceDest, i4FsMIFsIpTraceHopCount,
                                   pi4RetValFsMIFsIpTraceReachTime2);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpTraceReachTime3
 Input       :  The Indices
                FsMIFsIpTraceDest
                FsMIFsIpTraceHopCount

                The Object 
                retValFsMIFsIpTraceReachTime3
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpTraceReachTime3 (UINT4 u4FsMIFsIpTraceDest,
                               INT4 i4FsMIFsIpTraceHopCount,
                               INT4 *pi4RetValFsMIFsIpTraceReachTime3)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpTraceReachTime3 (u4FsMIFsIpTraceDest, i4FsMIFsIpTraceHopCount,
                                   pi4RetValFsMIFsIpTraceReachTime3);
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIFsIpAddressTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpAddressTable (INT4 i4FsMIStdIpContextId,
                                              UINT4 u4FsMIFsIpAddrTabAddress)
{
    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        return SNMP_FAILURE;
    }

    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsIpAddressTable
            (u4FsMIFsIpAddrTabAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpAddressTable (INT4 *pi4FsMIStdIpContextId,
                                      UINT4 *pu4FsMIFsIpAddrTabAddress)
{
    if (NULL == UtilIpGetCxt (IP_DEFAULT_CONTEXT))
    {
        return SNMP_FAILURE;

    }
    *pi4FsMIStdIpContextId = IP_DEFAULT_CONTEXT;

    return (nmhGetFirstIndexFsIpAddressTable (pu4FsMIFsIpAddrTabAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIFsIpAddrTabAddress
                nextFsMIFsIpAddrTabAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpAddressTable (INT4 i4FsMIStdIpContextId,
                                     INT4 *pi4NextFsMIStdIpContextId,
                                     UINT4 u4FsMIFsIpAddrTabAddress,
                                     UINT4 *pu4NextFsMIFsIpAddrTabAddress)
{
    UNUSED_PARAM (i4FsMIStdIpContextId);
    if (NULL == UtilIpGetCxt (IP_DEFAULT_CONTEXT))
    {
        return SNMP_FAILURE;
    }
    *pi4NextFsMIStdIpContextId = IP_DEFAULT_CONTEXT;
    return (nmhGetNextIndexFsIpAddressTable (u4FsMIFsIpAddrTabAddress,
                                             pu4NextFsMIFsIpAddrTabAddress));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpAddrTabIfaceId
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                retValFsMIFsIpAddrTabIfaceId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpAddrTabIfaceId (INT4 i4FsMIStdIpContextId,
                              UINT4 u4FsMIFsIpAddrTabAddress,
                              INT4 *pi4RetValFsMIFsIpAddrTabIfaceId)
{
    INT1                i1Return = SNMP_FAILURE;

    UNUSED_PARAM (i4FsMIStdIpContextId);

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpAddrTabIfaceId (u4FsMIFsIpAddrTabAddress,
                                  pi4RetValFsMIFsIpAddrTabIfaceId);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpAddrTabAdvertise
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                retValFsMIFsIpAddrTabAdvertise
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpAddrTabAdvertise (INT4 i4FsMIStdIpContextId,
                                UINT4 u4FsMIFsIpAddrTabAddress,
                                INT4 *pi4RetValFsMIFsIpAddrTabAdvertise)
{
    INT1                i1Return = SNMP_FAILURE;

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpAddrTabAdvertise (u4FsMIFsIpAddrTabAddress,
                                    pi4RetValFsMIFsIpAddrTabAdvertise);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpAddrTabPreflevel
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                retValFsMIFsIpAddrTabPreflevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpAddrTabPreflevel (INT4 i4FsMIStdIpContextId,
                                UINT4 u4FsMIFsIpAddrTabAddress,
                                INT4 *pi4RetValFsMIFsIpAddrTabPreflevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        return i1Return;
    }
    i1Return =
        nmhGetFsIpAddrTabPreflevel (u4FsMIFsIpAddrTabAddress,
                                    pi4RetValFsMIFsIpAddrTabPreflevel);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpAddrTabStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                retValFsMIFsIpAddrTabStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpAddrTabStatus (INT4 i4FsMIStdIpContextId,
                             UINT4 u4FsMIFsIpAddrTabAddress,
                             INT4 *pi4RetValFsMIFsIpAddrTabStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        return i1Return;
    }
    i1Return =
        nmhGetFsIpAddrTabStatus (u4FsMIFsIpAddrTabAddress,
                                 pi4RetValFsMIFsIpAddrTabStatus);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpAddrTabAdvertise
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                setValFsMIFsIpAddrTabAdvertise
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpAddrTabAdvertise (INT4 i4FsMIStdIpContextId,
                                UINT4 u4FsMIFsIpAddrTabAddress,
                                INT4 i4SetValFsMIFsIpAddrTabAdvertise)
{
    INT1                i1Return = SNMP_FAILURE;

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        return i1Return;
    }
    i1Return =
        nmhSetFsIpAddrTabAdvertise (u4FsMIFsIpAddrTabAddress,
                                    i4SetValFsMIFsIpAddrTabAdvertise);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpAddrTabPreflevel
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                setValFsMIFsIpAddrTabPreflevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpAddrTabPreflevel (INT4 i4FsMIStdIpContextId,
                                UINT4 u4FsMIFsIpAddrTabAddress,
                                INT4 i4SetValFsMIFsIpAddrTabPreflevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        return i1Return;
    }
    i1Return =
        nmhSetFsIpAddrTabPreflevel (u4FsMIFsIpAddrTabAddress,
                                    i4SetValFsMIFsIpAddrTabPreflevel);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpAddrTabAdvertise
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                testValFsMIFsIpAddrTabAdvertise
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpAddrTabAdvertise (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpContextId,
                                   UINT4 u4FsMIFsIpAddrTabAddress,
                                   INT4 i4TestValFsMIFsIpAddrTabAdvertise)
{
    INT1                i1Return = SNMP_FAILURE;

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return i1Return;
    }
    i1Return =
        nmhTestv2FsIpAddrTabAdvertise (pu4ErrorCode, u4FsMIFsIpAddrTabAddress,
                                       i4TestValFsMIFsIpAddrTabAdvertise);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpAddrTabPreflevel
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress

                The Object 
                testValFsMIFsIpAddrTabPreflevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpAddrTabPreflevel (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdIpContextId,
                                   UINT4 u4FsMIFsIpAddrTabAddress,
                                   INT4 i4TestValFsMIFsIpAddrTabPreflevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (i4FsMIStdIpContextId != IP_DEFAULT_CONTEXT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return i1Return;
    }
    i1Return =
        nmhTestv2FsIpAddrTabPreflevel (pu4ErrorCode, u4FsMIFsIpAddrTabAddress,
                                       i4TestValFsMIFsIpAddrTabPreflevel);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpAddressTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpAddrTabAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpAddressTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIpRtrLstTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpRtrLstTable
 Input       :  The Indices
                FsMIFsIpRtrLstAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpRtrLstTable (UINT4 u4FsMIFsIpRtrLstAddress)
{
    return (nmhValidateIndexInstanceFsIpRtrLstTable (u4FsMIFsIpRtrLstAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpRtrLstTable
 Input       :  The Indices
                FsMIFsIpRtrLstAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpRtrLstTable (UINT4 *pu4FsMIFsIpRtrLstAddress)
{
    return (nmhGetFirstIndexFsIpRtrLstTable (pu4FsMIFsIpRtrLstAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpRtrLstTable
 Input       :  The Indices
                FsMIFsIpRtrLstAddress
                nextFsMIFsIpRtrLstAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpRtrLstTable (UINT4 u4FsMIFsIpRtrLstAddress,
                                    UINT4 *pu4NextFsMIFsIpRtrLstAddress)
{
    return (nmhGetNextIndexFsIpRtrLstTable
            (u4FsMIFsIpRtrLstAddress, pu4NextFsMIFsIpRtrLstAddress));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRtrLstIface
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                retValFsMIFsIpRtrLstIface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRtrLstIface (UINT4 u4FsMIFsIpRtrLstAddress,
                           INT4 *pi4RetValFsMIFsIpRtrLstIface)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpRtrLstIface (u4FsMIFsIpRtrLstAddress,
                               pi4RetValFsMIFsIpRtrLstIface);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRtrLstPreflevel
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                retValFsMIFsIpRtrLstPreflevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRtrLstPreflevel (UINT4 u4FsMIFsIpRtrLstAddress,
                               INT4 *pi4RetValFsMIFsIpRtrLstPreflevel)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpRtrLstPreflevel (u4FsMIFsIpRtrLstAddress,
                                   pi4RetValFsMIFsIpRtrLstPreflevel);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRtrLstStatic
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                retValFsMIFsIpRtrLstStatic
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRtrLstStatic (UINT4 u4FsMIFsIpRtrLstAddress,
                            INT4 *pi4RetValFsMIFsIpRtrLstStatic)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpRtrLstStatic (u4FsMIFsIpRtrLstAddress,
                                pi4RetValFsMIFsIpRtrLstStatic);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRtrLstStatus
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                retValFsMIFsIpRtrLstStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRtrLstStatus (UINT4 u4FsMIFsIpRtrLstAddress,
                            INT4 *pi4RetValFsMIFsIpRtrLstStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpRtrLstStatus (u4FsMIFsIpRtrLstAddress,
                                pi4RetValFsMIFsIpRtrLstStatus);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRtrLstIface
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                setValFsMIFsIpRtrLstIface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRtrLstIface (UINT4 u4FsMIFsIpRtrLstAddress,
                           INT4 i4SetValFsMIFsIpRtrLstIface)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIpRtrLstIface (u4FsMIFsIpRtrLstAddress,
                               i4SetValFsMIFsIpRtrLstIface);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRtrLstPreflevel
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                setValFsMIFsIpRtrLstPreflevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRtrLstPreflevel (UINT4 u4FsMIFsIpRtrLstAddress,
                               INT4 i4SetValFsMIFsIpRtrLstPreflevel)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIpRtrLstPreflevel (u4FsMIFsIpRtrLstAddress,
                                   i4SetValFsMIFsIpRtrLstPreflevel);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRtrLstStatus
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                setValFsMIFsIpRtrLstStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRtrLstStatus (UINT4 u4FsMIFsIpRtrLstAddress,
                            INT4 i4SetValFsMIFsIpRtrLstStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIpRtrLstStatus (u4FsMIFsIpRtrLstAddress,
                                i4SetValFsMIFsIpRtrLstStatus);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRtrLstIface
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                testValFsMIFsIpRtrLstIface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRtrLstIface (UINT4 *pu4ErrorCode,
                              UINT4 u4FsMIFsIpRtrLstAddress,
                              INT4 i4TestValFsMIFsIpRtrLstIface)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpRtrLstIface (pu4ErrorCode, u4FsMIFsIpRtrLstAddress,
                                  i4TestValFsMIFsIpRtrLstIface);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRtrLstPreflevel
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                testValFsMIFsIpRtrLstPreflevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRtrLstPreflevel (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMIFsIpRtrLstAddress,
                                  INT4 i4TestValFsMIFsIpRtrLstPreflevel)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpRtrLstPreflevel (pu4ErrorCode, u4FsMIFsIpRtrLstAddress,
                                      i4TestValFsMIFsIpRtrLstPreflevel);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRtrLstStatus
 Input       :  The Indices
                FsMIFsIpRtrLstAddress

                The Object 
                testValFsMIFsIpRtrLstStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRtrLstStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4FsMIFsIpRtrLstAddress,
                               INT4 i4TestValFsMIFsIpRtrLstStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpRtrLstStatus (pu4ErrorCode, u4FsMIFsIpRtrLstAddress,
                                   i4TestValFsMIFsIpRtrLstStatus);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpRtrLstTable
 Input       :  The Indices
                FsMIFsIpRtrLstAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpRtrLstTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIpPathMtuTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpPathMtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpPathMtuTable (INT4 i4FsMIStdIpContextId,
                                              UINT4 u4FsMIFsIpPmtuDestination,
                                              INT4 i4FsMIFsIpPmtuTos)
{
    if ((i4FsMIStdIpContextId < IP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsIpPathMtuTable (u4FsMIFsIpPmtuDestination,
                                                  i4FsMIFsIpPmtuTos)
        == SNMP_FAILURE)
    {
        UtilIpReleaseCxt ();
        return SNMP_FAILURE;
    }
    UtilIpReleaseCxt ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpPathMtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpPathMtuTable (INT4 *pi4FsMIStdIpContextId,
                                      UINT4 *pu4FsMIFsIpPmtuDestination,
                                      INT4 *pi4FsMIFsIpPmtuTos)
{
    INT4                i4PrevCxtId = 0;
    INT1                i1Return = VCM_FAILURE;

    i1Return =
        (INT1) VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);

    while (i1Return == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (nmhGetFirstIndexFsIpPathMtuTable (pu4FsMIFsIpPmtuDestination,
                                              pi4FsMIFsIpPmtuTos)
            == SNMP_SUCCESS)
        {
            UtilIpReleaseCxt ();
            return SNMP_SUCCESS;
        }
        UtilIpReleaseCxt ();
        i4PrevCxtId = *pi4FsMIStdIpContextId;
        i1Return = (INT1) VcmGetNextActiveL3Context ((UINT4) i4PrevCxtId,
                                                     (UINT4 *)
                                                     pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpPathMtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIFsIpPmtuDestination
                nextFsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos
                nextFsMIFsIpPmtuTos
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpPathMtuTable (INT4 i4FsMIStdIpContextId,
                                     INT4 *pi4NextFsMIStdIpContextId,
                                     UINT4 u4FsMIFsIpPmtuDestination,
                                     UINT4 *pu4NextFsMIFsIpPmtuDestination,
                                     INT4 i4FsMIFsIpPmtuTos,
                                     INT4 *pi4NextFsMIFsIpPmtuTos)
{
    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhGetNextIndexFsIpPathMtuTable (u4FsMIFsIpPmtuDestination,
                                             pu4NextFsMIFsIpPmtuDestination,
                                             i4FsMIFsIpPmtuTos,
                                             pi4NextFsMIFsIpPmtuTos)
            == SNMP_SUCCESS)
        {
            *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
            UtilIpReleaseCxt ();
            return SNMP_SUCCESS;
        }

        while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                          (UINT4 *) pi4NextFsMIStdIpContextId)
               == VCM_SUCCESS)
        {
            if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) ==
                SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if (nmhGetFirstIndexFsIpPathMtuTable
                (pu4NextFsMIFsIpPmtuDestination,
                 pi4NextFsMIFsIpPmtuTos) == SNMP_SUCCESS)
            {
                UtilIpReleaseCxt ();
                return SNMP_SUCCESS;
            }
            UtilIpReleaseCxt ();
            i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
        }
        return SNMP_FAILURE;
    }

    /* Invalid context is given. Get the PMTU entry from the next valid context
     * and return.
     */
    while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhGetFirstIndexFsIpPathMtuTable (pu4NextFsMIFsIpPmtuDestination,
                                              pi4NextFsMIFsIpPmtuTos)
            == SNMP_SUCCESS)
        {
            UtilIpReleaseCxt ();
            return SNMP_SUCCESS;
        }
        UtilIpReleaseCxt ();
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpPathMtu
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                retValFsMIFsIpPathMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpPathMtu (INT4 i4FsMIStdIpContextId,
                       UINT4 u4FsMIFsIpPmtuDestination, INT4 i4FsMIFsIpPmtuTos,
                       INT4 *pi4RetValFsMIFsIpPathMtu)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpPathMtu (u4FsMIFsIpPmtuDestination, i4FsMIFsIpPmtuTos,
                           pi4RetValFsMIFsIpPathMtu);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpPmtuDisc
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                retValFsMIFsIpPmtuDisc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpPmtuDisc (INT4 i4FsMIStdIpContextId,
                        UINT4 u4FsMIFsIpPmtuDestination, INT4 i4FsMIFsIpPmtuTos,
                        INT4 *pi4RetValFsMIFsIpPmtuDisc)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpPmtuDisc (u4FsMIFsIpPmtuDestination, i4FsMIFsIpPmtuTos,
                            pi4RetValFsMIFsIpPmtuDisc);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpPmtuEntryStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                retValFsMIFsIpPmtuEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpPmtuEntryStatus (INT4 i4FsMIStdIpContextId,
                               UINT4 u4FsMIFsIpPmtuDestination,
                               INT4 i4FsMIFsIpPmtuTos,
                               INT4 *pi4RetValFsMIFsIpPmtuEntryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpPmtuEntryStatus (u4FsMIFsIpPmtuDestination, i4FsMIFsIpPmtuTos,
                                   pi4RetValFsMIFsIpPmtuEntryStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpPathMtu
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                setValFsMIFsIpPathMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpPathMtu (INT4 i4FsMIStdIpContextId,
                       UINT4 u4FsMIFsIpPmtuDestination, INT4 i4FsMIFsIpPmtuTos,
                       INT4 i4SetValFsMIFsIpPathMtu)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpPathMtu (u4FsMIFsIpPmtuDestination, i4FsMIFsIpPmtuTos,
                           i4SetValFsMIFsIpPathMtu);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpPmtuDisc
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                setValFsMIFsIpPmtuDisc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpPmtuDisc (INT4 i4FsMIStdIpContextId,
                        UINT4 u4FsMIFsIpPmtuDestination, INT4 i4FsMIFsIpPmtuTos,
                        INT4 i4SetValFsMIFsIpPmtuDisc)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpPmtuDisc (u4FsMIFsIpPmtuDestination, i4FsMIFsIpPmtuTos,
                            i4SetValFsMIFsIpPmtuDisc);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpPmtuEntryStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                setValFsMIFsIpPmtuEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpPmtuEntryStatus (INT4 i4FsMIStdIpContextId,
                               UINT4 u4FsMIFsIpPmtuDestination,
                               INT4 i4FsMIFsIpPmtuTos,
                               INT4 i4SetValFsMIFsIpPmtuEntryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpPmtuEntryStatus (u4FsMIFsIpPmtuDestination, i4FsMIFsIpPmtuTos,
                                   i4SetValFsMIFsIpPmtuEntryStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpPathMtu
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                testValFsMIFsIpPathMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpPathMtu (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                          UINT4 u4FsMIFsIpPmtuDestination,
                          INT4 i4FsMIFsIpPmtuTos, INT4 i4TestValFsMIFsIpPathMtu)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpPathMtu (pu4ErrorCode, u4FsMIFsIpPmtuDestination,
                              i4FsMIFsIpPmtuTos, i4TestValFsMIFsIpPathMtu);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpPmtuDisc
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                testValFsMIFsIpPmtuDisc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpPmtuDisc (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                           UINT4 u4FsMIFsIpPmtuDestination,
                           INT4 i4FsMIFsIpPmtuTos,
                           INT4 i4TestValFsMIFsIpPmtuDisc)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpPmtuDisc (pu4ErrorCode, u4FsMIFsIpPmtuDestination,
                               i4FsMIFsIpPmtuTos, i4TestValFsMIFsIpPmtuDisc);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpPmtuEntryStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos

                The Object 
                testValFsMIFsIpPmtuEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpPmtuEntryStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpContextId,
                                  UINT4 u4FsMIFsIpPmtuDestination,
                                  INT4 i4FsMIFsIpPmtuTos,
                                  INT4 i4TestValFsMIFsIpPmtuEntryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpPmtuEntryStatus (pu4ErrorCode, u4FsMIFsIpPmtuDestination,
                                      i4FsMIFsIpPmtuTos,
                                      i4TestValFsMIFsIpPmtuEntryStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpPathMtuTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpPmtuDestination
                FsMIFsIpPmtuTos
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpPathMtuTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIpCommonRoutingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable (INT4 i4FsMIStdIpContextId,
                                                    UINT4 u4FsMIFsIpRouteDest,
                                                    UINT4 u4FsMIFsIpRouteMask,
                                                    INT4 i4FsMIFsIpRouteTos,
                                                    UINT4
                                                    u4FsMIFsIpRouteNextHop,
                                                    INT4 i4FsMIFsIpRouteProto)
{
    if ((i4FsMIStdIpContextId < IP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (VCM_FALSE == VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId))
    {
        return SNMP_FAILURE;
    }
    return (nmhValidateIndexInstanceFsIpCommonRoutingTable (u4FsMIFsIpRouteDest,
                                                            u4FsMIFsIpRouteMask,
                                                            i4FsMIFsIpRouteTos,
                                                            u4FsMIFsIpRouteNextHop,
                                                            i4FsMIFsIpRouteProto));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpCommonRoutingTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpCommonRoutingTable (INT4 *pi4FsMIStdIpContextId,
                                            UINT4 *pu4FsMIFsIpRouteDest,
                                            UINT4 *pu4FsMIFsIpRouteMask,
                                            INT4 *pi4FsMIFsIpRouteTos,
                                            UINT4 *pu4FsMIFsIpRouteNextHop,
                                            INT4 *pi4FsMIFsIpRouteProto)
{
    UINT4               u4PrevCxt = 0;
    INT1                i1RetVal = VCM_FAILURE;

    i1RetVal =
        (INT1) VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);
    while (i1RetVal == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhGetFirstIndexFsIpCommonRoutingTable (pu4FsMIFsIpRouteDest,
                                                    pu4FsMIFsIpRouteMask,
                                                    pi4FsMIFsIpRouteTos,
                                                    pu4FsMIFsIpRouteNextHop,
                                                    pi4FsMIFsIpRouteProto)
            == SNMP_SUCCESS)
        {
            UtilIpReleaseCxt ();
            return SNMP_SUCCESS;
        }
        UtilIpReleaseCxt ();
        u4PrevCxt = (UINT4) *pi4FsMIStdIpContextId;
        i1RetVal = (INT1) VcmGetNextActiveL3Context (u4PrevCxt,
                                                     (UINT4 *)
                                                     pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpCommonRoutingTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIFsIpRouteDest
                nextFsMIFsIpRouteDest
                FsMIFsIpRouteMask
                nextFsMIFsIpRouteMask
                FsMIFsIpRouteTos
                nextFsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                nextFsMIFsIpRouteNextHop
                FsMIFsIpRouteProto
                nextFsMIFsIpRouteProto
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpCommonRoutingTable (INT4 i4FsMIStdIpContextId,
                                           INT4 *pi4NextFsMIStdIpContextId,
                                           UINT4 u4FsMIFsIpRouteDest,
                                           UINT4 *pu4NextFsMIFsIpRouteDest,
                                           UINT4 u4FsMIFsIpRouteMask,
                                           UINT4 *pu4NextFsMIFsIpRouteMask,
                                           INT4 i4FsMIFsIpRouteTos,
                                           INT4 *pi4NextFsMIFsIpRouteTos,
                                           UINT4 u4FsMIFsIpRouteNextHop,
                                           UINT4 *pu4NextFsMIFsIpRouteNextHop,
                                           INT4 i4FsMIFsIpRouteProto,
                                           INT4 *pi4NextFsMIFsIpRouteProto)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4RetVal = nmhGetNextIndexFsIpCommonRoutingTable (u4FsMIFsIpRouteDest,
                                                          pu4NextFsMIFsIpRouteDest,
                                                          u4FsMIFsIpRouteMask,
                                                          pu4NextFsMIFsIpRouteMask,
                                                          i4FsMIFsIpRouteTos,
                                                          pi4NextFsMIFsIpRouteTos,
                                                          u4FsMIFsIpRouteNextHop,
                                                          pu4NextFsMIFsIpRouteNextHop,
                                                          i4FsMIFsIpRouteProto,
                                                          pi4NextFsMIFsIpRouteProto);
        UtilIpReleaseCxt ();

        if (i4RetVal == SNMP_SUCCESS)
        {
            *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
            return SNMP_SUCCESS;
        }

        while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                          (UINT4 *) pi4NextFsMIStdIpContextId)
               == VCM_SUCCESS)
        {
            if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) ==
                SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i4RetVal = nmhGetFirstIndexFsIpCommonRoutingTable
                (pu4NextFsMIFsIpRouteDest, pu4NextFsMIFsIpRouteMask,
                 pi4NextFsMIFsIpRouteTos, pu4NextFsMIFsIpRouteNextHop,
                 pi4NextFsMIFsIpRouteProto);
            UtilIpReleaseCxt ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
        }
        return SNMP_FAILURE;
    }

    while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4RetVal = nmhGetFirstIndexFsIpCommonRoutingTable
            (pu4NextFsMIFsIpRouteDest, pu4NextFsMIFsIpRouteMask,
             pi4NextFsMIFsIpRouteTos, pu4NextFsMIFsIpRouteNextHop,
             pi4NextFsMIFsIpRouteProto);
        UtilIpReleaseCxt ();

        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRouteProtoInstanceId
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRouteProtoInstanceId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRouteProtoInstanceId (INT4 i4FsMIStdIpContextId,
                                    UINT4 u4FsMIFsIpRouteDest,
                                    UINT4 u4FsMIFsIpRouteMask,
                                    INT4 i4FsMIFsIpRouteTos,
                                    UINT4 u4FsMIFsIpRouteNextHop,
                                    INT4 i4FsMIFsIpRouteProto,
                                    INT4 *pi4RetValFsMIFsIpRouteProtoInstanceId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRouteProtoInstanceId (u4FsMIFsIpRouteDest,
                                        u4FsMIFsIpRouteMask, i4FsMIFsIpRouteTos,
                                        u4FsMIFsIpRouteNextHop,
                                        i4FsMIFsIpRouteProto,
                                        pi4RetValFsMIFsIpRouteProtoInstanceId);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRouteIfIndex (INT4 i4FsMIStdIpContextId,
                            UINT4 u4FsMIFsIpRouteDest,
                            UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                            UINT4 u4FsMIFsIpRouteNextHop,
                            INT4 i4FsMIFsIpRouteProto,
                            INT4 *pi4RetValFsMIFsIpRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRouteIfIndex (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                                i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                                i4FsMIFsIpRouteProto,
                                pi4RetValFsMIFsIpRouteIfIndex);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRouteType (INT4 i4FsMIStdIpContextId, UINT4 u4FsMIFsIpRouteDest,
                         UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                         UINT4 u4FsMIFsIpRouteNextHop,
                         INT4 i4FsMIFsIpRouteProto,
                         INT4 *pi4RetValFsMIFsIpRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRouteType (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                             i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                             i4FsMIFsIpRouteProto, pi4RetValFsMIFsIpRouteType);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRouteAge
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRouteAge (INT4 i4FsMIStdIpContextId, UINT4 u4FsMIFsIpRouteDest,
                        UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                        UINT4 u4FsMIFsIpRouteNextHop, INT4 i4FsMIFsIpRouteProto,
                        INT4 *pi4RetValFsMIFsIpRouteAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRouteAge (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                            i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                            i4FsMIFsIpRouteProto, pi4RetValFsMIFsIpRouteAge);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRouteNextHopAS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRouteNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRouteNextHopAS (INT4 i4FsMIStdIpContextId,
                              UINT4 u4FsMIFsIpRouteDest,
                              UINT4 u4FsMIFsIpRouteMask,
                              INT4 i4FsMIFsIpRouteTos,
                              UINT4 u4FsMIFsIpRouteNextHop,
                              INT4 i4FsMIFsIpRouteProto,
                              INT4 *pi4RetValFsMIFsIpRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRouteNextHopAS (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                                  i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                                  i4FsMIFsIpRouteProto,
                                  pi4RetValFsMIFsIpRouteNextHopAS);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRouteMetric1
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRouteMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRouteMetric1 (INT4 i4FsMIStdIpContextId,
                            UINT4 u4FsMIFsIpRouteDest,
                            UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                            UINT4 u4FsMIFsIpRouteNextHop,
                            INT4 i4FsMIFsIpRouteProto,
                            INT4 *pi4RetValFsMIFsIpRouteMetric1)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRouteMetric1 (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                                i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                                i4FsMIFsIpRouteProto,
                                pi4RetValFsMIFsIpRouteMetric1);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRoutePreference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRoutePreference (INT4 i4FsMIStdIpContextId,
                               UINT4 u4FsMIFsIpRouteDest,
                               UINT4 u4FsMIFsIpRouteMask,
                               INT4 i4FsMIFsIpRouteTos,
                               UINT4 u4FsMIFsIpRouteNextHop,
                               INT4 i4FsMIFsIpRouteProto,
                               INT4 *pi4RetValFsMIFsIpRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRoutePreference (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                                   i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                                   i4FsMIFsIpRouteProto,
                                   pi4RetValFsMIFsIpRoutePreference);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpRouteStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                retValFsMIFsIpRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpRouteStatus (INT4 i4FsMIStdIpContextId, UINT4 u4FsMIFsIpRouteDest,
                           UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                           UINT4 u4FsMIFsIpRouteNextHop,
                           INT4 i4FsMIFsIpRouteProto,
                           INT4 *pi4RetValFsMIFsIpRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIpRouteStatus (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                               i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                               i4FsMIFsIpRouteProto,
                               pi4RetValFsMIFsIpRouteStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                setValFsMIFsIpRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRouteIfIndex (INT4 i4FsMIStdIpContextId,
                            UINT4 u4FsMIFsIpRouteDest,
                            UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                            UINT4 u4FsMIFsIpRouteNextHop,
                            INT4 i4FsMIFsIpRouteProto,
                            INT4 i4SetValFsMIFsIpRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpRouteIfIndex (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                                i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                                i4FsMIFsIpRouteProto,
                                i4SetValFsMIFsIpRouteIfIndex);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                setValFsMIFsIpRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRouteType (INT4 i4FsMIStdIpContextId, UINT4 u4FsMIFsIpRouteDest,
                         UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                         UINT4 u4FsMIFsIpRouteNextHop,
                         INT4 i4FsMIFsIpRouteProto,
                         INT4 i4SetValFsMIFsIpRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpRouteType (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                             i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                             i4FsMIFsIpRouteProto, i4SetValFsMIFsIpRouteType);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRouteNextHopAS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                setValFsMIFsIpRouteNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRouteNextHopAS (INT4 i4FsMIStdIpContextId,
                              UINT4 u4FsMIFsIpRouteDest,
                              UINT4 u4FsMIFsIpRouteMask,
                              INT4 i4FsMIFsIpRouteTos,
                              UINT4 u4FsMIFsIpRouteNextHop,
                              INT4 i4FsMIFsIpRouteProto,
                              INT4 i4SetValFsMIFsIpRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpRouteNextHopAS (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                                  i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                                  i4FsMIFsIpRouteProto,
                                  i4SetValFsMIFsIpRouteNextHopAS);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRoutePreference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                setValFsMIFsIpRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRoutePreference (INT4 i4FsMIStdIpContextId,
                               UINT4 u4FsMIFsIpRouteDest,
                               UINT4 u4FsMIFsIpRouteMask,
                               INT4 i4FsMIFsIpRouteTos,
                               UINT4 u4FsMIFsIpRouteNextHop,
                               INT4 i4FsMIFsIpRouteProto,
                               INT4 i4SetValFsMIFsIpRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpRoutePreference (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                                   i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                                   i4FsMIFsIpRouteProto,
                                   i4SetValFsMIFsIpRoutePreference);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpRouteStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                setValFsMIFsIpRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpRouteStatus (INT4 i4FsMIStdIpContextId, UINT4 u4FsMIFsIpRouteDest,
                           UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                           UINT4 u4FsMIFsIpRouteNextHop,
                           INT4 i4FsMIFsIpRouteProto,
                           INT4 i4SetValFsMIFsIpRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIpRouteStatus (u4FsMIFsIpRouteDest, u4FsMIFsIpRouteMask,
                               i4FsMIFsIpRouteTos, u4FsMIFsIpRouteNextHop,
                               i4FsMIFsIpRouteProto,
                               i4SetValFsMIFsIpRouteStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRouteIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                testValFsMIFsIpRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRouteIfIndex (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               UINT4 u4FsMIFsIpRouteDest,
                               UINT4 u4FsMIFsIpRouteMask,
                               INT4 i4FsMIFsIpRouteTos,
                               UINT4 u4FsMIFsIpRouteNextHop,
                               INT4 i4FsMIFsIpRouteProto,
                               INT4 i4TestValFsMIFsIpRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpRouteIfIndex (pu4ErrorCode, u4FsMIFsIpRouteDest,
                                   u4FsMIFsIpRouteMask, i4FsMIFsIpRouteTos,
                                   u4FsMIFsIpRouteNextHop, i4FsMIFsIpRouteProto,
                                   i4TestValFsMIFsIpRouteIfIndex);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRouteType
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                testValFsMIFsIpRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRouteType (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                            UINT4 u4FsMIFsIpRouteDest,
                            UINT4 u4FsMIFsIpRouteMask, INT4 i4FsMIFsIpRouteTos,
                            UINT4 u4FsMIFsIpRouteNextHop,
                            INT4 i4FsMIFsIpRouteProto,
                            INT4 i4TestValFsMIFsIpRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpRouteType (pu4ErrorCode, u4FsMIFsIpRouteDest,
                                u4FsMIFsIpRouteMask, i4FsMIFsIpRouteTos,
                                u4FsMIFsIpRouteNextHop, i4FsMIFsIpRouteProto,
                                i4TestValFsMIFsIpRouteType);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRouteNextHopAS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                testValFsMIFsIpRouteNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRouteNextHopAS (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                                 UINT4 u4FsMIFsIpRouteDest,
                                 UINT4 u4FsMIFsIpRouteMask,
                                 INT4 i4FsMIFsIpRouteTos,
                                 UINT4 u4FsMIFsIpRouteNextHop,
                                 INT4 i4FsMIFsIpRouteProto,
                                 INT4 i4TestValFsMIFsIpRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpRouteNextHopAS (pu4ErrorCode, u4FsMIFsIpRouteDest,
                                     u4FsMIFsIpRouteMask, i4FsMIFsIpRouteTos,
                                     u4FsMIFsIpRouteNextHop,
                                     i4FsMIFsIpRouteProto,
                                     i4TestValFsMIFsIpRouteNextHopAS);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRoutePreference
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                testValFsMIFsIpRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRoutePreference (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdIpContextId,
                                  UINT4 u4FsMIFsIpRouteDest,
                                  UINT4 u4FsMIFsIpRouteMask,
                                  INT4 i4FsMIFsIpRouteTos,
                                  UINT4 u4FsMIFsIpRouteNextHop,
                                  INT4 i4FsMIFsIpRouteProto,
                                  INT4 i4TestValFsMIFsIpRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpRoutePreference (pu4ErrorCode, u4FsMIFsIpRouteDest,
                                      u4FsMIFsIpRouteMask, i4FsMIFsIpRouteTos,
                                      u4FsMIFsIpRouteNextHop,
                                      i4FsMIFsIpRouteProto,
                                      i4TestValFsMIFsIpRoutePreference);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpRouteStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto

                The Object 
                testValFsMIFsIpRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpRouteStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              UINT4 u4FsMIFsIpRouteDest,
                              UINT4 u4FsMIFsIpRouteMask,
                              INT4 i4FsMIFsIpRouteTos,
                              UINT4 u4FsMIFsIpRouteNextHop,
                              INT4 i4FsMIFsIpRouteProto,
                              INT4 i4TestValFsMIFsIpRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIpRouteStatus (pu4ErrorCode, u4FsMIFsIpRouteDest,
                                  u4FsMIFsIpRouteMask, i4FsMIFsIpRouteTos,
                                  u4FsMIFsIpRouteNextHop, i4FsMIFsIpRouteProto,
                                  i4TestValFsMIFsIpRouteStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpCommonRoutingTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpRouteDest
                FsMIFsIpRouteMask
                FsMIFsIpRouteTos
                FsMIFsIpRouteNextHop
                FsMIFsIpRouteProto
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpCommonRoutingTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIpifTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpifTable
 Input       :  The Indices
                FsMIFsIpifIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpifTable (INT4 i4FsMIFsIpifIndex)
{
    return (nmhValidateIndexInstanceFsIpifTable (i4FsMIFsIpifIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpifTable
 Input       :  The Indices
                FsMIFsIpifIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpifTable (INT4 *pi4FsMIFsIpifIndex)
{
    return (nmhGetFirstIndexFsIpifTable (pi4FsMIFsIpifIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpifTable
 Input       :  The Indices
                FsMIFsIpifIndex
                nextFsMIFsIpifIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpifTable (INT4 i4FsMIFsIpifIndex,
                                INT4 *pi4NextFsMIFsIpifIndex)
{
    return (nmhGetNextIndexFsIpifTable (i4FsMIFsIpifIndex,
                                        pi4NextFsMIFsIpifIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpifMaxReasmSize
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                retValFsMIFsIpifMaxReasmSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpifMaxReasmSize (INT4 i4FsMIFsIpifIndex,
                              INT4 *pi4RetValFsMIFsIpifMaxReasmSize)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpifMaxReasmSize (i4FsMIFsIpifIndex,
                                         pi4RetValFsMIFsIpifMaxReasmSize);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpifIcmpRedirectEnable
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                retValFsMIFsIpifIcmpRedirectEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpifIcmpRedirectEnable (INT4 i4FsMIFsIpifIndex,
                                    INT4 *pi4RetValFsMIFsIpifIcmpRedirectEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpifIcmpRedirectEnable
        (i4FsMIFsIpifIndex, pi4RetValFsMIFsIpifIcmpRedirectEnable);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpifDrtBcastFwdingEnable
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                retValFsMIFsIpifDrtBcastFwdingEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpifDrtBcastFwdingEnable (INT4 i4FsMIFsIpifIndex,
                                      INT4
                                      *pi4RetValFsMIFsIpifDrtBcastFwdingEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpifDrtBcastFwdingEnable
        (i4FsMIFsIpifIndex, pi4RetValFsMIFsIpifDrtBcastFwdingEnable);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpifContextId
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                retValFsMIFsIpifContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpifContextId (INT4 i4FsMIFsIpifIndex,
                           INT4 *pi4RetValFsMIFsIpifContextId)
{
    if (VcmGetContextIdFromCfaIfIndex ((UINT4) i4FsMIFsIpifIndex,
                                       (UINT4 *) pi4RetValFsMIFsIpifContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpifProxyArpAdminStatus
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                retValFsMIFsIpifProxyArpAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpifProxyArpAdminStatus (INT4 i4FsMIFsIpifIndex,
                                     INT4
                                     *pi4RetValFsMIFsIpifProxyArpAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpifProxyArpAdminStatus
        (i4FsMIFsIpifIndex, pi4RetValFsMIFsIpifProxyArpAdminStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpifLocalProxyArpAdminStatus
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object
                retValFsMIFsIpifLocalProxyArpAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpifLocalProxyArpAdminStatus (INT4 i4FsMIFsIpifIndex,
                                          INT4
                                          *pi4RetValFsMIFsIpifLocalProxyArpAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIpifLocalProxyArpAdminStatus
        (i4FsMIFsIpifIndex, pi4RetValFsMIFsIpifLocalProxyArpAdminStatus);
    return i1Return;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpifMaxReasmSize
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                setValFsMIFsIpifMaxReasmSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpifMaxReasmSize (INT4 i4FsMIFsIpifIndex,
                              INT4 i4SetValFsMIFsIpifMaxReasmSize)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpifMaxReasmSize (i4FsMIFsIpifIndex,
                                         i4SetValFsMIFsIpifMaxReasmSize);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpifIcmpRedirectEnable
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                setValFsMIFsIpifIcmpRedirectEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpifIcmpRedirectEnable (INT4 i4FsMIFsIpifIndex,
                                    INT4 i4SetValFsMIFsIpifIcmpRedirectEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpifIcmpRedirectEnable
        (i4FsMIFsIpifIndex, i4SetValFsMIFsIpifIcmpRedirectEnable);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpifDrtBcastFwdingEnable
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                setValFsMIFsIpifDrtBcastFwdingEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpifDrtBcastFwdingEnable (INT4 i4FsMIFsIpifIndex,
                                      INT4
                                      i4SetValFsMIFsIpifDrtBcastFwdingEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpifDrtBcastFwdingEnable
        (i4FsMIFsIpifIndex, i4SetValFsMIFsIpifDrtBcastFwdingEnable);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpifProxyArpAdminStatus
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                setValFsMIFsIpifProxyArpAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpifProxyArpAdminStatus (INT4 i4FsMIFsIpifIndex,
                                     INT4 i4SetValFsMIFsIpifProxyArpAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpifProxyArpAdminStatus
        (i4FsMIFsIpifIndex, i4SetValFsMIFsIpifProxyArpAdminStatus);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhSetFsMIFsIpifLocalProxyArpAdminStatus
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object
                setValFsMIFsIpifLocalProxyArpAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpifLocalProxyArpAdminStatus (INT4 i4FsMIFsIpifIndex,
                                          INT4
                                          i4SetValFsMIFsIpifLocalProxyArpAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpifLocalProxyArpAdminStatus
        (i4FsMIFsIpifIndex, i4SetValFsMIFsIpifLocalProxyArpAdminStatus);
    return i1Return;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpifMaxReasmSize
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                testValFsMIFsIpifMaxReasmSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpifMaxReasmSize (UINT4 *pu4ErrorCode, INT4 i4FsMIFsIpifIndex,
                                 INT4 i4TestValFsMIFsIpifMaxReasmSize)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpifMaxReasmSize (pu4ErrorCode,
                                            i4FsMIFsIpifIndex,
                                            i4TestValFsMIFsIpifMaxReasmSize);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpifIcmpRedirectEnable
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                testValFsMIFsIpifIcmpRedirectEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpifIcmpRedirectEnable (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIFsIpifIndex,
                                       INT4
                                       i4TestValFsMIFsIpifIcmpRedirectEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpifIcmpRedirectEnable
        (pu4ErrorCode, i4FsMIFsIpifIndex,
         i4TestValFsMIFsIpifIcmpRedirectEnable);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpifDrtBcastFwdingEnable
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                testValFsMIFsIpifDrtBcastFwdingEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpifDrtBcastFwdingEnable (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIFsIpifIndex,
                                         INT4
                                         i4TestValFsMIFsIpifDrtBcastFwdingEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpifDrtBcastFwdingEnable
        (pu4ErrorCode, i4FsMIFsIpifIndex,
         i4TestValFsMIFsIpifDrtBcastFwdingEnable);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpifProxyArpAdminStatus
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object 
                testValFsMIFsIpifProxyArpAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpifProxyArpAdminStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIFsIpifIndex,
                                        INT4
                                        i4TestValFsMIFsIpifProxyArpAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpifProxyArpAdminStatus
        (pu4ErrorCode, i4FsMIFsIpifIndex,
         i4TestValFsMIFsIpifProxyArpAdminStatus);
    return i1Return;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpifLocalProxyArpAdminStatus
 Input       :  The Indices
                FsMIFsIpifIndex

                The Object
                testValFsMIFsIpifLocalProxyArpAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpifLocalProxyArpAdminStatus (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIFsIpifIndex,
                                             INT4
                                             i4TestValFsMIFsIpifLocalProxyArpAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIpifLocalProxyArpAdminStatus
        (pu4ErrorCode, i4FsMIFsIpifIndex,
         i4TestValFsMIFsIpifLocalProxyArpAdminStatus);
    return i1Return;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpifTable
 Input       :  The Indices
                FsMIFsIpifIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpifTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIcmpGlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIcmpGlobalTable (INT4 i4FsMIStdIpContextId)
{
    if ((i4FsMIStdIpContextId < IP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (NULL == UtilIpGetCxt ((UINT4) i4FsMIStdIpContextId))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIcmpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIcmpGlobalTable (INT4 *pi4FsMIStdIpContextId)
{
    if (VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (NULL == UtilIpGetCxt ((UINT4) *pi4FsMIStdIpContextId))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIcmpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIcmpGlobalTable (INT4 i4FsMIStdIpContextId,
                                      INT4 *pi4NextFsMIStdIpContextId)
{
    if (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                   (UINT4 *) pi4NextFsMIStdIpContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (NULL == UtilIpGetCxt ((UINT4) *pi4NextFsMIStdIpContextId))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpSendRedirectEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpSendRedirectEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpSendRedirectEnable (INT4 i4FsMIStdIpContextId,
                                    INT4 *pi4RetValFsMIFsIcmpSendRedirectEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsIcmpSendRedirectEnable
        (pi4RetValFsMIFsIcmpSendRedirectEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpSendUnreachableEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpSendUnreachableEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpSendUnreachableEnable (INT4 i4FsMIStdIpContextId,
                                       INT4
                                       *pi4RetValFsMIFsIcmpSendUnreachableEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpSendUnreachableEnable
        (pi4RetValFsMIFsIcmpSendUnreachableEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpSendEchoReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpSendEchoReplyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpSendEchoReplyEnable (INT4 i4FsMIStdIpContextId,
                                     INT4
                                     *pi4RetValFsMIFsIcmpSendEchoReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpSendEchoReplyEnable
        (pi4RetValFsMIFsIcmpSendEchoReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpNetMaskReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpNetMaskReplyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpNetMaskReplyEnable (INT4 i4FsMIStdIpContextId,
                                    INT4 *pi4RetValFsMIFsIcmpNetMaskReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpNetMaskReplyEnable (pi4RetValFsMIFsIcmpNetMaskReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpTimeStampReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpTimeStampReplyEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpTimeStampReplyEnable (INT4 i4FsMIStdIpContextId,
                                      INT4
                                      *pi4RetValFsMIFsIcmpTimeStampReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpTimeStampReplyEnable
        (pi4RetValFsMIFsIcmpTimeStampReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpInDomainNameRequests
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpInDomainNameRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpInDomainNameRequests (INT4 i4FsMIStdIpContextId,
                                      UINT4
                                      *pu4RetValFsMIFsIcmpInDomainNameRequests)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpInDomainNameRequests
        (pu4RetValFsMIFsIcmpInDomainNameRequests);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpInDomainNameReply
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpInDomainNameReply
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpInDomainNameReply (INT4 i4FsMIStdIpContextId,
                                   UINT4 *pu4RetValFsMIFsIcmpInDomainNameReply)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpInDomainNameReply (pu4RetValFsMIFsIcmpInDomainNameReply);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpOutDomainNameRequests
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpOutDomainNameRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpOutDomainNameRequests (INT4 i4FsMIStdIpContextId,
                                       UINT4
                                       *pu4RetValFsMIFsIcmpOutDomainNameRequests)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpOutDomainNameRequests
        (pu4RetValFsMIFsIcmpOutDomainNameRequests);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpOutDomainNameReply
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpOutDomainNameReply
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpOutDomainNameReply (INT4 i4FsMIStdIpContextId,
                                    UINT4
                                    *pu4RetValFsMIFsIcmpOutDomainNameReply)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpOutDomainNameReply (pu4RetValFsMIFsIcmpOutDomainNameReply);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpDirectQueryEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpDirectQueryEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpDirectQueryEnable (INT4 i4FsMIStdIpContextId,
                                   INT4 *pi4RetValFsMIFsIcmpDirectQueryEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpDirectQueryEnable (pi4RetValFsMIFsIcmpDirectQueryEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpDomainName
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpDomainName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpDomainName (INT4 i4FsMIStdIpContextId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsMIFsIcmpDomainName)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsDomainName (pRetValFsMIFsIcmpDomainName);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpTimeToLive
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpTimeToLive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpTimeToLive (INT4 i4FsMIStdIpContextId,
                            INT4 *pi4RetValFsMIFsIcmpTimeToLive)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsTimeToLive (pi4RetValFsMIFsIcmpTimeToLive);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpInSecurityFailures
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpInSecurityFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpInSecurityFailures (INT4 i4FsMIStdIpContextId,
                                    UINT4
                                    *pu4RetValFsMIFsIcmpInSecurityFailures)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpInSecurityFailures (pu4RetValFsMIFsIcmpInSecurityFailures);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpOutSecurityFailures
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpOutSecurityFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpOutSecurityFailures (INT4 i4FsMIStdIpContextId,
                                     UINT4
                                     *pu4RetValFsMIFsIcmpOutSecurityFailures)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpOutSecurityFailures
        (pu4RetValFsMIFsIcmpOutSecurityFailures);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpSendSecurityFailuresEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpSendSecurityFailuresEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpSendSecurityFailuresEnable (INT4 i4FsMIStdIpContextId,
                                            INT4
                                            *pi4RetValFsMIFsIcmpSendSecurityFailuresEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpSendSecurityFailuresEnable
        (pi4RetValFsMIFsIcmpSendSecurityFailuresEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIcmpRecvSecurityFailuresEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsIcmpRecvSecurityFailuresEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIcmpRecvSecurityFailuresEnable (INT4 i4FsMIStdIpContextId,
                                            INT4
                                            *pi4RetValFsMIFsIcmpRecvSecurityFailuresEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsIcmpRecvSecurityFailuresEnable
        (pi4RetValFsMIFsIcmpRecvSecurityFailuresEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpSendRedirectEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpSendRedirectEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpSendRedirectEnable (INT4 i4FsMIStdIpContextId,
                                    INT4 i4SetValFsMIFsIcmpSendRedirectEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpSendRedirectEnable (i4SetValFsMIFsIcmpSendRedirectEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpSendUnreachableEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpSendUnreachableEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpSendUnreachableEnable (INT4 i4FsMIStdIpContextId,
                                       INT4
                                       i4SetValFsMIFsIcmpSendUnreachableEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpSendUnreachableEnable
        (i4SetValFsMIFsIcmpSendUnreachableEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpSendEchoReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpSendEchoReplyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpSendEchoReplyEnable (INT4 i4FsMIStdIpContextId,
                                     INT4 i4SetValFsMIFsIcmpSendEchoReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpSendEchoReplyEnable (i4SetValFsMIFsIcmpSendEchoReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpNetMaskReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpNetMaskReplyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpNetMaskReplyEnable (INT4 i4FsMIStdIpContextId,
                                    INT4 i4SetValFsMIFsIcmpNetMaskReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpNetMaskReplyEnable (i4SetValFsMIFsIcmpNetMaskReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpTimeStampReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpTimeStampReplyEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpTimeStampReplyEnable (INT4 i4FsMIStdIpContextId,
                                      INT4
                                      i4SetValFsMIFsIcmpTimeStampReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpTimeStampReplyEnable
        (i4SetValFsMIFsIcmpTimeStampReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpDirectQueryEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpDirectQueryEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpDirectQueryEnable (INT4 i4FsMIStdIpContextId,
                                   INT4 i4SetValFsMIFsIcmpDirectQueryEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpDirectQueryEnable (i4SetValFsMIFsIcmpDirectQueryEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpDomainName
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpDomainName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpDomainName (INT4 i4FsMIStdIpContextId,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsMIFsIcmpDomainName)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsDomainName (pSetValFsMIFsIcmpDomainName);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpTimeToLive
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpTimeToLive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpTimeToLive (INT4 i4FsMIStdIpContextId,
                            INT4 i4SetValFsMIFsIcmpTimeToLive)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsTimeToLive (i4SetValFsMIFsIcmpTimeToLive);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpSendSecurityFailuresEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpSendSecurityFailuresEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpSendSecurityFailuresEnable (INT4 i4FsMIStdIpContextId,
                                            INT4
                                            i4SetValFsMIFsIcmpSendSecurityFailuresEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpSendSecurityFailuresEnable
        (i4SetValFsMIFsIcmpSendSecurityFailuresEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIcmpRecvSecurityFailuresEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                setValFsMIFsIcmpRecvSecurityFailuresEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIcmpRecvSecurityFailuresEnable (INT4 i4FsMIStdIpContextId,
                                            INT4
                                            i4SetValFsMIFsIcmpRecvSecurityFailuresEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsIcmpRecvSecurityFailuresEnable
        (i4SetValFsMIFsIcmpRecvSecurityFailuresEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpSendRedirectEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpSendRedirectEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpSendRedirectEnable (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdIpContextId,
                                       INT4
                                       i4TestValFsMIFsIcmpSendRedirectEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpSendRedirectEnable (pu4ErrorCode,
                                           i4TestValFsMIFsIcmpSendRedirectEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpSendUnreachableEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpSendUnreachableEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpSendUnreachableEnable (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdIpContextId,
                                          INT4
                                          i4TestValFsMIFsIcmpSendUnreachableEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpSendUnreachableEnable (pu4ErrorCode,
                                              i4TestValFsMIFsIcmpSendUnreachableEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpSendEchoReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpSendEchoReplyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpSendEchoReplyEnable (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdIpContextId,
                                        INT4
                                        i4TestValFsMIFsIcmpSendEchoReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpSendEchoReplyEnable (pu4ErrorCode,
                                            i4TestValFsMIFsIcmpSendEchoReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpNetMaskReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpNetMaskReplyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpNetMaskReplyEnable (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdIpContextId,
                                       INT4
                                       i4TestValFsMIFsIcmpNetMaskReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpNetMaskReplyEnable (pu4ErrorCode,
                                           i4TestValFsMIFsIcmpNetMaskReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpTimeStampReplyEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpTimeStampReplyEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpTimeStampReplyEnable (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdIpContextId,
                                         INT4
                                         i4TestValFsMIFsIcmpTimeStampReplyEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpTimeStampReplyEnable (pu4ErrorCode,
                                             i4TestValFsMIFsIcmpTimeStampReplyEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpDirectQueryEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpDirectQueryEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpDirectQueryEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdIpContextId,
                                      INT4 i4TestValFsMIFsIcmpDirectQueryEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpDirectQueryEnable (pu4ErrorCode,
                                          i4TestValFsMIFsIcmpDirectQueryEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpDomainName
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpDomainName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpDomainName (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsMIFsIcmpDomainName)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsDomainName (pu4ErrorCode, pTestValFsMIFsIcmpDomainName);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpTimeToLive
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpTimeToLive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpTimeToLive (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                               INT4 i4TestValFsMIFsIcmpTimeToLive)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsTimeToLive (pu4ErrorCode, i4TestValFsMIFsIcmpTimeToLive);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpSendSecurityFailuresEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpSendSecurityFailuresEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpSendSecurityFailuresEnable (UINT4 *pu4ErrorCode,
                                               INT4 i4FsMIStdIpContextId,
                                               INT4
                                               i4TestValFsMIFsIcmpSendSecurityFailuresEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpSendSecurityFailuresEnable (pu4ErrorCode,
                                                   i4TestValFsMIFsIcmpSendSecurityFailuresEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIcmpRecvSecurityFailuresEnable
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                testValFsMIFsIcmpRecvSecurityFailuresEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIcmpRecvSecurityFailuresEnable (UINT4 *pu4ErrorCode,
                                               INT4 i4FsMIStdIpContextId,
                                               INT4
                                               i4TestValFsMIFsIcmpRecvSecurityFailuresEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsIcmpRecvSecurityFailuresEnable (pu4ErrorCode,
                                                   i4TestValFsMIFsIcmpRecvSecurityFailuresEnable);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIcmpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIcmpGlobalTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsUdpGlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsUdpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsUdpGlobalTable (INT4 i4FsMIStdIpContextId)
{
    if ((i4FsMIStdIpContextId < IP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsUdpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsUdpGlobalTable (INT4 *pi4FsMIStdIpContextId)
{
    if (VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsUdpGlobalTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsUdpGlobalTable (INT4 i4FsMIStdIpContextId,
                                     INT4 *pi4NextFsMIStdIpContextId)
{
    if (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                   (UINT4 *) pi4NextFsMIStdIpContextId)
        == VCM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsUdpInNoCksum
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsUdpInNoCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsUdpInNoCksum (INT4 i4FsMIStdIpContextId,
                          UINT4 *pu4RetValFsMIFsUdpInNoCksum)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsUdpInNoCksum (pu4RetValFsMIFsUdpInNoCksum);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsUdpInIcmpErr
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsUdpInIcmpErr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsUdpInIcmpErr (INT4 i4FsMIStdIpContextId,
                          UINT4 *pu4RetValFsMIFsUdpInIcmpErr)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsUdpInIcmpErr (pu4RetValFsMIFsUdpInIcmpErr);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsUdpInErrCksum
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsUdpInErrCksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsUdpInErrCksum (INT4 i4FsMIStdIpContextId,
                           UINT4 *pu4RetValFsMIFsUdpInErrCksum)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsUdpInErrCksum (pu4RetValFsMIFsUdpInErrCksum);
    UtilIpReleaseCxt ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsUdpInBcast
 Input       :  The Indices
                FsMIStdIpContextId

                The Object 
                retValFsMIFsUdpInBcast
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsUdpInBcast (INT4 i4FsMIStdIpContextId,
                        UINT4 *pu4RetValFsMIFsUdpInBcast)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsUdpInBcast (pu4RetValFsMIFsUdpInBcast);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIFsIpCidrAggTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIpCidrAggTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpCidrAggAddress
                FsMIFsIpCidrAggAddressMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIpCidrAggTable (INT4 i4FsMIStdIpContextId,
                                              UINT4 u4FsMIFsIpCidrAggAddress,
                                              UINT4
                                              u4FsMIFsIpCidrAggAddressMask)
{
    if ((i4FsMIStdIpContextId < IP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }
    return (nmhValidateIndexInstanceFsCidrAggTable
            (u4FsMIFsIpCidrAggAddress, u4FsMIFsIpCidrAggAddressMask));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIpCidrAggTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpCidrAggAddress
                FsMIFsIpCidrAggAddressMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIpCidrAggTable (INT4 *pi4FsMIStdIpContextId,
                                      UINT4 *pu4FsMIFsIpCidrAggAddress,
                                      UINT4 *pu4FsMIFsIpCidrAggAddressMask)
{
    UINT4               u4PrevCxt = 0;
    INT1                i1RetVal = VCM_FAILURE;

    i1RetVal =
        (INT1) VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);
    while (i1RetVal == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhGetFirstIndexFsCidrAggTable (pu4FsMIFsIpCidrAggAddress,
                                            pu4FsMIFsIpCidrAggAddressMask)
            == SNMP_SUCCESS)
        {
            UtilIpReleaseCxt ();
            return SNMP_SUCCESS;
        }
        UtilIpReleaseCxt ();
        u4PrevCxt = (UINT4) *pi4FsMIStdIpContextId;
        i1RetVal = (INT1) VcmGetNextActiveL3Context (u4PrevCxt,
                                                     (UINT4 *)
                                                     pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIpCidrAggTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIFsIpCidrAggAddress
                nextFsMIFsIpCidrAggAddress
                FsMIFsIpCidrAggAddressMask
                nextFsMIFsIpCidrAggAddressMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIpCidrAggTable (INT4 i4FsMIStdIpContextId,
                                     INT4 *pi4NextFsMIStdIpContextId,
                                     UINT4 u4FsMIFsIpCidrAggAddress,
                                     UINT4 *pu4NextFsMIFsIpCidrAggAddress,
                                     UINT4 u4FsMIFsIpCidrAggAddressMask,
                                     UINT4 *pu4NextFsMIFsIpCidrAggAddressMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i1RetVal = nmhGetNextIndexFsCidrAggTable
            (u4FsMIFsIpCidrAggAddress, pu4NextFsMIFsIpCidrAggAddress,
             u4FsMIFsIpCidrAggAddressMask, pu4NextFsMIFsIpCidrAggAddressMask);
        UtilIpReleaseCxt ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
            UtilIpReleaseCxt ();
            return SNMP_SUCCESS;
        }

        UtilIpReleaseCxt ();
        while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                          (UINT4 *) pi4NextFsMIStdIpContextId)
               == VCM_SUCCESS)
        {
            if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) ==
                SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal = nmhGetFirstIndexFsCidrAggTable
                (pu4NextFsMIFsIpCidrAggAddress,
                 pu4NextFsMIFsIpCidrAggAddressMask);
            UtilIpReleaseCxt ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return i1RetVal;
            }
            i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
        }
        return SNMP_FAILURE;
    }
    /* Invalid context is given. So get the next valid context and return the
     * cidr agg entry in that cotnext.
     */
    while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i1RetVal = nmhGetFirstIndexFsCidrAggTable
            (pu4NextFsMIFsIpCidrAggAddress, pu4NextFsMIFsIpCidrAggAddressMask);
        UtilIpReleaseCxt ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIpCidrAggStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpCidrAggAddress
                FsMIFsIpCidrAggAddressMask

                The Object 
                retValFsMIFsIpCidrAggStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpCidrAggStatus (INT4 i4FsMIStdIpContextId,
                             UINT4 u4FsMIFsIpCidrAggAddress,
                             UINT4 u4FsMIFsIpCidrAggAddressMask,
                             INT4 *pi4RetValFsMIFsIpCidrAggStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsCidrAggStatus (u4FsMIFsIpCidrAggAddress,
                               u4FsMIFsIpCidrAggAddressMask,
                               pi4RetValFsMIFsIpCidrAggStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpCidrAggStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpCidrAggAddress
                FsMIFsIpCidrAggAddressMask

                The Object 
                setValFsMIFsIpCidrAggStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIpCidrAggStatus (INT4 i4FsMIStdIpContextId,
                             UINT4 u4FsMIFsIpCidrAggAddress,
                             UINT4 u4FsMIFsIpCidrAggAddressMask,
                             INT4 i4SetValFsMIFsIpCidrAggStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsCidrAggStatus (u4FsMIFsIpCidrAggAddress,
                               u4FsMIFsIpCidrAggAddressMask,
                               i4SetValFsMIFsIpCidrAggStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpCidrAggStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpCidrAggAddress
                FsMIFsIpCidrAggAddressMask

                The Object 
                testValFsMIFsIpCidrAggStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpCidrAggStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                                UINT4 u4FsMIFsIpCidrAggAddress,
                                UINT4 u4FsMIFsIpCidrAggAddressMask,
                                INT4 i4TestValFsMIFsIpCidrAggStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsCidrAggStatus (pu4ErrorCode, u4FsMIFsIpCidrAggAddress,
                                  u4FsMIFsIpCidrAggAddressMask,
                                  i4TestValFsMIFsIpCidrAggStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpCidrAggTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsIpCidrAggAddress
                FsMIFsIpCidrAggAddressMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpCidrAggTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsCidrAdvertTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsCidrAdvertTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsCidrAdvertAddress
                FsMIFsCidrAdvertAddressMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsCidrAdvertTable (INT4 i4FsMIStdIpContextId,
                                               UINT4 u4FsMIFsCidrAdvertAddress,
                                               UINT4
                                               u4FsMIFsCidrAdvertAddressMask)
{
    if ((i4FsMIStdIpContextId < IP_DEFAULT_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= IP_SIZING_CONTEXT_COUNT))
    {
        return SNMP_FAILURE;
    }
    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_FALSE)
    {
        return SNMP_FAILURE;
    }

    return (nmhValidateIndexInstanceFsCidrAdvertTable
            (u4FsMIFsCidrAdvertAddress, u4FsMIFsCidrAdvertAddressMask));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsCidrAdvertTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsCidrAdvertAddress
                FsMIFsCidrAdvertAddressMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsCidrAdvertTable (INT4 *pi4FsMIStdIpContextId,
                                       UINT4 *pu4FsMIFsCidrAdvertAddress,
                                       UINT4 *pu4FsMIFsCidrAdvertAddressMask)
{
    UINT4               u4PrevCxt = 0;
    INT1                i1RetVal = VCM_FAILURE;

    i1RetVal =
        (INT1) VcmGetFirstActiveL3Context ((UINT4 *) pi4FsMIStdIpContextId);
    while (i1RetVal == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (nmhGetFirstIndexFsCidrAdvertTable (pu4FsMIFsCidrAdvertAddress,
                                               pu4FsMIFsCidrAdvertAddressMask)
            == SNMP_SUCCESS)
        {
            UtilIpReleaseCxt ();
            return SNMP_SUCCESS;
        }
        UtilIpReleaseCxt ();
        u4PrevCxt = (UINT4) *pi4FsMIStdIpContextId;
        i1RetVal = (INT1) VcmGetNextActiveL3Context (u4PrevCxt,
                                                     (UINT4 *)
                                                     pi4FsMIStdIpContextId);
    }
    return SNMP_FAILURE;
}

 /****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsCidrAdvertTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMIFsCidrAdvertAddress
                nextFsMIFsCidrAdvertAddress
                FsMIFsCidrAdvertAddressMask
                nextFsMIFsCidrAdvertAddressMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsCidrAdvertTable (INT4 i4FsMIStdIpContextId,
                                      INT4 *pi4NextFsMIStdIpContextId,
                                      UINT4 u4FsMIFsCidrAdvertAddress,
                                      UINT4 *pu4NextFsMIFsCidrAdvertAddress,
                                      UINT4 u4FsMIFsCidrAdvertAddressMask,
                                      UINT4 *pu4NextFsMIFsCidrAdvertAddressMask)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (VcmIsL3VcExist ((UINT4) i4FsMIStdIpContextId) == VCM_TRUE)
    {
        if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i1RetVal = nmhGetNextIndexFsCidrAdvertTable
            (u4FsMIFsCidrAdvertAddress, pu4NextFsMIFsCidrAdvertAddress,
             u4FsMIFsCidrAdvertAddressMask, pu4NextFsMIFsCidrAdvertAddressMask);
        UtilIpReleaseCxt ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
            return SNMP_SUCCESS;
        }

        while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                          (UINT4 *) pi4NextFsMIStdIpContextId)
               == VCM_SUCCESS)
        {
            if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) ==
                SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal = nmhGetFirstIndexFsCidrAdvertTable
                (pu4NextFsMIFsCidrAdvertAddress,
                 pu4NextFsMIFsCidrAdvertAddressMask);
            UtilIpReleaseCxt ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return i1RetVal;
            }
            i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
        }
        return SNMP_FAILURE;
    }
    /* Invalid context is given. So get the next valid context and return the
     * cidr agg entry in that cotnext.
     */
    while (VcmGetNextActiveL3Context ((UINT4) i4FsMIStdIpContextId,
                                      (UINT4 *) pi4NextFsMIStdIpContextId)
           == VCM_SUCCESS)
    {
        if (UtilIpSetCxt ((UINT4) *pi4NextFsMIStdIpContextId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i1RetVal = nmhGetFirstIndexFsCidrAdvertTable
            (pu4NextFsMIFsCidrAdvertAddress,
             pu4NextFsMIFsCidrAdvertAddressMask);
        UtilIpReleaseCxt ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
        i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsCidrAdvertStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsCidrAdvertAddress
                FsMIFsCidrAdvertAddressMask

                The Object 
                retValFsMIFsCidrAdvertStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsCidrAdvertStatus (INT4 i4FsMIStdIpContextId,
                              UINT4 u4FsMIFsCidrAdvertAddress,
                              UINT4 u4FsMIFsCidrAdvertAddressMask,
                              INT4 *pi4RetValFsMIFsCidrAdvertStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsCidrAdvertStatus (u4FsMIFsCidrAdvertAddress,
                                  u4FsMIFsCidrAdvertAddressMask,
                                  pi4RetValFsMIFsCidrAdvertStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsCidrAdvertStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsCidrAdvertAddress
                FsMIFsCidrAdvertAddressMask

                The Object 
                setValFsMIFsCidrAdvertStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsCidrAdvertStatus (INT4 i4FsMIStdIpContextId,
                              UINT4 u4FsMIFsCidrAdvertAddress,
                              UINT4 u4FsMIFsCidrAdvertAddressMask,
                              INT4 i4SetValFsMIFsCidrAdvertStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsCidrAdvertStatus (u4FsMIFsCidrAdvertAddress,
                                  u4FsMIFsCidrAdvertAddressMask,
                                  i4SetValFsMIFsCidrAdvertStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsCidrAdvertStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsCidrAdvertAddress
                FsMIFsCidrAdvertAddressMask

                The Object 
                testValFsMIFsCidrAdvertStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsCidrAdvertStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                                 UINT4 u4FsMIFsCidrAdvertAddress,
                                 UINT4 u4FsMIFsCidrAdvertAddressMask,
                                 INT4 i4TestValFsMIFsCidrAdvertStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilIpSetCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsCidrAdvertStatus (pu4ErrorCode, u4FsMIFsCidrAdvertAddress,
                                     u4FsMIFsCidrAdvertAddressMask,
                                     i4TestValFsMIFsCidrAdvertStatus);
    UtilIpReleaseCxt ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsCidrAdvertTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMIFsCidrAdvertAddress
                FsMIFsCidrAdvertAddressMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsCidrAdvertTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpInAdvertisements
 Input       :  The Indices

                The Object 
                retValFsMIFsIrdpInAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpInAdvertisements (UINT4 *pu4RetValFsMIFsIrdpInAdvertisements)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpInAdvertisements (pu4RetValFsMIFsIrdpInAdvertisements);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpInSolicitations
 Input       :  The Indices

                The Object 
                retValFsMIFsIrdpInSolicitations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpInSolicitations (UINT4 *pu4RetValFsMIFsIrdpInSolicitations)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsIrdpInSolicitations (pu4RetValFsMIFsIrdpInSolicitations);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpOutAdvertisements
 Input       :  The Indices

                The Object 
                retValFsMIFsIrdpOutAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpOutAdvertisements (UINT4 *pu4RetValFsMIFsIrdpOutAdvertisements)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpOutAdvertisements (pu4RetValFsMIFsIrdpOutAdvertisements);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpOutSolicitations
 Input       :  The Indices

                The Object 
                retValFsMIFsIrdpOutSolicitations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpOutSolicitations (UINT4 *pu4RetValFsMIFsIrdpOutSolicitations)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpOutSolicitations (pu4RetValFsMIFsIrdpOutSolicitations);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpSendAdvertisementsEnable
 Input       :  The Indices

                The Object 
                retValFsMIFsIrdpSendAdvertisementsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpSendAdvertisementsEnable (INT4
                                          *pi4RetValFsMIFsIrdpSendAdvertisementsEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpSendAdvertisementsEnable
        (pi4RetValFsMIFsIrdpSendAdvertisementsEnable);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIrdpSendAdvertisementsEnable
 Input       :  The Indices

                The Object 
                setValFsMIFsIrdpSendAdvertisementsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIrdpSendAdvertisementsEnable (INT4
                                          i4SetValFsMIFsIrdpSendAdvertisementsEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIrdpSendAdvertisementsEnable
        (i4SetValFsMIFsIrdpSendAdvertisementsEnable);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIrdpSendAdvertisementsEnable
 Input       :  The Indices

                The Object 
                testValFsMIFsIrdpSendAdvertisementsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIrdpSendAdvertisementsEnable (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4TestValFsMIFsIrdpSendAdvertisementsEnable)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIrdpSendAdvertisementsEnable (pu4ErrorCode,
                                                 i4TestValFsMIFsIrdpSendAdvertisementsEnable);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIrdpSendAdvertisementsEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIrdpSendAdvertisementsEnable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsIrdpIfConfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsIrdpIfConfTable
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsIrdpIfConfTable (INT4 i4FsMIFsIrdpIfConfIfNum,
                                               INT4 i4FsMIFsIrdpIfConfSubref)
{
    return (nmhValidateIndexInstanceFsIrdpIfConfTable
            (i4FsMIFsIrdpIfConfIfNum, i4FsMIFsIrdpIfConfSubref));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsIrdpIfConfTable
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsIrdpIfConfTable (INT4 *pi4FsMIFsIrdpIfConfIfNum,
                                       INT4 *pi4FsMIFsIrdpIfConfSubref)
{
    return (nmhGetFirstIndexFsIrdpIfConfTable (pi4FsMIFsIrdpIfConfIfNum,
                                               pi4FsMIFsIrdpIfConfSubref));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsIrdpIfConfTable
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                nextFsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref
                nextFsMIFsIrdpIfConfSubref
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsIrdpIfConfTable (INT4 i4FsMIFsIrdpIfConfIfNum,
                                      INT4 *pi4NextFsMIFsIrdpIfConfIfNum,
                                      INT4 i4FsMIFsIrdpIfConfSubref,
                                      INT4 *pi4NextFsMIFsIrdpIfConfSubref)
{
    return (nmhGetNextIndexFsIrdpIfConfTable (i4FsMIFsIrdpIfConfIfNum,
                                              pi4NextFsMIFsIrdpIfConfIfNum,
                                              i4FsMIFsIrdpIfConfSubref,
                                              pi4NextFsMIFsIrdpIfConfSubref));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpIfConfAdvertisementAddress
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                retValFsMIFsIrdpIfConfAdvertisementAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpIfConfAdvertisementAddress (INT4 i4FsMIFsIrdpIfConfIfNum,
                                            INT4 i4FsMIFsIrdpIfConfSubref,
                                            UINT4
                                            *pu4RetValFsMIFsIrdpIfConfAdvertisementAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpIfConfAdvertisementAddress (i4FsMIFsIrdpIfConfIfNum,
                                                i4FsMIFsIrdpIfConfSubref,
                                                pu4RetValFsMIFsIrdpIfConfAdvertisementAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpIfConfMaxAdvertisementInterval
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                retValFsMIFsIrdpIfConfMaxAdvertisementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpIfConfMaxAdvertisementInterval (INT4 i4FsMIFsIrdpIfConfIfNum,
                                                INT4 i4FsMIFsIrdpIfConfSubref,
                                                INT4
                                                *pi4RetValFsMIFsIrdpIfConfMaxAdvertisementInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpIfConfMaxAdvertisementInterval (i4FsMIFsIrdpIfConfIfNum,
                                                    i4FsMIFsIrdpIfConfSubref,
                                                    pi4RetValFsMIFsIrdpIfConfMaxAdvertisementInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpIfConfMinAdvertisementInterval
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                retValFsMIFsIrdpIfConfMinAdvertisementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpIfConfMinAdvertisementInterval (INT4 i4FsMIFsIrdpIfConfIfNum,
                                                INT4 i4FsMIFsIrdpIfConfSubref,
                                                INT4
                                                *pi4RetValFsMIFsIrdpIfConfMinAdvertisementInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpIfConfMinAdvertisementInterval (i4FsMIFsIrdpIfConfIfNum,
                                                    i4FsMIFsIrdpIfConfSubref,
                                                    pi4RetValFsMIFsIrdpIfConfMinAdvertisementInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpIfConfAdvertisementLifetime
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                retValFsMIFsIrdpIfConfAdvertisementLifetime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpIfConfAdvertisementLifetime (INT4 i4FsMIFsIrdpIfConfIfNum,
                                             INT4 i4FsMIFsIrdpIfConfSubref,
                                             INT4
                                             *pi4RetValFsMIFsIrdpIfConfAdvertisementLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpIfConfAdvertisementLifetime (i4FsMIFsIrdpIfConfIfNum,
                                                 i4FsMIFsIrdpIfConfSubref,
                                                 pi4RetValFsMIFsIrdpIfConfAdvertisementLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpIfConfPerformRouterDiscovery
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                retValFsMIFsIrdpIfConfPerformRouterDiscovery
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpIfConfPerformRouterDiscovery (INT4 i4FsMIFsIrdpIfConfIfNum,
                                              INT4 i4FsMIFsIrdpIfConfSubref,
                                              INT4
                                              *pi4RetValFsMIFsIrdpIfConfPerformRouterDiscovery)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpIfConfPerformRouterDiscovery (i4FsMIFsIrdpIfConfIfNum,
                                                  i4FsMIFsIrdpIfConfSubref,
                                                  pi4RetValFsMIFsIrdpIfConfPerformRouterDiscovery);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIrdpIfConfSolicitationAddress
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                retValFsMIFsIrdpIfConfSolicitationAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIrdpIfConfSolicitationAddress (INT4 i4FsMIFsIrdpIfConfIfNum,
                                           INT4 i4FsMIFsIrdpIfConfSubref,
                                           UINT4
                                           *pu4RetValFsMIFsIrdpIfConfSolicitationAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIrdpIfConfSolicitationAddress (i4FsMIFsIrdpIfConfIfNum,
                                               i4FsMIFsIrdpIfConfSubref,
                                               pu4RetValFsMIFsIrdpIfConfSolicitationAddress);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIrdpIfConfAdvertisementAddress
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                setValFsMIFsIrdpIfConfAdvertisementAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIrdpIfConfAdvertisementAddress (INT4 i4FsMIFsIrdpIfConfIfNum,
                                            INT4 i4FsMIFsIrdpIfConfSubref,
                                            UINT4
                                            u4SetValFsMIFsIrdpIfConfAdvertisementAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIrdpIfConfAdvertisementAddress (i4FsMIFsIrdpIfConfIfNum,
                                                i4FsMIFsIrdpIfConfSubref,
                                                u4SetValFsMIFsIrdpIfConfAdvertisementAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIrdpIfConfMaxAdvertisementInterval
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                setValFsMIFsIrdpIfConfMaxAdvertisementInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIrdpIfConfMaxAdvertisementInterval (INT4 i4FsMIFsIrdpIfConfIfNum,
                                                INT4 i4FsMIFsIrdpIfConfSubref,
                                                INT4
                                                i4SetValFsMIFsIrdpIfConfMaxAdvertisementInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIrdpIfConfMaxAdvertisementInterval (i4FsMIFsIrdpIfConfIfNum,
                                                    i4FsMIFsIrdpIfConfSubref,
                                                    i4SetValFsMIFsIrdpIfConfMaxAdvertisementInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIrdpIfConfMinAdvertisementInterval
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                setValFsMIFsIrdpIfConfMinAdvertisementInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIrdpIfConfMinAdvertisementInterval (INT4 i4FsMIFsIrdpIfConfIfNum,
                                                INT4 i4FsMIFsIrdpIfConfSubref,
                                                INT4
                                                i4SetValFsMIFsIrdpIfConfMinAdvertisementInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIrdpIfConfMinAdvertisementInterval (i4FsMIFsIrdpIfConfIfNum,
                                                    i4FsMIFsIrdpIfConfSubref,
                                                    i4SetValFsMIFsIrdpIfConfMinAdvertisementInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIrdpIfConfAdvertisementLifetime
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                setValFsMIFsIrdpIfConfAdvertisementLifetime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIrdpIfConfAdvertisementLifetime (INT4 i4FsMIFsIrdpIfConfIfNum,
                                             INT4 i4FsMIFsIrdpIfConfSubref,
                                             INT4
                                             i4SetValFsMIFsIrdpIfConfAdvertisementLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIrdpIfConfAdvertisementLifetime (i4FsMIFsIrdpIfConfIfNum,
                                                 i4FsMIFsIrdpIfConfSubref,
                                                 i4SetValFsMIFsIrdpIfConfAdvertisementLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIrdpIfConfPerformRouterDiscovery
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                setValFsMIFsIrdpIfConfPerformRouterDiscovery
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIrdpIfConfPerformRouterDiscovery (INT4 i4FsMIFsIrdpIfConfIfNum,
                                              INT4 i4FsMIFsIrdpIfConfSubref,
                                              INT4
                                              i4SetValFsMIFsIrdpIfConfPerformRouterDiscovery)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIrdpIfConfPerformRouterDiscovery (i4FsMIFsIrdpIfConfIfNum,
                                                  i4FsMIFsIrdpIfConfSubref,
                                                  i4SetValFsMIFsIrdpIfConfPerformRouterDiscovery);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsIrdpIfConfSolicitationAddress
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                setValFsMIFsIrdpIfConfSolicitationAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsIrdpIfConfSolicitationAddress (INT4 i4FsMIFsIrdpIfConfIfNum,
                                           INT4 i4FsMIFsIrdpIfConfSubref,
                                           UINT4
                                           u4SetValFsMIFsIrdpIfConfSolicitationAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsIrdpIfConfSolicitationAddress (i4FsMIFsIrdpIfConfIfNum,
                                               i4FsMIFsIrdpIfConfSubref,
                                               u4SetValFsMIFsIrdpIfConfSolicitationAddress);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIrdpIfConfAdvertisementAddress
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                testValFsMIFsIrdpIfConfAdvertisementAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIrdpIfConfAdvertisementAddress (UINT4 *pu4ErrorCode,
                                               INT4 i4FsMIFsIrdpIfConfIfNum,
                                               INT4 i4FsMIFsIrdpIfConfSubref,
                                               UINT4
                                               u4TestValFsMIFsIrdpIfConfAdvertisementAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIrdpIfConfAdvertisementAddress
        (pu4ErrorCode, i4FsMIFsIrdpIfConfIfNum, i4FsMIFsIrdpIfConfSubref,
         u4TestValFsMIFsIrdpIfConfAdvertisementAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIrdpIfConfMaxAdvertisementInterval
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                testValFsMIFsIrdpIfConfMaxAdvertisementInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIrdpIfConfMaxAdvertisementInterval (UINT4 *pu4ErrorCode,
                                                   INT4 i4FsMIFsIrdpIfConfIfNum,
                                                   INT4
                                                   i4FsMIFsIrdpIfConfSubref,
                                                   INT4
                                                   i4TestValFsMIFsIrdpIfConfMaxAdvertisementInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIrdpIfConfMaxAdvertisementInterval
        (pu4ErrorCode, i4FsMIFsIrdpIfConfIfNum, i4FsMIFsIrdpIfConfSubref,
         i4TestValFsMIFsIrdpIfConfMaxAdvertisementInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIrdpIfConfMinAdvertisementInterval
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                testValFsMIFsIrdpIfConfMinAdvertisementInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIrdpIfConfMinAdvertisementInterval (UINT4 *pu4ErrorCode,
                                                   INT4 i4FsMIFsIrdpIfConfIfNum,
                                                   INT4
                                                   i4FsMIFsIrdpIfConfSubref,
                                                   INT4
                                                   i4TestValFsMIFsIrdpIfConfMinAdvertisementInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIrdpIfConfMinAdvertisementInterval
        (pu4ErrorCode, i4FsMIFsIrdpIfConfIfNum, i4FsMIFsIrdpIfConfSubref,
         i4TestValFsMIFsIrdpIfConfMinAdvertisementInterval);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIrdpIfConfAdvertisementLifetime
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                testValFsMIFsIrdpIfConfAdvertisementLifetime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIrdpIfConfAdvertisementLifetime (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIFsIrdpIfConfIfNum,
                                                INT4 i4FsMIFsIrdpIfConfSubref,
                                                INT4
                                                i4TestValFsMIFsIrdpIfConfAdvertisementLifetime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIrdpIfConfAdvertisementLifetime
        (pu4ErrorCode, i4FsMIFsIrdpIfConfIfNum, i4FsMIFsIrdpIfConfSubref,
         i4TestValFsMIFsIrdpIfConfAdvertisementLifetime);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIrdpIfConfPerformRouterDiscovery
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                testValFsMIFsIrdpIfConfPerformRouterDiscovery
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIrdpIfConfPerformRouterDiscovery (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsMIFsIrdpIfConfIfNum,
                                                 INT4 i4FsMIFsIrdpIfConfSubref,
                                                 INT4
                                                 i4TestValFsMIFsIrdpIfConfPerformRouterDiscovery)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIrdpIfConfPerformRouterDiscovery
        (pu4ErrorCode, i4FsMIFsIrdpIfConfIfNum, i4FsMIFsIrdpIfConfSubref,
         i4TestValFsMIFsIrdpIfConfPerformRouterDiscovery);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIrdpIfConfSolicitationAddress
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref

                The Object 
                testValFsMIFsIrdpIfConfSolicitationAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIrdpIfConfSolicitationAddress (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIFsIrdpIfConfIfNum,
                                              INT4 i4FsMIFsIrdpIfConfSubref,
                                              UINT4
                                              u4TestValFsMIFsIrdpIfConfSolicitationAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhTestv2FsIrdpIfConfSolicitationAddress
        (pu4ErrorCode, i4FsMIFsIrdpIfConfIfNum, i4FsMIFsIrdpIfConfSubref,
         u4TestValFsMIFsIrdpIfConfSolicitationAddress);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIrdpIfConfTable
 Input       :  The Indices
                FsMIFsIrdpIfConfIfNum
                FsMIFsIrdpIfConfSubref
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIrdpIfConfTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsRarpClientRetransmissionTimeout
 Input       :  The Indices

                The Object 
                retValFsMIFsRarpClientRetransmissionTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsRarpClientRetransmissionTimeout (INT4
                                             *pi4RetValFsMIFsRarpClientRetransmissionTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsRarpClientRetransmissionTimeout
        (pi4RetValFsMIFsRarpClientRetransmissionTimeout);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsRarpClientMaxRetries
 Input       :  The Indices

                The Object 
                retValFsMIFsRarpClientMaxRetries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsRarpClientMaxRetries (INT4 *pi4RetValFsMIFsRarpClientMaxRetries)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsRarpClientMaxRetries (pi4RetValFsMIFsRarpClientMaxRetries);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsRarpClientPktsDiscarded
 Input       :  The Indices

                The Object 
                retValFsMIFsRarpClientPktsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsRarpClientPktsDiscarded (UINT4
                                     *pu4RetValFsMIFsRarpClientPktsDiscarded)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsRarpClientPktsDiscarded
        (pu4RetValFsMIFsRarpClientPktsDiscarded);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsRarpServerStatus
 Input       :  The Indices

                The Object 
                retValFsMIFsRarpServerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsRarpServerStatus (INT4 *pi4RetValFsMIFsRarpServerStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsRarpServerStatus (pi4RetValFsMIFsRarpServerStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsRarpServerPktsDiscarded
 Input       :  The Indices

                The Object 
                retValFsMIFsRarpServerPktsDiscarded
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsRarpServerPktsDiscarded (UINT4
                                     *pu4RetValFsMIFsRarpServerPktsDiscarded)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsRarpServerPktsDiscarded
        (pu4RetValFsMIFsRarpServerPktsDiscarded);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsRarpServerTableMaxEntries
 Input       :  The Indices

                The Object 
                retValFsMIFsRarpServerTableMaxEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsRarpServerTableMaxEntries (INT4
                                       *pi4RetValFsMIFsRarpServerTableMaxEntries)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsRarpServerTableMaxEntries
        (pi4RetValFsMIFsRarpServerTableMaxEntries);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsRarpClientRetransmissionTimeout
 Input       :  The Indices

                The Object 
                setValFsMIFsRarpClientRetransmissionTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsRarpClientRetransmissionTimeout (INT4
                                             i4SetValFsMIFsRarpClientRetransmissionTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsRarpClientRetransmissionTimeout
        (i4SetValFsMIFsRarpClientRetransmissionTimeout);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsRarpClientMaxRetries
 Input       :  The Indices

                The Object 
                setValFsMIFsRarpClientMaxRetries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsRarpClientMaxRetries (INT4 i4SetValFsMIFsRarpClientMaxRetries)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsRarpClientMaxRetries (i4SetValFsMIFsRarpClientMaxRetries);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsRarpServerStatus
 Input       :  The Indices

                The Object 
                setValFsMIFsRarpServerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsRarpServerStatus (INT4 i4SetValFsMIFsRarpServerStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsRarpServerStatus (i4SetValFsMIFsRarpServerStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsRarpServerTableMaxEntries
 Input       :  The Indices

                The Object 
                setValFsMIFsRarpServerTableMaxEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsRarpServerTableMaxEntries (INT4
                                       i4SetValFsMIFsRarpServerTableMaxEntries)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsRarpServerTableMaxEntries
        (i4SetValFsMIFsRarpServerTableMaxEntries);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsRarpClientRetransmissionTimeout
 Input       :  The Indices

                The Object 
                testValFsMIFsRarpClientRetransmissionTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsRarpClientRetransmissionTimeout (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4TestValFsMIFsRarpClientRetransmissionTimeout)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsRarpClientRetransmissionTimeout (pu4ErrorCode,
                                                    i4TestValFsMIFsRarpClientRetransmissionTimeout);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsRarpClientMaxRetries
 Input       :  The Indices

                The Object 
                testValFsMIFsRarpClientMaxRetries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsRarpClientMaxRetries (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMIFsRarpClientMaxRetries)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsRarpClientMaxRetries (pu4ErrorCode,
                                         i4TestValFsMIFsRarpClientMaxRetries);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsRarpServerStatus
 Input       :  The Indices

                The Object 
                testValFsMIFsRarpServerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsRarpServerStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMIFsRarpServerStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsRarpServerStatus (pu4ErrorCode,
                                     i4TestValFsMIFsRarpServerStatus);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsRarpServerTableMaxEntries
 Input       :  The Indices

                The Object 
                testValFsMIFsRarpServerTableMaxEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsRarpServerTableMaxEntries (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsMIFsRarpServerTableMaxEntries)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsRarpServerTableMaxEntries (pu4ErrorCode,
                                              i4TestValFsMIFsRarpServerTableMaxEntries);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsRarpClientRetransmissionTimeout
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsRarpClientRetransmissionTimeout (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIFsRarpClientMaxRetries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsRarpClientMaxRetries (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIFsRarpServerStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsRarpServerStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIFsRarpServerTableMaxEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsRarpServerTableMaxEntries (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIFsRarpServerDatabaseTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIFsRarpServerDatabaseTable
 Input       :  The Indices
                FsMIFsHardwareAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIFsRarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE *
                                                       pFsMIFsHardwareAddress)
{
    return (nmhValidateIndexInstanceFsRarpServerDatabaseTable
            (pFsMIFsHardwareAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIFsRarpServerDatabaseTable
 Input       :  The Indices
                FsMIFsHardwareAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIFsRarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE *
                                               pFsMIFsHardwareAddress)
{
    return (nmhGetFirstIndexFsRarpServerDatabaseTable (pFsMIFsHardwareAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIFsRarpServerDatabaseTable
 Input       :  The Indices
                FsMIFsHardwareAddress
                nextFsMIFsHardwareAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIFsRarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE *
                                              pFsMIFsHardwareAddress,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFsMIFsHardwareAddress)
{
    return (nmhGetNextIndexFsRarpServerDatabaseTable (pFsMIFsHardwareAddress,
                                                      pNextFsMIFsHardwareAddress));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIFsHardwareAddrLen
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                retValFsMIFsHardwareAddrLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsHardwareAddrLen (tSNMP_OCTET_STRING_TYPE * pFsMIFsHardwareAddress,
                             INT4 *pi4RetValFsMIFsHardwareAddrLen)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsHardwareAddrLen (pFsMIFsHardwareAddress,
                                 pi4RetValFsMIFsHardwareAddrLen);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsProtocolAddress
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                retValFsMIFsProtocolAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsProtocolAddress (tSNMP_OCTET_STRING_TYPE * pFsMIFsHardwareAddress,
                             UINT4 *pu4RetValFsMIFsProtocolAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsProtocolAddress (pFsMIFsHardwareAddress,
                                 pu4RetValFsMIFsProtocolAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsEntryStatus
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                retValFsMIFsEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsEntryStatus (tSNMP_OCTET_STRING_TYPE * pFsMIFsHardwareAddress,
                         INT4 *pi4RetValFsMIFsEntryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsEntryStatus (pFsMIFsHardwareAddress,
                             pi4RetValFsMIFsEntryStatus);
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsHardwareAddrLen
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                setValFsMIFsHardwareAddrLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsHardwareAddrLen (tSNMP_OCTET_STRING_TYPE * pFsMIFsHardwareAddress,
                             INT4 i4SetValFsMIFsHardwareAddrLen)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsHardwareAddrLen (pFsMIFsHardwareAddress,
                                 i4SetValFsMIFsHardwareAddrLen);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsProtocolAddress
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                setValFsMIFsProtocolAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsProtocolAddress (tSNMP_OCTET_STRING_TYPE * pFsMIFsHardwareAddress,
                             UINT4 u4SetValFsMIFsProtocolAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsProtocolAddress (pFsMIFsHardwareAddress,
                                 u4SetValFsMIFsProtocolAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIFsEntryStatus
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                setValFsMIFsEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIFsEntryStatus (tSNMP_OCTET_STRING_TYPE * pFsMIFsHardwareAddress,
                         INT4 i4SetValFsMIFsEntryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhSetFsEntryStatus (pFsMIFsHardwareAddress, i4SetValFsMIFsEntryStatus);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsHardwareAddrLen
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                testValFsMIFsHardwareAddrLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsHardwareAddrLen (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIFsHardwareAddress,
                                INT4 i4TestValFsMIFsHardwareAddrLen)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsHardwareAddrLen (pu4ErrorCode, pFsMIFsHardwareAddress,
                                    i4TestValFsMIFsHardwareAddrLen);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsProtocolAddress
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                testValFsMIFsProtocolAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsProtocolAddress (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIFsHardwareAddress,
                                UINT4 u4TestValFsMIFsProtocolAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsProtocolAddress (pu4ErrorCode, pFsMIFsHardwareAddress,
                                    u4TestValFsMIFsProtocolAddress);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIFsEntryStatus
 Input       :  The Indices
                FsMIFsHardwareAddress

                The Object 
                testValFsMIFsEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsEntryStatus (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsMIFsHardwareAddress,
                            INT4 i4TestValFsMIFsEntryStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsEntryStatus (pu4ErrorCode, pFsMIFsHardwareAddress,
                                i4TestValFsMIFsEntryStatus);
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsRarpServerDatabaseTable
 Input       :  The Indices
                FsMIFsHardwareAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsRarpServerDatabaseTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIFsIpProxyArpSubnetOption
 Input       :  The Indices

                The Object 
                retValFsMIFsIpProxyArpSubnetOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIFsIpProxyArpSubnetOption (INT4 *pi4RetValFsMIFsIpProxyArpSubnetOption)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhGetFsIpProxyArpSubnetOption (pi4RetValFsMIFsIpProxyArpSubnetOption);
    return i1Return;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIFsIpProxyArpSubnetOption
 Input       :  The Indices

                The Object 
                setValFsMIFsIpProxyArpSubnetOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsMIFsIpProxyArpSubnetOption
    (INT4 i4SetValFsMIFsIpProxyArpSubnetOption)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsIpProxyArpSubnetOption
        (i4SetValFsMIFsIpProxyArpSubnetOption);
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIFsIpProxyArpSubnetOption
 Input       :  The Indices

                The Object 
                testValFsMIFsIpProxyArpSubnetOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIFsIpProxyArpSubnetOption (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsMIFsIpProxyArpSubnetOption)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsIpProxyArpSubnetOption (pu4ErrorCode,
                                           i4TestValFsMIFsIpProxyArpSubnetOption);
    return i1Return;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIFsIpProxyArpSubnetOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIFsIpProxyArpSubnetOption (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
