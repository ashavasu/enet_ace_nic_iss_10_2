/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipexport.c,v 1.29 2014/03/11 14:13:00 siva Exp $
 *
 * Description:Contanins list of exported APIs to access Ipif Table 
 *             by other modules  
 *
 *******************************************************************/
#include "ipinc.h"

/* All these Routines use Port as Input */
/*****************************************************************************
 *                                                                           *
 * Function     : IpGetIfConfigRecord                                        *
 *                                                                           *
 * Description  : This procedure calls the IP module to provide with the     *
 *                config details of the interface specified by the Index     *
 *                                                                           *
 * Input        :  u4Port        : Index of the interface whose config       *
 *                                 details are needed.                       *
 *                 pIfConfigRecord : Record to store the config info of Iface*
 * Output       : pIfConfigRecord, filled with all the info. relating to     *
 *                the interface requested.                                   *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *                                                                           *
 *****************************************************************************/
INT4
IpGetIfConfigRecord (UINT2 u2Port, tIfConfigRecord * pIfConfigRecord)
{
    tCfaIfInfo          IfInfo;
    tIpConfigInfo       IpInfo;

    /* Take a Data Structure lock over here */
    if ((u2Port >= IPIF_MAX_LOGICAL_IFACES) || (pIfConfigRecord == NULL))
    {
        return IP_FAILURE;
    }

    IPFWD_CXT_LOCK ();

    IPFWD_DS_LOCK ();
    if (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType ==
        IP_IF_TYPE_INVALID)
    {
        IPFWD_DS_UNLOCK ();
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    if (gIpGlobalInfo.Ipif_config[u2Port].pIpCxt == NULL)
    {
        IPFWD_DS_UNLOCK ();
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    pIfConfigRecord->InterfaceId.u4IfIndex =
        gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    pIfConfigRecord->InterfaceId.u2_SubReferenceNum =
        gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u2_SubReferenceNum;
    pIfConfigRecord->InterfaceId.u1_InterfaceType =
        gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType;

    pIfConfigRecord->u1Admin = gIpGlobalInfo.Ipif_config[u2Port].u1Admin;
    pIfConfigRecord->u1Oper = gIpGlobalInfo.Ipif_config[u2Port].u1Oper;

    pIfConfigRecord->u1InterfaceType =
        gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType;
    pIfConfigRecord->u2Routing_Protocol =
        gIpGlobalInfo.Ipif_config[u2Port].u2Routing_Protocol;
    pIfConfigRecord->u4Mtu = gIpGlobalInfo.Ipif_config[u2Port].u4Mtu;
    pIfConfigRecord->u1EncapType =
        gIpGlobalInfo.Ipif_config[u2Port].u1EncapType;
    pIfConfigRecord->u1IpForwardingEnable =
        gIpGlobalInfo.Ipif_config[u2Port].u1IpForwardingEnable;
    pIfConfigRecord->u1IpDrtdBcastFwdingEnable =
        gIpGlobalInfo.Ipif_config[u2Port].u1IpDrtdBcastFwdingEnable;
    pIfConfigRecord->u4ContextId =
        gIpGlobalInfo.Ipif_config[u2Port].pIpCxt->u4ContextId;
    pIfConfigRecord->u1ProxyArpAdminStatus =
        gIpGlobalInfo.Ipif_config[u2Port].u1ProxyArpAdminStatus;
    pIfConfigRecord->u1LocalProxyArpStatus =
        gIpGlobalInfo.Ipif_config[u2Port].u1LocalProxyArpStatus;

    IPFWD_DS_UNLOCK ();

    IPFWD_CXT_UNLOCK ();

    /* Get the Primary ip address from the IP interface table */
    if (CfaIpIfGetIfInfo (pIfConfigRecord->InterfaceId.u4IfIndex,
                          &IpInfo) == CFA_SUCCESS)
    {
        pIfConfigRecord->u4Addr = IpInfo.u4Addr;
        pIfConfigRecord->u4Net_mask = IpInfo.u4NetMask;
        pIfConfigRecord->u4Bcast_addr = IpInfo.u4BcastAddr;
    }
    CfaGetIfInfo (pIfConfigRecord->InterfaceId.u4IfIndex, &IfInfo);
    pIfConfigRecord->u4IfSpeed = IfInfo.u4IfSpeed;

    return IP_SUCCESS;
}

/* All these Routine use IfIndex as Index */
/*****************************************************************************
 *                                                                           *
 * Function     : IpGetIfIdFromIfIndex                                       *
 *                                                                           *
 * Description  : Calls IP, to return the Interface Id corresponding to the  *
 *                index passed.                                              *
 *                                                                           *
 * Input        :  pIfId : Pointer to the Interface Id                       *
 *                 u4Index : Port Index                                      *
 * Output       : Interface Id corresponding to the port index passed.       *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
VOID
IpGetIfIdFromIfIndex (UINT4 u4Index, tIP_INTERFACE * pIfId)
{
    UINT2               u2Port = ZERO;
    /* Get Logical port from IfIndex */
    IpGetPortFromIfIndex (u4Index, &u2Port);
    /* Function to get Interface given logical port */
    ipif_get_iface_id (u2Port, pIfId);
}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpGetPortFromIfIndex                                |
*|                                                                     |
*| Description   : Get the port number from IfIndex.                   |  
*|                 local net.                                          |  
*|                                                                     |  
*| Input         : u2IfIndex.                                          |
*|                                                                     |  
*| Output        : u2Port of the Interface                             |  
*|                                                                     |  
*| Returns       : IP_SUCCESS / IP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
IpGetPortFromIfIndex (UINT4 u4IfIndex, UINT2 *pu2Port)
{
#ifdef CFA_WANTED
    tCfaIfInfo          IfInfo;
    tIfConfigRecord     IfConfigRecord;
    UINT2               u2TmpPort = 0;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return IP_FAILURE;
    }
    u2TmpPort = (UINT2) IfInfo.i4IpPort;

    if ((u2TmpPort < IPIF_MAX_LOGICAL_IFACES) &&
        (IpGetIfConfigRecord (u2TmpPort, &IfConfigRecord) == IP_SUCCESS))
    {
        *pu2Port = u2TmpPort;
        return IP_SUCCESS;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu2Port);
#endif
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : ipif_get_iface_id
 *
 * Input(s)           : u2Port - Port for which the Interface ID is needed
 *
 * Output(s)          : pIfId - Pointer to the Interface ID structure.
 *
 * Returns            : None.                  
 *
 * Action :
 *
 * This function takes in the index and returns iface ID.
 * Iface ID is a structure of 4-bytes and is expressed as UINT4.
 * Returns
 *
+-------------------------------------------------------------------*/
VOID
ipif_get_iface_id (UINT2 u2Port, tIP_INTERFACE * pIfId)
{
    /* Take a lock */
    IPFWD_DS_LOCK ();

    if (u2Port < IPIF_MAX_LOGICAL_IFACES)
    {

        *pIfId = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId;
    }
    else
    {
        IPIF_INVALIDATE_IF_ID (pIfId);
    }

    /* Unlock */
    IPFWD_DS_UNLOCK ();
}

/* All These Routines Use Addr as the Index */

/*****************************************************************************
 *                                                                           *
 * Function           : IpGetIfIndexFromAddr                                 *
 *                                                                           *
 * Input(s)           : IP address                                           * 
 *                                                                           *
 * Output(s)          : u4IfaceIndex                                         *
 *                                                                           *
 * Returns            : IP_SUCCESS, IP_FAILURE                               *
 *                                                                           * 
 * Action :                                                                  *
 *   Returns the IP Iface Index (index into the ipif_glbtab) of the record   *
 *   containing IP address, u4Addr.                                          *  
 *                                                                           *
 *****************************************************************************/
INT4
IpGetIfIndexFromAddr (UINT4 u4Addr, UINT4 *pu4IfaceIndex)
{
    return (IpGetIfIndexFromAddrInCxt
            (IP_DEFAULT_CONTEXT, u4Addr, pu4IfaceIndex));
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpGetIfIndexFromAddrInCxt                            *
 *                                                                           *
 * Input(s)           : u4Context Id - The context identified                *
 *                      u4Addr       - IP address                            * 
 *                                                                           *
 * Output(s)          : u4IfaceIndex                                         *
 *                                                                           *
 * Returns            : IP_SUCCESS, IP_FAILURE                               *
 *                                                                           * 
 * Action :                                                                  *
 *   Returns the IP Iface Index (index into the ipif_glbtab) of the record   *
 *   containing IP address, u4Addr in the specified context                  *  
 *                                                                           *
 *****************************************************************************/
INT4
IpGetIfIndexFromAddrInCxt (UINT4 u4ContextId, UINT4 u4Addr,
                           UINT4 *pu4IfaceIndex)
{

    /* Use the API provided for IP interface table and get the
     * CFA interface index */
    if (CfaIpIfGetIfIndexFromIpAddressInCxt (u4ContextId, u4Addr,
                                             pu4IfaceIndex) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpGetPortFromAddr
 *
 * Input(s)           : IP address
 *
 * Output(s)          : None
 *
 * Returns            : IP Port No
 *
 * Action :
 *   Returns the IP Port No from Ipif_config table containing  IP address,
 *   u4Addr.
 *
+-------------------------------------------------------------------*/
INT4
IpGetPortFromAddr (UINT4 u4Addr)
{
    return (IpGetPortFromAddrInCxt (IP_DEFAULT_CONTEXT, u4Addr));
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpGetPortFromAddrInCxt
 *
 * Input(s)           : u4ContextId - The context id
 *                      u4Addr - IP address
 *
 * Output(s)          : None
 *
 * Returns            : IP Port No
 *
 * Action :
 *   Returns the IP Port No of the interface to which the specified IP address
 *   configured and which is mapped to the specified context.
 *
+-------------------------------------------------------------------*/
INT4
IpGetPortFromAddrInCxt (UINT4 u4ContextId, UINT4 u4Addr)
{
    UINT4               u4IfaceIndex = 0;
    UINT2               u2Port = 0;

    /* Use the API provided for IP interface table and get the
     * CFA interface index */
    if (CfaIpIfGetIfIndexFromIpAddressInCxt (u4ContextId, u4Addr,
                                             &u4IfaceIndex) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }
    if (IpGetPortFromIfIndex (u4IfaceIndex, &u2Port) == IP_FAILURE)
    {
        return IP_FAILURE;
    }

    return (INT4) u2Port;
}

/*-------------------------------------------------------------------+
 * Function           : IpifIsOurAddress
 *
 * Input(s)           : u4Addr
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE/FALSE 
 *
 * Action :
 *
 * Takes an IP address and finds out if the address belongs to
 * our host. All the address nodes are scanned for this purpose.
 *
+-------------------------------------------------------------------*/
INT4
IpifIsOurAddress (UINT4 u4Addr)
{
    return (IpifIsOurAddressInCxt (IP_DEFAULT_CONTEXT, u4Addr));
}

/*-------------------------------------------------------------------+
 * Function           : IpifIsOurAddressInCxt
 *
 * Input(s)           : u4ContextId - The context id
 *                      u4Addr - Ip address
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE/FALSE 
 *
 * Action :
 *
 * Check if the specified ip address belongs to any of the interface 
 * mapped to the specified context.                             
 *
+-------------------------------------------------------------------*/
INT4
IpifIsOurAddressInCxt (UINT4 u4ContextId, UINT4 u4Addr)
{
    if (CfaIpIfIsOurAddressInCxt (u4ContextId, u4Addr) == CFA_SUCCESS)
    {
        return TRUE;
    }
    return FALSE;
}

/*-------------------------------------------------------------------+
 * Function           : IpifBroadCastLocal
 * 
 * Input(s)           : u4Addr
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE if IP_SUCCESS
 *                      FALSE otherwise.
 *
 * Action :
 *
 * Takes an IP address and finds out if it is a Local Broadcast address or not
+-------------------------------------------------------------------*/

INT4
IpifIsBroadCastLocal (UINT4 u4Addr)
{
    return (IpifIsBroadCastLocalInCxt (IP_DEFAULT_CONTEXT, u4Addr));
}

/*-------------------------------------------------------------------+
 * Function           : IpifBroadCastLocalInCxt
 * 
 * Input(s)           : u4ContextId, u4Addr
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE if IP_SUCCESS
 *                      FALSE otherwise.
 *
 * Action :
 *
 * Takes an IP address and finds out if it is a Local Broadcast address or not
 * in the specified context
+-------------------------------------------------------------------*/

INT4
IpifIsBroadCastLocalInCxt (UINT4 u4ContextId, UINT4 u4Addr)
{
    if (CfaIpIfIsLocalBroadcastInCxt (u4ContextId, u4Addr) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpIsLocalAddr                                       |
*|                                                                     |
*| Description   : Checks if the given address belongs to any of the   |  
*|                 local net in default context                        |  
*|                                                                     |  
*| Input         : u4Addr, pu2Port                                     |
*|                                                                     |  
*| Output        : u2Port of the Interface to which the addr belongs   |  
*|                                                                     |  
*| Returns       : IP_SUCCESS / IP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
IpIsLocalAddr (UINT4 u4Addr, UINT2 *pu2Port)
{
    return (IpIsLocalAddrInCxt (IP_DEFAULT_CONTEXT, u4Addr, pu2Port));
}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpIsLocalAddr                                       |
*|                                                                     |
*| Description   : Checks if the given address belongs to any of the   |  
*|                 local net in the specified context                  |  
*|                                                                     |  
*| Input         : u4ContextId, u4Addr, pu2Port                        |
*|                                                                     |  
*| Output        : u2Port of the Interface to which the addr belongs   |  
*|                                                                     |  
*| Returns       : IP_SUCCESS / IP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
IpIsLocalAddrInCxt (UINT4 u4ContextId, UINT4 u4Addr, UINT2 *pu2Port)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4Addr;
    RtQuery.u4DestinationSubnetMask = IP_DEF_NET_MASK;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4ContextId;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if ((NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID) &&
            (NetIpRtInfo.u4RtIfIndx <= IPIF_MAX_LOGICAL_IFACES))
        {
            *pu2Port = (UINT2) NetIpRtInfo.u4RtIfIndx;
            return IP_SUCCESS;
        }
    }
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_src_addr_to_use_for_dest
 *
 * Input(s)           : u4Dest, u4pSrc_addr_to_use
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 *   Gets the IP address for the interface on which the given destination
 * could be reached.
 *
+-------------------------------------------------------------------*/
INT4
ip_src_addr_to_use_for_dest (UINT4 u4Dest, UINT4 *pu4pSrc_addr_to_use)
{
    return (ip_src_addr_to_use_for_dest_InCxt (IP_DEFAULT_CONTEXT, u4Dest,
                                               pu4pSrc_addr_to_use));
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_src_addr_to_use_for_dest_InCxt
 *
 * Input(s)           : u4ContextId, u4Dest, u4pSrc_addr_to_use
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 *   Gets the IP address for the interface on which the given destination
 * could be reached in the specified context
 *
+-------------------------------------------------------------------*/
INT4
ip_src_addr_to_use_for_dest_InCxt (UINT4 u4ContextId, UINT4 u4Dest,
                                   UINT4 *pu4pSrc_addr_to_use)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4Dest;
    RtQuery.u4DestinationSubnetMask = IP_DEF_NET_MASK;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4ContextId;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpRtInfo.u4RtIfIndx <= IPIF_MAX_LOGICAL_IFACES)
        {
            if (NetIpRtInfo.u4NextHop != 0)
            {
                u4Dest = NetIpRtInfo.u4NextHop;
            }
            if (IpGetSrcAddressOnInterface ((UINT2) NetIpRtInfo.u4RtIfIndx,
                                            u4Dest,
                                            pu4pSrc_addr_to_use) == IP_SUCCESS)
            {
                return IP_SUCCESS;
            }
        }
    }
    return IP_FAILURE;
}

/* This Routine use Interface Id as Input */

/*-------------------------------------------------------------------+
 * Function           : IpifGetPortFromIfId
 *
 * Input(s)           : InterfaceId
 *
 * Output(s)          : None.
 *
 * Returns            : u2Port
 *
 * Action :
 *
 * This routine reurns the port numbwer from Interface Id
+-------------------------------------------------------------------*/
UINT4
IpifGetPortFromIfId (tIP_INTERFACE InterfaceId)
{
    UINT2               u2Port = 0;
    tIP_INTERFACE       IfId;

    /* Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2Port)
    {
        IfId = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId;
        if (IfId.u1_InterfaceType != IP_IF_TYPE_INVALID)
        {
            if ((IfId.u4IfIndex == InterfaceId.u4IfIndex) &&
                (IfId.u2_SubReferenceNum == InterfaceId.u2_SubReferenceNum))
            {
                /* UnLock */
                IPFWD_DS_UNLOCK ();

                return u2Port;
            }
        }
    }

    /* UnLock */
    IPFWD_DS_UNLOCK ();

    return IPIF_INVALID_INDEX;
}

/* These are other routines that take different input */

/*-------------------------------------------------------------------+
 * Function           : IpifGetFirstPort
 *
 * Input(s)           : InterfaceId
 *
 * Output(s)          : None.
 *
 * Returns            : u2Port
 *
 * Action :
 *
 * This routine reurns the first port number which is admin up
+-------------------------------------------------------------------*/
INT1
IpifGetFirstPort (UINT2 *pu2Port)
{
    UINT2               u2Port = 0;

    /* Lock */
    IPFWD_DS_LOCK ();

    IPIF_LOGICAL_IFACES_SCAN (u2Port)
    {
        if (gIpGlobalInfo.Ipif_config[u2Port].u1Admin == IPIF_ADMIN_ENABLE)
        {
            *pu2Port = u2Port;
            /* UnLock */
            IPFWD_DS_UNLOCK ();

            return IP_SUCCESS;
        }
    }

    /* UnLock */
    IPFWD_DS_UNLOCK ();

    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : IpifGetNextPort
 *
 * Input(s)           : InterfaceId
 *
 * Output(s)          : None.
 *
 * Returns            : u2Port
 *
 * Action :
 *
 * This routine reurns the next port number
+-------------------------------------------------------------------*/
INT1
IpifGetNextPort (UINT2 u2Port, UINT2 *pu2Port)
{
    /* Lock it */
    IPFWD_DS_LOCK ();
    *pu2Port = gIpGlobalInfo.Ipif_config[u2Port].u2Next;
    IPFWD_DS_UNLOCK ();

    if ((*pu2Port == IPIF_INVALID_INDEX) ||
        (*pu2Port == IPIF_MAX_LOGICAL_IFACES))
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : IpUpdateInterfaceForwardingStatus
 *
 * Input(s)           : Address
 *
 * Output(s)          : None.
 *
 * Returns            : u2Port
 *
 * Action :
 *
 * This routine updtaes the IP forwarding status on a given port
 * +-------------------------------------------------------------------*/

INT4
IpUpdateInterfaceForwardingStatus (UINT2 u2Port, UINT1 u1IpForwardingState)
{
    /* Lock */
    IPFWD_DS_LOCK ();

    gIpGlobalInfo.Ipif_config[u2Port].u1IpForwardingEnable =
        u1IpForwardingState;

    /* Unlock */
    IPFWD_DS_UNLOCK ();

    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : IpifGetIfaceEntryCount
 * 
 * Input(s)           : None
 * 
 * Output(s)          : None.
 *
 * Returns            : Interface Entry Count
 *
 * Action :
 *
 * This routine return the interface entry count 
 * +-------------------------------------------------------------------*/

VOID
IpifGetIfaceEntryCount (UINT4 *pu4IfaceEntryCount)
{
    *pu4IfaceEntryCount = 0;

}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpifValidateIfIndex                                 |
*|                                                                     |
*| Description   : Gets the validity of the physical                   |
*|                 interface index by checking the operstatus.         |
*|                                                                     |  
*| Input         : u2IfIndex.                                          |
*|                                                                     |  
*| Output        : None.                                               |  
*|                                                                     |  
*| Returns       : IP_SUCCESS  : if operstatus is up                   |
*|                 IP_FAILURE  : if operstatus is down                 |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
IpifValidateIfIndex (UINT4 u4IfIndex)
{
    UINT2               u2Port = 0;
    INT4                i4RetVal = 0;
    if ((i4RetVal = IpGetPortFromIfIndex (u4IfIndex, &u2Port)) == IP_SUCCESS)
    {
        if (u2Port < IPIF_MAX_LOGICAL_IFACES)
        {
            /* Lock */
            IPFWD_DS_LOCK ();

            if (gIpGlobalInfo.Ipif_config[u2Port].u1Oper == IPIF_OPER_ENABLE)
            {
                /* Unlock */
                IPFWD_DS_UNLOCK ();
                return IP_SUCCESS;
            }
        }
    }

    /* Unlock */
    IPFWD_DS_UNLOCK ();
    return IP_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpIsLocalNet                                         *
 *                                                                           *
 * Input(s)           : IP address                                           *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action : Checks whether IP belongs to Local Interface mapped to the       *
 *          default context.                                                 * 
 *****************************************************************************/
INT4
IpIsLocalNet (UINT4 u4Addr)
{
    return (IpIsLocalNetInCxt (IP_DEFAULT_CONTEXT, u4Addr));
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpIsLocalNet                                         *
 *                                                                           *
 * Input(s)           : ContextId and IP address                             *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action : Checks whether IP belongs to Local Interface mapped to the       *
 *          default context.                                                 * 
 *****************************************************************************/
INT4
IpIsLocalNetInCxt (UINT4 u4ContextId, UINT4 u4Addr)
{
    if (CfaIpIfIsLocalNetInCxt (u4ContextId, u4Addr) == CFA_SUCCESS)
    {
        return IP_SUCCESS;
    }
    return IP_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpIsLocalNetOnInterface                              *
 *                                                                           *
 * Input(s)           : IfIndex and IP address                               *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action : Checks whether IP belongs to Local network in the                *
 *          Specified Interface.                                             * 
 *****************************************************************************/
INT4
IpIsLocalNetOnInterface (UINT4 u4IfIndex, UINT4 u4Addr)
{
    if (CfaIpIfIsLocalNetOnInterface (u4IfIndex, u4Addr) == CFA_SUCCESS)
    {
        return IP_SUCCESS;
    }
    return IP_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpGetSrcAddressOnInterface                           *
 *                                                                           *
 * Input(s)           : IP address                                           *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action :                                                                  *
 *   Fetches the Source IP address over the interface based on destination   *
 *****************************************************************************/
INT4
IpGetSrcAddressOnInterface (UINT2 u2Port, UINT4 u4Dest, UINT4 *pu4SrcIp)
{
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT1               u1OperStatus = 0;

    IPFWD_DS_LOCK ();
    u4CfaIfIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    IPFWD_DS_UNLOCK ();

    CfaGetIfUnnumAssocIPIf (u4CfaIfIndex, &u4UnnumAssocIPIf);
    if (0 != u4UnnumAssocIPIf)
    {
        /* If interface is  unnuumbered then assign the Assosiated ip interface */
        CfaGetIfOperStatus (u4UnnumAssocIPIf, &u1OperStatus);
        if (u1OperStatus == CFA_IF_UP)
        {
            u4CfaIfIndex = u4UnnumAssocIPIf;
        }

    }

    if (CfaIpIfGetSrcAddressOnInterface (u4CfaIfIndex, u4Dest,
                                         pu4SrcIp) == CFA_SUCCESS)
    {
        return IP_SUCCESS;
    }
    return IP_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpIfGetNextSecondaryAddress                          *
 *                                                                           *
 * Input(s)           : IP address                                           *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action             : Get the next IP address w.r.t to the given IP        *
 *                      address.If the primary IP address is given,first     *                          secondary address will be provided                   *  
 *****************************************************************************/
INT4
IpIfGetNextSecondaryAddress (UINT2 u2Port, UINT4 u4IpAddr,
                             UINT4 *pu4NextIpAddr, UINT4 *pu4NetMask)
{
    UINT4               u4CfaIfIndex = 0;
    IPFWD_DS_LOCK ();
    u4CfaIfIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    IPFWD_DS_UNLOCK ();

    if (CfaIpIfGetNextSecondaryAddress (u4CfaIfIndex, u4IpAddr,
                                        pu4NextIpAddr,
                                        pu4NetMask) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpIfSetSecondaryAddressInfo                          *
 *                                                                           *
 * Input(s)           : u4IfIndex     - Interface Index                      *
 *                      u4IpAddr      - Secondary Address                    *
 *                      u4NetMask     - Net Mask                             *
 *                      u4Flag        - Flag to create or delete IP Address  *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action             : Creates or Deletes Secondry Address                  *  
 *****************************************************************************/
INT4
IpIfSetSecondaryAddressInfo (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT4 u4NetMask,
                             UINT4 u4Flag)
{
    UINT4               u4CfaIfIndex = 0;

    IPFWD_DS_LOCK ();
    u4CfaIfIndex = gIpGlobalInfo.Ipif_config[u4IfIndex].InterfaceId.u4IfIndex;
    IPFWD_DS_UNLOCK ();

    if (CfaIpIfSetSecondaryAddressInfoWr (u4CfaIfIndex, u4IpAddr, u4NetMask,
                                          u4Flag) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpIfGetAddressInfo                                   *
 *                                                                           *
 * Input(s)           : IP address                                           *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action             : Provides information associated with the given       * 
 *                      ip address in the default context.                   *
 *****************************************************************************/
INT4
IpIfGetAddressInfo (UINT4 u4IpAddr, UINT4 *pu4NetMask, UINT4 *pu4BcastAddr,
                    UINT4 *pu4IfIndex)
{
    return (IpIfGetAddressInfoInCxt (IP_DEFAULT_CONTEXT, u4IpAddr, pu4NetMask,
                                     pu4BcastAddr, pu4IfIndex));
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpIfGetAddressInfoInCxt                              *
 *                                                                           *
 * Input(s)           : IP address                                           *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           *
 * Action             : Provides information associated with the given       * 
 *                      ip address from the specified context                *
 *****************************************************************************/
INT4
IpIfGetAddressInfoInCxt (UINT4 u4ContextId, UINT4 u4IpAddr, UINT4 *pu4NetMask,
                         UINT4 *pu4BcastAddr, UINT4 *pu4IfIndex)
{
    if (CfaIpIfGetIpAddressInfoInCxt (u4ContextId, u4IpAddr, pu4NetMask,
                                      pu4BcastAddr, pu4IfIndex) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpGetFreePort                                        *
 *                                                                           *
 * Description        : Get the Free port number                             *
 *                                                                           *
 * Input(s)           : NONE                                                 *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : Free Port Number                                     *
 *                                                                           *
 *****************************************************************************/
UINT2
IpGetFreePort (UINT4 u4CfaIfIndex)
{
    UINT2               u2IpPort = 0;
    for (u2IpPort = 0; u2IpPort < IPIF_MAX_LOGICAL_IFACES; u2IpPort++)
    {
        if ((gIpGlobalInfo.Ipif_config[u2IpPort].InterfaceId.u1_InterfaceType ==
             IP_IF_TYPE_INVALID) &&
            (gIpGlobalInfo.Ipif_config[u2IpPort].InterfaceId.u4IfIndex
             == (UINT4) IPIF_INVALID_INDEX))
        {
            gIpGlobalInfo.Ipif_config[u2IpPort].InterfaceId.u4IfIndex =
                u4CfaIfIndex;
            return u2IpPort;
        }
    }
    return IPIF_INVALID_INDEX;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpUpdateInHdrErrInCxt                                *
 *                                                                           *
 * Description        : Increment the error count for input header error     *
 *                      in the specified context.                            *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : NONE                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
IpUpdateInHdrErrInCxt (UINT4 u4ContextId)
{
    tIpCxt             *pIpCxt = NULL;

    IPFWD_CXT_LOCK ();
    if (NULL != (pIpCxt = UtilIpGetCxt (u4ContextId)))
    {
        pIpCxt->Ip_stats.u4In_hdr_err++;
    }
    IPFWD_CXT_UNLOCK ();
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpUpdateInIfaceErrInCxt                              *
 *                                                                           *
 * Description        : Increment the error count for input interface        *
 *                      error in the specified context.                      *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : NONE                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
IpUpdateInIfaceErrInCxt (UINT4 u4ContextId)
{
    tIpCxt             *pIpCxt = NULL;

    IPFWD_CXT_LOCK ();
    if (NULL != (pIpCxt = UtilIpGetCxt (u4ContextId)))
    {
        pIpCxt->Ip_stats.u4In_iface_err++;
    }
    IPFWD_CXT_UNLOCK ();
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpUpdateOutIfaceErrInCxt                             *
 *                                                                           *
 * Description        : Increment the error count for outgoing interface     *
 *                      error in the specified context.                      *
 *                                                                           *
 * Input(s)           : u4ContextId                                          *
 *                                                                           *
 * Output(s)          : NONE.                                                *
 *                                                                           *
 * Returns            : NONE                                                 *
 *                                                                           *
 *****************************************************************************/
VOID
IpUpdateOutIfaceErrInCxt (UINT4 u4ContextId)
{
    tIpCxt             *pIpCxt = NULL;

    IPFWD_CXT_LOCK ();
    if (NULL != (pIpCxt = UtilIpGetCxt (u4ContextId)))
    {
        pIpCxt->Ip_stats.u4Out_gen_err++;
    }
    IPFWD_CXT_UNLOCK ();
}

/*********************************************************************
 * Function           : IpIfaceMapping
 *
 * Description        : This function is called for VCM when ever an interface is
 *                      mapped to a context or is unmapped from the context. 
 *
 * Input(s)           : u4ContextId, u4IfIndex, u1MapType
 *
 * Output(s)          : None
 *
 * Returns            : Nonde
 *  
*********************************************************************/
VOID
IpIfaceMapping (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1MapType)
{
    tCfaIfInfo          CfaIfInfo;
    tIfaceQMsg         *pIfaceQMsg = NULL;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    pIfaceQMsg =
        (tIfaceQMsg *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.IfacePoolId);

    if (NULL == pIfaceQMsg)
    {
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC,
                    INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC, IP_NAME,
                    "pIfaceQMsg mem alloc failed \r\n");
        return;
    }

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IfacePoolId,
                            (UINT1 *) pIfaceQMsg);
        return;
    }

    pIfaceQMsg->u4ContextId = u4ContextId;
    pIfaceQMsg->u2IfIndex = (UINT2) u4IfIndex;
    pIfaceQMsg->u2Port = (UINT2) CfaIfInfo.i4IpPort;

    if (u1MapType == VCM_IFACE_MAP)
    {
        pIfaceQMsg->u1MsgType = IP_INTERFACE_MAP;
    }
    else
    {
        pIfaceQMsg->u1MsgType = IP_INTERFACE_UNMAP;
    }

    if (OsixQueSend (gIpGlobalInfo.IpCfaIfQId, (UINT1 *) &pIfaceQMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    IP_NAME, "OsixQueSend failed \r\n");
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IfacePoolId,
                            (UINT1 *) pIfaceQMsg);
        return;
    }

    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFW_VCM_INTERFACE_EVENT);
    return;
}

/*********************************************************************
 * Function           : IpIsCxtExist
 *
 * Description        : This function checks if a context exist in IP with the   
 *                      specified context id.                                
 *
 * Input(s)           : u4ContextId                        
 *
 * Output(s)          : None
 *
 * Returns            : IP_TRUE/IP_FALSE
 *  
*********************************************************************/
INT1
IpIsCxtExist (UINT4 u4ContextId)
{
    IPFWD_CXT_LOCK ();
    if (UtilIpGetCxt (u4ContextId) == NULL)
    {
        IPFWD_CXT_UNLOCK ();
        return IP_FALSE;
    }

    IPFWD_CXT_UNLOCK ();
    return IP_TRUE;
}

/*********************************************************************
 * Function           : IpGetProxyArpSubnetOption
 *
 * Description        : This function returns the status of 
 *                                      Proxy ARP Subnet Check Option   
 *
 * Input(s)           : pi4ProxyArpSubnetOption - pointer                    
 *
 * Output(s)          : None
 *
 * Returns            : IP_TRUE
 *  
*********************************************************************/
INT1
IpGetProxyArpSubnetOption (INT4 *pi4ProxyArpSubnetOption)
{
    *pi4ProxyArpSubnetOption = gIpGlobalInfo.u1ProxyArp_SubnetOption;
    return IP_TRUE;
}
