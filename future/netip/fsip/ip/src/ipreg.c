/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipreg.c,v 1.23 2016/03/10 13:15:06 siva Exp $
 *
 * Description:This file contains the functions related to  
 *             registration/deregistration of applications  
 *             with IP.                                    
 *
 *******************************************************************/
/**** $$TRACE_MODULE_NAME     = IP_FWD     ****/
/**** $$TRACE_SUB_MODULE_NAME = MAIN       ****/

#include "ipinc.h"

#ifdef DHCPC_WANTED
#include "dhcp.h"
#endif

extern UINT2        gu2VlanId;
extern UINT1        gu1IfType;
/*
 * ****************************************************************************
 * Function Name     :  IpInitRegTable
 *
 * Description       :  This function intialises the Registration Table at
 *                      startup.
 *
 * Global Variables  :  aRegTable
 *    Affected
 *
 * Input(s)          :  None
 *
 * Output(s)         :  None
 *
 * Returns           :  None
 * *****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpInitRegTable
 * $$TRACE_PROCEDURE_LEVEL = MAIN     
 * *****************************************************************************
 */
#ifdef __STDC__
VOID
IpInitRegTable (VOID)
#else
VOID
IpInitRegTable (VOID)
#endif
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < IP_MAX_PROTOCOLS; u1Index++)
    {
        gIpGlobalInfo.aHLProtoRegFn[u1Index].pIfStChng = NULL;
        gIpGlobalInfo.aHLProtoRegFn[u1Index].pRtChng = NULL;
        gIpGlobalInfo.aHLProtoRegFn[u1Index].pProtoPktRecv = NULL;
        gIpGlobalInfo.aHLProtoRegFn[u1Index].u2RegCount = 0;
        gIpGlobalInfo.aProtoRegFn[u1Index].pAppRcv = NULL;
        gIpGlobalInfo.aProtoRegFn[u1Index].pIfStChg = NULL;
        gIpGlobalInfo.aProtoRegFn[u1Index].pRtChg = NULL;
        gIpGlobalInfo.aProtoRegFn[u1Index].u2RegCount = 0;
    }
    gIpGlobalInfo.i1RegTableUp = REGUP;
    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpRegisterHigherLayerProtocol
 *
 * Description       :  This function registers the Protocol specified by the
 *                      application in default IP context. This function is an 
 *                      obsoleted function. Is is still maintained for backward 
 *                      compatability. New modules must not use this function. 
 *                      They should use the function
 *                      NetIpv4RegisterHigherLayerProtocol
 *
 * Global Variables  :  gIpGlobalInfo.aProtoRegFn
 *    Affected
 *
 * Input(s)          :  u1ProtocolId : Protocol Identifier to be registered.
 *                      pAppRcv      : Pointer to callback function to receive
 *                                     the datagrams.
 *                      pIfStChg     : Pointer to callback function to be
 *                                     invoked in case of change in interface
 *                                     state change.
 *                      pRtChg       : Pointer to callback function to be
 *                                     invoked in case of route change.
 *
 * Output(s)         :  None.
 *
 * Returns           :  SUCCESS or FAILURE
 *
 * Note              : The check for ProtoId to call IpActOnProtoRegDeregInCxt, 
 *                     could also be added for any protocol, who require 
 *                     Interface status change Notifications. Currently, 
 *                     this is given only to RIP, since RIP only needs this.
 * ****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpRegisterHigherLayerProtocol
 * $$TRACE_PROCEDURE_LEVEL = MAIN     
 * *****************************************************************************
 */

#ifdef __STDC__
INT1
IpRegisterHigherLayerProtocol (UINT1 u1ProtocolId,
                               VOID (*pAppRcv) (tIP_BUF_CHAIN_HEADER *, UINT2,
                                                UINT2, tIP_INTERFACE, UINT1),
                               VOID (*pIfStChg) (UINT4, UINT4),
                               VOID (*pRtChg) (tRtInfo *, UINT4))
#else
INT1
IpRegisterHigherLayerProtocol (u1ProtocolId, pAppRcv, pIfStChg, pRtChg)
     UINT1               u1ProtocolId;
     VOID                (*pAppRcv) (tIP_BUF_CHAIN_HEADER *, UINT2,
                                     UINT2, tIP_INTERFACE, UINT1);
     VOID                (*pIfStChg) (UINT4, UINT4);
     VOID                (*pRtChg) (tRtInfo *, UINT4);
#endif

{
    tIpCxt             *pIpCxt = NULL;

    IPFWD_CXT_LOCK ();

    pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);
    if (NULL == pIpCxt)
    {
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    if (gIpGlobalInfo.i1RegTableUp != REGUP)
    {
        IPFWD_CXT_UNLOCK ();
        return FAILURE;
    }

    if (u1ProtocolId >= IP_MAX_PROTOCOLS)
    {
        IPFWD_CXT_UNLOCK ();
        return FAILURE;
    }

    if (pIpCxt->au1RegTable[u1ProtocolId] == REGISTER)
    {
        IPFWD_CXT_UNLOCK ();
        return FAILURE;
    }

    if (0 == gIpGlobalInfo.aProtoRegFn[u1ProtocolId].u2RegCount)
    {
        /* Protocol from no other context have registered so far. So setting
         * the function pointers in the global table 
         */
        gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pAppRcv =
            (VOID (*)(tIP_BUF_CHAIN_HEADER *, UINT2, UINT2,
                      tIP_INTERFACE, UINT1)) pAppRcv;
        gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pIfStChg =
            (VOID (*)(UINT4, UINT4)) pIfStChg;
        gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pRtChg =
            (VOID (*)(tRtInfo *, UINT4)) pRtChg;
        gIpGlobalInfo.aProtoRegFn[u1ProtocolId].u2RegCount++;
    }
    pIpCxt->au1RegTable[u1ProtocolId] = REGISTER;

    if (u1ProtocolId == RIP_ID)
    {
        IpActOnProtoRegDeregInCxt (pIpCxt->u4ContextId, u1ProtocolId);
    }
    IPFWD_CXT_UNLOCK ();
    return SUCCESS;
}

/*
 * ****************************************************************************
 * Function Name     :  IpDeRegisterHigherLayerProtocol
 *
 * Description       :  This function deregisters the Protocol specified by the
 *                      application. This function is retained for backward
 *                      compatability. Any new protocol must use NetIpv4 fnction
 *                      for protocol registration and deregistration. This 
 *                      function takes care of deregistering the protocol from 
 *                      default context.
 *
 * Global Variables  :  aRegTable
 *    Affected
 *
 * Input(s)          :  u1ProtocolId : Protocol Identifier to be deregistered.
 *
 * Output(s)         :  None.
 *
 * Returns           :  SUCCESS or FAILURE
 * *****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpDeRegisterHigherLayerProtocol 
 * $$TRACE_PROCEDURE_LEVEL = MAIN
 * *****************************************************************************
 */

#ifdef __STDC__
INT1
IpDeRegisterHigherLayerProtocol (UINT1 u1ProtocolId)
#else
INT1
IpDeRegisterHigherLayerProtocol (u1ProtocolId)
     UINT1               u1ProtocolId;
#endif

{
    tIpCxt             *pIpCxt = NULL;

    IPFWD_CXT_LOCK ();
    pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);

    if (NULL == pIpCxt)
    {
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }
    if (u1ProtocolId >= IP_MAX_PROTOCOLS)
    {
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }
    if (pIpCxt->au1RegTable[u1ProtocolId] == DEREGISTER)
    {
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    pIpCxt->au1RegTable[u1ProtocolId] = DEREGISTER;
    gIpGlobalInfo.aProtoRegFn[u1ProtocolId].u2RegCount--;

    if (0 == gIpGlobalInfo.aProtoRegFn[u1ProtocolId].u2RegCount)
    {
        /* Protocols from all contexts have deregistered. So setting the
         * function pointers to NULL.
         */
        gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pAppRcv = NULL;
        gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pIfStChg = NULL;
        gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pRtChg = NULL;
    }
    IPFWD_CXT_UNLOCK ();
    return IP_SUCCESS;
}

/*
 * ****************************************************************************
 * Function Name     :  IpIfStChgNotify
 *
 * Description       :  This function notifies an interface state change to all
 *                      the applications that have registered for the same in 
 *                      default context. This function is invoked by IPIF 
 *                      submodule. 
 *
 * Global Variables  :  None.
 *    Affected
 *
 * Input(s)          :  pIfEntry : Pointer to the interface whose status has
 *                                   changed.
 *                      u4BitMap   : Bit map indicating the fields that have
 *                                   changed.
 *
 * Output(s)         :  None.
 *
 * Returns           :  None.
 * *****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpIfStChgNotify 
 * $$TRACE_PROCEDURE_LEVEL = CUTL
 * *****************************************************************************
 */

#ifdef __STDC__
VOID
IpIfStChgNotify (UINT4 u4Port, UINT4 u4BitMap)
#else
VOID
IpIfStChgNotify (u4Port, u4BitMap)
     UINT4               u4Port;
     UINT4               u4BitMap;
#endif

{
    tIpCxt             *pIpCxt = NULL;
    UINT1               u1Protocol = 0;

    pIpCxt = UtilIpGetCxtFromIpPortNum (u4Port);
    if (NULL == pIpCxt)
    {
        return;
    }

    if (u4BitMap == 0)
    {
        return;
    }

    if (pIpCxt->u4ContextId != IP_DEFAULT_CONTEXT)
    {
        /* Interface is mapped to some other context, not to default context. 
         * So no need to send indication as this function sends indication 
         * for protocols registered in default context alone. 
         */
        return;
    }

    for (u1Protocol = 0; u1Protocol < IP_MAX_PROTOCOLS; u1Protocol++)
    {
        if ((pIpCxt->au1RegTable[u1Protocol] == REGISTER) &&
            (gIpGlobalInfo.aProtoRegFn[u1Protocol].pIfStChg != NULL))
        {
            gIpGlobalInfo.aProtoRegFn[u1Protocol].pIfStChg (u4Port, u4BitMap);
        }
    }

    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpIfSecAddrChange
 *
 * Description       :  This function notifies an interface's secondary address 
 * 						change to all the applications that have registered for 
 * 						the same in default context. This function is invoked 
 * 						by IPIF submodule. 
 *
 * Global Variables  :  None.
 *    Affected
 *
 * Input(s)          :  pIfaceQMsg : Contains the info on interface whose Sec. 
 * 						addr has changed.
 *                      u4BitMap   : Bit map indicating the fields that have
 *                                   changed.
 *
 * Output(s)         :  None.
 *
 * Returns           :  None.
 * *****************************************************************************/
VOID
IpIfSecAddrChange (tIfaceQMsg * pIfaceQMsg, UINT4 u4BitMap)
{
    tIpCxt             *pIpCxt = NULL;
	tNetIpv4IfInfo      IpIfInfo;
    UINT1               u1Protocol = 0;
    UINT2               u2Port = pIfaceQMsg->u2Port;
    UINT4               u4IfIndex = (UINT4) pIfaceQMsg->u2IfIndex;
    UINT4               u4IpAddress = pIfaceQMsg->u4Address;
    UINT4               u4SubnetMask;
    UINT4               u4BcastAddress = pIfaceQMsg->u4BcastAddr;

	u4SubnetMask = CfaGetCidrSubnetMask(pIfaceQMsg->u4Netmask);
    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        return;
    }

    if (u4BitMap == 0)
    {
        return;
    }
	
	MEMSET(&IpIfInfo, 0, sizeof(tNetIpv4IfInfo));
	IpIfInfo.u4IfIndex = u4IfIndex;
	IpIfInfo.u4Addr = u4IpAddress;
	IpIfInfo.u4NetMask = u4SubnetMask;
	IpIfInfo.u4BcastAddr = u4BcastAddress;
	IpIfInfo.u4ContextId = pIpCxt->u4ContextId;


    for (u1Protocol = 0; u1Protocol < IP_MAX_PROTOCOLS; u1Protocol++)
    {
		if ((pIpCxt->au1RegTable[u1Protocol] == REGISTER) &&
				(gIpGlobalInfo.aHLProtoRegFn[u1Protocol].pIfStChng != NULL))
		{
			gIpGlobalInfo.aHLProtoRegFn[u1Protocol].pIfStChng
				(&IpIfInfo, u4BitMap);
		}
    }

    return;
}


/*
 * ****************************************************************************
 * Function Name     :  IpRtChgNotifyInCxt
 *
 * Description       :  This function notifies a route change to all the
 *                      applications that have registered for the same. This
 *                      function is invoked by RIP. This function is maintained 
 *                      for backward compatability. This function notifies
 *                      protocols registered in default context alone.
 *
 * Global Variables  :  None.
 *    Affected
 *
 * Input(s)          :  pRtEntry : Pointer to the route which has changed.
 *                      u4BitMap : Bit map indicating the fields that have
 *                                 changed.
 *                      u4Context : Rtm Context ID
 *
 * Output(s)         :  None.
 *
 * Returns           :  None.
 * *****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpRtChgNotify
 * $$TRACE_PROCEDURE_LEVEL = CUTL
 * *****************************************************************************
 */

#ifdef __STDC__
VOID
IpRtChgNotifyInCxt (UINT4 u4ContextId, tRtInfo * pRtEntry, UINT4 u4BitMap)
#else
VOID
IpRtChgNotifyInCxt (u4ContextId, pRtEntry, u4BitMap)
     UINT4               u4ContextId;
     tRtInfo            *pRtEntry;
     UINT4               u4BitMap;
#endif

{
    tIpCxt             *pIpCxt = NULL;
    UINT1               u1Protocol = 0;

    IPFWD_CXT_LOCK ();

    if (u4ContextId != IP_DEFAULT_CONTEXT)
    {
        /* Route change indication is for some other context. So return; 
         */
        IPFWD_CXT_UNLOCK ();
        return;
    }

    pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);
    if (NULL == pIpCxt)
    {
        IPFWD_CXT_UNLOCK ();
        return;
    }

    if (u4BitMap == 0)
    {
        IPFWD_CXT_UNLOCK ();
        return;
    }

    if (pRtEntry->u4RtIfIndx > IPIF_MAX_LOGICAL_IFACES)
    {
        IPFWD_CXT_UNLOCK ();
        return;
    }

    for (u1Protocol = 0; u1Protocol < IP_MAX_PROTOCOLS; u1Protocol++)
    {
        if ((pIpCxt->au1RegTable[u1Protocol] == REGISTER) &&
            (gIpGlobalInfo.aProtoRegFn[u1Protocol].pRtChg != NULL))
        {
            if (pRtEntry->u2RtProto != u1Protocol)
            {
                gIpGlobalInfo.aProtoRegFn[u1Protocol].pRtChg
                    (pRtEntry, u4BitMap);
            }
        }
    }

    IPFWD_CXT_UNLOCK ();
    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpInitMCastRegTable
 *
 * Description       :  This function intialises the Multicast protocols 
 *                      Registration Table at startup.
 *
 * Global Variables  :  aMCastProtoRegTable
 *    Affected
 *
 * Input(s)          :  None
 *
 * Output(s)         :  None
 *
 * Returns           :  None
 * *****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpInitRegTable
 * $$TRACE_PROCEDURE_LEVEL = MAIN     
 * *****************************************************************************
 */
#ifdef __STDC__
VOID
IpInitMCastRegTable (VOID)
#else
VOID
IpInitMCastRegTable (VOID)
#endif
{
    UINT1               u1Index = 0;

    for (u1Index = 0; u1Index < IP_MAX_PROTOCOLS; u1Index++)
    {
        gIpGlobalInfo.aMCastProtoRegTable[u1Index].u1RegFlag = DEREGISTER;
        gIpGlobalInfo.aMCastProtoRegTable[u1Index].pAppRcv = NULL;
    }
    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpRegHLProtocolForMCastPkts
 *
 * Description       :  This function registers the Protocol specified by the
 *                      application.
 *
 * Global Variables  :  aMCastProtoRegTable
 *    Affected
 *
 * Input(s)          :  u1ProtocolId : Protocol Identifier to be registered.
 *                      pAppRcv      : Pointer to callback function to receive
 *                                     the multicast datagrams.
 *
 * Output(s)         :  None.
 *
 * Returns           :  if SUCCESS returns Application Id 
 *                      otherwise returns FAILURE
 * *****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpRegHLProtocolForMCastPkts
 * $$TRACE_PROCEDURE_LEVEL = MAIN     
 * *****************************************************************************
 */

#ifdef __STDC__
UINT1
IpRegHLProtocolForMCastPkts (VOID (*pAppRcv) (tIP_BUF_CHAIN_HEADER *))
#else
UINT1
IpRegHLProtocolForMCastPkts (pAppRcv)
     VOID               *pAppRcv;
#endif
{
    UINT1               u1Index = 0;

    if (pAppRcv == NULL)
    {
        return ((UINT1) FAILURE);
    }

    for (u1Index = 0; u1Index < IP_MAX_PROTOCOLS; u1Index++)
    {
        if (gIpGlobalInfo.aMCastProtoRegTable[u1Index].u1RegFlag == DEREGISTER)
        {
            break;
        }
    }
    if (u1Index == IP_MAX_PROTOCOLS)
    {
        return ((UINT1) FAILURE);
    }
    gIpGlobalInfo.aMCastProtoRegTable[u1Index].u1RegFlag = REGISTER;
    gIpGlobalInfo.aMCastProtoRegTable[u1Index].pAppRcv =
        (VOID (*)(tIP_BUF_CHAIN_HEADER *)) pAppRcv;

    return u1Index;
}

/*
 * ****************************************************************************
 * Function Name     :  IpDeRegHLProtocolForMCastPkts
 *
 * Description       :  This function deregisters the Protocol specified by the
 *                      application for receiving multicast packets.
 *
 * Global Variables  :  aMCastProtoRegTable
 *    Affected
 *
 * Input(s)          :  u1AppId : Protocol Identifier to be deregistered.
 *
 * Output(s)         :  None.
 *
 * Returns           :  SUCCESS or FAILURE
 * *****************************************************************************
 * $$TRACE_PROCEDURE_NAME  = IpDeRegHLProtocolForMCastPkts 
 * $$TRACE_PROCEDURE_LEVEL = MAIN
 * *****************************************************************************
 */

#ifdef __STDC__
INT4
IpDeRegHLProtocolForMCastPkts (UINT1 u1AppId)
#else
INT4
IpDeRegHLProtocolForMCastPkts (u1AppId)
     UINT1               u1AppId;
#endif

{

    if (u1AppId >= IP_MAX_PROTOCOLS)
    {
        return FAILURE;
    }
    if (gIpGlobalInfo.aMCastProtoRegTable[u1AppId].u1RegFlag == DEREGISTER)
    {
        return FAILURE;
    }

    gIpGlobalInfo.aMCastProtoRegTable[u1AppId].u1RegFlag = DEREGISTER;
    gIpGlobalInfo.aMCastProtoRegTable[u1AppId].pAppRcv = NULL;

    return SUCCESS;
}

/*
 *******************************************************************************
 * Function Name     :  IpActOnProtoRegDeregInCxt
 *
 * Description       :  This function Intimates the OperStatus of all the IP
 *                      level interfaces that are up and mapped to the specified
 *                      context when the protocol registers
 *                      with IP for Interface State Change Notification.
 *                      
 * Global Variables  :  gIpGlobalInfo
 *    Affected
 *
 * Input(s)          :  u4ContextId - the context id
 *                      u1ProtocolId : Protocol Identifier to be deregistered.
 *
 * Output(s)         :  None.
 *
 * Returns           :  VOID
 * *****************************************************************************
 */
VOID
IpActOnProtoRegDeregInCxt (UINT4 u4ContextId, UINT1 u1ProtocolId)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    tCfaIfInfo          IfInfo;
    tIpConfigInfo       IpInfo;
    tIpCxt             *pIpCxt = NULL;
    tIpCxt             *pIfIpCxt = NULL;
    UINT4               u4Port = 0;
    UINT4               u4CfaIfIndex = 0;

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return;
    }

    if (pIpCxt->au1RegTable[u1ProtocolId] == REGISTER)
    {
        /* PROTOCOL Registration */
        if ((gIpGlobalInfo.aHLProtoRegFn[u1ProtocolId].pIfStChng != NULL) ||
            (gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pIfStChg != NULL))
        {
            /* Take a Data Structure Lock */
            IPFWD_DS_LOCK ();

            IPIF_LOGICAL_IFACES_SCAN (u4Port)
            {
                pIfIpCxt = gIpGlobalInfo.Ipif_config[u4Port].pIpCxt;
                if ((NULL != pIfIpCxt) &&
                    (pIfIpCxt->u4ContextId == pIpCxt->u4ContextId))
                {
                    if (gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pIfStChg !=
                        NULL)
                    {
                        if (gIpGlobalInfo.Ipif_config[u4Port].u1Oper)
                        {
                            gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pIfStChg
                                (u4Port,
                                 gIpGlobalInfo.Ipif_config[u4Port].u1Oper);
                        }
                    }

                    if (gIpGlobalInfo.aHLProtoRegFn[u1ProtocolId].pIfStChng !=
                        NULL)
                    {
                        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

                        u4CfaIfIndex = gIpGlobalInfo.Ipif_config[u4Port].
                            InterfaceId.u4IfIndex;
                        NetIpIfInfo.u4IfIndex = u4Port;
                        NetIpIfInfo.u4Mtu =
                            gIpGlobalInfo.Ipif_config[u4Port].u4Mtu;

                        /* Get the Primary ip address from the IP interface table */
                        if (CfaIpIfGetIfInfo (u4CfaIfIndex, &IpInfo) ==
                            CFA_SUCCESS)
                        {
                            NetIpIfInfo.u4Addr = IpInfo.u4Addr;
                            NetIpIfInfo.u4NetMask = IpInfo.u4NetMask;
                            NetIpIfInfo.u4BcastAddr = IpInfo.u4BcastAddr;
                        }
                        CfaGetIfInfo (u4CfaIfIndex, &IfInfo);
                        NetIpIfInfo.u4IfSpeed = IfInfo.u4IfSpeed;

                        NetIpIfInfo.u4RoutingProtocol =
                            gIpGlobalInfo.Ipif_config[u4Port].
                            u2Routing_Protocol;
                        NetIpIfInfo.u4Admin =
                            gIpGlobalInfo.Ipif_config[u4Port].u1Admin;
                        NetIpIfInfo.u4Oper =
                            gIpGlobalInfo.Ipif_config[u4Port].u1Oper;
                        NetIpIfInfo.u4IfType =
                            gIpGlobalInfo.Ipif_config[u4Port].u1IfaceType;
                        NetIpIfInfo.u4ContextId = pIfIpCxt->u4ContextId;
                        NetIpIfInfo.u4CfaIfIndex = u4CfaIfIndex;
			NetIpIfInfo.u2VlanId = IfInfo.u2VlanId;

                        CfaGetInterfaceNameFromIndex (u4CfaIfIndex,
                                                      NetIpIfInfo.au1IfName);

                        gIpGlobalInfo.aHLProtoRegFn[u1ProtocolId].pIfStChng
                            (&NetIpIfInfo,
                             gIpGlobalInfo.Ipif_config[u4Port].u1Oper);
                    }
                }
            }
            /* Unlock it */
            IPFWD_DS_UNLOCK ();
        }
    }
    else
    {
        /* PROTOCOL Deregistration */
        if ((gIpGlobalInfo.aHLProtoRegFn[u1ProtocolId].pRtChng != NULL) ||
            (gIpGlobalInfo.aProtoRegFn[u1ProtocolId].pRtChg != NULL))
        {
            RtmNetIpv4HandleProtoDeRegInCxt (u4ContextId, u1ProtocolId);
        }
    }
    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpIfStateChngNotify
 *
 * Description       :  This function notifies an interface state change to all
 *                      the applications that have registered for the same.
 *                      This function is invoked by IPIF submodule.
 *
 * Global Variables  :  None.
 *    Affected
 *
 * Input(s)          :  pIfInfo    : Pointer to the interface whose status has
 *                                   changed.
 *                      u4BitMap   : Bit map indicating the fields that have
 *                                   changed.
 *                                   OPER_STATE /IFACE_DELETED/IP_ADDR_BIT_MASK 
 *                                   /IFACE_MTU_MASK/IF_SPEED_BIT_MASK 
 *
 *
 *
 * Output(s)         :  None.
 *
 * Returns           :  None.
 * *****************************************************************************
 */

VOID
IpIfStateChngNotify (UINT2 u2Port, UINT4 u4BitMap)
{
    tCfaIfInfo          IfInfo;
    tNetIpv4IfInfo      IpIfInfo;
    tIpConfigInfo       IpInfo;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4CfaIfIndex = 0;
    UINT1               u1Protocol = 0;

    if (u4BitMap == 0)
    {
        return;
    }

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        /* The interface is not mapped. So return */
        return;
    }

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    u4CfaIfIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    IpIfInfo.u4IfIndex = (UINT4) u2Port;
    IpIfInfo.u4Mtu = gIpGlobalInfo.Ipif_config[u2Port].u4Mtu;
    IpIfInfo.u4RoutingProtocol =
        gIpGlobalInfo.Ipif_config[u2Port].u2Routing_Protocol;
    IpIfInfo.u4Admin = gIpGlobalInfo.Ipif_config[u2Port].u1Admin;
    IpIfInfo.u4Oper = gIpGlobalInfo.Ipif_config[u2Port].u1Oper;
    IpIfInfo.u4IfType = gIpGlobalInfo.Ipif_config[u2Port].u1IfaceType;

    IpIfInfo.u4CfaIfIndex = u4CfaIfIndex;
    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    CfaGetIfInfo (u4CfaIfIndex, &IfInfo);
    IpIfInfo.u4IfSpeed = IfInfo.u4IfSpeed;
    IpIfInfo.u4ContextId = pIpCxt->u4ContextId;
    IpIfInfo.u1CfaIfType = gu1IfType;

    /* Get the Primary ip address from the IP interface table */
    if (CfaIpIfGetIfInfo (u4CfaIfIndex, &IpInfo) == CFA_SUCCESS)
    {
        IpIfInfo.u4Addr = IpInfo.u4Addr;
        IpIfInfo.u4NetMask = IpInfo.u4NetMask;
        IpIfInfo.u4BcastAddr = IpInfo.u4BcastAddr;
	IpIfInfo.u2VlanId = IfInfo.u2VlanId;
    }

    for (u1Protocol = 0; u1Protocol < IP_MAX_PROTOCOLS; u1Protocol++)
    {
        if ((pIpCxt->au1RegTable[u1Protocol] == REGISTER) &&
            (gIpGlobalInfo.aHLProtoRegFn[u1Protocol].pIfStChng != NULL))
        {
            gIpGlobalInfo.aHLProtoRegFn[u1Protocol].pIfStChng
                (&IpIfInfo, u4BitMap);
        }
    }

#ifdef DHCPC_WANTED
    
    if (((u4BitMap & IP_ADDR_BIT_MASK) == IP_ADDR_BIT_MASK) && 
        (IpInfo.u1AddrAllocMethod == CFA_IP_ALLOC_POOL) &&
        (IpIfInfo.u4Addr != 0))
    {
        if (IpIfInfo.u4Oper == IPIF_OPER_ENABLE)
        {
            RtmApiHandleOperUpIndication (u2Port);
        }
    }

    if (IP_DEFAULT_CONTEXT == pIpCxt->u4ContextId)
    {
        /* DHCP runs in default context. So indicate only if the interface is
         * mapped to default context. 
         */
        DhcpCHandleIfStateChg ((UINT4) u2Port, u4BitMap);
    }
#endif

#ifdef ARP_WANTED
    ArpNotifyIfStChg (u2Port, u4BitMap, IpIfInfo.u4Oper);
#endif /* ARP_WANTED */
    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpRtChngNotifyInCxt
 *
 * Description       :  This function notifies a route change to all the
 *                      applications that have registered for the same in the
 *                      specified context. 
 *
 * Global Variables  :  None.
 *    Affected
 *
 * Input(s)          :  u4ContextId : The context id
 *                      pRtEntry  : Pointer to the route which has changed.
 *                      u1CmdType : ADD | DELETE | MODIFY. 
 *
 * Output(s)         :  None.
 *
 * Returns           :  None.
 * *****************************************************************************
 */

VOID
IpRtChngNotifyInCxt (UINT4 u4ContextId, tRtInfo * pRtEntry, tRtInfo * pRtEntry1, UINT1 u1CmdType)
{
    tNetIpv4RtInfo      NetRtInfo;
    tNetIpv4RtInfo      NetRtInfo1;
    tIpCxt             *pIpCxt = NULL;
    UINT1               u1Protocol = 0;

    MEMSET (&NetRtInfo, 0, sizeof(tNetIpv4RtInfo));
    MEMSET (&NetRtInfo1, 0, sizeof(tNetIpv4RtInfo));


    IPFWD_CXT_LOCK ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (pIpCxt == NULL)
    {
        IPFWD_CXT_UNLOCK ();
        return;
    }
    if (pRtEntry->u4RtIfIndx > IPIF_MAX_LOGICAL_IFACES)
    {
        IPFWD_CXT_UNLOCK ();
        return;
    }
    NetRtInfo.u4DestNet = pRtEntry->u4DestNet;
    NetRtInfo.u4DestMask = pRtEntry->u4DestMask;
    NetRtInfo.u4Tos = pRtEntry->u4Tos;
    NetRtInfo.u4NextHop = pRtEntry->u4NextHop;
    NetRtInfo.u4RtIfIndx = pRtEntry->u4RtIfIndx;
    NetRtInfo.u4RtAge = pRtEntry->u4RtAge;
    NetRtInfo.u4RtNxtHopAs = pRtEntry->u4RtNxtHopAS;
    NetRtInfo.i4Metric1 = pRtEntry->i4Metric1;
    NetRtInfo.u4RowStatus = pRtEntry->u4RowStatus;
    NetRtInfo.u2RtType = pRtEntry->u2RtType;
    NetRtInfo.u2RtProto = pRtEntry->u2RtProto;
    NetRtInfo.u4RouteTag = pRtEntry->u4RouteTag;
    NetRtInfo.u4ContextId = u4ContextId;
    NetRtInfo.u4Flag=pRtEntry->u4Flag;

    if ((u1CmdType == NETIPV4_MODIFY_ROUTE) && (pRtEntry1 != NULL))
    {
        NetRtInfo1.u4DestNet = pRtEntry1->u4DestNet;
        NetRtInfo1.u4DestMask = pRtEntry1->u4DestMask;
        NetRtInfo1.u4Tos = pRtEntry1->u4Tos;
        NetRtInfo1.u4NextHop = pRtEntry1->u4NextHop;
        NetRtInfo1.u4RtIfIndx = pRtEntry1->u4RtIfIndx;
        NetRtInfo1.u4RtAge = pRtEntry1->u4RtAge;
        NetRtInfo1.u4RtNxtHopAs = pRtEntry1->u4RtNxtHopAS;
        NetRtInfo1.i4Metric1 = pRtEntry1->i4Metric1;
        NetRtInfo1.u4RowStatus = pRtEntry1->u4RowStatus;
        NetRtInfo1.u2RtType = pRtEntry1->u2RtType;
        NetRtInfo1.u2RtProto = pRtEntry1->u2RtProto;
        NetRtInfo1.u4RouteTag = pRtEntry1->u4RouteTag;
        NetRtInfo1.u4ContextId = u4ContextId;
    }
    for (u1Protocol = 0; u1Protocol < IP_MAX_PROTOCOLS; u1Protocol++)
    {
        if ((pIpCxt->au1RegTable[u1Protocol] == REGISTER) &&
            (gIpGlobalInfo.aHLProtoRegFn[u1Protocol].pRtChng) != NULL)
        {
            if (pRtEntry->u2RtProto != u1Protocol)
            {
                gIpGlobalInfo.aHLProtoRegFn[u1Protocol].pRtChng
                    (&NetRtInfo, &NetRtInfo1, u1CmdType);
            }
        }
    }
    IPFWD_CXT_UNLOCK ();
    return;
}

/****************************************************************************
 * Function Name     :  IpRegisterWithVcm
 *
 * Description       :  This function register with VCM a call back function 
 *                      for context and deletion indication.                     
 *
 * Input(s)          :  None                              
 *
 * Output(s)         :  None.
 *
 * Returns           :  IP_SUCCESS / IP_FAILURE .
 * *****************************************************************************/
INT4
IpRegisterWithVcm (VOID)
{
    tVcmRegInfo         VcmRegInfo;

    MEMSET (&VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.u1ProtoId = IPFWD_PROTOCOL_ID;
    VcmRegInfo.pIfMapChngAndCxtChng = IpCxtChngHdlr;

    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}
