/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipoutput.c,v 1.26 2016/04/07 10:35:24 siva Exp $
 *
 * Description:This file contains the output processing of   
 *             packets from higher layer and then delivering 
 *             to lower layer for transmission of packet.   
 *
 *******************************************************************/
#include "ipinc.h"

/*************************************************************************/
/* Function Name     :  IpOutputProcessPkt                               */
/*                                                                       */
/* Description       :  This function is called whenever a packet has to */
/*                       sent out  from higher layers.This function does */
/*                      destination address verification,the address     */
/*                      classification,taking decision to find a route fo*/
/*                      the destination or loopback the packet .This     */
/*                      function takes care of forwarding the packet in  */
/*                      the specified context.                           */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  pIpCxt - The ip context strucutre pointer        */
/*                      pBuf - The IP Buffer Pointer                     */
/*                      pIp  - The IP header Pointer                     */
/*                      u2Rt_port - The transmiting interface port Index */
/*                                                                       */
/* Output(s)         :  None                                             */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                */
/*************************************************************************/
#ifdef __STDC__
INT4
IpOutputProcessPktInCxt (tIpCxt * pIpCxt, tIP_BUF_CHAIN_HEADER * pBuf,
                         t_IP * pIp, UINT2 u2Rt_port)
#else
INT4
IpOutputProcessPktInCxt (pIpCxt, pBuf, pIp, u2Rt_port)
     tIpCxt             *pIpCxt;
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
     UINT2               u2Rt_port;
#endif
{
    INT1                i1Flag = 0;
    UINT2               u2Port = 0;
    INT4                i4HLDelvStatus = 0;
#ifdef SLI_WANTED
    INT4                i4SliEnqStatus = 0;
#endif
    UINT4               u4CxtId = 0;

    u4CxtId = pIpCxt->u4ContextId;

    if ((i1Flag =
         (INT1) IpVerifyGetAddrTypeInCxt (u4CxtId, pIp, IP_ADDR_DEST))
        == (INT1) IP_FAILURE)
    {

        IP_CXT_TRC_ARG2 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "Suppressing pkt with src = %x, dst = %x.\n",
                         pIp->u4Src, pIp->u4Dest);

        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                         "Releasing buffer %x.\n", pBuf);

        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    switch (i1Flag)
    {

        case IP_FOR_THIS_NODE:
            /* intentional fall through for loopback */
        case IP_ADDR_LOOP_BACK:
            /* 
             * When the Destination address one of the router's interfaces
             * then check whether the source address has been filled by 
             * application if not sent set to be destination address
             */
            if (pIp->u4Src == IP_ANY_ADDR)
            {
                pIp->u4Src = pIp->u4Dest;
                pIp->u2Cksum = ip_update_cksum (pIp, pIp->u4Src);
                IP_PKT_ASSIGN_CKSUM (pBuf, pIp->u2Cksum);
                IP_PKT_ASSIGN_SRC (pBuf, pIp->u4Src);
            }

            /* Find the corresponding interface index */
            if ((u2Port = (UINT2) ipif_get_handle_from_addr_InCxt
                 (u4CxtId, pIp->u4Dest)) == (UINT2) IP_FAILURE)
            {
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
#ifdef SLI_WANTED
            i4SliEnqStatus =
                SliEnqueueIpv4RawPacketInCxt (u4CxtId, pIp->u1Proto, pIp->u2Len,
                                              pIp->u4Src, pIp->u4Dest, pBuf);
#endif
            i4HLDelvStatus = IpDeliverHigherLayerProtocol (pBuf, pIp,
                                                           (UINT1) i1Flag,
                                                           u2Port);

            if (i4HLDelvStatus == IP_FAILURE)
            {
#ifdef SLI_WANTED
                if (i4SliEnqStatus == SLI_FAILURE)
#endif
                {
                    IpHandleUnKnownProtosInCxt (pIpCxt, pBuf, u2Port, i1Flag);
                }
                IP_RELEASE_BUF (pBuf, FALSE);
            }
            break;

        case IP_UCAST:
            if (IpOutputForwardInCxt (pIpCxt, pBuf, pIp) == IP_FAILURE)
            {
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            break;

        case IP_MCAST:
            if (IpOutputMcastForward (u2Rt_port, pBuf, pIp) == IP_FAILURE)
            {
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            IP4SYS_INC_OUT_MCAST_PKT (u4CxtId);
            IP4IF_INC_OUT_MCAST_PKT (u2Rt_port);

            IP4SYS_INC_OUT_MCAST_OCTETS (pIp->u2Len, u4CxtId);
            IP4IF_INC_OUT_MCAST_OCTETS (u2Rt_port, pIp->u2Len);
            break;

        case IP_BCAST:
            if (IpOutputBcastForwardInCxt (pIpCxt, u2Rt_port, pBuf, pIp)
                == IP_FAILURE)
            {
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            break;

        default:
            IP_RELEASE_BUF (pBuf, FALSE);
            return IP_FAILURE;

    }
    return IP_SUCCESS;

}

/*************************************************************************/
/* Function Name     :  IpOutputForwardInCxt                             */
/*                                                                       */
/* Description       :  The function forwards the Unicast packets.The    */
/*                      does the following steps :                       */
/*                      1. Finds a route to reach the destination in the */
/*                         the specified context                         */
/*                      2. Check if the forwarding interface to reach the*/
/*                         destination is Adminstratively and Operational*/
/*                         up.                                           */
/*                      3. Deliver the packet to lower layer for transmit*/
/*                         -ting                                         */
/*                                                                       */
/* Input(s)          :  pIpCxt - pointer to the ip context structure.    */
/*                      pBuf - The IP buffer pointer                    */
/*                      pIp  - The IP header pointer                     */
/*                                                                       */
/* Output(s)         :  None                                             */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                */
/*************************************************************************/
#ifdef __STDC__
INT4
IpOutputForwardInCxt (tIpCxt * pIpCxt, tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp)
#else
INT4
IpOutputForwardInCxt (pIpCxt, pBuf, pIp)
     tIpCxt             *pIpCxt;
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
#endif
{
    UINT4               u4Rt_gw = 0;
    UINT4               u4Address = 0;
    UINT4               u4CxtId = 0;
    UINT2               u2Rt_port = 0;
#ifdef MPLS_L3VPN_WANTED
    UINT2               u2TmpCkSum = 0;
#endif

    u4CxtId = pIpCxt->u4ContextId;

    if (IpGetRouteInCxt (u4CxtId, pIp->u4Dest, pIp->u4Src, pIp->u1Proto,
                         pIp->u1Tos, &u2Rt_port, &u4Rt_gw) == IP_FAILURE)
    {
        if (u4CxtId == IP_DEFAULT_CONTEXT)
        {
            if (IrdpGetDefaultRoute (&u2Rt_port, &u4Rt_gw) == IP_FAILURE)
            {
                pIpCxt->Ip_stats.u4Out_no_routes++;

                if ((ipif_is_our_address_InCxt (u4CxtId, pIp->u4Src) == TRUE) ||
                    (pIp->u4Src == IP_ANY_ADDR))
                {
                    IP4SYS_INC_OUT_NO_ROUTES (u4CxtId);
                    IP4IF_INC_OUT_NO_ROUTES (u2Rt_port);
                }
                else
                {
                    IP4SYS_INC_IN_NO_ROUTES (u4CxtId);
                    IP4IF_INC_IN_NO_ROUTES (u2Rt_port);
                }

                return IP_FAILURE;
            }
        }

    }

    if (pIp->u4Src == IP_ANY_ADDR)
    {
#ifdef MPLS_L3VPN_WANTED
        if (u4CxtId != 0)
        {
            CfaIpIfGetHighestIpAddrInCxt (u4CxtId, &u4Address);
            pIp->u4Src = u4Address;
            u2TmpCkSum = pIp->u2Cksum;
            pIp->u2Cksum = ip_update_cksum (pIp, pIp->u4Src);
            IP_PKT_ASSIGN_CKSUM (pBuf, pIp->u2Cksum);
            IP_PKT_ASSIGN_SRC (pBuf, pIp->u4Src);
           
            if (MplsProcessL3VpnPacket (pBuf, u2Rt_port, pIp->u4Dest, u4CxtId) ==
                MPLS_SUCCESS)
            {
                return IP_SUCCESS;
            }
            pIp->u2Cksum = u2TmpCkSum;
        }
#endif
            if (IpGetSrcAddressOnInterface (u2Rt_port,
                                            u4Rt_gw, &u4Address) == IP_FAILURE)
            {
                IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, ALL_FAILURE_TRC, IP_NAME,
                                 "Getting source IP for destination failed %x\n",
                                 pIp->u4Dest);
            }
            pIp->u4Src = u4Address;
            pIp->u2Cksum = ip_update_cksum (pIp, pIp->u4Src);
            IP_PKT_ASSIGN_CKSUM (pBuf, pIp->u2Cksum);
            IP_PKT_ASSIGN_SRC (pBuf, pIp->u4Src);
    }
#ifdef MPLS_L3VPN_WANTED
    else if (u4CxtId != 0)
    {
        if (MplsProcessL3VpnPacket (pBuf, u2Rt_port, pIp->u4Dest, u4CxtId) ==
            MPLS_SUCCESS)
        {
            return IP_SUCCESS;
        }
    }
#endif


    if (ipif_is_ok_to_forward (u2Rt_port) == FALSE)
    {
        /* Forwarding is disabled or this interface is not up */

        t_ICMP              Icmp;
        IPIF_INC_OUT_GEN_ERR (u2Rt_port);

        pIpCxt->Ip_stats.u4Out_gen_err++;
        /*
         * ICMP error message is not sent for IP_MCAST and IP_BCAST
         */
        Icmp.i1Type = ICMP_DEST_UNREACH;
        Icmp.i1Code = ICMP_NET_UNREACH;
        Icmp.args.u4Unused = 0;
        Icmp.u4ContextId = u4CxtId;
        icmp_error_msg (pBuf, &Icmp);
        return IP_FAILURE;
    }

    if (IpOutput (u2Rt_port, u4Rt_gw, pIp, pBuf, IP_UCAST) == IP_FAILURE)
    {
        return IP_FAILURE;
    }

    IP4IF_INC_OUT_REQUESTS (u2Rt_port);
    return IP_SUCCESS;
}

/*****************************************************************************/
/* Function Name     :  IpOutputMcastForward                                 */
/*                                                                           */
/* Description       :  This function does the multicast forwarding for      */
/*                      the  packets from higher layers in default context.  */
/*                      The following steps are taken for forwarding:        */
/*                      1. Check if the transmitting interface is provided   */
/*                         by the application,if so the forward through that */
/*                         interface.                                        */
/*                      2. If the tramsmitting interface is not provided     */
/*                         by the application then forwarding decision is    */
/*                         taken by looking up the IGMP group management     */
/*                         table.                                            */
/*                      3. Check whether the forwarding interface is up and  */
/*                         send the packet to lower layer for packet         */
/*                         transmission.                                     */
/*                                                                           */
/* Global Variables  :  None                                                 */
/*                                                                           */
/* Input(s)          :  u2Rt_port - The transmitting port Index              */
/*                      pBuf -The IP Buffer pointer                         */
/*                      pIp  - The IP header pointer                         */
/*                                                                           */
/* Output(s)         : None                                                  */
/*                                                                           */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                    */
/*****************************************************************************/
#ifdef __STDC__
INT4
IpOutputMcastForward (UINT2 u2Rt_port, tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp)
#else
INT4
IpOutputMcastForward (u2Rt_port, pBuf, pIp)
     UINT2               u2Rt_port;
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4Address = 0;
    UINT4               u4CxtId = 0;

    pIpCxt = UtilIpGetCxtFromIpPortNum (u2Rt_port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    if (pIp->u4Src == IP_ANY_ADDR)
    {
        if (IpGetSrcAddressOnInterface (u2Rt_port,
                                        pIp->u4Dest, &u4Address) == IP_FAILURE)
        {
            IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, ALL_FAILURE_TRC, IP_NAME,
                             "Getting source IP for destination failed %x\n",
                             pIp->u4Dest);
        }
        pIp->u4Src = u4Address;
        pIp->u2Cksum = ip_update_cksum (pIp, pIp->u4Src);
        IP_PKT_ASSIGN_CKSUM (pBuf, pIp->u2Cksum);
        IP_PKT_ASSIGN_SRC (pBuf, pIp->u4Src);
    }

    if (ipif_is_ok_to_forward (u2Rt_port) == FALSE)
    {
        /* Forwarding is disabled or this interface is not up */
        IPIF_INC_OUT_GEN_ERR (u2Rt_port);
        pIpCxt->Ip_stats.u4Out_gen_err++;
        return IP_FAILURE;
    }

    if (IpOutput (u2Rt_port, pIp->u4Dest, pIp, pBuf, IP_MCAST) == IP_FAILURE)
    {
        return IP_FAILURE;
    }

    IP4IF_INC_OUT_REQUESTS (u2Rt_port);
    return IP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpOutputBcastForwardInCxt                        */
/*                                                                       */
/* Description       :  This function does the broadcast forwarding for  */
/*                      the packets from higher layers in the specified  */
/*                      context. It takes the following steps for        */
/*                      forwarding                                       */
/*                                                                       */
/*                      1. If the destination is limited broadcast then  */
/*                         packet is delivered to the trasmitting        */
/*                         interface given by application and goto step 3*/
/*                         If no tranmitting interface is provided then  */
/*                         the packet is discarded.                      */
/*                      2. Check for matching broadcast address of the   */
/*                         destination broadcast address and interface   */
/*                         broadcast address.If any matches then get the */
/*                         interface index and goto step 3.              */
/*                         interface and send the packet to thelowerlayer*/
/*                         for packet transmission                       */
/*                                                                       */
/* Global Variables  :   None                                            */
/*                                                                       */
/* Inputs            :     pIpCxt    - pointer to ip context structure   */
/*                         u2Rt_port - The tramitting interface          */
/*                         pBuf      - The IP Buffer pointer            */
/*                         pIp       - The IP header pointer             */
/*                                                                       */
/* Outputs           :  None                                             */
/*                                                                       */
/* Result            :  IP_SUCCESS | IP_FAILURE                                */
/*************************************************************************/
#ifdef __STDC__
INT4
IpOutputBcastForwardInCxt (tIpCxt * pIpCxt, UINT2 u2Rt_port,
                           tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp)
#else
INT4
IpOutputBcastForwardInCxt (pIpCxt, u2Rt_port, pBuf, pIp)
     tIpCxt             *pIpCxt;
     UINT2               u2Rt_port;
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
#endif
{
    tIP_SLL             BcastIfList;
    t_IF_LIST_NODE     *pBcastIfNode = NULL;
    tIP_BUF_CHAIN_HEADER *pOutBuf = NULL;
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4CxtId = pIpCxt->u4ContextId;
    UINT4               u4IfCxtId = 0;
    UINT2               u2IfNum = 0;
    UINT2               u2Index = 0;
    UINT2               u2FwdIfaces = 0;
    UINT2               u2TotalTxCount = 0;

    IP_SLL_Init (&BcastIfList);

    if (pIp->u4Dest != IP_GEN_BCAST_ADDR)
    {
        if (u2Rt_port >= IPIF_MAX_LOGICAL_IFACES)
        {
            /* Take a Data Structure Lock */
            IPFWD_DS_LOCK ();

            IPIF_LOGICAL_IFACES_SCAN (u2Index)
            {

                if (gIpGlobalInfo.Ipif_config[u2Index].InterfaceId.
                    u1_InterfaceType == IP_IF_TYPE_INVALID)
                {
                    continue;
                }
                u4IfCxtId =
                    gIpGlobalInfo.Ipif_config[u2Index].pIpCxt->u4ContextId;
                if (u4CxtId != u4IfCxtId)
                {
                    /* Interface is not mapped to this context. So don't forward 
                     * on this interface. 
                     */
                    continue;
                }
                u4CfaIfIndex =
                    gIpGlobalInfo.Ipif_config[u2Index].InterfaceId.u4IfIndex;
                if (CfaIpIfCheckInterfaceBcastAddr (u4CfaIfIndex,
                                                    pIp->u4Dest) == CFA_SUCCESS)
                {
                    if ((pBcastIfNode = (t_IF_LIST_NODE *) IP_ALLOCATE_MEM_BLOCK
                         (gIpGlobalInfo.Ip_mems.IpIfPoolId)) == NULL)
                    {
                        /* Unlock it */
                        IPFWD_DS_UNLOCK ();
                        return IP_FAILURE;
                    }
                    pBcastIfNode->u2Port = u2Index;
                    IP_SLL_Add (&BcastIfList, &(pBcastIfNode->Link));
                }
            }
            /* Unlock it */
            IPFWD_DS_UNLOCK ();

            u2FwdIfaces = (UINT2) IP_SLL_Count (&BcastIfList);
        }
        else
        {
            u2FwdIfaces = 1;
        }
    }
    else
    {
        if (u2Rt_port >= IPIF_MAX_LOGICAL_IFACES)
        {
            return IP_FAILURE;
        }
        u2FwdIfaces = 1;
    }

    while (u2FwdIfaces-- > 0)
    {
        if (u2Rt_port >= IPIF_MAX_LOGICAL_IFACES)
        {
            pBcastIfNode = (t_IF_LIST_NODE *) IP_SLL_Get (&BcastIfList);
            if (pBcastIfNode == NULL)
            {
                continue;
            }
            u2IfNum = pBcastIfNode->u2Port;
            IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IpIfPoolId,
                                  pBcastIfNode);
        }
        else
        {
            /* Broadcast directed towards only one interface */
            u2IfNum = u2Rt_port;
        }

        if (pIp->u4Src == IP_ANY_ADDR)
        {
            /* Fill the source ip address of the packet */
            if (IpGetSrcAddressOnInterface (u2IfNum,
                                            pIp->u4Dest,
                                            &(pIp->u4Src)) == IP_FAILURE)
            {
                IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, ALL_FAILURE_TRC, IP_NAME,
                                 "Getting source IP for destination failed %x\n",
                                 pIp->u4Dest);
            }
            pIp->u2Cksum = ip_update_cksum (pIp, pIp->u4Src);
            IP_PKT_ASSIGN_CKSUM (pBuf, pIp->u2Cksum);
            IP_PKT_ASSIGN_SRC (pBuf, pIp->u4Src);
        }

        if (ipif_is_ok_to_forward (u2IfNum) == FALSE)
        {
            /* Forwarding is disabled or this interface is not up */
            IPIF_INC_OUT_GEN_ERR (u2IfNum);
            pIpCxt->Ip_stats.u4Out_gen_err++;
            return IP_FAILURE;
        }

        pOutBuf = u2FwdIfaces ? IP_DUPLICATE_BUF (pBuf) : pBuf;

        if (pOutBuf == NULL)
        {
            return IP_FAILURE;
        }

        if (IpOutput (u2IfNum, pIp->u4Dest, pIp, pOutBuf, IP_BCAST) ==
            IP_FAILURE)
        {
            return IP_FAILURE;
        }

        IP4SYS_INC_OUT_BCAST_PKT (u4CxtId);
        IP4IF_INC_OUT_BCAST_PKT (u2IfNum);

        u2TotalTxCount++;

    }

    IP4IF_INC_OUT_REQUESTS (u2Rt_port);
    if (u2TotalTxCount > 1)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
    }

    return IP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpOutput                                         */
/*                                                                       */
/* Description       :  This function checks in the filter list to verify*/
/*                      any filtering has to be done this address , check*/
/*                      whether the datagram size is less than MTU of the*/
/*                      forwarding interface,if so then call the routine */
/*                      to fragment and send the datagram                */
/*                                                                       */
/* Global Variables  :  Ip_stats - The global variable for IP statistics.*/
/*                                                                       */
/* Input(s)          :  u2Rt_port - The logical port index for the       */
/*                                  forwarding interface                 */
/*                      pIp       - The IP header pointer                */
/*                      pBuf      - The IP Buffer pointer               */
/*                      i1Flag    - The flag denoting the mode of        */
/*                                  transmission (Unicast,Multicast o    */
/*                                  broadcast)                           */
/*                                                                       */
/* Output(s)         :  None                                             */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                */
/*************************************************************************/
#ifdef __STDC__
INT4
IpOutput (UINT2 u2Rt_port, UINT4 u4Rt_gw, t_IP * pIp,
          tIP_BUF_CHAIN_HEADER * pBuf, INT1 i1Flag)
#else
INT4
IpOutput (u2Rt_port, u4Rt_gw, pIp, pBuf, i1Flag)
     UINT2               u2Rt_port;
     UINT4               u4Rt_gw;
     t_IP               *pIp;
     tIP_BUF_CHAIN_HEADER *pBuf;
     INT1                i1Flag;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    tPmtuInfo          *pPmtuEntry = NULL;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4BcastAddr = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4MTU = 0;
    UINT4               u4CxtId = 0;
    UINT1               u1InterfaceType = 0;

    if (u2Rt_port >= IPIF_MAX_LOGICAL_IFACES)
    {
        return IP_FAILURE;
    }

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Rt_port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    /* Identify the IP address to be used w.r.t to the destination of 
     * the given packet */
    if (ip_src_addr_to_use_for_dest_InCxt (u4CxtId, pIp->u4Dest, &u4IpAddress)
        == IP_FAILURE)
    {
        IP_CXT_TRC (u4CxtId, IP_MOD_TRC, MGMT_TRC,
                    IP_NAME, "Unable to find matching Address\n");
    }
    /* Get the netmask associated with the IP address */
    if (IpIfGetAddressInfoInCxt (u4CxtId, u4IpAddress, &u4NetMask, &u4BcastAddr,
                                 &u4IfaceIndex) == IP_FAILURE)
    {
        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                         "IpIfGetAddressInfo failed for %x\n", u4IpAddress);
    }
    if (u4NetMask != IP_ADDR_31BIT_MASK)
    {
        /* Check added not to allow IP packets to the 
         * interface network address except 32 bit addresses */
        if ((pIp->u4Dest == (u4IpAddress & u4NetMask)) &&
            (pIp->u4Dest != u4IpAddress))
        {
            return IP_FAILURE;
        }
    }

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    u1InterfaceType = gIpGlobalInfo.Ipif_config[u2Rt_port].u1IfaceType;
    u4MTU = gIpGlobalInfo.Ipif_config[u2Rt_port].u4Mtu;
    u4IfaceIndex = gIpGlobalInfo.Ipif_config[u2Rt_port].InterfaceId.u4IfIndex;
    IPFWD_DS_UNLOCK ();

    if ((u2Rt_port != IPIF_INVALID_INDEX) && (u1InterfaceType == UNNUMBERED))
    {
        /* Get the IpAddres from w.r.t to the destination IP Address */
        pIp->u4Src = u4IpAddress;
    }

    if (PMTU_ENABLED == pIpCxt->u2PmtuStatusFlag)
    {
        pPmtuEntry = PmtuLookupEntryInCxt (pIpCxt, pIp->u4Dest, pIp->u1Tos);
        if ((pPmtuEntry != NULL)
            && (pPmtuEntry->u1PmtuDiscFlag & IPPMTUDISC_DO))
        {
            /*If PMTU Entry exists,Use the discovered PMTU as Mtu */
            u4MTU = pPmtuEntry->u4PMtu;
        }
    }

    /* Send it if it fits into mtu */
    if (pIp->u2Len <= (UINT2) u4MTU)
    {
        /* Depending on the return status, freeing the memory is done */
        IP_CXT_TRC_ARG4 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "s = %x, d = %x, len %d, sending on interface %d.\n",
                         pIp->u4Src, pIp->u4Dest, pIp->u2Len, u4IfaceIndex);

        return IpTxDatagram (pBuf, u2Rt_port, u4Rt_gw, (UINT1) i1Flag,
                             pIp->u2Len);
    }
    else
    {
        IP4SYS_INC_OUT_FRAG_REQDS (u4CxtId);
        IP4IF_INC_OUT_FRAG_REQDS (u2Rt_port);

        if ((IP_DF_SET (pIp->u2Fl_offs) == FALSE) ||
            ((pPmtuEntry != NULL) &&
             (pPmtuEntry->u1PmtuDiscFlag & IPPMTUDISC_ALLOW_FRAG)))

        {
            if (pPmtuEntry != NULL)
            {
                pPmtuEntry->u1PmtuDiscFlag &= (UINT1) (~IPPMTUDISC_ALLOW_FRAG);
            }

            /* FIX From PMT */
            if (ip_fragment_and_send
                (u2Rt_port, pIp, pBuf, u4Rt_gw, (UINT1) i1Flag,
                 u4MTU) == IP_FAILURE)
            {
                pIpCxt->Ip_stats.u4Out_frag_err++;
                IP4SYS_INC_OUT_FRAG_FAILS (u4CxtId);
                IP4IF_INC_OUT_FRAG_FAILS (u2Rt_port);

                IP_CXT_TRC_ARG3 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                                 "s = %x, d = %x, len %d, "
                                 "dropped (frag needed, DF set).\n",
                                 pIp->u4Src, pIp->u4Dest, pIp->u2Len);

                IPIF_INC_OUT_FRAG_ERR (u2Rt_port);

            }
        }
        else
        {
            t_ICMP              Icmp;
            pIpCxt->Ip_stats.u4Out_frag_err++;
            IP4SYS_INC_OUT_FRAG_FAILS (u4CxtId);
            IP4IF_INC_OUT_FRAG_FAILS (u2Rt_port);

            IP_CXT_TRC_ARG3 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "s = %x, d = %x, len %d, "
                             "dropped (frag needed, DF set).\n",
                             pIp->u4Src, pIp->u4Dest, pIp->u2Len);

            IPIF_INC_OUT_FRAG_ERR (u2Rt_port);
            if ((i1Flag == IP_BCAST) || (i1Flag == IP_MCAST))
            {
                return IP_FAILURE;
            }
            Icmp.i1Type = ICMP_DEST_UNREACH;
            Icmp.i1Code = ICMP_FRAG_NEEDED;
            Icmp.args.u4Unused = 0;
            ICMP_NH_MTU (Icmp) = (UINT2) u4MTU;
            Icmp.u4ContextId = u4CxtId;
            icmp_error_msg (pBuf, &Icmp);
            return IP_FAILURE;
        }
    }
    return IP_SUCCESS;
}
