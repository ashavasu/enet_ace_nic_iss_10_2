
 /* $Id: fsmpipwr.c,v 1.5 2011/10/25 09:53:33 siva Exp $ */

# include  "lr.h"
# include  "fssnmp.h"
# include  "ipinc.h"
# include  "fsmpipwr.h"
# include  "fsmpipdb.h"

VOID
RegisterFSMPIP ()
{
    SNMPRegisterMib (&fsmpipOID, &fsmpipEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmpipOID, (const UINT1 *) "fsmpip");
}

VOID
UnRegisterFSMPIP ()
{
    SNMPUnRegisterMib (&fsmpipOID, &fsmpipEntry);
    SNMPDelSysorEntry (&fsmpipOID, (const UINT1 *) "fsmpip");
}

INT4
FsMIFsIpGlobalDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsIpGlobalDebug (&(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpGlobalDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetFsMIFsIpGlobalDebug (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpGlobalDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhTestv2FsMIFsIpGlobalDebug (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpGlobalDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpGlobalDebug (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpGlobalTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpGlobalTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpGlobalTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpInLengthErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpInLengthErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIFsIpInCksumErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpInCksumErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIFsIpInVersionErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpInVersionErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpInTTLErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpInTTLErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpInOptionErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpInOptionErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpInBroadCastsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpInBroadCasts (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpOutGenErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpOutGenErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpOptProcEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpOptProcEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpNumMultipathGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpNumMultipath (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpLoadShareEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpLoadShareEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpEnablePMTUDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpEnablePMTUD (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuEntryAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpPmtuEntryAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpContextDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpContextDebug (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpOptProcEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpOptProcEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpNumMultipathSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpNumMultipath (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpLoadShareEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpLoadShareEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpEnablePMTUDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpEnablePMTUD (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuEntryAgeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpPmtuEntryAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpContextDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpContextDebug (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIFsIpOptProcEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpOptProcEnable (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIFsIpNumMultipathTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpNumMultipath (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpLoadShareEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpLoadShareEnable (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpEnablePMTUDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpEnablePMTUD (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuEntryAgeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpPmtuEntryAge (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpContextDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpContextDebug (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpGlobalTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpGlobalTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpTraceConfigTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    UDP_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpTraceConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpTraceConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpTraceConfigAdminStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceConfigAdminStatus (pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMaxTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceConfigMaxTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMinTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceConfigMinTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigOperStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceConfigOperStatus (pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceConfigTimeout (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceConfigMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigAdminStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpTraceConfigAdminStatus (pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMaxTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpTraceConfigMaxTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMinTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpTraceConfigMinTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpTraceConfigTimeout (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpTraceConfigMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpTraceConfigAdminStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMaxTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpTraceConfigMaxTTL (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMinTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpTraceConfigMinTTL (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpTraceConfigTimeout (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigMtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpTraceConfigMtu (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpTraceConfigTable (pu4Error, pSnmpIndexList,
                                          pSnmpvarbinds);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpTraceTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    UDP_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpTraceTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpTraceTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
FsMIFsIpTraceIntermHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceIntermHop (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->u4_ULongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceReachTime1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceReachTime1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceReachTime2Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceReachTime2 (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpTraceReachTime3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpTraceReachTime3 (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpAddressTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpAddressTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpAddressTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpAddrTabIfaceIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpAddrTabIfaceId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddrTabAdvertiseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpAddrTabAdvertise (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddrTabPreflevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpAddrTabPreflevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddrTabStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpAddressTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpAddrTabStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddrTabAdvertiseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpAddrTabAdvertise (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddrTabPreflevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpAddrTabPreflevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddrTabAdvertiseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpAddrTabAdvertise (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddrTabPreflevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpAddrTabPreflevel (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpAddressTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpAddressTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpRtrLstTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpRtrLstTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpRtrLstTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpRtrLstIfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRtrLstIface (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstPreflevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpRtrLstPreflevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstStaticGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRtrLstStatic (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRtrLstStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstIfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpRtrLstIface (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstPreflevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpRtrLstPreflevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpRtrLstStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstIfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRtrLstIface (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstPreflevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRtrLstPreflevel (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRtrLstStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRtrLstTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpRtrLstTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpPathMtuTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpPathMtuTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpPathMtuTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpPathMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpPathMtuTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpPathMtu (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuDiscGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpPathMtuTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpPmtuDisc (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuEntryStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpPathMtuTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpPmtuEntryStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPathMtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpPathMtu (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuDiscSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpPmtuDisc (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuEntryStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpPmtuEntryStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPathMtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpPathMtu (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuDiscTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpPmtuDisc (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPmtuEntryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpPmtuEntryStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpPathMtuTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpPathMtuTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpCommonRoutingTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    RTM_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpCommonRoutingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpCommonRoutingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue),
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue)) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpRouteProtoInstanceIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpRouteProtoInstanceId (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[3].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[4].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[5].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRouteIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRouteType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        pMultiIndex->pIndex[5].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRouteAge (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteNextHopASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpRouteNextHopAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteMetric1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRouteMetric1 (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRoutePreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpRoutePreference (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue,
         pMultiIndex->pIndex[5].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].u4_ULongValue,
                                          pMultiIndex->pIndex[5].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpRouteIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpRouteType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiIndex->pIndex[3].i4_SLongValue,
                                        pMultiIndex->pIndex[4].u4_ULongValue,
                                        pMultiIndex->pIndex[5].i4_SLongValue,
                                        pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteNextHopASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpRouteNextHopAS (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].i4_SLongValue,
                                      pMultiIndex->pIndex[4].u4_ULongValue,
                                      pMultiIndex->pIndex[5].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRoutePreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpRoutePreference (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].i4_SLongValue,
                                       pMultiIndex->pIndex[4].u4_ULongValue,
                                       pMultiIndex->pIndex[5].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIpRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].u4_ULongValue,
                                          pMultiIndex->pIndex[5].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRouteIfIndex (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[3].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[4].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[5].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRouteType (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].u4_ULongValue,
                                           pMultiIndex->pIndex[5].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteNextHopASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRouteNextHopAS (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[3].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[4].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[5].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRoutePreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRoutePreference (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[4].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[5].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpRouteStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[3].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[4].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[5].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpCommonRoutingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpCommonRoutingTable (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpifTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpifTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpifTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpifMaxReasmSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpifMaxReasmSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifIcmpRedirectEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpifIcmpRedirectEnable (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifDrtBcastFwdingEnableGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpifDrtBcastFwdingEnable (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifContextIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIpifContextId (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifMaxReasmSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpifMaxReasmSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifIcmpRedirectEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpifIcmpRedirectEnable (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifDrtBcastFwdingEnableSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpifDrtBcastFwdingEnable (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifMaxReasmSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpifMaxReasmSize (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifIcmpRedirectEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpifIcmpRedirectEnable (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifDrtBcastFwdingEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpifDrtBcastFwdingEnable (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpifTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIcmpGlobalTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIcmpGlobalTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIcmpGlobalTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpifProxyArpAdminStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpifProxyArpAdminStatus (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifProxyArpAdminStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpifProxyArpAdminStatus (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpifProxyArpAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpifProxyArpAdminStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;

}

INT4
FsMIFsIpifLocalProxyArpAdminStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIFsIpifLocalProxyArpAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIFsIpifLocalProxyArpAdminStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsMIFsIpifLocalProxyArpAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIFsIpifLocalProxyArpAdminStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIFsIpifLocalProxyArpAdminStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsMIFsIcmpSendRedirectEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpSendRedirectEnable (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendUnreachableEnableGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpSendUnreachableEnable (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendEchoReplyEnableGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpSendEchoReplyEnable (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpNetMaskReplyEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpNetMaskReplyEnable (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpTimeStampReplyEnableGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpTimeStampReplyEnable (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpInDomainNameRequestsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpInDomainNameRequests (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpInDomainNameReplyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpInDomainNameReply (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpOutDomainNameRequestsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpOutDomainNameRequests (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpOutDomainNameReplyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpOutDomainNameReply (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpDirectQueryEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpDirectQueryEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpDomainNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIcmpDomainName (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpTimeToLiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();

        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsIcmpTimeToLive (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpInSecurityFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpInSecurityFailures (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpOutSecurityFailuresGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpOutSecurityFailures (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendSecurityFailuresEnableGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpSendSecurityFailuresEnable (pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    &(pMultiData->
                                                      i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpRecvSecurityFailuresEnableGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIcmpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIcmpRecvSecurityFailuresEnable (pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    &(pMultiData->
                                                      i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendRedirectEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpSendRedirectEnable (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendUnreachableEnableSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpSendUnreachableEnable (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendEchoReplyEnableSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpSendEchoReplyEnable (pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpNetMaskReplyEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpNetMaskReplyEnable (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpTimeStampReplyEnableSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpTimeStampReplyEnable (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpDirectQueryEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpDirectQueryEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpDomainNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIcmpDomainName (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpTimeToLiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsIcmpTimeToLive (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendSecurityFailuresEnableSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpSendSecurityFailuresEnable (pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpRecvSecurityFailuresEnableSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIcmpRecvSecurityFailuresEnable (pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendRedirectEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpSendRedirectEnable (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendUnreachableEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpSendUnreachableEnable (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendEchoReplyEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpSendEchoReplyEnable (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpNetMaskReplyEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpNetMaskReplyEnable (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpTimeStampReplyEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpTimeStampReplyEnable (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpDirectQueryEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpDirectQueryEnable (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpDomainNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpDomainName (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpTimeToLiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpTimeToLive (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpSendSecurityFailuresEnableTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpSendSecurityFailuresEnable (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpRecvSecurityFailuresEnableTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIcmpRecvSecurityFailuresEnable (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIcmpGlobalTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIcmpGlobalTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsUdpGlobalTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsUdpGlobalTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsUdpGlobalTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsUdpInNoCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsUdpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsUdpInNoCksum (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsUdpInIcmpErrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsUdpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsUdpInIcmpErr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsUdpInErrCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsUdpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsUdpInErrCksum (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsUdpInBcastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsUdpGlobalTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsUdpInBcast (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIpCidrAggTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIpCidrAggTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIpCidrAggTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIpCidrAggStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIpCidrAggTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIpCidrAggStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpCidrAggStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIpCidrAggStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpCidrAggStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIpCidrAggStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpCidrAggTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIpCidrAggTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsCidrAdvertTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsCidrAdvertTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsCidrAdvertTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsCidrAdvertStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsCidrAdvertTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsCidrAdvertStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsCidrAdvertStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsCidrAdvertStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsCidrAdvertStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsCidrAdvertStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsCidrAdvertTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsCidrAdvertTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpInAdvertisementsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsIrdpInAdvertisements (&(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpInSolicitationsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsIrdpInSolicitations (&(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpOutAdvertisementsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsIrdpOutAdvertisements (&(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpOutSolicitationsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsIrdpOutSolicitations (&(pMultiData->u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpSendAdvertisementsEnableGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhGetFsMIFsIrdpSendAdvertisementsEnable (&(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpSendAdvertisementsEnableSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhSetFsMIFsIrdpSendAdvertisementsEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpSendAdvertisementsEnableTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhTestv2FsMIFsIrdpSendAdvertisementsEnable (pu4Error,
                                                     pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpSendAdvertisementsEnableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIrdpSendAdvertisementsEnable (pu4Error, pSnmpIndexList,
                                                    pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsIrdpIfConfTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsIrdpIfConfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsIrdpIfConfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsIrdpIfConfAdvertisementAddressGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIrdpIfConfAdvertisementAddress (pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    &(pMultiData->
                                                      u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfMaxAdvertisementIntervalGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIrdpIfConfMaxAdvertisementInterval (pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        &(pMultiData->
                                                          i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfMinAdvertisementIntervalGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIrdpIfConfMinAdvertisementInterval (pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        &(pMultiData->
                                                          i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfAdvertisementLifetimeGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIrdpIfConfAdvertisementLifetime (pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     &(pMultiData->
                                                       i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfPerformRouterDiscoveryGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIrdpIfConfPerformRouterDiscovery (pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      &(pMultiData->
                                                        i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfSolicitationAddressGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsIrdpIfConfSolicitationAddress (pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   &(pMultiData->
                                                     u4_ULongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfAdvertisementAddressSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIrdpIfConfAdvertisementAddress (pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfMaxAdvertisementIntervalSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIrdpIfConfMaxAdvertisementInterval (pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfMinAdvertisementIntervalSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIrdpIfConfMinAdvertisementInterval (pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfAdvertisementLifetimeSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIrdpIfConfAdvertisementLifetime (pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfPerformRouterDiscoverySet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIrdpIfConfPerformRouterDiscovery (pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfSolicitationAddressSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsIrdpIfConfSolicitationAddress (pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfAdvertisementAddressTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIrdpIfConfAdvertisementAddress (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfMaxAdvertisementIntervalTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIrdpIfConfMaxAdvertisementInterval (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  i4_SLongValue,
                                                                  pMultiIndex->
                                                                  pIndex[1].
                                                                  i4_SLongValue,
                                                                  pMultiData->
                                                                  i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfMinAdvertisementIntervalTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIrdpIfConfMinAdvertisementInterval (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  i4_SLongValue,
                                                                  pMultiIndex->
                                                                  pIndex[1].
                                                                  i4_SLongValue,
                                                                  pMultiData->
                                                                  i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfAdvertisementLifetimeTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIrdpIfConfAdvertisementLifetime (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfPerformRouterDiscoveryTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIrdpIfConfPerformRouterDiscovery (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                i4_SLongValue,
                                                                pMultiData->
                                                                i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfSolicitationAddressTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsIrdpIfConfSolicitationAddress (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             i4_SLongValue,
                                                             pMultiIndex->
                                                             pIndex[1].
                                                             i4_SLongValue,
                                                             pMultiData->
                                                             u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIrdpIfConfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsIrdpIfConfTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientRetransmissionTimeoutGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhGetFsMIFsRarpClientRetransmissionTimeout (&
                                                     (pMultiData->
                                                      i4_SLongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientMaxRetriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsRarpClientMaxRetries (&(pMultiData->i4_SLongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientPktsDiscardedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhGetFsMIFsRarpClientPktsDiscarded (&(pMultiData->u4_ULongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsRarpServerStatus (&(pMultiData->i4_SLongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerPktsDiscardedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhGetFsMIFsRarpServerPktsDiscarded (&(pMultiData->u4_ULongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerTableMaxEntriesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhGetFsMIFsRarpServerTableMaxEntries (&(pMultiData->i4_SLongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientRetransmissionTimeoutSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhSetFsMIFsRarpClientRetransmissionTimeout (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientMaxRetriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetFsMIFsRarpClientMaxRetries (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetFsMIFsRarpServerStatus (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerTableMaxEntriesSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhSetFsMIFsRarpServerTableMaxEntries (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientRetransmissionTimeoutTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhTestv2FsMIFsRarpClientRetransmissionTimeout (pu4Error,
                                                        pMultiData->
                                                        i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientMaxRetriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhTestv2FsMIFsRarpClientMaxRetries (pu4Error,
                                             pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhTestv2FsMIFsRarpServerStatus (pu4Error, pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerTableMaxEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal =
        nmhTestv2FsMIFsRarpServerTableMaxEntries (pu4Error,
                                                  pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientRetransmissionTimeoutDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsRarpClientRetransmissionTimeout (pu4Error, pSnmpIndexList,
                                                       pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpClientMaxRetriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsRarpClientMaxRetries (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsRarpServerStatus (pu4Error, pSnmpIndexList,
                                        pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerTableMaxEntriesDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsRarpServerTableMaxEntries (pu4Error, pSnmpIndexList,
                                                 pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsMIFsRarpServerDatabaseTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    ARP_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIFsRarpServerDatabaseTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            ARP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIFsRarpServerDatabaseTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            ARP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ARP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMIFsHardwareAddrLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsRarpServerDatabaseTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsHardwareAddrLen (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsProtocolAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsRarpServerDatabaseTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsMIFsProtocolAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->u4_ULongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsEntryStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsMIFsRarpServerDatabaseTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsMIFsEntryStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &(pMultiData->i4_SLongValue));

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsHardwareAddrLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsHardwareAddrLen (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsProtocolAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsMIFsProtocolAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->u4_ULongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsEntryStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsMIFsEntryStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsHardwareAddrLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsHardwareAddrLen (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsProtocolAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsProtocolAddress (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->u4_ULongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsEntryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsMIFsEntryStatus (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsRarpServerDatabaseTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsMIFsRarpServerDatabaseTable (pu4Error, pSnmpIndexList,
                                               pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpProxyArpSubnetOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsMIFsIpProxyArpSubnetOption
        (&(pMultiData->i4_SLongValue));
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpProxyArpSubnetOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetFsMIFsIpProxyArpSubnetOption (pMultiData->i4_SLongValue);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpProxyArpSubnetOptionTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhTestv2FsMIFsIpProxyArpSubnetOption
        (pu4Error, pMultiData->i4_SLongValue);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsMIFsIpProxyArpSubnetOptionDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsMIFsIpProxyArpSubnetOption
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}
